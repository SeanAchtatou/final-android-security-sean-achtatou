package com.saubcy.games.Docker.market;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import com.saubcy.games.Docker.market.ObjectFactory;

public class ShowFactory {
    protected static void drawDock(Canvas canvas, DockView dv) {
        Bitmap bitmap;
        canvas.drawColor(dv.parent.getResources().getColor(R.color.BackGround));
        for (int i = 0; i < LevelInfo.COL; i++) {
            for (int j = 0; j < LevelInfo.ROW; j++) {
                if (LevelInfo.WALL == dv.LevelData[j][i] && (bitmap = getBitmap(dv.LevelData[j][i], dv, false)) != null) {
                    canvas.drawBitmap(bitmap, dv.parent.UnitWidth * ((float) i), dv.parent.UnitHeight * ((float) j), (Paint) null);
                }
            }
        }
    }

    protected static void drawFlags(Canvas canvas, DockView dv) {
        int total = dv.FlagsList.size();
        for (int i = 0; i < total; i++) {
            ObjectFactory.Box flag = dv.FlagsList.get(i);
            canvas.drawBitmap(getBitmap(flag.code, dv, false), dv.parent.UnitWidth * ((float) flag.pos.col), dv.parent.UnitHeight * ((float) flag.pos.row), (Paint) null);
        }
    }

    protected static void drawBoxes(Canvas canvas, DockView dv) {
        Bitmap bitmap;
        int total = dv.BoxesList.size();
        for (int i = 0; i < total; i++) {
            ObjectFactory.Box box = dv.BoxesList.get(i);
            if (dv.ActiveBox != null && box.pos.col == dv.ActiveBox.pos.col && box.pos.row == dv.ActiveBox.pos.row) {
                bitmap = getBitmap(box.code, dv, true);
            } else {
                bitmap = getBitmap(box.code, dv, false);
            }
            canvas.drawBitmap(bitmap, dv.parent.UnitWidth * ((float) box.pos.col), dv.parent.UnitHeight * ((float) box.pos.row), (Paint) null);
        }
    }

    protected static void drawInfo(Canvas canvas, DockView dv) {
        String text;
        canvas.drawText(String.valueOf(dv.parent.getBaseContext().getResources().getString(R.string.info_level)) + (dv.level + 1), (float) dv.InfoXOffset, ((float) dv.InfoYOffset) + dv.RealTextHeight, dv.PTextInfo);
        canvas.drawText(String.valueOf(dv.parent.getBaseContext().getResources().getString(R.string.info_moves)) + dv.step, (float) dv.InfoXOffset, ((float) dv.InfoYOffset) + (dv.RealTextHeight * 2.0f), dv.PTextInfo);
        String text2 = dv.parent.getBaseContext().getResources().getString(R.string.info_best);
        if (dv.best == 0) {
            text = String.valueOf(text2) + "--";
        } else {
            text = String.valueOf(text2) + dv.best;
        }
        canvas.drawText(text, (float) dv.InfoXOffset, ((float) dv.InfoYOffset) + (dv.RealTextHeight * 3.0f), dv.PTextInfo);
    }

    protected static Bitmap getBitmap(int code, DockView dv, boolean flag) {
        if (LevelInfo.BLANK == code) {
            return null;
        }
        if (LevelInfo.WALL == code) {
            return dv.useWall;
        }
        if (code >= LevelInfo.BOX[0] && code <= LevelInfo.BOX[LevelInfo.BOX.length - 1]) {
            int i = 0;
            while (i < LevelInfo.BOX.length) {
                if (LevelInfo.BOX[i] != code) {
                    i++;
                } else if (!flag || dv.ActiveBox == null || dv.GameLock) {
                    return dv.useBoxes[i][4];
                } else {
                    return dv.useBoxes[i][dv.ActiveBox.angle];
                }
            }
            return null;
        } else if (code < LevelInfo.FLAG[0] || code > LevelInfo.FLAG[LevelInfo.FLAG.length - 1]) {
            return null;
        } else {
            for (int i2 = 0; i2 < LevelInfo.FLAG.length; i2++) {
                if (LevelInfo.FLAG[i2] == code) {
                    return dv.useFlags[i2];
                }
            }
            return null;
        }
    }

    public static boolean isBlock(int row, int col, DockView dv) {
        if (LevelInfo.BLANK == dv.LevelData[row][col]) {
            return false;
        }
        if (LevelInfo.WALL == dv.LevelData[row][col]) {
            return true;
        }
        if (dv.LevelData[row][col] >= LevelInfo.BOX[0] && dv.LevelData[row][col] <= LevelInfo.BOX[LevelInfo.BOX.length - 1]) {
            return true;
        }
        if (dv.LevelData[row][col] < LevelInfo.FLAG[0] || dv.LevelData[row][col] > LevelInfo.FLAG[LevelInfo.FLAG.length - 1]) {
            return true;
        }
        return false;
    }

    /* JADX INFO: Multiple debug info for r2v16 java.lang.String: [D('top' float), D('text' java.lang.String)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    protected static void darwWelcome(Canvas canvas, WelcomeView wv) {
        canvas.drawColor(wv.parent.getResources().getColor(R.color.WelcomBackGround));
        canvas.drawBitmap(Bitmap.createBitmap(wv.SawCloud, 0, 0, wv.SawCloud.getWidth(), wv.SawCloud.getHeight(), (Matrix) null, true), (float) wv.CloudOffset, (float) wv.YCloudOffset, (Paint) null);
        Bitmap bitmap = Bitmap.createBitmap(wv.SawBoard, 0, 0, wv.SawBoard.getWidth(), wv.SawBoard.getHeight(), wv.MatrixBoardRate, true);
        canvas.drawBitmap(bitmap, ((float) (wv.RealWidth - bitmap.getWidth())) / 2.0f, (float) (wv.YCloudOffset + wv.CloudHeight + wv.YBoardOffset), (Paint) null);
        Bitmap bitmap2 = Bitmap.createBitmap(wv.SawGrass, 0, 0, wv.SawGrass.getWidth(), wv.SawGrass.getHeight(), wv.MatrixGrassRate, true);
        canvas.drawBitmap(bitmap2, ((float) (wv.RealWidth - bitmap2.getWidth())) / 2.0f, (float) (wv.RealHeight - bitmap2.getHeight()), (Paint) null);
        String text = wv.parent.getBaseContext().getResources().getString(R.string.app_name);
        canvas.drawText(text, (((float) wv.RealWidth) - wv.pTitleOut.measureText(text)) / 2.0f, (float) wv.YTitleOffset, wv.pTitleOut);
        String text2 = wv.parent.getBaseContext().getResources().getString(R.string.welcome_start);
        canvas.drawText(text2, (((float) wv.RealWidth) - wv.pButton.measureText(text2)) / 2.0f, (((float) ((wv.YCloudOffset + wv.CloudHeight) + wv.YBoardOffset)) + ((wv.RealBoardHeight * 2.0f) / 3.0f)) - wv.RealTextHeight, wv.pButton);
        String text3 = wv.parent.getBaseContext().getResources().getString(R.string.welcome_help);
        canvas.drawText(text3, (((float) wv.RealWidth) - wv.pButton.measureText(text3)) / 2.0f, (((float) ((wv.YCloudOffset + wv.CloudHeight) + wv.YBoardOffset)) + wv.RealBoardHeight) - wv.RealTextHeight, wv.pButton);
    }

    public static class DockBuild implements Runnable {
        DockView parent = null;
        SurfaceHolder surfaceHolder;

        public DockBuild(DockView parent2) {
            this.parent = parent2;
            this.surfaceHolder = parent2.holder;
        }

        public void run() {
            Canvas canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas(null);
                synchronized (this.surfaceHolder) {
                    ShowFactory.drawDock(canvas, this.parent);
                    ShowFactory.drawFlags(canvas, this.parent);
                    ShowFactory.drawBoxes(canvas, this.parent);
                    ShowFactory.drawInfo(canvas, this.parent);
                }
                if (canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                } finally {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    protected static class BoxHeartBeat extends Thread {
        private final int intervals = 100;
        private DockView parent = null;
        private int state;
        SurfaceHolder surfaceHolder;

        public BoxHeartBeat(DockView parent2) {
            this.parent = parent2;
            this.surfaceHolder = parent2.holder;
            this.state = 1;
        }

        public void run() {
            Canvas canvas = null;
            ObjectFactory.Box box = this.parent.ActiveBox;
            box.angle = this.state % 4;
            while (this.parent.ActiveBox != null && box == this.parent.ActiveBox) {
                try {
                    canvas = this.surfaceHolder.lockCanvas(null);
                    synchronized (this.surfaceHolder) {
                        ShowFactory.drawDock(canvas, this.parent);
                        ShowFactory.drawFlags(canvas, this.parent);
                        ShowFactory.drawBoxes(canvas, this.parent);
                        ShowFactory.drawInfo(canvas, this.parent);
                    }
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                    } catch (Throwable th) {
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                        throw th;
                    }
                }
                this.state++;
                box.angle = this.state % 4;
                try {
                    sleep(100);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    protected static class BoxMove extends Thread {
        public static final int BOTTOM = 3;
        public static final int[] COLOPVALUE;
        public static final int LEFT = 0;
        public static final int RIGHT = 2;
        public static final int[] ROWOPVALUE;
        public static final int TOP = 1;
        private int direction;
        private DockView parent = null;
        SurfaceHolder surfaceHolder;

        static {
            int[] iArr = new int[4];
            iArr[1] = -1;
            iArr[3] = 1;
            ROWOPVALUE = iArr;
            int[] iArr2 = new int[4];
            iArr2[0] = -1;
            iArr2[2] = 1;
            COLOPVALUE = iArr2;
        }

        public BoxMove(DockView parent2, int direction2) {
            this.parent = parent2;
            this.surfaceHolder = parent2.holder;
            this.direction = direction2;
        }

        public void run() {
            Canvas canvas = null;
            if (this.parent.ActiveBox != null) {
                this.parent.GameLock = true;
                try {
                    canvas = this.surfaceHolder.lockCanvas(null);
                    synchronized (this.surfaceHolder) {
                        while (!ShowFactory.isBlock(this.parent.ActiveBox.pos.row + ROWOPVALUE[this.direction], this.parent.ActiveBox.pos.col + COLOPVALUE[this.direction], this.parent)) {
                            this.parent.LevelData[this.parent.ActiveBox.pos.row + ROWOPVALUE[this.direction]][this.parent.ActiveBox.pos.col + COLOPVALUE[this.direction]] = this.parent.LevelData[this.parent.ActiveBox.pos.row][this.parent.ActiveBox.pos.col];
                            this.parent.LevelData[this.parent.ActiveBox.pos.row][this.parent.ActiveBox.pos.col] = LevelInfo.BLANK;
                            this.parent.ActiveBox.pos.row += ROWOPVALUE[this.direction];
                            this.parent.ActiveBox.pos.col += COLOPVALUE[this.direction];
                        }
                        ShowFactory.drawDock(canvas, this.parent);
                        ShowFactory.drawFlags(canvas, this.parent);
                        ShowFactory.drawBoxes(canvas, this.parent);
                        ShowFactory.drawInfo(canvas, this.parent);
                    }
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                        this.parent.setBoxesState(this.parent.ActiveBox);
                        this.parent.GameLock = false;
                        this.parent.moh.sendEmptyMessage(0);
                    }
                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                    } finally {
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                            this.parent.setBoxesState(this.parent.ActiveBox);
                            this.parent.GameLock = false;
                            this.parent.moh.sendEmptyMessage(0);
                        }
                    }
                }
            }
        }
    }

    protected static class CloudMove extends Thread {
        private final int intervals = 150;
        private WelcomeView parent = null;
        SurfaceHolder surfaceHolder;

        public CloudMove(WelcomeView parent2) {
            this.parent = parent2;
            this.surfaceHolder = parent2.holder;
        }

        public void run() {
            Canvas canvas = null;
            while (1 == this.parent.isShow) {
                try {
                    canvas = this.surfaceHolder.lockCanvas(null);
                    synchronized (this.surfaceHolder) {
                        ShowFactory.darwWelcome(canvas, this.parent);
                    }
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                    } catch (Throwable th) {
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                        throw th;
                    }
                }
                this.parent.CloudOffset += this.parent.CloudStep;
                if (this.parent.CloudOffset >= this.parent.parent.Width + (this.parent.CloudWidth * 2)) {
                    this.parent.CloudOffset = this.parent.CloudWidth * -1;
                    this.parent.CloudOffset += this.parent.CloudStep;
                } else {
                    try {
                        sleep(150);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }
}
