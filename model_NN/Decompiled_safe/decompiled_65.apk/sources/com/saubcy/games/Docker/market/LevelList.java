package com.saubcy.games.Docker.market;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.saubcy.games.Docker.market.ObjectFactory;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LevelList extends Activity implements UpdatePointsNotifier {
    public static final String LEVEL_LIST_DATA = "com.saubcy.games.Docker.LEVEL_LIST_DATA";
    public static final String LEVEL_SELECT = "com.saubcy.games.Docker.LEVEL_SELECT";
    public static final String LEVEL_STATE = "com.saubcy.games.Docker.LEVEL_STATE";
    public static final String LEVEL_STEP = "com.saubcy.games.Docker.LEVEL_STEP";
    public static final int LOCKED = 0;
    public static final int PRICE = 50;
    public static final int UNLOCK = 1;
    private int Gold = 50;
    private SharedPreferences LevelInfoHander = null;
    /* access modifiers changed from: private */
    public List<Map<String, Object>> LevelStates = null;
    private SimpleAdapter adapter = null;
    /* access modifiers changed from: private */
    public int isUnlockState = 0;
    private ListView lv_LevelList = null;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Toast.makeText(LevelList.this.getApplicationContext(), LevelList.this.getBaseContext().getResources().getString(R.string.gold_got_fail), 0).show();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public int unlockedLevel = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        loadViews();
        showLevelState();
    }

    private void init() {
        AppConnect.getInstance(this).getPoints(this);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFlags(128, 128);
        setRequestedOrientation(1);
    }

    private void showLevelState() {
        this.LevelStates = loadLevelState();
        this.adapter = new SimpleAdapter(this, this.LevelStates, R.layout.level_list_row, new String[]{"image", "name", "step"}, new int[]{R.id.im_lock_flag, R.id.tv_level_rank, R.id.tv_level_step});
        this.lv_LevelList.setAdapter((ListAdapter) this.adapter);
    }

    private void loadViews() {
        setContentView((int) R.layout.level_list);
        this.lv_LevelList = (ListView) findViewById(R.id.lv_levle_list);
        this.lv_LevelList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Map<String, Object> dataRow = (Map) LevelList.this.LevelStates.get(position);
                int state = Integer.parseInt((String) dataRow.get("state"));
                int level = Integer.parseInt((String) dataRow.get("level"));
                if (state == 0) {
                    LevelList.this.showUnlockDialog(level);
                } else {
                    LevelList.this.startLevel(level);
                }
            }
        });
        this.LevelInfoHander = getSharedPreferences(LEVEL_LIST_DATA, 0);
    }

    /* access modifiers changed from: private */
    public void showUnlockDialog(int level) {
        this.unlockedLevel = level;
        if (this.Gold <= 50) {
            new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.unlock_nogold)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.sensor_open_agree), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    AppConnect.getInstance(LevelList.this).showOffers(LevelList.this);
                }
            }).setNegativeButton(getBaseContext().getResources().getString(R.string.sensor_open_not_agree), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            }).show();
            return;
        }
        new AlertDialog.Builder(this).setMessage(String.valueOf(getBaseContext().getResources().getString(R.string.unlock_check_pre)) + " " + this.Gold + getBaseContext().getResources().getString(R.string.unlock_check_suffix) + ", " + getBaseContext().getResources().getString(R.string.unlock_check) + 50 + getBaseContext().getResources().getString(R.string.unlock_check_suffix_2)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.sensor_open_agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (LevelList.this.unlockedLevel < LevelInfo.LEVEL.length - 1) {
                    LevelList.this.isUnlockState = 1;
                    AppConnect.getInstance(LevelList.this).spendPoints(50, LevelList.this);
                }
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.sensor_open_not_agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).show();
    }

    private List<Map<String, Object>> loadLevelState() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < LevelInfo.LEVEL.length; i++) {
            Map<String, Object> map = new HashMap<>();
            ObjectFactory.LevelInfo info = getLevelInfo(this.LevelInfoHander, i);
            if (info.state == 0) {
                map.put("image", Integer.valueOf((int) R.drawable.locker));
            } else {
                map.put("image", Integer.valueOf((int) R.drawable.star));
            }
            map.put("name", String.valueOf(getBaseContext().getResources().getString(R.string.level_name_prefix)) + " " + (i + 1));
            map.put("level", new StringBuilder(String.valueOf(i)).toString());
            map.put("state", new StringBuilder(String.valueOf(info.state)).toString());
            if (info.step == 0) {
                map.put("step", String.valueOf(getBaseContext().getResources().getString(R.string.step_info_prefix)) + "--");
            } else {
                map.put("step", String.valueOf(getBaseContext().getResources().getString(R.string.step_info_prefix)) + " " + info.step);
            }
            list.add(map);
        }
        return list;
    }

    /* access modifiers changed from: protected */
    public void startLevel(int level) {
        Intent intent = new Intent(this, Dock.class);
        intent.putExtra(LEVEL_SELECT, level);
        startActivity(intent);
    }

    public static ObjectFactory.LevelInfo getLevelInfo(SharedPreferences content, int level) {
        ObjectFactory.LevelInfo info = new ObjectFactory.LevelInfo(level);
        if (level == 0) {
            info.state = 1;
        } else {
            info.state = content.getInt(LEVEL_STATE + level, 0);
        }
        info.step = content.getInt(LEVEL_STEP + level, 0);
        return info;
    }

    public static void setLevelInfo(SharedPreferences content, ObjectFactory.LevelInfo info) {
        SharedPreferences.Editor editor = content.edit();
        editor.putInt(LEVEL_STATE + info.level, info.state);
        editor.putInt(LEVEL_STEP + info.level, info.step);
        editor.commit();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        showLevelState();
    }

    public void getUpdatePoints(String arg0, int arg1) {
        Log.d("trace", "getUpdatePoints(" + arg0 + "," + arg1 + ")");
        if (arg1 < this.Gold && 1 == this.isUnlockState) {
            this.isUnlockState = 0;
            ObjectFactory.LevelInfo info = getLevelInfo(this.LevelInfoHander, this.unlockedLevel);
            info.state = 1;
            setLevelInfo(this.LevelInfoHander, info);
            startLevel(this.unlockedLevel);
        }
        this.Gold = arg1;
    }

    public void getUpdatePointsFailed(String arg0) {
        Log.d("trace", "getUpdatePointsFailed(" + arg0 + ")");
        this.isUnlockState = 0;
        Message msg = new Message();
        msg.what = 1;
        this.mHandler.sendMessage(msg);
    }
}
