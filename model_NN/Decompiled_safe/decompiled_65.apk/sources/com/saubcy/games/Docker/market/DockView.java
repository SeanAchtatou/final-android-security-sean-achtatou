package com.saubcy.games.Docker.market;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.saubcy.games.Docker.market.ObjectFactory;
import com.saubcy.games.Docker.market.ShowFactory;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class DockView extends SurfaceView implements SurfaceHolder.Callback, GestureDetector.OnGestureListener {
    public static final int BaseHeight = 480;
    public static final int BaseWidth = 320;
    protected static final int[] BoxesId = {R.drawable.box_0, R.drawable.box_1, R.drawable.box_2, R.drawable.box_3, R.drawable.box_4};
    protected static final int[] FlagsId = {R.drawable.flag_0, R.drawable.flag_1, R.drawable.flag_2, R.drawable.flag_3, R.drawable.flag_4};
    protected static int[] Rotate = null;
    protected static final int WallId = 2130837528;
    ObjectFactory.Box ActiveBox = null;
    protected int BaseInfoTextSize = 20;
    protected final float BaseScale = 0.65f;
    protected List<ObjectFactory.Box> BoxesList = null;
    protected List<ObjectFactory.Box> FlagsList = null;
    protected boolean GameLock = false;
    protected int InfoXOffset = 5;
    protected int InfoYOffset = 5;
    protected int[][] LevelData = ((int[][]) Array.newInstance(Integer.TYPE, LevelInfo.ROW, LevelInfo.COL));
    protected Matrix MatrixRate = null;
    protected Paint PTextInfo = null;
    protected int RealHeight = 480;
    protected float RealTextHeight = 0.0f;
    protected int RealWidth = 320;
    protected float ScreenHeightScale;
    protected float ScreenWidthScale;
    protected int best = 0;
    protected GestureDetector gd = new GestureDetector(this);
    protected SurfaceHolder holder;
    protected int level = 4;
    protected MoveOverHandler moh = null;
    protected Dock parent = null;
    protected int step = 0;
    protected Bitmap[][] useBoxes = null;
    protected Bitmap[] useFlags = null;
    protected Bitmap useWall = null;

    static {
        int[] iArr = new int[4];
        iArr[0] = 270;
        iArr[1] = 180;
        iArr[2] = 90;
        Rotate = iArr;
    }

    public DockView(Context context) {
        super(context);
        this.parent = (Dock) context;
        init();
    }

    public DockView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.parent = (Dock) context;
        init();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int wMeasureSpec, int hMeasureSpec) {
        setMeasuredDimension(this.parent.Width, this.parent.Height);
    }

    private void init() {
        this.holder = getHolder();
        this.holder.addCallback(this);
        this.RealWidth = this.parent.Width;
        this.RealHeight = this.parent.Height;
        this.ScreenWidthScale = ((float) this.RealWidth) / 320.0f;
        this.ScreenHeightScale = ((float) this.RealHeight) / 480.0f;
        this.moh = new MoveOverHandler(this);
        this.PTextInfo = new Paint(1);
        this.PTextInfo.setTextSize(((float) this.BaseInfoTextSize) * this.ScreenWidthScale);
        this.PTextInfo.setColor(getResources().getColor(R.color.InfoTextColor));
        Paint.FontMetrics fm = this.PTextInfo.getFontMetrics();
        this.RealTextHeight = (float) Math.ceil((double) (fm.descent - fm.ascent));
        loadImages();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void loadImages() {
        this.MatrixRate = new Matrix();
        this.MatrixRate.postScale(0.65f * this.ScreenWidthScale, 0.65f * this.ScreenHeightScale);
        Bitmap[] SawBoxes = new Bitmap[BoxesId.length];
        this.useBoxes = (Bitmap[][]) Array.newInstance(Bitmap.class, BoxesId.length, 5);
        for (int i = 0; i < BoxesId.length; i++) {
            SawBoxes[i] = ((BitmapDrawable) getResources().getDrawable(BoxesId[i])).getBitmap();
            for (int j = 0; j < 4; j++) {
                Matrix TempMatrix = new Matrix(this.MatrixRate);
                TempMatrix.postRotate((float) Rotate[j % 4]);
                this.useBoxes[i][j] = Bitmap.createBitmap(SawBoxes[i], 0, 0, SawBoxes[i].getWidth(), SawBoxes[i].getHeight(), TempMatrix, true);
            }
            this.useBoxes[i][4] = Bitmap.createBitmap(SawBoxes[i], 0, 0, SawBoxes[i].getWidth(), SawBoxes[i].getHeight(), this.MatrixRate, true);
        }
        Bitmap[] SawFlags = new Bitmap[FlagsId.length];
        this.useFlags = new Bitmap[FlagsId.length];
        for (int i2 = 0; i2 < FlagsId.length; i2++) {
            SawFlags[i2] = ((BitmapDrawable) getResources().getDrawable(FlagsId[i2])).getBitmap();
            this.useFlags[i2] = Bitmap.createBitmap(SawFlags[i2], 0, 0, SawFlags[i2].getWidth(), SawFlags[i2].getHeight(), this.MatrixRate, true);
        }
        Bitmap SawWall = ((BitmapDrawable) getResources().getDrawable(R.drawable.wall)).getBitmap();
        this.useWall = Bitmap.createBitmap(SawWall, 0, 0, SawWall.getWidth(), SawWall.getHeight(), this.MatrixRate, true);
    }

    private void loadLevelData(int level2) {
        this.BoxesList = new ArrayList();
        this.FlagsList = new ArrayList();
        for (int i = 0; i < LevelInfo.COL; i++) {
            for (int j = 0; j < LevelInfo.ROW; j++) {
                this.LevelData[j][i] = LevelInfo.LEVEL[level2][j][i];
                if (this.LevelData[j][i] >= LevelInfo.BOX[0] && this.LevelData[j][i] <= LevelInfo.BOX[LevelInfo.BOX.length - 1]) {
                    int count = 0;
                    while (true) {
                        if (count >= LevelInfo.BOX.length) {
                            break;
                        } else if (LevelInfo.BOX[count] == this.LevelData[j][i]) {
                            this.BoxesList.add(new ObjectFactory.Box(j, i, count, this.LevelData[j][i]));
                            break;
                        } else {
                            count++;
                        }
                    }
                } else if (this.LevelData[j][i] >= LevelInfo.FLAG[0] && this.LevelData[j][i] <= LevelInfo.FLAG[LevelInfo.FLAG.length - 1]) {
                    int count2 = 0;
                    while (true) {
                        if (count2 >= LevelInfo.FLAG.length) {
                            break;
                        } else if (LevelInfo.FLAG[count2] == this.LevelData[j][i]) {
                            this.FlagsList.add(new ObjectFactory.Box(j, i, count2, this.LevelData[j][i]));
                            break;
                        } else {
                            count2++;
                        }
                    }
                }
            }
        }
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        if (this.parent.BoxesList == null) {
            newGame();
            return;
        }
        new Thread(new ShowFactory.DockBuild(this)).start();
        this.parent.BoxesList = null;
        this.parent.FlagsList = null;
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
    }

    /* access modifiers changed from: protected */
    public void newGame() {
        this.step = 0;
        this.level %= LevelInfo.LEVEL.length;
        this.best = getLevelInfo(this.level).step;
        loadLevelData(this.level);
        new Thread(new ShowFactory.DockBuild(this)).start();
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.gd.onTouchEvent(event);
    }

    private ObjectFactory.Box getBoxByCoordinate(float x, float y) {
        int row = (int) (y / this.parent.UnitHeight);
        int col = (int) (x / this.parent.UnitWidth);
        int total = this.BoxesList.size();
        for (int i = 0; i < total; i++) {
            ObjectFactory.Box TempBox = this.BoxesList.get(i);
            if (row == TempBox.pos.row && col == TempBox.pos.col) {
                return TempBox;
            }
        }
        return null;
    }

    public boolean onDown(MotionEvent e) {
        if (this.ActiveBox != null) {
            return true;
        }
        return false;
    }

    public void onShowPress(MotionEvent e) {
        if (!this.GameLock) {
            this.ActiveBox = getBoxByCoordinate(e.getX(), e.getY());
            if (this.ActiveBox != null) {
                this.parent.vibratorBack();
                new Thread(new ShowFactory.BoxHeartBeat(this)).start();
            }
        }
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (this.ActiveBox == null) {
            return false;
        }
        if (this.parent.OpMode != 0) {
            return false;
        }
        if (e2.getX() - e1.getX() <= 100.0f || Math.abs(velocityX) <= 200.0f) {
            if (e2.getY() - e1.getY() <= 100.0f || Math.abs(velocityY) <= 200.0f) {
                if (e1.getX() - e2.getX() <= 50.0f || Math.abs(velocityX) <= 100.0f) {
                    if (e1.getY() - e2.getY() > 33.0f && Math.abs(velocityY) > 66.0f && !ShowFactory.isBlock(this.ActiveBox.pos.row + ShowFactory.BoxMove.ROWOPVALUE[1], this.ActiveBox.pos.col + ShowFactory.BoxMove.COLOPVALUE[1], this)) {
                        new Thread(new ShowFactory.BoxMove(this, 1)).start();
                    }
                } else if (!ShowFactory.isBlock(this.ActiveBox.pos.row + ShowFactory.BoxMove.ROWOPVALUE[0], this.ActiveBox.pos.col + ShowFactory.BoxMove.COLOPVALUE[0], this)) {
                    new Thread(new ShowFactory.BoxMove(this, 0)).start();
                }
            } else if (!ShowFactory.isBlock(this.ActiveBox.pos.row + ShowFactory.BoxMove.ROWOPVALUE[3], this.ActiveBox.pos.col + ShowFactory.BoxMove.COLOPVALUE[3], this)) {
                new Thread(new ShowFactory.BoxMove(this, 3)).start();
            }
        } else if (!ShowFactory.isBlock(this.ActiveBox.pos.row + ShowFactory.BoxMove.ROWOPVALUE[2], this.ActiveBox.pos.col + ShowFactory.BoxMove.COLOPVALUE[2], this)) {
            new Thread(new ShowFactory.BoxMove(this, 2)).start();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean checkFinish() {
        this.step++;
        int total = this.BoxesList.size();
        for (int i = 0; i < total; i++) {
            ObjectFactory.Box box = this.BoxesList.get(i);
            if (box.state == 0 && box.code != LevelInfo.BOX[0]) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void setBoxesState(ObjectFactory.Box box) {
        if (box != null) {
            int total = this.FlagsList.size();
            this.parent.vibratorBack();
            for (int i = 0; i < total; i++) {
                ObjectFactory.Box flag = this.FlagsList.get(i);
                if (flag != null && box.pos.row == flag.pos.row && box.pos.col == flag.pos.col && box.color == flag.color) {
                    box.state = 1;
                    this.ActiveBox = null;
                    return;
                }
            }
            box.state = 0;
        }
    }

    protected class MoveOverHandler extends Handler {
        private DockView parent;

        MoveOverHandler(DockView parent2) {
            this.parent = parent2;
        }

        public void handleMessage(Message msg) {
            if (DockView.this.checkFinish()) {
                DockView.this.setLevelInfo(DockView.this.level, DockView.this.step);
                if (DockView.this.level < LevelInfo.LEVEL.length - 1) {
                    ObjectFactory.LevelInfo info = DockView.this.getLevelInfo(DockView.this.level + 1);
                    DockView.this.setLevelInfo(info.level, info.step);
                    ObjectFactory.LevelInfo unused = DockView.this.getLevelInfo(DockView.this.level + 1);
                }
                this.parent.parent.showFinishDialog();
                DockView.this.level++;
            }
        }
    }

    /* access modifiers changed from: private */
    public ObjectFactory.LevelInfo getLevelInfo(int level2) {
        return LevelList.getLevelInfo(this.parent.LevelInfoHander, level2);
    }

    /* access modifiers changed from: private */
    public void setLevelInfo(int level2, int step2) {
        ObjectFactory.LevelInfo info = new ObjectFactory.LevelInfo(level2);
        ObjectFactory.LevelInfo infoOld = getLevelInfo(level2);
        info.state = 1;
        if (infoOld.step == 0 || step2 < infoOld.step) {
            info.step = step2;
        } else {
            info.step = infoOld.step;
        }
        LevelList.setLevelInfo(this.parent.LevelInfoHander, info);
    }
}
