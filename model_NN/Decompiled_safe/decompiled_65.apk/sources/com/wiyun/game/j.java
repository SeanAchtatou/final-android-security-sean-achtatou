package com.wiyun.game;

import com.wiyun.game.f.a;
import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.protocol.HttpContext;

class j implements HttpResponseInterceptor {
    j() {
    }

    public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
        Header ceheader = response.getEntity().getContentEncoding();
        if (ceheader != null) {
            HeaderElement[] codecs = ceheader.getElements();
            for (HeaderElement name : codecs) {
                if (name.getName().equalsIgnoreCase("gzip")) {
                    response.setEntity(new a(response.getEntity()));
                    return;
                }
            }
        }
    }
}
