package com.wiyun.game;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.FaceDetector;
import android.widget.Toast;
import com.wiyun.game.d.g;

class a implements Runnable {
    float a = 1.0f;
    Matrix b;
    FaceDetector.Face[] c = new FaceDetector.Face[3];
    int d;
    /* access modifiers changed from: package-private */
    public final /* synthetic */ CropImage e;

    a(CropImage cropImage) {
        this.e = cropImage;
    }

    /* access modifiers changed from: private */
    public void a() {
        g hv = new g(this.e.p);
        int width = this.e.r.getWidth();
        int height = this.e.r.getHeight();
        Rect imageRect = new Rect(0, 0, width, height);
        int cropWidth = (Math.min(width, height) * 4) / 5;
        int cropHeight = cropWidth;
        if (!(this.e.g == 0 || this.e.h == 0)) {
            if (this.e.g > this.e.h) {
                cropHeight = (this.e.h * cropWidth) / this.e.g;
            } else {
                cropWidth = (this.e.g * cropHeight) / this.e.h;
            }
        }
        int x = (width - cropWidth) / 2;
        int y = (height - cropHeight) / 2;
        hv.a(this.b, imageRect, new RectF((float) x, (float) y, (float) (x + cropWidth), (float) (y + cropHeight)), this.e.i, (this.e.g == 0 || this.e.h == 0) ? false : true);
        this.e.p.a(hv);
    }

    /* access modifiers changed from: private */
    public void a(FaceDetector.Face f) {
        PointF midPoint = new PointF();
        int r = ((int) (f.eyesDistance() * this.a)) * 2;
        f.getMidPoint(midPoint);
        midPoint.x *= this.a;
        midPoint.y *= this.a;
        int midX = (int) midPoint.x;
        int midY = (int) midPoint.y;
        g hv = new g(this.e.p);
        Rect imageRect = new Rect(0, 0, this.e.r.getWidth(), this.e.r.getHeight());
        RectF faceRect = new RectF((float) midX, (float) midY, (float) midX, (float) midY);
        faceRect.inset((float) (-r), (float) (-r));
        if (faceRect.left < 0.0f) {
            faceRect.inset(-faceRect.left, -faceRect.left);
        }
        if (faceRect.top < 0.0f) {
            faceRect.inset(-faceRect.top, -faceRect.top);
        }
        if (faceRect.right > ((float) imageRect.right)) {
            faceRect.inset(faceRect.right - ((float) imageRect.right), faceRect.right - ((float) imageRect.right));
        }
        if (faceRect.bottom > ((float) imageRect.bottom)) {
            faceRect.inset(faceRect.bottom - ((float) imageRect.bottom), faceRect.bottom - ((float) imageRect.bottom));
        }
        hv.a(this.b, imageRect, faceRect, this.e.i, (this.e.g == 0 || this.e.h == 0) ? false : true);
        this.e.p.a(hv);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap b() {
        if (this.e.r == null) {
            return null;
        }
        if (this.e.r.getWidth() > 256) {
            this.a = 256.0f / ((float) this.e.r.getWidth());
        }
        Matrix matrix = new Matrix();
        matrix.setScale(this.a, this.a);
        return Bitmap.createBitmap(this.e.r, 0, 0, this.e.r.getWidth(), this.e.r.getHeight(), matrix, true);
    }

    public void run() {
        this.b = this.e.p.getImageMatrix();
        Bitmap faceBitmap = b();
        this.a = 1.0f / this.a;
        if (faceBitmap != null && this.e.o) {
            this.d = new FaceDetector(faceBitmap.getWidth(), faceBitmap.getHeight(), this.c.length).findFaces(faceBitmap, this.c);
        }
        if (!(faceBitmap == null || faceBitmap == this.e.r)) {
            faceBitmap.recycle();
        }
        this.e.j.post(new Runnable() {
            public void run() {
                a.this.e.a = a.this.d > 1;
                if (a.this.d > 0) {
                    for (int i = 0; i < a.this.d; i++) {
                        a.this.a(a.this.c[i]);
                    }
                } else {
                    a.this.a();
                }
                a.this.e.p.invalidate();
                if (a.this.e.p.l.size() == 1) {
                    a.this.e.c = a.this.e.p.l.get(0);
                    a.this.e.c.a(true);
                }
                if (a.this.d > 1) {
                    Toast.makeText(a.this.e, "Multi face crop help", 0).show();
                }
            }
        });
    }
}
