package com.wiyun.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.wiyun.game.d.e;
import com.wiyun.game.d.g;
import java.util.ArrayList;
import java.util.Iterator;

class CropImageView extends e {
    ArrayList<g> l = new ArrayList<>();
    g m = null;
    float n;
    float o;
    int p;
    private Context q;

    public CropImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.q = context;
    }

    private void a(MotionEvent event) {
        for (int i = 0; i < this.l.size(); i++) {
            g hv = this.l.get(i);
            hv.a(false);
            hv.c();
        }
        int i2 = 0;
        while (true) {
            if (i2 >= this.l.size()) {
                break;
            }
            g hv2 = this.l.get(i2);
            if (hv2.a(event.getX(), event.getY()) == 1) {
                i2++;
            } else if (!hv2.a()) {
                hv2.a(true);
                hv2.c();
            }
        }
        invalidate();
    }

    private void b(g hv) {
        Rect r = hv.d;
        int panDeltaX1 = Math.max(0, this.g - r.left);
        int panDeltaX2 = Math.min(0, this.h - r.right);
        int panDeltaY1 = Math.max(0, this.i - r.top);
        int panDeltaY2 = Math.min(0, this.j - r.bottom);
        int panDeltaX = panDeltaX1 != 0 ? panDeltaX1 : panDeltaX2;
        int panDeltaY = panDeltaY1 != 0 ? panDeltaY1 : panDeltaY2;
        if (panDeltaX != 0 || panDeltaY != 0) {
            b((float) panDeltaX, (float) panDeltaY);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private void c(g gVar) {
        Rect rect = gVar.d;
        float max = Math.max(1.0f, Math.min((((float) getWidth()) / ((float) rect.width())) * 0.6f, (((float) getHeight()) / ((float) rect.height())) * 0.6f) * a());
        if (((double) (Math.abs(max - a()) / max)) > 0.1d) {
            float[] fArr = {gVar.e.centerX(), gVar.e.centerY()};
            getImageMatrix().mapPoints(fArr);
            a(max, fArr[0], fArr[1], 300.0f);
        }
        b(gVar);
    }

    /* access modifiers changed from: protected */
    public void a(float deltaX, float deltaY) {
        super.a(deltaX, deltaY);
        for (int i = 0; i < this.l.size(); i++) {
            g hv = this.l.get(i);
            hv.f.postTranslate(deltaX, deltaY);
            hv.c();
        }
    }

    /* access modifiers changed from: protected */
    public void a(float scale, float centerX, float centerY) {
        super.a(scale, centerX, centerY);
        Iterator<g> it = this.l.iterator();
        while (it.hasNext()) {
            g hv = it.next();
            hv.f.set(getImageMatrix());
            hv.c();
        }
    }

    public void a(g hv) {
        this.l.add(hv);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < this.l.size(); i++) {
            this.l.get(i).a(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (this.c.b() != null) {
            Iterator<g> it = this.l.iterator();
            while (it.hasNext()) {
                g hv = it.next();
                hv.f.set(getImageMatrix());
                hv.c();
                if (hv.b) {
                    c(hv);
                }
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.d.e.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wiyun.game.CropImageView.a(float, float):void
      com.wiyun.game.d.e.a(android.graphics.Bitmap, int):void
      com.wiyun.game.d.e.a(com.wiyun.game.d.c, android.graphics.Matrix):void
      com.wiyun.game.d.e.a(android.graphics.Matrix, int):float
      com.wiyun.game.d.e.a(float, float):void
      com.wiyun.game.d.e.a(android.graphics.Bitmap, boolean):void
      com.wiyun.game.d.e.a(com.wiyun.game.d.c, boolean):void
      com.wiyun.game.d.e.a(boolean, boolean):void */
    public boolean onTouchEvent(MotionEvent event) {
        CropImage cropImage = (CropImage) this.q;
        if (cropImage.b) {
            return false;
        }
        switch (event.getAction()) {
            case 0:
                if (cropImage.a) {
                    a(event);
                    break;
                } else {
                    int i = 0;
                    while (true) {
                        if (i >= this.l.size()) {
                            break;
                        } else {
                            g hv = this.l.get(i);
                            int edge = hv.a(event.getX(), event.getY());
                            if (edge != 1) {
                                this.p = edge;
                                this.m = hv;
                                this.n = event.getX();
                                this.o = event.getY();
                                this.m.a(edge == 32 ? g.a.Move : g.a.Grow);
                                break;
                            } else {
                                i++;
                            }
                        }
                    }
                }
            case 1:
                if (cropImage.a) {
                    for (int i2 = 0; i2 < this.l.size(); i2++) {
                        g hv2 = this.l.get(i2);
                        if (hv2.a()) {
                            cropImage.c = hv2;
                            for (int j = 0; j < this.l.size(); j++) {
                                if (j != i2) {
                                    this.l.get(j).b(true);
                                }
                            }
                            c(hv2);
                            ((CropImage) this.q).a = false;
                            return true;
                        }
                    }
                } else if (this.m != null) {
                    c(this.m);
                    this.m.a(g.a.None);
                }
                this.m = null;
                break;
            case 2:
                if (!cropImage.a) {
                    if (this.m != null) {
                        this.m.a(this.p, event.getX() - this.n, event.getY() - this.o);
                        this.n = event.getX();
                        this.o = event.getY();
                        b(this.m);
                        break;
                    }
                } else {
                    a(event);
                    break;
                }
                break;
        }
        switch (event.getAction()) {
            case 1:
                a(true, true);
                break;
            case 2:
                if (a() == 1.0f) {
                    a(true, true);
                    break;
                }
                break;
        }
        return true;
    }
}
