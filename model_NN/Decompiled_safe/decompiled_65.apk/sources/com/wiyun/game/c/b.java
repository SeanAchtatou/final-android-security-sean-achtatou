package com.wiyun.game.c;

import android.app.Activity;
import android.content.Context;
import com.wiyun.game.WiGame;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.util.EncodingUtils;

public class b extends f {
    private static final byte[] i = EncodingUtils.getAsciiBytes("; filename=");
    private g j;

    public b(String name, g partSource) {
        this(name, partSource, null, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(String name, g partSource, String contentType, String charset) {
        super(name, contentType == null ? "application/octet-stream" : contentType, charset == null ? "ISO-8859-1" : charset, "binary");
        if (partSource == null) {
            throw new IllegalArgumentException("Source may not be null");
        }
        this.j = partSource;
    }

    public long a() {
        return this.j.a();
    }

    /* access modifiers changed from: protected */
    public void a(OutputStream out) throws IOException {
        super.a(out);
        String filename = this.j.b();
        if (filename != null) {
            out.write(i);
            out.write(c);
            out.write(EncodingUtils.getAsciiBytes(filename));
            out.write(c);
        }
    }

    /* access modifiers changed from: protected */
    public void b(OutputStream out) throws IOException {
        if (a() != 0) {
            byte[] tmp = new byte[4096];
            InputStream instream = this.j.c();
            while (true) {
                try {
                    int len = instream.read(tmp);
                    if (len >= 0) {
                        out.write(tmp, 0, len);
                        if (j()) {
                            Context context = WiGame.getContext();
                            if (context instanceof Activity) {
                                final int l = len;
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    public void run() {
                                        WiGame.wyGameSaveProgress(b.this.k(), l);
                                    }
                                });
                            }
                        }
                    } else {
                        return;
                    }
                } finally {
                    instream.close();
                }
            }
        }
    }
}
