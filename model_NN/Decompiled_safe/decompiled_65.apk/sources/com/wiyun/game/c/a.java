package com.wiyun.game.c;

import android.app.Activity;
import android.content.Context;
import com.wiyun.game.WiGame;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.http.util.EncodingUtils;

public class a extends f {
    private byte[] i;
    private String j;

    public a(String name, String value) {
        this(name, value, "utf-8");
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(String name, String value, String charset) {
        super(name, "text/plain", charset == null ? "US-ASCII" : charset, "8bit");
        if (value == null) {
            throw new IllegalArgumentException("Value may not be null");
        } else if (value.indexOf(0) != -1) {
            throw new IllegalArgumentException("NULs may not be present in string parts");
        } else {
            this.j = value;
        }
    }

    private byte[] l() {
        if (this.i == null) {
            this.i = EncodingUtils.getBytes(this.j, e());
        }
        return this.i;
    }

    public long a() {
        return (long) l().length;
    }

    public String b() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void b(OutputStream out) throws IOException {
        out.write(l());
        if (j()) {
            Context context = WiGame.getContext();
            if (context instanceof Activity) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        WiGame.wyGameSaveProgress(a.this.k(), (int) a.this.a());
                    }
                });
            }
        }
    }
}
