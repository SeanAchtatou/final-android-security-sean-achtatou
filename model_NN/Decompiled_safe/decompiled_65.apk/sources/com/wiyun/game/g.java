package com.wiyun.game;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Message;

class g extends BroadcastReceiver {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ LoadGameDialog a;

    g(LoadGameDialog loadGameDialog) {
        this.a = loadGameDialog;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.wiyun.game.IMAGE_DOWNLOADED".equals(action)) {
            final String id = intent.getStringExtra("image_id");
            if (this.a.c.containsKey(id)) {
                this.a.runOnUiThread(new Runnable() {
                    public void run() {
                        h.b(g.this.a.c, true, id, null);
                        g.this.a.b();
                    }
                });
            }
        } else if ("com.wiyun.game.BLOB_DOWNLOADED".equals(action)) {
            WiGame.w().sendMessage(Message.obtain(WiGame.w(), 1002, intent.getStringExtra("tmp_path")));
            this.a.sendBroadcast(new Intent("com.wiyun.game.RESET"));
            this.a.finish();
        }
    }
}
