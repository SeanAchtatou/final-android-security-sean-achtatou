package com.wiyun.game.a;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class c {

    public interface a {
        void a(int i);

        void b(int i);
    }

    public static Dialog a(final Context context, final int id, int iconId, String title, String message, int positiveButtonId, int negativeButtonId, final a listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (iconId != 0) {
            builder.setIcon(iconId);
        }
        if (title != null) {
            builder.setTitle(title);
        }
        if (message != null) {
            builder.setMessage(message);
        }
        if (positiveButtonId != 0) {
            builder.setPositiveButton(positiveButtonId, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (context instanceof Activity) {
                        ((Activity) context).removeDialog(id);
                    }
                    if (listener != null) {
                        listener.b(id);
                    }
                }
            });
        }
        if (negativeButtonId != 0) {
            builder.setNegativeButton(negativeButtonId, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (context instanceof Activity) {
                        ((Activity) context).removeDialog(id);
                        if (listener != null) {
                            listener.a(id);
                        }
                    }
                }
            });
        }
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                if (context instanceof Activity) {
                    ((Activity) context).removeDialog(id);
                }
            }
        });
        return builder.create();
    }
}
