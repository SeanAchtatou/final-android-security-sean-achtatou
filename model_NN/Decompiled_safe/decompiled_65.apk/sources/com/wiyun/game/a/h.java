package com.wiyun.game.a;

import android.content.Context;
import com.wiyun.game.t;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class h {
    private Calendar a = GregorianCalendar.getInstance();
    private boolean b;

    public h(boolean includeTime) {
        this.b = includeTime;
    }

    public String a(Context context, long milli) {
        long now = System.currentTimeMillis();
        long delta = now - milli;
        if (delta <= 1000) {
            return String.format(t.h("wy_label_before_x_second"), 1);
        } else if (delta < 60000) {
            return String.format(t.h("wy_label_before_x_second"), Long.valueOf(delta / 1000));
        } else if (delta < 3600000) {
            return String.format(t.h("wy_label_before_x_minute"), Long.valueOf(delta / 60000));
        } else if (delta < 86400000) {
            long hour = delta / 3600000;
            long minute = (delta % 3600000) / 60000;
            if (minute == 0) {
                return String.format(t.h("wy_label_before_x_hour"), Long.valueOf(hour));
            }
            return String.format(t.h("wy_label_before_x_hour_minute"), Long.valueOf(hour), Long.valueOf(minute));
        } else {
            this.a.setTimeInMillis(now);
            int year1 = this.a.get(1);
            int month1 = this.a.get(2) + 1;
            int day1 = this.a.get(5);
            this.a.setTimeInMillis(milli);
            int year2 = this.a.get(1);
            int month2 = this.a.get(2) + 1;
            int day2 = this.a.get(5);
            if (year1 == year2 && month1 == month2) {
                return String.format(t.h("wy_label_before_x_day"), Integer.valueOf(day1 - day2));
            } else if (year1 == year2) {
                return String.format(this.b ? t.h("wy_date_format_MMddHHmm") : t.h("wy_date_format_MMdd"), Integer.valueOf(month2), Integer.valueOf(day2), Integer.valueOf(this.a.get(10)), Integer.valueOf(this.a.get(12)));
            } else {
                return String.format(this.b ? t.h("wy_date_format_yyyyMMddHHmm") : t.h("wy_date_format_yyyyMMdd"), Integer.valueOf(year2), Integer.valueOf(month2), Integer.valueOf(day2), Integer.valueOf(this.a.get(10)), Integer.valueOf(this.a.get(12)));
            }
        }
    }
}
