package com.wiyun.game.e;

public class d {
    private static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final char[] b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private final String c = "UTF-8";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.e.d.a(byte[], boolean):char[]
     arg types: [byte[], int]
     candidates:
      com.wiyun.game.e.d.a(byte[], char[]):char[]
      com.wiyun.game.e.d.a(byte[], boolean):char[] */
    public static char[] a(byte[] data) {
        return a(data, true);
    }

    public static char[] a(byte[] data, boolean toLowerCase) {
        return a(data, toLowerCase ? a : b);
    }

    protected static char[] a(byte[] data, char[] toDigits) {
        int l = data.length;
        char[] out = new char[(l << 1)];
        int j = 0;
        for (int i = 0; i < l; i++) {
            int j2 = j + 1;
            out[j] = toDigits[(data[i] & 240) >>> 4];
            j = j2 + 1;
            out[j2] = toDigits[data[i] & 15];
        }
        return out;
    }

    public static String b(byte[] data) {
        return new String(a(data));
    }

    public String toString() {
        return String.valueOf(super.toString()) + "[charsetName=" + this.c + "]";
    }
}
