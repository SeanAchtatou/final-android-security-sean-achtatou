package com.wiyun.game.e;

import java.io.UnsupportedEncodingException;

public class b {
    private static IllegalStateException a(String charsetName, UnsupportedEncodingException e) {
        return new IllegalStateException(String.valueOf(charsetName) + ": " + e);
    }

    public static String a(byte[] bytes) {
        return a(bytes, "UTF-8");
    }

    public static String a(byte[] bytes, String charsetName) {
        if (bytes == null) {
            return null;
        }
        try {
            return new String(bytes, charsetName);
        } catch (UnsupportedEncodingException e) {
            throw a(charsetName, e);
        }
    }

    public static byte[] a(String string) {
        return a(string, "UTF-8");
    }

    public static byte[] a(String string, String charsetName) {
        if (string == null) {
            return null;
        }
        try {
            return string.getBytes(charsetName);
        } catch (UnsupportedEncodingException e) {
            throw a(charsetName, e);
        }
    }
}
