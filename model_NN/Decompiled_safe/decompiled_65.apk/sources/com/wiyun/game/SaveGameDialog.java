package com.wiyun.game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.wiyun.game.b.b;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;

public class SaveGameDialog extends Activity implements View.OnClickListener, b {
    /* access modifiers changed from: private */
    public ProgressBar a;
    /* access modifiers changed from: private */
    public TextView b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f;
    private String g;
    private byte[] h;
    private byte[] i;
    private WiGameClient j = new s(this);

    private void a() {
        Intent intent = getIntent();
        this.e = intent.getStringExtra("message");
        this.f = intent.getStringExtra("name");
        this.g = intent.getStringExtra("description");
        this.h = intent.getByteArrayExtra("blob");
        this.i = intent.getByteArrayExtra("image");
        d.a().a(this);
        WiGame.addWiGameClient(this.j);
    }

    private void b() {
        this.a = (ProgressBar) findViewById(t.d("wy_pb_upload"));
        this.b = (TextView) findViewById(t.d("wy_tv_progress_hint"));
        this.b.setText(this.e);
        ((Button) findViewById(t.d("wy_b_hide"))).setOnClickListener(this);
    }

    public void b(final e e2) {
        switch (e2.a) {
            case 6:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(SaveGameDialog.this, (String) e2.e, 0).show();
                            SaveGameDialog.this.finish();
                        }
                    });
                    return;
                } else {
                    f.a((String) null, this.f, this.g, this.h, this.i);
                    return;
                }
            case 76:
                e2.d = false;
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            WiGame.g(SaveGameDialog.this.f);
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            WiGame.f(SaveGameDialog.this.f);
                        }
                    });
                }
                finish();
                return;
            default:
                return;
        }
    }

    public void onClick(View v) {
        if (v.getId() == t.d("wy_b_hide")) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_save_game_dialog"));
        a();
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        d.a().b(this);
        WiGame.removeWiGameClient(this.j);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (!WiGame.isLoggedIn() && !WiGame.v()) {
            f.b(WiGame.getMyId());
        } else if (WiGame.isLoggedIn()) {
            f.a((String) null, this.f, this.g, this.h, this.i);
        }
    }
}
