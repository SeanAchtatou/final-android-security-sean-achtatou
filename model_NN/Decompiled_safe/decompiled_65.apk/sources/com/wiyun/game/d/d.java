package com.wiyun.game.d;

import android.graphics.BitmapFactory;
import java.util.Iterator;
import java.util.WeakHashMap;

public class d {
    private static d b = null;
    private final WeakHashMap<Thread, b> a = new WeakHashMap<>();

    private enum a {
        CANCEL,
        ALLOW
    }

    private static class b {
        public a a;
        public BitmapFactory.Options b;

        private b() {
            this.a = a.ALLOW;
        }

        /* synthetic */ b(b bVar) {
            this();
        }

        public String toString() {
            return "thread state = " + (this.a == a.CANCEL ? "Cancel" : this.a == a.ALLOW ? "Allow" : "?") + ", options = " + this.b;
        }
    }

    public static class c implements Iterable<Thread> {
        private final WeakHashMap<Thread, Object> a = new WeakHashMap<>();

        public Iterator<Thread> iterator() {
            return this.a.keySet().iterator();
        }
    }

    private d() {
    }

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (b == null) {
                b = new d();
            }
            dVar = b;
        }
        return dVar;
    }

    private synchronized b b(Thread t) {
        b status;
        status = this.a.get(t);
        if (status == null) {
            status = new b(null);
            this.a.put(t, status);
        }
        return status;
    }

    public synchronized void a(c threads) {
        Iterator<Thread> it = threads.iterator();
        while (it.hasNext()) {
            a(it.next());
        }
    }

    public synchronized void a(Thread t) {
        b status = b(t);
        status.a = a.CANCEL;
        if (status.b != null) {
            status.b.requestCancelDecode();
        }
        notifyAll();
    }
}
