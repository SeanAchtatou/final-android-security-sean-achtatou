package com.wiyun.game;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

class n extends BroadcastReceiver {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ FullImageGallery a;

    n(FullImageGallery fullImageGallery) {
        this.a = fullImageGallery;
    }

    public void onReceive(Context context, Intent intent) {
        if ("com.wiyun.game.IMAGE_DOWNLOADED".equals(intent.getAction())) {
            final String id = intent.getStringExtra("image_id");
            if (this.a.f.containsKey(id)) {
                this.a.runOnUiThread(new Runnable() {
                    public void run() {
                        View panel = (View) n.this.a.e.get(id);
                        if (panel != null) {
                            ((ImageView) panel.findViewById(t.d("wy_image"))).setImageBitmap(h.b(n.this.a.f, true, id, null));
                            panel.findViewById(t.d("wy_panel")).setVisibility(8);
                        }
                    }
                });
            }
        }
    }
}
