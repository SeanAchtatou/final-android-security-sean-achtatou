package com.wiyun.game.model.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.wiyun.game.model.User;
import org.json.JSONObject;

public class ag extends User implements Parcelable {
    public static final Parcelable.Creator<ag> CREATOR = new af();
    private boolean A;
    private String B;
    private String C;
    private String D;
    private int E;
    private String u;
    private String v;
    private String w;
    private int x;
    private boolean y = true;
    private boolean z = true;

    public ag() {
    }

    public ag(Parcel in) {
        this.a = in.readString();
        this.b = in.readString();
        this.d = in.readInt();
        this.e = in.readInt();
        this.x = in.readInt();
        this.n = in.readString();
        this.y = in.readInt() == 1;
        this.z = in.readInt() == 1;
        this.A = in.readInt() == 1;
        this.g = in.readInt();
        this.h = in.readInt();
        this.k = in.readInt() == 1;
        this.i = in.readString();
        this.j = in.readString();
        this.f = in.readInt();
        this.D = in.readString();
        this.p = in.readInt();
        this.q = in.readInt();
        this.r = in.readInt();
        this.s = in.readInt();
        this.o = in.readInt();
        this.t = in.readInt();
        this.l = in.readDouble();
        this.m = in.readDouble();
        this.E = in.readInt();
        this.u = in.readString();
        this.v = in.readString();
        this.w = in.readString();
        this.c = in.readInt() == 1;
    }

    public static ag a(JSONObject jSONObject) {
        ag agVar = new ag();
        String optString = jSONObject.optString("user_id");
        if (TextUtils.isEmpty(optString)) {
            return null;
        }
        agVar.i(optString);
        agVar.j(jSONObject.optString("username"));
        agVar.h(jSONObject.optInt("point"));
        agVar.g(jSONObject.optInt("coins"));
        agVar.e(jSONObject.optInt("app_point"));
        agVar.a(jSONObject.optString("avatar"));
        agVar.a(jSONObject.optBoolean("share_location", true));
        agVar.b(jSONObject.optBoolean("share_status", true));
        agVar.c(jSONObject.optInt("app_count"));
        agVar.b(jSONObject.optInt("friends_count"));
        agVar.c(jSONObject.optBoolean("is_my_friend"));
        agVar.b(jSONObject.optString("last_app_name"));
        agVar.c(jSONObject.optString("last_app_id"));
        agVar.d(jSONObject.optInt("status"));
        agVar.l(jSONObject.optInt("pending_friend_count"));
        agVar.m(jSONObject.optInt("pending_sharing_count"));
        agVar.j(jSONObject.optInt("pending_challenge_count"));
        agVar.k(jSONObject.optInt("pending_current_challenge_count"));
        agVar.a(jSONObject.optInt("unread_message_count"));
        agVar.i(jSONObject.optInt("notice_count"));
        agVar.d(jSONObject.optBoolean("has_usable_password"));
        agVar.a(jSONObject.optDouble("lat", 0.0d));
        agVar.b(jSONObject.optDouble("lon", 0.0d));
        agVar.f(jSONObject.optInt("score"));
        agVar.d(jSONObject.optString("platform"));
        agVar.g(jSONObject.optString("brand", "unknown"));
        agVar.h(jSONObject.optString("model", "unknown"));
        agVar.e("F".equalsIgnoreCase(jSONObject.optString("gender")));
        return agVar;
    }

    public void a(double latitude) {
        this.l = latitude;
    }

    public void a(int unreadMessageCount) {
        this.o = unreadMessageCount;
    }

    public void a(String avatarUrl) {
        this.n = avatarUrl;
    }

    public void a(boolean shareLocation) {
        this.y = shareLocation;
    }

    public boolean a() {
        return this.y;
    }

    public void b(double longitude) {
        this.m = longitude;
    }

    public void b(int friendCount) {
        this.g = friendCount;
    }

    public void b(String lastAppName) {
        this.i = lastAppName;
    }

    public void b(boolean shareStatus) {
        this.z = shareStatus;
    }

    public boolean b() {
        return this.z;
    }

    public int c() {
        return this.f;
    }

    public void c(int appCount) {
        this.h = appCount;
    }

    public void c(String lastAppId) {
        this.j = lastAppId;
    }

    public void c(boolean friend) {
        this.k = friend;
    }

    public void d(int state) {
        this.f = state;
    }

    public void d(String platform) {
        this.u = platform;
    }

    public void d(boolean hasRetrieval) {
        this.A = hasRetrieval;
    }

    public boolean d() {
        return this.A;
    }

    public int describeContents() {
        return 0;
    }

    public int e() {
        return this.E;
    }

    public void e(int thisGamePoint) {
        this.x = thisGamePoint;
    }

    public void e(String boundEmail) {
        this.B = boundEmail;
    }

    public void e(boolean female) {
        this.c = female;
    }

    public String f() {
        return this.B;
    }

    public void f(int score) {
        this.E = score;
    }

    public void f(String boundMobile) {
        this.C = boundMobile;
    }

    public String g() {
        return this.C;
    }

    public void g(int coins) {
        this.e = coins;
    }

    public void g(String brand) {
        this.v = brand;
    }

    public void h(int honor) {
        this.d = honor;
    }

    public void h(String model) {
        this.w = model;
    }

    public void i(int noticeCount) {
        this.t = noticeCount;
    }

    public void i(String id) {
        this.a = id;
    }

    public void j(int pendingChallengeCount) {
        this.r = pendingChallengeCount;
    }

    public void j(String name) {
        this.b = name;
    }

    public void k(int pendingChallengeCount) {
        this.s = pendingChallengeCount;
    }

    public void l(int pendingFriendCount) {
        this.p = pendingFriendCount;
    }

    public void m(int count) {
        this.q = count;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.a);
        dest.writeString(this.b);
        dest.writeInt(this.d);
        dest.writeInt(this.e);
        dest.writeInt(this.x);
        dest.writeString(this.n);
        dest.writeInt(this.y ? 1 : 0);
        dest.writeInt(this.z ? 1 : 0);
        dest.writeInt(this.A ? 1 : 0);
        dest.writeInt(this.g);
        dest.writeInt(this.h);
        dest.writeInt(this.k ? 1 : 0);
        dest.writeString(this.i);
        dest.writeString(this.j);
        dest.writeInt(this.f);
        dest.writeString(this.D);
        dest.writeInt(this.p);
        dest.writeInt(this.q);
        dest.writeInt(this.r);
        dest.writeInt(this.s);
        dest.writeInt(this.o);
        dest.writeInt(this.t);
        dest.writeDouble(this.l);
        dest.writeDouble(this.m);
        dest.writeInt(this.E);
        dest.writeString(this.u);
        dest.writeString(this.v);
        dest.writeString(this.w);
        dest.writeInt(this.c ? 1 : 0);
    }
}
