package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class j {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private boolean h;
    private String i;
    private long j;

    public static j a(JSONObject dict) {
        j i2 = new j();
        String id = dict.optString("id");
        String appId = dict.optString("app_id");
        String userId = dict.optString("user_id");
        if (TextUtils.isEmpty(id) || TextUtils.isEmpty(appId) || TextUtils.isEmpty(userId)) {
            return null;
        }
        i2.a(id);
        i2.b(appId);
        i2.d(userId);
        i2.c(dict.optString("app_name"));
        i2.h(dict.optString("app_icon"));
        i2.e(dict.optString("username"));
        i2.f(dict.optString("avatar"));
        i2.g(dict.optString("message"));
        i2.a(dict.optLong("sent_time"));
        i2.a("F".equalsIgnoreCase(dict.optString("gender")));
        return i2;
    }

    public void a(long sentTime) {
        this.j = sentTime;
    }

    public void a(String id) {
        this.a = id;
    }

    public void a(boolean female) {
        this.h = female;
    }

    public void b(String appId) {
        this.b = appId;
    }

    public void c(String appName) {
        this.c = appName;
    }

    public void d(String userId) {
        this.e = userId;
    }

    public void e(String username) {
        this.f = username;
    }

    public void f(String avatarUrl) {
        this.g = avatarUrl;
    }

    public void g(String message) {
        this.i = message;
    }

    public void h(String appIcon) {
        this.d = appIcon;
    }
}
