package com.wiyun.game.model.a;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class ab extends k implements Parcelable {
    public static final Parcelable.Creator<ab> CREATOR = new e();
    private int j;
    private int k;
    private int l;
    private int m;
    private boolean n;
    private HashMap<String, String> o;
    private String p;
    private String[] q;

    public ab() {
        this.o = new HashMap<>();
    }

    public ab(Parcel in) {
        this.a = in.readInt();
        this.j = in.readInt();
        this.k = in.readInt();
        this.l = in.readInt();
        this.e = in.readInt();
        this.m = in.readInt();
        this.n = in.readInt() == 1;
        this.b = in.readString();
        this.c = in.readString();
        this.d = in.readString();
        this.f = in.readString();
        this.g = in.readString();
        this.p = in.readString();
        this.h = in.readString();
        this.q = new String[in.readInt()];
        in.readStringArray(this.q);
        this.o = (HashMap) in.readSerializable();
    }

    public static ab a(JSONObject dict) {
        ab i = new ab();
        String id = dict.optString("id");
        String name = dict.optString("name");
        if (TextUtils.isEmpty(id) || TextUtils.isEmpty(name)) {
            return null;
        }
        i.f(id);
        i.e(name);
        i.b(dict.optString("description"));
        i.d(dict.optString("alias"));
        i.a(dict.optInt("coins"));
        i.b(dict.optInt("honor"));
        i.f(dict.optInt("type"));
        i.c(dict.optInt("max_quantity"));
        i.c(dict.optString("icon"));
        i.a(dict.optBoolean("active", true));
        i.e(dict.optInt("count"));
        i.d(dict.optInt("my_buy_count"));
        i.a(dict.optString("dlc_id"));
        JSONObject p2 = dict.optJSONObject("properties");
        if (p2 != null) {
            i.p = p2.toString();
            Iterator iter = p2.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                String value = p2.optString(key);
                if (!TextUtils.isEmpty(value)) {
                    i.o.put(key, value);
                }
            }
        }
        JSONArray urls = dict.optJSONArray("screenshots");
        int size = urls.length();
        i.q = new String[size];
        for (int j2 = 0; j2 < size; j2++) {
            i.q[j2] = urls.optString(j2);
        }
        return i;
    }

    public void a(int coins) {
        this.k = coins;
    }

    public void a(String dlc) {
        this.h = dlc;
    }

    public void a(boolean active) {
        this.n = active;
    }

    public void b(int honor) {
        this.l = honor;
    }

    public void b(String description) {
        this.g = description;
    }

    public void c(int maxUserPurchaseCount) {
        this.m = maxUserPurchaseCount;
    }

    public void c(String iconUrl) {
        this.f = iconUrl;
    }

    public void d(int myBuyCount) {
        this.j = myBuyCount;
    }

    public void d(String alias) {
        this.d = alias;
    }

    public int describeContents() {
        return 0;
    }

    public void e(int count) {
        this.a = count;
    }

    public void e(String name) {
        this.c = name;
    }

    public void f(int type) {
        this.e = type;
    }

    public void f(String id) {
        this.b = id;
    }

    public String i() {
        return this.p;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(this.a);
        out.writeInt(this.j);
        out.writeInt(this.k);
        out.writeInt(this.l);
        out.writeInt(this.e);
        out.writeInt(this.m);
        out.writeInt(this.n ? 1 : 0);
        out.writeString(this.b);
        out.writeString(this.c);
        out.writeString(this.d);
        out.writeString(this.f);
        out.writeString(this.g);
        out.writeString(this.p);
        out.writeString(this.h);
        out.writeInt(this.q.length);
        out.writeStringArray(this.q);
        out.writeSerializable(this.o);
    }
}
