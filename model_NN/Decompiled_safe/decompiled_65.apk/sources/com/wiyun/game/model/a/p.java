package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class p {
    private String a;
    private int b;
    private String c;
    private String d;
    private String e;
    private boolean f;
    private long g;
    private String h;
    private String i;
    private double j;
    private double k;
    private int l;
    private int m;
    private boolean n;

    public static p a(JSONObject dict) {
        p score = new p();
        String id = dict.optString("id");
        String userId = dict.optString("user_id");
        if (TextUtils.isEmpty(id) || TextUtils.isEmpty(userId)) {
            return null;
        }
        score.e(id);
        score.d(userId);
        score.b(dict.optString("username"));
        score.a(dict.optString("country"));
        score.b(dict.optInt("score"));
        score.a(dict.optLong("create_time"));
        score.c(dict.optString("avatar"));
        score.a(dict.optDouble("lat", 0.0d));
        score.b(dict.optDouble("lon", 0.0d));
        score.c(dict.optInt("rank"));
        score.a(dict.optBoolean("has_data"));
        score.a(dict.optInt("status"));
        score.b("F".equalsIgnoreCase(dict.optString("gender")));
        score.f(dict.optString("platform"));
        return score;
    }

    public String a() {
        return this.e;
    }

    public void a(double latitude) {
        this.j = latitude;
    }

    public void a(int state) {
        this.l = state;
    }

    public void a(long createTime) {
        this.g = createTime;
    }

    public void a(String countryCode) {
        this.c = countryCode;
    }

    public void a(boolean hasData) {
        this.n = hasData;
    }

    public String b() {
        return this.i;
    }

    public void b(double longitude) {
        this.k = longitude;
    }

    public void b(int score) {
        this.b = score;
    }

    public void b(String username) {
        this.e = username;
    }

    public void b(boolean female) {
        this.f = female;
    }

    public String c() {
        return this.h;
    }

    public void c(int rank) {
        this.m = rank;
    }

    public void c(String avatarUrl) {
        this.i = avatarUrl;
    }

    public double d() {
        return this.j;
    }

    public void d(String userId) {
        this.h = userId;
    }

    public double e() {
        return this.k;
    }

    public void e(String id) {
        this.a = id;
    }

    public int f() {
        return this.m;
    }

    public void f(String platform) {
        this.d = platform;
    }

    public boolean g() {
        return this.f;
    }
}
