package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class v {
    private String a;
    private String b;
    private boolean c;
    private String d;
    private String e;
    private int f = -2;
    private int g;
    private int h;

    public static v a(JSONObject dict) {
        v c2 = new v();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        c2.a(id);
        c2.b(dict.optInt("result"));
        return c2;
    }

    public static v b(JSONObject dict) {
        v c2 = new v();
        c2.b(dict.optInt("result"));
        return c2;
    }

    public static v c(JSONObject dict) {
        v c2 = new v();
        String id = dict.optString("id");
        String toUid = dict.optString("user_id");
        if (TextUtils.isEmpty(id) || TextUtils.isEmpty(toUid)) {
            return null;
        }
        c2.a(id);
        c2.c(toUid);
        c2.d(dict.optString("username"));
        c2.b(dict.optString("avatar"));
        c2.b(dict.optInt("result"));
        c2.c(dict.optInt("score"));
        c2.a(dict.optInt("status"));
        c2.a("F".equalsIgnoreCase(dict.optString("gender")));
        return c2;
    }

    public String a() {
        return this.d;
    }

    public void a(int state) {
        this.h = state;
    }

    public void a(String id) {
        this.d = id;
    }

    public void a(boolean female) {
        this.c = female;
    }

    public void b(int result) {
        this.f = result;
    }

    public void b(String avatarUrl) {
        this.e = avatarUrl;
    }

    public void c(int score) {
        this.g = score;
    }

    public void c(String toUserId) {
        this.a = toUserId;
    }

    public void d(String toUsername) {
        this.b = toUsername;
    }
}
