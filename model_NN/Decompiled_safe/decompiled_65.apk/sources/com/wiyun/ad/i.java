package com.wiyun.ad;

import android.graphics.Bitmap;

class i {
    String a;
    String b;
    String c;
    int d;
    String e;
    String f;
    String g;
    int h;
    float i = 13.0f;
    String j;
    int k;
    String l;
    int m;
    int n;
    String o;
    String p;
    String q;
    String r;
    Bitmap s;
    Bitmap t;
    String u;

    i() {
    }

    public void a() {
        if (this.t != null && !this.t.isRecycled()) {
            this.t.recycle();
        }
        this.t = null;
        if (this.s != null && !this.s.isRecycled()) {
            this.s.recycle();
        }
        this.s = null;
    }

    public boolean equals(Object obj) {
        return obj instanceof i ? ((i) obj).a.equals(this.a) && ((i) obj).b.equals(this.b) : super.equals(obj);
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
