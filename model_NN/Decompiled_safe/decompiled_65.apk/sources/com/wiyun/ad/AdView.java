package com.wiyun.ad;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.wiyun.ad.l;
import java.io.File;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

public class AdView extends FrameLayout implements View.OnClickListener {
    public static final int AD_TYPE_BANNER = 2;
    public static final int AD_TYPE_FULL_SCREEN = 3;
    public static final int AD_TYPE_TEXT = 1;
    public static final int TRANSITION_BOTTOM_PUSH = 6;
    public static final int TRANSITION_FADE = 7;
    public static final int TRANSITION_FLIP_X = 1;
    public static final int TRANSITION_FLIP_Y = 2;
    public static final int TRANSITION_LEFT_PUSH = 3;
    public static final int TRANSITION_RANDOM = 0;
    public static final int TRANSITION_RIGHT_PUSH = 4;
    public static final int TRANSITION_TOP_PUSH = 5;
    /* access modifiers changed from: private */
    public a a;
    private boolean b;
    /* access modifiers changed from: private */
    public boolean c;
    private int d;
    private int e;
    private Timer f;
    private int g;
    private int h;
    private boolean i;
    private boolean j;
    private int k;
    private boolean l;
    private int m;
    private String n;
    private String o;
    private String p;
    private boolean q;
    /* access modifiers changed from: private */
    public AdListener r;
    private t s;
    /* access modifiers changed from: private */
    public boolean t;
    private boolean u;
    private LinearLayout v;
    /* access modifiers changed from: private */
    public i w;
    /* access modifiers changed from: private */
    public View x;
    private View y;
    private Drawable z;

    public interface AdListener {
        void onAdClicked();

        void onAdLoadFailed();

        void onAdLoaded();

        void onExitButtonClicked();
    }

    private final class a implements Runnable {
        /* access modifiers changed from: private */
        public a b;
        private int c;

        public a(a aVar, int i) {
            this.b = aVar;
            this.c = i;
        }

        public void run() {
            AdView.this.a.setVisibility(8);
            this.b.setVisibility(0);
            h hVar = new h(90.0f, 0.0f, ((float) AdView.this.getWidth()) / 2.0f, ((float) AdView.this.getHeight()) / 2.0f, -0.4f * ((float) (this.c == 2 ? AdView.this.getHeight() : AdView.this.getWidth())), false, this.c != 2);
            hVar.setDuration(700);
            hVar.setFillAfter(true);
            hVar.setInterpolator(new DecelerateInterpolator());
            hVar.setAnimationListener(new Animation.AnimationListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void
                 arg types: [com.wiyun.ad.AdView, int]
                 candidates:
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.view.View):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.widget.LinearLayout):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.a):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.i):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.a, int):void
                  com.wiyun.ad.AdView.a(int, java.lang.String):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void */
                public void onAnimationEnd(Animation animation) {
                    AdView.this.a.clearAnimation();
                    AdView.this.removeView(AdView.this.a);
                    AdView.this.f();
                    if (AdView.this.a.a() != null) {
                        AdView.this.a.a().a();
                    }
                    AdView.this.a = a.this.b;
                    AdView.this.c = false;
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            AdView.this.startAnimation(hVar);
        }
    }

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.g = 0;
        this.h = 0;
        this.m = 2;
        this.n = "*/*";
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        this.d = 0;
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "textColor", 0);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", 0);
            if (attributeUnsignedIntValue != 0) {
                setTextColor(attributeUnsignedIntValue);
            }
            if (attributeUnsignedIntValue2 != 0) {
                setBackgroundColor(attributeUnsignedIntValue2);
            }
            this.o = attributeSet.getAttributeValue(str, "resId");
            if (this.o != null) {
                this.o = this.o.trim();
            }
            this.l = attributeSet.getAttributeBooleanValue(str, "testMode", false);
            this.m = attributeSet.getAttributeIntValue(str, "testAdType", 2);
            this.q = attributeSet.getAttributeBooleanValue(str, "showLoadingHint", false);
            setRefreshInterval(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            setGoneIfFail(attributeSet.getAttributeBooleanValue(str, "goneIfFail", isGoneIfFail()));
            this.k = attributeSet.getAttributeIntValue(str, "transition", 1);
            this.j = attributeSet.getAttributeBooleanValue(str, "autoStart", false);
        }
        File[] listFiles = getContext().getCacheDir().listFiles();
        if (listFiles != null) {
            long currentTimeMillis = System.currentTimeMillis();
            for (File file : listFiles) {
                if (file.lastModified() - currentTimeMillis > 86400000) {
                    file.delete();
                }
            }
        }
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            Toast.makeText(context, "you must add INTERNET permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            Toast.makeText(context, "you must add READ_PHONE_STATE permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
            Toast.makeText(context, "you must add ACCESS_WIFI_STATE permission", 0).show();
        }
        if (context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            Toast.makeText(context, "you must add WRITE_EXTERNAL_STORAGE permission", 0).show();
        }
    }

    /* access modifiers changed from: private */
    public void a(final LinearLayout linearLayout) {
        if (!this.t) {
            this.t = true;
            TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, 0.0f, 1, -1.0f);
            translateAnimation.setDuration(200);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setAnimationListener(new Animation.AnimationListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void
                 arg types: [com.wiyun.ad.AdView, int]
                 candidates:
                  com.wiyun.ad.AdView.b(com.wiyun.ad.a, int):void
                  com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void */
                public void onAnimationEnd(Animation animation) {
                    ViewGroup viewGroup = (ViewGroup) linearLayout.getParent();
                    if (viewGroup != null) {
                        viewGroup.removeView(linearLayout);
                    }
                    AdView.this.t = false;
                    AdView.this.x = (View) null;
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            linearLayout.startAnimation(translateAnimation);
        }
    }

    private void a(a aVar) {
        this.a = aVar;
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(350);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        startAnimation(alphaAnimation);
    }

    private void a(final a aVar, int i2) {
        TranslateAnimation translateAnimation;
        TranslateAnimation translateAnimation2;
        switch (i2) {
            case 3:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 1.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation2 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                break;
            case 4:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation2 = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                break;
            case 5:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
                translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                break;
            case 6:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                break;
            default:
                translateAnimation = null;
                translateAnimation2 = null;
                break;
        }
        translateAnimation.setDuration(700);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        translateAnimation2.setDuration(700);
        translateAnimation2.setFillAfter(true);
        translateAnimation2.setInterpolator(new DecelerateInterpolator());
        translateAnimation2.setAnimationListener(new Animation.AnimationListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void
             arg types: [com.wiyun.ad.AdView, int]
             candidates:
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.view.View):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.widget.LinearLayout):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.a):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.i):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.a, int):void
              com.wiyun.ad.AdView.a(int, java.lang.String):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void */
            public void onAnimationEnd(Animation animation) {
                AdView.this.a.clearAnimation();
                AdView.this.removeView(AdView.this.a);
                AdView.this.f();
                if (AdView.this.a.a() != null) {
                    AdView.this.a.a().a();
                }
                AdView.this.a = aVar;
                AdView.this.c = false;
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.a.startAnimation(translateAnimation);
        aVar.startAnimation(translateAnimation2);
    }

    private void a(boolean z2) {
        synchronized (this) {
            if (z2) {
                if (this.e > 0) {
                    if (this.f == null) {
                        this.f = new Timer();
                        this.f.schedule(new TimerTask() {
                            public void run() {
                                AdView.this.post(new Runnable() {
                                    public void run() {
                                        AdView.this.requestAd();
                                    }
                                });
                            }
                        }, (long) (this.e * 1000), (long) (this.e * 1000));
                    }
                }
            }
            if (this.f != null) {
                this.f.cancel();
                this.f = null;
            }
        }
    }

    private void b(a aVar) {
        if (!this.c) {
            this.c = true;
            int i2 = this.k;
            if (i2 == 0) {
                i2 = (Math.abs((int) (SystemClock.uptimeMillis() / 1000)) % 7) + 1;
            }
            switch (i2) {
                case 3:
                case 4:
                case 5:
                case 6:
                    a(aVar, i2);
                    return;
                case 7:
                    c(aVar);
                    return;
                default:
                    b(aVar, i2);
                    return;
            }
        }
    }

    private void b(final a aVar, final int i2) {
        aVar.setVisibility(8);
        h hVar = new h(0.0f, -90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, -0.4f * ((float) (i2 == 2 ? getHeight() : getWidth())), true, i2 != 2);
        hVar.setDuration(700);
        hVar.setFillAfter(true);
        hVar.setInterpolator(new AccelerateInterpolator());
        hVar.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                AdView.this.post(new a(aVar, i2));
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startAnimation(hVar);
    }

    private void c(final a aVar) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(700);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation2.setDuration(700);
        alphaAnimation2.setFillAfter(true);
        alphaAnimation2.setInterpolator(new AccelerateInterpolator());
        alphaAnimation2.setAnimationListener(new Animation.AnimationListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void
             arg types: [com.wiyun.ad.AdView, int]
             candidates:
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.view.View):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.widget.LinearLayout):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.a):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.i):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.a, int):void
              com.wiyun.ad.AdView.a(int, java.lang.String):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void */
            public void onAnimationEnd(Animation animation) {
                AdView.this.a.clearAnimation();
                AdView.this.removeView(AdView.this.a);
                AdView.this.f();
                if (AdView.this.a.a() != null) {
                    AdView.this.a.a().a();
                }
                AdView.this.a = aVar;
                AdView.this.c = false;
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.a.startAnimation(alphaAnimation);
        aVar.startAnimation(alphaAnimation2);
    }

    /* access modifiers changed from: private */
    public void e() {
        if (!(this.s == null || this.s.getParent() == null)) {
            this.s.a(false);
            removeView(this.s);
        }
        if (this.a != null) {
            this.a.setVisibility(0);
        }
        if (this.w != null) {
            synchronized (this) {
                if (this.a == null || !this.w.equals(this.a.a())) {
                    boolean z2 = this.a == null;
                    a aVar = new a(getContext(), this.w, this.r);
                    int backgroundColor = getBackgroundColor();
                    if (backgroundColor == 0) {
                        backgroundColor = this.w.k;
                    }
                    if (backgroundColor == 0) {
                        backgroundColor = -16777216;
                    }
                    int textColor = getTextColor();
                    if (textColor == 0) {
                        textColor = this.w.h;
                    }
                    if (textColor == 0) {
                        textColor = -1;
                    }
                    aVar.setBackgroundColor(backgroundColor);
                    aVar.a(textColor);
                    int d2 = super.getVisibility();
                    aVar.setVisibility(d2);
                    try {
                        if (this.r != null) {
                            try {
                                this.r.onAdLoaded();
                            } catch (Exception e2) {
                                Log.w("WiYun", "Unhandled exception raised in your AdListener.onAdLoaded().", e2);
                            }
                        }
                        addView(aVar);
                        if (this.w.d == 3 && this.y == null) {
                            this.y = g();
                        }
                        if (d2 != 0) {
                            if (this.a != null) {
                                removeView(this.a);
                            }
                            f();
                            if (!(this.a == null || this.a.a() == null)) {
                                this.a.a().a();
                            }
                            this.a = aVar;
                        } else if (z2) {
                            a(aVar);
                        } else if (this.q) {
                            removeView(this.a);
                            f();
                            if (this.a.a() != null) {
                                this.a.a().a();
                            }
                            a(aVar);
                        } else {
                            b(aVar);
                        }
                    } catch (Exception e3) {
                        Log.e("WiYun", "Unhandled exception placing AdContainer into AdView.", e3);
                    }
                } else {
                    if (this.a.getParent() == null) {
                        addView(this.a);
                    }
                    this.w.a();
                }
            }
        } else {
            if (this.a != null && this.a.getParent() == null) {
                addView(this.a);
            }
            if (this.r != null) {
                try {
                    this.r.onAdLoadFailed();
                } catch (Exception e4) {
                    Log.w("WiYun", "Unhandled exception raised in your AdListener.onLoadAdFailed().", e4);
                }
            }
        }
        this.b = false;
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.w != null && this.w.d != 3 && this.y != null) {
            removeView(this.y);
            this.y = null;
            if (this.z != null) {
                this.z.setCallback(null);
                this.z = null;
            }
        }
    }

    private View g() {
        if (this.z == null) {
            this.z = s.a(c.c, c.a, c.b);
        }
        Button button = new Button(getContext());
        button.setBackgroundDrawable(this.z);
        button.setOnClickListener(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 53;
        addView(button, layoutParams);
        return button;
    }

    private void h() {
        if (f.a() && getContext().checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
            f.a(new File(Environment.getExternalStorageDirectory(), ".wiad_cache"));
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.v != null) {
            removeView(this.v);
            this.v = null;
            if (this.a != null) {
                this.a.setVisibility(0);
            }
        }
        this.u = false;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        ProgressBar progressBar;
        if (this.v != null && (progressBar = (ProgressBar) this.v.findViewById(65536)) != null) {
            progressBar.setProgress(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, String str) {
        if (this.v == null && !this.u) {
            this.u = true;
            LinearLayout linearLayout = new LinearLayout(getContext());
            this.v = linearLayout;
            linearLayout.setGravity(16);
            linearLayout.setOrientation(1);
            linearLayout.setPadding(10, 0, 12, 0);
            linearLayout.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3879462, -7167046}));
            TextView textView = new TextView(getContext());
            textView.setTextAppearance(getContext(), 16973895);
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setSingleLine(true);
            textView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            textView.setText(String.valueOf(s.a("downloading")) + " " + str);
            linearLayout.addView(textView);
            try {
                SeekBar seekBar = new SeekBar(getContext());
                seekBar.setThumb(null);
                seekBar.setId(65536);
                seekBar.setMax(i2);
                seekBar.setProgress(0);
                seekBar.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                linearLayout.addView(seekBar);
            } catch (Exception e2) {
            }
            addView(linearLayout, new FrameLayout.LayoutParams(-1, getHeight()));
            if (this.a != null) {
                this.a.setVisibility(4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (!this.t) {
            this.t = true;
            final LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setGravity(16);
            linearLayout.setOrientation(0);
            linearLayout.setPadding(5, 5, 0, 0);
            linearLayout.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3879462, -7167046}));
            final EditText editText = new EditText(getContext());
            editText.setTextAppearance(getContext(), 16973895);
            editText.setTypeface(Typeface.DEFAULT_BOLD);
            editText.setSingleLine(true);
            editText.setHint(this.w.g);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.weight = 1.0f;
            editText.setLayoutParams(layoutParams);
            linearLayout.addView(editText);
            editText.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View view, int i, KeyEvent keyEvent) {
                    if (keyEvent.getAction() == 0) {
                        if (i == 66) {
                            String editable = ((EditText) view).getEditableText().toString();
                            if (!TextUtils.isEmpty(editable)) {
                                final Context applicationContext = AdView.this.getContext().getApplicationContext();
                                new Thread() {
                                    public void run() {
                                        b.a(applicationContext, AdView.this.w);
                                    }
                                }.start();
                                Intent intent = new Intent("android.intent.action.VIEW");
                                intent.addFlags(268435456);
                                try {
                                    intent.setData(Uri.parse(AdView.this.w.q.replace("%query%", URLEncoder.encode(editable, "utf-8"))));
                                    applicationContext.startActivity(intent);
                                } catch (Exception e) {
                                    Log.e("WiYun", "Could not open browser on ad click to " + AdView.this.w.q, e);
                                }
                                if (AdView.this.r != null) {
                                    AdView.this.r.onAdClicked();
                                }
                                AdView.this.a(linearLayout);
                            }
                            return true;
                        } else if (i == 4) {
                            AdView.this.a(linearLayout);
                            return true;
                        }
                    }
                    return false;
                }
            });
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(final View view, boolean z) {
                    if (z) {
                        view.post(new Runnable() {
                            public void run() {
                                InputMethodManager inputMethodManager = (InputMethodManager) AdView.this.getContext().getSystemService("input_method");
                                if (inputMethodManager.isActive(view)) {
                                    inputMethodManager.showSoftInput(view, 0);
                                } else {
                                    view.postDelayed(this, 50);
                                }
                            }
                        });
                        return;
                    }
                    view.post(new Runnable() {
                        public void run() {
                            ((InputMethodManager) AdView.this.getContext().getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    });
                    if (view.getParent() != null) {
                        AdView.this.a(linearLayout);
                    }
                }
            });
            addView(linearLayout, new FrameLayout.LayoutParams(-1, getHeight()));
            TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, -1.0f, 1, 0.0f);
            translateAnimation.setDuration(200);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setAnimationListener(new Animation.AnimationListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void
                 arg types: [com.wiyun.ad.AdView, int]
                 candidates:
                  com.wiyun.ad.AdView.b(com.wiyun.ad.a, int):void
                  com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void */
                public void onAnimationEnd(Animation animation) {
                    editText.requestFocus();
                    AdView.this.t = false;
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            editText.startAnimation(translateAnimation);
            this.x = editText;
        }
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.p;
    }

    public int getBackgroundColor() {
        return this.g;
    }

    public int getRefreshInterval() {
        return this.e;
    }

    public String getResId() {
        return this.o;
    }

    public int getTestAdType() {
        return this.m;
    }

    public int getTextColor() {
        return this.h;
    }

    public int getTransitionType() {
        return this.k;
    }

    public int getVisibility() {
        if (hasAd()) {
            return super.getVisibility();
        }
        if (!this.i) {
            return super.getVisibility();
        }
        if (!this.b || !this.q) {
            return 8;
        }
        return super.getVisibility();
    }

    public boolean hasAd() {
        return this.a != null;
    }

    public boolean isGoneIfFail() {
        return this.i;
    }

    public boolean isShowLoadingHint() {
        return this.q;
    }

    public boolean isTestMode() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        l.a(getContext(), (l.a) null);
        h();
        if (this.j) {
            requestAd();
        }
    }

    public void onClick(View view) {
        if (this.r != null) {
            this.r.onExitButtonClicked();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        l.a();
        b.a = false;
        super.onDetachedFromWindow();
    }

    public void onWindowFocusChanged(boolean z2) {
        a(z2);
    }

    public void requestAd() {
        int currentTimeMillis = (int) (System.currentTimeMillis() / 1000);
        if (!this.b && !this.c && currentTimeMillis - this.d >= 30) {
            this.w = null;
            this.b = true;
            this.d = currentTimeMillis;
            if (this.q) {
                if (this.s == null) {
                    this.s = new t(s.a(), 1, getContext());
                    this.s.a(-1);
                    this.s.a(16.0f);
                }
                if (this.x != null) {
                    removeView(this.x);
                    this.x = null;
                }
                if (this.a != null) {
                    this.a.setVisibility(4);
                }
                if (this.s.getParent() == null) {
                    addView(this.s, new FrameLayout.LayoutParams(-1, -1));
                }
                this.s.a(true);
            }
            new Thread() {
                public void run() {
                    try {
                        AdView.this.w = b.a(AdView.this.getContext(), AdView.this);
                        Handler handler = AdView.this.getHandler();
                        while (handler == null) {
                            Thread.sleep(100);
                            handler = AdView.this.getHandler();
                        }
                        handler.post(new Runnable() {
                            public void run() {
                                AdView.this.e();
                            }
                        });
                    } catch (Exception e) {
                        Log.e("WiYun", "Unhandled exception requesting a fresh ad.", e);
                    }
                }
            }.start();
        }
    }

    public void setBackgroundColor(int i2) {
        super.setBackgroundColor(i2);
        this.g = i2;
        if (this.a != null) {
            this.a.setBackgroundColor(i2);
        }
        invalidate();
    }

    public void setGoneIfFail(boolean z2) {
        this.i = z2;
    }

    public void setListener(AdListener adListener) {
        synchronized (this) {
            this.r = adListener;
        }
    }

    public void setRefreshInterval(int i2) {
        int i3;
        if (i2 <= 0) {
            i3 = 0;
        } else {
            if (i2 < 30) {
                m.a("AdView.setRefreshInterval(" + i2 + ") seconds must be >= " + 30);
            }
            i3 = i2;
        }
        this.e = i3;
        if (i3 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public void setResId(String str) {
        this.o = str;
    }

    public void setShowLoadingHint(boolean z2) {
        this.q = z2;
    }

    public void setTestAdType(int i2) {
        this.m = i2;
    }

    public void setTestMode(boolean z2) {
        this.l = z2;
    }

    public void setTextColor(int i2) {
        this.h = i2;
        if (this.a != null) {
            this.a.a(i2);
        }
        invalidate();
    }

    public void setTransitionType(int i2) {
        this.k = i2;
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
            }
        }
    }
}
