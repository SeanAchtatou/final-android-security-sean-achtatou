package com.qq.provider;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.util.Log;
import com.qq.AppService.AppService;
import com.qq.AppService.s;
import com.qq.d.b;
import com.qq.g.c;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.tmsecurelite.virusscan.IScanListener;
import com.tencent.tmsecurelite.virusscan.IVirusScan;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

/* compiled from: ProGuard */
public final class ak implements Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static ak f329a = null;
    /* access modifiers changed from: private */
    public IVirusScan b = null;
    /* access modifiers changed from: private */
    public an c = null;
    /* access modifiers changed from: private */
    public volatile boolean d = false;
    private IScanListener e = new al(this);
    private Context f = null;
    private BufferedInputStream g = null;
    private BufferedOutputStream h = null;
    /* access modifiers changed from: private */
    public Vector<byte[]> i = null;
    private volatile boolean j = false;

    /* access modifiers changed from: private */
    public void a(boolean z, IVirusScan iVirusScan) {
        if (!z) {
            this.b = iVirusScan;
        }
    }

    public static synchronized ak a() {
        ak akVar;
        synchronized (ak.class) {
            if (f329a == null) {
                f329a = new ak();
            }
            akVar = f329a;
        }
        return akVar;
    }

    public static synchronized void a(Context context) {
        synchronized (ak.class) {
            if (f329a != null) {
                Context b2 = AppService.b();
                if (b2 != null) {
                    context = b2;
                }
                f329a.b(context);
                for (int i2 = 0; i2 < 1000 && f329a.b != null; i2++) {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
                f329a = null;
            }
        }
    }

    public void b(Context context) {
        Context applicationContext = context.getApplicationContext();
        if (!(applicationContext == null || this.c == null)) {
            try {
                applicationContext.unbindService(this.c);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            Log.d("com.qq.connect", "unbindservice com.tencent.qqpimsecure::virusScanManager!");
        }
        this.b = null;
        this.c = null;
    }

    private void c(Context context) {
        new am(this, context).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.ak.a(boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.qq.provider.ak.a(com.qq.provider.ak, com.qq.provider.an):com.qq.provider.an
      com.qq.provider.ak.a(com.qq.provider.ak, com.tencent.tmsecurelite.virusscan.IVirusScan):com.tencent.tmsecurelite.virusscan.IVirusScan
      com.qq.provider.ak.a(int, int):void
      com.qq.provider.ak.a(int, com.qq.d.b):void
      com.qq.provider.ak.a(int, java.util.ArrayList<com.qq.d.b>):void
      com.qq.provider.ak.a(com.qq.provider.ak, byte[]):void
      com.qq.provider.ak.a(com.qq.provider.ak, boolean):boolean
      com.qq.provider.ak.a(com.qq.provider.ak, int):byte[]
      com.qq.provider.ak.a(android.content.Context, com.qq.g.c):void
      com.qq.provider.ak.a(boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void */
    private void b() {
        a(true, (IVirusScan) null);
        if (this.b != null) {
            try {
                this.b.freeScanner();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (this.c != null) {
            this.c.a();
        }
        a(false, (IVirusScan) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.ak.a(boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.qq.provider.ak.a(com.qq.provider.ak, com.qq.provider.an):com.qq.provider.an
      com.qq.provider.ak.a(com.qq.provider.ak, com.tencent.tmsecurelite.virusscan.IVirusScan):com.tencent.tmsecurelite.virusscan.IVirusScan
      com.qq.provider.ak.a(int, int):void
      com.qq.provider.ak.a(int, com.qq.d.b):void
      com.qq.provider.ak.a(int, java.util.ArrayList<com.qq.d.b>):void
      com.qq.provider.ak.a(com.qq.provider.ak, byte[]):void
      com.qq.provider.ak.a(com.qq.provider.ak, boolean):boolean
      com.qq.provider.ak.a(com.qq.provider.ak, int):byte[]
      com.qq.provider.ak.a(android.content.Context, com.qq.g.c):void
      com.qq.provider.ak.a(boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void */
    public void a(Context context, c cVar) {
        PackageInfo packageInfo;
        boolean z;
        ArrayList arrayList = new ArrayList();
        if (this.b != null) {
            arrayList.add(s.a(2));
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        q.a(context);
        b();
        try {
            packageInfo = context.getPackageManager().getPackageInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME, 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null) {
            arrayList.add(s.a(-2));
            cVar.a(arrayList);
            cVar.a(0);
        } else if (packageInfo.versionCode < 1007) {
            arrayList.add(s.a(-1));
            cVar.a(arrayList);
            cVar.a(0);
        } else if (cVar.e() < 1) {
            cVar.a(1);
        } else {
            this.c = new an(this, null);
            int h2 = cVar.h();
            this.c.a(h2);
            c(context);
            int b2 = this.c.b(h2);
            if (h2 > 0 || b2 > 0) {
                a(true, (IVirusScan) null);
                if (this.b == null) {
                    try {
                        Thread.sleep(450);
                    } catch (InterruptedException e3) {
                        e3.printStackTrace();
                    }
                }
                a(true, (IVirusScan) null);
                if (this.b == null) {
                    arrayList.add(s.a(-4));
                    cVar.a(arrayList);
                    cVar.a(0);
                    return;
                }
                try {
                    z = this.b.checkVersion(2);
                } catch (RemoteException e4) {
                    e4.printStackTrace();
                    z = false;
                }
                if (!z) {
                    b();
                    arrayList.add(s.a(-3));
                    cVar.a(arrayList);
                    cVar.a(0);
                    return;
                }
                arrayList.add(s.a(1));
                cVar.a(arrayList);
                cVar.a(0);
                return;
            }
            arrayList.add(s.a(b2));
            cVar.a(arrayList);
            cVar.a(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.provider.ak.a(boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.qq.provider.ak.a(com.qq.provider.ak, com.qq.provider.an):com.qq.provider.an
      com.qq.provider.ak.a(com.qq.provider.ak, com.tencent.tmsecurelite.virusscan.IVirusScan):com.tencent.tmsecurelite.virusscan.IVirusScan
      com.qq.provider.ak.a(int, int):void
      com.qq.provider.ak.a(int, com.qq.d.b):void
      com.qq.provider.ak.a(int, java.util.ArrayList<com.qq.d.b>):void
      com.qq.provider.ak.a(com.qq.provider.ak, byte[]):void
      com.qq.provider.ak.a(com.qq.provider.ak, boolean):boolean
      com.qq.provider.ak.a(com.qq.provider.ak, int):byte[]
      com.qq.provider.ak.a(android.content.Context, com.qq.g.c):void
      com.qq.provider.ak.a(boolean, com.tencent.tmsecurelite.virusscan.IVirusScan):void */
    private void a(Context context, c cVar, int i2) {
        PackageInfo packageInfo;
        boolean z;
        ArrayList arrayList = new ArrayList();
        if (this.b != null) {
            arrayList.add(s.a(2));
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        q.a(context);
        b();
        try {
            packageInfo = context.getPackageManager().getPackageInfo(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME, 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null) {
            arrayList.add(s.a(-2));
            cVar.a(arrayList);
            cVar.a(0);
        } else if (packageInfo.versionCode < 1007) {
            arrayList.add(s.a(-1));
            cVar.a(arrayList);
            cVar.a(0);
        } else {
            this.c = new an(this, null);
            this.c.a(i2);
            c(context);
            int b2 = this.c.b(i2);
            if (i2 > 0 || b2 > 0) {
                a(true, (IVirusScan) null);
                if (this.b == null) {
                    try {
                        Thread.sleep(450);
                    } catch (InterruptedException e3) {
                        e3.printStackTrace();
                    }
                }
                a(true, (IVirusScan) null);
                if (this.b == null) {
                    arrayList.add(s.a(-4));
                    cVar.a(arrayList);
                    cVar.a(0);
                    return;
                }
                try {
                    z = this.b.checkVersion(2);
                } catch (RemoteException e4) {
                    e4.printStackTrace();
                    z = false;
                }
                if (!z) {
                    b();
                    arrayList.add(s.a(-3));
                    cVar.a(arrayList);
                    cVar.a(0);
                    return;
                }
                arrayList.add(s.a(1));
                cVar.a(arrayList);
                cVar.a(0);
                return;
            }
            arrayList.add(s.a(b2));
            cVar.a(arrayList);
            cVar.a(0);
        }
    }

    public void b(Context context, c cVar) {
        b();
        a(context);
        cVar.a(0);
    }

    public void c(Context context, c cVar) {
        boolean z;
        boolean z2 = true;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 2) {
            cVar.a(1);
        } else {
            if (cVar.h() > 0) {
                z = true;
            } else {
                z = false;
            }
            if (cVar.h() <= 0) {
                z2 = false;
            }
            if (this.d && z2) {
                try {
                    this.b.cancelScan();
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            } else if (this.d) {
                cVar.a(4);
                return;
            }
            try {
                this.b.scanInstalledPackages(this.e, z);
            } catch (RemoteException e4) {
                e4.printStackTrace();
            }
        }
    }

    public void d(Context context, c cVar) {
        boolean z;
        boolean z2 = true;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 2) {
            cVar.a(1);
        } else {
            File b2 = n.b();
            String c2 = n.c();
            if (b2 == null && c2 == null) {
                cVar.a(9);
                return;
            }
            if (cVar.h() > 0) {
                z = true;
            } else {
                z = false;
            }
            if (cVar.h() <= 0) {
                z2 = false;
            }
            if (this.d && z2) {
                try {
                    this.b.cancelScan();
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            } else if (this.d) {
                cVar.a(4);
                return;
            }
            try {
                this.b.scanSdcardApks(this.e, z);
            } catch (RemoteException e4) {
                e4.printStackTrace();
            }
        }
    }

    public void e(Context context, c cVar) {
        boolean z;
        boolean z2 = true;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 2) {
            cVar.a(1);
        } else {
            if (cVar.h() > 0) {
                z = true;
            } else {
                z = false;
            }
            if (cVar.h() <= 0) {
                z2 = false;
            }
            if (this.d && z2) {
                try {
                    this.b.cancelScan();
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            } else if (this.d) {
                cVar.a(4);
                return;
            }
            try {
                this.b.scanGlobal(this.e, z);
            } catch (RemoteException e4) {
                e4.printStackTrace();
            }
        }
    }

    public void f(Context context, c cVar) {
        boolean z;
        boolean z2;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 3) {
            cVar.a(1);
        } else {
            if (cVar.h() > 0) {
                z = true;
            } else {
                z = false;
            }
            if (cVar.h() > 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            int h2 = cVar.h();
            if (this.d && z2) {
                try {
                    this.b.cancelScan();
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            } else if (this.d) {
                cVar.a(4);
                return;
            }
            if (h2 <= 0) {
                cVar.a(1);
            } else if (cVar.e() < h2 + 3) {
                cVar.a(1);
            } else {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < h2; i2++) {
                    String j2 = cVar.j();
                    if (!s.b(j2)) {
                        arrayList.add(j2);
                    }
                }
                try {
                    this.b.scanApks(arrayList, this.e, z);
                } catch (RemoteException e4) {
                    e4.printStackTrace();
                }
            }
        }
    }

    public void g(Context context, c cVar) {
        boolean z;
        boolean z2;
        if (this.b == null) {
            cVar.a(7);
        } else if (cVar.e() < 3) {
            cVar.a(1);
        } else {
            if (cVar.h() > 0) {
                z = true;
            } else {
                z = false;
            }
            if (cVar.h() > 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            int h2 = cVar.h();
            if (this.d && z2) {
                try {
                    this.b.cancelScan();
                } catch (RemoteException e2) {
                    e2.printStackTrace();
                }
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
            if (h2 <= 0) {
                cVar.a(1);
            } else if (cVar.e() < h2 + 3) {
                cVar.a(1);
            } else {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < h2; i2++) {
                    String j2 = cVar.j();
                    if (!s.b(j2)) {
                        arrayList.add(j2);
                    }
                }
                try {
                    this.b.scanPackages(arrayList, this.e, z);
                } catch (RemoteException e4) {
                    e4.printStackTrace();
                }
            }
        }
    }

    public void h(Context context, c cVar) {
        if (this.b == null) {
            cVar.a(7);
            return;
        }
        try {
            this.b.pauseScan();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void i(Context context, c cVar) {
        if (this.b == null) {
            cVar.a(7);
            return;
        }
        try {
            this.b.continueScan();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void j(Context context, c cVar) {
        if (this.b == null) {
            cVar.a(7);
            return;
        }
        try {
            this.b.cancelScan();
        } catch (RemoteException e2) {
            e2.printStackTrace();
        }
    }

    public void a(Context context, BufferedInputStream bufferedInputStream, BufferedOutputStream bufferedOutputStream) {
        this.f = context;
        this.g = bufferedInputStream;
        this.h = bufferedOutputStream;
        this.i = new Vector<>(16);
    }

    private void c() {
        this.g = null;
        this.h = null;
        this.f = null;
        this.i = null;
    }

    public void run() {
        if (this.g != null && this.h != null && this.f != null) {
            c cVar = new c();
            a(this.f, cVar, (int) EventDispatcherEnum.CACHE_EVENT_START);
            a(cVar);
            this.j = true;
            while (this.j && this.i != null) {
                try {
                    if (this.i.size() > 0) {
                        b(this.i.get(0));
                        this.i.remove(0);
                    } else {
                        synchronized (this) {
                            try {
                                wait(50);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            c();
            return;
        }
        return;
    }

    /* access modifiers changed from: private */
    public void a(byte[] bArr) {
        if (bArr != null && this.i != null && this.j) {
            this.i.add(bArr);
            synchronized (this) {
                notifyAll();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        a(b(i2));
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3) {
        byte[] bArr = new byte[28];
        System.arraycopy(s.a(28), 0, bArr, 0, 4);
        System.arraycopy(s.a(4), 0, bArr, 4, 4);
        System.arraycopy(s.a(16), 0, bArr, 8, 4);
        System.arraycopy(s.a(4), 0, bArr, 12, 4);
        System.arraycopy(s.a(i2), 0, bArr, 16, 4);
        System.arraycopy(s.a(4), 0, bArr, 20, 4);
        System.arraycopy(s.a(i3), 0, bArr, 24, 4);
        a(bArr);
    }

    /* access modifiers changed from: private */
    public void a(int i2, b bVar) {
        if (bVar != null) {
            a(b.a(i2, bVar));
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, ArrayList<b> arrayList) {
        if (arrayList != null) {
            a(b.a(i2, arrayList));
        }
    }

    private void b(byte[] bArr) {
        if (this.h != null) {
            try {
                this.h.write(bArr);
                this.h.flush();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void a(c cVar) {
        if (this.h != null && cVar != null) {
            b(s.a(cVar.a(), cVar.b()));
        }
    }

    /* access modifiers changed from: private */
    public byte[] b(int i2) {
        byte[] bArr = new byte[20];
        System.arraycopy(s.a(20), 0, bArr, 0, 4);
        System.arraycopy(s.a(4), 0, bArr, 4, 4);
        System.arraycopy(s.a(16), 0, bArr, 8, 4);
        System.arraycopy(s.a(4), 0, bArr, 12, 4);
        System.arraycopy(s.a(i2), 0, bArr, 16, 4);
        return bArr;
    }
}
