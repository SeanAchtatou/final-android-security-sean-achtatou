package com.qq.AppService;

import android.content.Context;
import android.util.Log;
import com.qq.g.c;
import com.qq.provider.a;
import com.qq.provider.aa;
import com.qq.provider.ab;
import com.qq.provider.af;
import com.qq.provider.ai;
import com.qq.provider.aj;
import com.qq.provider.ak;
import com.qq.provider.b;
import com.qq.provider.d;
import com.qq.provider.e;
import com.qq.provider.f;
import com.qq.provider.g;
import com.qq.provider.h;
import com.qq.provider.j;
import com.qq.provider.l;
import com.qq.provider.n;
import com.qq.provider.o;
import com.qq.provider.p;
import com.qq.provider.q;
import com.qq.provider.v;
import com.qq.provider.w;
import com.qq.provider.x;
import com.qq.provider.y;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class ag extends Thread {

    /* renamed from: a  reason: collision with root package name */
    public volatile boolean f215a = false;
    public volatile boolean b = false;
    private c c = null;
    private Context d = null;
    private Socket e = null;
    private volatile boolean f = false;
    private byte[] g = new byte[4];
    private InputStream h = null;
    private OutputStream i = null;
    private boolean j = false;
    private boolean k = false;

    public ag(Context context, Socket socket) {
        this.d = context;
        this.e = socket;
        this.f = true;
    }

    public void a() {
        this.j = true;
    }

    public void a(Socket socket) {
        if (this.f215a) {
            this.b = false;
            this.f215a = false;
            this.e = socket;
            synchronized (this) {
                notify();
            }
        }
    }

    public void b() {
        this.f = false;
        this.f215a = false;
        if (this.e != null) {
            try {
                this.e.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.e = null;
        }
        synchronized (this) {
            notify();
        }
    }

    public void run() {
        while (this.f) {
            if (this.f215a) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            if (!this.f215a) {
                if (this.e == null) {
                    this.f215a = true;
                    this.e = null;
                } else {
                    c();
                    this.f215a = true;
                    if (this.j) {
                        return;
                    }
                }
            }
        }
    }

    public void c() {
        byte[] a2;
        System.currentTimeMillis();
        if (this.e != null) {
            try {
                this.h = this.e.getInputStream();
                if (this.h == null) {
                    this.e.close();
                    this.e = null;
                    if (!this.b) {
                        this.b = true;
                    }
                    if (this.e != null) {
                        try {
                            this.e.close();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        this.e = null;
                    }
                    if (this.h != null) {
                        try {
                            this.h.close();
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                        this.h = null;
                    }
                    if (this.i != null) {
                        try {
                            this.i.close();
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                        this.i = null;
                    }
                    if (this.c != null) {
                        this.c.b = null;
                    } else {
                        return;
                    }
                } else {
                    this.i = this.e.getOutputStream();
                    if (this.i == null) {
                        if (this.e != null) {
                            this.e.close();
                            this.e = null;
                        }
                        if (this.h != null) {
                            try {
                                this.h.close();
                            } catch (Exception e5) {
                                e5.printStackTrace();
                            }
                            this.h = null;
                        }
                        if (!this.b) {
                            this.b = true;
                        }
                        if (this.e != null) {
                            try {
                                this.e.close();
                            } catch (Exception e6) {
                                e6.printStackTrace();
                            }
                            this.e = null;
                        }
                        if (this.h != null) {
                            try {
                                this.h.close();
                            } catch (Exception e7) {
                                e7.printStackTrace();
                            }
                            this.h = null;
                        }
                        if (this.i != null) {
                            try {
                                this.i.close();
                            } catch (Exception e8) {
                                e8.printStackTrace();
                            }
                            this.i = null;
                        }
                        if (this.c != null) {
                            this.c.b = null;
                        } else {
                            return;
                        }
                    } else {
                        int read = this.h.read(this.g);
                        if (read < this.g.length) {
                            if (!this.b) {
                                this.b = true;
                            }
                            if (this.e != null) {
                                try {
                                    this.e.close();
                                } catch (Exception e9) {
                                    e9.printStackTrace();
                                }
                                this.e = null;
                            }
                            if (this.h != null) {
                                try {
                                    this.h.close();
                                } catch (Exception e10) {
                                    e10.printStackTrace();
                                }
                                this.h = null;
                            }
                            if (this.i != null) {
                                try {
                                    this.i.close();
                                } catch (Exception e11) {
                                    e11.printStackTrace();
                                }
                                this.i = null;
                            }
                            if (this.c != null) {
                                this.c.b = null;
                            } else {
                                return;
                            }
                        } else {
                            int a3 = s.a(this.g);
                            if (a3 <= 0 || a3 >= 4194304) {
                                if (!this.b) {
                                    this.b = true;
                                }
                                if (this.e != null) {
                                    try {
                                        this.e.close();
                                    } catch (Exception e12) {
                                        e12.printStackTrace();
                                    }
                                    this.e = null;
                                }
                                if (this.h != null) {
                                    try {
                                        this.h.close();
                                    } catch (Exception e13) {
                                        e13.printStackTrace();
                                    }
                                    this.h = null;
                                }
                                if (this.i != null) {
                                    try {
                                        this.i.close();
                                    } catch (Exception e14) {
                                        e14.printStackTrace();
                                    }
                                    this.i = null;
                                }
                                if (this.c != null) {
                                    this.c.b = null;
                                } else {
                                    return;
                                }
                            } else {
                                this.c = new c();
                                this.c.b = new byte[a3];
                                int i2 = read;
                                while (i2 >= 0 && read < a3) {
                                    i2 = this.h.read(this.c.b, read, a3 - read);
                                    if (i2 > 0) {
                                        read += i2;
                                    }
                                }
                                this.i.write(s.hs);
                                this.i.flush();
                                this.b = true;
                                if (read < a3 && this.c != null) {
                                    this.c.b = null;
                                }
                                if (this.c == null || this.c.b == null) {
                                    Log.d("com.qq.connect", "no data");
                                }
                                if (!(this.c == null || this.c.b == null)) {
                                    this.k = false;
                                    try {
                                        a2 = a(this.d, this.e);
                                    } catch (Throwable th) {
                                        th.printStackTrace();
                                        if (this.c == null || this.c.d() == null) {
                                            Log.d("com.qq.connect", th.toString() + " cmd is null");
                                        } else {
                                            Log.d("com.qq.connect", th.toString() + " cmd:" + new String(this.c.d()));
                                        }
                                        a2 = s.a(8, (ArrayList<byte[]>) null);
                                    }
                                    if (this.k) {
                                        h.a();
                                    }
                                    if (a2 != null) {
                                        int length = a2.length;
                                        int i3 = 0;
                                        while (length > i3) {
                                            if (length - i3 >= 8192) {
                                                this.i.write(a2, i3, 8192);
                                                this.i.flush();
                                                i3 += 8192;
                                            } else {
                                                this.i.write(a2, i3, length - i3);
                                                this.i.flush();
                                                i3 = length;
                                            }
                                        }
                                        this.i.write(s.hs);
                                        this.i.flush();
                                    }
                                }
                                if (this.e != null) {
                                    try {
                                        this.e.close();
                                    } catch (Exception e15) {
                                        e15.printStackTrace();
                                    }
                                    this.e = null;
                                }
                                if (this.h != null) {
                                    try {
                                        this.h.close();
                                    } catch (Exception e16) {
                                        e16.printStackTrace();
                                    }
                                    this.h = null;
                                }
                                if (this.i != null) {
                                    try {
                                        this.i.close();
                                    } catch (Exception e17) {
                                        e17.printStackTrace();
                                    }
                                    this.i = null;
                                }
                                System.currentTimeMillis();
                                if (!this.b) {
                                    this.b = true;
                                }
                                if (this.e != null) {
                                    try {
                                        this.e.close();
                                    } catch (Exception e18) {
                                        e18.printStackTrace();
                                    }
                                    this.e = null;
                                }
                                if (this.h != null) {
                                    try {
                                        this.h.close();
                                    } catch (Exception e19) {
                                        e19.printStackTrace();
                                    }
                                    this.h = null;
                                }
                                if (this.i != null) {
                                    try {
                                        this.i.close();
                                    } catch (Exception e20) {
                                        e20.printStackTrace();
                                    }
                                    this.i = null;
                                }
                                if (this.c != null) {
                                    this.c.b = null;
                                } else {
                                    return;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e21) {
                e21.printStackTrace();
            } catch (Throwable th2) {
                try {
                    th2.printStackTrace();
                    if (!this.b) {
                        this.b = true;
                    }
                    if (this.e != null) {
                        try {
                            this.e.close();
                        } catch (Exception e22) {
                            e22.printStackTrace();
                        }
                        this.e = null;
                    }
                    if (this.h != null) {
                        try {
                            this.h.close();
                        } catch (Exception e23) {
                            e23.printStackTrace();
                        }
                        this.h = null;
                    }
                    if (this.i != null) {
                        try {
                            this.i.close();
                        } catch (Exception e24) {
                            e24.printStackTrace();
                        }
                        this.i = null;
                    }
                    if (this.c != null) {
                        this.c.b = null;
                    } else {
                        return;
                    }
                } catch (Throwable th3) {
                    if (!this.b) {
                        this.b = true;
                    }
                    if (this.e != null) {
                        try {
                            this.e.close();
                        } catch (Exception e25) {
                            e25.printStackTrace();
                        }
                        this.e = null;
                    }
                    if (this.h != null) {
                        try {
                            this.h.close();
                        } catch (Exception e26) {
                            e26.printStackTrace();
                        }
                        this.h = null;
                    }
                    if (this.i != null) {
                        try {
                            this.i.close();
                        } catch (Exception e27) {
                            e27.printStackTrace();
                        }
                        this.i = null;
                    }
                    if (this.c != null) {
                        this.c.b = null;
                        this.c = null;
                    }
                    throw th3;
                }
            }
            this.c = null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r6v0, types: [com.qq.AppService.ag] */
    private byte[] a(Context context, Socket socket) {
        byte[] bArr;
        this.c.c();
        byte[] d2 = this.c.d();
        if (AppService.d) {
            String inetAddress = socket.getInetAddress().toString();
            if (!inetAddress.contains("127.0.0.1") && (AppService.g == null || !AppService.g.contains(inetAddress))) {
                if (!s.b(d2, s.bf)) {
                    return s.a(4, (ArrayList<byte[]>) null);
                }
                ab.a().h(context, this.c);
                return s.a(this.c.a(), this.c.b());
            }
        }
        if (this.c.f293a) {
            if (!AppService.e) {
                if (s.b(d2, s.ba) || s.b(d2, s.bG) || s.b(d2, s.gV)) {
                    this.k = false;
                } else if (s.b(d2, s.bx) || s.b(d2, s.bk)) {
                    this.k = false;
                } else if (s.b(d2, s.da)) {
                    this.k = false;
                } else if (s.b(d2, s.aY) || s.b(d2, s.bB)) {
                    this.k = false;
                } else if (s.b(d2, s.bA) || s.b(d2, s.bd)) {
                    this.k = false;
                } else if (s.b(d2, s.dA)) {
                    this.k = false;
                } else if (s.b(d2, s.aZ) || s.b(d2, s.bb)) {
                    this.k = false;
                } else if (s.b(d2, s.bg) || s.b(d2, s.bD)) {
                    this.k = false;
                } else if (s.b(d2, s.bE)) {
                    this.k = false;
                } else if (s.b(d2, s.gc) || s.b(d2, s.bt)) {
                    this.k = false;
                } else if (s.b(d2, s.aH)) {
                    this.k = false;
                } else {
                    this.k = true;
                    Context b2 = AppService.b();
                    if (b2 == null) {
                        b2 = context;
                    }
                    h.a(b2, (String) null);
                }
            }
            if (s.b(d2, s.f251a)) {
                j.a().F(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.b)) {
                j.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.d)) {
                j.a().E(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.k)) {
                j.a().s(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.e)) {
                j.a().x(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.h)) {
                j.a().A(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.g)) {
                j.a().C(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.i)) {
                j.a().m(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.f)) {
                j.a().y(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.c)) {
                j.a().D(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.j)) {
                j.a().n(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.A)) {
                j.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.B)) {
                j.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.C)) {
                j.a().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.D)) {
                j.a().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.E)) {
                j.a().o(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.F)) {
                j.a().q(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.G)) {
                j.a().p(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.H)) {
                j.a().r(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.I)) {
                j.a().u(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.J)) {
                j.a().v(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.K)) {
                j.a().w(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.z)) {
                j.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.l)) {
                j.a().S(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.m)) {
                j.a().R(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.n)) {
                j.a().T(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.w)) {
                j.a().L(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.v)) {
                j.a().N(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.u)) {
                j.a().M(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.s)) {
                j.a().I(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.r)) {
                j.a().H(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.x)) {
                j.a().Q(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.q)) {
                j.a().y(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.t)) {
                j.a().K(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.y)) {
                j.a().G(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.o)) {
                j.a().O(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.p)) {
                j.a().P(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.L)) {
                w.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.M)) {
                w.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.N)) {
                w.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.O)) {
                w.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.P)) {
                w.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.U)) {
                j.a().t(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.V)) {
                j.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.W)) {
                j.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.X)) {
                j.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.Y)) {
                j.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.Z)) {
                j.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aa)) {
                j.a().J(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ab)) {
                j.a().ac(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ac)) {
                j.a().ab(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ad)) {
                j.a().U(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ae)) {
                j.a().W(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.af)) {
                j.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ag)) {
                j.a().z(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aj)) {
                f.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ah)) {
                f.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.an)) {
                f.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.al)) {
                f.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ak)) {
                f.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ai)) {
                f.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.am)) {
                f.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ao)) {
                f.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ap)) {
                f.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aq)) {
                g.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.Q)) {
                j.a().Z(context, this.c);
                bArr = null;
            } else if (s.b(d2, s.S)) {
                j.a().X(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.R)) {
                j.a().V(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.T)) {
                j.a().Y(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bb)) {
                ab.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aU)) {
                ab.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aY)) {
                ab.a().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bc)) {
                ab.a().m(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aX)) {
                ab.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aZ)) {
                ab.a().p(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ba)) {
                ab.a().C(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bd)) {
                ab.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aW)) {
                ab.a().q(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.aV)) {
                ab.a().r(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.bf)) {
                ab.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bg)) {
                ab.a().a(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bh)) {
                ab.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bj)) {
                ab.s(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bk)) {
                ab.a().t(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bl)) {
                ab.a().u(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bm)) {
                ab.a().v(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bn)) {
                ab.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bo)) {
                ab.a().a(context, socket);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.be)) {
                if (!ab.a().A(context, this.c)) {
                    bArr = s.a(this.c.a(), this.c.b());
                } else {
                    bArr = this.c.b;
                }
            } else if (s.b(d2, s.bv)) {
                if (!ab.a().z(context, this.c)) {
                    bArr = s.a(this.c.a(), this.c.b());
                } else {
                    bArr = this.c.b;
                }
            } else if (s.b(d2, s.bt)) {
                ab.a().y(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.br)) {
                ab.a().w(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bs)) {
                ab.a().x(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bx)) {
                ab.a().a(context, this.c, socket);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.by)) {
                ab.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bz)) {
                new v().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bA)) {
                ab.a().n(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bB)) {
                ab.a().o(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bC)) {
                ab.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bD)) {
                ab.a().B(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bE)) {
                ab.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bF)) {
                ab.a().H(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bG)) {
                ab.a().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bH)) {
                ab.a().F(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bI)) {
                ab.a().J(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bJ)) {
                ab.a().I(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bp)) {
                com.qq.provider.ag.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bq)) {
                com.qq.provider.ag.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cp)) {
                ab.a().G(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cq)) {
                ab.a().D(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cr)) {
                ab.a().E(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bi)) {
                aa.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cs)) {
                p.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ct)) {
                p.a().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cu)) {
                p.a().n(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cv)) {
                p.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cw)) {
                p.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cx)) {
                p.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cy)) {
                p.a().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cz)) {
                p.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cA)) {
                p.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cB)) {
                p.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cC)) {
                p.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cG)) {
                p.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cH)) {
                p.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cD)) {
                a.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cE)) {
                a.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cF)) {
                a.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cI)) {
                p.a().o(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cJ)) {
                p.a().p(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cK)) {
                p.a().w(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cL)) {
                p.a().x(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cM)) {
                p.a().y(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cN)) {
                p.a().r(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cO)) {
                p.a().q(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cP)) {
                p.a().A(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cQ)) {
                p.a().z(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cR)) {
                p.a().v(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cS)) {
                p.a().u(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cT)) {
                p.a().s(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cU)) {
                p.a().t(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cV)) {
                p.a().B(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cW)) {
                p.a().C(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cX)) {
                p.a().m(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cY)) {
                p.a().D(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cZ)) {
                p.a().E(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aD)) {
                y.a(context).a(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.at)) {
                y.a(context).b(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ay)) {
                y.a(context).c(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.az)) {
                y.a(context).d(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.av)) {
                y.a(context).g(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aw)) {
                y.a(context).h(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ax)) {
                x.a(context).a(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aC)) {
                x.a(context).b(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ar)) {
                y.a(context).i(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.as)) {
                y.a(context).j(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.au)) {
                y.a(context).k(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aB)) {
                y.a(context).l(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aA)) {
                y.a(context).m(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aE)) {
                y.a(context).e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aF)) {
                y.a(context).d(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.aG)) {
                y.a(context).e(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aH)) {
                y.a(context).a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aI)) {
                y.a(context).f(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aJ)) {
                y.a(context).f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aK)) {
                y.a(context).b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aL)) {
                y.a(context).c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aM)) {
                bArr = null;
            } else if (s.b(d2, s.aN)) {
                x.a(context).c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aO)) {
                x.a(context).a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aP)) {
                x.a(context).b(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.aT)) {
                x.a(context).g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aQ)) {
                x.a(context).e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aR)) {
                x.a(context).f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.aS)) {
                x.a(context).d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.da)) {
                h.c().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.de)) {
                h.c().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dh)) {
                n.a().a(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dA)) {
                n.a().b(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.di)) {
                n.a().c(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dj)) {
                n.a().d(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dk)) {
                n.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dl)) {
                n.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dm)) {
                n.a().e(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dn)) {
                n.a().f(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.f0do)) {
                n.a().o(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dp)) {
                n.a().o(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dq)) {
                n.a().g(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dr)) {
                n.a().g(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ds)) {
                n.a().h(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dt)) {
                n.a().h(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.du)) {
                n.a().i(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dv)) {
                n.a().i(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dx)) {
                n.a().j(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dw)) {
                n.a().l(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dB)) {
                n.a().m(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dC)) {
                n.a().n(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dD)) {
                n.a().k(this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dE)) {
                n.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dL)) {
                n.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dM)) {
                n.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dN)) {
                n.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dO)) {
                n.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dP)) {
                n.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dQ)) {
                n.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dR)) {
                n.a().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dS)) {
                n.a().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dT)) {
                n.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dU)) {
                n.a().m(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dV)) {
                n.a().o(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dW)) {
                n.a().s(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dX)) {
                n.a().p(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dY)) {
                n.a().q(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.dZ)) {
                n.a().r(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ea)) {
                n.a().n(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ec)) {
                o.a(context).t(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ed)) {
                o.a(context).u(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ee)) {
                o.a(context).i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ef)) {
                o.a(context).k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eg)) {
                o.a(context).o(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eh)) {
                o.a(context).p(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ei)) {
                o.a(context).r(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ej)) {
                o.a(context).h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ek)) {
                o.a(context).s(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.el)) {
                o.a(context).q(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ep)) {
                o.a(context).w(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eq)) {
                o.a(context).x(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.em)) {
                o.a(context).f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eo)) {
                o.a(context).v(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.er)) {
                o.a(context).y(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.es)) {
                o.a(context).z(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.et)) {
                o.a(context).A(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.eu)) {
                o.a(context).d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ev)) {
                o.a(context).e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.en)) {
                o.a(context).j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ew)) {
                o.a(context).g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ex)) {
                o.a(context).a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ey)) {
                o.a(context).c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ez)) {
                o.a(context).b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eA)) {
                o.a(context).B(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eJ)) {
                o.a(context).H(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eB)) {
                o.a(context).m(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eC)) {
                o.a(context).C(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eD)) {
                o.a(context).D(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eE)) {
                o.a(context).E(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eF)) {
                o.a(context).F(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eG)) {
                o.a(context).G(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eH)) {
                o.a(context).l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eI)) {
                o.a(context).n(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eK)) {
                com.qq.provider.c.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eL)) {
                com.qq.provider.c.a().g(context, this.c);
                bArr = s.a(this.c.a(), (ArrayList<byte[]>) null);
            } else if (s.b(d2, s.eM)) {
                com.qq.provider.c.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eN)) {
                com.qq.provider.c.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eO)) {
                com.qq.provider.c.a().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eP)) {
                com.qq.provider.c.a().n(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fm)) {
                com.qq.provider.c.a().o(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eQ)) {
                com.qq.provider.c.a().p(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eR)) {
                com.qq.provider.c.a().s(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eW)) {
                com.qq.provider.c.a().t(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eX)) {
                com.qq.provider.c.a().v(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eS)) {
                com.qq.provider.c.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eY)) {
                com.qq.provider.c.a().r(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eT)) {
                com.qq.provider.c.a().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eU)) {
                com.qq.provider.c.a().m(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eV)) {
                com.qq.provider.c.a().u(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.eZ)) {
                com.qq.provider.c.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fa)) {
                com.qq.provider.c.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fl)) {
                com.qq.provider.c.a().q(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fn)) {
                com.qq.provider.c.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fo)) {
                com.qq.provider.c.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fr)) {
                com.qq.provider.c.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fb)) {
                com.qq.provider.c.a().w(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fc)) {
                com.qq.provider.c.a().x(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fd)) {
                com.qq.provider.c.a().y(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fe)) {
                com.qq.provider.c.a().z(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ff)) {
                com.qq.provider.c.a().A(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fg)) {
                com.qq.provider.c.a().B(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fh)) {
                com.qq.provider.c.a().C(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fi)) {
                com.qq.provider.c.a().D(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fj)) {
                com.qq.provider.c.a().E(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fk)) {
                com.qq.provider.c.a().F(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fp)) {
                com.qq.provider.c.a().G(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fq)) {
                com.qq.provider.c.a().H(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fs)) {
                l.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ft)) {
                l.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fu)) {
                l.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fv)) {
                l.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fw)) {
                l.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fx)) {
                l.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fy)) {
                l.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fz)) {
                l.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fA)) {
                l.a().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fB)) {
                l.a().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fC)) {
                l.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fD)) {
                l.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fE)) {
                ai.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fF)) {
                ai.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fG)) {
                ai.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fH)) {
                ai.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fI)) {
                ai.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fJ)) {
                aj.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fK)) {
                ai.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fL)) {
                aj.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fM)) {
                ai.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fN)) {
                ai.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fO)) {
                aj.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fP)) {
                ai.a().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fQ)) {
                ai.a().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fR)) {
                ai.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fS)) {
                ai.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fT)) {
                ai.a().m(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bK)) {
                d.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bL)) {
                d.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bM)) {
                d.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bN)) {
                d.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bO)) {
                d.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bP)) {
                d.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bQ)) {
                a.a(context).b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bR)) {
                a.a(context).c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bS)) {
                a.a(context).d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bT)) {
                a.a(context).e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bU)) {
                a.a(context).f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bV)) {
                a.a(context).g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bW)) {
                a.a(context).h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bX)) {
                a.a(context).i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bY)) {
                a.a(context).a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.bZ)) {
                e.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ca)) {
                e.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cb)) {
                e.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cc)) {
                e.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cd)) {
                e.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ce)) {
                e.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cf)) {
                e.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cg)) {
                e.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ch)) {
                e.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ci)) {
                e.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cj)) {
                e.a().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ck)) {
                e.a().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cl)) {
                e.a().n(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cm)) {
                e.a().m(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.cn)) {
                af.a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.co)) {
                af.b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fU)) {
                b.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fW)) {
                b.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fV)) {
                b.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fX)) {
                b.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fY)) {
                b.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gc)) {
                new v().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gd)) {
                new v().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.fZ)) {
                new v().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gi)) {
                new v().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ga)) {
                new v().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gb)) {
                new v().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gg)) {
                new v().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gh)) {
                new v().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gj)) {
                new v().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gN)) {
                q.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gO)) {
                q.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gP)) {
                q.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gQ)) {
                q.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gX)) {
                q.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gR)) {
                q.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gS)) {
                q.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gT)) {
                q.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gU)) {
                q.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gV)) {
                q.a().l(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gW)) {
                q.a().m(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gY)) {
                q.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.gZ)) {
                q.a().k(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ha)) {
                q.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hb)) {
                ak.a().a(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hc)) {
                ak.a().b(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hd)) {
                ak.a().c(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.he)) {
                ak.a().d(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hf)) {
                ak.a().e(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hg)) {
                ak.a().f(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hh)) {
                ak.a().g(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hi)) {
                ak.a().h(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hj)) {
                ak.a().j(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hk)) {
                ak.a().i(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hm)) {
                q.a().n(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hn)) {
                q.a().o(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.ho)) {
                q.a().p(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else if (s.b(d2, s.hp)) {
                q.a().q(context, this.c);
                bArr = s.a(this.c.a(), this.c.b());
            } else {
                this.c.a(3);
                byte[] a2 = s.a(this.c.a(), (ArrayList<byte[]>) null);
                Log.d("com.qq.connect", "cmd:" + new String(d2) + " is not support");
                bArr = a2;
            }
            if (bArr != null) {
            }
            return bArr;
        } else if (s.b(d2, s.bf)) {
            ab.a().h(context, this.c);
            return s.a(this.c.a(), this.c.b());
        } else if (s.b(d2, s.dk)) {
            n.a().f(context, this.c);
            return s.a(this.c.a(), this.c.b());
        } else if (s.b(d2, s.dl)) {
            n.a().g(context, this.c);
            return s.a(this.c.a(), this.c.b());
        } else if (s.b(d2, s.dA)) {
            n.a().b(this.c);
            return s.a(this.c.a(), this.c.b());
        } else {
            Log.d("com.qq.connect", "request to exec:" + new String(d2) + " permission denied.");
            return s.a(4, (ArrayList<byte[]>) null);
        }
    }
}
