package com.qq.AppService;

import com.qq.m.a;
import com.tencent.assistant.utils.XLog;
import com.tencent.wcs.c.b;
import java.net.InetAddress;

/* compiled from: ProGuard */
class d implements as {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppService f237a;

    d(AppService appService) {
        this.f237a = appService;
    }

    public int a(boolean z) {
        XLog.d("com.qq.connect", "SingleConnectionServer onDisconnect:" + z);
        b.a("SingleConnectionServer.XCallback onDisconnect, isWifiModel " + AppService.d + "acceptAddress " + AppService.g + " isUsbModel " + AppService.e + " bPassive " + z);
        if ((!AppService.d || AppService.g == null || !AppService.g.contains("127.0.0.1")) && z) {
            new a().start();
        }
        return 0;
    }

    public int a(InetAddress inetAddress) {
        XLog.d("com.qq.connect", "onConnect");
        this.f237a.p();
        AstApp.i().j().sendEmptyMessage(5003);
        return 0;
    }
}
