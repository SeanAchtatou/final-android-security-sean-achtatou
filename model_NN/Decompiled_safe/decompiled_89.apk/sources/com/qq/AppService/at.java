package com.qq.AppService;

import android.util.Log;
import com.qq.ndk.Native;
import com.qq.provider.h;
import com.tencent.wcs.c.b;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;

/* compiled from: ProGuard */
class at extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ar f225a;
    private Socket b = null;
    private DataOutputStream c = null;
    /* access modifiers changed from: private */
    public volatile boolean d = true;
    private InputStream e = null;

    public at(ar arVar, Socket socket) {
        this.f225a = arVar;
        arVar.b = true;
        if (!(arVar.h == null || socket == null)) {
            arVar.h.a(socket.getInetAddress());
        }
        if (this.e != null) {
            try {
                this.e.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.e = null;
        }
        try {
            if (this.c != null) {
                this.c.close();
            }
            this.c = null;
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        try {
            if (this.b != null) {
                this.b.close();
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        this.b = socket;
        this.c = null;
    }

    public void a(boolean z) {
        if (this.d) {
            int i = 0;
            while (this.f225a.e.size() > 0 && i < 10) {
                synchronized (this.f225a) {
                    this.f225a.notifyAll();
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                i++;
            }
            boolean z2 = this.f225a.b;
            this.f225a.b = false;
            this.d = false;
            if (this.e != null) {
                try {
                    this.e.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                this.e = null;
            }
            try {
                if (this.c != null) {
                    this.c.close();
                }
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            this.c = null;
            try {
                if (this.b != null) {
                    this.b.close();
                }
                this.b = null;
            } catch (Exception e5) {
                e5.printStackTrace();
            }
            synchronized (this.f225a) {
                this.f225a.notifyAll();
            }
            if (z2 && this.f225a.h != null) {
                this.f225a.h.a(z);
            }
            ay.b();
        }
    }

    public void run() {
        int i;
        boolean z;
        super.run();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (!this.f225a.f224a || !this.d || this.b == null) {
                break;
            }
            if (this.e == null) {
                try {
                    this.e = this.b.getInputStream();
                } catch (IOException e2) {
                    e2.printStackTrace();
                } catch (Throwable th) {
                    b.a("SingleConnectionServer Throwable #########");
                    th.printStackTrace();
                }
            }
            if (this.b != null && this.c == null) {
                try {
                    this.c = new DataOutputStream(this.b.getOutputStream());
                } catch (Exception e3) {
                    try {
                        this.b.close();
                        this.b = null;
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
            }
            if (this.f225a.e.size() == 0) {
                synchronized (this.f225a) {
                    try {
                        this.f225a.wait(15000);
                    } catch (InterruptedException e5) {
                        e5.printStackTrace();
                    }
                }
                if (!this.f225a.f || this.f225a.e.size() != 0) {
                    i = 0;
                } else {
                    this.f225a.a(s.hs);
                    i = i2 + 1;
                }
            } else {
                i = 0;
            }
            int a2 = this.f225a.a(this.e);
            if (a2 > 0) {
                for (int i4 = 0; i4 < 10 && a2 > 0; i4++) {
                    a2 = this.f225a.a(this.e);
                }
                i = 0;
            } else if (a2 == -1) {
                b.a("SingleConnectionServer read == -1");
                Log.d("com.qq.connect", "read == -1");
                break;
            } else if (a2 == 0) {
            }
            if (i > 4) {
                Log.d("com.qq.connect", "heart > 4");
                b.a("SingleConnectionServer heart = " + i);
                if (AppService.e || AppService.g == null || !AppService.g.contains("127.0.0.1")) {
                    b.a("SingleConnectionServer heart > 4  break !!!" + i);
                } else {
                    i2 = i;
                }
            } else {
                try {
                    this.f225a.a(this.c);
                    if (AppService.d && !AppService.e && AppService.g != null && this.f225a.d && (i3 = i3 + 1) >= 50) {
                        try {
                            z = a();
                        } catch (Exception e6) {
                            e6.printStackTrace();
                            z = false;
                        }
                        b.a("SingleConnectionServer wifiAuth " + z);
                        if (!z) {
                            AppService.g = null;
                            break;
                        }
                        i3 = 0;
                    }
                    i2 = i;
                } catch (Exception e7) {
                    b.a("SingleConnectionServer send heart error");
                    try {
                        if (this.c != null) {
                            this.c.close();
                        }
                        this.c = null;
                        if (this.b != null) {
                            this.b.close();
                        }
                        this.b = null;
                    } catch (IOException e8) {
                        e8.printStackTrace();
                    }
                }
            }
        }
        b.a("SingleConnectionServer heart > 4  break !!!" + i);
        Log.d("com.qq.connect.chat", "thread ternimated");
        b.a("SingleConnectionServer Stop isConnect " + this.f225a.b);
        a(this.f225a.b);
    }

    private boolean a() {
        int i;
        long currentTimeMillis = System.currentTimeMillis();
        this.c.write(h.a(currentTimeMillis));
        this.c.flush();
        int i2 = 30;
        if (!AppService.e && AppService.g != null && AppService.g.contains("127.0.0.1")) {
            i2 = 100;
        }
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= i2) {
                i = i4;
                break;
            }
            try {
                i4 = b();
            } catch (SocketTimeoutException e2) {
            }
            if (i4 != 0) {
                i = i4;
                break;
            }
            i3++;
        }
        if (i <= 0) {
            Log.d("com.qq.connect", "auth:" + i);
            return false;
        }
        byte[] a2 = a(i);
        if (a2 == null) {
            return false;
        }
        byte[] unEncrypt = new Native().unEncrypt(a2, 0, a2.length);
        if (unEncrypt == null) {
            return false;
        }
        String b2 = s.b(unEncrypt);
        Log.d("com.qq.connect", "wifiAuth getReply" + b2);
        if (b2 == null) {
            return false;
        }
        long j = 0;
        try {
            j = Long.parseLong(b2);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        if (currentTimeMillis != j) {
            return false;
        }
        return true;
    }

    private int b() {
        byte[] bArr = new byte[4];
        int i = 0;
        if (this.e != null) {
            i = this.e.read(bArr);
        }
        if (i == 4) {
            return s.a(bArr);
        }
        return -1;
    }

    private byte[] a(int i) {
        int i2;
        if (i <= 0) {
            return null;
        }
        byte[] bArr = new byte[i];
        int i3 = 0;
        while (i3 < i) {
            try {
                i2 = this.e.read(bArr, i3, i - i3);
            } catch (SocketTimeoutException e2) {
                i2 = 0;
            }
            if (i2 > 0) {
                i3 += i2;
            } else if (i2 == -1) {
                break;
            }
        }
        if (i3 == i) {
            return bArr;
        }
        return null;
    }
}
