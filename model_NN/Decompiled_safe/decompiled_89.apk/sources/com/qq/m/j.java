package com.qq.m;

import com.qq.AppService.s;
import java.util.ArrayList;

/* compiled from: ProGuard */
public final class j {

    /* renamed from: a  reason: collision with root package name */
    public long f318a = 0;
    public int b = 0;
    public String c = null;
    public long d = 0;
    public long e = 0;
    public String f = null;
    public String g = null;
    public int h = 0;
    public String i = null;
    public byte[] j = null;
    public String k = null;
    public String l = null;
    public String m = null;
    public long n = 0;
    public long o = 0;
    public int p = 0;
    public String q = null;
    public String r;
    public boolean s = false;

    public void a(ArrayList<byte[]> arrayList) {
        int i2;
        if (arrayList != null) {
            arrayList.add(s.a(this.f318a));
            arrayList.add(s.a(this.b));
            arrayList.add(s.a(this.c));
            arrayList.add(s.a(this.d));
            arrayList.add(s.a(s.b(this.e)));
            arrayList.add(s.a(this.f));
            arrayList.add(s.a(this.g));
            arrayList.add(s.a(this.h));
            arrayList.add(s.a(this.i));
            if (this.j != null) {
                arrayList.add(this.j);
            } else {
                arrayList.add(s.hs);
            }
            arrayList.add(s.a(this.k));
            arrayList.add(s.a(this.l));
            arrayList.add(s.a(this.m));
            arrayList.add(s.a(this.n));
            if (this.o != 0) {
                arrayList.add(s.a(s.b(this.o)));
            } else {
                arrayList.add(s.hr);
            }
            arrayList.add(s.a(this.p));
            arrayList.add(s.a(this.q));
            arrayList.add(s.a(this.r));
            if (this.s) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            arrayList.add(s.a(i2));
        }
    }
}
