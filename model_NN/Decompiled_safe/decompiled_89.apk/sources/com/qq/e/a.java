package com.qq.e;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;
import com.qq.AppService.AstApp;
import com.tencent.wcs.b.c;

/* compiled from: ProGuard */
public final class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private volatile PowerManager.WakeLock f288a = null;
    private volatile PowerManager.WakeLock b = null;
    private volatile boolean c = false;
    private volatile int d = 0;

    public synchronized void a(Context context, String str) {
        synchronized (this) {
            if (this.d < 0) {
                this.d = 0;
            }
            this.d++;
            if (this.b == null) {
                ((KeyguardManager) context.getSystemService("keyguard")).newKeyguardLock("unLock").disableKeyguard();
                this.b = ((PowerManager) context.getSystemService("power")).newWakeLock(268435462, "unlock");
                this.b.acquire();
            } else if (this.b != null && !this.b.isHeld()) {
                this.b.acquire();
            }
        }
    }

    public synchronized void a() {
        synchronized (this) {
            this.d--;
            if (this.d <= 0) {
                notifyAll();
            }
        }
    }

    public void run() {
        super.run();
        this.c = true;
        this.d = 0;
        while (this.c) {
            if (this.d > 0 || this.b == null) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else if (this.d <= 0) {
                for (int i = 0; i < 10 && this.c; i++) {
                    try {
                        Thread.sleep(888);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
                if (!this.c) {
                    break;
                } else if (this.d <= 0) {
                    a(AstApp.i());
                }
            } else {
                this.d = 0;
            }
        }
        a(AstApp.i());
        if (this.f288a != null) {
            try {
                if (this.f288a.isHeld()) {
                    this.f288a.release();
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
            this.f288a = null;
            return;
        }
        return;
    }

    private void a(Context context) {
        boolean z = true;
        synchronized (this) {
            if (this.b == null || !this.b.isHeld()) {
                z = false;
            } else {
                this.b.release();
            }
            this.b = null;
            this.d = 0;
        }
        if (z && c.p > 0) {
            try {
                if (this.f288a != null) {
                    if (this.f288a.isHeld()) {
                        this.f288a.release();
                    }
                    this.f288a = null;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
            try {
                this.f288a = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "keep_lock");
                this.f288a.setReferenceCounted(false);
                this.f288a.acquire((long) (c.p * 1000));
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
        }
    }

    public void b() {
        this.c = false;
        synchronized (this) {
            notifyAll();
        }
    }
}
