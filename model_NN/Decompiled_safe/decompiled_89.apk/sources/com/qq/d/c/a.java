package com.qq.d.c;

import android.database.Cursor;
import com.qq.AppService.s;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public int f278a = 0;
    public String b = null;
    public String c = null;
    public String d = null;
    public long e = 0;
    public int f = -1;
    public String g = null;
    public String h = null;
    public String i = null;
    public String j = null;
    public int k = 0;

    public a() {
    }

    public a(Cursor cursor) {
        this.f278a = cursor.getInt(cursor.getColumnIndex("_id"));
        this.b = cursor.getString(cursor.getColumnIndex("name"));
        this.c = cursor.getString(cursor.getColumnIndex(SocialConstants.PARAM_TYPE));
        this.d = cursor.getString(cursor.getColumnIndex("data"));
        this.e = cursor.getLong(cursor.getColumnIndex("size"));
        this.f = cursor.getInt(cursor.getColumnIndex("kind"));
        this.g = cursor.getString(cursor.getColumnIndex("label"));
        this.h = s.b(cursor.getLong(cursor.getColumnIndex("date_add")));
        this.i = s.b(cursor.getLong(cursor.getColumnIndex("date_modify")));
        this.j = s.b(cursor.getLong(cursor.getColumnIndex("read_times")));
        this.k = cursor.getInt(cursor.getColumnIndex("read_times"));
    }
}
