package com.qq.ndk;

/* compiled from: ProGuard */
public class NativeFileObject {
    public static final int LNK = 18;
    public static final int MT = 23;
    public static final int PERMISION = 7777;
    public static final int REG = 16;
    public static final int SOCK = 20;
    public static final int S_IFBLK = 393216;
    public static final int S_IFCHR = 131072;
    public static final int S_IFDIR = 262144;
    public static final int S_IFIFO = 65536;
    public static final int S_IFLNK = 1179648;
    public static final int S_IFMT = 1507328;
    public static final int S_IFREG = 1048576;
    public static final int S_IFSOCK = 1310720;
    public static final int S_ISGID = 8192;
    public static final int S_ISUID = 16384;
    public static final int S_ISVTX = 4096;
    public String fileName = null;
    public long fileSize = 0;
    public int fileType = 0;
    public int gid = 0;
    public long llAccessTime = 0;
    public long llCreateTime = 0;
    public long llModifyTime = 0;
    public int permission = 0;
    public int type = 0;
    public int uid = 0;
}
