package com.qq.f;

import android.database.Cursor;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.qq.util.b;
import com.qq.util.g;
import java.io.File;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class a {
    private static a i;

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f289a;
    private volatile boolean b;
    private ReferenceQueue<String> c;
    private ArrayList<WeakReference<String>> d;
    private ReferenceQueue<String> e;
    private ArrayList<WeakReference<String>> f;
    private ArrayList<String> g;
    private ArrayList<String> h;

    private a() {
        this.f289a = false;
        this.b = false;
        this.d = null;
        this.f = null;
        this.g = new ArrayList<>();
        this.h = new ArrayList<>();
        this.c = new ReferenceQueue<>();
        this.d = new ArrayList<>();
        this.e = new ReferenceQueue<>();
        this.f = new ArrayList<>();
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (i == null) {
                i = new a();
            }
            aVar = i;
        }
        return aVar;
    }

    public void b() {
        if (!this.f289a) {
            new b(this).start();
        }
    }

    public void c() {
        if (!this.f289a) {
            this.f289a = true;
            this.g.clear();
            try {
                a(b.b().c());
                a(1);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            this.g.clear();
            this.f289a = false;
        }
    }

    public void d() {
        if (!this.b) {
            this.b = true;
            this.h.clear();
            try {
                a(g.a().b());
                a(2);
            } catch (Throwable th) {
                th.printStackTrace();
            }
            this.h.clear();
            this.b = false;
        }
    }

    private void a(ArrayList<String> arrayList) {
        File externalStorageDirectory;
        if (arrayList != null && arrayList.size() != 0) {
            if (("mounted".equals(Environment.getExternalStorageState()) && Environment.getExternalStorageDirectory().canRead()) && (externalStorageDirectory = Environment.getExternalStorageDirectory()) != null && externalStorageDirectory.exists()) {
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    String str = arrayList.get(i2);
                    if (!TextUtils.isEmpty(str)) {
                        a(externalStorageDirectory.getAbsolutePath() + File.separator + str);
                    }
                }
            }
        }
    }

    private void a(String str) {
        File[] listFiles;
        if (!TextUtils.isEmpty(str)) {
            File file = new File(str);
            if (!file.exists()) {
                return;
            }
            if (!file.isDirectory()) {
                int b2 = b(str);
                if (b2 == 1 && !this.g.contains(str) && b.b().c(str) > 0 && !c(str)) {
                    this.g.add(str);
                } else if (b2 == 2 && !this.h.contains(str)) {
                    this.h.add(str);
                }
            } else if (!new File(file, ".nomedia").exists() && (listFiles = file.listFiles()) != null && listFiles.length != 0) {
                for (File absolutePath : listFiles) {
                    a(absolutePath.getAbsolutePath());
                }
            }
        }
    }

    private int b(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        if (str.endsWith(".png") || str.endsWith(".jpg") || str.endsWith(".jpeg")) {
            return 1;
        }
        if (str.endsWith(".3gp") || str.endsWith(".mp4")) {
            return 2;
        }
        return 0;
    }

    private void a(int i2) {
        int i3;
        int i4;
        Cursor cursor = null;
        int i5 = 0;
        if (i2 != 1 && i2 != 2) {
            return;
        }
        if (i2 != 1 || this.g.size() != 0) {
            if (i2 != 2 || this.h.size() != 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(" in(");
                if (i2 == 1) {
                    i3 = this.g.size();
                } else if (i2 == 2) {
                    i3 = this.h.size();
                } else {
                    i3 = 0;
                }
                for (int i6 = 0; i6 < i3; i6++) {
                    if (i6 > 0) {
                        sb.append(',');
                    }
                    sb.append('\'');
                    if (i2 == 1) {
                        sb.append(this.g.get(i6).replace("'", "''"));
                    } else if (i2 == 2) {
                        sb.append(this.h.get(i6).replace("'", "''"));
                    }
                    sb.append('\'');
                }
                sb.append(')');
                if (i2 == 1) {
                    cursor = AstApp.i().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_data"}, "_data" + sb.toString(), null, null);
                } else if (i2 == 2) {
                    cursor = AstApp.i().getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, new String[]{"_data"}, "_data" + sb.toString(), null, null);
                }
                if (cursor != null) {
                    for (boolean moveToFirst = cursor.moveToFirst(); moveToFirst; moveToFirst = cursor.moveToNext()) {
                        String string = cursor.getString(0);
                        if (i2 == 1) {
                            this.g.remove(string);
                            d(string);
                        } else if (i2 == 2) {
                            this.h.remove(string);
                            e(string);
                        }
                    }
                    cursor.close();
                    if (i2 == 1) {
                        i4 = this.g.size();
                    } else if (i2 == 2) {
                        i4 = this.h.size();
                    } else {
                        i4 = 0;
                    }
                    if (i4 > 0) {
                        String[] strArr = new String[i4];
                        if (i2 == 1) {
                            while (i5 < i4) {
                                strArr[i5] = this.g.get(i5);
                                d(strArr[i5]);
                                i5++;
                            }
                        } else if (i2 == 2) {
                            while (i5 < i4) {
                                strArr[i5] = this.h.get(i5);
                                e(strArr[i5]);
                                i5++;
                            }
                        }
                        com.qq.a.a.b bVar = new com.qq.a.a.b(AstApp.i());
                        if (i2 == 1) {
                            bVar.a(strArr, "png");
                        } else if (i2 == 2) {
                            bVar.a(strArr, "3gp");
                        }
                    }
                }
            }
        }
    }

    private boolean c(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        while (true) {
            Reference<? extends String> poll = this.c.poll();
            if (poll == null) {
                break;
            }
            this.d.remove(poll);
        }
        Iterator<WeakReference<String>> it = this.d.iterator();
        while (it.hasNext()) {
            String str2 = (String) it.next().get();
            if (str2 != null && str.equals(str2)) {
                return true;
            }
        }
        return false;
    }

    private void d(String str) {
        if (!TextUtils.isEmpty(str)) {
            while (true) {
                Reference<? extends String> poll = this.c.poll();
                if (poll == null) {
                    break;
                }
                this.d.remove(poll);
            }
            Iterator<WeakReference<String>> it = this.d.iterator();
            while (it.hasNext()) {
                String str2 = (String) it.next().get();
                if (str2 != null && str.equals(str2)) {
                    return;
                }
            }
            this.d.add(new WeakReference(str, this.c));
        }
    }

    private void e(String str) {
        if (!TextUtils.isEmpty(str)) {
            while (true) {
                Reference<? extends String> poll = this.e.poll();
                if (poll == null) {
                    break;
                }
                this.f.remove(poll);
            }
            Iterator<WeakReference<String>> it = this.f.iterator();
            while (it.hasNext()) {
                String str2 = (String) it.next().get();
                if (str2 != null && str.equals(str2)) {
                    return;
                }
            }
            this.f.add(new WeakReference(str, this.e));
        }
    }
}
