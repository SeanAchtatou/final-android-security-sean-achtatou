package com.qq.i;

import com.qq.AppService.s;
import com.qq.taf.jce.JceStruct;
import com.tencent.connect.common.Constants;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;

/* compiled from: ProGuard */
public class a {
    public static String e = "10.96.88.33";
    public static int f = 8888;
    public static boolean g = false;
    private static final byte[] r = {JceStruct.SIMPLE_LIST, 10};

    /* renamed from: a  reason: collision with root package name */
    public String f297a = null;
    public String b = null;
    public String c = null;
    public int d = -1;
    private URL h = null;
    private URL[] i = null;
    private byte[] j = null;
    private int k = 0;
    private int l = 0;
    private String m = null;
    private Socket n = null;
    private InputStream o = null;
    private OutputStream p = null;
    private DataInputStream q = null;
    private int s = 0;
    private int t = 0;
    private boolean u = false;
    private String v = null;
    private String w = null;
    private String x = null;
    private String y = null;

    public void a(byte[] bArr) {
        this.j = bArr;
    }

    public void a(String str) {
        this.m = str;
    }

    public a(String str, boolean z, b[] bVarArr, byte[] bArr, String str2) {
        int i2 = 0;
        if (str != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            if (bVarArr != null && bVarArr.length > 0) {
                for (b bVar : bVarArr) {
                    if (!(bVar == null || bVar.f298a == null || bVar.b == null)) {
                        if (i2 == 0) {
                            sb.append('?');
                        } else {
                            sb.append('&');
                        }
                        i2++;
                        sb.append(bVar.f298a);
                        sb.append('=');
                        sb.append(URLEncoder.encode(bVar.b));
                    }
                }
            }
            try {
                this.h = new URL(sb.toString());
            } catch (MalformedURLException e2) {
                e2.printStackTrace();
            }
            if (this.h == null) {
                this.d = -1;
                return;
            }
            this.u = z;
            this.j = bArr;
            this.f297a = str2;
            this.n = new Socket();
        }
    }

    public int a() {
        return this.d;
    }

    public void a(int i2) {
        if (i2 > 0) {
            this.k = i2;
        }
    }

    public void b(int i2) {
        this.l = i2;
    }

    public void b() {
        InetSocketAddress inetSocketAddress;
        InetSocketAddress inetSocketAddress2;
        int i2 = 80;
        this.s = 0;
        if ((this.h != null || this.i != null) && this.n != null) {
            try {
                if (this.n != null) {
                    this.n.setSoTimeout(this.l);
                }
            } catch (SocketException e2) {
                e2.printStackTrace();
            }
            if (this.h != null) {
                String host = this.h.getHost();
                if (host == null) {
                    this.d = -2;
                    return;
                }
                int port = this.h.getPort();
                if (port > 0) {
                    i2 = port;
                }
                InetSocketAddress inetSocketAddress3 = new InetSocketAddress(host, i2);
                if (g) {
                    inetSocketAddress2 = new InetSocketAddress(e, f);
                } else {
                    inetSocketAddress2 = inetSocketAddress3;
                }
                try {
                    if (this.n != null) {
                        this.n.connect(inetSocketAddress2, this.k);
                    }
                } catch (IOException e3) {
                    f();
                    this.d = -3;
                    return;
                }
            } else if (this.i != null) {
                for (int i3 = 0; i3 < this.i.length; i3++) {
                    String host2 = this.i[i3].getHost();
                    if (host2 == null) {
                        this.d = -2;
                    } else {
                        if (this.n == null) {
                            this.n = new Socket();
                        }
                        int port2 = this.i[i3].getPort();
                        if (port2 <= 0) {
                            port2 = 80;
                        }
                        InetSocketAddress inetSocketAddress4 = new InetSocketAddress(host2, port2);
                        if (g) {
                            inetSocketAddress = new InetSocketAddress(e, f);
                        } else {
                            inetSocketAddress = inetSocketAddress4;
                        }
                        try {
                            if (this.n != null) {
                                this.n.connect(inetSocketAddress, this.k);
                            }
                        } catch (IOException e4) {
                            f();
                            this.d = -3;
                        }
                    }
                }
                if (this.n == null) {
                    this.d = -3;
                    return;
                }
            } else {
                return;
            }
            try {
                this.o = this.n.getInputStream();
                this.p = this.n.getOutputStream();
            } catch (Exception e5) {
                e5.printStackTrace();
            }
            if (this.o == null || this.p == null) {
                this.d = -4;
            }
        }
    }

    public void c() {
        String str;
        if (this.n != null && this.o != null && this.p != null && this.h != null) {
            String host = this.h.getHost();
            String path = this.h.getPath();
            if (path == null || !path.startsWith("/")) {
                path = "/";
            }
            String query = this.h.getQuery();
            if (query == null) {
                str = Constants.STR_EMPTY;
            } else {
                str = "?" + query;
            }
            try {
                if (!g) {
                    if (this.u) {
                        this.p.write(("POST " + path + str + " HTTP/1.1").getBytes("UTF-8"));
                    } else {
                        this.p.write(("GET " + path + str + " HTTP/1.1").getBytes("UTF-8"));
                    }
                } else if (this.u) {
                    this.p.write(("POST " + this.h + " HTTP/1.1").getBytes("UTF-8"));
                } else {
                    this.p.write(("GET " + this.h + " HTTP/1.1").getBytes("UTF-8"));
                }
                this.p.write(r);
                int port = this.h.getPort();
                if (port <= 0) {
                    this.p.write(("HOST: " + host).getBytes("UTF-8"));
                } else {
                    this.p.write(("HOST: " + host + ":" + port).getBytes("UTF-8"));
                }
                this.p.write(r);
                this.p.write("User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)".getBytes());
                if (this.m != null) {
                    this.p.write(r);
                    this.p.write(("Content-Type: " + this.m).getBytes());
                }
                if (this.v != null) {
                    this.p.write(r);
                    this.p.write(("If-Modified-Since: " + this.v).getBytes());
                }
                if (this.w != null) {
                    this.p.write(r);
                    this.p.write(("ETag: " + this.w).getBytes());
                }
                if (this.x != null) {
                    this.p.write(r);
                    this.p.write(("If-None-Match: " + this.x).getBytes());
                }
                if (this.u) {
                    this.p.write(r);
                    this.p.write(("Content-Length: " + this.j.length).getBytes("UTF-8"));
                    if (this.f297a != null && this.f297a.length() > 0) {
                        this.p.write(r);
                        this.p.write(("Cookie: " + this.f297a).getBytes("UTF-8"));
                    }
                    this.p.write(r);
                    this.p.write(r);
                    this.p.flush();
                    this.p.write(this.j);
                    this.p.flush();
                } else {
                    if (this.f297a != null && this.f297a.length() > 0) {
                        this.p.write(r);
                        this.p.write(("Cookie: " + this.f297a).getBytes("UTF-8"));
                    }
                    this.p.write(r);
                    this.p.write(r);
                    this.p.flush();
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            try {
                h();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            if (this.s == 301 || (this.s == 302 && this.c != null)) {
                f();
                this.h = null;
                try {
                    this.h = new URL(this.c);
                } catch (MalformedURLException e4) {
                    e4.printStackTrace();
                }
                if (this.h != null) {
                    this.n = new Socket();
                    b();
                    c();
                }
            }
        }
    }

    public byte[] a(boolean z) {
        boolean z2;
        byte[] bArr;
        if (this.s != 200) {
            this.d = this.s;
            return null;
        } else if (this.q == null) {
            return null;
        } else {
            int i2 = 1;
            int i3 = 0;
            boolean z3 = false;
            byte[] bArr2 = null;
            while (true) {
                if (i2 <= 0) {
                    break;
                }
                if (this.t <= 0) {
                    this.t = 0;
                    int g2 = g();
                    if (g2 > 0) {
                        this.t = g2;
                        z3 = true;
                    }
                    z2 = z3;
                } else {
                    if (z3) {
                        int g3 = g();
                        if (g3 <= 0) {
                            break;
                        }
                        this.t = g3 + this.t;
                    }
                    z2 = z3;
                }
                if (this.t <= i3) {
                    bArr = bArr2;
                } else if (bArr2 == null) {
                    bArr = new byte[this.t];
                } else {
                    bArr = new byte[this.t];
                    System.arraycopy(bArr2, 0, bArr, 0, i3);
                }
                if (this.t > i3) {
                    int i4 = i3;
                    while (i4 < this.t) {
                        try {
                            int i5 = this.t - i4;
                            if (i5 > 8192) {
                                i5 = 8192;
                            }
                            i2 = this.q.read(bArr, i4, i5);
                            if (i2 > 0) {
                                i4 += i2;
                            } else if (i2 == -1) {
                                break;
                            }
                        } catch (IOException e2) {
                            e2.printStackTrace();
                            i3 = i4;
                            bArr2 = bArr;
                            z3 = z2;
                        }
                    }
                    i3 = i4;
                    bArr2 = bArr;
                    z3 = z2;
                } else if (this.t != i3 || !z2) {
                    bArr2 = bArr;
                } else {
                    bArr2 = bArr;
                    z3 = z2;
                }
            }
            bArr2 = bArr;
            if (i3 < this.t) {
                this.d = -5;
                return null;
            }
            this.d = 0;
            return bArr2;
        }
    }

    private int g() {
        String str;
        String str2 = null;
        if (this.q == null) {
            return -1;
        }
        try {
            str = this.q.readLine();
        } catch (Throwable th) {
            th.printStackTrace();
            str = null;
        }
        while (str != null && s.b(str)) {
            try {
                str = this.q.readLine();
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
        }
        str2 = str;
        if (!s.b(str2)) {
            try {
                return Integer.parseInt(str2, 16);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return 0;
    }

    private int b(String str) {
        int indexOf;
        String substring;
        if (str == null || (indexOf = str.indexOf(58)) < 0 || indexOf + 1 >= str.length() || (substring = str.substring(indexOf + 1)) == null) {
            return -1;
        }
        try {
            this.t = Integer.parseInt(substring.trim());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return this.t;
    }

    private String c(String str) {
        int indexOf;
        String substring;
        if (str != null && (indexOf = str.indexOf("filename=")) >= 0 && indexOf + 1 < str.length() && (substring = str.substring(indexOf + 1)) != null) {
            return substring.trim();
        }
        return null;
    }

    private String d(String str) {
        int indexOf;
        String substring;
        if (str != null && (indexOf = str.indexOf(58)) >= 0 && indexOf + 1 < str.length() && (substring = str.substring(indexOf + 1)) != null) {
            return substring.trim();
        }
        return null;
    }

    public int d() {
        return this.s;
    }

    private int e(String str) {
        String trim;
        int indexOf;
        String substring;
        if (str == null || (indexOf = (trim = str.trim()).indexOf(32)) < 0 || indexOf + 1 >= trim.length() || (substring = trim.substring(indexOf + 1)) == null) {
            return -1;
        }
        String trim2 = substring.trim();
        try {
            this.s = Integer.parseInt(trim2.substring(0, trim2.indexOf(32)));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return this.s;
    }

    private void h() {
        String c2;
        if (this.o != null) {
            this.q = new DataInputStream(this.o);
            String readLine = this.q.readLine();
            if (readLine != null) {
                e(readLine);
                while (readLine != null && !readLine.equals(Constants.STR_EMPTY)) {
                    if (readLine.startsWith("Content-Length")) {
                        b(readLine);
                    } else if (readLine.startsWith("Set-Cookie")) {
                        g(readLine);
                    } else if (readLine.startsWith("Location")) {
                        f(readLine);
                    } else if (readLine.startsWith("Last-Modified:")) {
                        String d2 = d(readLine);
                        if (d2 != null) {
                            this.v = d2;
                        }
                    } else if (readLine.startsWith("ETag:")) {
                        String d3 = d(readLine);
                        if (d3 != null) {
                            this.w = d3;
                        }
                    } else if (readLine.startsWith("If-None-Match:")) {
                        String d4 = d(readLine);
                        if (d4 != null) {
                            this.x = d4;
                        }
                    } else if (readLine.startsWith("Content-Disposition:") && (c2 = c(readLine)) != null) {
                        this.y = c2;
                    }
                    readLine = this.q.readLine();
                }
            }
        }
    }

    private void f(String str) {
        int indexOf;
        if (str != null && (indexOf = str.indexOf(58)) >= 0 && indexOf + 1 < str.length()) {
            this.c = str.substring(indexOf + 1);
        }
    }

    private void g(String str) {
        int indexOf;
        if (str != null && (indexOf = str.indexOf(58)) >= 0 && indexOf + 1 < str.length()) {
            this.b = str.substring(indexOf + 1);
        }
    }

    public InetAddress e() {
        if (this.n == null) {
            return null;
        }
        return this.n.getLocalAddress();
    }

    public synchronized void f() {
        if (this.o != null) {
            try {
                this.o.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            this.o = null;
        }
        if (this.p != null) {
            try {
                this.p.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            this.p = null;
        }
        if (this.q != null) {
            try {
                this.q.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            this.q = null;
        }
        if (this.n != null) {
            try {
                this.n.close();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
            this.n = null;
        }
        return;
    }
}
