package com.google.zxing.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.a;
import com.google.zxing.b.a.a.c;
import com.google.zxing.b.a.e;
import com.google.zxing.d;
import com.google.zxing.g;
import com.google.zxing.h;
import java.util.Hashtable;
import java.util.Vector;

public final class i extends k {

    /* renamed from: a  reason: collision with root package name */
    private final Vector f145a;

    public i(Hashtable hashtable) {
        Vector vector = hashtable == null ? null : (Vector) hashtable.get(d.c);
        boolean z = (hashtable == null || hashtable.get(d.g) == null) ? false : true;
        this.f145a = new Vector();
        if (vector != null) {
            if (vector.contains(a.f) || vector.contains(a.d) || vector.contains(a.e) || vector.contains(a.c)) {
                this.f145a.addElement(new j(hashtable));
            }
            if (vector.contains(a.i)) {
                this.f145a.addElement(new c(z));
            }
            if (vector.contains(a.j)) {
                this.f145a.addElement(new d());
            }
            if (vector.contains(a.h)) {
                this.f145a.addElement(new b());
            }
            if (vector.contains(a.l)) {
                this.f145a.addElement(new h());
            }
            if (vector.contains(a.k)) {
                this.f145a.addElement(new a());
            }
            if (vector.contains(a.m)) {
                this.f145a.addElement(new e());
            }
            if (vector.contains(a.o)) {
                this.f145a.addElement(new c());
            }
        }
        if (this.f145a.isEmpty()) {
            this.f145a.addElement(new j(hashtable));
            this.f145a.addElement(new c());
            this.f145a.addElement(new d());
            this.f145a.addElement(new b());
            this.f145a.addElement(new h());
            this.f145a.addElement(new e());
            this.f145a.addElement(new c());
        }
    }

    public h a(int i, com.google.zxing.common.a aVar, Hashtable hashtable) {
        int i2 = 0;
        while (i2 < this.f145a.size()) {
            try {
                return ((k) this.f145a.elementAt(i2)).a(i, aVar, hashtable);
            } catch (ReaderException e) {
                i2++;
            }
        }
        throw NotFoundException.a();
    }

    public void a() {
        int size = this.f145a.size();
        for (int i = 0; i < size; i++) {
            ((g) this.f145a.elementAt(i)).a();
        }
    }
}
