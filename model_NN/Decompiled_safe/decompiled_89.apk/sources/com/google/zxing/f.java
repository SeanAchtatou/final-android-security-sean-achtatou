package com.google.zxing;

import com.google.zxing.b.i;
import com.google.zxing.d.a;
import java.util.Hashtable;
import java.util.Vector;

public final class f implements g {

    /* renamed from: a  reason: collision with root package name */
    private Hashtable f194a;
    private Vector b;

    private h b(c cVar) {
        int i = 0;
        while (i < this.b.size()) {
            try {
                return ((g) this.b.elementAt(i)).a(cVar, this.f194a);
            } catch (ReaderException e) {
                i++;
            }
        }
        throw NotFoundException.a();
    }

    public h a(c cVar) {
        if (this.b == null) {
            a((Hashtable) null);
        }
        return b(cVar);
    }

    public h a(c cVar, Hashtable hashtable) {
        a(hashtable);
        return b(cVar);
    }

    public void a() {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            ((g) this.b.elementAt(i)).a();
        }
    }

    public void a(Hashtable hashtable) {
        boolean z = false;
        this.f194a = hashtable;
        boolean z2 = hashtable != null && hashtable.containsKey(d.d);
        Vector vector = hashtable == null ? null : (Vector) hashtable.get(d.c);
        this.b = new Vector();
        if (vector != null) {
            if (vector.contains(a.d) || vector.contains(a.c) || vector.contains(a.f) || vector.contains(a.e) || vector.contains(a.i) || vector.contains(a.j) || vector.contains(a.h) || vector.contains(a.l) || vector.contains(a.m) || vector.contains(a.o)) {
                z = true;
            }
            if (z && !z2) {
                this.b.addElement(new i(hashtable));
            }
            if (vector.contains(a.f114a)) {
                this.b.addElement(new a());
            }
            if (vector.contains(a.b)) {
                this.b.addElement(new com.google.zxing.a.a());
            }
            if (vector.contains(a.n)) {
                this.b.addElement(new com.google.zxing.c.a());
            }
            if (z && z2) {
                this.b.addElement(new i(hashtable));
            }
        }
        if (this.b.isEmpty()) {
            if (!z2) {
                this.b.addElement(new i(hashtable));
            }
            this.b.addElement(new a());
            this.b.addElement(new com.google.zxing.a.a());
            if (z2) {
                this.b.addElement(new i(hashtable));
            }
        }
    }
}
