package com.google.zxing.common.reedsolomon;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final a f171a;

    public c(a aVar) {
        this.f171a = aVar;
    }

    private int[] a(b bVar) {
        int a2 = bVar.a();
        if (a2 == 1) {
            return new int[]{bVar.a(1)};
        }
        int[] iArr = new int[a2];
        int i = 0;
        for (int i2 = 1; i2 < 256 && i < a2; i2++) {
            if (bVar.b(i2) == 0) {
                iArr[i] = this.f171a.c(i2);
                i++;
            }
        }
        if (i == a2) {
            return iArr;
        }
        throw new ReedSolomonException("Error locator degree does not match number of roots");
    }

    private int[] a(b bVar, int[] iArr, boolean z) {
        int i;
        int length = iArr.length;
        int[] iArr2 = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            int c = this.f171a.c(iArr[i2]);
            int i3 = 1;
            int i4 = 0;
            while (i4 < length) {
                if (i2 != i4) {
                    int c2 = this.f171a.c(iArr[i4], c);
                    i = this.f171a.c(i3, (c2 & 1) == 0 ? c2 | 1 : c2 & -2);
                } else {
                    i = i3;
                }
                i4++;
                i3 = i;
            }
            iArr2[i2] = this.f171a.c(bVar.b(c), this.f171a.c(i3));
            if (z) {
                iArr2[i2] = this.f171a.c(iArr2[i2], c);
            }
        }
        return iArr2;
    }

    private b[] a(b bVar, b bVar2, int i) {
        if (bVar.a() >= bVar2.a()) {
            b bVar3 = bVar2;
            bVar2 = bVar;
            bVar = bVar3;
        }
        b b = this.f171a.b();
        b a2 = this.f171a.a();
        b a3 = this.f171a.a();
        b b2 = this.f171a.b();
        while (bVar.a() >= i / 2) {
            if (bVar.b()) {
                throw new ReedSolomonException("r_{i-1} was zero");
            }
            b a4 = this.f171a.a();
            int c = this.f171a.c(bVar.a(bVar.a()));
            b bVar4 = a4;
            b bVar5 = bVar2;
            while (bVar5.a() >= bVar.a() && !bVar5.b()) {
                int a5 = bVar5.a() - bVar.a();
                int c2 = this.f171a.c(bVar5.a(bVar5.a()), c);
                bVar4 = bVar4.a(this.f171a.a(a5, c2));
                bVar5 = bVar5.a(bVar.a(a5, c2));
            }
            bVar2 = bVar;
            bVar = bVar5;
            b a6 = bVar4.b(a2).a(b);
            b = a2;
            a2 = a6;
            b bVar6 = b2;
            b2 = bVar4.b(b2).a(a3);
            a3 = bVar6;
        }
        int a7 = b2.a(0);
        if (a7 == 0) {
            throw new ReedSolomonException("sigmaTilde(0) was zero");
        }
        int c3 = this.f171a.c(a7);
        return new b[]{b2.c(c3), bVar.c(c3)};
    }

    public void a(int[] iArr, int i) {
        b bVar = new b(this.f171a, iArr);
        int[] iArr2 = new int[i];
        boolean equals = this.f171a.equals(a.b);
        int i2 = 0;
        boolean z = true;
        while (i2 < i) {
            int b = bVar.b(this.f171a.a(equals ? i2 + 1 : i2));
            iArr2[(iArr2.length - 1) - i2] = b;
            i2++;
            z = b != 0 ? false : z;
        }
        if (!z) {
            b[] a2 = a(this.f171a.a(i, 1), new b(this.f171a, iArr2), i);
            b bVar2 = a2[0];
            b bVar3 = a2[1];
            int[] a3 = a(bVar2);
            int[] a4 = a(bVar3, a3, equals);
            for (int i3 = 0; i3 < a3.length; i3++) {
                int length = (iArr.length - 1) - this.f171a.b(a3[i3]);
                if (length < 0) {
                    throw new ReedSolomonException("Bad error location");
                }
                iArr[length] = a.b(iArr[length], a4[i3]);
            }
        }
    }
}
