package com.google.zxing.a.a;

import com.google.zxing.FormatException;
import com.google.zxing.common.b;

final class a {

    /* renamed from: a  reason: collision with root package name */
    private final b f116a;
    private final b b;
    private final e c;

    a(b bVar) {
        int c2 = bVar.c();
        if (c2 < 10 || c2 > 144 || (c2 & 1) != 0) {
            throw FormatException.a();
        }
        this.c = a(bVar);
        this.f116a = b(bVar);
        this.b = new b(this.f116a.c());
    }

    /* access modifiers changed from: package-private */
    public int a(int i, int i2) {
        int i3 = (a(i + -1, 0, i, i2) ? 1 : 0) << 1;
        if (a(i - 1, 1, i, i2)) {
            i3 |= 1;
        }
        int i4 = i3 << 1;
        if (a(i - 1, 2, i, i2)) {
            i4 |= 1;
        }
        int i5 = i4 << 1;
        if (a(0, i2 - 2, i, i2)) {
            i5 |= 1;
        }
        int i6 = i5 << 1;
        if (a(0, i2 - 1, i, i2)) {
            i6 |= 1;
        }
        int i7 = i6 << 1;
        if (a(1, i2 - 1, i, i2)) {
            i7 |= 1;
        }
        int i8 = i7 << 1;
        if (a(2, i2 - 1, i, i2)) {
            i8 |= 1;
        }
        int i9 = i8 << 1;
        return a(3, i2 + -1, i, i2) ? i9 | 1 : i9;
    }

    /* access modifiers changed from: package-private */
    public e a(b bVar) {
        if (this.c != null) {
            return this.c;
        }
        int c2 = bVar.c();
        return e.a(c2, c2);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        if (i < 0) {
            i6 = i + i3;
            i5 = (4 - ((i3 + 4) & 7)) + i2;
        } else {
            i5 = i2;
            i6 = i;
        }
        if (i5 < 0) {
            i5 += i4;
            i6 += 4 - ((i4 + 4) & 7);
        }
        this.b.b(i5, i6);
        return this.f116a.a(i5, i6);
    }

    /* access modifiers changed from: package-private */
    public byte[] a() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        boolean z = false;
        byte[] bArr = new byte[this.c.f()];
        int c2 = this.f116a.c();
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        int i7 = 0;
        int i8 = 4;
        int i9 = 0;
        while (true) {
            if (i8 == c2 && i7 == 0 && !z4) {
                bArr[i9] = (byte) a(c2, c2);
                int i10 = i7 + 2;
                i3 = i9 + 1;
                i5 = i8 - 2;
                i6 = i10;
                z4 = true;
            } else if (i8 == c2 - 2 && i7 == 0 && (c2 & 3) != 0 && !z3) {
                bArr[i9] = (byte) b(c2, c2);
                int i11 = i7 + 2;
                i3 = i9 + 1;
                i5 = i8 - 2;
                i6 = i11;
                z3 = true;
            } else if (i8 == c2 + 4 && i7 == 2 && (c2 & 7) == 0 && !z2) {
                bArr[i9] = (byte) c(c2, c2);
                int i12 = i7 + 2;
                i3 = i9 + 1;
                i5 = i8 - 2;
                i6 = i12;
                z2 = true;
            } else if (i8 == c2 - 2 && i7 == 0 && (c2 & 7) == 4 && !z) {
                bArr[i9] = (byte) d(c2, c2);
                int i13 = i7 + 2;
                i3 = i9 + 1;
                i5 = i8 - 2;
                i6 = i13;
                z = true;
            } else {
                int i14 = i7;
                int i15 = i8;
                int i16 = i9;
                int i17 = i15;
                while (true) {
                    if (i17 >= c2 || i14 < 0 || this.b.a(i14, i17)) {
                        i = i16;
                    } else {
                        i = i16 + 1;
                        bArr[i16] = (byte) b(i17, i14, c2, c2);
                    }
                    i17 -= 2;
                    i2 = i14 + 2;
                    if (i17 < 0 || i2 >= c2) {
                        int i18 = i17 + 1;
                        int i19 = i2 + 3;
                        int i20 = i;
                    } else {
                        i14 = i2;
                        i16 = i;
                    }
                }
                int i182 = i17 + 1;
                int i192 = i2 + 3;
                int i202 = i;
                while (true) {
                    if (i182 < 0 || i192 >= c2 || this.b.a(i192, i182)) {
                        i3 = i202;
                    } else {
                        i3 = i202 + 1;
                        bArr[i202] = (byte) b(i182, i192, c2, c2);
                    }
                    i182 += 2;
                    i4 = i192 - 2;
                    if (i182 >= c2 || i4 < 0) {
                        i5 = i182 + 3;
                        i6 = i4 + 1;
                    } else {
                        i192 = i4;
                        i202 = i3;
                    }
                }
                i5 = i182 + 3;
                i6 = i4 + 1;
            }
            if (i5 >= c2 && i6 >= c2) {
                break;
            }
            i9 = i3;
            i7 = i6;
            i8 = i5;
        }
        if (i3 == this.c.f()) {
            return bArr;
        }
        throw FormatException.a();
    }

    /* access modifiers changed from: package-private */
    public int b(int i, int i2) {
        int i3 = (a(i + -3, 0, i, i2) ? 1 : 0) << 1;
        if (a(i - 2, 0, i, i2)) {
            i3 |= 1;
        }
        int i4 = i3 << 1;
        if (a(i - 1, 0, i, i2)) {
            i4 |= 1;
        }
        int i5 = i4 << 1;
        if (a(0, i2 - 4, i, i2)) {
            i5 |= 1;
        }
        int i6 = i5 << 1;
        if (a(0, i2 - 3, i, i2)) {
            i6 |= 1;
        }
        int i7 = i6 << 1;
        if (a(0, i2 - 2, i, i2)) {
            i7 |= 1;
        }
        int i8 = i7 << 1;
        if (a(0, i2 - 1, i, i2)) {
            i8 |= 1;
        }
        int i9 = i8 << 1;
        return a(1, i2 + -1, i, i2) ? i9 | 1 : i9;
    }

    /* access modifiers changed from: package-private */
    public int b(int i, int i2, int i3, int i4) {
        int i5 = 0;
        if (a(i - 2, i2 - 2, i3, i4)) {
            i5 = 1;
        }
        int i6 = i5 << 1;
        if (a(i - 2, i2 - 1, i3, i4)) {
            i6 |= 1;
        }
        int i7 = i6 << 1;
        if (a(i - 1, i2 - 2, i3, i4)) {
            i7 |= 1;
        }
        int i8 = i7 << 1;
        if (a(i - 1, i2 - 1, i3, i4)) {
            i8 |= 1;
        }
        int i9 = i8 << 1;
        if (a(i - 1, i2, i3, i4)) {
            i9 |= 1;
        }
        int i10 = i9 << 1;
        if (a(i, i2 - 2, i3, i4)) {
            i10 |= 1;
        }
        int i11 = i10 << 1;
        if (a(i, i2 - 1, i3, i4)) {
            i11 |= 1;
        }
        int i12 = i11 << 1;
        return a(i, i2, i3, i4) ? i12 | 1 : i12;
    }

    /* access modifiers changed from: package-private */
    public b b(b bVar) {
        int b2 = this.c.b();
        int c2 = this.c.c();
        if (bVar.c() != b2) {
            throw new IllegalArgumentException("Dimension of bitMarix must match the version size");
        }
        int d = this.c.d();
        int e = this.c.e();
        int i = b2 / d;
        int i2 = c2 / e;
        b bVar2 = new b(i * d);
        for (int i3 = 0; i3 < i; i3++) {
            int i4 = i3 * d;
            for (int i5 = 0; i5 < i2; i5++) {
                int i6 = i5 * e;
                for (int i7 = 0; i7 < d; i7++) {
                    int i8 = ((d + 2) * i3) + 1 + i7;
                    int i9 = i4 + i7;
                    for (int i10 = 0; i10 < e; i10++) {
                        if (bVar.a(((e + 2) * i5) + 1 + i10, i8)) {
                            bVar2.b(i6 + i10, i9);
                        }
                    }
                }
            }
        }
        return bVar2;
    }

    /* access modifiers changed from: package-private */
    public int c(int i, int i2) {
        int i3 = (a(i + -1, 0, i, i2) ? 1 : 0) << 1;
        if (a(i - 1, i2 - 1, i, i2)) {
            i3 |= 1;
        }
        int i4 = i3 << 1;
        if (a(0, i2 - 3, i, i2)) {
            i4 |= 1;
        }
        int i5 = i4 << 1;
        if (a(0, i2 - 2, i, i2)) {
            i5 |= 1;
        }
        int i6 = i5 << 1;
        if (a(0, i2 - 1, i, i2)) {
            i6 |= 1;
        }
        int i7 = i6 << 1;
        if (a(1, i2 - 3, i, i2)) {
            i7 |= 1;
        }
        int i8 = i7 << 1;
        if (a(1, i2 - 2, i, i2)) {
            i8 |= 1;
        }
        int i9 = i8 << 1;
        return a(1, i2 + -1, i, i2) ? i9 | 1 : i9;
    }

    /* access modifiers changed from: package-private */
    public int d(int i, int i2) {
        int i3 = (a(i + -3, 0, i, i2) ? 1 : 0) << 1;
        if (a(i - 2, 0, i, i2)) {
            i3 |= 1;
        }
        int i4 = i3 << 1;
        if (a(i - 1, 0, i, i2)) {
            i4 |= 1;
        }
        int i5 = i4 << 1;
        if (a(0, i2 - 2, i, i2)) {
            i5 |= 1;
        }
        int i6 = i5 << 1;
        if (a(0, i2 - 1, i, i2)) {
            i6 |= 1;
        }
        int i7 = i6 << 1;
        if (a(1, i2 - 1, i, i2)) {
            i7 |= 1;
        }
        int i8 = i7 << 1;
        if (a(2, i2 - 1, i, i2)) {
            i8 |= 1;
        }
        int i9 = i8 << 1;
        return a(3, i2 + -1, i, i2) ? i9 | 1 : i9;
    }
}
