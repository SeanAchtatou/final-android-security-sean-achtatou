package com.google.zxing;

import com.google.zxing.common.a;
import com.google.zxing.common.b;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final b f151a;
    private b b;

    public c(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Binarizer must be non-null.");
        }
        this.f151a = bVar;
        this.b = null;
    }

    public int a() {
        return this.f151a.a().b();
    }

    public a a(int i, a aVar) {
        return this.f151a.a(i, aVar);
    }

    public int b() {
        return this.f151a.a().c();
    }

    public b c() {
        if (this.b == null) {
            this.b = this.f151a.b();
        }
        return this.b;
    }

    public boolean d() {
        return this.f151a.a().d();
    }

    public c e() {
        return new c(this.f151a.a(this.f151a.a().e()));
    }
}
