package com.google.zxing.d.a;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    private final int f184a;
    private final s[] b;

    t(int i, s sVar) {
        this.f184a = i;
        this.b = new s[]{sVar};
    }

    t(int i, s sVar, s sVar2) {
        this.f184a = i;
        this.b = new s[]{sVar, sVar2};
    }

    public int a() {
        return this.f184a;
    }

    public s[] b() {
        return this.b;
    }
}
