package com.tencent.cloud.a;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.callback.w;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.CFTGetOneMoreAppRequest;
import com.tencent.assistant.protocol.jce.CFTGetOneMoreAppResponse;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.utils.ba;
import com.tencent.cloud.adapter.UpdateRecOneMoreAdapter;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a extends BaseEngine<w> {

    /* renamed from: a  reason: collision with root package name */
    private static a f3453a;
    /* access modifiers changed from: private */
    public int b;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f3453a == null) {
                f3453a = new a();
            }
            aVar = f3453a;
        }
        return aVar;
    }

    public int a(int i, long j) {
        CFTGetOneMoreAppRequest cFTGetOneMoreAppRequest = new CFTGetOneMoreAppRequest(i, j);
        UpdateRecOneMoreAdapter.f3459a = j;
        this.b = send(cFTGetOneMoreAppRequest);
        ba.a().postDelayed(new b(this, this.b), 1500);
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        CFTGetOneMoreAppResponse cFTGetOneMoreAppResponse;
        ArrayList<CardItem> arrayList;
        if (i == this.b && (cFTGetOneMoreAppResponse = (CFTGetOneMoreAppResponse) jceStruct2) != null && (arrayList = cFTGetOneMoreAppResponse.b) != null) {
            cFTGetOneMoreAppResponse.a();
            notifyDataChangedInMainThread(new e(this, u.a(arrayList, new d(this), 1)));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        if (i == this.b) {
            notifyDataChangedInMainThread(new f(this, i2));
        }
    }
}
