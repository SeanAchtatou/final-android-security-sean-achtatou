package com.tencent.cloud.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.updaterec.UpdateRecListItemInfoView;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
public class UpdateRecOneMoreAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    public static long f3459a = -1;
    /* access modifiers changed from: private */
    public Context b;
    private List<SimpleAppModel> c;
    private int d = 2000;
    private String e = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public View f;
    private b g = new b();

    public UpdateRecOneMoreAdapter(Context context, View view, List<SimpleAppModel> list) {
        this.b = context;
        this.f = view;
        this.c = list;
        if (context instanceof BaseActivity) {
            this.d = ((BaseActivity) context).f();
        }
    }

    public void a(String str) {
        this.e = str;
    }

    public int getCount() {
        if (this.c != null) {
            return this.c.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        c cVar;
        SimpleAppModel simpleAppModel = this.c.get(i);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, simpleAppModel, a.a(Constants.VIA_REPORT_TYPE_WPA_STATE, i), 100, null);
        if (buildSTInfo != null) {
            buildSTInfo.contentId = this.e;
            buildSTInfo.extraData = Constants.STR_EMPTY + f3459a;
        }
        if (view == null) {
            view = LayoutInflater.from(this.b).inflate((int) R.layout.updaterec_app_one_more_item, (ViewGroup) null);
            c cVar2 = new c(this, null);
            cVar2.f3462a = (TXAppIconView) view.findViewById(R.id.app_icon_img);
            cVar2.b = (TextView) view.findViewById(R.id.app_name_txt);
            cVar2.c = (DownloadButton) view.findViewById(R.id.state_app_btn);
            cVar2.h = (UpdateRecListItemInfoView) view.findViewById(R.id.download_info);
            cVar2.d = view.findViewById(R.id.app_updatesizeinfo);
            cVar2.e = (TextView) view.findViewById(R.id.app_size_sumsize);
            cVar2.f = (TextView) view.findViewById(R.id.app_score_truesize);
            cVar2.g = (ImageView) view.findViewById(R.id.app_one_more_item_line);
            view.setTag(cVar2);
            cVar = cVar2;
        } else {
            cVar = (c) view.getTag();
        }
        view.setOnClickListener(new a(this, simpleAppModel, buildSTInfo));
        if (simpleAppModel != null) {
            cVar.b.setText(simpleAppModel.d);
            cVar.f3462a.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        cVar.c.a(simpleAppModel);
        cVar.h.a(UpdateRecListItemInfoView.InfoType.ONEMORE_DESC);
        cVar.h.a(simpleAppModel);
        if (i == getCount() - 1) {
            cVar.g.setVisibility(8);
        } else {
            cVar.g.setVisibility(0);
        }
        if (s.a(simpleAppModel)) {
            cVar.c.setClickable(false);
        } else {
            cVar.c.setClickable(true);
            cVar.c.a(buildSTInfo, new b(this, simpleAppModel));
        }
        if (!(this.g == null || f3459a == -1)) {
            this.g.exposure(buildSTInfo);
            f3459a = -1;
        }
        return view;
    }
}
