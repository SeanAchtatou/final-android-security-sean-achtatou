package com.tencent.assistantv2.activity;

import com.tencent.assistant.db.table.z;
import com.tencent.assistant.module.callback.ag;
import com.tencent.assistant.protocol.jce.DesktopShortCut;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;

/* compiled from: ProGuard */
class cm extends ag {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActivity f2809a;

    cm(MainActivity mainActivity) {
        this.f2809a = mainActivity;
    }

    public void a(int i, int i2, ArrayList<DesktopShortCut> arrayList) {
        if (i2 == 0) {
            k.a(new STInfoV2(204002, "03_001", 2000, STConst.ST_DEFAULT_SLOT, 100));
            if (this.f2809a.Z == null) {
                z unused = this.f2809a.Z = new z();
            }
            ArrayList<DesktopShortCut> a2 = this.f2809a.Z.a();
            if (arrayList != null && arrayList.size() > 0) {
                ArrayList unused2 = this.f2809a.Y = arrayList;
                for (int i3 = 0; i3 < arrayList.size(); i3++) {
                    DesktopShortCut desktopShortCut = arrayList.get(i3);
                    if (a2 == null || a2.size() == 0) {
                        this.f2809a.a(desktopShortCut);
                    } else {
                        boolean z = false;
                        for (int i4 = 0; i4 < a2.size(); i4++) {
                            if (this.f2809a.a(desktopShortCut, a2.get(i3))) {
                                z = true;
                            }
                        }
                        if (!z) {
                            this.f2809a.a(desktopShortCut);
                        }
                    }
                }
                this.f2809a.a(a2);
            }
        }
    }
}
