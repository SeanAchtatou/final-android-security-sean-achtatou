package com.tencent.assistantv2.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.SplashActivity;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.m;
import com.tencent.assistant.model.ShareBaseModel;
import com.tencent.assistant.module.timer.RecoverAppListReceiver;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class bx extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuideActivity f2794a;

    bx(GuideActivity guideActivity) {
        this.f2794a = guideActivity;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2794a, 200);
        buildSTInfo.scene = STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO;
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            String str2 = (String) view.getTag(R.id.tma_st_slot_tag);
            if (this.f2794a.T != 0) {
                str = "03_" + str2;
            } else if (this.f2794a.S == null || !(this.f2794a.S.b == 1 || this.f2794a.S.b == 3)) {
                str = "04_003";
            } else {
                str = "04_" + str2;
            }
        }
        buildSTInfo.slotId = str;
        return buildSTInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.GuideActivity.a(com.tencent.assistantv2.activity.GuideActivity, boolean):boolean
     arg types: [com.tencent.assistantv2.activity.GuideActivity, int]
     candidates:
      com.tencent.assistantv2.activity.GuideActivity.a(com.tencent.assistantv2.activity.GuideActivity, int):int
      com.tencent.assistantv2.activity.GuideActivity.a(com.tencent.assistantv2.activity.GuideActivity, com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse):com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.GuideActivity.a(com.tencent.assistantv2.activity.GuideActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistantv2.activity.GuideActivity, com.tencent.assistant.model.ShareBaseModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistantv2.activity.GuideActivity, com.tencent.assistant.model.ShareBaseModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void */
    public void onTMAClick(View view) {
        String str;
        switch (view.getId()) {
            case R.id.btn_skip /*2131166053*/:
                if (SplashActivity.f384a) {
                    r.a(this.f2794a, System.currentTimeMillis());
                    m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
                }
                this.f2794a.A();
                return;
            case R.id.btn_login /*2131166054*/:
                boolean unused = this.f2794a.O = true;
                if (this.f2794a.V && SplashActivity.f384a) {
                    r.a(this.f2794a, System.currentTimeMillis());
                    m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
                }
                if (this.f2794a.V) {
                    b.a(this.f2794a, (Bundle) null, new ActionUrl("tmast://webview?url=http://qzs.qq.com/open/mobile/share_cj2015/index.html&mode=0", 1));
                    return;
                } else {
                    this.f2794a.A();
                    return;
                }
            case R.id.showtips /*2131166055*/:
            case R.id.loadingView /*2131166056*/:
            default:
                return;
            case R.id.layout_btn /*2131166057*/:
                this.f2794a.E().a(false);
                String str2 = a.f().b;
                ShareBaseModel shareBaseModel = new ShareBaseModel();
                shareBaseModel.c = this.f2794a.y();
                shareBaseModel.b = Constants.STR_EMPTY;
                shareBaseModel.f = Constants.STR_EMPTY;
                shareBaseModel.e = Constants.STR_EMPTY;
                shareBaseModel.f1633a = this.f2794a.getString(R.string.guide_share_title, new Object[]{"我"});
                XLog.i(this.f2794a.u, "targetUrl=" + shareBaseModel.d);
                if (this.f2794a.B.isChecked()) {
                    boolean unused2 = this.f2794a.N = !this.f2794a.C.isChecked();
                    this.f2794a.E().a((Activity) this.f2794a, shareBaseModel, false);
                }
                if (this.f2794a.C.isChecked()) {
                    GuideActivity guideActivity = this.f2794a;
                    Object[] objArr = new Object[1];
                    if (TextUtils.isEmpty(str2)) {
                        str = "我";
                    } else {
                        str = str2;
                    }
                    objArr[0] = str;
                    shareBaseModel.f1633a = guideActivity.getString(R.string.guide_share_title, objArr);
                    this.f2794a.E().a((Context) this.f2794a, shareBaseModel, true);
                }
                if (!this.f2794a.B.isChecked() && !this.f2794a.C.isChecked()) {
                    this.f2794a.A();
                    return;
                }
                return;
        }
    }
}
