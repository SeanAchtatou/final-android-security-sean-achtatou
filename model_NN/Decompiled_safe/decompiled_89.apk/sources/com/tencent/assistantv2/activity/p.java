package com.tencent.assistantv2.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class p extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f2858a;

    p(AppDetailActivityV5 appDetailActivityV5) {
        this.f2858a = appDetailActivityV5;
    }

    public void onTMAClick(View view) {
        int unused = this.f2858a.R = view.getId() - 136;
        this.f2858a.A.setCurrentItem(this.f2858a.R);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2858a.z, 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = a.a("07", this.clickViewId - 136);
            if (this.f2858a.aq != null) {
                buildSTInfo.appId = this.f2858a.aq.appId;
                buildSTInfo.contentId = this.f2858a.aq.contentId;
                buildSTInfo.updateWithExternalPara(this.f2858a.p);
            }
        }
        return buildSTInfo;
    }
}
