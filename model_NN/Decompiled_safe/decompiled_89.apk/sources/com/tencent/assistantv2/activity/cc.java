package com.tencent.assistantv2.activity;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.RankFriendsListAdapter;
import com.tencent.assistant.adapter.ee;
import com.tencent.assistant.component.RankFriendsListPage;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.de;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.adapter.RankNormalListAdapter;
import com.tencent.assistantv2.adapter.u;
import com.tencent.assistantv2.b.ae;
import com.tencent.assistantv2.b.ak;
import com.tencent.assistantv2.b.al;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.RankCustomizeListPage;
import com.tencent.assistantv2.component.RankNormalListPage;
import com.tencent.assistantv2.component.RankSecondNavigationView;
import com.tencent.assistantv2.component.TXSecondViewPager;
import com.tencent.assistantv2.component.co;
import com.tencent.assistantv2.manager.RankTabType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class cc extends ay implements UIEventListener, co {
    protected List<View> P = new ArrayList();
    protected List<Object> S = new ArrayList();
    private final String T = "HotTabActivity:";
    private ak U;
    private HashMap<String, cj> V = new HashMap<>();
    private HashMap<Integer, Boolean> W = new HashMap<>();
    private LinearLayout X;
    private TXSecondViewPager Y;
    private View Z;
    private View aa;
    /* access modifiers changed from: private */
    public int ab = -1;
    private int ac = -1;
    private ee ad = null;
    /* access modifiers changed from: private */
    public RankSecondNavigationView ae;
    private boolean af = true;
    private ApkResCallback.Stub ag = new cf(this);
    private ViewPager.OnPageChangeListener ah = new ch(this);
    private ListViewScrollListener ai = new ci(this);

    public void d(Bundle bundle) {
        super.d(bundle);
        System.currentTimeMillis();
        this.X = new LinearLayout(this.Q);
        a(this.X);
    }

    public void b(View view) {
        this.Z = view.findViewById(R.id.filterTip);
        this.aa = this.Z.findViewById(R.id.tip_filterBtn);
        this.Z.setVisibility(8);
        this.Y = (TXSecondViewPager) view.findViewById(R.id.vPager);
        this.ae = (RankSecondNavigationView) view.findViewById(R.id.navigationview);
        this.ae.a(this);
        this.U = ae.a().a(1);
        ArrayList arrayList = new ArrayList();
        for (al alVar : this.U.b) {
            arrayList.add(alVar.f2950a);
        }
        this.ae.a(arrayList);
        this.ad = new ee(this.P);
        this.Y.setAdapter(this.ad);
        this.Y.setOnPageChangeListener(this.ah);
        this.Y.a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.cc.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.activity.cc.a(com.tencent.assistantv2.activity.cc, int):int
      com.tencent.assistantv2.activity.cc.a(int, long):boolean
      com.tencent.assistantv2.activity.cc.a(com.tencent.assistantv2.b.al, com.tencent.assistantv2.activity.cj):boolean
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):void
      android.support.v4.app.Fragment.a(android.view.View, android.os.Bundle):void
      com.tencent.assistantv2.activity.cc.a(int, boolean):void */
    public void d(boolean z) {
        MainActivity mainActivity;
        int i;
        View inflate;
        super.j();
        if (this.af) {
            this.af = false;
            this.X.removeAllViews();
            try {
                inflate = this.R.inflate((int) R.layout.act3, (ViewGroup) null);
            } catch (Throwable th) {
                cq.a().b();
                inflate = this.R.inflate((int) R.layout.act3, (ViewGroup) null);
            }
            this.X.addView(inflate);
            this.X.requestLayout();
            this.X.forceLayout();
            this.X.invalidate();
            b(inflate);
        }
        if (this.Q instanceof MainActivity) {
            mainActivity = (MainActivity) this.Q;
        } else {
            mainActivity = null;
        }
        if (mainActivity != null) {
            i = mainActivity.j().getInt("colnum") - 1;
        } else {
            i = -1;
        }
        if (i >= 0 && i <= this.U.b.size() - 1) {
            a(i, false);
            ba.a().postDelayed(new cd(this), 200);
        } else if (this.ab == -1) {
            a(0, false);
            this.ae.a(0);
        } else {
            a(this.ab, false);
        }
        for (Object next : this.S) {
            if (next instanceof RankNormalListAdapter) {
                ((RankNormalListAdapter) next).c();
                ((RankNormalListAdapter) next).notifyDataSetChanged();
            } else if (next instanceof u) {
                ((u) next).a();
                ((u) next).notifyDataSetChanged();
            }
        }
        Iterator<String> it = this.V.keySet().iterator();
        if (it != null) {
            while (it.hasNext()) {
                cj cjVar = this.V.get(it.next());
                if (!(cjVar == null || cjVar.f2806a == null)) {
                    if (cjVar.f2806a instanceof RankNormalListPage) {
                        ((RankNormalListPage) cjVar.f2806a).onResume();
                    }
                    if (cjVar.f2806a instanceof RankCustomizeListPage) {
                        ((RankCustomizeListPage) cjVar.f2806a).e();
                    }
                }
            }
        }
        ApkResourceManager.getInstance().registerApkResCallback(this.ag);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW, this);
        ba.a().postDelayed(new ce(this), 200);
    }

    public void k() {
        super.k();
        for (Object next : this.S) {
            if (next instanceof RankNormalListAdapter) {
                ((RankNormalListAdapter) next).b();
            } else if (next instanceof u) {
                ((u) next).b();
            }
        }
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.ag);
    }

    public void n() {
        super.n();
        Iterator<String> it = this.V.keySet().iterator();
        if (it != null) {
            while (it.hasNext()) {
                cj cjVar = this.V.get(it.next());
                if (!(cjVar == null || cjVar.f2806a == null)) {
                    if (cjVar.f2806a instanceof RankNormalListPage) {
                        ((RankNormalListPage) cjVar.f2806a).onPause();
                    }
                    if (cjVar.f2806a instanceof RankCustomizeListPage) {
                        ((RankCustomizeListPage) cjVar.f2806a).d();
                    }
                }
            }
        }
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW, this);
    }

    public int J() {
        if (this.P == null || this.P.size() <= this.ab || this.ab < 0) {
            return STConst.ST_PAGE_RANK_HOT;
        }
        View view = this.P.get(this.ab);
        if (view instanceof RankNormalListPage) {
            return ((RankNormalListPage) this.P.get(this.ab)).getPageId();
        }
        return view instanceof RankCustomizeListPage ? ((RankCustomizeListPage) this.P.get(this.ab)).f() : STConst.ST_PAGE_RANK_HOT;
    }

    public cc() {
        super(MainActivity.i());
    }

    public void C() {
    }

    public int D() {
        return 0;
    }

    public int E() {
        return 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.cc.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.activity.cc.a(com.tencent.assistantv2.activity.cc, int):int
      com.tencent.assistantv2.activity.cc.a(int, long):boolean
      com.tencent.assistantv2.activity.cc.a(com.tencent.assistantv2.b.al, com.tencent.assistantv2.activity.cj):boolean
      android.support.v4.app.Fragment.a(android.content.Context, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.Fragment.a(int, android.support.v4.app.Fragment):void
      android.support.v4.app.Fragment.a(android.content.Intent, int):void
      android.support.v4.app.Fragment.a(android.view.Menu, android.view.MenuInflater):void
      android.support.v4.app.Fragment.a(android.view.View, android.os.Bundle):void
      com.tencent.assistantv2.activity.cc.a(int, boolean):void */
    public void b(int i) {
        a(i, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0047 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0157  */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r8, boolean r9) {
        /*
            r7 = this;
            r6 = 1
            com.tencent.assistantv2.manager.i r0 = com.tencent.assistantv2.manager.i.a()
            com.tencent.assistantv2.manager.j r0 = r0.b()
            boolean r4 = r0.d()
            r2 = 0
            com.tencent.assistantv2.b.ak r0 = r7.U
            java.util.List<com.tencent.assistantv2.b.al> r0 = r0.b
            java.lang.Object r0 = r0.get(r8)
            com.tencent.assistantv2.b.al r0 = (com.tencent.assistantv2.b.al) r0
            java.util.HashMap<java.lang.String, com.tencent.assistantv2.activity.cj> r1 = r7.V
            java.lang.String r3 = r0.f2950a
            java.lang.Object r1 = r1.get(r3)
            com.tencent.assistantv2.activity.cj r1 = (com.tencent.assistantv2.activity.cj) r1
            if (r0 == 0) goto L_0x0173
            if (r1 == 0) goto L_0x0173
            boolean r3 = r7.a(r0, r1)
            if (r3 == 0) goto L_0x0173
            int r3 = r0.b
            com.tencent.assistantv2.manager.RankTabType r5 = com.tencent.assistantv2.manager.RankTabType.APPLIST
            int r5 = r5.ordinal()
            if (r3 != r5) goto L_0x004c
            com.tencent.assistantv2.adapter.RankNormalListAdapter r3 = r1.b
            if (r3 == 0) goto L_0x0173
            android.view.View r1 = r1.f2806a
            com.tencent.assistantv2.component.RankNormalListPage r1 = (com.tencent.assistantv2.component.RankNormalListPage) r1
            boolean r1 = r1.getNeedRefresh()
            r3 = r1
        L_0x0043:
            int r1 = r7.ab
            if (r8 != r1) goto L_0x005a
            if (r4 != 0) goto L_0x005a
            if (r3 != 0) goto L_0x005a
        L_0x004b:
            return
        L_0x004c:
            com.tencent.assistantv2.adapter.u r3 = r1.c
            if (r3 == 0) goto L_0x0173
            android.view.View r1 = r1.f2806a
            com.tencent.assistantv2.component.RankCustomizeListPage r1 = (com.tencent.assistantv2.component.RankCustomizeListPage) r1
            boolean r1 = r1.g()
            r3 = r1
            goto L_0x0043
        L_0x005a:
            com.tencent.assistantv2.b.ak r1 = r7.U
            java.util.List<com.tencent.assistantv2.b.al> r1 = r1.b
            java.util.Iterator r4 = r1.iterator()
        L_0x0062:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x0090
            java.lang.Object r1 = r4.next()
            com.tencent.assistantv2.b.al r1 = (com.tencent.assistantv2.b.al) r1
            java.util.HashMap<java.lang.String, com.tencent.assistantv2.activity.cj> r2 = r7.V
            java.lang.String r5 = r1.f2950a
            boolean r2 = r2.containsKey(r5)
            if (r2 != 0) goto L_0x007c
            r7.b(r1)
            goto L_0x0062
        L_0x007c:
            java.util.HashMap<java.lang.String, com.tencent.assistantv2.activity.cj> r2 = r7.V
            java.lang.String r5 = r1.f2950a
            java.lang.Object r2 = r2.get(r5)
            com.tencent.assistantv2.activity.cj r2 = (com.tencent.assistantv2.activity.cj) r2
            boolean r2 = r7.a(r1, r2)
            if (r2 != 0) goto L_0x0062
            r7.b(r1)
            goto L_0x0062
        L_0x0090:
            if (r0 == 0) goto L_0x00e6
            java.util.HashMap<java.lang.String, com.tencent.assistantv2.activity.cj> r1 = r7.V
            java.lang.String r2 = r0.f2950a
            java.lang.Object r1 = r1.get(r2)
            com.tencent.assistantv2.activity.cj r1 = (com.tencent.assistantv2.activity.cj) r1
            boolean r2 = r7.a(r0, r1)
            if (r2 == 0) goto L_0x00e6
            int r2 = r0.b
            com.tencent.assistantv2.manager.RankTabType r4 = com.tencent.assistantv2.manager.RankTabType.APPLIST
            int r4 = r4.ordinal()
            if (r2 != r4) goto L_0x010e
            com.tencent.assistantv2.adapter.RankNormalListAdapter r2 = r1.b
            if (r2 == 0) goto L_0x00df
            java.util.HashMap<java.lang.Integer, java.lang.Boolean> r2 = r7.W
            java.lang.Integer r4 = java.lang.Integer.valueOf(r8)
            boolean r2 = r2.containsKey(r4)
            if (r2 == 0) goto L_0x00be
            if (r3 == 0) goto L_0x0108
        L_0x00be:
            java.util.HashMap<java.lang.Integer, java.lang.Boolean> r2 = r7.W
            java.lang.Integer r3 = java.lang.Integer.valueOf(r8)
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r6)
            r2.put(r3, r4)
            int r0 = r7.a(r0)
            com.tencent.assistantv2.st.business.CostTimeSTManager$TIMETYPE r2 = com.tencent.assistantv2.st.business.CostTimeSTManager.TIMETYPE.START
            long r3 = java.lang.System.currentTimeMillis()
            com.tencent.assistantv2.st.k.a(r0, r2, r3)
            android.view.View r0 = r1.f2806a
            com.tencent.assistantv2.component.RankNormalListPage r0 = (com.tencent.assistantv2.component.RankNormalListPage) r0
            r0.loadFirstPage()
        L_0x00df:
            com.tencent.assistantv2.component.TXSecondViewPager r0 = r7.Y
            r0.setCurrentItem(r8)
            r7.ab = r8
        L_0x00e6:
            java.util.List<android.view.View> r0 = r7.P
            int r1 = r7.ab
            java.lang.Object r0 = r0.get(r1)
            boolean r0 = r0 instanceof com.tencent.assistantv2.component.RankNormalListPage
            if (r0 == 0) goto L_0x0157
            java.util.List<android.view.View> r0 = r7.P
            int r1 = r7.ab
            java.lang.Object r0 = r0.get(r1)
            com.tencent.assistantv2.component.RankNormalListPage r0 = (com.tencent.assistantv2.component.RankNormalListPage) r0
            com.tencent.assistantv2.component.RankNormalListPage r0 = (com.tencent.assistantv2.component.RankNormalListPage) r0
            r0.onResume()
        L_0x0101:
            if (r9 == 0) goto L_0x004b
            r7.M()
            goto L_0x004b
        L_0x0108:
            com.tencent.assistantv2.adapter.RankNormalListAdapter r0 = r1.b
            r0.notifyDataSetChanged()
            goto L_0x00df
        L_0x010e:
            int r2 = r0.b
            com.tencent.assistantv2.manager.RankTabType r4 = com.tencent.assistantv2.manager.RankTabType.LISTGROUP
            int r4 = r4.ordinal()
            if (r2 != r4) goto L_0x00df
            com.tencent.assistantv2.adapter.u r2 = r1.c
            if (r2 == 0) goto L_0x00df
            java.util.HashMap<java.lang.Integer, java.lang.Boolean> r2 = r7.W
            java.lang.Integer r4 = java.lang.Integer.valueOf(r8)
            boolean r2 = r2.containsKey(r4)
            if (r2 == 0) goto L_0x012a
            if (r3 == 0) goto L_0x0151
        L_0x012a:
            java.util.HashMap<java.lang.Integer, java.lang.Boolean> r2 = r7.W
            java.lang.Integer r3 = java.lang.Integer.valueOf(r8)
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r6)
            r2.put(r3, r4)
            int r2 = r7.a(r0)
            com.tencent.assistantv2.st.business.CostTimeSTManager$TIMETYPE r3 = com.tencent.assistantv2.st.business.CostTimeSTManager.TIMETYPE.START
            long r4 = java.lang.System.currentTimeMillis()
            com.tencent.assistantv2.st.k.a(r2, r3, r4)
            android.view.View r1 = r1.f2806a
            com.tencent.assistantv2.component.RankCustomizeListPage r1 = (com.tencent.assistantv2.component.RankCustomizeListPage) r1
            int r2 = r0.f
            long r3 = r0.e
            int r0 = (int) r3
            r1.a(r2, r0)
            goto L_0x00df
        L_0x0151:
            com.tencent.assistantv2.adapter.u r0 = r1.c
            r0.notifyDataSetChanged()
            goto L_0x00df
        L_0x0157:
            java.util.List<android.view.View> r0 = r7.P
            int r1 = r7.ab
            java.lang.Object r0 = r0.get(r1)
            boolean r0 = r0 instanceof com.tencent.assistantv2.component.RankCustomizeListPage
            if (r0 == 0) goto L_0x0101
            java.util.List<android.view.View> r0 = r7.P
            int r1 = r7.ab
            java.lang.Object r0 = r0.get(r1)
            com.tencent.assistantv2.component.RankCustomizeListPage r0 = (com.tencent.assistantv2.component.RankCustomizeListPage) r0
            com.tencent.assistantv2.component.RankCustomizeListPage r0 = (com.tencent.assistantv2.component.RankCustomizeListPage) r0
            r0.e()
            goto L_0x0101
        L_0x0173:
            r3 = r2
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistantv2.activity.cc.a(int, boolean):void");
    }

    private int a(al alVar) {
        if (alVar != null) {
            if (alVar.b == RankTabType.APPLIST.ordinal()) {
                if (alVar.e == 0) {
                    if (alVar.f == 6) {
                        return STConst.ST_PAGE_RANK_HOT;
                    }
                    if (alVar.f == 5) {
                        return STConst.ST_PAGE_RANK_CLASSIC;
                    }
                }
                if (alVar.f == 99) {
                    return STConst.ST_PAGE_RANK_FRIENDS;
                }
            } else if (alVar.b == RankTabType.LISTGROUP.ordinal() && alVar.f == 2) {
                return STConst.ST_PAGE_RANK_RECOMMEND;
            } else {
                return -1;
            }
        }
        return -1;
    }

    private boolean a(al alVar, cj cjVar) {
        if (alVar == null) {
            return true;
        }
        if (cjVar != null) {
            if (cjVar.f2806a == null) {
                return false;
            }
            if (alVar.b == RankTabType.APPLIST.ordinal() && cjVar.b == null) {
                return false;
            }
            if (alVar.b == RankTabType.LISTGROUP.ordinal() && cjVar.c == null) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.adapter.u.a(int, long):void
     arg types: [?, int]
     candidates:
      com.tencent.assistantv2.adapter.u.a(int, int):int
      com.tencent.assistantv2.adapter.u.a(java.util.Map<com.tencent.assistant.model.AppGroupInfo, java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>>, long):void
      com.tencent.assistantv2.adapter.u.a(int, long):void */
    private void b(al alVar) {
        RankNormalListPage rankNormalListPage;
        RankNormalListAdapter rankNormalListAdapter;
        if (alVar == null) {
            return;
        }
        if (alVar.b == RankTabType.APPLIST.ordinal()) {
            k kVar = new k(alVar.e, alVar.f, (short) alVar.g);
            if (c(alVar)) {
                rankNormalListPage = new RankFriendsListPage(this.Q, TXScrollViewBase.ScrollMode.PULL_FROM_END, kVar);
            } else {
                rankNormalListPage = new RankNormalListPage(this.Q, TXScrollViewBase.ScrollMode.PULL_FROM_END, kVar);
            }
            rankNormalListPage.setBaseFragment(this);
            rankNormalListPage.onResume();
            this.P.add(rankNormalListPage);
            if (c(alVar)) {
                rankNormalListAdapter = new RankFriendsListAdapter(this.Q, rankNormalListPage, kVar.a());
            } else {
                rankNormalListAdapter = new RankNormalListAdapter(this.Q, rankNormalListPage, kVar.a());
            }
            rankNormalListAdapter.a(a(alVar), -100);
            rankNormalListAdapter.a(RankNormalListAdapter.ListType.LISTTYPEGAMESORT);
            rankNormalListAdapter.b(ListItemInfoView.InfoType.DOWNTIMES_SIZE.ordinal());
            rankNormalListAdapter.a(a(alVar.h));
            rankNormalListPage.setAdapter(rankNormalListAdapter, this.ai);
            rankNormalListAdapter.c();
            this.S.add(rankNormalListAdapter);
            this.V.put(alVar.f2950a, new cj(this, rankNormalListPage, rankNormalListAdapter, null));
        } else if (alVar.b == RankTabType.LISTGROUP.ordinal()) {
            RankCustomizeListPage rankCustomizeListPage = new RankCustomizeListPage(this.Q, new de(a(alVar.f, alVar.e)), alVar.f, (int) alVar.e);
            rankCustomizeListPage.a(this);
            rankCustomizeListPage.e();
            this.P.add(rankCustomizeListPage);
            u uVar = new u(this.Q, rankCustomizeListPage, STConst.ST_PAGE_COMPETITIVE);
            uVar.a((int) STConst.ST_PAGE_RANK_RECOMMEND, -100L);
            rankCustomizeListPage.a(uVar, this.ai);
            uVar.a();
            this.S.add(uVar);
            this.V.put(alVar.f2950a, new cj(this, rankCustomizeListPage, null, uVar));
        }
    }

    private boolean a(int i, long j) {
        return b(i, j);
    }

    private boolean b(int i, long j) {
        return i == 2 && j == 0;
    }

    private boolean a(byte b) {
        return (b & 1) == 1;
    }

    private void A() {
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_HOTTAB_DOWNLOAD_FILTER_SHOW:
                A();
                return;
            default:
                return;
        }
    }

    private boolean c(al alVar) {
        if (alVar == null || alVar.f != 99) {
            return false;
        }
        return true;
    }

    public void I() {
        M();
    }
}
