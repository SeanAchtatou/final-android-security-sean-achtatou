package com.tencent.assistantv2.activity;

import android.view.View;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class dw implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShowSameTagAppsActivity f2846a;

    dw(ShowSameTagAppsActivity showSameTagAppsActivity) {
        this.f2846a = showSameTagAppsActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.ShowSameTagAppsActivity.a(com.tencent.assistantv2.activity.ShowSameTagAppsActivity, boolean, boolean):void
     arg types: [com.tencent.assistantv2.activity.ShowSameTagAppsActivity, int, int]
     candidates:
      com.tencent.assistantv2.activity.ShowSameTagAppsActivity.a(int, int, com.tencent.assistant.protocol.jce.GetAppFromTagResponse):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistantv2.model.a.b.a(int, int, com.tencent.assistant.protocol.jce.GetAppFromTagResponse):void
      com.tencent.assistantv2.activity.ShowSameTagAppsActivity.a(com.tencent.assistantv2.activity.ShowSameTagAppsActivity, boolean, boolean):void */
    public void onClick(View view) {
        if (this.f2846a.v == null || this.f2846a.w == null) {
            this.f2846a.n.a(this.f2846a.t, this.f2846a.u, 0, Constants.STR_EMPTY, this.f2846a.F, this.f2846a.F + ShowSameTagAppsActivity.E);
        } else {
            this.f2846a.n.a(this.f2846a.t, this.f2846a.u, (long) Integer.parseInt(this.f2846a.v), this.f2846a.w, this.f2846a.F, this.f2846a.F + ShowSameTagAppsActivity.E);
        }
        this.f2846a.a(false, false);
    }
}
