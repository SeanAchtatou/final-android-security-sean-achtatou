package com.tencent.assistantv2.activity;

import android.view.animation.Transformation;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.component.k;

/* compiled from: ProGuard */
class ak implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2758a;

    ak(AssistantTabActivity assistantTabActivity) {
        this.f2758a = assistantTabActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.AssistantTabActivity.e(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):void
     arg types: [com.tencent.assistantv2.activity.AssistantTabActivity, int]
     candidates:
      com.tencent.assistantv2.activity.AssistantTabActivity.e(com.tencent.assistantv2.activity.AssistantTabActivity, int):int
      com.tencent.assistantv2.activity.AssistantTabActivity.e(com.tencent.assistantv2.activity.AssistantTabActivity, boolean):void */
    public void a(float f, float f2, float f3, float f4) {
        if (f3 >= 0.0f && this.f2758a.P.getCurrentView().getId() == R.id.layout_tips_result && f4 > f3) {
            this.f2758a.d(false);
        }
    }

    public void b(float f, float f2, float f3, float f4) {
        boolean z = true;
        if (this.f2758a.aN.b() == 0.0f && this.f2758a.v.getScrollY() <= 0) {
            this.f2758a.v.smoothScrollTo(0, 0);
        }
        if (this.f2758a.aN.b() != 0.0f) {
            this.f2758a.v.smoothScrollTo(0, 0);
        }
        if (this.f2758a.aN.b() == 0.0f && this.f2758a.P.getCurrentView().getId() == R.id.tv_tips && f3 > f4) {
            this.f2758a.bg.postDelayed(new al(this), 10);
        }
        if (this.f2758a.aN.b() == 1.0f) {
            this.f2758a.v.b(0.0f);
            boolean z2 = (!this.f2758a.aG) & (!this.f2758a.aZ);
            if (this.f2758a.be) {
                z = false;
            }
            if (z2 && z) {
                this.f2758a.bg.postDelayed(new am(this), 1000);
            }
        }
        if (this.f2758a.aN.b() == this.f2758a.aP) {
            this.f2758a.j();
            this.f2758a.v.b(this.f2758a.aQ);
        }
        if (this.f2758a.aZ) {
            boolean unused = this.f2758a.aZ = false;
            this.f2758a.bg.postDelayed(new an(this), 200);
        }
    }

    public void a(float f, Transformation transformation) {
        this.f2758a.v.smoothScrollTo(0, 0);
    }
}
