package com.tencent.assistantv2.b;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ab {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f2940a;
    private int b;
    /* access modifiers changed from: private */
    public int c;
    private long d;
    /* access modifiers changed from: private */
    public boolean e;
    private ad f;
    private ad g;
    private ArrayList<SimpleAppModel> h;
    private ArrayList<SmartCardWrapper> i;

    private ab(r rVar) {
        this.f2940a = rVar;
        this.b = 0;
        this.c = -1;
        this.d = -1;
        this.e = false;
        this.h = null;
        this.i = null;
    }

    /* synthetic */ ab(r rVar, s sVar) {
        this(rVar);
    }

    public void a(ad adVar) {
        if (adVar != null && adVar.f2942a != null && adVar.f2942a.length != 0) {
            a();
            this.b = 1;
            this.f = adVar;
            this.c = this.f2940a.getUniqueId();
            TemporaryThreadManager.get().start(new ac(this, adVar));
        }
    }

    public void a() {
        this.b = 0;
        this.c = -1;
        this.f = null;
        this.g = null;
        this.e = false;
        this.h = null;
    }

    public int b() {
        return this.c;
    }

    public long c() {
        return this.d;
    }

    public boolean d() {
        return 1 == this.b;
    }

    public ad e() {
        return this.f;
    }

    public ad f() {
        return this.g;
    }

    public ArrayList<SimpleAppModel> g() {
        return this.h;
    }

    public ArrayList<SmartCardWrapper> h() {
        return this.i;
    }

    public void a(long j, ArrayList<SimpleAppModel> arrayList, ArrayList<SmartCardWrapper> arrayList2, boolean z, ad adVar) {
        this.d = j;
        this.h = arrayList;
        this.e = z;
        this.i = arrayList2;
        this.g = adVar;
        this.b = 2;
    }

    public void i() {
        this.b = 2;
    }
}
