package com.tencent.assistantv2.b;

import com.tencent.assistant.activity.GameRankTabType;
import com.tencent.assistant.protocol.jce.GftGetNavigationResponse;
import com.tencent.assistant.protocol.jce.SubNavigationNode;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class h {

    /* renamed from: a  reason: collision with root package name */
    public long f2959a;
    public List<i> b = new ArrayList(4);
    public int c;

    private h() {
        this.b.add(new i("新游", GameRankTabType.NEWGAME.ordinal(), 0, null, -2, 11, 20, (byte) 0));
        this.b.add(new i("单机", GameRankTabType.ONEPC.ordinal(), 0, null, -2, 8, 20, (byte) 0));
        this.b.add(new i("网游", GameRankTabType.NETGAME.ordinal(), 0, null, -2, 9, 20, (byte) 0));
        this.b.add(new i("人气", GameRankTabType.RENQI.ordinal(), 0, null, -2, 10, 20, (byte) 0));
        this.c = 2;
    }

    public static h a(GftGetNavigationResponse gftGetNavigationResponse) {
        h hVar = null;
        try {
            hVar = b(gftGetNavigationResponse);
        } catch (Exception e) {
            XLog.e("GameGetNavigationEngine", "parse tab container fail,type:.ex:" + e);
            e.printStackTrace();
        }
        if (hVar == null) {
            return new h();
        }
        return hVar;
    }

    public static h b(GftGetNavigationResponse gftGetNavigationResponse) {
        if (gftGetNavigationResponse == null || gftGetNavigationResponse.b() == null || gftGetNavigationResponse.b().size() <= 0) {
            return null;
        }
        h hVar = new h();
        hVar.c = 2;
        hVar.f2959a = gftGetNavigationResponse.a();
        ArrayList<SubNavigationNode> b2 = gftGetNavigationResponse.b();
        ArrayList arrayList = new ArrayList(gftGetNavigationResponse.b().size());
        Iterator<SubNavigationNode> it = b2.iterator();
        while (it.hasNext()) {
            SubNavigationNode next = it.next();
            arrayList.add(new i(next.f2383a, next.b, next.c, next.d, next.e, next.f, next.g, next.h));
        }
        hVar.b = arrayList;
        return hVar;
    }
}
