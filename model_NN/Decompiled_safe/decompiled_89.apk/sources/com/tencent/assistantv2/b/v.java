package com.tencent.assistantv2.b;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.model.a.g;
import java.util.ArrayList;

/* compiled from: ProGuard */
class v implements CallbackHelper.Caller<g> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f2972a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ ArrayList d;
    final /* synthetic */ r e;

    v(r rVar, ad adVar, int i, boolean z, ArrayList arrayList) {
        this.e = rVar;
        this.f2972a = adVar;
        this.b = i;
        this.c = z;
        this.d = arrayList;
    }

    /* renamed from: a */
    public void call(g gVar) {
        XLog.d("GetHomeEngine", "to caller has next:" + this.f2972a.c + ",pagerContext:" + this.f2972a);
        gVar.a(this.b, 0, this.f2972a.c, this.f2972a, this.c, this.e.c, this.e.e, this.e.f, this.d, this.e.g);
    }
}
