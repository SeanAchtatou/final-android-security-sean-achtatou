package com.tencent.assistantv2.model;

import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistantv2.st.model.StatInfo;
import java.io.File;

/* compiled from: ProGuard */
public abstract class AbstractDownloadInfo {

    /* renamed from: a  reason: collision with root package name */
    public String f3307a;
    public String b;
    public String c;
    public long d;
    public long e;
    public long f;
    public String g;
    public String h;
    public DownState i = DownState.INIT;
    public int j = 0;
    public a k = new a();
    public StatInfo l = new StatInfo();

    /* compiled from: ProGuard */
    public enum DownState {
        INIT,
        DOWNLOADING,
        QUEUING,
        SUCC,
        FAIL,
        PAUSED,
        DELETE,
        WAITTING_FOR_WIFI
    }

    public abstract String a();

    public int b() {
        int i2;
        if (this.k.f3314a > 0 && (i2 = (int) ((((double) this.k.b) * 100.0d) / ((double) this.k.f3314a))) != 0) {
            return i2;
        }
        return 1;
    }

    public boolean c() {
        String str = a() + File.separator + this.f3307a;
        if (!FileUtil.isFileExists(str)) {
            return false;
        }
        this.h = str;
        this.f = System.currentTimeMillis();
        this.i = DownState.SUCC;
        this.j = 0;
        return true;
    }

    public void d() {
        if (this.i == DownState.DOWNLOADING || this.i == DownState.QUEUING) {
            this.i = DownState.PAUSED;
        }
    }

    public boolean e() {
        if (this.i == DownState.SUCC) {
            if (new File(this.h).exists()) {
                return true;
            }
            if (this.k != null) {
                this.k.b = 0;
            }
        }
        return false;
    }
}
