package com.tencent.assistantv2.model;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public class SimpleVideoModel implements Parcelable {
    public static final Parcelable.Creator<SimpleVideoModel> CREATOR = new g();

    /* renamed from: a  reason: collision with root package name */
    public String f3312a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public CARD_TYPE j;
    public String k;
    public String l;

    /* compiled from: ProGuard */
    public enum CARD_TYPE {
        NORMAL,
        RICH
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f3312a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeString(this.k);
        parcel.writeString(this.l);
    }
}
