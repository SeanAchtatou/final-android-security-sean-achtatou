package com.tencent.assistantv2.mediadownload;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.s;
import com.tencent.assistant.module.ds;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;

/* compiled from: ProGuard */
public class a implements s {
    private static a b;

    /* renamed from: a  reason: collision with root package name */
    private ds f3283a = new ds();
    private String c;

    private a() {
        this.f3283a.register(this);
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (b == null) {
                b = new a();
            }
            aVar = b;
        }
        return aVar;
    }

    public WantInstallAppStatus a(String str, int i, boolean z) {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(str);
        if (z) {
            SimpleAppModel simpleAppModel = new SimpleAppModel();
            simpleAppModel.c = str;
            if (localApkInfo == null) {
                if (!TextUtils.isEmpty(simpleAppModel.c)) {
                    localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c);
                } else if (simpleAppModel.f1634a > 0) {
                    localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.f1634a);
                }
            }
            if (localApkInfo != null && !TextUtils.isEmpty(localApkInfo.manifestMd5)) {
                simpleAppModel.ak = localApkInfo.manifestMd5;
                simpleAppModel.D = localApkInfo.mVersionCode;
            }
            this.c = str;
            this.f3283a.a(simpleAppModel);
            return WantInstallAppStatus.DOWNLOADING;
        } else if (localApkInfo == null) {
            return WantInstallAppStatus.NOT_INSTALL;
        } else {
            return WantInstallAppStatus.NOT_MATCH_VERSION;
        }
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        SimpleAppModel a2 = u.a(appSimpleDetail);
        DownloadInfo a3 = DownloadProxy.a().a(a2);
        if (a3 != null && a3.needReCreateInfo(a2)) {
            DownloadProxy.a().b(a3.downloadTicket);
            a3 = null;
        }
        if (a3 == null) {
            StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(null, a2);
            buildDownloadSTInfo.sourceScene = STConst.ST_PAGE_DOWNLOAD;
            a3 = DownloadInfo.createDownloadInfo(a2, buildDownloadSTInfo);
        }
        a3.autoInstall = false;
        com.tencent.assistant.download.a.a().a(a3, SimpleDownloadInfo.UIType.NORMAL);
    }

    public void onGetAppInfoFail(int i, int i2) {
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_HOST_GET_FAIL, this.c));
    }
}
