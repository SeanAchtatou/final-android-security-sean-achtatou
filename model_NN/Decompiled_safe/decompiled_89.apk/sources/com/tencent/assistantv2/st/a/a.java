package com.tencent.assistantv2.st.a;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.f;

/* compiled from: ProGuard */
public class a {
    public static Bundle a(Uri uri, Bundle bundle) {
        if (a(uri)) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            if (!bundle.containsKey(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME)) {
                bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, -1000);
            }
            b(uri);
        }
        return b(uri, bundle);
    }

    private static boolean a(Uri uri) {
        String queryParameter = uri.getQueryParameter(com.tencent.assistant.b.a.D);
        if (!TextUtils.isEmpty(uri.getQueryParameter(com.tencent.assistant.b.a.E)) || TextUtils.isEmpty(queryParameter)) {
            return true;
        }
        return false;
    }

    private static void b(Uri uri) {
        String queryParameter = uri.getQueryParameter(com.tencent.assistant.b.a.n);
        if (!TextUtils.isEmpty(queryParameter)) {
            if (queryParameter.equals("com.tencent.mm")) {
                f.a((byte) 2);
            } else if (queryParameter.equals("com.tencent.mobileqq")) {
                f.a((byte) 3);
            }
        }
        f.b(uri.getQueryParameter(com.tencent.assistant.b.a.j));
        f.a(uri.getQueryParameter(com.tencent.assistant.b.a.y));
    }

    private static Bundle b(Uri uri, Bundle bundle) {
        if (uri == null) {
            return null;
        }
        if (bundle == null) {
            bundle = new Bundle();
        }
        String queryParameter = uri.getQueryParameter(com.tencent.assistant.b.a.j);
        if (!TextUtils.isEmpty(queryParameter)) {
            bundle.putString(com.tencent.assistant.b.a.j, queryParameter);
        }
        String queryParameter2 = uri.getQueryParameter(com.tencent.assistant.b.a.y);
        if (!TextUtils.isEmpty(queryParameter2)) {
            bundle.putString(com.tencent.assistant.b.a.y, queryParameter2);
        }
        String queryParameter3 = uri.getQueryParameter(com.tencent.assistant.b.a.n);
        if (!TextUtils.isEmpty(queryParameter3)) {
            bundle.putString(com.tencent.assistant.b.a.n, queryParameter3);
        }
        String queryParameter4 = uri.getQueryParameter(com.tencent.assistant.b.a.o);
        if (!TextUtils.isEmpty(queryParameter4)) {
            bundle.putString(com.tencent.assistant.b.a.o, queryParameter4);
        }
        String queryParameter5 = uri.getQueryParameter(com.tencent.assistant.b.a.ac);
        if (!TextUtils.isEmpty(queryParameter5)) {
            bundle.putString(com.tencent.assistant.b.a.ac, queryParameter5);
        }
        String queryParameter6 = uri.getQueryParameter(com.tencent.assistant.b.a.ad);
        if (TextUtils.isEmpty(queryParameter6)) {
            return bundle;
        }
        bundle.putString(com.tencent.assistant.b.a.ad, queryParameter6);
        return bundle;
    }
}
