package com.tencent.assistantv2.st.business;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.login.d;
import com.tencent.assistant.utils.bb;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.eup.b;

/* compiled from: ProGuard */
final class l implements b {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3337a = false;

    l() {
    }

    public byte[] a(boolean z, String str, String str2, String str3, int i, long j) {
        return null;
    }

    public String b(boolean z, String str, String str2, String str3, int i, long j) {
        int i2;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("deviceInfo:").append(t.s()).append("\n");
        stringBuffer.append("qua:").append(Global.getQUA()).append("\n");
        String str4 = Constants.STR_EMPTY;
        if (AstApp.m() != null) {
            str4 = AstApp.m().getClass().getSimpleName();
        }
        String str5 = str4 + "," + AstApp.o();
        stringBuffer.append("versionName:").append(Global.getAppVersionName4Crash() + "\n");
        AppConst.IdentityType d = d.a().d();
        StringBuffer stringBuffer2 = new StringBuffer();
        if (d == AppConst.IdentityType.WX) {
            stringBuffer2.append("wx:");
            stringBuffer2.append(d.a().s());
        } else if (d == AppConst.IdentityType.MOBILEQ) {
            stringBuffer2.append("qq:");
            stringBuffer2.append(d.a().p());
        } else {
            stringBuffer2.append(Global.getChannelId());
        }
        stringBuffer2.append("--");
        if (str5 == null) {
            str5 = Constants.STR_EMPTY;
        }
        stringBuffer2.append(str5);
        try {
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            if (contextClassLoader != null) {
                stringBuffer2.append("--");
                stringBuffer2.append(contextClassLoader.toString());
            }
        } catch (Exception | SecurityException e) {
        }
        stringBuffer2.append("--").append(k.g());
        stringBuffer.append("contact:").append(stringBuffer2.toString() + "\n");
        stringBuffer.append("uuid:").append(Global.getUUID4Crash() + "\n");
        StringBuffer append = stringBuffer.append("appliationCreateTime:");
        StringBuilder sb = new StringBuilder();
        if (AstApp.d > 0) {
            i2 = cv.a(Long.valueOf(AstApp.d));
        } else {
            i2 = 0;
        }
        append.append(sb.append(i2).append("\n").toString());
        stringBuffer.append("crashTime:").append(cv.a() + "\n");
        stringBuffer.append("vmMemory:").append("maxMemory=" + bt.a(Runtime.getRuntime().maxMemory()) + ", totalMemory=" + bt.a(Runtime.getRuntime().totalMemory()) + ", freeMemory=" + bt.a(Runtime.getRuntime().freeMemory()) + "\n");
        return stringBuffer.toString();
    }

    public void a(boolean z) {
    }

    public boolean b(boolean z) {
        try {
            if (!this.f3337a) {
                return true;
            }
            new bb();
            bb.a();
            return true;
        } catch (Throwable th) {
            return true;
        }
    }

    public boolean a(boolean z, String str, String str2, String str3, int i, long j, String str4, String str5, String str6) {
        try {
            this.f3337a = false;
            return true;
        } catch (Throwable th) {
            this.f3337a = false;
            return true;
        }
    }
}
