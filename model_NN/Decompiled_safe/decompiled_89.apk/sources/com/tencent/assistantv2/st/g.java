package com.tencent.assistantv2.st;

import com.tencent.assistant.st.STListener;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STListener f3353a;
    final /* synthetic */ b b;

    g(b bVar, STListener sTListener) {
        this.b = bVar;
        this.f3353a = sTListener;
    }

    public void run() {
        com.tencent.assistant.st.g a2 = this.b.d.a(this.f3353a.getSTType());
        if (a2 != null && a2.b != null) {
            this.b.b(a2.b.f2374a, a2.b.c);
        }
    }
}
