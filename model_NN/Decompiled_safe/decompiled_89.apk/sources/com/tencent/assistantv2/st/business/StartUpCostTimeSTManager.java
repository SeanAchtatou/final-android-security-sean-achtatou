package com.tencent.assistantv2.st.business;

import android.os.Handler;
import com.tencent.assistant.Global;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.ba;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class StartUpCostTimeSTManager extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private static StartUpCostTimeSTManager f3326a = null;
    private static Handler d;
    /* access modifiers changed from: private */
    public w c = new w(this);

    /* compiled from: ProGuard */
    public enum TIMETYPE {
        START,
        END,
        CANCEL,
        B5_TIME,
        B6_TIME,
        B7_TIME,
        B8_TIME,
        B9_TIME,
        B10_TIME,
        B11_TIME,
        B12_TIME,
        B13_TIME,
        B14_TIME,
        B15_TIME
    }

    public static synchronized StartUpCostTimeSTManager a() {
        StartUpCostTimeSTManager startUpCostTimeSTManager;
        synchronized (StartUpCostTimeSTManager.class) {
            if (f3326a == null) {
                f3326a = new StartUpCostTimeSTManager();
            }
            startUpCostTimeSTManager = f3326a;
        }
        return startUpCostTimeSTManager;
    }

    private StartUpCostTimeSTManager() {
        d = ba.a("TimeCostLogHandler");
    }

    public byte getSTType() {
        return 24;
    }

    public void flush() {
    }

    public void a(TIMETYPE timetype, long j, int i) {
        if (!this.c.e && timetype != TIMETYPE.START) {
            return;
        }
        if (timetype == TIMETYPE.CANCEL) {
            this.c.e = false;
        } else if (timetype == TIMETYPE.START) {
            this.c.e = true;
            this.c.f3345a = j;
            this.c.c = c.h().f1925a.getIntValue();
        } else if (timetype == TIMETYPE.END) {
            this.c.b = j - this.c.f3345a;
            v vVar = this.c.f.get("B10");
            if (vVar == null) {
                vVar = this.c.f.get("B15");
            }
            if (vVar != null) {
                vVar.c = j - vVar.b;
            }
            this.c.d = i;
            if (this.c.b > 0) {
                d.postDelayed(new u(this), 50);
            }
        } else if (timetype == TIMETYPE.B5_TIME) {
            a(j, Constants.STR_EMPTY, "B5", i);
        } else if (timetype == TIMETYPE.B6_TIME) {
            a(j, "B5", "B6", i);
        } else if (timetype == TIMETYPE.B7_TIME) {
            a(j, "B6", "B7", i);
        } else if (timetype == TIMETYPE.B8_TIME) {
            a(j, "B7", "B8", i);
        } else if (timetype == TIMETYPE.B9_TIME) {
            a(j, "B8", "B9", i);
        } else if (timetype == TIMETYPE.B10_TIME) {
            a(j, "B9", "B10", i);
            a(j, "B15", "B10", i);
        } else if (timetype == TIMETYPE.B11_TIME) {
            a(j, Constants.STR_EMPTY, "B11", i);
        } else if (timetype == TIMETYPE.B12_TIME) {
            a(j, "B11", "B12", i);
        } else if (timetype == TIMETYPE.B13_TIME) {
            a(j, "B12", "B13", i);
        } else if (timetype == TIMETYPE.B14_TIME) {
            a(j, "B13", "B14", i);
        } else if (timetype == TIMETYPE.B15_TIME) {
            a(j, "B14", "B15", i);
        }
    }

    private void a(long j, String str, String str2, int i) {
        v vVar = this.c.f.get(str);
        if (vVar != null) {
            vVar.c = j - vVar.b;
        }
        v vVar2 = new v(this);
        vVar2.f3344a = str2;
        vVar2.b = j;
        this.c.f.put(vVar2.f3344a, vVar2);
    }

    /* access modifiers changed from: private */
    public void a(w wVar) {
        boolean z;
        Map<String, String> b = b(wVar);
        if (wVar != null) {
            z = true;
        } else {
            z = false;
        }
        wVar.e = false;
        a.a(StartUpCostTimeSTManager.class.getSimpleName(), z, 0, 0, b, true);
    }

    private Map<String, String> b(w wVar) {
        if (wVar == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        try {
            hashMap.put("B0", URLEncoder.encode(Global.getQUA(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        hashMap.put("B1", Global.getPhoneGuid());
        hashMap.put("B2", String.valueOf(wVar.c));
        hashMap.put("B3", wVar.f3345a + "," + wVar.b + "," + wVar.d);
        ConcurrentHashMap<String, v> concurrentHashMap = wVar.f;
        if (concurrentHashMap != null && !concurrentHashMap.isEmpty()) {
            for (Map.Entry next : wVar.f.entrySet()) {
                if (next != null) {
                    v vVar = (v) next.getValue();
                    hashMap.put(vVar.f3344a, vVar.b + "," + vVar.c + "," + vVar.d);
                }
            }
        }
        return hashMap;
    }
}
