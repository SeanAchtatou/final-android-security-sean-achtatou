package com.tencent.assistantv2.st;

import com.tencent.assistant.protocol.jce.StatReportItem;
import com.tencent.assistant.st.g;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3352a;

    f(b bVar) {
        this.f3352a = bVar;
    }

    public void run() {
        ArrayList<StatReportItem> a2 = this.f3352a.d.a();
        if (a2 != null && a2.size() > 0) {
            XLog.d("reportSTInstall", "实时系统安装日志的上报");
            int a3 = this.f3352a.e.a(a2);
            if (this.f3352a.c != null) {
                Iterator<StatReportItem> it = a2.iterator();
                while (it.hasNext()) {
                    g gVar = new g();
                    gVar.b = it.next();
                    gVar.c = true;
                    this.f3352a.c.put(Integer.valueOf(a3), gVar);
                }
            }
        }
    }
}
