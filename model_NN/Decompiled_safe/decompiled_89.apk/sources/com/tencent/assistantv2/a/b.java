package com.tencent.assistantv2.a;

import android.content.Context;
import android.view.ViewStub;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.NpcCfg;
import com.tencent.assistant.protocol.jce.NpcListCfg;
import com.tencent.assistant.st.h;
import com.tencent.assistantv2.db.a.c;
import java.util.List;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    a f2739a = null;
    private c b = new c();

    public boolean a(Context context, ViewStub viewStub, int i) {
        switch (i) {
            case 1:
                NpcCfg b2 = b();
                if (b2 != null) {
                    this.f2739a = new c(context, viewStub, b2, this);
                }
                return true;
            default:
                return false;
        }
    }

    public void a() {
        if (this.f2739a != null) {
            this.f2739a.a();
        }
    }

    public NpcCfg b() {
        NpcListCfg q = as.w().q();
        if (q != null) {
            return a(q.f2245a);
        }
        return null;
    }

    public boolean a(NpcCfg npcCfg) {
        if (npcCfg != null && b(npcCfg) && !c(npcCfg)) {
            return true;
        }
        return false;
    }

    public void a(NpcCfg npcCfg, boolean z) {
        if (z) {
            if (npcCfg.i == 1) {
                this.b.a(npcCfg.f2244a, npcCfg.h + 1);
            }
        } else if (npcCfg.h != 0 || npcCfg.i != 1) {
            this.b.a(npcCfg.f2244a, this.b.a(npcCfg.f2244a) + 1);
        }
    }

    private NpcCfg a(List<NpcCfg> list) {
        NpcCfg npcCfg = null;
        if (!(list == null || list.size() == 0)) {
            for (NpcCfg next : list) {
                if (next == null || !b(next) || ((npcCfg != null && npcCfg.f2244a >= next.f2244a) || c(next))) {
                    next = npcCfg;
                }
                npcCfg = next;
            }
        }
        return npcCfg;
    }

    private boolean b(NpcCfg npcCfg) {
        if (npcCfg != null) {
            long a2 = h.a();
            if (npcCfg.c >= a2 || npcCfg.d <= a2) {
                return false;
            }
            return true;
        }
        return false;
    }

    private boolean c(NpcCfg npcCfg) {
        if (npcCfg == null) {
            return true;
        }
        int a2 = this.b.a(npcCfg.f2244a);
        if (npcCfg.h == 0 && npcCfg.i == 1) {
            if (a2 <= npcCfg.h) {
                return false;
            }
            return true;
        } else if (a2 < npcCfg.h) {
            return false;
        } else {
            return true;
        }
    }
}
