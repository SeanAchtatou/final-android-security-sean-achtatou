package com.tencent.assistantv2.manager;

import com.tencent.assistant.m;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.module.update.c;
import com.tencent.assistant.protocol.jce.DataUpdateInfo;
import com.tencent.assistant.protocol.jce.InTimePushCfg;
import com.tencent.assistant.utils.XLog;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static k f3280a;
    private InTimePushCfg b;

    public static synchronized k a() {
        k kVar;
        synchronized (k.class) {
            if (f3280a == null) {
                f3280a = new k();
            }
            kVar = f3280a;
        }
        return kVar;
    }

    public void a(InTimePushCfg inTimePushCfg) {
        XLog.d("IntimePushManager", "setIntimePushCfg");
        this.b = inTimePushCfg;
        if (inTimePushCfg != null && inTimePushCfg.f2218a != null) {
            Iterator<DataUpdateInfo> it = inTimePushCfg.f2218a.iterator();
            while (it.hasNext()) {
                a(it.next());
            }
        }
    }

    public void a(DataUpdateInfo dataUpdateInfo) {
        if (dataUpdateInfo != null) {
            XLog.d("IntimePushManager", "processDataUpdateInfo:" + ((int) dataUpdateInfo.f2050a));
            switch (dataUpdateInfo.f2050a) {
                case 19:
                    b(dataUpdateInfo);
                    return;
                default:
                    return;
            }
        }
    }

    private void b(DataUpdateInfo dataUpdateInfo) {
        long a2 = m.a().a(dataUpdateInfo.f2050a);
        XLog.d("IntimePushManager", "oldVersion, info.version:" + a2 + "," + dataUpdateInfo.b);
        if (a2 < dataUpdateInfo.b) {
            m.a().a(dataUpdateInfo.f2050a, dataUpdateInfo.b);
            c.a().a(AppUpdateConst.RequestLaunchType.TYPE_INTIME_UPDATE_PUSH, (Map<String, String>) null);
        }
    }
}
