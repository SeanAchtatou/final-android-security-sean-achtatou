package com.tencent.assistantv2.adapter.smartlist;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class u extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.AppState f2928a;
    final /* synthetic */ v b;
    final /* synthetic */ s c;

    u(s sVar, AppConst.AppState appState, v vVar) {
        this.c = sVar;
        this.f2928a = appState;
        this.b = vVar;
    }

    public void a(DownloadInfo downloadInfo) {
        if (!(this.f2928a != AppConst.AppState.DOWNLOAD || this.b == null || this.b.q == null)) {
            this.b.q.setVisibility(8);
        }
        a.a().a(downloadInfo);
    }

    public void a(SimpleAppModel simpleAppModel, View view) {
        this.c.a(simpleAppModel, view);
    }
}
