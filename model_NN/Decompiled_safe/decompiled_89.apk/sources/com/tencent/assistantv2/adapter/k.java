package com.tencent.assistantv2.adapter;

import android.widget.ImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class k extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CategoryDetailListAdapter f2895a;

    k(CategoryDetailListAdapter categoryDetailListAdapter) {
        this.f2895a = categoryDetailListAdapter;
    }

    public void a(DownloadInfo downloadInfo) {
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a((ImageView) this.f2895a.h.findViewWithTag(downloadInfo.downloadTicket));
    }
}
