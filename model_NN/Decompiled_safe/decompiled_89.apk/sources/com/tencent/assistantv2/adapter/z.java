package com.tencent.assistantv2.adapter;

import android.widget.ImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class z extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f2937a;

    z(u uVar) {
        this.f2937a = uVar;
    }

    public void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            a.a().a(downloadInfo);
            com.tencent.assistant.utils.a.a((ImageView) this.f2937a.j.findViewWithTag(downloadInfo.downloadTicket));
        }
    }
}
