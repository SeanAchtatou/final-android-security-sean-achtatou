package com.tencent.assistantv2.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;

/* compiled from: ProGuard */
class o {

    /* renamed from: a  reason: collision with root package name */
    View f2899a;
    View b;
    View c;
    ImageView d;
    TextView e;
    TextView f;
    TXAppIconView g;
    TextView h;
    DownloadButton i;
    ListItemInfoView j;
    TextView k;
    ImageView l;
    final /* synthetic */ CategoryDetailListAdapter m;

    private o(CategoryDetailListAdapter categoryDetailListAdapter) {
        this.m = categoryDetailListAdapter;
    }

    /* synthetic */ o(CategoryDetailListAdapter categoryDetailListAdapter, g gVar) {
        this(categoryDetailListAdapter);
    }
}
