package com.tencent.assistantv2.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.l;
import android.support.v4.app.r;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.assistantv2.activity.EBookTabActivity;
import com.tencent.assistantv2.activity.bc;
import com.tencent.assistantv2.activity.bd;
import com.tencent.assistantv2.activity.bo;
import com.tencent.assistantv2.activity.cc;
import com.tencent.assistantv2.activity.dy;
import com.tencent.assistantv2.activity.dz;
import com.tencent.assistantv2.activity.v;
import com.tencent.assistantv2.b.al;
import com.tencent.connect.common.Constants;
import java.lang.ref.SoftReference;
import java.util.List;

/* compiled from: ProGuard */
public class q extends r {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2901a = q.class.getName();
    private SparseArray<SoftReference<Fragment>> b = new SparseArray<>();
    private SparseArray<SoftReference<View>> c = new SparseArray<>();
    private Activity d;
    private List<al> e;
    private List<al> f;
    private String g = Constants.STR_EMPTY;
    private int h = -1;

    public q(l lVar, Activity activity, List<al> list, List<al> list2) {
        super(lVar);
        this.d = activity;
        this.e = list;
        this.f = list2;
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        Log.e("YYB5_0", "-----------instantiateItem:" + i);
        return super.instantiateItem(viewGroup, i);
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        Log.e("YYB5_0", "-----------destroyItem:" + i);
        super.destroyItem(viewGroup, i, obj);
    }

    public int getCount() {
        return this.e.size();
    }

    private Fragment a(al alVar, String str) {
        switch (r.f2902a[alVar.a().ordinal()]) {
            case 1:
                return new v();
            case 2:
                return new bd();
            case 3:
                return new cc();
            case 4:
                return new bo();
            case 5:
                return new bc();
            case 6:
                return new bc();
            case 7:
                return new dy();
            case 8:
            case 9:
                return new dz().a(str);
            default:
                return null;
        }
    }

    public int getItemPosition(Object obj) {
        return super.getItemPosition(obj);
    }

    public Fragment a(int i) {
        Fragment fragment;
        al alVar = this.e.get(i);
        SoftReference softReference = this.b.get(i);
        if (softReference == null || softReference.get() == null) {
            fragment = null;
        } else {
            fragment = (Fragment) softReference.get();
        }
        if (fragment != null) {
            return fragment;
        }
        Fragment a2 = a(alVar, this.e.get(i).d);
        this.b.put(i, new SoftReference(a2));
        return a2;
    }

    public View c(int i) {
        View view;
        SoftReference softReference = this.c.get(i);
        if (softReference == null || softReference.get() == null) {
            view = null;
        } else {
            view = (View) softReference.get();
        }
        if (view == null) {
            this.c.put(i, new SoftReference(view));
        }
        return view;
    }

    public void a(boolean z, int i) {
        if (c(i) instanceof EBookTabActivity) {
            ((EBookTabActivity) c(i)).a(z);
        }
    }
}
