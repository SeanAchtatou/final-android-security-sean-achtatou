package com.tencent.assistantv2.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
class af implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2880a;
    final /* synthetic */ RankNormalListAdapter b;

    af(RankNormalListAdapter rankNormalListAdapter, SimpleAppModel simpleAppModel) {
        this.b = rankNormalListAdapter;
        this.f2880a = simpleAppModel;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.b.f, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", this.f2880a.Z);
        this.b.f.startActivity(intent);
    }
}
