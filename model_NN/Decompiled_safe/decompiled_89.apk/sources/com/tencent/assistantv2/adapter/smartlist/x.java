package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.component.HorizonMultiImageView;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListItemRelateNewsView;
import com.tencent.assistantv2.component.ListRecommendReasonView;

/* compiled from: ProGuard */
public class x extends s {
    public IViewInvalidater e;

    public x(Context context, ab abVar, IViewInvalidater iViewInvalidater) {
        super(context, abVar, iViewInvalidater);
        this.e = iViewInvalidater;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public Pair<View, Object> a() {
        View inflate = LayoutInflater.from(this.f2908a).inflate((int) R.layout.app_universal_item_v5, (ViewGroup) null, false);
        y yVar = new y();
        yVar.j = (TextView) inflate.findViewById(R.id.download_rate_desc);
        yVar.k = (TXAppIconView) inflate.findViewById(R.id.app_icon_img);
        yVar.k.setInvalidater(this.e);
        yVar.l = (TextView) inflate.findViewById(R.id.title);
        yVar.m = (DownloadButton) inflate.findViewById(R.id.state_app_btn);
        yVar.n = (ListItemInfoView) inflate.findViewById(R.id.download_info);
        if (this.b.b() != null) {
            yVar.n.a(this.b.b());
        }
        yVar.q = (HorizonMultiImageView) inflate.findViewById(R.id.snap_shot_pics);
        yVar.r = inflate.findViewById(R.id.empty_padding_bottom);
        yVar.o = (TextView) inflate.findViewById(R.id.desc);
        yVar.p = (ListRecommendReasonView) inflate.findViewById(R.id.reasonView);
        yVar.v = (ListItemRelateNewsView) inflate.findViewById(R.id.relate_news);
        yVar.s = new c();
        yVar.s.f686a = (ListView) inflate.findViewById(R.id.one_more_list);
        yVar.s.b = (RelativeLayout) inflate.findViewById(R.id.one_more_list_parent);
        yVar.s.f686a.setDivider(null);
        yVar.s.e = (ImageView) inflate.findViewById(R.id.one_more_app_line_top_long);
        yVar.s.j = (RelativeLayout) inflate.findViewById(R.id.app_one_more_loading_parent);
        yVar.s.i = (TextView) inflate.findViewById(R.id.app_one_more_loading);
        yVar.s.f = (ProgressBar) inflate.findViewById(R.id.app_one_more_loading_gif);
        yVar.s.g = (ImageView) inflate.findViewById(R.id.one_more_app_error_img);
        yVar.s.d = (ImageView) inflate.findViewById(R.id.one_more_app_arrow);
        yVar.s.h = (TextView) inflate.findViewById(R.id.app_one_more_desc);
        yVar.u = (TextView) inflate.findViewById(R.id.sort_text);
        return Pair.create(inflate, yVar);
    }
}
