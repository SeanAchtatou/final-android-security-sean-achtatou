package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ah extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f3034a;
    final /* synthetic */ FileDownloadButton b;

    ah(FileDownloadButton fileDownloadButton, STInfoV2 sTInfoV2) {
        this.b = fileDownloadButton;
        this.f3034a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.b.b(this.b.b, this.f3034a);
    }

    public STInfoV2 getStInfo() {
        if (this.f3034a != null) {
            this.f3034a.actionId = a.b(this.b.b.i);
            this.f3034a.status = a.d(this.b.b.i);
        }
        return this.f3034a;
    }
}
