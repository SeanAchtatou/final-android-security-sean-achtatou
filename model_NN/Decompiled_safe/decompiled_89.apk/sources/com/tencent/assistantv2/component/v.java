package com.tencent.assistantv2.component;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
class v implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3265a;
    final /* synthetic */ AppConst.AppState b;
    final /* synthetic */ DownloadButton c;

    v(DownloadButton downloadButton, String str, AppConst.AppState appState) {
        this.c = downloadButton;
        this.f3265a = str;
        this.b = appState;
    }

    public void run() {
        String a2 = this.c.d();
        if (a2 != null && a2.equals(this.f3265a)) {
            this.c.b(this.b);
        }
    }
}
