package com.tencent.assistantv2.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class w extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FriendTalkView f3087a;

    w(FriendTalkView friendTalkView) {
        this.f3087a = friendTalkView;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3087a.d, 200);
        buildSTInfo.scene = STConst.ST_PAGE_APP_DETAIL;
        String str = STConst.ST_DEFAULT_SLOT;
        if (view.getTag(R.id.tma_st_slot_tag) instanceof String) {
            str = this.f3087a.c + "_" + ((String) view.getTag(R.id.tma_st_slot_tag));
        }
        buildSTInfo.slotId = str;
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        if (this.f3087a.k != null) {
            this.f3087a.k.a(view);
        }
    }
}
