package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.ApkMgrActivity;
import com.tencent.assistant.activity.BigFileCleanActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class dt implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f3185a;

    dt(TxManagerCommContainView txManagerCommContainView) {
        this.f3185a = txManagerCommContainView;
    }

    public void onClick(View view) {
        String str;
        String str2;
        if (this.f3185a.u != null) {
            String str3 = Constants.STR_EMPTY;
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3185a.u, 200);
            if ((!(this.f3185a.u instanceof SpaceCleanActivity) || this.f3185a.w == null) && (!(this.f3185a.u instanceof BigFileCleanActivity) || this.f3185a.w == null)) {
                this.f3185a.i();
                if (this.f3185a.u instanceof SpaceCleanActivity) {
                    buildSTInfo.scene = STConst.ST_PAGE_RUBBISH_CLEAR_FINISH_STEWARD;
                    str3 = this.f3185a.f3019a.getString(R.string.rubbish_clear_title);
                } else if (this.f3185a.u instanceof ApkMgrActivity) {
                    buildSTInfo.scene = STConst.ST_PAGE_APK_MANAGER_EMPTY;
                    str3 = this.f3185a.f3019a.getString(R.string.apkmgr_title);
                } else if (this.f3185a.u instanceof BigFileCleanActivity) {
                    str3 = this.f3185a.f3019a.getString(R.string.space_clean_big_file_title);
                    buildSTInfo.scene = STConst.ST_PAGE_BIG_FILE_CLEAN_FINISH_STEWARD;
                }
                if (this.f3185a.v.getName().equals(SpaceCleanActivity.class.getName())) {
                    buildSTInfo.slotId = "03_001";
                    str = str3;
                    str2 = this.f3185a.f3019a.getString(R.string.rubbish_clear_title);
                } else {
                    buildSTInfo.slotId = "04_001";
                    str = str3;
                    str2 = this.f3185a.f3019a.getString(R.string.soft_admin);
                }
            } else {
                str = this.f3185a.f3019a.getString(R.string.rubbish_clear_title);
                str2 = this.f3185a.f3019a.getString(R.string.mobile_accelerate_title);
                this.f3185a.j();
                buildSTInfo.scene = STConst.ST_PAGE_RUBBISH_CLEAR_FINISH_STEWARD;
                buildSTInfo.slotId = "03_001";
            }
            k.a(buildSTInfo);
            this.f3185a.a(str, str2);
        }
    }
}
