package com.tencent.assistantv2.component.appdetail;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.tagpage.TagPageActivity;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class t extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTagInfo f3084a;
    final /* synthetic */ long b;
    final /* synthetic */ String c;
    final /* synthetic */ int d;
    final /* synthetic */ DetailTagItemView e;

    t(DetailTagItemView detailTagItemView, AppTagInfo appTagInfo, long j, String str, int i) {
        this.e = detailTagItemView;
        this.f3084a = appTagInfo;
        this.b = j;
        this.c = str;
        this.d = i;
    }

    public void onTMAClick(View view) {
        if (this.f3084a != null && !TextUtils.isEmpty(this.f3084a.f2005a)) {
            Intent intent = new Intent(this.e.f3051a, TagPageActivity.class);
            intent.putExtra("tagID", this.f3084a.f2005a);
            intent.putExtra("tagName", this.f3084a.b);
            intent.putExtra("appID", this.b + Constants.STR_EMPTY);
            intent.putExtra("pkgName", this.c);
            intent.putExtra("tagSubTitle", this.f3084a.e);
            ((Activity) this.e.f3051a).startActivityForResult(intent, 0);
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.e.f3051a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e.f3051a, 200);
        buildSTInfo.slotId = a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, this.d);
        buildSTInfo.appId = this.b;
        buildSTInfo.packageName = this.c;
        buildSTInfo.contentId = a.a(STCommonInfo.ContentIdType.DETAILAPPTAG, this.f3084a.f2005a);
        buildSTInfo.extraData = this.f3084a.b;
        return buildSTInfo;
    }
}
