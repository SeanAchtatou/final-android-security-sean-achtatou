package com.tencent.assistantv2.component;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;
import com.tencent.assistant.event.EventDispatcherEnum;

/* compiled from: ProGuard */
public abstract class dh {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static Toast f3175a;
    private static Handler b = new Handler();
    private static Runnable c = new di();

    private static void b(Context context, CharSequence charSequence, int i) {
        b.removeCallbacks(c);
        switch (i) {
            case 0:
                i = 1000;
                break;
            case 1:
                i = EventDispatcherEnum.CACHE_EVENT_START;
                break;
        }
        if (f3175a != null) {
            f3175a.setText(charSequence);
        } else {
            f3175a = Toast.makeText(context, charSequence, i);
        }
        b.postDelayed(c, (long) i);
        f3175a.show();
    }

    public static void a(Context context, CharSequence charSequence, int i) {
        if (context != null) {
            if (i < 0) {
                i = 0;
            }
            b(context, charSequence, i);
        }
    }

    public static void a(Context context, int i, int i2) {
        if (context != null) {
            if (i2 < 0) {
                i2 = 0;
            }
            b(context, context.getResources().getString(i), i2);
        }
    }
}
