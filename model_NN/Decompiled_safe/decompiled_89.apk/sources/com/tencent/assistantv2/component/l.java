package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.mgr.f;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class l extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BookReadButton f3219a;

    l(BookReadButton bookReadButton) {
        this.f3219a = bookReadButton;
    }

    public void onTMAClick(View view) {
        if (this.f3219a.b != null) {
            f.a().a(this.f3219a.getContext(), this.f3219a.b.c, this.f3219a.b.f3315a, -1, -1, -1, this.f3219a.b.e);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.f3219a.c != null) {
            this.f3219a.c.actionId = 200;
            this.f3219a.c.status = Constants.VIA_REPORT_TYPE_SET_AVATAR;
        }
        return this.f3219a.c;
    }
}
