package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class r implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ColorCardView f3222a;

    r(ColorCardView colorCardView) {
        this.f3222a = colorCardView;
    }

    public void onClick(View view) {
        int i;
        try {
            i = ((Integer) view.getTag(R.id.category_pos)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        this.f3222a.a(i, 200);
        if (this.f3222a.c != null) {
            this.f3222a.c.onClick(view);
        }
    }
}
