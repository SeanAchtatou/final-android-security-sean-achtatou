package com.tencent.assistantv2.component.search;

import android.content.Context;
import com.tencent.assistantv2.component.search.ISearchResultPage;

/* compiled from: ProGuard */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private ISearchResultPage f3241a = null;

    public ISearchResultPage a() {
        return this.f3241a;
    }

    public void a(Context context, j jVar) {
        if (jVar != null) {
            switch (l.f3242a[jVar.a().ordinal()]) {
                case 1:
                    if (this.f3241a == null || this.f3241a.h() != ISearchResultPage.PageType.NATIVE) {
                        this.f3241a = new NativeSearchResultPage(context);
                        this.f3241a.b(jVar.b());
                        this.f3241a.c(jVar.c());
                        return;
                    }
                    return;
                case 2:
                    if (this.f3241a == null || this.f3241a.h() != ISearchResultPage.PageType.WEB) {
                        this.f3241a = new WebSearchResultPage(context);
                        this.f3241a.b(jVar.b());
                        this.f3241a.c(jVar.c());
                        ((WebSearchResultPage) this.f3241a).c(jVar.d());
                        return;
                    }
                    return;
                default:
                    if (this.f3241a == null) {
                        this.f3241a = new NativeSearchResultPage(context);
                        this.f3241a.b(jVar.b());
                        this.f3241a.c(jVar.c());
                        return;
                    }
                    return;
            }
        }
    }
}
