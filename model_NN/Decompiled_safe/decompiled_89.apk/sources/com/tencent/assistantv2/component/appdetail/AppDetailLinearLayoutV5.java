package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.AppdetailScrollView;
import com.tencent.assistant.component.appdetail.AppdetailViewPager;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;

/* compiled from: ProGuard */
public class AppDetailLinearLayoutV5 extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private AppDetailHeaderViewV5 f3043a;
    private AppdetailScrollView b;
    private LinearLayout c;
    private AppdetailViewPager d;
    private SecondNavigationTitleViewV5 e;
    private boolean f = false;
    private int g = -1;
    private b h;
    private c i;

    public AppDetailLinearLayoutV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public AppDetailLinearLayoutV5(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (!this.f) {
            this.f3043a = (AppDetailHeaderViewV5) findViewById(R.id.simple_msg_view);
            this.b = (AppdetailScrollView) findViewById(R.id.parent_scrollview);
            this.d = (AppdetailViewPager) findViewById(R.id.appdetail_viewpager);
            this.c = (LinearLayout) findViewById(R.id.tab_view);
            this.e = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
            if (this.f3043a != null) {
                this.b.setMaxScrollUp(this.f3043a.getHeight());
                ViewGroup.LayoutParams layoutParams = this.d.getLayoutParams();
                layoutParams.height = ((i5 - i3) - this.c.getHeight()) - this.e.getHeight();
                this.g = layoutParams.height;
            }
            if (this.h != null) {
                this.h.a(this.g);
            }
            this.f = !this.f;
        }
    }

    public void a(b bVar) {
        this.h = bVar;
    }

    public void a(c cVar) {
        this.i = cVar;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        this.i.a();
        return super.onInterceptTouchEvent(motionEvent);
    }
}
