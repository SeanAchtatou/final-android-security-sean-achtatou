package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.bp;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.st.STConst;

/* compiled from: ProGuard */
public class GameRankNormalListPage extends RelativeLayout implements NetworkMonitor.ConnectivityChangeListener, ar {

    /* renamed from: a  reason: collision with root package name */
    protected Context f2993a = null;
    protected LayoutInflater b = null;
    protected LoadingView c;
    protected NormalErrorRecommendPage d;
    protected GameRankNormalListView e;
    protected View.OnClickListener f = new am(this);
    private APN g = APN.NO_NETWORK;

    public GameRankNormalListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2993a = context;
        a(context);
    }

    public GameRankNormalListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2993a = context;
        a(context);
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        this.b = LayoutInflater.from(context);
        View inflate = this.b.inflate((int) R.layout.game_ranknormallist_component_view, this);
        this.e = (GameRankNormalListView) inflate.findViewById(R.id.applist);
        this.e.setVisibility(8);
        this.c = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.c.setVisibility(0);
        this.d = (NormalErrorRecommendPage) inflate.findViewById(R.id.error_page);
        this.d.setButtonClickListener(this.f);
        this.d.setIsAutoLoading(true);
        cq.a().a(this);
    }

    public void c(int i) {
        this.c.setVisibility(8);
        this.e.setVisibility(8);
        this.d.setVisibility(0);
        this.d.setErrorType(i);
    }

    public void a() {
        this.c.setVisibility(0);
        this.e.setVisibility(8);
        this.d.setVisibility(8);
    }

    public void b() {
        a();
        this.e.a(d());
    }

    public void G() {
        this.e.setVisibility(0);
        this.d.setVisibility(8);
        this.c.setVisibility(8);
    }

    public void H() {
    }

    public int c() {
        bp d2 = this.e.d();
        if (d2 == null || d2.f1728a != 0) {
            return 2000;
        }
        if (d2.b == 6) {
            return STConst.ST_PAGE_RANK_HOT;
        }
        if (d2.b == 5) {
            return STConst.ST_PAGE_RANK_CLASSIC;
        }
        if (d2.b == 2) {
            return STConst.ST_PAGE_RANK_RECOMMEND;
        }
        return 2000;
    }

    public void onConnected(APN apn) {
        if (this.e.f() == null || this.e.f().getCount() <= 0) {
        }
    }

    public void onDisconnected(APN apn) {
        this.g = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.g = apn2;
    }

    public boolean d() {
        if (this.e != null) {
            return this.e.g();
        }
        return true;
    }
}
