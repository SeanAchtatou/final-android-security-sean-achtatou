package com.tencent.assistantv2.component;

import android.view.View;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cm extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3158a;
    final /* synthetic */ RankSecondNavigationView b;

    cm(RankSecondNavigationView rankSecondNavigationView, int i) {
        this.b = rankSecondNavigationView;
        this.f3158a = i;
    }

    public void onTMAClick(View view) {
        TextView textView = (TextView) view.findViewWithTag(Integer.valueOf((int) R.id.hot_navigation_text));
        if (textView != null) {
            int intValue = ((Integer) textView.getTag(R.id.hot_navigation_pos)).intValue();
            this.b.a(intValue);
            if (this.b.c != null) {
                this.b.c.b(intValue);
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.getContext(), 200);
        buildSTInfo.slotId = a.a("06", this.f3158a);
        return buildSTInfo;
    }
}
