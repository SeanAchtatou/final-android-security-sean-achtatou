package com.tencent.assistantv2.component;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.cy;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.manager.i;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ca extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankCustomizeListView f3135a;

    ca(RankCustomizeListView rankCustomizeListView) {
        this.f3135a = rankCustomizeListView;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        this.f3135a.h++;
        if (this.f3135a.h < 10) {
            int i = this.f3135a.h - 1;
            this.f3135a.g[i] = true;
            if (i > 0) {
                this.f3135a.g[i - 1] = false;
            }
            this.f3135a.c.postDelayed(new cb(this, i), 2000);
            if (this.f3135a.h == 5) {
                this.f3135a.b.setBackgroundResource(R.color.apk_list_bg);
            }
            if (this.f3135a.h > 5) {
                this.f3135a.f.setText(RankCustomizeListView.t[this.f3135a.h % 5]);
                this.f3135a.f.setVisibility(0);
                return;
            }
            SwitchButton switchButton = this.f3135a.c;
            if (this.f3135a.c.getSwitchState()) {
                z = false;
            }
            switchButton.updateSwitchStateWithAnim(z);
            m.a().s(this.f3135a.c.getSwitchState());
            i.a().b().a(this.f3135a.c.getSwitchState());
            if (this.f3135a.c.getSwitchState()) {
                Toast.makeText(this.f3135a.getContext(), (int) R.string.hide_installed_apps_in_list, 4).show();
            } else {
                Toast.makeText(this.f3135a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel, 4).show();
            }
            this.f3135a.m.a();
            if (this.f3135a.m.getGroupCount() > 0) {
                for (int i2 = 0; i2 < this.f3135a.m.getGroupCount(); i2++) {
                    this.f3135a.expandGroup(i2);
                }
                this.f3135a.updateFootViewText();
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.f3135a.j.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, this.f3135a.j.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, 200);
        if (!this.f3135a.i || !(this.f3135a.j instanceof cy)) {
            sTInfoV2.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, 0);
        } else {
            sTInfoV2.slotId = a.a(((cy) this.f3135a.j).K(), 0);
        }
        if (!i.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        sTInfoV2.actionId = 200;
        return sTInfoV2;
    }
}
