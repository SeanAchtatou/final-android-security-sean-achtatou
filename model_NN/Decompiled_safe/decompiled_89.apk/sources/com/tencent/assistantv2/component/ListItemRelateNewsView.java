package com.tencent.assistantv2.component;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.NewsInfo;
import com.tencent.assistant.protocol.jce.RelateNews;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ListItemRelateNewsView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private TextView f2999a;
    private TextView b;
    private LinearLayout c;
    private RelativeLayout.LayoutParams d;
    private final int[] e;
    private final int f;
    private final int g;
    private final int h;
    private final int i;

    public ListItemRelateNewsView(Context context) {
        this(context, null);
    }

    public ListItemRelateNewsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = new int[]{R.drawable.common_icon_relate_news_tag_evaluation, R.drawable.common_icon_relate_news_tag_strategy, R.drawable.common_icon_relate_news_tag_news, R.drawable.common_icon_relate_news_tag_video};
        this.f = 0;
        this.g = 1;
        this.h = 2;
        this.i = 3;
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        LayoutInflater.from(context).inflate((int) R.layout.list_item_relate_news_layout, this);
        this.f2999a = (TextView) findViewById(R.id.news_title);
        this.b = (TextView) findViewById(R.id.news_more);
        this.c = (LinearLayout) findViewById(R.id.news_content);
        this.d = new RelativeLayout.LayoutParams(-1, 1);
    }

    public void a(SimpleAppModel simpleAppModel) {
        this.c.removeAllViews();
        if (simpleAppModel == null) {
            setVisibility(8);
            return;
        }
        RelateNews relateNews = simpleAppModel.aw;
        if (relateNews == null || relateNews.d == null || relateNews.d.isEmpty()) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        if (!TextUtils.isEmpty(relateNews.f2275a)) {
            this.f2999a.setText(Html.fromHtml(relateNews.f2275a));
        }
        this.b.setOnClickListener(new az(this, getContext(), simpleAppModel));
        ArrayList<NewsInfo> arrayList = relateNews.d;
        if (arrayList != null) {
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                NewsInfo newsInfo = arrayList.get(i2);
                if (newsInfo != null) {
                    View inflate = LayoutInflater.from(getContext()).inflate((int) R.layout.list_item_relate_news_item, (ViewGroup) null);
                    ((TextView) inflate.findViewById(R.id.item_title)).setText(newsInfo.b);
                    inflate.findViewById(R.id.item_tag);
                    View findViewById = inflate.findViewById(R.id.cut_line);
                    ((TextView) inflate.findViewById(R.id.item_desc)).setText(newsInfo.c);
                    inflate.setOnClickListener(new ay(this, getContext(), newsInfo));
                    this.c.addView(inflate);
                    if (i2 < arrayList.size() - 1) {
                        findViewById.setVisibility(0);
                    } else {
                        findViewById.setVisibility(8);
                    }
                }
            }
        }
    }
}
