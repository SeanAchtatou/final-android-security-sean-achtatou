package com.tencent.assistantv2.component.categorydetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.TagGroup;

/* compiled from: ProGuard */
class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FloatTagHeader f3144a;

    private f(FloatTagHeader floatTagHeader) {
        this.f3144a = floatTagHeader;
    }

    public void onClick(View view) {
        if (((TagGroup) view.getTag()) != null && !view.equals(this.f3144a.d.get(this.f3144a.i))) {
            this.f3144a.a(((Integer) view.getTag(R.id.category_detail_btn_index)).intValue());
            if (this.f3144a.h != null) {
                this.f3144a.h.onClick(view);
            }
        }
    }
}
