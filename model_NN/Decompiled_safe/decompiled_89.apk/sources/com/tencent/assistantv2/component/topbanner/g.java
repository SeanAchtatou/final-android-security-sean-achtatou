package com.tencent.assistantv2.component.topbanner;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class g extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TopBannerView f3257a;

    g(TopBannerView topBannerView) {
        this.f3257a = topBannerView;
    }

    public void onTMAClick(View view) {
        if (this.f3257a.e != null && this.f3257a.e.i != null) {
            b.a(this.f3257a.getContext(), this.f3257a.e.i);
            b.a().f(this.f3257a.e);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3257a.getContext(), 200);
        buildSTInfo.slotId = this.f3257a.c();
        if (this.f3257a.e != null) {
            switch (this.f3257a.e.f) {
                case 1:
                    buildSTInfo.status = "01";
                    break;
                case 2:
                    buildSTInfo.status = "02";
                    break;
                case 3:
                    buildSTInfo.status = "03";
                    break;
                case 4:
                    buildSTInfo.status = "04";
                    break;
                case 5:
                    buildSTInfo.status = "05";
                    break;
            }
        }
        return buildSTInfo;
    }
}
