package com.tencent.assistantv2.component;

/* compiled from: ProGuard */
class cx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3166a;
    final /* synthetic */ long b;
    final /* synthetic */ ScaningProgressView c;

    cx(ScaningProgressView scaningProgressView, int i, long j) {
        this.c = scaningProgressView;
        this.f3166a = i;
        this.b = j;
    }

    public void run() {
        this.c.e.setColorType(this.f3166a);
        this.c.e.setValueDirect((((double) this.b) * 1.0d) / 1024.0d);
    }
}
