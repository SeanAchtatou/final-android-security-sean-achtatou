package com.tencent.assistantv2.component.appdetail;

import android.view.View;
import com.tencent.assistant.model.c;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class ac implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3065a;
    final /* synthetic */ c b;
    final /* synthetic */ ab c;

    ac(ab abVar, int i, c cVar) {
        this.c = abVar;
        this.f3065a = i;
        this.b = cVar;
    }

    public void run() {
        int i = 0;
        while (true) {
            if (i < this.c.f3064a.m.size()) {
                if (this.c.f3064a.k[i].getTag() != null && this.c.f3064a.k[i].getTag().toString().equals(Constants.STR_EMPTY + this.f3065a)) {
                    this.c.f3064a.k[i].a(this.b.a());
                    this.c.f3064a.o.set(i, this.b.a());
                    this.c.f3064a.l[i].a(this.b.a(), this.c.f3064a.j[i]);
                    this.c.f3064a.k[i].c();
                    break;
                }
                i++;
            } else {
                i = -1;
                break;
            }
        }
        if (i >= 0) {
            this.c.f3064a.a(this.b.a(), (View) null, i);
        }
    }
}
