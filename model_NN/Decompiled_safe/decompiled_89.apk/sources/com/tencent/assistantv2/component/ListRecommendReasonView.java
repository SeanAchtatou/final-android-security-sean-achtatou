package com.tencent.assistantv2.component;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.ListRecommend;
import com.tencent.assistant.protocol.jce.ListRecommendIIT;
import com.tencent.assistant.protocol.jce.ListRecommendTop;
import com.tencent.assistant.protocol.jce.RecommendIT;
import com.tencent.assistant.utils.df;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ListRecommendReasonView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    List<bc> f3000a;
    private SimpleAppModel b;
    private IViewInvalidater c;

    public ListRecommendReasonView(Context context) {
        this(context, null);
    }

    public ListRecommendReasonView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3000a = null;
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.assistantv2.component.ListRecommendReasonView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a() {
        View inflate = LayoutInflater.from(getContext()).inflate((int) R.layout.list_recommend_view, (ViewGroup) this, true);
        this.f3000a = new ArrayList(3);
        this.f3000a.add(new bc((RelativeLayout) inflate.findViewById(R.id.node_1), (TextView) inflate.findViewById(R.id.node_order1), (TXImageView) inflate.findViewById(R.id.node_img1), (TextView) inflate.findViewById(R.id.node_desc1)));
        this.f3000a.add(new bc((RelativeLayout) inflate.findViewById(R.id.node_2), (TextView) inflate.findViewById(R.id.node_order2), (TXImageView) inflate.findViewById(R.id.node_img2), (TextView) inflate.findViewById(R.id.node_desc2)));
        this.f3000a.add(new bc((RelativeLayout) inflate.findViewById(R.id.node_3), (TextView) inflate.findViewById(R.id.node_order3), (TXImageView) inflate.findViewById(R.id.node_img3), (TextView) inflate.findViewById(R.id.node_desc3)));
    }

    public void a(SimpleAppModel simpleAppModel, IViewInvalidater iViewInvalidater) {
        ListRecommend listRecommend;
        boolean z;
        int i;
        this.c = iViewInvalidater;
        this.b = simpleAppModel;
        if (this.b.av != null) {
            listRecommend = this.b.av.b();
        } else {
            listRecommend = null;
        }
        if (listRecommend == null || listRecommend.a() == 0) {
            setVisibility(8);
        } else if (listRecommend.f2228a == 1) {
            ListRecommendIIT b2 = listRecommend.b();
            if (b2 == null || ((b2.a() == null || b2.a().size() == 0) && TextUtils.isEmpty(b2.b()))) {
                setVisibility(8);
                return;
            }
            setVisibility(0);
            ArrayList arrayList = new ArrayList(3);
            if (b2.f2229a != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= b2.f2229a.size()) {
                        break;
                    }
                    arrayList.add(new bb(b2.f2229a.get(i3), 0, 4, 24, 24));
                    i2 = i3 + 1;
                }
            }
            a(listRecommend.b, arrayList);
            a(new bd(b2.b));
        } else if (listRecommend.f2228a == 2) {
            ArrayList<RecommendIT> c2 = listRecommend.c();
            if (c2 == null || c2.size() == 0) {
                setVisibility(8);
                return;
            }
            setVisibility(0);
            ArrayList arrayList2 = new ArrayList(c2.size());
            ArrayList arrayList3 = new ArrayList(c2.size());
            int i4 = 8;
            if (c2.size() > 1) {
                z = true;
            } else {
                z = false;
            }
            int i5 = 0;
            while (i5 < c2.size()) {
                int i6 = c2.get(i5).c;
                if (i5 >= c2.size() - 1) {
                    i = 0;
                } else {
                    i = i4;
                }
                bb bbVar = new bb(i6 < 0 ? null : i6 + Constants.STR_EMPTY, c2.get(i5).f2274a, 0, 4, 24, 24);
                if (z) {
                    bbVar.h = 85;
                }
                arrayList2.add(bbVar);
                arrayList3.add(new bd(c2.get(i5).b, 0, i));
                i5++;
                i4 = i;
            }
            a(listRecommend.b, arrayList2);
            a(arrayList3);
        } else if (listRecommend.f2228a == 3 || listRecommend.f2228a == 4) {
            ListRecommendTop d = listRecommend.d();
            if (d == null) {
                setVisibility(8);
                return;
            }
            setVisibility(0);
            ArrayList arrayList4 = new ArrayList(2);
            arrayList4.add(new bb(d.f2231a, 0, 4, (int) R.drawable.common_cup, 18, 18));
            if (listRecommend.f2228a == 3) {
                arrayList4.add(new bb(R.drawable.common_ranking_big, 4, 2));
            }
            a(listRecommend.b, arrayList4);
            ArrayList arrayList5 = new ArrayList(2);
            arrayList5.add(new bd(d.b, 0, 0));
            int i7 = 0;
            if (listRecommend.f2228a == 4) {
                i7 = 4;
            }
            arrayList5.add(new bd(d.d, i7, 0));
            a(arrayList5);
        }
    }

    private void a(int i, List<bb> list) {
        int i2;
        int i3;
        int a2;
        int i4;
        int i5 = 0;
        if (list != null && list.size() > 0) {
            int size = list.size();
            if (size > this.f3000a.size()) {
                i2 = this.f3000a.size();
            } else {
                i2 = size;
            }
            for (int i6 = 0; i6 < i2; i6++) {
                bc bcVar = this.f3000a.get(i6);
                if (list.get(i6) == null) {
                    a(bcVar);
                } else {
                    TXImageView tXImageView = bcVar.c;
                    bb bbVar = list.get(i6);
                    tXImageView.setInvalidater(this.c);
                    TXImageView.TXImageViewType tXImageViewType = TXImageView.TXImageViewType.NETWORK_IMAGE_ICON;
                    if (i == 1) {
                        tXImageViewType = TXImageView.TXImageViewType.ROUND_IMAGE;
                    }
                    tXImageView.updateImageView(bbVar.b, bbVar.c, tXImageViewType);
                    ViewGroup.LayoutParams layoutParams = tXImageView.getLayoutParams();
                    if (layoutParams instanceof RelativeLayout.LayoutParams) {
                        if (TextUtils.isEmpty(bbVar.f3112a)) {
                            a2 = 0;
                        } else {
                            a2 = df.a(getContext(), 6.0f);
                        }
                        if (bbVar.e > 0) {
                            i4 = df.a(getContext(), (float) bbVar.e);
                        } else {
                            i4 = 0;
                        }
                        ((RelativeLayout.LayoutParams) layoutParams).setMargins(a2, 0, i4, 0);
                    }
                    ViewGroup.LayoutParams layoutParams2 = bcVar.f3113a.getLayoutParams();
                    if (layoutParams2 instanceof LinearLayout.LayoutParams) {
                        if (bbVar.d > 0) {
                            i3 = df.a(getContext(), (float) bbVar.d);
                        } else {
                            i3 = 0;
                        }
                        ((LinearLayout.LayoutParams) layoutParams2).setMargins(i3, 0, 0, 0);
                        int i7 = bbVar.h;
                        if (bbVar.h > 0) {
                            i7 = df.a(getContext(), (float) bbVar.h);
                        }
                        layoutParams2.width = i7;
                    }
                    if (bbVar.f > 0) {
                        layoutParams.height = df.a(getContext(), (float) bbVar.f);
                    } else {
                        layoutParams.height = bbVar.f;
                    }
                    if (bbVar.g > 0) {
                        layoutParams.width = df.a(getContext(), (float) bbVar.g);
                    } else {
                        layoutParams.width = bbVar.g;
                    }
                    tXImageView.setVisibility(0);
                    if (TextUtils.isEmpty(bbVar.f3112a)) {
                        bcVar.b.setVisibility(8);
                    } else {
                        bcVar.b.setVisibility(0);
                        bcVar.b.setText(bbVar.f3112a);
                    }
                    bcVar.f3113a.setVisibility(0);
                }
            }
            i5 = i2;
        }
        if (i5 < this.f3000a.size()) {
            while (i5 < this.f3000a.size()) {
                a(this.f3000a.get(i5));
                i5++;
            }
        }
    }

    private void a(bc bcVar) {
        ViewGroup.LayoutParams layoutParams = bcVar.f3113a.getLayoutParams();
        layoutParams.width = -2;
        if (layoutParams instanceof LinearLayout.LayoutParams) {
            ((LinearLayout.LayoutParams) layoutParams).setMargins(0, 0, 0, 0);
        }
        bcVar.c.setVisibility(8);
        bcVar.b.setVisibility(8);
    }

    private void a(bd bdVar) {
        if (bdVar == null || TextUtils.isEmpty(bdVar.f3114a)) {
            a((List<bd>) null);
            return;
        }
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(null);
        arrayList.add(null);
        arrayList.add(bdVar);
        a(arrayList);
    }

    private void a(List<bd> list) {
        int i;
        if (list == null || list.size() <= 0) {
            i = 0;
        } else {
            int size = list.size();
            if (size > this.f3000a.size()) {
                i = this.f3000a.size();
            } else {
                i = size;
            }
            for (int i2 = 0; i2 < i; i2++) {
                bc bcVar = this.f3000a.get(i2);
                bd bdVar = list.get(i2);
                if (bdVar == null || TextUtils.isEmpty(bdVar.f3114a)) {
                    bcVar.d.setText(Constants.STR_EMPTY);
                    bcVar.d.setPadding(0, 0, 0, 0);
                } else {
                    bcVar.d.setText(b(bdVar));
                    bcVar.d.setPadding(df.a(getContext(), (float) bdVar.b), 0, df.a(getContext(), (float) bdVar.c), 0);
                    bcVar.d.setVisibility(0);
                }
            }
        }
        if (i < this.f3000a.size()) {
            while (i < this.f3000a.size()) {
                bc bcVar2 = this.f3000a.get(i);
                bcVar2.d.setText(Constants.STR_EMPTY);
                bcVar2.d.setPadding(0, 0, 0, 0);
                i++;
            }
        }
    }

    private Spanned b(bd bdVar) {
        if (bdVar == null) {
            return null;
        }
        if (this.b == null || this.b.av == null) {
            return new SpannableString(bdVar.f3114a);
        }
        return this.b.av.a(bdVar.f3114a);
    }
}
