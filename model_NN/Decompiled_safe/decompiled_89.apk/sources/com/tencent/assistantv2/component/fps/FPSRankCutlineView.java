package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/* compiled from: ProGuard */
public class FPSRankCutlineView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3211a = true;

    public FPSRankCutlineView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public FPSRankCutlineView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FPSRankCutlineView(Context context) {
        super(context);
    }

    public void setVisibility(int i) {
        this.f3211a = false;
        super.setVisibility(i);
        this.f3211a = true;
    }

    public void requestLayout() {
        if (this.f3211a) {
            super.requestLayout();
        }
    }
}
