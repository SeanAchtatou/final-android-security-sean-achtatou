package com.tencent.connect.auth;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.connect.auth.AuthMap;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import com.tencent.open.a.n;
import com.tencent.open.b.g;
import com.tencent.open.c.c;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.ServerSetting;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.Util;
import com.tencent.open.web.security.JniInterface;
import com.tencent.open.web.security.SecureJsInterface;
import com.tencent.open.web.security.b;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class AuthDialog extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f3486a = (n.d + ".authDlg");
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public OnTimeListener c;
    private IUiListener d;
    /* access modifiers changed from: private */
    public Handler e;
    private FrameLayout f;
    private LinearLayout g;
    /* access modifiers changed from: private */
    public FrameLayout h;
    private ProgressBar i;
    private String j;
    /* access modifiers changed from: private */
    public c k;
    /* access modifiers changed from: private */
    public Context l;
    /* access modifiers changed from: private */
    public b m;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public String p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public long r = 0;
    /* access modifiers changed from: private */
    public long s = 30000;
    /* access modifiers changed from: private */
    public HashMap<String, Runnable> t;

    static /* synthetic */ String a(AuthDialog authDialog, Object obj) {
        String str = authDialog.b + obj;
        authDialog.b = str;
        return str;
    }

    static /* synthetic */ int m(AuthDialog authDialog) {
        int i2 = authDialog.o;
        authDialog.o = i2 + 1;
        return i2;
    }

    static {
        try {
            Context context = Global.getContext();
            if (context == null) {
                n.b(f3486a, "-->load wbsafeedit lib fail, because context is null.");
            } else if (new File(context.getFilesDir().toString() + "/" + AuthAgent.SECURE_LIB_NAME).exists()) {
                System.load(context.getFilesDir().toString() + "/" + AuthAgent.SECURE_LIB_NAME);
                n.b(f3486a, "-->load wbsafeedit lib success.");
            } else {
                n.b(f3486a, "-->load wbsafeedit lib fail, because so is not exists.");
            }
        } catch (Exception e2) {
            n.b(f3486a, "-->load wbsafeedit lib error.", e2);
        }
    }

    public AuthDialog(Context context, String str, String str2, IUiListener iUiListener, QQToken qQToken) {
        super(context, 16973840);
        this.l = context;
        this.b = str2;
        this.c = new OnTimeListener(str, str2, qQToken.getAppId(), iUiListener);
        this.e = new THandler(this.c, context.getMainLooper());
        this.d = iUiListener;
        this.j = str;
        this.m = new b();
        getWindow().setSoftInputMode(32);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        c();
        e();
        this.t = new HashMap<>();
    }

    public void onBackPressed() {
        if (!this.n) {
            this.c.onCancel();
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* compiled from: ProGuard */
    class THandler extends Handler {
        private OnTimeListener b;

        public THandler(OnTimeListener onTimeListener, Looper looper) {
            super(looper);
            this.b = onTimeListener;
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    this.b.a((String) message.obj);
                    return;
                case 2:
                    this.b.onCancel();
                    return;
                case 3:
                    AuthDialog.b(AuthDialog.this.l, (String) message.obj);
                    return;
                default:
                    return;
            }
        }
    }

    /* compiled from: ProGuard */
    class OnTimeListener implements IUiListener {

        /* renamed from: a  reason: collision with root package name */
        String f3492a;
        String b;
        private String d;
        private IUiListener e;

        public OnTimeListener(String str, String str2, String str3, IUiListener iUiListener) {
            this.d = str;
            this.f3492a = str2;
            this.b = str3;
            this.e = iUiListener;
        }

        /* access modifiers changed from: private */
        public void a(String str) {
            try {
                onComplete(Util.parseJson(str));
            } catch (JSONException e2) {
                e2.printStackTrace();
                onError(new UiError(-4, Constants.MSG_JSON_ERROR, str));
            }
        }

        public void onComplete(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            g.a().a(this.d + "_H5", SystemClock.elapsedRealtime(), 0, 0, jSONObject.optInt("ret", -6), this.f3492a, false);
            if (this.e != null) {
                this.e.onComplete(jSONObject);
                this.e = null;
            }
        }

        public void onError(UiError uiError) {
            String str = uiError.errorMessage != null ? uiError.errorMessage + this.f3492a : this.f3492a;
            g.a().a(this.d + "_H5", SystemClock.elapsedRealtime(), 0, 0, uiError.errorCode, str, false);
            String unused = AuthDialog.this.a(str);
            if (this.e != null) {
                this.e.onError(uiError);
                this.e = null;
            }
        }

        public void onCancel() {
            if (this.e != null) {
                this.e.onCancel();
                this.e = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        StringBuilder sb = new StringBuilder(str);
        if (!TextUtils.isEmpty(this.q) && this.q.length() >= 4) {
            sb.append("_u_").append(this.q.substring(this.q.length() - 4));
        }
        return sb.toString();
    }

    /* compiled from: ProGuard */
    class LoginWebViewClient extends WebViewClient {
        private LoginWebViewClient() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.connect.auth.AuthDialog.a(com.tencent.connect.auth.AuthDialog, java.lang.Object):java.lang.String
         arg types: [com.tencent.connect.auth.AuthDialog, java.lang.String]
         candidates:
          com.tencent.connect.auth.AuthDialog.a(com.tencent.connect.auth.AuthDialog, long):long
          com.tencent.connect.auth.AuthDialog.a(com.tencent.connect.auth.AuthDialog, java.lang.String):java.lang.String
          com.tencent.connect.auth.AuthDialog.a(android.content.Context, java.lang.String):void
          com.tencent.connect.auth.AuthDialog.a(com.tencent.connect.auth.AuthDialog, boolean):boolean
          com.tencent.connect.auth.AuthDialog.a(com.tencent.connect.auth.AuthDialog, java.lang.Object):java.lang.String */
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            n.b(AuthDialog.f3486a, "-->Redirect URL: " + str);
            if (str.startsWith(AuthConstants.REDIRECT_BROWSER_URI)) {
                JSONObject parseUrlToJson = Util.parseUrlToJson(str);
                boolean unused = AuthDialog.this.n = AuthDialog.this.f();
                if (!AuthDialog.this.n) {
                    if (parseUrlToJson.optString("fail_cb", null) != null) {
                        AuthDialog.this.callJs(parseUrlToJson.optString("fail_cb"), Constants.STR_EMPTY);
                    } else if (parseUrlToJson.optInt("fall_to_wv") == 1) {
                        AuthDialog.a(AuthDialog.this, (Object) (AuthDialog.this.b.indexOf("?") > -1 ? "&" : "?"));
                        AuthDialog.a(AuthDialog.this, (Object) "browser_error=1");
                        AuthDialog.this.k.loadUrl(AuthDialog.this.b);
                    } else {
                        String optString = parseUrlToJson.optString("redir", null);
                        if (optString != null) {
                            AuthDialog.this.k.loadUrl(optString);
                        }
                    }
                }
                return true;
            } else if (str.startsWith(ServerSetting.DEFAULT_REDIRECT_URI)) {
                AuthDialog.this.c.onComplete(Util.parseUrlToJson(str));
                AuthDialog.this.dismiss();
                return true;
            } else if (str.startsWith("auth://cancel")) {
                AuthDialog.this.c.onCancel();
                AuthDialog.this.dismiss();
                return true;
            } else if (str.startsWith("auth://close")) {
                AuthDialog.this.dismiss();
                return true;
            } else if (str.startsWith("download://")) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(Uri.decode(str.substring("download://".length()))));
                    intent.addFlags(268435456);
                    AuthDialog.this.l.startActivity(intent);
                } catch (Exception e) {
                    n.b(AuthDialog.f3486a, "-->start download activity exception, e: " + e.getMessage());
                }
                return true;
            } else if (str.startsWith(AuthConstants.PROGRESS_URI)) {
                try {
                    List<String> pathSegments = Uri.parse(str).getPathSegments();
                    if (pathSegments.isEmpty()) {
                        return true;
                    }
                    int intValue = Integer.valueOf(pathSegments.get(0)).intValue();
                    if (intValue == 0) {
                        AuthDialog.this.h.setVisibility(8);
                        AuthDialog.this.k.setVisibility(0);
                    } else if (intValue == 1) {
                        AuthDialog.this.h.setVisibility(0);
                    }
                    return true;
                } catch (Exception e2) {
                    return true;
                }
            } else if (str.startsWith(AuthConstants.ON_LOGIN_URI)) {
                try {
                    List<String> pathSegments2 = Uri.parse(str).getPathSegments();
                    if (!pathSegments2.isEmpty()) {
                        String unused2 = AuthDialog.this.q = pathSegments2.get(0);
                    }
                } catch (Exception e3) {
                }
                return true;
            } else if (AuthDialog.this.m.a(AuthDialog.this.k, str)) {
                return true;
            } else {
                n.c(AuthDialog.f3486a, "-->Redirect URL: return false");
                return false;
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            n.c(AuthDialog.f3486a, "-->onReceivedError, errorCode: " + i + " | description: " + str);
            if (!Util.checkNetWork(AuthDialog.this.l)) {
                AuthDialog.this.c.onError(new UiError(9001, "当前网络不可用，请稍后重试！", str2));
                AuthDialog.this.dismiss();
            } else if (!AuthDialog.this.p.startsWith(ServerSetting.DOWNLOAD_QQ_URL)) {
                long elapsedRealtime = SystemClock.elapsedRealtime() - AuthDialog.this.r;
                if (AuthDialog.this.o >= 1 || elapsedRealtime >= AuthDialog.this.s) {
                    AuthDialog.this.k.loadUrl(AuthDialog.this.b());
                    return;
                }
                AuthDialog.m(AuthDialog.this);
                AuthDialog.this.e.postDelayed(new Runnable() {
                    public void run() {
                        AuthDialog.this.k.loadUrl(AuthDialog.this.p);
                    }
                }, 500);
            } else {
                AuthDialog.this.c.onError(new UiError(i, str, str2));
                AuthDialog.this.dismiss();
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            n.b(AuthDialog.f3486a, "-->onPageStarted, url: " + str);
            super.onPageStarted(webView, str, bitmap);
            AuthDialog.this.h.setVisibility(0);
            long unused = AuthDialog.this.r = SystemClock.elapsedRealtime();
            if (!TextUtils.isEmpty(AuthDialog.this.p)) {
                AuthDialog.this.e.removeCallbacks((Runnable) AuthDialog.this.t.remove(AuthDialog.this.p));
            }
            String unused2 = AuthDialog.this.p = str;
            TimeOutRunable timeOutRunable = new TimeOutRunable(AuthDialog.this.p);
            AuthDialog.this.t.put(str, timeOutRunable);
            AuthDialog.this.e.postDelayed(timeOutRunable, 120000);
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            n.b(AuthDialog.f3486a, "-->onPageFinished, url: " + str);
            AuthDialog.this.h.setVisibility(8);
            if (AuthDialog.this.k != null) {
                AuthDialog.this.k.setVisibility(0);
            }
            if (!TextUtils.isEmpty(str)) {
                AuthDialog.this.e.removeCallbacks((Runnable) AuthDialog.this.t.remove(str));
            }
        }

        @TargetApi(8)
        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            sslErrorHandler.cancel();
            AuthDialog.this.c.onError(new UiError(sslError.getPrimaryError(), "请求不合法，请检查手机安全设置，如系统时间、代理等。", "ssl error"));
            AuthDialog.this.dismiss();
        }
    }

    /* compiled from: ProGuard */
    class TimeOutRunable implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        String f3494a = Constants.STR_EMPTY;

        public TimeOutRunable(String str) {
            this.f3494a = str;
        }

        public void run() {
            n.b(AuthDialog.f3486a, "-->timeoutUrl: " + this.f3494a + " | mRetryUrl: " + AuthDialog.this.p);
            if (this.f3494a.equals(AuthDialog.this.p)) {
                AuthDialog.this.c.onError(new UiError(9002, "请求页面超时，请稍后重试！", AuthDialog.this.p));
                AuthDialog.this.dismiss();
            }
        }
    }

    /* access modifiers changed from: private */
    public String b() {
        String str = ServerSetting.DOWNLOAD_QQ_URL + this.b.substring(this.b.indexOf("?") + 1);
        n.c(f3486a, "-->generateDownloadUrl, url: http://qzs.qq.com/open/mobile/login/qzsjump.html?");
        return str;
    }

    private void c() {
        d();
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        this.k = new c(this.l);
        this.k.setLayoutParams(layoutParams);
        this.f = new FrameLayout(this.l);
        layoutParams.gravity = 17;
        this.f.setLayoutParams(layoutParams);
        this.f.addView(this.k);
        this.f.addView(this.h);
        setContentView(this.f);
    }

    private void d() {
        this.i = new ProgressBar(this.l);
        this.i.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.g = new LinearLayout(this.l);
        TextView textView = null;
        if (this.j.equals(SystemUtils.ACTION_LOGIN)) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.gravity = 16;
            layoutParams.leftMargin = 5;
            textView = new TextView(this.l);
            if (Locale.getDefault().getLanguage().equals("zh")) {
                textView.setText("登录中...");
            } else {
                textView.setText("Logging in...");
            }
            textView.setTextColor(Color.rgb(255, 255, 255));
            textView.setTextSize(18.0f);
            textView.setLayoutParams(layoutParams);
        }
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 17;
        this.g.setLayoutParams(layoutParams2);
        this.g.addView(this.i);
        if (textView != null) {
            this.g.addView(textView);
        }
        this.h = new FrameLayout(this.l);
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-1, -2);
        layoutParams3.leftMargin = 80;
        layoutParams3.rightMargin = 80;
        layoutParams3.topMargin = 40;
        layoutParams3.bottomMargin = 40;
        layoutParams3.gravity = 17;
        this.h.setLayoutParams(layoutParams3);
        this.h.setBackgroundResource(17301504);
        this.h.addView(this.g);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void e() {
        this.k.setVerticalScrollBarEnabled(false);
        this.k.setHorizontalScrollBarEnabled(false);
        this.k.setWebViewClient(new LoginWebViewClient());
        this.k.setWebChromeClient(new WebChromeClient());
        this.k.clearFormData();
        this.k.clearSslPreferences();
        this.k.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                return true;
            }
        });
        this.k.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                    case 1:
                        if (view.hasFocus()) {
                            return false;
                        }
                        view.requestFocus();
                        return false;
                    default:
                        return false;
                }
            }
        });
        WebSettings settings = this.k.getSettings();
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setCacheMode(-1);
        settings.setNeedInitialFocus(false);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setJavaScriptEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setDatabasePath(this.l.getDir("databases", 0).getPath());
        settings.setDomStorageEnabled(true);
        n.b(f3486a, "-->mUrl : " + this.b);
        this.p = this.b;
        this.k.loadUrl(this.b);
        this.k.setVisibility(4);
        this.k.getSettings().setSavePassword(false);
        this.m.a(new SecureJsInterface(), "SecureJsInterface");
        SecureJsInterface.isPWDEdit = false;
        super.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                try {
                    JniInterface.clearAllPWD();
                } catch (Exception e) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean f() {
        AuthMap instance = AuthMap.getInstance();
        String makeKey = instance.makeKey();
        AuthMap.Auth auth = new AuthMap.Auth();
        auth.listener = this.d;
        auth.dialog = this;
        auth.key = makeKey;
        String str = instance.set(auth);
        String substring = this.b.substring(0, this.b.indexOf("?"));
        Bundle parseUrl = Util.parseUrl(this.b);
        parseUrl.putString("token_key", makeKey);
        parseUrl.putString("serial", str);
        parseUrl.putString("browser", "1");
        this.b = substring + "?" + Util.encodeUrl(parseUrl);
        return Util.openBrowser(this.l, this.b);
    }

    /* access modifiers changed from: private */
    public static void b(Context context, String str) {
        try {
            JSONObject parseJson = Util.parseJson(str);
            int i2 = parseJson.getInt(SocialConstants.PARAM_TYPE);
            Toast.makeText(context.getApplicationContext(), parseJson.getString(SocialConstants.PARAM_SEND_MSG), i2).show();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void callJs(String str, String str2) {
        this.k.loadUrl("javascript:" + str + "(" + str2 + ");void(" + System.currentTimeMillis() + ");");
    }

    public void dismiss() {
        this.t.clear();
        this.e.removeCallbacksAndMessages(null);
        if (isShowing()) {
            super.dismiss();
        }
        if (this.k != null) {
            this.k.destroy();
            this.k = null;
        }
    }
}
