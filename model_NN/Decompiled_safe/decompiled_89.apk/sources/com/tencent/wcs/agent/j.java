package com.tencent.wcs.agent;

import com.tencent.wcs.agent.config.RemoteType;
import com.tencent.wcs.b.c;
import com.tencent.wcs.c.b;
import java.io.IOException;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f3837a;

    j(h hVar) {
        this.f3837a = hVar;
    }

    public void run() {
        try {
            if (this.f3837a.k.b() == RemoteType.WEB) {
                this.f3837a.g();
            } else if (this.f3837a.k.b() == RemoteType.PC) {
                this.f3837a.f();
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (!this.f3837a.g) {
                if (c.f3846a) {
                    b.a("AgentSession read input exception channelId " + this.f3837a.m + " messageId " + this.f3837a.n + " port " + this.f3837a.o + " remote " + this.f3837a.k.a() + ", close it now");
                }
                this.f3837a.a(null, h.h(this.f3837a), 2);
                if (this.f3837a.s != null) {
                    this.f3837a.s.a(this.f3837a.n);
                }
            }
        }
    }
}
