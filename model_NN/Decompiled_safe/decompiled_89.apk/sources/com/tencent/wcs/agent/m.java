package com.tencent.wcs.agent;

import com.tencent.wcs.jce.TransDataMessage;
import java.util.Comparator;

/* compiled from: ProGuard */
class m implements Comparator<TransDataMessage> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f3840a;

    m(l lVar) {
        this.f3840a = lVar;
    }

    /* renamed from: a */
    public int compare(TransDataMessage transDataMessage, TransDataMessage transDataMessage2) {
        return transDataMessage.f - transDataMessage2.f;
    }
}
