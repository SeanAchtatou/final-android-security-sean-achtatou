package com.tencent.wcs.agent;

import com.tencent.wcs.jce.ServiceTransData;
import java.util.Comparator;

/* compiled from: ProGuard */
class i implements Comparator<ServiceTransData> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f3836a;

    i(h hVar) {
        this.f3836a = hVar;
    }

    /* renamed from: a */
    public int compare(ServiceTransData serviceTransData, ServiceTransData serviceTransData2) {
        return serviceTransData.e - serviceTransData2.e;
    }
}
