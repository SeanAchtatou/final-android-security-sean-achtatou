package com.tencent.wcs.proxy;

import com.tencent.wcs.b.c;
import com.tencent.wcs.c.b;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private int f3895a;
    private long b;
    private boolean c;

    public a() {
        c();
    }

    public boolean a(int i) {
        if (!this.c) {
            return false;
        }
        if (i != 100) {
            this.f3895a++;
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (this.b == 0) {
            this.b = currentTimeMillis;
            return false;
        } else if (this.f3895a > 0) {
            this.f3895a = 0;
            this.b = currentTimeMillis;
            return false;
        } else if (currentTimeMillis - this.b > ((long) ((c.k * 1000) - 100))) {
            b.a("AutoCloser auto close long connection service");
            return true;
        } else {
            this.b = currentTimeMillis;
            return false;
        }
    }

    public void a() {
        this.c = true;
    }

    public void b() {
        this.c = false;
    }

    public void c() {
        this.f3895a = 0;
        this.b = 0;
        this.c = c.q;
    }
}
