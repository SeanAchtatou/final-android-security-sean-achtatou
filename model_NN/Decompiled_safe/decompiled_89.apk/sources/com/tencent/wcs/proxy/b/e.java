package com.tencent.wcs.proxy.b;

/* compiled from: ProGuard */
public class e extends a<byte[], Integer> {
    public static e b() {
        return g.f3900a;
    }

    private e() {
    }

    /* access modifiers changed from: protected */
    public byte[] a(Integer... numArr) {
        if (b(numArr)) {
            return new byte[numArr[0].intValue()];
        }
        return new byte[1024];
    }

    /* access modifiers changed from: protected */
    public boolean b(Integer... numArr) {
        return numArr != null && numArr.length > 0 && numArr[0].intValue() > 1024;
    }

    /* access modifiers changed from: protected */
    public boolean a(byte[] bArr) {
        return bArr != null && bArr.length < 1024;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void c(byte[] bArr) {
    }
}
