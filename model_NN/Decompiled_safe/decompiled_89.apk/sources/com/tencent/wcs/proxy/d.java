package com.tencent.wcs.proxy;

import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.ba;
import com.tencent.wcs.c.b;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3912a;

    d(c cVar) {
        this.f3912a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.wcs.proxy.c.a(com.tencent.wcs.proxy.c, boolean):boolean
     arg types: [com.tencent.wcs.proxy.c, int]
     candidates:
      com.tencent.wcs.proxy.c.a(com.tencent.wcs.proxy.c, int):int
      com.tencent.wcs.proxy.c.a(com.tencent.wcs.proxy.c, boolean):boolean */
    public void run() {
        synchronized (this.f3912a.l) {
            if (this.f3912a.b != null) {
                boolean c = this.f3912a.b.c();
                b.a("WanServiceManager start wireless service result: " + c);
                if (c) {
                    boolean unused = this.f3912a.m = true;
                    this.f3912a.b.a(100, this.f3912a.c);
                    this.f3912a.b.a(1997, this.f3912a.f);
                    this.f3912a.b.a((int) STConstAction.ACTION_HIT_PAUSE, this.f3912a.e);
                    this.f3912a.b.a((int) STConst.ST_PAGE_NECESSARY, this.f3912a.d);
                    this.f3912a.b.a(1996, this.f3912a);
                    this.f3912a.b.a(1994, this.f3912a);
                    this.f3912a.b.a((int) STConstAction.ACTION_HIT_EXPAND, this.f3912a.j);
                    this.f3912a.f();
                    int unused2 = this.f3912a.o = 0;
                    ba.a().post(new e(this));
                } else if (this.f3912a.o < 5) {
                    c.j(this.f3912a);
                    this.f3912a.b.d();
                    this.f3912a.r();
                } else {
                    ba.a().post(new f(this));
                }
            }
        }
    }
}
