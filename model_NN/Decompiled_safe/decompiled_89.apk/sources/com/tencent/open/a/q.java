package com.tencent.open.a;

/* compiled from: ProGuard */
public abstract class q {

    /* renamed from: a  reason: collision with root package name */
    private volatile int f3760a;
    private volatile boolean b;
    private p c;

    /* access modifiers changed from: protected */
    public abstract void a(int i, Thread thread, long j, String str, String str2, Throwable th);

    public q() {
        this(f.b, true, p.f3759a);
    }

    public q(int i, boolean z, p pVar) {
        this.f3760a = f.b;
        this.b = true;
        this.c = p.f3759a;
        a(i);
        a(z);
        a(pVar);
    }

    public void b(int i, Thread thread, long j, String str, String str2, Throwable th) {
        if (d() && h.a(this.f3760a, i)) {
            a(i, thread, j, str, str2, th);
        }
    }

    public void a(int i) {
        this.f3760a = i;
    }

    public boolean d() {
        return this.b;
    }

    public void a(boolean z) {
        this.b = z;
    }

    public p e() {
        return this.c;
    }

    public void a(p pVar) {
        this.c = pVar;
    }
}
