package com.tencent.open.b;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import com.tencent.open.a.n;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.OpenConfig;
import com.tencent.open.utils.ThreadManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class g {

    /* renamed from: a  reason: collision with root package name */
    protected static final String f3766a = (n.d + ".ReportManager");
    protected static g b;
    protected Random c = new Random();
    protected List<Serializable> d = Collections.synchronizedList(new ArrayList());
    protected List<Serializable> e = Collections.synchronizedList(new ArrayList());
    protected HandlerThread f = null;
    protected Handler g;

    public static synchronized g a() {
        g gVar;
        synchronized (g.class) {
            if (b == null) {
                b = new g();
            }
            gVar = b;
        }
        return gVar;
    }

    private g() {
        if (this.f == null) {
            this.f = new HandlerThread("opensdk.report.handlerthread", 10);
            this.f.start();
        }
        if (this.f.isAlive() && this.f.getLooper() != null) {
            this.g = new h(this, this.f.getLooper());
        }
    }

    public void a(Bundle bundle, String str, boolean z) {
        if (bundle != null) {
            n.b(f3766a, "-->reportVia, bundle: " + bundle.toString());
            if (a("report_via", str) || z) {
                this.g.post(new i(this, bundle, z));
            }
        }
    }

    public void a(String str, long j, long j2, long j3, int i) {
        a(str, j, j2, j3, i, Constants.STR_EMPTY, false);
    }

    public void a(String str, long j, long j2, long j3, int i, String str2, boolean z) {
        n.b(f3766a, "-->reportCgi, command: " + str + " | startTime: " + j + " | reqSize:" + j2 + " | rspSize: " + j3 + " | responseCode: " + i + " | detail: " + str2);
        if (a("report_cgi", Constants.STR_EMPTY + i) || z) {
            this.g.post(new j(this, j, str, str2, i, j2, j3, z));
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        ThreadManager.executeOnNetWorkThread(new k(this));
    }

    /* access modifiers changed from: protected */
    public boolean a(String str, String str2) {
        int i;
        boolean z = true;
        boolean z2 = false;
        n.b(f3766a, "-->availableFrequency, report: " + str + " | ext: " + str2);
        if (!TextUtils.isEmpty(str)) {
            if (str.equals("report_cgi")) {
                try {
                    int a2 = a(Integer.parseInt(str2));
                    if (this.c.nextInt(100) >= a2) {
                        z = false;
                    }
                    z2 = z;
                    i = a2;
                } catch (Exception e2) {
                }
            } else if (str.equals("report_via")) {
                int a3 = e.a(str2);
                if (this.c.nextInt(100) < a3) {
                    z2 = true;
                    i = a3;
                } else {
                    i = a3;
                }
            } else {
                i = 100;
            }
            n.b(f3766a, "-->availableFrequency, result: " + z2 + " | frequency: " + i);
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public boolean a(String str, int i) {
        int i2 = 5;
        if (str.equals("report_cgi")) {
            int i3 = OpenConfig.getInstance(Global.getContext(), null).getInt("Common_CGIReportMaxcount");
            if (i3 != 0) {
                i2 = i3;
            }
        } else if (str.equals("report_via")) {
            int i4 = OpenConfig.getInstance(Global.getContext(), null).getInt("Agent_ReportBatchCount");
            if (i4 != 0) {
                i2 = i4;
            }
        } else {
            i2 = 0;
        }
        n.b(f3766a, "-->availableCount, report: " + str + " | dataSize: " + i + " | maxcount: " + i2);
        if (i >= i2) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        if (i == 0) {
            int i2 = OpenConfig.getInstance(Global.getContext(), null).getInt("Common_CGIReportFrequencySuccess");
            if (i2 == 0) {
                return 10;
            }
            return i2;
        }
        int i3 = OpenConfig.getInstance(Global.getContext(), null).getInt("Common_CGIReportFrequencyFailed");
        if (i3 == 0) {
            return 100;
        }
        return i3;
    }

    /* access modifiers changed from: protected */
    public Bundle c() {
        if (this.d.size() == 0) {
            return null;
        }
        b bVar = (b) this.d.get(0);
        if (bVar == null) {
            n.b(f3766a, "-->prepareCgiData, the 0th cgireportitem is null.");
            return null;
        }
        String str = bVar.f3762a.get("appid");
        List<Serializable> a2 = f.a().a("report_cgi");
        if (a2 != null) {
            this.d.addAll(a2);
        }
        n.b(f3766a, "-->prepareCgiData, mCgiList size: " + this.d.size());
        if (this.d.size() == 0) {
            return null;
        }
        Bundle bundle = new Bundle();
        try {
            bundle.putString("appid", str);
            bundle.putString("releaseversion", Constants.SDK_VERSION_REPORT);
            bundle.putString("device", Build.DEVICE);
            bundle.putString("qua", Constants.SDK_QUA);
            bundle.putString("key", "apn,frequency,commandid,resultcode,tmcost,reqsize,rspsize,detail,touin,deviceinfo");
            for (int i = 0; i < this.d.size(); i++) {
                b bVar2 = (b) this.d.get(i);
                bundle.putString(i + "_1", bVar2.f3762a.get("apn"));
                bundle.putString(i + "_2", bVar2.f3762a.get("frequency"));
                bundle.putString(i + "_3", bVar2.f3762a.get("commandid"));
                bundle.putString(i + "_4", bVar2.f3762a.get("resultCode"));
                bundle.putString(i + "_5", bVar2.f3762a.get("timeCost"));
                bundle.putString(i + "_6", bVar2.f3762a.get("reqSize"));
                bundle.putString(i + "_7", bVar2.f3762a.get("rspSize"));
                bundle.putString(i + "_8", bVar2.f3762a.get("detail"));
                bundle.putString(i + "_9", bVar2.f3762a.get("uin"));
                bundle.putString(i + "_10", c.e(Global.getContext()) + "&" + bVar2.f3762a.get("deviceInfo"));
            }
            n.b(f3766a, "-->prepareCgiData, end. params: " + bundle.toString());
            return bundle;
        } catch (Exception e2) {
            n.b(f3766a, "-->prepareCgiData, exception.", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public Bundle d() {
        List<Serializable> a2 = f.a().a("report_via");
        if (a2 != null) {
            this.e.addAll(a2);
        }
        n.b(f3766a, "-->prepareViaData, mViaList size: " + this.e.size());
        if (this.e.size() == 0) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        Iterator<Serializable> it = this.e.iterator();
        while (it.hasNext()) {
            JSONObject jSONObject = new JSONObject();
            b bVar = (b) it.next();
            for (String next : bVar.f3762a.keySet()) {
                try {
                    String str = bVar.f3762a.get(next);
                    if (str == null) {
                        str = Constants.STR_EMPTY;
                    }
                    jSONObject.put(next, str);
                } catch (JSONException e2) {
                    n.a(f3766a, "-->prepareViaData, put bundle to json array exception", e2);
                }
            }
            jSONArray.put(jSONObject);
        }
        n.b(f3766a, "-->prepareViaData, JSONArray array: " + jSONArray.toString());
        Bundle bundle = new Bundle();
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("data", jSONArray);
            bundle.putString("data", jSONObject2.toString());
            return bundle;
        } catch (JSONException e3) {
            n.a(f3766a, "-->prepareViaData, put bundle to json array exception", e3);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        ThreadManager.executeOnNetWorkThread(new l(this));
    }

    public void a(String str, String str2, Bundle bundle, boolean z) {
        ThreadManager.executeOnSubThread(new m(this, bundle, str, z, str2));
    }
}
