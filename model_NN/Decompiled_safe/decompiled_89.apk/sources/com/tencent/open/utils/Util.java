package com.tencent.open.utils;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Process;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import com.tencent.open.a.n;
import com.tencent.open.b.a;
import com.tencent.open.utils.HttpUtils;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class Util {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f3794a = (n.d + "." + Util.class.getName());
    private static String b = Constants.STR_EMPTY;
    private static String c = Constants.STR_EMPTY;
    private static String d = Constants.STR_EMPTY;
    private static String e = Constants.STR_EMPTY;
    private static int f = -1;
    private static String g;
    private static boolean h = true;
    private static String i = "0123456789ABCDEF";

    public static String encodePostBody(Bundle bundle, String str) {
        if (bundle == null) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        int size = bundle.size();
        int i2 = -1;
        for (String next : bundle.keySet()) {
            int i3 = i2 + 1;
            Object obj = bundle.get(next);
            if (!(obj instanceof String)) {
                i2 = i3;
            } else {
                sb.append("Content-Disposition: form-data; name=\"" + next + "\"" + "\r\n" + "\r\n" + ((String) obj));
                if (i3 < size - 1) {
                    sb.append("\r\n--" + str + "\r\n");
                }
                i2 = i3;
            }
        }
        return sb.toString();
    }

    public static String encodeUrl(Bundle bundle) {
        if (bundle == null) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if ((obj instanceof String) || (obj instanceof String[])) {
                if (obj instanceof String[]) {
                    if (z) {
                        z = false;
                    } else {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(next) + "=");
                    String[] stringArray = bundle.getStringArray(next);
                    if (stringArray != null) {
                        for (int i2 = 0; i2 < stringArray.length; i2++) {
                            if (i2 == 0) {
                                sb.append(URLEncoder.encode(stringArray[i2]));
                            } else {
                                sb.append(URLEncoder.encode("," + stringArray[i2]));
                            }
                        }
                    }
                } else {
                    if (z) {
                        z = false;
                    } else {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(next) + "=" + URLEncoder.encode(bundle.getString(next)));
                }
                z = z;
            }
        }
        return sb.toString();
    }

    public static Bundle decodeUrl(String str) {
        Bundle bundle = new Bundle();
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                if (split2.length == 2) {
                    bundle.putString(URLDecoder.decode(split2[0]), URLDecoder.decode(split2[1]));
                }
            }
        }
        return bundle;
    }

    public static JSONObject decodeUrlToJson(JSONObject jSONObject, String str) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                if (split2.length == 2) {
                    try {
                        jSONObject.put(URLDecoder.decode(split2[0]), URLDecoder.decode(split2[1]));
                    } catch (JSONException e2) {
                        n.e(f3794a, "decodeUrlToJson has exception: " + e2.getMessage());
                    }
                }
            }
        }
        return jSONObject;
    }

    public static Bundle parseUrl(String str) {
        try {
            URL url = new URL(str.replace("auth://", "http://"));
            Bundle decodeUrl = decodeUrl(url.getQuery());
            decodeUrl.putAll(decodeUrl(url.getRef()));
            return decodeUrl;
        } catch (MalformedURLException e2) {
            return new Bundle();
        }
    }

    public static JSONObject parseUrlToJson(String str) {
        try {
            URL url = new URL(str.replace("auth://", "http://"));
            JSONObject decodeUrlToJson = decodeUrlToJson(null, url.getQuery());
            decodeUrlToJson(decodeUrlToJson, url.getRef());
            return decodeUrlToJson;
        } catch (MalformedURLException e2) {
            return new JSONObject();
        }
    }

    /* compiled from: ProGuard */
    public class Statistic {
        public long reqSize;
        public String response;
        public long rspSize;

        public Statistic(String str, int i) {
            this.response = str;
            this.reqSize = (long) i;
            if (this.response != null) {
                this.rspSize = (long) this.response.length();
            }
        }
    }

    /* compiled from: ProGuard */
    class TBufferedOutputStream extends BufferedOutputStream {

        /* renamed from: a  reason: collision with root package name */
        private int f3796a = 0;

        public TBufferedOutputStream(OutputStream outputStream) {
            super(outputStream);
        }

        public void write(byte[] bArr) {
            super.write(bArr);
            this.f3796a += bArr.length;
        }

        public int getLength() {
            return this.f3796a;
        }
    }

    public static Statistic upload(Context context, String str, Bundle bundle) {
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        if (context == null || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || ((activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable())) {
            Bundle bundle2 = new Bundle(bundle);
            String string = bundle2.getString("appid_for_getting_config");
            bundle2.remove("appid_for_getting_config");
            HttpClient httpClient = HttpUtils.getHttpClient(context, string, str);
            HttpPost httpPost = new HttpPost(str);
            Bundle bundle3 = new Bundle();
            for (String next : bundle2.keySet()) {
                Object obj = bundle2.get(next);
                if (obj instanceof byte[]) {
                    bundle3.putByteArray(next, (byte[]) obj);
                }
            }
            httpPost.setHeader("Content-Type", "multipart/form-data; boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
            httpPost.setHeader("Connection", "Keep-Alive");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.write(getBytesUTF8("--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
            byteArrayOutputStream.write(getBytesUTF8(encodePostBody(bundle2, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f")));
            if (!bundle3.isEmpty()) {
                int size = bundle3.size();
                byteArrayOutputStream.write(getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
                int i2 = -1;
                for (String next2 : bundle3.keySet()) {
                    i2++;
                    byteArrayOutputStream.write(getBytesUTF8("Content-Disposition: form-data; name=\"" + next2 + "\"; filename=\"" + "value.file" + "\"" + "\r\n"));
                    byteArrayOutputStream.write(getBytesUTF8("Content-Type: application/octet-stream\r\n\r\n"));
                    byte[] byteArray = bundle3.getByteArray(next2);
                    if (byteArray != null) {
                        byteArrayOutputStream.write(byteArray);
                    }
                    if (i2 < size - 1) {
                        byteArrayOutputStream.write(getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
                    }
                }
            }
            byteArrayOutputStream.write(getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f--\r\n"));
            byte[] byteArray2 = byteArrayOutputStream.toByteArray();
            int length = byteArray2.length + 0;
            byteArrayOutputStream.close();
            httpPost.setEntity(new ByteArrayEntity(byteArray2));
            HttpResponse execute = httpClient.execute(httpPost);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return new Statistic(a(execute), length);
            }
            throw new HttpUtils.HttpStatusException(HttpUtils.HttpStatusException.ERROR_INFO + statusCode);
        }
        throw new HttpUtils.NetworkUnavailableException(HttpUtils.NetworkUnavailableException.ERROR_INFO);
    }

    private static String a(HttpResponse httpResponse) {
        InputStream inputStream;
        InputStream content = httpResponse.getEntity().getContent();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Header firstHeader = httpResponse.getFirstHeader("Content-Encoding");
        if (firstHeader == null || firstHeader.getValue().toLowerCase().indexOf("gzip") <= -1) {
            inputStream = content;
        } else {
            inputStream = new GZIPInputStream(content);
        }
        byte[] bArr = new byte[Process.PROC_PARENS];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static JSONObject parseJson(String str) {
        if (str.equals("false")) {
            str = "{value : false}";
        }
        if (str.equals("true")) {
            str = "{value : true}";
        }
        if (str.contains("allback(")) {
            str = str.replaceFirst("[\\s\\S]*allback\\(([\\s\\S]*)\\);[^\\)]*\\z", "$1").trim();
        }
        if (str.contains("online[0]=")) {
            str = "{online:" + str.charAt(str.length() - 2) + "}";
        }
        return new JSONObject(str);
    }

    public static void showAlert(Context context, String str, String str2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(str);
        builder.setMessage(str2);
        builder.create().show();
    }

    public static void logd(String str, String str2) {
        if (h) {
            n.b(str, str2);
        }
    }

    public static String getUserIp() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces != null && networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e2) {
            logd("Tencent-Util", e2.toString());
        }
        return Constants.STR_EMPTY;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    private static boolean a(Context context) {
        Signature[] signatureArr;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.tencent.mtt", 64);
            String str = packageInfo.versionName;
            if (SystemUtils.compareVersion(str, "4.3") < 0 || str.startsWith("4.4") || (signatureArr = packageInfo.signatures) == null) {
                return false;
            }
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(signatureArr[0].toByteArray());
                String hexString = toHexString(instance.digest());
                instance.reset();
                if (hexString.equals("d8391a394d4a179e6fe7bdb8a301258b")) {
                    return true;
                }
                return false;
            } catch (NoSuchAlgorithmException e2) {
                n.e(f3794a, "isQQBrowerAvailable has exception: " + e2.getMessage());
                return false;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            return false;
        }
    }

    public static boolean openBrowser(Context context, String str) {
        boolean z;
        try {
            z = a(context);
            if (z) {
                try {
                    a(context, "com.tencent.mtt", "com.tencent.mtt.MainActivity", str);
                } catch (Exception e2) {
                    if (z) {
                        try {
                            a(context, "com.android.browser", "com.android.browser.BrowserActivity", str);
                        } catch (Exception e3) {
                            try {
                                a(context, "com.google.android.browser", "com.android.browser.BrowserActivity", str);
                            } catch (Exception e4) {
                                try {
                                    a(context, "com.android.chrome", "com.google.android.apps.chrome.Main", str);
                                } catch (Exception e5) {
                                    return false;
                                }
                            }
                        }
                    } else {
                        try {
                            a(context, "com.google.android.browser", "com.android.browser.BrowserActivity", str);
                        } catch (Exception e6) {
                            try {
                                a(context, "com.android.chrome", "com.google.android.apps.chrome.Main", str);
                            } catch (Exception e7) {
                                return false;
                            }
                        }
                    }
                    return true;
                }
            } else {
                a(context, "com.android.browser", "com.android.browser.BrowserActivity", str);
            }
        } catch (Exception e8) {
            z = false;
        }
        return true;
    }

    private static void a(Context context, String str, String str2, String str3) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(str, str2));
        intent.setAction("android.intent.action.VIEW");
        intent.addFlags(1073741824);
        intent.addFlags(268435456);
        intent.setData(Uri.parse(str3));
        context.startActivity(intent);
    }

    public static boolean isMobileQQSupportShare(Context context) {
        try {
            if (SystemUtils.compareVersion(context.getPackageManager().getPackageInfo("com.tencent.mobileqq", 0).versionName, "4.1") >= 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            n.b("checkMobileQQ", "error");
            return false;
        }
    }

    public static String encrypt(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(getBytesUTF8(str));
            byte[] digest = instance.digest();
            if (digest == null) {
                return str;
            }
            StringBuilder sb = new StringBuilder();
            for (byte b2 : digest) {
                sb.append(a(b2 >>> 4));
                sb.append(a(b2));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e2) {
            n.e(f3794a, "encrypt has exception: " + e2.getMessage());
            return str;
        }
    }

    private static char a(int i2) {
        int i3 = i2 & 15;
        if (i3 < 10) {
            return (char) (i3 + 48);
        }
        return (char) ((i3 - 10) + 97);
    }

    public static void reportBernoulli(final Context context, String str, long j, String str2) {
        final Bundle bundle = new Bundle();
        bundle.putString("appid_for_getting_config", str2);
        bundle.putString("strValue", str2);
        bundle.putString("nValue", str);
        bundle.putString("qver", Constants.SDK_VERSION);
        if (j != 0) {
            bundle.putLong("elt", j);
        }
        new Thread() {
            public void run() {
                try {
                    HttpUtils.openUrl2(context, "http://cgi.qplus.com/report/report", Constants.HTTP_GET, bundle);
                } catch (Exception e) {
                    n.e(Util.f3794a, "reportBernoulli has exception: " + e.getMessage());
                }
            }
        }.start();
    }

    public static boolean hasSDCard() {
        File file = null;
        if (Environment.getExternalStorageState().equals("mounted")) {
            file = Environment.getExternalStorageDirectory();
        }
        if (file != null) {
            return true;
        }
        return false;
    }

    public static String toHexString(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b2 : bArr) {
            String num = Integer.toString(b2 & 255, 16);
            if (num.length() == 1) {
                num = "0" + num;
            }
            sb.append(num);
        }
        return sb.toString();
    }

    public static String toHexString(String str) {
        byte[] bytesUTF8 = getBytesUTF8(str);
        StringBuilder sb = new StringBuilder(bytesUTF8.length * 2);
        for (int i2 = 0; i2 < bytesUTF8.length; i2++) {
            sb.append(i.charAt((bytesUTF8[i2] & 240) >> 4));
            sb.append(i.charAt((bytesUTF8[i2] & 15) >> 0));
        }
        return sb.toString();
    }

    public static String hexToString(String str) {
        if ("0x".equals(str.substring(0, 2))) {
            str = str.substring(2);
        }
        byte[] bArr = new byte[(str.length() / 2)];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            try {
                bArr[i2] = (byte) (Integer.parseInt(str.substring(i2 * 2, (i2 * 2) + 2), 16) & 255);
            } catch (Exception e2) {
                n.e(f3794a, "hexToString has exception: " + e2.getMessage());
            }
        }
        try {
            return new String(bArr, "utf-8");
        } catch (Exception e3) {
            n.e(f3794a, "hexToString has exception: " + e3.getMessage());
            return str;
        }
    }

    public static String getAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            n.e(f3794a, "getAppVersion error" + e2.getMessage());
            return Constants.STR_EMPTY;
        }
    }

    public static final String getApplicationLable(Context context) {
        CharSequence applicationLabel;
        if (context == null || (applicationLabel = context.getPackageManager().getApplicationLabel(context.getApplicationInfo())) == null) {
            return null;
        }
        return applicationLabel.toString();
    }

    public static final boolean isValidUrl(String str) {
        if (str == null) {
            return false;
        }
        if (str.startsWith("http://") || str.startsWith("https://")) {
            return true;
        }
        return false;
    }

    public static final boolean isValidPath(String str) {
        File file;
        if (str == null || (file = new File(str)) == null || !file.exists()) {
            return false;
        }
        return true;
    }

    public static boolean fileExists(String str) {
        File file;
        if (str == null || (file = new File(str)) == null || !file.exists()) {
            return false;
        }
        return true;
    }

    public static final String subString(String str, int i2, String str2, String str3) {
        int i3 = 0;
        if (TextUtils.isEmpty(str)) {
            return Constants.STR_EMPTY;
        }
        if (TextUtils.isEmpty(str2)) {
            str2 = "UTF-8";
        }
        try {
            if (str.getBytes(str2).length <= i2) {
                return str;
            }
            int i4 = 0;
            while (i3 < str.length()) {
                int length = str.substring(i3, i3 + 1).getBytes(str2).length;
                if (i4 + length > i2) {
                    String substring = str.substring(0, i3);
                    try {
                        if (!TextUtils.isEmpty(str3)) {
                            substring = substring + str3;
                        }
                        return substring;
                    } catch (Exception e2) {
                        str = substring;
                        e = e2;
                        System.out.println("StructMsg sSubString error : " + e.getMessage());
                        return str;
                    }
                } else {
                    i4 += length;
                    i3++;
                }
            }
            return str;
        } catch (Exception e3) {
            e = e3;
            System.out.println("StructMsg sSubString error : " + e.getMessage());
            return str;
        }
    }

    public static int parseIntValue(String str) {
        return parseIntValue(str, 0);
    }

    public static int parseIntValue(String str, int i2) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e2) {
            return i2;
        }
    }

    public static boolean checkNetWork(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return true;
        }
        NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
        if (allNetworkInfo == null) {
            return false;
        }
        for (NetworkInfo isConnectedOrConnecting : allNetworkInfo) {
            if (isConnectedOrConnecting.isConnectedOrConnecting()) {
                return true;
            }
        }
        return false;
    }

    public static Bundle composeViaReportParams(String str, String str2, String str3, String str4, String str5, String str6) {
        return composeViaReportParams(str, str3, str4, str2, str5, str6, Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY);
    }

    public static Bundle composeViaReportParams(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12) {
        Bundle bundle = new Bundle();
        bundle.putString("openid", str);
        bundle.putString("report_type", str2);
        bundle.putString("act_type", str3);
        bundle.putString("via", str4);
        bundle.putString("app_id", str5);
        bundle.putString("result", str6);
        bundle.putString(SocialConstants.PARAM_TYPE, str7);
        bundle.putString("login_status", str8);
        bundle.putString("need_user_auth", str9);
        bundle.putString("to_uin", str10);
        bundle.putString("call_source", str11);
        bundle.putString("to_type", str12);
        return bundle;
    }

    public static Bundle composeHaboCgiReportParams(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PARAM_PLATFORM, "1");
        bundle.putString("result", str);
        bundle.putString("code", str2);
        bundle.putString("tmcost", str3);
        bundle.putString("rate", str4);
        bundle.putString("cmd", str5);
        bundle.putString("uin", str6);
        bundle.putString("appid", str7);
        bundle.putString("share_type", str8);
        bundle.putString("detail", str9);
        bundle.putString("os_ver", Build.VERSION.RELEASE);
        bundle.putString("network", a.a(Global.getContext()));
        bundle.putString("apn", a.b(Global.getContext()));
        bundle.putString("model_name", Build.MODEL);
        bundle.putString("sdk_ver", Constants.SDK_VERSION);
        bundle.putString("packagename", Global.getPackageName());
        bundle.putString("app_ver", getAppVersionName(Global.getContext(), Global.getPackageName()));
        return bundle;
    }

    public static String getLocation(Context context) {
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            Criteria criteria = new Criteria();
            criteria.setCostAllowed(false);
            criteria.setAccuracy(2);
            String bestProvider = locationManager.getBestProvider(criteria, true);
            if (bestProvider != null) {
                Location lastKnownLocation = locationManager.getLastKnownLocation(bestProvider);
                if (lastKnownLocation == null) {
                    return Constants.STR_EMPTY;
                }
                double latitude = lastKnownLocation.getLatitude();
                g = latitude + "*" + lastKnownLocation.getLongitude();
                return g;
            }
        } catch (Exception e2) {
            n.b("getLocation", "getLocation>>>", e2);
        }
        return Constants.STR_EMPTY;
    }

    public static void getPackageInfo(Context context, String str) {
        if (context != null) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 0);
                c = packageInfo.versionName;
                b = c.substring(0, c.lastIndexOf(46));
                e = c.substring(c.lastIndexOf(46) + 1, c.length());
                f = packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e2) {
                n.e(f3794a, "getPackageInfo has exception: " + e2.getMessage());
            } catch (Exception e3) {
                n.e(f3794a, "getPackageInfo has exception: " + e3.getMessage());
            }
        }
    }

    public static String getVersionName(Context context, String str) {
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        getPackageInfo(context, str);
        return c;
    }

    public static String getAppVersionName(Context context, String str) {
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        getPackageInfo(context, str);
        return b;
    }

    public static String getQUA3(Context context, String str) {
        if (context == null) {
            return Constants.STR_EMPTY;
        }
        d = getAppVersionName(context, str);
        return d;
    }

    public static byte[] getBytesUTF8(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e2) {
            return null;
        }
    }

    public static boolean isNumeric(String str) {
        if (!Pattern.compile("[0-9]*").matcher(str).matches()) {
            return false;
        }
        return true;
    }
}
