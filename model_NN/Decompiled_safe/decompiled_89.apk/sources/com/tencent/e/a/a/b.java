package com.tencent.e.a.a;

import com.tencent.d.a.f;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    protected static final String f3661a = b.class.getSimpleName();

    public synchronized ArrayList<c> a() {
        ArrayList<c> a2;
        a2 = new a().a();
        f.a(f3661a, "result size = " + (a2 == null ? "null" : Integer.valueOf(a2.size())));
        return a2;
    }

    public synchronized boolean a(long j) {
        boolean a2;
        f.a(f3661a, "dbIdentity = " + j);
        if (j < 0) {
            f.a(f3661a, "dbIdentity < 0,return false");
            a2 = false;
        } else {
            a2 = new a().a(j);
            f.a(f3661a, "result = " + a2);
        }
        return a2;
    }
}
