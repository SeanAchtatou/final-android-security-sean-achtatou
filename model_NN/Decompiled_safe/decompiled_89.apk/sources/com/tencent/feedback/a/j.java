package com.tencent.feedback.a;

import android.content.Context;
import android.util.SparseArray;
import com.tencent.assistant.st.STConstAction;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import com.tencent.feedback.common.h;
import com.tencent.feedback.common.i;
import com.tencent.feedback.common.n;
import com.tencent.feedback.proguard.B;
import com.tencent.feedback.proguard.C;
import com.tencent.feedback.proguard.D;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.ae;
import com.tencent.feedback.proguard.ag;
import com.tencent.feedback.proguard.ao;
import com.tencent.securemodule.service.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public final class j implements i {

    /* renamed from: a  reason: collision with root package name */
    private static j f3671a = null;
    private SparseArray<h> b = new SparseArray<>(5);
    private List<b> c = new ArrayList(5);
    private e d;
    private Context e = null;
    private boolean f = true;
    private int g = 0;

    public static synchronized j a(Context context) {
        j jVar;
        synchronized (j.class) {
            if (f3671a == null) {
                f3671a = new j(context, true);
                g.h("rqdp{  create uphandler up:true}", new Object[0]);
            }
            jVar = f3671a;
        }
        return jVar;
    }

    public static synchronized j a(Context context, boolean z) {
        j jVar;
        synchronized (j.class) {
            if (f3671a == null) {
                f3671a = new j(context, z);
                g.h("rqdp{  create uphandler up:}%b", Boolean.valueOf(z));
            }
            if (f3671a.e() != z) {
                f3671a.a(z);
                g.h("rqdp{  change uphandler up:}%b", Boolean.valueOf(z));
            }
            jVar = f3671a;
        }
        return jVar;
    }

    private j(Context context, boolean z) {
        this.e = context.getApplicationContext();
        this.f = z;
        this.d = e.a(this.e);
    }

    public final synchronized boolean a(int i, h hVar) {
        boolean z;
        if (hVar == null) {
            z = false;
        } else {
            this.b.append(i, hVar);
            z = true;
        }
        return z;
    }

    public final synchronized boolean a(b bVar) {
        boolean z;
        if (bVar == null) {
            z = false;
        } else {
            if (!this.c.contains(bVar)) {
                this.c.add(bVar);
            }
            z = true;
        }
        return z;
    }

    private synchronized int a() {
        return this.g;
    }

    private synchronized void a(int i) {
        this.g = i;
    }

    public final void a(a aVar) {
        boolean z;
        if (!e()) {
            if (aVar.b() == 11) {
                g.h("rqdp{   Not UpProc real event sync 2 DB done false}", new Object[0]);
                aVar.a();
                aVar.a(false);
            }
            if (aVar.b != 1111) {
                g.h("rqdp{   Not UpProc not req }%d", Integer.valueOf(aVar.b()));
                return;
            }
            g.h("rqdp{   NotUpProc com req start}", new Object[0]);
        }
        if (!i.b(this.e)) {
            g.c("rqdp{  doUpload network is disabled!}", new Object[0]);
            if (aVar.b() == 11) {
                aVar.a(false);
                return;
            }
            return;
        }
        a(h.a(this.e));
        if (aVar == null) {
            g.d("rqdp{  upData == null?}", new Object[0]);
            return;
        }
        int b2 = aVar.b();
        long j = 0;
        long j2 = 0;
        boolean z2 = false;
        int i = -1;
        b[] b3 = b();
        if (b3 != null) {
            for (b a2 : b3) {
                a2.a(b2);
            }
        }
        String c2 = aVar.c();
        if (c2 == null) {
            g.d("rqdp{  url error}", new Object[0]);
            if (aVar.b() == 11) {
                aVar.a(false);
            }
            a(b2, -1, 0, 0, false, "url error");
            return;
        }
        g.h("rqdp{  start upload cmd:}%d rqdp{  url:}%s rqdp{  datas:}%s", Integer.valueOf(b2), c2, aVar.getClass().toString());
        byte[] b4 = b(aVar);
        if (b4 == null) {
            g.d("rqdp{  sData error}", new Object[0]);
            a(b2, -1, 0, 0, false, "sData error");
            return;
        }
        e c3 = c();
        if (c3 == null) {
            g.d("rqdp{  reqH error}", new Object[0]);
            a(b2, -1, 0, 0, false, "reqH error");
            return;
        }
        try {
            d dVar = new d();
            byte[] a3 = c3.a(c2, b4, dVar);
            if (a3 == null) {
                a(a() + 1);
            } else {
                a(0);
            }
            j = dVar.a();
            j2 = dVar.b();
            D a4 = a(a3);
            if (a4 != null) {
                i = a4.b;
                z2 = a4.f3704a == 0;
            }
            if (!(aVar == null || a4 == null)) {
                e a5 = e.a(this.e);
                if (a5 != null) {
                    a5.c(a4.d);
                    a5.a(a4.g - new Date().getTime());
                    g.h("rqdp{  fix ip:}%s rqdp{  tmgap:}%d", a5.k(), Long.valueOf(a5.l()));
                }
                byte[] bArr = a4.c;
                if (bArr != null) {
                    byte[] b5 = ac.b(bArr, a4.f, a4.e, ao.a(this.e).b().e());
                    SparseArray<h> d2 = d();
                    if (d2 != null && d2.size() > 0) {
                        int b6 = aVar.b();
                        int i2 = a4.b;
                        if (i2 != 0) {
                            switch (b6) {
                                case 8:
                                case 12:
                                    if (i2 != 11) {
                                        g.c("rqdp{  UNMATCH req:}%d rqdp{  , rep:}%d", Integer.valueOf(b6), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(d2, i2, b5);
                                    break;
                                case 15:
                                    if (i2 != 15) {
                                        g.c("rqdp{  UNMATCH req:}%d rqdp{  , rep:}%d", Integer.valueOf(b6), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(d2, i2, b5);
                                    break;
                                case 200:
                                    if (i2 != 305) {
                                        g.c("rqdp{  UNMATCH req:}%d rqdp{  , rep:}%d", Integer.valueOf(b6), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(d2, i2, b5);
                                    break;
                                case Constants.PLATFROM_ANDROID_PHONE /*201*/:
                                    if (i2 != 301) {
                                        g.c("rqdp{  UNMATCH req:}%d rqdp{  , rep:}%d", Integer.valueOf(b6), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(d2, i2, b5);
                                    break;
                                case Constants.PLATFROM_ANDROID_PAD /*202*/:
                                case 206:
                                    if (i2 != 302) {
                                        g.c("rqdp{  UNMATCH req:}%d rqdp{  , rep:}%d", Integer.valueOf(b6), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(d2, i2, b5);
                                    break;
                                case 203:
                                case 205:
                                    if (i2 != 303) {
                                        g.c("rqdp{  UNMATCH req:}%d rqdp{  , rep:}%d", Integer.valueOf(b6), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(d2, i2, b5);
                                    break;
                                case 204:
                                case 207:
                                    if (i2 != 304) {
                                        g.c("rqdp{  UNMATCH req:}%d rqdp{  , rep:}%d", Integer.valueOf(b6), Integer.valueOf(i2));
                                        break;
                                    }
                                    a(d2, i2, b5);
                                    break;
                                default:
                                    g.c("rqdp{  unknown req:}%d ", Integer.valueOf(b6));
                                    break;
                            }
                        } else {
                            g.h("rqdp{  response no datas}", new Object[0]);
                        }
                    } else {
                        g.h("rqdp{  no handler!}", new Object[0]);
                    }
                } else {
                    g.h("rqdp{  no response}", new Object[0]);
                }
            }
            a(b2, i, j, j2, z2, null);
            aVar.a(z2);
        } catch (Throwable th) {
            th = th;
            z2 = z;
            aVar.a(z2);
            throw th;
        }
    }

    private static byte[] b(a aVar) {
        if (aVar != null) {
            try {
                C a2 = aVar.a();
                if (a2 != null) {
                    g.b("rqdp{  [pid:}%s rqdp{  \nbid:}%s_%s rqdp{  \nsid:}%s_%s \n]", a2.b, a2.n, a2.c, a2.d, a2.e);
                    ae aeVar = new ae();
                    aeVar.d();
                    aeVar.a(1);
                    aeVar.d("test");
                    aeVar.e("test");
                    aeVar.a("detail", a2);
                    return aeVar.a();
                }
            } catch (Throwable th) {
                th.printStackTrace();
                a.d();
            }
        }
        return null;
    }

    private static D a(byte[] bArr) {
        if (bArr != null) {
            try {
                ae aeVar = new ae();
                aeVar.d();
                aeVar.a(bArr);
                Object b2 = aeVar.b("detail", new D());
                if (D.class.isInstance(b2)) {
                    g.b("rqdp{  covert to ResponsePackage}", new Object[0]);
                    return D.class.cast(b2);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return null;
    }

    private void a(int i, int i2, long j, long j2, boolean z, String str) {
        b[] b2 = b();
        if (b2 != null) {
            for (b a2 : b2) {
                a2.a(i, i2, j, j2, z, str);
            }
        }
    }

    private synchronized b[] b() {
        b[] bVarArr;
        if (this.c == null || this.c.size() <= 0) {
            bVarArr = null;
        } else {
            bVarArr = (b[]) this.c.toArray(new b[0]);
        }
        return bVarArr;
    }

    private synchronized e c() {
        return this.d;
    }

    private synchronized SparseArray<h> d() {
        SparseArray<h> sparseArray;
        if (this.b == null || this.b.size() <= 0) {
            sparseArray = null;
        } else {
            new n();
            sparseArray = n.a(this.b);
        }
        return sparseArray;
    }

    private static B b(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        try {
            B b2 = new B();
            b2.a(new ag(bArr));
            return b2;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    private boolean a(SparseArray<h> sparseArray, int i, byte[] bArr) {
        if (sparseArray == null || bArr == null) {
            return true;
        }
        switch (i) {
            case 304:
                B b2 = b(bArr);
                if (b2 == null) {
                    g.c("rqdp{  mix error}", new Object[0]);
                    return false;
                }
                Map<Integer, byte[]> map = b2.f3702a;
                if (map != null && map.size() > 0) {
                    if (map.containsKey(301)) {
                        a(sparseArray, 301, map.get(301));
                    }
                    if (map.containsKey(303)) {
                        a(sparseArray, 303, map.get(303));
                    }
                }
                return true;
            case STConstAction.ACTION_HIT_INSTALL:
                B b3 = b(bArr);
                if (b3 == null) {
                    g.c("rqdp{  mix error}", new Object[0]);
                    return false;
                }
                Map<Integer, byte[]> map2 = b3.f3702a;
                if (map2 != null && map2.size() > 0) {
                    for (Integer intValue : map2.keySet()) {
                        int intValue2 = intValue.intValue();
                        a(sparseArray, intValue2, map2.get(Integer.valueOf(intValue2)));
                    }
                }
                return true;
            default:
                h hVar = sparseArray.get(i);
                if (hVar == null) {
                    g.c("rqdp{  no handler key:}%d", Integer.valueOf(i));
                    return false;
                }
                try {
                    g.b("rqdp{  key:}%d rqdp{  handler: }%s", Integer.valueOf(i), hVar.getClass().toString());
                    hVar.a(i, bArr, true);
                    return true;
                } catch (Throwable th) {
                    th.printStackTrace();
                    g.d("rqdp{  handle error key:}%d", Integer.valueOf(i));
                    return false;
                }
        }
    }

    private synchronized boolean e() {
        return this.f;
    }

    private synchronized void a(boolean z) {
        this.f = z;
    }
}
