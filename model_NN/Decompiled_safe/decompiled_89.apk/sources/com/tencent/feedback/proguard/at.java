package com.tencent.feedback.proguard;

import android.util.SparseArray;
import com.tencent.feedback.common.n;

/* compiled from: ProGuard */
public final class at {

    /* renamed from: a  reason: collision with root package name */
    private SparseArray<au> f3732a;
    private String b;
    private int c;
    private int d;
    private String e;
    private String f;
    private int g;
    private int h;
    private boolean i;
    private boolean j;

    public at() {
        this.f3732a = null;
        this.b = "http://monitor.uu.qq.com/analytics/upload";
        this.c = -1;
        this.d = 6;
        this.e = "*^@K#K@!";
        this.f = "S(@L@L@)";
        this.g = 1;
        this.h = 1;
        this.i = true;
        this.j = false;
        this.f3732a = new SparseArray<>(5);
        this.f3732a.append(3, new au(3));
        this.f3732a = this.f3732a;
    }

    public final synchronized String a() {
        return this.b;
    }

    public final synchronized void a(String str) {
        this.b = str;
    }

    public final synchronized int b() {
        return this.c;
    }

    public final synchronized void a(int i2) {
        this.c = i2;
    }

    public final synchronized int c() {
        return this.d;
    }

    public final synchronized void b(int i2) {
        if (i2 > 0) {
            this.d = i2;
        }
    }

    public final synchronized String d() {
        return this.e;
    }

    public final synchronized void b(String str) {
        this.e = str;
    }

    public final synchronized String e() {
        return this.f;
    }

    public final synchronized void c(String str) {
        this.f = str;
    }

    public final synchronized int f() {
        return this.g;
    }

    public final synchronized void c(int i2) {
        this.g = i2;
    }

    public final synchronized int g() {
        return this.h;
    }

    public final synchronized void d(int i2) {
        this.h = i2;
    }

    public final synchronized boolean h() {
        return this.i;
    }

    public final synchronized void a(boolean z) {
        this.i = z;
    }

    public final synchronized SparseArray<au> i() {
        SparseArray<au> sparseArray;
        if (this.f3732a != null) {
            new n();
            sparseArray = n.a(this.f3732a);
        } else {
            sparseArray = null;
        }
        return sparseArray;
    }

    public final synchronized au e(int i2) {
        au auVar;
        if (this.f3732a != null) {
            auVar = this.f3732a.get(i2);
        } else {
            auVar = null;
        }
        return auVar;
    }

    public final synchronized boolean j() {
        return this.j;
    }

    public final synchronized void b(boolean z) {
        this.j = z;
    }
}
