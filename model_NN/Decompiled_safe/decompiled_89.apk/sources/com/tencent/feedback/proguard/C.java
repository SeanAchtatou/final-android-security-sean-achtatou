package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;

public final class C extends C0008j {
    private static byte[] r;

    /* renamed from: a  reason: collision with root package name */
    public byte f3703a = 0;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public int f = 0;
    public byte[] g = null;
    public String h = Constants.STR_EMPTY;
    public String i = Constants.STR_EMPTY;
    public byte j = 0;
    public byte k = 0;
    public String l = Constants.STR_EMPTY;
    public String m = Constants.STR_EMPTY;
    public String n = Constants.STR_EMPTY;
    public String o = Constants.STR_EMPTY;
    private byte p = 0;
    private String q = Constants.STR_EMPTY;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    public final void a(ag agVar) {
        this.f3703a = agVar.a(this.f3703a, 0, true);
        this.b = agVar.b(1, true);
        this.c = agVar.b(2, true);
        this.d = agVar.b(3, true);
        this.e = agVar.b(4, true);
        this.f = agVar.a(this.f, 5, true);
        if (r == null) {
            byte[] bArr = new byte[1];
            r = bArr;
            bArr[0] = 0;
        }
        byte[] bArr2 = r;
        this.g = agVar.c(6, true);
        this.h = agVar.b(7, false);
        this.i = agVar.b(8, false);
        this.j = agVar.a(this.j, 9, false);
        this.k = agVar.a(this.k, 10, false);
        this.l = agVar.b(11, false);
        this.m = agVar.b(12, false);
        this.n = agVar.b(13, false);
        this.p = agVar.a(this.p, 14, false);
        this.o = agVar.b(15, false);
        this.q = agVar.b(16, false);
    }

    public final void a(ah ahVar) {
        ahVar.a(this.f3703a, 0);
        ahVar.a(this.b, 1);
        ahVar.a(this.c, 2);
        ahVar.a(this.d, 3);
        ahVar.a(this.e, 4);
        ahVar.a(this.f, 5);
        ahVar.a(this.g, 6);
        if (this.h != null) {
            ahVar.a(this.h, 7);
        }
        if (this.i != null) {
            ahVar.a(this.i, 8);
        }
        ahVar.a(this.j, 9);
        ahVar.a(this.k, 10);
        if (this.l != null) {
            ahVar.a(this.l, 11);
        }
        if (this.m != null) {
            ahVar.a(this.m, 12);
        }
        if (this.n != null) {
            ahVar.a(this.n, 13);
        }
        ahVar.a(this.p, 14);
        if (this.o != null) {
            ahVar.a(this.o, 15);
        }
        if (this.q != null) {
            ahVar.a(this.q, 16);
        }
    }

    public final void a(StringBuilder sb, int i2) {
    }
}
