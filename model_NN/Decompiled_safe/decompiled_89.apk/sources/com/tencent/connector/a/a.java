package com.tencent.connector.a;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.qq.AppService.AppService;
import com.qq.AppService.t;
import com.qq.l.p;
import com.qq.m.b;
import com.qq.m.c;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.connect.common.Constants;
import com.tencent.wcs.jce.MInviteResponse;

/* compiled from: ProGuard */
public class a implements c {

    /* renamed from: a  reason: collision with root package name */
    private Context f3513a;
    private Handler b;
    private String c;
    private String d;
    private long e;
    private AppService.BusinessConnectionType f;

    public a(Context context, Handler handler, String str) {
        this.f3513a = context;
        this.b = handler;
        this.c = str;
        this.f = AppService.BusinessConnectionType.QRCODE;
    }

    public a(Context context, Handler handler, String str, long j) {
        this.f3513a = context;
        this.b = handler;
        this.d = str;
        this.e = j;
        this.f = AppService.BusinessConnectionType.QQ;
    }

    public void a() {
        if (!TextUtils.isEmpty(this.c)) {
            a(this.c);
        } else if (this.e > 0 && !TextUtils.isEmpty(this.d)) {
            a(this.d, this.e);
        }
    }

    public void b() {
        this.f3513a = null;
        this.b = null;
    }

    private void a(String str) {
        p.m().b(p.m().p().a(), 0, -1);
        TemporaryThreadManager.get().start(new b(this.f3513a, null, str, this));
    }

    private void a(String str, long j) {
        p.m().b(p.m().p().a(), 0, -1);
        TemporaryThreadManager.get().start(new b(this.f3513a, str, null, this, j + Constants.STR_EMPTY));
    }

    public void a(int i, int i2, MInviteResponse mInviteResponse) {
        if (i != 200 || mInviteResponse == null || mInviteResponse.f3871a == null || !mInviteResponse.f3871a.equals(AppService.l)) {
            com.tencent.assistant.st.a.a().b((byte) 2);
            if (i == 0) {
                p.m().c(p.m().p().a(), 2, 1);
            } else {
                p.m().c(p.m().p().a(), 2, 2);
            }
            t.a();
            if (this.b != null) {
                this.b.sendMessage(this.b.obtainMessage(10, this.f));
            }
        } else if (mInviteResponse.b == 3 || mInviteResponse.b == 6) {
            com.tencent.assistant.st.a.a().b((byte) 3);
            p.m().c(p.m().p().a(), 3, 4);
            t.a();
            if (this.b != null) {
                this.b.sendMessage(this.b.obtainMessage(11, this.f));
            }
        } else if (mInviteResponse.b == 0) {
            p.m().c(p.m().p().a(), 1, -1);
            if (this.b != null) {
                this.b.sendMessage(this.b.obtainMessage(12, this.f));
            }
        }
    }
}
