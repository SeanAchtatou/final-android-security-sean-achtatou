package com.tencent.connector.qrcode.a;

import android.hardware.Camera;
import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
public final class a implements Camera.AutoFocusCallback {

    /* renamed from: a  reason: collision with root package name */
    private Handler f3558a;
    private int b;

    /* access modifiers changed from: package-private */
    public void a(Handler handler, int i) {
        this.f3558a = handler;
        this.b = i;
    }

    public void onAutoFocus(boolean z, Camera camera) {
        if (this.f3558a != null) {
            Message obtainMessage = this.f3558a.obtainMessage(this.b, Boolean.valueOf(z));
            if (z) {
                this.f3558a.sendMessageDelayed(obtainMessage, 2000);
            } else {
                this.f3558a.sendMessageDelayed(obtainMessage, 4000);
            }
            this.f3558a = null;
        }
    }
}
