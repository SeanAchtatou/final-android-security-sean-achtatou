package com.tencent.connector.component;

import android.view.View;
import com.qq.AppService.AppService;

/* compiled from: ProGuard */
class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentVersionLowTip f3542a;

    m(ContentVersionLowTip contentVersionLowTip) {
        this.f3542a = contentVersionLowTip;
    }

    public void onClick(View view) {
        if (this.f3542a.i == 0) {
            if (this.f3542a.b != null) {
                AppService.BusinessConnectionType Q = AppService.Q();
                if (Q == AppService.BusinessConnectionType.QQ) {
                    this.f3542a.b.i();
                } else if (Q == AppService.BusinessConnectionType.QRCODE) {
                    this.f3542a.b.a(this.f3542a.pcName, this.f3542a.qrcode);
                } else {
                    this.f3542a.b.b();
                }
            }
        } else if (this.f3542a.i == 1 && this.f3542a.b != null) {
            this.f3542a.a();
        }
    }
}
