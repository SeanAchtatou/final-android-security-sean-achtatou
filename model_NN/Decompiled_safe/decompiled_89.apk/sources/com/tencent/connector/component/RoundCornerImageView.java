package com.tencent.connector.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.bf;

/* compiled from: ProGuard */
public class RoundCornerImageView extends TXImageView {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f3528a;
    private Paint b = new Paint();

    public RoundCornerImageView(Context context) {
        super(context);
    }

    public RoundCornerImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        this.f3528a = bf.a(drawable);
        invalidate();
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.f3528a = bitmap;
        invalidate();
    }

    public void setBackgroundResource(int i) {
        this.f3528a = bf.a(getContext().getResources().getDrawable(i));
        invalidate();
    }

    public void setImageResource(int i) {
        this.f3528a = bf.a(getContext().getResources().getDrawable(i));
        invalidate();
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f3528a != null) {
            canvas.drawBitmap(bf.a(this.f3528a, getWidth(), getHeight()), 0.0f, 0.0f, this.b);
        }
    }
}
