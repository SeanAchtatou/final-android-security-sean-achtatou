package com.tencent.connector.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.ConnectionActivity;

/* compiled from: ProGuard */
public class ContentConnectionException extends LinearLayout {
    public static final int EXP_NETWORK_LOST = 0;
    public static final int EXP_NOT_OUR_QRCODE = 3;
    public static final int EXP_NO_WIFI = 1;
    public static final int EXP_NO_WIFI_NO_PLUGIN = 2;
    public static final int EXP_PC_VERSION_TOO_LOW = 4;
    public static final int EXP_QRCODE_TIMEOUT = 5;

    /* renamed from: a  reason: collision with root package name */
    private Context f3520a;
    private ConnectionActivity b;
    private ImageView c;
    private TextView d;
    private TextView e;
    private Button f;
    private View.OnClickListener g;

    public ContentConnectionException(Context context) {
        super(context);
        this.f3520a = context;
    }

    public ContentConnectionException(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3520a = context;
    }

    public void setHostActivity(ConnectionActivity connectionActivity) {
        this.b = connectionActivity;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (ImageView) findViewById(R.id.exception_logo);
        this.d = (TextView) findViewById(R.id.exception_txt);
        this.e = (TextView) findViewById(R.id.exception_sub_txt);
        this.f = (Button) findViewById(R.id.action);
        this.g = new a(this);
        if (this.f != null) {
            this.f.setOnClickListener(this.g);
        }
    }

    private void a(int i) {
        if (this.d != null && i > 0) {
            this.d.setText(i);
        }
    }

    private void b(int i) {
        if (this.e == null) {
            return;
        }
        if (i > 0) {
            this.e.setVisibility(0);
            this.e.setText(i);
            return;
        }
        this.e.setVisibility(4);
    }

    private void c(int i) {
        if (this.c != null && i > 0) {
            this.c.setImageResource(i);
        }
    }

    private void d(int i) {
        if (this.f != null) {
            this.f.setText(i);
        }
    }

    public void display(int i) {
        this.f.setTag(Integer.valueOf(i));
        switch (i) {
            case 0:
                c((int) R.drawable.logo_network_error);
                a((int) R.string.exp_network_lost);
                b((int) R.string.exp_network_lost_tip);
                d(R.string.exp_network_lost_action);
                return;
            case 1:
            case 2:
                c((int) R.drawable.logo_network_error);
                a((int) R.string.exp_no_wifi);
                b(0);
                d(R.string.exp_no_wifi_action);
                return;
            case 3:
                c((int) R.drawable.logo_alert);
                a((int) R.string.exp_invalid_qrcode);
                b((int) R.string.exp_invalid_qrcode_tip_1);
                d(R.string.exp_invalid_qrcode_action);
                return;
            case 4:
                c((int) R.drawable.logo_alert);
                a((int) R.string.exp_invalid_qrcode);
                b((int) R.string.exp_invalid_qrcode_tip_2);
                d(R.string.ok);
                return;
            case 5:
                c((int) R.drawable.logo_alert);
                a((int) R.string.exp_timeout);
                b((int) R.string.exp_invalid_qrcode_tip_3);
                d(R.string.exp_invalid_qrcode_action);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.b != null) {
            this.b.f();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.b != null) {
            this.b.g();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.b != null) {
            this.b.b();
        }
    }
}
