package com.tencent.connector.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.ConnectionActivity;

/* compiled from: ProGuard */
public class ContentCurrentConnection extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f3524a;
    private ConnectionActivity b;
    private ImageView c;
    private TextView d;
    private TextView e;
    private Button f;
    private BannerQQStatus g;
    private View.OnClickListener h;
    private View i;

    public ContentCurrentConnection(Context context) {
        super(context);
        this.f3524a = context;
    }

    public ContentCurrentConnection(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3524a = context;
    }

    public void setHostActivity(ConnectionActivity connectionActivity) {
        this.b = connectionActivity;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (ImageView) findViewById(R.id.ic_connection_media);
        this.d = (TextView) findViewById(R.id.pc_name);
        this.e = (TextView) findViewById(R.id.connection_media);
        this.f = (Button) findViewById(R.id.disconnect);
        this.g = (BannerQQStatus) findViewById(R.id.qq_status);
        this.i = findViewById(R.id.qq_status_zone);
        this.h = new h(this);
        if (this.f != null) {
            this.f.setOnClickListener(this.h);
        }
    }

    public void displayUsbConnection(String str) {
        a(str, getResources().getString(R.string.label_usb_cable));
        a((int) R.drawable.logo_usb_on);
    }

    public void displayQrcodeConnection(String str) {
        a(str, getResources().getString(R.string.label_qrcode));
        a((int) R.drawable.logo_qrcode);
    }

    public void displayQQConnection(String str, String str2, String str3) {
        a(str, str2);
        a((int) R.drawable.logo_qrcode);
        a(str3);
    }

    private void a(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            str = this.f3524a.getString(R.string.tip_no_pc_name);
        }
        if (this.d != null) {
            this.d.setText(getResources().getString(R.string.label_connection_with_pc_done, str));
        }
        if (TextUtils.isEmpty(str2)) {
            str2 = this.f3524a.getString(R.string.tip_no_pc_name);
        }
        if (this.e != null) {
            this.e.setText(getResources().getString(R.string.label_connection_by, str2));
        }
    }

    private void a(int i2) {
        if (this.c != null) {
            this.c.setVisibility(0);
            this.c.setImageResource(i2);
        }
        if (this.i != null) {
            this.i.setVisibility(8);
        }
    }

    private void a(String str) {
        if (this.c != null) {
            this.c.setVisibility(8);
        }
        if (this.i != null) {
            this.i.setVisibility(0);
        }
        if (this.g != null) {
            this.g.updateWraper(R.drawable.banner_qq_status_login);
            this.g.updateBigQQImage(str);
            this.g.updateSmallQQImage(str);
        }
    }
}
