package com.tencent.assistant.manager;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.ah;
import com.tencent.assistant.module.callback.e;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.GetSmartCardsResponse;
import com.tencent.assistant.protocol.jce.SmartCardWrapper;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class k implements ah, e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f1545a;

    private k(h hVar) {
        this.f1545a = hVar;
    }

    /* synthetic */ k(h hVar, i iVar) {
        this(hVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.h.a(com.tencent.assistant.manager.h, boolean):boolean
     arg types: [com.tencent.assistant.manager.h, int]
     candidates:
      com.tencent.assistant.manager.h.a(com.tencent.assistant.manager.h, long):long
      com.tencent.assistant.manager.h.a(com.tencent.assistant.manager.h, com.tencent.assistant.manager.j):com.tencent.assistant.manager.j
      com.tencent.assistant.manager.h.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, int):java.util.List<com.tencent.assistant.model.e>
      com.tencent.assistant.manager.h.a(com.tencent.assistant.manager.h, boolean):boolean */
    public void a(int i, int i2, List<SmartCardWrapper> list, int i3, JceStruct jceStruct) {
        if (jceStruct != null) {
            GetSmartCardsResponse getSmartCardsResponse = (GetSmartCardsResponse) jceStruct;
            synchronized (this.f1545a) {
                this.f1545a.d.a(getSmartCardsResponse.b);
            }
        }
        boolean unused = this.f1545a.f = true;
        if (this.f1545a.g) {
            this.f1545a.a(this.f1545a.h);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.h.a(com.tencent.assistant.manager.h, boolean):boolean
     arg types: [com.tencent.assistant.manager.h, int]
     candidates:
      com.tencent.assistant.manager.h.a(com.tencent.assistant.manager.h, long):long
      com.tencent.assistant.manager.h.a(com.tencent.assistant.manager.h, com.tencent.assistant.manager.j):com.tencent.assistant.manager.j
      com.tencent.assistant.manager.h.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, int):java.util.List<com.tencent.assistant.model.e>
      com.tencent.assistant.manager.h.a(com.tencent.assistant.manager.h, boolean):boolean */
    public void a(int i, int i2) {
        boolean unused = this.f1545a.f = true;
        if (this.f1545a.g) {
            this.f1545a.a(this.f1545a.h);
        }
    }

    public void a(int i, int i2, boolean z, byte[] bArr, boolean z2, ArrayList<ColorCardItem> arrayList, List<SimpleAppModel> list) {
        long unused = this.f1545a.b = System.currentTimeMillis();
        j jVar = new j(this.f1545a, null);
        jVar.f1544a = i;
        jVar.b = i2;
        jVar.c = z;
        jVar.d = bArr;
        jVar.e = z2;
        jVar.f = arrayList;
        jVar.g = list;
        boolean unused2 = this.f1545a.g = true;
        if (!this.f1545a.f) {
            j unused3 = this.f1545a.h = jVar;
        } else {
            this.f1545a.a(jVar);
        }
    }
}
