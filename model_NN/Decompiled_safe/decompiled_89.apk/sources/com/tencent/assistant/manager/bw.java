package com.tencent.assistant.manager;

import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;

/* compiled from: ProGuard */
class bw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallTaskBean f1517a;
    final /* synthetic */ bo b;

    bw(bo boVar, InstallUninstallTaskBean installUninstallTaskBean) {
        this.b = boVar;
        this.f1517a = installUninstallTaskBean;
    }

    public void run() {
        this.b.h = false;
        this.b.f.remove(this.f1517a);
        if (this.b.f.size() == 0) {
            this.b.g = true;
            if (this.b.b != null) {
                this.b.b.postDelayed(new bx(this), 2000);
                return;
            }
            return;
        }
        this.b.a(this.b.f.get(this.b.f.size() - 1));
    }
}
