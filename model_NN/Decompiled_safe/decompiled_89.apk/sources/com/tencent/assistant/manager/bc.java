package com.tencent.assistant.manager;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.login.d;
import com.tencent.assistant.model.t;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class bc extends l implements UIEventListener {
    public static String e = "1101070898";
    public static String f = "openid";
    public static String g = Constants.PARAM_ACCESS_TOKEN;
    public static String h = "seq";
    private static bc i;
    private int j = -1;

    private bc() {
    }

    public static bc b() {
        if (i == null) {
            i = new bc();
        }
        return i;
    }

    public void c() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
    }

    public int a(Activity activity) {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
        d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
        this.j = a();
        return this.j;
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3, int i4) {
        t tVar = (t) this.c.remove(Integer.valueOf(i2));
        if (tVar != null) {
            Message obtainMessage = AstApp.i().j().obtainMessage();
            obtainMessage.what = EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_SUCCESS;
            obtainMessage.arg1 = i4;
            obtainMessage.arg2 = tVar.c;
            obtainMessage.obj = tVar;
            AstApp.i().j().sendMessage(obtainMessage);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3) {
        t tVar = (t) this.c.remove(Integer.valueOf(i2));
        if (tVar != null) {
            if (tVar.b > 3 || tVar.f1676a == null) {
                Message obtainMessage = AstApp.i().j().obtainMessage();
                obtainMessage.what = EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_FAIL;
                obtainMessage.obj = tVar;
                obtainMessage.arg2 = tVar.c;
                AstApp.i().j().sendMessage(obtainMessage);
                return;
            }
            int a2 = this.b.a(tVar.f1676a);
            tVar.b++;
            this.c.put(Integer.valueOf(a2), tVar);
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                if (message.arg1 == AppConst.LoginEgnineType.ENGINE_MOBILE_QQ.ordinal() && this.j != -1) {
                    Message obtainMessage = AstApp.i().j().obtainMessage();
                    obtainMessage.what = EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS;
                    d.a().c();
                    Bundle bundle = new Bundle();
                    bundle.putString("qqNumber", d.a().p() + Constants.STR_EMPTY);
                    obtainMessage.obj = bundle;
                    obtainMessage.arg2 = this.j;
                    AstApp.i().j().sendMessage(obtainMessage);
                    this.j = -1;
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
                if (message.arg1 == AppConst.LoginEgnineType.ENGINE_MOBILE_QQ.ordinal() && this.j != -1) {
                    Message obtainMessage2 = AstApp.i().j().obtainMessage();
                    obtainMessage2.what = EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL;
                    obtainMessage2.arg2 = this.j;
                    AstApp.i().j().sendMessage(obtainMessage2);
                    this.j = -1;
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL:
                if (message.arg1 == AppConst.LoginEgnineType.ENGINE_MOBILE_QQ.ordinal() && this.j != -1) {
                    Message obtainMessage3 = AstApp.i().j().obtainMessage();
                    obtainMessage3.what = EventDispatcherEnum.UI_EVENT_QQ_AUTH_CANCEL;
                    obtainMessage3.arg2 = this.j;
                    AstApp.i().j().sendMessage(obtainMessage3);
                    this.j = -1;
                    return;
                }
                return;
            default:
                return;
        }
    }
}
