package com.tencent.assistant.manager.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.ag;
import android.widget.RemoteViews;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.r;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class y {
    public static Notification a(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, PendingIntent pendingIntent, boolean z, boolean z2) {
        return a(context, R.drawable.notification_manual_icon, charSequence, charSequence2, charSequence3, System.currentTimeMillis(), pendingIntent, null, z, z2);
    }

    public static Notification a(Context context, int i, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, long j, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, boolean z2) {
        ag a2 = new ag(context).c(Constants.STR_EMPTY).b(z).a(z2).a(j);
        if (i != 0) {
            a2.a(i);
        }
        if (charSequence != null) {
            a2.a(charSequence);
        }
        if (charSequence2 != null) {
            a2.b(charSequence2);
        }
        if (pendingIntent != null) {
            a2.a(pendingIntent);
        }
        if (pendingIntent2 != null) {
            a2.b(pendingIntent2);
        }
        if (charSequence3 != null) {
            a2.d(charSequence3);
        }
        try {
            return a2.a();
        } catch (Throwable th) {
            return a(context, i, charSequence, charSequence2, null, charSequence3, j, pendingIntent, pendingIntent2, z, z2);
        }
    }

    public static Notification a(Context context, int i, RemoteViews remoteViews, CharSequence charSequence, long j, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, boolean z2) {
        ag a2 = new ag(context).c(Constants.STR_EMPTY).b(z).a(z2).a(j);
        if (remoteViews != null) {
            a2.a(remoteViews);
        }
        if (pendingIntent != null) {
            a2.a(pendingIntent);
        }
        if (pendingIntent2 != null) {
            a2.b(pendingIntent2);
        }
        if (i != 0) {
            a2.a(i);
        }
        if (charSequence != null) {
            a2.d(charSequence);
        }
        try {
            Notification a3 = a2.a();
            if (a3 == null || r.d() > 10) {
                return a3;
            }
            a3.contentView = remoteViews;
            return a3;
        } catch (Throwable th) {
            return a(context, i, null, null, remoteViews, charSequence, j, pendingIntent, pendingIntent2, z, z2);
        }
    }

    public static Notification a(Context context, int i, CharSequence charSequence, CharSequence charSequence2, RemoteViews remoteViews, CharSequence charSequence3, long j, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, boolean z2) {
        Notification notification = new Notification();
        notification.icon = i;
        notification.when = j;
        if (charSequence3 != null) {
            notification.tickerText = charSequence3;
        }
        notification.setLatestEventInfo(context, charSequence, charSequence2, pendingIntent);
        if (pendingIntent2 != null) {
            notification.deleteIntent = pendingIntent2;
        }
        if (remoteViews != null) {
            notification.contentView = remoteViews;
        }
        if (z) {
            notification.flags |= 16;
        }
        if (z2) {
            notification.flags |= 32;
        }
        return notification;
    }
}
