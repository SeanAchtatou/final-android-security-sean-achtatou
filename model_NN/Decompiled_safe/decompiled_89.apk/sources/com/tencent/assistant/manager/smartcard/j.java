package com.tencent.assistant.manager.smartcard;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.k;
import com.tencent.assistant.model.a.l;
import com.tencent.assistant.model.a.r;
import com.tencent.assistant.model.a.s;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class j extends y {
    public boolean a(i iVar, List<Long> list) {
        if (iVar == null || iVar.i != 5) {
            return false;
        }
        return a((k) iVar, (r) this.f1610a.get(Integer.valueOf(iVar.i)), (s) this.b.get(Integer.valueOf(iVar.i)), list);
    }

    private boolean a(k kVar, r rVar, s sVar, List<Long> list) {
        if (sVar == null) {
            return false;
        }
        kVar.a(list);
        if (rVar == null) {
            rVar = new r();
            rVar.f = kVar.j;
            rVar.e = kVar.i;
            this.f1610a.put(Integer.valueOf(rVar.e), rVar);
        }
        if (rVar.b >= sVar.b) {
            a(kVar.s, kVar.j + "||" + kVar.i + "|" + 1, kVar.i);
            return false;
        } else if (rVar.f1651a >= sVar.f1652a) {
            a(kVar.s, kVar.j + "||" + kVar.i + "|" + 2, kVar.i);
            return false;
        } else {
            a(kVar);
            if (kVar.c != null && kVar.c.size() >= sVar.g) {
                return true;
            }
            a(kVar.s, kVar.j + "||" + kVar.i + "|" + 3, kVar.i);
            return false;
        }
    }

    private void a(k kVar) {
        if (kVar != null && kVar.c != null && kVar.c.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (l next : kVar.c) {
                if (next.f1645a == null || ApkResourceManager.getInstance().getInstalledApkInfo(next.f1645a.c) != null) {
                    arrayList.add(next);
                }
            }
            kVar.c.removeAll(arrayList);
        }
    }

    public void a(i iVar) {
        if (iVar != null && iVar.i == 5) {
            k kVar = (k) iVar;
            s sVar = (s) this.b.get(Integer.valueOf(kVar.i));
            if (sVar != null) {
                kVar.p = sVar.d;
                kVar.d = sVar.g;
                kVar.e = kVar.c.size();
            }
        }
    }
}
