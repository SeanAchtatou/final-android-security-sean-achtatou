package com.tencent.assistant.manager.smartcard;

import android.util.SparseArray;
import com.tencent.assistant.db.table.aa;
import com.tencent.assistant.db.table.ab;
import com.tencent.assistant.db.table.ac;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.a;
import com.tencent.assistant.model.a.c;
import com.tencent.assistant.model.a.d;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.m;
import com.tencent.assistant.model.a.n;
import com.tencent.assistant.model.a.p;
import com.tencent.assistant.model.a.q;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.model.a.v;
import com.tencent.assistant.protocol.jce.SmartCardCfg;
import com.tencent.assistant.protocol.jce.SmartCardCfgList;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.cv;
import java.util.List;

/* compiled from: ProGuard */
public class o implements z {

    /* renamed from: a  reason: collision with root package name */
    private static o f1594a;
    /* access modifiers changed from: private */
    public SparseArray<y> b = new SparseArray<>();
    private aa c = new aa();
    /* access modifiers changed from: private */
    public ac d = new ac();
    /* access modifiers changed from: private */
    public ab e = new ab();

    public static synchronized o a() {
        o oVar;
        synchronized (o.class) {
            if (f1594a == null) {
                f1594a = new o();
            }
            oVar = f1594a;
        }
        return oVar;
    }

    public o() {
        c();
    }

    private void c() {
        TemporaryThreadManager.get().start(new p(this));
    }

    /* access modifiers changed from: private */
    public void d() {
        this.b.put(0, new ac());
        this.b.put(4, new m());
        this.b.put(1, new i());
        this.b.put(2, new g());
        this.b.put(3, new aa());
        this.b.put(6, new n());
        this.b.put(5, new j());
        this.b.put(7, new e());
        this.b.put(8, new h());
        ab abVar = new ab();
        this.b.put(9, abVar);
        this.b.put(10, abVar);
        this.b.put(11, abVar);
        this.b.put(12, abVar);
        this.b.put(13, abVar);
        this.b.put(14, abVar);
        this.b.put(15, abVar);
        this.b.put(27, abVar);
        this.b.put(16, new a());
        this.b.put(17, new l());
        this.b.put(23, new k());
        this.b.put(24, new d());
        this.b.put(22, new f());
        b.a(this.b);
    }

    /* access modifiers changed from: private */
    public void e() {
        y d2;
        List<s> a2 = this.c.a();
        if (a2 != null && a2.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    s sVar = a2.get(i2);
                    y yVar = this.b.get(sVar.e);
                    if (yVar != null) {
                        yVar.a(sVar);
                    } else {
                        c a3 = b.a(sVar.e);
                        if (!(a3 == null || (d2 = a3.d()) == null)) {
                            d2.a(sVar);
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        y d2;
        List<q> a2 = this.d.a();
        if (a2 != null && a2.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    q qVar = a2.get(i2);
                    y yVar = this.b.get(qVar.f1650a);
                    if (yVar != null) {
                        yVar.a(qVar);
                    } else {
                        c a3 = b.a(qVar.f1650a);
                        if (!(a3 == null || (d2 = a3.d()) == null)) {
                            d2.a(qVar);
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void b() {
        y d2;
        List<p> a2 = this.e.a();
        if (a2 != null && a2.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    p pVar = a2.get(i2);
                    if (pVar != null) {
                        y yVar = this.b.get(pVar.f1649a);
                        if (yVar != null) {
                            yVar.a(pVar);
                        } else {
                            c a3 = b.a(pVar.f1649a);
                            if (!(a3 == null || (d2 = a3.d()) == null)) {
                                d2.a(pVar);
                            }
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        this.d.a(cv.b(30));
        this.e.a(cv.b(30));
    }

    public i a(List<i> list, List<Long> list2, int i) {
        y d2;
        if (list == null || list.size() <= 0) {
            return null;
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return null;
            }
            i iVar = list.get(i3);
            if (iVar.i == 0) {
                b(iVar);
            } else if (iVar.i == 16) {
                c(iVar);
            } else if (iVar.i == 17) {
                d(iVar);
            } else if (iVar.i == 23) {
                e(iVar);
            } else if (iVar.i == 24) {
                f(iVar);
            }
            if (iVar instanceof a) {
                s b2 = ((a) iVar).b();
                iVar.a(list2);
                ((a) iVar).c();
                c a2 = b.a(iVar.i);
                if (!(a2 == null || (d2 = a2.d()) == null)) {
                    if (b2 != null) {
                        TemporaryThreadManager.get().start(new q(this, d2, b2));
                    }
                    if (d2.a(iVar, list2)) {
                        iVar.a(i);
                        return iVar;
                    }
                }
            }
            y yVar = this.b.get(iVar.i);
            if (yVar != null && yVar.a(iVar, list2)) {
                return iVar;
            }
            i2 = i3 + 1;
        }
    }

    public void a(i iVar) {
        y yVar;
        if (iVar != null && (yVar = this.b.get(iVar.i)) != null) {
            yVar.a(iVar);
        }
    }

    public boolean b(i iVar) {
        if (iVar == null || !(iVar instanceof v)) {
            return false;
        }
        v vVar = (v) iVar;
        s sVar = new s();
        sVar.e = vVar.i;
        sVar.f = vVar.j;
        sVar.f1652a = vVar.d;
        sVar.c = vVar.e;
        sVar.d = vVar.b;
        sVar.h = (long) vVar.c;
        y yVar = this.b.get(sVar.e);
        if (yVar != null) {
            yVar.a(sVar);
        }
        TemporaryThreadManager.get().start(new r(this, sVar));
        return true;
    }

    public boolean c(i iVar) {
        if (iVar == null || !(iVar instanceof c)) {
            return false;
        }
        c cVar = (c) iVar;
        s sVar = new s();
        sVar.e = cVar.i;
        sVar.f = cVar.j;
        sVar.f1652a = cVar.e;
        sVar.c = cVar.f;
        sVar.d = cVar.c;
        sVar.h = (long) cVar.d;
        y yVar = this.b.get(sVar.e);
        if (yVar != null) {
            yVar.a(sVar);
        }
        TemporaryThreadManager.get().start(new s(this, sVar));
        return true;
    }

    public boolean d(i iVar) {
        if (iVar == null || !(iVar instanceof n)) {
            return false;
        }
        n nVar = (n) iVar;
        s sVar = new s();
        sVar.e = nVar.i;
        sVar.f = nVar.j;
        sVar.f1652a = nVar.g;
        sVar.c = nVar.h;
        sVar.d = nVar.e;
        sVar.h = (long) nVar.f;
        y yVar = this.b.get(sVar.e);
        if (yVar != null) {
            yVar.a(sVar);
        }
        TemporaryThreadManager.get().start(new t(this, sVar));
        return true;
    }

    public boolean e(i iVar) {
        if (iVar == null || !(iVar instanceof m)) {
            return false;
        }
        m mVar = (m) iVar;
        s sVar = new s();
        sVar.e = mVar.i;
        sVar.f = mVar.j;
        sVar.d = mVar.b;
        sVar.h = mVar.c;
        y yVar = this.b.get(sVar.e);
        if (yVar != null) {
            yVar.a(sVar);
        }
        TemporaryThreadManager.get().start(new u(this, sVar));
        return true;
    }

    public boolean f(i iVar) {
        if (iVar == null || !(iVar instanceof d)) {
            return false;
        }
        d dVar = (d) iVar;
        s sVar = new s();
        sVar.e = dVar.i;
        sVar.f = dVar.j;
        sVar.f1652a = dVar.e;
        sVar.c = dVar.f;
        sVar.d = dVar.c;
        sVar.h = dVar.d;
        TemporaryThreadManager.get().start(new v(this, sVar));
        return true;
    }

    public boolean a(SmartCardCfgList smartCardCfgList) {
        int i = 0;
        if (smartCardCfgList == null || smartCardCfgList.f2317a == null || smartCardCfgList.f2317a.size() <= 0) {
            return false;
        }
        while (true) {
            int i2 = i;
            if (i2 >= smartCardCfgList.f2317a.size()) {
                return true;
            }
            SmartCardCfg smartCardCfg = smartCardCfgList.f2317a.get(i2);
            if (smartCardCfg != null) {
                s sVar = new s();
                sVar.a(smartCardCfg);
                y yVar = this.b.get(sVar.e);
                if (yVar != null) {
                    yVar.a(sVar);
                }
                a(sVar);
            }
            i = i2 + 1;
        }
    }

    public boolean a(s sVar) {
        return this.c.a(sVar) > 0;
    }

    public void a(int i, int i2) {
        q qVar = new q();
        qVar.b = i2;
        qVar.f1650a = i;
        qVar.c = true;
        qVar.e = System.currentTimeMillis() / 1000;
        y yVar = this.b.get(qVar.f1650a);
        if (yVar != null) {
            yVar.a(qVar);
        }
        a(qVar);
    }

    public void a(int i, int i2, SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null) {
            p pVar = new p();
            pVar.b = i2;
            pVar.f1649a = i;
            pVar.c = simpleAppModel.c;
            pVar.d = simpleAppModel.g;
            pVar.e = simpleAppModel.ad;
            pVar.f = System.currentTimeMillis() / 1000;
            y yVar = this.b.get(pVar.f1649a);
            if (yVar != null) {
                yVar.a(pVar);
            }
            a(pVar);
        }
    }

    public void b(int i, int i2) {
        q qVar = new q();
        qVar.b = i2;
        qVar.f1650a = i;
        qVar.d = true;
        qVar.e = System.currentTimeMillis() / 1000;
        y yVar = this.b.get(qVar.f1650a);
        if (yVar != null) {
            yVar.a(qVar);
        }
        a(qVar);
    }

    public void a(q qVar) {
        TemporaryThreadManager.get().start(new w(this, qVar));
    }

    public void a(p pVar) {
        TemporaryThreadManager.get().start(new x(this, pVar));
    }
}
