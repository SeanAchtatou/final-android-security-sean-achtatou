package com.tencent.assistant.manager.smartcard.view;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.manager.smartcard.b.a;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class c extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1606a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardGiftNode c;

    c(NormalSmartCardGiftNode normalSmartCardGiftNode, a aVar, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardGiftNode;
        this.f1606a = aVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        b.a(this.c.f1602a, this.f1606a.f1589a.aa);
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.updateStatusToDetail(this.f1606a.f1589a);
            this.b.recommendId = this.f1606a.f1589a.y;
        }
        return this.b;
    }
}
