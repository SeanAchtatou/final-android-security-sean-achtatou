package com.tencent.assistant.manager.notification;

import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class u implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static u f1580a = null;

    private u() {
        c();
    }

    public static synchronized u a() {
        u uVar;
        synchronized (u.class) {
            if (f1580a == null) {
                f1580a = new u();
            }
            uVar = f1580a;
        }
        return uVar;
    }

    public void b() {
    }

    private void c() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        AstApp.i().k().addUIEventListener(1002, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(1007, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(1010, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        AstApp.i().k().addUIEventListener(1016, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_CHECKUPDATE_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        AstApp.i().k().addUIEventListener(1023, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(1027, this);
        AstApp.i().k().addUIEventListener(1032, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FINISH, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PUSH_MESSAGE_NEW, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, long):void
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification):void
      com.tencent.assistant.manager.notification.v.a(int, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.manager.notification.v.a(boolean, int):void
      com.tencent.assistant.manager.notification.v.a(java.lang.String, boolean):void */
    public void handleUIEvent(Message message) {
        String str;
        InstallUninstallTaskBean installUninstallTaskBean;
        DownloadInfo d;
        DownloadInfo downloadInfo;
        switch (message.what) {
            case 1002:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING:
                if (c.a()) {
                    v.a().a(true);
                    v.a().b(false, -1);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
                DownloadInfo a2 = a(message);
                int percent = SimpleDownloadInfo.getPercent(a2);
                if (a2 != null && v.a().c(percent)) {
                    v.a().a(true, false, percent);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE:
                DownloadInfo a3 = a(message);
                if (a3 != null) {
                    v.a().a(true, false, SimpleDownloadInfo.getPercent(a3));
                }
                v.a().a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                DownloadInfo a4 = a(message);
                if (a4 != null) {
                    v.a().a(true, false, SimpleDownloadInfo.getPercent(a4));
                    return;
                }
                return;
            case 1007:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL:
                if (!c.a()) {
                    v.a().b(true, DownloadProxy.a().f());
                }
                v.a().a(false);
                DownloadInfo a5 = a(message);
                if (a5 != null && SimpleDownloadInfo.getPercent(a5) < 100) {
                    v.a().a(true, false, SimpleDownloadInfo.getPercent(a5));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE:
                v.a().a(false);
                v.a().b(false, DownloadProxy.a().f());
                if ((message.obj instanceof DownloadInfo) && (downloadInfo = (DownloadInfo) message.obj) != null && !TextUtils.isEmpty(downloadInfo.downloadTicket) && !TextUtils.isEmpty(downloadInfo.packageName) && downloadInfo.packageName.equals(AstApp.i().getPackageName())) {
                    v.a().a(false, false, 0);
                    return;
                }
                return;
            case 1010:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC:
                v.a().a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
                if (!m.a().j() && (str = (String) message.obj) != null && !TextUtils.isEmpty(str)) {
                    h.a().b(str);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL:
            default:
                return;
            case 1023:
                if (!m.a().j() && (installUninstallTaskBean = (InstallUninstallTaskBean) message.obj) != null && !TextUtils.isEmpty(installUninstallTaskBean.packageName) && (d = DownloadProxy.a().d(installUninstallTaskBean.downloadTicket)) != null && !TextUtils.isEmpty(d.downloadTicket) && !TextUtils.isEmpty(d.packageName) && !d.packageName.equals(AstApp.i().getPackageName()) && !d.isSllUpdateApk() && !d.isUpdateApk()) {
                    h.a().a(d.packageName, d.name, (int) d.appId, d.iconUrl);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean2 = (InstallUninstallTaskBean) message.obj;
                    if (installUninstallTaskBean2.promptForInstallUninstall) {
                        v.a().a(installUninstallTaskBean2.appName, true);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean3 = (InstallUninstallTaskBean) message.obj;
                    if (installUninstallTaskBean3.promptForInstallUninstall) {
                        v.a().a(Constants.STR_EMPTY, false);
                        v.a().a(installUninstallTaskBean3.packageName, installUninstallTaskBean3.appName);
                        v.a().b(true);
                        return;
                    }
                    return;
                }
                return;
            case 1027:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    InstallUninstallTaskBean installUninstallTaskBean4 = (InstallUninstallTaskBean) message.obj;
                    if (installUninstallTaskBean4.promptForInstallUninstall) {
                        v.a().a(installUninstallTaskBean4.appName, false);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_GOFRONT:
                v.a().a(112);
                return;
            case EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_START:
                v.a().b(0);
                return;
            case EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FAILED:
                v.a().b(-1);
                return;
            case EventDispatcherEnum.UI_EVENT_PHOTOBACKUP_FINISH:
                v.a().b(1);
                return;
            case EventDispatcherEnum.UI_EVENT_PUSH_MESSAGE_NEW:
                v.a().c(true);
                return;
        }
    }

    private DownloadInfo a(Message message) {
        String str = Constants.STR_EMPTY;
        if (message.obj instanceof String) {
            str = (String) message.obj;
        }
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        DownloadInfo d = DownloadProxy.a().d(str);
        if (d == null || TextUtils.isEmpty(d.packageName) || !d.packageName.equals(AstApp.i().getPackageName()) || d.isUiTypeWiseDownload()) {
            return null;
        }
        if (message.what != 1006 || d.response == null) {
            return d;
        }
        d.response.f1263a = d.response.b;
        return d;
    }
}
