package com.tencent.assistant.manager.notification.a;

import android.graphics.Bitmap;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class e implements com.tencent.assistant.manager.notification.a.a.e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1556a;

    e(d dVar) {
        this.f1556a = dVar;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f1556a.i.setImageViewBitmap(R.id.big_icon, bitmap);
        }
    }
}
