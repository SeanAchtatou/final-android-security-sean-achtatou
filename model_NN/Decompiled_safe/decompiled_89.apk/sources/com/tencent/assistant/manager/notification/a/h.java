package com.tencent.assistant.manager.notification.a;

import android.text.Html;
import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.ActionLink;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.f;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
public class h extends d {
    public h(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (super.c() && !TextUtils.isEmpty(this.c.c) && this.c.l != null && !TextUtils.isEmpty(this.c.l.b) && this.c.l.d != null && this.c.l.d.size() > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        a((int) R.layout.notification_card_1);
        if (this.i == null) {
            return false;
        }
        i();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        ActionLink l = l();
        if (l != null) {
            this.j = b(R.layout.notification_card1_right2);
            this.j.setTextViewText(R.id.rightButton, Html.fromHtml(l.b()));
            this.e = l.a();
            this.j.setOnClickPendingIntent(R.id.rightButton, k());
            this.i.removeAllViews(R.id.rightContainer);
            this.i.addView(R.id.rightContainer, this.j);
            this.i.setViewVisibility(R.id.rightContainer, 0);
        }
        if (this.j != null) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public ActionLink l() {
        if (this.c == null || this.c.l == null) {
            return null;
        }
        Map<Byte, ActionLink> c = this.c.l.c();
        String a2 = this.c.l.a();
        int b = this.c.l.b();
        if (c == null || TextUtils.isEmpty(a2)) {
            return null;
        }
        int a3 = f.a(a2, b);
        a(a3, a2);
        ActionLink actionLink = c.get(Byte.valueOf((byte) a3));
        if (actionLink == null || TextUtils.isEmpty(actionLink.b()) || actionLink.a() == null) {
            return null;
        }
        return actionLink;
    }

    /* access modifiers changed from: protected */
    public void a(int i, String str) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
                this.f = 2;
                this.h = new ArrayList();
                this.h.add(str);
                break;
            case 4:
                this.f = 9;
                break;
        }
        this.g = e();
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return true;
    }
}
