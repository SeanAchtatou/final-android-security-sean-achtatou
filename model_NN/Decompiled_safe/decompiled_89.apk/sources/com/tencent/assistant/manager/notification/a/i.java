package com.tencent.assistant.manager.notification.a;

import android.text.TextUtils;
import android.widget.RemoteViews;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.notification.a.a.f;
import com.tencent.assistant.protocol.jce.PushIconInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import java.util.List;

/* compiled from: ProGuard */
public class i extends d {
    protected static final int[] q = {R.id.icon1, R.id.icon2, R.id.icon3, R.id.icon4, R.id.icon5};
    protected List<PushIconInfo> p = null;

    public i(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
        if (pushInfo != null) {
            this.p = pushInfo.e;
        }
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (super.c() && this.p != null && this.p.size() > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        a((int) R.layout.notification_card_3);
        if (this.i == null) {
            return false;
        }
        a(this.i, this.p, q);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void a(RemoteViews remoteViews, List<PushIconInfo> list, int[] iArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < iArr.length && i2 < list.size()) {
                PushIconInfo pushIconInfo = list.get(i2);
                if (pushIconInfo != null && !TextUtils.isEmpty(pushIconInfo.b)) {
                    f fVar = new f(pushIconInfo);
                    fVar.a(new j(this, remoteViews, iArr, i2));
                    a(fVar);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
