package com.tencent.assistant.manager.notification;

import android.app.PendingIntent;
import android.graphics.Bitmap;
import com.tencent.assistant.manager.notification.a.a.e;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
class k implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f1570a;
    final /* synthetic */ CharSequence b;
    final /* synthetic */ PendingIntent c;
    final /* synthetic */ PendingIntent d;
    final /* synthetic */ j e;

    k(j jVar, n nVar, CharSequence charSequence, PendingIntent pendingIntent, PendingIntent pendingIntent2) {
        this.e = jVar;
        this.f1570a = nVar;
        this.b = charSequence;
        this.c = pendingIntent;
        this.d = pendingIntent2;
    }

    public void a(Bitmap bitmap) {
        ba.a().post(new l(this, bitmap));
    }
}
