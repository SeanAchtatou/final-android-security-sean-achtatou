package com.tencent.assistant.manager.smartcard.a;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem;
import com.tencent.assistant.component.smartcard.SmartcardListener;
import com.tencent.assistant.manager.smartcard.b.d;
import com.tencent.assistant.manager.smartcard.c;
import com.tencent.assistant.manager.smartcard.view.NormalSmartCardInterestItem;
import com.tencent.assistant.manager.smartcard.y;
import com.tencent.assistant.model.a.a;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.protocol.jce.SmartCardPicTemplate;

/* compiled from: ProGuard */
public class b extends c {
    public Class<? extends JceStruct> a() {
        return SmartCardPicTemplate.class;
    }

    public a b() {
        return new d();
    }

    /* access modifiers changed from: protected */
    public y c() {
        return new com.tencent.assistant.manager.smartcard.a.a.b();
    }

    public NormalSmartcardBaseItem a(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        return new NormalSmartCardInterestItem(context, iVar, smartcardListener, iViewInvalidater);
    }
}
