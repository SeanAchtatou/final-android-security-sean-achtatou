package com.tencent.assistant.manager;

import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.downloadsdk.DownloadManager;
import java.util.Iterator;

/* compiled from: ProGuard */
class ac implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f1474a;

    ac(DownloadProxy downloadProxy) {
        this.f1474a = downloadProxy;
    }

    public void run() {
        Iterator<DownloadInfo> it = this.f1474a.c().iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING || next.downloadState == SimpleDownloadInfo.DownloadState.QUEUING) {
                next.downloadState = SimpleDownloadInfo.DownloadState.FAIL;
                DownloadManager.a().a(next.getDownloadSubType(), next.downloadTicket);
                this.f1474a.a(next, SimpleDownloadInfo.DownloadState.FAIL);
                this.f1474a.e.a(next);
            }
        }
    }
}
