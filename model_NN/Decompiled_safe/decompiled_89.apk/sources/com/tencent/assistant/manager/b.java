package com.tencent.assistant.manager;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Looper;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.sdk.c.a;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.st.k;
import com.tencent.connect.common.Constants;
import com.tencent.e.a.a.c;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public static b f1496a = null;
    private static List<String> k = new ArrayList();
    UIEventListener b = new c(this);
    private final String c = "AppDetailActionManager";
    private List<String> d = Collections.synchronizedList(new ArrayList());
    private List<String> e = Collections.synchronizedList(new ArrayList());
    private Map<String, c> f = Collections.synchronizedMap(new HashMap());
    /* access modifiers changed from: private */
    public boolean g = false;
    private boolean h = false;
    private int i = 1;
    private int j = 0;

    static {
        k.add("991255");
    }

    public static b a() {
        if (f1496a == null) {
            f1496a = new b();
            f1496a.i();
        }
        return f1496a;
    }

    private b() {
    }

    public void b() {
        if (!this.h) {
            this.h = true;
            try {
                e();
            } catch (Exception e2) {
            }
            if (this.d.size() == 0) {
                f();
            }
            this.h = false;
            d();
        }
    }

    public boolean c() {
        List<c> g2 = g();
        if (g2 != null && g2.size() > 0) {
            return true;
        }
        Cursor query = AstApp.i().getApplicationContext().getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/pref"), null, null, null, null);
        if (query != null) {
            String str = Constants.STR_EMPTY;
            try {
                if (query.getCount() > 0) {
                    query.moveToFirst();
                    int columnIndex = query.getColumnIndex("data");
                    if (columnIndex != -1) {
                        str = query.getString(columnIndex);
                    }
                }
                query.close();
                JSONObject jSONObject = new JSONObject(str);
                String string = jSONObject.getString("starttime");
                String string2 = jSONObject.getString("endtime");
                String string3 = jSONObject.getString(SocialConstants.PARAM_URL);
                long parseLong = Long.parseLong(string);
                long parseLong2 = Long.parseLong(string2);
                long currentTimeMillis = System.currentTimeMillis();
                if (parseLong < currentTimeMillis && parseLong2 > currentTimeMillis && !TextUtils.isEmpty(string3)) {
                    return true;
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return false;
    }

    private void d() {
        if (this.d.size() > 0) {
            String remove = this.d.remove(0);
            this.e.add(remove);
            Context m = AstApp.m();
            if (m == null) {
                m = AstApp.i().getApplicationContext();
            }
            com.tencent.assistant.link.b.c(m, remove);
        }
    }

    private void e() {
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/pref");
        Cursor query = AstApp.i().getApplicationContext().getContentResolver().query(parse, null, null, null, null);
        if (query != null) {
            String str = Constants.STR_EMPTY;
            if (query.getCount() > 0) {
                query.moveToFirst();
                int columnIndex = query.getColumnIndex("data");
                if (columnIndex != -1) {
                    str = query.getString(columnIndex);
                }
            }
            query.close();
            try {
                JSONObject jSONObject = new JSONObject(str);
                String string = jSONObject.getString("starttime");
                String string2 = jSONObject.getString("endtime");
                String string3 = jSONObject.getString(SocialConstants.PARAM_URL);
                long parseLong = Long.parseLong(string);
                long parseLong2 = Long.parseLong(string2);
                long currentTimeMillis = System.currentTimeMillis();
                if (parseLong < currentTimeMillis && parseLong2 > currentTimeMillis && !TextUtils.isEmpty(string3)) {
                    this.d.add(string3);
                }
                AstApp.i().getApplicationContext().getContentResolver().delete(parse, null, null);
            } catch (JSONException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    private void f() {
        try {
            List<c> g2 = g();
            if (g2 != null && g2.size() > 0) {
                c cVar = g2.get(g2.size() - 1);
                if (cVar.i == 0) {
                    this.d.add(cVar.f);
                    this.f.put(cVar.f, cVar);
                    a(cVar, 0);
                } else {
                    try {
                        if (Looper.myLooper() == null) {
                            Looper.prepare();
                        }
                    } catch (Throwable th) {
                    }
                    a.a(cVar.j, cVar.b);
                    a(cVar, 14);
                }
            }
            a(g2);
        } catch (Exception e2) {
        }
    }

    private void a(c cVar, int i2) {
        Uri parse;
        String str = "traceid";
        String str2 = "_" + i2;
        if (!(cVar == null || TextUtils.isEmpty(cVar.f) || (parse = Uri.parse(cVar.f)) == null)) {
            String a2 = com.tencent.e.a.b.a.a(parse.getQueryParameter(com.tencent.assistant.b.a.F));
            if (!TextUtils.isEmpty(a2)) {
                Uri parse2 = Uri.parse(a2);
                str2 = h.a(cVar.b, String.valueOf(cVar.c), parse2.getQueryParameter("channelid"), parse2.getQueryParameter("via"), parse2.getQueryParameter("uin"), parse2.getQueryParameter("appid"), parse2.getQueryParameter("extradata"), parse2.getQueryParameter("pname"), parse2.getQueryParameter("versioncode")) + "_" + i2;
                str = parse2.getQueryParameter("traceid");
            }
        }
        k.a("StatSdcard", str, str2);
    }

    /* access modifiers changed from: private */
    public List<c> g() {
        try {
            ArrayList<c> a2 = new com.tencent.e.a.a.b().a();
            if (a2 == null || a2.size() == 0) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            long currentTimeMillis = System.currentTimeMillis();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= a2.size()) {
                    return arrayList;
                }
                c cVar = a2.get(i3);
                if (cVar.g <= currentTimeMillis && cVar.h >= currentTimeMillis && cVar.i <= this.i && cVar.e == this.j) {
                    arrayList.add(cVar);
                }
                i2 = i3 + 1;
            }
        } catch (Throwable th) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void a(List<c> list) {
        if (list != null) {
            try {
                if (list.size() != 0) {
                    com.tencent.e.a.a.b bVar = new com.tencent.e.a.a.b();
                    int i2 = 0;
                    while (true) {
                        int i3 = i2;
                        if (i3 < list.size()) {
                            bVar.a((long) list.get(i3).f3662a);
                            i2 = i3 + 1;
                        } else {
                            return;
                        }
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        try {
            AstApp.i().getApplicationContext().getContentResolver().delete(Uri.parse("content://com.tencent.mm.sdk.comm.provider/pref"), null, null);
        } catch (Exception e2) {
        }
    }

    private void i() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this.b);
        AstApp.i().k().addUIEventListener(1032, this.b);
    }

    private void b(String str) {
        if (this.d.size() > 0 && this.d.contains(str)) {
            this.d.remove(str);
        }
    }

    public void a(String str) {
        if (!this.e.contains(str)) {
            this.e.add(str);
            TemporaryThreadManager.get().start(new e(this));
        }
        b(str);
    }
}
