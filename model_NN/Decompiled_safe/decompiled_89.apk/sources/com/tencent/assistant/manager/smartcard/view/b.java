package com.tencent.assistant.manager.smartcard.view;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class b extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f1605a;
    final /* synthetic */ NormalSmartCardGiftNode b;

    b(NormalSmartCardGiftNode normalSmartCardGiftNode, STInfoV2 sTInfoV2) {
        this.b = normalSmartCardGiftNode;
        this.f1605a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.b.a(this.b.m.f1589a, this.f1605a);
    }

    public STInfoV2 getStInfo() {
        if (this.f1605a == null || !(this.f1605a instanceof STInfoV2)) {
            return null;
        }
        this.f1605a.updateStatus(this.b.m.f1589a);
        return this.f1605a;
    }
}
