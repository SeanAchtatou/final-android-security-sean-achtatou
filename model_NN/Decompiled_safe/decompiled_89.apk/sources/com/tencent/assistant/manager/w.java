package com.tencent.assistant.manager;

import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager;

/* compiled from: ProGuard */
class w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f1624a;

    w(DownloadProxy downloadProxy) {
        this.f1624a = downloadProxy;
    }

    public void run() {
        InstallUninstallDialogManager unused = this.f1624a.k = new InstallUninstallDialogManager();
        this.f1624a.k.a(true);
        bo.a();
        this.f1624a.b();
        this.f1624a.b = new DownloadProxy.DownloadTaskQueue(this.f1624a, null);
        Thread thread = new Thread(this.f1624a.b);
        thread.setName("Thread_DownloadProxy");
        thread.start();
    }
}
