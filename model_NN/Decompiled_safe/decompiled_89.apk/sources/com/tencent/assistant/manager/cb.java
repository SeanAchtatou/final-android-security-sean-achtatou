package com.tencent.assistant.manager;

import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class cb implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1523a;
    final /* synthetic */ bz b;

    cb(bz bzVar, DownloadInfo downloadInfo) {
        this.b = bzVar;
        this.f1523a = downloadInfo;
    }

    public void onClick(View view) {
        XLog.d("RecommendDownloadManager", "<install> 点击，开始下载推荐应用");
        DownloadProxy.a().d(this.f1523a);
        a.a().a(this.f1523a);
        this.b.b.h();
        int i = 2000;
        if (AstApp.m() != null) {
            i = AstApp.m().f();
        }
        this.b.b.a(STConst.ST_PAGE_INSTALL_RECOMMEND, i, "03_001", 200, null);
        bo.a("rec_pop_tips_click", this.f1523a.appId);
    }
}
