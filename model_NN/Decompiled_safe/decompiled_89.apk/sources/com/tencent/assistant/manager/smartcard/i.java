package com.tencent.assistant.manager.smartcard;

import com.tencent.assistant.model.a.g;
import com.tencent.assistant.model.a.p;
import com.tencent.assistant.model.a.q;
import com.tencent.assistant.model.a.r;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.utils.cv;
import java.util.List;

/* compiled from: ProGuard */
public class i extends y {
    public boolean a(com.tencent.assistant.model.a.i iVar, List<Long> list) {
        if (iVar == null || !(iVar instanceof g)) {
            return false;
        }
        g gVar = (g) iVar;
        return a(gVar, (r) this.f1610a.get(Integer.valueOf(gVar.i)), (s) this.b.get(Integer.valueOf(gVar.i)), (List) this.c.get(Integer.valueOf(iVar.i)));
    }

    private boolean a(g gVar, r rVar, s sVar, List<p> list) {
        if (sVar == null) {
            return false;
        }
        if (rVar == null) {
            rVar = new r();
            rVar.f = gVar.j;
            rVar.e = gVar.i;
            this.f1610a.put(Integer.valueOf(rVar.e), rVar);
        }
        if (gVar.c == null || gVar.c.size() < sVar.g) {
            a(gVar.s, gVar.j + "||" + gVar.i + "|" + 3, gVar.i);
            return false;
        } else if (rVar.b >= sVar.b) {
            a(gVar.s, gVar.j + "||" + gVar.i + "|" + 1, gVar.i);
            return false;
        } else if (rVar.f1651a < sVar.f1652a) {
            return true;
        } else {
            a(gVar.s, gVar.j + "||" + gVar.i + "|" + 2, gVar.i);
            return false;
        }
    }

    public void a(q qVar) {
        r rVar;
        int i;
        if (qVar != null) {
            r rVar2 = (r) this.f1610a.get(Integer.valueOf(qVar.f1650a));
            if (rVar2 == null) {
                r rVar3 = new r();
                rVar3.e = qVar.f1650a;
                rVar3.f = qVar.b;
                rVar = rVar3;
            } else {
                rVar = rVar2;
            }
            if (qVar.d) {
                rVar.d = true;
            }
            if (qVar.c) {
                if (cv.b(qVar.e * 1000)) {
                    rVar.f1651a++;
                }
                s sVar = (s) this.b.get(Integer.valueOf(qVar.f1650a));
                if (sVar != null) {
                    i = sVar.i;
                } else {
                    i = 7;
                }
                if (cv.a(qVar.e * 1000, i)) {
                    rVar.b++;
                }
                rVar.c++;
            }
            this.f1610a.put(Integer.valueOf(rVar.e), rVar);
        }
    }

    public void a(s sVar) {
        this.b.put(Integer.valueOf(sVar.e), sVar);
    }

    public void a(com.tencent.assistant.model.a.i iVar) {
        if (iVar != null && iVar.i == 1) {
            g gVar = (g) iVar;
            s sVar = (s) this.b.get(Integer.valueOf(gVar.i));
            if (sVar != null) {
                gVar.p = sVar.d;
                gVar.f1641a = a(gVar, sVar.g, sVar.j);
            }
        }
    }

    private int a(g gVar, int i, int i2) {
        if (!(gVar == null || gVar.c == null)) {
            int size = gVar.c.size();
            if (size >= i && size < i2) {
                return i;
            }
            if (size >= i2) {
                return i2;
            }
        }
        return 0;
    }
}
