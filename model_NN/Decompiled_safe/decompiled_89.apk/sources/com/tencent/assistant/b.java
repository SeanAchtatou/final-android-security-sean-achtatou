package com.tencent.assistant;

import android.text.TextUtils;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ct;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f844a;
    private static HashMap<String, String> d = new HashMap<>();
    private String b = Constants.STR_EMPTY;
    private long c = 0;

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f844a == null) {
                f844a = new b();
                f844a.b();
            }
            bVar = f844a;
        }
        return bVar;
    }

    public void b() {
        Map<String, String> readConfigFile = FileUtil.readConfigFile(FileUtil.getCommonPath(null) + File.separator + "dev_config.aid");
        XLog.d("Config", "************* init devConfig" + readConfigFile);
        if (readConfigFile.containsKey("APP_STATUS")) {
            String str = readConfigFile.get("APP_STATUS");
            if (b(str)) {
                this.b = str;
                if (readConfigFile.containsKey("time")) {
                    String str2 = readConfigFile.get("time");
                    if (!TextUtils.isEmpty(str2) && !str2.equals("NA")) {
                        this.c = ct.c(str2.trim());
                    }
                }
            }
        }
    }

    public void a(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("APP_STATUS=" + str);
        stringBuffer.append("\n");
        stringBuffer.append("time=" + System.currentTimeMillis());
        FileUtil.write(FileUtil.getCommonPath(null) + File.separator + "dev_config.aid", stringBuffer.toString());
        i();
    }

    private boolean b(String str) {
        if (TextUtils.isEmpty(str) || !str.equals("DEV")) {
            return false;
        }
        return true;
    }

    private boolean g() {
        long currentTimeMillis = System.currentTimeMillis() - this.c;
        if (currentTimeMillis <= 0 || currentTimeMillis >= TesDownloadConfig.TES_CONFIG_CHECK_PERIOD) {
            return false;
        }
        return true;
    }

    public int c() {
        if (f()) {
            h();
            if (d.containsKey("Connect_Server") && !d.get("Connect_Server").equals(STConst.ST_DEFAULT_SLOT)) {
                XLog.d("Config", "****************** getServerAddress=" + d.get("Connect_Server"));
                try {
                    return Integer.valueOf(d.get("Connect_Server")).intValue();
                } catch (Exception e) {
                }
            }
        }
        return -1;
    }

    public String d() {
        if (f()) {
            h();
            if (d.containsKey("QUA_ChannelId") && !d.get("QUA_ChannelId").equals(STConst.ST_DEFAULT_SLOT)) {
                XLog.d("Config", "****************** getChannelId=" + d.get("QUA_ChannelId"));
                return d.get("QUA_ChannelId");
            }
        }
        return Constants.STR_EMPTY;
    }

    public String e() {
        if (f()) {
            h();
            if (d.containsKey("QUA_Version") && !d.get("QUA_Version").equals(STConst.ST_DEFAULT_SLOT)) {
                XLog.d("Config", "****************** getAppVersion=" + d.get("QUA_Version"));
                return d.get("QUA_Version");
            }
        }
        return Constants.STR_EMPTY;
    }

    public boolean f() {
        if (!b(this.b) || !g()) {
            return false;
        }
        return true;
    }

    private void h() {
        Map<String, String> readConfigFile;
        if (d.isEmpty() && (readConfigFile = FileUtil.readConfigFile(FileUtil.getCommonPath(null) + File.separator + "assistant_setting.ini")) != null && readConfigFile.size() > 0) {
            d.putAll(readConfigFile);
        }
    }

    private void i() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Connect_Server=0").append("\n");
        stringBuffer.append("QUA_ChannelId=0").append("\n");
        stringBuffer.append("QUA_Version=-1");
        FileUtil.write(FileUtil.getCommonPath(null) + File.separator + "assistant_setting.ini", stringBuffer.toString());
    }
}
