package com.tencent.assistant.apilevel;

import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.utils.r;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
public class b extends WebChromeClient {
    private static final FrameLayout.LayoutParams i = new FrameLayout.LayoutParams(-1, -1);

    /* renamed from: a  reason: collision with root package name */
    protected BrowserActivity.WebChromeClientListener f824a = null;
    protected Activity b = null;
    private View c;
    private WebChromeClient.CustomViewCallback d;
    private int e;
    private FrameLayout f;
    private FrameLayout g;
    private String h;

    public b(BrowserActivity.WebChromeClientListener webChromeClientListener) {
        this.f824a = webChromeClientListener;
        this.b = webChromeClientListener.getActivity();
    }

    public void onProgressChanged(WebView webView, int i2) {
        this.f824a.onProgressChanged(webView, i2);
    }

    public void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
        callback.invoke(str, true, false);
    }

    public void onReceivedTitle(WebView webView, String str) {
        if (webView != null) {
            this.h = webView.getUrl();
        }
        this.f824a.onReceivedTitle(webView, str);
        if (webView != null) {
            try {
                Method method = webView.getClass().getMethod("removeJavascriptInterface", String.class);
                if (method != null) {
                    method.invoke(webView, "searchBoxJavaBridge_");
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (this.c != null) {
            customViewCallback.onCustomViewHidden();
            return;
        }
        this.e = this.b.getRequestedOrientation();
        this.g = new c(this.b);
        this.g.addView(view, i);
        ((FrameLayout) this.b.getWindow().getDecorView()).addView(this.g, i);
        this.c = view;
        a(true);
        this.d = customViewCallback;
    }

    public void onShowCustomView(View view, int i2, WebChromeClient.CustomViewCallback customViewCallback) {
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (this.c != null) {
            customViewCallback.onCustomViewHidden();
            return;
        }
        this.e = this.b.getRequestedOrientation();
        this.g = new c(this.b);
        this.g.addView(view, i);
        ((FrameLayout) this.b.getWindow().getDecorView()).addView(this.g, i);
        this.c = view;
        a(true);
        this.d = customViewCallback;
        this.b.setRequestedOrientation(i2);
    }

    public void onHideCustomView() {
        if (this.c != null) {
            a(false);
            ((FrameLayout) this.b.getWindow().getDecorView()).removeView(this.g);
            this.g = null;
            this.c = null;
            this.d.onCustomViewHidden();
            this.b.setRequestedOrientation(this.e);
        }
    }

    private void a(boolean z) {
        Window window = this.b.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        if (z) {
            attributes.flags |= 1024;
        } else {
            attributes.flags &= -1025;
            if (this.c != null) {
                a(this.c);
            } else {
                a(this.f);
            }
        }
        window.setAttributes(attributes);
    }

    private void a(View view) {
        if (r.d() >= 14) {
            try {
                view.getClass().getMethod("setSystemUiVisibility", Integer.TYPE).invoke(view, 0);
            } catch (Exception e2) {
            }
        }
    }
}
