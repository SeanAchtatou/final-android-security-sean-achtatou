package com.tencent.assistant.thumbnailCache;

/* compiled from: ProGuard */
class l implements p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f2578a;

    l(k kVar) {
        this.f2578a = kVar;
    }

    public void thumbnailRequestCompleted(o oVar) {
        synchronized (this.f2578a.d) {
            if (((o) this.f2578a.d.remove(oVar.c())) != null) {
                o oVar2 = (o) this.f2578a.e.remove(oVar.c());
                if (oVar2 != null) {
                    oVar2.f = oVar.f;
                    oVar2.b(0);
                }
                if (this.f2578a.d.size() < this.f2578a.f2577a.c()) {
                    this.f2578a.d.notify();
                }
                if (this.f2578a.g < System.currentTimeMillis()) {
                    k.a(this.f2578a, 60000);
                    this.f2578a.c();
                }
            }
        }
    }

    public void thumbnailRequestFailed(o oVar) {
        synchronized (this.f2578a.d) {
            if (((o) this.f2578a.d.remove(oVar.c())) != null) {
                o oVar2 = (o) this.f2578a.e.remove(oVar.c());
                if (oVar2 != null) {
                    oVar2.b(2);
                }
                if (this.f2578a.d.size() < this.f2578a.f2577a.c()) {
                    this.f2578a.d.notify();
                }
            }
        }
    }
}
