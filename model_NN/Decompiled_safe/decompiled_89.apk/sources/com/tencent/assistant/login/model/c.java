package com.tencent.assistant.login.model;

import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.TicketOAuth2Code;
import com.tencent.assistant.utils.bj;

/* compiled from: ProGuard */
public class c extends a {
    public boolean d;
    private String e;

    public c(String str, boolean z) {
        super(AppConst.IdentityType.WXCODE);
        this.e = str;
        this.d = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.a.c.a(java.lang.String, java.lang.String, boolean):byte[]
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.assistant.login.a.c.a(byte[], java.lang.String, boolean):java.lang.String
      com.tencent.assistant.login.a.c.a(java.lang.String, java.lang.String, boolean):byte[] */
    /* access modifiers changed from: protected */
    public JceStruct a() {
        return new TicketOAuth2Code(!TextUtils.isEmpty(this.e) ? com.tencent.assistant.login.a.c.a(this.e, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnWCwGlN/jRLEi4PxWrOqy2I0tSRs8UhjX+Q5nkYVDiqerMhSH8c6jJgjYbVsETfH85wgSjFR7U5STafxsBBAUWADt7dy7NS0GuNN9IyX5U0AEBSI9GPsPd7JmtwiiOS1cJKnHIUb+fKwaaWTDvi208KOvGe2WplhKpaQ2Eo9kTwIDAQAB", true) : null);
    }

    /* access modifiers changed from: protected */
    public byte[] getKey() {
        if (!TextUtils.isEmpty(this.e)) {
            return bj.a(this.e);
        }
        return null;
    }
}
