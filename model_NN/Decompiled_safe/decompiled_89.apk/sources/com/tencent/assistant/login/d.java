package com.tencent.assistant.login;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.appbakcup.n;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.login.model.a;
import com.tencent.assistant.login.model.b;
import com.tencent.assistant.login.model.c;
import com.tencent.assistant.module.ee;
import com.tencent.assistant.protocol.jce.Ticket;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.HashMap;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static d f1458a;
    private a b;
    private a c;
    private Object d = new Object();
    private a e;

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (f1458a == null) {
                f1458a = new d();
                f1458a.h();
            }
            dVar = f1458a;
        }
        return dVar;
    }

    public d() {
        String a2 = com.tencent.assistant.login.a.a.a();
        String d2 = com.tencent.assistant.login.a.a.d();
        if (!TextUtils.isEmpty(a2)) {
            if (!TextUtils.isEmpty(d2)) {
                this.e = c(AppConst.IdentityType.WX);
            } else {
                this.e = c(AppConst.IdentityType.MOBILEQ);
            }
        }
        n.a().b();
    }

    public void b() {
    }

    public void a(AppConst.IdentityType identityType) {
        this.e = c(identityType);
    }

    public a c() {
        a aVar;
        synchronized (this.d) {
            if (this.b == null) {
                this.b = b.b();
            }
            aVar = this.b;
        }
        return aVar;
    }

    public AppConst.IdentityType d() {
        return c().getType();
    }

    public synchronized void a(a aVar) {
        boolean c2 = c(aVar);
        synchronized (this.d) {
            this.c = null;
            if (aVar == null) {
                aVar = b.b();
            }
            this.b = aVar;
        }
        y();
        if (c2) {
            ee.a().b();
        }
    }

    public void b(a aVar) {
        synchronized (this.d) {
            this.c = this.b;
            if (aVar == null) {
                aVar = b.b();
            }
            this.b = aVar;
        }
    }

    private boolean c(a aVar) {
        try {
            XLog.d("LoginProxy", "id = " + aVar + " mId = " + this.b);
            XLog.d("LoginProxy", "id.type = " + aVar.getType() + " mid.type = " + this.b.getType());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (aVar == null || aVar.getType() == AppConst.IdentityType.NONE || aVar.getType() == AppConst.IdentityType.WXCODE) {
            return false;
        }
        if (this.b != null && this.b.getType() != AppConst.IdentityType.NONE) {
            if (this.b.getType() == AppConst.IdentityType.WXCODE) {
                if (aVar.getType() == AppConst.IdentityType.WX) {
                    if (((c) this.b).d) {
                        return true;
                    }
                    return false;
                } else if (aVar.getType() == AppConst.IdentityType.MOBILEQ) {
                    return true;
                }
            } else if (this.b.getType() == AppConst.IdentityType.WX) {
                if (aVar.getType() == AppConst.IdentityType.WX) {
                    if (((com.tencent.assistant.login.model.d) this.b).equals((com.tencent.assistant.login.model.d) aVar)) {
                        return false;
                    }
                    return true;
                } else if (aVar.getType() == AppConst.IdentityType.MOBILEQ) {
                    return true;
                }
            } else if (this.b.getType() == AppConst.IdentityType.MOBILEQ) {
                if (aVar.getType() == AppConst.IdentityType.WX) {
                    return true;
                }
                if (aVar.getType() == AppConst.IdentityType.MOBILEQ) {
                    if (((MoblieQIdentityInfo) this.b).equals((MoblieQIdentityInfo) aVar)) {
                        return false;
                    }
                    return true;
                }
            }
            return false;
        } else if (aVar.getType() == AppConst.IdentityType.WXCODE) {
            return false;
        } else {
            return true;
        }
    }

    public void e() {
        synchronized (this.d) {
            this.b = b.b();
            z();
        }
    }

    public void f() {
        synchronized (this.d) {
            if (this.c == null) {
                this.b = b.b();
            } else {
                this.b = this.c;
                this.c = null;
            }
        }
    }

    public Ticket g() {
        Ticket ticket;
        synchronized (this.d) {
            if (this.b != null) {
                ticket = this.b.getTicket();
            } else {
                b b2 = b.b();
                this.b = b2;
                ticket = b2.getTicket();
            }
        }
        return ticket;
    }

    public void h() {
        if (this.e != null) {
            this.e.g();
        }
    }

    private boolean y() {
        if (this.b.getType() == AppConst.IdentityType.WX) {
            com.tencent.assistant.login.model.d dVar = (com.tencent.assistant.login.model.d) this.b;
            com.tencent.assistant.login.a.a.a(dVar.b(), dVar.c(), dVar.d());
            return true;
        } else if (this.b.getType() != AppConst.IdentityType.MOBILEQ) {
            return true;
        } else {
            MoblieQIdentityInfo moblieQIdentityInfo = (MoblieQIdentityInfo) this.b;
            com.tencent.assistant.login.a.a.a(moblieQIdentityInfo.getAccount(), moblieQIdentityInfo.getUin(), null, null);
            return true;
        }
    }

    private boolean z() {
        if (this.e != null) {
            this.e.f();
        }
        com.tencent.assistant.login.a.a.a(null, null, null);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.d.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.assistant.login.d.a(com.tencent.assistant.AppConst$IdentityType, android.os.Bundle):void
      com.tencent.assistant.login.d.a(boolean, java.lang.String):void */
    public void a(Ticket ticket) {
        boolean z;
        a aVar;
        int i;
        int i2 = 2000;
        if (ticket != null && ticket.f2396a == AppConst.IdentityType.WX.ordinal() && ticket.b != null) {
            a a2 = com.tencent.assistant.login.model.d.a(ticket.b);
            synchronized (this.d) {
                if (this.b.getType() == AppConst.IdentityType.WXCODE) {
                    z = !((c) this.b).d;
                } else {
                    z = false;
                }
            }
            if (a2 != null) {
                a(a2);
                if (AstApp.m() != null) {
                    i = AstApp.m().f();
                    i2 = AstApp.m().p();
                } else {
                    i = 2000;
                }
                k.a(new STInfoV2(i, STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, STConstAction.ACTION_HIT_LOGIN_SUCCESS_WX));
                aVar = a2;
            } else {
                a b2 = b.b();
                a(b2);
                aVar = b2;
            }
            if (z) {
                Message obtainMessage = AstApp.i().j().obtainMessage(aVar != null ? EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS : EventDispatcherEnum.UI_EVENT_LOGIN_FAIL);
                obtainMessage.arg1 = AppConst.LoginEgnineType.ENGINE_WX.ordinal();
                if (this.e != null) {
                    obtainMessage.obj = this.e.a();
                }
                AstApp.i().j().sendMessage(obtainMessage);
                if (aVar != null) {
                    a(true, "success");
                } else {
                    a(false, "fail");
                }
            }
        }
    }

    private void a(boolean z, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", str);
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", t.g());
        com.tencent.beacon.event.a.a("Login_WX", z, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: Login_WX, params : " + hashMap.toString());
    }

    private void b(AppConst.IdentityType identityType, Bundle bundle) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", identityType.name());
        hashMap.put("B2", bundle != null ? bundle.getInt(AppConst.KEY_FROM_TYPE) + Constants.STR_EMPTY : "0");
        hashMap.put("B3", a(bundle));
        hashMap.put("B4", Global.getPhoneGuidAndGen());
        hashMap.put("B5", Global.getQUAForBeacon());
        hashMap.put("B6", t.g());
        com.tencent.beacon.event.a.a("LoginTriggerAll", true, -1, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event : LoginTriggerAll. paras : " + hashMap.toString());
    }

    private String a(Bundle bundle) {
        if (bundle == null) {
            return "默认";
        }
        switch (bundle.getInt(AppConst.KEY_FROM_TYPE)) {
            case 0:
                return "默认";
            case 1:
                return "分享";
            case 2:
                return "收藏";
            case 3:
                return "抢号";
            case 4:
                return "内测";
            case 5:
                return "手Q透传票据";
            case 6:
                return "应用吧";
            case 7:
                return "无缝连接";
            case 8:
                return "账号栏";
            case 9:
                return "猜你喜欢";
            case 10:
                return "应用备份";
            case 11:
                return "应用详情页发话题";
            case 12:
                return "应用详情页发话题-带提示";
            case 13:
                return "好友榜";
            case 14:
                return "QQ阅读插件";
            case 15:
                return "隐私设置";
            case 16:
                return "引导页";
            case 17:
                return "免费wifi";
            case 18:
                return "第一次启动后恢复历史应用";
            default:
                return "默认";
        }
    }

    public Bundle i() {
        if (this.e != null) {
            return this.e.a();
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.login.d.a(com.tencent.assistant.AppConst$IdentityType, android.os.Bundle, boolean):void
     arg types: [com.tencent.assistant.AppConst$IdentityType, android.os.Bundle, int]
     candidates:
      com.tencent.assistant.login.d.a(java.lang.String, java.lang.String, java.lang.String):void
      com.tencent.assistant.login.d.a(com.tencent.assistant.AppConst$IdentityType, android.os.Bundle, boolean):void */
    public void a(AppConst.IdentityType identityType, Bundle bundle) {
        a(identityType, bundle, true);
    }

    public void a(AppConst.IdentityType identityType, Bundle bundle, boolean z) {
        synchronized (this.d) {
            this.c = this.b;
        }
        this.e = c(identityType);
        if (this.e != null) {
            this.e.a(bundle);
            this.e.d();
        }
        if (z) {
            b(identityType, bundle);
        }
    }

    public void b(AppConst.IdentityType identityType) {
        a(identityType, (Bundle) null);
    }

    private a c(AppConst.IdentityType identityType) {
        return identityType == AppConst.IdentityType.WX ? com.tencent.assistant.login.b.a.h() : b.h();
    }

    public boolean j() {
        boolean z;
        synchronized (this.d) {
            z = (this.b == null || this.b.getType() == AppConst.IdentityType.NONE || this.b.getType() == AppConst.IdentityType.WXCODE) ? false : true;
        }
        return z;
    }

    public boolean k() {
        boolean z;
        synchronized (this.d) {
            z = this.b != null && this.b.getType() == AppConst.IdentityType.MOBILEQ;
        }
        return z;
    }

    public boolean l() {
        boolean z;
        synchronized (this.d) {
            z = this.b != null && this.b.getType() == AppConst.IdentityType.WX;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String m() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x001e }
            if (r0 != r2) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r0 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r0     // Catch:{ all -> 0x001e }
            java.lang.String r0 = r0.getSKey()     // Catch:{ all -> 0x001e }
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            r0 = 0
            goto L_0x001a
        L_0x001e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.login.d.m():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        return com.tencent.connect.common.Constants.STR_EMPTY;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String n() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x001f }
            if (r0 != r2) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r0 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r0     // Catch:{ all -> 0x001f }
            java.lang.String r0 = r0.getSid()     // Catch:{ all -> 0x001f }
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            java.lang.String r0 = ""
            goto L_0x001a
        L_0x001f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.login.d.n():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        return com.tencent.connect.common.Constants.STR_EMPTY;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String o() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x001f }
            if (r0 != r2) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r0 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r0     // Catch:{ all -> 0x001f }
            java.lang.String r0 = r0.getVkey()     // Catch:{ all -> 0x001f }
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            java.lang.String r0 = ""
            goto L_0x001a
        L_0x001f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.login.d.o():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long p() {
        /*
            r3 = this;
            java.lang.Object r2 = r3.d
            monitor-enter(r2)
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001f }
            com.tencent.assistant.AppConst$IdentityType r1 = com.tencent.assistant.AppConst.IdentityType.MOBILEQ     // Catch:{ all -> 0x001f }
            if (r0 != r1) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001f }
            com.tencent.assistant.login.model.MoblieQIdentityInfo r0 = (com.tencent.assistant.login.model.MoblieQIdentityInfo) r0     // Catch:{ all -> 0x001f }
            long r0 = r0.getUin()     // Catch:{ all -> 0x001f }
            monitor-exit(r2)     // Catch:{ all -> 0x001f }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r2)     // Catch:{ all -> 0x001f }
            r0 = 0
            goto L_0x001a
        L_0x001f:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x001f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.login.d.p():long");
    }

    public String q() {
        com.tencent.assistant.login.a.b f = com.tencent.assistant.login.a.a.f();
        if (f == null || TextUtils.isEmpty(f.b)) {
            return null;
        }
        return f.b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String r() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.WX     // Catch:{ all -> 0x001e }
            if (r0 != r2) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.login.model.d r0 = (com.tencent.assistant.login.model.d) r0     // Catch:{ all -> 0x001e }
            java.lang.String r0 = r0.c()     // Catch:{ all -> 0x001e }
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            r0 = 0
            goto L_0x001a
        L_0x001e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.login.d.r():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String s() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.d
            monitor-enter(r1)
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r0 = r0.getType()     // Catch:{ all -> 0x001e }
            com.tencent.assistant.AppConst$IdentityType r2 = com.tencent.assistant.AppConst.IdentityType.WX     // Catch:{ all -> 0x001e }
            if (r0 != r2) goto L_0x001b
            com.tencent.assistant.login.model.a r0 = r3.b     // Catch:{ all -> 0x001e }
            com.tencent.assistant.login.model.d r0 = (com.tencent.assistant.login.model.d) r0     // Catch:{ all -> 0x001e }
            java.lang.String r0 = r0.b()     // Catch:{ all -> 0x001e }
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
        L_0x001a:
            return r0
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            r0 = 0
            goto L_0x001a
        L_0x001e:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001e }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.login.d.s():java.lang.String");
    }

    public boolean t() {
        return ApkResourceManager.getInstance().getInstalledApkInfo("com.tencent.mobileqq") != null;
    }

    public boolean u() {
        boolean t = t();
        if (t) {
            try {
                PackageInfo packageInfo = AstApp.i().getPackageManager().getPackageInfo("com.tencent.mobileqq", 0);
                if (packageInfo != null && packageInfo.versionCode < 66) {
                    return false;
                }
            } catch (PackageManager.NameNotFoundException e2) {
                return false;
            }
        }
        return t;
    }

    public boolean v() {
        return ApkResourceManager.getInstance().getInstalledApkInfo("com.tencent.mm") != null;
    }

    public int w() {
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo("com.tencent.mobileqq");
        if (installedApkInfo != null) {
            return installedApkInfo.mVersionCode;
        }
        return 0;
    }

    public void x() {
        e();
        com.tencent.assistant.login.a.a.a((com.tencent.assistant.login.a.b) null);
        synchronized (this.d) {
            this.c = null;
        }
        ba.a().post(new e(this));
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_LOGOUT));
        com.tencent.assistant.plugin.a.b.a();
    }

    public void a(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            synchronized (this.d) {
                this.b = new com.tencent.assistant.login.model.d(str, str2, str3);
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_INIT_IDENTITY_SUCCESS);
            }
        }
    }

    public void a(MoblieQIdentityInfo moblieQIdentityInfo) {
        synchronized (this.d) {
            if (moblieQIdentityInfo != null) {
                this.b = moblieQIdentityInfo;
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_INIT_IDENTITY_SUCCESS);
            }
        }
    }
}
