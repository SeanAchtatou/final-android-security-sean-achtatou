package com.tencent.assistant.kapalaiadapter.a;

import android.content.Context;
import com.tencent.assistant.kapalaiadapter.g;

/* compiled from: ProGuard */
public class a implements j {

    /* renamed from: a  reason: collision with root package name */
    private Object[] f1379a = null;

    public Object a(int i, Context context) {
        char c = 0;
        if (this.f1379a == null) {
            try {
                this.f1379a = new Object[2];
                this.f1379a[0] = context.getSystemService("phone");
                this.f1379a[1] = context.getSystemService("phone2");
            } catch (Exception e) {
            }
        }
        if (this.f1379a == null || this.f1379a.length <= i) {
            return null;
        }
        Object[] objArr = this.f1379a;
        if (i > 0) {
            c = 1;
        }
        return objArr[c];
    }

    public String b(int i, Context context) {
        Object a2 = a(i, context);
        if (a2 == null) {
            return null;
        }
        try {
            return (String) g.a(a2, "getSubscriberId");
        } catch (Exception e) {
            return null;
        }
    }

    public String c(int i, Context context) {
        Object a2 = a(i, context);
        if (a2 == null) {
            return null;
        }
        try {
            return (String) g.a(a2, "getDeviceId");
        } catch (Exception e) {
            return null;
        }
    }
}
