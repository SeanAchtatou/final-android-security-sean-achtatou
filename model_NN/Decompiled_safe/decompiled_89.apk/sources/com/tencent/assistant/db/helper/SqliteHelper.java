package com.tencent.assistant.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;
import android.util.Log;
import com.tencent.assistant.db.table.IBaseTable;

/* compiled from: ProGuard */
public abstract class SqliteHelper extends SQLiteOpenHelper {
    private static final String TAG = SqliteHelper.class.getSimpleName();

    public abstract int getDBVersion();

    public abstract Class<?>[] getTables();

    public SqliteHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, cursorFactory, i);
    }

    @Deprecated
    public synchronized SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase writableDatabase;
        writableDatabase = super.getWritableDatabase();
        while (true) {
            if (writableDatabase.isDbLockedByCurrentThread() || writableDatabase.isDbLockedByOtherThreads()) {
                SystemClock.sleep(10);
            }
        }
        return writableDatabase;
    }

    @Deprecated
    public synchronized SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase readableDatabase;
        readableDatabase = super.getReadableDatabase();
        while (true) {
            if (readableDatabase.isDbLockedByCurrentThread() || readableDatabase.isDbLockedByOtherThreads()) {
                SystemClock.sleep(10);
            }
        }
        return readableDatabase;
    }

    public synchronized SQLiteDatabaseWrapper getWritableDatabaseWrapper() {
        SQLiteDatabase sQLiteDatabase;
        sQLiteDatabase = null;
        try {
            sQLiteDatabase = getWritedb();
        } catch (Throwable th) {
            Log.w("download_db", "get write db final fail." + th);
            th.printStackTrace();
        }
        return new SQLiteDatabaseWrapper(sQLiteDatabase);
    }

    private SQLiteDatabase getWritedb() {
        SQLiteDatabase writableDatabase = super.getWritableDatabase();
        while (true) {
            if (!writableDatabase.isDbLockedByCurrentThread() && !writableDatabase.isDbLockedByOtherThreads()) {
                return writableDatabase;
            }
            SystemClock.sleep(10);
        }
    }

    public synchronized SQLiteDatabaseWrapper getReadableDatabaseWrapper() {
        SQLiteDatabase sQLiteDatabase;
        sQLiteDatabase = null;
        try {
            sQLiteDatabase = getReaddb();
        } catch (Throwable th) {
            Log.w("download_db", "get read db final fail." + th);
            th.printStackTrace();
        }
        return new SQLiteDatabaseWrapper(sQLiteDatabase);
    }

    private SQLiteDatabase getReaddb() {
        SQLiteDatabase readableDatabase = super.getReadableDatabase();
        while (true) {
            if (!readableDatabase.isDbLockedByCurrentThread() && !readableDatabase.isDbLockedByOtherThreads()) {
                return readableDatabase;
            }
            SystemClock.sleep(10);
        }
    }

    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        int version = sQLiteDatabase.getVersion();
        if (version != 0) {
            if (version < getDBVersion()) {
                onUpgrade(sQLiteDatabase, version, getDBVersion());
            } else if (version > getDBVersion()) {
                onDowngrade(sQLiteDatabase, version, getDBVersion());
            }
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        createTable(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        while (i < i2) {
            for (Class<?> newInstance : getTables()) {
                try {
                    IBaseTable iBaseTable = (IBaseTable) newInstance.newInstance();
                    iBaseTable.beforeTableAlter(i, i + 1, sQLiteDatabase);
                    String[] alterSQL = iBaseTable.getAlterSQL(i, i + 1);
                    if (alterSQL != null) {
                        for (String execSQL : alterSQL) {
                            sQLiteDatabase.execSQL(execSQL);
                        }
                    }
                    iBaseTable.afterTableAlter(i, i + 1, sQLiteDatabase);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            i++;
        }
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        deleteTable(sQLiteDatabase);
        createTable(sQLiteDatabase);
    }

    private void createTable(SQLiteDatabase sQLiteDatabase) {
        String str;
        for (Class<?> newInstance : getTables()) {
            try {
                str = ((IBaseTable) newInstance.newInstance()).createTableSQL();
            } catch (Exception e) {
                e.printStackTrace();
                str = null;
            }
            if (str != null && str.length() > 0) {
                sQLiteDatabase.execSQL(str);
            }
        }
    }

    private void deleteTable(SQLiteDatabase sQLiteDatabase) {
        for (Class<?> newInstance : getTables()) {
            try {
                sQLiteDatabase.delete(((IBaseTable) newInstance.newInstance()).tableName(), null, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int getSumTableVer() {
        int i;
        Class<?>[] tables = getTables();
        int length = tables.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            try {
                i = ((IBaseTable) tables[i2].newInstance()).tableVersion() + i3;
            } catch (Exception e) {
                e.printStackTrace();
                i = i3;
            }
            i2++;
            i3 = i;
        }
        return i3;
    }
}
