package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.db.a.a;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;

/* compiled from: ProGuard */
public class i implements IBaseTable {
    public i() {
    }

    public i(Context context) {
    }

    /* access modifiers changed from: protected */
    public a a(Cursor cursor) {
        a aVar = new a();
        aVar.f1227a = cursor.getLong(cursor.getColumnIndex("timesStamp"));
        aVar.b = cursor.getLong(cursor.getColumnIndex("appId"));
        aVar.c = cursor.getLong(cursor.getColumnIndex(CommentDetailTabView.PARAMS_APK_ID));
        aVar.d = cursor.getString(cursor.getColumnIndex("packageName"));
        aVar.e = cursor.getInt(cursor.getColumnIndex(CommentDetailTabView.PARAMS_VERSION_CODE));
        aVar.f = cursor.getInt(cursor.getColumnIndex("localVersionCode"));
        aVar.g = cursor.getString(cursor.getColumnIndex("localFilePath"));
        aVar.h = cursor.getString(cursor.getColumnIndex("patchFilePath"));
        aVar.k = cursor.getString(cursor.getColumnIndex("localFileMd5"));
        aVar.l = cursor.getString(cursor.getColumnIndex("patchFileMd5"));
        aVar.i = cursor.getLong(cursor.getColumnIndex("localFileSize"));
        aVar.j = cursor.getLong(cursor.getColumnIndex("patchFileSize"));
        aVar.m = cursor.getInt(cursor.getColumnIndex("errorId"));
        aVar.n = cursor.getString(cursor.getColumnIndex("errorMsg"));
        return aVar;
    }

    private ContentValues d(a aVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("timesStamp", Long.valueOf(aVar.f1227a));
        contentValues.put("appId", Long.valueOf(aVar.b));
        contentValues.put(CommentDetailTabView.PARAMS_APK_ID, Long.valueOf(aVar.c));
        contentValues.put("packageName", aVar.d);
        contentValues.put(CommentDetailTabView.PARAMS_VERSION_CODE, Integer.valueOf(aVar.e));
        contentValues.put("localVersionCode", Integer.valueOf(aVar.f));
        contentValues.put("localFilePath", aVar.g);
        contentValues.put("patchFilePath", aVar.h);
        contentValues.put("localFileMd5", aVar.k);
        contentValues.put("patchFileMd5", aVar.l);
        contentValues.put("localFileSize", Long.valueOf(aVar.i));
        contentValues.put("patchFileSize", Long.valueOf(aVar.j));
        contentValues.put("errorId", Integer.valueOf(aVar.m));
        contentValues.put("errorMsg", aVar.n);
        return contentValues;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0044 A[Catch:{ Exception -> 0x0038, all -> 0x0041 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.tencent.assistant.db.a.a> a() {
        /*
            r10 = this;
            r9 = 0
            monitor-enter(r10)
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ all -> 0x0048 }
            r8.<init>()     // Catch:{ all -> 0x0048 }
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()     // Catch:{ all -> 0x0048 }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ all -> 0x0048 }
            java.lang.String r1 = "diff_upload_info"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0038, all -> 0x0041 }
            if (r0 == 0) goto L_0x0030
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x004f, all -> 0x004b }
            if (r1 == 0) goto L_0x0030
        L_0x0023:
            com.tencent.assistant.db.a.a r1 = r10.a(r0)     // Catch:{ Exception -> 0x004f, all -> 0x004b }
            r8.add(r1)     // Catch:{ Exception -> 0x004f, all -> 0x004b }
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x004f, all -> 0x004b }
            if (r1 != 0) goto L_0x0023
        L_0x0030:
            if (r0 == 0) goto L_0x0035
            r0.close()     // Catch:{ all -> 0x0048 }
        L_0x0035:
            r0 = r8
        L_0x0036:
            monitor-exit(r10)
            return r0
        L_0x0038:
            r0 = move-exception
            r0 = r9
        L_0x003a:
            if (r0 == 0) goto L_0x003f
            r0.close()     // Catch:{ all -> 0x0048 }
        L_0x003f:
            r0 = r8
            goto L_0x0036
        L_0x0041:
            r0 = move-exception
        L_0x0042:
            if (r9 == 0) goto L_0x0047
            r9.close()     // Catch:{ all -> 0x0048 }
        L_0x0047:
            throw r0     // Catch:{ all -> 0x0048 }
        L_0x0048:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x004b:
            r1 = move-exception
            r9 = r0
            r0 = r1
            goto L_0x0042
        L_0x004f:
            r1 = move-exception
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.i.a():java.util.ArrayList");
    }

    public synchronized boolean a(a aVar) {
        return getHelper().getWritableDatabaseWrapper().insert("diff_upload_info", null, d(aVar)) != -1;
    }

    public synchronized int b(a aVar) {
        int delete;
        if (aVar == null) {
            delete = -1;
        } else {
            delete = getHelper().getWritableDatabaseWrapper().delete("diff_upload_info", "packageName = ?", new String[]{aVar.d});
        }
        return delete;
    }

    public synchronized void c(a aVar) {
        if (aVar != null) {
            try {
                if (a(aVar, getHelper().getWritableDatabaseWrapper()) <= 0) {
                    a(aVar);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    private int a(a aVar, SQLiteDatabaseWrapper sQLiteDatabaseWrapper) {
        if (aVar == null) {
            return -1;
        }
        try {
            int update = sQLiteDatabaseWrapper.update("diff_upload_info", d(aVar), "packageName = ?", new String[]{aVar.d});
            if (update <= 0) {
                return 0;
            }
            return update;
        } catch (Exception e) {
            e.printStackTrace();
            return -2;
        }
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "diff_upload_info";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists diff_upload_info (_id INTEGER PRIMARY KEY AUTOINCREMENT,timesStamp INTEGER,appId INTEGER,apkId INTEGER,packageName TEXT,versionCode INTEGER,localVersionCode INTEGER,localFilePath TEXT,patchFilePath TEXT,localFileMd5 TEXT,patchFileMd5 TEXT,localFileSize INTEGER,patchFileSize INTEGER,errorId INTEGER,errorMsg TEXT);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i != 7 || i2 != 8) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists diff_upload_info (_id INTEGER PRIMARY KEY AUTOINCREMENT,timesStamp INTEGER,appId INTEGER,apkId INTEGER,packageName TEXT,versionCode INTEGER,localVersionCode INTEGER,localFilePath TEXT,patchFilePath TEXT,localFileMd5 TEXT,patchFileMd5 TEXT,localFileSize INTEGER,patchFileSize INTEGER,errorId INTEGER,errorMsg TEXT);"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
