package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.List;

/* compiled from: ProGuard */
public class n implements IBaseTable {
    public n() {
    }

    public n(Context context) {
    }

    /* access modifiers changed from: protected */
    public void a(SQLiteStatement sQLiteStatement, LocalApkInfo localApkInfo, long j) {
        sQLiteStatement.bindString(1, localApkInfo.mPackageName);
        sQLiteStatement.bindString(2, localApkInfo.mAppName);
        sQLiteStatement.bindLong(3, (long) localApkInfo.mVersionCode);
        sQLiteStatement.bindString(4, localApkInfo.mVersionName);
        sQLiteStatement.bindLong(5, localApkInfo.occupySize);
        sQLiteStatement.bindLong(6, localApkInfo.mInstallDate);
        sQLiteStatement.bindString(7, localApkInfo.mLocalFilePath);
        sQLiteStatement.bindLong(8, 0);
        sQLiteStatement.bindLong(9, 0);
        sQLiteStatement.bindString(10, localApkInfo.mSortKey);
        sQLiteStatement.bindLong(11, (long) localApkInfo.mAppIconRes);
        sQLiteStatement.bindLong(12, (long) localApkInfo.launchCount);
        sQLiteStatement.bindLong(13, (long) localApkInfo.flags);
        sQLiteStatement.bindString(14, localApkInfo.signature);
        sQLiteStatement.bindString(15, localApkInfo.manifestMd5);
        sQLiteStatement.bindLong(16, j);
        sQLiteStatement.bindLong(17, localApkInfo.mLastLaunchTime);
        sQLiteStatement.bindLong(18, localApkInfo.mFakeLastLaunchTime);
        sQLiteStatement.bindLong(19, localApkInfo.mDataUsage);
        sQLiteStatement.bindLong(20, localApkInfo.mBatteryUsage);
        sQLiteStatement.bindLong(21, (long) localApkInfo.mInstalleLocation);
        sQLiteStatement.bindLong(22, (long) localApkInfo.mUid);
        sQLiteStatement.bindLong(23, (long) localApkInfo.mGrayVersionCode);
    }

    /* access modifiers changed from: protected */
    public void a(SQLiteStatement sQLiteStatement, LocalApkInfo localApkInfo) {
        sQLiteStatement.bindString(1, localApkInfo.mSortKey);
        sQLiteStatement.bindString(2, localApkInfo.signature);
        sQLiteStatement.bindString(3, localApkInfo.manifestMd5);
        sQLiteStatement.bindString(4, localApkInfo.mPackageName);
    }

    /* access modifiers changed from: protected */
    public LocalApkInfo a(Cursor cursor) {
        LocalApkInfo localApkInfo = new LocalApkInfo();
        localApkInfo.mAppid = cursor.getLong(cursor.getColumnIndex("appid"));
        localApkInfo.mPackageName = cursor.getString(cursor.getColumnIndex("packagename"));
        localApkInfo.mAppName = cursor.getString(cursor.getColumnIndex("appname"));
        localApkInfo.mVersionCode = cursor.getInt(cursor.getColumnIndex("versioncode"));
        localApkInfo.mVersionName = cursor.getString(cursor.getColumnIndex("versionname"));
        localApkInfo.occupySize = cursor.getLong(cursor.getColumnIndex("appsize"));
        localApkInfo.mInstallDate = cursor.getLong(cursor.getColumnIndex("installtime"));
        localApkInfo.mLocalFilePath = cursor.getString(cursor.getColumnIndex("filepath"));
        localApkInfo.mSortKey = cursor.getString(cursor.getColumnIndex("sortkey"));
        localApkInfo.mAppIconRes = cursor.getInt(cursor.getColumnIndex("iconRes"));
        localApkInfo.launchCount = cursor.getInt(cursor.getColumnIndex("launchCount"));
        localApkInfo.flags = cursor.getInt(cursor.getColumnIndex("flags"));
        localApkInfo.signature = cursor.getString(cursor.getColumnIndex("signature"));
        localApkInfo.manifestMd5 = cursor.getString(cursor.getColumnIndex("manifest_md5"));
        localApkInfo.mLastLaunchTime = cursor.getLong(cursor.getColumnIndex("lastLaunchTime"));
        localApkInfo.mFakeLastLaunchTime = cursor.getLong(cursor.getColumnIndex("fakeLastLaunchTime"));
        localApkInfo.mDataUsage = cursor.getLong(cursor.getColumnIndex("dataUsage"));
        localApkInfo.mBatteryUsage = cursor.getLong(cursor.getColumnIndex("batteryUsage"));
        localApkInfo.mInstalleLocation = (byte) cursor.getInt(cursor.getColumnIndex("installeLocation"));
        localApkInfo.mUid = cursor.getInt(cursor.getColumnIndex("uid"));
        localApkInfo.mGrayVersionCode = (byte) cursor.getInt(cursor.getColumnIndex("grayVersionCode"));
        return localApkInfo;
    }

    public synchronized boolean a(LocalApkInfo localApkInfo, long j) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("installed", Long.valueOf(j));
        contentValues.put("packagename", localApkInfo.mPackageName);
        contentValues.put("appname", localApkInfo.mAppName);
        contentValues.put("versioncode", Integer.valueOf(localApkInfo.mVersionCode));
        contentValues.put("versionname", localApkInfo.mVersionName);
        contentValues.put("appsize", Long.valueOf(localApkInfo.occupySize));
        contentValues.put("installtime", Long.valueOf(localApkInfo.mInstallDate));
        contentValues.put("filepath", localApkInfo.mLocalFilePath);
        contentValues.put("sortkey", localApkInfo.mSortKey);
        contentValues.put("iconRes", Integer.valueOf(localApkInfo.mAppIconRes));
        contentValues.put("launchCount", Integer.valueOf(localApkInfo.launchCount));
        contentValues.put("flags", Integer.valueOf(localApkInfo.flags));
        contentValues.put("signature", localApkInfo.signature);
        contentValues.put("manifest_md5", localApkInfo.manifestMd5);
        contentValues.put("lastLaunchTime", Long.valueOf(localApkInfo.mLastLaunchTime));
        contentValues.put("fakeLastLaunchTime", Long.valueOf(localApkInfo.mFakeLastLaunchTime));
        contentValues.put("dataUsage", Long.valueOf(localApkInfo.mDataUsage));
        contentValues.put("batteryUsage", Long.valueOf(localApkInfo.mBatteryUsage));
        contentValues.put("installeLocation", Byte.valueOf(localApkInfo.mInstalleLocation));
        contentValues.put("uid", Integer.valueOf(localApkInfo.mUid));
        contentValues.put("grayVersionCode", Integer.valueOf(localApkInfo.mGrayVersionCode));
        return getHelper().getWritableDatabaseWrapper().insert("local_appinfo", null, contentValues) > 0;
    }

    /* JADX INFO: finally extract failed */
    public boolean a(List<LocalApkInfo> list) {
        if (list == null || list.isEmpty()) {
            return false;
        }
        SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
        try {
            writableDatabaseWrapper.beginTransaction();
            SQLiteStatement compileStatement = writableDatabaseWrapper.compileStatement("update local_appinfo set sortkey=?, signature=?, manifest_md5=? where packagename=?");
            for (LocalApkInfo a2 : list) {
                a(compileStatement, a2);
                compileStatement.execute();
            }
            writableDatabaseWrapper.setTransactionSuccessful();
            writableDatabaseWrapper.endTransaction();
            return true;
        } catch (Throwable th) {
            writableDatabaseWrapper.endTransaction();
            throw th;
        }
    }

    public boolean b(List<LocalApkInfo> list) {
        SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
        try {
            writableDatabaseWrapper.beginTransaction();
            SQLiteStatement compileStatement = writableDatabaseWrapper.compileStatement("update local_appinfo set appid=? where packagename=?");
            for (LocalApkInfo next : list) {
                if (next.mAppid > 0) {
                    compileStatement.bindLong(1, next.mAppid);
                    compileStatement.bindString(2, next.mPackageName);
                    compileStatement.execute();
                }
            }
            writableDatabaseWrapper.setTransactionSuccessful();
            return true;
        } finally {
            writableDatabaseWrapper.endTransaction();
        }
    }

    public boolean a(List<String> list, long j) {
        SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
        try {
            writableDatabaseWrapper.beginTransaction();
            SQLiteStatement compileStatement = writableDatabaseWrapper.compileStatement("update local_appinfo set lastLaunchTime=?, fakeLastLaunchTime=?, launchCount=launchCount+1 where packagename=?");
            for (String bindString : list) {
                compileStatement.bindLong(1, j);
                compileStatement.bindLong(2, j);
                compileStatement.bindString(3, bindString);
                compileStatement.execute();
            }
            writableDatabaseWrapper.setTransactionSuccessful();
            return true;
        } finally {
            writableDatabaseWrapper.endTransaction();
        }
    }

    public boolean b(LocalApkInfo localApkInfo, long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("appname", localApkInfo.mAppName);
        contentValues.put("versioncode", Integer.valueOf(localApkInfo.mVersionCode));
        contentValues.put("versionname", localApkInfo.mVersionName);
        contentValues.put("appsize", Long.valueOf(localApkInfo.occupySize));
        contentValues.put("installtime", Long.valueOf(localApkInfo.mInstallDate));
        contentValues.put("filepath", localApkInfo.mLocalFilePath);
        contentValues.put("sortkey", localApkInfo.mSortKey);
        contentValues.put("iconRes", Integer.valueOf(localApkInfo.mAppIconRes));
        contentValues.put("launchCount", Integer.valueOf(localApkInfo.launchCount));
        contentValues.put("flags", Integer.valueOf(localApkInfo.flags));
        contentValues.put("signature", localApkInfo.signature);
        contentValues.put("manifest_md5", localApkInfo.manifestMd5);
        contentValues.put("lastLaunchTime", Long.valueOf(localApkInfo.mLastLaunchTime));
        contentValues.put("fakeLastLaunchTime", Long.valueOf(localApkInfo.mFakeLastLaunchTime));
        contentValues.put("dataUsage", Long.valueOf(localApkInfo.mDataUsage));
        contentValues.put("batteryUsage", Long.valueOf(localApkInfo.mBatteryUsage));
        contentValues.put("installeLocation", Byte.valueOf(localApkInfo.mInstalleLocation));
        contentValues.put("uid", Integer.valueOf(localApkInfo.mUid));
        contentValues.put("grayVersionCode", Integer.valueOf(localApkInfo.mGrayVersionCode));
        if (getHelper().getWritableDatabaseWrapper().update("local_appinfo", contentValues, "packagename=? and installed=?", new String[]{localApkInfo.mPackageName, Long.toString(j)}) > 0) {
            return true;
        }
        return false;
    }

    public synchronized int b(List<LocalApkInfo> list, long j) {
        SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
        try {
            writableDatabaseWrapper.beginTransaction();
            writableDatabaseWrapper.delete("local_appinfo", "installed= ?", new String[]{String.valueOf(j)});
            SQLiteStatement compileStatement = writableDatabaseWrapper.compileStatement("INSERT INTO local_appinfo(packagename,appname,versioncode,versionname,appsize,installtime,filepath,issysapp,issysupdated,sortkey,iconRes,launchCount,flags,signature,manifest_md5,installed,lastLaunchTime,fakeLastLaunchTime, dataUsage, batteryUsage, installeLocation, uid, grayVersionCode)values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            for (LocalApkInfo a2 : list) {
                a(compileStatement, a2, j);
                compileStatement.executeInsert();
            }
            writableDatabaseWrapper.setTransactionSuccessful();
            writableDatabaseWrapper.endTransaction();
        } catch (Throwable th) {
            writableDatabaseWrapper.endTransaction();
            throw th;
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistant.localres.model.LocalApkInfo> a(long r11) {
        /*
            r10 = this;
            r9 = 0
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "local_appinfo"
            r2 = 0
            java.lang.String r3 = "installed = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0040, all -> 0x004c }
            r5 = 0
            java.lang.String r6 = java.lang.Long.toString(r11)     // Catch:{ Exception -> 0x0040, all -> 0x004c }
            r4[r5] = r6     // Catch:{ Exception -> 0x0040, all -> 0x004c }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0040, all -> 0x004c }
            if (r1 == 0) goto L_0x0039
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0056 }
            if (r0 == 0) goto L_0x0039
        L_0x002c:
            com.tencent.assistant.localres.model.LocalApkInfo r0 = r10.a(r1)     // Catch:{ Exception -> 0x0056 }
            r8.add(r0)     // Catch:{ Exception -> 0x0056 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0056 }
            if (r0 != 0) goto L_0x002c
        L_0x0039:
            if (r1 == 0) goto L_0x003e
            r1.close()
        L_0x003e:
            r0 = r8
        L_0x003f:
            return r0
        L_0x0040:
            r0 = move-exception
            r1 = r9
        L_0x0042:
            r0.printStackTrace()     // Catch:{ all -> 0x0053 }
            if (r1 == 0) goto L_0x004a
            r1.close()
        L_0x004a:
            r0 = r8
            goto L_0x003f
        L_0x004c:
            r0 = move-exception
        L_0x004d:
            if (r9 == 0) goto L_0x0052
            r9.close()
        L_0x0052:
            throw r0
        L_0x0053:
            r0 = move-exception
            r9 = r1
            goto L_0x004d
        L_0x0056:
            r0 = move-exception
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.n.a(long):java.util.List");
    }

    public int a(LocalApkInfo localApkInfo) {
        return getHelper().getWritableDatabaseWrapper().delete("local_appinfo", "packagename=?", new String[]{localApkInfo.mPackageName});
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "local_appinfo";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists local_appinfo (_id INTEGER PRIMARY KEY AUTOINCREMENT,appid INTEGER,packagename TEXT,appname TEXT,versioncode INTEGER,versionname TEXT,appsize TEXT,installtime INTEGER,filepath TEXT,issysapp INTEGER,issysupdated INTEGER,sortkey TEXT,iconRes INTEGER,launchCount INTEGER,flags INTEGER,signature TEXT,manifest_md5 TEXT,installed INTEGER, lastLaunchTime INTEGER, fakeLastLaunchTime INTEGER, dataUsage INTEGER, batteryUsage INTEGER, installeLocation INTEGER, uid INTEGER,grayVersionCode INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i == 2 && i2 == 3) {
            return new String[]{"alter table local_appinfo add column grayVersionCode INTEGER;"};
        } else if (i != 12 || i2 != 13) {
            return null;
        } else {
            return new String[]{"alter table local_appinfo add column fakeLastLaunchTime INTEGER;"};
        }
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
