package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.download.m;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.DownloadTask;
import com.tencent.downloadsdk.storage.a.d;
import com.tencent.downloadsdk.storage.helper.SDKDBHelper;
import com.tencent.downloadsdk.storage.helper.SqliteHelper;
import java.util.Iterator;

/* compiled from: ProGuard */
public class k implements d {

    /* renamed from: a  reason: collision with root package name */
    j f1239a = new j();

    public k() {
    }

    public k(Context context) {
    }

    public DownloadInfo a(Cursor cursor) {
        DownloadInfo downloadInfo = new DownloadInfo();
        downloadInfo.downloadState = SimpleDownloadInfo.DownloadState.values()[cursor.getInt(cursor.getColumnIndexOrThrow("state"))];
        if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.DELETED) {
            downloadInfo.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
        }
        downloadInfo.fileType = SimpleDownloadInfo.DownloadType.values()[cursor.getInt(cursor.getColumnIndexOrThrow("fileType"))];
        downloadInfo.downloadTicket = cursor.getString(cursor.getColumnIndexOrThrow("downloadTicket"));
        downloadInfo.appId = cursor.getLong(cursor.getColumnIndexOrThrow("appId"));
        downloadInfo.apkId = cursor.getLong(cursor.getColumnIndexOrThrow(CommentDetailTabView.PARAMS_APK_ID));
        downloadInfo.packageName = cursor.getString(cursor.getColumnIndexOrThrow("packageName"));
        downloadInfo.name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        downloadInfo.iconUrl = cursor.getString(cursor.getColumnIndexOrThrow("iconUrl"));
        downloadInfo.versionName = cursor.getString(cursor.getColumnIndexOrThrow("versionName"));
        downloadInfo.versionCode = (int) cursor.getLong(cursor.getColumnIndexOrThrow(CommentDetailTabView.PARAMS_VERSION_CODE));
        downloadInfo.apkUrlList = ct.b(cursor.getString(cursor.getColumnIndexOrThrow("apkUrl")));
        downloadInfo.fileSize = cursor.getLong(cursor.getColumnIndexOrThrow("fileSize"));
        downloadInfo.fileMd5 = cursor.getString(cursor.getColumnIndexOrThrow("fileMd5"));
        downloadInfo.signatrue = cursor.getString(cursor.getColumnIndexOrThrow("signatrue"));
        downloadInfo.sllApkUrlList = ct.b(cursor.getString(cursor.getColumnIndexOrThrow("sllApkUrl")));
        downloadInfo.sllFileSize = cursor.getLong(cursor.getColumnIndexOrThrow("sllFileSize"));
        downloadInfo.sllFileMd5 = cursor.getString(cursor.getColumnIndexOrThrow("sllFileMd5"));
        downloadInfo.sllUpdate = cursor.getInt(cursor.getColumnIndexOrThrow("sllUpdate"));
        downloadInfo.isUpdate = cursor.getInt(cursor.getColumnIndexOrThrow("isUpdate"));
        downloadInfo.createTime = cursor.getLong(cursor.getColumnIndexOrThrow("createTime"));
        if (downloadInfo.statInfo == null) {
            downloadInfo.statInfo = new StatInfo();
        }
        downloadInfo.statInfo.sourceScene = (int) cursor.getLong(cursor.getColumnIndexOrThrow("sourceSence"));
        downloadInfo.statInfo.f3356a = cursor.getLong(cursor.getColumnIndexOrThrow("sourceVersion"));
        downloadInfo.statInfo.extraData = cursor.getString(cursor.getColumnIndexOrThrow("st_extraInfo"));
        downloadInfo.statInfo.searchId = cursor.getLong(cursor.getColumnIndexOrThrow("searchId"));
        downloadInfo.response = new m();
        downloadInfo.response.f1263a = cursor.getLong(cursor.getColumnIndexOrThrow("received_length"));
        downloadInfo.response.b = cursor.getLong(cursor.getColumnIndexOrThrow("total_length"));
        downloadInfo.response.d = DownloadTask.PRIORITY.values()[cursor.getInt(cursor.getColumnIndexOrThrow("priority"))];
        downloadInfo.response.e = cursor.getLong(cursor.getColumnIndexOrThrow("last_modify"));
        downloadInfo.response.f = cursor.getInt(cursor.getColumnIndexOrThrow("fakePercent"));
        downloadInfo.hostAppId = cursor.getString(cursor.getColumnIndexOrThrow("hostAppId"));
        downloadInfo.via = cursor.getString(cursor.getColumnIndexOrThrow("via"));
        downloadInfo.uin = cursor.getString(cursor.getColumnIndexOrThrow("uin"));
        downloadInfo.uinType = cursor.getString(cursor.getColumnIndexOrThrow("uinType"));
        downloadInfo.downloadEndTime = cursor.getLong(cursor.getColumnIndexOrThrow("download_end_time"));
        downloadInfo.grayVersionCode = cursor.getInt(cursor.getColumnIndexOrThrow("grayVersionCode"));
        downloadInfo.channelId = cursor.getString(cursor.getColumnIndexOrThrow("channelId"));
        downloadInfo.setFilePath(cursor.getString(cursor.getColumnIndexOrThrow("filePath")));
        downloadInfo.actionFlag = (byte) cursor.getInt(cursor.getColumnIndexOrThrow("actionFlag"));
        downloadInfo.uiType = SimpleDownloadInfo.UIType.values()[cursor.getInt(cursor.getColumnIndexOrThrow("uiType"))];
        downloadInfo.sdkId = cursor.getString(cursor.getColumnIndexOrThrow("sdkId"));
        downloadInfo.categoryId = cursor.getLong(cursor.getColumnIndexOrThrow("categoryId"));
        downloadInfo.downloadingPath = cursor.getString(cursor.getColumnIndexOrThrow("downloadingPath"));
        downloadInfo.statInfo.callerVia = cursor.getString(cursor.getColumnIndexOrThrow("stVia"));
        downloadInfo.statInfo.callerUin = cursor.getString(cursor.getColumnIndexOrThrow("stUin"));
        downloadInfo.minQLauncherVersionCode = cursor.getInt(cursor.getColumnIndex("minQLauncherVersionCode"));
        downloadInfo.maxQLauncherVersionCode = cursor.getInt(cursor.getColumnIndex("maxQLauncherVersionCode"));
        downloadInfo.themeVersionCode = cursor.getInt(cursor.getColumnIndex("themeVersionCode"));
        downloadInfo.localVersionCode = cursor.getInt(cursor.getColumnIndex("localVersionCode"));
        downloadInfo.sllLocalManifestMd5 = cursor.getString(cursor.getColumnIndex("sllLocalManifestMd5"));
        downloadInfo.sllLocalVersionCode = cursor.getInt(cursor.getColumnIndex("sllLocalVersionCode"));
        downloadInfo.overWriteChannelId = (byte) cursor.getInt(cursor.getColumnIndex("overWriteChannelId"));
        downloadInfo.isAutoOpen = (byte) cursor.getInt(cursor.getColumnIndex("autoOpenInstalledApp"));
        return downloadInfo;
    }

    public void a(ContentValues contentValues, DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            contentValues.put("fileType", Integer.valueOf(downloadInfo.fileType.ordinal()));
            contentValues.put("downloadUrl", downloadInfo.downloadTicket);
            contentValues.put("downloadTicket", downloadInfo.downloadTicket);
            contentValues.put("appId", Long.valueOf(downloadInfo.appId));
            if (downloadInfo.apkId > 0) {
                contentValues.put(CommentDetailTabView.PARAMS_APK_ID, Long.valueOf(downloadInfo.apkId));
            }
            contentValues.put("packageName", downloadInfo.packageName);
            contentValues.put("name", downloadInfo.name);
            contentValues.put("iconUrl", downloadInfo.iconUrl);
            contentValues.put("versionName", downloadInfo.versionName);
            contentValues.put(CommentDetailTabView.PARAMS_VERSION_CODE, Integer.valueOf(downloadInfo.versionCode));
            contentValues.put("apkUrl", ct.a(downloadInfo.apkUrlList));
            contentValues.put("fileSize", Long.valueOf(downloadInfo.fileSize));
            contentValues.put("fileMd5", downloadInfo.fileMd5);
            contentValues.put("signatrue", downloadInfo.signatrue);
            contentValues.put("sllApkUrl", ct.a(downloadInfo.sllApkUrlList));
            contentValues.put("sllFileSize", Long.valueOf(downloadInfo.sllFileSize));
            contentValues.put("sllFileMd5", downloadInfo.sllFileMd5);
            contentValues.put("sllUpdate", Integer.valueOf(downloadInfo.sllUpdate));
            contentValues.put("state", Integer.valueOf(downloadInfo.downloadState.ordinal()));
            contentValues.put("isUpdate", Integer.valueOf(downloadInfo.isUpdate));
            contentValues.put("createTime", Long.valueOf(downloadInfo.createTime));
            if (downloadInfo.response != null) {
                contentValues.put("fakePercent", Integer.valueOf(downloadInfo.response.f));
            }
            contentValues.put("hostAppId", downloadInfo.hostAppId);
            contentValues.put("via", downloadInfo.via);
            contentValues.put("uin", downloadInfo.uin);
            contentValues.put("uinType", downloadInfo.uinType);
            contentValues.put("download_end_time", Long.valueOf(downloadInfo.downloadEndTime));
            contentValues.put("grayVersionCode", Integer.valueOf(downloadInfo.grayVersionCode));
            contentValues.put("channelId", downloadInfo.channelId);
            contentValues.put("filePath", downloadInfo.getFilePath());
            contentValues.put("actionFlag", Byte.valueOf(downloadInfo.actionFlag));
            contentValues.put("uiType", Integer.valueOf(downloadInfo.uiType.ordinal()));
            contentValues.put("sdkId", downloadInfo.sdkId);
            contentValues.put("categoryId", Long.valueOf(downloadInfo.categoryId));
            contentValues.put("downloadingPath", downloadInfo.downloadingPath);
            contentValues.put("minQLauncherVersionCode", Integer.valueOf(downloadInfo.minQLauncherVersionCode));
            contentValues.put("maxQLauncherVersionCode", Integer.valueOf(downloadInfo.maxQLauncherVersionCode));
            contentValues.put("themeVersionCode", Integer.valueOf(downloadInfo.themeVersionCode));
            contentValues.put("localVersionCode", Integer.valueOf(downloadInfo.localVersionCode));
            contentValues.put("sllLocalManifestMd5", downloadInfo.sllLocalManifestMd5);
            contentValues.put("sllLocalVersionCode", Integer.valueOf(downloadInfo.sllLocalVersionCode));
            contentValues.put("overWriteChannelId", Byte.valueOf(downloadInfo.overWriteChannelId));
            contentValues.put("autoOpenInstalledApp", Byte.valueOf(downloadInfo.isAutoOpen));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.download.DownloadInfo a(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            if (r0 != 0) goto L_0x003a
            com.tencent.downloadsdk.storage.helper.SqliteHelper r0 = r5.e()
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()
            java.lang.String r2 = "select * from downloadsinfo as a left outer join tbl_download as b on a.downloadTicket = b.id where a.downloadTicket = ? "
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0030, all -> 0x003c }
            r4 = 0
            r3[r4] = r6     // Catch:{ Exception -> 0x0030, all -> 0x003c }
            android.database.Cursor r2 = r0.rawQuery(r2, r3)     // Catch:{ Exception -> 0x0030, all -> 0x003c }
            if (r2 == 0) goto L_0x0048
            boolean r0 = r2.moveToFirst()     // Catch:{ Exception -> 0x0046 }
            if (r0 == 0) goto L_0x0048
            com.tencent.assistant.download.DownloadInfo r0 = r5.a(r2)     // Catch:{ Exception -> 0x0046 }
        L_0x0027:
            r5.b(r0)     // Catch:{ Exception -> 0x0046 }
            if (r2 == 0) goto L_0x002f
            r2.close()
        L_0x002f:
            return r0
        L_0x0030:
            r0 = move-exception
            r2 = r1
        L_0x0032:
            r0.printStackTrace()     // Catch:{ all -> 0x0044 }
            if (r2 == 0) goto L_0x003a
            r2.close()
        L_0x003a:
            r0 = r1
            goto L_0x002f
        L_0x003c:
            r0 = move-exception
            r2 = r1
        L_0x003e:
            if (r2 == 0) goto L_0x0043
            r2.close()
        L_0x0043:
            throw r0
        L_0x0044:
            r0 = move-exception
            goto L_0x003e
        L_0x0046:
            r0 = move-exception
            goto L_0x0032
        L_0x0048:
            r0 = r1
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.k.a(java.lang.String):com.tencent.assistant.download.DownloadInfo");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.tencent.assistant.download.DownloadInfo> a() {
        /*
            r5 = this;
            r2 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.tencent.downloadsdk.storage.helper.SqliteHelper r1 = r5.e()
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.String r3 = "select * from downloadsinfo as a left outer join tbl_download as b on a.downloadTicket = b.id  order by _id desc"
            r4 = 0
            android.database.Cursor r2 = r1.rawQuery(r3, r4)     // Catch:{ Exception -> 0x0035 }
            if (r2 == 0) goto L_0x002f
            boolean r1 = r2.moveToFirst()     // Catch:{ Exception -> 0x0035 }
            if (r1 == 0) goto L_0x002f
        L_0x001d:
            com.tencent.assistant.download.DownloadInfo r1 = r5.a(r2)     // Catch:{ Exception -> 0x0035 }
            r5.b(r1)     // Catch:{ Exception -> 0x0035 }
            if (r1 == 0) goto L_0x0029
            r0.add(r1)     // Catch:{ Exception -> 0x0035 }
        L_0x0029:
            boolean r1 = r2.moveToNext()     // Catch:{ Exception -> 0x0035 }
            if (r1 != 0) goto L_0x001d
        L_0x002f:
            if (r2 == 0) goto L_0x0034
            r2.close()
        L_0x0034:
            return r0
        L_0x0035:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x003f }
            if (r2 == 0) goto L_0x0034
            r2.close()
            goto L_0x0034
        L_0x003f:
            r0 = move-exception
            if (r2 == 0) goto L_0x0045
            r2.close()
        L_0x0045:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.k.a():java.util.ArrayList");
    }

    public synchronized void a(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            boolean z = true;
            try {
                SQLiteDatabase writableDatabase = e().getWritableDatabase();
                if (a(downloadInfo, writableDatabase) <= 0) {
                    ContentValues contentValues = new ContentValues();
                    a(contentValues, downloadInfo);
                    writableDatabase.insert("downloadsinfo", null, contentValues);
                }
            } catch (Exception e) {
                e.printStackTrace();
                z = false;
            } catch (OutOfMemoryError e2) {
                e2.printStackTrace();
                z = false;
            }
            if (z) {
                c(downloadInfo);
            }
        }
    }

    private int a(DownloadInfo downloadInfo, SQLiteDatabase sQLiteDatabase) {
        if (downloadInfo == null) {
            return -1;
        }
        try {
            ContentValues contentValues = new ContentValues();
            a(contentValues, downloadInfo);
            int update = sQLiteDatabase.update("downloadsinfo", contentValues, "downloadTicket = ?", new String[]{downloadInfo.downloadTicket});
            if (update <= 0) {
                return 0;
            }
            return update;
        } catch (Throwable th) {
            th.printStackTrace();
            return -2;
        }
    }

    public void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                e().getWritableDatabase().delete("downloadsinfo", "downloadTicket = ?", new String[]{str});
            } catch (Exception e) {
            }
        }
        c(str);
    }

    public int b() {
        return 1;
    }

    public String c() {
        return "downloadsinfo";
    }

    public String d() {
        return "CREATE TABLE if not exists downloadsinfo(_id INTEGER PRIMARY KEY AUTOINCREMENT,fileType TEXT,downloadUrl TEXT UNIQUE,appId INTEGER,packageName TEXT,name TEXT,iconUrl TEXT,downloadTimes INTEGER,rating INTEGER,versionName TEXT,versionCode INTEGER,apkUrl TEXT,fileSize INTEGER,fileMd5 TEXT,newFeature TEXT,signatrue TEXT, apkDate INTEGER, sllApkUrl TEXT, sllFileSize INTEGER, sllFileMd5 TEXT,sllUpdate INTEGER,isUpdate INTEGER,flag INTEGER, mergeFilePath TEXT,createTime INTEGER,sourceSence INTEGER,sourceVersion INTEGER,st_extraInfo TEXT,state INTEGER,downloadTicket TEXT,subType INTEGER,apkId INTEGER,searchId INTEGER,fakePercent INTEGER,hostAppId TEXT,hostPackageName TEXT,hostVersionCode TEXT,via TEXT,taskId TEXT,uin TEXT,uinType TEXT,download_end_time INTEGER,grayVersionCode INTEGER,filePath TEXT ,channelId TEXT,actionFlag INTEGER,uiType INTEGER,categoryId INTEGER,sdkId TEXT,downloadingPath TEXT,stVia TEXT,stUin TEXT,stBeaconSN INTEGER,stSourceBeaconSN INTEGER,minQLauncherVersionCode INTEGER DEFAULT 0,maxQLauncherVersionCode INTEGER DEFAULT 0,themeVersionCode INTEGER DEFAULT 0,localVersionCode INTEGER,sllLocalManifestMd5 TEXT,sllLocalVersionCode INTEGER,overWriteChannelId INTEGER,autoOpenInstalledApp INTEGER)";
    }

    public String[] a(int i, int i2) {
        if (i == 1 && i2 == 2) {
            return new String[]{d()};
        } else if (i == 2 && i2 == 3) {
            return new String[]{"alter table downloadsinfo add column overWriteChannelId INTEGER;"};
        } else if (i2 != 5) {
            return null;
        } else {
            return new String[]{"alter table downloadsinfo add column autoOpenInstalledApp INTEGER;"};
        }
    }

    public void a(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper e() {
        return SDKDBHelper.getDBHelper(DownloadManager.a().b());
    }

    public void b(int i, int i2, SQLiteDatabase sQLiteDatabase) {
        if (i == 1 && i2 == 2) {
            b(sQLiteDatabase);
        }
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
        b(sQLiteDatabase);
    }

    private void b(SQLiteDatabase sQLiteDatabase) {
        int i;
        Cursor cursor = null;
        try {
            cursor = sQLiteDatabase.rawQuery("SELECT DISTINCT value FROM globalkv WHERE key='is_data_moved' LIMIT 1", null);
            if (cursor.moveToFirst()) {
                i = Integer.parseInt(cursor.getString(0));
            } else {
                i = -1;
            }
            if (i == -1) {
                o oVar = new o();
                Iterator<DownloadInfo> it = oVar.a().iterator();
                while (it.hasNext()) {
                    ContentValues contentValues = new ContentValues();
                    a(contentValues, it.next());
                    sQLiteDatabase.replace("downloadsinfo", null, contentValues);
                }
                oVar.b();
                sQLiteDatabase.execSQL("REPLACE INTO globalkv (key, value) VALUES ('is_data_moved', '1');");
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
        } catch (SQLException e2) {
            e2.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    private void b(DownloadInfo downloadInfo) {
        StatInfo a2;
        if (downloadInfo != null && (a2 = this.f1239a.a(downloadInfo.downloadTicket)) != null) {
            downloadInfo.statInfo.scene = a2.scene;
            downloadInfo.statInfo.slotId = a2.slotId;
            downloadInfo.statInfo.sourceSceneSlotId = a2.sourceSceneSlotId;
            downloadInfo.statInfo.status = a2.status;
            downloadInfo.statInfo.recommendId = a2.recommendId;
            downloadInfo.statInfo.contentId = a2.contentId;
            downloadInfo.statInfo.pushInfo = a2.pushInfo;
            downloadInfo.statInfo.searchPreId = a2.searchPreId;
            downloadInfo.statInfo.expatiation = a2.expatiation;
            downloadInfo.statInfo.callerPackageName = a2.callerPackageName;
            downloadInfo.statInfo.traceId = a2.traceId;
            if (a2.sourceScene != 0) {
                downloadInfo.statInfo.sourceScene = a2.sourceScene;
            }
            if (a2.searchId != 0) {
                downloadInfo.statInfo.searchId = a2.searchId;
            }
            if (!TextUtils.isEmpty(a2.extraData)) {
                downloadInfo.statInfo.extraData = a2.extraData;
            }
            if (!TextUtils.isEmpty(a2.callerVia)) {
                downloadInfo.statInfo.callerVia = a2.callerVia;
            }
            if (!TextUtils.isEmpty(a2.callerUin)) {
                downloadInfo.statInfo.callerUin = a2.callerUin;
            }
            if (!TextUtils.isEmpty(a2.callerVersionCode)) {
                downloadInfo.statInfo.callerVersionCode = a2.callerVersionCode;
            } else {
                downloadInfo.statInfo.callerVersionCode = downloadInfo.hostVersionCode;
            }
        }
    }

    private void c(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            this.f1239a.a(downloadInfo.downloadTicket, downloadInfo.statInfo);
        }
    }

    private void c(String str) {
        this.f1239a.b(str);
    }
}
