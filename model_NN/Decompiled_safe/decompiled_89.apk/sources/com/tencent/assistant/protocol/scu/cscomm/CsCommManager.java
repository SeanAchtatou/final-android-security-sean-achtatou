package com.tencent.assistant.protocol.scu.cscomm;

import android.content.Context;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.protocol.jce.PkgReq;
import com.tencent.assistant.protocol.jce.PkgRsp;
import com.tencent.assistant.protocol.jce.Request;
import com.tencent.assistant.protocol.jce.Response;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.util.HashMap;

/* compiled from: ProGuard */
public class CsCommManager {

    /* renamed from: a  reason: collision with root package name */
    private static CsCommManager f2439a = null;
    private boolean b = false;

    public static native void clearAuthTicket();

    public static native int startAuthComm(Context context);

    public native int decryptResponse(PkgRsp pkgRsp, Response response);

    public native int encryptRequest(Request request, PkgReq pkgReq);

    static {
        Log.i("cscomm", "load yyb_cryptor lib");
        try {
            System.loadLibrary("yyb_cscomm");
            Log.i("cscomm", "startAuthComm");
            startAuthComm(AstApp.i());
        } catch (Throwable th) {
            a(true, th.getMessage());
            Log.e("cscomm", "load yyb_cscomm.so failed : ", th);
        }
    }

    public static synchronized CsCommManager a() {
        CsCommManager csCommManager;
        synchronized (CsCommManager.class) {
            if (f2439a == null) {
                f2439a = new CsCommManager();
            }
            csCommManager = f2439a;
        }
        return csCommManager;
    }

    public static void a(boolean z, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUA());
        hashMap.put("B3", t.g());
        hashMap.put("B4", z ? "reload" : "firstLoad");
        if (str == null) {
            str = Constants.STR_EMPTY;
        }
        hashMap.put("B5", str);
        a.a("SOLoadFailed", true, -1, -1, hashMap, true);
    }
}
