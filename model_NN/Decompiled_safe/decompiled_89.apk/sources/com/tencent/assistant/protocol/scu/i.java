package com.tencent.assistant.protocol.scu;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.ProtocolDecoder;
import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.g;
import com.tencent.assistant.protocol.j;
import com.tencent.assistant.protocol.jce.AuthRequest;
import com.tencent.assistant.protocol.jce.Request;
import com.tencent.assistant.protocol.jce.Response;
import com.tencent.assistant.protocol.jce.RspHead;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import com.tencent.assistant.protocol.l;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class i implements g, h, p {

    /* renamed from: a  reason: collision with root package name */
    private static i f2441a;
    private ConcurrentHashMap<Integer, m> b = new ConcurrentHashMap<>();
    private n c = new n();
    private SecurityRequestHoldQueue d = new SecurityRequestHoldQueue();
    private HandlerThread e = null;
    private Handler f = null;

    private i() {
        this.d.a(this);
        d.a().a(this);
    }

    public static synchronized i c() {
        i iVar;
        synchronized (i.class) {
            if (f2441a == null) {
                f2441a = new i();
            }
            iVar = f2441a;
        }
        return iVar;
    }

    public int a(int i, List<JceStruct> list, g gVar, ProtocolDecoder protocolDecoder) {
        if (d.a().b()) {
            return a(i, list, gVar, protocolDecoder, (byte) 1);
        }
        if (this.c.a(list)) {
            return b(i, list, gVar, protocolDecoder);
        }
        this.d.a(a(i, list, gVar, (l) null, protocolDecoder), (byte) 1);
        d.a().a((byte) 2);
        return 0;
    }

    public int a(int i, List<JceStruct> list, g gVar, ProtocolDecoder protocolDecoder, byte b2) {
        l a2 = k.a(i, list);
        m a3 = a(i, list, gVar, a2, protocolDecoder);
        this.b.put(Integer.valueOf(i), a3);
        return a(a3, a2.f2443a, b2);
    }

    private int b(int i, List<JceStruct> list, g gVar, ProtocolDecoder protocolDecoder) {
        return a(i, d.a().a(list), gVar, protocolDecoder, (byte) 2);
    }

    private int a(m mVar, Request request, int i) {
        if (mVar == null || request == null) {
            return -1;
        }
        int a2 = j.a().a(mVar.f, request, mVar.h, this);
        if (e.a().n()) {
            a(mVar.f, i, mVar.f2444a);
        }
        if (a2 != -1) {
            return 1;
        }
        this.b.remove(Integer.valueOf(mVar.f));
        return -1;
    }

    private m a(int i, List<JceStruct> list, g gVar, l lVar, ProtocolDecoder protocolDecoder) {
        m mVar = new m();
        mVar.b = gVar;
        mVar.f2444a = list;
        mVar.c = protocolDecoder;
        mVar.f = i;
        mVar.e = System.currentTimeMillis();
        if (lVar != null) {
            mVar.h = lVar.b;
            Request request = lVar.f2443a;
            if (!(request == null || request.head == null || request.head.t != 1)) {
                mVar.g = true;
            }
        }
        return mVar;
    }

    public void a(RspHead rspHead) {
        if (rspHead != null) {
            if (rspHead.e == 0) {
                e.a().a(rspHead);
            }
            d.a().a(rspHead.l, rspHead.m);
        }
    }

    public void a(int i, int i2, Request request, Response response) {
        m remove = this.b.remove(Integer.valueOf(i));
        if (remove != null) {
            RspHead rspHead = null;
            if (response != null) {
                rspHead = response.head;
            }
            boolean z = rspHead != null && rspHead.l == -5;
            boolean z2 = i2 == -1018;
            if ((z || z2) && !remove.g) {
                int i3 = remove.d;
                int currentTimeMillis = (int) ((System.currentTimeMillis() - remove.e) / 1000);
                if (i3 < 1 && currentTimeMillis < e.a().q()) {
                    if (!this.c.a(remove.f2444a)) {
                        this.d.a(remove, (byte) 2);
                        if (e.a().n()) {
                            a(i, remove, rspHead, i2, null, true);
                            return;
                        }
                        return;
                    } else if (z2) {
                        this.d.a(remove, (byte) 2);
                        if (e.a().n()) {
                            a(i, remove, rspHead, i2, null, true);
                            return;
                        }
                        return;
                    }
                }
            }
            if (i2 == -1019 && !remove.g) {
                if (d.a().c()) {
                    a(remove, request, 5);
                } else {
                    this.d.a(remove, (byte) 2);
                }
            }
            a(i2, remove, response);
        }
    }

    private void a(int i, m mVar, Response response) {
        boolean z = false;
        g gVar = mVar.b;
        List<RequestResponePair> a2 = k.a(i, mVar.f2444a, response, mVar.c);
        RspHead rspHead = null;
        if (response != null) {
            rspHead = response.head;
        }
        if (e.a().n()) {
            a(mVar.f, mVar, rspHead, i, a2, false);
        }
        if (mVar.g && a2 != null && a2.size() > 1) {
            Iterator<RequestResponePair> it = a2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                RequestResponePair next = it.next();
                if (next.request instanceof AuthRequest) {
                    a2.remove(next);
                    ArrayList arrayList = new ArrayList(1);
                    arrayList.add(next);
                    d.a().a(rspHead);
                    d.a().onProtocoRequestFinish(mVar.f, i, arrayList);
                    break;
                }
            }
        }
        if (i == 0) {
            if (a2 != null && a2.size() > 0) {
                int i2 = 0;
                while (true) {
                    if (i2 >= a2.size()) {
                        break;
                    } else if (a2.get(i2).errorCode == 0) {
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            z = true;
            if (z) {
                i = -841;
            }
        }
        if (gVar != null) {
            gVar.onProtocoRequestFinish(mVar.f, i, a2);
        }
    }

    public void b(int i) {
        c(i);
    }

    private void c(int i) {
        m remove = this.b.remove(Integer.valueOf(i));
        if (remove == null || !remove.g) {
            j.a().a(i);
            return;
        }
        remove.b = null;
        if (e.a().n()) {
            a(remove.f, remove.g);
        }
    }

    public void a(int i) {
        synchronized (this.d) {
            while (this.d.size() > 0) {
                m mVar = (m) this.d.remove();
                if (mVar != null) {
                    a(i, mVar, (Response) null);
                }
            }
        }
    }

    public void a() {
        synchronized (this.d) {
            while (this.d.size() > 0) {
                m mVar = (m) this.d.remove();
                if (mVar != null) {
                    a(mVar.f, mVar.f2444a, mVar.b, mVar.c, (byte) 3);
                }
            }
        }
    }

    public void b() {
    }

    public void a(List<m> list) {
        if (list != null && list.size() > 0) {
            for (m a2 : list) {
                a(-875, a2, (Response) null);
            }
        }
    }

    public void a(long j) {
        if (this.e == null) {
            this.e = new HandlerThread("ProtocolSecurityManager");
            this.e.start();
            Looper looper = this.e.getLooper();
            if (looper != null) {
                this.f = new Handler(looper);
            }
            if (this.f == null) {
                this.e.stop();
                this.e = null;
            }
        }
        if (this.f != null) {
            this.f.postDelayed(new j(this), j);
        }
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        synchronized (this.d) {
            m mVar = (m) this.d.poll();
            if (mVar != null) {
                a(i, mVar, (Response) null);
            }
        }
    }

    public void a(int i, int i2, List<JceStruct> list) {
        if (!l.a(list) && list != null && list.size() != 0) {
            StatCSChannelData statCSChannelData = new StatCSChannelData();
            statCSChannelData.b = 2;
            statCSChannelData.c = 1;
            statCSChannelData.d = 1;
            statCSChannelData.f = i;
            StringBuilder sb = new StringBuilder();
            sb.append("cmd:");
            for (JceStruct a2 : list) {
                sb.append(l.a(a2)).append(";");
            }
            sb.append("type: " + i2);
            statCSChannelData.g = sb.toString();
            e.a().a(statCSChannelData);
        }
    }

    public void a(int i, m mVar, RspHead rspHead, int i2, List<RequestResponePair> list, boolean z) {
        int i3 = 1;
        if (mVar != null && mVar.f2444a != null && mVar.f2444a.size() != 0) {
            StatCSChannelData statCSChannelData = new StatCSChannelData();
            statCSChannelData.b = 2;
            statCSChannelData.c = 1;
            statCSChannelData.d = 2;
            statCSChannelData.f = i;
            StringBuilder sb = new StringBuilder();
            sb.append("errorCode:").append(i2).append(";");
            StringBuilder append = sb.append("isRehold:");
            if (!z) {
                i3 = 0;
            }
            append.append(i3).append(";");
            if (!z && list != null && list.size() > 0) {
                sb.append("RspErrorCode:");
                for (RequestResponePair next : list) {
                    sb.append("cmdId:").append(k.a(next.request)).append(" rsp:").append(next.errorCode).append(";");
                    if (next.response != null) {
                        try {
                            Field declaredField = next.response.getClass().getDeclaredField("ret");
                            declaredField.setAccessible(true);
                            sb.append(" ret:").append(((Integer) declaredField.get(next.response)).intValue()).append(";");
                        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e2) {
                        }
                    }
                }
            }
            if (rspHead != null) {
                sb.append(" csTicketState:").append(rspHead.l).append(";");
            }
            statCSChannelData.g = sb.toString();
            e.a().a(statCSChannelData);
        }
    }

    public void a(int i, boolean z) {
        StringBuilder append;
        int i2 = 1;
        StatCSChannelData statCSChannelData = new StatCSChannelData();
        statCSChannelData.b = 2;
        statCSChannelData.c = 1;
        statCSChannelData.d = 3;
        statCSChannelData.f = i;
        if (!z) {
            i2 = 0;
        }
        append.append(i2).append(";");
        statCSChannelData.g = "addAuthFlag:";
        e.a().a(statCSChannelData);
    }
}
