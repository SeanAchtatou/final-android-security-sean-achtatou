package com.tencent.assistant.download;

import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.model.StatInfo;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1252a;
    final /* synthetic */ StatInfo b;
    final /* synthetic */ boolean c;
    final /* synthetic */ a d;

    b(a aVar, SimpleAppModel simpleAppModel, StatInfo statInfo, boolean z) {
        this.d = aVar;
        this.f1252a = simpleAppModel;
        this.b = statInfo;
        this.c = z;
    }

    public void run() {
        if (this.f1252a != null && this.f1252a.h <= r.d()) {
            DownloadInfo createDownloadInfo = DownloadInfo.createDownloadInfo(this.f1252a, this.b);
            createDownloadInfo.scene = this.b.sourceScene;
            createDownloadInfo.statInfo.recommendId = this.b.recommendId;
            if (be.a().b(createDownloadInfo)) {
                be.a().d(createDownloadInfo);
                return;
            }
            if (this.c) {
                createDownloadInfo.downloadState = SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI;
            } else if (createDownloadInfo.downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI) {
                createDownloadInfo.downloadState = SimpleDownloadInfo.DownloadState.INIT;
            }
            DownloadProxy.a().c(createDownloadInfo);
        }
    }
}
