package com.tencent.assistant.download;

import android.os.Handler;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public abstract class k {

    /* renamed from: a  reason: collision with root package name */
    private static Object f1261a = new Object();
    private static Handler b = null;

    public static Handler a() {
        if (b == null) {
            synchronized (f1261a) {
                if (b == null) {
                    b = ba.a("DownLogicHandler");
                }
            }
        }
        return b;
    }
}
