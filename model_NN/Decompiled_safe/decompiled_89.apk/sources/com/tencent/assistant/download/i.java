package com.tencent.assistant.download;

import com.tencent.assistantv2.st.model.StatInfo;
import java.util.List;

/* compiled from: ProGuard */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1259a;
    final /* synthetic */ StatInfo b;
    final /* synthetic */ a c;

    i(a aVar, List list, StatInfo statInfo) {
        this.c = aVar;
        this.f1259a = list;
        this.b = statInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, boolean):boolean
     arg types: [com.tencent.assistant.download.a, int]
     candidates:
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, java.util.List):java.util.List
      com.tencent.assistant.download.a.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void
      com.tencent.assistant.download.a.a(long, long):boolean
      com.tencent.assistant.download.a.a(java.lang.String, int):boolean
      com.tencent.assistant.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$UIType):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.assistant.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, boolean):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, boolean):boolean */
    public void run() {
        if (this.c.c() == 0) {
            List a2 = this.c.a(this.f1259a);
            if (a2 != null && !a2.isEmpty()) {
                this.c.b(a2, this.b);
            }
            boolean unused = this.c.g = false;
        }
    }
}
