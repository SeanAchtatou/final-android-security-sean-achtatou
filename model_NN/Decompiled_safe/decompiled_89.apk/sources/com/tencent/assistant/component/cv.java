package com.tencent.assistant.component;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class cv extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RollTextView f1004a;

    cv(RollTextView rollTextView) {
        this.f1004a = rollTextView;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 100000:
                if (this.f1004a.mRate > 0.0d && this.f1004a.mCurrentValue < this.f1004a.mDestValue) {
                    this.f1004a.setText(this.f1004a.valueToString(this.f1004a.getSizeValue(this.f1004a.mCurrentValue)));
                    RollTextView.access$118(this.f1004a, this.f1004a.mRate);
                    this.f1004a.mHandler.sendEmptyMessageDelayed(100000, 50);
                } else if (this.f1004a.mRate >= 0.0d || this.f1004a.mCurrentValue <= this.f1004a.mDestValue) {
                    this.f1004a.setText(this.f1004a.valueToString(this.f1004a.getSizeValue(this.f1004a.mDestValue)));
                    double unused = this.f1004a.mCurrentValue = this.f1004a.mDestValue;
                } else {
                    this.f1004a.setText(this.f1004a.valueToString(this.f1004a.getSizeValue(this.f1004a.mCurrentValue)));
                    RollTextView.access$118(this.f1004a, this.f1004a.mRate);
                    this.f1004a.mHandler.sendEmptyMessageDelayed(100000, 50);
                }
                this.f1004a.setUnit(this.f1004a.mCurrentValue);
                return;
            default:
                return;
        }
    }
}
