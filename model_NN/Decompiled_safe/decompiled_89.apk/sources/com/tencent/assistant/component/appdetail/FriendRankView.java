package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.CustomTextView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.AppHotFriend;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.component.RankNormalListView;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class FriendRankView extends RelativeLayout implements CustomTextView.CustomTextViewInterface {

    /* renamed from: a  reason: collision with root package name */
    String f906a = RankNormalListView.ST_HIDE_INSTALLED_APPS;
    private Context b;
    private CustomTextView c;
    private TXImageView d;
    private TextView e;
    private TextView f;
    private TXImageView g;
    private TextView h;
    private TextView i;
    private TXImageView j;
    private TextView k;
    private TextView l;

    public FriendRankView(Context context) {
        super(context);
        a(context);
    }

    public FriendRankView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public FriendRankView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
    }

    private void a(Context context) {
        this.b = context;
        LayoutInflater.from(this.b).inflate((int) R.layout.appdetail_friend_rank_view, this);
        this.c = (CustomTextView) findViewById(R.id.title);
        this.d = (TXImageView) findViewById(R.id.picture1);
        this.e = (TextView) findViewById(R.id.rank1);
        this.f = (TextView) findViewById(R.id.nick_name1);
        this.g = (TXImageView) findViewById(R.id.picture2);
        this.h = (TextView) findViewById(R.id.rank2);
        this.i = (TextView) findViewById(R.id.nick_name2);
        this.j = (TXImageView) findViewById(R.id.picture3);
        this.k = (TextView) findViewById(R.id.rank3);
        this.l = (TextView) findViewById(R.id.nick_name3);
        this.c.setStListener(this);
    }

    public void refresh(String str, ArrayList<AppHotFriend> arrayList) {
        if (!TextUtils.isEmpty(str)) {
            this.c.setText(str);
            this.c.setVisibility(0);
        } else {
            this.c.setVisibility(8);
        }
        if (arrayList.size() > 2) {
            this.d.updateImageView(arrayList.get(0).c, R.drawable.common_icon_useravarta_default, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.e.setText(String.format(this.b.getString(R.string.friend_rank), Integer.valueOf(arrayList.get(0).d)));
            this.f.setText(arrayList.get(0).b);
            this.g.updateImageView(arrayList.get(1).c, R.drawable.common_icon_useravarta_default, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.h.setText(String.format(this.b.getString(R.string.friend_rank), Integer.valueOf(arrayList.get(1).d)));
            this.i.setText(arrayList.get(1).b);
            this.j.updateImageView(arrayList.get(2).c, R.drawable.common_icon_useravarta_default, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.k.setText(String.format(this.b.getString(R.string.friend_rank), Integer.valueOf(arrayList.get(2).d)));
            this.l.setText(arrayList.get(2).b);
            setVisibility(0);
            return;
        }
        setVisibility(8);
    }

    public void viewExposureST() {
        if (this.b instanceof AppDetailActivityV5) {
            STInfoV2 i2 = ((AppDetailActivityV5) this.b).i();
            i2.slotId = a.a(this.f906a, "001");
            k.a(i2);
        }
    }
}
