package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public abstract class TXLoadingLayoutBase extends FrameLayout {
    protected TXScrollViewBase.ScrollDirection mScrollDirection;
    protected TXScrollViewBase.ScrollMode mScrollMode;

    public abstract int getContentSize();

    public abstract int getTriggerSize();

    public abstract void hideAllSubViews();

    public abstract void loadFail();

    public abstract void loadFinish(String str);

    public abstract void loadSuc();

    public abstract void onPull(int i);

    public abstract void pullToRefresh();

    public abstract void refreshFail(String str);

    public abstract void refreshSuc();

    public abstract void refreshing();

    public abstract void releaseToRefresh();

    public abstract void reset();

    public abstract void setHeight(int i);

    public abstract void setWidth(int i);

    public abstract void showAllSubViews();

    public TXLoadingLayoutBase(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context);
        this.mScrollDirection = scrollDirection;
        this.mScrollMode = scrollMode;
    }

    public TXLoadingLayoutBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
