package com.tencent.assistant.component;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Scroller;
import java.util.LinkedList;
import java.util.Queue;

/* compiled from: ProGuard */
public class HorizontalListView extends AdapterView<ListAdapter> {
    private static final String TAG = HorizontalListView.class.getSimpleName();
    private static final int TOUCH_STATE_REST = 0;
    private static final int TOUCH_STATE_SCROLLING = 1;
    int childMassureHight = -1;
    protected ListAdapter mAdapter;
    public boolean mAlwaysOverrideTouch = true;
    private float mAngleSlop = 0.577f;
    /* access modifiers changed from: private */
    public int mChildWidth = 0;
    protected int mCurrentX;
    /* access modifiers changed from: private */
    public boolean mDataChanged = false;
    private DataSetObserver mDataObserver = new as(this);
    private int mDelta = 5;
    private int mDisplayOffset = 0;
    /* access modifiers changed from: private */
    public int mEstimateMaxX = 0;
    private int mGap = 0;
    private GestureDetector mGesture;
    private float mLastTouchX = 0.0f;
    private float mLastTouchY = 0.0f;
    /* access modifiers changed from: private */
    public int mLeftViewIndex = -1;
    private int mMaxX = Integer.MAX_VALUE;
    protected int mNextX;
    private GestureDetector.OnGestureListener mOnGesture = new au(this);
    /* access modifiers changed from: private */
    public AdapterView.OnItemClickListener mOnItemClicked;
    /* access modifiers changed from: private */
    public AdapterView.OnItemLongClickListener mOnItemLongClicked;
    /* access modifiers changed from: private */
    public AdapterView.OnItemSelectedListener mOnItemSelected;
    private OnScrollChangeListener mOnScrollChangeListener = null;
    private Queue<View> mRemovedViewQueue = new LinkedList();
    private int mRightViewIndex = 0;
    protected Scroller mScroller;
    private int mTouchState = 0;

    /* compiled from: ProGuard */
    public interface OnScrollChangeListener {
        void onScroll(int i);

        void onSizeChange(int i, int i2, int i3);
    }

    public HorizontalListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initView();
        this.mScroller = new Scroller(getContext());
        this.mGesture = new GestureDetector(getContext(), this.mOnGesture);
    }

    public HorizontalListView(Context context) {
        super(context);
        initView();
        this.mScroller = new Scroller(getContext());
        this.mGesture = new GestureDetector(getContext(), this.mOnGesture);
    }

    public HorizontalListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initView();
        this.mScroller = new Scroller(getContext());
        this.mGesture = new GestureDetector(getContext(), this.mOnGesture);
    }

    private synchronized void initView() {
        this.mLeftViewIndex = -1;
        this.mRightViewIndex = 0;
        this.mDisplayOffset = 0;
        this.mCurrentX = 0;
        this.mNextX = 0;
        this.mMaxX = Integer.MAX_VALUE;
    }

    public void setGap(int i) {
        this.mGap = i;
    }

    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
        this.mOnItemSelected = onItemSelectedListener;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.mOnItemClicked = onItemClickListener;
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener onItemLongClickListener) {
        this.mOnItemLongClicked = onItemLongClickListener;
    }

    public void setOnScrollChangeListener(OnScrollChangeListener onScrollChangeListener) {
        this.mOnScrollChangeListener = onScrollChangeListener;
    }

    public ListAdapter getAdapter() {
        return this.mAdapter;
    }

    public View getSelectedView() {
        return null;
    }

    public void setAdapter(ListAdapter listAdapter) {
        if (this.mAdapter != null) {
            this.mAdapter.unregisterDataSetObserver(this.mDataObserver);
        }
        this.mAdapter = listAdapter;
        if (this.mAdapter != null) {
            this.mAdapter.registerDataSetObserver(this.mDataObserver);
        }
        reset();
    }

    /* access modifiers changed from: private */
    public synchronized void reset() {
        initView();
        removeAllViewsInLayout();
        requestLayout();
    }

    public void setSelection(int i) {
    }

    private void addAndMeasureChild(View view, int i) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-1, -1);
        }
        addViewInLayout(view, i, layoutParams, false);
        view.measure(view.getMeasuredWidth(), view.getMeasuredHeight());
        int measuredHeight = view.getMeasuredHeight();
        if (this.childMassureHight < measuredHeight) {
            this.childMassureHight = measuredHeight;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        int i5 = i3 - i;
        if (this.mAdapter != null) {
            if (this.mDataChanged || z) {
                if (this.mDataChanged) {
                    this.mEstimateMaxX = 0;
                    this.mChildWidth = 0;
                }
                int i6 = this.mCurrentX;
                initView();
                removeAllViewsInLayout();
                this.mNextX = i6;
                if (this.mNextX > this.mEstimateMaxX - i5) {
                    this.mNextX = this.mEstimateMaxX - i5;
                }
                this.mDataChanged = false;
            }
            if (this.mScroller.computeScrollOffset()) {
                this.mNextX = this.mScroller.getCurrX();
            }
            if (this.mMaxX < 0) {
                this.mMaxX = 0;
            }
            if (this.mNextX >= this.mMaxX) {
                this.mNextX = this.mMaxX;
                this.mScroller.forceFinished(true);
            }
            if (this.mNextX <= 0) {
                this.mNextX = 0;
                this.mScroller.forceFinished(true);
            }
            int i7 = this.mCurrentX - this.mNextX;
            removeNonVisibleItems(i7);
            fillList(i7);
            boolean positionItems = positionItems(i7);
            this.mCurrentX = this.mNextX;
            if (!this.mScroller.isFinished()) {
                post(new at(this));
            }
            if (this.mOnScrollChangeListener != null) {
                this.mOnScrollChangeListener.onScroll(this.mCurrentX);
                if (positionItems) {
                    this.mOnScrollChangeListener.onSizeChange(i5, this.mEstimateMaxX, getScrollX());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        if (this.mOnScrollChangeListener != null) {
            this.mOnScrollChangeListener.onScroll(i);
        }
    }

    private void fillList(int i) {
        int i2;
        int i3 = 0;
        View childAt = getChildAt(getChildCount() - 1);
        if (childAt != null) {
            i2 = childAt.getRight();
        } else {
            i2 = 0;
        }
        fillListRight(i2, i);
        View childAt2 = getChildAt(0);
        if (childAt2 != null) {
            i3 = childAt2.getLeft();
        }
        fillListLeft(i3, i);
    }

    private void fillListRight(int i, int i2) {
        View view;
        while (i + i2 < getWidth() && this.mRightViewIndex < this.mAdapter.getCount() && (view = this.mAdapter.getView(this.mRightViewIndex, this.mRemovedViewQueue.poll(), this)) != null) {
            addAndMeasureChild(view, -1);
            i += view.getMeasuredWidth();
            if (this.mRightViewIndex == this.mAdapter.getCount() - 1) {
                this.mMaxX = (this.mCurrentX + i) - getWidth();
            }
            if (this.mMaxX < 0) {
                this.mMaxX = 0;
            }
            this.mRightViewIndex++;
        }
    }

    private void fillListLeft(int i, int i2) {
        View view;
        while (i + i2 > 0 && this.mLeftViewIndex >= 0 && (view = this.mAdapter.getView(this.mLeftViewIndex, this.mRemovedViewQueue.poll(), this)) != null) {
            addAndMeasureChild(view, 0);
            i -= view.getMeasuredWidth();
            this.mLeftViewIndex--;
            this.mDisplayOffset -= view.getMeasuredWidth();
        }
    }

    private void removeNonVisibleItems(int i) {
        View childAt = getChildAt(0);
        while (childAt != null && childAt.getRight() + i <= 0) {
            this.mDisplayOffset += childAt.getMeasuredWidth();
            this.mRemovedViewQueue.offer(childAt);
            removeViewInLayout(childAt);
            this.mLeftViewIndex++;
            childAt = getChildAt(0);
        }
        View childAt2 = getChildAt(getChildCount() - 1);
        while (childAt2 != null && childAt2.getLeft() + i >= getWidth()) {
            this.mRemovedViewQueue.offer(childAt2);
            removeViewInLayout(childAt2);
            this.mRightViewIndex--;
            childAt2 = getChildAt(getChildCount() - 1);
        }
    }

    private boolean positionItems(int i) {
        if (getChildCount() > 0) {
            this.mDisplayOffset += i;
            int i2 = this.mDisplayOffset;
            for (int i3 = 0; i3 < getChildCount(); i3++) {
                View childAt = getChildAt(i3);
                this.mChildWidth = childAt.getMeasuredWidth();
                childAt.layout(i2, 0, this.mChildWidth + i2, childAt.getMeasuredHeight());
                i2 += this.mChildWidth + this.mGap;
            }
        }
        int i4 = this.mEstimateMaxX;
        if (this.mAdapter != null && this.mEstimateMaxX <= 0) {
            this.mEstimateMaxX = (this.mChildWidth * this.mAdapter.getCount()) - this.mGap;
        }
        if (i4 != this.mEstimateMaxX) {
            return true;
        }
        return false;
    }

    public synchronized void scrollTo(int i) {
        this.mScroller.startScroll(this.mNextX, 0, i - this.mNextX, 0);
        requestLayout();
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return super.dispatchTouchEvent(motionEvent) | this.mGesture.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (f != 0.0f) {
            synchronized (this) {
                this.mScroller.fling(this.mNextX, 0, (int) (-f), 0, 0, this.mMaxX, 0, 0);
            }
        }
        requestLayout();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onDown(MotionEvent motionEvent) {
        synchronized (this) {
            this.mScroller.forceFinished(true);
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.mLastTouchX = motionEvent.getX();
                this.mLastTouchY = motionEvent.getY();
                break;
            case 1:
            case 3:
            case 4:
                this.mTouchState = 0;
                break;
            case 2:
                if (Math.abs(motionEvent.getY() - this.mLastTouchY) / Math.abs(motionEvent.getX() - this.mLastTouchX) < this.mAngleSlop) {
                    this.mTouchState = 1;
                    getParent().requestDisallowInterceptTouchEvent(true);
                    break;
                }
                break;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void destroy() {
        this.mOnGesture = null;
        this.mDataObserver = null;
        this.mRemovedViewQueue = null;
        this.mOnScrollChangeListener = null;
        this.mOnItemLongClicked = null;
        this.mOnItemClicked = null;
        this.mOnItemSelected = null;
    }
}
