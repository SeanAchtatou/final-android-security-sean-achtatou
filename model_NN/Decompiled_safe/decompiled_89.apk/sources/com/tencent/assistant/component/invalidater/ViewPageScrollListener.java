package com.tencent.assistant.component.invalidater;

import android.support.v4.view.ViewPager;

/* compiled from: ProGuard */
public class ViewPageScrollListener extends CommonViewInvalidater implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private int f1066a = 0;

    public void onPageScrollStateChanged(int i) {
        this.f1066a = i;
        if (canHandleMessage()) {
            handleQueueMsg();
        }
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public void onPageSelected(int i) {
    }

    /* access modifiers changed from: protected */
    public boolean canHandleMessage() {
        return this.f1066a == 0;
    }
}
