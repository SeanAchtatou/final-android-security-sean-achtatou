package com.tencent.assistant.component.appdetail;

import android.content.Context;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.cq;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class i extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f918a;
    final /* synthetic */ AppdetailFlagView b;

    i(AppdetailFlagView appdetailFlagView, Context context) {
        this.b = appdetailFlagView;
        this.f918a = context;
    }

    public void onTMAClick(View view) {
        if (this.b.f897a.getVisibility() == 8) {
            this.b.b();
            this.b.f897a.setVisibility(0);
            try {
                this.b.b.setImageResource(R.drawable.icon_close);
            } catch (Throwable th) {
                cq.a().b();
            }
        } else {
            this.b.f897a.setVisibility(8);
            try {
                this.b.b.setImageResource(R.drawable.icon_open);
            } catch (Throwable th2) {
                cq.a().b();
            }
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f918a instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b.getContext(), 200);
        buildSTInfo.slotId = a.a(this.b.f, this.b.g);
        if (this.b.f897a.getVisibility() == 8) {
            buildSTInfo.status = "01";
            return buildSTInfo;
        }
        buildSTInfo.status = "02";
        return buildSTInfo;
    }
}
