package com.tencent.assistant.component.listener;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public abstract class OnTMAClickListener implements View.OnClickListener {
    protected int clickViewId = 0;

    public abstract void onTMAClick(View view);

    public void onClick(View view) {
        doClick(view);
    }

    public void doClick(View view) {
        this.clickViewId = view.getId();
        userActionReport(view);
        onTMAClick(view);
    }

    /* access modifiers changed from: protected */
    public void userActionReport(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(view.getContext(), 200);
        if (buildSTInfo != null && (view.getTag(R.id.tma_st_slot_tag) instanceof String)) {
            buildSTInfo.slotId = (String) view.getTag(R.id.tma_st_slot_tag);
        }
        k.a(buildSTInfo);
    }
}
