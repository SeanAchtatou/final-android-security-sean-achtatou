package com.tencent.assistant.component;

import android.view.View;

/* compiled from: ProGuard */
class al implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FeedbackInputView f890a;

    al(FeedbackInputView feedbackInputView) {
        this.f890a = feedbackInputView;
    }

    public void onClick(View view) {
        this.f890a.inputView.setFocusable(true);
        this.f890a.inputView.setCursorVisible(true);
        this.f890a.inputView.setFocusableInTouchMode(true);
    }
}
