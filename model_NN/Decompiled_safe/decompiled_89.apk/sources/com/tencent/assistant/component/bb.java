package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.link.b;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.HashMap;

/* compiled from: ProGuard */
class bb implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalErrorRecommendPage f957a;

    bb(NormalErrorRecommendPage normalErrorRecommendPage) {
        this.f957a = normalErrorRecommendPage;
    }

    public void onClick(View view) {
        if (30 == this.f957a.currentState) {
            Intent intent = new Intent("android.settings.SETTINGS");
            if (b.a(this.f957a.getContext(), intent)) {
                this.f957a.getContext().startActivity(intent);
            } else {
                Toast.makeText(this.f957a.getContext(), (int) R.string.dialog_not_found_activity, 0).show();
            }
        } else if (50 == this.f957a.currentState || 70 == this.f957a.currentState) {
            new HashMap();
            int activityPageId = this.f957a.getActivityPageId();
            if (this.f957a.context instanceof BaseActivity) {
                BaseActivity baseActivity = (BaseActivity) this.f957a.context;
                if (activityPageId == 2000) {
                    activityPageId = baseActivity.f();
                }
                baseActivity.p();
            }
            k.a(new STInfoV2(activityPageId, "03_001", activityPageId, STConst.ST_DEFAULT_SLOT, 200));
            this.f957a.context.startActivity(new Intent(this.f957a.context, MainActivity.class));
        } else if (this.f957a.listener != null) {
            this.f957a.listener.onClick(view);
        }
    }
}
