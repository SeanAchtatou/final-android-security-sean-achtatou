package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.a.y;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class aj extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardYuyiTagItem f1135a;
    private Context b;
    private y c;

    public aj(SearchSmartCardYuyiTagItem searchSmartCardYuyiTagItem, Context context, y yVar) {
        this.f1135a = searchSmartCardYuyiTagItem;
        this.b = context;
        this.c = yVar;
    }

    public void onTMAClick(View view) {
        if (this.c != null) {
            b.a(this.f1135a.getContext(), this.c.n);
        }
    }

    public STInfoV2 getStInfo(View view) {
        return null;
    }
}
