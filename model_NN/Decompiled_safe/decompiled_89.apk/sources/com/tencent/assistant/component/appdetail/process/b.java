package com.tencent.assistant.component.appdetail.process;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.PopViewDialog;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.r;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.model.StatInfo;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class b extends e implements UIEventListener, g {
    private Context n;
    private SimpleAppModel o = null;
    private long p;
    private PopViewDialog q;
    private AstApp r;
    private int s = 1;
    private String t;
    private String u;
    private List<String> v = new ArrayList();
    private Bundle w;

    public b(a aVar) {
        super(aVar);
    }

    public void a(SimpleAppModel simpleAppModel, long j, boolean z, PopViewDialog popViewDialog, StatInfo statInfo, Bundle bundle, boolean z2, Context context) {
        this.w = bundle;
        String string = bundle.getString(a.e);
        String string2 = bundle.getString(a.p);
        this.n = context;
        this.r = AstApp.i();
        this.f930a = z;
        this.o = simpleAppModel;
        this.p = j;
        this.q = popViewDialog;
        this.t = string;
        this.u = string2;
        AppConst.AppState a2 = a(simpleAppModel);
        if (AppConst.AppState.SDKUNSUPPORT == a2) {
            a(context.getString(R.string.unsupported));
            b(context.getResources().getColor(R.color.apk_size));
            return;
        }
        f.a().a(this.o.q(), this);
        j();
        a(a2);
        b(false);
    }

    private void j() {
        if (this.t != null) {
            String[] split = this.t.split(";");
            for (String add : split) {
                this.v.add(add);
            }
            k();
        }
    }

    private void a(AppConst.AppState appState) {
        b(appState);
    }

    private void k() {
        boolean z;
        boolean z2 = true;
        if (this.v.contains("1") || this.v.contains("2") || this.v.contains("3")) {
            if (this.v.contains("3")) {
                z = !c.d();
            } else {
                z = false;
            }
            if (e.a(this.o.c, this.o.g, this.o.ad)) {
                c(AppConst.AppState.INSTALLED);
                return;
            }
            DownloadInfo a2 = DownloadProxy.a().a(this.o);
            if (a2 != null && a2.needReCreateInfo(this.o)) {
                DownloadProxy.a().b(a2.downloadTicket);
                a2 = null;
            }
            StatInfo a3 = com.tencent.assistantv2.st.page.a.a(i());
            if (a2 != null) {
                a2.updateDownloadInfoStatInfo(a3);
            } else if (!z) {
                a2 = DownloadInfo.createDownloadInfo(this.o, a3);
            } else {
                return;
            }
            h();
            if (this.w != null) {
                a2.initCallParamsFromActionBundle(this.w);
            }
            if (this.v.contains("2")) {
                z2 = false;
            }
            a2.autoInstall = z2;
            switch (d.f929a[a(this.o).ordinal()]) {
                case 1:
                    com.tencent.assistant.download.a.a().d(a2);
                    return;
                case 2:
                case 3:
                    if (!z) {
                        com.tencent.assistant.download.a.a().a(a2);
                        return;
                    }
                    return;
                case 4:
                default:
                    return;
                case 5:
                    if (!z) {
                        com.tencent.assistant.download.a.a().b(a2);
                        return;
                    }
                    return;
                case 6:
                case 7:
                    if (!z) {
                        com.tencent.assistant.download.a.a().a(a2);
                        return;
                    }
                    return;
            }
        }
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        a(DownloadProxy.a().d(str), appState);
    }

    private void b(AppConst.AppState appState) {
        String str = null;
        if (this.o != null) {
            str = this.o.q();
        }
        if (!TextUtils.isEmpty(str)) {
            if (appState == null || appState == AppConst.AppState.ILLEGAL) {
                appState = a(this.o);
            }
            a(DownloadProxy.a().a(this.o), appState);
            return;
        }
        a(this.n.getResources().getString(R.string.illegal_data));
    }

    public void handleUIEvent(Message message) {
        int e;
        switch (message.what) {
            case 1016:
                if (this.o != null) {
                    b(a(this.o));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS /*1081*/:
                XLog.i("xjp", "[AppdetailActionProcess] : 登录成功");
                Bundle bundle = (Bundle) message.obj;
                if (bundle == null || !bundle.containsKey(AppConst.KEY_FROM_TYPE)) {
                    e = com.tencent.assistant.login.a.a.e();
                    com.tencent.assistant.login.a.a.a(0);
                } else {
                    e = bundle.getInt(AppConst.KEY_FROM_TYPE);
                    com.tencent.assistant.login.a.a.a(0);
                }
                if (11 == e || 12 == e) {
                    com.tencent.assistant.link.b.a(this.n, Uri.parse("tmast://webview?url=http://mq.wsq.qq.com/direct?route=sId/t/new&pkgName=" + this.o.c));
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.component.appdetail.process.b.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.assistant.component.appdetail.process.b.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.assistant.component.appdetail.process.e.a(int, int):void
      com.tencent.assistant.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.assistant.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.assistant.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.component.appdetail.process.e.a(boolean, com.tencent.assistant.component.appdetail.process.AppdetailActionUIListener$AuthType):void
      com.tencent.assistant.component.appdetail.process.b.a(boolean, int):void */
    private void a(DownloadInfo downloadInfo, AppConst.AppState appState) {
        if (!(this.s == 2 && ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) != null && appState == AppConst.AppState.INSTALLED)) {
        }
        if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.FAIL || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.QUEUING) {
            if (!(downloadInfo == null || downloadInfo.response == null)) {
                d(0);
                if (3 == this.s) {
                    a(true, (int) R.drawable.btn_green_selector);
                } else {
                    a(false, (int) R.drawable.btn_green_selector);
                }
                if (b(downloadInfo, appState)) {
                    a(downloadInfo.response.f, 0);
                } else {
                    a(SimpleDownloadInfo.getPercent(downloadInfo), 0);
                }
            }
        } else if (appState == AppConst.AppState.INSTALLED) {
            a(0, 100);
            d(8);
            if (this.s == 1) {
                a(true, (int) R.drawable.appdetail_bar_btn_installed_selector_v5_shixin);
            } else {
                a(true, (int) R.drawable.btn_green_selector);
            }
        } else if (appState == AppConst.AppState.DOWNLOADED) {
            d(8);
            a(0, 0);
            a(true, (int) R.drawable.appdetail_bar_btn_downloaded_selector_v5);
        } else if (appState == AppConst.AppState.INSTALLING) {
            a(0, 100);
            a(true, (int) R.drawable.common_btn_big_disabled);
        } else if ((appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) && (this.o.h() || this.o.i())) {
            a(0, 100);
            a(true, (int) R.drawable.appdetail_bar_btn_downloaded_selector_v5);
        } else {
            a(0, 100);
            a(true, (int) R.drawable.btn_green_selector);
        }
        c(appState);
        d(appState);
    }

    private boolean b(DownloadInfo downloadInfo, AppConst.AppState appState) {
        if (downloadInfo == null || downloadInfo.response == null) {
            return false;
        }
        if (appState != AppConst.AppState.DOWNLOADING) {
            if (downloadInfo.response.b <= 0) {
                return false;
            }
            if (!(appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.FAIL)) {
                return false;
            }
        }
        if (downloadInfo.response.f > SimpleDownloadInfo.getPercent(downloadInfo)) {
            return true;
        }
        return false;
    }

    private void a(boolean z, int i) {
        if (z) {
            c(0);
            a(this.n.getResources().getDrawable(i));
            return;
        }
        c(8);
    }

    public void a() {
        AppConst.AppState a2 = a(this.o);
        if (AppConst.AppState.SDKUNSUPPORT == a2) {
            Toast.makeText(this.n, this.n.getResources().getString(R.string.canot_support_sofrware), 0).show();
            return;
        }
        DownloadInfo a3 = DownloadProxy.a().a(this.o);
        if (a3 != null && a3.needReCreateInfo(this.o)) {
            DownloadProxy.a().b(a3.downloadTicket);
            a3 = null;
        }
        StatInfo a4 = com.tencent.assistantv2.st.page.a.a(i());
        if (a3 == null) {
            a3 = DownloadInfo.createDownloadInfo(this.o, a4);
        } else {
            a3.updateDownloadInfoStatInfo(a4);
        }
        if (this.w != null) {
            a3.initCallParamsFromActionBundle(this.w);
        }
        if (!TextUtils.isEmpty(this.o.q())) {
            if (!(this.s == 2 && ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) == null)) {
            }
            if (3 != this.s) {
                switch (d.f929a[a2.ordinal()]) {
                    case 1:
                        com.tencent.assistant.download.a.a().d(a3);
                        a(this.n.getResources().getString(R.string.install));
                        return;
                    case 2:
                    case 3:
                        com.tencent.assistant.download.a.a().a(a3);
                        c(a3, a2);
                        return;
                    case 4:
                        com.tencent.assistant.download.a.a().c(a3);
                        if (!be.a().c(this.o.c)) {
                            a(this.n.getResources().getString(R.string.open));
                            return;
                        }
                        return;
                    case 5:
                        com.tencent.assistant.download.a.a().b(a3);
                        c(a3, a2);
                        return;
                    case 6:
                    case 7:
                        com.tencent.assistant.download.a.a().a(a3);
                        c(a3, a2);
                        return;
                    case 8:
                        com.tencent.assistant.download.a.a().b(a3.downloadTicket);
                        a(this.n.getResources().getString(R.string.continuing));
                        return;
                    case 9:
                        com.tencent.assistant.download.a.a().b(a3.downloadTicket);
                        a(this.n.getResources().getString(R.string.pause));
                        return;
                    case 10:
                        Toast.makeText(this.n, (int) R.string.tips_slicent_install, 0).show();
                        return;
                    case 11:
                        Toast.makeText(this.n, (int) R.string.tips_slicent_uninstall, 0).show();
                        return;
                    default:
                        return;
                }
            } else if (d.a().l()) {
                l();
            } else if (d.a().k()) {
                com.tencent.assistant.link.b.a(this.n, Uri.parse("tmast://webview?url=http://mq.wsq.qq.com/direct?route=sId/t/new&pkgName=" + this.o.c));
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 11);
                com.tencent.assistant.login.a.a.a(11);
                d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            }
        }
    }

    private void l() {
        c cVar = new c(this);
        Resources resources = this.n.getResources();
        cVar.titleRes = resources.getString(R.string.login_prompt);
        cVar.contentRes = resources.getString(R.string.login_prompt_content);
        cVar.lBtnTxtRes = resources.getString(R.string.cancel);
        cVar.rBtnTxtRes = resources.getString(R.string.login_prompt_switch_account);
        v.a(cVar);
    }

    private void c(AppConst.AppState appState) {
        if (appState == null || appState == AppConst.AppState.ILLEGAL) {
            appState = a(this.o);
        }
        if (!(this.s == 2 && ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) == null)) {
        }
        if (3 == this.s) {
            a(this.n.getResources().getString(R.string.appdetail_appbar_send_topic));
            return;
        }
        switch (d.f929a[appState.ordinal()]) {
            case 1:
                a(this.n.getResources().getString(R.string.install));
                return;
            case 2:
            case 5:
                a(this.n.getResources().getString(R.string.continuing));
                return;
            case 3:
                a(this.n, this.o);
                return;
            case 4:
                if (be.a().c(this.o.c)) {
                    be.a();
                    if (!TextUtils.isEmpty(be.h())) {
                        be.a();
                        if (be.h().equals(this.o.c)) {
                            a(this.n.getResources().getString(R.string.qube_apk_using));
                            return;
                        }
                    }
                    a(this.n.getResources().getString(R.string.qube_apk_use));
                    return;
                }
                a(this.n.getResources().getString(R.string.open));
                return;
            case 6:
                if (this.o.c()) {
                    a(this.n.getResources().getString(R.string.jionfirstrelease) + " " + bt.a(this.o.k));
                    return;
                } else if (this.o.h()) {
                    a(this.n.getResources().getString(R.string.jionbeta) + " " + bt.a(this.o.k));
                    return;
                } else if (this.o.i()) {
                    a(this.n.getResources().getString(R.string.jionbeta) + " " + bt.a(this.o.k));
                    return;
                } else {
                    a(this.n, this.o);
                    return;
                }
            case 7:
                if (this.o.a() && !this.o.h() && !this.o.i()) {
                    a(this.n.getResources().getString(R.string.slim_update), bt.a(this.o.k), bt.a(this.o.v));
                    return;
                } else if (!this.o.a() || (!this.o.h() && !this.o.i())) {
                    a(this.n.getResources().getString(R.string.update) + " " + bt.a(this.o.k));
                    return;
                } else {
                    a(this.n.getResources().getString(R.string.jionbeta));
                    return;
                }
            case 8:
                m();
                return;
            case 9:
                a(this.n.getResources().getString(R.string.queuing));
                return;
            case 10:
                a(this.n.getResources().getString(R.string.installing));
                return;
            case 11:
                a(this.n.getResources().getString(R.string.uninstalling));
                return;
            default:
                a(this.n, this.o);
                return;
        }
    }

    private void d(AppConst.AppState appState) {
        DownloadInfo a2 = DownloadProxy.a().a(this.o);
        int i = 0;
        if (a2 != null) {
            if (b(a2, appState)) {
                i = a2.response.f;
            } else {
                i = SimpleDownloadInfo.getPercent(a2);
            }
        }
        if (appState == AppConst.AppState.INSTALLED) {
            b(this.n.getResources().getColor(R.color.white));
        } else if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED) {
            b(this.n.getResources().getColor(R.color.appdetail_tag_text_color_blue_n));
        } else if (appState == AppConst.AppState.DOWNLOADED) {
            b(this.n.getResources().getColor(R.color.white));
        } else if (appState == AppConst.AppState.FAIL) {
        } else {
            if ((appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) && (this.o.h() || this.o.i())) {
                b(this.n.getResources().getColor(R.color.white));
            } else if (appState == AppConst.AppState.QUEUING && i <= 0) {
                b(this.n.getResources().getColor(R.color.appdetail_tag_text_color_blue_n));
            } else if (appState == AppConst.AppState.QUEUING && i > 0) {
            } else {
                if (appState == AppConst.AppState.INSTALLING) {
                    b(this.n.getResources().getColor(R.color.state_disable));
                } else {
                    b(this.n.getResources().getColor(17170443));
                }
            }
        }
    }

    private void m() {
        c(DownloadProxy.a().a(this.o), a(this.o));
    }

    private void c(DownloadInfo downloadInfo, AppConst.AppState appState) {
        double d = 0.0d;
        if (downloadInfo != null) {
            if (b(downloadInfo, appState)) {
                d = (double) downloadInfo.response.f;
            } else {
                d = SimpleDownloadInfo.getPercentFloat(downloadInfo);
            }
        }
        a(String.format(this.n.getResources().getString(R.string.downloading_percent), String.format("%.1f", Double.valueOf(d))));
    }

    public void a(boolean z, AppConst.AppState appState, long j, String str, int i, int i2, boolean z2) {
        this.f930a = z;
        if (i > 0) {
            this.b = str;
            this.c = i;
            this.e = i2;
            this.d = j;
            this.f = z2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.component.appdetail.process.b.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.assistant.component.appdetail.process.b.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.assistant.component.appdetail.process.e.a(int, int):void
      com.tencent.assistant.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.assistant.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.assistant.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.component.appdetail.process.e.a(boolean, com.tencent.assistant.component.appdetail.process.AppdetailActionUIListener$AuthType):void
      com.tencent.assistant.component.appdetail.process.b.a(boolean, int):void */
    public void a(int i) {
        AppConst.AppState a2 = a(this.o);
        if ((a2 == null || AppConst.AppState.SDKUNSUPPORT != a2) && i <= 3 && i >= 1) {
            this.s = i;
            if (this.o != null) {
                if (i == 2) {
                    if (ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) != null) {
                    }
                } else if (1 == i) {
                    if (a2 == AppConst.AppState.INSTALLED) {
                        a(0, 100);
                        a(true, (int) R.drawable.appdetail_bar_btn_installed_selector_v5_shixin);
                    } else {
                        a(DownloadProxy.a().a(this.o), a2);
                    }
                } else if (3 == i) {
                    a(true, (int) R.drawable.btn_green_selector);
                }
                c(a2);
                d(a2);
            }
        }
    }

    private AppConst.AppState a(SimpleAppModel simpleAppModel) {
        AppConst.AppState b;
        return (TextUtils.isEmpty(this.t) || (b = b(simpleAppModel)) == AppConst.AppState.ILLEGAL) ? u.d(simpleAppModel) : b;
    }

    private AppConst.AppState b(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null) {
            return AppConst.AppState.ILLEGAL;
        }
        if (simpleAppModel.h > r.d()) {
            return AppConst.AppState.SDKUNSUPPORT;
        }
        AppConst.AppState appState = AppConst.AppState.ILLEGAL;
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad);
        if (a2 != null) {
            appState = u.b(a2);
        }
        if (appState != AppConst.AppState.ILLEGAL) {
            return appState;
        }
        if (TextUtils.isEmpty(this.t) || !simpleAppModel.ab) {
            return AppConst.AppState.ILLEGAL;
        }
        if (e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return AppConst.AppState.INSTALLED;
        }
        return AppConst.AppState.UPDATE;
    }

    public void b() {
        this.r.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
    }

    public void c() {
        if (this.o != null) {
            AppConst.AppState a2 = a(this.o);
            f.a().a(this.o.q(), this);
            a(a2);
        }
        this.r.k().addUIEventListener(1016, this);
    }

    public void d() {
        this.r.k().removeUIEventListener(1016, this);
    }

    public void e() {
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS, this);
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL, this);
        this.r.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
    }

    public void a(View view) {
        int id = view.getId();
        if (id == R.id.btn_pause_download) {
            a();
        } else if (id == R.id.btn_delete_download) {
            DownloadInfo a2 = DownloadProxy.a().a(this.o);
            if (a2 != null) {
                DownloadProxy.a().b(a2.downloadTicket);
            }
            a((DownloadInfo) null, a(this.o));
        } else if (id == R.id.appdetail_progress_btn_for_cmd) {
            if (ApkResourceManager.getInstance().getLocalApkInfo(this.o.c) != null || (be.a().c(this.o.c) && be.a().d(this.o.c))) {
                this.q.setData(this.f930a, this.o.f1634a, this.p, this.d, this.b, this.c, this.e, (AppDetailActivityV5) this.n, this.o.d, this.f);
                try {
                    this.q.show();
                } catch (Throwable th) {
                }
            } else {
                ((View) view.getParent()).setVisibility(8);
                Toast.makeText(this.n, (int) R.string.comment_txt_tips_need_install, 4).show();
            }
        } else if (id != R.id.appdetail_progress_btn_for_appbar) {
        } else {
            if (d.a().l()) {
                l();
            } else if (d.a().k()) {
                com.tencent.assistant.link.b.a(this.n, Uri.parse("tmast://webview?url=http://mq.wsq.qq.com/direct?route=sId/t/new&pkgName=" + this.o.c));
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 11);
                com.tencent.assistant.login.a.a.a(11);
                d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            }
        }
    }
}
