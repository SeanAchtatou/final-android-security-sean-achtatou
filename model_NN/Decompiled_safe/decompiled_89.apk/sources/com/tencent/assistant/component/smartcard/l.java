package com.tencent.assistant.component.smartcard;

import android.widget.ImageView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class l extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalSmartcardAppListItem f1151a;

    l(NormalSmartcardAppListItem normalSmartcardAppListItem) {
        this.f1151a = normalSmartcardAppListItem;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.component.smartcard.NormalSmartcardAppListItem.a(com.tencent.assistant.component.smartcard.NormalSmartcardAppListItem, boolean):boolean
     arg types: [com.tencent.assistant.component.smartcard.NormalSmartcardAppListItem, int]
     candidates:
      com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem.a(java.lang.String, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.component.smartcard.NormalSmartcardBaseItem.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.page.STInfoV2):void
      com.tencent.assistant.component.smartcard.NormalSmartcardAppListItem.a(com.tencent.assistant.component.smartcard.NormalSmartcardAppListItem, boolean):boolean */
    public void a(DownloadInfo downloadInfo) {
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a((ImageView) this.f1151a.findViewWithTag(downloadInfo.downloadTicket));
        if (this.f1151a.smartcardModel.i == 2 && !this.f1151a.n) {
            boolean unused = this.f1151a.n = true;
            Toast.makeText(this.f1151a.f1115a, this.f1151a.f1115a.getResources().getString(R.string.smartcard_installed_delete_app_toast), 0).show();
        }
    }
}
