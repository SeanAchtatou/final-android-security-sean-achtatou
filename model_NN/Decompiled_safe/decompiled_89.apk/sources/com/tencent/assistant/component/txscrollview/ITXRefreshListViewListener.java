package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public interface ITXRefreshListViewListener {
    void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState);
}
