package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
class p implements TXScrollViewBase.ISmoothScrollRunnableListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXRefreshListView f1212a;

    p(TXRefreshListView tXRefreshListView) {
        this.f1212a = tXRefreshListView;
    }

    public void onSmoothScrollFinished() {
        if (this.f1212a.mRefreshListViewListener != null) {
            this.f1212a.mRefreshListViewListener.onTXRefreshListViewRefresh(this.f1212a.mCurScrollState);
        }
    }
}
