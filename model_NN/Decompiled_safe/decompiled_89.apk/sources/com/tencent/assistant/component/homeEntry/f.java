package com.tencent.assistant.component.homeEntry;

import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bc;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f1059a;

    f(e eVar) {
        this.f1059a = eVar;
    }

    public void run() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1059a.f1058a.b, 100);
        buildSTInfo.scene = STConst.ST_PAGE_COMPETITIVE;
        if (this.f1059a.f1058a.mSourceData != null) {
            bc.a(this.f1059a.f1058a.mSourceData.e, buildSTInfo);
        }
        b.getInstance().exposure(buildSTInfo);
    }
}
