package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.m;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class QuickEntranceView extends LinearLayout implements p {
    public static final int APP_RECOMMEND_CARD_TYPE_QUICK_ENTRANCE = 3;
    public static final int CARD_TYPE_QUICK_ENTRANCE = 1;
    private static final int CARD_VIEW_ID = 10000;
    private static final int EXPOSURE_MAX_COUNT = 2;
    private static final int MSG_ANIMATION_END = 2;
    private static final int MSG_PIC_LOAD_SUC = 1;
    /* access modifiers changed from: private */
    public List<ColorCardItem> cards = new ArrayList();
    /* access modifiers changed from: private */
    public Context context;
    private Handler handler = new ck(this);
    /* access modifiers changed from: private */
    public ArrayList<QuickEntranceNotify> notifyCache;
    /* access modifiers changed from: private */
    public String priority = Constants.STR_EMPTY;
    private String quickSlotTag = STConst.ST_DEFAULT_SLOT;
    /* access modifiers changed from: private */
    public List<ImageView> viewIcons = new ArrayList();
    private List<RelativeLayout> viewLayouts = new ArrayList();
    private List<ImageView> viewShadowImages = new ArrayList();
    /* access modifiers changed from: private */
    public List<TextView> viewTexts = new ArrayList();

    public QuickEntranceView(Context context2) {
        super(context2);
        this.context = context2;
        init();
    }

    public QuickEntranceView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        init();
    }

    private void init() {
        inflate(this.context, R.layout.quick_entrance_layout, this);
        int a2 = df.a(this.context, 7.0f);
        setPadding(a2, a2, 0, 0);
        this.viewLayouts.clear();
        this.viewTexts.clear();
        this.viewShadowImages.clear();
        int b = (((df.b() - (a2 * 5)) / 4) * 100) / 162;
        setBackgroundColor(this.context.getResources().getColor(R.color.apk_list_bg));
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.item_layout_first);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) relativeLayout.getLayoutParams();
        layoutParams.height = b;
        relativeLayout.setLayoutParams(layoutParams);
        this.viewLayouts.add(relativeLayout);
        RelativeLayout relativeLayout2 = (RelativeLayout) findViewById(R.id.item_layout_second);
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) relativeLayout2.getLayoutParams();
        layoutParams2.height = b;
        relativeLayout2.setLayoutParams(layoutParams2);
        this.viewLayouts.add(relativeLayout2);
        RelativeLayout relativeLayout3 = (RelativeLayout) findViewById(R.id.item_layout_third);
        LinearLayout.LayoutParams layoutParams3 = (LinearLayout.LayoutParams) relativeLayout3.getLayoutParams();
        layoutParams3.height = b;
        relativeLayout3.setLayoutParams(layoutParams3);
        this.viewLayouts.add(relativeLayout3);
        RelativeLayout relativeLayout4 = (RelativeLayout) findViewById(R.id.item_layout_fourth);
        LinearLayout.LayoutParams layoutParams4 = (LinearLayout.LayoutParams) relativeLayout4.getLayoutParams();
        layoutParams4.height = b;
        relativeLayout4.setLayoutParams(layoutParams4);
        this.viewLayouts.add(relativeLayout4);
        this.viewTexts.add((TextView) findViewById(R.id.item_first));
        this.viewShadowImages.add((ImageView) findViewById(R.id.item_shadow_img_first));
        this.viewIcons.add((ImageView) findViewById(R.id.item_first_icon));
        this.viewTexts.add((TextView) findViewById(R.id.item_second));
        this.viewShadowImages.add((ImageView) findViewById(R.id.item_shadow_img_second));
        this.viewIcons.add((ImageView) findViewById(R.id.item_second_icon));
        this.viewTexts.add((TextView) findViewById(R.id.item_third));
        this.viewShadowImages.add((ImageView) findViewById(R.id.item_shadow_img_third));
        this.viewIcons.add((ImageView) findViewById(R.id.item_third_icon));
        this.viewTexts.add((TextView) findViewById(R.id.item_fourth));
        this.viewShadowImages.add((ImageView) findViewById(R.id.item_shadow_img_fourth));
        this.viewIcons.add((ImageView) findViewById(R.id.item_fourth_icon));
        TemporaryThreadManager.get().start(new cl(this));
    }

    public void setQuickSlotTag(String str) {
        this.quickSlotTag = str;
    }

    public void clearAnimation() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.viewIcons.size()) {
                ImageView imageView = this.viewIcons.get(i2);
                if (imageView != null) {
                    imageView.clearAnimation();
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void hasNotifyPrompt(ArrayList<QuickEntranceNotify> arrayList) {
        QuickEntranceNotify quickEntranceNotify;
        this.priority = arrayList.get(0).e;
        for (int i = 0; i < arrayList.size(); i++) {
            QuickEntranceNotify quickEntranceNotify2 = arrayList.get(i);
            Iterator<QuickEntranceNotify> it = this.notifyCache.iterator();
            while (true) {
                if (!it.hasNext()) {
                    quickEntranceNotify = null;
                    break;
                }
                quickEntranceNotify = it.next();
                if (quickEntranceNotify2.f1631a == quickEntranceNotify.f1631a) {
                    break;
                }
            }
            if (quickEntranceNotify == null) {
                this.notifyCache.add(quickEntranceNotify2);
            } else if (quickEntranceNotify.c > 0 && quickEntranceNotify.c < quickEntranceNotify2.b) {
                quickEntranceNotify.b = quickEntranceNotify2.b;
                quickEntranceNotify.c = 0;
                quickEntranceNotify.d = 0;
            }
        }
        this.notifyCache = sortData(this.notifyCache);
        saveToDB(this.notifyCache);
    }

    public void launchNotifyPrompt() {
        if (this.notifyCache != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.notifyCache.size()) {
                    return;
                }
                if (this.notifyCache.get(i2).d >= 2 || !notifyPrompt(this.notifyCache.get(i2))) {
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private boolean notifyPrompt(QuickEntranceNotify quickEntranceNotify) {
        int i = 0;
        while (i < this.cards.size()) {
            ColorCardItem colorCardItem = this.cards.get(i);
            if (colorCardItem == null || colorCardItem.f != quickEntranceNotify.f1631a || i >= this.viewIcons.size()) {
                i++;
            } else {
                startupNotifyAnimation(this.viewIcons.get(i));
                quickEntranceNotify.d++;
                quickEntranceNotify.c = System.currentTimeMillis() + m.a().A();
                saveToDB(this.notifyCache);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public synchronized void saveToDB(ArrayList<QuickEntranceNotify> arrayList) {
        TemporaryThreadManager.get().start(new cm(this, arrayList));
    }

    /* access modifiers changed from: private */
    public synchronized ArrayList<QuickEntranceNotify> readFromDB() {
        ArrayList<QuickEntranceNotify> arrayList;
        byte[] N = m.a().N();
        ArrayList<QuickEntranceNotify> arrayList2 = new ArrayList<>();
        if (N == null) {
            arrayList = arrayList2;
        } else {
            try {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(N);
                ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                while (true) {
                    QuickEntranceNotify quickEntranceNotify = (QuickEntranceNotify) objectInputStream.readObject();
                    if (quickEntranceNotify == null) {
                        break;
                    }
                    arrayList2.add(quickEntranceNotify);
                }
                byteArrayInputStream.close();
                objectInputStream.close();
            } catch (Exception e) {
            }
            arrayList = arrayList2;
        }
        return arrayList;
    }

    private ArrayList<QuickEntranceNotify> sortData(ArrayList<QuickEntranceNotify> arrayList) {
        ArrayList<QuickEntranceNotify> arrayList2 = new ArrayList<>();
        String[] split = this.priority.split(",");
        for (String valueOf : split) {
            int intValue = Integer.valueOf(valueOf).intValue();
            int i = 0;
            while (true) {
                if (i >= arrayList.size()) {
                    break;
                } else if (intValue == arrayList.get(i).f1631a) {
                    arrayList.get(i).e = this.priority;
                    arrayList2.add(arrayList.get(i));
                    arrayList.remove(i);
                    break;
                } else {
                    i++;
                }
            }
        }
        return arrayList2;
    }

    public void startupNotifyAnimation(ImageView imageView) {
        if (imageView != null) {
            Animation createNotifyAnimation = createNotifyAnimation();
            createNotifyAnimation.setAnimationListener(new cn(this));
            imageView.setAnimation(createNotifyAnimation);
        }
    }

    private Animation createNotifyAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, -df.a(5.0f));
        translateAnimation.setDuration(333);
        translateAnimation.setRepeatCount(29);
        translateAnimation.setRepeatMode(2);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        return translateAnimation;
    }

    public void refreshCards(List<ColorCardItem> list, int i) {
        if (list == null || list.size() <= 0) {
            removeAllViews();
            setPadding(0, 0, 0, 0);
            return;
        }
        this.cards.clear();
        if (i <= 0) {
            i = 1;
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            ColorCardItem colorCardItem = list.get(i2);
            if (colorCardItem.e() == i) {
                this.cards.add(colorCardItem);
            }
        }
        if (this.cards.size() > 0) {
            if (getChildCount() == 0) {
                init();
            }
            refreshCards();
            return;
        }
        removeAllViews();
        setPadding(0, 0, 0, 0);
    }

    private void refreshCards() {
        int size;
        boolean z;
        if (this.cards.size() > this.viewTexts.size()) {
            size = this.viewTexts.size();
        } else {
            size = this.cards.size();
        }
        for (int i = 0; i < size; i++) {
            ColorCardItem colorCardItem = this.cards.get(i);
            TextView textView = this.viewTexts.get(i);
            ImageView imageView = this.viewIcons.get(i);
            int id = textView.getId() - 10000;
            textView.setId(colorCardItem.f + 10000);
            ImageView imageView2 = this.viewShadowImages.get(i);
            textView.setText(colorCardItem.c());
            textView.setBackgroundColor(this.context.getResources().getColor(R.color.quick_entrance_default_bg_color));
            sendImageRequest(textView, colorCardItem.a());
            sendIconRequest(imageView, colorCardItem.f());
            String str = colorCardItem.b().f1970a;
            ActionUrl b = colorCardItem.b();
            int i2 = colorCardItem.f;
            imageView2.setTag(R.id.tma_st_slot_tag, this.quickSlotTag + ct.a(i + 1));
            imageView2.setOnClickListener(new co(this, imageView, i2, b, str));
            if (id != colorCardItem.f) {
                String str2 = this.quickSlotTag + ct.a(i + 1);
                int i3 = colorCardItem.f;
                if (imageView.getAnimation() != null) {
                    z = true;
                } else {
                    z = false;
                }
                quickReport(str2, i3, z);
            }
        }
    }

    private void sendImageRequest(TextView textView, String str) {
        Bitmap a2 = k.b().a(str, 1, this);
        if (a2 != null && !a2.isRecycled()) {
            textView.setText(Constants.STR_EMPTY);
            textView.setBackgroundDrawable(new BitmapDrawable(a2));
        }
    }

    private void sendIconRequest(ImageView imageView, String str) {
        if (imageView != null) {
            Bitmap a2 = k.b().a(str, 1, this);
            if (a2 == null || a2.isRecycled()) {
                imageView.setImageBitmap(null);
                return;
            }
            a2.setDensity(ThumbnailUtils.TARGET_SIZE_MINI_THUMBNAIL);
            imageView.setImageBitmap(a2);
        }
    }

    private void quickReport(String str, int i, boolean z) {
        int i2;
        int i3 = 2000;
        if (this.context instanceof BaseActivity) {
            i2 = ((BaseActivity) this.context).f();
            i3 = ((BaseActivity) this.context).p();
        } else {
            i2 = 2000;
        }
        STInfoV2 sTInfoV2 = new STInfoV2(i2, str, i3, STConst.ST_DEFAULT_SLOT, 100);
        sTInfoV2.extraData = (z ? "0" : "2") + "|" + i;
        com.tencent.assistantv2.st.k.a(sTInfoV2);
    }

    public void thumbnailRequestStarted(o oVar) {
    }

    public void thumbnailRequestCompleted(o oVar) {
        this.handler.obtainMessage(1, oVar).sendToTarget();
    }

    public void thumbnailRequestCancelled(o oVar) {
    }

    public void thumbnailRequestFailed(o oVar) {
    }
}
