package com.tencent.assistant.component;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.DownloadActivity;
import com.tencent.assistant.component.listener.OnTMAClickListener;
import com.tencent.assistant.plugin.PluginActivity;

/* compiled from: ProGuard */
class ai extends OnTMAClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f887a;
    final /* synthetic */ DownloadProgressButton b;

    ai(DownloadProgressButton downloadProgressButton, Context context) {
        this.b = downloadProgressButton;
        this.f887a = context;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f887a, DownloadActivity.class);
        if (this.f887a instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f887a).f());
        }
        this.f887a.startActivity(intent);
    }
}
