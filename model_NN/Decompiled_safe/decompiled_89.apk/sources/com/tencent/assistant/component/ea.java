package com.tencent.assistant.component;

import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.UpdateListView;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.bo;
import com.tencent.assistant.manager.cd;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
class ea extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListView f1036a;

    ea(UpdateListView updateListView) {
        this.f1036a = updateListView;
    }

    public void onTMAClick(View view) {
        boolean z;
        boolean z2;
        boolean z3 = false;
        UpdateListView.UpdateAllType access$200 = this.f1036a.getUpdateAllType();
        if (access$200 == UpdateListView.UpdateAllType.ALLDOWNLOADING) {
            Toast.makeText(this.f1036a.mContext, (int) R.string.toast_all_updating, 0).show();
        } else if (access$200 == UpdateListView.UpdateAllType.ALLUPDATED) {
            Toast.makeText(this.f1036a.mContext, (int) R.string.toast_all_updated, 0).show();
        } else if (u.a(this.f1036a.mUpdateListAdapter.b) != null) {
            cd c = bo.a().c();
            if (c == null || !c.a()) {
                z = false;
            } else {
                z = true;
            }
            int F = m.a().F();
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            if (F <= 0 || currentTimeMillis - ((long) F) >= 86400) {
                z2 = z;
            } else {
                z2 = false;
            }
            if (z2) {
                View unused = this.f1036a.extraMsgView = this.f1036a.mInflater.inflate((int) R.layout.updatelist_updateall_extra, (ViewGroup) null);
                if (this.f1036a.extraMsgView.findViewById(R.id.select_check) != null) {
                    if (c.j == 1) {
                        z3 = true;
                    }
                    CheckBox unused2 = this.f1036a.extraMsgViewCheckBox = (CheckBox) this.f1036a.extraMsgView.findViewById(R.id.select_check);
                    this.f1036a.extraMsgViewCheckBox.setChecked(z3);
                    this.f1036a.extraMsgViewCheckBox.setText(c.h);
                }
            } else {
                View unused3 = this.f1036a.extraMsgView = null;
            }
            eb ebVar = new eb(this, c, access$200);
            ebVar.hasTitle = true;
            ebVar.titleRes = AstApp.i().getString(R.string.dialog_updatelist_updateall_title);
            ebVar.blockCaller = true;
            ebVar.contentRes = AstApp.i().getString(R.string.dialog_updateall_enter);
            ebVar.extraMsgView = this.f1036a.extraMsgView;
            Dialog b = v.b(ebVar);
            if (b != null && b.getOwnerActivity() != null && !b.getOwnerActivity().isFinishing()) {
                STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_UPDATE_UPDATEALL, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_UPDATE, STConst.ST_DEFAULT_SLOT, 100);
                sTInfoV2.pushInfo = this.f1036a.pushInfo;
                k.a(sTInfoV2);
                b.show();
                if (z2) {
                    c.c();
                }
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildUpdateStatInfo = this.f1036a.buildUpdateStatInfo();
        if (buildUpdateStatInfo != null) {
            buildUpdateStatInfo.slotId = a.a("07", "001");
            List<SimpleAppModel> a2 = u.a(this.f1036a.mUpdateListAdapter.b);
            String str = Constants.STR_EMPTY;
            if (a2 != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= a2.size()) {
                        break;
                    }
                    SimpleAppModel simpleAppModel = a2.get(i2);
                    if (simpleAppModel != null) {
                        if (str.length() > 0) {
                            str = str + "|";
                        }
                        str = str + simpleAppModel.f1634a + "_" + simpleAppModel.b;
                    }
                    i = i2 + 1;
                }
            }
            buildUpdateStatInfo.extraData = str;
        }
        return buildUpdateStatInfo;
    }
}
