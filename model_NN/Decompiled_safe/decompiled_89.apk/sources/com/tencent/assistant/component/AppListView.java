package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.AppAdapter;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class AppListView extends TXGetMoreListView implements ITXRefreshListViewListener {
    private final String KEY_DATA = "key_data";
    private final String KEY_ISFIRSTPAGE = "isFirstPage";
    private final int MSG_LOAD_LOCAL_APPLIST = 2;
    private final int MSG_REFRESH_APPLIST = 1;
    private b enginecallBack = new j(this);
    /* access modifiers changed from: private */
    public AppAdapter mAdapter = null;
    private k mAppEngine = null;
    /* access modifiers changed from: private */
    public ApplistRefreshListener mListener;
    /* access modifiers changed from: private */
    public ViewInvalidateMessageHandler pageMessageHandler = new k(this);
    private int retryCount = 1;
    private ImageView topPaddingView;
    /* access modifiers changed from: private */
    public ViewPageScrollListener viewPagerlistener;

    /* compiled from: ProGuard */
    public interface ApplistRefreshListener {
        void onErrorHappened(int i);

        void onNetworkLoading();

        void onNetworkNoError();

        void onNextPageLoadFailed();
    }

    public void setViewPageListener(ViewPageScrollListener viewPageScrollListener) {
        this.viewPagerlistener = viewPageScrollListener;
    }

    public void registerRefreshListener(ApplistRefreshListener applistRefreshListener) {
        this.mListener = applistRefreshListener;
    }

    public k getmAppEngine() {
        return this.mAppEngine;
    }

    public void setmAppEngine(k kVar) {
        this.mAppEngine = kVar;
        kVar.register(this.enginecallBack);
        setRefreshListViewListener(this);
    }

    public AppListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initAdapter();
    }

    public AppListView(Context context, TXScrollViewBase.ScrollMode scrollMode, k kVar) {
        super(context);
        this.mAppEngine = kVar;
        kVar.register(this.enginecallBack);
        setRefreshListViewListener(this);
        initAdapter();
    }

    public ListView getListView() {
        return (ListView) this.mScrollContentView;
    }

    private void initAdapter() {
        ListAdapter adapter = ((ListView) this.mScrollContentView).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.mAdapter = (AppAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.mAdapter = (AppAdapter) ((ListView) this.mScrollContentView).getAdapter();
        }
        if (this.mAdapter == null) {
        }
    }

    public void loadFirstPage() {
        if (this.mAdapter == null) {
            initAdapter();
        }
        if (this.mAdapter.getCount() > 0) {
            this.viewPagerlistener.sendMessage(new ViewInvalidateMessage(2, null, this.pageMessageHandler));
            return;
        }
        this.mAppEngine.c();
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        this.mAppEngine.f();
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void recycleData() {
        super.recycleData();
        this.mAppEngine.unregister(this.enginecallBack);
    }

    /* access modifiers changed from: protected */
    public ListView createScrollContentView(Context context) {
        ListView listView = (ListView) ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.app_listview, (ViewGroup) null);
        this.topPaddingView = new ImageView(context);
        this.topPaddingView.setLayoutParams(new AbsListView.LayoutParams(-1, df.a(context, 6.0f)));
        this.topPaddingView.setBackgroundColor(getResources().getColor(17170445));
        listView.addHeaderView(this.topPaddingView);
        if (this.mScrollMode != TXScrollViewBase.ScrollMode.NONE) {
            this.mFooterLoadingView = createLoadingLayout(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
            this.mFooterLoadingView.setVisibility(0);
            listView.addFooterView(this.mFooterLoadingView);
            listView.setOnScrollListener(this);
        }
        return listView;
    }

    /* access modifiers changed from: private */
    public void onAppListLoadedFinishedHandler(int i, int i2, boolean z) {
        if (i2 == 0) {
            this.mListener.onNetworkNoError();
            if (this.mAdapter == null) {
                initAdapter();
            }
            if (this.mAdapter.getCount() == 0) {
                this.mListener.onErrorHappened(10);
                return;
            }
            this.mAdapter.notifyDataSetChanged();
            onRefreshComplete(this.mAppEngine.h());
        } else if (!z) {
            onRefreshComplete(this.mAppEngine.h());
            this.mListener.onNextPageLoadFailed();
        } else if (-800 == i2) {
            this.mListener.onErrorHappened(30);
        } else if (this.retryCount <= 0) {
            this.mListener.onErrorHappened(20);
        } else {
            this.retryCount--;
            this.mAppEngine.e();
        }
    }

    public void removeTopPadding() {
        ((ListView) this.mScrollContentView).removeHeaderView(this.topPaddingView);
    }
}
