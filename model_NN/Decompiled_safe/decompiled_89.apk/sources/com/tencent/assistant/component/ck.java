package com.tencent.assistant.component;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class ck extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ QuickEntranceView f993a;

    ck(QuickEntranceView quickEntranceView) {
        this.f993a = quickEntranceView;
    }

    public void handleMessage(Message message) {
        Bitmap bitmap;
        if (message.what == 1) {
            o oVar = (o) message.obj;
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.f993a.cards.size()) {
                    if (this.f993a.cards.get(i2) != null) {
                        if (((ColorCardItem) this.f993a.cards.get(i2)).a().equals(oVar.c())) {
                            Bitmap bitmap2 = oVar.f;
                            if (bitmap2 != null && !bitmap2.isRecycled() && i2 < this.f993a.viewTexts.size()) {
                                TextView textView = (TextView) this.f993a.viewTexts.get(i2);
                                textView.setText(Constants.STR_EMPTY);
                                textView.setBackgroundDrawable(new BitmapDrawable(bitmap2));
                            }
                        } else if (!TextUtils.isEmpty(((ColorCardItem) this.f993a.cards.get(i2)).f()) && ((ColorCardItem) this.f993a.cards.get(i2)).f().equals(oVar.c()) && (bitmap = oVar.f) != null && !bitmap.isRecycled() && i2 < this.f993a.viewIcons.size()) {
                            bitmap.setDensity(ThumbnailUtils.TARGET_SIZE_MINI_THUMBNAIL);
                            ((ImageView) this.f993a.viewIcons.get(i2)).setImageBitmap(bitmap);
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } else {
            if (message.what == 2) {
            }
        }
    }
}
