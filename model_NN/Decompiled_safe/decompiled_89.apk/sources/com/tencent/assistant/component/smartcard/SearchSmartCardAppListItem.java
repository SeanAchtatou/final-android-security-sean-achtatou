package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.b;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.adapter.smartlist.aa;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.appdetail.TXDwonloadProcessBar;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SearchSmartCardAppListItem extends SearchSmartCardBaseItem {
    private View i;
    private TextView j;
    private TextView k;
    private ImageView l;
    private ImageView m;
    private LinearLayout n;
    private aa o;
    private final String p = "17";

    public SearchSmartCardAppListItem(Context context) {
        super(context);
    }

    public SearchSmartCardAppListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardAppListItem(Context context, i iVar, SmartcardListener smartcardListener, aa aaVar) {
        super(context, iVar, smartcardListener);
        this.o = aaVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.c = this.b.inflate((int) R.layout.search_smartcard_app, this);
        } catch (Throwable th) {
            cq.a().b();
            this.c = this.b.inflate((int) R.layout.search_smartcard_app, this);
        }
        this.i = findViewById(R.id.title_ly);
        this.j = (TextView) findViewById(R.id.title);
        this.k = (TextView) findViewById(R.id.desc);
        this.l = (ImageView) findViewById(R.id.close);
        this.m = (ImageView) findViewById(R.id.divider);
        this.n = (LinearLayout) findViewById(R.id.app_list);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        this.n.removeAllViews();
        b bVar = (b) this.smartcardModel;
        if (bVar == null || bVar.f1636a <= 0 || bVar.c == null || bVar.c.size() == 0 || bVar.c.size() < bVar.f1636a) {
            setAllVisibility(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        setAllVisibility(0);
        ArrayList arrayList = new ArrayList(bVar.c);
        if (bVar.p) {
            this.l.setVisibility(0);
        } else {
            this.l.setVisibility(8);
        }
        this.l.setOnClickListener(this.g);
        this.j.setText(Html.fromHtml(bVar.k));
        int size = arrayList.size() > bVar.f1636a ? bVar.f1636a : arrayList.size();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.m.getLayoutParams();
        if (TextUtils.isEmpty(bVar.m)) {
            layoutParams.setMargins(0, df.b(14.0f), 0, 0);
            this.k.setVisibility(8);
        } else {
            layoutParams.setMargins(0, df.b(9.0f), 0, 0);
            if (bVar.i == 3) {
                long j2 = 0;
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    j2 += ((SimpleAppModel) arrayList.get(i2)).k;
                }
                this.k.setText(Html.fromHtml(bVar.m + "<font color=\"#5ac65c\">" + bt.a(j2) + "</font>"));
            } else {
                this.k.setText(bVar.m);
            }
            this.k.setVisibility(0);
        }
        this.n.addView(a(arrayList.subList(0, size)));
        if (bVar.b > bVar.f1636a) {
            View a2 = a(bVar.o + "(" + bVar.b + ")");
            this.n.addView(a2, new LinearLayout.LayoutParams(-1, df.b(40.0f)));
            a2.setOnClickListener(this.h);
        }
    }

    public void setAllVisibility(int i2) {
        this.c.setVisibility(i2);
        this.i.setVisibility(i2);
        this.n.setVisibility(i2);
    }

    private View a(List<SimpleAppModel> list) {
        LinearLayout linearLayout = new LinearLayout(this.f1115a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        for (int i2 = 0; i2 < list.size(); i2++) {
            View inflate = this.b.inflate((int) R.layout.smartcard_app_item, (ViewGroup) null);
            SimpleAppModel simpleAppModel = list.get(i2);
            ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            ((TextView) inflate.findViewById(R.id.name)).setText(simpleAppModel.d);
            TextView textView = (TextView) inflate.findViewById(R.id.size);
            textView.setText(bt.a(simpleAppModel.k));
            TXDwonloadProcessBar tXDwonloadProcessBar = (TXDwonloadProcessBar) inflate.findViewById(R.id.progress);
            tXDwonloadProcessBar.a(simpleAppModel, new View[]{textView});
            DownloadButton downloadButton = (DownloadButton) inflate.findViewById(R.id.btn);
            downloadButton.a(simpleAppModel);
            downloadButton.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            if (s.a(simpleAppModel)) {
                downloadButton.setClickable(false);
            } else {
                downloadButton.setClickable(true);
                STInfoV2 a2 = a(c(i2), 200);
                if (a2 != null) {
                    a2.scene = a(0);
                    a2.searchId = this.f;
                    a2.extraData = b(0);
                    a2.updateWithSimpleAppModel(simpleAppModel);
                }
                downloadButton.a(a2, new v(this), (d) null, downloadButton, tXDwonloadProcessBar);
            }
            inflate.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            inflate.setOnClickListener(new w(this, simpleAppModel, i2));
            linearLayout.addView(inflate, layoutParams);
        }
        return linearLayout;
    }

    private View a(String str) {
        View inflate = this.b.inflate((int) R.layout.smartcard_list_footer, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.text)).setText(str);
        ((ImageView) inflate.findViewById(R.id.icon)).setImageResource(R.drawable.go);
        return inflate;
    }

    /* access modifiers changed from: private */
    public String c(int i2) {
        return c() + ct.a(i2 + 1);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a("17", this.o == null ? 0 : this.o.a());
    }

    /* access modifiers changed from: protected */
    public int d() {
        return com.tencent.assistantv2.st.page.d.f3360a;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.o == null) {
            return null;
        }
        return this.o.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.o == null) {
            return 0;
        }
        return this.o.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.o == null) {
            return 2000;
        }
        return this.o.b();
    }
}
