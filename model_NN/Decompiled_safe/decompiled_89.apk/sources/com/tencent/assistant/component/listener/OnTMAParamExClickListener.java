package com.tencent.assistant.component.listener;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public abstract class OnTMAParamExClickListener extends OnTMAClickListener {
    public abstract STInfoV2 getStInfo(View view);

    /* access modifiers changed from: protected */
    public void userActionReport(View view) {
        STInfoV2 stInfo = getStInfo(view);
        if (stInfo != null && (view.getContext() instanceof BaseActivity)) {
            ((BaseActivity) view.getContext()).a(stInfo.slotId, stInfo.status);
        }
        k.a(stInfo);
    }
}
