package com.tencent.assistant.component.appdetail;

import android.view.View;

/* compiled from: ProGuard */
class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBarTabView f910a;

    a(AppBarTabView appBarTabView) {
        this.f910a = appBarTabView;
    }

    public void onClick(View view) {
        if (this.f910a.b != null) {
            this.f910a.b.setVisibility(0);
            this.f910a.b.reload();
        }
        this.f910a.g.setVisibility(4);
    }
}
