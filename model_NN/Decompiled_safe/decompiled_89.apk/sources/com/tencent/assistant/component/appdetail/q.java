package com.tencent.assistant.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class q extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CustomRelateAppView f940a;

    q(CustomRelateAppView customRelateAppView) {
        this.f940a = customRelateAppView;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.ralate_app_layout1 /*2131165582*/:
                if (this.f940a.f.size() > 0) {
                    this.f940a.a((SimpleAppModel) this.f940a.f.get(0));
                    return;
                }
                return;
            case R.id.ralate_app_layout2 /*2131165586*/:
                if (this.f940a.f.size() > 1) {
                    this.f940a.a((SimpleAppModel) this.f940a.f.get(1));
                    return;
                }
                return;
            case R.id.ralate_app_layout3 /*2131165590*/:
                if (this.f940a.f.size() > 2) {
                    this.f940a.a((SimpleAppModel) this.f940a.f.get(2));
                    return;
                }
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo() {
        SimpleAppModel simpleAppModel;
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f940a.f903a, 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = this.f940a.a(this.f940a.position, this.f940a.a(getClickViewId()));
            int a2 = this.f940a.a(getClickViewId());
            if (this.f940a.f.size() > a2 && (simpleAppModel = (SimpleAppModel) this.f940a.f.get(a2)) != null) {
                buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
            }
        }
        return buildSTInfo;
    }
}
