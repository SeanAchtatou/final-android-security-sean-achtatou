package com.tencent.assistant.component.homeEntry;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bc;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HomeEntryCellBase f1054a;

    a(HomeEntryCellBase homeEntryCellBase) {
        this.f1054a = homeEntryCellBase;
    }

    public void onTMAClick(View view) {
        XLog.v("home_entry", "HomeEntryCellBaseOnClickListener---onTMAClick--");
        if (this.f1054a.b != null && this.f1054a.b.e != null && !TextUtils.isEmpty(this.f1054a.b.e.f1970a)) {
            b.a(this.f1054a.f1052a, this.f1054a.b.e);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1054a.getContext(), 200);
        buildSTInfo.scene = STConst.ST_PAGE_COMPETITIVE;
        if (this.f1054a.b != null) {
            bc.a(this.f1054a.b.f, buildSTInfo);
        }
        XLog.v("home_entry", "HomeEntryCellBaseOnClickListener---getStInfo--stInfo.slotId= " + buildSTInfo.slotId + "--stInfo.pushInfo= " + buildSTInfo.pushInfo);
        return buildSTInfo;
    }
}
