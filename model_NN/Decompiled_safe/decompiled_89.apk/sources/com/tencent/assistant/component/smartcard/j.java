package com.tencent.assistant.component.smartcard;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class j extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfUpdateManager.SelfUpdateInfo f1149a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardSelfUpdateItem c;

    j(NormalSmartCardSelfUpdateItem normalSmartCardSelfUpdateItem, SelfUpdateManager.SelfUpdateInfo selfUpdateInfo, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardSelfUpdateItem;
        this.f1149a = selfUpdateInfo;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (this.c.t == this.f1149a.f1469a) {
            long unused = this.c.t = 0;
            this.c.r.setVisibility(0);
            this.c.s.setVisibility(8);
            this.c.p.setImageDrawable(this.c.f1115a.getResources().getDrawable(R.drawable.icon_open));
            return;
        }
        long unused2 = this.c.t = this.f1149a.f1469a;
        this.c.r.setVisibility(8);
        this.c.s.setVisibility(0);
        this.c.p.setImageDrawable(this.c.f1115a.getResources().getDrawable(R.drawable.icon_close));
    }

    public STInfoV2 getStInfo() {
        return this.b;
    }
}
