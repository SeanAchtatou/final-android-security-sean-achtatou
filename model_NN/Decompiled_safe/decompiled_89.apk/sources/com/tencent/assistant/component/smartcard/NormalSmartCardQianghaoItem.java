package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Process;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.bd;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.n;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class NormalSmartCardQianghaoItem extends NormalSmartcardBaseItem {
    n f;
    private TextView i;
    private TXImageView j;
    private TextView k;
    private ListItemInfoView l;
    private TextView m;
    private DownloadButton n;
    private boolean o;

    public NormalSmartCardQianghaoItem(Context context) {
        this(context, null);
    }

    public NormalSmartCardQianghaoItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f = null;
        this.o = false;
    }

    public NormalSmartCardQianghaoItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
        this.f = null;
        this.o = false;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.b.inflate((int) R.layout.smartcard_qianghao, this);
            this.o = false;
            this.i = (TextView) findViewById(R.id.qianghao_num);
            this.j = (TXImageView) findViewById(R.id.iconimg);
            this.k = (TextView) findViewById(R.id.appname);
            this.n = (DownloadButton) findViewById(R.id.downloadbtn);
            this.l = (ListItemInfoView) findViewById(R.id.iteminfo);
            this.l.a(ListItemInfoView.InfoType.CATEGORY_SIZE);
            this.m = (TextView) findViewById(R.id.desc);
            f();
        } catch (Throwable th) {
            cq.a().b();
            this.o = true;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.o) {
            a();
        } else {
            f();
        }
    }

    private void f() {
        if (!this.o) {
            if (this.smartcardModel instanceof n) {
                this.f = (n) this.smartcardModel;
            }
            if (this.f != null && this.f.b != null) {
                this.n.a(this.f.b);
                STInfoV2 a2 = a("03_001", 200);
                if (a2 != null) {
                    a2.updateWithSimpleAppModel(this.f.b);
                }
                if (s.a(this.f.b)) {
                    this.n.setClickable(false);
                } else {
                    this.n.setClickable(true);
                    this.n.a(a2);
                }
                setOnClickListener(new h(this, this.f.u, a2));
                this.j.updateImageView(this.f.b.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                this.k.setText(this.f.b.d);
                this.l.a(this.f.b);
                if (bd.a().b(this.f.b.f1634a)) {
                    this.i.setText(a(String.valueOf(this.f.c + Process.LAST_APPLICATION_UID + 1)));
                } else {
                    this.i.setText(a(String.valueOf(this.f.c + Process.LAST_APPLICATION_UID)));
                }
                this.m.setText(this.f.d);
            }
        }
    }

    private String a(String str) {
        if (str == null || str.length() < 2) {
            return str;
        }
        String str2 = Constants.STR_EMPTY;
        for (int i2 = 0; i2 < str.length(); i2++) {
            str2 = str2 + str.charAt(i2);
            if (i2 < str.length() - 1) {
                str2 = str2 + " ";
            }
        }
        return str2;
    }

    /* access modifiers changed from: protected */
    public SimpleAppModel c() {
        if (this.f != null) {
            return this.f.b;
        }
        return null;
    }
}
