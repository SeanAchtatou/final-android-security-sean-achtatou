package com.tencent.assistant.component.treasurebox;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.assistant.component.treasurebox.AppTreasureBoxCell;
import com.tencent.assistant.m;
import com.tencent.assistant.model.s;
import com.tencent.assistant.utils.df;
import java.util.List;

/* compiled from: ProGuard */
public class AppTreasureBoxView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1178a = true;

    public AppTreasureBoxView(Context context) {
        super(context);
        a(context);
    }

    public AppTreasureBoxView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        setOrientation(1);
    }

    public void onGoInScreen() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof AppTreasureBoxCell1) {
                ((AppTreasureBoxCell1) childAt).onGoinScreen();
            }
        }
    }

    public void onLeaveScreen() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof AppTreasureBoxCell1) {
                ((AppTreasureBoxCell1) childAt).onLeaveScreen();
            }
        }
    }

    public void onDestroy() {
        removeAll();
    }

    public void refreshItemList(List<s> list, AppTreasureBoxCell.AppTreasureBoxItemClickListener appTreasureBoxItemClickListener) {
        if (list == null || list.isEmpty()) {
            removeAllViews();
            addView(AppTreasureBoxCell.createCell(false, null, getContext(), appTreasureBoxItemClickListener, 0));
            this.f1178a = false;
            return;
        }
        this.f1178a = true;
        int size = list.size();
        removeAllViews();
        for (int i = 0; i < size; i++) {
            AppTreasureBoxCell1 appTreasureBoxCell1 = (AppTreasureBoxCell1) AppTreasureBoxCell.createCell(true, list.get(i), getContext(), appTreasureBoxItemClickListener, i);
            if (i == size - 1) {
                if (size == 1) {
                    setPadding(0, 0, 0, df.a(getContext(), 57.0f));
                    appTreasureBoxCell1.setBottomDividerVisibility(true);
                } else {
                    setPadding(0, 0, 0, df.a(getContext(), 0.0f));
                    appTreasureBoxCell1.setBottomDividerVisibility(false);
                }
            }
            addView(appTreasureBoxCell1);
        }
        m.a().O();
    }

    public boolean removeChild(int i) {
        int childCount;
        if (!this.f1178a || i >= (childCount = getChildCount())) {
            return false;
        }
        for (int i2 = 0; i2 < childCount; i2++) {
            if (i == i2) {
                if (getChildAt(i2) instanceof AppTreasureBoxCell1) {
                    removeViewAt(i2);
                }
                return true;
            }
        }
        return false;
    }

    public void removeAll() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof AppTreasureBoxCell1) {
                AppTreasureBoxCell1 appTreasureBoxCell1 = (AppTreasureBoxCell1) childAt;
                appTreasureBoxCell1.onLeaveScreen();
                removeView(appTreasureBoxCell1);
            }
        }
    }
}
