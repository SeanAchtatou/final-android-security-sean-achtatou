package com.tencent.assistant.component.popwindow;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackupRecoveryPopWindowView f1102a;

    a(BackupRecoveryPopWindowView backupRecoveryPopWindowView) {
        this.f1102a = backupRecoveryPopWindowView;
    }

    public void onTMAClick(View view) {
        this.f1102a.d();
    }

    public STInfoV2 getStInfo() {
        String str;
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1102a.b, null, STConst.ST_DEFAULT_SLOT, STConstAction.ACTION_HIT_STARTPOP_INSTALL, null);
        if (buildSTInfo != null) {
            String str2 = Constants.STR_EMPTY;
            ArrayList<Boolean> c = this.f1102a.f1101a.c();
            for (int i = 0; i < this.f1102a.l.size(); i++) {
                if (c.get(i).booleanValue()) {
                    if (str2.length() > 0) {
                        str = str2 + "|";
                    } else {
                        str = str2;
                    }
                    str2 = str + ((SimpleAppModel) this.f1102a.l.get(i)).f1634a + "|" + ((SimpleAppModel) this.f1102a.l.get(i)).c + "|" + ((SimpleAppModel) this.f1102a.l.get(i)).g;
                }
            }
            buildSTInfo.extraData = str2;
            buildSTInfo.recommendId = this.f1102a.a(0);
        }
        return buildSTInfo;
    }
}
