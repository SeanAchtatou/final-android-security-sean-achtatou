package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class LineBreakTextView extends TextView {
    private ArrayList<String> lines = new ArrayList<>();
    private int mMaxLines = -1;
    private Paint paint = getPaint();

    public LineBreakTextView(Context context) {
        super(context);
    }

    public LineBreakTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public LineBreakTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.save();
        int lineHeight = getLineHeight();
        this.paint.setColor(getCurrentTextColor());
        canvas.translate((float) getPaddingLeft(), (float) (getPaddingTop() + ((int) (((((float) lineHeight) - this.paint.descent()) - this.paint.ascent()) / 2.0f))));
        int i = 0;
        for (int i2 = 0; i2 < this.lines.size(); i2++) {
            canvas.drawText(this.lines.get(i2), 0.0f, (float) i, this.paint);
            i += lineHeight;
        }
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int lineHeight;
        String substring;
        super.onMeasure(i, i2);
        if (getText() == null || getText().length() == 0 || getVisibility() == 8) {
            setMeasuredDimension(0, 0);
            return;
        }
        String obj = getText().toString();
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2) + getPaddingTop() + getPaddingBottom();
        int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
        this.lines.clear();
        if (this.paint.measureText(obj) <= ((float) paddingLeft)) {
            this.lines.add(obj);
            lineHeight = size2 + getLineHeight();
        } else if (this.mMaxLines > 0) {
            int i3 = 0;
            int i4 = 0;
            lineHeight = size2;
            int i5 = 0;
            while (i4 < this.mMaxLines) {
                obj = obj.substring(i3);
                if (i4 != this.mMaxLines - 1) {
                    i3 = this.paint.breakText(obj, true, (float) paddingLeft, null);
                    substring = obj.substring(0, i3);
                } else if (this.paint.measureText(obj) > ((float) paddingLeft)) {
                    i3 = this.paint.breakText(obj, true, ((float) paddingLeft) - this.paint.measureText("..."), null);
                    substring = obj.substring(0, i3) + "...";
                } else {
                    i3 = i5;
                    substring = obj;
                }
                this.lines.add(substring);
                lineHeight += getLineHeight();
                i4++;
                i5 = i3;
            }
        } else {
            int i6 = size2;
            String str = obj;
            int i7 = 0;
            int i8 = 0;
            while (true) {
                str = str.substring(i7);
                if (this.paint.measureText(str) <= ((float) paddingLeft)) {
                    break;
                }
                i7 = this.paint.breakText(str, true, (float) paddingLeft, null);
                this.lines.add(str.substring(0, i7));
                i6 += getLineHeight();
                i8++;
            }
            this.lines.add(str);
            lineHeight = i6 + getLineHeight();
        }
        setMeasuredDimension(size, lineHeight);
    }

    public void setMaxLines(int i) {
        this.mMaxLines = i;
        requestLayout();
    }
}
