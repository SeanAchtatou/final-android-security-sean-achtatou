package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.t;
import com.tencent.assistant.model.a.u;
import com.tencent.assistantv2.st.b.d;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class SmartSquareCardWithUser extends NormalSmartcardBaseItem {
    private TextView f;
    private View i;
    private View j;
    private ImageView k;
    private ImageView l;
    private ImageView m;
    private ImageView n;
    private SmartSquareAppWithUserItem o;
    private SmartSquareAppWithUserItem p;
    private SmartSquareAppWithUserItem q;
    private SmartSquareAppWithUserItem r;
    private TextView s;
    private ImageView t;
    private View u;

    public SmartSquareCardWithUser(Context context) {
        this(context, null);
    }

    public SmartSquareCardWithUser(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SmartSquareCardWithUser(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    public void setDataSource(List<t> list) {
        if (list == null || list.size() < 2) {
            setVisibility(8);
            setMinimumHeight(0);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setVisibility(0);
        for (int i2 = 0; i2 < 2; i2++) {
            t tVar = list.get(i2);
            STInfoV2 a2 = a(tVar, i2);
            if (!(tVar == null || tVar.f1653a == null)) {
                if (i2 == 0) {
                    this.o.setData(tVar, a2);
                } else {
                    this.p.setData(tVar, a2);
                }
            }
        }
        if (list.size() >= 4) {
            this.j.setVisibility(0);
            this.l.setVisibility(0);
            this.m.setVisibility(0);
            this.n.setVisibility(8);
            this.k.setVisibility(0);
            for (int i3 = 2; i3 < 4; i3++) {
                t tVar2 = list.get(i3);
                STInfoV2 a3 = a(tVar2, i3);
                if (!(tVar2 == null || tVar2.f1653a == null)) {
                    new SmartSquareAppItem(this.f1115a).setData(tVar2, a3);
                    if (i3 == 2) {
                        this.q.setData(tVar2, a3);
                    } else {
                        this.r.setData(tVar2, a3);
                    }
                }
            }
            return;
        }
        this.j.setVisibility(8);
        this.m.setVisibility(8);
        this.k.setVisibility(8);
        this.l.setVisibility(8);
        this.n.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_square_with_user, this);
        this.f = (TextView) findViewById(R.id.title);
        this.i = findViewById(R.id.topPart);
        this.j = findViewById(R.id.bottomPart);
        this.k = (ImageView) findViewById(R.id.line1_split);
        this.l = (ImageView) findViewById(R.id.line2_split);
        this.m = (ImageView) findViewById(R.id.line_split);
        this.n = (ImageView) findViewById(R.id.line1_single_split);
        this.o = (SmartSquareAppWithUserItem) findViewById(R.id.topleft_anchor);
        this.p = (SmartSquareAppWithUserItem) findViewById(R.id.topright_anchor);
        this.q = (SmartSquareAppWithUserItem) findViewById(R.id.bottomleft_anchor);
        this.r = (SmartSquareAppWithUserItem) findViewById(R.id.bottomright_anchor);
        this.s = (TextView) findViewById(R.id.show_all_titlte);
        this.t = (ImageView) findViewById(R.id.show_all_img);
        this.u = findViewById(R.id.show_all_layout);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        u uVar = null;
        if (this.smartcardModel instanceof u) {
            uVar = (u) this.smartcardModel;
        }
        if (uVar != null) {
            this.f.setText(uVar.k);
            if (uVar.b()) {
                this.f.setBackgroundResource(R.drawable.common_index_tag);
                this.f.setTextColor(getResources().getColor(R.color.smart_card_applist_title_font_color));
            } else {
                this.f.setBackgroundResource(0);
                this.f.setTextColor(getResources().getColor(R.color.apk_name_v5));
            }
            String str = uVar.n;
            if (TextUtils.isEmpty(str) || TextUtils.isEmpty(uVar.o)) {
                this.u.setVisibility(8);
            } else {
                this.u.setVisibility(0);
                this.s.setText(uVar.o);
                this.u.setOnClickListener(new ao(this, str));
            }
            setDataSource(uVar.a());
        }
    }

    private STInfoV2 a(t tVar, int i2) {
        STInfoV2 a2 = a(a.a("03", i2), 100);
        if (!(a2 == null || tVar == null)) {
            a2.updateWithSimpleAppModel(tVar.f1653a);
        }
        if (this.e == null) {
            this.e = new d();
        }
        this.e.a(a2);
        return a2;
    }
}
