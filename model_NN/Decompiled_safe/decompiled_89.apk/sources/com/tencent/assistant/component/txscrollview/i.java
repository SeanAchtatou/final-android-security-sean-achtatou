package com.tencent.assistant.component.txscrollview;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.thumbnailCache.o;
import java.util.HashMap;

/* compiled from: ProGuard */
class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXImageView f1205a;

    i(TXImageView tXImageView) {
        this.f1205a = tXImageView;
    }

    public void handleMessage(Message message) {
        Bitmap bitmap;
        if (message.what == 0) {
            o oVar = (o) message.obj;
            if (oVar != null) {
                Bitmap unused = this.f1205a.mBitmap = oVar.f;
                if (this.f1205a.invalidater != null) {
                    if (oVar.c().equals(this.f1205a.mImageUrlString)) {
                        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(0);
                        HashMap hashMap = new HashMap();
                        hashMap.put("URL", oVar.c());
                        hashMap.put("TYPE", Integer.valueOf(oVar.d()));
                        viewInvalidateMessage.params = hashMap;
                        viewInvalidateMessage.target = this.f1205a.invalidateHandler;
                        this.f1205a.invalidater.sendMessage(viewInvalidateMessage);
                    }
                } else if (oVar.d() == this.f1205a.mImageType.getThumbnailRequestType() && oVar.c().equals(this.f1205a.mImageUrlString) && (bitmap = oVar.f) != null && !bitmap.isRecycled()) {
                    try {
                        this.f1205a.setImageBitmap(bitmap);
                    } catch (Throwable th) {
                        cq.a().b();
                    }
                    boolean unused2 = this.f1205a.mIsLoadFinish = true;
                    this.f1205a.onImageLoadFinishCallListener(bitmap);
                }
            }
        } else {
            this.f1205a.sendImageRequestToInvalidater();
        }
    }
}
