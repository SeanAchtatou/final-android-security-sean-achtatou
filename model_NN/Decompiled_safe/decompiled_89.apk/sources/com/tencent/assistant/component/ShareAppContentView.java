package com.tencent.assistant.component;

import android.content.Context;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.ShareAppBar;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class ShareAppContentView extends LinearLayout implements ShareAppBar.ShareAppCallback {
    private int mContentLength = -1;
    private ImageView mMascotView;
    private TextView mShareContent;
    private ShareAppBar shareAppBar = null;
    private LinearLayout shareAppBottomBarLayout = null;
    private TextView tvShareAppBarTxt = null;

    public ShareAppContentView(Context context) {
        super(context);
        initViews(context);
    }

    public ShareAppContentView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initViews(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.assistant.component.ShareAppContentView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initViews(Context context) {
        setOrientation(1);
        LayoutInflater.from(context).inflate((int) R.layout.dialog_share_yyb_once, (ViewGroup) this, true);
        this.mShareContent = (TextView) findViewById(R.id.share_content);
        this.mMascotView = (ImageView) findViewById(R.id.yyb_mascot);
        this.tvShareAppBarTxt = (TextView) findViewById(R.id.tv_share_app_bar_txt);
        this.tvShareAppBarTxt.setVisibility(8);
        this.shareAppBottomBarLayout = (LinearLayout) findViewById(R.id.share_app_bottom_bar_layout);
        this.shareAppBar = (ShareAppBar) findViewById(R.id.layout_share);
        if (this.shareAppBar != null) {
            this.shareAppBar.setShareAppCallback(this);
        }
    }

    public void setShareContentText(CharSequence charSequence) {
        if (this.mContentLength != -1) {
            this.mShareContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(this.mContentLength)});
            ((LinearLayout.LayoutParams) this.mShareContent.getLayoutParams()).rightMargin = df.a(getContext(), 15.0f);
            requestLayout();
        }
        this.mShareContent.setText(charSequence);
    }

    public void setMascotSrc(int i) {
        this.mMascotView.setImageResource(i);
    }

    public void setContentLength(int i) {
        this.mContentLength = i;
    }

    public void noSupportShareApp() {
        if (this.shareAppBottomBarLayout != null) {
            this.shareAppBottomBarLayout.setVisibility(8);
        }
    }
}
