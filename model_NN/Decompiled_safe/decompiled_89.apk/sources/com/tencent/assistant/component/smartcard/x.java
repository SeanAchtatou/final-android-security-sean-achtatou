package com.tencent.assistant.component.smartcard;

import android.view.View;

/* compiled from: ProGuard */
class x implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardBaseItem f1163a;

    x(SearchSmartCardBaseItem searchSmartCardBaseItem) {
        this.f1163a = searchSmartCardBaseItem;
    }

    public void onClick(View view) {
        if (this.f1163a.smartcardListener != null) {
            this.f1163a.smartcardListener.onActionClose(this.f1163a.smartcardModel.i, this.f1163a.smartcardModel.j);
        }
        this.f1163a.a("04_001", 200, this.f1163a.smartcardModel.r, -1);
    }
}
