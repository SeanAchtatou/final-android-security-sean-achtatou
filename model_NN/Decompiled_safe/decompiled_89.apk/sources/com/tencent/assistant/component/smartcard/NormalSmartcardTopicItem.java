package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.v;

/* compiled from: ProGuard */
public class NormalSmartcardTopicItem extends NormalSmartcardBaseItem {
    protected ImageView f;
    protected TXImageView i;
    protected TextView j;
    protected RelativeLayout k;
    private boolean l;

    public NormalSmartcardTopicItem(Context context) {
        super(context);
    }

    public NormalSmartcardTopicItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardTopicItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.c = this.b.inflate((int) R.layout.smartcard_topic, this);
            this.i = (TXImageView) this.c.findViewById(R.id.topic_pic);
            this.l = false;
            setOnClickListener(this.h);
            this.k = (RelativeLayout) this.c.findViewById(R.id.topic_more_layout);
            this.j = (TextView) this.c.findViewById(R.id.topic_title);
            this.f = (ImageView) this.c.findViewById(R.id.close);
            this.f.setOnClickListener(this.g);
            f();
        } catch (Exception e) {
            this.l = true;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.l) {
            a();
        } else {
            f();
        }
    }

    /* access modifiers changed from: protected */
    public void f() {
        if (!this.l) {
            v vVar = null;
            if (this.smartcardModel instanceof v) {
                vVar = (v) this.smartcardModel;
            }
            if (vVar != null) {
                this.i.setInvalidater(this.d);
                this.i.updateImageView(vVar.f1655a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
                this.k.setVisibility(8);
                vVar.a(false);
                if (vVar.a()) {
                    this.f.setVisibility(0);
                } else {
                    this.f.setVisibility(8);
                }
            }
        }
    }
}
