package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.a.g;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.protocol.jce.SmartCardLibaoItem;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class NormalSmartcardLibaoItem extends NormalSmartcardBaseItem {
    private View f;
    private TextView i;
    private TextView j;
    private ImageView k;
    private ImageView l;
    private LinearLayout m;

    public NormalSmartcardLibaoItem(Context context) {
        super(context);
    }

    public NormalSmartcardLibaoItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardLibaoItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_libao, this);
        this.f = findViewById(R.id.title_ly);
        this.i = (TextView) findViewById(R.id.title);
        this.j = (TextView) findViewById(R.id.desc);
        this.k = (ImageView) findViewById(R.id.close);
        this.l = (ImageView) findViewById(R.id.divider);
        this.m = (LinearLayout) findViewById(R.id.gift_list);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        boolean z;
        this.m.removeAllViews();
        g gVar = (g) this.smartcardModel;
        if (gVar == null || gVar.c == null || gVar.c.size() == 0) {
            setAllVisibility(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        setAllVisibility(0);
        ArrayList<SmartCardLibaoItem> arrayList = gVar.c;
        if (gVar.p) {
            this.k.setVisibility(0);
        } else {
            this.k.setVisibility(8);
        }
        this.i.setText(gVar.k);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.l.getLayoutParams();
        if (TextUtils.isEmpty(gVar.m)) {
            layoutParams.setMargins(0, df.b(14.0f), 0, 0);
            this.j.setVisibility(8);
        } else {
            layoutParams.setMargins(0, df.b(9.0f), 0, 0);
            this.j.setText(gVar.m);
            this.j.setVisibility(0);
        }
        this.k.setOnClickListener(this.g);
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            SmartCardLibaoItem smartCardLibaoItem = arrayList.get(i2);
            if (i2 == size - 1) {
                z = true;
            } else {
                z = false;
            }
            this.m.addView(a(smartCardLibaoItem, z, i2));
        }
        if (gVar.b > size) {
            View a2 = a(gVar.o);
            this.m.addView(a2, new LinearLayout.LayoutParams(-1, df.b(40.0f)));
            a2.setOnClickListener(this.h);
        }
    }

    public void setAllVisibility(int i2) {
        this.c.setVisibility(i2);
        this.f.setVisibility(i2);
        this.m.setVisibility(i2);
    }

    private View a(SmartCardLibaoItem smartCardLibaoItem, boolean z, int i2) {
        View inflate = this.b.inflate((int) R.layout.smartcard_libao_item, (ViewGroup) null);
        ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(smartCardLibaoItem.f2322a, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        ((TextView) inflate.findViewById(R.id.name)).setText(smartCardLibaoItem.b);
        ((TextView) inflate.findViewById(R.id.desc)).setText(smartCardLibaoItem.c);
        TextView textView = (TextView) inflate.findViewById(R.id.btn);
        r rVar = new r(this, smartCardLibaoItem, a(a.a("03", i2), 200));
        textView.setId(11112222);
        textView.setTag(R.id.tma_st_smartcard_tag, e());
        textView.setOnClickListener(rVar);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.divider);
        if (z) {
            imageView.setVisibility(8);
        } else {
            imageView.setVisibility(0);
        }
        inflate.setTag(R.id.tma_st_smartcard_tag, e());
        inflate.setOnClickListener(rVar);
        return inflate;
    }

    private View a(String str) {
        View inflate = this.b.inflate((int) R.layout.smartcard_list_footer, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.text)).setText(str);
        ((ImageView) inflate.findViewById(R.id.icon)).setImageResource(R.drawable.go);
        return inflate;
    }
}
