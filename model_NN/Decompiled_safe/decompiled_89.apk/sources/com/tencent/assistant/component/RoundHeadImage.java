package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.bf;

/* compiled from: ProGuard */
public class RoundHeadImage extends TXImageView {
    private Bitmap mBitmap;
    private Paint mPaint = new Paint();

    public RoundHeadImage(Context context) {
        super(context);
    }

    public RoundHeadImage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        this.mBitmap = bf.a(drawable);
        invalidate();
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
        invalidate();
    }

    public void setBackgroundResource(int i) {
        this.mBitmap = bf.a(getContext().getResources().getDrawable(i));
        invalidate();
    }

    public void setImageResource(int i) {
        this.mBitmap = bf.a(getContext().getResources().getDrawable(i));
        invalidate();
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawBitmap(bf.a(this.mBitmap, getWidth(), getHeight()), 0.0f, 0.0f, this.mPaint);
    }
}
