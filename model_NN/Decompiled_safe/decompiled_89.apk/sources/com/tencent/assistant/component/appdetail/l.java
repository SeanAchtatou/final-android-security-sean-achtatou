package com.tencent.assistant.component.appdetail;

import android.view.GestureDetector;
import android.view.MotionEvent;

/* compiled from: ProGuard */
class l extends GestureDetector.SimpleOnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailScrollView f921a;

    l(AppdetailScrollView appdetailScrollView) {
        this.f921a = appdetailScrollView;
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (f2 < 0.0f) {
            if (this.f921a.getScrollY() >= this.f921a.f899a && this.f921a.i != null) {
                this.f921a.i.fling(-((int) f2));
                this.f921a.i.scrollDeltaY(0);
            }
        } else if (this.f921a.i != null && this.f921a.i.canScrollDown()) {
            this.f921a.i.fling(-((int) f2));
            this.f921a.i.scrollDeltaY(0);
        }
        return false;
    }
}
