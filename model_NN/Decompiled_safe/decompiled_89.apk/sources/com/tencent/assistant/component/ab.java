package com.tencent.assistant.component;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentReply;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.component.RankNormalListView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
class ab extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f880a;
    final /* synthetic */ List b;
    final /* synthetic */ boolean c;
    final /* synthetic */ ViewGroup d;
    final /* synthetic */ int e;
    final /* synthetic */ CommentDetail f;
    final /* synthetic */ int g;
    final /* synthetic */ TextView h;
    final /* synthetic */ boolean i;
    final /* synthetic */ CommentDetailView j;

    ab(CommentDetailView commentDetailView, ac acVar, List list, boolean z, ViewGroup viewGroup, int i2, CommentDetail commentDetail, int i3, TextView textView, boolean z2) {
        this.j = commentDetailView;
        this.f880a = acVar;
        this.b = list;
        this.c = z;
        this.d = viewGroup;
        this.e = i2;
        this.f = commentDetail;
        this.g = i3;
        this.h = textView;
        this.i = z2;
    }

    public void onTMAClick(View view) {
        int i2;
        this.f880a.g = true;
        boolean booleanValue = ((Boolean) view.getTag()).booleanValue();
        int size = this.b.size();
        if (this.c) {
            this.d.removeViewAt(0);
            int i3 = size > 4 ? size - 4 : 0;
            for (int i4 = i3; i4 < size; i4++) {
                CommentReply commentReply = (CommentReply) this.b.get(i4);
                this.d.addView(this.j.getReplyView(commentReply.e, this.e, commentReply.d, commentReply.c, commentReply.b), i4 - i3);
            }
            if (size > 4) {
                this.d.addView(this.j.getMoreReplyLayout(this.f880a, this.f, size - 4, this.b.subList(0, i3), true, this.e, this.d, booleanValue, this.g), 0, new LinearLayout.LayoutParams(-1, df.b(36.0f)));
                return;
            }
            return;
        }
        this.d.removeViewAt(this.d.getChildCount() - 1);
        if (size > 4) {
            i2 = 4;
        } else {
            i2 = size;
        }
        for (int i5 = 0; i5 < i2; i5++) {
            CommentReply commentReply2 = (CommentReply) this.b.get(i5);
            View access$1100 = this.j.getReplyView(commentReply2.e, this.e, commentReply2.d, commentReply2.c, commentReply2.b);
            this.d.addView(access$1100);
            if (i5 == i2 - 1 && size <= 4) {
                access$1100.findViewById(R.id.divider).setVisibility(8);
            }
        }
        if (size > 4) {
            this.d.addView(this.j.getMoreReplyLayout(this.f880a, this.f, size - 4, this.b.subList(i2, size), false, this.e, this.d, booleanValue, this.g), new LinearLayout.LayoutParams(-1, df.b(36.0f)));
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.j.getContext() instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 i2 = ((AppDetailActivityV5) this.j.getContext()).i();
        int intValue = ((Integer) this.h.getTag(R.id.tag)).intValue();
        if (this.i) {
            i2.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, intValue + this.e);
        } else {
            i2.slotId = a.a("09", intValue + this.e);
        }
        i2.actionId = 200;
        return i2;
    }
}
