package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/* compiled from: ProGuard */
public class CustomBackupRelativeLayout extends RelativeLayout {
    private boolean isClickable = true;

    public CustomBackupRelativeLayout(Context context) {
        super(context);
    }

    public CustomBackupRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.isClickable) {
            return true;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.isClickable) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setClickEnable(boolean z) {
        this.isClickable = z;
    }

    public boolean getClickEnable() {
        return this.isClickable;
    }
}
