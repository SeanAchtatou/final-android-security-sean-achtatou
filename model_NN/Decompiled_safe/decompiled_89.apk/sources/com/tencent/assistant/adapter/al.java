package com.tencent.assistant.adapter;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.wisedownload.r;
import java.util.Comparator;

/* compiled from: ProGuard */
class al implements Comparator<SimpleAppModel> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f698a;

    al(ac acVar) {
        this.f698a = acVar;
    }

    /* renamed from: a */
    public int compare(SimpleAppModel simpleAppModel, SimpleAppModel simpleAppModel2) {
        if (!(simpleAppModel == null || simpleAppModel2 == null)) {
            boolean b = r.b(simpleAppModel);
            boolean b2 = r.b(simpleAppModel2);
            if (b) {
                return -1;
            }
            if (b2) {
                return 1;
            }
        }
        return 0;
    }
}
