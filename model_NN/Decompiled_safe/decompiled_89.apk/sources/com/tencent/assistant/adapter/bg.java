package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.model.d;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class bg extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f720a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ DownloadInfoMultiAdapter c;

    bg(DownloadInfoMultiAdapter downloadInfoMultiAdapter, d dVar, STInfoV2 sTInfoV2) {
        this.c = downloadInfoMultiAdapter;
        this.f720a = dVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (this.f720a != null) {
            bh bhVar = new bh(this);
            bhVar.titleRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_title);
            bhVar.contentRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm);
            bhVar.rBtnTxtRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_btn);
            v.a(bhVar);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = Constants.VIA_REPORT_TYPE_MAKE_FRIEND;
        }
        return this.b;
    }
}
