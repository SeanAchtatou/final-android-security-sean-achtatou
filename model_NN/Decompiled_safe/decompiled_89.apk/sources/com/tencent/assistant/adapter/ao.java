package com.tencent.assistant.adapter;

import com.tencent.assistant.model.spaceclean.a;
import java.util.Comparator;

/* compiled from: ProGuard */
class ao implements Comparator<a> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileAdapter f701a;

    ao(BigFileAdapter bigFileAdapter) {
        this.f701a = bigFileAdapter;
    }

    /* renamed from: a */
    public int compare(a aVar, a aVar2) {
        if (aVar == null || aVar2 == null) {
            return 0;
        }
        return aVar.h - aVar2.h;
    }
}
