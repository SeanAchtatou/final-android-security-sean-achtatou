package com.tencent.assistant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.Feedback;
import com.tencent.assistant.utils.cv;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class HelperFeedbackAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f673a;
    private LayoutInflater b = null;
    private View.OnClickListener c = null;
    private ArrayList<Feedback> d;

    public HelperFeedbackAdapter() {
    }

    public HelperFeedbackAdapter(Context context, ArrayList<Feedback> arrayList) {
        this.f673a = context;
        this.b = LayoutInflater.from(context);
        this.d = arrayList;
    }

    public void a(View.OnClickListener onClickListener) {
        this.c = onClickListener;
    }

    public void a(ArrayList<Feedback> arrayList) {
        if (arrayList != null) {
            if (this.d != null) {
                this.d.clear();
            } else {
                this.d = new ArrayList<>();
            }
            this.d.addAll(arrayList);
            notifyDataSetChanged();
        }
    }

    public void b(ArrayList<Feedback> arrayList) {
        if (this.d == null) {
            this.d = new ArrayList<>();
        }
        this.d.addAll(arrayList);
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.d != null) {
            return this.d.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.d != null) {
            return this.d.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        cc ccVar;
        if (view == null) {
            view = this.b.inflate((int) R.layout.feedback_list_item, (ViewGroup) null);
            ccVar = new cc();
            ccVar.f736a = (TextView) view.findViewById(R.id.feedback_time);
            ccVar.b = (TextView) view.findViewById(R.id.feedback_answer_time);
            ccVar.c = (TextView) view.findViewById(R.id.feedback_question_author);
            ccVar.d = (TextView) view.findViewById(R.id.feedback_question);
            ccVar.e = (TextView) view.findViewById(R.id.feedback_answer_author);
            ccVar.f = (TextView) view.findViewById(R.id.feedback_answer);
            view.setTag(ccVar);
        } else {
            ccVar = (cc) view.getTag();
        }
        view.setOnClickListener(this.c);
        a(ccVar, (Feedback) getItem(i));
        return view;
    }

    private void a(cc ccVar, Feedback feedback) {
        if (ccVar != null && feedback != null) {
            ccVar.f736a.setText(cv.c(Long.valueOf(feedback.d * 1000)));
            ccVar.b.setText(cv.c(Long.valueOf(feedback.e * 1000)));
            ccVar.c.setText(this.f673a.getResources().getText(R.string.feedback_list_question_author));
            ccVar.d.setText(feedback.b);
            ccVar.e.setText(String.format(this.f673a.getResources().getString(R.string.feedback_list_answer_author), feedback.g));
            ccVar.f.setText(feedback.c);
        }
    }
}
