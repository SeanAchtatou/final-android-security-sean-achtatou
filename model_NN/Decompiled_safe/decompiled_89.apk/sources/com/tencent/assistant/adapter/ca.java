package com.tencent.assistant.adapter;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.adapter.AppAdapter;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.e;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.adapter.smartlist.SmartItemType;
import com.tencent.assistantv2.adapter.smartlist.ab;
import com.tencent.assistantv2.adapter.smartlist.ac;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: ProGuard */
public class ca extends BaseExpandableListAdapter implements UIEventListener {
    private static int f = 0;
    private static int g = (f + 1);

    /* renamed from: a  reason: collision with root package name */
    public AstApp f734a = AstApp.i();
    private LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> b = new LinkedHashMap<>();
    private Context c;
    private LayoutInflater d;
    private LayoutInflater e;
    private View h;
    private AppAdapter.ListType i = AppAdapter.ListType.LISTTYPENORMAL;
    private HashMap<String, Integer> j = new HashMap<>();
    private ArrayList<AppGroupInfo> k;
    private Set<SimpleAppModel> l = new HashSet();
    private StatInfo m = new StatInfo();
    private b n = null;
    private String o = "03_";

    public ca() {
    }

    public ca(Context context, View view, int i2) {
        this.c = context;
        this.h = view;
        this.k = new ArrayList<>();
        this.d = LayoutInflater.from(this.c);
        this.e = LayoutInflater.from(this.c);
        this.m.sourceScene = i2;
        this.h = view;
    }

    public void a(boolean z, Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, long j2, boolean z2) {
        this.m.f3356a = j2;
        if (map != null && map.size() > 0) {
            Iterator<Map.Entry<AppGroupInfo, ArrayList<SimpleAppModel>>> it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry next = it.next();
                Pair<ArrayList<SimpleAppModel>, ArrayList<SimpleAppModel>> a2 = a((ArrayList) next.getValue());
                if (((ArrayList) a2.first).size() > 0) {
                    next.setValue(a2.first);
                } else {
                    it.remove();
                }
                if (((ArrayList) a2.second).size() > 0) {
                    this.l.addAll((Collection) a2.second);
                }
            }
            if (!z2 && this.l.size() > 0) {
                AppGroupInfo appGroupInfo = new AppGroupInfo();
                appGroupInfo.a(this.c.getResources().getString(R.string.bibei_column_hasinstall_name));
                map.put(appGroupInfo, new ArrayList(this.l));
            }
            if (z) {
                this.b.clear();
                this.k.clear();
            }
            this.b.putAll(map);
            this.k.addAll(map.keySet());
            notifyDataSetChanged();
        }
    }

    public Object getChild(int i2, int i3) {
        if (this.b == null || this.b.size() <= 0) {
            return null;
        }
        return this.b.get(this.k.get(i2)).get(i3);
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        cb cbVar;
        if (view == null) {
            view = this.d.inflate((int) R.layout.group_item, (ViewGroup) null);
            cb cbVar2 = new cb(this);
            cbVar2.f735a = (TextView) view.findViewById(R.id.group_title);
            view.setTag(cbVar2);
            cbVar = cbVar2;
        } else {
            cbVar = (cb) view.getTag();
        }
        cbVar.f735a.setText(this.k.get(i2).a());
        return view;
    }

    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel;
        ArrayList arrayList = this.b.get(this.k.get(i2));
        if (arrayList == null || i3 > arrayList.size() - 1) {
            simpleAppModel = null;
        } else {
            simpleAppModel = (SimpleAppModel) arrayList.get(i3);
        }
        XLog.d("icerao", "group position:" + i2 + ",childP:" + i3 + ",model:" + simpleAppModel.d);
        ab abVar = new ab();
        SmartItemType smartItemType = SmartItemType.NORMAL;
        if (f == a(i2, i3)) {
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                smartItemType = SmartItemType.NORMAL_NO_REASON;
            } else {
                smartItemType = SmartItemType.NORMAL;
            }
        } else if (g == a(i2, i3)) {
            smartItemType = SmartItemType.COMPETITIVE;
        }
        e eVar = new e();
        eVar.b = 1;
        eVar.c = simpleAppModel;
        abVar.a(a(simpleAppModel, i2, i3));
        return ac.a(this.c, abVar, view, smartItemType, i3, eVar, null);
    }

    public int getChildrenCount(int i2) {
        if (this.b == null || this.k == null || i2 >= this.k.size()) {
            return 0;
        }
        ArrayList arrayList = this.b.get(this.k.get(i2));
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    public Object getGroup(int i2) {
        if (this.k == null || this.k.size() <= 0 || i2 >= this.k.size() || i2 < 0) {
            return null;
        }
        return this.k.get(i2);
    }

    public int getGroupCount() {
        if (this.b == null || this.b.size() <= 0) {
            return 0;
        }
        return this.b.size();
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return false;
    }

    public void handleUIEvent(Message message) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.k.size()) {
                ArrayList arrayList = this.b.get(this.k.get(i3));
                switch (message.what) {
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                        DownloadInfo downloadInfo = null;
                        if (!(message.obj instanceof DownloadInfo) || ((downloadInfo = (DownloadInfo) message.obj) != null && !TextUtils.isEmpty(downloadInfo.downloadTicket))) {
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                if (((SimpleAppModel) it.next()).q().equals(downloadInfo.downloadTicket)) {
                                    notifyDataSetChanged();
                                }
                            }
                            notifyDataSetChanged();
                            break;
                        } else {
                            return;
                        }
                        break;
                    case 1016:
                        u.g(arrayList);
                        notifyDataSetChanged();
                        break;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public int a(int i2, int i3) {
        SimpleAppModel.CARD_TYPE card_type;
        ArrayList arrayList;
        SimpleAppModel simpleAppModel;
        SimpleAppModel.CARD_TYPE card_type2 = SimpleAppModel.CARD_TYPE.NORMAL;
        if (this.b == null || this.k == null || i2 >= this.k.size() || (arrayList = this.b.get(this.k.get(i2))) == null || i3 >= arrayList.size() || (simpleAppModel = (SimpleAppModel) arrayList.get(i3)) == null) {
            card_type = card_type2;
        } else {
            card_type = simpleAppModel.U;
        }
        if (SimpleAppModel.CARD_TYPE.NORMAL == card_type) {
            return f;
        }
        if (SimpleAppModel.CARD_TYPE.QUALITY == card_type) {
            return g;
        }
        return f;
    }

    public void a() {
        this.f734a.k().addUIEventListener(1002, this);
        this.f734a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.f734a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.f734a.k().addUIEventListener(1007, this);
        this.f734a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.f734a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.f734a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.f734a.k().addUIEventListener(1010, this);
        notifyDataSetChanged();
    }

    public void b() {
        this.j.clear();
        this.f734a.k().removeUIEventListener(1002, this);
        this.f734a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.f734a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.f734a.k().removeUIEventListener(1007, this);
        this.f734a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.f734a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.f734a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.f734a.k().removeUIEventListener(1010, this);
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel, int i2, int i3) {
        if (simpleAppModel == null) {
            return null;
        }
        if (this.n == null) {
            this.n = new b();
        }
        AppConst.AppState d2 = u.d(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, simpleAppModel, b(i2, i3), 100, a.a(d2, simpleAppModel));
        this.n.exposure(buildSTInfo);
        return buildSTInfo;
    }

    private String b(int i2, int i3) {
        return this.o + String.format("%02d", Integer.valueOf(i2 + 1)) + "_" + ct.a(i3 + 1);
    }

    private Pair<ArrayList<SimpleAppModel>, ArrayList<SimpleAppModel>> a(List<SimpleAppModel> list) {
        ArrayList arrayList = new ArrayList(list.size());
        ArrayList arrayList2 = new ArrayList(4);
        for (SimpleAppModel next : list) {
            if (!com.tencent.assistant.utils.e.a(next.c, next.g)) {
                arrayList.add(next);
            } else {
                arrayList2.add(next);
            }
        }
        return Pair.create(arrayList, arrayList2);
    }
}
