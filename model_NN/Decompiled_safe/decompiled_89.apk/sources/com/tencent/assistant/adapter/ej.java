package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ej extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ el f793a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ UserInstalledAppListAdapter d;

    ej(UserInstalledAppListAdapter userInstalledAppListAdapter, el elVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        this.d = userInstalledAppListAdapter;
        this.f793a = elVar;
        this.b = localApkInfo;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.f793a.e.setSelected(!this.f793a.e.isSelected());
        this.b.mIsSelect = this.f793a.e.isSelected();
        if (this.d.g != null) {
            this.d.g.sendMessage(this.d.g.obtainMessage(10705, this.b));
        }
    }

    public STInfoV2 getStInfo() {
        this.c.actionId = 200;
        this.c.status = "02";
        this.c.extraData = this.b.mAppName;
        return this.c;
    }
}
