package com.tencent.assistant.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.ApkMetaInfoLoader;
import com.tencent.assistant.component.DownloadListFooterView;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.component.invalidater.CommonViewInvalidater;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.DownloadInfoWrapper;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.callback.z;
import com.tencent.assistant.module.ex;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.InstalledAppItem;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.component.BookReadButton;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.FileDownloadButton;
import com.tencent.assistantv2.component.MovingProgressBar;
import com.tencent.assistantv2.component.VideoDownloadButton;
import com.tencent.assistantv2.mediadownload.FileOpenSelector;
import com.tencent.assistantv2.mediadownload.b;
import com.tencent.assistantv2.mediadownload.c;
import com.tencent.assistantv2.mediadownload.m;
import com.tencent.assistantv2.mediadownload.o;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.d;
import com.tencent.assistantv2.model.h;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@SuppressLint({"NewApi"})
/* compiled from: ProGuard */
public class DownloadInfoMultiAdapter extends BaseExpandableListAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    public boolean f671a = false;
    public View.OnClickListener b = new ba(this);
    private LayoutInflater c;
    private final List<DownloadInfoWrapper> d = new ArrayList();
    private final List<DownloadInfoWrapper> e = new ArrayList();
    /* access modifiers changed from: private */
    public final List<DownloadInfoWrapper> f = new ArrayList();
    /* access modifiers changed from: private */
    public final List<DownloadInfoWrapper> g = new ArrayList();
    private Comparator<DownloadInfoWrapper> h = new bt();
    private Comparator<DownloadInfoWrapper> i = new bv();
    private Map<String, View> j = new HashMap();
    private CreatingTaskStatusEnum k = CreatingTaskStatusEnum.NONE;
    private AstApp l;
    /* access modifiers changed from: private */
    public Context m;
    private bs n = null;
    private View.OnClickListener o = null;
    private CommonViewInvalidater p;
    private ex q = new ex();
    private boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = false;
    private TXExpandableListView t;
    /* access modifiers changed from: private */
    public DownloadListFooterView u;
    /* access modifiers changed from: private */
    public ApkMetaInfoLoader v = new ApkMetaInfoLoader();
    private z w = new az(this);
    private ViewInvalidateMessageHandler x = new aw(this);

    /* compiled from: ProGuard */
    public enum CreatingTaskStatusEnum {
        NONE,
        CREATING,
        FAIL
    }

    public void a(boolean z) {
        this.r = z;
    }

    public void a(CommonViewInvalidater commonViewInvalidater) {
        this.p = commonViewInvalidater;
    }

    public void a(DownloadListFooterView downloadListFooterView) {
        this.u = downloadListFooterView;
    }

    public DownloadInfoMultiAdapter(Context context, TXExpandableListView tXExpandableListView, View.OnClickListener onClickListener) {
        this.m = context;
        this.t = tXExpandableListView;
        this.c = LayoutInflater.from(context);
        this.l = AstApp.i();
        this.o = onClickListener;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public void a() {
        k();
        if (this.f.size() > 50) {
            synchronized (this.f) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < this.f.size(); i2++) {
                    DownloadInfoWrapper downloadInfoWrapper = this.f.get(i2);
                    if (downloadInfoWrapper.f1246a != DownloadInfoWrapper.InfoType.App) {
                        arrayList.add(Integer.valueOf(i2));
                    } else if (u.a(downloadInfoWrapper.b, true, false) == AppConst.AppState.INSTALLED) {
                        arrayList.add(Integer.valueOf(i2));
                    }
                }
                if (arrayList.size() > 50) {
                    for (int size = arrayList.size() - 1; size >= 50; size--) {
                        int intValue = ((Integer) arrayList.get(size)).intValue();
                        this.f.remove(intValue);
                        a(this.f.get(intValue));
                    }
                }
            }
        }
        l();
        notifyDataSetChanged();
        if (this.f671a) {
            this.t.postDelayed(new as(this), 500);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* access modifiers changed from: package-private */
    public void b() {
        ArrayList arrayList = new ArrayList();
        for (DownloadInfoWrapper next : this.f) {
            AppConst.AppState a2 = u.a(next.b, true, false);
            if (next.f1246a == DownloadInfoWrapper.InfoType.App && !be.a().b(next.b) && a2 == AppConst.AppState.DOWNLOADED && next.b.isDownloadFileExist()) {
                arrayList.add(next.b);
            }
        }
        if (arrayList.size() >= 5) {
            a((ArrayList<DownloadInfo>) arrayList);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.mediadownload.c.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistantv2.mediadownload.c.a(com.tencent.assistantv2.mediadownload.c, java.lang.String):void
      com.tencent.assistantv2.mediadownload.c.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public void a(DownloadInfoWrapper downloadInfoWrapper) {
        if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.App) {
            DownloadProxy.a().b(downloadInfoWrapper.b.downloadTicket, false);
        } else if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.Video) {
            o.c().b(downloadInfoWrapper.c.c);
        } else if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.Book) {
            b.a().a(downloadInfoWrapper.d.c);
        } else if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.CommonFile) {
            c.c().a(downloadInfoWrapper.e.c, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>
     arg types: [com.tencent.assistant.download.SimpleDownloadInfo$DownloadType, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):java.lang.String
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, int):com.tencent.assistant.download.DownloadInfo
      com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, boolean):void
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.assistant.download.DownloadInfo> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    private void k() {
        AppUpdateInfo a2;
        this.d.clear();
        this.f.clear();
        ArrayList<DownloadInfo> a3 = DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK, true);
        List<h> a4 = o.c().a();
        List<d> a5 = c.c().a();
        List<com.tencent.assistantv2.model.b> b2 = b.a().b();
        if (a3 != null && a3.size() > 0) {
            try {
                Collections.sort(a3);
            } catch (Exception e2) {
            }
            HashSet hashSet = new HashSet(a3.size());
            ArrayList<DownloadInfo> arrayList = new ArrayList<>(a3.size());
            for (int i2 = 0; i2 < a3.size(); i2++) {
                DownloadInfo downloadInfo = a3.get(i2);
                if (hashSet.contains(Long.valueOf(downloadInfo.appId))) {
                    DownloadProxy.a().b(downloadInfo.downloadTicket, false);
                } else {
                    hashSet.add(Long.valueOf(downloadInfo.appId));
                    arrayList.add(downloadInfo);
                }
            }
            for (DownloadInfo downloadInfo2 : arrayList) {
                if (downloadInfo2 != null && downloadInfo2.needReCreateInfo() && (a2 = k.b().a(downloadInfo2.packageName)) != null && a2.d == downloadInfo2.versionCode && a2.w == downloadInfo2.grayVersionCode) {
                    u.a(a2, downloadInfo2);
                }
                switch (ay.f711a[u.a(downloadInfo2, true, true).ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        this.d.add(new DownloadInfoWrapper(downloadInfo2));
                        break;
                    default:
                        if (be.a().b(downloadInfo2)) {
                            downloadInfo2.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                        }
                        this.f.add(new DownloadInfoWrapper(downloadInfo2));
                        break;
                }
            }
            if (this.f.size() > 0) {
                synchronized (this.f) {
                    ArrayList arrayList2 = new ArrayList();
                    for (int i3 = 0; i3 < this.f.size(); i3++) {
                        DownloadInfoWrapper downloadInfoWrapper = this.f.get(i3);
                        if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.App) {
                            AppConst.AppState a6 = u.a(downloadInfoWrapper.b, true, true);
                            PackageInfo d2 = e.d(downloadInfoWrapper.b.packageName, 0);
                            if (a6 == AppConst.AppState.INSTALLED && !be.a().b(downloadInfoWrapper.b) && d2 == null) {
                                arrayList2.add(Integer.valueOf(i3));
                            }
                        }
                    }
                    if (arrayList2.size() > 0) {
                        for (int size = arrayList2.size() - 1; size >= 0; size--) {
                            int intValue = ((Integer) arrayList2.get(size)).intValue();
                            this.f.remove(intValue);
                            DownloadProxy.a().b(this.f.get(intValue).b.downloadTicket, false);
                        }
                    }
                }
            }
        }
        if (a4 != null && a4.size() > 0) {
            for (h next : a4) {
                if (next.i == AbstractDownloadInfo.DownState.SUCC) {
                    this.f.add(new DownloadInfoWrapper(next));
                } else {
                    this.d.add(new DownloadInfoWrapper(next));
                }
            }
        }
        if (b2 != null && b2.size() > 0) {
            for (com.tencent.assistantv2.model.b downloadInfoWrapper2 : b2) {
                this.f.add(new DownloadInfoWrapper(downloadInfoWrapper2));
            }
        }
        if (a5 != null && a5.size() > 0) {
            for (d next2 : a5) {
                if (next2.i == AbstractDownloadInfo.DownState.SUCC) {
                    this.f.add(new DownloadInfoWrapper(next2));
                } else {
                    this.d.add(new DownloadInfoWrapper(next2));
                }
            }
        }
        try {
            Collections.sort(this.d, this.h);
            Collections.sort(this.f, this.i);
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        DownloadInfoWrapper downloadInfoWrapper;
        this.e.clear();
        this.e.addAll(this.d);
        if (!this.r) {
            if (this.f != null) {
                this.g.clear();
                this.g.addAll(this.f);
                if (!this.s && !this.g.isEmpty()) {
                    int i2 = 0;
                    Iterator<DownloadInfoWrapper> it = this.g.iterator();
                    while (true) {
                        int i3 = i2;
                        if (it.hasNext()) {
                            DownloadInfoWrapper next = it.next();
                            if (next.f1246a != DownloadInfoWrapper.InfoType.App || next.d()) {
                                i2 = i3 + 1;
                            } else {
                                i2 = i3;
                            }
                            if (i2 > 2) {
                                it.remove();
                            }
                        } else {
                            return;
                        }
                    }
                }
            }
        } else if (this.s) {
            if (this.f != null && !this.f.isEmpty()) {
                this.e.addAll(this.f);
            }
        } else if (this.f != null && !this.f.isEmpty()) {
            Iterator<DownloadInfoWrapper> it2 = this.f.iterator();
            DownloadInfoWrapper downloadInfoWrapper2 = null;
            DownloadInfoWrapper downloadInfoWrapper3 = null;
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                DownloadInfoWrapper next2 = it2.next();
                if (downloadInfoWrapper3 != null) {
                    break;
                }
                if (next2.c()) {
                    DownloadInfoWrapper downloadInfoWrapper4 = downloadInfoWrapper2;
                    downloadInfoWrapper = next2;
                    next2 = downloadInfoWrapper4;
                } else if (downloadInfoWrapper2 == null) {
                    downloadInfoWrapper = downloadInfoWrapper3;
                } else {
                    next2 = downloadInfoWrapper2;
                    downloadInfoWrapper = downloadInfoWrapper3;
                }
                if (downloadInfoWrapper != null && next2 != null) {
                    downloadInfoWrapper3 = downloadInfoWrapper;
                    downloadInfoWrapper2 = next2;
                    break;
                }
                downloadInfoWrapper3 = downloadInfoWrapper;
                downloadInfoWrapper2 = next2;
            }
            if (downloadInfoWrapper3 != null) {
                this.e.add(downloadInfoWrapper3);
            } else if (downloadInfoWrapper2 != null) {
                this.e.add(downloadInfoWrapper2);
            }
        }
    }

    public void a(String str, InstalledAppItem installedAppItem) {
        DownloadInfo downloadInfo;
        InstalledAppItem installedAppItem2;
        int i2;
        DownloadInfo downloadInfo2;
        if (this.u != null) {
            if (this.e.size() > 0) {
                Iterator<DownloadInfoWrapper> it = this.e.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        downloadInfo2 = null;
                        break;
                    }
                    DownloadInfoWrapper next = it.next();
                    if (next.f1246a == DownloadInfoWrapper.InfoType.App) {
                        downloadInfo2 = next.b;
                        break;
                    }
                }
                downloadInfo = downloadInfo2;
            } else {
                if (this.g.size() > 0) {
                    Iterator<DownloadInfoWrapper> it2 = this.g.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        DownloadInfoWrapper next2 = it2.next();
                        if (next2.f1246a == DownloadInfoWrapper.InfoType.App) {
                            downloadInfo = next2.b;
                            break;
                        }
                    }
                }
                downloadInfo = null;
            }
            if (installedAppItem == null) {
                InstalledAppItem installedAppItem3 = new InstalledAppItem();
                installedAppItem3.b = downloadInfo != null ? downloadInfo.appId : 0;
                installedAppItem3.f2221a = downloadInfo != null ? downloadInfo.packageName : Constants.STR_EMPTY;
                if (downloadInfo != null) {
                    i2 = downloadInfo.versionCode;
                } else {
                    i2 = 0;
                }
                installedAppItem3.c = i2;
                installedAppItem2 = installedAppItem3;
            } else {
                installedAppItem2 = installedAppItem;
            }
            ArrayList<InstalledAppItem> a2 = a(this.e, 5);
            ArrayList<InstalledAppItem> a3 = a(this.g, 2);
            this.q.register(this.w);
            this.q.a(2, installedAppItem2, a3, a2, (byte) 0, str);
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<RecommendAppInfo> a(List<RecommendAppInfo> list) {
        ArrayList<RecommendAppInfo> arrayList = new ArrayList<>();
        for (RecommendAppInfo next : list) {
            List<DownloadInfo> e2 = DownloadProxy.a().e(next.a());
            if (e2 == null || e2.isEmpty()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public ArrayList<RecommendAppInfo> b(List<RecommendAppInfo> list) {
        ArrayList<RecommendAppInfo> arrayList = new ArrayList<>();
        for (RecommendAppInfo next : list) {
            if (ApkResourceManager.getInstance().getLocalApkInfo(next.a()) == null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private ArrayList<InstalledAppItem> a(List<DownloadInfoWrapper> list, int i2) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayList<InstalledAppItem> arrayList = new ArrayList<>();
        int i3 = 0;
        for (DownloadInfoWrapper next : list) {
            if (next != null && next.f1246a == DownloadInfoWrapper.InfoType.App) {
                if (i3 >= i2) {
                    break;
                }
                InstalledAppItem installedAppItem = new InstalledAppItem();
                installedAppItem.a(next.b.appId);
                installedAppItem.a(next.b.packageName);
                installedAppItem.a(next.b.versionCode);
                arrayList.add(installedAppItem);
                i3++;
            }
        }
        return arrayList;
    }

    private int m() {
        return this.d.size() + this.f.size();
    }

    public int c() {
        return this.e.size() + this.g.size();
    }

    public void d() {
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.l.k().addUIEventListener(1007, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        this.l.k().addUIEventListener(1027, this);
    }

    public void e() {
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.l.k().removeUIEventListener(1007, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        this.l.k().removeUIEventListener(1027, this);
    }

    public int getGroupCount() {
        int i2;
        int i3 = 1;
        if (this.e.size() > 0 || this.k != CreatingTaskStatusEnum.NONE) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        if (this.g.size() <= 0) {
            i3 = 0;
        }
        return i2 + i3;
    }

    public int a(int i2) {
        if (i2 == 0 && (this.e.size() > 0 || this.k != CreatingTaskStatusEnum.NONE)) {
            return 0;
        }
        if (i2 != -1) {
            return 1;
        }
        return -1;
    }

    public int getChildrenCount(int i2) {
        if (a(i2) == 0) {
            return this.e.size() + 1;
        }
        return this.g.size();
    }

    public Object getGroup(int i2) {
        return Integer.valueOf(i2);
    }

    @SuppressLint({"Override"})
    public int getChildType(int i2, int i3) {
        if (a(i2) == 0 && i3 == 0) {
            return 1;
        }
        DownloadInfoWrapper downloadInfoWrapper = (DownloadInfoWrapper) getChild(i2, i3);
        if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.Video) {
            return 2;
        }
        if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.Book) {
            return 3;
        }
        if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.CommonFile) {
            return 4;
        }
        return 0;
    }

    @SuppressLint({"Override"})
    public int getChildTypeCount() {
        return 5;
    }

    private int c(int i2) {
        return i2 - 1;
    }

    public Object getChild(int i2, int i3) {
        if (a(i2) == 0) {
            int c2 = c(i3);
            if (c2 < 0 || c2 >= this.e.size()) {
                return null;
            }
            return this.e.get(c2);
        } else if (this.g.size() > i3) {
            return this.g.get(i3);
        } else {
            return null;
        }
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public boolean hasStableIds() {
        return false;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        bp bpVar;
        if (i2 < 0) {
            return null;
        }
        if (view == null || view.getTag() == null || !(view.getTag() instanceof bp) || ((bp) view.getTag()).f729a == null) {
            view = this.c.inflate((int) R.layout.downloadapp_group_item, (ViewGroup) null);
            bw bwVar = new bw(this, null);
            bwVar.f732a = view.findViewById(R.id.group_layout);
            bwVar.b = (TextView) view.findViewById(R.id.group_title);
            bwVar.c = (TextView) view.findViewById(R.id.group_title_num);
            bwVar.d = (Button) view.findViewById(R.id.group_action);
            bpVar = new bp();
            bpVar.f729a = bwVar;
            view.setTag(bpVar);
        } else {
            bpVar = (bp) view.getTag();
        }
        bw bwVar2 = bpVar.f729a;
        String[] b2 = b(i2);
        if (b2 != null) {
            bwVar2.b.setText(b2[0]);
            bwVar2.c.setText(" " + b2[1]);
        }
        if (a(i2) == 1) {
            bwVar2.d.setText(this.m.getResources().getString(R.string.down_page_group_clear_text));
            bwVar2.d.setVisibility(0);
            bwVar2.d.setOnClickListener(this.b);
        } else {
            bwVar2.d.setVisibility(8);
        }
        if (!this.r) {
            return view;
        }
        bwVar2.f732a.setVisibility(8);
        return view;
    }

    public String[] b(int i2) {
        if (a(i2) == 0) {
            int size = this.e.size();
            String[] strArr = new String[2];
            strArr[0] = this.m.getResources().getString(R.string.down_page_group_downloading_title);
            StringBuilder append = new StringBuilder().append("(");
            if (this.k != CreatingTaskStatusEnum.NONE) {
                size++;
            }
            strArr[1] = append.append(size).append(")").toString();
            return strArr;
        }
        return new String[]{this.m.getResources().getString(R.string.down_page_group_finish_title), "(" + this.f.size() + ")"};
    }

    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        bz bzVar;
        View view2;
        XLog.d("DownloadActivity", "getchildView:childposition:" + i3 + ",group:" + i2);
        boolean z2 = i2 == getGroupCount() + -1;
        int childType = getChildType(i2, i3);
        DownloadInfoWrapper downloadInfoWrapper = (DownloadInfoWrapper) getChild(i2, i3);
        if (downloadInfoWrapper == null && childType != 1) {
            return null;
        }
        String a2 = a(i2, i3);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.m, 200);
        buildSTInfo.slotId = a2;
        if (view == null || view.getTag() == null || !(view.getTag() instanceof bp)) {
            bp bpVar = new bp();
            bzVar = new bz();
            if (childType == 3) {
                br r2 = r();
                bzVar.d = r2;
                view2 = r2.f;
            } else if (childType == 2) {
                bx p2 = p();
                bzVar.c = p2;
                view2 = p2.f;
            } else if (childType == 1) {
                bs n2 = n();
                if (n2 == null) {
                    return new View(this.m);
                }
                bzVar.b = n2;
                view2 = n2.f731a;
            } else if (childType == 4) {
                bu q2 = q();
                bzVar.e = q2;
                view2 = q2.f;
            } else {
                by o2 = o();
                bzVar.f733a = o2;
                view2 = o2.f;
            }
            bpVar.b = bzVar;
            view2.setTag(bpVar);
        } else {
            bp bpVar2 = (bp) view.getTag();
            if (bpVar2.b == null) {
                bpVar2.b = new bz();
            }
            bz bzVar2 = bpVar2.b;
            if (childType == 0 && bzVar2.f733a == null) {
                bzVar2.f733a = o();
            } else if (childType == 1 && bzVar2.b == null) {
                bzVar2.b = n();
            } else if (childType == 2 && bzVar2.c == null) {
                bzVar2.c = p();
            } else if (childType == 3 && bzVar2.d == null) {
                bzVar2.d = r();
            } else if (childType == 4 && bzVar2.e == null) {
                bzVar2.e = q();
            }
            bz bzVar3 = bzVar2;
            view2 = bzVar2.a(childType);
            bzVar = bzVar3;
        }
        if (childType == 1) {
            bzVar.b.e.setBackgroundResource(R.drawable.common_download_item_selector);
            a(bzVar.b, this.k);
            if (z) {
                bzVar.b.f.setVisibility(8);
            } else {
                bzVar.b.f.setVisibility(0);
            }
        } else {
            a(downloadInfoWrapper, view2);
            if (childType == 0) {
                DownloadInfo downloadInfo = downloadInfoWrapper.b;
                by byVar = bzVar.f733a;
                byVar.f730a = i3;
                byVar.b = i2;
                byVar.o = downloadInfo.downloadTicket;
                a(byVar, downloadInfo, buildSTInfo);
                byVar.g.setBackgroundResource(R.drawable.common_download_item_selector);
                a(i3, z, z2, byVar);
            } else if (childType == 2) {
                h hVar = downloadInfoWrapper.c;
                bx bxVar = bzVar.c;
                bxVar.f730a = i3;
                bxVar.b = i2;
                bxVar.o = hVar.c;
                a(bxVar, hVar, buildSTInfo);
                bxVar.g.setBackgroundResource(R.drawable.common_download_item_selector);
                a(i3, z, z2, bxVar);
            } else if (childType == 4) {
                d dVar = downloadInfoWrapper.e;
                bu buVar = bzVar.e;
                buVar.f730a = i3;
                buVar.b = i2;
                buVar.o = dVar.c;
                a(buVar, dVar, buildSTInfo);
                buVar.g.setBackgroundResource(R.drawable.common_download_item_selector);
                a(i3, z, z2, buVar);
            } else if (childType == 3) {
                com.tencent.assistantv2.model.b bVar = downloadInfoWrapper.d;
                br brVar = bzVar.d;
                brVar.f730a = i3;
                brVar.b = i2;
                brVar.l = bVar.c;
                a(brVar, bVar, buildSTInfo);
                brVar.g.setBackgroundResource(R.drawable.common_download_item_selector);
                a(i3, z, z2, brVar);
            }
        }
        if (z) {
            view2.setPadding(0, 0, 0, df.a(this.m, 5.0f));
        } else {
            view2.setPadding(0, 0, 0, 0);
        }
        return view2;
    }

    private void a(int i2, boolean z, boolean z2, bq bqVar) {
        bqVar.c.setVisibility(8);
        if (z) {
            boolean z3 = c() >= m();
            if (!z2 || this.s || z3) {
                bqVar.e.setVisibility(8);
                return;
            }
            bqVar.e.setVisibility(0);
            bqVar.c.setVisibility(0);
            bqVar.c.setOnClickListener(new bc(this, bqVar));
        } else if (i2 == 0) {
            bqVar.e.setVisibility(0);
        } else {
            bqVar.e.setVisibility(0);
        }
    }

    private void a(DownloadInfoWrapper downloadInfoWrapper, View view) {
        if (downloadInfoWrapper != null && view != null) {
            if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.App) {
                this.j.put("app|" + downloadInfoWrapper.b.downloadTicket, view);
            } else if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.Video) {
                this.j.put("video|" + downloadInfoWrapper.c.c, view);
            } else if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.Book) {
                this.j.put("book|" + downloadInfoWrapper.d.c, view);
            } else if (downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.CommonFile) {
                this.j.put("file|" + downloadInfoWrapper.e.c, view);
            }
        }
    }

    private View a(String str) {
        if (this.j != null) {
            return this.j.get("app|" + str);
        }
        return null;
    }

    private bx b(String str) {
        View view;
        if (!(str == null || (view = this.j.get("video|" + str)) == null || !(view.getTag() instanceof bp))) {
            bp bpVar = (bp) view.getTag();
            if (bpVar == null || bpVar.b == null || bpVar.b.c == null) {
                return null;
            }
            bx bxVar = bpVar.b.c;
            if (bxVar.o != null && bxVar.o.equals(str)) {
                return bxVar;
            }
        }
        return null;
    }

    private bu c(String str) {
        View view;
        if (!(str == null || (view = this.j.get("file|" + str)) == null || !(view.getTag() instanceof bp))) {
            bp bpVar = (bp) view.getTag();
            if (bpVar == null || bpVar.b == null || bpVar.b.e == null) {
                return null;
            }
            bu buVar = bpVar.b.e;
            if (buVar.o != null && buVar.o.equals(str)) {
                return buVar;
            }
        }
        return null;
    }

    private void a(bs bsVar, CreatingTaskStatusEnum creatingTaskStatusEnum) {
        if (bsVar == null) {
            return;
        }
        if (creatingTaskStatusEnum == CreatingTaskStatusEnum.NONE) {
            bsVar.e.setVisibility(8);
            bsVar.f731a.setLayoutParams(new AbsListView.LayoutParams(0, 0));
            return;
        }
        switch (ay.b[creatingTaskStatusEnum.ordinal()]) {
            case 1:
                bsVar.b.setImageResource(R.drawable.small_morefunction_installion);
                bsVar.b.startAnimation(AnimationUtils.loadAnimation(this.m, R.anim.circle));
                bsVar.c.setText(this.m.getResources().getString(R.string.down_page_pop_loading));
                bsVar.d.setVisibility(4);
                break;
            case 2:
                bsVar.b.setAnimation(null);
                bsVar.d.setText(this.m.getResources().getString(R.string.down_page_pop_retry));
                bsVar.b.setImageResource(R.drawable.common_icon_appdetail_didnotpast);
                bsVar.c.setText(this.m.getResources().getString(R.string.down_page_pop_fail));
                bsVar.d.setVisibility(0);
                bsVar.d.setTextColor(this.m.getResources().getColor(R.color.state_update));
                break;
        }
        bsVar.f731a.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
        bsVar.e.setVisibility(0);
    }

    private String a(int i2, int i3) {
        if (a(i2) == 0) {
            return a.a("01", i3);
        }
        return a.a("02", i3);
    }

    private void a(bx bxVar, h hVar, STInfoV2 sTInfoV2) {
        if (bxVar != null && hVar != null) {
            bxVar.h.updateImageView(hVar.p, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            bxVar.j.setText(hVar.n);
            if (sTInfoV2 != null) {
                sTInfoV2.resourceType = com.tencent.assistantv2.st.page.d.d;
                sTInfoV2.packageName = hVar.c;
            }
            bxVar.i.a(hVar, sTInfoV2);
            a(bxVar, hVar);
            bxVar.p.setOnClickListener(new bd(this, hVar, sTInfoV2));
        }
    }

    private void a(bu buVar, d dVar, STInfoV2 sTInfoV2) {
        if (buVar != null && dVar != null) {
            if (sTInfoV2 != null) {
                sTInfoV2.resourceType = com.tencent.assistantv2.st.page.d.g;
                sTInfoV2.packageName = dVar.c;
            }
            buVar.i.a(dVar, sTInfoV2);
            a(buVar, dVar);
            buVar.p.setOnClickListener(new bg(this, dVar, sTInfoV2));
        }
    }

    private void a(TXImageView tXImageView, TextView textView, m mVar, d dVar) {
        tXImageView.setTag(dVar.c);
        textView.setText(dVar.g());
        switch (ay.c[mVar.d.ordinal()]) {
            case 1:
                tXImageView.setImageResource(R.drawable.icon_file_music);
                return;
            case 2:
                tXImageView.setImageResource(R.drawable.icon_file_video);
                return;
            case 3:
                tXImageView.setImageResource(R.drawable.icon_file_txt);
                return;
            case 4:
                tXImageView.setImageResource(R.drawable.icon_file_pic);
                return;
            case 5:
                tXImageView.setImageResource(R.drawable.icon_file_zip);
                return;
            case 6:
                if (dVar.i == AbstractDownloadInfo.DownState.SUCC) {
                    ApkMetaInfoLoader.MetaInfo metaInfoFromCache = this.v.getMetaInfoFromCache(dVar.h);
                    if (metaInfoFromCache == null || metaInfoFromCache.icon == null) {
                        TemporaryThreadManager.get().start(new bj(this, dVar, tXImageView, textView));
                        return;
                    }
                    if (metaInfoFromCache.icon != null) {
                        tXImageView.setImageDrawable(metaInfoFromCache.icon);
                    }
                    if (!TextUtils.isEmpty(metaInfoFromCache.name)) {
                        textView.setText(metaInfoFromCache.name);
                        return;
                    }
                    return;
                }
                tXImageView.setImageResource(R.drawable.icon_file_apk);
                return;
            default:
                tXImageView.setImageResource(R.drawable.icon_file_other);
                return;
        }
    }

    private void a(br brVar, com.tencent.assistantv2.model.b bVar, STInfoV2 sTInfoV2) {
        String str;
        if (brVar != null && bVar != null) {
            if (sTInfoV2 != null) {
                sTInfoV2.resourceType = com.tencent.assistantv2.st.page.d.b;
                sTInfoV2.packageName = bVar.c;
            }
            brVar.h.updateImageView(bVar.d, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            brVar.j.setText(bVar.f3315a);
            brVar.i.a(bVar, sTInfoV2);
            if (bVar.f <= -1) {
                str = "全本";
            } else {
                str = "共下载" + (bVar.f == 0 ? 1 : bVar.f) + "章";
            }
            brVar.k.setText(String.format(this.m.getResources().getString(R.string.down_page_book_desc), str, cv.g(bVar.j)));
        }
    }

    private void a(bx bxVar, h hVar) {
        if (bxVar != null && hVar != null) {
            if (hVar.i != AbstractDownloadInfo.DownState.SUCC) {
                bxVar.n.setText(bt.a(hVar.k.b) + "/" + bt.a(hVar.k.f3314a == 0 ? hVar.d : hVar.k.f3314a));
                bxVar.n.setVisibility(0);
                bxVar.k.setProgress(hVar.b());
                bxVar.k.setVisibility(0);
                if (hVar.i == AbstractDownloadInfo.DownState.DOWNLOADING) {
                    bxVar.m.setText(String.format(this.m.getResources().getString(R.string.down_page_downloading), hVar.k.c));
                    bxVar.m.setVisibility(0);
                } else if (hVar.i == AbstractDownloadInfo.DownState.QUEUING) {
                    bxVar.m.setVisibility(0);
                    bxVar.m.setText(this.m.getResources().getString(R.string.down_page_queuing));
                } else {
                    bxVar.m.setVisibility(0);
                    bxVar.m.setText(this.m.getResources().getString(R.string.down_page_pause));
                }
                bxVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                bxVar.p.setVisibility(0);
            } else {
                bxVar.p.setVisibility(8);
                bxVar.n.setVisibility(8);
                bxVar.k.setVisibility(8);
                bxVar.m.setVisibility(0);
                bxVar.m.setText(String.format(this.m.getResources().getString(R.string.down_page_video_name), cv.g(hVar.f)));
                bxVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_end_time_text_size));
            }
            bxVar.i.a();
        }
    }

    private void a(bu buVar, d dVar) {
        if (buVar != null && dVar != null) {
            m b2 = FileOpenSelector.b(dVar.h);
            a(buVar.h, buVar.j, b2, dVar);
            if (dVar.i != AbstractDownloadInfo.DownState.SUCC) {
                buVar.n.setText(bt.a(dVar.k.b) + "/" + bt.a(dVar.k.f3314a == 0 ? dVar.d : dVar.k.f3314a));
                buVar.n.setVisibility(0);
                buVar.k.setProgress(dVar.b());
                buVar.k.setVisibility(0);
                if (dVar.i == AbstractDownloadInfo.DownState.DOWNLOADING) {
                    buVar.m.setText(String.format(this.m.getResources().getString(R.string.down_page_downloading), dVar.k.c));
                    buVar.m.setVisibility(0);
                } else if (dVar.i == AbstractDownloadInfo.DownState.QUEUING) {
                    buVar.m.setVisibility(0);
                    buVar.m.setText(this.m.getResources().getString(R.string.down_page_queuing));
                } else {
                    buVar.m.setVisibility(0);
                    buVar.m.setText(this.m.getResources().getString(R.string.down_page_pause));
                }
                buVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                buVar.p.setVisibility(0);
            } else {
                buVar.p.setVisibility(8);
                buVar.n.setVisibility(8);
                buVar.k.setVisibility(8);
                buVar.m.setVisibility(0);
                buVar.m.setText(a(b2, dVar));
                buVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_end_time_text_size));
            }
            buVar.i.a();
        }
    }

    private String a(m mVar, d dVar) {
        String str;
        switch (ay.c[mVar.d.ordinal()]) {
            case 1:
                str = "音乐(来自外部)";
                break;
            case 2:
                str = "视频(来自外部)";
                break;
            case 3:
            default:
                str = "其他(来自外部)";
                break;
            case 4:
                str = "图片(来自外部)";
                break;
            case 5:
                str = "压缩包(来自外部)";
                break;
            case 6:
                str = "应用(来自外部)";
                break;
            case 7:
                str = "文档(来自外部)";
                break;
        }
        return str + "　" + cv.g(dVar.f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.by, boolean):void
     arg types: [com.tencent.assistant.adapter.by, int]
     candidates:
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(int, int):java.lang.String
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistantv2.mediadownload.m, com.tencent.assistantv2.model.d):java.lang.String
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, java.util.List):java.util.ArrayList
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.assistant.download.DownloadInfoWrapper>, int):java.util.ArrayList<com.tencent.assistant.protocol.jce.InstalledAppItem>
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.component.invalidater.ViewInvalidateMessage):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.download.DownloadInfo):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, com.tencent.assistant.download.DownloadInfoWrapper):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.bs, com.tencent.assistant.adapter.DownloadInfoMultiAdapter$CreatingTaskStatusEnum):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.bu, com.tencent.assistantv2.model.d):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.bx, com.tencent.assistantv2.model.h):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.download.DownloadInfoWrapper, android.view.View):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.DownloadInfoMultiAdapter, boolean):boolean
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.assistant.download.DownloadInfoWrapper>, long):boolean
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.util.List<com.tencent.assistant.download.DownloadInfoWrapper>, com.tencent.assistant.download.DownloadInfoWrapper):boolean
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(java.lang.String, com.tencent.assistant.protocol.jce.InstalledAppItem):void
      com.tencent.assistant.adapter.DownloadInfoMultiAdapter.a(com.tencent.assistant.adapter.by, boolean):void */
    private void a(by byVar, DownloadInfo downloadInfo, STInfoV2 sTInfoV2) {
        if (byVar == null) {
            return;
        }
        if (downloadInfo != null) {
            AppConst.AppState a2 = u.a(downloadInfo, true, true);
            if (sTInfoV2 != null) {
                if (downloadInfo.statInfo != null) {
                    String str = downloadInfo.statInfo.callerUin;
                    String str2 = downloadInfo.statInfo.callerVia;
                    if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                        sTInfoV2.updateWithExternalPara(str2, str, downloadInfo.hostPackageName, downloadInfo.hostVersionCode, downloadInfo.statInfo.traceId);
                    }
                }
                sTInfoV2.updateWithDownloadInfo(downloadInfo);
            }
            if (!TextUtils.isEmpty(downloadInfo.iconUrl)) {
                byVar.h.updateImageView(downloadInfo.iconUrl, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else if (downloadInfo.isUpdate == 1) {
                byVar.h.updateImageView(downloadInfo.packageName, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
            }
            byVar.i.a(downloadInfo);
            byVar.i.setOnClickListener(new bl(this, downloadInfo, sTInfoV2));
            byVar.p.setOnClickListener(new bm(this, downloadInfo, sTInfoV2));
            byVar.j.setText(downloadInfo.name);
            a(byVar, downloadInfo, a2);
            a(byVar, true);
            return;
        }
        a(byVar, false);
    }

    private void a(by byVar, boolean z) {
        if (z) {
            byVar.f.setVisibility(0);
        } else {
            byVar.f.setVisibility(8);
        }
    }

    private void a(by byVar, DownloadInfo downloadInfo, AppConst.AppState appState) {
        long j2 = 0;
        switch (ay.f711a[appState.ordinal()]) {
            case 2:
                int a2 = u.a(downloadInfo, appState);
                byVar.k.setVisibility(0);
                byVar.n.setVisibility(0);
                byVar.k.setProgress(a2);
                byVar.l.setText(String.format(this.m.getString(R.string.down_page_download_percent), Integer.valueOf(a2)));
                StringBuilder sb = new StringBuilder();
                if (!(downloadInfo == null || downloadInfo.response == null)) {
                    j2 = downloadInfo.response.f1263a;
                }
                byVar.n.setText(sb.append(bt.a(j2)).append("/").append(bt.a(SimpleDownloadInfo.getDownloadDisplayByteSize(downloadInfo))).toString());
                TextView textView = byVar.m;
                String string = this.m.getResources().getString(R.string.down_page_downloading);
                Object[] objArr = new Object[1];
                objArr[0] = (downloadInfo == null || downloadInfo.response == null) ? 0 : downloadInfo.response.c;
                textView.setText(String.format(string, objArr));
                byVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                byVar.p.setVisibility(0);
                b(byVar);
                return;
            case 3:
            case 6:
                int a3 = u.a(downloadInfo, appState);
                byVar.k.setVisibility(0);
                byVar.n.setVisibility(0);
                byVar.k.setProgress(a3);
                byVar.l.setText(String.format(this.m.getString(R.string.down_page_download_percent), Integer.valueOf(a3)));
                StringBuilder sb2 = new StringBuilder();
                if (!(downloadInfo == null || downloadInfo.response == null)) {
                    j2 = downloadInfo.response.f1263a;
                }
                byVar.n.setText(sb2.append(bt.a(j2)).append("/").append(bt.a(SimpleDownloadInfo.getDownloadDisplayByteSize(downloadInfo))).toString());
                byVar.m.setText(this.m.getResources().getString(R.string.down_page_pause));
                byVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                byVar.p.setVisibility(0);
                b(byVar);
                return;
            case 4:
                int a4 = u.a(downloadInfo, appState);
                byVar.k.setVisibility(0);
                byVar.n.setVisibility(0);
                byVar.k.setProgress(a4);
                byVar.l.setText(String.format(this.m.getString(R.string.down_page_download_percent), Integer.valueOf(a4)));
                StringBuilder sb3 = new StringBuilder();
                if (!(downloadInfo == null || downloadInfo.response == null)) {
                    j2 = downloadInfo.response.f1263a;
                }
                byVar.n.setText(sb3.append(bt.a(j2)).append("/").append(bt.a(SimpleDownloadInfo.getDownloadDisplayByteSize(downloadInfo))).toString());
                byVar.m.setText(this.m.getResources().getString(R.string.down_page_queuing));
                byVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_gray_text_size));
                byVar.p.setVisibility(0);
                b(byVar);
                return;
            case 5:
            default:
                b(byVar);
                byVar.k.setVisibility(4);
                byVar.n.setVisibility(4);
                if (be.a().c(downloadInfo.packageName)) {
                    byVar.m.setText(String.format(this.m.getString(R.string.down_page_download_qube_finish), cv.g(downloadInfo.downloadEndTime)));
                } else {
                    byVar.m.setText(String.format(this.m.getString(R.string.down_page_download_finish), cv.g(downloadInfo.downloadEndTime)));
                }
                byVar.m.setTextSize(0, (float) this.m.getResources().getDimensionPixelSize(R.dimen.app_down_item_end_time_text_size));
                byVar.p.setVisibility(8);
                return;
            case 7:
                a(byVar);
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    /* access modifiers changed from: private */
    public void a(DownloadInfo downloadInfo) {
        switch (ay.f711a[u.a(downloadInfo, true, true).ordinal()]) {
            case 1:
            case 5:
                com.tencent.assistant.download.a.a().a(downloadInfo);
                return;
            case 2:
            case 4:
                com.tencent.assistant.download.a.a().b(downloadInfo.downloadTicket);
                return;
            case 3:
                com.tencent.assistant.download.a.a().b(downloadInfo);
                return;
            case 6:
            case 10:
                com.tencent.assistant.download.a.a().a(downloadInfo);
                return;
            case 7:
                Toast.makeText(this.m, (int) R.string.tips_slicent_install, 0).show();
                return;
            case 8:
                if (!downloadInfo.isApkFileExist()) {
                    AppConst.TwoBtnDialogInfo b2 = b(downloadInfo);
                    if (b2 != null) {
                        v.a(b2);
                        return;
                    }
                    return;
                }
                com.tencent.assistant.download.a.a().d(downloadInfo);
                return;
            case 9:
                String str = downloadInfo.packageName;
                if (TextUtils.isEmpty(str)) {
                    return;
                }
                if (be.a().b(downloadInfo)) {
                    be.a().e(downloadInfo);
                    return;
                } else if (AstApp.i().getPackageManager().getLaunchIntentForPackage(str) != null) {
                    com.tencent.assistant.download.a.a().c(str);
                    return;
                } else if (e.d(str, 0) != null) {
                    at atVar = new at(this, downloadInfo);
                    atVar.titleRes = this.m.getResources().getString(R.string.down_uninstall_title);
                    atVar.contentRes = this.m.getResources().getString(R.string.down_cannot_open_tips);
                    atVar.btnTxtRes = this.m.getResources().getString(R.string.down_uninstall_tips_close);
                    v.a(atVar);
                    return;
                } else {
                    boolean isSuccApkFileExist = downloadInfo.isSuccApkFileExist();
                    au auVar = new au(this, isSuccApkFileExist, downloadInfo);
                    auVar.hasTitle = true;
                    auVar.titleRes = this.m.getResources().getString(R.string.down_open_title);
                    if (isSuccApkFileExist) {
                        auVar.contentRes = this.m.getResources().getString(R.string.down_open_install_content);
                        auVar.rBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_right_install);
                    } else {
                        auVar.contentRes = this.m.getResources().getString(R.string.down_open_download_content);
                        auVar.rBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_right_down);
                    }
                    auVar.lBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_left_del);
                    v.a(auVar);
                    return;
                }
            case 11:
                Toast.makeText(this.m, (int) R.string.unsupported, 0).show();
                return;
            case 12:
                Toast.makeText(this.m, (int) R.string.tips_slicent_uninstall, 0).show();
                return;
            default:
                return;
        }
    }

    private AppConst.TwoBtnDialogInfo b(DownloadInfo downloadInfo) {
        av avVar = new av(this, downloadInfo);
        avVar.hasTitle = true;
        avVar.titleRes = this.m.getResources().getString(R.string.down_page_dialog_title);
        avVar.contentRes = this.m.getResources().getString(R.string.down_page_dialog_content);
        avVar.lBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_left_del);
        avVar.rBtnTxtRes = this.m.getResources().getString(R.string.down_page_dialog_right_down);
        return avVar;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    public void f() {
        this.l.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
    }

    public void g() {
        this.l.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
    }

    public void h() {
        d();
    }

    public void i() {
        e();
        this.q.unregister(this.w);
    }

    public void handleUIEvent(Message message) {
        this.p.sendMessage(new ViewInvalidateMessage(message.what, message.obj, this.x));
    }

    /* access modifiers changed from: private */
    public void a(ViewInvalidateMessage viewInvalidateMessage) {
        DownloadInfo c2;
        DownloadInfoWrapper downloadInfoWrapper;
        boolean z;
        DownloadInfo downloadInfo;
        String str = Constants.STR_EMPTY;
        if (viewInvalidateMessage.params instanceof String) {
            str = (String) viewInvalidateMessage.params;
            if (!TextUtils.isEmpty(str)) {
                DownloadInfo d2 = DownloadProxy.a().d(str);
                if (d2 != null && d2.isUiTypeWiseDownload()) {
                    return;
                }
            } else {
                return;
            }
        }
        String str2 = str;
        if (!(viewInvalidateMessage.params instanceof DownloadInfo) || (downloadInfo = (DownloadInfo) viewInvalidateMessage.params) == null || !downloadInfo.isUiTypeWiseDownload()) {
            try {
                switch (viewInvalidateMessage.what) {
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING /*1003*/:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START /*1004*/:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC /*1006*/:
                    case 1007:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING /*1008*/:
                        DownloadInfo c3 = DownloadProxy.a().c(str2);
                        if (c3 != null) {
                            if (!b(new DownloadInfoWrapper(c3))) {
                                c(c3);
                                break;
                            } else {
                                notifyDataSetChanged();
                                break;
                            }
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE /*1005*/:
                        DownloadInfo c4 = DownloadProxy.a().c(str2);
                        if (c4 != null) {
                            c(c4);
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE /*1153*/:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE /*1162*/:
                        if (viewInvalidateMessage.params instanceof DownloadInfo) {
                            downloadInfoWrapper = new DownloadInfoWrapper((DownloadInfo) viewInvalidateMessage.params);
                        } else if (viewInvalidateMessage.params instanceof h) {
                            downloadInfoWrapper = new DownloadInfoWrapper((h) viewInvalidateMessage.params);
                        } else if (viewInvalidateMessage.params instanceof d) {
                            downloadInfoWrapper = new DownloadInfoWrapper((d) viewInvalidateMessage.params);
                        } else {
                            downloadInfoWrapper = null;
                        }
                        if (downloadInfoWrapper != null) {
                            synchronized (this.e) {
                                boolean a2 = a(this.e, downloadInfoWrapper);
                                boolean a3 = a(this.d, downloadInfoWrapper);
                                if (a2 || a3) {
                                    z = true;
                                } else {
                                    z = false;
                                }
                            }
                            synchronized (this.g) {
                                boolean a4 = a(this.g, downloadInfoWrapper);
                                boolean a5 = a(this.f, downloadInfoWrapper);
                                if (a4 || a5) {
                                    z = true;
                                }
                            }
                            if (z) {
                                l();
                                notifyDataSetChanged();
                                break;
                            }
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_APP_INSTALL /*1011*/:
                        notifyDataSetChanged();
                        break;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD /*1015*/:
                        DownloadInfo downloadInfo2 = (DownloadInfo) viewInvalidateMessage.params;
                        if (downloadInfo2 != null) {
                            a(this.e, downloadInfo2.appId);
                            a(this.d, downloadInfo2.appId);
                            a(this.g, downloadInfo2.appId);
                            a(this.f, downloadInfo2.appId);
                            a(this.e, downloadInfo2, 0);
                            a(this.d, downloadInfo2, 0);
                            notifyDataSetChanged();
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START /*1025*/:
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC /*1026*/:
                    case 1027:
                        String str3 = ((InstallUninstallTaskBean) viewInvalidateMessage.params).downloadTicket;
                        if (!TextUtils.isEmpty(str3) && (c2 = DownloadProxy.a().c(str3)) != null) {
                            c(c2);
                            break;
                        }
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START /*1147*/:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING /*1148*/:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE /*1149*/:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING /*1150*/:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL /*1151*/:
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC /*1154*/:
                        h hVar = (h) viewInvalidateMessage.params;
                        if (hVar != null) {
                            a(b(hVar.c), hVar);
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD /*1152*/:
                        h hVar2 = (h) viewInvalidateMessage.params;
                        if (hVar2 != null) {
                            DownloadInfoWrapper downloadInfoWrapper2 = new DownloadInfoWrapper(hVar2);
                            a(this.e, downloadInfoWrapper2);
                            a(this.d, downloadInfoWrapper2);
                            a(this.g, downloadInfoWrapper2);
                            a(this.f, downloadInfoWrapper2);
                            a(this.e, downloadInfoWrapper2, 0);
                            a(this.d, downloadInfoWrapper2, 0);
                            notifyDataSetChanged();
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START /*1155*/:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME /*1156*/:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING /*1157*/:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE /*1158*/:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING /*1159*/:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL /*1160*/:
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC /*1163*/:
                        d dVar = (d) viewInvalidateMessage.params;
                        if (dVar != null) {
                            a(c(dVar.c), dVar);
                            break;
                        }
                        break;
                    case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD /*1161*/:
                        d dVar2 = (d) viewInvalidateMessage.params;
                        if (dVar2 != null) {
                            DownloadInfoWrapper downloadInfoWrapper3 = new DownloadInfoWrapper(dVar2);
                            a(this.e, downloadInfoWrapper3);
                            a(this.d, downloadInfoWrapper3);
                            a(this.g, downloadInfoWrapper3);
                            a(this.f, downloadInfoWrapper3);
                            a(this.e, downloadInfoWrapper3, 0);
                            a(this.d, downloadInfoWrapper3, 0);
                            notifyDataSetChanged();
                            break;
                        }
                        break;
                    default:
                        l();
                        notifyDataSetChanged();
                        break;
                }
                for (int i2 = 0; i2 < getGroupCount(); i2++) {
                    this.t.expandGroup(i2);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    private void c(DownloadInfo downloadInfo) {
        View a2;
        bp bpVar;
        bz bzVar;
        by byVar;
        if (downloadInfo != null && (a2 = a(downloadInfo.downloadTicket)) != null && (bpVar = (bp) a2.getTag()) != null && (bzVar = bpVar.b) != null && (byVar = bzVar.f733a) != null && ct.a(downloadInfo.downloadTicket, byVar.o)) {
            a(byVar, downloadInfo, u.a(downloadInfo, true, true));
        }
    }

    private boolean b(DownloadInfoWrapper downloadInfoWrapper) {
        boolean z;
        if (downloadInfoWrapper == null) {
            return false;
        }
        synchronized (this.g) {
            z = a(this.g, downloadInfoWrapper) || a(this.f, downloadInfoWrapper);
        }
        if (!this.e.contains(downloadInfoWrapper)) {
            a(this.e, downloadInfoWrapper, 0);
            z = true;
        }
        if (!this.d.contains(downloadInfoWrapper)) {
            a(this.d, downloadInfoWrapper, 0);
            z = true;
        }
        if (!z) {
            return z;
        }
        notifyDataSetChanged();
        return z;
    }

    private boolean a(List<DownloadInfoWrapper> list, DownloadInfo downloadInfo, int i2) {
        if (list == null || list.size() < 0 || downloadInfo == null || i2 < 0) {
            return false;
        }
        list.add(i2, new DownloadInfoWrapper(downloadInfo));
        return false;
    }

    private boolean a(List<DownloadInfoWrapper> list, DownloadInfoWrapper downloadInfoWrapper, int i2) {
        if (list == null || list.size() < 0 || downloadInfoWrapper == null || i2 < 0) {
            return false;
        }
        list.add(i2, downloadInfoWrapper);
        return false;
    }

    private boolean a(List<DownloadInfoWrapper> list, DownloadInfoWrapper downloadInfoWrapper) {
        if (list == null || list.size() <= 0 || downloadInfoWrapper == null) {
            return false;
        }
        int i2 = 0;
        while (true) {
            if (i2 >= list.size()) {
                i2 = -1;
                break;
            } else if (list.get(i2).equals(downloadInfoWrapper)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 < 0) {
            return false;
        }
        list.remove(i2);
        return true;
    }

    private boolean a(List<DownloadInfoWrapper> list, long j2) {
        if (list == null || list.size() <= 0) {
            return false;
        }
        int i2 = 0;
        while (true) {
            if (i2 >= list.size()) {
                i2 = -1;
                break;
            }
            DownloadInfoWrapper downloadInfoWrapper = list.get(i2);
            if (downloadInfoWrapper != null && downloadInfoWrapper.f1246a == DownloadInfoWrapper.InfoType.App && downloadInfoWrapper.b != null && downloadInfoWrapper.b.downloadTicket != null && downloadInfoWrapper.b.appId == j2) {
                break;
            }
            i2++;
        }
        if (i2 < 0) {
            return false;
        }
        list.remove(i2);
        return true;
    }

    private bs n() {
        if (this.n == null) {
            try {
                View inflate = View.inflate(this.m, R.layout.appinfo_download_creating_item, null);
                this.n = new bs(this, null);
                this.n.f731a = inflate;
                this.n.e = inflate.findViewById(R.id.loading_container);
                this.n.b = (ImageView) inflate.findViewById(R.id.app_icon);
                this.n.c = (TextView) inflate.findViewById(R.id.task_status);
                this.n.d = (Button) inflate.findViewById(R.id.retry_btn);
                this.n.d.setOnClickListener(this.o);
                this.n.f = (ImageView) inflate.findViewById(R.id.last_line);
            } catch (Throwable th) {
                cq.a().b();
                th.printStackTrace();
                return null;
            }
        }
        return this.n;
    }

    private by o() {
        by byVar = new by();
        try {
            View inflate = View.inflate(this.m, R.layout.appinfo_download_item, null);
            byVar.f = inflate;
            byVar.g = (RelativeLayout) inflate.findViewById(R.id.layout_item);
            byVar.i = (DownloadButton) inflate.findViewById(R.id.down_btn);
            byVar.i.a(true);
            byVar.i.b(true);
            byVar.h = (TXImageView) inflate.findViewById(R.id.app_icon);
            byVar.j = (TextView) inflate.findViewById(R.id.app_name);
            byVar.l = (TextView) inflate.findViewById(R.id.down_percent);
            byVar.m = (TextView) inflate.findViewById(R.id.down_speed);
            byVar.n = (TextView) inflate.findViewById(R.id.down_size);
            byVar.k = (ProgressBar) inflate.findViewById(R.id.down_progress);
            byVar.c = (RelativeLayout) inflate.findViewById(R.id.view_all_layout);
            byVar.d = (RelativeLayout) inflate.findViewById(R.id.view_all_btn_layout);
            byVar.e = (ImageView) inflate.findViewById(R.id.last_line);
            byVar.p = (ImageView) inflate.findViewById(R.id.delete_img);
            byVar.q = (MovingProgressBar) inflate.findViewById(R.id.download_listitem_install_cursor);
            byVar.q.a(this.m.getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
            byVar.q.b(this.m.getResources().getDimensionPixelSize(R.dimen.install_progress_bar_width));
        } catch (Throwable th) {
            cq.a().b();
        }
        return byVar;
    }

    private bx p() {
        bx bxVar = new bx();
        View inflate = View.inflate(this.m, R.layout.video_download_item, null);
        bxVar.f = inflate;
        bxVar.g = (RelativeLayout) inflate.findViewById(R.id.layout_item);
        bxVar.i = (VideoDownloadButton) inflate.findViewById(R.id.down_btn);
        bxVar.h = (TXImageView) inflate.findViewById(R.id.app_icon);
        bxVar.j = (TextView) inflate.findViewById(R.id.app_name);
        bxVar.l = (TextView) inflate.findViewById(R.id.down_percent);
        bxVar.m = (TextView) inflate.findViewById(R.id.down_speed);
        bxVar.n = (TextView) inflate.findViewById(R.id.down_size);
        bxVar.k = (ProgressBar) inflate.findViewById(R.id.down_progress);
        bxVar.c = (RelativeLayout) inflate.findViewById(R.id.view_all_layout);
        bxVar.d = (RelativeLayout) inflate.findViewById(R.id.view_all_btn_layout);
        bxVar.e = (ImageView) inflate.findViewById(R.id.last_line);
        bxVar.p = (ImageView) inflate.findViewById(R.id.delete_img);
        return bxVar;
    }

    private bu q() {
        bu buVar = new bu();
        View inflate = View.inflate(this.m, R.layout.file_download_item, null);
        buVar.f = inflate;
        buVar.g = (RelativeLayout) inflate.findViewById(R.id.layout_item);
        buVar.i = (FileDownloadButton) inflate.findViewById(R.id.down_btn);
        buVar.h = (TXImageView) inflate.findViewById(R.id.app_icon);
        buVar.j = (TextView) inflate.findViewById(R.id.app_name);
        buVar.l = (TextView) inflate.findViewById(R.id.down_percent);
        buVar.m = (TextView) inflate.findViewById(R.id.down_speed);
        buVar.n = (TextView) inflate.findViewById(R.id.down_size);
        buVar.k = (ProgressBar) inflate.findViewById(R.id.down_progress);
        buVar.c = (RelativeLayout) inflate.findViewById(R.id.view_all_layout);
        buVar.d = (RelativeLayout) inflate.findViewById(R.id.view_all_btn_layout);
        buVar.e = (ImageView) inflate.findViewById(R.id.last_line);
        buVar.p = (ImageView) inflate.findViewById(R.id.delete_img);
        return buVar;
    }

    private br r() {
        br brVar = new br();
        View inflate = View.inflate(this.m, R.layout.book_download_item, null);
        brVar.f = inflate;
        brVar.g = (RelativeLayout) inflate.findViewById(R.id.layout_item);
        brVar.i = (BookReadButton) inflate.findViewById(R.id.down_btn);
        brVar.h = (TXImageView) inflate.findViewById(R.id.book_icon);
        brVar.j = (TextView) inflate.findViewById(R.id.book_name);
        brVar.k = (TextView) inflate.findViewById(R.id.book_desc_text);
        brVar.c = (RelativeLayout) inflate.findViewById(R.id.view_all_layout);
        brVar.d = (RelativeLayout) inflate.findViewById(R.id.view_all_btn_layout);
        brVar.e = (ImageView) inflate.findViewById(R.id.last_line);
        return brVar;
    }

    public CreatingTaskStatusEnum j() {
        return this.k;
    }

    public void a(CreatingTaskStatusEnum creatingTaskStatusEnum) {
        this.k = creatingTaskStatusEnum;
    }

    public void b(CreatingTaskStatusEnum creatingTaskStatusEnum) {
        this.k = creatingTaskStatusEnum;
        a(this.n, this.k);
    }

    private void a(by byVar) {
        byVar.m.setText(this.m.getResources().getString(R.string.install_show_message));
        byVar.m.setVisibility(0);
        byVar.l.setVisibility(8);
        byVar.n.setVisibility(8);
        byVar.k.setVisibility(8);
        byVar.q.setVisibility(0);
        byVar.q.a();
    }

    private void b(by byVar) {
        byVar.q.b();
        byVar.q.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList<DownloadInfo> arrayList) {
        ax axVar = new ax(this, arrayList);
        axVar.hasTitle = true;
        axVar.titleRes = this.m.getString(R.string.dialog_title_check_apk_installed);
        axVar.contentRes = this.m.getString(R.string.downloaded_app_install_content, Integer.valueOf(arrayList.size()));
        axVar.lBtnTxtRes = this.m.getString(R.string.down_page_dialog_left_del);
        axVar.rBtnTxtRes = this.m.getString(R.string.downloaded_app_install_right_btn);
        v.a(axVar);
        com.tencent.assistantv2.st.k.a(new STInfoV2(STConst.ST_PAGE_DOWNLOAD, SecondNavigationTitleView.TMA_ST_NAVBAR_HOME_TAG, 0, STConst.ST_DEFAULT_SLOT, 100));
    }
}
