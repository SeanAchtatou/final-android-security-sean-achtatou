package com.tencent.assistant.adapter;

import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.manager.DownloadProxy;

/* compiled from: ProGuard */
class av extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f708a;
    final /* synthetic */ DownloadInfoMultiAdapter b;

    av(DownloadInfoMultiAdapter downloadInfoMultiAdapter, DownloadInfo downloadInfo) {
        this.b = downloadInfoMultiAdapter;
        this.f708a = downloadInfo;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        this.f708a.response = null;
        DownloadProxy.a().b(this.f708a.downloadTicket);
        a.a().a(this.f708a);
        Toast.makeText(this.b.m, this.b.m.getResources().getString(R.string.down_add_tips), 0).show();
    }

    public void onCancell() {
    }
}
