package com.tencent.assistant.adapter;

import android.os.Message;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class f extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalApkInfo f796a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ d c;

    f(d dVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        this.c = dVar;
        this.f796a = localApkInfo;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.c.f759a.sendMessage(Message.obtain(this.c.f759a, 110001, this.f796a));
    }

    public STInfoV2 getStInfo() {
        this.b.actionId = STConstAction.ACTION_HIT_INSTALL;
        this.b.status = STConst.ST_STATUS_DEFAULT;
        return this.b;
    }
}
