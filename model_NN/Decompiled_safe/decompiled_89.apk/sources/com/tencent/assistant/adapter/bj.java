package com.tencent.assistant.adapter;

import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.model.d;

/* compiled from: ProGuard */
class bj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f723a;
    final /* synthetic */ TXImageView b;
    final /* synthetic */ TextView c;
    final /* synthetic */ DownloadInfoMultiAdapter d;

    bj(DownloadInfoMultiAdapter downloadInfoMultiAdapter, d dVar, TXImageView tXImageView, TextView textView) {
        this.d = downloadInfoMultiAdapter;
        this.f723a = dVar;
        this.b = tXImageView;
        this.c = textView;
    }

    public void run() {
        ba.a().post(new bk(this, this.d.v.getMetaInfo(this.f723a.h)));
    }
}
