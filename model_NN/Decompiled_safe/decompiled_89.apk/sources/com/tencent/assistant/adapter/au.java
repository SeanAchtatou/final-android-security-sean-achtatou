package com.tencent.assistant.adapter;

import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.manager.DownloadProxy;

/* compiled from: ProGuard */
class au extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f707a;
    final /* synthetic */ DownloadInfo b;
    final /* synthetic */ DownloadInfoMultiAdapter c;

    au(DownloadInfoMultiAdapter downloadInfoMultiAdapter, boolean z, DownloadInfo downloadInfo) {
        this.c = downloadInfoMultiAdapter;
        this.f707a = z;
        this.b = downloadInfo;
    }

    public void onLeftBtnClick() {
    }

    public void onRightBtnClick() {
        if (this.f707a) {
            a.a().d(this.b);
            return;
        }
        this.b.response = null;
        DownloadProxy.a().b(this.b.downloadTicket);
        a.a().a(this.b);
        Toast.makeText(this.c.m, this.c.m.getResources().getString(R.string.down_add_tips), 0).show();
    }

    public void onCancell() {
    }
}
