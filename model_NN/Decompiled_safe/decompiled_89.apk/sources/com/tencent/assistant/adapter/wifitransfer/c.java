package com.tencent.assistant.adapter.wifitransfer;

import android.view.View;
import com.tencent.assistant.localres.WiFiReceiveItem;

/* compiled from: ProGuard */
class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WiFiReceiveItem f815a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ WifiReceiveAdapter d;

    c(WifiReceiveAdapter wifiReceiveAdapter, WiFiReceiveItem wiFiReceiveItem, int i, int i2) {
        this.d = wifiReceiveAdapter;
        this.f815a = wiFiReceiveItem;
        this.b = i;
        this.c = i2;
    }

    public void onClick(View view) {
        if (this.f815a.mReceiveState == 1 && this.d.k != null) {
            this.d.k.a(this.b, this.c, this.f815a.mFilePath, this.f815a.mFileName, this.f815a.mFileType);
        }
    }
}
