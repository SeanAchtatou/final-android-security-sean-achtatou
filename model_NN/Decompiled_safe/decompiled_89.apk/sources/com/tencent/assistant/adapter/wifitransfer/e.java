package com.tencent.assistant.adapter.wifitransfer;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.DownloadProgressBar;
import com.tencent.assistant.component.txscrollview.TXImageView;

/* compiled from: ProGuard */
class e {

    /* renamed from: a  reason: collision with root package name */
    public View f817a;
    public TXImageView b;
    public TextView c;
    public DownloadProgressBar d;
    public ImageView e;
    public ImageView f;
    final /* synthetic */ WifiReceiveAdapter g;

    private e(WifiReceiveAdapter wifiReceiveAdapter) {
        this.g = wifiReceiveAdapter;
    }

    /* synthetic */ e(WifiReceiveAdapter wifiReceiveAdapter, b bVar) {
        this(wifiReceiveAdapter);
    }
}
