package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.aj;
import com.tencent.assistant.protocol.jce.SourceCheckRequest;
import com.tencent.assistant.protocol.jce.SourceCheckResponse;

/* compiled from: ProGuard */
class ft implements CallbackHelper.Caller<aj> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ JceStruct f1830a;
    final /* synthetic */ int b;
    final /* synthetic */ SourceCheckResponse c;
    final /* synthetic */ fs d;

    ft(fs fsVar, JceStruct jceStruct, int i, SourceCheckResponse sourceCheckResponse) {
        this.d = fsVar;
        this.f1830a = jceStruct;
        this.b = i;
        this.c = sourceCheckResponse;
    }

    /* renamed from: a */
    public void call(aj ajVar) {
        if (((SourceCheckRequest) this.f1830a).b == 1) {
            ajVar.a(this.b, 0, this.c.b, this.c.a(), this.c.e);
            return;
        }
        ajVar.a(this.b, 0, this.c.b, this.c.a(), this.d.a(this.c));
    }
}
