package com.tencent.assistant.module;

import com.tencent.assistant.manager.as;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.protocol.jce.GftGetAppListResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
class bx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bw f1736a;

    bx(bw bwVar) {
        this.f1736a = bwVar;
    }

    public void run() {
        boolean z = true;
        GftGetAppListResponse b = as.w().b(this.f1736a.f1735a.f1728a, this.f1736a.f1735a.b, this.f1736a.c);
        if (b == null || b.e != this.f1736a.f1735a.f || b.b == null || b.b.size() <= 0) {
            int unused = this.f1736a.h = this.f1736a.f1735a.a(this.f1736a.h, this.f1736a.c);
            return;
        }
        bw bwVar = this.f1736a;
        long j = b.e;
        ArrayList<SimpleAppModel> b2 = u.b(b.b);
        if (b.d != 1) {
            z = false;
        }
        bwVar.a(j, b2, z, b.c);
    }
}
