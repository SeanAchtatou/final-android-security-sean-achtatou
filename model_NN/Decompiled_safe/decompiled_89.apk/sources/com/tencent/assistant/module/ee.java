package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.module.callback.u;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.GetUserInfoRequest;
import com.tencent.assistant.protocol.jce.GetUserInfoResponse;

/* compiled from: ProGuard */
public class ee extends BaseEngine<u> implements NetworkMonitor.ConnectivityChangeListener {
    private static ee e;

    /* renamed from: a  reason: collision with root package name */
    u f1792a = new eh(this);
    private int b = -1;
    private int c;
    private Object d = new Object();

    public static synchronized ee a() {
        ee eeVar;
        synchronized (ee.class) {
            if (e == null) {
                e = new ee();
            }
            eeVar = e;
        }
        return eeVar;
    }

    private ee() {
        register(this.f1792a);
        cq.a().a(this);
    }

    public int b() {
        d();
        return c();
    }

    private int c() {
        GetUserInfoRequest getUserInfoRequest = new GetUserInfoRequest();
        if (this.b > 0) {
            cancel(this.b);
        }
        this.b = send(getUserInfoRequest);
        return this.b;
    }

    private void d() {
        synchronized (this.d) {
            this.c = 0;
        }
    }

    private void e() {
        synchronized (this.d) {
            this.c++;
        }
    }

    private boolean f() {
        boolean z;
        synchronized (this.d) {
            if (this.c < 2) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        d();
        notifyDataChangedInMainThread(new ef(this, i, jceStruct, (GetUserInfoResponse) jceStruct2));
        this.b = -1;
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = true;
        if (a.b(i2) && f() && i2 != -801 && i2 != -800) {
            c();
            e();
            z = false;
        }
        if (z) {
            d();
            notifyDataChangedInMainThread(new eg(this, i, i2, jceStruct, jceStruct2));
        }
        this.b = -1;
    }

    public void onConnected(APN apn) {
        if (d.a().j() && !a.g()) {
            b();
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
