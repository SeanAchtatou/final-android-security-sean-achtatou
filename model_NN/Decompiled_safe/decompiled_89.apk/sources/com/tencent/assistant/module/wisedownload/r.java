package com.tencent.assistant.module.wisedownload;

import android.text.TextUtils;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.utils.cv;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class r {
    public static long a() {
        return p.a(SimpleDownloadInfo.UIType.WISE_SELF_UPDAET);
    }

    public static boolean a(SimpleAppModel simpleAppModel) {
        return p.a(SimpleDownloadInfo.UIType.WISE_SELF_UPDAET, simpleAppModel);
    }

    public static boolean b(SimpleAppModel simpleAppModel) {
        return p.a(SimpleDownloadInfo.UIType.WISE_APP_UPDATE, simpleAppModel);
    }

    public static boolean a(DownloadInfo downloadInfo) {
        return p.a(SimpleDownloadInfo.UIType.WISE_SELF_UPDAET, downloadInfo);
    }

    public static boolean b(DownloadInfo downloadInfo) {
        return p.a(SimpleDownloadInfo.UIType.WISE_APP_UPDATE, downloadInfo);
    }

    public static long b() {
        return p.a(SimpleDownloadInfo.UIType.WISE_APP_UPDATE);
    }

    public static List<DownloadInfo> c() {
        return p.b(SimpleDownloadInfo.UIType.WISE_APP_UPDATE);
    }

    public static List<DownloadInfo> d() {
        ArrayList arrayList = new ArrayList();
        Iterator<DownloadInfo> it = DownloadProxy.a().d().iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.downloadState == SimpleDownloadInfo.DownloadState.SUCC && next.isUiTypeWiseUpdateDownload()) {
                String filePath = next.getFilePath();
                if (!TextUtils.isEmpty(filePath) && new File(filePath).exists()) {
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }

    public static boolean e() {
        List<AppUpdateInfo> g;
        boolean z = false;
        List<DownloadInfo> d = d();
        if (d == null || d.isEmpty() || (g = u.g()) == null || g.isEmpty()) {
            return false;
        }
        Iterator<DownloadInfo> it = d.iterator();
        while (true) {
            boolean z2 = z;
            if (it.hasNext()) {
                DownloadInfo next = it.next();
                Iterator<AppUpdateInfo> it2 = g.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        z = z2;
                        break;
                    }
                    AppUpdateInfo next2 = it2.next();
                    if (next.packageName.equals(next2.f2006a) && next.versionCode == next2.d) {
                        z = true;
                        break;
                    }
                }
            } else {
                return z2;
            }
        }
    }

    public static long f() {
        return p.a(SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD);
    }

    public static long g() {
        return p.a(SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD);
    }

    public static List<DownloadInfo> h() {
        return p.b(SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD);
    }

    public static long i() {
        return p.a(SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD);
    }

    public static List<DownloadInfo> j() {
        return p.b(SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD);
    }

    public static void k() {
        boolean z;
        LocalApkInfo localApkInfo;
        ArrayList<DownloadInfo> arrayList = new ArrayList<>();
        arrayList.addAll(p.b(SimpleDownloadInfo.UIType.WISE_APP_UPDATE));
        arrayList.addAll(p.b(SimpleDownloadInfo.UIType.WISE_SELF_UPDAET));
        ArrayList arrayList2 = new ArrayList();
        List<AutoDownloadInfo> c = com.tencent.assistant.module.update.u.a().c();
        if (c != null && !c.isEmpty()) {
            arrayList2.addAll(c);
        }
        List<AutoDownloadInfo> g = com.tencent.assistant.module.update.u.a().g();
        if (g != null && !g.isEmpty()) {
            arrayList2.addAll(g);
        }
        ArrayList<DownloadInfo> arrayList3 = new ArrayList<>();
        if (arrayList.size() > 0) {
            for (DownloadInfo downloadInfo : arrayList) {
                boolean z2 = false;
                if (arrayList2.size() > 0) {
                    Iterator it = arrayList2.iterator();
                    while (true) {
                        z = z2;
                        if (!it.hasNext()) {
                            break;
                        }
                        AutoDownloadInfo autoDownloadInfo = (AutoDownloadInfo) it.next();
                        if (autoDownloadInfo.f2010a.equals(downloadInfo.packageName) && autoDownloadInfo.d > downloadInfo.versionCode) {
                            z = true;
                            arrayList3.add(downloadInfo);
                        }
                        z2 = z;
                    }
                } else {
                    z = false;
                }
                if (!z && (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName)) != null && localApkInfo.mVersionCode >= downloadInfo.versionCode && !cv.d(downloadInfo.downloadEndTime)) {
                    arrayList3.add(downloadInfo);
                }
            }
        }
        if (arrayList3.size() > 0) {
            for (DownloadInfo c2 : arrayList3) {
                c(c2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    private static void c(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            DownloadProxy.a().b(downloadInfo.downloadTicket, true);
        }
    }
}
