package com.tencent.assistant.module.timer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.tencent.assistant.backgroundscan.BackgroundScanTimerJob;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.a;
import com.tencent.assistant.manager.notification.z;
import com.tencent.assistant.module.timer.job.AppUpdateTimerJob;
import com.tencent.assistant.module.timer.job.GetAppExInfoScheduleJob;
import com.tencent.assistant.module.timer.job.GetOtherPushUpdateInfoTimerJob;
import com.tencent.assistant.uninstall.e;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class ScheduleJobReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static d f1860a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.uninstall.e.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.assistant.uninstall.e.a(com.tencent.assistant.uninstall.e, android.content.Context):void
      com.tencent.assistant.uninstall.e.a(com.tencent.assistant.uninstall.e, boolean):boolean
      com.tencent.assistant.uninstall.e.a(android.content.Context, boolean):void */
    public void onReceive(Context context, Intent intent) {
        XLog.d("ScheduleJobReceiver", "onReceive, action:" + intent.getAction());
        try {
            String action = intent.getAction();
            if (action.equals("com.tencent.android.qqdownloader.action.SCHEDULE_JOB")) {
                String stringExtra = intent.getStringExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB");
                XLog.i("ScheduleJobReceiver", "on receive,action:" + action + ",this:" + this + ",clazz:" + stringExtra + ",mQueue=" + f1860a);
                if (!TextUtils.isEmpty(stringExtra)) {
                    if (f1860a == null) {
                        f1860a = new d();
                    }
                    f1860a.a(stringExtra);
                }
            } else if (action.equals("android.intent.action.BOOT_COMPLETED")) {
                e.a().a(context.getApplicationContext(), true);
                a();
            } else if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                a();
            } else if (action.equals("android.intent.action.SCREEN_ON")) {
                a();
            } else if (action.equals("android.intent.action.USER_PRESENT")) {
                a.a().b();
                z.a().b();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void a() {
        GetAppExInfoScheduleJob.i().d();
        b();
        GetOtherPushUpdateInfoTimerJob.g().e();
        BackgroundScanTimerJob.a().e();
    }

    public static void b() {
        if (m.a().q()) {
            AppUpdateTimerJob.g().e();
        } else {
            AppUpdateTimerJob.g().f();
        }
    }
}
