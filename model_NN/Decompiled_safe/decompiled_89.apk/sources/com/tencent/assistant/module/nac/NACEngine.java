package com.tencent.assistant.module.nac;

import com.tencent.assistant.protocol.jce.IPData;
import com.tencent.assistant.protocol.jce.IPDataAddress;

/* compiled from: ProGuard */
public class NACEngine {

    /* renamed from: a  reason: collision with root package name */
    protected c f1844a;
    protected NACEMode b = NACEMode.NACMODE_DOMAIN;
    protected long c = System.currentTimeMillis();
    protected int d = 0;

    /* compiled from: ProGuard */
    public enum NACEMode {
        NACMODE_ADV,
        NACMODE_IPLIST,
        NACMODE_DOMAIN
    }

    public NACEngine(c cVar) {
        this.f1844a = cVar;
    }

    public synchronized void a(IPData iPData) {
        if (iPData != null) {
            if (iPData.b.isEmpty()) {
                this.f1844a.e.clear();
                b(NACEMode.NACMODE_DOMAIN);
            } else {
                this.f1844a.e.clear();
                this.f1844a.e.addAll(iPData.b);
                b(NACEMode.NACMODE_IPLIST);
            }
        }
    }

    public synchronized void b(IPData iPData) {
        if (iPData != null) {
            if (iPData.b.isEmpty()) {
                this.f1844a.f.clear();
            } else {
                this.f1844a.f.clear();
                this.f1844a.f.addAll(iPData.b);
            }
        }
    }

    public synchronized f a() {
        IPDataAddress iPDataAddress;
        switch (a.f1846a[this.b.ordinal()]) {
            case 1:
                iPDataAddress = this.f1844a.f.get(this.d);
                break;
            case 2:
                iPDataAddress = null;
                break;
            case 3:
                iPDataAddress = this.f1844a.e.get(this.d);
                break;
            default:
                iPDataAddress = null;
                break;
        }
        return new f(iPDataAddress, this.b, this.f1844a.b, this.f1844a.c, this.f1844a.d);
    }

    public synchronized void a(boolean z, long j) {
        if (!z) {
            if (j > this.c) {
                c();
            }
        }
    }

    public synchronized void a(NACEMode nACEMode) {
        b(nACEMode);
    }

    private void b(NACEMode nACEMode) {
        switch (a.f1846a[nACEMode.ordinal()]) {
            case 1:
                if (this.f1844a.f != null && this.f1844a.f.size() > 0) {
                    this.b = NACEMode.NACMODE_ADV;
                    break;
                } else {
                    this.b = NACEMode.NACMODE_DOMAIN;
                    break;
                }
                break;
            case 2:
                this.b = NACEMode.NACMODE_DOMAIN;
                break;
            case 3:
                if (this.f1844a.e != null && this.f1844a.e.size() > 0) {
                    this.b = NACEMode.NACMODE_IPLIST;
                    break;
                } else {
                    b(NACEMode.NACMODE_ADV);
                    break;
                }
                break;
        }
        this.d = 0;
        this.c = System.currentTimeMillis();
    }

    private void c() {
        switch (a.f1846a[this.b.ordinal()]) {
            case 1:
                if (this.d >= this.f1844a.f.size() - 1) {
                    b(NACEMode.NACMODE_DOMAIN);
                    return;
                } else {
                    this.d++;
                    return;
                }
            case 2:
                b(NACEMode.NACMODE_IPLIST);
                return;
            case 3:
                if (this.d >= this.f1844a.e.size() - 1) {
                    b(NACEMode.NACMODE_ADV);
                    return;
                } else {
                    this.d++;
                    return;
                }
            default:
                return;
        }
    }

    public short b() {
        return this.f1844a.f1848a;
    }
}
