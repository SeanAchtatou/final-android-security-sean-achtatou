package com.tencent.assistant.module;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class ab extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aa f1686a;

    ab(aa aaVar) {
        this.f1686a = aaVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null && i == 1) {
            this.f1686a.a(localApkInfo);
        }
    }
}
