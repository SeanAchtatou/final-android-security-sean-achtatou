package com.tencent.assistant.module.a;

import java.util.Iterator;

/* compiled from: ProGuard */
class f extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1684a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(d dVar, String str) {
        super(str);
        this.f1684a = dVar;
    }

    public void run() {
        Iterator it = this.f1684a.b.iterator();
        while (it.hasNext()) {
            try {
                ((g) it.next()).run();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        this.f1684a.b.clear();
    }
}
