package com.tencent.assistant.module.timer;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* compiled from: ProGuard */
class c extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1863a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(b bVar, Looper looper) {
        super(looper);
        this.f1863a = bVar;
    }

    public void handleMessage(Message message) {
        ScheduleJob scheduleJob = (ScheduleJob) message.obj;
        scheduleJob.d_();
        this.f1863a.a(scheduleJob);
    }
}
