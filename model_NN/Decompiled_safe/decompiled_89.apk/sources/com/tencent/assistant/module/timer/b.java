package com.tencent.assistant.module.timer;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f1862a;
    private Handler b = null;

    private b() {
        HandlerThread handlerThread = new HandlerThread("timer");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        if (looper != null) {
            this.b = new c(this, looper);
        } else {
            Log.e("SimpleTimer", "SimpleTimer loop == null");
        }
    }

    protected static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f1862a == null) {
                f1862a = new b();
            }
            bVar = f1862a;
        }
        return bVar;
    }

    public void a(ScheduleJob scheduleJob) {
        if (this.b != null && scheduleJob != null) {
            this.b.sendMessageDelayed(this.b.obtainMessage(scheduleJob.b(), 0, 0, scheduleJob), (long) (scheduleJob.h() * 1000));
        }
    }
}
