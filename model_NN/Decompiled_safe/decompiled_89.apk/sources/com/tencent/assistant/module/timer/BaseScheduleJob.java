package com.tencent.assistant.module.timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.SystemClock;
import com.qq.AppService.AstApp;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
public abstract class BaseScheduleJob implements ScheduleJob {
    /* access modifiers changed from: protected */
    public abstract void e();

    /* access modifiers changed from: protected */
    public abstract long f();

    public boolean c_() {
        return true;
    }

    public final int b() {
        return getClass().getSimpleName().hashCode();
    }

    public void d_() {
        m.a().b(g(), Long.valueOf(System.currentTimeMillis()));
        e();
    }

    public void d() {
        Intent intent = new Intent("com.tencent.android.qqdownloader.action.SCHEDULE_JOB");
        intent.putExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB", getClass().getName());
        PendingIntent broadcast = PendingIntent.getBroadcast(AstApp.i(), b(), intent, 268435456);
        int h = h() * 1000;
        ((AlarmManager) AstApp.i().getSystemService("alarm")).setRepeating(3, f() + SystemClock.elapsedRealtime(), (long) h, broadcast);
    }

    /* access modifiers changed from: protected */
    public String g() {
        return "timer_" + b();
    }
}
