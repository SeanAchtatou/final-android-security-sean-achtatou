package com.tencent.assistant.module;

import com.tencent.assistant.protocol.jce.AppSecretUserProfile;
import com.tencent.assistant.protocol.jce.SetUserProfileRequest;
import com.tencent.assistant.protocol.jce.UserProfile;
import com.tencent.assistant.utils.bh;
import java.util.ArrayList;

/* compiled from: ProGuard */
class fj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1822a;
    final /* synthetic */ boolean b;
    final /* synthetic */ fh c;

    fj(fh fhVar, int i, boolean z) {
        this.c = fhVar;
        this.f1822a = i;
        this.b = z;
    }

    public void run() {
        if (this.f1822a == 4) {
            UserProfile userProfile = new UserProfile((byte) 4, bh.a(new AppSecretUserProfile(this.b ? 1 : 0)), 0);
            ArrayList<UserProfile> arrayList = new ArrayList<>();
            arrayList.clear();
            arrayList.add(userProfile);
            SetUserProfileRequest setUserProfileRequest = new SetUserProfileRequest();
            setUserProfileRequest.f2305a = arrayList;
            this.c.b.put(Integer.valueOf(this.c.send(setUserProfileRequest)), 4);
        }
    }
}
