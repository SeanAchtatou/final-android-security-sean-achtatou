package com.tencent.assistant.module;

import android.os.Message;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.AppSecretUserProfile;
import com.tencent.assistant.protocol.jce.SetUserProfileRequest;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bh;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class fh extends aw {

    /* renamed from: a  reason: collision with root package name */
    private static fh f1820a;
    /* access modifiers changed from: private */
    public Map<Integer, Integer> b = Collections.synchronizedMap(new HashMap());

    private fh() {
    }

    public static synchronized fh a() {
        fh fhVar;
        synchronized (fh.class) {
            if (f1820a == null) {
                f1820a = new fh();
            }
            fhVar = f1820a;
        }
        return fhVar;
    }

    public void a(boolean z, AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type) {
        TemporaryThreadManager.get().start(new fi(this, wise_download_switch_type, z));
    }

    public void a(boolean z, int i) {
        TemporaryThreadManager.get().start(new fj(this, i, z));
    }

    public void b() {
        boolean e = m.a().e(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
        if (!e) {
            a().a(e, AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
        }
        boolean e2 = m.a().e(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD);
        if (!e2) {
            a().a(e2, AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
     arg types: [com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (this.b.containsKey(Integer.valueOf(i))) {
            int intValue = this.b.get(Integer.valueOf(i)).intValue();
            if (intValue == 1) {
                m.a().b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE, true);
            } else if (intValue == 3) {
                m.a().b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD, true);
            } else if (intValue == 4) {
                Message obtainMessage = AstApp.i().j().obtainMessage();
                obtainMessage.obj = (AppSecretUserProfile) bh.b(((SetUserProfileRequest) jceStruct).a().get(0).a(), AppSecretUserProfile.class);
                obtainMessage.what = EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_SUCC;
                AstApp.i().j().sendMessage(obtainMessage);
            }
            this.b.remove(Integer.valueOf(i));
            Log.v("SetUserProfileEngine", "onRequestSuccessed");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
     arg types: [com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        if (this.b.containsKey(Integer.valueOf(i))) {
            int intValue = this.b.get(Integer.valueOf(i)).intValue();
            if (intValue == 1) {
                m.a().b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE, false);
            } else if (intValue == 3) {
                m.a().b(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD, false);
            } else if (intValue == 4) {
                Message obtainMessage = AstApp.i().j().obtainMessage();
                obtainMessage.what = EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_FAIL;
                AstApp.i().j().sendMessage(obtainMessage);
            }
            this.b.remove(Integer.valueOf(i));
        }
    }
}
