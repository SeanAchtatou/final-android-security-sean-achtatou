package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.d;
import com.tencent.assistant.protocol.jce.ReportAppResponse;

/* compiled from: ProGuard */
class y implements CallbackHelper.Caller<d> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1920a;
    final /* synthetic */ ReportAppResponse b;
    final /* synthetic */ long c;
    final /* synthetic */ long d;
    final /* synthetic */ x e;

    y(x xVar, int i, ReportAppResponse reportAppResponse, long j, long j2) {
        this.e = xVar;
        this.f1920a = i;
        this.b = reportAppResponse;
        this.c = j;
        this.d = j2;
    }

    /* renamed from: a */
    public void call(d dVar) {
        dVar.a(this.f1920a, this.b.f2280a, this.c, this.d);
    }
}
