package com.tencent.assistant.module.b;

import android.text.TextUtils;
import com.tencent.assistant.db.a.a;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.utils.FileUtil;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1713a;
    final /* synthetic */ String b;
    final /* synthetic */ long c;
    final /* synthetic */ long d;
    final /* synthetic */ int e;
    final /* synthetic */ String f;
    final /* synthetic */ b g;

    e(b bVar, String str, String str2, long j, long j2, int i, String str3) {
        this.g = bVar;
        this.f1713a = str;
        this.b = str2;
        this.c = j;
        this.d = j2;
        this.e = i;
        this.f = str3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.b.b.a(com.tencent.assistant.module.b.b, com.tencent.assistant.db.a.a, boolean):void
     arg types: [com.tencent.assistant.module.b.b, com.tencent.assistant.db.a.a, int]
     candidates:
      com.tencent.assistant.module.b.b.a(java.lang.String, java.lang.String, java.util.List<java.lang.String>):boolean
      com.tencent.assistant.module.b.b.a(com.tencent.assistant.module.b.b, com.tencent.assistant.db.a.a, boolean):void */
    public void run() {
        LocalApkInfo localApkInfo;
        if (FileUtil.isFileExists(this.f1713a) || FileUtil.isFileExists(this.b)) {
            a aVar = new a();
            aVar.f1227a = System.currentTimeMillis();
            aVar.b = this.c;
            aVar.c = this.d;
            aVar.g = this.f1713a;
            aVar.h = this.b;
            DownloadInfo d2 = DownloadProxy.a().d(String.valueOf(this.d));
            if (d2 != null) {
                aVar.e = d2.versionCode;
                aVar.d = d2.packageName;
                if ((TextUtils.isEmpty(aVar.d) || aVar.f == 0) && (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.c)) != null) {
                    aVar.f = localApkInfo.mVersionCode;
                    aVar.d = localApkInfo.mPackageName;
                }
                aVar.m = this.e;
                aVar.n = this.f;
                this.g.a(aVar, false);
            }
        }
    }
}
