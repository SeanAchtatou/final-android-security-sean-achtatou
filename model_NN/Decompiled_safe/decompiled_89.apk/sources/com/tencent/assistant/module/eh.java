package com.tencent.assistant.module;

import android.os.Message;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.login.a.b;
import com.tencent.assistant.module.callback.u;
import com.tencent.assistant.protocol.jce.GetUserInfoRequest;
import com.tencent.assistant.protocol.jce.GetUserInfoResponse;

/* compiled from: ProGuard */
class eh implements u {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ee f1795a;

    eh(ee eeVar) {
        this.f1795a = eeVar;
    }

    public void a(int i, GetUserInfoRequest getUserInfoRequest, GetUserInfoResponse getUserInfoResponse) {
        if (getUserInfoRequest != null) {
            b bVar = new b(getUserInfoResponse.b, getUserInfoResponse.c, getUserInfoResponse.d);
            a.a(bVar);
            Message obtainMessage = AstApp.i().j().obtainMessage();
            obtainMessage.obj = bVar;
            obtainMessage.arg1 = i;
            Log.d("Donaldxu", "UI_EVENT_GET_USERINFO_SUCCESS---profile = " + bVar.f1456a + " nickName = " + bVar.b + " bitmap= " + bVar.c);
            obtainMessage.what = EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS;
            AstApp.i().j().sendMessage(obtainMessage);
        }
    }

    public void a(int i, int i2, GetUserInfoRequest getUserInfoRequest, GetUserInfoResponse getUserInfoResponse) {
        a.a((b) null);
        Message obtainMessage = AstApp.i().j().obtainMessage();
        obtainMessage.what = EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL;
        obtainMessage.arg1 = i;
        Log.d("Donaldxu", "UI_EVENT_GET_USERINFO_FAIL errorCode=" + i2);
        AstApp.i().j().sendMessage(obtainMessage);
    }
}
