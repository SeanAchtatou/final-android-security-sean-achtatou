package com.tencent.assistant.module.callback;

import com.tencent.assistant.module.callback.CallbackHelper;

/* compiled from: ProGuard */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CallbackHelper.Caller f1742a;
    final /* synthetic */ CallbackHelper b;

    i(CallbackHelper callbackHelper, CallbackHelper.Caller caller) {
        this.b = callbackHelper;
        this.f1742a = caller;
    }

    public void run() {
        this.b.broadcast(this.f1742a);
    }
}
