package com.tencent.assistant.module;

import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.protocol.jce.DataUpdateInfo;
import com.tencent.assistant.protocol.jce.GetUnionUpdateInfoRequest;
import com.tencent.assistant.protocol.jce.GetUnionUpdateInfoResponse;
import com.tencent.assistant.protocol.jce.NotifyInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.manager.k;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: ProGuard */
public class dy extends aw {

    /* renamed from: a  reason: collision with root package name */
    static final byte[] f1789a = {1, 3, 4, 5, 6, JceStruct.SIMPLE_LIST, 14, 17, 19};
    private static dy e;
    protected ReferenceQueue<eb> b = new ReferenceQueue<>();
    protected ConcurrentLinkedQueue<WeakReference<eb>> c = new ConcurrentLinkedQueue<>();
    final HashMap<Byte, ed> d = new HashMap<>();
    private long f = 0;
    private UIEventListener g = new dz(this);

    private dy() {
        this.d.put((byte) 1, cz.a());
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this.g);
    }

    public static synchronized dy a() {
        dy dyVar;
        synchronized (dy.class) {
            if (e == null) {
                e = new dy();
            }
            dyVar = e;
        }
        return dyVar;
    }

    public void a(eb ebVar) {
        if (ebVar != null) {
            while (true) {
                Reference<? extends eb> poll = this.b.poll();
                if (poll == null) {
                    break;
                }
                this.c.remove(poll);
            }
            Iterator<WeakReference<eb>> it = this.c.iterator();
            while (it.hasNext()) {
                if (((eb) it.next().get()) == ebVar) {
                    return;
                }
            }
            this.c.add(new WeakReference(ebVar, this.b));
        }
    }

    private boolean a(byte b2) {
        return b2 == 19;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z;
        XLog.d("linmg", "GetUnionUpdateInfoEngine onRequestSuccessed....");
        GetUnionUpdateInfoResponse getUnionUpdateInfoResponse = (GetUnionUpdateInfoResponse) jceStruct2;
        ArrayList<DataUpdateInfo> a2 = getUnionUpdateInfoResponse.a();
        if (a2 != null && a2.size() > 0) {
            boolean z2 = false;
            Iterator<DataUpdateInfo> it = a2.iterator();
            while (true) {
                z = z2;
                if (!it.hasNext()) {
                    break;
                }
                DataUpdateInfo next = it.next();
                if (a(next.f2050a)) {
                    XLog.d("linmg", "GetUnionUpdateInfoEngine is InTime");
                    k.a().a(next);
                } else {
                    z = true;
                    m.a().a(next.f2050a, next.b);
                }
                z2 = z;
            }
            if (z) {
                b();
            }
        }
        a(getUnionUpdateInfoResponse.b());
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
    }

    private void a(List<NotifyInfo> list) {
        if (list != null && list.size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (NotifyInfo next : list) {
                if (next != null) {
                    byte b2 = next.f2243a;
                    if (b2 == 1) {
                        ed edVar = this.d.get(Byte.valueOf(b2));
                        if (edVar != null) {
                            edVar.a(next.b);
                        }
                    } else if (b2 == 2) {
                        QuickEntranceNotify quickEntranceNotify = new QuickEntranceNotify();
                        quickEntranceNotify.f1631a = next.c;
                        quickEntranceNotify.b = next.d;
                        quickEntranceNotify.e = next.e;
                        arrayList.add(quickEntranceNotify);
                    }
                }
            }
            if (arrayList.size() > 0) {
                a((ArrayList<QuickEntranceNotify>) arrayList);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        Iterator<WeakReference<eb>> it = this.c.iterator();
        while (it.hasNext()) {
            eb ebVar = (eb) it.next().get();
            if (ebVar != null) {
                ebVar.d();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList<QuickEntranceNotify> arrayList) {
        Iterator<WeakReference<eb>> it = this.c.iterator();
        while (it.hasNext()) {
            eb ebVar = (eb) it.next().get();
            if (ebVar != null) {
                ebVar.a(arrayList);
            }
        }
    }

    public int c() {
        if (System.currentTimeMillis() - this.f < 5000) {
            return -1;
        }
        this.f = System.currentTimeMillis();
        XLog.d("NotifyPrompt", "****************** NotifyInfo request");
        GetUnionUpdateInfoRequest getUnionUpdateInfoRequest = new GetUnionUpdateInfoRequest();
        getUnionUpdateInfoRequest.f2173a = new ArrayList<>();
        for (byte b2 : f1789a) {
            DataUpdateInfo dataUpdateInfo = new DataUpdateInfo();
            dataUpdateInfo.f2050a = b2;
            dataUpdateInfo.b = m.a().a(b2);
            getUnionUpdateInfoRequest.f2173a.add(dataUpdateInfo);
        }
        return send(getUnionUpdateInfoRequest);
    }
}
