package com.tencent.assistant.module;

import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.t;
import java.util.List;

/* compiled from: ProGuard */
class dw implements CallbackHelper.Caller<t> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1787a;
    final /* synthetic */ boolean b;
    final /* synthetic */ List c;
    final /* synthetic */ AppGroupInfo d;
    final /* synthetic */ dv e;

    dw(dv dvVar, int i, boolean z, List list, AppGroupInfo appGroupInfo) {
        this.e = dvVar;
        this.f1787a = i;
        this.b = z;
        this.c = list;
        this.d = appGroupInfo;
    }

    /* renamed from: a */
    public void call(t tVar) {
        tVar.a(this.f1787a, 0, this.b, this.c, this.d);
    }
}
