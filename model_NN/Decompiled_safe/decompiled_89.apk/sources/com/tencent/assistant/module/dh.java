package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.q;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/* compiled from: ProGuard */
class dh implements CallbackHelper.Caller<q> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1774a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ de d;

    dh(de deVar, int i, int i2, boolean z) {
        this.d = deVar;
        this.f1774a = i;
        this.b = i2;
        this.c = z;
    }

    /* renamed from: a */
    public void call(q qVar) {
        q qVar2 = qVar;
        qVar2.a(this.f1774a, this.b, this.c, new LinkedHashMap(), new ArrayList(), true);
    }
}
