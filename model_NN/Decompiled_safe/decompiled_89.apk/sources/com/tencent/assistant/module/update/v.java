package com.tencent.assistant.module.update;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.List;

/* compiled from: ProGuard */
class v extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f1899a;

    v(u uVar) {
        this.f1899a = uVar;
    }

    public void onLoadInstalledApkSuccess(List<LocalApkInfo> list) {
        this.f1899a.l();
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null && 1 == i && this.f1899a.c != null) {
            TemporaryThreadManager.get().start(new w(this, localApkInfo));
        }
    }
}
