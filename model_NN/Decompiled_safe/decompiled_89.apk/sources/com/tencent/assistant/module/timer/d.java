package com.tencent.assistant.module.timer;

import com.tencent.assistant.backgroundscan.BackgroundScanTimerJob;
import java.util.ArrayList;
import java.util.LinkedList;

/* compiled from: ProGuard */
public class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private LinkedList<String> f1864a = new LinkedList<>();
    private Object b = new Object();

    public d() {
        setName("Thread_TimerJobQueue");
        setDaemon(true);
        start();
    }

    public void run() {
        ArrayList<String> arrayList;
        while (true) {
            synchronized (this.b) {
                while (this.f1864a.size() == 0) {
                    try {
                        this.b.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                arrayList = new ArrayList<>(this.f1864a);
                this.f1864a.clear();
            }
            if (arrayList != null && !arrayList.isEmpty()) {
                for (String b2 : arrayList) {
                    b(b2);
                }
            } else {
                return;
            }
        }
    }

    public void a(String str) {
        synchronized (this.b) {
            this.f1864a.add(str);
            this.b.notify();
        }
    }

    private void b(String str) {
        TimerJob timerJob;
        try {
            Class<?> cls = Class.forName(str);
            if (cls.equals(BackgroundScanTimerJob.class)) {
                BackgroundScanTimerJob a2 = BackgroundScanTimerJob.a();
                if (a2 != null && a2.c_()) {
                    a2.d_();
                }
            } else if ((cls.newInstance() instanceof TimerJob) && (timerJob = (TimerJob) cls.newInstance()) != null && timerJob.c_()) {
                timerJob.d_();
            }
        } catch (ClassNotFoundException | Exception | IllegalAccessException | InstantiationException e) {
        }
    }
}
