package com.tencent.assistant.tagpage;

import android.view.View;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f2557a;
    final /* synthetic */ int b;
    final /* synthetic */ TagPageCardAdapter c;

    m(TagPageCardAdapter tagPageCardAdapter, r rVar, int i) {
        this.c = tagPageCardAdapter;
        this.f2557a = rVar;
        this.b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.TagPageCardAdapter, boolean):boolean
     arg types: [com.tencent.assistant.tagpage.TagPageCardAdapter, int]
     candidates:
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.TagPageCardAdapter, android.media.MediaPlayer):android.media.MediaPlayer
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.TagPageCardAdapter, int):java.lang.String
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.r, java.lang.String):void
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.r, int):void
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.TagPageCardAdapter, boolean):boolean */
    public void onClick(View view) {
        if (this.c.k != null && !this.c.k.isPlaying()) {
            boolean unused = this.c.t = true;
            this.c.a(this.f2557a, this.b);
            TagPageActivity.n = this.b;
        }
        this.c.a(this.c.a(this.b) + "_06", Constants.STR_EMPTY, 200);
    }
}
