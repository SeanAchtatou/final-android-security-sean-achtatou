package com.tencent.assistant.uninstall;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class b {
    private static List<String> c = new ArrayList();

    /* renamed from: a  reason: collision with root package name */
    public String f2582a;
    public String b;

    static {
        c.add("com.taobao.browser");
        c.add("com.snda.wifilocating");
    }

    public b() {
    }

    public b(String str, String str2) {
        this.f2582a = str;
        this.b = str2;
    }

    /* access modifiers changed from: private */
    public static List<b> b(Context context) {
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.BROWSABLE");
        intent.setData(Uri.parse(a.f2581a));
        for (ResolveInfo next : packageManager.queryIntentActivities(intent, 0)) {
            ApplicationInfo applicationInfo = next.activityInfo.applicationInfo;
            boolean z = Build.VERSION.SDK_INT >= 12 ? (applicationInfo.flags & 2097152) != 0 : false;
            if (applicationInfo.enabled && !z && !c.contains(applicationInfo.packageName)) {
                b bVar = new b(applicationInfo.packageName, next.activityInfo.name);
                if ((applicationInfo.flags & 1) == 0 && (applicationInfo.flags & 128) == 0 && !applicationInfo.packageName.equals("com.tencent.mtt")) {
                    arrayList.add(bVar);
                } else {
                    arrayList.add(0, bVar);
                }
            }
        }
        return arrayList;
    }
}
