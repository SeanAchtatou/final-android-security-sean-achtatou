package com.tencent.assistant.appbakcup;

import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.utils.ba;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class n implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static n f839a;
    private static int[] f = {EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_FAIL, EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_FAIL};
    private r b;
    private q c;
    private int d = -1;
    private int e = -1;

    private n() {
    }

    public static synchronized n a() {
        n nVar;
        synchronized (n.class) {
            if (f839a == null) {
                f839a = new n();
            }
            nVar = f839a;
        }
        return nVar;
    }

    public void b() {
        c();
        ba.a("BackupPushManagerHandler").postDelayed(new o(this), 5000);
    }

    private void c() {
        for (int addUIEventListener : f) {
            try {
                AstApp.i().k().addUIEventListener(addUIEventListener, this);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS /*1081*/:
                Bundle bundle = (Bundle) message.obj;
                if (bundle != null && bundle.getInt(AppConst.KEY_FROM_TYPE) != 10) {
                    if (message.arg1 != AppConst.LoginEgnineType.ENGINE_MOBILE_QQ.ordinal() || message.arg2 != 1) {
                        if (m.c() == 0 || m.b() == 0) {
                            d();
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS /*1124*/:
                if (message.arg1 == this.d) {
                    a(true);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_FAIL /*1125*/:
                if (message.arg1 == this.d) {
                    a(false);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_SUCCESS /*1126*/:
                if (message.arg1 == this.e) {
                    b(true);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_FAIL /*1127*/:
                if (message.arg1 == this.e) {
                    b(false);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(boolean z) {
        if (this.b != null && this.b.c() && this.b.d() && !m.g() && !m.h()) {
            if (this.c == null) {
                this.c = new q();
            }
            this.e = this.c.a(this.b.e());
        }
        this.b = null;
    }

    private void b(boolean z) {
        ArrayList<BackupApp> a2 = this.c.a();
        if (a2 == null || a2.size() >= 10) {
        }
        this.c = null;
    }

    private void d() {
        if (this.b == null) {
            this.b = new r();
        }
        this.d = this.b.a();
    }
}
