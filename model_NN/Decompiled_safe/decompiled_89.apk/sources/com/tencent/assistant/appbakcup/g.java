package com.tencent.assistant.appbakcup;

import android.view.View;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackupApplistDialog f835a;

    g(BackupApplistDialog backupApplistDialog) {
        this.f835a = backupApplistDialog;
    }

    public void onClick(View view) {
        if (this.f835a.i != null) {
            this.f835a.i.a(this.f835a.d.b());
        }
        k.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_APPLIST, "04_001", 0, STConst.ST_DEFAULT_SLOT, 200));
        this.f835a.dismiss();
    }
}
