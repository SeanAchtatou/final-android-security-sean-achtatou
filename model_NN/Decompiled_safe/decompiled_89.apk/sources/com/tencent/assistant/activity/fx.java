package com.tencent.assistant.activity;

import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.utils.br;
import java.util.ArrayList;

/* compiled from: ProGuard */
class fx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f593a;

    fx(SpaceCleanActivity spaceCleanActivity) {
        this.f593a = spaceCleanActivity;
    }

    public void run() {
        ArrayList q = this.f593a.B();
        if (SpaceScanManager.a().j()) {
            SpaceScanManager.a().a(this.f593a.X, q);
        } else {
            SpaceScanManager.a().a(this.f593a.aa, q);
        }
        br.g();
        br.b(0, SpaceCleanActivity.class);
    }
}
