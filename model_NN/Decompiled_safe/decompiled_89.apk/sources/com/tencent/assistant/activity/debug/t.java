package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.manager.cr;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class t implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f516a;

    t(DActivity dActivity) {
        this.f516a = dActivity;
    }

    public void onClick(View view) {
        if (cr.a().c()) {
            TemporaryThreadManager.get().start(new u(this));
        } else {
            Toast.makeText(this.f516a.getApplicationContext(), "没有临时root权限", 0).show();
        }
    }
}
