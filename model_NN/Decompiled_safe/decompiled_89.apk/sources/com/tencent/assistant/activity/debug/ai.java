package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import java.util.List;

/* compiled from: ProGuard */
class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f483a;

    ai(DActivity dActivity) {
        this.f483a = dActivity;
    }

    public void onClick(View view) {
        List<PluginInfo> a2 = d.b().a(1);
        if (a2 == null || a2.size() == 0) {
            Toast.makeText(this.f483a, "请先安装插件!", 1).show();
            return;
        }
        for (PluginInfo next : a2) {
            if (next != null && next.getPackageName().equals("p.com.tencent.android.qqdownloader")) {
                PluginProxyActivity.a(this.f483a, next.getPackageName(), next.getVersion(), "p.com.tencent.assistant.activity.PhotoBackupNewActivity", next.getInProcess(), null, null);
                return;
            }
        }
    }
}
