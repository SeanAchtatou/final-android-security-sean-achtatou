package com.tencent.assistant.activity;

import android.app.ActivityManager;
import android.content.pm.PackageManager;
import android.os.Message;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class fy implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f594a;

    fy(SpaceCleanActivity spaceCleanActivity) {
        this.f594a = spaceCleanActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.SpaceCleanActivity.d(com.tencent.assistant.activity.SpaceCleanActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.SpaceCleanActivity, int]
     candidates:
      com.tencent.assistant.activity.SpaceCleanActivity.d(com.tencent.assistant.activity.SpaceCleanActivity, long):void
      com.tencent.assistant.activity.SpaceCleanActivity.d(com.tencent.assistant.activity.SpaceCleanActivity, boolean):boolean */
    public void run() {
        boolean z = false;
        PackageManager packageManager = this.f594a.getPackageManager();
        XLog.d("miles", "loadData---run");
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.f594a.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            int i = 0;
            while (true) {
                if (i >= runningAppProcesses.size()) {
                    break;
                }
                String str = runningAppProcesses.get(i).pkgList[0];
                if (!AppConst.TENCENT_MOBILE_MANAGER_PKGNAME.equals(str) && !this.f594a.v.getPackageName().equals(str) && this.f594a.a(packageManager, str)) {
                    z = true;
                    break;
                }
                i++;
            }
        }
        boolean unused = this.f594a.M = z;
        boolean unused2 = this.f594a.N = true;
        Message.obtain(this.f594a.W, 29).sendToTarget();
    }
}
