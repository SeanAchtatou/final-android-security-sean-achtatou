package com.tencent.assistant.activity;

import com.tencent.assistant.component.listview.ManagerGeneralController;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class v extends ManagerGeneralController.BussinessInterface {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f657a;

    v(ApkMgrActivity apkMgrActivity) {
        this.f657a = apkMgrActivity;
    }

    public void doAction(Object obj, ManagerGeneralController.BussinessCallback bussinessCallback) {
        ManagerGeneralController.ResultInfo resultInfo = new ManagerGeneralController.ResultInfo();
        resultInfo.result = 0;
        resultInfo.data = (LocalApkInfo) obj;
        bussinessCallback.onFinished(resultInfo);
    }
}
