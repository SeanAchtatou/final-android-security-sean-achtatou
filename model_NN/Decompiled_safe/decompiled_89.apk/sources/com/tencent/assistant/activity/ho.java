package com.tencent.assistant.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class ho implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VideoActivityV2 f637a;

    private ho(VideoActivityV2 videoActivityV2) {
        this.f637a = videoActivityV2;
    }

    /* synthetic */ ho(VideoActivityV2 videoActivityV2, hl hlVar) {
        this(videoActivityV2);
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        XLog.i(VideoActivityV2.f387a, "[MyDownloaderListener] : url = " + str);
        XLog.i(VideoActivityV2.f387a, "[MyDownloaderListener] : userAgent = " + str2);
        XLog.i(VideoActivityV2.f387a, "[MyDownloaderListener] : contentDisposition = " + str3);
        XLog.i(VideoActivityV2.f387a, "[MyDownloaderListener] : mimetype = " + str4);
        XLog.i(VideoActivityV2.f387a, "[MyDownloaderListener] : contentLength = " + j);
        try {
            this.f637a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException | Exception e) {
        }
    }
}
