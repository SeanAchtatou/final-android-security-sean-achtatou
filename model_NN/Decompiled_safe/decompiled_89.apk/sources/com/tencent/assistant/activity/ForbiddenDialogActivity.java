package com.tencent.assistant.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.OneButtonDialogView;

/* compiled from: ProGuard */
public class ForbiddenDialogActivity extends BaseActivity {
    public View n;
    public RelativeLayout t;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b(getIntent());
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        b(intent);
    }

    private void b(Intent intent) {
        setContentView((int) R.layout.dialog_layout);
        getWindow().setLayout(-1, -2);
        this.n = findViewById(R.id.content_root);
        this.t = (RelativeLayout) findViewById(R.id.dialog_root);
        this.n.setOnClickListener(new cs(this));
        OneButtonDialogView oneButtonDialogView = new OneButtonDialogView(getBaseContext());
        oneButtonDialogView.setTitleAndMsg(true, getString(R.string.forbidden_title), getString(R.string.forbidden_msg));
        oneButtonDialogView.setButton(getString(R.string.forbidden_btn), new ct(this));
        b(oneButtonDialogView);
    }

    public void b(View view) {
        if (view != null) {
            view.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
            this.t.removeAllViews();
            view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fading_in));
            this.t.addView(view);
            return;
        }
        finish();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (getParent() != null) {
                return getParent().moveTaskToBack(true);
            }
            return moveTaskToBack(true);
        } else if (i != 82) {
            return super.onKeyDown(i, keyEvent);
        } else {
            return true;
        }
    }
}
