package com.tencent.assistant.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.HelperFeedbackAdapter;
import com.tencent.assistant.component.FeedbackInputView;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.callback.o;
import com.tencent.assistant.module.callback.x;
import com.tencent.assistant.module.cz;
import com.tencent.assistant.module.et;
import com.tencent.connect.common.Constants;
import java.util.Timer;

/* compiled from: ProGuard */
public class HelperFeedbackActivity extends BaseActivity implements ITXRefreshListViewListener {
    private View.OnClickListener A = new dr(this);
    private TextWatcher B = new ds(this);
    private View.OnKeyListener C = new dt(this);
    private View.OnClickListener D = new du(this);
    private o E = new dw(this);
    private x F = new dx(this);
    private SecondNavigationTitleView n;
    private LinearLayout t;
    /* access modifiers changed from: private */
    public FeedbackInputView u;
    private ListView v;
    /* access modifiers changed from: private */
    public View w;
    /* access modifiers changed from: private */
    public HelperFeedbackAdapter x;
    private et y;
    /* access modifiers changed from: private */
    public String z = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_feedback);
        v();
        if (cz.a().b() <= 0) {
            j();
        } else {
            this.u.clearFocus();
        }
        cz.a().register(this.E);
        cz.a().c();
        cz.a().d();
        this.y = new et();
        this.y.register(this.F);
    }

    private void v() {
        this.n = (SecondNavigationTitleView) findViewById(R.id.title_view);
        this.n.setActivityContext(this);
        this.n.setTitle(getString(R.string.help_feedback));
        this.n.hiddeSearch();
        this.t = (LinearLayout) findViewById(R.id.feedback_content_view);
        this.t.setVisibility(0);
        this.x = new HelperFeedbackAdapter(this, null);
        this.x.a(this.A);
        this.v = (ListView) findViewById(R.id.feedback_list);
        this.v.setAdapter((ListAdapter) this.x);
        this.v.setDivider(null);
        this.u = (FeedbackInputView) findViewById(R.id.feedback_input_view);
        this.u.addTextChangedListener(this.B);
        this.u.setOnKeyListener(this.C);
        this.u.setSendButtonClickListener(this.D);
        this.w = findViewById(R.id.feedback_line);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        cz.a().unregister(this.E);
        this.y.unregister(this.F);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        i();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 || motionEvent.getAction() == 2) {
            i();
        }
        return super.onTouchEvent(motionEvent);
    }

    public void i() {
        this.u.getInputView().setFocusable(true);
        this.u.getInputView().setFocusableInTouchMode(true);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive() && getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 2);
        }
    }

    public void j() {
        new Timer().schedule(new dv(this), 500);
    }

    /* access modifiers changed from: private */
    public void w() {
        String obj = this.u.getInputView().getText().toString();
        if (TextUtils.isEmpty(obj.trim())) {
            this.u.getInputView().setText(Constants.STR_EMPTY);
            this.u.getInputView().setSelection(0);
            Toast.makeText(this, (int) R.string.feedback_text_empty, 0).show();
        } else if (!obj.equals(this.z)) {
            this.z = obj;
            this.y.a(obj);
        }
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            cz.a().c();
        }
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
