package com.tencent.assistant.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.js.JsBridge;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.component.TxWebView;
import com.tencent.assistantv2.component.TxWebViewContainer;
import com.tencent.assistantv2.component.dz;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class VideoActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f386a = VideoActivity.class.getSimpleName();
    private TxWebViewContainer b;
    private Context c = null;
    private String d = null;
    private String e = null;
    private String f = Constants.STR_EMPTY;
    private JsBridge g = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = this;
        setContentView((int) R.layout.video_detail_layout);
        a(getIntent());
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        a(intent);
    }

    private void a(Intent intent) {
        if (intent.hasExtra("com.tencent.assistant.VIDEO_URL")) {
            String stringExtra = intent.getStringExtra("com.tencent.assistant.VIDEO_URL");
            XLog.i(f386a, "tempUrl : " + stringExtra);
            if (!TextUtils.isEmpty(stringExtra) && stringExtra.equals(this.d)) {
                XLog.i(f386a, "*** handleIntent return ***");
                return;
            }
        }
        b(intent);
        c();
        dz dzVar = new dz();
        dzVar.c = true;
        dzVar.b = 1;
        this.b.a(dzVar);
        b();
    }

    private void b() {
        if (this.d == null || TextUtils.isEmpty(this.d)) {
            this.b.a("file:///android_asset/video_player.html");
            return;
        }
        XLog.i(f386a, "[VideoActivity] ---> url : " + this.d);
        this.b.a(this.d);
    }

    private void b(Intent intent) {
        if (intent.hasExtra("com.tencent.assistant.VIDEO_URL")) {
            this.d = intent.getStringExtra("com.tencent.assistant.VIDEO_URL");
        }
        if (intent.hasExtra("com.tencent.assistant.VID")) {
            this.e = intent.getStringExtra("com.tencent.assistant.VID");
        }
        if (intent.hasExtra("com.tencent.assistant.videoSrc")) {
            this.f = intent.getStringExtra("com.tencent.assistant.videoSrc");
            XLog.i(f386a, "[initData] ---> videoSrc : " + this.f);
        }
    }

    private void c() {
        this.b = (TxWebViewContainer) findViewById(R.id.txwebviewcontainer);
        this.b.b(true);
        this.b.o();
        TxWebView p = this.b.p();
        if (p != null) {
            p.setWebViewClientExtension(new hk(this));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        XLog.i(f386a, "[onResume] ---> EndTime : " + System.currentTimeMillis());
        if (this.b != null) {
            this.b.a();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        XLog.i(f386a, "*** onPause ***");
        if (this.b != null) {
            this.b.b();
        }
        if ("1".equals(this.f)) {
            String str = "javascript:var param={site:1,vid:window['VideoData'].vid};var data=SohutvJSBridge.getHistory(param);data[0].videoSrc=" + this.f + ";window.location.href='jsb://saveData/-1/callback?key=video_watch_time&value='" + "+encodeURIComponent(JSON.stringify(data));";
            XLog.i(f386a, "jsPlayProgress : " + str);
            this.b.a(str);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        XLog.i(f386a, "*** onDestroy ***");
        if (this.b != null) {
            this.b.c();
        }
        super.onDestroy();
    }

    public void onDetachedFromWindow() {
        XLog.i(f386a, "*** onDetachedFromWindow ***");
        if (this.b != null) {
            this.b.d();
        }
        super.onDetachedFromWindow();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4 || keyEvent.getAction() != 0) {
            return super.dispatchKeyEvent(keyEvent);
        }
        finish();
        XLog.i(f386a, "*** dispatchKeyEvent ***");
        return true;
    }
}
