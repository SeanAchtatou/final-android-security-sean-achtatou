package com.tencent.assistant.activity;

import android.os.Handler;
import android.view.ViewTreeObserver;

/* compiled from: ProGuard */
class ez implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ReportActivity f568a;

    ez(ReportActivity reportActivity) {
        this.f568a = reportActivity;
    }

    public void onGlobalLayout() {
        this.f568a.F.scrollTo(0, this.f568a.F.getHeight());
        if (this.f568a.F.getRootView().getHeight() - this.f568a.F.getHeight() > 100) {
            new Handler().postDelayed(new fa(this), 300);
        }
    }
}
