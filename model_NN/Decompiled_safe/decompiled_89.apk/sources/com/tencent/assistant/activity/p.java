package com.tencent.assistant.activity;

import android.view.View;
import android.widget.ExpandableListView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class p implements ExpandableListView.OnChildClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f647a;

    p(ApkMgrActivity apkMgrActivity) {
        this.f647a = apkMgrActivity;
    }

    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        LocalApkInfo localApkInfo = (LocalApkInfo) expandableListView.getExpandableListAdapter().getChild(i, i2);
        if (localApkInfo != null) {
            String str = STConst.ST_DEFAULT_SLOT;
            if (this.f647a.w != null) {
                str = this.f647a.w.getListSlotId(i, i2);
            }
            STInfoV2 sTInfoV2 = new STInfoV2(this.f647a.f(), str, this.f647a.p(), STConst.ST_DEFAULT_SLOT, 200);
            if (localApkInfo != null) {
                sTInfoV2.appId = localApkInfo.mAppid;
            }
            k.a(sTInfoV2);
        }
        return false;
    }
}
