package com.tencent.assistant.activity;

import android.view.KeyEvent;
import android.view.View;

/* compiled from: ProGuard */
class dt implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f535a;

    dt(HelperFeedbackActivity helperFeedbackActivity) {
        this.f535a = helperFeedbackActivity;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 66 || 1 != keyEvent.getAction()) {
            return false;
        }
        this.f535a.i();
        return false;
    }
}
