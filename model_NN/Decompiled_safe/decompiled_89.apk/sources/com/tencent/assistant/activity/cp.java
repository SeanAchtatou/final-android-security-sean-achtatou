package com.tencent.assistant.activity;

import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistantv2.component.ea;

/* compiled from: ProGuard */
class cp implements ea {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ExternalCallActivity f457a;

    cp(ExternalCallActivity externalCallActivity) {
        this.f457a = externalCallActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, java.lang.String):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(com.tencent.assistantv2.component.SecondNavigationTitleViewV5, boolean):boolean
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, int):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(java.lang.String, android.text.TextUtils$TruncateAt):void
      com.tencent.assistantv2.component.SecondNavigationTitleViewV5.a(boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ExternalCallActivity.a(com.tencent.assistant.activity.ExternalCallActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.ExternalCallActivity, int]
     candidates:
      com.tencent.assistant.activity.BrowserActivity.a(int, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.Object):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(int, int):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.ExternalCallActivity.a(com.tencent.assistant.activity.ExternalCallActivity, boolean):boolean */
    public void a(int i, int i2, int i3, int i4) {
        if (this.f457a.C && i2 > i4) {
            this.f457a.A.startAnimation(AnimationUtils.loadAnimation(this.f457a.n, R.anim.float_banner_header_out_fade));
            this.f457a.A.setVisibility(8);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.addRule(3, R.id.browser_header_view);
            this.f457a.t.setLayoutParams(layoutParams);
            this.f457a.u.a(true, (int) R.anim.float_banner_header_in_fade);
            boolean unused = this.f457a.C = false;
            this.f457a.a((String) null, 100, 2);
        }
    }
}
