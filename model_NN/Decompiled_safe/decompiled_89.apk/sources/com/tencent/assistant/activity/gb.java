package com.tencent.assistant.activity;

import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class gb implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f598a;

    gb(SpaceCleanActivity spaceCleanActivity) {
        this.f598a = spaceCleanActivity;
    }

    public void a() {
        XLog.d("miles", "SpaceCleanActivity >> onBindTmsServiceSuccess..");
        if (this.f598a.L) {
            this.f598a.W.sendEmptyMessage(1);
        }
    }

    public void b() {
        XLog.d("miles", "SpaceCleanActivity >> onBindTmsServiceFailed..");
        if (this.f598a.L) {
            this.f598a.W.sendEmptyMessage(-2);
        }
    }

    public void c() {
        XLog.d("miles", "SpaceCleanActivity >> onTmsServiceDisconnected.");
        this.f598a.y();
    }
}
