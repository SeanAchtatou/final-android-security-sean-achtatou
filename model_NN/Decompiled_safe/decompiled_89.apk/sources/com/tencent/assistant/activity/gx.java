package com.tencent.assistant.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

/* compiled from: ProGuard */
class gx extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f620a;

    private gx(StartPopWindowActivity startPopWindowActivity) {
        this.f620a = startPopWindowActivity;
    }

    /* synthetic */ gx(StartPopWindowActivity startPopWindowActivity, gm gmVar) {
        this(startPopWindowActivity);
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.net.wifi.STATE_CHANGE")) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            this.f620a.u.postDelayed(new gy(this), 500);
        }
    }
}
