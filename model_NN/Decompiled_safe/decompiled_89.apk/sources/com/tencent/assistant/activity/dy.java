package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.List;

/* compiled from: ProGuard */
class dy implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstalledAppManagerActivity f540a;

    dy(InstalledAppManagerActivity installedAppManagerActivity) {
        this.f540a = installedAppManagerActivity;
    }

    public void run() {
        this.f540a.n.registerApkResCallback(this.f540a.v);
        List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
        if (localApkInfos != null && localApkInfos.size() > 0) {
            Message obtainMessage = this.f540a.V.obtainMessage();
            obtainMessage.obj = localApkInfos;
            obtainMessage.what = 10701;
            this.f540a.V.removeMessages(10701);
            this.f540a.V.sendMessage(obtainMessage);
        }
    }
}
