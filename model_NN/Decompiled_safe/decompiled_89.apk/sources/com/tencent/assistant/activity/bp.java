package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.component.spaceclean.RubbishItemView;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class bp extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileCleanActivity f430a;

    bp(BigFileCleanActivity bigFileCleanActivity) {
        this.f430a = bigFileCleanActivity;
    }

    public void onTMAClick(View view) {
        if (this.f430a.w.getFooterViewEnable()) {
            this.f430a.y.b();
            this.f430a.z();
            this.f430a.A();
            RubbishItemView.isDeleting = true;
            this.f430a.w.updateContent(this.f430a.getString(R.string.apkmgr_is_deleting));
            this.f430a.w.setFooterViewEnable(false);
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f430a.n, 200);
        buildSTInfo.scene = STConst.ST_PAGE_BIG_FILE_CLEAN_STEWARD;
        buildSTInfo.slotId = "05_001";
        return buildSTInfo;
    }
}
