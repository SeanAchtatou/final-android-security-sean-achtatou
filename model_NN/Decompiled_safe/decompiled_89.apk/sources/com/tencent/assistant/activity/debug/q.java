package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Button;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
class q implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f513a;

    q(DActivity dActivity) {
        this.f513a = dActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public void onClick(View view) {
        boolean z = false;
        boolean a2 = m.a().a("test_guide", false);
        m a3 = m.a();
        if (!a2) {
            z = true;
        }
        a3.b("test_guide", Boolean.valueOf(z));
        if (view instanceof Button) {
            ((Button) view).setText("引导功能：" + (!a2 ? "开" : "关"));
        }
    }
}
