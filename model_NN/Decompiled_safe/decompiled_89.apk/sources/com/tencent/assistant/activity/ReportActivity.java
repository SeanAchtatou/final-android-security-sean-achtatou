package com.tencent.assistant.activity;

import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.module.callback.d;
import com.tencent.assistant.module.x;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ReportActivity extends BaseActivity implements d {
    /* access modifiers changed from: private */
    public TextView A;
    /* access modifiers changed from: private */
    public TextView B;
    /* access modifiers changed from: private */
    public EditText C;
    /* access modifiers changed from: private */
    public long D;
    /* access modifiers changed from: private */
    public long E;
    /* access modifiers changed from: private */
    public ScrollView F;
    /* access modifiers changed from: private */
    public x G = new x();
    private View.OnClickListener H = new fc(this);
    TextWatcher n = new fb(this);
    /* access modifiers changed from: private */
    public TextView t;
    /* access modifiers changed from: private */
    public TextView u;
    /* access modifiers changed from: private */
    public TextView v;
    /* access modifiers changed from: private */
    public TextView w;
    /* access modifiers changed from: private */
    public TextView x;
    /* access modifiers changed from: private */
    public TextView y;
    /* access modifiers changed from: private */
    public TextView z;

    public int f() {
        return STConst.ST_PAGE_APP_DETAIL_MORE_REPORT;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.report_view);
        this.G.register(this);
        i();
        j();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.G.unregister(this);
        super.onDestroy();
    }

    private void i() {
        this.D = getIntent().getLongExtra("appid", 0);
        this.E = getIntent().getLongExtra("apkid", 0);
    }

    private void j() {
        SecondNavigationTitleViewV5 secondNavigationTitleViewV5 = (SecondNavigationTitleViewV5) findViewById(R.id.header);
        secondNavigationTitleViewV5.a(this);
        secondNavigationTitleViewV5.d();
        secondNavigationTitleViewV5.b(getString(R.string.inform));
        this.F = (ScrollView) findViewById(R.id.scroller);
        this.F.getViewTreeObserver().addOnGlobalLayoutListener(new ez(this));
        this.t = (TextView) findViewById(R.id.submit);
        this.t.setOnClickListener(this.H);
        this.u = (TextView) findViewById(R.id.checkbox_downlaod);
        this.v = (TextView) findViewById(R.id.checkbox_install);
        this.w = (TextView) findViewById(R.id.checkbox_version);
        this.x = (TextView) findViewById(R.id.checkbox_plugin);
        this.y = (TextView) findViewById(R.id.checkbox_fee);
        this.z = (TextView) findViewById(R.id.checkbox_virus);
        this.A = (TextView) findViewById(R.id.checkbox_right);
        this.B = (TextView) findViewById(R.id.checkbox_private);
        this.u.setOnClickListener(this.H);
        this.v.setOnClickListener(this.H);
        this.w.setOnClickListener(this.H);
        this.y.setOnClickListener(this.H);
        this.x.setOnClickListener(this.H);
        this.z.setOnClickListener(this.H);
        this.A.setOnClickListener(this.H);
        this.B.setOnClickListener(this.H);
        this.C = (EditText) findViewById(R.id.content);
        this.C.addTextChangedListener(this.n);
    }

    /* access modifiers changed from: private */
    public boolean v() {
        return this.u.isSelected() || this.v.isSelected() || this.w.isSelected() || this.y.isSelected() || this.x.isSelected() || this.z.isSelected() || this.A.isSelected() || this.B.isSelected() || this.C.getText().toString().length() != 0;
    }

    /* access modifiers changed from: private */
    public void w() {
        if (this.y.isSelected() || this.x.isSelected() || this.z.isSelected() || this.B.isSelected()) {
            this.v.setEnabled(false);
            this.u.setEnabled(false);
            this.v.setSelected(false);
            this.u.setSelected(false);
            this.t.setEnabled(true);
            return;
        }
        this.v.setEnabled(true);
        this.u.setEnabled(true);
    }

    /* access modifiers changed from: private */
    public byte[] x() {
        ArrayList arrayList = new ArrayList();
        if (this.u.isSelected()) {
            arrayList.add((byte) 1);
        }
        if (this.v.isSelected()) {
            arrayList.add((byte) 2);
        }
        if (this.w.isSelected()) {
            arrayList.add((byte) 5);
        }
        if (this.x.isSelected()) {
            arrayList.add((byte) 8);
        }
        if (this.y.isSelected()) {
            arrayList.add((byte) 6);
        }
        if (this.w.isSelected()) {
            arrayList.add((byte) 5);
        }
        if (this.A.isSelected()) {
            arrayList.add((byte) 9);
        }
        if (this.B.isSelected()) {
            arrayList.add((byte) 10);
        }
        byte[] bArr = new byte[arrayList.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= arrayList.size()) {
                return bArr;
            }
            bArr[i2] = ((Byte) arrayList.get(i2)).byteValue();
            i = i2 + 1;
        }
    }

    public void a(int i, int i2, long j, long j2) {
        if (i2 == 0 && this.D == j && this.E == j2) {
            Toast.makeText(this, getString(R.string.report_success), 0).show();
        } else if (i2 == 1) {
            Toast.makeText(this, getString(R.string.repeat_report), 0).show();
        } else {
            Toast.makeText(this, getString(R.string.report_fail), 0).show();
        }
    }
}
