package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.module.et;
import com.tencent.assistant.utils.t;

/* compiled from: ProGuard */
class b implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AboutActivity f415a;

    b(AboutActivity aboutActivity) {
        this.f415a = aboutActivity;
    }

    public boolean onLongClick(View view) {
        if (this.f415a.z == null) {
            et unused = this.f415a.z = new et();
            this.f415a.z.register(new c(this));
        }
        String unused2 = this.f415a.A = "RANDOM:" + this.f415a.b(6) + "\n" + "IMEI:" + t.g();
        this.f415a.z.a(this.f415a.A);
        return true;
    }
}
