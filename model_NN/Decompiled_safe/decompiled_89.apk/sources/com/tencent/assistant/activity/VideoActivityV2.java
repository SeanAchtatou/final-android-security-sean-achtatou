package com.tencent.assistant.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ZoomButtonsController;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.apilevel.b;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.js.JsBridge;
import com.tencent.assistant.net.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.r;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.FixedWebViewV2;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class VideoActivityV2 extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f387a = VideoActivityV2.class.getSimpleName();
    private static ArrayList<String> k = new ArrayList<>();
    private static ArrayList<String> l = new ArrayList<>();
    private FrameLayout b = null;
    /* access modifiers changed from: private */
    public WebView c = null;
    /* access modifiers changed from: private */
    public ProgressBar d = null;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage e = null;
    /* access modifiers changed from: private */
    public Context f = null;
    private String g = null;
    private String h = null;
    private String i = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public JsBridge j = null;

    static {
        k.add("MT870");
        k.add("XT910");
        k.add("XT928");
        k.add("MT917");
        k.add("Lenovo A60");
        l.add("GT-N7100");
        l.add("GT-N7102");
        l.add("GT-N7105");
        l.add("GT-N7108");
        l.add("GT-N7108D");
        l.add("GT-N7109");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f = this;
        setContentView((int) R.layout.video_detail_layout_v2);
        a(getIntent());
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        a(intent);
    }

    private void a(Intent intent) {
        if (intent.hasExtra("com.tencent.assistant.VIDEO_URL")) {
            String stringExtra = intent.getStringExtra("com.tencent.assistant.VIDEO_URL");
            XLog.i(f387a, "tempUrl : " + stringExtra);
            if (!TextUtils.isEmpty(stringExtra) && stringExtra.equals(this.g)) {
                XLog.i(f387a, "*** handleIntent return ***");
                return;
            }
        }
        if (intent.hasExtra("from_detail") && "1".equals(intent.getStringExtra("from_detail"))) {
            a();
        }
        b(intent);
        c();
        e();
        d();
        a(getString(R.string.webview_player_toast_text));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        k.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL_VIDEO_PALY, STConst.ST_DEFAULT_SLOT, STConst.ST_PAGE_APP_DETAIL, STConst.ST_DEFAULT_SLOT, 100));
    }

    private void b(Intent intent) {
        if (intent.hasExtra("com.tencent.assistant.VIDEO_URL")) {
            this.g = intent.getStringExtra("com.tencent.assistant.VIDEO_URL");
        }
        if (intent.hasExtra("com.tencent.assistant.VID")) {
            this.h = intent.getStringExtra("com.tencent.assistant.VID");
        }
        if (intent.hasExtra("com.tencent.assistant.videoSrc")) {
            this.i = intent.getStringExtra("com.tencent.assistant.videoSrc");
            XLog.i(f387a, "[initData] ---> videoSrc : " + this.i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.FixedWebViewV2.<init>(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.assistantv2.component.FixedWebViewV2.<init>(android.content.Context, android.util.AttributeSet):void
      com.tencent.assistantv2.component.FixedWebViewV2.<init>(android.content.Context, boolean):void */
    private void c() {
        this.b = (FrameLayout) findViewById(R.id.video_detail_layout);
        this.d = (ProgressBar) findViewById(R.id.loading_view);
        this.d.setVisibility(8);
        this.c = new FixedWebViewV2(this.f, true);
        this.c.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.b.addView(this.c);
        this.j = new JsBridge((Activity) this.f, this.c);
    }

    private void d() {
        if (this.c != null) {
            int d2 = r.d();
            this.c.setScrollbarFadingEnabled(true);
            this.c.requestFocus();
            this.c.setFocusable(true);
            this.c.setFocusableInTouchMode(true);
            this.c.setScrollBarStyle(0);
            WebSettings settings = this.c.getSettings();
            settings.setBuiltInZoomControls(false);
            settings.setUserAgentString(settings.getUserAgentString() + "/" + "qqdownloader/" + 3 + "/apiLevel/" + Build.VERSION.SDK_INT);
            settings.setCacheMode(-1);
            settings.setJavaScriptEnabled(true);
            Class<?> cls = settings.getClass();
            try {
                Method method = cls.getMethod("setPluginsEnabled", Boolean.TYPE);
                if (method != null) {
                    method.invoke(settings, true);
                }
            } catch (NoSuchMethodException e2) {
                e2.printStackTrace();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            try {
                Method method2 = settings.getClass().getMethod("setDomStorageEnabled", Boolean.TYPE);
                if (method2 != null) {
                    method2.invoke(settings, true);
                }
            } catch (SecurityException e3) {
                e3.printStackTrace();
            } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException e4) {
            }
            try {
                Method method3 = this.c.getClass().getMethod("removeJavascriptInterface", String.class);
                if (method3 != null) {
                    method3.invoke(this.c, "searchBoxJavaBridge_");
                }
            } catch (Exception e5) {
                e5.printStackTrace();
            }
            settings.setAppCachePath(FileUtil.getWebViewCacheDir());
            settings.setDatabasePath(FileUtil.getWebViewCacheDir());
            settings.setDatabaseEnabled(true);
            settings.setAppCacheEnabled(true);
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            if (Build.VERSION.SDK_INT >= 8 && Build.VERSION.SDK_INT < 18) {
                settings.setPluginState(WebSettings.PluginState.ON);
            }
            settings.setSavePassword(false);
            if (f()) {
                settings.setUseWideViewPort(true);
                if (d2 >= 7) {
                    try {
                        settings.getClass().getMethod("setLoadWithOverviewMode", Boolean.TYPE).invoke(settings, true);
                    } catch (Exception e6) {
                    }
                }
                if (t.f2723a) {
                    if (d2 < 11) {
                        try {
                            Field declaredField = WebView.class.getDeclaredField("mZoomButtonsController");
                            declaredField.setAccessible(true);
                            ZoomButtonsController zoomButtonsController = new ZoomButtonsController(this.c);
                            zoomButtonsController.getZoomControls().setVisibility(8);
                            declaredField.set(this.c, zoomButtonsController);
                        } catch (Exception e7) {
                        }
                    } else {
                        try {
                            this.c.getSettings().getClass().getMethod("setDisplayZoomControls", Boolean.TYPE).invoke(this.c.getSettings(), false);
                        } catch (Exception e8) {
                        }
                    }
                }
            }
            if (d2 < 14 || !g()) {
                this.c.setWebChromeClient(new hp(this, null));
            } else {
                this.c.setWebChromeClient(new b(new hl(this)));
            }
            this.c.setWebViewClient(new hq(this, null));
            this.c.setDownloadListener(new ho(this, null));
            if (TextUtils.isEmpty(this.g) || this.g.trim().toLowerCase().startsWith("file:")) {
                this.c.loadUrl("file:///android_asset/video_player.html");
                return;
            }
            XLog.i(f387a, "[VideoActivity] ---> url : " + this.g);
            this.c.loadUrl(this.g);
        }
    }

    private void e() {
        this.e = (NormalErrorRecommendPage) findViewById(R.id.error_page_view);
        this.e.setButtonClickListener(new hm(this));
    }

    private void b(boolean z) {
        if (z) {
            this.e.setVisibility(4);
            this.c.setVisibility(0);
            return;
        }
        this.e.setVisibility(0);
        this.c.setVisibility(4);
    }

    public void a(boolean z) {
        if (!z) {
            b(true);
        } else if (!c.a()) {
            a(30);
        } else {
            a(20);
        }
    }

    private void a(int i2) {
        b(false);
        this.e.setErrorType(i2);
        this.e.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        XLog.i(f387a, "[onResume] ---> EndTime : " + System.currentTimeMillis());
        try {
            if (this.c != null) {
                this.c.getClass().getMethod("onResume", new Class[0]).invoke(this.c, null);
            }
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
        } catch (SecurityException e3) {
            e3.printStackTrace();
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
        } catch (InvocationTargetException e5) {
            e5.printStackTrace();
        } catch (NoSuchMethodException e6) {
            e6.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        XLog.i(f387a, "*** onPause ***");
        try {
            if (this.c != null) {
                this.c.getClass().getMethod("onPause", new Class[0]).invoke(this.c, null);
                if ("1".equals(this.i)) {
                    String str = "javascript:var param={site:1,vid:window['VideoData'].vid};var data=SohutvJSBridge.getHistory(param);data[0].videoSrc=" + this.i + ";window.location.href='jsb://saveData/-1/callback?key=video_watch_time&value='" + "+encodeURIComponent(JSON.stringify(data));";
                    XLog.i(f387a, "jsPlayProgress : " + str);
                    this.c.loadUrl(str);
                }
            }
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
        } catch (SecurityException e3) {
            e3.printStackTrace();
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
        } catch (InvocationTargetException e5) {
            e5.printStackTrace();
        } catch (NoSuchMethodException e6) {
            e6.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        XLog.i(f387a, "*** onDestroy ***");
        if (this.j != null) {
            this.j.recycle();
            this.j = null;
        }
        if (this.e != null) {
            this.e.destory();
            this.e = null;
        }
        super.onDestroy();
    }

    public void onDetachedFromWindow() {
        XLog.i(f387a, "*** onDetachedFromWindow ***");
        if (this.c != null) {
            this.b.removeAllViews();
            try {
                this.c.loadUrl("about:blank");
                this.c.stopLoading();
                this.c.setVisibility(8);
                this.c.destroy();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.c = null;
        }
        super.onDetachedFromWindow();
    }

    private boolean f() {
        String str = Build.MODEL;
        return !str.contains("vivo") && !k.contains(str);
    }

    private boolean g() {
        String str = Build.MODEL;
        XLog.i(f387a, "[isSupportTencentVideoFullScreenPlay] : " + str);
        return !l.contains(str);
    }

    private SharedPreferences h() {
        return AstApp.i().getSharedPreferences("videoplayer", 0);
    }

    private boolean i() {
        boolean z = h().getBoolean("first_play", true);
        if (z) {
            h().edit().putBoolean("first_play", false).commit();
        }
        return z;
    }

    private void a(String str) {
        if (r.d() < 14 && i()) {
            Toast.makeText(this.f, str, 1).show();
            ba.a().postDelayed(new hn(this, str), 2000);
        }
    }
}
