package com.tencent.assistant.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.a;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class CommentReplyActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public TextView n;
    /* access modifiers changed from: private */
    public EditText t;
    /* access modifiers changed from: private */
    public a u = a.a();
    /* access modifiers changed from: private */
    public CommentDetail v;
    /* access modifiers changed from: private */
    public SimpleAppModel w;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.comment_reply_box);
        j();
        v();
    }

    public int f() {
        return STConst.ST_PAGE_APP_DETAIL_COMMENT;
    }

    private void j() {
        Intent intent = getIntent();
        this.v = (CommentDetail) intent.getSerializableExtra("comment");
        this.w = (SimpleAppModel) intent.getParcelableExtra("simpleAppModel");
    }

    private void v() {
        this.n = (TextView) findViewById(R.id.reply_btn);
        this.t = (EditText) findViewById(R.id.input_box);
        if (!(this.v == null || this.v.k == null || this.v.k.size() <= 0)) {
            this.t.setHint("回复 " + this.v.k.get(this.v.k.size() - 1).d);
            this.t.setHintTextColor(getResources().getColor(R.color.comment_nick_name_user));
        }
        this.n.setOnClickListener(new by(this));
        this.t.addTextChangedListener(new bz(this));
    }

    public STInfoV2 i() {
        STInfoV2 sTInfoV2 = null;
        if (this.w != null) {
            sTInfoV2 = STInfoBuilder.buildSTInfo(this, this.w, STConst.ST_DEFAULT_SLOT, 100, null);
            if (this.w.y != null) {
                sTInfoV2.recommendId = this.w.y;
            }
        }
        return sTInfoV2;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
