package com.tencent.assistant.activity;

import android.app.Activity;
import android.webkit.WebView;
import com.tencent.assistant.activity.BrowserActivity;

/* compiled from: ProGuard */
class hl implements BrowserActivity.WebChromeClientListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VideoActivityV2 f634a;

    hl(VideoActivityV2 videoActivityV2) {
        this.f634a = videoActivityV2;
    }

    public Activity getActivity() {
        return this.f634a;
    }

    public void onProgressChanged(WebView webView, int i) {
        this.f634a.d.setProgress(i);
    }

    public void onReceivedTitle(WebView webView, String str) {
        this.f634a.setTitle(str);
    }
}
