package com.tencent.assistant.activity;

import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.manager.DownloadProxy;

/* compiled from: ProGuard */
class el extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f554a;
    final /* synthetic */ MobileManagerInstallActivity b;

    el(MobileManagerInstallActivity mobileManagerInstallActivity, DownloadInfo downloadInfo) {
        this.b = mobileManagerInstallActivity;
        this.f554a = downloadInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public void onLeftBtnClick() {
        DownloadProxy.a().b(this.f554a.downloadTicket, false);
    }

    public void onRightBtnClick() {
        this.f554a.response = null;
        a.a().a(this.f554a);
        Toast.makeText(this.b, this.b.getResources().getString(R.string.down_add_tips), 0).show();
    }

    public void onCancell() {
    }
}
