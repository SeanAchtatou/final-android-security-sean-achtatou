package com.tencent.assistant.activity;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* compiled from: ProGuard */
public class at extends Animation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBackupActivity f408a;
    private final float b;
    private final float c;
    private final float d;
    private final float e;
    private final float f;
    private final boolean g;
    private Camera h;

    public at(AppBackupActivity appBackupActivity, float f2, float f3, float f4, float f5, float f6, boolean z) {
        this.f408a = appBackupActivity;
        this.b = f2;
        this.c = f3;
        this.d = f4;
        this.e = f5;
        this.f = f6;
        this.g = z;
    }

    public void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.h = new Camera();
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        float f3 = this.b;
        float f4 = f3 + ((this.c - f3) * f2);
        float f5 = this.d;
        float f6 = this.e;
        Camera camera = this.h;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.g) {
            camera.translate(0.0f, 0.0f, this.f * f2);
        } else {
            camera.translate(0.0f, 0.0f, this.f * (1.0f - f2));
        }
        camera.rotateX(f4);
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f5, -f6);
        matrix.postTranslate(f5, f6);
        super.applyTransformation(f2, transformation);
    }
}
