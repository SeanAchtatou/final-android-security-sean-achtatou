package com.tencent.assistant.guessfavor;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.protocol.jce.RecommendAppInfoEx;
import java.util.List;

/* compiled from: ProGuard */
public interface i extends ActionCallback {
    void a(int i, int i2);

    void a(int i, int i2, List<RecommendAppInfoEx> list, List<SimpleAppModel> list2);
}
