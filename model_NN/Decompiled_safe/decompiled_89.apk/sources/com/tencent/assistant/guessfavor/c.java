package com.tencent.assistant.guessfavor;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.listener.OnTMAItemExClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class c extends OnTMAItemExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuessFavorActivity f1336a;

    c(GuessFavorActivity guessFavorActivity) {
        this.f1336a = guessFavorActivity;
    }

    public void onTMAItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        SimpleAppModel a2 = this.f1336a.v.getItem(i);
        if (a2 != null) {
            Intent intent = new Intent(this.f1336a, AppDetailActivityV5.class);
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1336a.f());
            intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, a2);
            intent.putExtra("statInfo", new StatInfo(a2.b, STConst.ST_PAGE_DOWNLOAD, 0, null, 0));
            this.f1336a.startActivity(intent);
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1336a.n, 200);
        buildSTInfo.slotId = "03_" + ct.a(getPosition() + 1);
        buildSTInfo.status = "_01";
        SimpleAppModel a2 = this.f1336a.v.getItem(getPosition());
        if (a2 != null) {
            buildSTInfo.recommendId = a2.y;
            buildSTInfo.appId = a2.f1634a;
        }
        return buildSTInfo;
    }
}
