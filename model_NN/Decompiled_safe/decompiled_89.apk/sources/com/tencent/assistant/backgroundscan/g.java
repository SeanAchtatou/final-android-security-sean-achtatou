package com.tencent.assistant.backgroundscan;

import com.tencent.assistant.db.table.f;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f856a;

    g(BackgroundScanManager backgroundScanManager) {
        this.f856a = backgroundScanManager;
    }

    public void run() {
        List<BackgroundScan> b = f.a().b();
        if (this.f856a.f()) {
            this.f856a.h();
        }
        String str = Constants.STR_EMPTY;
        for (BackgroundScan backgroundScan : b) {
            str = str + backgroundScan.toString() + " | ";
        }
        n.a().a("b_new_scan_push_scan_ret", (byte) 0, null, null, str, 0);
        n.a().a("b_new_scan_push_scan_ret_satisfy", (byte) 0, null, null, this.f856a.a(b).toString(), 0);
    }
}
