package com.tencent.assistant.floatingwindow;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.assistant.floatingwindow.WaveView;

/* compiled from: ProGuard */
final class x implements Parcelable.Creator<WaveView.SavedState> {
    x() {
    }

    /* renamed from: a */
    public WaveView.SavedState createFromParcel(Parcel parcel) {
        return new WaveView.SavedState(parcel);
    }

    /* renamed from: a */
    public WaveView.SavedState[] newArray(int i) {
        return new WaveView.SavedState[i];
    }
}
