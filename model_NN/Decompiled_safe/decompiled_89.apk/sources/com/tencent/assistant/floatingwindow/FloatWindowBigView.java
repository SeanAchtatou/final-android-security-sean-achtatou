package com.tencent.assistant.floatingwindow;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.SettingActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.activity.UpdateListActivity;
import com.tencent.assistant.component.RollTextView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.module.u;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.br;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import java.text.DecimalFormat;

/* compiled from: ProGuard */
public class FloatWindowBigView extends LinearLayout implements View.OnClickListener, View.OnTouchListener, UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    public static int f1278a;
    public static int b;
    private View c;
    private View d;
    private View e;
    private View f;
    private TextView g;
    private View h;
    private View i;
    private View j;
    /* access modifiers changed from: private */
    public RollTextView k;
    /* access modifiers changed from: private */
    public TextView l;
    /* access modifiers changed from: private */
    public WaveView m;
    /* access modifiers changed from: private */
    public int n = 75;
    /* access modifiers changed from: private */
    public int o = 75;
    private int p;
    /* access modifiers changed from: private */
    public boolean q = true;
    private long r;
    /* access modifiers changed from: private */
    public View s;
    private Animation t;
    private long u = 0;
    private long v = 0;

    public FloatWindowBigView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate((int) R.layout.float_window_big, this);
        setGravity(17);
        setOnTouchListener(this);
        this.c = findViewById(R.id.big_window_layout);
        f1278a = this.c.getLayoutParams().width;
        b = this.c.getLayoutParams().height;
        this.d = this.c.findViewById(R.id.ll_yyb_entrance);
        this.e = this.c.findViewById(R.id.ll_setting_entrance);
        this.j = this.c.findViewById(R.id.rl_accelerate_circle);
        this.k = (RollTextView) this.c.findViewById(R.id.mem_percent);
        this.l = (TextView) this.c.findViewById(R.id.header_size_tip);
        this.f = this.c.findViewById(R.id.rl_app_update_entrance);
        this.g = (TextView) this.c.findViewById(R.id.update_number);
        this.h = this.c.findViewById(R.id.rl_rubbish_clean_entrance);
        this.i = this.c.findViewById(R.id.rl_more_manage_entrance);
        this.m = (WaveView) this.c.findViewById(R.id.wave_view);
        this.s = this.c.findViewById(R.id.pb_accelerate);
        this.t = AnimationUtils.loadAnimation(getContext(), R.anim.speedcircle);
        this.j.setOnClickListener(this);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        this.f.setOnClickListener(this);
        this.h.setOnClickListener(this);
        this.i.setOnClickListener(this);
        this.k.setCurrentFormat(new DecimalFormat("0"));
        this.n = (int) (br.d() * 100.0f);
        this.o = this.n;
        this.k.setValue((double) this.n);
        this.m.a(this.n);
        this.p = (int) getResources().getDimension(R.dimen.floating_big_window_circle_size);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
        b();
        setBackgroundColor(1711276032);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        Rect rect = new Rect();
        this.c.getGlobalVisibleRect(rect);
        if (rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
            return false;
        }
        j.a().d(AstApp.i());
        j.a().a(AstApp.i());
        return false;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        j.a().d(AstApp.i());
        j.a().a(AstApp.i());
        return true;
    }

    public void onClick(View view) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        switch (view.getId()) {
            case R.id.ll_yyb_entrance /*2131166021*/:
                intent.setPackage(AstApp.i().getPackageName());
                intent.setAction("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.LAUNCHER");
                AstApp.i().startActivity(intent);
                j.a().d(AstApp.i());
                j.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "05_001", 200);
                return;
            case R.id.ll_setting_entrance /*2131166023*/:
                intent.setClass(AstApp.i(), SettingActivity.class);
                intent.addFlags(67108864);
                AstApp.i().startActivity(intent);
                j.a().d(AstApp.i());
                j.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "05_002", 200);
                return;
            case R.id.rl_accelerate_circle /*2131166025*/:
                if (this.q) {
                    this.q = false;
                    SpaceScanManager.a().p();
                    this.l.setText((int) R.string.floating_window_accelerating_tips);
                    this.s.setVisibility(0);
                    try {
                        this.s.setBackgroundResource(R.drawable.flaot_big_window_progress);
                    } catch (Throwable th) {
                        cq.a().b();
                    }
                    this.s.startAnimation(this.t);
                    this.u = System.currentTimeMillis();
                    j.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "03_001", 200);
                    return;
                }
                return;
            case R.id.rl_app_update_entrance /*2131166033*/:
                intent.setClass(AstApp.i(), UpdateListActivity.class);
                intent.addFlags(67108864);
                AstApp.i().startActivity(intent);
                j.a().d(AstApp.i());
                j.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "04_001", 200);
                return;
            case R.id.rl_rubbish_clean_entrance /*2131166036*/:
                intent.setClass(AstApp.i(), SpaceCleanActivity.class);
                intent.addFlags(67108864);
                AstApp.i().startActivity(intent);
                j.a().d(AstApp.i());
                j.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "04_002", 200);
                return;
            case R.id.rl_more_manage_entrance /*2131166037*/:
                intent.setClass(AstApp.i(), AssistantTabActivity.class);
                intent.addFlags(67108864);
                AstApp.i().startActivity(intent);
                j.a().d(AstApp.i());
                j.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, "04_003", 200);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (((this.p * this.n) / 100) + 1));
        translateAnimation.setDuration(1000);
        translateAnimation.setFillAfter(true);
        translateAnimation.setAnimationListener(new a(this));
        this.m.startAnimation(translateAnimation);
    }

    private void b() {
        int i2 = u.i();
        if (i2 > 0) {
            this.g.setVisibility(0);
            this.g.setText(String.valueOf(i2 > 99 ? "99+" : Integer.valueOf(i2)));
            return;
        }
        this.g.setVisibility(8);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS:
                XLog.d("floatingwindow", "FloatingWindowBigView >> 加速成功！");
                this.r = ((Long) message.obj).longValue();
                this.v = System.currentTimeMillis();
                if (this.v - this.u < 1500) {
                    postDelayed(new b(this), 1500 - (this.v - this.u));
                    return;
                }
                this.s.clearAnimation();
                this.s.setVisibility(8);
                a();
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL:
                XLog.d("floatingwindow", "FloatingWindowBigView >> 加速失败");
                this.v = System.currentTimeMillis();
                if (this.v - this.u < 1500) {
                    postDelayed(new c(this), 1500 - (this.v - this.u));
                    return;
                }
                this.s.clearAnimation();
                this.s.setVisibility(8);
                a();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (((this.p * this.o) / 100) + 1), (float) ((this.p * (this.o - this.n)) / 100));
        translateAnimation.setDuration(1000);
        translateAnimation.setAnimationListener(new d(this));
        this.m.startAnimation(translateAnimation);
    }
}
