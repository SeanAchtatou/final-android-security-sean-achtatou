package com.tencent.assistant.localres;

import com.tencent.assistant.model.SimpleAppModel;
import java.util.Comparator;

/* compiled from: ProGuard */
final class q implements Comparator<SimpleAppModel> {
    q() {
    }

    /* renamed from: a */
    public int compare(SimpleAppModel simpleAppModel, SimpleAppModel simpleAppModel2) {
        if (simpleAppModel == null || simpleAppModel2 == null) {
            return 0;
        }
        long j = simpleAppModel2.L - simpleAppModel.L;
        if (j > 0) {
            return 1;
        }
        if (j != 0) {
            return -1;
        }
        return 0;
    }
}
