package com.tencent.assistant.localres;

import android.content.Context;
import android.provider.MediaStore;
import com.tencent.assistant.localres.model.LocalAV;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class LocalAVLoader extends LocalMediaLoader<LocalAV> {
    public static final int TYPE_ALL = 3;
    public static final int TYPE_MUSIC = 2;
    public static final int TYPE_VEDIO = 1;
    private int f = 3;

    public LocalAVLoader(Context context) {
        super(context);
        this.c = 6;
    }

    public void setSubType(int i) {
        this.f = i;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.clear();
        if (this.f == 3 || this.f == 1) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(b(MediaStore.Video.Media.EXTERNAL_CONTENT_URI));
            arrayList.addAll(b(MediaStore.Video.Media.INTERNAL_CONTENT_URI));
            a(arrayList);
            this.b.addAll(arrayList);
        }
        if (this.f == 3 || this.f == 2) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(a(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI));
            a(arrayList2);
            this.b.addAll(arrayList2);
        }
        a(this, this.b, true);
    }

    private void a(ArrayList<LocalAV> arrayList) {
        boolean z;
        int size = arrayList.size();
        int i = 1;
        while (i < size) {
            int i2 = 0;
            boolean z2 = false;
            while (i2 < size - i) {
                if (arrayList.get(i2).createDate < arrayList.get(i2 + 1).createDate) {
                    arrayList.set(i2, arrayList.get(i2 + 1));
                    arrayList.set(i2 + 1, arrayList.get(i2));
                    z = true;
                } else {
                    z = z2;
                }
                i2++;
                z2 = z;
            }
            if (z2) {
                i++;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:65:0x02bd A[SYNTHETIC, Splitter:B:65:0x02bd] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.ArrayList<com.tencent.assistant.localres.model.LocalAV> a(android.net.Uri r31) {
        /*
            r30 = this;
            monitor-enter(r30)
            java.lang.String r3 = "LocalAVLocal"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0279 }
            r4.<init>()     // Catch:{ all -> 0x0279 }
            java.lang.String r5 = "loadAudio url = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0279 }
            java.lang.String r5 = r31.toString()     // Catch:{ all -> 0x0279 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0279 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0279 }
            com.tencent.assistant.utils.XLog.d(r3, r4)     // Catch:{ all -> 0x0279 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ all -> 0x0279 }
            r10.<init>()     // Catch:{ all -> 0x0279 }
            java.lang.String r6 = "duration> 0"
            r9 = 0
            r0 = r30
            android.content.Context r3 = r0.f1410a     // Catch:{ Exception -> 0x02c3, all -> 0x02c1 }
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch:{ Exception -> 0x02c3, all -> 0x02c1 }
            r5 = 0
            r7 = 0
            java.lang.String r8 = "date_modified DESC"
            r4 = r31
            android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x02c3, all -> 0x02c1 }
            if (r3 == 0) goto L_0x0222
            boolean r4 = r3.moveToFirst()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            if (r4 == 0) goto L_0x0222
            java.lang.String r4 = "_id"
            int r5 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "_data"
            int r6 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "title"
            int r7 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "_display_name"
            int r8 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "_size"
            int r9 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "date_added"
            int r11 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "date_modified"
            int r12 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "mime_type"
            int r13 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "duration"
            int r14 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "artist"
            int r15 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "artist_key"
            int r16 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "composer"
            int r17 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "album"
            int r18 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "album_key"
            int r19 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "album_id"
            int r20 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "year"
            int r21 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "is_music"
            int r22 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "is_podcast"
            int r23 = r3.getColumnIndex(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "is_ringtone"
            int r24 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "is_alarm"
            int r25 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "is_notification"
            int r26 = r3.getColumnIndexOrThrow(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
        L_0x00bd:
            com.tencent.assistant.localres.model.LocalAV r27 = new com.tencent.assistant.localres.model.LocalAV     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r27.<init>()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            int r4 = r3.getInt(r5)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.id = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = r3.getString(r6)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.path = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = r3.getString(r7)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.name = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = r3.getString(r8)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.description = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            long r28 = r3.getLong(r9)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r28
            r2 = r27
            r2.size = r0     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            java.lang.String r4 = r0.name     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = com.tencent.assistant.utils.bv.a(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.softKey = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            long r28 = r3.getLong(r11)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r28
            r2 = r27
            r2.createDate = r0     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            long r28 = r3.getLong(r12)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r28
            r2 = r27
            r2.updateDate = r0     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = r3.getString(r13)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.mimeType = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            int r4 = r3.getInt(r14)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.duration = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = r3.getString(r15)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.artist = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r16
            java.lang.String r4 = r3.getString(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.artistKey = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r17
            java.lang.String r4 = r3.getString(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.composer = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r18
            java.lang.String r4 = r3.getString(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.album = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r19
            java.lang.String r4 = r3.getString(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.albumKey = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r20
            java.lang.String r4 = r3.getString(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.albumId = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r21
            int r4 = r3.getInt(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            r0.createYear = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r22
            int r4 = r3.getInt(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r28 = 1
            r0 = r28
            if (r4 != r0) goto L_0x0229
            r4 = 1
        L_0x016d:
            r0 = r27
            r0.musicFlag = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r4 = -1
            r0 = r23
            if (r0 == r4) goto L_0x0187
            r0 = r23
            int r4 = r3.getInt(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r28 = 1
            r0 = r28
            if (r4 != r0) goto L_0x022c
            r4 = 1
        L_0x0183:
            r0 = r27
            r0.podcastFlag = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
        L_0x0187:
            r0 = r24
            int r4 = r3.getInt(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r28 = 1
            r0 = r28
            if (r4 != r0) goto L_0x022f
            r4 = 1
        L_0x0194:
            r0 = r27
            r0.ringtoneFlag = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r25
            int r4 = r3.getInt(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r28 = 1
            r0 = r28
            if (r4 != r0) goto L_0x0232
            r4 = 1
        L_0x01a5:
            r0 = r27
            r0.alarmFlag = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r26
            int r4 = r3.getInt(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r28 = 1
            r0 = r28
            if (r4 != r0) goto L_0x0235
            r4 = 1
        L_0x01b6:
            r0 = r27
            r0.notificationFlag = r4     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            java.lang.String r4 = r0.path     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            if (r4 != 0) goto L_0x027c
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            java.lang.String r0 = r0.path     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r28 = r0
            r0 = r28
            r4.<init>(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            boolean r28 = r4.exists()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            if (r28 == 0) goto L_0x0237
            boolean r4 = r4.isFile()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            if (r4 == 0) goto L_0x0237
            r0 = r27
            r10.add(r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r4 = "LocalAVLocal"
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r28.<init>()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r29 = "name = "
            java.lang.StringBuilder r28 = r28.append(r29)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            java.lang.String r0 = r0.name     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r29 = r0
            java.lang.StringBuilder r28 = r28.append(r29)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r29 = " path = "
            java.lang.StringBuilder r28 = r28.append(r29)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            java.lang.String r0 = r0.path     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r27 = r0
            r0 = r28
            r1 = r27
            java.lang.StringBuilder r27 = r0.append(r1)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r28 = " 文件存在"
            java.lang.StringBuilder r27 = r27.append(r28)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r27 = r27.toString()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            com.tencent.assistant.utils.XLog.d(r4, r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
        L_0x021c:
            boolean r4 = r3.moveToNext()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            if (r4 != 0) goto L_0x00bd
        L_0x0222:
            if (r3 == 0) goto L_0x0227
            r3.close()     // Catch:{ all -> 0x0279 }
        L_0x0227:
            monitor-exit(r30)
            return r10
        L_0x0229:
            r4 = 0
            goto L_0x016d
        L_0x022c:
            r4 = 0
            goto L_0x0183
        L_0x022f:
            r4 = 0
            goto L_0x0194
        L_0x0232:
            r4 = 0
            goto L_0x01a5
        L_0x0235:
            r4 = 0
            goto L_0x01b6
        L_0x0237:
            java.lang.String r4 = "LocalAVLocal"
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r28.<init>()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r29 = "name = "
            java.lang.StringBuilder r28 = r28.append(r29)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            java.lang.String r0 = r0.name     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r29 = r0
            java.lang.StringBuilder r28 = r28.append(r29)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r29 = " path = "
            java.lang.StringBuilder r28 = r28.append(r29)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            java.lang.String r0 = r0.path     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r27 = r0
            r0 = r28
            r1 = r27
            java.lang.StringBuilder r27 = r0.append(r1)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r28 = " 文件不存在"
            java.lang.StringBuilder r27 = r27.append(r28)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r27 = r27.toString()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            com.tencent.assistant.utils.XLog.d(r4, r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            goto L_0x021c
        L_0x0272:
            r4 = move-exception
        L_0x0273:
            if (r3 == 0) goto L_0x0227
            r3.close()     // Catch:{ all -> 0x0279 }
            goto L_0x0227
        L_0x0279:
            r3 = move-exception
            monitor-exit(r30)
            throw r3
        L_0x027c:
            java.lang.String r4 = "LocalAVLocal"
            java.lang.StringBuilder r28 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r28.<init>()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r29 = "name = "
            java.lang.StringBuilder r28 = r28.append(r29)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            java.lang.String r0 = r0.name     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r29 = r0
            java.lang.StringBuilder r28 = r28.append(r29)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r29 = " path = "
            java.lang.StringBuilder r28 = r28.append(r29)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            java.lang.String r0 = r0.path     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r27 = r0
            r0 = r28
            r1 = r27
            java.lang.StringBuilder r27 = r0.append(r1)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r28 = " 文件名为空"
            java.lang.StringBuilder r27 = r27.append(r28)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            java.lang.String r27 = r27.toString()     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            r0 = r27
            com.tencent.assistant.utils.XLog.d(r4, r0)     // Catch:{ Exception -> 0x0272, all -> 0x02b8 }
            goto L_0x021c
        L_0x02b8:
            r4 = move-exception
            r9 = r3
            r3 = r4
        L_0x02bb:
            if (r9 == 0) goto L_0x02c0
            r9.close()     // Catch:{ all -> 0x0279 }
        L_0x02c0:
            throw r3     // Catch:{ all -> 0x0279 }
        L_0x02c1:
            r3 = move-exception
            goto L_0x02bb
        L_0x02c3:
            r3 = move-exception
            r3 = r9
            goto L_0x0273
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.localres.LocalAVLoader.a(android.net.Uri):java.util.ArrayList");
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0222 A[SYNTHETIC, Splitter:B:47:0x0222] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.ArrayList<com.tencent.assistant.localres.model.LocalAV> b(android.net.Uri r30) {
        /*
            r29 = this;
            monitor-enter(r29)
            java.lang.String r3 = "LocalAVLocal"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0212 }
            r4.<init>()     // Catch:{ all -> 0x0212 }
            java.lang.String r5 = "loadVideo  url = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0212 }
            java.lang.String r5 = r30.toString()     // Catch:{ all -> 0x0212 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0212 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0212 }
            com.tencent.assistant.utils.XLog.d(r3, r4)     // Catch:{ all -> 0x0212 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ all -> 0x0212 }
            r10.<init>()     // Catch:{ all -> 0x0212 }
            java.lang.String r6 = "duration> 0"
            r9 = 0
            r0 = r29
            android.content.Context r3 = r0.f1410a     // Catch:{ Throwable -> 0x0229, all -> 0x0226 }
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch:{ Throwable -> 0x0229, all -> 0x0226 }
            r5 = 0
            r7 = 0
            java.lang.String r8 = "date_modified DESC"
            r4 = r30
            android.database.Cursor r4 = r3.query(r4, r5, r6, r7, r8)     // Catch:{ Throwable -> 0x0229, all -> 0x0226 }
            if (r4 == 0) goto L_0x01f4
            boolean r3 = r4.moveToFirst()     // Catch:{ Throwable -> 0x0208 }
            if (r3 == 0) goto L_0x01f4
            java.lang.String r3 = "_id"
            int r5 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "_data"
            int r6 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "title"
            int r7 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "_display_name"
            int r8 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "_size"
            int r9 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "date_added"
            int r11 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "date_modified"
            int r12 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "mime_type"
            int r13 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "duration"
            int r14 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "artist"
            int r15 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "album"
            int r16 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "resolution"
            int r17 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "description"
            int r18 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "isprivate"
            int r19 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "tags"
            int r20 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "category"
            int r21 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "language"
            int r22 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "latitude"
            int r23 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "longitude"
            int r24 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "datetaken"
            int r25 = r4.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x0208 }
        L_0x00b7:
            com.tencent.assistant.localres.model.LocalAV r26 = new com.tencent.assistant.localres.model.LocalAV     // Catch:{ Throwable -> 0x0208 }
            r26.<init>()     // Catch:{ Throwable -> 0x0208 }
            int r3 = r4.getInt(r5)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.id = r3     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = r4.getString(r6)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.path = r3     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = r4.getString(r7)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.name = r3     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = r4.getString(r8)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.description = r3     // Catch:{ Throwable -> 0x0208 }
            long r27 = r4.getLong(r9)     // Catch:{ Throwable -> 0x0208 }
            r0 = r27
            r2 = r26
            r2.size = r0     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            java.lang.String r3 = r0.name     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = com.tencent.assistant.utils.bv.a(r3)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.softKey = r3     // Catch:{ Throwable -> 0x0208 }
            long r27 = r4.getLong(r11)     // Catch:{ Throwable -> 0x0208 }
            r0 = r27
            r2 = r26
            r2.createDate = r0     // Catch:{ Throwable -> 0x0208 }
            long r27 = r4.getLong(r12)     // Catch:{ Throwable -> 0x0208 }
            r0 = r27
            r2 = r26
            r2.updateDate = r0     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = r4.getString(r13)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.mimeType = r3     // Catch:{ Throwable -> 0x0208 }
            int r3 = r4.getInt(r14)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.duration = r3     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = r4.getString(r15)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.artist = r3     // Catch:{ Throwable -> 0x0208 }
            r0 = r16
            java.lang.String r3 = r4.getString(r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.album = r3     // Catch:{ Throwable -> 0x0208 }
            r0 = r17
            java.lang.String r3 = r4.getString(r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.resolution = r3     // Catch:{ Throwable -> 0x0208 }
            r0 = r18
            java.lang.String r3 = r4.getString(r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.description = r3     // Catch:{ Throwable -> 0x0208 }
            r0 = r19
            int r3 = r4.getInt(r0)     // Catch:{ Throwable -> 0x0208 }
            r27 = 1
            r0 = r27
            if (r3 != r0) goto L_0x01fb
            r3 = 1
        L_0x0149:
            r0 = r26
            r0.privateFlag = r3     // Catch:{ Throwable -> 0x0208 }
            r0 = r20
            java.lang.String r3 = r4.getString(r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.tags = r3     // Catch:{ Throwable -> 0x0208 }
            r0 = r21
            java.lang.String r3 = r4.getString(r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.category = r3     // Catch:{ Throwable -> 0x0208 }
            r0 = r22
            java.lang.String r3 = r4.getString(r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.language = r3     // Catch:{ Throwable -> 0x0208 }
            r0 = r23
            double r27 = r4.getDouble(r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r27
            r2 = r26
            r2.latitude = r0     // Catch:{ Throwable -> 0x0208 }
            r0 = r24
            double r27 = r4.getDouble(r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r27
            r2 = r26
            r2.longitude = r0     // Catch:{ Throwable -> 0x0208 }
            r0 = r25
            int r3 = r4.getInt(r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            r0.datetaken = r3     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "LocalAVLocal"
            java.lang.StringBuilder r27 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0208 }
            r27.<init>()     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r28 = "name = "
            java.lang.StringBuilder r27 = r27.append(r28)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            java.lang.String r0 = r0.name     // Catch:{ Throwable -> 0x0208 }
            r28 = r0
            java.lang.StringBuilder r27 = r27.append(r28)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r28 = " path = "
            java.lang.StringBuilder r27 = r27.append(r28)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            java.lang.String r0 = r0.path     // Catch:{ Throwable -> 0x0208 }
            r28 = r0
            java.lang.StringBuilder r27 = r27.append(r28)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r27 = r27.toString()     // Catch:{ Throwable -> 0x0208 }
            r0 = r27
            com.tencent.assistant.utils.XLog.d(r3, r0)     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            java.lang.String r3 = r0.path     // Catch:{ Throwable -> 0x0208 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Throwable -> 0x0208 }
            if (r3 != 0) goto L_0x0215
            java.io.File r3 = new java.io.File     // Catch:{ Throwable -> 0x0208 }
            r0 = r26
            java.lang.String r0 = r0.path     // Catch:{ Throwable -> 0x0208 }
            r27 = r0
            r0 = r27
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0208 }
            boolean r27 = r3.exists()     // Catch:{ Throwable -> 0x0208 }
            if (r27 == 0) goto L_0x01fe
            boolean r3 = r3.isFile()     // Catch:{ Throwable -> 0x0208 }
            if (r3 == 0) goto L_0x01fe
            r0 = r26
            r10.add(r0)     // Catch:{ Throwable -> 0x0208 }
            java.lang.String r3 = "LocalAVLocal"
            java.lang.String r26 = "文件存在"
            r0 = r26
            com.tencent.assistant.utils.XLog.d(r3, r0)     // Catch:{ Throwable -> 0x0208 }
        L_0x01ee:
            boolean r3 = r4.moveToNext()     // Catch:{ Throwable -> 0x0208 }
            if (r3 != 0) goto L_0x00b7
        L_0x01f4:
            if (r4 == 0) goto L_0x01f9
            r4.close()     // Catch:{ all -> 0x0212 }
        L_0x01f9:
            monitor-exit(r29)
            return r10
        L_0x01fb:
            r3 = 0
            goto L_0x0149
        L_0x01fe:
            java.lang.String r3 = "LocalAVLocal"
            java.lang.String r26 = "文件不存在"
            r0 = r26
            com.tencent.assistant.utils.XLog.d(r3, r0)     // Catch:{ Throwable -> 0x0208 }
            goto L_0x01ee
        L_0x0208:
            r3 = move-exception
        L_0x0209:
            r3.printStackTrace()     // Catch:{ all -> 0x021f }
            if (r4 == 0) goto L_0x01f9
            r4.close()     // Catch:{ all -> 0x0212 }
            goto L_0x01f9
        L_0x0212:
            r3 = move-exception
            monitor-exit(r29)
            throw r3
        L_0x0215:
            java.lang.String r3 = "LocalAVLocal"
            java.lang.String r26 = "文件名为空"
            r0 = r26
            com.tencent.assistant.utils.XLog.d(r3, r0)     // Catch:{ Throwable -> 0x0208 }
            goto L_0x01ee
        L_0x021f:
            r3 = move-exception
        L_0x0220:
            if (r4 == 0) goto L_0x0225
            r4.close()     // Catch:{ all -> 0x0212 }
        L_0x0225:
            throw r3     // Catch:{ all -> 0x0212 }
        L_0x0226:
            r3 = move-exception
            r4 = r9
            goto L_0x0220
        L_0x0229:
            r3 = move-exception
            r4 = r9
            goto L_0x0209
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.localres.LocalAVLoader.b(android.net.Uri):java.util.ArrayList");
    }
}
