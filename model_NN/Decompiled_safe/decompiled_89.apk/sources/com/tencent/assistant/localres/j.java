package com.tencent.assistant.localres;

import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.n;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f1434a;
    final /* synthetic */ long b;
    final /* synthetic */ ApkResourceManager c;

    j(ApkResourceManager apkResourceManager, List list, long j) {
        this.c = apkResourceManager;
        this.f1434a = list;
        this.b = j;
    }

    public void run() {
        for (String str : this.f1434a) {
            LocalApkInfo localApkInfo = (LocalApkInfo) this.c.h.get(str);
            if (localApkInfo != null) {
                localApkInfo.mLastLaunchTime = this.b;
                localApkInfo.mFakeLastLaunchTime = this.b;
                if (this.c.c == null) {
                    n unused = this.c.c = new n(AstApp.i());
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(str);
                this.c.c.a(arrayList, this.b);
            }
        }
    }
}
