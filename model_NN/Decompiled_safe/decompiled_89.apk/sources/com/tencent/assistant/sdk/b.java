package com.tencent.assistant.sdk;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.plugin.UserLoginInfo;
import com.tencent.assistant.sdk.param.jce.IPCBaseParam;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.sdk.param.jce.PluginLoginInfo;
import com.tencent.assistant.sdk.param.jce.QueryLoginInfoRequest;
import com.tencent.assistant.sdk.param.jce.QueryLoginInfoResponse;

/* compiled from: ProGuard */
public class b extends r {
    private QueryLoginInfoRequest k;

    public b(Context context, IPCRequest iPCRequest) {
        super(context, iPCRequest);
    }

    /* access modifiers changed from: protected */
    public void a(JceStruct jceStruct) {
        this.k = (QueryLoginInfoRequest) jceStruct;
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        QueryLoginInfoResponse queryLoginInfoResponse = new QueryLoginInfoResponse();
        String str = null;
        if (this.k != null) {
            str = this.k.a();
        }
        UserLoginInfo a2 = com.tencent.assistant.plugin.a.b.a(str);
        queryLoginInfoResponse.a(a2.getState());
        PluginLoginInfo pluginLoginInfo = new PluginLoginInfo();
        pluginLoginInfo.a(a2.getUin());
        pluginLoginInfo.b(a2.getPic());
        pluginLoginInfo.a(a2.getNickName());
        pluginLoginInfo.a(a2.getA2());
        queryLoginInfoResponse.a(pluginLoginInfo);
        return queryLoginInfoResponse;
    }

    /* access modifiers changed from: protected */
    public IPCBaseParam b() {
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean a(DownloadInfo downloadInfo) {
        return false;
    }
}
