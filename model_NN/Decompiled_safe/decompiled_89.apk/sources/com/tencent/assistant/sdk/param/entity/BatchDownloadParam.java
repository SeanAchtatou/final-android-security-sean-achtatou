package com.tencent.assistant.sdk.param.entity;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class BatchDownloadParam implements Parcelable {
    public static final Parcelable.Creator<BatchDownloadParam> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public String f2470a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;
    public String f = Constants.STR_EMPTY;
    public String g = Constants.STR_EMPTY;
    public String h = Constants.STR_EMPTY;
    public String i = Constants.STR_EMPTY;
    public String j = Constants.STR_EMPTY;
    public String k = Constants.STR_EMPTY;
    public String l = Constants.STR_EMPTY;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f2470a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeString(this.j);
        parcel.writeString(this.k);
        parcel.writeString(this.l);
    }
}
