package com.tencent.assistant.sdk;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.sdk.b.a;
import com.tencent.assistant.sdk.param.jce.BatchDownloadActionResponse;
import com.tencent.assistant.sdk.param.jce.IPCDownloadParam;
import com.tencent.assistant.sdk.param.jce.IPCQueryDownloadInfo;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class g extends t {
    private final String n = "SDKBatchQueryResolver";

    public g(Context context, IPCRequest iPCRequest) {
        super(context, iPCRequest);
    }

    /* access modifiers changed from: protected */
    public void a(JceStruct jceStruct) {
        super.a(jceStruct);
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        if (this.l == null || this.l.b == null) {
            return null;
        }
        this.m = new BatchDownloadActionResponse();
        this.m.f2472a = this.k;
        ArrayList arrayList = new ArrayList();
        Iterator<IPCDownloadParam> it = this.l.b.iterator();
        while (it.hasNext()) {
            IPCDownloadParam next = it.next();
            if (next != null) {
                DownloadInfo a2 = p.a(next.a());
                IPCQueryDownloadInfo iPCQueryDownloadInfo = new IPCQueryDownloadInfo();
                if (a2 == null || a2.response == null) {
                    LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(next.a().d, Integer.valueOf(next.a().c).intValue(), 0);
                    if (localApkInfo != null) {
                        iPCQueryDownloadInfo.d = localApkInfo.occupySize;
                        iPCQueryDownloadInfo.e = localApkInfo.occupySize;
                        iPCQueryDownloadInfo.b = localApkInfo.mLocalFilePath;
                        iPCQueryDownloadInfo.c = 4;
                    }
                } else {
                    iPCQueryDownloadInfo.d = a2.response.f1263a;
                    iPCQueryDownloadInfo.e = a2.response.b;
                    iPCQueryDownloadInfo.b = a2.getCurrentValidPath();
                    iPCQueryDownloadInfo.c = a.a(a2);
                }
                iPCQueryDownloadInfo.f = DownloadProxy.a().l();
                iPCQueryDownloadInfo.g = DownloadProxy.a().k();
                arrayList.add(iPCQueryDownloadInfo);
            }
        }
        this.m.a(arrayList);
        return this.m;
    }
}
