package com.tencent.assistant.securescan;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.df;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ScanningAnimView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private static float f2496a = 1.16f;
    private static final float b = (f2496a * 185.0f);
    private static final float c = (f2496a * 48.0f);
    private static final float d = (f2496a * 15.0f);
    private static final float e = ((float) (((double) f2496a) * 5.5d));
    private Context f;
    /* access modifiers changed from: private */
    public ViewFlipper g;
    private PackageManager h;
    private List<ApplicationInfo> i = new ArrayList();
    /* access modifiers changed from: private */
    public ImageView j;
    /* access modifiers changed from: private */
    public ImageView k;
    private View l;
    /* access modifiers changed from: private */
    public Bitmap m;
    /* access modifiers changed from: private */
    public Bitmap n;
    private Bitmap o;
    private LayoutInflater p;
    /* access modifiers changed from: private */
    public int q = 0;
    /* access modifiers changed from: private */
    public int r = 0;
    /* access modifiers changed from: private */
    public int s = 0;
    /* access modifiers changed from: private */
    public Boolean t = false;
    private HandlerThread u;
    /* access modifiers changed from: private */
    public Handler v;
    /* access modifiers changed from: private */
    public long w = 0;
    /* access modifiers changed from: private */
    public long x = 0;
    /* access modifiers changed from: private */
    public Handler y = new n(this);

    static /* synthetic */ int c(ScanningAnimView scanningAnimView) {
        int i2 = scanningAnimView.s;
        scanningAnimView.s = i2 + 1;
        return i2;
    }

    static /* synthetic */ int o(ScanningAnimView scanningAnimView) {
        int i2 = scanningAnimView.q + 1;
        scanningAnimView.q = i2;
        return i2;
    }

    public ScanningAnimView(Context context) {
        super(context);
        this.f = context;
        a();
        b();
        a(context);
    }

    public ScanningAnimView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f = context;
        a();
        b();
        a(context);
    }

    private void a() {
        this.u = new HandlerThread("MyHandlerThread");
        this.u.start();
        this.v = new o(this, this.u.getLooper());
    }

    private void b() {
        this.h = this.f.getPackageManager();
        this.i = getAllApps();
        this.o = Bitmap.createBitmap(df.a(this.f, b), df.a(this.f, b), Bitmap.Config.ARGB_4444);
        this.v.sendEmptyMessage(11);
    }

    private void a(Context context) {
        this.p = LayoutInflater.from(context);
        this.p.inflate((int) R.layout.scanning_anim_layout, this);
        this.l = findViewById(R.id.scanning);
        this.g = (ViewFlipper) findViewById(R.id.flipperview);
        this.j = new ImageView(this.f);
        this.k = new ImageView(this.f);
    }

    private void c() {
        this.g.removeAllViews();
        this.g.clearAnimation();
        this.g.setInAnimation(this.f, R.anim.safescan_icons_view_in);
        this.g.setOutAnimation(this.f, R.anim.safescan_icons_view_out);
        this.g.getInAnimation().setAnimationListener(new p(this));
        if (this.n != null && !this.n.isRecycled()) {
            this.j.setImageBitmap(this.n);
        }
        this.t = false;
        this.r = 0;
        this.v.sendEmptyMessageDelayed(11, 100);
        this.g.addView(this.j, 0);
        this.g.addView(this.k, 1);
        this.g.setFlipInterval(1500);
    }

    /* access modifiers changed from: private */
    public List<ApplicationInfo> a(int i2) {
        if (this.i.size() < 9) {
            return b(i2);
        }
        ArrayList arrayList = new ArrayList();
        int size = (i2 * 9) % this.i.size();
        for (int i3 = 0; i3 < 9; i3++) {
            int size2 = size % this.i.size();
            arrayList.add(this.i.get(size2));
            size = size2 + 1;
        }
        return arrayList;
    }

    private List<ApplicationInfo> b(int i2) {
        if (this.i.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int size = (i2 * 3) % this.i.size();
        for (int i3 = 0; i3 < 3; i3++) {
            int size2 = size % this.i.size();
            arrayList.add(this.i.get(size2));
            size = size2 + 1;
        }
        return arrayList;
    }

    public List<ApplicationInfo> getAllApps() {
        try {
            return this.h.getInstalledApplications(0);
        } catch (Throwable th) {
            return new ArrayList();
        }
    }

    /* access modifiers changed from: private */
    public Bitmap a(List<ApplicationInfo> list) {
        Bitmap drawableToBitmap;
        if (list == null || this.o.isRecycled()) {
            return null;
        }
        Drawable defaultActivityIcon = this.f.getPackageManager().getDefaultActivityIcon();
        if (list.size() == 9) {
            Canvas canvas = new Canvas(this.o);
            Drawable drawable = null;
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= list.size()) {
                    return this.o;
                }
                int i4 = i3 / 3;
                int i5 = i3 % 3;
                try {
                    ApplicationInfo applicationInfo = list.get(i3);
                    if (applicationInfo == null) {
                        return null;
                    }
                    drawable = this.f.getPackageManager().getApplicationIcon(applicationInfo.packageName);
                    if (drawable == null || (drawableToBitmap = drawableToBitmap(drawable)) == null) {
                        return null;
                    }
                    if (drawableToBitmap != null && !drawableToBitmap.isRecycled()) {
                        canvas.drawBitmap(drawableToBitmap, (Rect) null, new Rect(df.a(this.f, e + ((c + d) * ((float) i5))), df.a(this.f, e + ((c + d) * ((float) i4))), df.a(this.f, (((float) i5) * (c + d)) + e + c), df.a(this.f, (((float) i4) * (c + d)) + e + c)), (Paint) null);
                        if ((drawable instanceof BitmapDrawable) && (defaultActivityIcon instanceof BitmapDrawable) && ((BitmapDrawable) drawable).getBitmap() != ((BitmapDrawable) defaultActivityIcon).getBitmap()) {
                            drawableToBitmap.recycle();
                            XLog.i("jesonma", "recycle,iconBitmap= " + drawableToBitmap.getClass());
                        }
                    }
                    i2 = i3 + 1;
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                } catch (Throwable th) {
                }
            }
        } else if (list.size() == 3) {
            return b(list);
        } else {
            return null;
        }
    }

    private Bitmap b(List<ApplicationInfo> list) {
        Drawable drawable;
        if (list.size() != 3 || this.o.isRecycled()) {
            return null;
        }
        Canvas canvas = new Canvas(this.o);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return this.o;
            }
            try {
                drawable = this.f.getPackageManager().getApplicationIcon(list.get(i3).packageName);
            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
                drawable = null;
            }
            if (drawable == null) {
                return null;
            }
            Bitmap drawableToBitmap = drawableToBitmap(drawable);
            if (drawableToBitmap == null) {
                return null;
            }
            if (drawableToBitmap != null && !drawableToBitmap.isRecycled()) {
                canvas.drawBitmap(drawableToBitmap, (Rect) null, new Rect(df.a(this.f, e + ((b - c) / 2.0f)), df.a(this.f, e + ((c + d) * ((float) i3))), df.a(this.f, e + ((b - c) / 2.0f) + c), df.a(this.f, e + ((c + d) * ((float) i3)) + c)), (Paint) null);
            }
            i2 = i3 + 1;
        }
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Drawable drawable2;
        if (drawable == null) {
            return null;
        }
        if ((drawable instanceof StateListDrawable) || (drawable instanceof LevelListDrawable)) {
            drawable2 = drawable.getCurrent();
        } else {
            drawable2 = drawable;
        }
        if (drawable2 instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable2).getBitmap();
        }
        int intrinsicWidth = drawable2.getIntrinsicWidth();
        int intrinsicHeight = drawable2.getIntrinsicHeight();
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, drawable2.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        drawable2.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable2.draw(canvas);
        return createBitmap;
    }

    public void startScanAnim() {
        if (this.r < 1) {
            this.t = true;
            if (!this.u.isAlive()) {
                this.r = 0;
                this.s = 0;
                this.v.sendEmptyMessageDelayed(11, 100);
            }
        } else if (this.g != null) {
            this.q = 0;
            if (!this.g.isFlipping()) {
                c();
                this.l.startAnimation(AnimationUtils.loadAnimation(this.f, R.anim.safescan_scanning_rotate));
                this.g.startFlipping();
                this.w = System.currentTimeMillis();
                this.g.getInAnimation().startNow();
            }
        }
    }

    public void stopScanAnim() {
        if (this.g != null) {
            this.g.stopFlipping();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.g != null) {
            this.g.stopFlipping();
        }
    }

    public void destroy() {
        if (this.u != null) {
            this.u.quit();
        }
        if (this.n != null) {
            if (!this.n.isRecycled()) {
                this.n.recycle();
            }
            this.n = null;
        }
        if (this.m != null) {
            if (!this.m.isRecycled()) {
                this.m.recycle();
            }
            this.m = null;
        }
    }
}
