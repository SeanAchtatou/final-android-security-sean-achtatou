package com.tencent.assistant.securescan;

import android.app.Activity;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class i extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2507a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ e d;

    i(e eVar, SimpleAppModel simpleAppModel, int i, int i2) {
        this.d = eVar;
        this.f2507a = simpleAppModel;
        this.b = i;
        this.c = i2;
    }

    public void onTMAClick(View view) {
        j jVar = new j(this);
        jVar.blockCaller = true;
        jVar.hasTitle = true;
        jVar.titleRes = this.d.f.getResources().getString(R.string.secure_scan_exchange_offical);
        jVar.contentRes = this.d.c(this.f2507a);
        jVar.lBtnTxtRes = this.d.f.getResources().getString(R.string.secure_scan_cancle);
        jVar.rBtnTxtRes = this.d.f.getResources().getString(R.string.secure_scan_exchange_offical);
        if (jVar != null) {
            v.a((Activity) this.d.f, jVar);
        }
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.d.f, this.f2507a, this.d.a(this.b, this.c, 100), 200, null);
    }
}
