package com.tencent.assistant.securescan;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class z implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f2524a;

    private z(StartScanActivity startScanActivity) {
        this.f2524a = startScanActivity;
    }

    /* synthetic */ z(StartScanActivity startScanActivity, q qVar) {
        this(startScanActivity);
    }

    public void onAnimationEnd(Animation animation) {
        switch (this.f2524a.t) {
            case 10:
                this.f2524a.c(20);
                break;
            case 11:
                this.f2524a.j();
                break;
            case 12:
                this.f2524a.A.setVisibility(8);
                this.f2524a.C();
                break;
        }
        this.f2524a.H.stopScanAnim();
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
