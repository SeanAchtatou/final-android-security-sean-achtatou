package com.tencent.assistant.plugin;

/* compiled from: ProGuard */
public interface LoginStateCallback {
    void gotUserInfo(UserLoginInfo userLoginInfo);

    void gotUserStateChange(UserStateInfo userStateInfo);
}
