package com.tencent.assistant.plugin.mgr;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.activity.PluginDownActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.av;
import com.tencent.assistant.model.j;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.tauth.AuthActivity;

/* compiled from: ProGuard */
public class f implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static Runnable f1950a = null;
    private static f b;

    private f() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_PLUGIN_DIALOG_NORMAL_INSTALLED, this);
    }

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (b == null) {
                b = new f();
            }
            fVar = b;
        }
        return fVar;
    }

    public int a(Context context, String str, String str2, int i, int i2, int i3, String str3) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, 1000);
        bundle.putString("id", str);
        bundle.putString("name", str2);
        if (i != -1) {
            bundle.putInt("maxchap", i);
        }
        if (i3 != -1) {
            bundle.putInt("finish", i3);
        }
        if (i2 != -1) {
            bundle.putInt("chap", i2);
        }
        bundle.putString("author", str3);
        return a(context, bundle);
    }

    public int a(Context context, String str, String str2, int i, int i2, String str3) {
        return a(context, str, str2, i, -1, i2, str3);
    }

    public int a(Context context, String str, String str2, int i, int i2) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, 1001);
        bundle.putString("id", str);
        bundle.putString("name", str2);
        bundle.putInt("maxchap", i);
        bundle.putInt("finish", i2);
        return a(context, bundle);
    }

    public int b(Context context, String str, String str2, int i, int i2, String str3) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE);
        bundle.putString("id", str);
        bundle.putString("name", str2);
        bundle.putInt("maxchap", i);
        bundle.putInt("finish", i2);
        bundle.putString("author", str3);
        return a(context, bundle);
    }

    public int a(Context context, long j, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START);
        bundle.putLong("uin", j);
        bundle.putInt("pay_requestcode", i);
        return a(context, bundle);
    }

    private int a(Context context, Bundle bundle) {
        PluginInfo a2 = d.b().a("com.qqreader");
        g gVar = new g(this, bundle);
        j a3 = av.a().a("com.qqreader");
        if (a2 == null || (a3 != null && a3.d > a2.getVersion() && c.a("com.qqreader") < 0)) {
            f1950a = gVar;
            Intent intent = new Intent(context, PluginDownActivity.class);
            intent.putExtra("package_name", "com.qqreader");
            context.startActivity(intent);
            return -1;
        }
        gVar.run();
        return 0;
    }

    public int a(Context context, String str, String str2) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, 1002);
        bundle.putString("id", str);
        bundle.putString("name", str2);
        return a(context, bundle);
    }

    public int a(Context context, String str) {
        Bundle bundle = new Bundle();
        bundle.putInt(AuthActivity.ACTION_KEY, EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING);
        bundle.putString("id", str);
        return a(context, bundle);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_PLUGIN_DIALOG_NORMAL_INSTALLED:
                if ((message.obj instanceof String) && "com.qqreader".equals(message.obj) && f1950a != null) {
                    f1950a.run();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
