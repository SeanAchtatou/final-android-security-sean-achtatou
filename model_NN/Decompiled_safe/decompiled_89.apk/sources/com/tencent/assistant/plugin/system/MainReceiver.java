package com.tencent.assistant.plugin.system;

import android.content.BroadcastReceiver;
import android.content.Context;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.c;

/* compiled from: ProGuard */
public class MainReceiver extends PluginDispatchReceiver {
    /* access modifiers changed from: protected */
    public int a() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public BroadcastReceiver a(Context context, PluginInfo pluginInfo) {
        return c.d(context, pluginInfo);
    }
}
