package com.tencent.assistant.plugin.a;

import com.tencent.assistant.login.a.b;
import com.tencent.assistant.login.d;
import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.plugin.SimpleLoginInfo;

/* compiled from: ProGuard */
public abstract class a {
    public static SimpleLoginInfo a() {
        d a2 = d.a();
        if (a2.k()) {
            MoblieQIdentityInfo moblieQIdentityInfo = (MoblieQIdentityInfo) a2.c();
            b f = com.tencent.assistant.login.a.a.f();
            if (moblieQIdentityInfo != null) {
                SimpleLoginInfo simpleLoginInfo = new SimpleLoginInfo();
                simpleLoginInfo.nickName = f.b;
                simpleLoginInfo.skey = moblieQIdentityInfo.getSKey();
                simpleLoginInfo.uin = moblieQIdentityInfo.getUin();
                simpleLoginInfo.sid = moblieQIdentityInfo.getSid();
                simpleLoginInfo.vkey = moblieQIdentityInfo.getVkey();
                return simpleLoginInfo;
            }
        }
        return null;
    }
}
