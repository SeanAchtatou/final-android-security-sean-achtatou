package com.tencent.assistant.plugin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import com.tencent.assistant.plugin.mgr.e;
import com.tencent.assistant.plugin.system.AppService;
import com.tencent.assistant.plugin.system.ConnectAppService;
import com.tencent.assistant.plugin.system.DockAccelerationService;
import com.tencent.assistant.plugin.system.DockSecureService;
import java.io.File;
import java.io.FileOutputStream;

/* compiled from: ProGuard */
public class PluginContext extends ContextThemeWrapper {

    /* renamed from: a  reason: collision with root package name */
    private AssetManager f1930a = null;
    private Resources b = null;
    private Resources.Theme c = null;
    private int d;
    private ClassLoader e;
    private Context f;
    private PluginInfo g;

    private AssetManager a(String str) {
        return e.d(str);
    }

    private Resources a(Context context, AssetManager assetManager) {
        if (context != null) {
            try {
                return new Resources(assetManager, context.getResources().getDisplayMetrics(), context.getResources().getConfiguration());
            } catch (Exception e2) {
            }
        } else {
            Log.e("p.com.qq.connect", "ctx != null ########");
            return null;
        }
    }

    private Resources.Theme a(Resources resources) {
        Resources.Theme newTheme = resources.newTheme();
        this.d = b("com.android.internal.R.style.Theme");
        newTheme.applyStyle(this.d, true);
        return newTheme;
    }

    public String getPackageName() {
        return this.g.getPackageName();
    }

    private int b(String str) {
        try {
            String substring = str.substring(0, str.indexOf(".R.") + 2);
            int lastIndexOf = str.lastIndexOf(".");
            String substring2 = str.substring(lastIndexOf + 1, str.length());
            String substring3 = str.substring(0, lastIndexOf);
            return Class.forName(substring + "$" + substring3.substring(substring3.lastIndexOf(".") + 1, substring3.length())).getDeclaredField(substring2).getInt(null);
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    public Resources getRes() {
        return this.b;
    }

    public PluginContext(Context context, int i, PluginInfo pluginInfo, ClassLoader classLoader) {
        super(context, i);
        this.g = pluginInfo;
        this.e = classLoader;
        this.f1930a = a(this.g.getPluginApkPath());
        this.b = a(context, this.f1930a);
        this.c = a(this.b);
        this.f = context;
    }

    public Resources getResources() {
        return this.b;
    }

    public AssetManager getAssets() {
        return this.f1930a;
    }

    public Resources.Theme getTheme() {
        return this.c;
    }

    public ClassLoader getClassLoader() {
        if (this.e != null) {
            return this.e;
        }
        return super.getClassLoader();
    }

    public boolean startPluginService(Intent intent, String str) {
        Class cls = null;
        if (!TextUtils.isEmpty(str)) {
            if (str.equals(this.g.getAppServiceImpl())) {
                cls = this.g.getInProcess() == 1 ? ConnectAppService.class : AppService.class;
            } else if (str.equals(this.g.getExtendServiceImpl(PluginInfo.META_DATA_DOCK_SEC_SERVICE))) {
                if (this.g.getInProcess() == 0) {
                    cls = DockSecureService.class;
                }
            } else if (str.equals(this.g.getAccelerationServiceImpl()) && this.g.getInProcess() == 0) {
                cls = DockAccelerationService.class;
            }
        }
        if (cls == null) {
            return false;
        }
        intent.setClass(this.f, cls);
        this.f.startService(intent);
        return true;
    }

    public File getDir(String str, int i) {
        return super.getDir("plugin_dir_" + this.g.getPackageName() + "_" + str, i);
    }

    public File getFilesDir() {
        return super.getDir("plugin_files_" + this.g.getPackageName(), 0);
    }

    public File getCacheDir() {
        return super.getDir("plugin_cache_" + this.g.getPackageName(), 0);
    }

    public SharedPreferences getSharedPreferences(String str, int i) {
        return super.getSharedPreferences("plugin_prefs_" + this.g.getPackageName() + "_" + str, i);
    }

    public FileOutputStream openFileOutput(String str, int i) {
        return super.openFileOutput("plugin_output_" + str, i);
    }

    public PluginInfo getPluginInfo() {
        return this.g;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.e = classLoader;
    }
}
