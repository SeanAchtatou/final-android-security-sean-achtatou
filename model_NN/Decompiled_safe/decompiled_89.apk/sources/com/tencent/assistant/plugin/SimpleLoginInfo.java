package com.tencent.assistant.plugin;

/* compiled from: ProGuard */
public class SimpleLoginInfo {
    public String nickName;
    public String sid;
    public String skey;
    public long uin;
    public String vkey;

    public String toString() {
        return "SimpleLoginInfo{nickName='" + this.nickName + '\'' + ", uin=" + this.uin + ", skey='" + this.skey + '\'' + ", sid='" + this.sid + '\'' + ", vkey='" + this.vkey + '\'' + '}';
    }
}
