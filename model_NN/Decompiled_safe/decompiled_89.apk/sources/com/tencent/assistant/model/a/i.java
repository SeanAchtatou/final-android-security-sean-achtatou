package com.tencent.assistant.model.a;

import com.tencent.assistant.manager.smartcard.b;
import java.util.List;

/* compiled from: ProGuard */
public class i {
    public int i;
    public int j;
    public String k;
    public String l;
    public String m;
    public String n;
    public String o;
    public boolean p;
    public String q;
    public byte[] r = null;
    public int s = 2000;
    public int t = -1;

    public int i() {
        return this.i;
    }

    public String j() {
        return this.k;
    }

    public void a(int i2) {
        this.t = i2;
    }

    public int k() {
        return a(this.i, this.j);
    }

    public static int a(int i2, int i3) {
        if (i2 == 16 || i2 == 0 || i2 == 17) {
            if (i2 == 0) {
                return i3 + 10000;
            }
            return (i2 * 1000) + i3;
        } else if (b.a(i2) != null) {
            return (100000 * i2) + i3;
        } else {
            return i2;
        }
    }

    public List<Long> h() {
        return null;
    }

    public void a(List<Long> list) {
    }

    public String d() {
        return String.valueOf(i());
    }
}
