package com.tencent.assistant.model.a;

import com.tencent.assistant.protocol.jce.SmartCardOp;

/* compiled from: ProGuard */
public class v extends i {

    /* renamed from: a  reason: collision with root package name */
    public String f1655a;
    public boolean b;
    public int c;
    public int d;
    public int e;

    public boolean a() {
        return this.b;
    }

    public void a(boolean z) {
        this.b = z;
    }

    public void a(SmartCardOp smartCardOp) {
        if (smartCardOp != null) {
            this.k = smartCardOp.b;
            this.m = smartCardOp.c;
            this.n = smartCardOp.e;
            this.o = smartCardOp.j;
            this.f1655a = smartCardOp.d;
            this.j = smartCardOp.f2323a;
            this.b = smartCardOp.f;
            this.c = smartCardOp.g;
            this.d = smartCardOp.h;
            this.e = smartCardOp.i;
        }
    }
}
