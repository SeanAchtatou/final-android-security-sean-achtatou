package com.tencent.assistant.model.a;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.SmartCardCpaAdvertise;

/* compiled from: ProGuard */
public class d extends i {

    /* renamed from: a  reason: collision with root package name */
    public String f1638a;
    public SimpleAppModel b;
    public boolean c;
    public long d;
    public int e;
    public int f;

    public boolean a() {
        return this.c;
    }

    public void a(boolean z) {
        this.c = z;
    }

    public void a(SmartCardCpaAdvertise smartCardCpaAdvertise, int i) {
        this.i = i;
        if (smartCardCpaAdvertise != null) {
            this.b = u.a(smartCardCpaAdvertise.f2318a);
            this.f1638a = smartCardCpaAdvertise.b;
            this.e = smartCardCpaAdvertise.d;
            this.f = smartCardCpaAdvertise.e;
            this.d = smartCardCpaAdvertise.c;
        }
    }
}
