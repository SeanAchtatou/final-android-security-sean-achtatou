package com.tencent.assistant.graphic;

import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public class a extends AnimationDrawable {

    /* renamed from: a  reason: collision with root package name */
    private int f1325a = 0;
    private boolean b = false;
    private int c = -1;
    private int d = 0;

    public void a(int i) {
        this.f1325a = i;
    }

    public void b(int i) {
        this.c = i;
    }

    public void draw(Canvas canvas) {
        if (!this.b) {
            super.draw(canvas);
        }
    }

    public boolean selectDrawable(int i) {
        boolean selectDrawable = super.selectDrawable(i);
        if (i != 0 && i == getNumberOfFrames() - 1 && this.c > 0) {
            this.d++;
            if (this.d > this.c - 1) {
                a();
            }
        }
        return selectDrawable;
    }

    private void a() {
        ba.a().postDelayed(new b(this), 0);
    }
}
