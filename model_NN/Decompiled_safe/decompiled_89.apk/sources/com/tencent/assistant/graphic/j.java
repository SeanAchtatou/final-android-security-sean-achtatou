package com.tencent.assistant.graphic;

import android.text.TextUtils;
import com.tencent.assistant.utils.r;
import java.io.File;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f1332a;
    final /* synthetic */ i b;

    j(i iVar, g gVar) {
        this.b = iVar;
        this.f1332a = gVar;
    }

    public void run() {
        if (this.f1332a != null && !TextUtils.isEmpty(this.f1332a.f1330a)) {
            int f = (int) (r.f() * 120.0f);
            if (this.f1332a.b == 0 || this.f1332a.b > f) {
                this.f1332a.b = f;
            }
            String a2 = this.b.a(this.f1332a.f1330a);
            File file = new File(a2);
            if (!file.exists()) {
                boolean unused = this.b.a(this.f1332a.f1330a, a2);
            }
            e eVar = null;
            if (file.exists()) {
                eVar = this.b.a(a2, this.f1332a.b, this.f1332a.c);
            }
            this.b.a(this.f1332a, eVar);
        }
    }
}
