package com.tencent.assistant.service;

import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.module.callback.g;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class a implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DockRubbishRelateService f2528a;

    a(DockRubbishRelateService dockRubbishRelateService) {
        this.f2528a = dockRubbishRelateService;
    }

    public void a() {
        XLog.d("DockRubbish", "DockRubbishRelateService >> 管家服务绑定成功，开始扫描...");
        long unused = this.f2528a.c = 0;
        long unused2 = this.f2528a.d = 0;
        long unused3 = this.f2528a.e = 0;
        long unused4 = this.f2528a.f = 0;
        long unused5 = this.f2528a.g = 0;
        this.f2528a.a();
        SpaceScanManager.a().a(this.f2528a.i);
    }

    public void b() {
        XLog.d("DockRubbish", "DockRubbishRelateService >> onBindTmsServiceFailed..");
        StringBuilder sb = new StringBuilder();
        sb.append("[status]\nresultcode=").append(-2);
        this.f2528a.a(sb.toString());
        this.f2528a.a(-2, false);
        this.f2528a.stopSelf();
    }

    public void c() {
        XLog.d("DockRubbish", "DockRubbishRelateService >> onTmsServiceDisconnected.");
        StringBuilder sb = new StringBuilder();
        sb.append("[status]\nresultcode=").append(-4);
        this.f2528a.a(sb.toString());
        this.f2528a.a(-4, false);
        this.f2528a.stopSelf();
    }
}
