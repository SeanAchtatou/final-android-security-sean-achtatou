package com.tencent.assistant.receiver;

import android.content.Intent;
import com.tencent.assistant.utils.l;

/* compiled from: ProGuard */
class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Intent f2447a;
    final /* synthetic */ BatteryStatusReceiver b;

    a(BatteryStatusReceiver batteryStatusReceiver, Intent intent) {
        this.b = batteryStatusReceiver;
        this.f2447a = intent;
    }

    public void run() {
        boolean z = false;
        int intExtra = this.f2447a.getIntExtra("level", 0);
        int intExtra2 = this.f2447a.getIntExtra("scale", 100);
        int intExtra3 = this.f2447a.getIntExtra("status", 1);
        if (intExtra3 == 2 || intExtra3 == 5) {
            z = true;
        }
        l.a((intExtra * 100) / intExtra2);
        l.a(z);
    }
}
