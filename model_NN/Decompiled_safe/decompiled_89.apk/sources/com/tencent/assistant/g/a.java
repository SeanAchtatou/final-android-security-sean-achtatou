package com.tencent.assistant.g;

import android.content.Context;
import com.qq.AppService.AstApp;
import com.tencent.assistant.activity.ShareBaseActivity;
import com.tencent.assistant.component.ShareAppBar;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.ch;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.model.ShareBaseModel;

/* compiled from: ProGuard */
public class a implements ShareAppBar.OnShareClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ShareAppBar f1308a;
    private ShareBaseActivity b;
    private ch c;
    private ShareAppModel d;
    private ShareBaseModel e;
    private boolean f = true;
    private String[] g = null;
    private int h = 0;
    private UIEventListener i = new b(this);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
     arg types: [com.tencent.assistant.activity.ShareBaseActivity, com.tencent.assistant.model.ShareAppModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistant.activity.ShareBaseActivity, com.tencent.assistant.model.ShareBaseModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void */
    public void shareToWX() {
        e e2 = e();
        if (e2 != null) {
            if (this.g != null) {
                e2.a(this.g);
            }
            if (this.f) {
                e2.a((Context) this.b, this.d, false);
            } else {
                e2.a((Context) this.b, this.e, false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
     arg types: [com.tencent.assistant.activity.ShareBaseActivity, com.tencent.assistant.model.ShareAppModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistant.activity.ShareBaseActivity, com.tencent.assistant.model.ShareBaseModel, int]
     candidates:
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.app.Activity, com.tencent.assistant.model.ShareBaseModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareAppModel, boolean):void
      com.tencent.assistant.g.e.a(android.content.Context, com.tencent.assistant.model.ShareBaseModel, boolean):void */
    public void shareToTimeLine() {
        e e2 = e();
        if (e2 != null) {
            if (this.g != null) {
                e2.a(this.g);
            }
            if (this.f) {
                e2.a((Context) this.b, this.d, true);
            } else {
                e2.a((Context) this.b, this.e, true);
            }
        }
    }

    public void shareToQZ() {
        e e2 = e();
        if (e2 != null) {
            if (this.g != null) {
                e2.a(this.g);
            }
            if (this.f) {
                e2.a(this.b, this.d);
            } else {
                e2.a(this.b, this.e);
            }
        }
    }

    public void shareToQQ() {
        e e2 = e();
        if (e2 != null) {
            if (this.g != null) {
                e2.a(this.g);
            }
            if (this.f) {
                e2.b(this.b, this.d);
            } else {
                e2.b(this.b, this.e);
            }
        }
    }

    public a(ShareBaseActivity shareBaseActivity, ShareAppBar shareAppBar) {
        this.b = shareBaseActivity;
        this.f1308a = shareAppBar;
        this.f1308a.setOnShareClickListener(this);
    }

    private void c() {
        this.c = new c(this);
        cq.a().a(this.c);
    }

    private void d() {
        cq.a().b(this.c);
    }

    public void a(ShareAppModel shareAppModel) {
        this.d = shareAppModel;
        this.f = true;
    }

    public void a(ShareBaseModel shareBaseModel) {
        this.e = shareBaseModel;
        this.f = false;
    }

    public void a(int i2) {
        this.h = i2;
    }

    public void a() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this.i);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this.i);
        e().b();
        if (this.f1308a.getVisibility() == 0) {
            this.f1308a.refreshState();
        }
        c();
    }

    public void b() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this.i);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this.i);
        e().a();
        d();
    }

    private e e() {
        if (this.h == 0) {
            return this.b.E();
        }
        return this.b.b(this.h);
    }
}
