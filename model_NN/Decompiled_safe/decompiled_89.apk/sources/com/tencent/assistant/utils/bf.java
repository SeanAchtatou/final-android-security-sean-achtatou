package com.tencent.assistant.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.qq.AppService.AstApp;
import java.util.HashMap;

/* compiled from: ProGuard */
public class bf {

    /* renamed from: a  reason: collision with root package name */
    private static HashMap<Integer, Bitmap> f2648a = new HashMap<>();

    public static Bitmap a(String str, BitmapFactory.Options options, int i) {
        if (i > 3) {
            return null;
        }
        try {
            return BitmapFactory.decodeFile(str, options);
        } catch (OutOfMemoryError e) {
            options.inSampleSize++;
            return a(str, options, i + 1);
        }
    }

    public static Bitmap a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = false;
        return a(bArr, options);
    }

    public static Bitmap a(byte[] bArr, BitmapFactory.Options options) {
        if (options == null) {
            options = new BitmapFactory.Options();
        }
        return a(bArr, options, 0);
    }

    public static Bitmap a(byte[] bArr, BitmapFactory.Options options, int i) {
        if (i > 5) {
            return null;
        }
        try {
            return BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        } catch (OutOfMemoryError e) {
            options.inSampleSize++;
            return a(bArr, options, i + 1);
        }
    }

    public static Bitmap a(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable.draw(canvas);
        return createBitmap;
    }

    public static Bitmap a(Bitmap bitmap, int i, int i2) {
        return a(bitmap, i, i2, Bitmap.Config.ARGB_8888);
    }

    public static Bitmap a(Bitmap bitmap, int i, int i2, Bitmap.Config config) {
        float f;
        float f2;
        int min = Math.min(i, i2);
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float f3 = 0.0f;
        if (width <= height) {
            f2 = (float) width;
            f = (float) width;
        } else {
            f3 = (float) ((width - height) / 2);
            f = ((float) width) - f3;
            f2 = (float) height;
        }
        Bitmap createBitmap = Bitmap.createBitmap(min, min, config);
        try {
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            Rect rect = new Rect((int) f3, 0, (int) f, (int) f2);
            Rect rect2 = new Rect(0, 0, min, min);
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(-12434878);
            canvas.drawCircle((float) (min / 2), (float) (min / 2), (float) ((min / 2) - 1), paint);
            try {
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            } catch (NoSuchFieldError e) {
            }
            canvas.drawBitmap(bitmap, rect, rect2, paint);
            return createBitmap;
        } catch (Throwable th) {
            return null;
        }
    }

    public static Bitmap a(int i) {
        if (f2648a.containsKey(Integer.valueOf(i))) {
            return f2648a.get(Integer.valueOf(i));
        }
        Bitmap a2 = a(AstApp.i().getResources().getDrawable(i));
        if (a2 == null) {
            return a2;
        }
        Bitmap a3 = a(a2, a2.getWidth(), a2.getHeight());
        if (a3 != a2) {
            a2.recycle();
        }
        f2648a.put(Integer.valueOf(i), a3);
        return a3;
    }
}
