package com.tencent.assistant.utils;

import android.os.Message;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.optimize.f;
import org.json.JSONArray;

/* compiled from: ProGuard */
class bn extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bk f2654a;

    bn(bk bkVar) {
        this.f2654a = bkVar;
    }

    public void c() {
        XLog.i(this.f2654a.b, ">.space clean start scan");
    }

    public void a(int i) {
        XLog.i(this.f2654a.b, ">.space clean onScanProgressChanged=" + i);
    }

    public void b() {
        XLog.i(this.f2654a.b, "..space scan finish>>");
        if (this.f2654a.h) {
            boolean unused = this.f2654a.h = false;
            Message obtainMessage = AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_SUCCESS);
            obtainMessage.obj = Long.valueOf(this.f2654a.c);
            AstApp.i().j().dispatchMessage(obtainMessage);
            SpaceScanManager.a().a(new bo(this), this.f2654a.g);
        }
    }

    public void a() {
        XLog.i(this.f2654a.b, "..space scan onScanCanceled>>");
        AstApp.i().j().dispatchMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_FAIL));
    }

    public void a(int i, DataEntity dataEntity) {
        try {
            if (dataEntity.getBoolean("rubbish.suggest") && i != 2) {
                long j = dataEntity.getLong("rubbish.size");
                JSONArray jSONArray = dataEntity.getJSONArray("rubbish.path.array");
                bk.a(this.f2654a, j);
                if (jSONArray != null && jSONArray.length() > 0) {
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        Object obj = jSONArray.get(i2);
                        if (obj != null) {
                            this.f2654a.g.add(obj.toString());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
