package com.tencent.assistant.utils;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class aj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f2631a;
    final /* synthetic */ Dialog b;

    aj(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f2631a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f2631a != null) {
            this.f2631a.onRightBtnClick();
            this.b.dismiss();
        }
    }
}
