package com.tencent.assistant.utils.installuninstall;

import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cr;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f2714a;

    w(v vVar) {
        this.f2714a = vVar;
    }

    public void run() {
        boolean a2 = InstallUninstallUtil.a();
        boolean c = cr.a().c();
        XLog.d("InstallUninstallHelper", "handle root switch, permRootReqResult=" + a2 + ", tempRootReqResult=" + c);
        if (a2 || c) {
            m.a().b(true);
            this.f2714a.f2713a.style = c ? 2 : 1;
            if (this.f2714a.f2713a.action == 1) {
                ac.a().b(this.f2714a.f2713a);
            } else if (this.f2714a.f2713a.action == -1) {
                TemporaryThreadManager.get().start(new x(this));
            }
        } else {
            Toast.makeText(AstApp.i().getBaseContext(), AstApp.i().getBaseContext().getString(R.string.toast_root_request_fail), 1).show();
            if (this.f2714a.f2713a.action == 1) {
                ac.a().a(this.f2714a.f2713a);
            } else if (this.f2714a.f2713a.action == -1 && this.f2714a.f2713a.trySystemAfterSilentFail) {
                this.f2714a.b.f2708a.sendMessage(this.f2714a.b.f2708a.obtainMessage(1024, this.f2714a.f2713a.packageName));
                InstallUninstallUtil.a(this.f2714a.f2713a.packageName);
            }
        }
    }
}
