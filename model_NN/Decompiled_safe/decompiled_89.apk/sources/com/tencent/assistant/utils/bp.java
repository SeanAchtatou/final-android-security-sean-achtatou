package com.tencent.assistant.utils;

import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.localres.callback.c;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.List;

/* compiled from: ProGuard */
class bp extends c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bk f2656a;

    bp(bk bkVar) {
        this.f2656a = bkVar;
    }

    public void a(List<LocalApkInfo> list, boolean z, boolean z2, int i) {
        if (i == 0) {
            this.f2656a.a(list);
            return;
        }
        AstApp.i().j().dispatchMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_APK_DEL_FAIL));
    }

    public void a(List<LocalApkInfo> list) {
    }

    public void a(List<LocalApkInfo> list, LocalApkInfo localApkInfo, boolean z, boolean z2) {
    }

    public void a(List<LocalApkInfo> list, int i, long j) {
    }

    public void b(List<LocalApkInfo> list, int i, long j) {
    }

    public void a(int i, int i2, long j) {
    }
}
