package com.tencent.assistant.utils;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class ct {

    /* renamed from: a  reason: collision with root package name */
    public static final Spanned f2674a = new SpannableString(Constants.STR_EMPTY);
    private static String[] b = new String[6];

    static {
        try {
            b[0] = AstApp.i().getString(R.string.download);
            b[1] = AstApp.i().getString(R.string.update);
            b[2] = AstApp.i().getString(R.string.empty_string);
            b[3] = AstApp.i().getString(R.string.bout);
            b[4] = AstApp.i().getString(R.string.ten_thousnd);
            b[5] = AstApp.i().getString(R.string.one_hundred_million);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static String a(String str) {
        if (str == null) {
            return null;
        }
        return Pattern.compile("\\s*|\t|\r|\n").matcher(str).replaceAll(Constants.STR_EMPTY);
    }

    public static String a(long j, int i) {
        double d;
        DecimalFormat decimalFormat;
        if (j < 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        if (j < 10000) {
            decimalFormat = new DecimalFormat("#0");
            d = (double) j;
            sb2.append(b[3]);
        } else if (j < 100000) {
            d = ((double) j) / 10000.0d;
            decimalFormat = new DecimalFormat("#0.0");
            sb2.append(b[4]);
        } else if (j < 100000000) {
            d = (double) (j / 10000);
            decimalFormat = new DecimalFormat("#0");
            sb2.append(b[4]);
        } else if (j < 1000000000) {
            d = ((double) j) / 1.0E8d;
            decimalFormat = new DecimalFormat("#0.0");
            sb2.append(b[5]);
        } else {
            d = (double) (j / 100000000);
            decimalFormat = new DecimalFormat("#0");
            sb2.append(b[5]);
        }
        sb2.append(b[i % 3]);
        return sb.append(decimalFormat.format(d)).append((CharSequence) sb2).toString();
    }

    public static String a(long j) {
        double d;
        DecimalFormat decimalFormat;
        if (j < 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        if (j < 10000) {
            decimalFormat = new DecimalFormat("#0");
            d = (double) j;
        } else if (j < 100000) {
            d = ((double) j) / 10000.0d;
            decimalFormat = new DecimalFormat("#0.0");
            sb2.append("万");
        } else if (j < 100000000) {
            d = (double) (j / 10000);
            decimalFormat = new DecimalFormat("#0");
            sb2.append("万");
        } else if (j < 1000000000) {
            d = ((double) j) / 1.0E8d;
            decimalFormat = new DecimalFormat("#0.0");
            sb2.append("亿");
        } else {
            d = (double) (j / 100000000);
            decimalFormat = new DecimalFormat("#0");
            sb2.append("亿");
        }
        return sb.append(decimalFormat.format(d)).append((CharSequence) sb2).toString();
    }

    public static String a(List<String> list) {
        StringBuilder sb = new StringBuilder();
        for (String str : list) {
            sb.append(str + ",");
        }
        return sb.toString();
    }

    public static ArrayList<String> b(String str) {
        ArrayList<String> arrayList = new ArrayList<>();
        if (!TextUtils.isEmpty(str)) {
            String[] split = str.split(",");
            for (String str2 : split) {
                if (!TextUtils.isEmpty(str2)) {
                    arrayList.add(str2);
                }
            }
        }
        return arrayList;
    }

    public static boolean b(List<String> list) {
        return list != null && list.size() > 0 && !TextUtils.isEmpty(list.get(0));
    }

    public static String a(double d) {
        return Math.round(d) + AstApp.i().getString(R.string.down_page_speed_unit);
    }

    public static boolean a(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str != null) {
            return str.equals(str2);
        }
        return false;
    }

    public static long c(String str) {
        try {
            return Long.parseLong(str);
        } catch (Exception e) {
            return 0;
        }
    }

    public static int d(String str) {
        return a(str, 0);
    }

    public static int a(String str, int i) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return i;
        }
    }

    public static String a(int i) {
        if (i < 0) {
            return "000";
        }
        StringBuilder sb = new StringBuilder();
        if (i < 10) {
            sb.append(STConst.ST_STATUS_DEFAULT);
        } else if (i < 100) {
            sb.append("0");
        }
        sb.append(i);
        return sb.toString();
    }

    public static String b(int i) {
        if (i < 0) {
            return STConst.ST_STATUS_DEFAULT;
        }
        return String.format("%02d", Integer.valueOf(i));
    }

    public static String[] b(String str, String str2) {
        if (str == null || str2 == null || str2.length() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int indexOf = str.indexOf(str2, i);
            if (indexOf < 0) {
                break;
            }
            arrayList.add(str.substring(i, indexOf));
            i = str2.length() + indexOf;
        }
        arrayList.add(str.substring(i));
        int size = arrayList.size() - 1;
        while (size >= 0 && ((String) arrayList.get(size)).length() == 0) {
            arrayList.remove(size);
            size--;
        }
        return (String[]) arrayList.toArray(new String[0]);
    }

    public static String e(String str) {
        String str2;
        if (str == null) {
            return str;
        }
        String[] split = str.split("\n");
        String str3 = Constants.STR_EMPTY;
        for (String str4 : split) {
            if (str4 != null) {
                String trim = str4.trim();
                int i = 0;
                while (i < trim.length() && trim.charAt(i) == 12288) {
                    i++;
                }
                if (i < trim.length()) {
                    str2 = trim.substring(i);
                } else {
                    str2 = trim;
                }
                str3 = str3 + str2 + "\n";
            }
        }
        return str3;
    }

    public static String f(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        int i = 0;
        while (i < trim.length() && trim.charAt(i) == 12288) {
            i++;
        }
        if (i < trim.length()) {
            return trim.substring(i);
        }
        return trim;
    }
}
