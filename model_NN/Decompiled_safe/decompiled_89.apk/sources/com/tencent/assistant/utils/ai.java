package com.tencent.assistant.utils;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f2630a;
    final /* synthetic */ Dialog b;

    ai(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f2630a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f2630a != null) {
            try {
                this.f2630a.onLeftBtnClick();
                this.b.dismiss();
            } catch (Throwable th) {
            }
        }
    }
}
