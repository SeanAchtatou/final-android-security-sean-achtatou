package com.tencent.assistant.utils;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class aa implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.OneBtnDialogInfo f2623a;

    aa(AppConst.OneBtnDialogInfo oneBtnDialogInfo) {
        this.f2623a = oneBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f2623a.onCancell();
    }
}
