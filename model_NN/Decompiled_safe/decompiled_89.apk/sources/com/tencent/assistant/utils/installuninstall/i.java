package com.tencent.assistant.utils.installuninstall;

import com.tencent.assistant.manager.DownloadProxy;

/* compiled from: ProGuard */
class i extends o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallDialogManager f2703a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(InstallUninstallDialogManager installUninstallDialogManager) {
        super(installUninstallDialogManager);
        this.f2703a = installUninstallDialogManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public void onLeftBtnClick() {
        boolean unused = this.f2703a.c = false;
        this.f2703a.c();
        DownloadProxy.a().b(this.b.downloadTicket, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onRightBtnClick() {
        boolean unused = this.f2703a.c = false;
        this.f2703a.c();
        this.f2703a.b(this.b.downloadTicket);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onCancell() {
        boolean unused = this.f2703a.c = false;
        this.f2703a.c();
    }
}
