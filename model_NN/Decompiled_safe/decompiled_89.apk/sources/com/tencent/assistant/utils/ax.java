package com.tencent.assistant.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
final class ax implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f2641a;
    final /* synthetic */ Activity b;

    ax(Dialog dialog, Activity activity) {
        this.f2641a = dialog;
        this.b = activity;
    }

    public void onClick(View view) {
        this.f2641a.dismiss();
        if (m.a().y()) {
            boolean unused = FunctionUtils.e(this.b);
            this.b.finish();
            return;
        }
        this.b.finish();
        AstApp.i().f();
    }
}
