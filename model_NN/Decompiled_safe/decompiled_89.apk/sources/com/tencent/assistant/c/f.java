package com.tencent.assistant.c;

import android.graphics.drawable.AnimationDrawable;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AnimationDrawable f869a;
    final /* synthetic */ d b;

    f(d dVar, AnimationDrawable animationDrawable) {
        this.b = dVar;
        this.f869a = animationDrawable;
    }

    public void run() {
        int i;
        int i2;
        if (this.f869a != null) {
            float f = AstApp.i().getResources().getDisplayMetrics().density;
            int intrinsicWidth = this.f869a.getIntrinsicWidth();
            int intrinsicHeight = this.f869a.getIntrinsicHeight();
            int i3 = this.b.c != null ? this.b.c.j : 0;
            if (i3 > 0) {
                int i4 = intrinsicHeight / i3;
                i = intrinsicWidth / i3;
                i2 = i4;
            } else {
                i = intrinsicWidth;
                i2 = intrinsicHeight;
            }
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.b.d.getLayoutParams();
            if (i <= 0 || i2 <= 0) {
                layoutParams.width = df.b(97.0f);
                layoutParams.height = df.b(100.0f);
            } else {
                layoutParams.width = df.b((float) i);
                layoutParams.height = df.b((float) i2);
            }
            layoutParams.setMargins(0, 0, df.b(7.0f), df.b(7.0f));
            this.b.d.setLayoutParams(layoutParams);
            this.b.d.setBackgroundDrawable(this.f869a);
            this.f869a.start();
        }
    }
}
