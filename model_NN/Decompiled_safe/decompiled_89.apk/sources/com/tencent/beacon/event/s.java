package com.tencent.beacon.event;

import android.content.Context;
import com.tencent.beacon.a.a.a;
import com.tencent.beacon.a.a.e;
import com.tencent.beacon.a.h;
import java.util.HashMap;

/* compiled from: ProGuard */
public final class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f3442a;

    public s(Context context) {
        this.f3442a = context;
    }

    public final void run() {
        e o = h.o(this.f3442a);
        if (o != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("A43", new StringBuilder().append(o.c()).toString());
            hashMap.put("A44", new StringBuilder().append(o.d()).toString());
            hashMap.put("A41", new StringBuilder().append(o.a()).toString());
            hashMap.put("A42", new StringBuilder().append(o.b()).toString());
            t.a("rqd_useInfoEvent", true, 0, 0, hashMap, true);
            Context context = this.f3442a;
            if (context != null) {
                a.a(context, new int[]{8}, -1, Long.MAX_VALUE);
            }
            com.tencent.beacon.d.a.e("%s %d %d", "rqd_useInfoEvent", Long.valueOf(o.a()), Long.valueOf(o.b()));
        }
    }
}
