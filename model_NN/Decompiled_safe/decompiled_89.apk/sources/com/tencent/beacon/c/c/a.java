package com.tencent.beacon.c.c;

import com.tencent.beacon.e.c;
import com.tencent.beacon.e.d;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public final class a extends c implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public String f3413a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public String e = Constants.STR_EMPTY;

    public final void a(d dVar) {
        if (this.f3413a != null) {
            dVar.a(this.f3413a, 0);
        }
        if (this.b != null) {
            dVar.a(this.b, 1);
        }
        if (this.c != null) {
            dVar.a(this.c, 2);
        }
        if (this.d != null) {
            dVar.a(this.d, 3);
        }
        if (this.e != null) {
            dVar.a(this.e, 4);
        }
    }

    public final void a(com.tencent.beacon.e.a aVar) {
        this.f3413a = aVar.b(0, false);
        this.b = aVar.b(1, false);
        this.c = aVar.b(2, false);
        this.d = aVar.b(3, false);
        this.e = aVar.b(4, false);
    }
}
