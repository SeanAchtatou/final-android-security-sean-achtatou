package com.tencent.smtt.export.external.extension.interfaces;

import android.os.Bundle;

public interface IX5WebViewClientExtension {
    public static final int FRAME_LOADTYPE_BACK = 1;
    public static final int FRAME_LOADTYPE_BACKWMLDECKNOTACCESSIBLE = 10;
    public static final int FRAME_LOADTYPE_FORWARD = 2;
    public static final int FRAME_LOADTYPE_INDEXEDBACKFORWARD = 3;
    public static final int FRAME_LOADTYPE_PREREAD = 11;
    public static final int FRAME_LOADTYPE_REDIRECT = 7;
    public static final int FRAME_LOADTYPE_RELOAD = 4;
    public static final int FRAME_LOADTYPE_RELOADALLOWINGSTALEDATA = 5;
    public static final int FRAME_LOADTYPE_RELOADFROMORIGIN = 9;
    public static final int FRAME_LOADTYPE_REPLACE = 8;
    public static final int FRAME_LOADTYPE_SAME = 6;
    public static final int FRAME_LOADTYPE_STANDARD = 0;

    void handlePluginTag(String str, String str2, boolean z, String str3);

    void hideAddressBar();

    void onDoubleTapStart();

    void onFlingScrollBegin(int i, int i2, int i3);

    void onFlingScrollEnd();

    void onHideListBox();

    void onHistoryItemChanged();

    void onInputBoxTextChanged(IX5WebViewExtension iX5WebViewExtension, String str);

    Object onMiscCallBack(String str, Bundle bundle);

    void onMissingPluginClicked(String str, String str2, String str3, int i);

    void onNativeCrashReport(int i, String str);

    void onPinchToZoomStart();

    void onPreReadFinished();

    void onPromptScaleSaved();

    void onReportAdFilterInfo(int i, int i2, String str, boolean z);

    void onScrollChanged(int i, int i2, int i3, int i4);

    void onSetButtonStatus(boolean z, boolean z2);

    void onShowListBox(String[] strArr, int[] iArr, int[] iArr2, int i);

    void onShowMutilListBox(String[] strArr, int[] iArr, int[] iArr2, int[] iArr3);

    void onSlidingTitleOffScreen(int i, int i2);

    void onSoftKeyBoardHide(int i);

    void onSoftKeyBoardShow();

    void onTransitionToCommitted();

    void onUploadProgressChange(int i, int i2, String str);

    void onUploadProgressStart(int i);

    void onUrlChange(String str, String str2);

    boolean preShouldOverrideUrlLoading(IX5WebViewExtension iX5WebViewExtension, String str);
}
