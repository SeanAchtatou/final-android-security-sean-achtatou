package com.tencent.smtt.export.external.proxy;

import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.webkit.ValueCallback;
import com.tencent.smtt.export.external.interfaces.ConsoleMessage;
import com.tencent.smtt.export.external.interfaces.GeolocationPermissionsCallback;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.IX5WebViewBase;
import com.tencent.smtt.export.external.interfaces.JsPromptResult;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.export.external.interfaces.QuotaUpdater;

public class ProxyWebChromeClient implements IX5WebChromeClient {
    protected IX5WebChromeClient mWebChromeClient;

    public IX5WebChromeClient getmWebChromeClient() {
        return this.mWebChromeClient;
    }

    public void setWebChromeClient(IX5WebChromeClient webChromeClient) {
        this.mWebChromeClient = webChromeClient;
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onConsoleMessage(consoleMessage);
        }
        return false;
    }

    public boolean onCreateWindow(IX5WebViewBase view, boolean dialog, boolean userGesture, Message resultMsg) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onCreateWindow(view, dialog, userGesture, resultMsg);
        }
        return false;
    }

    public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissionsCallback callback) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onGeolocationPermissionsShowPrompt(origin, callback);
        }
    }

    public void onHideCustomView() {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onHideCustomView();
        }
    }

    public boolean onJsAlert(IX5WebViewBase view, String url, String message, JsResult result) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onJsAlert(view, url, message, result);
        }
        return false;
    }

    public boolean onJsPrompt(IX5WebViewBase view, String url, String message, String defaultValue, JsPromptResult result) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onJsPrompt(view, url, message, defaultValue, result);
        }
        return false;
    }

    public boolean onJsBeforeUnload(IX5WebViewBase view, String url, String message, JsResult result) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onJsBeforeUnload(view, url, message, result);
        }
        return false;
    }

    public boolean onJsTimeout() {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onJsTimeout();
        }
        return false;
    }

    public void onProgressChanged(IX5WebViewBase view, int newProgress) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onProgressChanged(view, newProgress);
        }
    }

    public void onReachedMaxAppCacheSize(long spaceNeeded, long totalUsedQuota, QuotaUpdater quotaUpdater) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onReachedMaxAppCacheSize(spaceNeeded, totalUsedQuota, quotaUpdater);
        }
    }

    public void onReceivedIcon(IX5WebViewBase view, Bitmap icon) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onReceivedIcon(view, icon);
        }
    }

    public void onReceivedTouchIconUrl(IX5WebViewBase view, String url, boolean precomposed) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onReceivedTouchIconUrl(view, url, precomposed);
        }
    }

    public void onReceivedTitle(IX5WebViewBase view, String title) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onReceivedTitle(view, title);
        }
    }

    public void onRequestFocus(IX5WebViewBase view) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onRequestFocus(view);
        }
    }

    public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback callback) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onShowCustomView(view, callback);
        }
    }

    public void onShowCustomView(View view, int requestedOrientation, IX5WebChromeClient.CustomViewCallback callback) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onShowCustomView(view, callback);
        }
    }

    public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, QuotaUpdater quotaUpdater) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onExceededDatabaseQuota(url, databaseIdentifier, currentQuota, estimatedSize, totalUsedQuota, quotaUpdater);
        }
    }

    public Bitmap getDefaultVideoPoster() {
        return null;
    }

    public void onConsoleMessage(String message, int lineNumber, String sourceID) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onConsoleMessage(message, lineNumber, sourceID);
        }
    }

    public void onGeolocationPermissionsHidePrompt() {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onGeolocationPermissionsHidePrompt();
        }
    }

    public boolean onJsConfirm(IX5WebViewBase view, String url, String message, JsResult result) {
        if (this.mWebChromeClient != null) {
            return this.mWebChromeClient.onJsConfirm(view, url, message, result);
        }
        return false;
    }

    public void onCloseWindow(IX5WebViewBase window) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onCloseWindow(window);
        }
    }

    public void getVisitedHistory(ValueCallback<String[]> callback) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.getVisitedHistory(callback);
        }
    }

    public void openFileChooser(ValueCallback<Uri[]> uploadFile, String acceptType, String captureType, boolean multiFiles) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.openFileChooser(uploadFile, acceptType, captureType, multiFiles);
        }
    }

    public void onGeolocationStartUpdating(ValueCallback<Location> onLocationChangedCallback, ValueCallback<Bundle> onErrorCallback) {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onGeolocationStartUpdating(onLocationChangedCallback, onErrorCallback);
        }
    }

    public void onGeolocationStopUpdating() {
        if (this.mWebChromeClient != null) {
            this.mWebChromeClient.onGeolocationStopUpdating();
        }
    }
}
