package com.tencent.smtt.sdk;

import android.content.Intent;
import android.net.Uri;
import com.tencent.smtt.export.external.interfaces.DownloadListener;
import com.tencent.smtt.sdk.stat.MttLoader;

class DownLoadListenerAdapter implements DownloadListener {
    private DownloadListener mListener;
    private WebView mWebView;

    public DownLoadListenerAdapter(WebView webview, DownloadListener listener, boolean isX5Core) {
        this.mListener = listener;
        this.mWebView = webview;
    }

    public void onDownloadStart(String url, String method, byte[] postData, String userAgent, String contentDisposition, String mimetype, long contentLength, String referUrl, String urlBeforeRedirect) {
        if (this.mListener != null) {
            this.mListener.onDownloadStart(url, userAgent, contentDisposition, mimetype, contentLength);
        } else if (QbSdk.canOpenMimeFileType(this.mWebView.getContext(), mimetype)) {
            Intent i = new Intent("com.tencent.QQBrowser.action.sdk.document");
            i.setFlags(268435456);
            i.putExtra("key_reader_sdk_url", url);
            i.putExtra("key_reader_sdk_type", 1);
            i.setData(Uri.parse(url));
            this.mWebView.getContext().startActivity(i);
        } else {
            MttLoader.loadUrl(this.mWebView.getContext(), url, null);
        }
    }

    public void onDownloadVideo(String url, long len, int videoType) {
    }
}
