package com.tencent.smtt.sdk;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.view.View;
import android.webkit.ValueCallback;
import com.tencent.smtt.export.external.WebViewWizardBase;
import com.tencent.smtt.export.external.interfaces.ConsoleMessage;
import com.tencent.smtt.export.external.interfaces.GeolocationPermissionsCallback;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.IX5WebViewBase;
import com.tencent.smtt.export.external.interfaces.JsPromptResult;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.export.external.interfaces.QuotaUpdater;
import com.tencent.smtt.export.external.proxy.X5ProxyWebChromeClient;
import com.tencent.smtt.sdk.WebView;

class SmttWebChromeClient extends X5ProxyWebChromeClient {
    private WebChromeClient mChromeClient;
    private WebView mWebView;

    public SmttWebChromeClient(WebViewWizardBase wizard, WebView webView, WebChromeClient chromeClient) {
        super(wizard);
        this.mWebView = webView;
        this.mChromeClient = chromeClient;
    }

    public void getVisitedHistory(ValueCallback<String[]> valueCallback) {
    }

    public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, QuotaUpdater quotaUpdater) {
        this.mChromeClient.onExceededDatabaseQuota(url, databaseIdentifier, currentQuota, estimatedSize, totalUsedQuota, quotaUpdater);
    }

    public Bitmap getDefaultVideoPoster() {
        return this.mChromeClient.getDefaultVideoPoster();
    }

    public void onCloseWindow(IX5WebViewBase window) {
        this.mWebView.setX5WebView(window);
        this.mChromeClient.onCloseWindow(this.mWebView);
    }

    public void onConsoleMessage(String message, int lineNumber, String sourceID) {
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        return this.mChromeClient.onConsoleMessage(consoleMessage);
    }

    public boolean onCreateWindow(IX5WebViewBase view, boolean dialog, boolean userGesture, final Message resultMsg) {
        WebView webView = this.mWebView;
        webView.getClass();
        final WebView.WebViewTransport transport = new WebView.WebViewTransport();
        Message wrapper = Message.obtain(resultMsg.getTarget(), new Runnable() {
            public void run() {
                WebView newWebView = transport.getWebView();
                if (newWebView != null) {
                    ((IX5WebViewBase.WebViewTransport) resultMsg.obj).setWebView(newWebView.getX5WebView());
                }
                resultMsg.sendToTarget();
            }
        });
        wrapper.obj = transport;
        return this.mChromeClient.onCreateWindow(this.mWebView, dialog, userGesture, wrapper);
    }

    public void onGeolocationPermissionsHidePrompt() {
        this.mChromeClient.onGeolocationPermissionsHidePrompt();
    }

    public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissionsCallback callback) {
        this.mChromeClient.onGeolocationPermissionsShowPrompt(origin, callback);
    }

    public void onHideCustomView() {
        this.mChromeClient.onHideCustomView();
    }

    public boolean onJsAlert(IX5WebViewBase view, String url, String message, JsResult result) {
        this.mWebView.setX5WebView(view);
        return this.mChromeClient.onJsAlert(this.mWebView, url, message, result);
    }

    public boolean onJsConfirm(IX5WebViewBase view, String url, String message, JsResult result) {
        this.mWebView.setX5WebView(view);
        return this.mChromeClient.onJsConfirm(this.mWebView, url, message, result);
    }

    public boolean onJsPrompt(IX5WebViewBase view, String url, String message, String defaultValue, JsPromptResult result) {
        this.mWebView.setX5WebView(view);
        return this.mChromeClient.onJsPrompt(this.mWebView, url, message, defaultValue, result);
    }

    public boolean onJsBeforeUnload(IX5WebViewBase view, String url, String message, JsResult result) {
        this.mWebView.setX5WebView(view);
        return this.mChromeClient.onJsBeforeUnload(this.mWebView, url, message, result);
    }

    public boolean onJsTimeout() {
        return this.mChromeClient.onJsTimeout();
    }

    public void onProgressChanged(IX5WebViewBase view, int newProgress) {
        this.mWebView.setX5WebView(view);
        this.mChromeClient.onProgressChanged(this.mWebView, newProgress);
    }

    public void onReachedMaxAppCacheSize(long spaceNeeded, long totalUsedQuota, QuotaUpdater quotaUpdater) {
        this.mChromeClient.onReachedMaxAppCacheSize(spaceNeeded, totalUsedQuota, quotaUpdater);
    }

    public void onReceivedIcon(IX5WebViewBase view, Bitmap icon) {
        this.mWebView.setX5WebView(view);
        this.mChromeClient.onReceivedIcon(this.mWebView, icon);
    }

    public void onReceivedTouchIconUrl(IX5WebViewBase view, String url, boolean precomposed) {
        this.mWebView.setX5WebView(view);
        this.mChromeClient.onReceivedTouchIconUrl(this.mWebView, url, precomposed);
    }

    public void onReceivedTitle(IX5WebViewBase view, String title) {
        this.mWebView.setX5WebView(view);
        this.mChromeClient.onReceivedTitle(this.mWebView, title);
        this.mWebView.hideSplashLogo();
    }

    public void onRequestFocus(IX5WebViewBase view) {
        this.mWebView.setX5WebView(view);
        this.mChromeClient.onRequestFocus(this.mWebView);
    }

    public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback callback) {
        this.mChromeClient.onShowCustomView(view, callback);
    }

    public void onShowCustomView(View view, int requestedOrientation, IX5WebChromeClient.CustomViewCallback callback) {
        this.mChromeClient.onShowCustomView(view, requestedOrientation, callback);
    }

    public void openFileChooser(final ValueCallback<Uri[]> uploadFile, String acceptType, String capture, boolean multiFiles) {
        this.mChromeClient.openFileChooser(new ValueCallback<Uri>() {
            public void onReceiveValue(Uri value) {
                uploadFile.onReceiveValue(new Uri[]{value});
            }
        }, acceptType, capture);
    }
}
