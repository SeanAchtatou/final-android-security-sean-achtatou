package com.tencent.tmsecurelite.c;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.tencent.tmsecurelite.commom.d;
import com.tencent.tmsecurelite.optimize.ISystemOptimize;

/* compiled from: ProGuard */
public abstract class c extends Binder implements a {
    public c() {
        attachInterface(this, "com.tencent.tmsecurelite.IPassWordSystem");
    }

    public static a a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IPassWordSystem");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof a)) {
            return new b(iBinder);
        }
        return (a) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        boolean z = false;
        switch (i) {
            case 1:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                String b = b();
                parcel2.writeNoException();
                parcel2.writeString(b);
                break;
            case 2:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                a(parcel.readString());
                parcel2.writeNoException();
                break;
            case 3:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                int c = c();
                parcel2.writeNoException();
                parcel2.writeInt(c);
                break;
            case 4:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                a(parcel.readInt());
                parcel2.writeNoException();
                break;
            case 5:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                String d = d();
                parcel2.writeNoException();
                parcel2.writeString(d);
                break;
            case 6:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                c(parcel.readString());
                parcel2.writeNoException();
                break;
            case 7:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                String e = e();
                parcel2.writeNoException();
                parcel2.writeString(e);
                break;
            case 8:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                e(parcel.readString());
                parcel2.writeNoException();
                break;
            case 9:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                boolean c2 = c(parcel.readInt());
                parcel2.writeNoException();
                if (!c2) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                break;
            case 10:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                int a2 = a();
                parcel2.writeNoException();
                parcel2.writeInt(a2);
                break;
            case 11:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                boolean f = f();
                parcel2.writeNoException();
                if (!f) {
                    z = true;
                }
                parcel2.writeInt(z ? 1 : 0);
                break;
            case 12:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                if (parcel.readInt() != 0) {
                    z = true;
                }
                a(z);
                parcel2.writeNoException();
                break;
            case 13:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                b(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 14:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                int b2 = b(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeInt(b2);
                break;
            case 15:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                c(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 16:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                int b3 = b(parcel.readInt());
                parcel2.writeNoException();
                parcel2.writeInt(b3);
                break;
            case 17:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                d(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 18:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                int d2 = d(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeInt(d2);
                break;
            case 19:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                e(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 20:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                int f2 = f(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeInt(f2);
                break;
            case ISystemOptimize.T_killTaskAsync /*22*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                a(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case 23:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                f(d.a(parcel.readStrongBinder()));
                parcel2.writeNoException();
                break;
            case ISystemOptimize.T_checkVersionAsync /*24*/:
                parcel.enforceInterface("com.tencent.tmsecurelite.IPassWordSystem");
                if (parcel.readInt() == 0) {
                    z = true;
                }
                int b4 = b(z);
                parcel2.writeNoException();
                parcel2.writeInt(b4);
                break;
        }
        return true;
    }

    public boolean c(int i) {
        return 1 >= i;
    }
}
