package com.tencent.tmsecurelite.commom;

import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import com.tencent.assistant.AppConst;
import com.tencent.tmsecurelite.a.j;
import com.tencent.tmsecurelite.b.b;
import com.tencent.tmsecurelite.optimize.h;
import com.tencent.tmsecurelite.virusscan.c;

/* compiled from: ProGuard */
public class ServiceManager {
    public static final String SERVICE_TYPE = "service_type";
    public static final int TYPE_DISTURB_INTERCEPT = 2;
    public static final int TYPE_FILE_SAFE = 3;
    public static final int TYPE_PASSWORD_SYSTEM = 4;
    public static final int TYPE_PAY_SECURE = 6;
    public static final int TYPE_ROOT_SERVICE = 5;
    public static final int TYPE_SYSTEM_OPTIMIZE = 0;
    public static final int TYPE_VIRUS_SCAN = 1;

    public static final Intent getIntent(int i) {
        Intent intent = new Intent();
        intent.setPackage(AppConst.TENCENT_MOBILE_MANAGER_PKGNAME);
        intent.setAction("com.tencent.qqpimsecure.TMS_LITE_SERVICE");
        intent.putExtra(SERVICE_TYPE, i);
        return intent;
    }

    public static final IInterface getInterface(int i, IBinder iBinder) {
        switch (i) {
            case 0:
                return h.a(iBinder);
            case 1:
                return c.a(iBinder);
            case 2:
                return b.a(iBinder);
            case 3:
                return j.a(iBinder);
            case 4:
                return com.tencent.tmsecurelite.c.c.a(iBinder);
            case 5:
                return com.tencent.tmsecurelite.e.c.a(iBinder);
            case 6:
                return com.tencent.tmsecurelite.d.c.a(iBinder);
            default:
                return null;
        }
    }
}
