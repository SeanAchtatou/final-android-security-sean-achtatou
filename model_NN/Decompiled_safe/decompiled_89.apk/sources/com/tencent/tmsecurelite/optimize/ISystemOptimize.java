package com.tencent.tmsecurelite.optimize;

import android.os.IInterface;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public interface ISystemOptimize extends IInterface {
    public static final String INTERFACE = "com.tencent.tmsecurelite.ISecureService";
    public static final int T_askForRoot = 11;
    public static final int T_askForRootAsync = 26;
    public static final int T_cancelScanRubbish = 33;
    public static final int T_checkVersion = 9;
    public static final int T_checkVersionAsync = 24;
    public static final int T_cleanRubbishAsync = 34;
    public static final int T_cleanSysRubbish = 15;
    public static final int T_cleanSysRubbishAsync = 30;
    public static final int T_clearAppsCache = 2;
    public static final int T_clearAppsCacheAsync = 17;
    public static final int T_findAppsWithAutoboot = 3;
    public static final int T_findAppsWithAutobootAsync = 18;
    public static final int T_findAppsWithCache = 1;
    public static final int T_findAppsWithCacheAsync = 16;
    public static final int T_getAllRuningTask = 6;
    public static final int T_getAllRuningTaskAsync = 21;
    public static final int T_getMemoryUsage = 12;
    public static final int T_getMemoryUsageAsync = 27;
    public static final int T_getSysRubbish = 14;
    public static final int T_getSysRubbishAsync = 29;
    public static final int T_hasRoot = 10;
    public static final int T_hasRootAsync = 25;
    public static final int T_isMemoryReachWarning = 13;
    public static final int T_isMemoryReachWarningAsync = 28;
    public static final int T_killTask = 7;
    public static final int T_killTaskAsync = 22;
    public static final int T_killTasks = 8;
    public static final int T_killTasksAsync = 23;
    public static final int T_setAutobootState = 4;
    public static final int T_setAutobootStateAsync = 19;
    public static final int T_setAutobootStates = 5;
    public static final int T_setAutobootStatesAsync = 20;
    public static final int T_startScanRubbish = 32;
    public static final int T_updateTmsConfigAsync = 31;
    public static final int VERSION = 2;

    @Deprecated
    boolean askForRoot();

    void askForRootAsync(b bVar);

    int cancelScanRubbish();

    @Deprecated
    boolean checkVersion(int i);

    void cleanRubbishAsync(b bVar, ArrayList<String> arrayList);

    @Deprecated
    void cleanSysRubbish();

    void cleanSysRubbishAsync(b bVar);

    @Deprecated
    boolean clearAppsCache();

    void clearAppsCacheAsync(b bVar);

    @Deprecated
    ArrayList<DataEntity> findAppsWithAutoboot(boolean z);

    void findAppsWithAutobootAsync(boolean z, b bVar);

    @Deprecated
    ArrayList<DataEntity> findAppsWithCache();

    void findAppsWithCacheAsync(b bVar);

    @Deprecated
    ArrayList<DataEntity> getAllRuningTask(boolean z);

    void getAllRuningTaskAsync(boolean z, b bVar);

    @Deprecated
    int getMemoryUsage();

    void getMemoryUsageAsync(b bVar);

    @Deprecated
    void getSysRubbishSize(a aVar);

    int getSysRubbishSizeAsync(a aVar);

    @Deprecated
    boolean hasRoot();

    void hasRootAsync(b bVar);

    @Deprecated
    boolean isMemoryReachWarning();

    void isMemoryReachWarningAsync(b bVar);

    @Deprecated
    boolean killTask(String str);

    void killTaskAsync(String str, b bVar);

    @Deprecated
    boolean killTasks(List<String> list);

    void killTasksAsync(List<String> list, b bVar);

    @Deprecated
    boolean setAutobootState(String str, boolean z);

    void setAutobootStateAsync(String str, boolean z, b bVar);

    @Deprecated
    boolean setAutobootStates(List<String> list, boolean z);

    void setAutobootStatesAsync(List<String> list, boolean z, b bVar);

    int startScanRubbish(b bVar);

    void updateTmsConfigAsync(b bVar);
}
