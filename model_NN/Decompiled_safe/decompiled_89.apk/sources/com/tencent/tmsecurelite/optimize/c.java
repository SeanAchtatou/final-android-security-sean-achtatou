package com.tencent.tmsecurelite.optimize;

import android.os.IBinder;
import android.os.Parcel;

/* compiled from: ProGuard */
public class c implements a {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f3819a;

    public c(IBinder iBinder) {
        this.f3819a = iBinder;
    }

    public IBinder asBinder() {
        return this.f3819a;
    }

    public void a() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            this.f3819a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }

    public void a(long j) {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeLong(j);
            this.f3819a.transact(2, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain.recycle();
            obtain2.recycle();
        }
    }
}
