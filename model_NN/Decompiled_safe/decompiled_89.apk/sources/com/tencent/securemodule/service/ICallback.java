package com.tencent.securemodule.service;

public interface ICallback {
    void onTaskFinished(int i);
}
