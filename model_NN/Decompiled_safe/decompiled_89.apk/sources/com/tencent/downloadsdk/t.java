package com.tencent.downloadsdk;

import android.os.SystemClock;
import android.text.TextUtils;
import com.tencent.downloadsdk.utils.f;
import com.tencent.downloadsdk.utils.k;
import java.util.ArrayList;

class t implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadTask f3643a;

    t(DownloadTask downloadTask) {
        this.f3643a = downloadTask;
    }

    public void a() {
        this.f3643a.i();
    }

    public void a(int i, byte[] bArr) {
        this.f3643a.a(i, bArr);
    }

    public void a(long j, double d) {
        this.f3643a.i = j;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (j - this.f3643a.v >= this.f3643a.B.q || elapsedRealtime - this.f3643a.w > 50) {
            long unused = this.f3643a.v = j;
            long unused2 = this.f3643a.w = elapsedRealtime;
            this.f3643a.k();
        }
        this.f3643a.a(d);
    }

    public void a(long j, String str, String str2, String str3) {
        this.f3643a.k = j;
        String unused = this.f3643a.F = str;
        String unused2 = this.f3643a.G = str2;
        if (TextUtils.isEmpty(this.f3643a.m)) {
            this.f3643a.m = str3;
            this.f3643a.j();
        }
        this.f3643a.a(this.f3643a.g.get(0), this.f3643a.k, true);
    }

    public void a(String str) {
        if (this.f3643a.q != null) {
            this.f3643a.q.b(this.f3643a.c, this.f3643a.d, str);
        }
    }

    public void a(String str, String str2) {
        if (this.f3643a.p == null) {
            this.f3643a.p = new ArrayList();
        }
        ao aoVar = new ao();
        aoVar.f3597a = k.f().ordinal();
        aoVar.b = str;
        aoVar.c = str2;
        aoVar.d = System.currentTimeMillis();
        if (!this.f3643a.p.contains(aoVar)) {
            this.f3643a.p.add(aoVar);
            this.f3643a.x.a(this.f3643a.c, this.f3643a.d, ao.a(this.f3643a.p));
        }
    }

    public void b() {
        this.f3643a.l();
    }

    public void c() {
    }

    public void d() {
    }

    public void e() {
        f.b("DownloadTask", "onSaveFileTerminated:" + this.f3643a);
        this.f3643a.k();
    }
}
