package com.tencent.downloadsdk.monitor;

import com.tencent.downloadsdk.utils.NetInfo;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class b {

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<c> f3613a = new ReferenceQueue<>();
    protected ConcurrentLinkedQueue<WeakReference<c>> b = new ConcurrentLinkedQueue<>();
    private NetworkMonitorReceiver c = new NetworkMonitorReceiver();

    protected b() {
    }

    /* access modifiers changed from: protected */
    public void a(NetInfo.APN apn) {
        Iterator<WeakReference<c>> it = this.b.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next().get();
            if (cVar != null) {
                cVar.a(apn);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(NetInfo.APN apn, NetInfo.APN apn2) {
        Iterator<WeakReference<c>> it = this.b.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next().get();
            if (cVar != null) {
                cVar.a(apn, apn2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(NetInfo.APN apn) {
        Iterator<WeakReference<c>> it = this.b.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next().get();
            if (cVar != null) {
                cVar.b(apn);
            }
        }
    }
}
