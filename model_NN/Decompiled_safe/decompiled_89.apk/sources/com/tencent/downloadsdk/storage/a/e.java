package com.tencent.downloadsdk.storage.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.an;
import com.tencent.downloadsdk.storage.helper.SDKDBHelper;
import com.tencent.downloadsdk.storage.helper.SqliteHelper;
import com.tencent.downloadsdk.utils.f;
import com.tencent.open.SocialConstants;

public class e implements d {
    private static final byte[] b = new byte[0];

    /* renamed from: a  reason: collision with root package name */
    public String f3641a = Constants.STR_EMPTY;

    private int a(an anVar, SQLiteDatabase sQLiteDatabase) {
        int i = -1;
        if (anVar != null) {
            synchronized (b) {
                try {
                    ContentValues contentValues = new ContentValues();
                    a(contentValues, anVar);
                    i = sQLiteDatabase.update("segfiletable", contentValues, "task_id = ? and seg_id = ?", new String[]{anVar.b, String.valueOf(anVar.c)});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return i;
    }

    private an a(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        an anVar = new an();
        anVar.b = cursor.getString(cursor.getColumnIndex("task_id"));
        anVar.c = cursor.getLong(cursor.getColumnIndex("seg_id"));
        anVar.e = cursor.getLong(cursor.getColumnIndex("total_length"));
        anVar.f = cursor.getLong(cursor.getColumnIndex("start_pos"));
        anVar.d = cursor.getString(cursor.getColumnIndex(SocialConstants.PARAM_URL));
        anVar.g = cursor.getLong(cursor.getColumnIndex("saved_length"));
        anVar.i = anVar.g;
        return anVar;
    }

    private void a(ContentValues contentValues, an anVar) {
        if (anVar != null && contentValues != null) {
            contentValues.put("task_id", anVar.b);
            contentValues.put("seg_id", Long.valueOf(anVar.c));
            contentValues.put("start_pos", Long.valueOf(anVar.f));
            contentValues.put("total_length", Long.valueOf(anVar.e));
            contentValues.put(SocialConstants.PARAM_URL, anVar.d);
            contentValues.put("saved_length", Long.valueOf(anVar.g));
        }
    }

    public SqliteHelper a() {
        return SDKDBHelper.getDBHelper(DownloadManager.a().b());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0055 A[Catch:{ Exception -> 0x0041, all -> 0x0050 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.tencent.downloadsdk.an> a(java.lang.String r9) {
        /*
            r8 = this;
            r0 = 0
            boolean r1 = android.text.TextUtils.isEmpty(r9)
            if (r1 == 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            byte[] r3 = com.tencent.downloadsdk.storage.a.e.b
            monitor-enter(r3)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0059 }
            r1.<init>()     // Catch:{ all -> 0x0059 }
            com.tencent.downloadsdk.storage.helper.SqliteHelper r2 = r8.a()     // Catch:{ Exception -> 0x0041, all -> 0x0050 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x0041, all -> 0x0050 }
            java.lang.String r4 = "select * from segfiletable where task_id = ?"
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x0041, all -> 0x0050 }
            r6 = 0
            r5[r6] = r9     // Catch:{ Exception -> 0x0041, all -> 0x0050 }
            android.database.Cursor r2 = r2.rawQuery(r4, r5)     // Catch:{ Exception -> 0x0041, all -> 0x0050 }
            if (r2 == 0) goto L_0x0039
            boolean r0 = r2.moveToFirst()     // Catch:{ Exception -> 0x005e }
            if (r0 == 0) goto L_0x0039
        L_0x002c:
            com.tencent.downloadsdk.an r0 = r8.a(r2)     // Catch:{ Exception -> 0x005e }
            r1.add(r0)     // Catch:{ Exception -> 0x005e }
            boolean r0 = r2.moveToNext()     // Catch:{ Exception -> 0x005e }
            if (r0 != 0) goto L_0x002c
        L_0x0039:
            if (r2 == 0) goto L_0x003e
            r2.close()     // Catch:{ all -> 0x0059 }
        L_0x003e:
            monitor-exit(r3)     // Catch:{ all -> 0x0059 }
            r0 = r1
            goto L_0x0007
        L_0x0041:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
        L_0x0045:
            r0.printStackTrace()     // Catch:{ all -> 0x005c }
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ all -> 0x0059 }
        L_0x004d:
            monitor-exit(r3)     // Catch:{ all -> 0x0059 }
            r0 = r1
            goto L_0x0007
        L_0x0050:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0053:
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ all -> 0x0059 }
        L_0x0058:
            throw r0     // Catch:{ all -> 0x0059 }
        L_0x0059:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0059 }
            throw r0
        L_0x005c:
            r0 = move-exception
            goto L_0x0053
        L_0x005e:
            r0 = move-exception
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.storage.a.e.a(java.lang.String):java.util.ArrayList");
    }

    public void a(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
    }

    public boolean a(an anVar) {
        boolean z = false;
        if (anVar != null) {
            synchronized (b) {
                try {
                    SQLiteDatabase writableDatabase = a().getWritableDatabase();
                    if (a(anVar, writableDatabase) <= 0) {
                        ContentValues contentValues = new ContentValues();
                        a(contentValues, anVar);
                        writableDatabase.insert("segfiletable", null, contentValues);
                    }
                    z = true;
                } catch (Exception e) {
                    e.printStackTrace();
                    this.f3641a = e.getClass().getSimpleName() + "(" + e.getLocalizedMessage() + ")";
                    f.b("SegFileTable update fail." + e.getClass().getSimpleName() + "(" + e.getLocalizedMessage() + ")");
                }
            }
        }
        return z;
    }

    public String[] a(int i, int i2) {
        if (i != 1 || i2 != 2) {
            return null;
        }
        return new String[]{d()};
    }

    public int b() {
        return 0;
    }

    public void b(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public boolean b(String str) {
        boolean z = false;
        if (!TextUtils.isEmpty(str)) {
            synchronized (b) {
                try {
                    SqliteHelper dBHelper = SDKDBHelper.getDBHelper(DownloadManager.a().b());
                    if (dBHelper != null) {
                        SQLiteDatabase writableDatabase = dBHelper.getWritableDatabase();
                        if (writableDatabase != null) {
                            if (writableDatabase.delete("segfiletable", "task_id=?", new String[]{str}) > 0) {
                                z = true;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return z;
    }

    public String c() {
        return "segfiletable";
    }

    public String d() {
        return "CREATE TABLE if not exists segfiletable( _id INTEGER PRIMARY KEY AUTOINCREMENT, task_id TEXT , seg_id INTEGER , url TEXT ,total_length INTEGER ,start_pos INTEGER ,saved_length INTEGER);";
    }
}
