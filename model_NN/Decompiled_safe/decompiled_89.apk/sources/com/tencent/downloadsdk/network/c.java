package com.tencent.downloadsdk.network;

import java.util.Locale;
import org.apache.http.Header;

public class c<T> {

    /* renamed from: a  reason: collision with root package name */
    public String f3620a;
    public String b;
    public int c;
    public String d;
    public long e;
    public String f;
    public String g;
    protected Header[] h;
    public T i;
    public int j;
    public Throwable k;
    public byte[] l;

    public String a() {
        if (this.h == null || this.h.length <= 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Header header : this.h) {
            sb.append(header.getName()).append("=");
            sb.append(header.getValue()).append(",");
        }
        sb.append("}");
        return sb.toString();
    }

    public String a(String str) {
        Locale locale = Locale.getDefault();
        if (this.h == null) {
            return null;
        }
        for (Header header : this.h) {
            String name = header.getName();
            if (name != null && name.toLowerCase(locale).equals(str.toLowerCase(locale))) {
                return header.getValue();
            }
        }
        return null;
    }
}
