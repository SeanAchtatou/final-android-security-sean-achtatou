package com.tencent.downloadsdk.utils;

import android.util.Log;
import com.tencent.downloadsdk.b;

public class f {
    public static void a(String str) {
        c("assistant", str);
    }

    public static void a(String str, String str2) {
        if (b.f3598a) {
            Log.d(str, str2);
        }
    }

    public static void a(String str, String str2, Throwable th) {
        if (b.f3598a) {
            Log.e(str, str2, th);
        }
    }

    public static void b(String str) {
        d("assistant", str);
    }

    public static void b(String str, String str2) {
        if (b.f3598a) {
            Log.i(str, str2);
        }
    }

    public static void c(String str, String str2) {
        if (b.f3598a) {
            Log.w(str, str2);
        }
    }

    public static void d(String str, String str2) {
        if (b.f3598a) {
            Log.e(str, str2);
        }
    }
}
