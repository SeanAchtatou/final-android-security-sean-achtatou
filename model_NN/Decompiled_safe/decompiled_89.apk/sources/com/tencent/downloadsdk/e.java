package com.tencent.downloadsdk;

import java.util.concurrent.RejectedExecutionException;

class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f3605a;

    e(c cVar) {
        this.f3605a = cVar;
    }

    public void run() {
        an b;
        if (this.f3605a.j.get() < this.f3605a.f3603a && (b = this.f3605a.t.b()) != null) {
            b.d = this.f3605a.C.a(0);
            b.j = this.f3605a.n;
            b.h = true;
            b.c();
            try {
                k kVar = new k(this.f3605a.r, this.f3605a.s, b, this.f3605a.C, this.f3605a.p, this.f3605a.z, this.f3605a, this.f3605a.J);
                kVar.d = this.f3605a.u.submit(kVar);
                this.f3605a.B.put(Long.valueOf(b.c), kVar);
            } catch (RejectedExecutionException e) {
                e.printStackTrace();
            }
        }
    }
}
