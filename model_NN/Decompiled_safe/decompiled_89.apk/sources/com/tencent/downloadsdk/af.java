package com.tencent.downloadsdk;

import android.text.TextUtils;
import com.tencent.downloadsdk.a.a;
import com.tencent.downloadsdk.storage.a.e;
import com.tencent.downloadsdk.utils.f;
import com.tencent.downloadsdk.utils.i;
import com.tencent.downloadsdk.utils.o;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

public class af extends Thread {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f3590a = false;
    private ConcurrentLinkedQueue<ag> b = new ConcurrentLinkedQueue<>();
    private aj c;
    private Object d = new Object();
    private volatile boolean e;
    private long f = 0;
    private long g = 0;
    private AtomicLong h = new AtomicLong(0);
    private Object i = new Object();
    private e j;
    private String k;
    private WeakReference<ah> l;
    private String m;
    private a n;
    private long o = 0;
    private long p = 0;
    private long q = 0;
    private long r;

    private boolean a(ag agVar) {
        if (this.e) {
            return false;
        }
        if (agVar == null) {
            return false;
        }
        long j2 = agVar.d.f + agVar.d.g;
        if (j2 != agVar.c) {
            f.b("DownloadWriteFile", "写入位置出错, savePos: " + j2 + " 实际应该写到位置：" + agVar.c);
            return false;
        } else if (!this.c.a(agVar.f3591a, agVar.b, j2)) {
            o.a().a(agVar.f3591a);
            if (this.l.get() != null) {
                this.l.get().a(-17);
            }
            return false;
        } else {
            o.a().a(agVar.f3591a);
            agVar.d.g += (long) agVar.b;
            this.g += (long) agVar.b;
            if (agVar.d.k == 0 || System.currentTimeMillis() - agVar.d.k >= 200 || agVar.d.g == agVar.d.e) {
                if (!this.j.a(agVar.d) && this.l.get() != null) {
                    this.l.get().a(this.j.f3641a);
                }
                agVar.d.k = System.currentTimeMillis();
                if (this.l.get() != null) {
                    this.l.get().a(this.g);
                }
            }
            if (this.g == this.f && this.l.get() != null) {
                this.l.get().a();
                this.e = true;
            }
            return true;
        }
    }

    public static boolean a(String str, long j2) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String str2 = str + ".yyb";
        if (TextUtils.isEmpty(str2)) {
            return false;
        }
        File file = new File(str2);
        return file.exists() && j2 > 0 && Math.abs(file.lastModified() - j2) <= 3000;
    }

    public static void c(String str) {
        if (!TextUtils.isEmpty(str)) {
            i.a(str + ".yyb");
            i.a(str);
        }
    }

    public static void d(String str) {
        if (!TextUtils.isEmpty(str)) {
            i.a(str + ".yyb");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0045, code lost:
        r11.q = (java.lang.System.currentTimeMillis() - r2) + r11.q;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0050, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean d() {
        /*
            r11 = this;
            r9 = 4194304(0x400000, double:2.0722615E-317)
            long r2 = java.lang.System.currentTimeMillis()
            r1 = 1
        L_0x0008:
            boolean r0 = r11.e
            if (r0 != 0) goto L_0x0054
            java.util.concurrent.ConcurrentLinkedQueue<com.tencent.downloadsdk.ag> r0 = r11.b
            java.lang.Object r0 = r0.poll()
            com.tencent.downloadsdk.ag r0 = (com.tencent.downloadsdk.ag) r0
            if (r0 == 0) goto L_0x0054
            java.util.concurrent.atomic.AtomicLong r4 = r11.h
            long r4 = r4.get()
            java.util.concurrent.atomic.AtomicLong r6 = r11.h
            int r7 = r0.b
            int r7 = -r7
            long r7 = (long) r7
            long r6 = r6.addAndGet(r7)
            int r4 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r4 < 0) goto L_0x003e
            int r4 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r4 > 0) goto L_0x003e
            java.lang.String r4 = "DownloadWriteFile"
            java.lang.String r5 = "cycleWriteFile...notifyAll"
            com.tencent.downloadsdk.utils.f.c(r4, r5)
            java.lang.Object r4 = r11.i
            monitor-enter(r4)
            java.lang.Object r5 = r11.i     // Catch:{ all -> 0x0051 }
            r5.notifyAll()     // Catch:{ all -> 0x0051 }
            monitor-exit(r4)     // Catch:{ all -> 0x0051 }
        L_0x003e:
            boolean r0 = r11.a(r0)
            if (r0 != 0) goto L_0x0008
            r0 = 0
        L_0x0045:
            long r4 = r11.q
            long r6 = java.lang.System.currentTimeMillis()
            long r1 = r6 - r2
            long r1 = r1 + r4
            r11.q = r1
            return r0
        L_0x0051:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0051 }
            throw r0
        L_0x0054:
            r0 = r1
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.af.d():boolean");
    }

    public void a() {
        f.b("DownloadWriteFile", "close");
        synchronized (this.d) {
            this.e = true;
            this.d.notify();
        }
    }

    public boolean a(an anVar) {
        if (this.j != null) {
            return this.j.a(anVar);
        }
        return false;
    }

    public boolean a(an anVar, byte[] bArr, int i2) {
        this.p++;
        long currentTimeMillis = System.currentTimeMillis();
        if (this.e) {
            return false;
        }
        synchronized (this.d) {
            this.b.offer(new ag(anVar, bArr, i2));
            this.d.notify();
        }
        if (this.h.addAndGet((long) i2) > 8388608) {
            f.c("DownloadWriteFile", "write...wait...");
            synchronized (this.i) {
                try {
                    this.i.wait(2000);
                } catch (InterruptedException e2) {
                }
            }
        }
        if (this.e) {
            return false;
        }
        this.o = (System.currentTimeMillis() - currentTimeMillis) + this.o;
        return true;
    }

    public boolean a(String str) {
        return new File(str).exists() || new File(new StringBuilder().append(str).append(".yyb").toString()).exists();
    }

    public boolean a(String str, String str2, long j2, long j3, ah ahVar) {
        this.m = str;
        this.k = str2;
        this.f = j2;
        this.g = j3;
        if (ahVar != null) {
            this.l = new WeakReference<>(ahVar);
        }
        this.j = new e();
        if (this.j == null) {
            return false;
        }
        this.c = new aj();
        if (this.c == null || !this.c.a(this.k, this.f)) {
            return false;
        }
        this.n = new a();
        start();
        return true;
    }

    public long b() {
        return this.f;
    }

    public void b(String str) {
        f.b("DownloadWriteFile", "deleteDB taskId: " + str);
        if (this.j == null) {
            this.j = new e();
        }
        this.j.b(str);
    }

    public long c() {
        return this.g;
    }

    public void run() {
        boolean z;
        try {
            f.b("DownloadWriteFile", "WriteFile thread run[" + Thread.currentThread().getName() + "]  this:" + this);
            this.f3590a = true;
            this.n.a();
            boolean z2 = false;
            while (true) {
                if (this.e) {
                    z = z2;
                    break;
                }
                long currentTimeMillis = System.currentTimeMillis();
                synchronized (this.d) {
                    if (this.b != null && this.b.isEmpty()) {
                        this.n.b();
                        try {
                            this.d.wait();
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
                if (!this.e) {
                    this.n.c();
                    this.r = (System.currentTimeMillis() - currentTimeMillis) + this.r;
                    z = d();
                    if (!z) {
                        break;
                    }
                    z2 = z;
                } else {
                    z = z2;
                    break;
                }
            }
            if (z && !d()) {
                f.b("DownloadWriteFile", "Write end list Failed.");
            }
            this.n.d();
            f.b("DownloadWriteFile", "========== mTotalLength：" + this.f + " mReceivedLength:" + this.g + "==========");
            f.c("DownloadWriteFile", "mWriteThreadFreeTime：" + this.r + "ms");
            f.c("DownloadWriteFile", "mWriteFileTime：" + this.q + "ms");
            f.c("DownloadWriteFile", "mCallWriteCnt：" + this.p);
            f.c("DownloadWriteFile", "mCallWriteConsumeTime：" + this.o + "ms");
            f.b("DownloadWriteFile", "===============================");
            this.c.a(this.g == this.f);
            this.c = null;
            f.a("DownloadWriteFile", "Close BufferList");
            if (this.b != null) {
                this.b.clear();
                synchronized (this.d) {
                    this.b = null;
                }
            }
            f.a("DownloadWriteFile", "Notity DownloadScheduler");
            this.f3590a = false;
            if (this.l != null && this.l.get() != null) {
                f.a("DownloadWriteFile", "mWriteFileListener != null");
                this.l.get().b();
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
