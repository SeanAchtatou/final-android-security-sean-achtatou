package defpackage;

import android.content.Intent;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.impl.SecureService;
import java.io.Serializable;
import java.util.ArrayList;

/* renamed from: aa  reason: default package */
public class aa extends x {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecureService f3a;
    private int b = 0;

    public aa(SecureService secureService) {
        this.f3a = secureService;
    }

    public void a() {
        this.f3a.a("1000020", (Serializable) null);
    }

    public void a(int i) {
        this.b = i;
    }

    public void a(ArrayList<AppInfo> arrayList) {
        if (arrayList.size() > 0) {
            this.f3a.a("1000022", arrayList);
            Intent intent = new Intent();
            intent.setAction("1000030");
            intent.putExtra("key_rise", arrayList);
            this.f3a.sendBroadcast(intent);
            return;
        }
        if (this.b != 0) {
            this.f3a.a("1000023", (Serializable) null);
        } else {
            this.f3a.a("1000021", (Serializable) null);
        }
        Intent intent2 = new Intent();
        intent2.setAction("1000031");
        intent2.putExtra("key_errcode", this.b);
        this.f3a.sendBroadcast(intent2);
    }
}
