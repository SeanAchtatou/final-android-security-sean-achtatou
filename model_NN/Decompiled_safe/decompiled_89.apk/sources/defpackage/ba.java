package defpackage;

import android.os.Process;

/* renamed from: ba  reason: default package */
final class ba implements Runnable {
    ba() {
    }

    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Process.killProcess(Process.myPid());
        System.exit(0);
    }
}
