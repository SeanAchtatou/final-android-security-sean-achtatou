package defpackage;

import android.content.DialogInterface;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: ao  reason: default package */
public class ao implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TransparentActivity f104a;

    public ao(TransparentActivity transparentActivity) {
        this.f104a = transparentActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f104a.finish();
    }
}
