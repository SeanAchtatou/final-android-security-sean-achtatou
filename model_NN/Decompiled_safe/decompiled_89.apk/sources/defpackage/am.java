package defpackage;

import android.content.DialogInterface;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: am  reason: default package */
public class am implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TransparentActivity f16a;

    public am(TransparentActivity transparentActivity) {
        this.f16a = transparentActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f16a.finish();
    }
}
