package defpackage;

import android.content.Context;
import android.os.Process;
import com.tencent.securemodule.impl.AppInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: v  reason: default package */
public class v {

    /* renamed from: a  reason: collision with root package name */
    private Context f3945a;
    private Map<Long, AppInfo> b = new HashMap();

    public v(Context context) {
        this.f3945a = context;
    }

    private ArrayList<AppInfo> a(ArrayList<f> arrayList) {
        return as.a(this.f3945a, 30003, 6) == 6 ? c(arrayList) : b(arrayList);
    }

    private HashMap<Long, AppInfo> a(List<AppInfo> list) {
        HashMap<Long, AppInfo> hashMap = new HashMap<>();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return hashMap;
            }
            AppInfo appInfo = list.get(i2);
            hashMap.put(Long.valueOf(ay.a(appInfo)), appInfo);
            i = i2 + 1;
        }
    }

    private ArrayList<AppInfo> b(ArrayList<f> arrayList) {
        AppInfo appInfo;
        ArrayList<AppInfo> arrayList2 = new ArrayList<>();
        Iterator<f> it = arrayList.iterator();
        while (it.hasNext()) {
            f next = it.next();
            if (!(next.b == 1 || next.b == 6 || next.b == 0 || (appInfo = this.b.get(Long.valueOf(next.f3929a))) == null)) {
                if (next.b == 9 || (next.d & Process.PROC_PARENS) == 512) {
                    appInfo.safeType = 4;
                } else if (next.c == 1) {
                    appInfo.safeType = 2;
                } else if (next.b == 10) {
                    appInfo.safeType = 3;
                } else {
                    appInfo.safeType = 5;
                }
                appInfo.safeLevel = next.c;
                appInfo.isOfficial = next.e;
                arrayList2.add(appInfo);
            }
        }
        return arrayList2;
    }

    private ArrayList<AppInfo> c(ArrayList<f> arrayList) {
        AppInfo appInfo;
        ArrayList<AppInfo> arrayList2 = new ArrayList<>();
        Iterator<f> it = arrayList.iterator();
        while (it.hasNext()) {
            f next = it.next();
            if (next.b == 3 && (appInfo = this.b.get(Long.valueOf(next.f3929a))) != null) {
                arrayList2.add(appInfo);
            }
        }
        return arrayList2;
    }

    private ArrayList<g> d(ArrayList<f> arrayList) {
        AppInfo appInfo;
        ArrayList<g> arrayList2 = new ArrayList<>();
        Iterator<f> it = arrayList.iterator();
        while (it.hasNext()) {
            f next = it.next();
            if (next.b == 6 && (appInfo = this.b.get(Long.valueOf(next.f3929a))) != null) {
                g gVar = new g();
                gVar.f3930a = ay.a(appInfo.getPkgName());
                gVar.b = ay.a(appInfo.getCertMd5());
                gVar.c = (int) appInfo.getFileSize();
                gVar.g = appInfo.getAppType() == 1;
                gVar.f = ay.a(appInfo.getSoftName());
                gVar.d = ay.a(appInfo.getVersionName());
                gVar.e = appInfo.getVersionCode();
                gVar.h = ar.a(appInfo.apkPath);
                arrayList2.add(gVar);
            }
        }
        return arrayList2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ar.a(android.content.Context, boolean):java.util.List<com.tencent.securemodule.impl.AppInfo>
     arg types: [android.content.Context, int]
     candidates:
      ar.a(android.content.Context, java.lang.String):com.tencent.securemodule.impl.AppInfo
      ar.a(android.content.Context, boolean):java.util.List<com.tencent.securemodule.impl.AppInfo> */
    public synchronized void a(x xVar) {
        if (xVar != null) {
            xVar.a();
        }
        long currentTimeMillis = System.currentTimeMillis();
        this.b = a(ar.a(this.f3945a, true));
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : this.b.entrySet()) {
            AppInfo appInfo = (AppInfo) next.getValue();
            arrayList.add(new d(((Long) next.getKey()).longValue(), appInfo != null && appInfo.getAppType() == 1));
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList<AppInfo> arrayList3 = new ArrayList<>();
        int a2 = ag.a(this.f3945a).a(arrayList, arrayList2);
        if (a2 == 0) {
            arrayList3 = a((ArrayList<f>) arrayList2);
            if (ag.a(this.f3945a).b(d(arrayList2), new ArrayList()) == 0) {
            }
        } else if (xVar != null) {
            xVar.a(a2);
        }
        long currentTimeMillis2 = 5000 - (System.currentTimeMillis() - currentTimeMillis);
        if (currentTimeMillis2 > 0) {
            try {
                Thread.sleep(currentTimeMillis2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (xVar != null) {
            xVar.a(arrayList3);
        }
    }
}
