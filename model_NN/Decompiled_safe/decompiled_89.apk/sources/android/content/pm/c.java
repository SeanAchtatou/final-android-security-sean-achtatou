package android.content.pm;

import android.content.a.a;
import android.os.Bundle;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.connect.common.Constants;
import java.io.InputStream;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final float[] f29a = {0.00390625f, 3.051758E-5f, 1.192093E-7f, 4.656613E-10f};
    private static final String[] b = {"px", "dip", "sp", "pt", "in", "mm", Constants.STR_EMPTY, Constants.STR_EMPTY};
    private static final String[] c = {"%", "%p", Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY};

    public static void a(InputStream inputStream, a aVar, int i) {
        boolean z;
        a aVar2 = new a();
        aVar2.a(inputStream);
        if ((i & 2) > 0) {
            aVar.j = new Bundle();
            z = true;
        } else {
            z = false;
        }
        while (true) {
            try {
                int next = aVar2.next();
                if (next != 1) {
                    switch (next) {
                        case 2:
                            String name = aVar2.getName();
                            for (int namespaceCount = aVar2.getNamespaceCount(aVar2.getDepth() - 1); namespaceCount != aVar2.getNamespaceCount(aVar2.getDepth()); namespaceCount++) {
                            }
                            String str = null;
                            String str2 = null;
                            for (int i2 = 0; i2 != aVar2.getAttributeCount(); i2++) {
                                String attributeValue = aVar2.getAttributeValue(i2);
                                String attributeName = aVar2.getAttributeName(i2);
                                if (attributeName.equals("package")) {
                                    aVar.f28a = attributeValue;
                                } else if (attributeName.equals(CommentDetailTabView.PARAMS_VERSION_CODE)) {
                                    try {
                                        aVar.c = Integer.parseInt(attributeValue);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else if (attributeName.equals("versionName")) {
                                    aVar.b = attributeValue;
                                } else if (attributeName.equals("minSdkVersion")) {
                                    try {
                                        aVar.d = Integer.parseInt(attributeValue);
                                    } catch (Exception e2) {
                                        e2.printStackTrace();
                                    }
                                } else if (!name.equals("uses-permission") || !attributeName.equals("name")) {
                                    if (name.equals("application") && attributeName.equals("icon")) {
                                        aVar.f = attributeValue;
                                    } else if (name.equals("application") && attributeName.equals("label")) {
                                        aVar.k = attributeValue;
                                        if (!aVar.k.startsWith("@")) {
                                            aVar.l = aVar.k;
                                        }
                                    } else if (z && name.equals("meta-data")) {
                                        if (attributeName.equals("name")) {
                                            str2 = attributeValue;
                                        } else if (attributeName.equals("value")) {
                                            str = attributeValue;
                                        }
                                        if (!(str2 == null || str == null)) {
                                            aVar.j.putString(str2, str);
                                            str = null;
                                            str2 = null;
                                        }
                                    }
                                } else if (attributeValue.indexOf("permission") > 0) {
                                    if (aVar.e == null) {
                                        aVar.e = new ArrayList<>();
                                        aVar.e.add(attributeValue);
                                    } else {
                                        aVar.e.add(attributeValue);
                                    }
                                }
                            }
                            break;
                    }
                } else {
                    return;
                }
            } catch (Exception e3) {
                e3.printStackTrace();
                return;
            }
        }
    }
}
