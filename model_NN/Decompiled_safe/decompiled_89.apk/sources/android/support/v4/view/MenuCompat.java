package android.support.v4.view;

import android.os.Build;
import android.view.MenuItem;

/* compiled from: ProGuard */
public class MenuCompat {
    static final ac IMPL;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            IMPL = new ab();
        } else {
            IMPL = new aa();
        }
    }

    public static boolean setShowAsAction(MenuItem menuItem, int i) {
        return IMPL.a(menuItem, i);
    }
}
