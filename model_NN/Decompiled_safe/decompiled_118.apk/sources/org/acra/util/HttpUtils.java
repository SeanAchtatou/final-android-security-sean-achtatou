package org.acra.util;

import com.boolbalabs.rollit.settings.Settings;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import org.acra.ACRA;
import org.apache.http.client.ClientProtocolException;

public class HttpUtils {
    public static void doPost(Map<?, ?> parameters, URL url, String login, String password) throws ClientProtocolException, IOException {
        String str;
        StringBuilder dataBfr = new StringBuilder();
        for (Object key : parameters.keySet()) {
            if (dataBfr.length() != 0) {
                dataBfr.append('&');
            }
            Object value = parameters.get(key);
            if (value == null) {
                value = Settings.FLURRY_ID_FULL;
            }
            dataBfr.append(URLEncoder.encode(key.toString(), "UTF-8")).append('=').append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        if (isNull(login)) {
            str = null;
        } else {
            str = login;
        }
        new HttpRequest(str, isNull(password) ? null : password).sendPost(url.toString(), dataBfr.toString());
    }

    private static boolean isNull(String aString) {
        return aString == null || aString == ACRA.NULL_VALUE;
    }
}
