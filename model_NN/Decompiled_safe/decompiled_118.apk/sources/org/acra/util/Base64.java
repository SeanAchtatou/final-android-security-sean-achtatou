package org.acra.util;

import java.io.UnsupportedEncodingException;

public class Base64 {
    static final /* synthetic */ boolean $assertionsDisabled = (!Base64.class.desiredAssertionStatus());
    public static final int CRLF = 4;
    public static final int DEFAULT = 0;
    public static final int NO_CLOSE = 16;
    public static final int NO_PADDING = 1;
    public static final int NO_WRAP = 2;
    public static final int URL_SAFE = 8;

    static abstract class Coder {
        public int op;
        public byte[] output;

        public abstract int maxOutputSize(int i);

        public abstract boolean process(byte[] bArr, int i, int i2, boolean z);

        Coder() {
        }
    }

    public static byte[] decode(String str, int flags) {
        return decode(str.getBytes(), flags);
    }

    public static byte[] decode(byte[] input, int flags) {
        return decode(input, 0, input.length, flags);
    }

    public static byte[] decode(byte[] input, int offset, int len, int flags) {
        Decoder decoder = new Decoder(flags, new byte[((len * 3) / 4)]);
        if (!decoder.process(input, offset, len, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (decoder.op == decoder.output.length) {
            return decoder.output;
        } else {
            byte[] temp = new byte[decoder.op];
            System.arraycopy(decoder.output, 0, temp, 0, decoder.op);
            return temp;
        }
    }

    static class Decoder extends Coder {
        private static final int[] DECODE = {SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, 62, SKIP, SKIP, SKIP, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, SKIP, SKIP, SKIP, EQUALS, SKIP, SKIP, SKIP, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP};
        private static final int[] DECODE_WEBSAFE = {SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, 62, SKIP, SKIP, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, SKIP, SKIP, SKIP, EQUALS, SKIP, SKIP, SKIP, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, SKIP, SKIP, SKIP, SKIP, 63, SKIP, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP, SKIP};
        private static final int EQUALS = -2;
        private static final int SKIP = -1;
        private final int[] alphabet;
        private int state;
        private int value;

        public Decoder(int flags, byte[] output) {
            this.output = output;
            this.alphabet = (flags & 8) == 0 ? DECODE : DECODE_WEBSAFE;
            this.state = 0;
            this.value = 0;
        }

        public int maxOutputSize(int len) {
            return ((len * 3) / 4) + 10;
        }

        public boolean process(byte[] input, int offset, int len, boolean finish) {
            int op;
            int op2;
            if (this.state == 6) {
                return false;
            }
            int p = offset;
            int len2 = len + offset;
            int state2 = this.state;
            int value2 = this.value;
            int op3 = 0;
            byte[] output = this.output;
            int[] alphabet2 = this.alphabet;
            while (true) {
                if (p < len2) {
                    if (state2 == 0) {
                        while (p + 4 <= len2 && (value2 = (alphabet2[input[p] & 255] << 18) | (alphabet2[input[p + 1] & 255] << 12) | (alphabet2[input[p + 2] & 255] << 6) | alphabet2[input[p + 3] & 255]) >= 0) {
                            output[op3 + 2] = (byte) value2;
                            output[op3 + 1] = (byte) (value2 >> 8);
                            output[op3] = (byte) (value2 >> 16);
                            op3 += 3;
                            p += 4;
                        }
                        if (p >= len2) {
                            op = op3;
                        }
                    }
                    int p2 = p + 1;
                    int d = alphabet2[input[p] & 255];
                    switch (state2) {
                        case 0:
                            if (d < 0) {
                                if (d == SKIP) {
                                    break;
                                } else {
                                    this.state = 6;
                                    return false;
                                }
                            } else {
                                value2 = d;
                                state2++;
                                break;
                            }
                        case 1:
                            if (d < 0) {
                                if (d == SKIP) {
                                    break;
                                } else {
                                    this.state = 6;
                                    return false;
                                }
                            } else {
                                value2 = (value2 << 6) | d;
                                state2++;
                                break;
                            }
                        case 2:
                            if (d < 0) {
                                if (d != EQUALS) {
                                    if (d == SKIP) {
                                        break;
                                    } else {
                                        this.state = 6;
                                        return false;
                                    }
                                } else {
                                    output[op3] = (byte) (value2 >> 4);
                                    state2 = 4;
                                    op3++;
                                    break;
                                }
                            } else {
                                value2 = (value2 << 6) | d;
                                state2++;
                                break;
                            }
                        case 3:
                            if (d < 0) {
                                if (d != EQUALS) {
                                    if (d == SKIP) {
                                        break;
                                    } else {
                                        this.state = 6;
                                        return false;
                                    }
                                } else {
                                    output[op3 + 1] = (byte) (value2 >> 2);
                                    output[op3] = (byte) (value2 >> 10);
                                    op3 += 2;
                                    state2 = 5;
                                    break;
                                }
                            } else {
                                value2 = (value2 << 6) | d;
                                output[op3 + 2] = (byte) value2;
                                output[op3 + 1] = (byte) (value2 >> 8);
                                output[op3] = (byte) (value2 >> 16);
                                op3 += 3;
                                state2 = 0;
                                break;
                            }
                        case 4:
                            if (d != EQUALS) {
                                if (d == SKIP) {
                                    break;
                                } else {
                                    this.state = 6;
                                    return false;
                                }
                            } else {
                                state2++;
                                break;
                            }
                        case 5:
                            if (d == SKIP) {
                                break;
                            } else {
                                this.state = 6;
                                return false;
                            }
                    }
                    p = p2;
                } else {
                    op = op3;
                }
            }
            if (!finish) {
                this.state = state2;
                this.value = value2;
                this.op = op;
                return true;
            }
            switch (state2) {
                case 0:
                    op2 = op;
                    break;
                case 1:
                    this.state = 6;
                    return false;
                case 2:
                    op2 = op + 1;
                    output[op] = (byte) (value2 >> 4);
                    break;
                case 3:
                    int op4 = op + 1;
                    output[op] = (byte) (value2 >> 10);
                    output[op4] = (byte) (value2 >> 2);
                    op2 = op4 + 1;
                    break;
                case 4:
                    this.state = 6;
                    return false;
                default:
                    op2 = op;
                    break;
            }
            this.state = state2;
            this.op = op2;
            return true;
        }
    }

    public static String encodeToString(byte[] input, int flags) {
        try {
            return new String(encode(input, flags), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static String encodeToString(byte[] input, int offset, int len, int flags) {
        try {
            return new String(encode(input, offset, len, flags), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] encode(byte[] input, int flags) {
        return encode(input, 0, input.length, flags);
    }

    public static byte[] encode(byte[] input, int offset, int len, int flags) {
        int i;
        Encoder encoder = new Encoder(flags, null);
        int output_len = (len / 3) * 4;
        if (!encoder.do_padding) {
            switch (len % 3) {
                case 1:
                    output_len += 2;
                    break;
                case 2:
                    output_len += 3;
                    break;
            }
        } else if (len % 3 > 0) {
            output_len += 4;
        }
        if (encoder.do_newline && len > 0) {
            int i2 = ((len - 1) / 57) + 1;
            if (encoder.do_cr) {
                i = 2;
            } else {
                i = 1;
            }
            output_len += i2 * i;
        }
        encoder.output = new byte[output_len];
        encoder.process(input, offset, len, true);
        if ($assertionsDisabled || encoder.op == output_len) {
            return encoder.output;
        }
        throw new AssertionError();
    }

    static class Encoder extends Coder {
        static final /* synthetic */ boolean $assertionsDisabled;
        private static final byte[] ENCODE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        private static final byte[] ENCODE_WEBSAFE = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        public static final int LINE_GROUPS = 19;
        private final byte[] alphabet;
        private int count;
        public final boolean do_cr;
        public final boolean do_newline;
        public final boolean do_padding;
        private final byte[] tail;
        int tailLen;

        static {
            boolean z;
            if (!Base64.class.desiredAssertionStatus()) {
                z = true;
            } else {
                z = false;
            }
            $assertionsDisabled = z;
        }

        public Encoder(int flags, byte[] output) {
            boolean z;
            boolean z2;
            this.output = output;
            this.do_padding = (flags & 1) == 0;
            if ((flags & 2) == 0) {
                z = true;
            } else {
                z = false;
            }
            this.do_newline = z;
            if ((flags & 4) != 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.do_cr = z2;
            this.alphabet = (flags & 8) == 0 ? ENCODE : ENCODE_WEBSAFE;
            this.tail = new byte[2];
            this.tailLen = 0;
            this.count = this.do_newline ? 19 : -1;
        }

        public int maxOutputSize(int len) {
            return ((len * 8) / 5) + 10;
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0059  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0101  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x015d  */
        /* JADX WARNING: Removed duplicated region for block: B:82:0x0213  */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x00ff A[SYNTHETIC] */
        public boolean process(byte[] r16, int r17, int r18, boolean r19) {
            /*
                r15 = this;
                byte[] r2 = r15.alphabet
                byte[] r6 = r15.output
                r4 = 0
                int r3 = r15.count
                r7 = r17
                int r18 = r18 + r17
                r11 = -1
                int r12 = r15.tailLen
                switch(r12) {
                    case 0: goto L_0x0011;
                    case 1: goto L_0x00b1;
                    case 2: goto L_0x00d7;
                    default: goto L_0x0011;
                }
            L_0x0011:
                r12 = -1
                if (r11 == r12) goto L_0x0250
                int r5 = r4 + 1
                int r12 = r11 >> 18
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r4] = r12
                int r4 = r5 + 1
                int r12 = r11 >> 12
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r5 = r4 + 1
                int r12 = r11 >> 6
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r4] = r12
                int r4 = r5 + 1
                r12 = r11 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r3 = r3 + -1
                if (r3 != 0) goto L_0x0250
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x0049
                int r5 = r4 + 1
                r12 = 13
                r6[r4] = r12
                r4 = r5
            L_0x0049:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
                r3 = 19
                r8 = r7
            L_0x0052:
                int r12 = r8 + 3
                r0 = r12
                r1 = r18
                if (r0 > r1) goto L_0x00ff
                byte r12 = r16[r8]
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r12 = r12 << 16
                int r13 = r8 + 1
                byte r13 = r16[r13]
                r13 = r13 & 255(0xff, float:3.57E-43)
                int r13 = r13 << 8
                r12 = r12 | r13
                int r13 = r8 + 2
                byte r13 = r16[r13]
                r13 = r13 & 255(0xff, float:3.57E-43)
                r11 = r12 | r13
                int r12 = r11 >> 18
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r12 = r5 + 1
                int r13 = r11 >> 12
                r13 = r13 & 63
                byte r13 = r2[r13]
                r6[r12] = r13
                int r12 = r5 + 2
                int r13 = r11 >> 6
                r13 = r13 & 63
                byte r13 = r2[r13]
                r6[r12] = r13
                int r12 = r5 + 3
                r13 = r11 & 63
                byte r13 = r2[r13]
                r6[r12] = r13
                int r7 = r8 + 3
                int r4 = r5 + 4
                int r3 = r3 + -1
                if (r3 != 0) goto L_0x0250
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x00a7
                int r5 = r4 + 1
                r12 = 13
                r6[r4] = r12
                r4 = r5
            L_0x00a7:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
                r3 = 19
                r8 = r7
                goto L_0x0052
            L_0x00b1:
                int r12 = r7 + 2
                r0 = r12
                r1 = r18
                if (r0 > r1) goto L_0x0011
                byte[] r12 = r15.tail
                r13 = 0
                byte r12 = r12[r13]
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r12 = r12 << 16
                int r8 = r7 + 1
                byte r13 = r16[r7]
                r13 = r13 & 255(0xff, float:3.57E-43)
                int r13 = r13 << 8
                r12 = r12 | r13
                int r7 = r8 + 1
                byte r13 = r16[r8]
                r13 = r13 & 255(0xff, float:3.57E-43)
                r11 = r12 | r13
                r12 = 0
                r15.tailLen = r12
                goto L_0x0011
            L_0x00d7:
                int r12 = r7 + 1
                r0 = r12
                r1 = r18
                if (r0 > r1) goto L_0x0011
                byte[] r12 = r15.tail
                r13 = 0
                byte r12 = r12[r13]
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r12 = r12 << 16
                byte[] r13 = r15.tail
                r14 = 1
                byte r13 = r13[r14]
                r13 = r13 & 255(0xff, float:3.57E-43)
                int r13 = r13 << 8
                r12 = r12 | r13
                int r8 = r7 + 1
                byte r13 = r16[r7]
                r13 = r13 & 255(0xff, float:3.57E-43)
                r11 = r12 | r13
                r12 = 0
                r15.tailLen = r12
                r7 = r8
                goto L_0x0011
            L_0x00ff:
                if (r19 == 0) goto L_0x0213
                int r12 = r15.tailLen
                int r12 = r8 - r12
                r13 = 1
                int r13 = r18 - r13
                if (r12 != r13) goto L_0x016c
                r9 = 0
                int r12 = r15.tailLen
                if (r12 <= 0) goto L_0x0167
                byte[] r12 = r15.tail
                int r10 = r9 + 1
                byte r12 = r12[r9]
                r9 = r10
                r7 = r8
            L_0x0117:
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r11 = r12 << 4
                int r12 = r15.tailLen
                int r12 = r12 - r9
                r15.tailLen = r12
                int r4 = r5 + 1
                int r12 = r11 >> 6
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r5 = r4 + 1
                r12 = r11 & 63
                byte r12 = r2[r12]
                r6[r4] = r12
                boolean r12 = r15.do_padding
                if (r12 == 0) goto L_0x0142
                int r4 = r5 + 1
                r12 = 61
                r6[r5] = r12
                int r5 = r4 + 1
                r12 = 61
                r6[r4] = r12
            L_0x0142:
                r4 = r5
                boolean r12 = r15.do_newline
                if (r12 == 0) goto L_0x0159
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x0152
                int r5 = r4 + 1
                r12 = 13
                r6[r4] = r12
                r4 = r5
            L_0x0152:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
            L_0x0158:
                r4 = r5
            L_0x0159:
                boolean r12 = org.acra.util.Base64.Encoder.$assertionsDisabled
                if (r12 != 0) goto L_0x0204
                int r12 = r15.tailLen
                if (r12 == 0) goto L_0x0204
                java.lang.AssertionError r12 = new java.lang.AssertionError
                r12.<init>()
                throw r12
            L_0x0167:
                int r7 = r8 + 1
                byte r12 = r16[r8]
                goto L_0x0117
            L_0x016c:
                int r12 = r15.tailLen
                int r12 = r8 - r12
                r13 = 2
                int r13 = r18 - r13
                if (r12 != r13) goto L_0x01e6
                r9 = 0
                int r12 = r15.tailLen
                r13 = 1
                if (r12 <= r13) goto L_0x01db
                byte[] r12 = r15.tail
                int r10 = r9 + 1
                byte r12 = r12[r9]
                r9 = r10
                r7 = r8
            L_0x0183:
                r12 = r12 & 255(0xff, float:3.57E-43)
                int r12 = r12 << 10
                int r13 = r15.tailLen
                if (r13 <= 0) goto L_0x01e0
                byte[] r13 = r15.tail
                int r10 = r9 + 1
                byte r13 = r13[r9]
                r9 = r10
            L_0x0192:
                r13 = r13 & 255(0xff, float:3.57E-43)
                int r13 = r13 << 2
                r11 = r12 | r13
                int r12 = r15.tailLen
                int r12 = r12 - r9
                r15.tailLen = r12
                int r4 = r5 + 1
                int r12 = r11 >> 12
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                int r5 = r4 + 1
                int r12 = r11 >> 6
                r12 = r12 & 63
                byte r12 = r2[r12]
                r6[r4] = r12
                int r4 = r5 + 1
                r12 = r11 & 63
                byte r12 = r2[r12]
                r6[r5] = r12
                boolean r12 = r15.do_padding
                if (r12 == 0) goto L_0x01c4
                int r5 = r4 + 1
                r12 = 61
                r6[r4] = r12
                r4 = r5
            L_0x01c4:
                boolean r12 = r15.do_newline
                if (r12 == 0) goto L_0x0159
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x01d3
                int r5 = r4 + 1
                r12 = 13
                r6[r4] = r12
                r4 = r5
            L_0x01d3:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
                goto L_0x0158
            L_0x01db:
                int r7 = r8 + 1
                byte r12 = r16[r8]
                goto L_0x0183
            L_0x01e0:
                int r8 = r7 + 1
                byte r13 = r16[r7]
                r7 = r8
                goto L_0x0192
            L_0x01e6:
                boolean r12 = r15.do_newline
                if (r12 == 0) goto L_0x0200
                if (r5 <= 0) goto L_0x0200
                r12 = 19
                if (r3 == r12) goto L_0x0200
                boolean r12 = r15.do_cr
                if (r12 == 0) goto L_0x024e
                int r4 = r5 + 1
                r12 = 13
                r6[r5] = r12
            L_0x01fa:
                int r5 = r4 + 1
                r12 = 10
                r6[r4] = r12
            L_0x0200:
                r7 = r8
                r4 = r5
                goto L_0x0159
            L_0x0204:
                boolean r12 = org.acra.util.Base64.Encoder.$assertionsDisabled
                if (r12 != 0) goto L_0x0226
                r0 = r7
                r1 = r18
                if (r0 == r1) goto L_0x0226
                java.lang.AssertionError r12 = new java.lang.AssertionError
                r12.<init>()
                throw r12
            L_0x0213:
                r12 = 1
                int r12 = r18 - r12
                if (r8 != r12) goto L_0x022c
                byte[] r12 = r15.tail
                int r13 = r15.tailLen
                int r14 = r13 + 1
                r15.tailLen = r14
                byte r14 = r16[r8]
                r12[r13] = r14
                r7 = r8
                r4 = r5
            L_0x0226:
                r15.op = r4
                r15.count = r3
                r12 = 1
                return r12
            L_0x022c:
                r12 = 2
                int r12 = r18 - r12
                if (r8 != r12) goto L_0x024b
                byte[] r12 = r15.tail
                int r13 = r15.tailLen
                int r14 = r13 + 1
                r15.tailLen = r14
                byte r14 = r16[r8]
                r12[r13] = r14
                byte[] r12 = r15.tail
                int r13 = r15.tailLen
                int r14 = r13 + 1
                r15.tailLen = r14
                int r14 = r8 + 1
                byte r14 = r16[r14]
                r12[r13] = r14
            L_0x024b:
                r7 = r8
                r4 = r5
                goto L_0x0226
            L_0x024e:
                r4 = r5
                goto L_0x01fa
            L_0x0250:
                r8 = r7
                r5 = r4
                goto L_0x0052
            */
            throw new UnsupportedOperationException("Method not decompiled: org.acra.util.Base64.Encoder.process(byte[], int, int, boolean):boolean");
        }
    }

    private Base64() {
    }
}
