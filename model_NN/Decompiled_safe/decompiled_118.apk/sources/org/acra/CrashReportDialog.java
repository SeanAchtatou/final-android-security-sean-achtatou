package org.acra;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.boolbalabs.rollit.settings.Settings;
import org.acra.ErrorReporter;

public class CrashReportDialog extends Activity {
    String mReportFileName = null;
    /* access modifiers changed from: private */
    public SharedPreferences prefs = null;
    /* access modifiers changed from: private */
    public EditText userComment = null;
    /* access modifiers changed from: private */
    public EditText userEmail = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mReportFileName = getIntent().getStringExtra("REPORT_FILE_NAME");
        Log.d(ACRA.LOG_TAG, "Opening CrashReportDialog for " + this.mReportFileName);
        if (this.mReportFileName == null) {
            finish();
        }
        requestWindowFeature(3);
        LinearLayout root = new LinearLayout(this);
        root.setOrientation(1);
        root.setPadding(10, 10, 10, 10);
        root.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ScrollView scroll = new ScrollView(this);
        root.addView(scroll, new LinearLayout.LayoutParams(-1, -1, 1.0f));
        TextView text = new TextView(this);
        text.setText(getText(ACRA.getConfig().resDialogText()));
        scroll.addView(text, -1, -1);
        int commentPromptId = ACRA.getConfig().resDialogCommentPrompt();
        if (commentPromptId != 0) {
            TextView label = new TextView(this);
            label.setText(getText(commentPromptId));
            label.setPadding(label.getPaddingLeft(), 10, label.getPaddingRight(), label.getPaddingBottom());
            root.addView(label, new LinearLayout.LayoutParams(-1, -2));
            this.userComment = new EditText(this);
            this.userComment.setLines(2);
            root.addView(this.userComment, new LinearLayout.LayoutParams(-1, -2));
        }
        int emailPromptId = ACRA.getConfig().resDialogEmailPrompt();
        if (emailPromptId != 0) {
            TextView label2 = new TextView(this);
            label2.setText(getText(emailPromptId));
            label2.setPadding(label2.getPaddingLeft(), 10, label2.getPaddingRight(), label2.getPaddingBottom());
            root.addView(label2, new LinearLayout.LayoutParams(-1, -2));
            this.userEmail = new EditText(this);
            this.userEmail.setSingleLine();
            this.userEmail.setInputType(33);
            this.prefs = getSharedPreferences(ACRA.getConfig().sharedPreferencesName(), ACRA.getConfig().sharedPreferencesMode());
            this.userEmail.setText(this.prefs.getString(ACRA.PREF_USER_EMAIL_ADDRESS, Settings.FLURRY_ID_FULL));
            root.addView(this.userEmail, new LinearLayout.LayoutParams(-1, -2));
        }
        LinearLayout buttons = new LinearLayout(this);
        buttons.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        buttons.setPadding(buttons.getPaddingLeft(), 10, buttons.getPaddingRight(), buttons.getPaddingBottom());
        Button yes = new Button(this);
        yes.setText(17039379);
        yes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ErrorReporter err = ErrorReporter.getInstance();
                err.getClass();
                ErrorReporter.ReportsSenderWorker worker = new ErrorReporter.ReportsSenderWorker();
                worker.setApprovePendingReports();
                if (CrashReportDialog.this.userComment != null) {
                    worker.setUserComment(CrashReportDialog.this.mReportFileName, CrashReportDialog.this.userComment.getText().toString());
                }
                if (!(CrashReportDialog.this.prefs == null || CrashReportDialog.this.userEmail == null)) {
                    String usrEmail = CrashReportDialog.this.userEmail.getText().toString();
                    SharedPreferences.Editor prefEditor = CrashReportDialog.this.prefs.edit();
                    prefEditor.putString(ACRA.PREF_USER_EMAIL_ADDRESS, usrEmail);
                    prefEditor.commit();
                    worker.setUserEmail(CrashReportDialog.this.mReportFileName, usrEmail);
                }
                Log.v(ACRA.LOG_TAG, "About to start ReportSenderWorker from CrashReportDialog");
                worker.start();
                int toastId = ACRA.getConfig().resDialogOkToast();
                if (toastId != 0) {
                    Toast.makeText(CrashReportDialog.this.getApplicationContext(), toastId, 1).show();
                }
                CrashReportDialog.this.finish();
            }
        });
        buttons.addView(yes, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        Button no = new Button(this);
        no.setText(17039369);
        no.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ErrorReporter.getInstance().deletePendingReports();
                CrashReportDialog.this.finish();
            }
        });
        buttons.addView(no, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        root.addView(buttons, new LinearLayout.LayoutParams(-1, -2));
        setContentView(root);
        int resTitle = ACRA.getConfig().resDialogTitle();
        if (resTitle != 0) {
            setTitle(resTitle);
        }
        getWindow().setFeatureDrawableResource(3, ACRA.getConfig().resDialogIcon());
        cancelNotification();
    }

    /* access modifiers changed from: protected */
    public void cancelNotification() {
        ((NotificationManager) getSystemService("notification")).cancel(666);
    }
}
