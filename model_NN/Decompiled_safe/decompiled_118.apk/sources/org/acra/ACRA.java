package org.acra;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.util.Log;
import com.boolbalabs.rollit.settings.Settings;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.EmailIntentSender;
import org.acra.sender.GoogleFormSender;
import org.acra.sender.HttpPostSender;

public class ACRA {
    public static final ReportField[] DEFAULT_MAIL_REPORT_FIELDS = {ReportField.USER_COMMENT, ReportField.ANDROID_VERSION, ReportField.APP_VERSION_NAME, ReportField.BRAND, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE};
    public static final ReportField[] DEFAULT_REPORT_FIELDS = {ReportField.REPORT_ID, ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.PACKAGE_NAME, ReportField.FILE_PATH, ReportField.PHONE_MODEL, ReportField.BRAND, ReportField.PRODUCT, ReportField.ANDROID_VERSION, ReportField.BUILD, ReportField.TOTAL_MEM_SIZE, ReportField.AVAILABLE_MEM_SIZE, ReportField.CUSTOM_DATA, ReportField.IS_SILENT, ReportField.STACK_TRACE, ReportField.INITIAL_CONFIGURATION, ReportField.CRASH_CONFIGURATION, ReportField.DISPLAY, ReportField.USER_COMMENT, ReportField.USER_EMAIL, ReportField.USER_APP_START_DATE, ReportField.USER_CRASH_DATE, ReportField.DUMPSYS_MEMINFO, ReportField.DROPBOX, ReportField.LOGCAT, ReportField.EVENTSLOG, ReportField.RADIOLOG, ReportField.DEVICE_ID, ReportField.INSTALLATION_ID, ReportField.DEVICE_FEATURES, ReportField.ENVIRONMENT, ReportField.SHARED_PREFERENCES, ReportField.SETTINGS_SYSTEM, ReportField.SETTINGS_SECURE};
    public static final boolean DEV_LOGGING = false;
    public static final String LOG_TAG = ACRA.class.getSimpleName();
    static final int NOTIF_CRASH_ID = 666;
    public static final String NULL_VALUE = "ACRA-NULL-STRING";
    public static final String PREF_ALWAYS_ACCEPT = "acra.alwaysaccept";
    public static final String PREF_DISABLE_ACRA = "acra.disable";
    public static final String PREF_ENABLE_ACRA = "acra.enable";
    public static final String PREF_ENABLE_DEVICE_ID = "acra.deviceid.enable";
    public static final String PREF_ENABLE_SYSTEM_LOGS = "acra.syslog.enable";
    public static final String PREF_USER_EMAIL_ADDRESS = "acra.user.email";
    static final String RES_DIALOG_COMMENT_PROMPT = "RES_DIALOG_COMMENT_PROMPT";
    static final String RES_DIALOG_ICON = "RES_DIALOG_ICON";
    static final String RES_DIALOG_OK_TOAST = "RES_DIALOG_OK_TOAST";
    static final String RES_DIALOG_TEXT = "RES_DIALOG_TEXT";
    static final String RES_DIALOG_TITLE = "RES_DIALOG_TITLE";
    static final String RES_NOTIF_ICON = "RES_NOTIF_ICON";
    static final String RES_NOTIF_TEXT = "RES_NOTIF_TEXT";
    static final String RES_NOTIF_TICKER_TEXT = "RES_NOTIF_TICKER_TEXT";
    static final String RES_NOTIF_TITLE = "RES_NOTIF_TITLE";
    static final String RES_TOAST_TEXT = "RES_TOAST_TEXT";
    private static Time mAppStartDate;
    private static Application mApplication;
    private static SharedPreferences.OnSharedPreferenceChangeListener mPrefListener;
    private static ReportsCrashes mReportsCrashes;

    public static void init(Application app) {
        mAppStartDate = new Time();
        mAppStartDate.setToNow();
        mApplication = app;
        mReportsCrashes = (ReportsCrashes) mApplication.getClass().getAnnotation(ReportsCrashes.class);
        if (mReportsCrashes != null) {
            SharedPreferences prefs = getACRASharedPreferences();
            Log.d(LOG_TAG, "Set OnSharedPreferenceChangeListener.");
            mPrefListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                    boolean z;
                    if (ACRA.PREF_DISABLE_ACRA.equals(key) || ACRA.PREF_ENABLE_ACRA.equals(key)) {
                        Boolean disableAcra = false;
                        try {
                            if (!sharedPreferences.getBoolean(ACRA.PREF_ENABLE_ACRA, true)) {
                                z = true;
                            } else {
                                z = false;
                            }
                            disableAcra = Boolean.valueOf(sharedPreferences.getBoolean(ACRA.PREF_DISABLE_ACRA, z));
                        } catch (Exception e) {
                        }
                        if (disableAcra.booleanValue()) {
                            ErrorReporter.getInstance().disable();
                            return;
                        }
                        try {
                            ACRA.initAcra();
                        } catch (ACRAConfigurationException e2) {
                            Log.w(ACRA.LOG_TAG, "Error : ", e2);
                        }
                    }
                }
            };
            boolean disableAcra = false;
            try {
                disableAcra = prefs.getBoolean(PREF_DISABLE_ACRA, !prefs.getBoolean(PREF_ENABLE_ACRA, true));
            } catch (Exception e) {
            }
            if (disableAcra) {
                Log.d(LOG_TAG, "ACRA is disabled for " + mApplication.getPackageName() + ".");
            } else {
                try {
                    initAcra();
                } catch (ACRAConfigurationException e2) {
                    Log.w(LOG_TAG, "Error : ", e2);
                }
            }
            prefs.registerOnSharedPreferenceChangeListener(mPrefListener);
        }
    }

    /* access modifiers changed from: private */
    public static void initAcra() throws ACRAConfigurationException {
        checkCrashResources();
        Log.d(LOG_TAG, "ACRA is enabled for " + mApplication.getPackageName() + ", intializing...");
        ErrorReporter errorReporter = ErrorReporter.getInstance();
        errorReporter.setReportingInteractionMode(mReportsCrashes.mode());
        errorReporter.setAppStartDate(mAppStartDate);
        if (!Settings.FLURRY_ID_FULL.equals(mReportsCrashes.mailTo())) {
            Log.w(LOG_TAG, mApplication.getPackageName() + " reports will be sent by email (if accepted by user).");
            errorReporter.addReportSender(new EmailIntentSender(mApplication));
        } else {
            PackageManager pm = mApplication.getPackageManager();
            if (pm != null) {
                if (pm.checkPermission("android.permission.INTERNET", mApplication.getPackageName()) != 0) {
                    Log.e(LOG_TAG, mApplication.getPackageName() + " should be granted permission " + "android.permission.INTERNET" + " if you want your crash reports to be sent. If you don't want to add this permission to your application you can also enable sending reports by email. If this is your will then provide your email address in @ReportsCrashes(mailTo=\"your.account@domain.com\"");
                } else if (mReportsCrashes.formUri() != null && !Settings.FLURRY_ID_FULL.equals(mReportsCrashes.formUri())) {
                    errorReporter.addReportSender(new HttpPostSender(mReportsCrashes.formUri(), null));
                } else if (mReportsCrashes.formKey() != null && !Settings.FLURRY_ID_FULL.equals(mReportsCrashes.formKey().trim())) {
                    errorReporter.addReportSender(new GoogleFormSender(mReportsCrashes.formKey()));
                }
            }
        }
        errorReporter.init(mApplication.getApplicationContext());
        errorReporter.checkReportsOnApplicationStart();
    }

    static void checkCrashResources() throws ACRAConfigurationException {
        switch (mReportsCrashes.mode()) {
            case TOAST:
                if (mReportsCrashes.resToastText() == 0) {
                    throw new ACRAConfigurationException("TOAST mode: you have to define the resToastText parameter in your application @ReportsCrashes() annotation.");
                }
                return;
            case NOTIFICATION:
                if (mReportsCrashes.resNotifTickerText() == 0 || mReportsCrashes.resNotifTitle() == 0 || mReportsCrashes.resNotifText() == 0 || mReportsCrashes.resDialogText() == 0) {
                    throw new ACRAConfigurationException("NOTIFICATION mode: you have to define at least the resNotifTickerText, resNotifTitle, resNotifText, resDialogText parameters in your application @ReportsCrashes() annotation.");
                }
                return;
            default:
                return;
        }
    }

    public static SharedPreferences getACRASharedPreferences() {
        if (!Settings.FLURRY_ID_FULL.equals(mReportsCrashes.sharedPreferencesName())) {
            Log.d(LOG_TAG, "Retrieve SharedPreferences " + mReportsCrashes.sharedPreferencesName());
            return mApplication.getSharedPreferences(mReportsCrashes.sharedPreferencesName(), mReportsCrashes.sharedPreferencesMode());
        }
        Log.d(LOG_TAG, "Retrieve application default SharedPreferences.");
        return PreferenceManager.getDefaultSharedPreferences(mApplication);
    }

    public static ReportsCrashes getConfig() {
        return mReportsCrashes;
    }
}
