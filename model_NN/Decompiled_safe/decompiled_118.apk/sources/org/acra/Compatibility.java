package org.acra;

import android.content.Context;
import android.os.Build;
import java.lang.reflect.Field;

public class Compatibility {
    static int getAPILevel() {
        try {
            return Build.VERSION.class.getField("SDK_INT").getInt(null);
        } catch (SecurityException e) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (NoSuchFieldException e2) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (IllegalArgumentException e3) {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (IllegalAccessException e4) {
            return Integer.parseInt(Build.VERSION.SDK);
        }
    }

    static String getDropBoxServiceName() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Field serviceName = Context.class.getField("DROPBOX_SERVICE");
        if (serviceName != null) {
            return (String) serviceName.get(null);
        }
        return null;
    }
}
