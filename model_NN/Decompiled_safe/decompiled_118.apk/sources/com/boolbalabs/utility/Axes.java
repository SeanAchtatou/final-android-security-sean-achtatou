package com.boolbalabs.utility;

public class Axes {
    private final Point3D temp = new Point3D();
    public Point3D xAxis = new Point3D(1.0f, 0.0f, 0.0f);
    public Point3D yAxis = new Point3D(0.0f, 1.0f, 0.0f);
    public Point3D zAxis = new Point3D(0.0f, 0.0f, 1.0f);

    public String toString() {
        return String.valueOf(this.xAxis.toString()) + this.yAxis.toString() + this.zAxis.toString();
    }

    public Point3D toPoint3D() {
        this.temp.set(this.xAxis.x + this.yAxis.x + this.zAxis.x, this.xAxis.y + this.yAxis.y + this.zAxis.y, this.xAxis.z + this.yAxis.z + this.zAxis.z);
        return this.temp;
    }

    public void reset() {
        this.xAxis.set(1.0f, 0.0f, 0.0f);
        this.yAxis.set(0.0f, 1.0f, 0.0f);
        this.zAxis.set(0.0f, 0.0f, 1.0f);
    }
}
