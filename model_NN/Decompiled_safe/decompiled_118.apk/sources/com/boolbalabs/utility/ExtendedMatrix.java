package com.boolbalabs.utility;

import android.opengl.Matrix;

public class ExtendedMatrix extends Matrix {
    private static float[] temp = new float[32];

    /* JADX INFO: Multiple debug info for r9v1 float: [D('fx' float), D('centerX' float)] */
    /* JADX INFO: Multiple debug info for r10v1 float: [D('fy' float), D('centerY' float)] */
    /* JADX INFO: Multiple debug info for r11v1 float: [D('fz' float), D('centerZ' float)] */
    /* JADX INFO: Multiple debug info for r0v3 float: [D('rlf' float), D('sx' float)] */
    /* JADX INFO: Multiple debug info for r14v2 float: [D('upZ' float), D('sy' float)] */
    /* JADX INFO: Multiple debug info for r12v4 float: [D('rls' float), D('sz' float)] */
    public static void setLookAtM(float[] rm, int rmOffset, float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {
        float centerX2 = centerX - eyeX;
        float centerY2 = centerY - eyeY;
        float centerZ2 = centerZ - eyeZ;
        float rlf = 1.0f / Matrix.length(centerX2, centerY2, centerZ2);
        float fx = centerX2 * rlf;
        float fy = centerY2 * rlf;
        float fz = centerZ2 * rlf;
        float sx = (fy * upZ) - (fz * upY);
        float sy = (fz * upX) - (upZ * fx);
        float sz = (upY * fx) - (upX * fy);
        float rls = 1.0f / Matrix.length(sx, sy, sz);
        float sx2 = sx * rls;
        float sy2 = sy * rls;
        float sz2 = rls * sz;
        rm[rmOffset + 0] = sx2;
        rm[rmOffset + 1] = (sy2 * fz) - (sz2 * fy);
        rm[rmOffset + 2] = -fx;
        rm[rmOffset + 3] = 0.0f;
        rm[rmOffset + 4] = sy2;
        rm[rmOffset + 5] = (sz2 * fx) - (sx2 * fz);
        rm[rmOffset + 6] = -fy;
        rm[rmOffset + 7] = 0.0f;
        rm[rmOffset + 8] = sz2;
        rm[rmOffset + 9] = (sx2 * fy) - (sy2 * fx);
        rm[rmOffset + 10] = -fz;
        rm[rmOffset + 11] = 0.0f;
        rm[rmOffset + 12] = 0.0f;
        rm[rmOffset + 13] = 0.0f;
        rm[rmOffset + 14] = 0.0f;
        rm[rmOffset + 15] = 1.0f;
        translateM(rm, rmOffset, -eyeX, -eyeY, -eyeZ);
    }

    public static void rotateM(float[] m, int mOffset, float a, float x, float y, float z) {
        setRotateM(temp, 0, a, x, y, z);
        multiplyMM(temp, 16, m, mOffset, temp, 0);
        System.arraycopy(temp, 16, m, mOffset, 16);
    }

    public static void gluPerspective(float[] result, float fovy, float aspect, float zNear, float zFar) {
        float top = zNear * ((float) Math.tan(((double) fovy) * 0.008726646259971648d));
        float bottom = -top;
        Matrix.frustumM(result, 0, bottom * aspect, top * aspect, bottom, top, zNear, zFar);
    }
}
