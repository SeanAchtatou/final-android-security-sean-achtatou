package com.boolbalabs.utility;

import android.os.Bundle;

public class ActionQueue {
    private static final int QUEUE_SIZE = 10;
    private Bundle[] actionQueue = new Bundle[10];
    int getIndex = 0;
    int putIndex = 0;

    private void increasePutIndex() {
        this.putIndex++;
        if (this.putIndex == 10) {
            this.putIndex = 0;
        }
    }

    private void increaseGetIndex() {
        this.getIndex++;
        if (this.getIndex == 10) {
            this.getIndex = 0;
        }
    }

    public void put(Bundle newMovement) {
        this.actionQueue[this.putIndex] = newMovement;
        increasePutIndex();
    }

    public Bundle get() {
        if (isEmpty()) {
            return null;
        }
        int returnIndex = this.getIndex;
        increaseGetIndex();
        return this.actionQueue[returnIndex];
    }

    public boolean isEmpty() {
        return this.putIndex == this.getIndex;
    }
}
