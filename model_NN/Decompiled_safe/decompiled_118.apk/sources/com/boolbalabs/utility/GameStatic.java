package com.boolbalabs.utility;

import android.graphics.Point;
import android.os.Bundle;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.gamecomponents.GameCamera;
import com.boolbalabs.rollit.gamecomponents.RotatingCube;
import com.boolbalabs.rollit.settings.RollItConstants;

public class GameStatic {
    public static final float[] projectionM = new float[16];
    public static final float[] result = new float[3];
    public static final int[] viewport;

    static {
        int[] iArr = new int[4];
        iArr[2] = ScreenMetrics.screenWidthPix;
        iArr[3] = ScreenMetrics.screenHeightPix;
        viewport = iArr;
    }

    public static Bundle makeBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE);
        bundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_NONE);
        bundle.putInt(RollItConstants.KEY_STRENGTH, 1);
        bundle.putSerializable(RollItConstants.KEY_CURR_COORDS, RotatingCube.currentCoordinates);
        bundle.putSerializable(RollItConstants.KEY_MIDDLE_COORDS, RotatingCube.currentCoordinates);
        bundle.putSerializable(RollItConstants.KEY_TARGET_COORDS, RotatingCube.currentCoordinates);
        bundle.putFloatArray(RollItConstants.KEY_PARABOLA_COEFFS, new float[]{0.0f, 0.0f, 0.0f});
        return bundle;
    }

    public static boolean shiftWasLongEnough(Point touchDownPoint, Point touchUpPoint) {
        if (((float) vectorLength2D(touchDownPoint, touchUpPoint)) < 30.0f) {
            return false;
        }
        return true;
    }

    public static int detectMajorDirection(float screenAngle) {
        float resultAngle = limitAngle(GameCamera.getInstance().getRotationAngle() + screenAngle);
        if (-40.0f < resultAngle && resultAngle < 40.0f) {
            return RollItConstants.DIRECTION_EAST;
        }
        if (50.0f < resultAngle && resultAngle < 130.0f) {
            return RollItConstants.DIRECTION_NORTH;
        }
        if ((140.0f < resultAngle && resultAngle < 180.0f) || (-180.0f < resultAngle && resultAngle < -140.0f)) {
            return RollItConstants.DIRECTION_WEST;
        }
        if (-130.0f >= resultAngle || resultAngle >= -50.0f) {
            return RollItConstants.DIRECTION_NONE;
        }
        return RollItConstants.DIRECTION_SOUTH;
    }

    private static float limitAngle(float angle) {
        float angle2 = angle % 360.0f;
        if (Math.abs(angle2) <= 180.0f) {
            return angle2;
        }
        return angle2 - (((float) (angle2 > 0.0f ? 1 : -1)) * 360.0f);
    }

    public static double vectorLength2D(Point point1, Point point2) {
        return Math.sqrt(Math.pow((double) (point1.x - point2.x), 2.0d) + Math.pow((double) (point1.y - point2.y), 2.0d));
    }

    public static double flatAnglei(Point start, Point end) {
        int sign = 1;
        if (end.y > start.y) {
            sign = -1;
        }
        return ((double) sign) * Math.acos(((double) (end.x - start.x)) / vectorLength2D(start, end));
    }

    public static float vectorLength3D(Point3D begin, Point3D end) {
        return (float) Math.sqrt(Math.pow((double) (end.x - begin.x), 2.0d) + Math.pow((double) (end.y - begin.y), 2.0d) + Math.pow((double) (end.z - begin.z), 2.0d));
    }

    public static float radiusVectorLength3D(Point3D end) {
        return (float) Math.sqrt(Math.pow((double) end.x, 2.0d) + Math.pow((double) end.y, 2.0d) + Math.pow((double) end.z, 2.0d));
    }

    public static float parabola(float a, float b, float c, float x) {
        return (a * x * x) + (b * x) + c;
    }

    public static String getDirectionName(int direction) {
        switch (direction) {
            case RollItConstants.DIRECTION_NONE:
                return "NONE";
            case RollItConstants.DIRECTION_NORTH:
                return "NORTH";
            case RollItConstants.DIRECTION_SOUTH:
                return "SOUTH";
            case RollItConstants.DIRECTION_EAST:
                return "EAST";
            case RollItConstants.DIRECTION_WEST:
                return "WEST";
            default:
                return null;
        }
    }

    public static void printMatrix(String description, float[] matrix) {
        if (description == null) {
            description = "Matrix";
        }
        DebugLog.v(description, "* " + matrix[0] + " *" + "* " + matrix[4] + " *" + "* " + matrix[8] + " *" + "* " + matrix[12] + " *");
        DebugLog.v(description, "* " + matrix[1] + " *" + "* " + matrix[5] + " *" + "* " + matrix[9] + " *" + "* " + matrix[13] + " *");
        DebugLog.v(description, "* " + matrix[2] + " *" + "* " + matrix[6] + " *" + "* " + matrix[10] + " *" + "* " + matrix[14] + " *");
        DebugLog.v(description, "* " + matrix[3] + " *" + "* " + matrix[7] + " *" + "* " + matrix[11] + " *" + "* " + matrix[15] + " * ");
        DebugLog.v("Delimiter", "---------------------------------");
    }
}
