package com.boolbalabs.utility;

import java.io.Serializable;

public class Point3D implements Serializable {
    private static final long serialVersionUID = 1;
    private static final Point3D temp = new Point3D();
    public float x = 0.0f;
    public float y = 0.0f;
    public float z = 0.0f;

    public Point3D() {
    }

    public Point3D(float x2, float y2, float z2) {
        this.x = x2;
        this.y = y2;
        this.z = z2;
    }

    public Point3D(Point3D arg) {
        this.x = arg.x;
        this.y = arg.y;
        this.z = arg.z;
    }

    public void set(Point3D arg) {
        this.x = arg.x;
        this.y = arg.y;
        this.z = arg.z;
    }

    public void set(float x2, float y2, float z2) {
        this.x = x2;
        this.y = y2;
        this.z = z2;
    }

    public double norm() {
        return Math.sqrt((double) ((this.x * this.x) + (this.y * this.y) + (this.z * this.z)));
    }

    public void normalise() {
        double norm = Math.sqrt((double) ((this.x * this.x) + (this.y * this.y) + (this.z * this.z)));
        if (norm != 0.0d) {
            this.x = (float) (((double) this.x) / norm);
            this.y = (float) (((double) this.y) / norm);
            this.z = (float) (((double) this.z) / norm);
        }
    }

    public float dot(Point3D p) {
        return (this.x * p.x) + (this.y * p.y) + (this.z * p.z);
    }

    public float distanceTo(Point3D p) {
        return (float) Math.sqrt((double) (((this.x - p.x) * (this.x - p.x)) + ((this.y - p.y) * (this.y - p.y)) + ((this.z - p.z) * (this.z - p.z))));
    }

    public String toString() {
        return "Coordinates: (" + this.x + "/" + this.y + "/" + this.z + ")";
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public void setZ(float z2) {
        this.z = z2;
    }

    public Point3D invert() {
        temp.set(this.x == 0.0f ? 0.0f : -this.x, this.y == 0.0f ? 0.0f : -this.y, this.z == 0.0f ? 0.0f : -this.z);
        return temp;
    }
}
