package com.boolbalabs.rollit.settings;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.level.LevelManager;
import com.flurry.android.FlurryAgent;
import java.util.ArrayList;
import java.util.HashMap;

public class Settings {
    public static boolean ADS_ENABLED = false;
    public static String FLURRY_ID = FLURRY_ID_LITE;
    public static final String FLURRY_ID_FULL = "";
    public static final String FLURRY_ID_LITE = "Z8UIW28UARPPSFBGLGIG";
    public static final int GAME_FREE = 124;
    public static final int GAME_FULL = 978;
    public static int GAME_TYPE = GAME_FREE;
    public static final int GLOBAL_TEXT_SIZE_RIP = 24;
    public static final int LAUNCHES_BEFORE_SHOW_RATE_ME = 10;
    public static Typeface MAIN_TYPEFACE = null;
    public static final String MARKET_URL = "market://details?id=com.boolbalabs.rollit";
    private static final String PREF_CURRENT_LEVEL_PACK_ID = "CurrentLevelPack";
    private static final String PREF_CURRENT_LEVEL_PACK_SIZE = "CurrentLevelPackSize of pack ";
    private static final String PREF_KEY_LAUNCHED_FIRST_TIME = "LaunchedFirstTime";
    private static final String PREF_KEY_LAUNCH_NUMBER = "RollItLaunchNumber";
    private static final String PREF_KEY_MUSIC = "Music";
    private static final String PREF_KEY_RATE_ME_DIALOG_SHOWN = "RateMeDialogShown";
    private static final String PREF_KEY_SOUND = "Sound";
    private static final String PREF_KEY_TOTAL_SCORE = "TOTAL_SCORE_";
    private static final String PREF_KEY_TOTAL_STEPS = "TotalScore";
    private static final String PREF_KEY_VIBRATE = "Vibrate";
    private static final String PREF_LAST_SCENE_ID = "LAST_SCENE_ID";
    private static final String PREF_LEVEL_PLAY_TIME_BEFORE_PAUSE = "LevelPlayTimeBeforePause";
    private static final String PREF_MAX_AVELIBLE_LEVEL_NUMBER = "MaxAvelibleLevelNumber of pack ";
    public static final String SCORELOOP_GAME_CURRENCY_CODE = "ZYI";
    public static final String SCORELOOP_GAME_ID = "dbf3e185-e477-4499-ac52-57bcb4d9c159";
    public static final String SCORELOOP_GAME_SECRET = "VZCOrwfplkZ8ZeuUfXMXLtp+ChYr9YAkWJFTj8jBGEsPFz0EY+808g==";
    public static final long VIBRATE_ON_FALL_ATTEMPT_TIME = 150;
    public static final long VIBRATE_ON_STEP_TIME = 50;
    public static final String WEB_MARKET_URL = "http://market.android.com/details?id=com.boolbalabs.rollit";
    public static ArrayList<Integer> allowedKeyCodes = null;
    public static final int buttonSound = 2131099649;
    public static final int commonTextureRef = 2130837525;
    public static int currentLaunch = 1;
    public static int currentLevel = 1;
    public static int currentLevelPackID = RollItConstants.LEVEL_PACK_GRASS.intValue();
    public static int currentLevelPackSize = -1;
    public static boolean deviceIsVerySlow = false;
    public static int flurryCameraUsed = 0;
    public static int flurryResetUsed = 0;
    public static final int gameplayMusicThemeId = 2131099655;
    public static final int gameplayTextureRef = 2130837533;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    public static int lastSceneID = 0;
    public static boolean launchedFirstTime = true;
    public static long levelPlayTimeBeforePause = 0;
    public static final int mainMusicThemeId = 2131099655;
    public static int maxAvelibleLevelNumber = 1;
    public static final int menuTextureRef = 2130837542;
    public static boolean rateMeDialogHasBeenShown = false;
    public static boolean scoreloopInitialized = false;
    private static Settings settings;
    private static SharedPreferences sharedPreferences;
    private static ArrayList<String> slowDevicesList;
    public static int totalScore = 0;
    public static int totalSteps = 0;
    private Resources gameResources;

    public static Settings getInstance() {
        return settings;
    }

    public static void initialise(SharedPreferences sharedPreferences2, Resources gameResources2) {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                settings = new Settings();
                sharedPreferences = sharedPreferences2;
                settings.gameResources = gameResources2;
                settings.createAllowedKeys();
                settings.createSlowDevicesList();
                MAIN_TYPEFACE = Typeface.createFromAsset(gameResources2.getAssets(), "fonts/segoe_print_bold.ttf");
                isInitialised = true;
            }
        }
    }

    public static void release() {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                if (settings != null) {
                    settings = null;
                }
                isInitialised = false;
            }
        }
    }

    private Settings() {
    }

    public void saveSharedPreferences() {
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putInt(PREF_KEY_LAUNCH_NUMBER, currentLaunch);
        ed.putBoolean(PREF_KEY_RATE_ME_DIALOG_SHOWN, rateMeDialogHasBeenShown);
        ed.putInt(PREF_CURRENT_LEVEL_PACK_ID, currentLevelPackID);
        ed.putInt(PREF_KEY_TOTAL_STEPS + RollItConstants.LEVEL_PACK_GRASS, totalSteps);
        ed.putInt(PREF_KEY_TOTAL_SCORE + RollItConstants.LEVEL_PACK_GRASS, totalScore);
        ed.putInt(PREF_MAX_AVELIBLE_LEVEL_NUMBER + currentLevelPackID, maxAvelibleLevelNumber);
        ed.putInt(PREF_CURRENT_LEVEL_PACK_SIZE + currentLevelPackID, currentLevelPackSize);
        ed.putLong(PREF_LEVEL_PLAY_TIME_BEFORE_PAUSE, levelPlayTimeBeforePause);
        ed.putBoolean(PREF_KEY_SOUND, ZageCommonSettings.soundEnabled);
        ed.putBoolean(PREF_KEY_MUSIC, ZageCommonSettings.musicEnabled);
        ed.putBoolean(PREF_KEY_VIBRATE, ZageCommonSettings.vibroEnabled);
        ed.putInt(PREF_LAST_SCENE_ID, lastSceneID);
        ed.commit();
    }

    public void loadSharedPreferences() {
        currentLaunch = sharedPreferences.getInt(PREF_KEY_LAUNCH_NUMBER, 0);
        rateMeDialogHasBeenShown = sharedPreferences.getBoolean(PREF_KEY_RATE_ME_DIALOG_SHOWN, false);
        if (currentLevelPackID == 0) {
            currentLevelPackID = sharedPreferences.getInt(PREF_CURRENT_LEVEL_PACK_ID, RollItConstants.LEVEL_PACK_GRASS.intValue());
        }
        totalSteps = sharedPreferences.getInt(PREF_KEY_TOTAL_STEPS + currentLevelPackID, 0);
        totalScore = sharedPreferences.getInt(PREF_KEY_TOTAL_SCORE + currentLevelPackID, 0);
        maxAvelibleLevelNumber = sharedPreferences.getInt(PREF_MAX_AVELIBLE_LEVEL_NUMBER + currentLevelPackID, 1);
        currentLevelPackSize = sharedPreferences.getInt(PREF_CURRENT_LEVEL_PACK_SIZE + currentLevelPackID, -1);
        levelPlayTimeBeforePause = sharedPreferences.getLong(PREF_LEVEL_PLAY_TIME_BEFORE_PAUSE, 0);
        ZageCommonSettings.soundEnabled = sharedPreferences.getBoolean(PREF_KEY_SOUND, true);
        ZageCommonSettings.musicEnabled = sharedPreferences.getBoolean(PREF_KEY_MUSIC, true);
        ZageCommonSettings.vibroEnabled = sharedPreferences.getBoolean(PREF_KEY_VIBRATE, true);
    }

    public void loadSettingsOnResume() {
        lastSceneID = sharedPreferences.getInt(PREF_LAST_SCENE_ID, 4);
    }

    public void resetPlayerProgress() {
        LevelManager.resetLevels();
        maxAvelibleLevelNumber = 1;
        totalSteps = 0;
        totalScore = 0;
        saveSharedPreferences();
        launchedFirstTime = true;
        saveLaunchedFirstTime();
    }

    public static void increaseCurrentLevel() {
        if (currentLevel <= currentLevelPackSize) {
            currentLevel++;
        }
    }

    public static void askToIncreaseMaxAvalibleLevel() {
        if (currentLevel <= currentLevelPackSize && currentLevel == maxAvelibleLevelNumber) {
            maxAvelibleLevelNumber++;
            if (maxAvelibleLevelNumber <= currentLevelPackSize) {
                LevelManager.getInstance().unlockCurrentLevel();
            }
            getInstance().saveSharedPreferences();
        }
    }

    public static void unlockAllLevels() {
        maxAvelibleLevelNumber = 1;
        while (maxAvelibleLevelNumber <= 100) {
            LevelManager.getInstance().unlockCurrentLevel();
            maxAvelibleLevelNumber++;
        }
        getInstance().saveSharedPreferences();
    }

    public static boolean setCurrentLevel(int level) {
        if (level <= 0 || level > maxAvelibleLevelNumber) {
            return false;
        }
        currentLevel = level;
        return true;
    }

    public static int getCurrentLevel() {
        return currentLevel;
    }

    private void createAllowedKeys() {
        allowedKeyCodes = new ArrayList<>();
        allowedKeyCodes.add(4);
        allowedKeyCodes.add(82);
        allowedKeyCodes.add(25);
        allowedKeyCodes.add(24);
    }

    private void createSlowDevicesList() {
        slowDevicesList = new ArrayList<>();
        slowDevicesList.add("HTC Wildfire");
    }

    public int getLevelPackID() {
        return currentLevelPackID;
    }

    public static SharedPreferences getPreferences() {
        return sharedPreferences;
    }

    public boolean isKeyAllowed(int key) {
        if (allowedKeyCodes != null) {
            return allowedKeyCodes.contains(Integer.valueOf(key));
        }
        return false;
    }

    public void sendFlurryStatistic() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("CAMERA_USED", String.valueOf(flurryCameraUsed));
        parameters.put("RESET_USED", String.valueOf(flurryResetUsed));
        parameters.put("SOUND", String.valueOf(ZageCommonSettings.soundEnabled));
        parameters.put("MUSIC", String.valueOf(ZageCommonSettings.musicEnabled));
        parameters.put("VIBRO", String.valueOf(ZageCommonSettings.vibroEnabled));
        FlurryAgent.onEvent("SESSION_ENDED", parameters);
        DebugLog.i("Flurry", "Session ended. Camera used: " + flurryCameraUsed + ". Reset used: " + flurryResetUsed + " Sound/Music/Vibro: " + ZageCommonSettings.soundEnabled + "/" + ZageCommonSettings.musicEnabled + "/" + ZageCommonSettings.vibroEnabled);
    }

    public void detectSlowDevice() {
        if (!slowDevicesList.contains(Build.MODEL) || !Build.VERSION.RELEASE.contains("2.1")) {
            deviceIsVerySlow = false;
        } else {
            deviceIsVerySlow = true;
        }
    }

    public void loadLaunchedFirstTime() {
        launchedFirstTime = sharedPreferences.getBoolean(PREF_KEY_LAUNCHED_FIRST_TIME, true);
    }

    public void saveLaunchedFirstTime() {
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.putBoolean(PREF_KEY_LAUNCHED_FIRST_TIME, launchedFirstTime);
        ed.commit();
    }

    public static int getTotalScore() {
        return totalScore;
    }
}
