package com.boolbalabs.rollit;

import android.os.Bundle;
import android.os.Message;
import com.boolbalabs.lib.game.Game;
import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.rollit.menucomponents.ChooseLevelMenu;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;

public class SceneChooseLevel extends GameScene {
    private ChooseLevelMenu chooseLevelMenu;

    public SceneChooseLevel(Game game, int sceneId) {
        super(game, sceneId);
        registerGameComponent(new ChooseLevelMenu());
    }

    public void initialize() {
        this.chooseLevelMenu = (ChooseLevelMenu) findComponentByClass(ChooseLevelMenu.class);
        this.chooseLevelMenu.initialize();
    }

    public void onResume() {
        this.chooseLevelMenu.userInteractionEnabled = true;
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_HIDE_LOADING_TOAST);
    }

    public void performAction(int actionCode, Bundle actionParameters) {
        switch (actionCode) {
            case RollItConstants.ACTION_SCROLL_LEVELS_RIGHT /*51355*/:
                scrollLevelsRight(actionParameters);
                return;
            case RollItConstants.ACTION_SCROLL_LEVELS_LEFT /*51475*/:
                scrollLevelsLeft(actionParameters);
                return;
            case RollItConstants.ACTION_RESET_LEVELS /*51953*/:
                resetLevelButtons();
                return;
            case RollItConstants.ACTION_APPLY_BUTTONS_MOVEMENT_FINISH /*73904*/:
                applyMovementFinished();
                return;
            case RollItConstants.ACTION_DISABLE_USER_INPUT /*73905*/:
                disableUserInput();
                return;
            case RollItConstants.ACTION_PLAY_SELECTED_LEVEL /*75352*/:
                playSelectedLevel(actionParameters);
                return;
            case RollItConstants.ACTION_BACK /*77253*/:
                switchToMainMenu();
                return;
            default:
                return;
        }
    }

    public void onShow() {
        super.onShow();
    }

    public void onHide() {
        super.onHide();
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_HIDE_ADS);
    }

    private void playSelectedLevel(Bundle levelParamenter) {
        this.mainGame.switchGameScene(1);
    }

    private void scrollLevelsLeft(Bundle actionParameters) {
        this.chooseLevelMenu.scrollLevelsLeft();
    }

    private void scrollLevelsRight(Bundle actionParameters) {
        this.chooseLevelMenu.scrollLevelsRight();
    }

    private void applyMovementFinished() {
        this.chooseLevelMenu.userInteractionEnabled = true;
        this.chooseLevelMenu.checkArrowsVisibility();
    }

    public void disableUserInput() {
        this.chooseLevelMenu.userInteractionEnabled = false;
    }

    private void switchToLevelpackMenu() {
        Settings.getInstance().saveSharedPreferences();
        askGameToSwitchGameScene(5);
    }

    private void switchToMainMenu() {
        Settings.getInstance().saveSharedPreferences();
        askGameToSwitchGameScene(2);
    }

    public GameScene getInstance() {
        return this;
    }

    public void unlockCurrentLevel() {
        this.chooseLevelMenu.unlockCurrentLevel();
    }

    private void resetLevelButtons() {
        if (this.chooseLevelMenu != null) {
            this.chooseLevelMenu.resetLevelButtons();
        }
    }

    public void loadLevelpack(int levelPackID) {
        this.chooseLevelMenu.loadCurrentLevelPack(levelPackID);
    }

    private void changeAdsViewGravity(int gravity) {
        Message msg = new Message();
        msg.what = RollItConstants.MESSAGE_CHANGE_ADS_GRAVITY;
        msg.arg1 = gravity;
        sendMessageToActivity(msg);
    }
}
