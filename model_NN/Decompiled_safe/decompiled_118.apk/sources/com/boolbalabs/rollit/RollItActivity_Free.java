package com.boolbalabs.rollit;

import android.location.Location;
import android.os.Bundle;
import android.widget.FrameLayout;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import com.flurry.android.FlurryAgent;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.inmobi.androidsdk.EducationType;
import com.inmobi.androidsdk.EthnicityType;
import com.inmobi.androidsdk.GenderType;
import com.inmobi.androidsdk.InMobiAdDelegate;
import com.inmobi.androidsdk.impl.InMobiAdView;
import java.util.Date;

public class RollItActivity_Free extends RollItActivity implements AdListener, InMobiAdDelegate {
    private static final String IN_MOBI_SITE_ID = "4028cba630724cd90131530a721a0dd4";
    private static final boolean IN_MOBI_TEST_MODE = false;
    private static final int SWITCH_COUNT_BEFORE_RELOADING_ADS = 10;
    private static int sceneSwitchCounter = 0;
    private final String ADMOB_ID = "a14e298812c7db0";
    private final boolean ADMOB_TESTING = false;
    private final String AD_KEYWORDS = "online games gaming puzzle 3D  cube brain fun smart logic money rich kids children business auto cars drinks";
    private boolean adMobReceived = false;
    private AdRequest adMobRequest;
    private AdView adMobView;
    private InMobiAdView inMobiAdView;
    private boolean inMobiReceived = false;

    public AdRequest buildAdMobRequest() {
        AdRequest adMobRequest2 = new AdRequest();
        adMobRequest2.addKeyword("online games gaming puzzle 3D  cube brain fun smart logic money rich kids children business auto cars drinks");
        adMobRequest2.setTesting(false);
        return adMobRequest2;
    }

    public void onCreate(Bundle savedInstanceState) {
        Settings.GAME_TYPE = Settings.GAME_FREE;
        Settings.FLURRY_ID = Settings.FLURRY_ID_LITE;
        Settings.ADS_ENABLED = true;
        super.onCreate(savedInstanceState);
    }

    public void onCreateAds(FrameLayout parentView) {
        if (Settings.ADS_ENABLED && !this.isLdpiDevice) {
            createAdMobAds(parentView);
            createInMobiAds(parentView);
        }
    }

    private void createAdMobAds(FrameLayout parentView) {
        try {
            this.adMobView = new AdView(this, AdSize.BANNER, "a14e298812c7db0");
            this.adMobView.setAdListener(this);
            this.adMobView.setId(RollItConstants.ADS_VIEW_ID);
            this.adMobRequest = buildAdMobRequest();
            this.adMobView.loadAd(this.adMobRequest);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ScreenMetrics.convertRipToPixel(320.0f), ScreenMetrics.convertRipToPixel(50.0f));
            params.gravity = 81;
            params.bottomMargin = ScreenMetrics.convertRipToPixel(2.0f);
            parentView.addView(this.adMobView, params);
            this.adMobView.setVisibility(8);
        } catch (Exception e) {
            FlurryAgent.onError("AdmobError", e.getStackTrace().toString(), Settings.FLURRY_ID_FULL);
        }
    }

    private void createInMobiAds(FrameLayout parentView) {
        try {
            this.inMobiAdView = (InMobiAdView) findViewById(R.id.adview);
            this.inMobiAdView.initialize(getApplicationContext(), this, this, 9);
            this.inMobiAdView.loadNewAd();
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ScreenMetrics.convertRipToPixel(320.0f), ScreenMetrics.convertRipToPixel(48.0f));
            params.gravity = 81;
            params.bottomMargin = ScreenMetrics.convertRipToPixel(2.0f);
            parentView.removeView(this.inMobiAdView);
            parentView.addView(this.inMobiAdView, params);
            this.inMobiAdView.setVisibility(8);
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            FlurryAgent.onError("inMobiError", e2.getStackTrace().toString(), Settings.FLURRY_ID_FULL);
        }
    }

    private void refreshAds() {
        this.adMobReceived = false;
        this.inMobiReceived = false;
        if (this.adMobView != null) {
            this.adMobView.loadAd(this.adMobRequest);
            DebugLog.v("DEBUG ADS", "ABMOB refreshed");
        }
        if (this.inMobiAdView != null) {
            this.inMobiAdView.loadNewAd();
            DebugLog.v("DEBUG ADS", "INMOBI refreshed");
        }
    }

    /* access modifiers changed from: protected */
    public void showAds() {
        sceneSwitchCounter++;
        if (sceneSwitchCounter >= 10) {
            refreshAds();
            sceneSwitchCounter = 0;
        }
        if (this.adMobReceived && this.adMobView != null) {
            this.adMobView.setVisibility(0);
            this.inMobiAdView.setVisibility(8);
            DebugLog.v("DEBUG ADS", "ADMOB SHOWN");
        } else if (this.inMobiReceived && this.inMobiAdView != null) {
            this.inMobiAdView.setVisibility(0);
            this.adMobView.setVisibility(8);
            DebugLog.v("DEBUG ADS", "INMOBI SHOWN");
        }
    }

    /* access modifiers changed from: protected */
    public void hideAds() {
        if (this.adMobView != null) {
            this.adMobView.setVisibility(8);
        }
        if (this.inMobiAdView != null) {
            this.inMobiAdView.setVisibility(8);
        }
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        DebugLog.v("ADS", "AdMob failed");
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        this.adMobReceived = true;
        DebugLog.v("ADS", "AdMob received");
    }

    public void adRequestCompleted(InMobiAdView arg0) {
        this.inMobiReceived = true;
        DebugLog.v("ADS", "InMobi received");
    }

    public void adRequestFailed(InMobiAdView arg0) {
        DebugLog.v("ADS", "InMobi failed");
    }

    public int age() {
        return 0;
    }

    public String areaCode() {
        return null;
    }

    public Location currentLocation() {
        return null;
    }

    public Date dateOfBirth() {
        return null;
    }

    public EducationType education() {
        return null;
    }

    public EthnicityType ethnicity() {
        return null;
    }

    public GenderType gender() {
        return null;
    }

    public int income() {
        return 0;
    }

    public String interests() {
        return null;
    }

    public boolean isLocationInquiryAllowed() {
        return false;
    }

    public boolean isPublisherProvidingLocation() {
        return false;
    }

    public String keywords() {
        return "online games gaming puzzle 3D  cube brain fun smart logic money rich kids children business auto cars drinks";
    }

    public String postalCode() {
        return null;
    }

    public String searchString() {
        return null;
    }

    public String siteId() {
        return IN_MOBI_SITE_ID;
    }

    public boolean testMode() {
        return false;
    }
}
