package com.boolbalabs.rollit.level;

import android.content.SharedPreferences;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.rollit.InGameHelp;
import com.boolbalabs.rollit.SceneGameplay;
import com.boolbalabs.rollit.gamecomponents.two_d.ScoreBoard;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import com.flurry.android.FlurryAgent;
import java.util.HashMap;

public class Level extends ZNode {
    private static final int BASE_SCORE_COEFF = 30;
    public static final String BEST_SCORE_TAG = "best_score: ";
    public static final String BEST_STEPS_TAG = "best_steps: ";
    public static final int DEFAULT_SCORE = 0;
    public static final int DEFAULT_STEPS_NUMBER = 0;
    private static final int LEVEL_BASE_SCORE = 50;
    private static final int MAX_SCORE_TIME_COEFF = 45;
    private static final int MILLISEC_TO_SEC_COEFF = 1000;
    private static final int MIN_TIME_BONUS = 10;
    private static final int QUICK_TIME_COEFF = 6;
    private static final int STEP_COST = 1;
    private static final int TIME_BONUS_BASE = 10;
    private static final int TIME_BONUS_COEFF = 5;
    private static SharedPreferences prefs;
    private int bestScore;
    private boolean bestScoreImproved = false;
    private int bestStepNumber;
    private boolean bestStepsImproved = false;
    private int currentScore;
    private int difficulty;
    private long levelCompleteTime;
    private int levelID;
    private int levelNumber;
    private long levelStartTime;
    private boolean levelWasCompletedBefore = true;
    private int maxSteps;
    private int minSteps;
    private String solutionStepSequence;
    private int stepsBeforeSolveButtonShows = 0;
    private boolean timeStarted = false;
    private boolean wasSolvedWithCheat = false;
    private final int xmlRef;

    public Level(int xmlRef2) {
        super(-1, 0);
        this.xmlRef = xmlRef2;
        prefs = Settings.getPreferences();
    }

    public void update() {
    }

    public void loadInfoFromPreferences() {
        boolean z;
        this.bestStepNumber = prefs.getInt(BEST_STEPS_TAG + String.valueOf(this.levelID), 0);
        this.bestScore = prefs.getInt(BEST_SCORE_TAG + String.valueOf(this.levelID), 0);
        if (this.bestStepNumber != 0) {
            z = true;
        } else {
            z = false;
        }
        this.levelWasCompletedBefore = z;
        DebugLog.v("Level", "Loaded :: " + this);
    }

    public void calculateLevelResults(ScoreBoard scoreBoard) {
        stopLevelTimer();
        int currentStepNumber = scoreBoard.getStepNumber();
        DebugLog.i("Level", "Level: " + this.levelID + "; Time: " + this.levelCompleteTime);
        this.currentScore = calculateScore(currentStepNumber, this.levelCompleteTime);
        saveResultsToPreferences(currentStepNumber, this.currentScore);
    }

    public int calculateScore(int stepsNumber, long levelCompleteTime2) {
        int i;
        int i2;
        if (this.wasSolvedWithCheat) {
            return 1;
        }
        int score = calculateBaseScore();
        if (stepsNumber < this.maxSteps) {
            i = calculateStepsBonus(stepsNumber);
        } else {
            i = 0;
        }
        int score2 = score + i;
        if (stepsNumber <= this.minSteps) {
            i2 = calculateTimeBonus(levelCompleteTime2);
        } else {
            i2 = 0;
        }
        int score3 = score2 + i2;
        if (stepsNumber < this.minSteps) {
            reportMinStepsImproved(stepsNumber);
        }
        return score3;
    }

    private void reportMinStepsImproved(int stepsNumber) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("INFO", "LVL: " + this.levelID + "; OLD: " + this.minSteps + "; NEW: " + stepsNumber);
        FlurryAgent.onEvent("MIN_LEVEL_STEPS IMPROVED!", parameters);
    }

    private int calculateStepsBonus(int stepsNumber) {
        int bonus = (this.maxSteps - stepsNumber) * this.difficulty * 1;
        DebugLog.v("Bonus calc", "Steps bonus: " + bonus);
        return bonus;
    }

    private int calculateTimeBonus(long time) {
        int timeBonus;
        if (time > ((long) calculateMaxScoredTime())) {
            timeBonus = 10;
        } else {
            timeBonus = (int) (10.0f + (((float) calculateMaxTimeBonus()) * (1.0f - (((float) time) / ((float) calculateMaxScoredTime())))));
            DebugLog.v("Bonus calc", "Base time bonus: " + timeBonus);
            if (time < calculateMaxExtraScoredTime()) {
                int timeExtraBonus = (int) (((float) ((this.difficulty * 10) + 80)) * (1.0f - (((float) time) / ((float) (calculateMaxExtraScoredTime() - calculateMinPossibleTime())))));
                timeBonus += timeExtraBonus;
                DebugLog.v("Bonus calc", "Extra time bonus: " + timeExtraBonus);
            }
        }
        DebugLog.v("Time bonus", "Total time bonus: " + timeBonus);
        return timeBonus;
    }

    private long calculateMinPossibleTime() {
        return (long) (this.minSteps * 400);
    }

    private long calculateMaxExtraScoredTime() {
        return calculateMinPossibleTime() * 6;
    }

    private int calculateMaxScoredTime() {
        return this.difficulty * MAX_SCORE_TIME_COEFF * MILLISEC_TO_SEC_COEFF;
    }

    public void saveResultsToPreferences(int currentStepNumber, int currentScore2) {
        SharedPreferences.Editor ed = prefs.edit();
        saveBestStepsIfAny(ed, currentStepNumber);
        saveBestScoreIfAny(ed, currentScore2);
        Settings.getInstance().saveSharedPreferences();
    }

    public void resetLevelStatistics() {
        SharedPreferences.Editor ed = prefs.edit();
        ed.remove(BEST_STEPS_TAG + String.valueOf(this.levelID));
        ed.remove(BEST_SCORE_TAG + String.valueOf(this.levelID));
        ed.commit();
    }

    private void saveBestStepsIfAny(SharedPreferences.Editor ed, int currentStepNumber) {
        this.bestStepsImproved = false;
        if (currentStepNumber < this.bestStepNumber || !this.levelWasCompletedBefore) {
            this.bestStepNumber = currentStepNumber;
            this.levelWasCompletedBefore = true;
            this.bestStepsImproved = true;
            saveNewTotalSteps(this.bestStepNumber);
            ed.putInt(BEST_STEPS_TAG + String.valueOf(this.levelID), this.bestStepNumber);
            ed.commit();
        }
    }

    private void saveBestScoreIfAny(SharedPreferences.Editor ed, int currentScore2) {
        this.bestScoreImproved = false;
        if (this.bestStepsImproved || scoreImproved(currentScore2)) {
            this.bestScore = currentScore2;
            this.bestScoreImproved = true;
            saveNewTotalScore(this.bestScore);
            ed.putInt(BEST_SCORE_TAG + String.valueOf(this.levelID), currentScore2);
            ed.commit();
        }
    }

    private boolean scoreImproved(int currentScore2) {
        return this.bestScore < currentScore2;
    }

    private void saveNewTotalScore(int totalScore) {
        int previousBestScore = prefs.getInt(BEST_SCORE_TAG + String.valueOf(this.levelID), 0);
        int previousTotalScore = Settings.totalScore;
        DebugLog.v("Score", "Old total score: " + previousTotalScore);
        int newTotalScore = (previousTotalScore - previousBestScore) + this.currentScore;
        Settings.totalScore = newTotalScore;
        DebugLog.v("Score", "New total score: " + newTotalScore);
    }

    private void saveNewTotalSteps(int currentStepsNumber) {
        Settings.totalSteps = (Settings.totalSteps - prefs.getInt(BEST_STEPS_TAG + String.valueOf(this.levelID), 0)) + this.bestStepNumber;
    }

    public void askToShowInGameHelpIfAny() {
        if (!InGameHelp.levelsWithHelpIDs.contains(Integer.valueOf(this.levelID)) || this.levelWasCompletedBefore) {
            DebugLog.v("Level", "LevelTimer STARTED from !showGameHelp");
            startLevelTimer();
            return;
        }
        LevelManager.getInstance().getGameplayScene().sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_DIALOG_IN_GAME_HELP);
    }

    public void startLevelTimer() {
        this.levelStartTime = System.currentTimeMillis();
        Settings.levelPlayTimeBeforePause = 0;
        this.timeStarted = true;
        DebugLog.v("Level", "LevelTimer STARTED: " + this.levelStartTime);
    }

    public void stopLevelTimer() {
        this.levelCompleteTime = System.currentTimeMillis() - this.levelStartTime;
        this.levelCompleteTime += Settings.levelPlayTimeBeforePause;
        DebugLog.v("Level", "LevelTimer STOPPED: " + this.levelCompleteTime);
        DebugLog.v("Level", "LevelTimer LevelCompleteTimeBeforePause: " + Settings.levelPlayTimeBeforePause);
        Settings.levelPlayTimeBeforePause = 0;
        this.timeStarted = false;
    }

    public void pauseLevelTimer() {
        if (this.timeStarted) {
            Settings.levelPlayTimeBeforePause += System.currentTimeMillis() - this.levelStartTime;
            DebugLog.v("Level", "LevelTimer PAUSED: " + Settings.levelPlayTimeBeforePause);
        }
    }

    public void resumeLevelTimer() {
        this.levelStartTime = System.currentTimeMillis();
        this.timeStarted = true;
    }

    public void setLevelNumber(int levelNumber2) {
        this.levelNumber = levelNumber2;
    }

    public int getLevelNumber() {
        return this.levelNumber;
    }

    public int getXmlRef() {
        return this.xmlRef;
    }

    public boolean canBeLoaded() {
        return isCompleted() || this.levelNumber == Settings.maxAvelibleLevelNumber;
    }

    public boolean isCompleted() {
        return this.levelNumber < Settings.maxAvelibleLevelNumber;
    }

    public int getBestScore() {
        return this.bestScore;
    }

    public void setID(int id) {
        this.levelID = id;
    }

    public int getID() {
        return this.levelID;
    }

    public int getBestStepNumber() {
        return this.bestStepNumber;
    }

    public boolean isStepNumberImproved() {
        return this.bestStepsImproved;
    }

    public boolean isScoreImproved() {
        return this.bestScoreImproved;
    }

    public boolean wasCompletedBefore() {
        return this.levelWasCompletedBefore;
    }

    public String toString() {
        return "Level " + this.levelID + ". LVL NUMBER: " + this.levelNumber + ". BEST: " + this.bestStepNumber;
    }

    public void setParentScene(SceneGameplay sceneGameplay) {
        this.parentGameScene = sceneGameplay;
    }

    public void setDifficulty(int difficulty2) {
        this.difficulty = difficulty2;
    }

    public void setSovleButtonStepOffset(int stepNubmer) {
        this.stepsBeforeSolveButtonShows = stepNubmer;
    }

    public int getSolveButtonStepOffset() {
        return this.stepsBeforeSolveButtonShows;
    }

    public boolean isSolveButtonAvailable() {
        return this.stepsBeforeSolveButtonShows > 0;
    }

    private int calculateMaxTimeBonus() {
        return (this.difficulty * 5) + 10;
    }

    private int calculateBaseScore() {
        return (this.difficulty * BASE_SCORE_COEFF) + LEVEL_BASE_SCORE;
    }

    public void setMaxSteps(int maxSteps2) {
        this.maxSteps = maxSteps2;
    }

    public void setMinSteps(int minSteps2) {
        this.minSteps = minSteps2;
    }

    public int getCurrentScore() {
        return this.currentScore;
    }

    public void setSolution(String solution) {
        this.solutionStepSequence = solution;
    }

    public long getCurrentPlayTime() {
        return System.currentTimeMillis() - this.levelStartTime;
    }

    public String getSolution() throws IllegalArgumentException {
        if (this.solutionStepSequence != null) {
            return this.solutionStepSequence;
        }
        throw new IllegalArgumentException("Attempt to handle not existing solution. Solution does not exist in xml of level " + this.levelID);
    }

    public void setSolveButtonUsed() {
        this.wasSolvedWithCheat = true;
    }

    public boolean solveButtonUsed() {
        return this.wasSolvedWithCheat;
    }

    public void restart() {
        this.wasSolvedWithCheat = false;
    }
}
