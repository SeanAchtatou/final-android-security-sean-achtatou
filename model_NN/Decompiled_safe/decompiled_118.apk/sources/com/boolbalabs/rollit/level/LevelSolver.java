package com.boolbalabs.rollit.level;

import android.os.Bundle;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.rollit.gamecomponents.GameCamera;
import com.boolbalabs.rollit.gamecomponents.RotatingCube;
import com.boolbalabs.rollit.level.LevelLoader;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.GameStatic;
import javax.microedition.khronos.opengles.GL10;

public class LevelSolver extends ZNode {
    private static final String COMMAND_DELIMITER = "\\W+";
    private final long PERFORM_STEP_DELAY = 50;
    private int currentStepDirection = 0;
    private int currentStepNumber = 0;
    private long currentTime;
    private long delayStartTime;
    private boolean delayTimerStarted = false;
    private String[] literalSolution;
    private Bundle movementBundle = GameStatic.makeBundle();
    private int[] numericSolution;
    private boolean solvingActivated = false;

    public LevelSolver() {
        super(-1, 0);
    }

    public void initialaizeWithSolution(String solutionOfCurrentLevel) {
        extractLiteralSolution(solutionOfCurrentLevel);
        decodeSolution();
        resetSolutionVariables();
    }

    public void startSolving() {
        this.solvingActivated = true;
    }

    private void extractLiteralSolution(String solution) {
        this.literalSolution = solution.split(COMMAND_DELIMITER);
        int solutionStepNumber = this.literalSolution.length;
        for (int i = 0; i < solutionStepNumber; i++) {
            this.literalSolution[i] = this.literalSolution[i].trim();
        }
    }

    private void decodeSolution() {
        int solutionStepNumber = this.literalSolution.length;
        this.numericSolution = new int[solutionStepNumber];
        for (int i = 0; i < solutionStepNumber; i++) {
            this.numericSolution[i] = LevelLoader.DIRECTION.valueOf(this.literalSolution[i]).getDirection();
        }
    }

    private void resetSolutionVariables() {
        this.currentStepNumber = 0;
    }

    private void printSolution() {
        DebugLog.v("Solver", "Solution initialized: ");
        int solutionStepNumber = this.numericSolution.length;
        for (int i = 0; i < solutionStepNumber; i++) {
            DebugLog.v("Solver", String.valueOf(this.numericSolution[i]) + " (" + this.literalSolution[i] + ")");
        }
    }

    public void drawNode(GL10 gl) {
    }

    public void update() {
        if (this.solvingActivated) {
            solve();
        }
    }

    private void solve() {
        if (gameComponentsAreStable()) {
            processNextStep();
        }
    }

    private void processNextStep() {
        if (!this.delayTimerStarted) {
            startDelayTimer();
            return;
        }
        updateDelayTimer();
        if (this.currentTime > 50) {
            performStepIfPosible();
        }
    }

    private void updateDelayTimer() {
        this.currentTime = System.currentTimeMillis() - this.delayStartTime;
    }

    private void startDelayTimer() {
        this.delayTimerStarted = true;
        this.delayStartTime = System.currentTimeMillis();
    }

    private void performStepIfPosible() {
        stopDelayTimer();
        if (gameComponentsAreStable()) {
            selectCurrentStep();
            packMovementBundle();
            performAction(RollItConstants.ACTION_SELECT_MOVEMENT_TYPE, this.movementBundle);
            checkForLastStep();
            DebugLog.v("Solver", String.valueOf(this.currentStepNumber) + ". " + GameStatic.getDirectionName(this.currentStepDirection));
        }
    }

    private void checkForLastStep() {
        if (this.currentStepNumber >= this.numericSolution.length) {
            stopSolving();
        }
    }

    private void stopDelayTimer() {
        this.delayTimerStarted = false;
    }

    private void selectCurrentStep() {
        this.currentStepDirection = this.numericSolution[this.currentStepNumber];
        this.currentStepNumber++;
    }

    private void packMovementBundle() {
        this.movementBundle.putInt(RollItConstants.KEY_DIRECTION, this.currentStepDirection);
        this.movementBundle.putSerializable(RollItConstants.KEY_CURR_COORDS, RotatingCube.currentCoordinates);
    }

    public void stopSolving() {
        this.solvingActivated = false;
        performAction(RollItConstants.ACTION_ENABLE_USER_INPUT, null);
    }

    private boolean gameComponentsAreStable() {
        return RotatingCube.state == 43001 && GameCamera.state == 43001;
    }
}
