package com.boolbalabs.rollit.level;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.RollItGame;
import com.boolbalabs.rollit.SceneGameplay;
import com.boolbalabs.rollit.gamecomponents.GameField;
import com.boolbalabs.rollit.settings.RollItConstants;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class LevelLoader {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$boolbalabs$rollit$level$LevelLoader$CELL_TYPE = null;
    private static LevelLoader instance = null;
    private static boolean isInitialised = false;
    private static final long serialVersionUID = 1;
    private int condition;
    private int direction;
    private RollItGame game;
    private GameField gameField;
    private SceneGameplay gameplayScene;
    private int levelTheme;
    private Level requestedLevel = new Level(R.xml.grass_level_with_all_cells);
    private Resources res;
    private String strCondition;
    private String strDirection;
    private String strLevelTheme;
    private String strType;
    private int targetX;
    private int targetZ;
    private int throwStrength;
    private CELL_TYPE type;
    private int x;
    private int y;
    private int z;

    private enum CELL_TYPE {
        START,
        FINISH,
        NORMAL,
        TELEPORT,
        CATAPULT,
        BUTTON,
        SHIFT,
        FALL_ON_STEP,
        BRIDGE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$boolbalabs$rollit$level$LevelLoader$CELL_TYPE() {
        int[] iArr = $SWITCH_TABLE$com$boolbalabs$rollit$level$LevelLoader$CELL_TYPE;
        if (iArr == null) {
            iArr = new int[CELL_TYPE.values().length];
            try {
                iArr[CELL_TYPE.BRIDGE.ordinal()] = 9;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[CELL_TYPE.BUTTON.ordinal()] = 6;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[CELL_TYPE.CATAPULT.ordinal()] = 5;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[CELL_TYPE.FALL_ON_STEP.ordinal()] = 8;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[CELL_TYPE.FINISH.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[CELL_TYPE.NORMAL.ordinal()] = 3;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[CELL_TYPE.SHIFT.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[CELL_TYPE.START.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[CELL_TYPE.TELEPORT.ordinal()] = 4;
            } catch (NoSuchFieldError e9) {
            }
            $SWITCH_TABLE$com$boolbalabs$rollit$level$LevelLoader$CELL_TYPE = iArr;
        }
        return iArr;
    }

    private enum LEVEL_THEME {
        GRASS(RollItConstants.TEXTURE_THEME_GRASS),
        ICE(RollItConstants.TEXTURE_THEME_ICE),
        SAND(RollItConstants.TEXTURE_THEME_SAND);
        
        int textureTheme;

        private LEVEL_THEME(int theme) {
            this.textureTheme = theme;
        }

        public int getTheme() {
            return this.textureTheme;
        }
    }

    enum DIRECTION {
        NORTH(RollItConstants.DIRECTION_NORTH),
        SOUTH(RollItConstants.DIRECTION_SOUTH),
        EAST(RollItConstants.DIRECTION_EAST),
        WEST(RollItConstants.DIRECTION_WEST);
        
        int direction;

        private DIRECTION(int dir) {
            this.direction = dir;
        }

        /* access modifiers changed from: package-private */
        public int getDirection() {
            return this.direction;
        }
    }

    private enum CONDITION {
        RED(RollItConstants.FALL_ON_NOT_SPECIAL_SIDE_DOWN),
        GREEN(RollItConstants.FALL_ON_SPECIAL_SIDE_DOWN),
        INITIALY_UP(RollItConstants.BRIDGE_CELL_UP),
        INITIALY_DOWN(RollItConstants.BRIDGE_CELL_DOWN);
        
        int cell_condition;

        private CONDITION(int condition) {
            this.cell_condition = condition;
        }

        /* access modifiers changed from: package-private */
        public int getCondition() {
            return this.cell_condition;
        }
    }

    private LevelLoader() {
    }

    public static LevelLoader getInstance() {
        return instance;
    }

    public static void initialize() {
        if (!isInitialised) {
            instance = new LevelLoader();
            isInitialised = true;
        }
    }

    public void initWithGame(RollItGame rollItGame) {
        this.game = rollItGame;
        this.res = this.game.getGameResources();
        this.gameplayScene = (SceneGameplay) this.game.getGameScene(1);
    }

    public void loadTextureThemes() {
        loadLevel(R.xml.grass_level_with_all_cells);
    }

    public void askToLoadLevel(int levelNumber) {
        this.requestedLevel = LevelManager.getInstance().getLevel(levelNumber - 1);
        if (this.requestedLevel.canBeLoaded()) {
            synchronized (this.gameplayScene) {
                loadLevel(this.requestedLevel.getXmlRef());
            }
        }
        this.gameplayScene.askGameToSwitchGameScene(1);
    }

    public void unloadOldLevel() {
        if (this.gameField == null) {
            this.gameField = this.gameplayScene.getGameField();
        }
        this.gameField.cleanUpLevel();
    }

    private void loadLevel(int levelRef) {
        unloadOldLevel();
        prepareGameFieldFromFile(levelRef);
        this.gameField.initialize();
        this.gameField.loadContent();
        this.gameplayScene.getGameCamera().set(this.gameField.start.x, this.gameField.start.y + 6.0f, this.gameField.start.z + 6.0f, this.gameField.start.x, this.gameField.start.y, this.gameField.start.z);
        this.gameplayScene.getRotatingCube().initialize();
    }

    private void createCell(CELL_TYPE type2, int x2, int y2, int z2, int direction2, int targetX2, int targetZ2, int throwStrength2, int condition2) {
        switch ($SWITCH_TABLE$com$boolbalabs$rollit$level$LevelLoader$CELL_TYPE()[type2.ordinal()]) {
            case 1:
                this.gameField.add(x2, y2, z2, 1);
                this.gameField.start.set((float) x2, (float) y2, (float) z2);
                return;
            case 2:
                this.gameField.add(x2, y2, z2, 2);
                return;
            case 3:
                this.gameField.add(x2, y2, z2, 1);
                return;
            case 4:
                this.gameField.add(x2, y2, z2, targetX2, targetZ2, 3);
                return;
            case 5:
                this.gameField.add(x2, y2, z2, direction2, throwStrength2, 4);
                return;
            case 6:
                this.gameField.add(x2, y2, z2, targetX2, targetZ2, 5);
                return;
            case 7:
                this.gameField.add(x2, y2, z2, direction2, 6);
                return;
            case 8:
                this.gameField.add(x2, y2, z2, condition2, 7);
                return;
            case 9:
                this.gameField.add(x2, y2, z2, condition2, 8);
                return;
            default:
                return;
        }
    }

    private void getGameFieldFromXML(int fileRef) throws XmlPullParserException, IOException {
        XmlResourceParser xpp = this.res.getXml(fileRef);
        try {
            xpp.next();
            for (int eventType = xpp.getEventType(); eventType != 1; eventType = xpp.next()) {
                if (eventType == 2) {
                    String name = xpp.getName();
                    if (name.equals("cell")) {
                        this.strType = xpp.getAttributeValue(null, "type");
                        this.type = CELL_TYPE.valueOf(this.strType);
                        this.x = xpp.getAttributeIntValue(null, "x", 0);
                        this.y = xpp.getAttributeIntValue(null, "y", 0);
                        this.z = xpp.getAttributeIntValue(null, "z", 0);
                        this.targetX = xpp.getAttributeIntValue(null, "target_x", -1);
                        this.targetZ = xpp.getAttributeIntValue(null, "target_z", -1);
                        this.throwStrength = xpp.getAttributeIntValue(null, RollItConstants.KEY_STRENGTH, -1);
                        this.strDirection = xpp.getAttributeValue(null, RollItConstants.KEY_DIRECTION);
                        if (this.strDirection == null) {
                            this.direction = RollItConstants.DIRECTION_NONE;
                        } else {
                            this.direction = DIRECTION.valueOf(this.strDirection).getDirection();
                        }
                        this.strCondition = xpp.getAttributeValue(null, "condition");
                        if (this.strCondition == null) {
                            this.condition = -1;
                        } else {
                            this.condition = CONDITION.valueOf(this.strCondition).getCondition();
                        }
                        createCell(this.type, this.x, this.y, this.z, this.direction, this.targetX, this.targetZ, this.throwStrength, this.condition);
                    } else if (name.equals("level")) {
                        this.strLevelTheme = xpp.getAttributeValue(null, "theme");
                        this.levelTheme = LEVEL_THEME.valueOf(this.strLevelTheme).getTheme();
                        this.gameplayScene.setTextureTheme(this.levelTheme);
                        this.requestedLevel.setMaxSteps(xpp.getAttributeIntValue(null, "max_steps", -1));
                        this.requestedLevel.setMinSteps(xpp.getAttributeIntValue(null, "min_steps", -1));
                        this.requestedLevel.setDifficulty(xpp.getAttributeIntValue(null, "difficulty", -1));
                        this.requestedLevel.setSovleButtonStepOffset(xpp.getAttributeIntValue(null, "solve_steps", 0));
                        if (this.requestedLevel.getSolveButtonStepOffset() != 0) {
                            this.requestedLevel.setSolution(xpp.getAttributeValue(null, "solution"));
                        } else {
                            this.requestedLevel.setSolution("NONE");
                        }
                        this.requestedLevel.setID(xpp.getAttributeIntValue(null, "id", -1));
                        this.gameplayScene.getGameCamera().setInitialYrot(xpp.getAttributeIntValue(null, "initial_rotation", 0));
                        this.gameplayScene.setCurrentLevel(this.requestedLevel);
                    }
                }
            }
        } finally {
            xpp.close();
        }
    }

    private void prepareGameFieldFromFile(int levelRef) {
        try {
            getGameFieldFromXML(levelRef);
        } catch (XmlPullParserException e) {
            Log.i("Roll It", e.getMessage());
        } catch (IOException e2) {
            Log.i("Roll It", e2.getMessage());
        }
    }

    public int getLevelIDbyRef(int levelRef) {
        int levelID = 0;
        XmlResourceParser xpp = this.res.getXml(levelRef);
        try {
            xpp.next();
            for (int eventType = xpp.getEventType(); eventType != 1; eventType = xpp.next()) {
                if (eventType == 2) {
                    if (xpp.getName().equals("level")) {
                        levelID = xpp.getAttributeIntValue(null, "id", -1);
                    }
                }
            }
        } catch (XmlPullParserException e) {
            XmlPullParserException e2 = e;
            Log.e("LevelLoader", "Error parsing level xml! XmlPullParserException");
            e2.printStackTrace();
        } catch (IOException e3) {
            IOException e4 = e3;
            Log.e("LevelLoader", "Error parsing level xml! IOException");
            e4.printStackTrace();
        } finally {
            xpp.close();
        }
        if (levelID == -1) {
            DebugLog.v("LevelLoader", "ID tag not found");
        }
        return levelID;
    }

    public void initializeLevel(Level level) {
        boolean parseComplete = false;
        XmlResourceParser xpp = this.res.getXml(level.getXmlRef());
        try {
            xpp.next();
            for (int eventType = xpp.getEventType(); eventType != 1 && !parseComplete; eventType = xpp.next()) {
                if (eventType == 2) {
                    if (xpp.getName().equals("level")) {
                        level.setMaxSteps(xpp.getAttributeIntValue(null, "max_steps", -1));
                        level.setMinSteps(xpp.getAttributeIntValue(null, "min_steps", -1));
                        level.setDifficulty(xpp.getAttributeIntValue(null, "difficulty", -1));
                        level.setID(xpp.getAttributeIntValue(null, "id", -1));
                        parseComplete = true;
                    }
                }
            }
        } catch (XmlPullParserException e) {
            XmlPullParserException e2 = e;
            Log.e("LevelLoader", "Error parsing level xml! XmlPullParserException");
            e2.printStackTrace();
        } catch (IOException e3) {
            IOException e4 = e3;
            Log.e("LevelLoader", "Error parsing level xml! IOException");
            e4.printStackTrace();
        } finally {
            xpp.close();
        }
    }
}
