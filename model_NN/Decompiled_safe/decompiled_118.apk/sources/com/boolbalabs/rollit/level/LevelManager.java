package com.boolbalabs.rollit.level;

import android.content.SharedPreferences;
import android.util.Log;
import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.RollItGame;
import com.boolbalabs.rollit.SceneChooseLevel;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import java.util.HashMap;
import java.util.HashSet;

public class LevelManager {
    private static Level[] currentLevelPack;
    private static RollItGame game;
    private static LevelManager instance;
    private static HashSet<Integer> levelIDs = new HashSet<>();
    private static final Level[] levelPackGrass = {new Level(R.xml.pack_01_level_01), new Level(R.xml.pack_01_level_02), new Level(R.xml.pack_01_level_03), new Level(R.xml.pack_01_level_04), new Level(R.xml.pack_01_level_05), new Level(R.xml.pack_01_level_06), new Level(R.xml.pack_01_level_07), new Level(R.xml.pack_01_level_08), new Level(R.xml.pack_01_level_09), new Level(R.xml.pack_01_level_10), new Level(R.xml.pack_01_level_11), new Level(R.xml.pack_01_level_12), new Level(R.xml.pack_01_level_13), new Level(R.xml.pack_01_level_14), new Level(R.xml.pack_01_level_15), new Level(R.xml.pack_01_level_16), new Level(R.xml.pack_01_level_17), new Level(R.xml.pack_01_level_18), new Level(R.xml.pack_01_level_19), new Level(R.xml.pack_01_level_20), new Level(R.xml.pack_01_level_21), new Level(R.xml.pack_01_level_22), new Level(R.xml.pack_01_level_23), new Level(R.xml.pack_01_level_24), new Level(R.xml.pack_01_level_25), new Level(R.xml.pack_01_level_26), new Level(R.xml.pack_01_level_27), new Level(R.xml.pack_01_level_28), new Level(R.xml.pack_01_level_29), new Level(R.xml.pack_01_level_30), new Level(R.xml.pack_01_level_31), new Level(R.xml.pack_01_level_32), new Level(R.xml.pack_01_level_33), new Level(R.xml.pack_01_level_34), new Level(R.xml.pack_01_level_35), new Level(R.xml.pack_01_level_36), new Level(R.xml.pack_01_level_37), new Level(R.xml.pack_01_level_38), new Level(R.xml.pack_01_level_39), new Level(R.xml.pack_01_level_40), new Level(R.xml.pack_01_level_41), new Level(R.xml.pack_01_level_42), new Level(R.xml.pack_01_level_43), new Level(R.xml.pack_01_level_44), new Level(R.xml.pack_01_level_45), new Level(R.xml.pack_01_level_46), new Level(R.xml.pack_01_level_47), new Level(R.xml.pack_01_level_48), new Level(R.xml.pack_01_level_49), new Level(R.xml.pack_01_level_50), new Level(R.xml.pack_01_level_51), new Level(R.xml.pack_01_level_52), new Level(R.xml.pack_01_level_53), new Level(R.xml.pack_01_level_54), new Level(R.xml.pack_01_level_55), new Level(R.xml.pack_01_level_56), new Level(R.xml.pack_01_level_57), new Level(R.xml.pack_01_level_58), new Level(R.xml.pack_01_level_59), new Level(R.xml.pack_01_level_60)};
    private static int[] levelPackIDKeySet = {RollItConstants.LEVEL_PACK_TEST.intValue(), RollItConstants.LEVEL_PACK_GRASS.intValue(), RollItConstants.LEVEL_PACK_ICE.intValue()};
    private static final Level[] levelPackIce = {new Level(R.xml.pack_02_level_01), new Level(R.xml.pack_02_level_02), new Level(R.xml.pack_02_level_03), new Level(R.xml.pack_02_level_04), new Level(R.xml.pack_02_level_05), new Level(R.xml.pack_02_level_06), new Level(R.xml.pack_02_level_07), new Level(R.xml.pack_02_level_08), new Level(R.xml.pack_02_level_09), new Level(R.xml.pack_02_level_10), new Level(R.xml.pack_02_level_11), new Level(R.xml.pack_02_level_12), new Level(R.xml.pack_02_level_13), new Level(R.xml.pack_02_level_14), new Level(R.xml.pack_02_level_15), new Level(R.xml.pack_02_level_16), new Level(R.xml.pack_02_level_17), new Level(R.xml.pack_02_level_18), new Level(R.xml.pack_02_level_19)};
    private static final Level[] levelPackTest = {new Level(R.xml.pack_00_level_01), new Level(R.xml.pack_00_level_02)};
    private static HashMap<Integer, Level[]> levelPacks = new HashMap<>();

    private LevelManager() {
        levelPacks.put(Integer.valueOf(levelPackIDKeySet[0]), levelPackTest);
        levelPacks.put(Integer.valueOf(levelPackIDKeySet[1]), levelPackGrass);
        levelPacks.put(Integer.valueOf(levelPackIDKeySet[2]), levelPackIce);
        currentLevelPack = levelPacks.get(Integer.valueOf(Settings.currentLevelPackID));
        if (currentLevelPack == null) {
            Log.e("Game's LevelManager", "Level pack with id: " + Settings.currentLevelPackID + " not found!" + " Default levelPack loaded");
            currentLevelPack = levelPackGrass;
            Settings.currentLevelPackID = RollItConstants.LEVEL_PACK_GRASS.intValue();
        }
    }

    public static void initialize() {
        if (instance == null) {
            instance = new LevelManager();
            Settings.currentLevelPackSize = currentLevelPack.length;
            Settings.getInstance().saveSharedPreferences();
        }
    }

    public void initWithGame(RollItGame rollItGame) {
        game = rollItGame;
        initializeLevels();
    }

    public static LevelManager getInstance() {
        return instance;
    }

    private static void initializeLevels() {
        for (int valueOf : levelPackIDKeySet) {
            Level[] levelPack = levelPacks.get(Integer.valueOf(valueOf));
            int numberOfLevelsInPack = levelPack.length;
            for (int j = 0; j < numberOfLevelsInPack; j++) {
                levelPack[j].setLevelNumber(j + 1);
                LevelLoader.getInstance().initializeLevel(levelPack[j]);
                levelIDs.add(Integer.valueOf(levelPack[j].getID()));
            }
        }
    }

    public Level getLevel(int levelNumber) {
        if (levelNumber >= 0 && levelNumber < currentLevelPack.length) {
            return currentLevelPack[levelNumber];
        }
        throw new IndexOutOfBoundsException("Current level pack doesn't have level " + (levelNumber + 1));
    }

    public Level[] getCurrentLevelPack() {
        return currentLevelPack;
    }

    public int getLevelPackSize(int levelPackID) {
        return levelPacks.get(Integer.valueOf(levelPackID)).length;
    }

    public Level[] getLevelPack(int levelPackID) {
        return levelPacks.get(Integer.valueOf(levelPackID));
    }

    public void setCurrentLevelPack(int levelPackID) {
        currentLevelPack = levelPacks.get(Integer.valueOf(Settings.currentLevelPackID));
        Settings.currentLevelPackSize = currentLevelPack.length;
    }

    public void unlockCurrentLevel() {
        ((SceneChooseLevel) game.getGameScene(3)).unlockCurrentLevel();
    }

    public GameScene getGameplayScene() {
        return game.getGameScene(1);
    }

    public static void resetLevels() {
        for (int valueOf : levelPackIDKeySet) {
            for (Level resetLevelStatistics : levelPacks.get(Integer.valueOf(valueOf))) {
                resetLevelStatistics.resetLevelStatistics();
            }
        }
        ((SceneChooseLevel) game.getGameScene(3)).performAction(RollItConstants.ACTION_RESET_LEVELS, null);
    }

    public void assignScoresToAllCompletedLevels() {
        SharedPreferences prefs = Settings.getPreferences();
        for (int valueOf : levelPackIDKeySet) {
            Level[] levelPack = levelPacks.get(Integer.valueOf(valueOf));
            int numberOfLevelsInPack = levelPack.length;
            int j = 0;
            while (j < numberOfLevelsInPack && !assignScoreToLevelIfNesessary(levelPack[j], prefs)) {
                j++;
            }
        }
        Settings.getInstance().saveSharedPreferences();
    }

    private boolean assignScoreToLevelIfNesessary(Level level, SharedPreferences prefs) {
        int levelID = level.getID();
        int bestStepNumber = prefs.getInt(Level.BEST_STEPS_TAG + String.valueOf(levelID), 0);
        int bestScore = prefs.getInt(Level.BEST_SCORE_TAG + String.valueOf(levelID), 0);
        if (bestStepNumber != 0 && bestScore == 0) {
            int currentScore = level.calculateScore(bestStepNumber, 3600000);
            level.saveResultsToPreferences(bestStepNumber, currentScore);
            Settings.totalScore += currentScore;
        }
        return bestStepNumber == 0;
    }
}
