package com.boolbalabs.rollit.menucomponents;

import android.graphics.Rect;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;

public class MenuBackgroundView extends ZNode {
    private Rect cropRectOnTexture;
    private Rect rectOnScreen = new Rect(0, 0, ScreenMetrics.screenWidthRip, ScreenMetrics.screenHeightRip);

    public MenuBackgroundView(int resourceId) {
        super(resourceId, 0);
        chooseBackgroundCropRect();
        setZGL(1.0f);
    }

    public void initialize() {
        super.initialize();
        initWithFrame(this.rectOnScreen, this.cropRectOnTexture);
    }

    private void chooseBackgroundCropRect() {
        if (ScreenMetrics.screenHeightPix <= 480) {
            this.cropRectOnTexture = new Rect(0, 0, ScreenMetrics.screenLargeSidePix, ScreenMetrics.screenSmallSidePix);
        } else {
            this.cropRectOnTexture = new Rect(0, 0, (int) (480.0f * ScreenMetrics.aspectRatioOriented), 480);
        }
    }
}
