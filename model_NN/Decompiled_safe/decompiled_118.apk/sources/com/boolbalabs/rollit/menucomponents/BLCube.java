package com.boolbalabs.rollit.menucomponents;

import android.graphics.Point;
import android.graphics.Rect;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.util.Log;
import android.view.animation.Interpolator;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.transitions.BackInterpolator;
import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.lib.transitions.QuadInterpolator;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.rollit.R;
import com.boolbalabs.utility.ExtendedMatrix;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class BLCube extends ZNode {
    private static final String BACK = "blcube_back.png";
    private static final String BOTTOM = "blcube_back.png";
    private static final String FRONT = "blcube_back.png";
    private static final String FRONT_BL = "blcube_front_BL.png";
    private static final String LEFT = "blcube_back.png";
    private static final String RIGHT = "blcube_back.png";
    private static final String TOP = "blcube_back.png";
    private static final int cubeTextureRef = 2130837504;
    private long animationLength;
    private boolean animationRunning = false;
    private Interpolator backInInterpolator;
    private Interpolator backInterpolator;
    private final float[] box = {-this.w, this.h, this.d, this.w, this.h, this.d, -this.w, this.h, -this.d, this.w, this.h, -this.d, -this.w, -this.h, this.d, -this.w, -this.h, -this.d, this.w, -this.h, this.d, this.w, -this.h, -this.d, this.w, -this.h, this.d, this.w, this.h, this.d, -this.w, -this.h, this.d, -this.w, this.h, this.d, -this.w, -this.h, -this.d, -this.w, this.h, -this.d, this.w, -this.h, -this.d, this.w, this.h, -this.d, -this.w, -this.h, this.d, -this.w, this.h, this.d, -this.w, -this.h, -this.d, -this.w, this.h, -this.d, this.w, -this.h, -this.d, this.w, this.h, -this.d, this.w, -this.h, this.d, this.w, this.h, this.d};
    private FloatBuffer cubeBuff;
    private long currentAnimationTime;
    private final float[] currentCameraMatrix = new float[16];
    private float[] currentMatrix = new float[16];
    private float d = 0.48f;
    private float[] defaultPosition = new float[16];
    private boolean frontFaceSwitched = false;
    private float h = 0.48f;
    private long pauseDuration = 500;
    private boolean pauseMode = false;
    private float progress;
    private Interpolator quadInterpolator;
    private long startAnimationTime;
    private long startPauseTime;
    private float[] stopTime = {0.1f, 0.3f, 0.35f, 0.75f, 0.8f, 1.0f};
    private final float[] tempMatrix = new float[16];
    private FloatBuffer texBuff;
    private float[] texCoords;
    private float textureHeight;
    private float textureWidth;
    private float totalMovement = -4.0f;
    private float totalRotation = 720.0f;
    private float w = 0.48f;
    private float xmove;
    private float xrot;
    private float ymove;
    private float yrot;
    private float zmove;
    private float zrot;

    public BLCube() {
        super(R.drawable.boolba_logo_texture, 1);
        this.userInteractionEnabled = true;
        this.backInterpolator = new BackInterpolator(EasingType.INOUT, 1.5f);
        this.backInInterpolator = new BackInterpolator(EasingType.INOUT, 0.5f);
        this.quadInterpolator = new QuadInterpolator(EasingType.OUT);
        this.cubeBuff = BufferUtils.makeFloatBuffer(this.box);
        this.animationLength = 5000;
        this.userInteractionEnabled = true;
    }

    public void initialize() {
        Matrix.setIdentityM(this.currentCameraMatrix, 0);
        ExtendedMatrix.setLookAtM(this.currentCameraMatrix, 0, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);
        Matrix.setIdentityM(this.defaultPosition, 0);
        Matrix.translateM(this.defaultPosition, 0, 0.0f, 0.0f, -2.5f);
        Matrix.rotateM(this.defaultPosition, 0, -30.0f, 0.0f, 1.0f, 0.0f);
        Matrix.rotateM(this.defaultPosition, 0, 30.0f, 1.0f, 0.0f, -0.7f);
        System.arraycopy(this.defaultPosition, 0, this.currentMatrix, 0, 16);
    }

    /* access modifiers changed from: protected */
    public void createTextureBuffer() {
    }

    public void update() {
        applyCameraPosition();
        applyDefaultPosition();
        if (this.animationRunning) {
            animate();
            applyAnimation();
        } else if (this.pauseMode) {
            waitForABit();
        }
    }

    public void onAfterLoad() {
        setTextures();
        startAnimation();
    }

    /* access modifiers changed from: protected */
    public void animate() {
        this.progress = calculateProgress();
        if (this.progress < 1.0f) {
            calculateAnimation();
            return;
        }
        this.animationRunning = false;
        this.pauseMode = true;
        this.startPauseTime = SystemClock.uptimeMillis();
    }

    private void calculateAnimation() {
        if (this.progress < this.stopTime[0]) {
            return;
        }
        if (this.progress < this.stopTime[1]) {
            this.zmove = this.totalMovement * this.quadInterpolator.getInterpolation((this.progress - this.stopTime[0]) / (this.stopTime[1] - this.stopTime[0]));
            this.xmove = this.totalMovement * this.backInInterpolator.getInterpolation((this.progress - this.stopTime[0]) / (this.stopTime[1] - this.stopTime[0]));
            this.ymove = 0.5f * this.totalMovement * this.quadInterpolator.getInterpolation((this.progress - this.stopTime[0]) / (this.stopTime[1] - this.stopTime[0]));
        } else if (this.progress < this.stopTime[2]) {
        } else {
            if (this.progress < this.stopTime[3]) {
                this.yrot = this.totalRotation * this.backInterpolator.getInterpolation((this.progress - this.stopTime[2]) / (this.stopTime[3] - this.stopTime[2]));
                if (this.yrot > (this.totalRotation * 3.0f) / 4.0f && !this.frontFaceSwitched) {
                    switchFrontFace();
                    this.frontFaceSwitched = true;
                }
            } else if (this.progress >= this.stopTime[4] && this.progress < this.stopTime[5]) {
                this.zmove = this.totalMovement * this.quadInterpolator.getInterpolation((1.0f - this.progress) / (this.stopTime[5] - this.stopTime[4]));
                this.xmove = this.totalMovement * this.quadInterpolator.getInterpolation((1.0f - this.progress) / (this.stopTime[5] - this.stopTime[4]));
                this.ymove = 0.5f * this.totalMovement * this.quadInterpolator.getInterpolation((1.0f - this.progress) / (this.stopTime[5] - this.stopTime[4]));
            }
        }
    }

    private void waitForABit() {
        if (SystemClock.uptimeMillis() - this.startPauseTime > this.pauseDuration) {
            stopAnimation();
        }
    }

    private void switchFrontFace() {
        Rect frontFaceRectOnTexture = TexturesManager.getInstance().getRectByFrameName(FRONT_BL);
        float frontT = ((float) frontFaceRectOnTexture.top) / this.textureHeight;
        float frontB = ((float) frontFaceRectOnTexture.bottom) / this.textureHeight;
        float frontL = ((float) frontFaceRectOnTexture.left) / this.textureWidth;
        float frontR = ((float) frontFaceRectOnTexture.right) / this.textureWidth;
        this.texBuff.position(16);
        this.texBuff.put(new float[]{frontR, frontB, frontR, frontT, frontL, frontB, frontL, frontT});
        this.texBuff.position(0);
    }

    public void draw(GL10 gl) {
        setupCube(gl);
        gl.glLoadMatrixf(this.currentMatrix, 0);
        drawCube(gl);
        gl.glLoadIdentity();
    }

    public void startAnimation() {
        this.animationRunning = true;
        this.pauseMode = false;
        this.startAnimationTime = System.currentTimeMillis();
    }

    public void stopAnimation() {
        this.frontFaceSwitched = false;
        this.animationRunning = false;
        this.parentGameScene.askGameToSwitchGameScene(2);
    }

    private void applyCameraPosition() {
        System.arraycopy(this.currentCameraMatrix, 0, this.currentMatrix, 0, 16);
    }

    private void applyDefaultPosition() {
        System.arraycopy(this.currentMatrix, 0, this.tempMatrix, 0, 16);
        Matrix.multiplyMM(this.currentMatrix, 0, this.tempMatrix, 0, this.defaultPosition, 0);
    }

    private void applyAnimation() {
        Matrix.translateM(this.currentMatrix, 0, this.xmove, this.ymove, this.zmove);
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.xrot, 1.0f, 0.0f, 0.0f);
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.yrot, 0.0f, 1.0f, 0.0f);
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.zrot, 0.0f, 0.0f, 1.0f);
    }

    private float calculateProgress() {
        this.currentAnimationTime = System.currentTimeMillis() - this.startAnimationTime;
        return ((float) this.currentAnimationTime) / ((float) this.animationLength);
    }

    public void loadContent() {
        TexturesManager.getInstance().addTexture(new Texture2D(getResourceId(), Texture2D.TextureFilter.Linear, Texture2D.TextureFilter.Linear, Texture2D.TextureWrap.ClampToEdge, Texture2D.TextureWrap.ClampToEdge));
    }

    public void setupCube(GL10 gl) {
        gl.glBindTexture(3553, getTextureGlobalIndex());
        gl.glVertexPointer(3, 5126, 0, this.cubeBuff);
        gl.glEnableClientState(32884);
        gl.glTexCoordPointer(2, 5126, 0, this.texBuff);
        gl.glEnableClientState(32888);
    }

    public void drawCube(GL10 gl) {
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glNormal3f(0.0f, 1.0f, 0.0f);
        gl.glDrawArrays(5, 0, 4);
        gl.glNormal3f(0.0f, -1.0f, 0.0f);
        gl.glDrawArrays(5, 4, 4);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glNormal3f(0.0f, 0.0f, 1.0f);
        gl.glDrawArrays(5, 8, 4);
        gl.glNormal3f(0.0f, 0.0f, -1.0f);
        gl.glDrawArrays(5, 12, 4);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glNormal3f(-1.0f, 0.0f, 0.0f);
        gl.glDrawArrays(5, 16, 4);
        gl.glNormal3f(1.0f, 0.0f, 0.0f);
        gl.glDrawArrays(5, 20, 4);
    }

    public boolean animationRunning() {
        return this.animationRunning;
    }

    public void setTextures() {
        TexturesManager tm = TexturesManager.getInstance();
        Rect topFaceRectOnTexture = tm.getRectByFrameName("blcube_back.png");
        Rect bottomFaceRectOnTexture = tm.getRectByFrameName("blcube_back.png");
        Rect leftFaceRectOnTexture = tm.getRectByFrameName("blcube_back.png");
        Rect rightFaceRectOnTexture = tm.getRectByFrameName("blcube_back.png");
        Rect frontFaceRectOnTexture = tm.getRectByFrameName("blcube_back.png");
        Rect backFaceRectOnTexture = tm.getRectByFrameName("blcube_back.png");
        if (topFaceRectOnTexture == null) {
            topFaceRectOnTexture = new Rect(0, 0, 20, 20);
            Log.e("Texture refresh", "WRONG TEXTURE TOP RECTANGLE NAME! default Rect (0,0,20,20) is set");
        }
        Texture2D currentTexture = tm.getTextureByResourceId(getResourceId());
        if (currentTexture != null) {
            this.textureWidth = (float) currentTexture.getWidth();
            this.textureHeight = (float) currentTexture.getHeight();
        } else {
            Log.e("Texture refresh", "CURRENT TEXTURE IS NULL, DEFAULT TEXTURE SIZES ARE SET");
        }
        float topT = ((float) topFaceRectOnTexture.top) / this.textureHeight;
        float topB = ((float) topFaceRectOnTexture.bottom) / this.textureHeight;
        float topL = ((float) topFaceRectOnTexture.left) / this.textureWidth;
        float topR = ((float) topFaceRectOnTexture.right) / this.textureWidth;
        float bottomT = ((float) bottomFaceRectOnTexture.top) / this.textureHeight;
        float bottomB = ((float) bottomFaceRectOnTexture.bottom) / this.textureHeight;
        float bottomL = ((float) bottomFaceRectOnTexture.left) / this.textureWidth;
        float bottomR = ((float) bottomFaceRectOnTexture.right) / this.textureWidth;
        float leftT = ((float) leftFaceRectOnTexture.top) / this.textureHeight;
        float leftB = ((float) leftFaceRectOnTexture.bottom) / this.textureHeight;
        float leftL = ((float) leftFaceRectOnTexture.left) / this.textureWidth;
        float leftR = ((float) leftFaceRectOnTexture.right) / this.textureWidth;
        float rightT = ((float) rightFaceRectOnTexture.top) / this.textureHeight;
        float rightB = ((float) rightFaceRectOnTexture.bottom) / this.textureHeight;
        float rightL = ((float) rightFaceRectOnTexture.left) / this.textureWidth;
        float rightR = ((float) rightFaceRectOnTexture.right) / this.textureWidth;
        float frontT = ((float) frontFaceRectOnTexture.top) / this.textureHeight;
        float frontB = ((float) frontFaceRectOnTexture.bottom) / this.textureHeight;
        float frontL = ((float) frontFaceRectOnTexture.left) / this.textureWidth;
        float frontR = ((float) frontFaceRectOnTexture.right) / this.textureWidth;
        float backT = ((float) backFaceRectOnTexture.top) / this.textureHeight;
        float backB = ((float) backFaceRectOnTexture.bottom) / this.textureHeight;
        float backL = ((float) backFaceRectOnTexture.left) / this.textureWidth;
        float backR = ((float) backFaceRectOnTexture.right) / this.textureWidth;
        this.texCoords = new float[]{topL, topB, topR, topB, topL, topT, topR, topT, bottomL, bottomT, bottomL, bottomB, bottomR, bottomT, bottomR, bottomB, frontR, frontB, frontR, frontT, frontL, frontB, frontL, frontT, backR, backB, backR, backT, backL, backB, backL, backT, leftR, leftB, leftR, leftT, leftL, leftB, leftL, leftT, rightR, rightB, rightR, rightT, rightL, rightB, rightL, rightT};
        this.texBuff = BufferUtils.makeFloatBuffer(this.texCoords);
    }

    public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
        this.parentGameScene.askGameToSwitchGameScene(2);
    }

    public boolean pointInside(Point point) {
        return true;
    }
}
