package com.boolbalabs.rollit.menucomponents;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import com.boolbalabs.lib.elements.BLButton;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.level.LevelManager;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import com.scoreloop.client.android.core.controller.RequestControllerException;

public class ChooseLevelpackMenu extends ZNode {
    private BLButton backArrow;
    private Rect backRectOnScreen = new Rect(10, 265, RequestControllerException.CODE_SOCIAL_PROVIDER_DISCONNECTED, 310);
    private int chooseLevelpackMenuTextureRef = R.drawable.rc_menu_texture;
    private BLButton easyPackButton;
    private Rect easyPackButtonRectOnScreen = new Rect((ScreenMetrics.screenWidthRip / 2) - 100, 80, (ScreenMetrics.screenWidthRip / 2) + 100, 160);
    /* access modifiers changed from: private */
    public Bundle levelPackBundle = new Bundle();
    private MenuBackgroundView menuBackground;
    private BLButton normalPackButton;
    private Rect normalPackButtonRectOnScreen = new Rect((ScreenMetrics.screenWidthRip / 2) - 100, 180, (ScreenMetrics.screenWidthRip / 2) + 100, 260);

    public ChooseLevelpackMenu() {
        super(-1, 0);
        this.userInteractionEnabled = true;
        createBackground();
        createButtons();
    }

    public void initialize() {
        initBackground();
        initButtons();
    }

    private void createBackground() {
        this.menuBackground = new MenuBackgroundView(R.drawable.rc_menu_bg_texture);
        addChild(this.menuBackground);
    }

    private void initBackground() {
        this.menuBackground.initialize();
    }

    private void createButtons() {
        this.easyPackButton = new BLButton(this.chooseLevelpackMenuTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                ChooseLevelpackMenu.this.setupBundle(ChooseLevelpackMenu.this.levelPackBundle, RollItConstants.LEVEL_PACK_GRASS);
                ChooseLevelpackMenu.this.setupChooseLevelSceneForCurrentLevelpack(RollItConstants.LEVEL_PACK_GRASS);
                performAction(RollItConstants.ACTION_PLAY_SELECTED_LEVELPACK, ChooseLevelpackMenu.this.levelPackBundle);
            }
        };
        this.easyPackButton.touchUpSound = R.raw.click_sound;
        addChild(this.easyPackButton);
        this.normalPackButton = new BLButton(this.chooseLevelpackMenuTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                ChooseLevelpackMenu.this.setupBundle(ChooseLevelpackMenu.this.levelPackBundle, RollItConstants.LEVEL_PACK_ICE);
                ChooseLevelpackMenu.this.setupChooseLevelSceneForCurrentLevelpack(RollItConstants.LEVEL_PACK_ICE);
                performAction(RollItConstants.ACTION_PLAY_SELECTED_LEVELPACK, ChooseLevelpackMenu.this.levelPackBundle);
            }
        };
        this.normalPackButton.touchUpSound = R.raw.click_sound;
        addChild(this.normalPackButton);
        this.backArrow = new BLButton(this.chooseLevelpackMenuTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                performAction(RollItConstants.ACTION_BACK, null);
            }
        };
        this.backArrow.touchUpSound = R.raw.click_sound;
        addChild(this.backArrow);
    }

    private void initButtons() {
        TexturesManager tm = TexturesManager.getInstance();
        Rect easyPackButtonCropRectOnTexture = tm.getRectByFrameName("menu_button_play.png");
        Rect easyPackButtonCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_play_pressed.png");
        this.easyPackButton.setRects(this.easyPackButtonRectOnScreen, easyPackButtonCropRectOnTexture, easyPackButtonCropRectOnTexture_pressed, easyPackButtonCropRectOnTexture_pressed);
        this.easyPackButton.initialize();
        Rect normalPackButtonCropRectOnTexture = tm.getRectByFrameName("menu_button_play.png");
        Rect normalPackButtonCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_play_pressed.png");
        this.normalPackButton.setRects(this.normalPackButtonRectOnScreen, normalPackButtonCropRectOnTexture, normalPackButtonCropRectOnTexture_pressed, normalPackButtonCropRectOnTexture_pressed);
        this.normalPackButton.initialize();
        Rect backCropRectOnTexture = tm.getRectByFrameName("menu_button_back.png");
        Rect backCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_back_pressed.png");
        this.backArrow.setRects(this.backRectOnScreen, backCropRectOnTexture, backCropRectOnTexture_pressed, backCropRectOnTexture_pressed);
        this.backArrow.initialize();
    }

    /* access modifiers changed from: private */
    public void setupBundle(Bundle levelPackBundle2, Integer levelPackID) {
        levelPackBundle2.putInt(RollItConstants.KEY_LEVEL_PACK_ID, levelPackID.intValue());
    }

    /* access modifiers changed from: private */
    public void setupChooseLevelSceneForCurrentLevelpack(Integer levelPackID) {
        Settings.currentLevelPackID = levelPackID.intValue();
        LevelManager.getInstance().setCurrentLevelPack(levelPackID.intValue());
    }

    public void update() {
    }
}
