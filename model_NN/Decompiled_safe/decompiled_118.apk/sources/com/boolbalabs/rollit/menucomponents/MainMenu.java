package com.boolbalabs.rollit.menucomponents;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.elements.BLButton;
import com.boolbalabs.lib.elements.BLCheckButton;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.transitions.TransitionMovement;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;

public class MainMenu extends ZNode {
    private static final int SMALL_BTN_TRANSITION_LENGTH = 320;
    private BLButton buttonHelp;
    private BLButton buttonHighscore;
    private BLButton buttonPlay;
    private Rect buttonSliderRectOnScreen = new Rect((ScreenMetrics.screenLargeSideRip - 5) - 64, 251, ScreenMetrics.screenLargeSideRip - 5, 315);
    private ZNode gameLabel;
    private Rect gameLabelRectOnScreen = new Rect((ScreenMetrics.screenWidthRip / 2) - 123, 15, (ScreenMetrics.screenWidthRip / 2) + Settings.GAME_FREE, 95);
    private Rect helpRectOnScreen = new Rect(5, 251, 69, 315);
    private Rect highscoreRectOnScreen = new Rect(5, 5, 69, 69);
    private MenuBackgroundView menuBackground;
    private int menuTextureRef = R.drawable.rc_menu_texture;
    private Rect musicRectOnScreen = new Rect(((ScreenMetrics.screenLargeSideRip - 5) - 32) - 20, 436, ((ScreenMetrics.screenLargeSideRip - 5) - 32) + 20, 476);
    private Rect playRectOnScreen = new Rect((ScreenMetrics.screenWidthRip / 2) - 122, 120, (ScreenMetrics.screenWidthRip / 2) + 123, 216);
    /* access modifiers changed from: private */
    public BLCheckButton settingsButton;
    /* access modifiers changed from: private */
    public BLCheckButton settingsMusicButton;
    /* access modifiers changed from: private */
    public BLCheckButton settingsSliderBackground;
    private Rect settingsSliderRectOnScreen = new Rect((ScreenMetrics.screenLargeSideRip - 5) - 64, 346, ScreenMetrics.screenLargeSideRip - 5, 566);
    /* access modifiers changed from: private */
    public BLCheckButton settingsSoundButton;
    /* access modifiers changed from: private */
    public BLCheckButton settingsVibrateButton;
    private Rect soundRectOnScreen = new Rect(((ScreenMetrics.screenLargeSideRip - 5) - 32) - 20, 376, ((ScreenMetrics.screenLargeSideRip - 5) - 32) + 20, 416);
    TransitionMovement transitionDown;
    TransitionMovement transitionUp;
    private Rect vibrateRectOnScreen = new Rect(((ScreenMetrics.screenLargeSideRip - 5) - 32) - 20, 496, ((ScreenMetrics.screenLargeSideRip - 5) - 32) + 20, 536);

    public MainMenu() {
        super(-1, 0);
        this.userInteractionEnabled = true;
        createBackground();
        createGameLabel();
        createButtons();
        createTransitions();
    }

    public void initialize() {
        initButtons();
        initGameLabel();
        this.visible = false;
        super.initialize();
        setupSelectableButtons();
    }

    private void createBackground() {
        this.menuBackground = new MenuBackgroundView(R.drawable.rc_menu_bg_texture);
        addChild(this.menuBackground);
    }

    private void createGameLabel() {
        this.gameLabel = new ZNode(this.menuTextureRef, 0);
        addChild(this.gameLabel);
    }

    private void createButtons() {
        this.buttonPlay = new BLButton(this.menuTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                DebugLog.i("buttonPlay", "buttonPlay touchClickAction " + touchDownPoint.toString());
                performAction(RollItConstants.ACTION_PLAY, null);
            }
        };
        this.buttonPlay.touchUpSound = R.raw.click_sound;
        addChild(this.buttonPlay);
        this.buttonHighscore = new BLButton(this.menuTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                DebugLog.i("buttonScore", "buttonHighscore touchClickAction " + touchDownPoint.toString());
                this.parentGameScene.sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_HIGH_SCORES);
            }
        };
        this.buttonHighscore.touchUpSound = R.raw.click_sound;
        addChild(this.buttonHighscore);
        this.buttonHelp = new BLButton(this.menuTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                this.parentGameScene.sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_DIALOG_HELP);
            }
        };
        this.buttonHelp.touchUpSound = R.raw.click_sound;
        addChild(this.buttonHelp);
        this.settingsSliderBackground = new BLCheckButton(this.menuTextureRef);
        addChild(this.settingsSliderBackground);
        this.settingsSoundButton = new BLCheckButton(this.menuTextureRef) {
            public void onSelectionChange(boolean selected) {
                ZageCommonSettings.soundEnabled = !selected;
            }
        };
        this.settingsSoundButton.touchUpSound = R.raw.click_sound;
        addChild(this.settingsSoundButton);
        this.settingsMusicButton = new BLCheckButton(this.menuTextureRef) {
            public void onSelectionChange(boolean selected) {
                ZageCommonSettings.musicEnabled = !selected;
                SoundManager sm = SoundManager.getInstance();
                if (sm != null && this.parentGameScene.isSceneInitialized()) {
                    if (ZageCommonSettings.musicEnabled) {
                        sm.playLoopingSound(R.raw.main_theme);
                    } else {
                        sm.pauseLoopingSounds();
                    }
                }
            }
        };
        this.settingsMusicButton.touchUpSound = R.raw.click_sound;
        addChild(this.settingsMusicButton);
        this.settingsVibrateButton = new BLCheckButton(this.menuTextureRef) {
            public void onSelectionChange(boolean selected) {
                ZageCommonSettings.vibroEnabled = !selected;
            }
        };
        this.settingsVibrateButton.touchUpSound = R.raw.click_sound;
        addChild(this.settingsVibrateButton);
        this.settingsButton = new BLCheckButton(this.menuTextureRef) {
            public void onSelectionChange(boolean selected) {
                if (!selected) {
                    MainMenu.this.settingsSliderBackground.setTransition(MainMenu.this.transitionDown);
                    MainMenu.this.settingsMusicButton.setTransition(MainMenu.this.transitionDown.createCopy(MainMenu.this.settingsMusicButton));
                    MainMenu.this.settingsSoundButton.setTransition(MainMenu.this.transitionDown.createCopy(MainMenu.this.settingsSoundButton));
                    MainMenu.this.settingsVibrateButton.setTransition(MainMenu.this.transitionDown.createCopy(MainMenu.this.settingsVibrateButton));
                } else {
                    MainMenu.this.settingsSliderBackground.setTransition(MainMenu.this.transitionUp);
                    MainMenu.this.settingsMusicButton.setTransition(MainMenu.this.transitionUp.createCopy(MainMenu.this.settingsMusicButton));
                    MainMenu.this.settingsSoundButton.setTransition(MainMenu.this.transitionUp.createCopy(MainMenu.this.settingsSoundButton));
                    MainMenu.this.settingsVibrateButton.setTransition(MainMenu.this.transitionUp.createCopy(MainMenu.this.settingsVibrateButton));
                }
                MainMenu.this.settingsSliderBackground.startTransition();
                MainMenu.this.settingsMusicButton.startTransition();
                MainMenu.this.settingsSoundButton.startTransition();
                MainMenu.this.settingsVibrateButton.startTransition();
                MainMenu.this.settingsButton.startTransition();
            }
        };
        this.settingsButton.touchUpSound = R.raw.click_sound;
        addChild(this.settingsButton);
    }

    private void createTransitions() {
        TransitionMovement transitionMovement = new TransitionMovement(this);
        transitionMovement.initialize(0.0f, 0.0f, -240.0f, 0.0f, 1.0f, 600, false);
        setTransition(transitionMovement);
        this.buttonPlay.setTransition(transitionMovement.createCopy(this.buttonPlay));
        this.transitionUp = new SliderTransition(this.settingsSliderBackground);
        this.transitionUp.initialize(0.0f, 0.0f, 0.0f, -320.0f, 1.0f, 400, true);
        this.transitionDown = new SliderTransition(this.settingsSliderBackground);
        this.transitionDown.initialize(0.0f, 0.0f, 0.0f, 320.0f, 1.0f, 400, true);
        SliderTransition fakeTransition = new SliderTransition(this.settingsButton);
        fakeTransition.initialize(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 400, false);
        this.settingsButton.setTransition(fakeTransition);
    }

    private void initButtons() {
        TexturesManager tm = TexturesManager.getInstance();
        Rect playCropRectOnTexture = tm.getRectByFrameName("menu_button_play.png");
        Rect playCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_play_pressed.png");
        this.buttonPlay.setRects(this.playRectOnScreen, playCropRectOnTexture, playCropRectOnTexture_pressed, playCropRectOnTexture_pressed);
        Rect highscoreCropRectOnTexture = tm.getRectByFrameName("menu_button_highscore.png");
        Rect highscoreCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_highscore_pressed.png");
        this.buttonHighscore.setRects(this.highscoreRectOnScreen, highscoreCropRectOnTexture, highscoreCropRectOnTexture_pressed, highscoreCropRectOnTexture_pressed);
        Rect sliderCropRectOnTexture = tm.getRectByFrameName("menu_slider_settings.png");
        Rect sliderCropRectOnTexture_selected = tm.getRectByFrameName("menu_slider_settings.png");
        this.settingsSliderBackground.userInteractionEnabled = false;
        this.settingsSliderBackground.setRects(this.settingsSliderRectOnScreen, sliderCropRectOnTexture, sliderCropRectOnTexture_selected);
        Rect soundCropRectOnTexture = tm.getRectByFrameName("menu_button_sound_on.png");
        Rect soundCropRectOnTexture_selected = tm.getRectByFrameName("menu_button_sound_off.png");
        this.settingsSoundButton.setRects(this.soundRectOnScreen, soundCropRectOnTexture, soundCropRectOnTexture_selected);
        Rect musicCropRectOnTexture = tm.getRectByFrameName("menu_button_music_on.png");
        Rect musicCropRectOnTexture_selected = tm.getRectByFrameName("menu_button_music_off.png");
        this.settingsMusicButton.setRects(this.musicRectOnScreen, musicCropRectOnTexture, musicCropRectOnTexture_selected);
        Rect vibrateCropRectOnTexture = tm.getRectByFrameName("menu_button_vibro_on.png");
        Rect vibrateCropRectOnTexture_selected = tm.getRectByFrameName("menu_button_vibro_off.png");
        this.settingsVibrateButton.setRects(this.vibrateRectOnScreen, vibrateCropRectOnTexture, vibrateCropRectOnTexture_selected);
        Rect helpCropRectOnTexture = tm.getRectByFrameName("menu_button_help.png");
        Rect helpCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_help_pressed.png");
        this.buttonHelp.setRects(this.helpRectOnScreen, helpCropRectOnTexture, helpCropRectOnTexture_pressed, helpCropRectOnTexture_pressed);
        Rect buttonSettingsCropRectOnTexture = tm.getRectByFrameName("menu_button_settings.png");
        Rect buttonSettingsCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_settings_pressed.png");
        this.settingsButton.setRects(this.buttonSliderRectOnScreen, buttonSettingsCropRectOnTexture, buttonSettingsCropRectOnTexture_pressed);
    }

    private void initGameLabel() {
        this.gameLabel.initWithFrame(this.gameLabelRectOnScreen, TexturesManager.getInstance().getRectByFrameName("menu_label_game_title.png"));
    }

    public void update() {
    }

    public void onShow() {
        this.visible = true;
        if (!Settings.deviceIsVerySlow) {
            startTransition();
        }
    }

    private void setupSelectableButtons() {
        boolean z;
        boolean z2;
        boolean z3;
        BLCheckButton bLCheckButton = this.settingsSoundButton;
        if (ZageCommonSettings.soundEnabled) {
            z = false;
        } else {
            z = true;
        }
        bLCheckButton.setSelected(z);
        BLCheckButton bLCheckButton2 = this.settingsMusicButton;
        if (ZageCommonSettings.musicEnabled) {
            z2 = false;
        } else {
            z2 = true;
        }
        bLCheckButton2.setSelected(z2);
        BLCheckButton bLCheckButton3 = this.settingsVibrateButton;
        if (ZageCommonSettings.vibroEnabled) {
            z3 = false;
        } else {
            z3 = true;
        }
        bLCheckButton3.setSelected(z3);
    }

    public void startTransition() {
        this.buttonPlay.startTransition();
    }

    public void onHide() {
        hideSlider();
    }

    private void hideSlider() {
        if (this.settingsButton.isSelected()) {
            this.settingsButton.setSelected(false);
            this.settingsSliderBackground.setFrameInRip(this.settingsSliderRectOnScreen);
            this.settingsMusicButton.setFrameInRip(this.musicRectOnScreen);
            this.settingsSoundButton.setFrameInRip(this.soundRectOnScreen);
            this.settingsVibrateButton.setFrameInRip(this.vibrateRectOnScreen);
        }
    }
}
