package com.boolbalabs.rollit.menucomponents;

import com.boolbalabs.lib.game.ZDrawable;
import com.boolbalabs.lib.transitions.TransitionMovement;

public abstract class ButtonTransition extends TransitionMovement {
    protected int direction;

    /* access modifiers changed from: protected */
    public abstract void performMovementStopAction();

    /* access modifiers changed from: protected */
    public abstract void updateOriginalPosition();

    public ButtonTransition(ZDrawable view) {
        super(view);
    }

    public void setDirection(int direction2) {
        this.direction = direction2;
    }

    public void onTransitionStart() {
        super.onTransitionStart();
    }

    public void onTransitionStop() {
        applyNewPosition();
        performMovementStopAction();
        reset();
    }

    private void applyNewPosition() {
        updateOriginalPosition();
        this.parentView.setPositionInRip(this.originalPosition);
    }
}
