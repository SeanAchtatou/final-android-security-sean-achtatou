package com.boolbalabs.rollit.menucomponents;

import android.graphics.Point;
import com.boolbalabs.lib.elements.BLButton;
import com.boolbalabs.lib.game.ZDrawable;
import com.boolbalabs.lib.graphics.NumberView2D;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.level.LevelLoader;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;

public class LevelButton extends BLButton {
    String[] digitsFramesNames = {"menu_digit_0.png", "menu_digit_1.png", "menu_digit_2.png", "menu_digit_3.png", "menu_digit_4.png", "menu_digit_5.png", "menu_digit_6.png", "menu_digit_7.png", "menu_digit_8.png", "menu_digit_9.png"};
    private int levelNumber;
    private ButtonTransition transition;

    public LevelButton(int levelNumber2) {
        super(R.drawable.rc_menu_texture);
        this.levelNumber = levelNumber2;
        this.transition = new LevelButtonTransition(this);
    }

    public void initialize() {
        super.initialize();
        Point fixedPointInRip = getCenterInRip();
        fixedPointInRip.y -= 6;
        fixedPointInRip.x -= 6;
        initializeNumberSubview(this.digitsFramesNames, this.digitsFramesNames, 24, fixedPointInRip, NumberView2D.ALIGN_CENTER);
        this.touchUpSound = R.raw.click_sound;
    }

    public void touchUpAction(Point touchDownPoint, Point touchUpPoint) {
        if (Settings.setCurrentLevel(this.levelNumber)) {
            LevelLoader.getInstance().askToLoadLevel(Settings.currentLevel);
        }
        super.touchUpAction(touchDownPoint, touchUpPoint);
    }

    public void touchDownAction(Point touchDownPoint) {
        if (Settings.setCurrentLevel(this.levelNumber)) {
            super.touchDownAction(touchDownPoint);
        }
    }

    public void update() {
        this.transition.innerUpdate();
    }

    public void move(int direction) {
        int sign;
        if (direction == 937245) {
            sign = -1;
        } else {
            sign = 1;
        }
        this.transition.initialize(0.0f, (float) (ChooseLevelMenu.SCREEN_WIDTH * sign), 0.0f, 0.0f, 1.0f, 500, true);
        this.transition.setDirection(direction);
        this.transition.start();
    }

    public void moveInstantly(int direction) {
        Point positionInRip = getPositionRip();
        if (direction == 937245) {
            positionInRip.x -= ChooseLevelMenu.SCREEN_WIDTH;
        } else if (direction == 984576) {
            positionInRip.x += ChooseLevelMenu.SCREEN_WIDTH;
        }
        setPositionInRip(positionInRip);
    }

    public void setPositionInRip(Point posInRip) {
        super.setPositionInRip(posInRip);
    }

    private class LevelButtonTransition extends ButtonTransition {
        public LevelButtonTransition(ZDrawable view) {
            super(view);
        }

        /* access modifiers changed from: protected */
        public void updateOriginalPosition() {
            if (this.direction == 984576) {
                this.originalPosition.x += ChooseLevelMenu.SCREEN_WIDTH;
            } else if (this.direction == 937245) {
                this.originalPosition.x -= ChooseLevelMenu.SCREEN_WIDTH;
            }
        }

        /* access modifiers changed from: protected */
        public void performMovementStopAction() {
            LevelButton.this.performAction(RollItConstants.ACTION_APPLY_BUTTONS_MOVEMENT_FINISH, null);
        }
    }
}
