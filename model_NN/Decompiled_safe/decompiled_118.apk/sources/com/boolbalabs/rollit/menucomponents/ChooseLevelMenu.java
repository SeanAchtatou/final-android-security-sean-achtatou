package com.boolbalabs.rollit.menucomponents;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.elements.BLButton;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.level.Level;
import com.boolbalabs.rollit.level.LevelManager;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import com.scoreloop.client.android.core.controller.RequestControllerException;
import java.util.ArrayList;

public class ChooseLevelMenu extends ZNode {
    private static final int HORIZONTAL_BORDER = ((int) (0.15f * ((float) SCREEN_WIDTH)));
    private static final int HORIZONTAL_OFFSET = 67;
    public static final int LEFT = 984576;
    private static final int LEVEL_BUTTON_SIZE = 60;
    private static final int NUMBER_OF_LEVELS_ON_SCREEN = 15;
    public static final int RIGHT = 937245;
    public static final int SCREEN_WIDTH = ScreenMetrics.screenLargeSideRip;
    private static final int TOTAL_LEVEL_BUTTONS_WIDTH = 328;
    private static final int VERTICAL_BORDER = 65;
    private static final int VERTICAL_OFFSET = 67;
    private static int arrowSize = LEVEL_BUTTON_SIZE;
    private BLButton backArrow;
    private Rect backRectOnScreen = new Rect(10, 265, RequestControllerException.CODE_SOCIAL_PROVIDER_DISCONNECTED, 310);
    private int chooseLevelMenuTextureRef = R.drawable.rc_menu_texture;
    private int currentButtonsNumber;
    private ArrayList<LevelButton> currentLevelButtons;
    private Level[] currentLevelPack;
    private int currentLevelScreen = 1;
    private Rect grassLevelButtonCropRectOnTexture;
    private Rect grassLevelButtonCropRectOnTexture_disabled;
    private Rect grassLevelButtonCropRectOnTexture_pressed;
    private ArrayList<LevelButton> grassLevelButtons = new ArrayList<>();
    private BLButton leftArrow;
    private Rect leftArrowRectOnScreen;
    private ZNode levelLabel;
    private Rect levelLabelRectOnScreen = new Rect((SCREEN_WIDTH / 2) - 72, 10, (SCREEN_WIDTH / 2) + 72, LEVEL_BUTTON_SIZE);
    private MenuBackgroundView menuBackground;
    private int numberOfLevelScreens = 1;
    private BLButton rightArrow;
    private Rect rightArrowRectOnScreen;

    public ChooseLevelMenu() {
        super(-1, 0);
        this.userInteractionEnabled = true;
        detectCurrentLevelPack();
        chooseArrowsSize();
        setArrowsScreenRects();
        createBackground();
        createLevelLabel();
        createDefaultButtons();
        createGrassLevelButtons();
    }

    private void setArrowsScreenRects() {
        this.rightArrowRectOnScreen = new Rect((SCREEN_WIDTH - (((SCREEN_WIDTH / 2) - 164) / 2)) - (arrowSize / 2), 160 - (arrowSize / 2), (SCREEN_WIDTH - (((SCREEN_WIDTH / 2) - 164) / 2)) + (arrowSize / 2), (arrowSize / 2) + 160);
        this.leftArrowRectOnScreen = new Rect((((SCREEN_WIDTH / 2) - 164) / 2) - (arrowSize / 2), 160 - (arrowSize / 2), (((SCREEN_WIDTH / 2) - 164) / 2) + (arrowSize / 2), (arrowSize / 2) + 160);
    }

    private void chooseArrowsSize() {
        if (ScreenMetrics.resolution == ScreenMetrics.RESOLUTION_LOW) {
            arrowSize = 45;
        } else {
            arrowSize = LEVEL_BUTTON_SIZE;
        }
    }

    public void initialize() {
        initBackground();
        initLevelLabel();
        initDefaultButtons();
        initGrassLevelButtons();
    }

    private void detectCurrentLevelPack() {
        this.currentLevelPack = LevelManager.getInstance().getCurrentLevelPack();
        this.currentButtonsNumber = Settings.currentLevelPackSize;
        this.numberOfLevelScreens = ((this.currentButtonsNumber - 1) / 15) + 1;
    }

    private void createBackground() {
        this.menuBackground = new MenuBackgroundView(R.drawable.rc_menu_bg_texture);
        addChild(this.menuBackground);
    }

    private void createLevelLabel() {
        this.levelLabel = new ZNode(this.chooseLevelMenuTextureRef, 0);
        addChild(this.levelLabel);
    }

    private void createDefaultButtons() {
        this.rightArrow = new BLButton(this.chooseLevelMenuTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                performAction(RollItConstants.ACTION_SCROLL_LEVELS_RIGHT, null);
            }
        };
        this.rightArrow.touchUpSound = R.raw.click_sound;
        addChild(this.rightArrow);
        this.leftArrow = new BLButton(this.chooseLevelMenuTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                performAction(RollItConstants.ACTION_SCROLL_LEVELS_LEFT, null);
            }
        };
        this.leftArrow.touchUpSound = R.raw.click_sound;
        addChild(this.leftArrow);
        this.backArrow = new BLButton(this.chooseLevelMenuTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                performAction(RollItConstants.ACTION_BACK, null);
            }
        };
        this.backArrow.touchUpSound = R.raw.click_sound;
        addChild(this.backArrow);
    }

    public void createGrassLevelButtons() {
        this.grassLevelButtons.clear();
        this.grassLevelButtons.ensureCapacity(this.currentButtonsNumber);
        this.grassLevelButtons.add(0, new LevelButton(0));
        int grassLevelPackSize = LevelManager.getInstance().getLevelPackSize(RollItConstants.LEVEL_PACK_GRASS.intValue());
        for (int i = 1; i <= grassLevelPackSize; i++) {
            LevelButton levelButton = new LevelButton(i);
            levelButton.register(this.parentGameScene);
            addChild(levelButton);
            this.grassLevelButtons.add(i, levelButton);
        }
        checkArrowsVisibility();
    }

    private void initBackground() {
        this.menuBackground.initialize();
    }

    private void initLevelLabel() {
        this.levelLabel.initWithFrame(this.levelLabelRectOnScreen, TexturesManager.getInstance().getRectByFrameName("menu_label_levels.png"));
    }

    private void initGrassLevelButtons() {
        this.grassLevelButtonCropRectOnTexture = TexturesManager.getInstance().getRectByFrameName("menu_button_level.png");
        this.grassLevelButtonCropRectOnTexture_pressed = TexturesManager.getInstance().getRectByFrameName("menu_button_level_pressed.png");
        this.grassLevelButtonCropRectOnTexture_disabled = TexturesManager.getInstance().getRectByFrameName("menu_button_level_disabled.png");
        Settings.currentLevelPackID = RollItConstants.LEVEL_PACK_GRASS.intValue();
        Settings.getInstance().loadSharedPreferences();
        this.currentLevelPack = LevelManager.getInstance().getLevelPack(RollItConstants.LEVEL_PACK_GRASS.intValue());
        this.currentButtonsNumber = this.currentLevelPack.length;
        for (int i = 1; i <= this.currentButtonsNumber; i++) {
            LevelButton levelButton = this.grassLevelButtons.get(i);
            setRects(levelButton, i, RollItConstants.LEVEL_PACK_GRASS.intValue());
            levelButton.initialize();
            levelButton.setNumberOnButton(i);
        }
    }

    private void initDefaultButtons() {
        TexturesManager tm = TexturesManager.getInstance();
        Rect rightArrowCropRectOnTexture = tm.getRectByFrameName("menu_button_right.png");
        Rect rightArrowCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_right_pressed.png");
        this.rightArrow.setRects(this.rightArrowRectOnScreen, rightArrowCropRectOnTexture, rightArrowCropRectOnTexture_pressed, rightArrowCropRectOnTexture_pressed);
        this.rightArrow.initialize();
        Rect leftArrowCropRectOnTexture = tm.getRectByFrameName("menu_button_left.png");
        Rect leftArrowCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_left_pressed.png");
        this.leftArrow.setRects(this.leftArrowRectOnScreen, leftArrowCropRectOnTexture, leftArrowCropRectOnTexture_pressed, leftArrowCropRectOnTexture_pressed);
        this.leftArrow.initialize();
        Rect backCropRectOnTexture = tm.getRectByFrameName("menu_button_back.png");
        Rect backCropRectOnTexture_pressed = tm.getRectByFrameName("menu_button_back_pressed.png");
        this.backArrow.setRects(this.backRectOnScreen, backCropRectOnTexture, backCropRectOnTexture_pressed, backCropRectOnTexture_pressed);
        this.backArrow.initialize();
    }

    private void setRects(LevelButton levelButton, int levelNumber, int levelPackID) {
        int realLevelNumber = levelNumber - 1;
        Level currentLevel = this.currentLevelPack[realLevelNumber];
        Rect rectOnScreen = new Rect((((SCREEN_WIDTH / 2) + (SCREEN_WIDTH * (realLevelNumber / 15))) + (((realLevelNumber % 5) - 2) * 67)) - 30, (((realLevelNumber % 15) / 5) * 67) + VERTICAL_BORDER, (SCREEN_WIDTH / 2) + (SCREEN_WIDTH * (realLevelNumber / 15)) + (((realLevelNumber % 5) - 2) * 67) + 30, (((realLevelNumber % 15) / 5) * 67) + VERTICAL_BORDER + LEVEL_BUTTON_SIZE);
        if (levelPackID == RollItConstants.LEVEL_PACK_GRASS.intValue()) {
            levelButton.setRects(rectOnScreen, this.grassLevelButtonCropRectOnTexture, this.grassLevelButtonCropRectOnTexture_pressed, this.grassLevelButtonCropRectOnTexture_disabled);
        } else {
            RollItConstants.LEVEL_PACK_ICE.intValue();
        }
        if (currentLevel.getLevelNumber() > Settings.maxAvelibleLevelNumber) {
            levelButton.setEnabled(false);
        }
    }

    public void update() {
    }

    public void scrollLevelsLeft() {
        if (this.currentLevelScreen != 1) {
            this.currentLevelScreen--;
        }
        moveButtons(LEFT);
        blockUserInput();
    }

    public void scrollLevelsRight() {
        if (this.currentLevelScreen != this.numberOfLevelScreens) {
            this.currentLevelScreen++;
        }
        moveButtons(RIGHT);
        blockUserInput();
    }

    private void blockUserInput() {
        performAction(RollItConstants.ACTION_DISABLE_USER_INPUT, null);
    }

    private void moveButtons(int direction) {
        hideArrows();
        for (int i = 1; i <= this.currentButtonsNumber; i++) {
            this.currentLevelButtons.get(i).move(direction);
        }
    }

    private void moveButtonsInstantly(int direction) {
        performAction(RollItConstants.ACTION_DISABLE_USER_INPUT, null);
        for (int i = 1; i <= this.currentButtonsNumber; i++) {
            this.currentLevelButtons.get(i).moveInstantly(direction);
        }
        performAction(RollItConstants.ACTION_APPLY_BUTTONS_MOVEMENT_FINISH, null);
    }

    private void hideArrows() {
        this.rightArrow.visible = false;
        this.leftArrow.visible = false;
    }

    public void checkArrowsVisibility() {
        if (this.currentLevelScreen == 1) {
            this.leftArrow.visible = false;
        } else {
            this.leftArrow.visible = true;
        }
        if (this.currentLevelScreen == this.numberOfLevelScreens) {
            this.rightArrow.visible = false;
        } else {
            this.rightArrow.visible = true;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void unlockCurrentLevel() {
        if (this.currentLevelButtons != null) {
            this.currentLevelButtons.get(Settings.maxAvelibleLevelNumber).setEnabled(true);
        } else if (this.grassLevelButtons.size() > Settings.maxAvelibleLevelNumber) {
            this.grassLevelButtons.get(Settings.maxAvelibleLevelNumber).setEnabled(true);
        }
    }

    public void resetLevelButtons() {
        int grassButtonsSize = this.grassLevelButtons.size();
        for (int i = 2; i < grassButtonsSize; i++) {
            this.grassLevelButtons.get(i).setEnabled(false);
        }
    }

    public void scrollLevelsLeftWithoutAnimation() {
        if (this.currentLevelScreen != 1) {
            this.currentLevelScreen--;
        }
        moveButtonsInstantly(LEFT);
    }

    public void scrollLevelsRightWithoutAnimation() {
        if (this.currentLevelScreen != this.numberOfLevelScreens) {
            this.currentLevelScreen++;
        }
        moveButtonsInstantly(RIGHT);
    }

    public void loadCurrentLevelPack(int levelPackID) {
        if (levelPackID == RollItConstants.LEVEL_PACK_GRASS.intValue()) {
            setupNewLevelButtons(this.grassLevelButtons);
        } else {
            RollItConstants.LEVEL_PACK_ICE.intValue();
        }
        checkArrowsVisibility();
    }

    private void setVisible(ArrayList<LevelButton> levelButtons, boolean visible) {
        int buttonsNumber = levelButtons.size();
        for (int i = 1; i < buttonsNumber; i++) {
            levelButtons.get(i).visible = visible;
        }
    }

    private void setupOldLevelButtons(ArrayList<LevelButton> oldLvlButtons) {
        while (this.currentLevelScreen > 1) {
            scrollLevelsLeftWithoutAnimation();
        }
        setVisible(oldLvlButtons, false);
    }

    private void setupNewLevelButtons(ArrayList<LevelButton> newLevelButtons) {
        this.currentLevelButtons = newLevelButtons;
        this.currentButtonsNumber = this.currentLevelButtons.size() - 1;
        this.numberOfLevelScreens = ((this.currentButtonsNumber - 1) / 15) + 1;
        setVisible(newLevelButtons, true);
    }
}
