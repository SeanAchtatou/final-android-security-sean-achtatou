package com.boolbalabs.rollit.menucomponents;

import com.boolbalabs.lib.game.ZDrawable;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.transitions.Transition;
import com.boolbalabs.lib.transitions.TransitionMovement;

public class SliderTransition extends TransitionMovement {
    private float accelerationSign;
    private long durationMs;
    private boolean fixedOnFinish;
    private boolean isInitialized = false;
    private float shiftEndRip_X;
    private float shiftEndRip_Y;
    private float shiftStartRip_X;
    private float shiftStartRip_Y;

    public SliderTransition(ZDrawable view) {
        super(view);
    }

    public void initialize(float shiftStartRip_X2, float shiftEndRip_X2, float shiftStartRip_Y2, float shiftEndRip_Y2, float accelerationSign2, long durationMs2, boolean fixedOnFinish2) {
        this.shiftStartRip_X = shiftStartRip_X2;
        this.shiftEndRip_X = shiftEndRip_X2;
        this.shiftStartRip_Y = shiftStartRip_Y2;
        this.shiftEndRip_Y = shiftEndRip_Y2;
        this.accelerationSign = accelerationSign2;
        this.durationMs = durationMs2;
        this.fixedOnFinish = true;
        super.initialize(shiftStartRip_X2, shiftEndRip_X2, shiftStartRip_Y2, shiftEndRip_Y2, accelerationSign2, durationMs2, true);
        this.isInitialized = true;
    }

    public void onTransitionStart() {
        super.onTransitionStart();
        ((ZNode) this.parentView).userInteractionEnabled = false;
    }

    public void onTransitionStop() {
        this.parentView.setPositionInRip(this.originalPosition.x, this.originalPosition.y + ((int) this.shiftEndRip_Y));
        reset();
        ((ZNode) this.parentView).userInteractionEnabled = true;
    }

    public Transition createCopy(ZDrawable view) {
        if (!this.isInitialized) {
            return null;
        }
        SliderTransition newTransition = new SliderTransition(view);
        newTransition.initialize(this.shiftStartRip_X, this.shiftEndRip_X, this.shiftStartRip_Y, this.shiftEndRip_Y, this.accelerationSign, this.durationMs, this.fixedOnFinish);
        return newTransition;
    }
}
