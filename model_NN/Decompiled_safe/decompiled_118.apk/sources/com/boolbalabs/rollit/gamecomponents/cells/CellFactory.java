package com.boolbalabs.rollit.gamecomponents.cells;

import android.util.Log;

public class CellFactory {
    public static Cell makeCell(int x, int y, int z, int arg1, int arg2, int cellType) {
        switch (cellType) {
            case 1:
                return NormalCell.getInstance().makeCell(x, y, z);
            case 2:
                return new FinishCell(x, y, z);
            case 3:
                return new TeleportCell(x, y, z, arg1, arg2);
            case 4:
                return new CatapultCell(x, y, z, arg1, arg2);
            case 5:
                return new ButtonCell(x, y, z, arg1, arg2);
            case 6:
                return new ShiftingCell(x, y, z, arg1);
            case 7:
                return new FallOnStepCell(x, y, z, arg1);
            case 8:
                return new BridgeCell(x, y, z, arg1);
            default:
                Log.e("Cell factory", "Wrong cell type: " + cellType);
                return null;
        }
    }

    public static Cell makeCell(int x, int y, int z, int arg1, int cellType) {
        return makeCell(x, y, z, arg1, 0, cellType);
    }

    public static Cell makeCell(int x, int y, int z, int cellType) {
        return makeCell(x, y, z, 0, cellType);
    }
}
