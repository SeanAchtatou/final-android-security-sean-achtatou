package com.boolbalabs.rollit.gamecomponents;

import android.graphics.Rect;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.Log;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.rollit.animators.Animator;
import com.boolbalabs.rollit.animators.ButtonPushAnimator;
import com.boolbalabs.rollit.animators.CatapultAnimator;
import com.boolbalabs.rollit.animators.ClimbDownAnimator;
import com.boolbalabs.rollit.animators.ClimbUpAnimator;
import com.boolbalabs.rollit.animators.FallAnimator;
import com.boolbalabs.rollit.animators.LevelCompleteAnimator;
import com.boolbalabs.rollit.animators.RollAnimator;
import com.boolbalabs.rollit.animators.ShiftAnimator;
import com.boolbalabs.rollit.animators.TeleportAnimator;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.Axes;
import com.boolbalabs.utility.Point3D;
import java.nio.FloatBuffer;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;

public class RotatingCubeSprite extends ZNode {
    private static final float[] box = {-w, h, d, w, h, d, -w, h, -d, w, h, -d, -w, -h, d, -w, -h, -d, w, -h, d, w, -h, -d, w, -h, d, w, h, d, -w, -h, d, -w, h, d, -w, -h, -d, -w, h, -d, w, -h, -d, w, h, -d, -w, -h, d, -w, h, d, -w, -h, -d, -w, h, -d, w, -h, -d, w, h, -d, w, -h, d, w, h, d};
    private static final FloatBuffer cubeBuff = BufferUtils.makeFloatBuffer(box);
    private static Animator currentAnimator;
    private static final float[] currentMatrix = new float[16];
    private static float d = 0.48f;
    private static float h = 0.48f;
    private static final float[] innerNormals = {0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f};
    private static final FloatBuffer innerNormalsBuff = BufferUtils.makeFloatBuffer(innerNormals);
    private static Axes lastStableAxes;
    private static final float[] lastStableMatrix = new float[16];
    private static final float[] outerNormals = {0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f};
    private static final FloatBuffer outerNormalsBuff = BufferUtils.makeFloatBuffer(outerNormals);
    private static final float[] tempMatrix = new float[16];
    private static float w = 0.48f;
    private boolean animationRunning = false;
    private final Animator buttonPushAnimator;
    private final Animator catapultAnimator;
    private final Animator climbDownAnimator;
    private final Animator climbUpAnimator;
    private final RotatingCube cubeModel;
    public int direction = RollItConstants.DIRECTION_NONE;
    private final Animator fallAnimator;
    private final Animator levelCompleteAnimator;
    private final Animator rollAnimator;
    private final Animator shiftAnimator;
    private float sideB = 1.0f;
    private float sideL = 0.0f;
    private float sideR = 1.0f;
    private float sideT = 0.5f;
    private final Animator teleportAnimator;
    private FloatBuffer texBuff = BufferUtils.makeFloatBuffer(new float[48]);
    private float[] texCoords;
    private float topB = 0.5f;
    private float topL = 0.0f;
    private float topR = 1.0f;
    private float topT = 0.0f;

    public RotatingCubeSprite(int resourceId, RotatingCube cubeModel2) {
        super(resourceId, 1);
        this.cubeModel = cubeModel2;
        lastStableAxes = new Axes();
        this.rollAnimator = new RollAnimator(this);
        this.fallAnimator = new FallAnimator(this);
        this.climbUpAnimator = new ClimbUpAnimator(this);
        this.climbDownAnimator = new ClimbDownAnimator(this);
        this.shiftAnimator = new ShiftAnimator(this);
        this.teleportAnimator = new TeleportAnimator(this);
        this.catapultAnimator = new CatapultAnimator(this);
        this.buttonPushAnimator = new ButtonPushAnimator(this);
        this.levelCompleteAnimator = new LevelCompleteAnimator(this);
    }

    public void initialize() {
        Matrix.setIdentityM(lastStableMatrix, 0);
        lastStableMatrix[12] = RotatingCube.currentCoordinates.x;
        lastStableMatrix[13] = RotatingCube.currentCoordinates.y;
        lastStableMatrix[14] = RotatingCube.currentCoordinates.z;
        System.arraycopy(lastStableMatrix, 0, currentMatrix, 0, 16);
        lastStableAxes.reset();
    }

    /* access modifiers changed from: protected */
    public void createTextureBuffer() {
    }

    public void update() {
        applyCameraTransformation();
        if (!this.animationRunning) {
            applyLastStableState();
        } else {
            applyAnimationTransformation();
        }
    }

    public void draw(GL10 gl) {
        setupCube(gl);
        gl.glLoadMatrixf(currentMatrix, 0);
        drawCube(gl);
        gl.glLoadIdentity();
    }

    public void startAnimation(int cubeMovement, Bundle movementBundle) {
        switch (cubeMovement) {
            case RollItConstants.MOVEMENT_NONE /*60100*/:
                throw new IllegalArgumentException("RotatingCubeSprite.startAnimation: cube doesn't move, but animation was started!");
            case RollItConstants.MOVEMENT_ROLL /*60101*/:
                currentAnimator = this.rollAnimator;
                break;
            case RollItConstants.MOVEMENT_FALL /*60102*/:
                currentAnimator = this.fallAnimator;
                break;
            case RollItConstants.MOVEMENT_CLIMB_UP /*60103*/:
                currentAnimator = this.climbUpAnimator;
                break;
            case RollItConstants.MOVEMENT_CLIMB_DOWN /*60104*/:
                currentAnimator = this.climbDownAnimator;
                break;
            case RollItConstants.MOVEMENT_SHIFT /*60105*/:
                currentAnimator = this.shiftAnimator;
                break;
            case RollItConstants.MOVEMENT_TELEPORT /*60106*/:
                currentAnimator = this.teleportAnimator;
                break;
            case RollItConstants.MOVEMENT_THROW /*60107*/:
                currentAnimator = this.catapultAnimator;
                break;
            case RollItConstants.MOVEMENT_PUSH_BUTTON /*60108*/:
                currentAnimator = this.buttonPushAnimator;
                break;
            case RollItConstants.MOVEMENT_LEVEL_COMPLETE /*60109*/:
                currentAnimator = this.levelCompleteAnimator;
                break;
            default:
                throw new IllegalArgumentException("RotatingCubeSprite.startAnimation: incorrect cube state: " + cubeMovement);
        }
        currentAnimator.initialize(lastStableMatrix, lastStableAxes, movementBundle);
        currentAnimator.startAnimation();
        setAnimationRunning(true);
    }

    private void setAnimationRunning(boolean running) {
        this.animationRunning = running;
    }

    private void applyCameraTransformation() {
        System.arraycopy(GameCamera.getInstance().getMatrix(), 0, currentMatrix, 0, 16);
    }

    private void applyLastStableState() {
        System.arraycopy(currentMatrix, 0, tempMatrix, 0, 16);
        Matrix.multiplyMM(currentMatrix, 0, tempMatrix, 0, lastStableMatrix, 0);
    }

    private void applyAnimationTransformation() {
        System.arraycopy(currentMatrix, 0, tempMatrix, 0, 16);
        Matrix.multiplyMM(currentMatrix, 0, tempMatrix, 0, currentAnimator.calculateNext(), 0);
    }

    public void loadContent() {
        TexturesManager.getInstance().addTexture(new Texture2D(getResourceId(), Texture2D.TextureFilter.Linear, Texture2D.TextureFilter.Linear, Texture2D.TextureWrap.ClampToEdge, Texture2D.TextureWrap.ClampToEdge));
    }

    public void unloadContent() {
        super.unloadContent();
    }

    public void applyAnimationFinishedNotification() {
        updateStableState();
        stopMovement();
        updateCubeModel();
        performAction(RollItConstants.ACTION_ON_STEP_COMPLETED, null);
    }

    private void updateStableState() {
        System.arraycopy(cleanInaccuracies(currentAnimator.getLastStableMatrix()), 0, lastStableMatrix, 0, 16);
        lastStableAxes = currentAnimator.worldAxes;
    }

    private void stopMovement() {
        setAnimationRunning(false);
    }

    private void updateCubeModel() {
        this.cubeModel.updateCubeCoordinates(lastStableMatrix[12], lastStableMatrix[13], lastStableMatrix[14]);
        this.cubeModel.updateState(RollItConstants.MOVEMENT_NONE);
        this.cubeModel.updateSpecialSide(lastStableAxes.yAxis);
        RotatingCube.state = RollItConstants.STATE_READY_TO_ACT;
    }

    private float[] cleanInaccuracies(float[] matrix) {
        for (int i = 0; i < 15; i++) {
            matrix[i] = (float) Math.round(matrix[i]);
        }
        return matrix;
    }

    public void reset() {
        setAnimationRunning(false);
        if (currentAnimator != null) {
            currentAnimator.reset();
        }
        Point3D start = GameField.getInstance().start;
        Matrix.setIdentityM(lastStableMatrix, 0);
        Matrix.translateM(lastStableMatrix, 0, start.x, start.y, start.z);
        System.arraycopy(lastStableMatrix, 0, currentMatrix, 0, 16);
        lastStableAxes.xAxis.set(1.0f, 0.0f, 0.0f);
        lastStableAxes.yAxis.set(0.0f, 1.0f, 0.0f);
        lastStableAxes.zAxis.set(0.0f, 0.0f, 1.0f);
    }

    public void setupCube(GL10 gl) {
        gl.glBindTexture(3553, getTextureGlobalIndex());
        gl.glVertexPointer(3, 5126, 0, cubeBuff);
        gl.glEnableClientState(32884);
        gl.glTexCoordPointer(2, 5126, 0, this.texBuff);
        gl.glEnableClientState(32888);
        gl.glEnableClientState(32885);
    }

    public void drawCube(GL10 gl) {
        gl.glEnable(2884);
        gl.glCullFace(1028);
        gl.glNormalPointer(5126, 0, innerNormalsBuff);
        gl.glDepthMask(true);
        gl.glDrawArrays(5, 0, 4);
        gl.glDepthMask(false);
        gl.glDrawArrays(5, 4, 4);
        gl.glDrawArrays(5, 8, 4);
        gl.glDrawArrays(5, 12, 4);
        gl.glDrawArrays(5, 16, 4);
        gl.glDrawArrays(5, 20, 4);
        gl.glCullFace(1029);
        gl.glNormalPointer(5126, 0, outerNormalsBuff);
        gl.glDepthMask(true);
        gl.glDrawArrays(5, 0, 4);
        gl.glDepthMask(false);
        gl.glDrawArrays(5, 4, 4);
        gl.glDrawArrays(5, 8, 4);
        gl.glDrawArrays(5, 12, 4);
        gl.glDrawArrays(5, 16, 4);
        gl.glDrawArrays(5, 20, 4);
        gl.glDepthMask(true);
        gl.glEnable(2884);
    }

    public static float[] getAnimationVariables() {
        return currentAnimator.getAimationVariables();
    }

    public static int getDirectionForWinEvent() {
        if (lastStableAxes.zAxis.z == 1.0f) {
            return RollItConstants.DIRECTION_NORTH;
        }
        if (lastStableAxes.zAxis.z == -1.0f) {
            return RollItConstants.DIRECTION_SOUTH;
        }
        if (lastStableAxes.zAxis.x == 1.0f) {
            return RollItConstants.DIRECTION_WEST;
        }
        if (lastStableAxes.zAxis.x == -1.0f) {
            return RollItConstants.DIRECTION_EAST;
        }
        Log.w("RotatingCubeSprite", "Direction not detected");
        return RollItConstants.DIRECTION_NONE;
    }

    public boolean animationRunning() {
        return this.animationRunning;
    }

    public void refreshTextures(HashMap<Integer, String> currentTexturesNames) {
        TexturesManager tm = TexturesManager.getInstance();
        String top = currentTexturesNames.get(RollItConstants.FACE_TOP);
        Rect topFaceRectOnTexture = tm.getRectByFrameName(top);
        Rect sideFaceRectOnTexture = tm.getRectByFrameName(currentTexturesNames.get(RollItConstants.FACE_SIDE));
        if (topFaceRectOnTexture == null) {
            topFaceRectOnTexture = new Rect(0, 0, 20, 20);
            Log.e("Texture refresh", "WRONG TEXTURE TOP RECTANGLE NAME: " + top + "; default Rect (0,0,20,20) is set");
        }
        if (sideFaceRectOnTexture == null) {
            sideFaceRectOnTexture = new Rect(0, 0, 20, 20);
            Log.e("Texture refresh", "WRONG TEXTURE SIDE RECTANGLE NAME: " + top + "; default Rect (0,0,20,20) is set");
        }
        int textureHeight = 4096;
        int textureWidth = 4096;
        Texture2D currentTexture = tm.getTextureByResourceId(getResourceId());
        if (currentTexture != null) {
            textureWidth = currentTexture.getWidth();
            textureHeight = currentTexture.getHeight();
        } else {
            Log.w("Texture refresh", "CubeSprite: current texture IS NULL, default texture sizes are set");
        }
        this.topT = ((float) (topFaceRectOnTexture.top + 1)) / ((float) textureHeight);
        this.topB = ((float) (topFaceRectOnTexture.bottom - 1)) / ((float) textureHeight);
        this.topL = ((float) (topFaceRectOnTexture.left + 1)) / ((float) textureWidth);
        this.topR = ((float) (topFaceRectOnTexture.right - 1)) / ((float) textureWidth);
        this.sideT = ((float) (sideFaceRectOnTexture.top + 1)) / ((float) textureHeight);
        this.sideB = ((float) (sideFaceRectOnTexture.bottom - 1)) / ((float) textureHeight);
        this.sideL = ((float) (sideFaceRectOnTexture.left + 1)) / ((float) textureWidth);
        this.sideR = ((float) (sideFaceRectOnTexture.right - 1)) / ((float) textureWidth);
        refreshTextureBuffer();
    }

    private void refreshTextureBuffer() {
        this.texCoords = new float[]{this.topL, this.topB, this.topR, this.topB, this.topL, this.topT, this.topR, this.topT, this.sideL, this.sideT, this.sideL, this.sideB, this.sideR, this.sideT, this.sideR, this.sideB, this.sideR, this.sideB, this.sideR, this.sideT, this.sideL, this.sideB, this.sideL, this.sideT, this.sideR, this.sideB, this.sideR, this.sideT, this.sideL, this.sideB, this.sideL, this.sideT, this.sideR, this.sideB, this.sideR, this.sideT, this.sideL, this.sideB, this.sideL, this.sideT, this.sideR, this.sideB, this.sideR, this.sideT, this.sideL, this.sideB, this.sideL, this.sideT};
        this.texBuff.clear();
        this.texBuff.put(this.texCoords);
        this.texBuff.position(0);
    }
}
