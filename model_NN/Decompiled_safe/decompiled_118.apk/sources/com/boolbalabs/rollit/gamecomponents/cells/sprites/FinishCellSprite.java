package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import android.opengl.Matrix;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;

public class FinishCellSprite extends AnimatedCellSprite {
    public static final float WIN_EVENT_ANIMATION_LENGTH_COEFF = 5.0f;
    private static float buttonYmove = 0.0f;
    private final float RAISE_HEIGHT = 2.0f;

    public FinishCellSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
        this.animationLength = 2000;
        this.totalMovement = 2.0f;
    }

    /* access modifiers changed from: protected */
    public void calculateAnimation() {
        if (this.progress < 0.2f) {
            buttonYmove = this.totalMovement * 5.0f * this.progress;
        }
    }

    /* access modifiers changed from: protected */
    public void applyAnimation() {
        Matrix.translateM(this.currentMatrix, 0, 0.0f, buttonYmove, 0.0f);
    }

    public void stopAnimation() {
        setAnimationRunning(false);
        this.cell.coordinate.y = 2.0f;
    }

    public void onLevelRestart() {
    }

    public static float getYmove() {
        return buttonYmove;
    }
}
