package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import android.opengl.Matrix;
import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.lib.transitions.SineInterpolator;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;

public class ButtonCellSprite extends AnimatedCellSprite {
    public static final float ANIMATION_LENGTH_COEFF = 2.0f;
    private static final float PUSH_DEPTH_COEFF = 0.4f;
    private static float buttonYmove = 0.0f;
    private int buttonPressedSound = R.raw.gameplay_button_pressed;

    public ButtonCellSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
        this.animationLength = 800;
        this.interpolator = new SineInterpolator(EasingType.OUT);
        this.totalMovement = PUSH_DEPTH_COEFF;
    }

    /* access modifiers changed from: protected */
    public void calculateAnimation() {
        float progress = calculateProgress();
        if (((double) progress) <= 0.2d) {
            buttonYmove = (-this.totalMovement) * this.interpolator.getInterpolation(5.0f * progress);
        } else if (((double) progress) <= 0.5d) {
            buttonYmove = -this.totalMovement;
        } else {
            buttonYmove = (-this.totalMovement) * this.interpolator.getInterpolation(2.0f * (1.0f - progress));
        }
    }

    /* access modifiers changed from: protected */
    public void applyAnimation() {
        Matrix.translateM(this.currentMatrix, 0, 0.0f, buttonYmove, 0.0f);
    }

    public static float getYmove() {
        return buttonYmove;
    }

    /* access modifiers changed from: protected */
    public void playAppropriateSound() {
        playSound(this.buttonPressedSound, ZageCommonSettings.soundEnabled);
    }
}
