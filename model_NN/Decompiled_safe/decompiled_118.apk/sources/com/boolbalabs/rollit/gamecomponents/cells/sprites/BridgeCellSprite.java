package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import com.boolbalabs.lib.transitions.BackInterpolator;
import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.rollit.gamecomponents.cells.BridgeCell;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;

public class BridgeCellSprite extends AnimatedCellSprite {
    private static final float BRIDGE_CELL_ANIMATION_LENGTH_COEFF = 2.0f;
    public static final float FLIGHT_HEIGHT = 2.0f;
    public boolean mustPerformAnimation = false;

    public BridgeCellSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
        this.interpolator = new BackInterpolator(EasingType.INOUT, 3.0f);
        this.animationLength = 800;
        this.totalMovement = 2.0f;
    }

    public void startMovement() {
        super.startMovement();
    }

    public void reinitialize() {
        this.mustPerformAnimation = false;
    }

    public void onLevelRestart() {
        if (this.mustPerformAnimation) {
            startMovement();
        }
        this.mustPerformAnimation = false;
    }

    /* access modifiers changed from: protected */
    public void calculateAnimation() {
        if (((BridgeCell) this.cell).isInAir()) {
            this.ymove = this.totalMovement * this.interpolator.getInterpolation(this.progress);
        } else {
            this.ymove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        }
    }

    public void stopAnimation() {
        setAnimationRunning(false);
        if (((BridgeCell) this.cell).isInAir()) {
            this.cell.coordinate.y = 2.0f;
        } else {
            this.cell.coordinate.y = 0.0f;
        }
    }

    public void playAppropriateSound() {
    }
}
