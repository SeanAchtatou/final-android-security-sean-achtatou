package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.lib.transitions.FreeFallInterpolator;
import com.boolbalabs.rollit.gamecomponents.RotatingCube;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.settings.RollItConstants;

public class FallOnStepCellSprite extends AnimatedCellSprite {
    private static final float FALL_LENGTH_COEFF = 1.3f;
    private int direction = 0;

    public FallOnStepCellSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
        this.interpolator = new FreeFallInterpolator(EasingType.IN, 5.0f, 1.5f);
        this.animationLength = 3200;
        this.totalMovement = 10.4f;
        this.totalRotation = 720.0f;
    }

    public void startMovement() {
        this.direction = RotatingCube.getDirection();
        super.startMovement();
    }

    public void stopAnimation() {
        this.direction = RollItConstants.DIRECTION_NONE;
        super.stopAnimation();
    }

    /* access modifiers changed from: protected */
    public void calculateAnimation() {
        this.ymove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        switch (this.direction) {
            case RollItConstants.DIRECTION_NONE /*62100*/:
                this.xrot = (this.totalRotation / 4.0f) * this.progress;
                this.zrot = (this.totalRotation / 4.0f) * this.progress;
                return;
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                this.xrot = (-this.totalRotation) * this.progress;
                return;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                this.xrot = this.totalRotation * this.progress;
                return;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                this.zrot = (-this.totalRotation) * this.progress;
                return;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                this.zrot = this.totalRotation * this.progress;
                return;
            default:
                return;
        }
    }
}
