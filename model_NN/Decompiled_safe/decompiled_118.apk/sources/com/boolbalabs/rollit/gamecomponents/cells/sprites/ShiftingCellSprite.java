package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import com.boolbalabs.rollit.gamecomponents.cells.Cell;

public class ShiftingCellSprite extends CellSprite {
    public ShiftingCellSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
    }
}
