package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import android.opengl.Matrix;
import com.boolbalabs.rollit.gamecomponents.GameCamera;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.gamecomponents.cells.NormalCell;
import javax.microedition.khronos.opengles.GL10;

public class NormalCellSprite extends CellSprite {
    private static int cellsCount = 0;
    float[] tempMatrix = new float[16];

    public NormalCellSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
    }

    public void draw(GL10 gl) {
        System.arraycopy(GameCamera.getInstance().getMatrix(), 0, this.currentMatrix, 0, 16);
        Matrix.translateM(this.currentMatrix, 0, 0.0f, -0.76f, 0.0f);
        for (int i = 0; i < cellsCount; i++) {
            System.arraycopy(this.currentMatrix, 0, this.tempMatrix, 0, 16);
            Matrix.translateM(this.tempMatrix, 0, NormalCell.coords[i].x, NormalCell.coords[i].y, NormalCell.coords[i].z);
            gl.glLoadMatrixf(this.tempMatrix, 0);
            drawShape(gl);
        }
        gl.glLoadIdentity();
    }

    public static void initializeCellsCount(int normalCellsCount) {
        cellsCount = normalCellsCount;
    }
}
