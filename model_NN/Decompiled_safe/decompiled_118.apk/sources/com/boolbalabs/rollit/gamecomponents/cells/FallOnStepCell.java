package com.boolbalabs.rollit.gamecomponents.cells;

import android.os.Bundle;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.RotatingCube;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.FallOnStepCellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.GameStatic;

public class FallOnStepCell extends Cell {
    private boolean alreadyRunning = false;
    private int fallCondition;
    private final Bundle fallOnStepCellBundle = GameStatic.makeBundle();

    public FallOnStepCell(int x, int y, int z, int condition) {
        this.coordinate.set((float) x, (float) y, (float) z);
        this.fallCondition = condition;
        setAppropriateTexturesNames(condition);
        this.cellTextureRef = R.drawable.rc_gameplay_texture_grass;
        this.cellSprite = new FallOnStepCellSprite(this.cellTextureRef, this);
        addChild(this.cellSprite);
    }

    public void reinitialize(int x, int y, int z, int condition, int arg2) {
        if (condition != this.fallCondition) {
            this.fallCondition = condition;
            setAppropriateTexturesNames(this.fallCondition);
            refreshTextures();
            this.cellSprite.reinitialize();
        }
        super.reinitialize(x, y, z, condition, arg2);
    }

    /* access modifiers changed from: protected */
    public void setAppropriateTexturesNames(int condition) {
        if (condition == 21001) {
            this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "fall_on_special_cell_top.png");
        } else if (condition == 21002) {
            this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "fall_on_not_special_cell_top.png");
        }
        this.allPossibleTexturesNames.put(RollItConstants.FACE_SIDE, "normal_cell_side.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_BOTTOM, "normal_cell_bottom.png");
    }

    public void activate() {
        if (!this.alreadyRunning && mustFall()) {
            this.fallOnStepCellBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_FALL);
            this.fallOnStepCellBundle.putInt(RollItConstants.KEY_DIRECTION, RotatingCube.getDirection());
            this.alreadyRunning = true;
            makeCellFall();
            performAction(RollItConstants.ACTION_FALL, this.fallOnStepCellBundle);
        }
    }

    private boolean mustFall() {
        if (this.fallCondition == 21001 && RotatingCube.specialSideDown()) {
            return true;
        }
        if (this.fallCondition != 21002 || RotatingCube.specialSideDown()) {
            return false;
        }
        return true;
    }

    private void makeCellFall() {
        ((FallOnStepCellSprite) this.cellSprite).startMovement();
    }

    public void onLevelRestart() {
        this.alreadyRunning = false;
        ((FallOnStepCellSprite) this.cellSprite).onLevelRestart();
    }
}
