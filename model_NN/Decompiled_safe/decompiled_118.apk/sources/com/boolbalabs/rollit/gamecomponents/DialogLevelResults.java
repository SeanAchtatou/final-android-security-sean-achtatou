package com.boolbalabs.rollit.gamecomponents;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import com.boolbalabs.lib.elements.BLButton;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.NumberView2D;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import com.flurry.android.FlurryAgent;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;

public class DialogLevelResults extends ZNode {
    private static final int DRAW_TIME = 1000;
    private static final int SCORE_DRAW_STEP = 5;
    private Rect bgRectOnScreenRip;
    private ZNode bgView;
    String[] bigDigitsFrameNames;
    private final int dialogSound = R.raw.level_completed;
    String[] digitsFrameNames;
    private Point highscorePointRip;
    private Rect highscoreRectOnScreenRip;
    private ZNode highscoreTextView;
    private NumberView2D highscoreView;
    private BLButton nextLevelButton;
    private Rect nextLevelRectOnScreen;
    private BLButton restartButton;
    private Rect restartRectOnScreen;
    private Point scorePointRip;
    private Rect scoreRectOnScreenRip;
    private ZNode scoreTextView;
    private NumberView2D scoreView;
    private final int textureRef = R.drawable.rc_common_texture;
    private TexturesManager texturesManager;
    private Rect totalRectOnScreenRip;
    private Point totalScorePointRip;
    private NumberView2D totalScoreView;
    private ZNode totalTextView;

    public DialogLevelResults() {
        super(-1, 0);
        if (ScreenMetrics.resolution_postfix.equals("lres")) {
            this.bgRectOnScreenRip = new Rect((ScreenMetrics.screenWidthRip / 2) - 160, 70, (ScreenMetrics.screenWidthRip / 2) + 140, 310);
        } else {
            this.bgRectOnScreenRip = new Rect((ScreenMetrics.screenWidthRip / 2) - 150, 30, (ScreenMetrics.screenWidthRip / 2) + 150, 270);
        }
        this.totalScorePointRip = new Point(this.bgRectOnScreenRip.right - 55, this.bgRectOnScreenRip.top + 30);
        this.highscorePointRip = new Point(this.bgRectOnScreenRip.right - 55, this.bgRectOnScreenRip.top + 60);
        this.scorePointRip = new Point(this.bgRectOnScreenRip.right - 55, this.bgRectOnScreenRip.top + 100);
        this.totalRectOnScreenRip = new Rect(this.bgRectOnScreenRip.left + 50, this.bgRectOnScreenRip.top + 30, this.bgRectOnScreenRip.left + 50 + 56, this.bgRectOnScreenRip.top + 30 + 24);
        this.highscoreRectOnScreenRip = new Rect(this.bgRectOnScreenRip.left + 50, this.bgRectOnScreenRip.top + 60, this.bgRectOnScreenRip.left + 50 + 106, this.bgRectOnScreenRip.top + 60 + 24);
        this.scoreRectOnScreenRip = new Rect(this.bgRectOnScreenRip.left + 50, this.bgRectOnScreenRip.top + 100, this.bgRectOnScreenRip.left + 50 + 98, this.bgRectOnScreenRip.top + 100 + 36);
        this.digitsFrameNames = new String[]{"gameplay_digit_0.png", "gameplay_digit_1.png", "gameplay_digit_2.png", "gameplay_digit_3.png", "gameplay_digit_4.png", "gameplay_digit_5.png", "gameplay_digit_6.png", "gameplay_digit_7.png", "gameplay_digit_8.png", "gameplay_digit_9.png"};
        this.bigDigitsFrameNames = new String[]{"gameplay_digit_big_0.png", "gameplay_digit_big_1.png", "gameplay_digit_big_2.png", "gameplay_digit_big_3.png", "gameplay_digit_big_4.png", "gameplay_digit_big_5.png", "gameplay_digit_big_6.png", "gameplay_digit_big_7.png", "gameplay_digit_big_8.png", "gameplay_digit_big_9.png"};
        this.nextLevelRectOnScreen = new Rect((this.bgRectOnScreenRip.right - 40) - 100, (this.bgRectOnScreenRip.bottom - 30) - 60, this.bgRectOnScreenRip.right - 40, this.bgRectOnScreenRip.bottom - 30);
        this.restartRectOnScreen = new Rect(this.bgRectOnScreenRip.left + 40, (this.bgRectOnScreenRip.bottom - 30) - 60, this.bgRectOnScreenRip.left + 40 + 100, this.bgRectOnScreenRip.bottom - 30);
        this.userInteractionEnabled = true;
        createViews();
        createNumberViews();
        setVisible(false);
    }

    private void createViews() {
        this.bgView = new ZNode(R.drawable.rc_common_texture, 0);
        addChild(this.bgView);
        this.totalTextView = new ZNode(R.drawable.rc_common_texture, 0);
        addChild(this.totalTextView, false);
        this.scoreTextView = new ZNode(R.drawable.rc_common_texture, 0);
        addChild(this.scoreTextView, false);
        this.highscoreTextView = new ZNode(R.drawable.rc_common_texture, 0);
        addChild(this.highscoreTextView, false);
        this.nextLevelButton = new BLButton(R.drawable.rc_common_texture) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                DialogLevelResults.this.nextLevelButtonClick();
            }
        };
        this.nextLevelButton.touchUpSound = R.raw.click_sound;
        addChild(this.nextLevelButton);
        this.restartButton = new BLButton(R.drawable.rc_common_texture) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                DialogLevelResults.this.replayButtonClick();
            }
        };
        this.restartButton.touchUpSound = R.raw.click_sound;
        addChild(this.restartButton);
    }

    private void createNumberViews() {
        this.totalScoreView = new NumberView2D(R.drawable.rc_common_texture);
        addChild(this.totalScoreView, false);
        this.scoreView = new NumberView2D(R.drawable.rc_common_texture);
        addChild(this.scoreView, false);
        this.highscoreView = new NumberView2D(R.drawable.rc_common_texture);
        addChild(this.highscoreView, false);
    }

    public void initialize() {
        this.texturesManager = TexturesManager.getInstance();
        this.bgView.initWithFrame(this.bgRectOnScreenRip, this.texturesManager.getRectByFrameName("gameplay_level_complete_dialog_bg.png"));
        this.nextLevelButton.setRects(this.nextLevelRectOnScreen, this.texturesManager.getRectByFrameName("gameplay_dialog_results_button_next.png"), this.texturesManager.getRectByFrameName("gameplay_dialog_results_button_next_pressed.png"), this.texturesManager.getRectByFrameName("gameplay_dialog_results_button_next_pressed.png"));
        this.restartButton.setRects(this.restartRectOnScreen, this.texturesManager.getRectByFrameName("gameplay_dialog_results_button_restart.png"), this.texturesManager.getRectByFrameName("gameplay_dialog_results_button_restart_pressed.png"), this.texturesManager.getRectByFrameName("gameplay_dialog_results_button_restart_pressed.png"));
        this.totalScoreView.initNumberView(this.totalScorePointRip, this.digitsFrameNames, NumberView2D.ALIGN_RIGHT, 24);
        this.scoreView.initNumberView(this.scorePointRip, this.bigDigitsFrameNames, NumberView2D.ALIGN_RIGHT, 36);
        this.scoreView.drawingMode = NumberView2D.NumberDrawingMode.NCONTINUOUS;
        this.highscoreView.initNumberView(this.highscorePointRip, this.digitsFrameNames, NumberView2D.ALIGN_RIGHT, 24);
        this.totalTextView.initWithFrame(this.totalRectOnScreenRip, this.texturesManager.getRectByFrameName("gameplay_label_total.png"));
        this.highscoreTextView.initWithFrame(this.highscoreRectOnScreenRip, this.texturesManager.getRectByFrameName("gameplay_label_highscore.png"));
        this.scoreTextView.initWithFrame(this.scoreRectOnScreenRip, this.texturesManager.getRectByFrameName("gameplay_label_score.png"));
        setAllToZero();
        super.initialize();
    }

    /* access modifiers changed from: protected */
    public void protectedUpdate() {
        if (this.bgView.visible) {
            super.protectedUpdate();
        }
    }

    public void update() {
    }

    public void drawNode(GL10 gl) {
        gl.glTexEnvf(8960, 8704, 7681.0f);
        super.drawNode(gl);
    }

    /* access modifiers changed from: private */
    public void nextLevelButtonClick() {
        performAction(RollItConstants.ACTION_PLAY_NEXT_LEVEL, null);
        hide();
    }

    /* access modifiers changed from: private */
    public void replayButtonClick() {
        Log.i("Flurry", "reset pressed");
        Settings.flurryResetUsed++;
        performAction(RollItConstants.ACTION_RESTART, null);
        hide();
    }

    public void show(Bundle resultsBundle) {
        int currentStepsNumber = resultsBundle.getInt(RollItConstants.KEY_CURRENT_STEPS, 0);
        int currentScore = resultsBundle.getInt(RollItConstants.KEY_CURRENT_SCORE, 0);
        int bestScore = resultsBundle.getInt(RollItConstants.KEY_BEST_SCORE, 0);
        int totalScore = resultsBundle.getInt(RollItConstants.KEY_TOTAL_SCORE_OF_CURRENT_PACK, 0);
        boolean scoreResultImproved = resultsBundle.getBoolean(RollItConstants.KEY_SCORE_IMPROVED, false);
        boolean wasCompletedBefore = resultsBundle.getBoolean(RollItConstants.KEY_WAS_COMPLETED_BEFORE, false);
        boolean solveButtonUsed = resultsBundle.getBoolean(RollItConstants.KEY_SOLVE_BUTTON_USED, false);
        int currentLevelNumber = resultsBundle.getInt(RollItConstants.KEY_CURRENT_LEVEL, -1);
        this.scoreView.setContinuousDrawingParameters(calculateDrawingSpeed(currentScore), 5);
        if (scoreResultImproved) {
            int scoreDrawTime = calculateDrawingSpeed(bestScore);
            this.highscoreView.setContinuousDrawingParameters(scoreDrawTime, 5);
            this.totalScoreView.setContinuousDrawingParameters(scoreDrawTime, 5);
            this.totalScoreView.drawingMode = NumberView2D.NumberDrawingMode.NDISCRETE;
            this.totalScoreView.setNumberToDraw(totalScore - bestScore);
            this.totalScoreView.increaseInstantly();
            this.highscoreView.drawingMode = NumberView2D.NumberDrawingMode.NCONTINUOUS;
            this.totalScoreView.drawingMode = NumberView2D.NumberDrawingMode.NCONTINUOUS;
        } else {
            this.highscoreView.drawingMode = NumberView2D.NumberDrawingMode.NDISCRETE;
            this.totalScoreView.drawingMode = NumberView2D.NumberDrawingMode.NDISCRETE;
        }
        this.scoreView.setNumberToDraw(currentScore);
        this.highscoreView.setNumberToDraw(bestScore);
        this.totalScoreView.setNumberToDraw(totalScore);
        setVisible(true);
        playSound(R.raw.level_completed, ZageCommonSettings.soundEnabled);
        sendInfoToFlurry(currentLevelNumber, currentStepsNumber, scoreResultImproved, wasCompletedBefore, solveButtonUsed);
    }

    private int calculateDrawingSpeed(int currentScore) {
        if (currentScore > 5) {
            return DRAW_TIME / (currentScore / 5);
        }
        return DRAW_TIME;
    }

    private void sendInfoToFlurry(int currentLevelID, int currentStepsNumber, boolean scoreImproved, boolean wasCompletedBefore, boolean solveButtonUsed) {
        DebugLog.i("Flurry", "level " + currentLevelID + " completed. Steps: " + currentStepsNumber + ". Result improved: " + scoreImproved + ". Was completed before: " + wasCompletedBefore + ". Solve button used: " + solveButtonUsed);
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("STEPS", String.valueOf(currentStepsNumber));
        parameters.put("RESULT_IMPROVED", String.valueOf(scoreImproved));
        parameters.put("WAS_COMPLETED_BEFORE", String.valueOf(wasCompletedBefore));
        parameters.put("SOLVE_BUTTON_USED", String.valueOf(solveButtonUsed));
        FlurryAgent.onEvent("Z_LEVEL_COMPLETED_" + currentLevelID, parameters);
    }

    public void hide() {
        setVisible(false);
        setAllToZero();
    }

    private void setVisible(boolean visible) {
        this.visible = visible;
        this.bgView.visible = visible;
        this.nextLevelButton.visible = visible;
        this.restartButton.visible = visible;
        this.scoreView.visible = visible;
        this.highscoreView.visible = visible;
        this.totalScoreView.visible = visible;
        this.highscoreTextView.visible = visible;
        this.scoreTextView.visible = visible;
        this.totalTextView.visible = visible;
    }

    private void setAllToZero() {
        this.scoreView.setNumberToDraw(0);
        this.highscoreView.setNumberToDraw(0);
        this.totalScoreView.setNumberToDraw(0);
    }
}
