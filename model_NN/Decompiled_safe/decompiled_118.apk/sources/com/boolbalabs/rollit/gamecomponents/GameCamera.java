package com.boolbalabs.rollit.gamecomponents;

import android.graphics.Point;
import android.opengl.Matrix;
import android.view.animation.Interpolator;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.lib.transitions.QuadInterpolator;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.gamecomponents.two_d.CameraSliderView;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.ExtendedMatrix;
import com.boolbalabs.utility.GameStatic;
import com.boolbalabs.utility.Point3D;
import javax.microedition.khronos.opengles.GL10;

public class GameCamera extends ZNode {
    private static GameCamera instance = null;
    public static int state = RollItConstants.STATE_READY_TO_ACT;
    private long animationLength;
    private boolean animationRunning = false;
    private float centerX;
    private float centerY;
    private float centerZ;
    private long currentAnimationTime;
    private final float[] currentCameraMatrix = new float[16];
    private float eyeX;
    private float eyeY;
    private float eyeZ;
    private float initialYrot;
    private CameraInputProcessor inputProcessor = new CameraInputProcessor();
    private Interpolator interpolator;
    private float[] lightPos = {3.0f, 5.0f, 1.0f, 0.0f};
    private Point3D oldCenterPoint = new Point3D();
    private Point3D oldEyePoint = new Point3D();
    private float progress;
    private float radius;
    private float radiusX = 5.0f;
    private float radiusY = 5.0f;
    private float radiusZ = 5.0f;
    private float rotationSpeed = 0.0f;
    private long startAnimationTime;
    private Point3D targetCenterPoint = new Point3D();
    private Point3D targetEyePoint = new Point3D();
    private float upX = 0.0f;
    private float upY = 1.0f;
    private float upZ = 0.0f;
    private float yrot = 0.0f;

    public static GameCamera getInstance() {
        if (instance == null) {
            instance = new GameCamera();
        }
        return instance;
    }

    private GameCamera() {
        super(-1, 0);
        Matrix.setIdentityM(this.currentCameraMatrix, 0);
        updateCamera();
        this.interpolator = new QuadInterpolator(EasingType.INOUT);
        this.userInteractionEnabled = false;
    }

    public void set(float eyeX2, float eyeY2, float eyeZ2, float centerX2, float centerY2, float centerZ2) {
        this.eyeX = eyeX2;
        this.eyeY = eyeY2;
        this.eyeZ = eyeZ2;
        this.centerX = centerX2;
        this.centerY = centerY2;
        this.centerZ = centerZ2;
        this.radiusX = eyeX2 - centerX2;
        this.radiusY = eyeY2 - centerY2;
        this.radiusZ = eyeZ2 - centerZ2;
        this.radius = (float) Math.sqrt((double) ((this.radiusX * this.radiusX) + (this.radiusZ * this.radiusZ)));
        this.oldCenterPoint.set(centerX2, centerY2, centerZ2);
        this.targetCenterPoint.set(centerX2, centerY2, centerZ2);
        this.oldEyePoint.set(this.radiusX + centerX2, this.radiusY + centerY2, this.radiusZ + centerZ2);
        this.targetEyePoint.set(this.radiusX + centerX2, this.radiusY + centerY2, this.radiusZ + centerZ2);
        instatntlyChangeRotation();
        updateCamera();
    }

    public void setInitialYrot(int yrot2) {
        this.initialYrot = -((float) yrot2);
    }

    public void initialize() {
        super.initialize();
        ExtendedMatrix.gluPerspective(GameStatic.projectionM, 45.0f, ((float) ScreenMetrics.screenWidthPix) / ((float) ScreenMetrics.screenHeightPix), 1.0f, 100.0f);
        refreshRotation();
    }

    public void update() {
        switch (state) {
            case RollItConstants.STATE_READY_TO_ACT /*43001*/:
            case RollItConstants.STATE_BLOCKED /*43003*/:
            default:
                return;
            case RollItConstants.STATE_MOVING /*43002*/:
                moveCamera();
                updateCamera();
                return;
            case RollItConstants.STATE_ROTATING /*43004*/:
                if (RotatingCube.state == 43001) {
                    rotateCamera();
                    updateCamera();
                    return;
                }
                return;
        }
    }

    public void touchMoveAction(Point touchDownPoint, Point currentPoint) {
        calculateRotationSpeed(currentPoint);
        setCameraState(RollItConstants.STATE_ROTATING);
    }

    public void touchUpAction(Point touchDownPoint, Point touchUpPoint) {
        resetRotationParameters();
        RotatingCube.state = RollItConstants.STATE_READY_TO_ACT;
        setCameraState(RollItConstants.STATE_READY_TO_ACT);
    }

    private void refreshRotation() {
        this.yrot = 0.0f;
        this.radiusX = (float) (((double) this.radius) * Math.sin((((double) this.yrot) * 3.141592653589793d) / 180.0d));
        this.radiusZ = (float) (((double) this.radius) * Math.cos((((double) this.yrot) * 3.141592653589793d) / 180.0d));
        this.eyeX = this.centerX + this.radiusX;
        this.eyeZ = this.centerZ + this.radiusZ;
    }

    private void instatntlyChangeRotation() {
        this.yrot = this.initialYrot;
        this.radiusX = (float) (((double) this.radius) * Math.sin((((double) this.yrot) * 3.141592653589793d) / 180.0d));
        this.radiusZ = (float) (((double) this.radius) * Math.cos((((double) this.yrot) * 3.141592653589793d) / 180.0d));
        this.eyeX = this.centerX + this.radiusX;
        this.eyeZ = this.centerZ + this.radiusZ;
        updateCamera();
    }

    private void moveCamera() {
        calculateProgress();
        if (this.progress > 1.0f) {
            stopCameraMovement();
        }
        calculateEyeMovement();
        calculateCenterMovement();
    }

    private void calculateProgress() {
        this.currentAnimationTime = System.currentTimeMillis() - this.startAnimationTime;
        this.progress = ((float) this.currentAnimationTime) / ((float) this.animationLength);
    }

    private void startCameraMovement() {
        this.animationRunning = true;
        setCameraState(RollItConstants.STATE_MOVING);
        this.startAnimationTime = System.currentTimeMillis();
    }

    private void stopCameraMovement() {
        this.oldCenterPoint.set(this.targetCenterPoint);
        this.oldEyePoint.set(this.targetEyePoint);
        setCameraState(RollItConstants.STATE_READY_TO_ACT);
        this.animationRunning = false;
    }

    public void followTheObject(Point3D targetCoordinates) {
        setCenterPosition(targetCoordinates);
        setEyePosition(targetCoordinates);
        setAnimationLength();
        startCameraMovement();
    }

    private void setEyePosition(Point3D targetCoordinates) {
        this.oldEyePoint.set(this.centerX + this.radiusX, this.centerY + this.radiusY, this.centerZ + this.radiusZ);
        this.targetEyePoint.set(targetCoordinates.x + this.radiusX, targetCoordinates.y + this.radiusY, targetCoordinates.z + this.radiusZ);
    }

    private void setCenterPosition(Point3D targetCoordinates) {
        this.oldCenterPoint.set(this.centerX, this.centerY, this.centerZ);
        this.targetCenterPoint.set(targetCoordinates);
    }

    public void followTheObject(float x, float y, float z) {
        this.targetCenterPoint.set(x, y, z);
        followTheObject(this.targetCenterPoint);
    }

    private void setAnimationLength() {
        this.animationLength = (long) (480.00003f * ((float) Math.sqrt(Math.pow((double) (this.targetCenterPoint.x - this.oldCenterPoint.x), 2.0d) + Math.pow((double) (this.targetCenterPoint.y - this.oldCenterPoint.y), 2.0d) + Math.pow((double) (this.targetCenterPoint.z - this.oldCenterPoint.z), 2.0d))));
        if (this.animationLength <= 0) {
            this.animationLength = 100;
        }
    }

    private void calculateCenterMovement() {
        this.centerX = this.oldCenterPoint.x + ((this.targetCenterPoint.x - this.oldCenterPoint.x) * this.interpolator.getInterpolation(this.progress));
        this.centerY = this.oldCenterPoint.y + ((this.targetCenterPoint.y - this.oldCenterPoint.y) * this.interpolator.getInterpolation(this.progress));
        this.centerZ = this.oldCenterPoint.z + ((this.targetCenterPoint.z - this.oldCenterPoint.z) * this.interpolator.getInterpolation(this.progress));
    }

    private void calculateEyeMovement() {
        this.eyeX = this.oldEyePoint.x + ((this.targetEyePoint.x - this.oldEyePoint.x) * this.interpolator.getInterpolation(this.progress));
        this.eyeY = this.oldEyePoint.y + ((this.targetEyePoint.y - this.oldEyePoint.y) * this.interpolator.getInterpolation(this.progress));
        this.eyeZ = this.oldEyePoint.z + ((this.targetEyePoint.z - this.oldEyePoint.z) * this.interpolator.getInterpolation(this.progress));
    }

    private void rotateCamera() {
        if (Math.abs(this.rotationSpeed) > 3.0f) {
            this.rotationSpeed = ((float) (this.rotationSpeed > 0.0f ? 1 : -1)) * 3.0f;
        }
        calculateCameraEyePosition();
    }

    private void calculateCameraEyePosition() {
        this.yrot -= this.rotationSpeed * 0.75f;
        this.radiusX = (float) (((double) this.radius) * Math.sin((((double) this.yrot) * 3.141592653589793d) / 180.0d));
        this.radiusZ = (float) (((double) this.radius) * Math.cos((((double) this.yrot) * 3.141592653589793d) / 180.0d));
        this.eyeX = this.centerX + this.radiusX;
        this.eyeZ = this.centerZ + this.radiusZ;
    }

    private void resetRotationParameters() {
        this.rotationSpeed = 0.0f;
    }

    private void calculateRotationSpeed(Point currentPoint) {
        int currentSpeedSign;
        this.rotationSpeed = (((float) Math.abs(currentPoint.y - CameraSliderView.centralPointWhenVisibleRip.y)) * 3.0f) / 110.0f;
        this.rotationSpeed *= (float) (currentPoint.y - CameraSliderView.centralPointWhenVisibleRip.y < 0 ? 1 : -1);
        if (Math.abs(this.rotationSpeed) >= 3.0f) {
            if (this.rotationSpeed > 0.0f) {
                currentSpeedSign = 1;
            } else {
                currentSpeedSign = -1;
            }
            this.rotationSpeed = ((float) currentSpeedSign) * 3.0f;
        }
    }

    private void updateCamera() {
        ExtendedMatrix.setLookAtM(this.currentCameraMatrix, 0, this.eyeX, this.eyeY, this.eyeZ, this.centerX, this.centerY, this.centerZ, this.upX, this.upY, this.upZ);
    }

    public void applyGlobalLightPosition() {
        GL10 gl = DisplayServiceOpenGL.getInstance().gl;
        gl.glLoadMatrixf(this.currentCameraMatrix, 0);
        gl.glLightfv(16384, 4611, this.lightPos, 0);
    }

    public boolean animationRunning() {
        return this.animationRunning;
    }

    private void setCameraState(int newState) {
        state = newState;
    }

    public float[] getMatrix() {
        return this.currentCameraMatrix;
    }

    public float getRotationAngle() {
        return this.yrot;
    }

    public float getRotationSpeed() {
        return this.rotationSpeed;
    }

    public void onLevelRestart() {
        followTheObject(GameField.getInstance().start);
    }

    public CameraInputProcessor getInputProcessor() {
        return this.inputProcessor;
    }

    public class CameraInputProcessor {
        Point currentPointRip = new Point();

        public CameraInputProcessor() {
        }

        /* access modifiers changed from: package-private */
        public boolean processTouchDown(Point touchDownPoint) {
            return true;
        }

        public boolean processTouchMove(Point touchDownPoint, Point currentPoint) {
            if (GameCamera.state == 43002 || RotatingCube.state != 43001) {
                return false;
            }
            ScreenMetrics.convertPointPixToRip(currentPoint, this.currentPointRip);
            GameCamera.this.touchMoveAction(touchDownPoint, this.currentPointRip);
            return true;
        }

        public boolean processTouchUp(Point touchDownPoint, Point touchUpPoint) {
            if (GameCamera.state == 43002 || RotatingCube.state != 43001) {
                return false;
            }
            GameCamera.this.touchUpAction(touchDownPoint, touchUpPoint);
            return true;
        }
    }
}
