package com.boolbalabs.rollit.gamecomponents.cells;

import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.GameField;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.NormalCellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.Point3D;
import com.inmobi.androidsdk.impl.Constants;
import java.util.Arrays;
import javax.microedition.khronos.opengles.GL10;

public class NormalCell extends Cell {
    private static int cellsCount = 0;
    public static final Point3D[] coords = new Point3D[Constants.HTTP_SUCCESS_CODE];
    private static NormalCell dummyInstance = new NormalCell();
    private static NormalCell instance;
    private boolean dummyUsed = false;
    private final Point3D tempCoordinate = new Point3D();
    private boolean themeAlreadyChosen = false;

    static {
        GameField.getInstance().getClass();
    }

    private NormalCell() {
        setAppropriateTexturesNames(0);
        this.cellTextureRef = R.drawable.rc_gameplay_texture_grass;
        this.cellSprite = new NormalCellSprite(this.cellTextureRef, this);
        addChild(this.cellSprite);
    }

    /* access modifiers changed from: protected */
    public void setAppropriateTexturesNames(int condition) {
        this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "normal_cell_top.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_SIDE, "normal_cell_side.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_BOTTOM, "normal_cell_bottom.png");
    }

    public static NormalCell getInstance() {
        if (instance == null) {
            instance = new NormalCell();
        }
        return instance;
    }

    public Cell makeCell(int x, int y, int z) {
        coords[cellsCount] = new Point3D((float) x, (float) y, (float) z);
        cellsCount++;
        return instance;
    }

    public static int getCellsCount() {
        return cellsCount;
    }

    public void initialize() {
        NormalCellSprite.initializeCellsCount(cellsCount);
        super.initialize();
    }

    public void draw(GL10 gl) {
        super.draw(gl);
    }

    public void reinitialize(int x, int y, int z, int arg1, int arg2) {
        super.reinitialize(x, y, z, arg1, arg2);
    }

    public Cell getCell(int x, int z) {
        int i = 0;
        while (true) {
            if (i < cellsCount) {
                if (coords[i].x == ((float) x) && coords[i].z == ((float) z)) {
                    this.tempCoordinate.x = coords[i].x;
                    this.tempCoordinate.y = coords[i].y;
                    this.tempCoordinate.z = coords[i].z;
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        if (!this.dummyUsed) {
            dummyInstance.coordinate.set(this.tempCoordinate);
            this.dummyUsed = true;
            return dummyInstance;
        }
        instance.coordinate.set(this.tempCoordinate);
        this.dummyUsed = false;
        return instance;
    }

    public void cleanUp() {
        Arrays.fill(coords, (Object) null);
        cellsCount = 0;
        this.themeAlreadyChosen = false;
    }

    public void refreshTextures() {
        if (!this.themeAlreadyChosen) {
            super.refreshTextures();
            this.themeAlreadyChosen = true;
        }
    }

    public void onLevelRestart() {
    }

    public int getTextureGlobalIndex() {
        return this.cellSprite.getTextureGlobalIndex();
    }
}
