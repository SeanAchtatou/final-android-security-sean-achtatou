package com.boolbalabs.rollit.gamecomponents.cells;

import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.rollit.SceneGameplay;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.CellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.Point3D;
import java.nio.Buffer;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;

public abstract class Cell extends ZNode {
    public static final int TYPE_BRIDGE = 8;
    public static final int TYPE_BUTTON = 5;
    public static final int TYPE_CATAPULT = 4;
    public static final int TYPE_FALL_ON_STEP = 7;
    public static final int TYPE_FINISH = 2;
    public static final int TYPE_NORMAL = 1;
    public static final int TYPE_SHIFT = 6;
    public static final int TYPE_TELEPORT = 3;
    HashMap<Integer, String> allPossibleTexturesNames = new HashMap<>();
    protected CellSprite cellSprite;
    protected int cellTextureRef;
    public Point3D coordinate = new Point3D();
    HashMap<Integer, String> currentTexturesNames = new HashMap<>();
    public int direction = RollItConstants.DIRECTION_NONE;
    public int state = RollItConstants.STATE_READY_TO_ACT;

    public abstract void onLevelRestart();

    /* access modifiers changed from: protected */
    public abstract void setAppropriateTexturesNames(int i);

    public Cell() {
        super(-1, 0);
        this.userInteractionEnabled = false;
    }

    public void reinitialize(int x, int y, int z, int arg1, int arg2) {
        this.coordinate.set((float) x, (float) y, (float) z);
        refreshTextures();
        super.initialize();
    }

    public void loadContent() {
        super.loadContent();
    }

    public void unloadContent() {
        super.unloadContent();
    }

    /* access modifiers changed from: protected */
    public void createTextureBuffer() {
    }

    public void update() {
    }

    public void draw(GL10 gl) {
        this.cellSprite.draw(gl);
    }

    public int getCellHeight() {
        return (int) this.coordinate.y;
    }

    public Point3D getCoordinate() {
        return this.coordinate;
    }

    public void activate() {
    }

    public void switchState() {
    }

    public void refreshTextures() {
        setAppropriateTexturesNames(0);
        this.currentTexturesNames.put(RollItConstants.FACE_TOP, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleTexturesNames.get(RollItConstants.FACE_TOP));
        this.currentTexturesNames.put(RollItConstants.FACE_SIDE, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleTexturesNames.get(RollItConstants.FACE_SIDE));
        this.currentTexturesNames.put(RollItConstants.FACE_BOTTOM, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleTexturesNames.get(RollItConstants.FACE_BOTTOM));
        this.cellSprite.refreshTextures(this.currentTexturesNames);
    }

    public Buffer getCellTextureCoordsBuffer() {
        return this.cellSprite.getTextureCoordsBuffer();
    }

    public void cleanUp() {
        this.state = RollItConstants.STATE_READY_TO_ACT;
        this.coordinate.set(0.0f, 0.0f, 0.0f);
        this.direction = RollItConstants.DIRECTION_NONE;
    }
}
