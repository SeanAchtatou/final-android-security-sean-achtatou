package com.boolbalabs.rollit.gamecomponents.two_d;

import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import com.boolbalabs.lib.elements.BLButton;
import com.boolbalabs.lib.elements.BLCheckButton;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.transitions.TransitionMovement;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.GameCamera;
import com.boolbalabs.rollit.gamecomponents.RotatingCube;
import com.boolbalabs.rollit.menucomponents.SliderTransition;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;

public class InteractionBoard extends ZNode {
    private final long SOLVE_BUTTON_SHOW_TIME_OFFSET = 200;
    /* access modifiers changed from: private */
    public BLCheckButton cameraButton;
    private Rect cameraButtonRectOnScreen = new Rect((ScreenMetrics.screenLargeSideRip - 5) - 64, 5, ScreenMetrics.screenLargeSideRip - 5, 69);
    /* access modifiers changed from: private */
    public CameraSliderView cameraSliderView;
    private long currentTime;
    private int interactionTextureRef = R.drawable.rc_common_texture;
    private BLButton restartButton;
    private Rect restartButtonRectOnScreen = new Rect((((ScreenMetrics.screenLargeSideRip - 5) - 64) - 10) - 64, 5, ((ScreenMetrics.screenLargeSideRip - 5) - 64) - 10, 69);
    private Rect sliderRectOnScreen = new Rect((ScreenMetrics.screenLargeSideRip - 5) - 64, (((-ScreenMetrics.screenSmallSideRip) + 5) + 64) + 5, ScreenMetrics.screenLargeSideRip - 5, ((((-ScreenMetrics.screenSmallSideRip) + 5) + 64) + 5) + CameraSliderView.SLIDER_HEIGHT_RIP);
    /* access modifiers changed from: private */
    public BLButton solveButton;
    private TransitionMovement solveButtonHiding;
    private Rect solveButtonRectOnScreen = new Rect(5, 251, 138, 315);
    private TransitionMovement solveButtonShowing;
    private long startTime;
    private boolean timerStarted = false;
    /* access modifiers changed from: private */
    public SliderTransition transitionDown;
    /* access modifiers changed from: private */
    public SliderTransition transitionUp;

    public InteractionBoard(GameCamera camera) {
        super(-1, 0);
        this.userInteractionEnabled = true;
        createElements(camera);
        createTransitions();
    }

    private void createElements(GameCamera camera) {
        this.cameraSliderView = new CameraSliderView(this.interactionTextureRef, camera);
        addChild(this.cameraSliderView);
        this.cameraButton = new BLCheckButton(this.interactionTextureRef) {
            public void touchDownAction(Point touchDownPoint) {
                if (InteractionBoard.this.cameraButton.userInteractionEnabled) {
                    super.touchDownAction(touchDownPoint);
                }
            }

            public void onSelectionChange(boolean selected) {
                if (selected) {
                    InteractionBoard.this.cameraSliderView.setTransition(InteractionBoard.this.transitionDown);
                } else {
                    InteractionBoard.this.cameraSliderView.setTransition(InteractionBoard.this.transitionUp);
                }
                InteractionBoard.this.cameraSliderView.startTransition();
                InteractionBoard.this.cameraButton.startTransition();
                InteractionBoard.this.cameraButton.playSound(R.raw.click_sound, ZageCommonSettings.soundEnabled);
            }
        };
        addChild(this.cameraButton);
        this.restartButton = new BLButton(this.interactionTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                if (allowedToRestartLevel()) {
                    Log.i("Flurry", "reset pressed");
                    Settings.flurryResetUsed++;
                    performAction(RollItConstants.ACTION_RESTART, null);
                }
            }

            private boolean allowedToRestartLevel() {
                return (GameCamera.state != 43002 && RotatingCube.state == 43001) || RotatingCube.currentMovement == 60102;
            }
        };
        addChild(this.restartButton);
        this.solveButton = new BLButton(this.interactionTextureRef) {
            public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
                if (allowedToStartSolvingTheLevel()) {
                    performAction(RollItConstants.ACTION_START_SOLVING, null);
                }
            }

            private boolean allowedToStartSolvingTheLevel() {
                return GameCamera.state != 43002 && RotatingCube.state == 43001;
            }
        };
        addChild(this.solveButton);
    }

    public void initialize() {
        initElements();
        super.initialize();
        this.cameraButton.setSelected(true);
    }

    private void createTransitions() {
        this.solveButtonShowing = new TransitionMovement(this.solveButton) {
            public void onTransitionStart() {
                super.onTransitionStart();
                InteractionBoard.this.solveButton.visible = true;
            }
        };
        this.solveButtonShowing.initialize(-150.0f, 0.0f, 0.0f, 0.0f, 1.0f, 300, false);
        this.solveButtonHiding = new TransitionMovement(this.solveButton) {
            public void onTransitionStop() {
                super.onTransitionStop();
                InteractionBoard.this.solveButton.visible = false;
            }
        };
        this.solveButtonHiding.initialize(0.0f, -150.0f, 0.0f, 0.0f, 1.0f, 300, false);
        SliderTransition fakeTransition = new SliderTransition(this.cameraButton);
        fakeTransition.initialize(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 400, false);
        this.cameraButton.setTransition(fakeTransition);
        this.transitionUp = new SliderTransition(this.cameraSliderView);
        this.transitionUp.initialize(0.0f, 0.0f, 0.0f, (float) (-ScreenMetrics.screenSmallSideRip), 1.0f, 400, true);
        this.transitionDown = new SliderTransition(this.cameraSliderView);
        this.transitionDown.initialize(0.0f, 0.0f, 0.0f, (float) ScreenMetrics.screenSmallSideRip, 1.0f, 400, true);
    }

    private void initElements() {
        TexturesManager tm = TexturesManager.getInstance();
        this.cameraButton.setRects(this.cameraButtonRectOnScreen, tm.getRectByFrameName("gameplay_button_camera_rotation.png"), tm.getRectByFrameName("gameplay_button_camera_rotation_pressed.png"));
        Rect restartRectOnTexture = tm.getRectByFrameName("gameplay_button_restart.png");
        Rect restartRectOnTexture_pressed = tm.getRectByFrameName("gameplay_button_restart_pressed.png");
        this.restartButton.setRects(this.restartButtonRectOnScreen, restartRectOnTexture, restartRectOnTexture_pressed, restartRectOnTexture_pressed);
        Rect solveRectOnTexture = tm.getRectByFrameName("gameplay_button_solve.png");
        Rect solveRectOnTexture_pressed = tm.getRectByFrameName("gameplay_button_solve_pressed.png");
        this.solveButton.setRects(this.solveButtonRectOnScreen, solveRectOnTexture, solveRectOnTexture_pressed, solveRectOnTexture_pressed);
        this.solveButton.visible = false;
        this.cameraSliderView.setRects(this.sliderRectOnScreen, tm.getRectByFrameName("gameplay_slider_camera_rotation.png"), tm.getRectByFrameName("gameplay_slider_camera_rotation_pressed_low.png"), tm.getRectByFrameName("gameplay_slider_camera_rotation_pressed_high.png"));
    }

    public void update() {
        updateSolveButton();
    }

    public void onPause() {
        stopSolveButtonShowTimer();
        super.onPause();
    }

    public void onLevelRestart() {
        stopSolveButtonShowTimer();
        hideSolveButton();
    }

    private void updateSolveButton() {
        if (this.timerStarted) {
            this.currentTime = System.currentTimeMillis() - this.startTime;
            if (this.currentTime > 200) {
                showSolveButtonIfPossible();
            }
        }
    }

    public void toggleCameraSliderState() {
        if (!this.cameraButton.getTransition().isStarted() || this.cameraButton.getTransition().isFinished()) {
            this.cameraButton.setSelected(!this.cameraButton.isSelected());
        }
    }

    private void showSolveButtonIfPossible() {
        stopSolveButtonShowTimer();
        if (RotatingCube.state == 43001) {
            showSolveButton();
        }
    }

    private void showSolveButton() {
        if (this.solveButton != null && !this.solveButton.visible) {
            this.solveButton.setTransition(this.solveButtonShowing);
            this.solveButton.startTransition();
        }
    }

    public void hideSolveButton() {
        if (this.solveButton != null && this.solveButton.visible) {
            this.solveButton.setTransition(this.solveButtonHiding);
            this.solveButton.startTransition();
        }
    }

    public boolean isSolveButtonVisible() {
        return this.solveButton.visible;
    }

    public void startSolveButtonShowTimer() {
        this.startTime = System.currentTimeMillis();
        this.timerStarted = true;
    }

    private void stopSolveButtonShowTimer() {
        this.timerStarted = false;
    }
}
