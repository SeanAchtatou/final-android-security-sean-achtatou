package com.boolbalabs.rollit.gamecomponents.two_d;

import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import com.boolbalabs.lib.elements.BLButton;
import com.boolbalabs.lib.transitions.Transition;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.gamecomponents.GameCamera;
import com.boolbalabs.rollit.settings.Settings;
import javax.microedition.khronos.opengles.GL10;

public class CameraSliderView extends BLButton {
    public static final int SLIDER_HEIGHT_RIP = 220;
    public static Point centralPointWhenVisibleRip = new Point();
    public static Rect rectOnScreenWhenVisibleRip = new Rect();
    private GameCamera camera;
    private Rect rectOnTexturePressedHigh;
    private Rect rectOnTexturePressedLow;

    public CameraSliderView(int resourceId, GameCamera camera2) {
        super(resourceId);
        this.camera = camera2;
        setZGL(0.0f);
    }

    public void initialize() {
        super.initialize();
        centralPointWhenVisibleRip.set(getCenterInRip().x, getCenterInRip().y + ScreenMetrics.screenSmallSideRip);
        rectOnScreenWhenVisibleRip = getFrameInRip();
        rectOnScreenWhenVisibleRip.bottom += ScreenMetrics.screenSmallSideRip;
        rectOnScreenWhenVisibleRip.top += ScreenMetrics.screenSmallSideRip;
    }

    public void setRects(Rect rectOnScreen, Rect rectOnTexture, Rect rectOnTexturePressedLow2, Rect rectOnTexturePressedHigh2) {
        super.setRects(rectOnScreen, rectOnTexture, rectOnTexturePressedLow2, rectOnTexturePressedHigh2);
        this.rectOnTexturePressedLow = rectOnTexturePressedLow2;
        this.rectOnTexturePressedHigh = rectOnTexturePressedHigh2;
    }

    public void startTransition() {
        super.startTransition();
    }

    public void setTransition(Transition transition) {
        super.setTransition(transition);
    }

    /* access modifiers changed from: protected */
    public boolean onTouchUp(Point touchDownPoint, Point touchUpPoint) {
        if (!pointInside(touchDownPoint)) {
            return false;
        }
        touchUpAction(touchDownPoint, touchUpPoint);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onTouchMove(Point touchDownPoint, Point currentPoint) {
        if (!pointInside(touchDownPoint) || !pointInside(currentPoint)) {
            return false;
        }
        touchMoveAction(touchDownPoint, currentPoint);
        setApprppriateTexture(currentPoint);
        return true;
    }

    private void setApprppriateTexture(Point currentPoint) {
        ScreenMetrics.convertPointPixToRip(currentPoint, currentPoint);
        if (currentPoint.y > centralPointWhenVisibleRip.y) {
            setRectOnTexture(this.rectOnTexturePressedLow);
        } else if (currentPoint.y < centralPointWhenVisibleRip.y) {
            setRectOnTexture(this.rectOnTexturePressedHigh);
        }
    }

    public void drawNode(GL10 gl) {
        super.drawNode(gl);
    }

    public void touchUpAction(Point touchDownPoint, Point touchUpPoint) {
        super.touchUpAction(touchDownPoint, touchUpPoint);
        this.camera.getInputProcessor().processTouchUp(touchDownPoint, touchUpPoint);
    }

    public void touchDownAction(Point touchDownPoint) {
        Log.i("Flurry", "camera used");
        Settings.flurryCameraUsed++;
    }

    public void touchMoveAction(Point touchDownPoint, Point currentPoint) {
        this.camera.getInputProcessor().processTouchMove(touchDownPoint, currentPoint);
    }
}
