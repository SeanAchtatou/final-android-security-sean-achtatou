package com.boolbalabs.rollit.gamecomponents.cells;

import android.os.Bundle;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.RotatingCube;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.FinishCellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.GameStatic;
import javax.microedition.khronos.opengles.GL10;

public class FinishCell extends Cell {
    private static boolean enabled = true;
    private final Bundle finishCellBundle = GameStatic.makeBundle();
    private float initialHeight;

    public FinishCell(int x, int y, int z) {
        this.coordinate.set((float) x, (float) y, (float) z);
        this.initialHeight = (float) y;
        setAppropriateTexturesNames(0);
        this.cellTextureRef = R.drawable.rc_gameplay_texture_grass;
        this.cellSprite = new FinishCellSprite(this.cellTextureRef, this);
        addChild(this.cellSprite);
    }

    public void reinitialize(int x, int y, int z, int arg1, int arg2) {
        this.initialHeight = (float) y;
        super.reinitialize(x, y, z, arg1, arg2);
    }

    /* access modifiers changed from: protected */
    public void setAppropriateTexturesNames(int condition) {
        this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "finish_cell_top.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_SIDE, "finish_cell_side.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_BOTTOM, "finish_cell_bottom.png");
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void activate() {
        if (RotatingCube.specialSideDown() && enabled) {
            this.finishCellBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_LEVEL_COMPLETE);
            this.finishCellBundle.putInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE);
            performAction(RollItConstants.ACTION_WIN_LEVEL, this.finishCellBundle);
            enabled = false;
            ((FinishCellSprite) this.cellSprite).startMovement();
        } else if (!RotatingCube.specialSideDown() && enabled) {
            performAction(RollItConstants.ACTION_SHOW_WRONG_SIDE_TOAST, null);
            enabled = false;
        }
    }

    public void update() {
    }

    public void draw(GL10 gl) {
        super.draw(gl);
    }

    public void onLevelRestart() {
        enable();
        this.coordinate.y = this.initialHeight;
        ((FinishCellSprite) this.cellSprite).onLevelRestart();
    }

    public static void enable() {
        enabled = true;
    }
}
