package com.boolbalabs.rollit.gamecomponents.two_d;

import android.graphics.Rect;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.settings.RollItConstants;

public class WrongSideToast extends ZNode {
    private static final long TOTAL_SHOW_TIME = 1000;
    private long showStartTime;
    private Rect toastRectOnScreen = new Rect((ScreenMetrics.screenLargeSideRip / 2) - 118, 75, (ScreenMetrics.screenLargeSideRip / 2) + 117, 123);

    public WrongSideToast() {
        super(R.drawable.rc_common_texture, 0);
    }

    public void initialize() {
        initWithFrame(this.toastRectOnScreen, TexturesManager.getInstance().getRectByFrameName("gameplay_label_wrong_side.png"));
    }

    public void hide() {
        this.visible = false;
    }

    public void update() {
        if (this.visible && System.currentTimeMillis() - this.showStartTime > TOTAL_SHOW_TIME) {
            performAction(RollItConstants.ACTION_HIDE_WRONG_SIDE_TOAST, null);
        }
    }

    public void show() {
        this.visible = true;
        this.showStartTime = System.currentTimeMillis();
    }
}
