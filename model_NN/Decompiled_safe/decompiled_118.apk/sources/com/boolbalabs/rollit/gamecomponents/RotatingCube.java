package com.boolbalabs.rollit.gamecomponents;

import android.graphics.Point;
import android.os.Bundle;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.SceneGameplay;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.ActionQueue;
import com.boolbalabs.utility.GameStatic;
import com.boolbalabs.utility.Point3D;
import java.util.HashMap;

public class RotatingCube extends ZNode {
    private static final Bundle cubeBundle = GameStatic.makeBundle();
    public static final Point3D currentCoordinates = new Point3D();
    public static int currentMovement = RollItConstants.MOVEMENT_NONE;
    private static int direction = RollItConstants.DIRECTION_NONE;
    private static boolean specialSideDown = false;
    public static int state = RollItConstants.STATE_READY_TO_ACT;
    HashMap<Integer, String> allPossibleTexturesNames = new HashMap<>();
    private Point correctTouchDownPoint = new Point();
    private int cubeTextureRef = R.drawable.rc_gameplay_texture_grass;
    HashMap<Integer, String> currentTexturesNames = new HashMap<>();
    private final ActionQueue movementQueue = new ActionQueue();
    private boolean mustUseCorrectedTouchDownPoint = false;
    private final RotatingCubeSprite rotatingCubeSprite;

    public RotatingCube() {
        super(-1, 1);
        this.userInteractionEnabled = true;
        setAppropriateTexturesNames(0);
        this.rotatingCubeSprite = new RotatingCubeSprite(this.cubeTextureRef, this);
        addChild(this.rotatingCubeSprite);
    }

    private void setAppropriateTexturesNames(int condition) {
        this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "cube_top.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_SIDE, "cube_side.png");
    }

    public void initialize() {
        currentCoordinates.set(GameField.getInstance().start);
        refreshTextures();
        super.initialize();
    }

    public void update() {
        switch (state) {
            case RollItConstants.STATE_READY_TO_ACT /*43001*/:
                if (!this.movementQueue.isEmpty()) {
                    startMovement(this.movementQueue.get());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void updateCubeCoordinates(float x, float y, float z) {
        currentCoordinates.x = x;
        currentCoordinates.y = y;
        currentCoordinates.z = z;
    }

    public void updateState(int newMovement) {
        currentMovement = newMovement;
    }

    public void updateSpecialSide(Point3D yAxis) {
        if (yAxis.y == -1.0f) {
            specialSideDown = true;
        } else {
            specialSideDown = false;
        }
    }

    private void startMovement(Bundle movementBundle) {
        currentMovement = movementBundle.getInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_NONE);
        direction = movementBundle.getInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE);
        updateGameComponentStates();
        this.rotatingCubeSprite.startAnimation(currentMovement, movementBundle);
    }

    private void updateGameComponentStates() {
        state = RollItConstants.STATE_MOVING;
        if (GameCamera.state != 43002) {
            GameCamera.state = RollItConstants.STATE_BLOCKED;
        }
    }

    public boolean pointInside(Point point) {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onTouchDown(Point touchDownPoint) {
        if (state != 43002) {
            return super.onTouchDown(touchDownPoint);
        }
        this.mustUseCorrectedTouchDownPoint = true;
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onTouchMove(Point touchDownPoint, Point currentPoint) {
        if (state == 43002) {
            this.correctTouchDownPoint.set(currentPoint.x, currentPoint.y);
            return false;
        } else if (state != 43001) {
            return false;
        } else {
            if (this.mustUseCorrectedTouchDownPoint) {
                return super.onTouchMove(this.correctTouchDownPoint, currentPoint);
            }
            return super.onTouchMove(touchDownPoint, currentPoint);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onTouchUp(Point touchDownPoint, Point touchUpPoint) {
        if (state == 43002) {
            this.mustUseCorrectedTouchDownPoint = false;
            return false;
        } else if (state != 43001) {
            return false;
        } else {
            if (!this.mustUseCorrectedTouchDownPoint) {
                return super.onTouchUp(touchDownPoint, touchUpPoint);
            }
            this.mustUseCorrectedTouchDownPoint = false;
            return super.onTouchUp(this.correctTouchDownPoint, touchUpPoint);
        }
    }

    public void touchUpAction(Point touchDownPoint, Point touchUpPoint) {
        if (state == 43001) {
            processTouchUp(touchDownPoint, touchUpPoint);
        }
    }

    public void processTouchUp(Point touchDownPoint, Point touchUpPoint) {
        if (GameStatic.shiftWasLongEnough(touchDownPoint, touchUpPoint)) {
            direction = GameStatic.detectMajorDirection((float) ((GameStatic.flatAnglei(touchDownPoint, touchUpPoint) / 3.141592653589793d) * 180.0d));
            if (direction != 62100) {
                cubeBundle.putInt(RollItConstants.KEY_DIRECTION, direction);
                cubeBundle.putSerializable(RollItConstants.KEY_CURR_COORDS, currentCoordinates);
                performAction(RollItConstants.ACTION_SELECT_MOVEMENT_TYPE, cubeBundle);
            }
        }
    }

    public void onLevelRestart() {
        currentMovement = RollItConstants.MOVEMENT_NONE;
        direction = RollItConstants.DIRECTION_NONE;
        currentCoordinates.set(GameField.getInstance().start);
        state = RollItConstants.STATE_READY_TO_ACT;
        this.rotatingCubeSprite.reset();
    }

    public void addMovement(Bundle movementBundle) {
        this.movementQueue.put(movementBundle);
    }

    public static Bundle getBundle() {
        return cubeBundle;
    }

    public static int getDirection() {
        return direction;
    }

    public static boolean specialSideDown() {
        return specialSideDown;
    }

    public void refreshTextures() {
        setAppropriateTexturesNames(0);
        this.currentTexturesNames.put(RollItConstants.FACE_TOP, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleTexturesNames.get(RollItConstants.FACE_TOP));
        this.currentTexturesNames.put(RollItConstants.FACE_SIDE, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleTexturesNames.get(RollItConstants.FACE_SIDE));
        this.rotatingCubeSprite.refreshTextures(this.currentTexturesNames);
    }
}
