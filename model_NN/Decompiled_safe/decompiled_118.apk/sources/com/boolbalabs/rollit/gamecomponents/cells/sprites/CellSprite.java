package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import android.graphics.Rect;
import android.opengl.Matrix;
import android.util.Log;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.rollit.gamecomponents.GameCamera;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.settings.RollItConstants;
import java.nio.FloatBuffer;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;

public abstract class CellSprite extends ZNode {
    private static final float[] box = {-w, h, d, w, h, d, -w, h, -d, w, h, -d, -w, -h, d, -w, -h, -d, w, -h, d, w, -h, -d, w, -h, d, w, h, d, -w, -h, d, -w, h, d, -w, -h, -d, -w, h, -d, w, -h, -d, w, h, -d, -w, -h, d, -w, h, d, -w, -h, -d, -w, h, -d, w, -h, -d, w, h, -d, w, -h, d, w, h, d};
    public static final FloatBuffer cellBuff = BufferUtils.makeFloatBuffer(box);
    private static float d = 0.5f;
    private static float h = 0.25f;
    private static final float[] normals = {0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f};
    public static final FloatBuffer normalsBuff = BufferUtils.makeFloatBuffer(normals);
    private static float w = 0.5f;
    protected float bottomB = 0.75f;
    protected float bottomL = 0.0f;
    protected float bottomR = 1.0f;
    protected float bottomT = 0.5f;
    protected Cell cell;
    protected float[] currentMatrix = new float[16];
    protected float sideB = 0.75f;
    protected float sideL = 0.0f;
    protected float sideR = 1.0f;
    protected float sideT = 0.5f;
    protected FloatBuffer texBuff = BufferUtils.makeFloatBuffer(new float[48]);
    protected float[] texCoords;
    protected float topB = 0.5f;
    protected float topL = 0.0f;
    protected float topR = 1.0f;
    protected float topT = 0.0f;

    public CellSprite(int resourceId, Cell cell2) {
        super(resourceId, 1);
        this.cell = cell2;
    }

    public CellSprite(int resourceId) {
        super(resourceId, 1);
    }

    public void reinitialize() {
    }

    /* access modifiers changed from: protected */
    public void createTextureBuffer() {
    }

    public void initialize() {
        super.initialize();
    }

    public void update() {
        applyCameraPosition();
        applyDefaultCellPosition();
    }

    public void draw(GL10 gl) {
        gl.glLoadMatrixf(this.currentMatrix, 0);
        drawShape(gl);
        gl.glLoadIdentity();
    }

    public void loadContent() {
        TexturesManager.getInstance().addTexture(new Texture2D(getResourceId(), Texture2D.TextureFilter.Linear, Texture2D.TextureFilter.Linear, Texture2D.TextureWrap.ClampToEdge, Texture2D.TextureWrap.ClampToEdge));
    }

    /* access modifiers changed from: protected */
    public void drawShape(GL10 gl) {
        gl.glDrawArrays(5, 0, 4);
        gl.glDrawArrays(5, 4, 4);
        gl.glDrawArrays(5, 8, 4);
        gl.glDrawArrays(5, 12, 4);
        gl.glDrawArrays(5, 16, 4);
        gl.glDrawArrays(5, 20, 4);
    }

    /* access modifiers changed from: protected */
    public void applyCameraPosition() {
        System.arraycopy(GameCamera.getInstance().getMatrix(), 0, this.currentMatrix, 0, 16);
    }

    /* access modifiers changed from: protected */
    public void applyDefaultCellPosition() {
        Matrix.translateM(this.currentMatrix, 0, this.cell.coordinate.x, this.cell.coordinate.y - 5.92f, this.cell.coordinate.z);
    }

    public void refreshTextures(HashMap<Integer, String> currentTexturesNames) {
        TexturesManager tm = TexturesManager.getInstance();
        String top = currentTexturesNames.get(RollItConstants.FACE_TOP);
        Rect topFaceRectOnTexture = tm.getRectByFrameName(top);
        Rect bottomFaceRectOnTexture = tm.getRectByFrameName(currentTexturesNames.get(RollItConstants.FACE_BOTTOM));
        Rect sideFaceRectOnTexture = tm.getRectByFrameName(currentTexturesNames.get(RollItConstants.FACE_SIDE));
        if (topFaceRectOnTexture == null) {
            topFaceRectOnTexture = new Rect(0, 0, 20, 20);
            Log.w("Texture refresh", String.valueOf(getClass().getSimpleName()) + " WRONG TEXTURE TOP RECTANGLE NAME: " + top + "; default Rect (0,0,20,20) is set");
        }
        if (bottomFaceRectOnTexture == null) {
            bottomFaceRectOnTexture = new Rect(0, 0, 20, 20);
            Log.e("Texture refresh", String.valueOf(getClass().getSimpleName()) + " WRONG TEXTURE BOTTOM RECTANGLE NAME: " + top + "; default Rect (0,0,20,20) is set");
        }
        if (sideFaceRectOnTexture == null) {
            sideFaceRectOnTexture = new Rect(0, 0, 20, 20);
            Log.e("Texture refresh", String.valueOf(getClass().getSimpleName()) + " WRONG TEXTURE SIDE RECTANGLE NAME: " + top + "; default Rect (0,0,20,20) is set");
        }
        int textureHeight = 4096;
        int textureWidth = 4096;
        Texture2D currentTexture = tm.getTextureByResourceId(getResourceId());
        if (currentTexture != null) {
            textureWidth = currentTexture.getWidth();
            textureHeight = currentTexture.getHeight();
        } else {
            Log.w("Texture refresh", "CellSprite: current texture IS NULL, default texture sizes are set");
        }
        this.topT = ((float) (topFaceRectOnTexture.top + 1)) / ((float) textureHeight);
        this.topB = ((float) (topFaceRectOnTexture.bottom - 1)) / ((float) textureHeight);
        this.topL = ((float) (topFaceRectOnTexture.left + 1)) / ((float) textureWidth);
        this.topR = ((float) (topFaceRectOnTexture.right - 1)) / ((float) textureWidth);
        this.bottomT = ((float) (bottomFaceRectOnTexture.top + 1)) / ((float) textureHeight);
        this.bottomB = ((float) (bottomFaceRectOnTexture.bottom - 1)) / ((float) textureHeight);
        this.bottomL = ((float) (bottomFaceRectOnTexture.left + 1)) / ((float) textureWidth);
        this.bottomR = ((float) (bottomFaceRectOnTexture.right - 1)) / ((float) textureWidth);
        this.sideT = ((float) (sideFaceRectOnTexture.top + 1)) / ((float) textureHeight);
        this.sideB = ((float) (sideFaceRectOnTexture.bottom - 1)) / ((float) textureHeight);
        this.sideL = ((float) (sideFaceRectOnTexture.left + 1)) / ((float) textureWidth);
        this.sideR = ((float) (sideFaceRectOnTexture.right - 1)) / ((float) textureWidth);
        refreshTextureBuffer();
    }

    /* access modifiers changed from: protected */
    public void refreshTextureBuffer() {
        this.texCoords = new float[]{this.topL, this.topB, this.topR, this.topB, this.topL, this.topT, this.topR, this.topT, this.bottomL, this.bottomT, this.bottomL, this.bottomB, this.bottomR, this.bottomT, this.bottomR, this.bottomB, this.sideR, this.sideB, this.sideR, this.sideT, this.sideL, this.sideB, this.sideL, this.sideT, this.sideR, this.sideB, this.sideR, this.sideT, this.sideL, this.sideB, this.sideL, this.sideT, this.sideR, this.sideB, this.sideR, this.sideT, this.sideL, this.sideB, this.sideL, this.sideT, this.sideR, this.sideB, this.sideR, this.sideT, this.sideL, this.sideB, this.sideL, this.sideT};
        this.texBuff.clear();
        this.texBuff.put(this.texCoords);
        this.texBuff.position(0);
    }

    public FloatBuffer getTextureCoordsBuffer() {
        return this.texBuff;
    }
}
