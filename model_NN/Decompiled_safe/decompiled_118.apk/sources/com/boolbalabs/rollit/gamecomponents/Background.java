package com.boolbalabs.rollit.gamecomponents;

import android.graphics.Rect;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.R;
import javax.microedition.khronos.opengles.GL10;

public class Background extends ZNode {
    private BackgroundView backgroundView = new BackgroundView(R.drawable.rc_gameplay_bg_texture);
    private final int bgRef = R.drawable.rc_gameplay_bg_texture;

    private class BackgroundView extends ZNode {
        private Rect cropRectOnTexture = new Rect(0, 0, ScreenMetrics.screenLargeSidePix, ScreenMetrics.screenSmallSidePix);
        private Rect rectOnScreen = new Rect(0, 0, ScreenMetrics.screenWidthRip, ScreenMetrics.screenHeightRip);

        public BackgroundView(int resourceId) {
            super(R.drawable.rc_gameplay_bg_texture, 0);
            setZGL(1.0f);
        }

        public void initialize() {
            super.initialize();
            initWithFrame(this.rectOnScreen, this.cropRectOnTexture);
        }
    }

    public Background() {
        super(-1, 0);
        addChild(this.backgroundView);
    }

    public void update() {
    }

    public void loadContent() {
        super.loadContent();
    }

    public void drawNode(GL10 gl) {
    }
}
