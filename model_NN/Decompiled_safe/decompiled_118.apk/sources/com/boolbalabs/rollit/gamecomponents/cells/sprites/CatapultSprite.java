package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import android.opengl.Matrix;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.ExtendedMatrix;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class CatapultSprite extends AnimatedCellSprite {
    private static final float ANIMATION_LENGTH_COEFF = 2.0f;
    private static final float CALAPULT_ANGLE_COEFF = 0.5f;
    private static final float CALAPULT_Y_SHIFT = 0.2f;
    private static final float[] calapultBox = {-0.45f, h, 0.45f, 0.45f, h, 0.45f, -0.45f, h, -0.45f, 0.45f, h, -0.45f, -0.45f, -0.08f, 0.45f, -0.45f, -0.08f, -0.45f, 0.45f, -0.08f, 0.45f, 0.45f, -0.08f, -0.45f, 0.45f, -0.08f, 0.45f, 0.45f, h, 0.45f, -0.45f, -0.08f, 0.45f, -0.45f, h, 0.45f, -0.45f, -0.08f, -0.45f, -0.45f, h, -0.45f, 0.45f, -0.08f, -0.45f, 0.45f, h, -0.45f, -0.45f, -0.08f, 0.45f, -0.45f, h, 0.45f, -0.45f, -0.08f, -0.45f, -0.45f, h, -0.45f, 0.45f, -0.08f, -0.45f, 0.45f, h, -0.45f, 0.45f, -0.08f, 0.45f, 0.45f, h, 0.45f};
    private static FloatBuffer catapultBuffer = BufferUtils.makeFloatBuffer(calapultBox);
    private static final float d = 0.45f;
    private static final float h = 0.08f;
    private static final float w = 0.45f;
    private int direction = RollItConstants.DIRECTION_NONE;
    private int throwSound = R.raw.catapult_throws;

    public CatapultSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
        this.direction = cell.direction;
        this.animationLength = 800;
        this.totalRotation = 45.0f;
    }

    public void reinitialize() {
        this.direction = this.cell.direction;
    }

    public void draw(GL10 gl) {
        gl.glVertexPointer(3, 5126, 0, catapultBuffer);
        gl.glNormalPointer(5126, 0, CellSprite.normalsBuff);
        gl.glTexCoordPointer(2, 5126, 0, this.texBuff);
        super.draw(gl);
    }

    public void startMovement() {
        super.startMovement();
    }

    /* access modifiers changed from: protected */
    public void applyAnimation() {
        switch (this.direction) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                Matrix.translateM(this.currentMatrix, 0, 0.0f, 0.0f, -0.45f);
                ExtendedMatrix.rotateM(this.currentMatrix, 0, this.xrot, 1.0f, 0.0f, 0.0f);
                Matrix.translateM(this.currentMatrix, 0, 0.0f, 0.0f, 0.45f);
                return;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                Matrix.translateM(this.currentMatrix, 0, 0.0f, 0.0f, 0.45f);
                ExtendedMatrix.rotateM(this.currentMatrix, 0, this.xrot, 1.0f, 0.0f, 0.0f);
                Matrix.translateM(this.currentMatrix, 0, 0.0f, 0.0f, -0.45f);
                return;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                Matrix.translateM(this.currentMatrix, 0, 0.45f, 0.0f, 0.0f);
                ExtendedMatrix.rotateM(this.currentMatrix, 0, this.zrot, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(this.currentMatrix, 0, -0.45f, 0.0f, 0.0f);
                return;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                Matrix.translateM(this.currentMatrix, 0, -0.45f, 0.0f, 0.0f);
                ExtendedMatrix.rotateM(this.currentMatrix, 0, this.zrot, 0.0f, 0.0f, 1.0f);
                Matrix.translateM(this.currentMatrix, 0, 0.45f, 0.0f, 0.0f);
                return;
            default:
                return;
        }
    }

    public void loadContent() {
        TexturesManager.getInstance().addTexture(new Texture2D(getResourceId(), Texture2D.TextureFilter.Linear, Texture2D.TextureFilter.Linear, Texture2D.TextureWrap.ClampToEdge, Texture2D.TextureWrap.ClampToEdge));
    }

    /* access modifiers changed from: protected */
    public void applyDefaultCellPosition() {
        Matrix.translateM(this.currentMatrix, 0, this.cell.coordinate.x, (this.cell.coordinate.y - 5.92f) + CALAPULT_Y_SHIFT, this.cell.coordinate.z);
    }

    /* access modifiers changed from: protected */
    public void calculateAnimation() {
        switch (this.direction) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                calculateThrowNorth();
                return;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                calculateThrowSouth();
                return;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                calculateThrowEast();
                return;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                calculateThrowWest();
                return;
            default:
                return;
        }
    }

    private void calculateThrowWest() {
        if (this.progress < 0.25f) {
            this.zrot = this.totalRotation * 4.0f * this.progress;
        } else if (this.progress < CALAPULT_ANGLE_COEFF) {
            this.zrot = this.totalRotation;
        } else {
            this.zrot = this.totalRotation * 2.0f * (1.0f - this.progress);
        }
    }

    private void calculateThrowEast() {
        if (this.progress < 0.25f) {
            this.zrot = (-this.totalRotation) * 4.0f * this.progress;
        } else if (this.progress < CALAPULT_ANGLE_COEFF) {
            this.zrot = -this.totalRotation;
        } else {
            this.zrot = (-this.totalRotation) * 2.0f * (1.0f - this.progress);
        }
    }

    private void calculateThrowSouth() {
        if (this.progress < 0.25f) {
            this.xrot = this.totalRotation * 4.0f * this.progress;
        } else if (this.progress < CALAPULT_ANGLE_COEFF) {
            this.xrot = this.totalRotation;
        } else {
            this.xrot = this.totalRotation * 2.0f * (1.0f - this.progress);
        }
    }

    private void calculateThrowNorth() {
        if (this.progress < 0.25f) {
            this.xrot = (-this.totalRotation) * 4.0f * this.progress;
        } else if (this.progress < CALAPULT_ANGLE_COEFF) {
            this.xrot = -this.totalRotation;
        } else {
            this.xrot = (-this.totalRotation) * 2.0f * (1.0f - this.progress);
        }
    }

    /* access modifiers changed from: protected */
    public void playAppropriateSound() {
        playSound(this.throwSound, ZageCommonSettings.soundEnabled);
    }
}
