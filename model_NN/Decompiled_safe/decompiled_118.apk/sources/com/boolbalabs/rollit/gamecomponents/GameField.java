package com.boolbalabs.rollit.gamecomponents;

import com.boolbalabs.lib.game.ActionPerformer;
import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.gamecomponents.cells.EmptyCell;
import com.boolbalabs.rollit.gamecomponents.cells.NormalCell;
import com.boolbalabs.rollit.gamecomponents.cells.managers.BridgeCellManager;
import com.boolbalabs.rollit.gamecomponents.cells.managers.ButtonCellManager;
import com.boolbalabs.rollit.gamecomponents.cells.managers.CatapultCellManager;
import com.boolbalabs.rollit.gamecomponents.cells.managers.CellManager;
import com.boolbalabs.rollit.gamecomponents.cells.managers.FallOnStepCellManager;
import com.boolbalabs.rollit.gamecomponents.cells.managers.FinishCellManager;
import com.boolbalabs.rollit.gamecomponents.cells.managers.NormalCellManager;
import com.boolbalabs.rollit.gamecomponents.cells.managers.ShiftCellManager;
import com.boolbalabs.rollit.gamecomponents.cells.managers.TeleportCellManager;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.CellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.Point3D;
import com.inmobi.androidsdk.impl.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import javax.microedition.khronos.opengles.GL10;

public class GameField extends ZNode {
    public static ActionPerformer actionPerformer;
    private static GameField instance = null;
    private final int FIELD_DEPTH = 10;
    public final int FIELD_SIZE = Constants.HTTP_SUCCESS_CODE;
    private final int FIELD_WIDTH = 20;
    private final int NUMBER_OF_MANAGERS;
    private final ArrayList<CellManager> cellManagers = new ArrayList<>();
    private final Cell[] cells = new Cell[Constants.HTTP_SUCCESS_CODE];
    public Point3D start = new Point3D();

    public static GameField getInstance() {
        if (instance == null) {
            instance = new GameField();
        }
        return instance;
    }

    private GameField() {
        super(-1, 0);
        this.cellManagers.add(new NormalCellManager());
        this.cellManagers.add(new ButtonCellManager());
        this.cellManagers.add(new BridgeCellManager());
        this.cellManagers.add(new TeleportCellManager());
        this.cellManagers.add(new FallOnStepCellManager());
        this.cellManagers.add(new FinishCellManager());
        this.cellManagers.add(new ShiftCellManager());
        this.cellManagers.add(new CatapultCellManager());
        this.NUMBER_OF_MANAGERS = this.cellManagers.size();
    }

    public void initialize() {
        for (int i = 0; i < this.NUMBER_OF_MANAGERS; i++) {
            this.cellManagers.get(i).initialize();
        }
    }

    public void register(GameScene gameScene) {
        super.register(gameScene);
        CellManager.setGameScene(gameScene);
    }

    public void update() {
        activateCurrentCell();
        updateCells();
    }

    private void activateCurrentCell() {
        Cell currentCell = getCell(RotatingCube.currentCoordinates);
        if (currentCell != null && currentCell.coordinate.y == RotatingCube.currentCoordinates.y && RotatingCube.currentMovement == 60100) {
            currentCell.activate();
        }
    }

    private void updateCells() {
        for (int i = 0; i < this.NUMBER_OF_MANAGERS; i++) {
            this.cellManagers.get(i).update();
        }
    }

    public void drawNode(GL10 gl) {
        setupGLforDrawingCells();
        for (int i = 0; i < this.NUMBER_OF_MANAGERS; i++) {
            this.cellManagers.get(i).drawAllCells(gl);
        }
        disableNormals(gl);
    }

    private void disableNormals(GL10 gl) {
        gl.glDisableClientState(32885);
    }

    private void setupGLforDrawingCells() {
        GL10 gl = DisplayServiceOpenGL.getInstance().gl;
        gl.glBindTexture(3553, NormalCell.getInstance().getTextureGlobalIndex());
        gl.glVertexPointer(3, 5126, 0, CellSprite.cellBuff);
        gl.glNormalPointer(5126, 0, CellSprite.normalsBuff);
        gl.glEnableClientState(32884);
        gl.glEnableClientState(32888);
        gl.glEnableClientState(32885);
    }

    public void loadContent() {
        for (int i = 0; i < this.NUMBER_OF_MANAGERS; i++) {
            this.cellManagers.get(i).loadContent();
        }
    }

    public void unloadContent() {
        for (int i = 0; i < this.NUMBER_OF_MANAGERS; i++) {
            this.cellManagers.get(i).unloadContent();
        }
    }

    public void add(int x, int y, int z, int cellType) {
        add(x, y, z, 0, 0, cellType);
    }

    public void add(int x, int y, int z, int arg1, int cellType) {
        add(x, y, z, arg1, 0, cellType);
    }

    public void add(int x, int y, int z, int arg1, int arg2, int cellType) {
        Cell cell = null;
        int i = 0;
        while (i < this.NUMBER_OF_MANAGERS && (cell = this.cellManagers.get(i).addCell(x, y, z, arg1, arg2, cellType)) == null) {
            i++;
        }
        if (!(this.cells[(z * 20) + x] instanceof EmptyCell)) {
            throw new IllegalArgumentException("LEVEL ERROR! Replacing not empty cell at (" + x + ", " + z + ")");
        }
        this.cells[(z * 20) + x] = cell;
    }

    public Cell getCell(int x, int z) {
        if (!outOfField(x, z)) {
            Cell cell = this.cells[(z * 20) + x];
            if (cell instanceof NormalCell) {
                return ((NormalCell) cell).getCell(x, z);
            }
            return cell;
        }
        Cell cell2 = EmptyCell.getInstance();
        cell2.coordinate.set((float) x, 0.0f, (float) z);
        return cell2;
    }

    public Cell getCell(Point3D coords) {
        return getCell((int) coords.x, (int) coords.z);
    }

    public Cell getTargetCell(Point3D coords, int direction, int movementLength) {
        int dz = 0;
        int dx = 0;
        switch (direction) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                dz = -movementLength;
                break;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                dz = movementLength;
                break;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                dx = movementLength;
                break;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                dx = -movementLength;
                break;
        }
        return getCell(((int) coords.x) + dx, ((int) coords.z) + dz);
    }

    public Cell getCurrentCell() {
        return getCell(RotatingCube.currentCoordinates);
    }

    private boolean outOfField(int x, int z) {
        return (z * 20) + x < 0 || (z * 20) + x > 200 || (this.cells[(z * 20) + x] instanceof EmptyCell);
    }

    public void cleanUpLevel() {
        for (int i = 0; i < this.NUMBER_OF_MANAGERS; i++) {
            this.cellManagers.get(i).cleanUp();
        }
        cleanUp();
    }

    public void onLevelRestart() {
        for (int i = 0; i < this.NUMBER_OF_MANAGERS; i++) {
            this.cellManagers.get(i).onLevelRestart();
        }
    }

    public static void setActionPerformer(ActionPerformer performer) {
        actionPerformer = performer;
    }

    private void cleanUp() {
        Arrays.fill(this.cells, EmptyCell.getInstance());
        getChildren().clear();
    }
}
