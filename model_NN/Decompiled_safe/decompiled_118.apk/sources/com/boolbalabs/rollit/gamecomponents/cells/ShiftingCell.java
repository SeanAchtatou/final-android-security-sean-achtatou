package com.boolbalabs.rollit.gamecomponents.cells;

import android.os.Bundle;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.ShiftingCellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.GameStatic;

public class ShiftingCell extends Cell {
    private static boolean active = false;
    private final Bundle shiftingCellBundle = GameStatic.makeBundle();

    public ShiftingCell(int x, int y, int z, int shiftDirection) {
        this.coordinate.set((float) x, (float) y, (float) z);
        this.direction = shiftDirection;
        setAppropriateTexturesNames(shiftDirection);
        this.cellTextureRef = R.drawable.rc_gameplay_texture_grass;
        this.cellSprite = new ShiftingCellSprite(this.cellTextureRef, this);
        addChild(this.cellSprite);
    }

    /* access modifiers changed from: protected */
    public void setAppropriateTexturesNames(int shiftDirection) {
        this.allPossibleTexturesNames.put(RollItConstants.FACE_SIDE, "normal_cell_side.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_BOTTOM, "normal_cell_bottom.png");
        switch (shiftDirection) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "shift_north_cell_top.png");
                return;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "shift_south_cell_top.png");
                return;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "shift_east_cell_top.png");
                return;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "shift_west_cell_top.png");
                return;
            default:
                return;
        }
    }

    public void reinitialize(int x, int y, int z, int shiftDirection, int arg2) {
        if (this.direction != shiftDirection) {
            this.direction = shiftDirection;
            setAppropriateTexturesNames(shiftDirection);
            refreshTextures();
            this.cellSprite.reinitialize();
        }
        super.reinitialize(x, y, z, shiftDirection, arg2);
    }

    public void activate() {
        if (!active) {
            this.shiftingCellBundle.putInt(RollItConstants.KEY_DIRECTION, this.direction);
            this.shiftingCellBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_SHIFT);
            performAction(RollItConstants.ACTION_SHIFT, this.shiftingCellBundle);
            active = true;
        }
    }

    public static void deactivate() {
        active = false;
    }

    public void onLevelRestart() {
        deactivate();
    }
}
