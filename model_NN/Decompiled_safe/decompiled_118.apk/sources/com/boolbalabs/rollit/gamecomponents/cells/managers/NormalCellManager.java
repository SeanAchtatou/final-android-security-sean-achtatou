package com.boolbalabs.rollit.gamecomponents.cells.managers;

import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.gamecomponents.cells.NormalCell;

public class NormalCellManager extends CellManager {
    private NormalCell instance;

    public NormalCellManager() {
        this.cellType = 1;
    }

    public Cell addCell(int x, int y, int z, int arg1, int arg2, int cellType) {
        if (this.cellType != cellType) {
            return null;
        }
        if (this.instance != null) {
            this.instance.makeCell(x, y, z);
            this.instance.refreshTextures();
        } else {
            this.instance = NormalCell.getInstance();
            this.instance.makeCell(x, y, z);
            this.cells.add(this.instance);
        }
        this.numberOfCellsCreated = 1;
        return this.instance;
    }
}
