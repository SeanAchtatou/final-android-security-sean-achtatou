package com.boolbalabs.rollit.gamecomponents.two_d;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.NumberView2D;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.R;
import javax.microedition.khronos.opengles.GL10;

public class ScoreBoard extends ZNode {
    private static final int DIGIT_HEIGHT = 24;
    private final int SKIP_FROM_TOP = 2;
    private Point bestFixedPointOnScreenRip = new Point(85, 2);
    private NumberView2D bestNumberView;
    private Rect bestRectOnScreenRip = new Rect(4, 2, 55, 26);
    private ZNode bestTextView;
    String[] digitsFramesNames = {"gameplay_digit_0.png", "gameplay_digit_1.png", "gameplay_digit_2.png", "gameplay_digit_3.png", "gameplay_digit_4.png", "gameplay_digit_5.png", "gameplay_digit_6.png", "gameplay_digit_7.png", "gameplay_digit_8.png", "gameplay_digit_9.png"};
    private Point levelFixedPointOnScreenRip = new Point((((ScreenMetrics.screenWidthRip / 2) - 20) + 9) + 10, 2);
    private NumberView2D levelNumberView;
    private Rect levelRectOnScreenRip = new Rect((ScreenMetrics.screenWidthRip / 2) - 64, 2, ((ScreenMetrics.screenWidthRip / 2) - 64) + 54, 26);
    private ZNode levelTextView;
    private final int scoreRef = R.drawable.rc_common_texture;
    private Point stepsFixedPointOnScreenRip = new Point(85, 32);
    private NumberView2D stepsNumberView;
    private Rect stepsRectOnScreenRip = new Rect(4, 32, 67, 56);
    private ZNode stepsTextView;

    public ScoreBoard() {
        super(-1, 0);
        createTextViews();
        createNumberViews();
    }

    public void initialize() {
        initNumberViews();
        initTextViews();
        super.initialize();
    }

    private void createNumberViews() {
        this.bestNumberView = new NumberView2D(R.drawable.rc_common_texture);
        addChild(this.bestNumberView, false);
        this.bestNumberView.setNumberToDraw(0);
        this.levelNumberView = new NumberView2D(R.drawable.rc_common_texture);
        addChild(this.levelNumberView, false);
        this.levelNumberView.setNumberToDraw(0);
        this.stepsNumberView = new NumberView2D(R.drawable.rc_common_texture);
        addChild(this.stepsNumberView);
        this.stepsNumberView.setNumberToDraw(0);
    }

    private void createTextViews() {
        this.bestTextView = new ZNode(R.drawable.rc_common_texture, 0);
        addChild(this.bestTextView, false);
        this.stepsTextView = new ZNode(R.drawable.rc_common_texture, 0);
        addChild(this.stepsTextView, false);
        this.levelTextView = new ZNode(R.drawable.rc_common_texture, 0);
        addChild(this.levelTextView, false);
    }

    private void initNumberViews() {
        this.bestNumberView.initNumberView(this.bestFixedPointOnScreenRip, this.digitsFramesNames, NumberView2D.ALIGN_LEFT, 24);
        this.levelNumberView.initNumberView(this.levelFixedPointOnScreenRip, this.digitsFramesNames, NumberView2D.ALIGN_LEFT, 24);
        this.stepsNumberView.initNumberView(this.stepsFixedPointOnScreenRip, this.digitsFramesNames, NumberView2D.ALIGN_LEFT, 24);
    }

    private void initTextViews() {
        TexturesManager tm = TexturesManager.getInstance();
        this.bestTextView.initWithFrame(this.bestRectOnScreenRip, tm.getRectByFrameName("gameplay_label_best.png"));
        this.stepsTextView.initWithFrame(this.stepsRectOnScreenRip, tm.getRectByFrameName("gameplay_label_steps.png"));
        this.levelTextView.initWithFrame(this.levelRectOnScreenRip, tm.getRectByFrameName("gameplay_label_level.png"));
    }

    public void resetScore() {
        this.stepsNumberView.setNumberToDraw(0);
    }

    public void increaseStepsNumber() {
        this.stepsNumberView.increaseNumberToDrawBy(1);
    }

    public void onLevelRestart() {
        this.stepsNumberView.setNumberToDraw(0);
    }

    public void initValues(int levelNumber, int bestSteps) {
        this.levelNumberView.setNumberToDraw(levelNumber);
        this.bestNumberView.setNumberToDraw(bestSteps);
    }

    public int getStepNumber() {
        return this.stepsNumberView.getRealValue();
    }

    public void drawNode(GL10 gl) {
        super.drawNode(gl);
    }
}
