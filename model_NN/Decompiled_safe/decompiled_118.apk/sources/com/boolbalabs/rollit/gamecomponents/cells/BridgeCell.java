package com.boolbalabs.rollit.gamecomponents.cells;

import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.BridgeCellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;

public class BridgeCell extends Cell {
    private boolean inAir = false;
    private int initialState = RollItConstants.BRIDGE_CELL_DOWN;

    public BridgeCell(int x, int y, int z, int initialState2) {
        this.coordinate.set((float) x, (float) y, (float) z);
        this.initialState = initialState2 == 22300 ? 22300 : 22301;
        resetToInitialState();
        resetCoordinate();
        setAppropriateTexturesNames(0);
        this.cellTextureRef = R.drawable.rc_gameplay_texture_grass;
        this.cellSprite = new BridgeCellSprite(this.cellTextureRef, this);
        this.cellSprite.reinitialize();
        addChild(this.cellSprite);
    }

    public void reinitialize(int x, int y, int z, int initialState2, int arg2) {
        super.reinitialize(x, y, z, initialState2, arg2);
        this.initialState = initialState2;
        resetToInitialState();
        resetCoordinate();
        this.cellSprite.reinitialize();
    }

    /* access modifiers changed from: protected */
    public void setAppropriateTexturesNames(int condition) {
        this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "bridge_cell_top.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_SIDE, "bridge_cell_side.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_BOTTOM, "bridge_cell_top.png");
    }

    public void onLevelRestart() {
        boolean oldInAir = this.inAir;
        resetToInitialState();
        if (oldInAir != this.inAir) {
            ((BridgeCellSprite) this.cellSprite).mustPerformAnimation = true;
        }
        ((BridgeCellSprite) this.cellSprite).onLevelRestart();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public void switchState() {
        this.inAir = !this.inAir;
        ((BridgeCellSprite) this.cellSprite).startMovement();
    }

    private void resetToInitialState() {
        if (this.initialState == 22300) {
            this.inAir = true;
        } else {
            this.inAir = false;
        }
    }

    private void resetCoordinate() {
        if (this.initialState == 22300) {
            this.coordinate.y = 2.0f;
        } else {
            this.coordinate.y = 0.0f;
        }
    }

    public boolean isInAir() {
        return this.inAir;
    }
}
