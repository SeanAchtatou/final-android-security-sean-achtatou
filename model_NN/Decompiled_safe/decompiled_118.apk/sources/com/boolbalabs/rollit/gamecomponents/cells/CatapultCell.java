package com.boolbalabs.rollit.gamecomponents.cells;

import android.os.Bundle;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.SceneGameplay;
import com.boolbalabs.rollit.gamecomponents.GameField;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.ArrowTrajectorySprite;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.CatapultCellSprite;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.CatapultSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.GameStatic;
import com.boolbalabs.utility.Point3D;
import java.util.Arrays;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;

public class CatapultCell extends Cell {
    private static final int THROW_STRENGTH_INCREMENT = 3;
    private static boolean activated = false;
    private float a;
    HashMap<Integer, String> allPossibleArrowTexturesNames = new HashMap<>();
    HashMap<Integer, String> allPossibleCatapultTexturesNames = new HashMap<>();
    private final ArrowTrajectorySprite arrowSprite;
    private float b;
    private boolean bundleHasToBeRepacked = true;
    private float c;
    private final Bundle catapultCellBundle = GameStatic.makeBundle();
    private final CatapultSprite catapultSprite;
    private final float[] coefficients = new float[3];
    private int currentThrowStrength;
    private int defaultThrowStrength;
    private final Point3D relativeEnd = new Point3D();
    private final Point3D relativeMiddle = new Point3D();
    private final Point3D targetCellCoords = new Point3D();
    private final Point3D tempPoint = new Point3D();
    private final Point3D throwVector = new Point3D();

    public CatapultCell(int x, int y, int z, int throwDirection, int throwStrength) {
        this.coordinate.set((float) x, (float) y, (float) z);
        this.direction = throwDirection;
        this.defaultThrowStrength = throwStrength;
        this.currentThrowStrength = this.defaultThrowStrength;
        this.cellTextureRef = R.drawable.rc_gameplay_texture_grass;
        setAppropriateTexturesNames(throwDirection);
        this.cellSprite = new CatapultCellSprite(this.cellTextureRef, this);
        addChild(this.cellSprite);
        this.catapultSprite = new CatapultSprite(this.cellTextureRef, this);
        addChild(this.catapultSprite);
        this.arrowSprite = new ArrowTrajectorySprite(this.cellTextureRef, this);
        addChild(this.arrowSprite);
    }

    private void setAppropriateCellTextureNames(int i) {
        this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "normal_cell_top.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_BOTTOM, "normal_cell_bottom.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_SIDE, "normal_cell_side.png");
    }

    private void setAppropriateArrowTextureNames(int i) {
        this.allPossibleArrowTexturesNames.put(RollItConstants.FACE_TOP, "catapult_arrow_color.png");
        this.allPossibleArrowTexturesNames.put(RollItConstants.FACE_BOTTOM, "catapult_arrow_color.png");
        this.allPossibleArrowTexturesNames.put(RollItConstants.FACE_SIDE, "catapult_arrow_color.png");
    }

    private void setAppropriateCatapultTextureNames(int throwDirection) {
        switch (throwDirection) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                this.allPossibleCatapultTexturesNames.put(RollItConstants.FACE_TOP, "catapult_north_top.png");
                break;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                this.allPossibleCatapultTexturesNames.put(RollItConstants.FACE_TOP, "catapult_south_top.png");
                break;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                this.allPossibleCatapultTexturesNames.put(RollItConstants.FACE_TOP, "catapult_east_top.png");
                break;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                this.allPossibleCatapultTexturesNames.put(RollItConstants.FACE_TOP, "catapult_west_top.png");
                break;
        }
        this.allPossibleCatapultTexturesNames.put(RollItConstants.FACE_BOTTOM, "normal_cell_top.png");
        this.allPossibleCatapultTexturesNames.put(RollItConstants.FACE_SIDE, "catapult_side.png");
    }

    public void initialize() {
        updateTrajectory();
        super.initialize();
    }

    public void reinitialize(int x, int y, int z, int throwDirection, int throwStrength) {
        this.defaultThrowStrength = throwStrength;
        this.currentThrowStrength = this.defaultThrowStrength;
        if (this.direction != throwDirection) {
            this.direction = throwDirection;
            this.catapultSprite.reinitialize();
        }
        super.reinitialize(x, y, z, throwDirection, throwStrength);
    }

    public void updateTrajectory() {
        findTargetCellCoordinates();
        setThrowVector();
        updateParabolaCoefficients();
    }

    private void findTargetCellCoordinates() {
        Cell targetCell = GameField.getInstance().getTargetCell(this.coordinate, this.direction, this.currentThrowStrength);
        this.tempPoint.set(targetCell.coordinate);
        if (targetCell instanceof EmptyCell) {
            this.tempPoint.set(this.tempPoint.x, GameField.getInstance().getTargetCell(this.coordinate, this.direction, this.currentThrowStrength - 1).coordinate.y, this.tempPoint.z);
        }
        this.targetCellCoords.set(this.tempPoint);
    }

    private void setThrowVector() {
        this.throwVector.set(this.targetCellCoords.x - this.coordinate.x, this.targetCellCoords.y - this.coordinate.y, this.targetCellCoords.z - this.coordinate.z);
    }

    public void activate() {
        if (!activated) {
            if (this.bundleHasToBeRepacked) {
                repackBundle();
            }
            performAction(RollItConstants.ACTION_THROW, this.catapultCellBundle);
            activated = true;
            this.catapultSprite.startMovement();
        }
    }

    private void repackBundle() {
        this.catapultCellBundle.putInt(RollItConstants.KEY_DIRECTION, this.direction);
        this.catapultCellBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_THROW);
        this.catapultCellBundle.putSerializable(RollItConstants.KEY_THROW_VECTOR, this.throwVector);
        this.catapultCellBundle.putSerializable(RollItConstants.KEY_TARGET_COORDS, this.targetCellCoords);
        this.catapultCellBundle.putFloatArray(RollItConstants.KEY_PARABOLA_COEFFS, this.coefficients);
        this.bundleHasToBeRepacked = false;
    }

    public static void deactivate() {
        activated = false;
    }

    public void draw(GL10 gl) {
        this.cellSprite.draw(gl);
        this.catapultSprite.draw(gl);
        this.arrowSprite.draw(gl);
    }

    public void switchState() {
        this.currentThrowStrength++;
        if (this.currentThrowStrength == this.defaultThrowStrength + 3) {
            this.currentThrowStrength = this.defaultThrowStrength;
        }
        updateTrajectory();
        this.arrowSprite.refreshTrajectory();
        this.arrowSprite.startMovement();
        this.bundleHasToBeRepacked = true;
    }

    private void findParabolaCoefficients(Point3D middle, Point3D end) {
        if (this.direction == 62104 || this.direction == 62103) {
            this.a = (end.y - ((end.x * middle.y) / middle.x)) / (end.x * (end.x - middle.x));
            this.b = (middle.y / middle.x) - (this.a * middle.x);
            this.c = 0.0f;
        } else {
            this.a = (end.y - ((end.z * middle.y) / middle.z)) / (end.z * (end.z - middle.z));
            this.b = (middle.y / middle.z) - (this.a * middle.z);
            this.c = 0.0f;
        }
        this.coefficients[0] = this.a;
        this.coefficients[1] = this.b;
        this.coefficients[2] = this.c;
    }

    private void updateParabolaCoefficients() {
        this.relativeEnd.set(this.throwVector);
        this.relativeMiddle.set(this.throwVector.x / 2.0f, (this.throwVector.y / 2.0f) + (GameStatic.radiusVectorLength3D(this.relativeEnd) / 2.0f), this.throwVector.z / 2.0f);
        findParabolaCoefficients(this.relativeMiddle, this.relativeEnd);
    }

    public float[] getParabolaCoeffs() {
        return this.coefficients;
    }

    public void onLevelRestart() {
        activated = false;
        this.currentThrowStrength = this.defaultThrowStrength;
        this.bundleHasToBeRepacked = true;
        updateTrajectory();
        this.arrowSprite.onLevelRestart();
    }

    public int getDirection() {
        return this.direction;
    }

    public int getThrowStrength() {
        return this.currentThrowStrength;
    }

    public Point3D getThrowVector() {
        return this.throwVector;
    }

    public void refreshTextures() {
        setAppropriateTexturesNames(this.direction);
        this.currentTexturesNames.put(RollItConstants.FACE_TOP, String.valueOf(SceneGameplay.themePrefix) + ((String) this.allPossibleTexturesNames.get(RollItConstants.FACE_TOP)));
        this.currentTexturesNames.put(RollItConstants.FACE_BOTTOM, String.valueOf(SceneGameplay.themePrefix) + ((String) this.allPossibleTexturesNames.get(RollItConstants.FACE_BOTTOM)));
        this.currentTexturesNames.put(RollItConstants.FACE_SIDE, String.valueOf(SceneGameplay.themePrefix) + ((String) this.allPossibleTexturesNames.get(RollItConstants.FACE_SIDE)));
        this.cellSprite.refreshTextures(this.currentTexturesNames);
        this.currentTexturesNames.put(RollItConstants.FACE_TOP, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleCatapultTexturesNames.get(RollItConstants.FACE_TOP));
        this.currentTexturesNames.put(RollItConstants.FACE_BOTTOM, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleCatapultTexturesNames.get(RollItConstants.FACE_BOTTOM));
        this.currentTexturesNames.put(RollItConstants.FACE_SIDE, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleCatapultTexturesNames.get(RollItConstants.FACE_SIDE));
        this.catapultSprite.refreshTextures(this.currentTexturesNames);
        this.currentTexturesNames.put(RollItConstants.FACE_TOP, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleArrowTexturesNames.get(RollItConstants.FACE_TOP));
        this.currentTexturesNames.put(RollItConstants.FACE_BOTTOM, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleArrowTexturesNames.get(RollItConstants.FACE_BOTTOM));
        this.currentTexturesNames.put(RollItConstants.FACE_SIDE, String.valueOf(SceneGameplay.themePrefix) + this.allPossibleArrowTexturesNames.get(RollItConstants.FACE_SIDE));
        this.arrowSprite.refreshTextures(this.currentTexturesNames);
    }

    /* access modifiers changed from: protected */
    public void setAppropriateTexturesNames(int throwDirection) {
        setAppropriateCellTextureNames(0);
        setAppropriateCatapultTextureNames(throwDirection);
        setAppropriateArrowTextureNames(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(float[], float):void}
     arg types: [float[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void} */
    public void cleanUp() {
        super.cleanUp();
        this.bundleHasToBeRepacked = true;
        this.defaultThrowStrength = 0;
        this.currentThrowStrength = 0;
        this.relativeMiddle.set(0.0f, 0.0f, 0.0f);
        this.relativeEnd.set(0.0f, 0.0f, 0.0f);
        this.throwVector.set(0.0f, 0.0f, 0.0f);
        this.c = 0.0f;
        this.b = 0.0f;
        this.a = 0.0f;
        Arrays.fill(this.coefficients, 0.0f);
    }
}
