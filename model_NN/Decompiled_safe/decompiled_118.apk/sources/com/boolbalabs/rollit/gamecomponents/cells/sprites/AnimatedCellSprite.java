package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import android.opengl.Matrix;
import android.view.animation.Interpolator;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.utility.ExtendedMatrix;

public abstract class AnimatedCellSprite extends CellSprite {
    protected long animationLength;
    protected boolean animationRunning = false;
    private long currentAnimationTime;
    protected Interpolator interpolator;
    private boolean mustStopAnimation = false;
    protected float progress;
    private long startAnimationTime;
    protected float totalMovement;
    protected float totalRotation;
    protected float xmove;
    protected float xrot;
    protected float ymove;
    protected float yrot;
    protected float zmove;
    protected float zrot;

    /* access modifiers changed from: protected */
    public abstract void calculateAnimation();

    public AnimatedCellSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
    }

    public void startMovement() {
        setAnimationRunning(true);
        playAppropriateSound();
        this.startAnimationTime = System.currentTimeMillis();
    }

    public void stopAnimation() {
        this.xmove = 0.0f;
        this.ymove = 0.0f;
        this.zmove = 0.0f;
        this.xrot = 0.0f;
        this.yrot = 0.0f;
        this.zrot = 0.0f;
        setAnimationRunning(false);
    }

    public void update() {
        super.update();
        if (this.animationRunning) {
            animate();
        }
    }

    /* access modifiers changed from: protected */
    public void animate() {
        if (this.mustStopAnimation) {
            stopAnimation();
            this.mustStopAnimation = false;
        } else {
            this.progress = calculateProgress();
            if (this.progress < 1.0f) {
                calculateAnimation();
            } else if (this.progress > 1.0f) {
                this.progress = 1.0f;
                calculateAnimation();
                this.mustStopAnimation = true;
            }
        }
        applyAnimation();
    }

    /* access modifiers changed from: protected */
    public void applyAnimation() {
        Matrix.translateM(this.currentMatrix, 0, this.xmove, this.ymove, this.zmove);
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.xrot, 1.0f, 0.0f, 0.0f);
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.yrot, 0.0f, 1.0f, 0.0f);
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.zrot, 0.0f, 0.0f, 1.0f);
    }

    public boolean animationRunning() {
        return this.animationRunning;
    }

    /* access modifiers changed from: protected */
    public void setAnimationRunning(boolean running) {
        this.animationRunning = running;
    }

    public void onLevelRestart() {
        stopAnimation();
    }

    /* access modifiers changed from: protected */
    public void playAppropriateSound() {
    }

    /* access modifiers changed from: protected */
    public float calculateProgress() {
        this.currentAnimationTime = System.currentTimeMillis() - this.startAnimationTime;
        return ((float) this.currentAnimationTime) / ((float) this.animationLength);
    }
}
