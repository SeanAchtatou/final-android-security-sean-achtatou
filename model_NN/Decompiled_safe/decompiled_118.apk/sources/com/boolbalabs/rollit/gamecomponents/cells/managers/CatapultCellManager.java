package com.boolbalabs.rollit.gamecomponents.cells.managers;

import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import javax.microedition.khronos.opengles.GL10;

public class CatapultCellManager extends CellManager {
    public CatapultCellManager() {
        this.cellType = 4;
    }

    public void drawAllCells(GL10 gl) {
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            ((Cell) this.cells.get(i)).draw(gl);
        }
    }
}
