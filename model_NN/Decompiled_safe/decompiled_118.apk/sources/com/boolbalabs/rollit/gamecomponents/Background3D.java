package com.boolbalabs.rollit.gamecomponents;

import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.rollit.R;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class Background3D extends ZNode {
    private BackgroundView backgroundView = new BackgroundView(R.drawable.rc_gameplay_bg_texture);
    private final int bgRef = R.drawable.rc_gameplay_bg_texture;

    private class BackgroundView extends ZNode {
        private FloatBuffer backgroundBuff;
        private float[] normals;
        private FloatBuffer normalsBuff;
        private float[] spherePoints;
        private FloatBuffer texBuff;
        private float[] texCoords;
        private int vertNumber;

        public BackgroundView(int resourceId) {
            super(R.drawable.rc_gameplay_bg_texture, 0);
            createSphere(0.0f, 0.0f, 0.0f, 1.0f, 8);
        }

        private void createSphere(float cx, float cy, float cz, float r, int p) {
            this.vertNumber = (p / 2) * (p + 1) * 2;
            Vertex[] vertexes = new Vertex[this.vertNumber];
            this.spherePoints = new float[(this.vertNumber * 3)];
            this.texCoords = new float[(this.vertNumber * 2)];
            this.normals = new float[(this.vertNumber * 3)];
            int k = -1;
            for (int i = 0; i < p / 2; i++) {
                float theta1 = ((((float) i) * 6.2831855f) / ((float) p)) - 1.5707964f;
                float theta2 = ((((float) (i + 1)) * 6.2831855f) / ((float) p)) - 1.5707964f;
                for (int j = 0; j <= p; j++) {
                    float theta3 = (((float) j) * 6.2831855f) / ((float) p);
                    float ex = (float) (Math.cos((double) theta2) * Math.cos((double) theta3));
                    float ey = (float) Math.sin((double) theta2);
                    float ez = (float) (Math.cos((double) theta2) * Math.sin((double) theta3));
                    int k2 = k + 1;
                    vertexes[k2] = new Vertex(cx + (r * ex), cy + (r * ey), cz + (r * ez), ex, ey, ez, -(((float) j) / ((float) p)), ((float) ((i + 1) * 2)) / ((float) p));
                    float ex2 = (float) (Math.cos((double) theta1) * Math.cos((double) theta3));
                    float ey2 = (float) Math.sin((double) theta1);
                    float ez2 = (float) (Math.cos((double) theta1) * Math.sin((double) theta3));
                    k = k2 + 1;
                    vertexes[k] = new Vertex(cx + (r * ex2), cy + (r * ey2), cz + (r * ez2), ex2, ey2, ez2, -(((float) j) / ((float) p)), ((float) (i * 2)) / ((float) p));
                }
            }
            fillArraysWithData(vertexes);
            cleanInaccuracies(this.spherePoints);
            this.backgroundBuff = BufferUtils.makeFloatBuffer(this.spherePoints);
            this.texBuff = BufferUtils.makeFloatBuffer(this.texCoords);
            this.normalsBuff = BufferUtils.makeFloatBuffer(this.normals);
        }

        private void fillArraysWithData(Vertex[] vertexes) {
            for (int i = 0; i < this.vertNumber; i++) {
                this.spherePoints[i * 3] = vertexes[i].px;
                this.spherePoints[(i * 3) + 1] = vertexes[i].py;
                this.spherePoints[(i * 3) + 2] = vertexes[i].pz;
                this.normals[i * 3] = vertexes[i].ex;
                this.normals[(i * 3) + 1] = vertexes[i].ey;
                this.normals[(i * 3) + 2] = vertexes[i].ez;
                this.texCoords[i * 2] = vertexes[i].tu;
                this.texCoords[(i * 2) + 1] = vertexes[i].tv;
            }
        }

        private void cleanInaccuracies(float[] array) {
            int size = array.length;
            for (int i = 0; i < size; i++) {
                if (Math.abs(array[i]) < 1.0E-5f) {
                    array[i] = 0.0f;
                }
            }
        }

        public void loadContent() {
            TexturesManager.getInstance().addTexture(new Texture2D(getResourceId(), Texture2D.TextureFilter.Linear, Texture2D.TextureFilter.Linear, Texture2D.TextureWrap.Wrap, Texture2D.TextureWrap.Wrap));
        }

        public void draw(GL10 gl) {
            prepareToDrawBackground(gl);
            drawShape(gl);
            setOldOpenGLProperties(gl);
        }

        private void prepareToDrawBackground(GL10 gl) {
            gl.glBindTexture(3553, getTextureGlobalIndex());
            gl.glVertexPointer(3, 5126, 0, this.backgroundBuff);
            gl.glEnableClientState(32884);
            gl.glTexCoordPointer(2, 5126, 0, this.texBuff);
            gl.glEnableClientState(32888);
            gl.glNormalPointer(5126, 0, this.normalsBuff);
            gl.glCullFace(1029);
            gl.glDisable(2896);
            gl.glEnableClientState(32885);
        }

        private void setOldOpenGLProperties(GL10 gl) {
            gl.glCullFace(1029);
            gl.glEnable(2896);
            gl.glDisableClientState(32885);
        }

        private void drawShape(GL10 gl) {
            gl.glDrawArrays(5, 0, this.vertNumber);
        }

        /* access modifiers changed from: protected */
        public void createTextureBuffer() {
        }
    }

    private class Vertex {
        public float ex;
        public float ey;
        public float ez;
        public float px;
        public float py;
        public float pz;
        public float tu;
        public float tv;

        public Vertex(float px2, float py2, float pz2, float ex2, float ey2, float ez2, float tu2, float tv2) {
            this.px = px2;
            this.py = py2;
            this.pz = pz2;
            this.ex = ex2;
            this.ey = ey2;
            this.ez = ez2;
            this.tu = tu2;
            this.tv = tv2;
        }
    }

    public Background3D() {
        super(-1, 1);
        addChild(this.backgroundView);
    }

    public void update() {
    }

    public void drawNode(GL10 gl) {
        super.drawNode(gl);
    }
}
