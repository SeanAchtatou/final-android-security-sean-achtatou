package com.boolbalabs.rollit.gamecomponents.cells;

import android.os.Bundle;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.GameField;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.TeleportCellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.GameStatic;
import com.boolbalabs.utility.Point3D;

public class TeleportCell extends Cell {
    private static boolean disabled = false;
    private Point3D target = new Point3D();
    private final Bundle teleportBundle = GameStatic.makeBundle();

    public TeleportCell(int x, int y, int z, int targetCellX, int targetCellZ) {
        this.coordinate.set((float) x, (float) y, (float) z);
        this.target.set((float) targetCellX, 0.0f, (float) targetCellZ);
        setAppropriateTexturesNames(0);
        this.cellTextureRef = R.drawable.rc_gameplay_texture_grass;
        this.cellSprite = new TeleportCellSprite(this.cellTextureRef, this);
        addChild(this.cellSprite);
    }

    /* access modifiers changed from: protected */
    public void setAppropriateTexturesNames(int condition) {
        this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "teleport_cell_top.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_SIDE, "normal_cell_side.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_BOTTOM, "normal_cell_bottom.png");
    }

    public void initialize() {
        super.initialize();
        Cell targetCell = GameField.getInstance().getCell((int) this.target.x, (int) this.target.z);
        this.target.y = targetCell.coordinate.y;
    }

    public void reinitialize(int x, int y, int z, int tergetX, int tergetZ) {
        this.target.set((float) tergetX, 0.0f, (float) tergetZ);
        Cell targetCell = GameField.getInstance().getCell((int) this.target.x, (int) this.target.z);
        this.target.y = targetCell.coordinate.y;
        super.reinitialize(x, y, z, tergetX, tergetZ);
    }

    public void activate() {
        if (!disabled) {
            this.teleportBundle.putInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE);
            this.teleportBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_TELEPORT);
            this.teleportBundle.putSerializable(RollItConstants.KEY_TARGET_COORDS, this.target);
            this.teleportBundle.putSerializable(RollItConstants.KEY_CURR_COORDS, this.coordinate);
            performAction(RollItConstants.ACTION_TELEPORT, this.teleportBundle);
            disabled = true;
        }
    }

    public static void enable() {
        disabled = false;
    }

    public void onLevelRestart() {
    }
}
