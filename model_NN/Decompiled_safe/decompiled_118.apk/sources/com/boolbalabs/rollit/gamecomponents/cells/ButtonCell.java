package com.boolbalabs.rollit.gamecomponents.cells;

import android.os.Bundle;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.ButtonCellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.GameStatic;
import com.boolbalabs.utility.Point3D;

public class ButtonCell extends Cell {
    private static boolean disabled = false;
    private final Bundle buttonCellBundle = GameStatic.makeBundle();
    Cell targetCell = null;
    private Point3D targetCellCoords = new Point3D();

    public ButtonCell(int x, int y, int z, int targetCellX, int targetCellZ) {
        this.coordinate.set((float) x, (float) y, (float) z);
        this.targetCellCoords.set((float) targetCellX, 0.0f, (float) targetCellZ);
        setAppropriateTexturesNames(0);
        this.cellTextureRef = R.drawable.rc_gameplay_texture_grass;
        this.cellSprite = new ButtonCellSprite(this.cellTextureRef, this);
        addChild(this.cellSprite);
    }

    /* access modifiers changed from: protected */
    public void setAppropriateTexturesNames(int condition) {
        this.allPossibleTexturesNames.put(RollItConstants.FACE_TOP, "button_cell_top.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_SIDE, "normal_cell_side.png");
        this.allPossibleTexturesNames.put(RollItConstants.FACE_BOTTOM, "normal_cell_bottom.png");
    }

    public void reinitialize(int x, int y, int z, int targetCellX, int targetCellZ) {
        super.reinitialize(x, y, z, targetCellX, targetCellZ);
        this.targetCellCoords.set((float) targetCellX, 0.0f, (float) targetCellZ);
    }

    public void activate() {
        if (!disabled) {
            this.buttonCellBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_PUSH_BUTTON);
            this.buttonCellBundle.putSerializable(RollItConstants.KEY_TARGET_COORDS, this.targetCellCoords);
            this.buttonCellBundle.putInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE);
            performAction(RollItConstants.ACTION_PUSH_BUTTON, this.buttonCellBundle);
            disabled = true;
            ((ButtonCellSprite) this.cellSprite).startMovement();
        }
    }

    public void onLevelRestart() {
        disabled = false;
    }

    public static void enable() {
        disabled = false;
    }
}
