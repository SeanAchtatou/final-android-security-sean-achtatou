package com.boolbalabs.rollit.gamecomponents.cells.managers;

import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import javax.microedition.khronos.opengles.GL10;

public class ShiftCellManager extends CellManager {
    public ShiftCellManager() {
        this.cellType = 6;
    }

    public void drawAllCells(GL10 gl) {
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            gl.glTexCoordPointer(2, 5126, 0, ((Cell) this.cells.get(i)).getCellTextureCoordsBuffer());
            ((Cell) this.cells.get(i)).draw(gl);
        }
    }
}
