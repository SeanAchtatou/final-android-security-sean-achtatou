package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import android.opengl.Matrix;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.rollit.gamecomponents.cells.CatapultCell;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.ExtendedMatrix;
import com.boolbalabs.utility.Point3D;
import java.nio.FloatBuffer;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;

public class ArrowTrajectorySprite extends AnimatedCellSprite {
    private static final float ARROW_LIFT = 2.0f;
    private static final float ARROW_THROW_ANIMATION_LENGTH_COEFF = 0.6f;
    private static final float D = 0.05f;
    private static final float FLIGHT_FLAT_COEFF = 2.0f;
    private static final float HH = 0.3f;
    private static final float HW = 0.2f;
    private static final float START_ANGLE = 135.0f;
    private static final float TH = 0.5f;
    private static final float TW = 0.05f;
    private static final float[] arrowBox = {0.0f, -HH, D, HW, 0.0f, D, TW, 0.0f, D, TW, TH, D, -TW, TH, D, -TW, 0.0f, D, -HW, 0.0f, D, 0.0f, -HH, -D, -HW, 0.0f, -D, -TW, 0.0f, -D, -TW, TH, -D, TW, TH, -D, TW, 0.0f, -D, HW, 0.0f, -D, -TW, TH, D, TW, TH, D, -TW, TH, -D, TW, TH, -D, TW, 0.0f, -D, TW, TH, -D, TW, 0.0f, D, TW, TH, D, -TW, 0.0f, D, -TW, TH, D, -TW, 0.0f, -D, -TW, TH, -D, TW, 0.0f, D, HW, 0.0f, D, TW, 0.0f, -D, HW, 0.0f, -D, -HW, 0.0f, D, -TW, 0.0f, D, -HW, 0.0f, -D, -TW, 0.0f, -D, 0.0f, -HH, -D, HW, 0.0f, -D, 0.0f, -HH, D, HW, 0.0f, D, 0.0f, -HH, D, -HW, 0.0f, D, 0.0f, -HH, -D, -HW, 0.0f, -D};
    private static FloatBuffer arrowBuffer = BufferUtils.makeFloatBuffer(arrowBox);
    private static final float[] arrowNormals = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f, -1.0f, -1.0f, 0.0f, -1.0f, -1.0f, 0.0f, -1.0f, -1.0f, 0.0f, -1.0f, -1.0f, 0.0f};
    private static FloatBuffer arrowNormalsBuffer = BufferUtils.makeFloatBuffer(arrowNormals);
    private float a;
    private float b;
    private float c;
    private float orientationAngle;
    private float throwStrength;
    private Point3D throwVector = new Point3D();

    public ArrowTrajectorySprite(int resourceId, Cell cell) {
        super(resourceId, cell);
        this.texCoords = new float[]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        this.texBuff = BufferUtils.makeFloatBuffer(this.texCoords);
    }

    public void initialize() {
        refreshTrajectory();
        initOrientation();
    }

    public void onLevelRestart() {
        super.onLevelRestart();
        refreshTrajectory();
    }

    public void update() {
        applyCameraPosition();
        applyDefaultCellPosition();
        if (this.animationRunning) {
            animate();
        } else {
            applyDefaultArrowPosition();
        }
        applyArrowOrientation();
    }

    public void draw(GL10 gl) {
        gl.glVertexPointer(3, 5126, 0, arrowBuffer);
        gl.glNormalPointer(5126, 0, arrowNormalsBuffer);
        gl.glTexCoordPointer(2, 5126, 0, this.texBuff);
        super.draw(gl);
    }

    /* access modifiers changed from: protected */
    public void drawShape(GL10 gl) {
        gl.glNormal3f(0.0f, 0.0f, 1.0f);
        gl.glDrawArrays(6, 0, 7);
        gl.glNormal3f(0.0f, 0.0f, -1.0f);
        gl.glDrawArrays(6, 7, 7);
        gl.glNormal3f(0.0f, 1.0f, 0.0f);
        gl.glDrawArrays(5, 14, 4);
        gl.glNormal3f(1.0f, 0.0f, 0.0f);
        gl.glDrawArrays(5, 18, 4);
        gl.glNormal3f(-1.0f, 0.0f, 0.0f);
        gl.glDrawArrays(5, 22, 4);
        gl.glNormal3f(0.0f, 1.0f, 0.0f);
        gl.glDrawArrays(5, 26, 4);
        gl.glNormal3f(0.0f, 1.0f, 0.0f);
        gl.glDrawArrays(5, 30, 4);
        gl.glNormal3f(1.0f, -1.0f, 0.0f);
        gl.glDrawArrays(5, 34, 4);
        gl.glNormal3f(-1.0f, -1.0f, 0.0f);
        gl.glDrawArrays(5, 38, 4);
    }

    private void applyArrowOrientation() {
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.orientationAngle, 0.0f, 1.0f, 0.0f);
    }

    private void applyDefaultArrowPosition() {
        Matrix.translateM(this.currentMatrix, 0, this.throwVector.x, this.throwVector.y + 2.0f, this.throwVector.z);
    }

    public void refreshTrajectory() {
        refreshParabolaCoefficients();
        refreshThrowProperties();
        refreshAnimationParameters();
    }

    private void refreshParabolaCoefficients() {
        float[] coefficients = ((CatapultCell) this.cell).getParabolaCoeffs();
        this.a = coefficients[0];
        this.b = coefficients[1];
        this.c = coefficients[2];
    }

    private void refreshThrowProperties() {
        this.throwVector = ((CatapultCell) this.cell).getThrowVector();
        this.throwStrength = (float) ((CatapultCell) this.cell).getThrowStrength();
    }

    private void refreshAnimationParameters() {
        this.animationLength = (long) (ARROW_THROW_ANIMATION_LENGTH_COEFF * this.throwStrength * 400.0f);
        this.totalMovement = this.throwStrength * 1.0f;
        this.totalRotation = START_ANGLE;
    }

    private void initOrientation() {
        switch (this.cell.direction) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                this.orientationAngle = 0.0f;
                return;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                this.orientationAngle = 180.0f;
                return;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                this.orientationAngle = -90.0f;
                return;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                this.orientationAngle = -90.0f;
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void calculateAnimation() {
        switch (this.cell.direction) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                calculateFlyNorth();
                return;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                calculateFlySouth();
                return;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                calculateFlyEast();
                return;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                calculateFlyWest();
                return;
            default:
                return;
        }
    }

    private void calculateFlyNorth() {
        this.xrot = START_ANGLE - (this.totalRotation * this.progress);
        this.zmove = (-this.totalMovement) * this.progress;
        this.ymove = ((parabola(this.zmove) + (this.progress * this.throwVector.y)) / 2.0f) + 2.0f;
    }

    private void calculateFlySouth() {
        this.xrot = -135.0f + (this.totalRotation * this.progress);
        this.zmove = this.totalMovement * this.progress;
        this.ymove = ((parabola(this.zmove) + (this.progress * this.throwVector.y)) / 2.0f) + 2.0f;
    }

    private void calculateFlyWest() {
        this.zrot = -135.0f + (this.totalRotation * this.progress);
        this.xmove = (-this.totalMovement) * this.progress;
        this.ymove = ((parabola(this.xmove) + (this.progress * this.throwVector.y)) / 2.0f) + 2.0f;
    }

    private void calculateFlyEast() {
        this.zrot = START_ANGLE - (this.totalRotation * this.progress);
        this.xmove = this.totalMovement * this.progress;
        this.ymove = ((parabola(this.xmove) + (this.progress * this.throwVector.y)) / 2.0f) + 2.0f;
    }

    private float parabola(float x) {
        return (this.a * x * x) + (this.b * x) + this.c;
    }

    public void refreshTextures(HashMap<Integer, String> currentTexturesNames) {
        super.refreshTextures(currentTexturesNames);
    }

    /* access modifiers changed from: protected */
    public void refreshTextureBuffer() {
        fillTextureArrayWithSameCoordinate(this.topL + ((this.topR - this.topL) / 2.0f), this.topT + ((this.topB - this.topT) / 2.0f));
        this.texBuff.clear();
        this.texBuff.put(this.texCoords);
        this.texBuff.position(0);
    }

    private void fillTextureArrayWithSameCoordinate(float s, float t) {
        int size = this.texCoords.length;
        for (int i = 0; i < size; i++) {
            if (i % 2 == 0) {
                this.texCoords[i] = s;
            } else {
                this.texCoords[i] = t;
            }
        }
    }
}
