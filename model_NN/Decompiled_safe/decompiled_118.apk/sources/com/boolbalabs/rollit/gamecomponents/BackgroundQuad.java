package com.boolbalabs.rollit.gamecomponents;

import android.graphics.Rect;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.rollit.R;
import javax.microedition.khronos.opengles.GL10;

public class BackgroundQuad extends ZNode {
    private BackgroundView backgroundView = new BackgroundView(R.drawable.rc_gameplay_bg_texture);
    private final int bgRef = R.drawable.rc_gameplay_bg_texture;

    private class BackgroundView extends ZNode {
        private Rect cropRectOnTexture;
        private Rect rectOnScreen = new Rect(0, 0, ScreenMetrics.screenWidthRip, ScreenMetrics.screenHeightRip);

        public BackgroundView(int resourceId) {
            super(R.drawable.rc_gameplay_bg_texture, 1);
            calculateBackgroundCropRect();
        }

        public void initialize() {
            super.initialize();
            initWithFrame(this.rectOnScreen, this.cropRectOnTexture);
        }

        public void draw(GL10 gl) {
            gl.glDisable(2896);
            drawVerts(gl);
            gl.glEnable(2896);
        }

        private void calculateBackgroundCropRect() {
            if (ScreenMetrics.screenHeightPix <= 480) {
                this.cropRectOnTexture = new Rect(0, 0, ScreenMetrics.screenLargeSidePix, ScreenMetrics.screenSmallSidePix);
            } else {
                this.cropRectOnTexture = new Rect(0, 0, (int) (480.0f * ScreenMetrics.aspectRatioOriented), 480);
            }
        }
    }

    public BackgroundQuad() {
        super(-1, 0);
        addChild(this.backgroundView);
    }

    public void update() {
    }

    public void drawNode(GL10 gl) {
        super.drawNode(gl);
    }
}
