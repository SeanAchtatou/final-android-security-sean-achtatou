package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import com.boolbalabs.rollit.gamecomponents.cells.Cell;

public class TeleportCellSprite extends CellSprite {
    public TeleportCellSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
    }
}
