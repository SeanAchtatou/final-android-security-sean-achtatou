package com.boolbalabs.rollit.gamecomponents.cells.managers;

import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import com.boolbalabs.rollit.gamecomponents.GameField;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.gamecomponents.cells.CellFactory;
import java.util.ArrayList;
import java.util.List;
import javax.microedition.khronos.opengles.GL10;

public abstract class CellManager {
    private static GameField gameField;
    private static GameScene parentGameScene;
    protected int cellType;
    protected List<Cell> cells = new ArrayList();
    protected int numberOfCellsCreated = 0;

    public void initialize() {
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            Cell cell = this.cells.get(i);
            cell.initialize();
            cell.register(parentGameScene);
        }
    }

    public void update() {
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            this.cells.get(i).update();
        }
    }

    public void drawAllCells(GL10 gl) {
        if (this.numberOfCellsCreated != 0) {
            initGLforCurrentCellType();
        }
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            this.cells.get(i).draw(gl);
        }
    }

    private void initGLforCurrentCellType() {
        DisplayServiceOpenGL.getInstance().gl.glTexCoordPointer(2, 5126, 0, this.cells.get(0).getCellTextureCoordsBuffer());
    }

    public void loadContent() {
        gameField = GameField.getInstance();
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            Cell cell = this.cells.get(i);
            cell.loadContent();
            int size = cell.getChildren().size();
            for (int j = 0; j < size; j++) {
                ZNode currentView = cell.getChildren().get(j);
                if (!gameField.getChildren().contains(currentView)) {
                    gameField.addChild(currentView);
                }
            }
        }
    }

    public void unloadContent() {
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            this.cells.get(i).unloadContent();
        }
    }

    public Cell addCell(int x, int y, int z, int arg1, int arg2, int cellType2) {
        Cell cell;
        if (this.cellType != cellType2) {
            return null;
        }
        if (this.numberOfCellsCreated == this.cells.size()) {
            cell = CellFactory.makeCell(x, y, z, arg1, arg2, cellType2);
            this.cells.add(this.numberOfCellsCreated, cell);
        } else {
            cell = this.cells.get(this.numberOfCellsCreated);
        }
        cell.reinitialize(x, y, z, arg1, arg2);
        this.numberOfCellsCreated++;
        return cell;
    }

    public void cleanUp() {
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            this.cells.get(i).cleanUp();
            this.cells.get(i).unregister();
        }
        this.numberOfCellsCreated = 0;
    }

    public void refreshTextures() {
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            this.cells.get(i).refreshTextures();
        }
    }

    public void onLevelRestart() {
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            this.cells.get(i).onLevelRestart();
        }
    }

    public static void setGameScene(GameScene gameScene) {
        parentGameScene = gameScene;
    }

    public void register() {
        for (int i = 0; i < this.numberOfCellsCreated; i++) {
            this.cells.get(i).register(parentGameScene);
        }
    }
}
