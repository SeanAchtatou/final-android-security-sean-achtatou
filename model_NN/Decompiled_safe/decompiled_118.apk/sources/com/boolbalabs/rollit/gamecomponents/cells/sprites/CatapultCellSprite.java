package com.boolbalabs.rollit.gamecomponents.cells.sprites;

import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import javax.microedition.khronos.opengles.GL10;

public class CatapultCellSprite extends CellSprite {
    public CatapultCellSprite(int resourceId, Cell cell) {
        super(resourceId, cell);
    }

    public void draw(GL10 gl) {
        gl.glVertexPointer(3, 5126, 0, CellSprite.cellBuff);
        gl.glNormalPointer(5126, 0, CellSprite.normalsBuff);
        gl.glTexCoordPointer(2, 5126, 0, this.texBuff);
        super.draw(gl);
    }
}
