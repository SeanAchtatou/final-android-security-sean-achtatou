package com.boolbalabs.rollit.gamecomponents.cells;

public class EmptyCell extends Cell {
    private static EmptyCell instance;

    private EmptyCell() {
        this.coordinate.set(0.0f, 0.0f, 0.0f);
    }

    public static EmptyCell getInstance() {
        if (instance == null) {
            instance = new EmptyCell();
        }
        return instance;
    }

    public void onLevelRestart() {
    }

    public void refreshTextures() {
    }

    /* access modifiers changed from: protected */
    public void setAppropriateTexturesNames(int condition) {
    }
}
