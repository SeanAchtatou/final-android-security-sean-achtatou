package com.boolbalabs.rollit;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.boolbalabs.lib.game.GameViewGL;
import com.boolbalabs.lib.managers.BitmapManager;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.managers.VibratorManager;
import com.boolbalabs.lib.services.ContentLoaderServiceOpenGL;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import com.boolbalabs.lib.utils.Constants;
import com.boolbalabs.lib.utils.CoordinatesParser;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.menucomponents.BLCube;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import com.flurry.android.FlurryAgent;
import com.google.ads.AdView;
import com.inmobi.androidsdk.impl.InMobiAdView;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.LeaderboardsScreenActivity;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;

public class RollItActivity extends Activity {
    public static final String TAG = "RollItGame";
    private final int SCORELOOP_LEVEL = 1;
    private final int SCORELOOP_MODE = 1;
    private Handler activityHandler;
    private Dialog crashDialog;
    private Dialog exitDialog;
    private Dialog fullversionDialog;
    private Dialog gameCompletedDialog;
    private GameViewGL gameViewGL;
    private Dialog helpDialog;
    private Dialog inGameHelpDialog;
    protected boolean isLdpiDevice = false;
    private Toast loadingToast;
    private Dialog marketNotFoundDialog;
    private FrameLayout parentView;
    private Dialog ratemeDialog;
    private RollItDialogs rollItDialogs;
    private RollItGame rollItGame;
    private Settings settings;
    private SoundManager soundManager;
    private TexturesManager texturesManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.parentView = (FrameLayout) findViewById(R.id.frameview);
        getWindow().addFlags(128);
        DebugLog.setDebugLogging(false);
        createLoadingToast();
        initializeManagersAndServices();
        createGame();
        createDialogs(this.rollItGame);
        if (this.parentView != null) {
            this.parentView.addView(this.gameViewGL);
            onCreateAds(this.parentView);
        } else {
            this.parentView = new FrameLayout(this);
            this.parentView.addView(this.gameViewGL);
            setContentView(this.parentView);
        }
        Settings.currentLaunch++;
        this.settings.saveSharedPreferences();
        Runtime.getRuntime().gc();
    }

    private void createLoadingToast() {
        View layout = getLayoutInflater().inflate((int) R.layout.loading_toast, (ViewGroup) null);
        this.loadingToast = new Toast(getApplicationContext());
        this.loadingToast.setDuration(3000);
        this.loadingToast.setView(layout);
    }

    private void createGame() {
        this.gameViewGL = new GameViewGL(this);
        this.rollItGame = new RollItGame(this, this);
        this.rollItGame.initialize();
        BLCube blCube = new BLCube();
        blCube.initialize();
        this.rollItGame.registerGameComponent(blCube, 4);
        this.activityHandler = createHandler();
        this.soundManager.setHandler(this.activityHandler);
        this.rollItGame.setActivityHandler(this.activityHandler);
        this.gameViewGL.startThread(this.rollItGame);
    }

    private void createDialogs(RollItGame rollItGame2) {
        this.rollItDialogs = new RollItDialogs(this, rollItGame2);
        this.ratemeDialog = this.rollItDialogs.createRateMeDialog();
        this.crashDialog = this.rollItDialogs.createCrashDialog();
        this.inGameHelpDialog = this.rollItDialogs.createInGameHelpDialog();
        this.helpDialog = this.rollItDialogs.createHelpDialog();
        this.exitDialog = this.rollItDialogs.createExitDialog();
        this.fullversionDialog = this.rollItDialogs.createFullVersionDialog();
        this.gameCompletedDialog = this.rollItDialogs.createGameCompletedDialog();
        this.marketNotFoundDialog = this.rollItDialogs.createMarketNotFoundDialog();
    }

    private void initializeManagersAndServices() {
        ScreenMetrics.initScreenMetrics(this, 2);
        Settings.initialise(getSharedPreferences(TAG, 0), getResources());
        this.settings = Settings.getInstance();
        this.settings.loadSharedPreferences();
        this.settings.detectSlowDevice();
        BitmapManager.init(getResources());
        SoundManager.init(this);
        this.soundManager = SoundManager.getInstance();
        VibratorManager.init(this);
        TexturesManager.init(getResources());
        CoordinatesParser.initialise(getResources());
        this.texturesManager = TexturesManager.getInstance();
        if (ScreenMetrics.resolution_postfix.equals("lres")) {
            this.isLdpiDevice = true;
        }
        if (ScreenMetrics.resolution_postfix.equals("xhres")) {
            ScreenMetrics.resolution_postfix = "hres";
        }
        this.texturesManager.loadCoordinatesFromFile("coordinates/rc_menu_texture_" + ScreenMetrics.resolution_postfix);
        this.texturesManager.loadCoordinatesFromFile("coordinates/rc_common_texture_" + ScreenMetrics.resolution_postfix);
        this.texturesManager.loadCoordinatesFromFile("coordinates/rc_gameplay_texture_grass_" + ScreenMetrics.resolution_postfix);
        this.texturesManager.loadCoordinatesFromFile("coordinates/boolba_logo_texture_" + ScreenMetrics.resolution_postfix);
        InGameHelp.initialize();
        DisplayServiceOpenGL.initialise();
        ContentLoaderServiceOpenGL.initialise();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        InGameHelp.dismissDialogIfShown();
        this.rollItDialogs.dismissAnyDialogIfShown();
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.gameViewGL.pauseGame();
        this.settings.saveSharedPreferences();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.settings.loadSettingsOnResume();
        setRequestedOrientation(0);
        this.gameViewGL.resumeGame();
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, Settings.FLURRY_ID);
    }

    public void onStop() {
        super.onStop();
        this.settings.sendFlurryStatistic();
        FlurryAgent.onEndSession(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.gameViewGL.stopThread();
        this.rollItGame.onExit();
        super.onDestroy();
        System.runFinalization();
        System.exit(0);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return this.ratemeDialog;
            case 2:
                return this.crashDialog;
            case 3:
                return this.helpDialog;
            case 4:
            default:
                return null;
            case 5:
                return this.fullversionDialog;
            case 6:
                return this.inGameHelpDialog;
            case 7:
                return this.gameCompletedDialog;
            case 8:
                return this.exitDialog;
            case 9:
                return this.marketNotFoundDialog;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 6:
                setDialogContent(dialog);
                break;
        }
        super.onPrepareDialog(id, dialog);
    }

    private void setDialogContent(Dialog dialog) {
        InGameHelp.setDialogContent(dialog, ((SceneGameplay) this.rollItGame.getGameScene(1)).getCurrentLevel().getID());
    }

    private Handler createHandler() {
        return new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case RollItConstants.MESSAGE_SHOW_DIALOG_RATEME /*1443*/:
                        RollItActivity.this.showRateMeDialog();
                        return;
                    case RollItConstants.MESSAGE_SHOW_DIALOG_CRASH /*1453*/:
                        RollItActivity.this.showCrashDialog();
                        return;
                    case RollItConstants.MESSAGE_SHOW_DIALOG_HELP /*1457*/:
                        RollItActivity.this.showHelpDialog();
                        return;
                    case RollItConstants.MESSAGE_SHOW_DIALOG_FULLVERSION /*1463*/:
                        RollItActivity.this.showFullVersionDialog();
                        return;
                    case RollItConstants.MESSAGE_SHOW_DIALOG_IN_GAME_HELP /*1467*/:
                        RollItActivity.this.showInGameHelpDialog();
                        return;
                    case RollItConstants.MESSAGE_SHOW_DIALOG_GAME_COMPLETED /*1469*/:
                        RollItActivity.this.showGameCompletedDialog();
                        return;
                    case RollItConstants.MESSAGE_SHOW_DIALOG_EXIT /*1472*/:
                        RollItActivity.this.showExitDialog();
                        return;
                    case RollItConstants.MESSAGE_SHOW_DIALOG_MARKET_NOT_FOUND /*1483*/:
                        RollItActivity.this.showMarketNotFoundDialog();
                        return;
                    case RollItConstants.MESSAGE_SHOW_LOADING_TOAST /*1504*/:
                    default:
                        return;
                    case RollItConstants.MESSAGE_HIDE_LOADING_TOAST /*1505*/:
                        RollItActivity.this.hideLoadingToast();
                        return;
                    case RollItConstants.MESSAGE_SHOW_ADS /*1604*/:
                        RollItActivity.this.showAds();
                        return;
                    case RollItConstants.MESSAGE_HIDE_ADS /*1605*/:
                        RollItActivity.this.hideAds();
                        return;
                    case RollItConstants.MESSAGE_CHANGE_ADS_GRAVITY /*1606*/:
                        RollItActivity.this.setAdsViewGravity(msg);
                        return;
                    case Constants.MESSAGE_RESTART_SOUND_MANAGER:
                        RollItActivity.this.restartSoundManager(msg);
                        return;
                    case RollItConstants.MESSAGE_SHOW_HIGH_SCORES /*87439*/:
                        RollItActivity.this.showHighScores();
                        return;
                    case RollItConstants.MESSAGE_EXIT_APPLICATION /*87996*/:
                        RollItActivity.this.finish();
                        return;
                }
            }
        };
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.settings.isKeyAllowed(keyCode)) {
            return false;
        }
        if (keyCode == 84) {
            return false;
        }
        if (keyCode == 82) {
            askGameToToggleCameraSlider();
            return true;
        } else if (keyCode == 24 || keyCode == 25) {
            adjustVolume(keyCode);
            return true;
        } else {
            if (keyCode == 4) {
                switch (this.rollItGame.getCurrentGameSceneId()) {
                    case 1:
                        this.rollItGame.switchGameScene(3);
                        return true;
                    case 2:
                        Settings.getInstance().saveSharedPreferences();
                        showDialog(8);
                        return true;
                    case 3:
                        this.rollItGame.switchGameScene(2);
                        return true;
                    case 5:
                        this.rollItGame.switchGameScene(2);
                        return true;
                }
            }
            return super.onKeyDown(keyCode, event);
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return true;
    }

    private void adjustVolume(int keyCode) {
        if (keyCode == 24) {
            this.soundManager.adjustVolume(1);
        } else if (keyCode == 25) {
            this.soundManager.adjustVolume(-1);
        }
    }

    private void askGameToToggleCameraSlider() {
        if (this.rollItGame.getCurrentGameSceneId() == 1) {
            ((SceneGameplay) this.rollItGame.getGameScene(1)).toggleCameraSlider();
        }
    }

    /* access modifiers changed from: private */
    public void restartSoundManager(Message msg) {
        SoundManager soundManager2 = SoundManager.getInstance();
        if (soundManager2 != null) {
            try {
                soundManager2.stopAllPlayingSounds();
            } catch (Exception e) {
                Exception e2 = e;
                if (e2 == null) {
                    return;
                }
                if (e2.getMessage() == null) {
                    Log.w("SOUNDMANAGER", "e.getMessage() is null");
                    return;
                } else {
                    Log.w("SOUNDMANAGER", e2.getMessage());
                    return;
                }
            }
        }
        SoundManager.release();
        SoundManager.init(this);
        SoundManager soundManager3 = SoundManager.getInstance();
        soundManager3.setHandler(this.activityHandler);
        if (soundManager3 != null && ZageCommonSettings.musicEnabled) {
            soundManager3.playLoopingSound(R.raw.main_theme);
        }
    }

    /* access modifiers changed from: private */
    public void showHighScores() {
        Intent intent = new Intent(this, LeaderboardsScreenActivity.class);
        intent.putExtra(LeaderboardsScreenActivity.LEADERBOARD, 0);
        intent.putExtra("mode", 1);
        submitScore();
        startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.ScoreloopManager.onGamePlayEnded(com.scoreloop.client.android.core.model.Score, java.lang.Boolean):void
     arg types: [com.scoreloop.client.android.core.model.Score, int]
     candidates:
      com.scoreloop.client.android.ui.ScoreloopManager.onGamePlayEnded(java.lang.Double, java.lang.Integer):void
      com.scoreloop.client.android.ui.ScoreloopManager.onGamePlayEnded(com.scoreloop.client.android.core.model.Score, java.lang.Boolean):void */
    /* access modifiers changed from: protected */
    public void submitScore() {
        Score s = new Score(Double.valueOf((double) Settings.getTotalScore()), null);
        s.setLevel(1);
        s.setMode(1);
        ScoreloopManagerSingleton.get().onGamePlayEnded(s, (Boolean) false);
    }

    public void onCreateAds(FrameLayout parentLayout) {
    }

    /* access modifiers changed from: protected */
    public void showAds() {
    }

    /* access modifiers changed from: protected */
    public void hideAds() {
    }

    /* access modifiers changed from: protected */
    public void setAdsViewGravity(Message msg) {
        if (Settings.ADS_ENABLED) {
            int gravity = msg.arg1;
            AdView adMobView = (AdView) this.parentView.findViewById(RollItConstants.ADS_VIEW_ID);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ScreenMetrics.convertRipToPixel(320.0f), ScreenMetrics.convertRipToPixel(50.0f));
            params.gravity = gravity;
            if (adMobView != null) {
                this.parentView.removeView(adMobView);
                this.parentView.addView(adMobView, params);
            }
            InMobiAdView inMobiVew = (InMobiAdView) this.parentView.findViewById(R.id.adview);
            if (inMobiVew != null) {
                this.parentView.removeView(inMobiVew);
                this.parentView.addView(inMobiVew, params);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showHelpDialog() {
        showDialog(3);
    }

    /* access modifiers changed from: private */
    public void showInGameHelpDialog() {
        showDialog(6);
    }

    /* access modifiers changed from: private */
    public void showFullVersionDialog() {
        showDialog(5);
    }

    /* access modifiers changed from: private */
    public void showGameCompletedDialog() {
        showDialog(7);
    }

    /* access modifiers changed from: private */
    public void showRateMeDialog() {
        showDialog(1);
    }

    /* access modifiers changed from: private */
    public void showExitDialog() {
        showDialog(8);
    }

    /* access modifiers changed from: private */
    public void showCrashDialog() {
        showDialog(2);
    }

    /* access modifiers changed from: private */
    public void showMarketNotFoundDialog() {
        showDialog(9);
    }

    private void showLoadingToast() {
        this.loadingToast.show();
    }

    /* access modifiers changed from: private */
    public void hideLoadingToast() {
        if (this.loadingToast != null) {
            this.loadingToast.cancel();
        }
    }
}
