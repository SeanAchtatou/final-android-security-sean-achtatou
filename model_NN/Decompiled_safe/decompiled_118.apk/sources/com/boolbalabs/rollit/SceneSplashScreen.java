package com.boolbalabs.rollit;

import android.os.Bundle;
import com.boolbalabs.lib.game.Game;
import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.menucomponents.BLCube;

public class SceneSplashScreen extends GameScene {
    private BLCube blCube;

    public SceneSplashScreen(Game game, int sceneId) {
        super(game, sceneId);
    }

    public void initialize() {
    }

    public void onShow() {
    }

    public void onHide() {
        SoundManager sm;
        if (ZageCommonSettings.musicEnabled && (sm = SoundManager.getInstance()) != null) {
            sm.playLoopingSound(R.raw.main_theme);
        }
    }

    public void loadContent() {
        super.loadContent();
        findGameplayComponents();
    }

    private void findGameplayComponents() {
        this.blCube = (BLCube) findComponentByClass(BLCube.class);
    }

    public void performAction(int actionCode, Bundle actionParameters) {
    }
}
