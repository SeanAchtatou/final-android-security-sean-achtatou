package com.boolbalabs.rollit;

import android.os.Bundle;
import android.os.Message;
import com.boolbalabs.lib.game.Game;
import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.rollit.menucomponents.ChooseLevelpackMenu;
import com.boolbalabs.rollit.settings.RollItConstants;

public class SceneChooseLevelpack extends GameScene {
    private ChooseLevelpackMenu chooseLevelpackMenu;

    public SceneChooseLevelpack(Game game, int sceneId) {
        super(game, sceneId);
        registerGameComponent(new ChooseLevelpackMenu());
    }

    public void initialize() {
        this.chooseLevelpackMenu = (ChooseLevelpackMenu) findComponentByClass(ChooseLevelpackMenu.class);
        this.chooseLevelpackMenu.initialize();
    }

    public void performAction(int actionCode, Bundle actionParameters) {
        switch (actionCode) {
            case RollItConstants.ACTION_PLAY_SELECTED_LEVELPACK /*75543*/:
                playSelectedLevelpack(actionParameters);
                return;
            case RollItConstants.ACTION_BACK /*77253*/:
                switchToMainMenu();
                return;
            default:
                return;
        }
    }

    private void switchToMainMenu() {
        askGameToSwitchGameScene(2);
    }

    private void playSelectedLevelpack(Bundle actionParameters) {
        ((SceneChooseLevel) ((RollItGame) this.mainGame).getGameScene(3)).loadLevelpack(Integer.valueOf(actionParameters.getInt(RollItConstants.KEY_LEVEL_PACK_ID, RollItConstants.LEVEL_PACK_GRASS.intValue())).intValue());
        this.mainGame.switchGameScene(3);
    }

    public void onShow() {
        super.onShow();
        changeAdsViewGravity(85);
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_ADS);
    }

    public void onHide() {
        super.onHide();
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_HIDE_ADS);
    }

    private void changeAdsViewGravity(int gravity) {
        Message msg = new Message();
        msg.what = RollItConstants.MESSAGE_CHANGE_ADS_GRAVITY;
        msg.arg1 = gravity;
        sendMessageToActivity(msg);
    }
}
