package com.boolbalabs.rollit;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.utils.ErrorReporter;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import java.util.ArrayList;
import java.util.List;

public class RollItDialogs {
    public static final int DIALOG_CRASH = 2;
    public static final int DIALOG_EXIT = 8;
    public static final int DIALOG_FULL_VERSION = 5;
    public static final int DIALOG_GAME_COMPLETED = 7;
    public static final int DIALOG_HELP = 3;
    public static final int DIALOG_IN_GAME_HELP = 6;
    public static final int DIALOG_MARKET_NOT_FOUND = 9;
    public static final int DIALOG_RATEME = 1;
    public static final int DIALOG_SETTINGS = 0;
    private List<Dialog> allDialogs = new ArrayList();
    private int buttonClickSoundId = R.raw.click_sound;
    /* access modifiers changed from: private */
    public RollItActivity rollItActivity;
    /* access modifiers changed from: private */
    public RollItGame rollItGame;

    public RollItDialogs(RollItActivity activity, RollItGame game) {
        this.rollItActivity = activity;
        this.rollItGame = game;
    }

    public void dismissAnyDialogIfShown() {
        int dialogsNumber = this.allDialogs.size();
        for (int i = 0; i < dialogsNumber; i++) {
            Dialog dialog = this.allDialogs.get(i);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog createHelpDialog() {
        View dialogView = LayoutInflater.from(this.rollItActivity).inflate((int) R.layout.dialog_help, (ViewGroup) null);
        ((TextView) dialogView.findViewById(R.id.help_how_to_play_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_solve_button_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_score_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_red_falling_cell_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_green_falling_cell_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_high_cell_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_bridge_button_cells_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_shift_cell_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_teleport_cell_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_catapult_cell_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_how_to_play_text)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_solve_button_text)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_score_text)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_red_falling_cell_text)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_green_falling_cell_text)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_high_cell_text)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_bridge_button_cells_text)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_shift_cell_text)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_teleport_cell_text)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_catapult_cell_text)).setTypeface(Settings.MAIN_TYPEFACE);
        final Dialog dialog = new Dialog(this.rollItActivity, 2131296285);
        dialog.setContentView(dialogView);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        ((Button) dialogView.findViewById(R.id.btn_cross)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
            }
        });
        this.allDialogs.add(dialog);
        return dialog;
    }

    public Dialog createInGameHelpDialog() {
        final View dialogView = LayoutInflater.from(this.rollItActivity).inflate((int) R.layout.dialog_in_game_help, (ViewGroup) null);
        ((TextView) dialogView.findViewById(R.id.help_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.help_text_1)).setTypeface(Settings.MAIN_TYPEFACE);
        final Dialog dialog = new Dialog(this.rollItActivity, 2131296285);
        dialog.setContentView(dialogView);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                ((ScrollView) dialogView.findViewById(R.id.help_scroll_view)).fullScroll(33);
                RollItDialogs.this.rollItGame.startLevelTimer();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                ((ScrollView) dialogView.findViewById(R.id.help_scroll_view)).fullScroll(33);
            }
        });
        ((Button) dialogView.findViewById(R.id.btn_cross)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
            }
        });
        this.allDialogs.add(dialog);
        return dialog;
    }

    /* access modifiers changed from: protected */
    public Dialog createFullVersionDialog() {
        View dialogView = LayoutInflater.from(this.rollItActivity).inflate((int) R.layout.dialog_fullversion, (ViewGroup) null);
        ((TextView) dialogView.findViewById(R.id.textview_fullversion)).setTypeface(Settings.MAIN_TYPEFACE);
        final Dialog dialog = new Dialog(this.rollItActivity, 2131296285);
        dialog.setContentView(dialogView);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        ((Button) dialogView.findViewById(R.id.btn_cross)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
            }
        });
        Button btnYes = (Button) dialogView.findViewById(R.id.btn_yes);
        btnYes.setTypeface(Settings.MAIN_TYPEFACE);
        btnYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
            }
        });
        Button btnNo = (Button) dialogView.findViewById(R.id.btn_cancel);
        btnNo.setTypeface(Settings.MAIN_TYPEFACE);
        btnNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
            }
        });
        this.allDialogs.add(dialog);
        return dialog;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public Dialog createCrashDialog() {
        View dialogView = LayoutInflater.from(this.rollItActivity).inflate((int) R.layout.dialog_crash, (ViewGroup) null);
        ((TextView) dialogView.findViewById(R.id.textview_crash)).setTypeface(Settings.MAIN_TYPEFACE);
        final Dialog dialog = new Dialog(this.rollItActivity, 2131296285);
        dialog.setContentView(dialogView);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        Button btnYes = (Button) dialogView.findViewById(R.id.btn_yes);
        btnYes.setTypeface(Settings.MAIN_TYPEFACE);
        btnYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                ErrorReporter.getInstance().sendReport(RollItDialogs.this.rollItActivity);
                dialog.cancel();
            }
        });
        Button btnNo = (Button) dialogView.findViewById(R.id.btn_no);
        btnNo.setTypeface(Settings.MAIN_TYPEFACE);
        btnNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                ErrorReporter.getInstance().clearErrorsWithoutSending();
            }
        });
        this.allDialogs.add(dialog);
        return dialog;
    }

    /* access modifiers changed from: protected */
    public Dialog createGameCompletedDialog() {
        View dialogView = LayoutInflater.from(this.rollItActivity).inflate((int) R.layout.dialog_game_completed, (ViewGroup) null);
        ((TextView) dialogView.findViewById(R.id.textview_game_completed_title)).setTypeface(Settings.MAIN_TYPEFACE);
        ((TextView) dialogView.findViewById(R.id.textview_game_completed_text)).setTypeface(Settings.MAIN_TYPEFACE);
        final Dialog dialog = new Dialog(this.rollItActivity, 2131296285);
        dialog.setContentView(dialogView);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        Button btnRateMe = (Button) dialogView.findViewById(R.id.btn_rateme);
        btnRateMe.setTypeface(Settings.MAIN_TYPEFACE);
        btnRateMe.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
                RollItDialogs.this.openGamePageInMarket();
            }
        });
        Button btnBackToMenu = (Button) dialogView.findViewById(R.id.btn_menu);
        btnBackToMenu.setTypeface(Settings.MAIN_TYPEFACE);
        btnBackToMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
                RollItDialogs.this.rollItGame.switchGameScene(2);
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                RollItDialogs.this.rollItGame.switchGameScene(2);
            }
        });
        this.allDialogs.add(dialog);
        return dialog;
    }

    /* access modifiers changed from: protected */
    public Dialog createRateMeDialog() {
        View dialogView = LayoutInflater.from(this.rollItActivity).inflate((int) R.layout.dialog_rateme, (ViewGroup) null);
        ((TextView) dialogView.findViewById(R.id.textview_rateme)).setTypeface(Settings.MAIN_TYPEFACE);
        final Dialog dialog = new Dialog(this.rollItActivity, 2131296285);
        dialog.setContentView(dialogView);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        Button btnYes = (Button) dialogView.findViewById(R.id.btn_rateme);
        btnYes.setTypeface(Settings.MAIN_TYPEFACE);
        btnYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
                RollItDialogs.this.openGamePageInMarket();
            }
        });
        Button btnNo = (Button) dialogView.findViewById(R.id.btn_rateme_cancel);
        btnNo.setTypeface(Settings.MAIN_TYPEFACE);
        btnNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                Settings.currentLaunch = 2;
            }
        });
        this.allDialogs.add(dialog);
        return dialog;
    }

    /* access modifiers changed from: protected */
    public Dialog createExitDialog() {
        View dialogView = LayoutInflater.from(this.rollItActivity).inflate((int) R.layout.dialog_exit, (ViewGroup) null);
        ((TextView) dialogView.findViewById(R.id.textview_exit)).setTypeface(Settings.MAIN_TYPEFACE);
        final Dialog dialog = new Dialog(this.rollItActivity, 2131296285);
        dialog.setContentView(dialogView);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        Button btnYes = (Button) dialogView.findViewById(R.id.btn_exit);
        btnYes.setTypeface(Settings.MAIN_TYPEFACE);
        btnYes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
                RollItDialogs.this.rollItActivity.finish();
            }
        });
        Button btnNo = (Button) dialogView.findViewById(R.id.btn_exit_cancel);
        btnNo.setTypeface(Settings.MAIN_TYPEFACE);
        btnNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
            }
        });
        this.allDialogs.add(dialog);
        return dialog;
    }

    public Dialog createMarketNotFoundDialog() {
        View dialogView = LayoutInflater.from(this.rollItActivity).inflate((int) R.layout.dialog_market_not_found, (ViewGroup) null);
        ((TextView) dialogView.findViewById(R.id.textview_market_not_found)).setTypeface(Settings.MAIN_TYPEFACE);
        final Dialog dialog = new Dialog(this.rollItActivity, 2131296285);
        dialog.setContentView(dialogView);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        Button btnOK = (Button) dialogView.findViewById(R.id.btn_ok);
        btnOK.setTypeface(Settings.MAIN_TYPEFACE);
        btnOK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RollItDialogs.this.playClick();
                dialog.cancel();
            }
        });
        this.allDialogs.add(dialog);
        return dialog;
    }

    /* access modifiers changed from: private */
    public void playClick() {
        SoundManager sm = SoundManager.getInstance();
        if (sm != null && ZageCommonSettings.soundEnabled) {
            sm.playShortSound(this.buttonClickSoundId);
        }
    }

    /* access modifiers changed from: private */
    public void openGamePageInMarket() {
        try {
            Settings.rateMeDialogHasBeenShown = true;
            this.rollItActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Settings.MARKET_URL)));
        } catch (ActivityNotFoundException e) {
            showApplicationInWebMarket();
        }
    }

    private void showApplicationInWebMarket() {
        try {
            this.rollItActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Settings.WEB_MARKET_URL)));
        } catch (ActivityNotFoundException e) {
            this.rollItGame.getGameScene(this.rollItGame.getCurrentGameSceneId()).sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_DIALOG_MARKET_NOT_FOUND);
        }
    }
}
