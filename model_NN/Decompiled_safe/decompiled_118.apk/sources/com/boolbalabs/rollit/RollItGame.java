package com.boolbalabs.rollit;

import android.content.Context;
import android.content.res.Resources;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.Message;
import com.boolbalabs.lib.game.Game;
import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.services.ContentLoaderServiceOpenGL;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.level.LevelLoader;
import com.boolbalabs.rollit.level.LevelManager;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import com.flurry.android.FlurryAgent;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

public class RollItGame extends Game {
    public static final int SCENE_CHOOSELEVEL = 3;
    public static final int SCENE_CHOOSE_LEVELPACK = 5;
    public static final int SCENE_GAMEPLAY = 1;
    public static final int SCENE_MAINMENU = 2;
    public static final int SCENE_SPLASH = 4;
    private Handler activityHandler;
    private boolean afterOnCreate = false;
    private DisplayServiceOpenGL displayService;
    private LevelLoader levelLoader;
    private LevelManager levelManager;
    private ContentLoaderServiceOpenGL loaderService;
    private RollItActivity mainActivity;
    private SceneChooseLevel sceneChooseLevel;
    private SceneChooseLevelpack sceneChooseLevelPack;
    private SceneGameMenu sceneGameMenu;
    private SceneGameplay sceneGameplay;
    private SceneSplashScreen sceneSplashScreen;
    private SoundManager soundManager;
    private TexturesManager texturesManager;

    public RollItGame(Context context, RollItActivity mainActivity2) {
        this.mainActivity = mainActivity2;
        ZageCommonSettings.openGLVertexModeDefaultViewDistance = 30.0f;
        LevelLoader.initialize();
        this.levelLoader = LevelLoader.getInstance();
        LevelManager.initialize();
        this.levelManager = LevelManager.getInstance();
        this.sceneGameMenu = new SceneGameMenu(this, 2);
        this.sceneChooseLevel = new SceneChooseLevel(this, 3);
        this.sceneGameplay = new SceneGameplay(this, 1);
        this.sceneSplashScreen = new SceneSplashScreen(this, 4);
        this.displayService = DisplayServiceOpenGL.getInstance();
        this.displayService.setGame(this);
        this.loaderService = ContentLoaderServiceOpenGL.getInstance();
        this.loaderService.setContentLoader(this);
        this.loaderService.setGameScenes(getGameScenes());
    }

    public void setActivityHandler(Handler activityHandler2) {
        this.activityHandler = activityHandler2;
        for (GameScene scene : getGameScenes().values()) {
            scene.setActivityHandler(this.activityHandler);
        }
    }

    public void initialize() {
        this.afterOnCreate = true;
        this.levelLoader.initWithGame(this);
        this.levelManager.initWithGame(this);
        this.sceneGameplay.initialize();
        this.sceneChooseLevel.initialize();
        this.sceneGameMenu.initialize();
        this.sceneSplashScreen.initialize();
        switchGameScene(3);
        LevelLoader.getInstance().loadTextureThemes();
        Settings.getInstance().loadLaunchedFirstTime();
        if (Settings.launchedFirstTime) {
            this.levelManager.assignScoresToAllCompletedLevels();
            Settings.launchedFirstTime = false;
            Settings.getInstance().saveLaunchedFirstTime();
        }
    }

    public void loadContent() {
        this.soundManager = SoundManager.getInstance();
        addLoopingSound(R.raw.main_theme);
        addShortSound(R.raw.click_sound);
        addShortSound(R.raw.catapult_throws);
        addShortSound(R.raw.cube_falls);
        addShortSound(R.raw.cube_rolled);
        addShortSound(R.raw.gameplay_button_pressed);
        addShortSound(R.raw.level_completed);
        addShortSound(R.raw.shift_sound);
        addShortSound(R.raw.teleport_middle);
        addShortSound(R.raw.fall_not_allowed);
        super.loadContent();
    }

    public void initCustomOpenGLParameters(GL10 gl, GLSurfaceView parentView) {
        if (gl instanceof GL11) {
            DebugLog.v("Roll It", "GL is 11");
        }
        gl.glEnable(2929);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glMaterialfv(1032, 4608, new float[]{0.5f, 0.5f, 0.5f, 1.0f}, 0);
        gl.glMaterialfv(1032, 4609, new float[]{1.0f, 1.0f, 1.0f, 1.0f}, 0);
        gl.glLightfv(16384, 4608, new float[]{0.6f, 0.6f, 0.6f, 1.0f}, 0);
        gl.glLightfv(16384, 4609, new float[]{0.9f, 0.9f, 0.9f, 1.0f}, 0);
        gl.glShadeModel(7425);
        gl.glEnable(3024);
        gl.glEnable(2896);
        if (this.currentScene != null && this.currentScene.isSceneInitialized()) {
            this.sceneGameplay.onResume();
            this.sceneGameMenu.onResume();
            this.sceneChooseLevel.onResume();
        }
    }

    public void drawSplashScreen() {
    }

    public void onAfterLoad() {
        super.onAfterLoad();
        loadCorrectScene();
    }

    public void onPause() {
        DebugLog.v("Roll it", "onPause called");
        Settings.lastSceneID = this.currentScene.getSceneId();
        if (this.currentScene != null) {
            this.currentScene.onPause();
        }
        Settings.getInstance().saveSharedPreferences();
        if (this.soundManager != null) {
            try {
                this.soundManager.pauseLoopingSounds();
            } catch (IllegalStateException e) {
                FlurryAgent.onEvent("Err: IllegalState in RollItGame.onPause()");
            }
        }
    }

    public void onResume() {
        DebugLog.v("Roll It", "onResume called");
        Settings.getInstance().loadSharedPreferences();
        this.sceneGameplay.disableUserInput();
        this.sceneGameMenu.disableUserInput();
        this.sceneChooseLevel.disableUserInput();
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_LOADING_TOAST);
    }

    public void onExit() {
        DebugLog.v("Roll It", "onExit called");
        super.onExit();
    }

    private void loadCorrectScene() {
        DebugLog.v("RollIt", "loadCorrectScene: afterOnCreate: " + this.afterOnCreate + "; Current Scene: " + this.currentScene);
        if (this.afterOnCreate) {
            switchGameScene(4);
            this.afterOnCreate = false;
        } else {
            switchGameScene(Settings.lastSceneID);
            if (this.soundManager != null && ZageCommonSettings.musicEnabled) {
                this.soundManager.playLoopingSound(R.raw.main_theme);
            }
        }
        DebugLog.v("RollIt", "Scene changed to: " + this.currentScene);
    }

    public GameScene getGameScene(int sceneId) {
        switch (sceneId) {
            case 1:
                return this.sceneGameplay;
            case 2:
                return this.sceneGameMenu;
            case 3:
                return this.sceneChooseLevel;
            case 4:
            default:
                return null;
            case 5:
                return this.sceneChooseLevelPack;
        }
    }

    public Resources getGameResources() {
        return this.mainActivity.getResources();
    }

    private void sendMessageToActivity(Message msg) {
        if (this.activityHandler != null) {
            try {
                this.activityHandler.sendMessage(msg);
            } catch (Exception e) {
            }
        }
    }

    private void sendEmptyMessageToActivity(int messageId) {
        if (this.activityHandler != null) {
            try {
                this.activityHandler.sendEmptyMessage(messageId);
            } catch (Exception e) {
            }
        }
    }

    public void startLevelTimer() {
        if (this.sceneGameplay != null) {
            DebugLog.v("Roll It", "LevelTimer STARTED from dialog");
            this.sceneGameplay.getCurrentLevel().startLevelTimer();
        }
    }
}
