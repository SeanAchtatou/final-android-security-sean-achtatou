package com.boolbalabs.rollit;

import android.app.Dialog;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;

public class InGameHelp {
    private static final HashMap<Integer, Integer> helpImages = new HashMap<>();
    private static final HashMap<Integer, Integer> helpTexts = new HashMap<>();
    private static final HashMap<Integer, Integer> helpTitles = new HashMap<>();
    private static Dialog inGameHelpDialog;
    public static final ArrayList<Integer> levelsWithHelpIDs = new ArrayList<>();

    public static void initialize() {
        levelsWithHelpIDs.add(1001);
        levelsWithHelpIDs.add(1003);
        levelsWithHelpIDs.add(1004);
        levelsWithHelpIDs.add(1006);
        levelsWithHelpIDs.add(1008);
        levelsWithHelpIDs.add(1011);
        levelsWithHelpIDs.add(1013);
        levelsWithHelpIDs.add(1015);
        helpTitles.put(levelsWithHelpIDs.get(0), Integer.valueOf((int) R.string.help_basics_title));
        helpTitles.put(levelsWithHelpIDs.get(1), Integer.valueOf((int) R.string.help_red_falling_cell_title));
        helpTitles.put(levelsWithHelpIDs.get(2), Integer.valueOf((int) R.string.help_green_falling_cell_title));
        helpTitles.put(levelsWithHelpIDs.get(3), Integer.valueOf((int) R.string.help_high_cell_title));
        helpTitles.put(levelsWithHelpIDs.get(4), Integer.valueOf((int) R.string.help_shift_cell_title));
        helpTitles.put(levelsWithHelpIDs.get(5), Integer.valueOf((int) R.string.help_bridge_button_cells_title));
        helpTitles.put(levelsWithHelpIDs.get(6), Integer.valueOf((int) R.string.help_teleport_cell_title));
        helpTitles.put(levelsWithHelpIDs.get(7), Integer.valueOf((int) R.string.help_catapult_cell_title));
        helpTexts.put(levelsWithHelpIDs.get(0), Integer.valueOf((int) R.string.help_basics_text));
        helpTexts.put(levelsWithHelpIDs.get(1), Integer.valueOf((int) R.string.help_red_falling_cell_text));
        helpTexts.put(levelsWithHelpIDs.get(2), Integer.valueOf((int) R.string.help_green_falling_cell_text));
        helpTexts.put(levelsWithHelpIDs.get(3), Integer.valueOf((int) R.string.help_high_cell_text));
        helpTexts.put(levelsWithHelpIDs.get(4), Integer.valueOf((int) R.string.help_shift_cell_text));
        helpTexts.put(levelsWithHelpIDs.get(5), Integer.valueOf((int) R.string.help_bridge_button_cells_text));
        helpTexts.put(levelsWithHelpIDs.get(6), Integer.valueOf((int) R.string.help_teleport_cell_text));
        helpTexts.put(levelsWithHelpIDs.get(7), Integer.valueOf((int) R.string.help_catapult_cell_text));
        helpImages.put(levelsWithHelpIDs.get(0), Integer.valueOf((int) R.drawable.help_how_to_play));
        helpImages.put(levelsWithHelpIDs.get(1), Integer.valueOf((int) R.drawable.help_red_leaf));
        helpImages.put(levelsWithHelpIDs.get(2), Integer.valueOf((int) R.drawable.help_yellow_leaf));
        helpImages.put(levelsWithHelpIDs.get(3), Integer.valueOf((int) R.drawable.help_high_cell));
        helpImages.put(levelsWithHelpIDs.get(4), Integer.valueOf((int) R.drawable.help_shift));
        helpImages.put(levelsWithHelpIDs.get(5), Integer.valueOf((int) R.drawable.help_bridge_and_button));
        helpImages.put(levelsWithHelpIDs.get(6), Integer.valueOf((int) R.drawable.help_teleport));
        helpImages.put(levelsWithHelpIDs.get(7), Integer.valueOf((int) R.drawable.help_catapult));
    }

    public static void setDialogContent(Dialog dialog, int levelID) {
        if (levelsWithHelpIDs.contains(Integer.valueOf(levelID))) {
            inGameHelpDialog = dialog;
            ((TextView) inGameHelpDialog.findViewById(R.id.help_title)).setText(helpTitles.get(Integer.valueOf(levelID)).intValue());
            ((TextView) inGameHelpDialog.findViewById(R.id.help_text_1)).setText(helpTexts.get(Integer.valueOf(levelID)).intValue());
            ((ImageView) inGameHelpDialog.findViewById(R.id.help_image)).setImageResource(helpImages.get(Integer.valueOf(levelID)).intValue());
        }
    }

    public static void dismissDialogIfShown() {
        if (inGameHelpDialog != null && inGameHelpDialog.isShowing()) {
            inGameHelpDialog.dismiss();
        }
    }
}
