package com.boolbalabs.rollit.animators;

import android.os.Bundle;
import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.lib.transitions.SineInterpolator;
import com.boolbalabs.rollit.gamecomponents.RotatingCubeSprite;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.FinishCellSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.Axes;

public class LevelCompleteAnimator extends Animator {
    public LevelCompleteAnimator(RotatingCubeSprite sprite) {
        super(sprite);
        this.interpolator = new SineInterpolator(EasingType.IN);
        this.animationLength = 2000;
    }

    public void initialize(float[] lastStableMatrix, Axes lastStableAxes, Bundle movementBundle) {
        super.initialize(lastStableMatrix, lastStableAxes, movementBundle);
        setTotalRotation();
    }

    /* access modifiers changed from: protected */
    public void calculateVertivcalAnimation() {
        if (this.progress < 0.2f) {
            this.ymove = FinishCellSprite.getYmove();
        } else if (this.progress < 0.7f) {
            this.yrot = (-this.totalRotation) * 2.0f * (this.progress - 0.2f);
        } else {
            this.yrot = -this.totalRotation;
        }
    }

    private void setTotalRotation() {
        float rotationCoeff = 0.0f;
        switch (RotatingCubeSprite.getDirectionForWinEvent()) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                rotationCoeff = 4.0f;
                break;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                rotationCoeff = 2.0f;
                break;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                rotationCoeff = 3.0f;
                break;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                rotationCoeff = 1.0f;
                break;
        }
        this.totalRotation = 90.0f * rotationCoeff;
    }

    /* access modifiers changed from: protected */
    public void postAnimationAxesUpdate() {
    }

    /* access modifiers changed from: protected */
    public void notifyAnimationFinished() {
        getCubeSprite().performAction(RollItConstants.ACTION_SHOW_LEVEL_RESULTS, null);
        super.notifyAnimationFinished();
    }
}
