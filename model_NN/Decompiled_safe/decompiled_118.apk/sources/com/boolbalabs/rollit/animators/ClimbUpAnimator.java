package com.boolbalabs.rollit.animators;

import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.lib.transitions.SineInterpolator;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.RotatingCubeSprite;
import com.boolbalabs.rollit.settings.RollItConstants;

public class ClimbUpAnimator extends Animator {
    static final long ROLL_LENGTH__IN_MILLISEC = 400;
    private float A = -6.0f;
    private float B = 7.0f;

    public ClimbUpAnimator(RotatingCubeSprite sprite) {
        super(sprite);
        this.animationLength = 800;
        this.interpolator = new SineInterpolator(EasingType.IN);
        this.totalRotation = 180.0f;
        this.totalMovement = 1.0f;
        this.finishSound = R.raw.cube_rolled;
    }

    /* access modifiers changed from: protected */
    public void calculateMoveEast() {
        this.zrot = (-this.totalRotation) * this.progress;
        this.xmove = this.totalMovement * this.interpolator.getInterpolation(this.progress);
        this.ymove = parabola(this.totalMovement * this.progress);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveWest() {
        this.zrot = this.totalRotation * this.progress;
        this.xmove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        this.ymove = parabola(this.totalMovement * this.progress);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveNorth() {
        this.xrot = (-this.totalRotation) * this.progress;
        this.zmove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        this.ymove = parabola(this.totalMovement * this.progress);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveSouth() {
        this.xrot = this.totalRotation * this.progress;
        this.zmove = this.totalMovement * this.interpolator.getInterpolation(this.progress);
        this.ymove = parabola(this.totalMovement * this.progress);
    }

    /* access modifiers changed from: protected */
    public void postAnimationAxesUpdate() {
        switch (this.animationDirection) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                this.worldAxes.zAxis.set(this.worldAxes.zAxis.invert());
                this.worldAxes.yAxis.set(this.worldAxes.yAxis.invert());
                return;
            case RollItConstants.DIRECTION_EAST /*62103*/:
            case RollItConstants.DIRECTION_WEST /*62104*/:
                this.worldAxes.yAxis.set(this.worldAxes.yAxis.invert());
                this.worldAxes.xAxis.set(this.worldAxes.xAxis.invert());
                return;
            default:
                return;
        }
    }

    private float parabola(float t) {
        return (this.A * t * t) + (this.B * t);
    }

    /* access modifiers changed from: protected */
    public void playFinishVibrtaion() {
        if (ZageCommonSettings.vibroEnabled) {
            this.vibratorManager.vibrate(50);
        }
    }
}
