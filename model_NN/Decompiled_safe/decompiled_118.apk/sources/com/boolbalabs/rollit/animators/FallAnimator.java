package com.boolbalabs.rollit.animators;

import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.lib.transitions.FreeFallInterpolator;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.GameField;
import com.boolbalabs.rollit.gamecomponents.RotatingCubeSprite;
import com.boolbalabs.rollit.settings.RollItConstants;

public class FallAnimator extends Animator {
    public static final int ROTATIONS_PER_FALL = 2;

    public FallAnimator(RotatingCubeSprite sprite) {
        super(sprite);
        this.animationLength = 3200;
        this.interpolator = new FreeFallInterpolator(EasingType.IN, 8.0f, 1.5f);
        this.totalRotation = 720.0f;
        this.totalMovement = 8.0f;
        this.startSound = R.raw.cube_falls;
    }

    /* access modifiers changed from: protected */
    public void calculateMoveEast() {
        this.zrot = (-this.totalRotation) * this.progress;
        this.ymove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        if (((double) (-this.zrot)) < 45.0d) {
            this.xmove = (float) ((0.707106781d * Math.sin((((double) ((-this.zrot) + 45.0f)) * 3.141592653589793d) / 180.0d)) - 0.5d);
        }
    }

    /* access modifiers changed from: protected */
    public void calculateMoveWest() {
        this.zrot = this.totalRotation * this.progress;
        this.ymove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        if (((double) this.zrot) < 45.0d) {
            this.xmove = (float) ((-0.707106781d * Math.sin((((double) (this.zrot + 45.0f)) * 3.141592653589793d) / 180.0d)) + 0.5d);
        }
    }

    /* access modifiers changed from: protected */
    public void calculateMoveNorth() {
        this.xrot = (-this.totalRotation) * this.progress;
        this.ymove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        if (((double) (-this.xrot)) < 45.0d) {
            this.zmove = (float) ((-0.707106781d * Math.sin((((double) ((-this.xrot) + 45.0f)) * 3.141592653589793d) / 180.0d)) + 0.5d);
        }
    }

    /* access modifiers changed from: protected */
    public void calculateMoveSouth() {
        this.xrot = this.totalRotation * this.progress;
        this.ymove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        if (((double) this.xrot) < 45.0d) {
            this.zmove = (float) ((0.707106781d * Math.sin((((double) (this.xrot + 45.0f)) * 3.141592653589793d) / 180.0d)) - 0.5d);
        }
    }

    /* access modifiers changed from: protected */
    public void calculateVertivcalAnimation() {
        this.xrot = (this.totalRotation / 4.0f) * this.progress;
        this.zrot = (this.totalRotation / 4.0f) * this.progress;
        this.ymove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
    }

    /* access modifiers changed from: protected */
    public void postAnimationAxesUpdate() {
    }

    /* access modifiers changed from: protected */
    public void notifyAnimationFinished() {
        super.notifyAnimationFinished();
        GameField.getInstance().performAction(RollItConstants.ACTION_RESTART, null);
    }

    public static float getYmovement() {
        return 0.0f;
    }
}
