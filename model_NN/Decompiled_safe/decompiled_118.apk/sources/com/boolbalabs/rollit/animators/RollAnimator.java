package com.boolbalabs.rollit.animators;

import android.os.Bundle;
import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.lib.transitions.SineInterpolator;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.RotatingCubeSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.Axes;

public class RollAnimator extends Animator {
    static final long ROLL_LENGTH__IN_MILLISEC = 400;
    public static boolean cubeWillFallNextStep = false;

    public RollAnimator(RotatingCubeSprite sprite) {
        super(sprite);
        this.animationLength = ROLL_LENGTH__IN_MILLISEC;
        this.interpolator = new SineInterpolator(EasingType.IN);
        this.totalRotation = 90.0f;
        this.totalMovement = 1.0f;
        this.finishSound = R.raw.cube_rolled;
    }

    public void initialize(float[] lastStableMatrix, Axes lastStableAxes, Bundle movementBundle) {
        super.initialize(lastStableMatrix, lastStableAxes, movementBundle);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveEast() {
        this.zrot = (-this.totalRotation) * this.interpolator.getInterpolation(this.progress);
        this.xmove = this.totalMovement * this.interpolator.getInterpolation(this.progress);
        this.ymove = (float) ((0.707106781d * Math.sin(((((double) this.zrot) - 225.0d) * 3.141592653589793d) / 180.0d)) - 0.5d);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveWest() {
        this.zrot = this.totalRotation * this.interpolator.getInterpolation(this.progress);
        this.xmove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        this.ymove = (float) ((0.707106781d * Math.sin((((double) (this.zrot + 45.0f)) * 3.141592653589793d) / 180.0d)) - 0.5d);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveNorth() {
        this.xrot = (-this.totalRotation) * this.interpolator.getInterpolation(this.progress);
        this.zmove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
        this.ymove = (float) ((0.707106781d * Math.sin((((double) (this.xrot - 225.0f)) * 3.141592653589793d) / 180.0d)) - 0.5d);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveSouth() {
        this.xrot = this.totalRotation * this.interpolator.getInterpolation(this.progress);
        this.zmove = this.totalMovement * this.interpolator.getInterpolation(this.progress);
        this.ymove = (float) ((0.707106781d * Math.sin((((double) (this.xrot + 45.0f)) * 3.141592653589793d) / 180.0d)) - 0.5d);
    }

    /* access modifiers changed from: protected */
    public void postAnimationAxesUpdate() {
        switch (this.animationDirection) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                this.tempPoint.set(this.worldAxes.yAxis);
                this.worldAxes.yAxis.set(this.worldAxes.zAxis);
                this.worldAxes.zAxis.set(this.tempPoint.invert());
                return;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                this.tempPoint.set(this.worldAxes.zAxis);
                this.worldAxes.zAxis.set(this.worldAxes.yAxis);
                this.worldAxes.yAxis.set(this.tempPoint.invert());
                return;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                this.tempPoint.set(this.worldAxes.xAxis);
                this.worldAxes.xAxis.set(this.worldAxes.yAxis);
                this.worldAxes.yAxis.set(this.tempPoint.invert());
                return;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                this.tempPoint.set(this.worldAxes.yAxis);
                this.worldAxes.yAxis.set(this.worldAxes.xAxis);
                this.worldAxes.xAxis.set(this.tempPoint.invert());
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void playFinishSound() {
        if (!cubeWillFallNextStep) {
            super.playFinishSound();
        }
    }

    /* access modifiers changed from: protected */
    public void playFinishVibrtaion() {
        if (!cubeWillFallNextStep && ZageCommonSettings.vibroEnabled) {
            this.vibratorManager.vibrate(50);
        }
    }

    /* access modifiers changed from: protected */
    public void notifyAnimationFinished() {
        cubeWillFallNextStep = false;
        super.notifyAnimationFinished();
    }
}
