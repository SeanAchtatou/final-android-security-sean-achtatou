package com.boolbalabs.rollit.animators;

import com.boolbalabs.rollit.gamecomponents.RotatingCubeSprite;
import com.boolbalabs.rollit.gamecomponents.cells.sprites.ButtonCellSprite;

public class ButtonPushAnimator extends Animator {
    public ButtonPushAnimator(RotatingCubeSprite sprite) {
        super(sprite);
        this.animationLength = 800;
    }

    /* access modifiers changed from: protected */
    public void calculateVertivcalAnimation() {
        this.ymove = ButtonCellSprite.getYmove();
    }

    /* access modifiers changed from: protected */
    public void postAnimationAxesUpdate() {
    }

    /* access modifiers changed from: protected */
    public void notifyAnimationFinished() {
        super.notifyAnimationFinished();
    }
}
