package com.boolbalabs.rollit.animators;

import android.opengl.Matrix;
import android.os.Bundle;
import android.view.animation.Interpolator;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.managers.VibratorManager;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.gamecomponents.GameCamera;
import com.boolbalabs.rollit.gamecomponents.RotatingCube;
import com.boolbalabs.rollit.gamecomponents.RotatingCubeSprite;
import com.boolbalabs.rollit.gamecomponents.cells.ButtonCell;
import com.boolbalabs.rollit.gamecomponents.cells.FinishCell;
import com.boolbalabs.rollit.gamecomponents.cells.TeleportCell;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.Axes;
import com.boolbalabs.utility.ExtendedMatrix;
import com.boolbalabs.utility.Point3D;

public abstract class Animator {
    int animationDirection = RollItConstants.DIRECTION_NONE;
    protected long animationLength;
    private final float[] animationVariables = new float[6];
    private RotatingCubeSprite cubeSprite;
    private long currentAnimationTime;
    float[] currentMatrix = new float[16];
    protected int customSound;
    protected boolean customSoundPlayed = false;
    protected int finishSound;
    protected Interpolator interpolator;
    float[] lastStableMatrix = new float[16];
    private boolean mustStopAnimation = false;
    protected float progress;
    private long startAnimationTime;
    protected int startSound;
    Point3D targetCoordinate = null;
    Point3D tempPoint;
    protected float totalMovement;
    protected float totalRotation;
    protected VibratorManager vibratorManager = VibratorManager.getInstance();
    public Axes worldAxes;
    float xmove = 0.0f;
    float xrot = 0.0f;
    float ymove = 0.0f;
    float yrot = 0.0f;
    float zmove = 0.0f;
    float zrot = 0.0f;

    /* access modifiers changed from: protected */
    public abstract void postAnimationAxesUpdate();

    Animator(RotatingCubeSprite sprite) {
        this.cubeSprite = sprite;
        this.tempPoint = new Point3D();
        this.worldAxes = new Axes();
        Matrix.setIdentityM(this.currentMatrix, 0);
    }

    public void initialize(float[] lastStableMatrix2, Axes lastStableAxes, Bundle movementBundle) {
        this.lastStableMatrix = lastStableMatrix2;
        this.worldAxes = lastStableAxes;
        this.animationDirection = movementBundle.getInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE);
    }

    public void startAnimation() {
        playStartSound();
        playStartVibrtaion();
        this.startAnimationTime = System.currentTimeMillis();
    }

    public float[] calculateNext() {
        if (this.mustStopAnimation) {
            playFinishSound();
            playFinishVibrtaion();
            stopAnimation();
        } else {
            this.progress = calculateProgress();
            if (this.progress < 1.0f) {
                loadLastStableMatrix();
                calculateNextFrame();
                translate();
                rotate();
            } else {
                finalizeAnimation();
            }
        }
        return this.currentMatrix;
    }

    private void finalizeAnimation() {
        this.progress = 1.0f;
        loadLastStableMatrix();
        calculateNextFrame();
        translate();
        rotate();
        this.mustStopAnimation = true;
    }

    private void loadLastStableMatrix() {
        System.arraycopy(this.lastStableMatrix, 0, this.currentMatrix, 0, 16);
    }

    private void calculateNextFrame() {
        switch (this.animationDirection) {
            case RollItConstants.DIRECTION_NONE /*62100*/:
                calculateVertivcalAnimation();
                break;
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                calculateMoveNorth();
                break;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                calculateMoveSouth();
                break;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                calculateMoveEast();
                break;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                calculateMoveWest();
                break;
        }
        if (this.customSound != 0 && !this.customSoundPlayed) {
            playCustomSound();
        }
        playCustomVibrtaion();
    }

    private void stopAnimation() {
        postAnimationAxesUpdate();
        resetAnimationVariables();
        saveNewStableMatrix();
        notifyAnimationFinished();
        this.mustStopAnimation = false;
        this.customSoundPlayed = false;
    }

    private void saveNewStableMatrix() {
        System.arraycopy(this.currentMatrix, 0, this.lastStableMatrix, 0, 16);
    }

    private void resetAnimationVariables() {
        this.xrot = 0.0f;
        this.yrot = 0.0f;
        this.zrot = 0.0f;
        this.xmove = 0.0f;
        this.ymove = 0.0f;
        this.zmove = 0.0f;
        this.animationDirection = RollItConstants.DIRECTION_NONE;
    }

    /* access modifiers changed from: protected */
    public void notifyAnimationFinished() {
        this.cubeSprite.applyAnimationFinishedNotification();
        GameCamera.getInstance().followTheObject(RotatingCube.currentCoordinates);
        if (!(this instanceof ButtonPushAnimator)) {
            ButtonCell.enable();
        }
        if (!(this instanceof TeleportAnimator)) {
            TeleportCell.enable();
        }
        if (!(this instanceof LevelCompleteAnimator)) {
            FinishCell.enable();
        }
    }

    public void translate() {
        Matrix.translateM(this.currentMatrix, 0, this.xmove * this.worldAxes.xAxis.x, this.xmove * this.worldAxes.xAxis.y, this.xmove * this.worldAxes.xAxis.z);
        Matrix.translateM(this.currentMatrix, 0, this.ymove * this.worldAxes.yAxis.x, this.ymove * this.worldAxes.yAxis.y, this.ymove * this.worldAxes.yAxis.z);
        Matrix.translateM(this.currentMatrix, 0, this.zmove * this.worldAxes.zAxis.x, this.zmove * this.worldAxes.zAxis.y, this.zmove * this.worldAxes.zAxis.z);
    }

    public void rotate() {
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.xrot, this.worldAxes.xAxis.x, this.worldAxes.xAxis.y, this.worldAxes.xAxis.z);
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.yrot, this.worldAxes.yAxis.x, this.worldAxes.yAxis.y, this.worldAxes.yAxis.z);
        ExtendedMatrix.rotateM(this.currentMatrix, 0, this.zrot, this.worldAxes.zAxis.x, this.worldAxes.zAxis.y, this.worldAxes.zAxis.z);
    }

    public float[] getLastStableMatrix() {
        return this.lastStableMatrix;
    }

    public boolean isRunning() {
        return this.animationDirection != 62100;
    }

    /* access modifiers changed from: protected */
    public void calculateMoveEast() {
    }

    /* access modifiers changed from: protected */
    public void calculateMoveWest() {
    }

    /* access modifiers changed from: protected */
    public void calculateMoveSouth() {
    }

    /* access modifiers changed from: protected */
    public void calculateMoveNorth() {
    }

    /* access modifiers changed from: protected */
    public void calculateVertivcalAnimation() {
    }

    /* access modifiers changed from: protected */
    public void playFinishSound() {
        SoundManager sm = SoundManager.getInstance();
        if (sm != null && this.finishSound != 0 && ZageCommonSettings.soundEnabled) {
            sm.playShortSound(this.finishSound);
        }
    }

    /* access modifiers changed from: protected */
    public void playStartSound() {
        SoundManager sm = SoundManager.getInstance();
        if (sm != null && this.startSound != 0 && ZageCommonSettings.soundEnabled) {
            sm.playShortSound(this.startSound);
        }
    }

    /* access modifiers changed from: protected */
    public void playCustomSound() {
        this.customSoundPlayed = true;
        SoundManager sm = SoundManager.getInstance();
        if (sm != null && this.customSound != 0 && ZageCommonSettings.soundEnabled) {
            sm.playShortSound(this.customSound);
        }
    }

    /* access modifiers changed from: protected */
    public void playStartVibrtaion() {
    }

    /* access modifiers changed from: protected */
    public void playCustomVibrtaion() {
    }

    /* access modifiers changed from: protected */
    public void playFinishVibrtaion() {
    }

    public void reset() {
        resetAnimationVariables();
    }

    public float[] getAimationVariables() {
        this.animationVariables[0] = this.xmove;
        this.animationVariables[1] = this.ymove;
        this.animationVariables[2] = this.zmove;
        this.animationVariables[3] = this.xrot;
        this.animationVariables[4] = this.yrot;
        this.animationVariables[5] = this.zrot;
        return this.animationVariables;
    }

    /* access modifiers changed from: protected */
    public float calculateProgress() {
        this.currentAnimationTime = System.currentTimeMillis() - this.startAnimationTime;
        return ((float) this.currentAnimationTime) / ((float) this.animationLength);
    }

    public RotatingCubeSprite getCubeSprite() {
        return this.cubeSprite;
    }
}
