package com.boolbalabs.rollit.animators;

import android.os.Bundle;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.GameField;
import com.boolbalabs.rollit.gamecomponents.RotatingCubeSprite;
import com.boolbalabs.rollit.gamecomponents.cells.CatapultCell;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.gamecomponents.cells.EmptyCell;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.Axes;
import com.boolbalabs.utility.Point3D;

public class CatapultAnimator extends Animator {
    public static final float CUBE_THROW_ANIMATION_LENGTH_COEFF = 0.7f;
    private static final float THROW_INTO_VOID_ANIMATION_LENGTH_COEFF = 2.0f;
    private float a = 0.0f;
    private float b = 0.0f;
    private float c = 0.0f;
    private Cell targetCell;
    private boolean throwIntoVoid = false;
    private int throwStrength = 1;
    private Point3D throwVector = new Point3D();

    public CatapultAnimator(RotatingCubeSprite sprite) {
        super(sprite);
        this.finishSound = R.raw.cube_rolled;
        this.customSound = R.raw.cube_falls;
    }

    public void initialize(float[] lastStableMatrix, Axes lastStableAxes, Bundle movementBundle) {
        setAnimationParameters(movementBundle);
        super.initialize(lastStableMatrix, lastStableAxes, movementBundle);
    }

    private void setAnimationParameters(Bundle movementBundle) {
        this.targetCell = GameField.getInstance().getCell((Point3D) movementBundle.getSerializable(RollItConstants.KEY_TARGET_COORDS));
        this.throwVector = (Point3D) movementBundle.getSerializable(RollItConstants.KEY_THROW_VECTOR);
        this.throwStrength = (int) Math.abs(this.throwVector.x + this.throwVector.z);
        float[] coefficients = (float[]) movementBundle.getSerializable(RollItConstants.KEY_PARABOLA_COEFFS);
        this.a = coefficients[0];
        this.b = coefficients[1];
        this.c = coefficients[2];
        this.animationLength = (long) (((float) this.throwStrength) * 0.7f * 400.0f);
        this.totalMovement = ((float) this.throwStrength) * 1.0f;
        this.totalRotation = ((float) this.throwStrength) * 90.0f;
        if (this.targetCell instanceof EmptyCell) {
            this.throwIntoVoid = true;
            this.animationLength = (long) (((float) this.animationLength) * 2.0f);
            this.totalMovement *= 2.0f;
            this.totalRotation *= 2.0f;
        }
    }

    /* access modifiers changed from: protected */
    public void calculateMoveNorth() {
        this.xrot = (-this.totalRotation) * this.progress;
        this.zmove = (-this.totalMovement) * this.progress;
        this.ymove = parabola(this.zmove);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveSouth() {
        this.xrot = this.totalRotation * this.progress;
        this.zmove = this.totalMovement * this.progress;
        this.ymove = parabola(this.zmove);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveWest() {
        this.zrot = this.totalRotation * this.progress;
        this.xmove = (-this.totalMovement) * this.progress;
        this.ymove = parabola(this.xmove);
    }

    /* access modifiers changed from: protected */
    public void calculateMoveEast() {
        this.zrot = (-this.totalRotation) * this.progress;
        this.xmove = this.totalMovement * this.progress;
        this.ymove = parabola(this.xmove);
    }

    private float parabola(float x) {
        return (this.a * x * x) + (this.b * x) + this.c;
    }

    /* access modifiers changed from: protected */
    public void playFinishSound() {
        if (!this.throwIntoVoid) {
            super.playFinishSound();
        }
    }

    /* access modifiers changed from: protected */
    public void playCustomSound() {
        if (this.throwIntoVoid && this.progress > 0.5f) {
            super.playCustomSound();
        }
    }

    /* access modifiers changed from: protected */
    public void playFinishVibrtaion() {
        if (!this.throwIntoVoid && ZageCommonSettings.vibroEnabled) {
            this.vibratorManager.vibrate(50);
        }
    }

    /* access modifiers changed from: protected */
    public void postAnimationAxesUpdate() {
        for (int i = 0; i < this.throwStrength; i++) {
            updateAxesAfter90Degrees();
        }
    }

    private void updateAxesAfter90Degrees() {
        switch (this.animationDirection) {
            case RollItConstants.DIRECTION_NORTH /*62101*/:
                this.tempPoint.set(this.worldAxes.yAxis);
                this.worldAxes.yAxis.set(this.worldAxes.zAxis);
                this.worldAxes.zAxis.set(this.tempPoint.invert());
                return;
            case RollItConstants.DIRECTION_SOUTH /*62102*/:
                this.tempPoint.set(this.worldAxes.zAxis);
                this.worldAxes.zAxis.set(this.worldAxes.yAxis);
                this.worldAxes.yAxis.set(this.tempPoint.invert());
                return;
            case RollItConstants.DIRECTION_EAST /*62103*/:
                this.tempPoint.set(this.worldAxes.xAxis);
                this.worldAxes.xAxis.set(this.worldAxes.yAxis);
                this.worldAxes.yAxis.set(this.tempPoint.invert());
                return;
            case RollItConstants.DIRECTION_WEST /*62104*/:
                this.tempPoint.set(this.worldAxes.yAxis);
                this.worldAxes.yAxis.set(this.worldAxes.xAxis);
                this.worldAxes.xAxis.set(this.tempPoint.invert());
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void notifyAnimationFinished() {
        super.notifyAnimationFinished();
        CatapultCell.deactivate();
        if (this.throwIntoVoid) {
            GameField.getInstance().performAction(RollItConstants.ACTION_RESTART, null);
            this.throwIntoVoid = false;
        }
    }
}
