package com.boolbalabs.rollit.animators;

import com.boolbalabs.lib.transitions.CubicInterpolator;
import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.RotatingCubeSprite;
import com.boolbalabs.rollit.gamecomponents.cells.ShiftingCell;

public class ShiftAnimator extends Animator {
    public static final int SHIFT_LENGTH_IN_MILLISEC = 400;

    public ShiftAnimator(RotatingCubeSprite sprite) {
        super(sprite);
        this.animationLength = 400;
        this.totalMovement = 1.0f;
        this.interpolator = new CubicInterpolator(EasingType.IN);
        this.startSound = R.raw.shift_sound;
    }

    public void calculateMoveEast() {
        this.xmove = this.totalMovement * this.interpolator.getInterpolation(this.progress);
    }

    public void calculateMoveWest() {
        this.xmove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
    }

    public void calculateMoveSouth() {
        this.zmove = this.totalMovement * this.interpolator.getInterpolation(this.progress);
    }

    public void calculateMoveNorth() {
        this.zmove = (-this.totalMovement) * this.interpolator.getInterpolation(this.progress);
    }

    /* access modifiers changed from: protected */
    public void postAnimationAxesUpdate() {
    }

    /* access modifiers changed from: protected */
    public void notifyAnimationFinished() {
        super.notifyAnimationFinished();
        ShiftingCell.deactivate();
    }
}
