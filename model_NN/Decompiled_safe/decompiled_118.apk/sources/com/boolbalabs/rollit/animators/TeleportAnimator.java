package com.boolbalabs.rollit.animators;

import android.os.Bundle;
import com.boolbalabs.lib.transitions.BackInterpolator;
import com.boolbalabs.lib.transitions.EasingType;
import com.boolbalabs.rollit.R;
import com.boolbalabs.rollit.gamecomponents.RotatingCubeSprite;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.utility.Axes;
import com.boolbalabs.utility.Point3D;

public class TeleportAnimator extends Animator {
    private float heightDifference = 0.0f;
    private Point3D startCoordinate;
    private Point3D targetCoordinate;
    private boolean teleported = false;

    public TeleportAnimator(RotatingCubeSprite sprite) {
        super(sprite);
        this.animationLength = 1200;
        this.interpolator = new BackInterpolator(EasingType.OUT, 5.0f);
        this.totalMovement = 1.0f;
        this.customSound = R.raw.teleport_middle;
    }

    public void initialize(float[] lastStableMatrix, Axes lastStableAxes, Bundle movementBundle) {
        super.initialize(lastStableMatrix, lastStableAxes, movementBundle);
        this.startCoordinate = (Point3D) movementBundle.getSerializable(RollItConstants.KEY_CURR_COORDS);
        this.targetCoordinate = (Point3D) movementBundle.getSerializable(RollItConstants.KEY_TARGET_COORDS);
        this.heightDifference = this.targetCoordinate.y - this.startCoordinate.y;
    }

    /* access modifiers changed from: protected */
    public void calculateVertivcalAnimation() {
        if (this.progress <= 0.33f) {
            this.ymove = this.totalMovement * this.interpolator.getInterpolation(this.progress * 3.0f);
        }
        if (this.progress >= 0.5f && !this.teleported) {
            this.xmove += this.targetCoordinate.x - this.lastStableMatrix[12];
            this.ymove += this.heightDifference;
            this.zmove += this.targetCoordinate.z - this.lastStableMatrix[14];
            this.teleported = true;
        }
        if (this.progress >= 0.66f) {
            this.ymove = this.heightDifference + (this.totalMovement * this.interpolator.getInterpolation((1.0f - this.progress) * 3.0f));
        }
    }

    /* access modifiers changed from: protected */
    public void postAnimationAxesUpdate() {
    }

    /* access modifiers changed from: protected */
    public void playCustomSound() {
        if (this.progress > 0.4f) {
            super.playCustomSound();
        }
    }

    /* access modifiers changed from: protected */
    public void notifyAnimationFinished() {
        super.notifyAnimationFinished();
        this.teleported = false;
    }
}
