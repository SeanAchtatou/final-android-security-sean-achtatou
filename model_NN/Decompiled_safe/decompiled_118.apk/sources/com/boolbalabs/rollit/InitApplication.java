package com.boolbalabs.rollit;

import android.app.Application;
import com.boolbalabs.rollit.settings.Settings;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(formKey = "dGZsU01WNVhFYkMwcFVpeTZ3N0FRUnc6MQ")
public class InitApplication extends Application {
    public void onCreate() {
        ACRA.init(this);
        ScoreloopManagerSingleton.init(this, Settings.SCORELOOP_GAME_SECRET);
        super.onCreate();
    }

    public void onTerminate() {
        super.onTerminate();
        ScoreloopManagerSingleton.destroy();
    }
}
