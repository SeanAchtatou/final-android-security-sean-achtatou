package com.boolbalabs.rollit;

import android.graphics.Point;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import com.boolbalabs.lib.game.Game;
import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.managers.VibratorManager;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import com.boolbalabs.rollit.gamecomponents.BackgroundQuad;
import com.boolbalabs.rollit.gamecomponents.DialogLevelResults;
import com.boolbalabs.rollit.gamecomponents.GameCamera;
import com.boolbalabs.rollit.gamecomponents.GameField;
import com.boolbalabs.rollit.gamecomponents.RotatingCube;
import com.boolbalabs.rollit.gamecomponents.cells.Cell;
import com.boolbalabs.rollit.gamecomponents.cells.EmptyCell;
import com.boolbalabs.rollit.gamecomponents.two_d.InteractionBoard;
import com.boolbalabs.rollit.gamecomponents.two_d.ScoreBoard;
import com.boolbalabs.rollit.gamecomponents.two_d.WrongSideToast;
import com.boolbalabs.rollit.level.Level;
import com.boolbalabs.rollit.level.LevelLoader;
import com.boolbalabs.rollit.level.LevelSolver;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;
import com.boolbalabs.utility.GameStatic;
import com.boolbalabs.utility.Point3D;
import javax.microedition.khronos.opengles.GL10;

public class SceneGameplay extends GameScene {
    public static final float[] LIGHTING_OFF = {1.0f, 1.0f, 1.0f, 1.0f};
    public static final float[] LIGHTING_ON = {0.0f, 0.0f, 0.0f, 1.0f};
    public static boolean cameraMoving = false;
    private static int currentLevelTheme = RollItConstants.TEXTURE_THEME_GRASS;
    public static String themePrefix = RollItConstants.PREFIX_TEXTURE_THEME_GRASS;
    private Level currentLevel;
    private DialogLevelResults dialogResults;
    private GameCamera gameCamera;
    private GameField gameField;
    private InteractionBoard interactionBoard;
    private BackgroundQuad levelBackground;
    private final Bundle levelResultBundle = new Bundle();
    private LevelSolver levelSolver;
    private RotatingCube rotatingCube;
    private ScoreBoard scoreBoard;
    private final Bundle tempBundle = GameStatic.makeBundle();
    private WrongSideToast wrongSideToast;

    public SceneGameplay(Game game, int sceneId) {
        super(game, sceneId);
        createGameComponents();
    }

    public void initialize() {
        findGameplayComponents();
        initializeGameComponents();
    }

    private void findGameplayComponents() {
        this.levelBackground = (BackgroundQuad) findComponentByClass(BackgroundQuad.class);
        this.rotatingCube = (RotatingCube) findComponentByClass(RotatingCube.class);
        this.gameField = (GameField) findComponentByClass(GameField.class);
        this.gameCamera = (GameCamera) findComponentByClass(GameCamera.class);
        this.interactionBoard = (InteractionBoard) findComponentByClass(InteractionBoard.class);
        this.scoreBoard = (ScoreBoard) findComponentByClass(ScoreBoard.class);
        this.dialogResults = (DialogLevelResults) findComponentByClass(DialogLevelResults.class);
        this.wrongSideToast = (WrongSideToast) findComponentByClass(WrongSideToast.class);
        this.levelSolver = (LevelSolver) findComponentByClass(LevelSolver.class);
    }

    public void initializeGameComponents() {
        this.levelBackground.initialize();
        this.scoreBoard.initialize();
        this.rotatingCube.initialize();
        this.gameCamera.initialize();
        this.interactionBoard.initialize();
        this.dialogResults.initialize();
        this.wrongSideToast.initialize();
        this.levelSolver.initialize();
    }

    private void createGameComponents() {
        GameField gameField2 = GameField.getInstance();
        BackgroundQuad gameBackground = new BackgroundQuad();
        ScoreBoard scoreBoard2 = new ScoreBoard();
        RotatingCube cube = new RotatingCube();
        DialogLevelResults levelResults = new DialogLevelResults();
        GameCamera gameCamera2 = GameCamera.getInstance();
        InteractionBoard interactionBoard2 = new InteractionBoard(gameCamera2);
        WrongSideToast wrongSideToast2 = new WrongSideToast();
        LevelSolver levelSolver2 = new LevelSolver();
        registerGameComponent(gameBackground);
        registerGameComponent(gameCamera2);
        registerGameComponent(gameField2);
        registerGameComponent(cube);
        registerGameComponent(levelResults);
        registerGameComponent(scoreBoard2);
        registerGameComponent(wrongSideToast2);
        registerGameComponent(interactionBoard2);
        registerGameComponent(levelSolver2);
    }

    public void performAction(int actionCode, Bundle actionParameters) {
        switch (actionCode) {
            case RollItConstants.ACTION_PLAY_NEXT_LEVEL /*53624*/:
                playNextLevel();
                return;
            case RollItConstants.ACTION_ENABLE_USER_INPUT /*73906*/:
                enableUserInput();
                return;
            case RollItConstants.ACTION_START_SOLVING /*75487*/:
                startSolvingTheLevel();
                return;
            case RollItConstants.ACTION_SHOW_WRONG_SIDE_TOAST /*75936*/:
                showWrongSideToast();
                return;
            case RollItConstants.ACTION_HIDE_WRONG_SIDE_TOAST /*75937*/:
                hideWrongSideToast();
                return;
            case RollItConstants.ACTION_WIN_LEVEL /*75945*/:
                winLevel(actionParameters);
                return;
            case RollItConstants.ACTION_RESTART /*76205*/:
                restartLevel();
                restartTime();
                return;
            case RollItConstants.ACTION_INCREASE_STEP_NUMBER /*93125*/:
                increaseStepNumber(actionParameters);
                return;
            case RollItConstants.ACTION_ON_STEP_COMPLETED /*93127*/:
                onStepCompleted();
                return;
            case RollItConstants.ACTION_SHOW_LEVEL_RESULTS /*93628*/:
                showLevelResuts();
                return;
            case RollItConstants.ACTION_SELECT_MOVEMENT_TYPE /*94000*/:
                selectCubeMoveType(actionParameters);
                return;
            case RollItConstants.ACTION_FALL /*94001*/:
                makeCubeFall(null);
                return;
            case RollItConstants.ACTION_NORMAL_ROLL /*94002*/:
                makeCubeRoll(actionParameters);
                return;
            case RollItConstants.ACTION_SHIFT /*94003*/:
                shiftCube(actionParameters);
                return;
            case RollItConstants.ACTION_TELEPORT /*94004*/:
                teleportCube(actionParameters);
                return;
            case RollItConstants.ACTION_THROW /*94005*/:
                throwCube(actionParameters);
                return;
            case RollItConstants.ACTION_PUSH_BUTTON /*94006*/:
                makeCubePushButton(actionParameters);
                return;
            default:
                return;
        }
    }

    public void onAfterLoad() {
        super.onAfterLoad();
        refreshTexturesTheme();
    }

    public void onCreate() {
        DebugLog.v("SceneGP", "onCreate called");
        super.onCreate();
    }

    public void onResume() {
        DebugLog.v("SceneGP", "onResume called");
        enableUserInput();
        resetTouchables();
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_HIDE_LOADING_TOAST);
        this.currentLevel.resumeLevelTimer();
    }

    public void onShow() {
        if (isSceneInitialized()) {
            DebugLog.v("SceneGP", "onShow called");
            initializeLevel();
            if (this.dialogResults != null) {
                this.dialogResults.hide();
            }
            restartLevel();
            enableUserInput();
            resetTouchables();
            this.currentLevel.askToShowInGameHelpIfAny();
            sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_ADS);
        }
    }

    public void onHide() {
        DebugLog.v("SceneGP", "onHide called");
        this.currentLevel.stopLevelTimer();
        this.levelSolver.stopSolving();
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_HIDE_ADS);
    }

    public void onPause() {
        DebugLog.v("SceneGP", "onPause called");
        this.currentLevel.pauseLevelTimer();
        this.levelSolver.stopSolving();
    }

    public void onDestroy() {
        DebugLog.v("SceneGP", "onDestroy called");
        super.onDestroy();
    }

    public void draw(GL10 gl) {
        gl.glTexEnvf(8960, 8704, 8448.0f);
        this.gameCamera.applyGlobalLightPosition();
        super.draw(gl);
    }

    public void update() {
        checkCameraMoving();
        super.update();
    }

    public void refreshTexturesTheme() {
        switch (currentLevelTheme) {
            case RollItConstants.TEXTURE_THEME_GRASS /*54001*/:
                themePrefix = RollItConstants.PREFIX_TEXTURE_THEME_GRASS;
                return;
            case RollItConstants.TEXTURE_THEME_ICE /*54002*/:
                themePrefix = RollItConstants.PREFIX_TEXTURE_THEME_ICE;
                return;
            case RollItConstants.TEXTURE_THEME_SAND /*54003*/:
                themePrefix = RollItConstants.PREFIX_TEXTURE_THEME_SAND;
                return;
            default:
                Log.e("Roll It", "SceneGameplay.refreshTexturesTheme :: Theme: " + currentLevelTheme + " does not exist!");
                themePrefix = RollItConstants.PREFIX_TEXTURE_THEME_GRASS;
                return;
        }
    }

    private void winLevel(Bundle movementBundle) {
        disableUserInput();
        this.currentLevel.calculateLevelResults(this.scoreBoard);
        DebugLog.v("SceneGP", "Level: " + this.currentLevel.getID() + "; Score: " + this.currentLevel.getCurrentScore());
        Settings.askToIncreaseMaxAvalibleLevel();
        this.rotatingCube.addMovement(movementBundle);
    }

    private void showLevelResuts() {
        createLevelResultBundle();
        this.dialogResults.show(this.levelResultBundle);
        System.gc();
    }

    private void hideWrongSideToast() {
        this.wrongSideToast.hide();
    }

    private void showWrongSideToast() {
        this.wrongSideToast.show();
    }

    private void createLevelResultBundle() {
        this.levelResultBundle.putInt(RollItConstants.KEY_CURRENT_STEPS, this.scoreBoard.getStepNumber());
        this.levelResultBundle.putInt(RollItConstants.KEY_BEST_STEPS, this.currentLevel.getBestStepNumber());
        this.levelResultBundle.putInt(RollItConstants.KEY_CURRENT_SCORE, this.currentLevel.getCurrentScore());
        this.levelResultBundle.putInt(RollItConstants.KEY_BEST_SCORE, this.currentLevel.getBestScore());
        this.levelResultBundle.putBoolean(RollItConstants.KEY_STEPS_IMPROVED, this.currentLevel.isStepNumberImproved());
        this.levelResultBundle.putBoolean(RollItConstants.KEY_SCORE_IMPROVED, this.currentLevel.isScoreImproved());
        this.levelResultBundle.putBoolean(RollItConstants.KEY_WAS_COMPLETED_BEFORE, this.currentLevel.wasCompletedBefore());
        this.levelResultBundle.putBoolean(RollItConstants.KEY_SOLVE_BUTTON_USED, this.currentLevel.solveButtonUsed());
        this.levelResultBundle.putInt(RollItConstants.KEY_TOTAL_STEPS_OF_CURRENT_PACK, Settings.totalSteps);
        this.levelResultBundle.putInt(RollItConstants.KEY_TOTAL_SCORE_OF_CURRENT_PACK, Settings.totalScore);
        this.levelResultBundle.putInt(RollItConstants.KEY_CURRENT_LEVEL, this.currentLevel.getID());
    }

    private void playNextLevel() {
        Settings.increaseCurrentLevel();
        if (Settings.currentLevel > Settings.currentLevelPackSize) {
            DebugLog.v("SceneGameplay", "Game completed");
            sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_DIALOG_GAME_COMPLETED);
            return;
        }
        LevelLoader.getInstance().askToLoadLevel(Settings.getCurrentLevel());
        initializeLevel();
        restartLevel();
        DebugLog.v("Roll It", "LVL won! Next: " + this.currentLevel);
        this.currentLevel.askToShowInGameHelpIfAny();
    }

    private void startSolvingTheLevel() {
        try {
            this.levelSolver.initialaizeWithSolution(this.currentLevel.getSolution());
            restartLevel();
            prepareForSolving();
            this.levelSolver.startSolving();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private void prepareForSolving() {
        this.currentLevel.setSolveButtonUsed();
        disableUserInput();
        this.interactionBoard.hideSolveButton();
    }

    private void increaseStepNumber(Bundle actionParameters) {
        if (this.scoreBoard != null) {
            this.scoreBoard.increaseStepsNumber();
        }
    }

    private void onStepCompleted() {
        boolean solveButtonAvailable = this.currentLevel.isSolveButtonAvailable();
        int solveButtonShowStepOffset = this.currentLevel.getSolveButtonStepOffset();
        if (solveButtonAvailable && this.rotatingCube.userInteractionEnabled && !this.interactionBoard.isSolveButtonVisible() && this.scoreBoard.getStepNumber() > solveButtonShowStepOffset) {
            Log.v("SOLVE", "Solve button timer started");
            this.interactionBoard.startSolveButtonShowTimer();
        }
    }

    private void makeCubeRoll(Bundle movementBundle) {
        movementBundle.putSerializable(RollItConstants.KEY_TARGET_COORDS, this.gameField.getTargetCell(RotatingCube.currentCoordinates, movementBundle.getInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE), 1).coordinate);
        movementBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_ROLL);
        this.rotatingCube.addMovement(movementBundle);
    }

    private void makeCubeFall(Bundle movementBundle) {
        this.tempBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_FALL);
        if (movementBundle == null) {
            this.tempBundle.putInt(RollItConstants.KEY_DIRECTION, RotatingCube.getDirection());
        } else {
            this.tempBundle.putInt(RollItConstants.KEY_DIRECTION, movementBundle.getInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE));
        }
        this.rotatingCube.addMovement(this.tempBundle);
    }

    private void makeCubeClimbUp(Bundle movementBundle) {
        int direction = movementBundle.getInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE);
        this.tempBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_CLIMB_UP);
        this.tempBundle.putInt(RollItConstants.KEY_DIRECTION, direction);
        this.rotatingCube.addMovement(this.tempBundle);
    }

    private void makeCubeClimbDown(Bundle movementBundle) {
        int direction = movementBundle.getInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE);
        this.tempBundle.putInt(RollItConstants.KEY_MOVEMENT, RollItConstants.MOVEMENT_CLIMB_DOWN);
        this.tempBundle.putInt(RollItConstants.KEY_DIRECTION, direction);
        this.rotatingCube.addMovement(this.tempBundle);
    }

    private void shiftCube(Bundle movementBundle) {
        this.rotatingCube.addMovement(movementBundle);
        Cell currentCell = findCurrentCell();
        Cell nextCell = getNextCell(movementBundle);
        if ((nextCell instanceof EmptyCell) || nextCell.getCellHeight() - currentCell.getCellHeight() > 1) {
            makeCubeFall(movementBundle);
        }
    }

    private void teleportCube(Bundle movementBundle) {
        this.rotatingCube.addMovement(movementBundle);
    }

    private void throwCube(Bundle movementBundle) {
        this.rotatingCube.addMovement(movementBundle);
    }

    private void makeCubePushButton(Bundle movementBundle) {
        this.gameField.getCell((Point3D) movementBundle.getSerializable(RollItConstants.KEY_TARGET_COORDS)).switchState();
        this.rotatingCube.addMovement(movementBundle);
    }

    private void selectCubeMoveType(Bundle movementBundle) {
        Cell nextCell = getNextCell(movementBundle);
        Cell currentCell = findCurrentCell();
        if (currentCell == null || nextCell == null) {
            Log.e("Roll It", "SceneGameplay.selectCubeMovetype :: Current or next cell is NULL!");
            return;
        }
        boolean stepSuccessful = false;
        if ((nextCell instanceof EmptyCell) || nextCell.getCellHeight() - currentCell.getCellHeight() > 1) {
            playFallNotAllowedSoundAndVibration();
        } else if (nextCell.getCellHeight() - currentCell.getCellHeight() == 0) {
            makeCubeRoll(movementBundle);
            stepSuccessful = true;
        } else if (nextCell.getCellHeight() - currentCell.getCellHeight() == 1) {
            makeCubeClimbUp(movementBundle);
            stepSuccessful = true;
        } else if (nextCell.getCellHeight() - currentCell.getCellHeight() == -1) {
            makeCubeClimbDown(movementBundle);
            stepSuccessful = true;
        } else {
            playFallNotAllowedSoundAndVibration();
        }
        if (stepSuccessful) {
            performAction(RollItConstants.ACTION_INCREASE_STEP_NUMBER, null);
        }
    }

    private void playFallNotAllowedSoundAndVibration() {
        if (ZageCommonSettings.vibroEnabled) {
            VibratorManager.getInstance().vibrate(150);
        }
        if (ZageCommonSettings.soundEnabled) {
            SoundManager.getInstance().playShortSound(R.raw.fall_not_allowed);
        }
    }

    private void restartLevel() {
        DebugLog.v("SceneGP", "restartLevel called");
        this.rotatingCube.onLevelRestart();
        this.gameField.onLevelRestart();
        this.gameCamera.onLevelRestart();
        this.interactionBoard.onLevelRestart();
        this.scoreBoard.onLevelRestart();
        this.scoreBoard.initValues(this.currentLevel.getLevelNumber(), this.currentLevel.getBestStepNumber());
        this.currentLevel.restart();
        enableUserInput();
    }

    private void restartTime() {
        this.currentLevel.startLevelTimer();
        DebugLog.v("ScepeGP", "LevelTimer STARTED from ACTION_RESET");
    }

    private Cell findCurrentCell() {
        return this.gameField.getCurrentCell();
    }

    public Cell getNextCell(Bundle movementBundle) {
        int direction = movementBundle.getInt(RollItConstants.KEY_DIRECTION, RollItConstants.DIRECTION_NONE);
        int distance = movementBundle.getInt(RollItConstants.KEY_STRENGTH, 1);
        return this.gameField.getTargetCell((Point3D) movementBundle.getSerializable(RollItConstants.KEY_CURR_COORDS), direction, distance);
    }

    public void disableUserInput() {
        this.rotatingCube.userInteractionEnabled = false;
        this.gameCamera.userInteractionEnabled = false;
        this.interactionBoard.userInteractionEnabled = false;
    }

    private void enableUserInput() {
        this.rotatingCube.userInteractionEnabled = true;
        this.gameCamera.userInteractionEnabled = true;
        this.interactionBoard.userInteractionEnabled = true;
    }

    private void resetTouchables() {
        RotatingCube.state = RollItConstants.STATE_READY_TO_ACT;
        GameCamera.state = RollItConstants.STATE_READY_TO_ACT;
    }

    private void initializeLevel() {
        this.currentLevel.loadInfoFromPreferences();
        this.scoreBoard.initValues(this.currentLevel.getLevelNumber(), this.currentLevel.getBestStepNumber());
    }

    public void checkCameraMoving() {
        cameraMoving = this.gameCamera.animationRunning();
    }

    public RotatingCube getRotatingCube() {
        return this.rotatingCube;
    }

    public GameField getGameField() {
        return this.gameField;
    }

    public GameCamera getGameCamera() {
        return this.gameCamera;
    }

    public InteractionBoard getCameraSlider() {
        return this.interactionBoard;
    }

    public BackgroundQuad getLvlBackground() {
        return this.levelBackground;
    }

    public void setTextureTheme(int levelTextureTheme) {
        currentLevelTheme = levelTextureTheme;
        refreshTexturesTheme();
    }

    public void setCurrentLevel(Level requestedLevel) {
        this.currentLevel = requestedLevel;
    }

    public Level getCurrentLevel() {
        return this.currentLevel;
    }

    public void toggleCameraSlider() {
        this.interactionBoard.toggleCameraSliderState();
    }

    /* access modifiers changed from: protected */
    public boolean onTouchDown(Point touchDownPoint) {
        DebugLog.i("Touch", "DOWN: " + touchDownPoint);
        return super.onTouchDown(touchDownPoint);
    }

    /* access modifiers changed from: protected */
    public boolean onTouchMove(Point touchDownPoint, Point currentPoint) {
        DebugLog.i("Touch", "MOVE: " + touchDownPoint + "; " + currentPoint);
        return super.onTouchMove(touchDownPoint, currentPoint);
    }

    /* access modifiers changed from: protected */
    public boolean onTouchUp(Point touchDownPoint, Point touchUpPoint) {
        DebugLog.i("Touch", "UP: " + touchDownPoint + "; " + touchUpPoint);
        return super.onTouchUp(touchDownPoint, touchUpPoint);
    }

    private void changeAdsViewGravity(int gravity) {
        Message msg = new Message();
        msg.what = RollItConstants.MESSAGE_CHANGE_ADS_GRAVITY;
        msg.arg1 = gravity;
        sendMessageToActivity(msg);
    }
}
