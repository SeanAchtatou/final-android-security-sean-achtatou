package com.boolbalabs.rollit;

import android.os.Bundle;
import android.os.Message;
import com.boolbalabs.lib.game.Game;
import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.lib.utils.ErrorReporter;
import com.boolbalabs.rollit.level.LevelManager;
import com.boolbalabs.rollit.menucomponents.MainMenu;
import com.boolbalabs.rollit.settings.RollItConstants;
import com.boolbalabs.rollit.settings.Settings;

public class SceneGameMenu extends GameScene {
    private boolean hasCrashed = false;
    private MainMenu mainMenu;

    public SceneGameMenu(Game game, int sceneId) {
        super(game, sceneId);
        registerGameComponent(new MainMenu());
    }

    public void initialize() {
        this.mainMenu = (MainMenu) findComponentByClass(MainMenu.class);
        this.mainMenu.initialize();
    }

    public void performAction(int actionCode, Bundle actionParameters) {
        switch (actionCode) {
            case RollItConstants.ACTION_PLAY /*75489*/:
                showChooseLevelMenu();
                return;
            case RollItConstants.ACTION_EXIT_APPLICATION /*78965*/:
                sendEmptyMessageToActivity(RollItConstants.MESSAGE_EXIT_APPLICATION);
                return;
            default:
                return;
        }
    }

    public void onResume() {
        this.mainMenu.userInteractionEnabled = true;
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_HIDE_LOADING_TOAST);
    }

    public void onShow() {
        this.mainMenu.onShow();
        checkRateMe();
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_ADS);
    }

    public void onHide() {
        this.mainMenu.onHide();
        super.onHide();
        sendEmptyMessageToActivity(RollItConstants.MESSAGE_HIDE_ADS);
    }

    private void checkRateMe() {
        if (Settings.currentLaunch > 10 && !this.hasCrashed && !Settings.rateMeDialogHasBeenShown) {
            sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_DIALOG_RATEME);
        }
    }

    @Deprecated
    private void checkCrash() {
        this.hasCrashed = ErrorReporter.getInstance().hasCrashed();
        if (this.hasCrashed) {
            sendEmptyMessageToActivity(RollItConstants.MESSAGE_SHOW_DIALOG_CRASH);
        }
    }

    private void showChooseLevelpackMenu() {
        this.mainGame.switchGameScene(5);
    }

    private void showChooseLevelMenu() {
        Settings.currentLevelPackID = RollItConstants.LEVEL_PACK_GRASS.intValue();
        LevelManager.getInstance().setCurrentLevelPack(Settings.currentLevelPackID);
        ((SceneChooseLevel) ((RollItGame) this.mainGame).getGameScene(3)).loadLevelpack(Settings.currentLevelPackID);
        this.mainGame.switchGameScene(3);
    }

    public void disableUserInput() {
        this.mainMenu.userInteractionEnabled = false;
    }

    private void changeAdsViewGravity(int gravity) {
        Message msg = new Message();
        msg.what = RollItConstants.MESSAGE_CHANGE_ADS_GRAVITY;
        msg.arg1 = gravity;
        sendMessageToActivity(msg);
    }
}
