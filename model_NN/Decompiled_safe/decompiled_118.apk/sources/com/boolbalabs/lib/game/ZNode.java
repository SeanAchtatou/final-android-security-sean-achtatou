package com.boolbalabs.lib.game;

import android.graphics.Point;
import android.os.Bundle;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.microedition.khronos.opengles.GL10;

public class ZNode extends ZDrawable {
    private ArrayList<ZNode> children;
    private boolean drawable;
    private ArrayList<ZNode> drawables;
    private boolean hasChildren;
    private int id;
    protected boolean isRegistered;
    protected ZNode parent;
    protected GameScene parentGameScene;
    private boolean pressed = false;
    private int resourceId;
    public int touchUpSound = 0;
    private boolean touchable;
    private ArrayList<ZNode> touchables;
    public boolean userInteractionEnabled;
    private int zChildrenCounter = 0;
    /* access modifiers changed from: private */
    public int zOrder;
    private int zOrderChildren;
    private ZOrderComparator zOrderComparator = new ZOrderComparator();

    public ZNode(int resourceId2, int drawingMode) {
        super(drawingMode);
        this.resourceId = resourceId2;
        this.parent = this;
        this.drawable = resourceId2 != -1;
        this.userInteractionEnabled = false;
        this.hasChildren = false;
        this.zOrderChildren = this.zChildrenCounter;
        this.drawables = new ArrayList<>();
        this.touchables = new ArrayList<>();
        this.children = new ArrayList<>();
        this.touchable = true;
        if (this.drawable) {
            this.drawables.add(this);
        }
    }

    public void register(GameScene gameScene) {
        this.parentGameScene = gameScene;
        this.isRegistered = true;
        for (int i = 0; i < this.children.size(); i++) {
            ZNode currentNode = this.children.get(i);
            currentNode.parentGameScene = gameScene;
            currentNode.isRegistered = true;
        }
    }

    public void unregister() {
        this.isRegistered = false;
        for (int i = 0; i < this.children.size(); i++) {
            this.children.get(i).isRegistered = false;
        }
    }

    public void initialize() {
        for (int i = 0; i < this.children.size(); i++) {
            this.children.get(i).initialize();
        }
    }

    public void loadContent() {
        if (this.drawable) {
            TexturesManager.getInstance().addTexture(new Texture2D(this.resourceId, Texture2D.TextureFilter.Nearest, Texture2D.TextureFilter.Linear, Texture2D.TextureWrap.ClampToEdge, Texture2D.TextureWrap.ClampToEdge));
        }
        int size = this.children.size();
        for (int i = 0; i < size; i++) {
            this.children.get(i).loadContent();
        }
    }

    public void unloadContent() {
        int size = this.children.size();
        for (int i = 0; i < size; i++) {
            this.children.get(i).unloadContent();
        }
    }

    public void update() {
    }

    public void onPause() {
        for (int i = 0; i < this.children.size(); i++) {
            this.children.get(i).onPause();
        }
    }

    public void onResume() {
        for (int i = 0; i < this.children.size(); i++) {
            this.children.get(i).onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void protectedUpdate() {
        super.update();
        update();
        int size = this.children.size();
        for (int i = 0; i < size; i++) {
            this.children.get(i).protectedUpdate();
        }
    }

    public void drawNode(GL10 gl) {
        if (this.visible) {
            int size = this.drawables.size();
            for (int i = 0; i < size; i++) {
                ZNode cNode = this.drawables.get(i);
                if (cNode == this) {
                    cNode.draw(gl);
                } else {
                    cNode.drawNode(gl);
                }
            }
        }
    }

    public void onAfterLoad() {
        int size = this.children.size();
        for (int i = 0; i < size; i++) {
            this.children.get(i).onAfterLoad();
        }
    }

    public void release() {
    }

    public void startTransition() {
        super.startTransition();
        int size = this.children.size();
        for (int i = 0; i < size; i++) {
            this.children.get(i).startTransition();
        }
    }

    public void startTransitionWithoutChildren() {
        super.startTransition();
    }

    public void stopTransition() {
        super.stopTransition();
        int size = this.children.size();
        for (int i = 0; i < size; i++) {
            this.children.get(i).stopTransition();
        }
    }

    public void stopTransitionWithoutChildren() {
        super.stopTransition();
    }

    public void resetTransition() {
        super.resetTransition();
        int size = this.children.size();
        for (int i = 0; i < size; i++) {
            this.children.get(i).resetTransition();
        }
    }

    public void resetTransitionWithoutChildren() {
        super.resetTransition();
    }

    /* access modifiers changed from: protected */
    public boolean onTouchDown(Point touchDownPoint) {
        boolean wasTouched = false;
        if (pointInside(touchDownPoint)) {
            this.pressed = true;
            playSound(this.touchUpSound, ZageCommonSettings.soundEnabled && this.touchUpSound > 0);
            touchDownAction(touchDownPoint);
            wasTouched = true;
        }
        int size = this.touchables.size();
        for (int i = 0; i < size; i++) {
            ZNode currentNode = this.touchables.get(i);
            if (currentNode.userInteractionEnabled && currentNode.visible && currentNode.onTouchDown(touchDownPoint)) {
                return true;
            }
        }
        return wasTouched;
    }

    public void touchDownAction(Point touchDownPoint) {
    }

    /* access modifiers changed from: protected */
    public boolean onTouchMove(Point touchDownPoint, Point currentPoint) {
        boolean wasTouched = false;
        if (pointInside(currentPoint)) {
            touchMoveAction(touchDownPoint, currentPoint);
            wasTouched = true;
        }
        int size = this.touchables.size();
        for (int i = 0; i < size; i++) {
            ZNode currentNode = this.touchables.get(i);
            if (currentNode.userInteractionEnabled && currentNode.visible && currentNode.onTouchMove(touchDownPoint, currentPoint)) {
                return true;
            }
        }
        return wasTouched;
    }

    public void touchMoveAction(Point touchDownPoint, Point currentPoint) {
    }

    /* access modifiers changed from: protected */
    public boolean onTouchUp(Point touchDownPoint, Point touchUpPoint) {
        boolean wasTouched = false;
        if (pointInside(touchDownPoint)) {
            this.pressed = false;
            touchUpAction(touchDownPoint, touchUpPoint);
            if (pointInside(touchUpPoint)) {
                touchClickAction(touchDownPoint, touchUpPoint);
            }
            wasTouched = true;
        }
        int size = this.touchables.size();
        for (int i = 0; i < size; i++) {
            ZNode currentNode = this.touchables.get(i);
            if (currentNode.userInteractionEnabled && currentNode.visible && currentNode.onTouchUp(touchDownPoint, touchUpPoint)) {
                return true;
            }
        }
        return wasTouched;
    }

    public void touchUpAction(Point touchDownPoint, Point touchUpPoint) {
    }

    public void touchClickAction(Point touchDownPoint, Point touchUpPoint) {
    }

    public ArrayList<ZNode> getChildren() {
        return this.children;
    }

    public ArrayList<ZNode> getDrawables() {
        return this.drawables;
    }

    public ArrayList<ZNode> getTouchables() {
        return this.touchables;
    }

    public void addChild(ZNode childNode, boolean touchable2) {
        if (childNode != null) {
            this.zChildrenCounter++;
            childNode.parent = this;
            this.children.add(childNode);
            this.hasChildren = true;
            if (childNode.drawable) {
                this.drawables.add(childNode);
            }
            childNode.touchable = touchable2;
            if (touchable2) {
                this.touchables.add(childNode);
            }
            childNode.setZOrder(this.zChildrenCounter);
        }
    }

    public void addChild(ZNode childNode) {
        addChild(childNode, true);
    }

    public void addChildren(ArrayList<? extends ZNode> childrenToAdd, boolean touchable2) {
        if (childrenToAdd != null) {
            for (int i = 0; i < childrenToAdd.size(); i++) {
                addChild((ZNode) childrenToAdd.get(i), touchable2);
            }
        }
    }

    public void addChildren(ArrayList<? extends ZNode> childrenToAdd) {
        addChildren(childrenToAdd, true);
    }

    public class ZOrderComparator implements Comparator<ZNode> {
        public ZOrderComparator() {
        }

        public int compare(ZNode object1, ZNode object2) {
            return object1.zOrder - object2.zOrder;
        }
    }

    public void setZOrder(int newZOrder) {
        this.zOrder = newZOrder;
        sortSiblings();
    }

    public void setZOrderAsChild(int newZOrder) {
        this.zOrderChildren = newZOrder;
        sortChildren();
    }

    private void sortSiblings() {
        boolean hasActualParent = (this.parent == null || this.parent == this) ? false : true;
        if (hasActualParent || this.parentGameScene != null) {
            ArrayList<ZNode> drawableSiblings = hasActualParent ? this.parent.getDrawables() : this.parentGameScene.getSceneDrawables();
            ArrayList<ZNode> touchableSiblings = hasActualParent ? this.parent.getTouchables() : this.parentGameScene.getSceneTouchables();
            int previousZOrderParent = 0;
            if (hasActualParent) {
                previousZOrderParent = this.parent.zOrder;
                this.parent.zOrder = this.parent.zOrderChildren;
            }
            synchronized (drawableSiblings) {
                Collections.sort(drawableSiblings, this.zOrderComparator);
            }
            synchronized (touchableSiblings) {
                Collections.sort(touchableSiblings, this.zOrderComparator);
                Collections.reverse(touchableSiblings);
            }
            if (hasActualParent) {
                this.parent.zOrder = previousZOrderParent;
            }
        }
    }

    private void sortChildren() {
        int previousZOrder = this.zOrder;
        this.zOrder = this.zOrderChildren;
        synchronized (this.drawables) {
            Collections.sort(this.drawables, this.zOrderComparator);
        }
        synchronized (this.touchables) {
            Collections.sort(this.touchables, this.zOrderComparator);
            Collections.reverse(this.touchables);
        }
        this.zOrder = previousZOrder;
    }

    public boolean isRegistered() {
        return this.isRegistered;
    }

    public int getResourceId() {
        return this.resourceId;
    }

    public boolean isDrawable() {
        return this.drawable;
    }

    public boolean isPressed() {
        return this.pressed;
    }

    public void setTexture(Texture2D texture) {
        if (this.drawable) {
            super.setTexture(texture);
        }
        for (int i = 0; i < this.children.size(); i++) {
            ZNode cNode = this.children.get(i);
            cNode.setTexture(TexturesManager.getInstance().getTextureByResourceId(cNode.getResourceId()));
        }
    }

    public void performAction(int actionCode, Bundle actionParameters) {
        if (this.parentGameScene != null) {
            this.parentGameScene.performAction(actionCode, actionParameters);
        }
    }

    public void playSound(int soundId, float playSpeed, boolean soundEnabled) {
        SoundManager soundManager;
        if (soundEnabled && (soundManager = SoundManager.getInstance()) != null) {
            soundManager.playShortSound(soundId, playSpeed);
        }
    }

    public void playSound(int soundId, boolean soundEnabled) {
        playSound(soundId, 1.0f, soundEnabled);
    }

    public void addShortSound(int soundId) {
        SoundManager soundManager = SoundManager.getInstance();
        if (soundManager != null) {
            soundManager.addShortSound(soundId, 1000);
        }
    }

    public void addLoopingSound(int soundId) {
        SoundManager soundManager = SoundManager.getInstance();
        if (soundManager != null) {
            soundManager.addLoopingSound(soundId);
        }
    }
}
