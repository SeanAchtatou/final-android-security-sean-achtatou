package com.boolbalabs.lib.game;

import android.opengl.GLSurfaceView;
import com.boolbalabs.lib.services.ContentLoaderServiceOpenGL;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/* compiled from: GameViewGL */
class GameRenderer implements GLSurfaceView.Renderer {
    private DisplayServiceOpenGL displayService;
    private GLSurfaceView parentView;

    GameRenderer() {
    }

    public void initialize(GLSurfaceView parentView2) {
        this.parentView = parentView2;
        this.displayService = DisplayServiceOpenGL.getInstance();
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        this.displayService.gl = gl;
        this.displayService.surfaceCreated();
        this.displayService.drawSplashScreen();
        ContentLoaderServiceOpenGL.getInstance().loadContent();
    }

    public void onSurfaceChanged(GL10 gl, int w, int h) {
        gl.glViewport(0, 0, w, h);
        this.displayService.sizeChanged(w, h);
        this.displayService.initCustomOpenGLParameters(gl, this.parentView);
    }

    public void onDrawFrame(GL10 gl) {
        this.displayService.draw();
    }
}
