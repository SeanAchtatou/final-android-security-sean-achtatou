package com.boolbalabs.lib.game;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;

public class GameViewGL extends GLSurfaceView {
    private static final Semaphore sEglSemaphore = new Semaphore(1);
    private final int MAIN_INPUT_POOL_SIZE = 20;
    private DisplayServiceOpenGL displayService;
    private GameRenderer gameRenderer;
    private GameThreadGL gameThreadGL;
    private boolean isGameInitialised = false;
    private boolean loadingFirstTime;
    private ArrayBlockingQueue<UserInputEvent> mainGameUserInputPool;
    private TexturesManager texturesManager;

    public GameViewGL(Context context) {
        super(context);
        initialize();
    }

    public GameViewGL(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    private void initialize() {
        createInputObjectPool();
        this.gameRenderer = new GameRenderer();
        this.gameRenderer.initialize(this);
        setRenderer(this.gameRenderer);
        this.displayService = DisplayServiceOpenGL.getInstance();
        setFocusable(true);
    }

    public void startThread(Game game) {
        this.gameThreadGL = new GameThreadGL(this);
        this.gameThreadGL.setGame(game);
        this.gameThreadGL.start();
    }

    public void pauseGame() {
        onPause();
        this.gameThreadGL.pauseGame();
    }

    public void stopThread() {
        try {
            this.gameThreadGL.requestExitAndWait();
            this.gameThreadGL = null;
        } catch (Exception e) {
        }
    }

    public void resumeGame() {
        this.gameThreadGL.resumeGame();
        onResume();
    }

    public static Semaphore getSemaphore() {
        return sEglSemaphore;
    }

    private void createInputObjectPool() {
        this.mainGameUserInputPool = new ArrayBlockingQueue<>(20);
        for (int i = 0; i < 20; i++) {
            this.mainGameUserInputPool.add(new UserInputEvent(this.mainGameUserInputPool));
        }
    }

    public void addTexture(Texture2D texture) {
        this.texturesManager.addTexture(texture);
    }

    public boolean onTouchEvent(MotionEvent event) {
        try {
            int hist = event.getHistorySize();
            if (hist > 0) {
                for (int i = 0; i < hist; i++) {
                    UserInputEvent input = this.mainGameUserInputPool.take();
                    input.initFromEventHistory(event, i);
                    this.gameThreadGL.feedInput(input);
                }
            }
            UserInputEvent input2 = this.mainGameUserInputPool.take();
            input2.initFromEvent(event);
            this.gameThreadGL.feedInput(input2);
        } catch (InterruptedException e) {
        }
        try {
            Thread.sleep(20);
            return true;
        } catch (InterruptedException e2) {
            return true;
        }
    }
}
