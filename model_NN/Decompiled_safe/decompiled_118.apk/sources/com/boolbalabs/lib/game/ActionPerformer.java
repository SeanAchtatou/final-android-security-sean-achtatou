package com.boolbalabs.lib.game;

import android.os.Bundle;

public interface ActionPerformer {
    void performAction(int i, Bundle bundle);
}
