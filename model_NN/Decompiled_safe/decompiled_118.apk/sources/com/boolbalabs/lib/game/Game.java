package com.boolbalabs.lib.game;

import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.util.Log;
import com.boolbalabs.lib.managers.SoundManager;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;

public abstract class Game implements ContentLoader {
    protected GameScene currentScene;
    private HashMap<Integer, GameScene> gameScenes;
    private boolean isGameInitialized;
    public boolean userInteractionEnabled;

    public abstract void drawSplashScreen();

    public abstract void initialize();

    public abstract void onPause();

    public abstract void onResume();

    public Game() {
        this.isGameInitialized = false;
        this.gameScenes = new HashMap<>();
        this.userInteractionEnabled = true;
        this.isGameInitialized = true;
    }

    /* access modifiers changed from: protected */
    public HashMap<Integer, GameScene> getGameScenes() {
        return this.gameScenes;
    }

    public void registerGameScene(GameScene newGameScene) {
        if (newGameScene != null && this.gameScenes != null) {
            if (!this.gameScenes.containsKey(Integer.valueOf(newGameScene.getSceneId()))) {
                this.gameScenes.put(Integer.valueOf(newGameScene.getSceneId()), newGameScene);
            } else {
                Log.i("WARNING", "There is already a scene with such ID");
            }
        }
    }

    public void registerGameComponent(ZNode zNode, int gameSceneId) {
        if (zNode != null && this.gameScenes != null && this.gameScenes.containsKey(Integer.valueOf(gameSceneId))) {
            this.gameScenes.get(Integer.valueOf(gameSceneId)).registerGameComponent(zNode);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public void unregisterGameComponent(ZNode zNode, int gameSceneId) {
        if (zNode != null && this.gameScenes != null && this.gameScenes.containsKey(Integer.valueOf(gameSceneId))) {
            this.gameScenes.get(Integer.valueOf(gameSceneId)).unregisterGameComponent(zNode);
        }
    }

    public void loadContent() {
        if (this.gameScenes != null && !this.gameScenes.isEmpty()) {
            for (Integer intValue : this.gameScenes.keySet()) {
                this.gameScenes.get(Integer.valueOf(intValue.intValue())).loadContent();
            }
        }
    }

    public void onAfterLoad() {
    }

    public void unloadContent() {
        if (this.gameScenes != null && !this.gameScenes.isEmpty()) {
            for (Integer intValue : this.gameScenes.keySet()) {
                this.gameScenes.get(Integer.valueOf(intValue.intValue())).unloadContent();
            }
        }
    }

    public void switchGameScene(int newGameSceneId) {
        if (this.isGameInitialized) {
            if (this.gameScenes.containsKey(Integer.valueOf(newGameSceneId))) {
                GameScene newScene = this.gameScenes.get(Integer.valueOf(newGameSceneId));
                if (newScene != this.currentScene) {
                    if (this.currentScene != null && this.currentScene.isSceneInitialized()) {
                        this.currentScene.onHide();
                    }
                    if (newScene.isSceneInitialized()) {
                        newScene.onShow();
                    }
                    this.currentScene = newScene;
                    return;
                }
                return;
            }
            throw new IllegalStateException("switchGameState (Game class) method");
        }
    }

    public int getCurrentGameSceneId() {
        if (this.currentScene != null) {
            return this.currentScene.getSceneId();
        }
        return -1;
    }

    public void update() {
        if (this.isGameInitialized && this.currentScene != null) {
            synchronized (this.currentScene) {
                this.currentScene.update();
            }
        }
    }

    public void draw(GL10 gl) {
        if (this.isGameInitialized) {
            synchronized (this.currentScene) {
                this.currentScene.draw(gl);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean onTouchDown(Point touchDownPoint) {
        if (!this.userInteractionEnabled || this.currentScene == null) {
            return false;
        }
        touchDownAction(touchDownPoint);
        return this.currentScene.onTouchDown(touchDownPoint);
    }

    public void touchDownAction(Point touchDownPoint) {
    }

    /* access modifiers changed from: protected */
    public boolean onTouchMove(Point touchDownPoint, Point currentPoint) {
        if (!this.userInteractionEnabled || this.currentScene == null) {
            return false;
        }
        touchMoveAction(touchDownPoint, currentPoint);
        return this.currentScene.onTouchMove(touchDownPoint, currentPoint);
    }

    public void touchMoveAction(Point touchDownPoint, Point currentPoint) {
    }

    /* access modifiers changed from: protected */
    public boolean onTouchUp(Point touchDownPoint, Point touchUpPoint) {
        if (!this.userInteractionEnabled || this.currentScene == null) {
            return false;
        }
        touchUpAction(touchDownPoint, touchUpPoint);
        return this.currentScene.onTouchUp(touchDownPoint, touchUpPoint);
    }

    public void touchUpAction(Point touchDownPoint, Point currentPoint) {
    }

    public void onExit() {
        DisplayServiceOpenGL.release();
    }

    public boolean isGameInitialized() {
        return this.isGameInitialized;
    }

    public void initCustomOpenGLParameters(GL10 gl, GLSurfaceView parentView) {
    }

    public void playSound(int soundId, boolean soundEnabled) {
        SoundManager soundManager;
        if (soundEnabled && (soundManager = SoundManager.getInstance()) != null) {
            soundManager.playShortSound(soundId);
        }
    }

    public void addShortSound(int soundId) {
        SoundManager soundManager = SoundManager.getInstance();
        if (soundManager != null) {
            soundManager.addShortSound(soundId, 1000);
        }
    }

    public void addLoopingSound(int soundId) {
        SoundManager soundManager = SoundManager.getInstance();
        if (soundManager != null) {
            soundManager.addLoopingSound(soundId);
        }
    }
}
