package com.boolbalabs.lib.game;

import android.view.KeyEvent;
import android.view.MotionEvent;
import java.util.concurrent.ArrayBlockingQueue;

public class UserInputEvent {
    public static final int ACTION_KEY_DOWN = 1;
    public static final int ACTION_KEY_UP = 2;
    public static final int ACTION_TOUCH_DOWN = 3;
    public static final int ACTION_TOUCH_MOVE = 4;
    public static final int ACTION_TOUCH_UP = 5;
    public static final byte EVENT_TYPE_KEY = 1;
    public static final byte EVENT_TYPE_TOUCH = 2;
    public int action;
    public byte eventType;
    public int keyCode;
    public ArrayBlockingQueue<UserInputEvent> mainGameUserInputPool;
    public long time;
    public int x;
    public int y;

    public UserInputEvent(ArrayBlockingQueue<UserInputEvent> mainGameInputPool) {
        this.mainGameUserInputPool = mainGameInputPool;
    }

    public void initFromEvent(KeyEvent event) {
        this.eventType = 1;
        switch (event.getAction()) {
            case 0:
                this.action = 1;
                break;
            case 1:
                this.action = 2;
                break;
            default:
                this.action = 0;
                break;
        }
        this.time = event.getEventTime();
        this.keyCode = event.getKeyCode();
    }

    public void initFromEvent(MotionEvent event) {
        this.eventType = 2;
        switch (event.getAction()) {
            case 0:
                this.action = 3;
                break;
            case 1:
                this.action = 5;
                break;
            case 2:
                this.action = 4;
                break;
            default:
                this.action = 0;
                break;
        }
        this.time = event.getEventTime();
        this.x = (int) event.getX();
        this.y = (int) event.getY();
    }

    public void initFromEventHistory(MotionEvent event, int historyItem) {
        this.eventType = 2;
        this.action = 4;
        this.time = event.getHistoricalEventTime(historyItem);
        this.x = (int) event.getHistoricalX(historyItem);
        this.y = (int) event.getHistoricalY(historyItem);
    }

    public void returnToPool() {
        this.mainGameUserInputPool.add(this);
    }
}
