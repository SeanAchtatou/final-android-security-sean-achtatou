package com.boolbalabs.lib.game;

import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.opengl.GLU;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.transitions.Transition;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.lib.utils.ScreenMetrics;
import com.boolbalabs.lib.utils.ZageCommonSettings;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

public class ZDrawable {
    public static final int MODE_CANVAS = 2;
    public static final int MODE_OPENGL_OES_EXT = 0;
    public static final int MODE_OPENGL_VERTICES = 1;
    private float affineShiftX;
    private float affineShiftY;
    private Point centerInPix = new Point();
    private Point centerInRip = new Point();
    private int centerPixX;
    private int centerPixY;
    private int centerRipX;
    private int centerRipY;
    public float defaultViewingDistance;
    private final double degToRadCoeff;
    private Texture2D drawableTexture;
    private int drawableTextureIndex = -1;
    private int drawingMode;
    private Rect frameInPix = new Rect();
    private Rect frameInRip = new Rect();
    private int heightPix;
    private int heightRip;
    private ShortBuffer indexBuffer;
    private int[] oesTextureCropRect;
    private float originalX;
    private float originalY;
    private PointF pivotPoint;
    private Point positionPix = new Point();
    private Point positionRip = new Point();
    private short[] quad_indices;
    private final double radToDegCoeff;
    private PointF rangeX;
    private PointF rangeY;
    private Rect rectOnTexture;
    private float rotationAngle;
    protected FloatBuffer textureBuffer;
    private Transition transition;
    private FloatBuffer vertexBuffer;
    private boolean vertexBufferCreated;
    public boolean visible;
    private int widthPix;
    private int widthRip;
    private float xGL;
    private int xPix;
    private int xRip;
    private float yGL;
    private int yPix;
    private int yRip;
    private float zGL = 0.0f;

    public ZDrawable(int drawingMode2) {
        short[] sArr = new short[6];
        sArr[1] = 1;
        sArr[2] = 2;
        sArr[4] = 3;
        sArr[5] = 2;
        this.quad_indices = sArr;
        this.rotationAngle = 0.0f;
        this.pivotPoint = new PointF();
        this.defaultViewingDistance = ZageCommonSettings.openGLVertexModeDefaultViewDistance;
        this.vertexBufferCreated = false;
        this.degToRadCoeff = 57.29577951308232d;
        this.radToDegCoeff = 0.017453292519943295d;
        this.drawingMode = 0;
        this.visible = true;
        setDrawingMode(drawingMode2);
    }

    public void initWithFrame(Rect rectOnScreenRip, Rect rectOnTexture2) {
        setFrameInRip(rectOnScreenRip);
        setRectOnTexture(rectOnTexture2);
    }

    private void setDrawingMode(int drawingMode2) {
        this.drawingMode = drawingMode2;
        switch (drawingMode2) {
            case 0:
                this.oesTextureCropRect = new int[4];
                return;
            case 1:
                createDefaultRange();
                calculateRangeXY();
                createIndexBuffer();
                return;
            case 2:
                return;
            default:
                this.drawingMode = 0;
                return;
        }
    }

    public void setRectOnTexture(Rect rectOnTexture2) {
        this.rectOnTexture = rectOnTexture2;
        switch (this.drawingMode) {
            case 0:
                this.oesTextureCropRect[0] = rectOnTexture2.left;
                this.oesTextureCropRect[1] = rectOnTexture2.bottom;
                this.oesTextureCropRect[2] = rectOnTexture2.width();
                this.oesTextureCropRect[3] = -rectOnTexture2.height();
                return;
            case 1:
                if (this.drawableTexture != null) {
                    createTextureBuffer();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void setTexture(Texture2D texture) {
        this.drawableTexture = texture;
        this.drawableTextureIndex = texture.getTextureGlobalIndex();
        if (this.drawingMode == 1) {
            createTextureBuffer();
        }
    }

    public Texture2D getTexture() {
        return this.drawableTexture;
    }

    public void draw(GL10 gl) {
        switch (this.drawingMode) {
            case 0:
                drawOES(gl);
                return;
            case 1:
                drawVerts(gl);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void drawOES(GL10 gl) {
        gl.glBindTexture(3553, this.drawableTextureIndex);
        ((GL11) gl).glTexParameteriv(3553, 35741, this.oesTextureCropRect, 0);
        ((GL11Ext) gl).glDrawTexfOES(this.xGL, this.yGL, this.zGL, (float) this.widthPix, (float) this.heightPix);
    }

    /* access modifiers changed from: protected */
    public void drawVerts(GL10 gl) {
        if (this.textureBuffer != null) {
            gl.glMatrixMode(5888);
            gl.glLoadIdentity();
            GLU.gluLookAt(gl, 0.0f, 0.0f, this.defaultViewingDistance, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
            gl.glEnableClientState(32884);
            gl.glEnableClientState(32888);
            gl.glTranslatef(this.pivotPoint.x, this.pivotPoint.y, 0.0f);
            gl.glRotatef(this.rotationAngle, 0.0f, 0.0f, 1.0f);
            gl.glTranslatef(-this.pivotPoint.x, -this.pivotPoint.y, 0.0f);
            gl.glTranslatef(this.affineShiftX, this.affineShiftY, 0.0f);
            gl.glBindTexture(3553, this.drawableTextureIndex);
            gl.glTexParameterx(3553, 10242, 10497);
            gl.glTexParameterx(3553, 10243, 10497);
            gl.glVertexPointer(3, 5126, 0, this.vertexBuffer);
            gl.glTexCoordPointer(2, 5126, 0, this.textureBuffer);
            gl.glDrawElements(5, 6, 5123, this.indexBuffer);
        }
    }

    /* access modifiers changed from: protected */
    public void update() {
        if (this.transition != null) {
            this.transition.innerUpdate();
        }
    }

    public void setPivotPointRip(Point newPivotPointRip) {
        setPivotPointRip(newPivotPointRip.x, newPivotPointRip.y);
    }

    public void setPivotPointRip(int x, int y) {
        if (this.drawingMode == 1) {
            this.pivotPoint.set(getScaledX(x), getScaledY(y));
        }
    }

    public boolean pointInside(int x, int y) {
        return this.widthPix > 0 && this.heightPix > 0 && x >= this.xPix && x <= this.xPix + this.widthPix && y >= this.yPix && y <= this.yPix + this.heightPix;
    }

    public boolean pointInside(Point point) {
        return pointInside(point.x, point.y);
    }

    public boolean pointInsideRip(int x, int y) {
        return this.widthRip > 0 && this.heightRip > 0 && x >= this.xRip && x <= this.xRip + this.widthRip && y >= this.yRip && y <= this.yRip + this.heightRip;
    }

    public boolean pointInsideRip(Point pointInRip) {
        return pointInsideRip(pointInRip.x, pointInRip.y);
    }

    public void setFrameInPix(Rect frame) {
        if (frame != null) {
            this.widthPix = frame.width();
            this.heightPix = frame.height();
            setPositionInPix(frame.left, frame.top);
        }
    }

    public void setFrameInRip(Rect frame) {
        if (frame != null) {
            this.widthRip = frame.width();
            this.heightRip = frame.height();
            setPositionInRip(frame.left, frame.top);
        }
    }

    public void setPositionInRip(int x, int y) {
        this.xRip = x;
        this.yRip = y;
        adjustCoordsPixToRip();
        adjustCenterRip();
        adjustCoordsOES();
        adjustVertexBuffer();
    }

    public void setPositionInRip(Point posInRip) {
        setPositionInRip(posInRip.x, posInRip.y);
    }

    public void setPositionInPix(int x, int y) {
        this.xPix = x;
        this.yPix = y;
        adjustCoordsRipToPix();
        adjustCenterPix();
        adjustCoordsOES();
        adjustVertexBuffer();
    }

    public void setPositionInPix(Point posInPix) {
        setPositionInPix(posInPix.x, posInPix.y);
    }

    public void setCenterInRip(int x, int y) {
        setPositionInRip(x - (this.widthRip >> 1), y - (this.heightRip >> 1));
    }

    private void adjustCoordsPixToRip() {
        this.xPix = ScreenMetrics.convertRipToPixel((float) this.xRip);
        this.yPix = ScreenMetrics.convertRipToPixel((float) this.yRip);
        this.widthPix = ScreenMetrics.convertRipToPixel((float) this.widthRip);
        this.heightPix = ScreenMetrics.convertRipToPixel((float) this.heightRip);
        adjustCenterPix();
    }

    private void adjustCoordsRipToPix() {
        this.xRip = ScreenMetrics.convertPixelToRip(this.xPix);
        this.yRip = ScreenMetrics.convertPixelToRip(this.yPix);
        this.widthRip = ScreenMetrics.convertPixelToRip(this.widthPix);
        this.heightRip = ScreenMetrics.convertPixelToRip(this.heightPix);
        adjustCenterRip();
    }

    private void adjustCenterRip() {
        this.centerRipX = this.xRip + (this.widthRip >> 1);
        this.centerRipY = this.yRip + (this.heightRip >> 1);
    }

    private void adjustCenterPix() {
        this.centerPixX = this.xPix + (this.widthPix >> 1);
        this.centerPixY = this.yPix + (this.heightPix >> 1);
    }

    private void adjustCoordsOES() {
        if (this.drawingMode == 0) {
            this.xGL = (float) this.xPix;
            this.yGL = (float) ((ScreenMetrics.screenHeightPix - this.yPix) - this.heightPix);
        }
    }

    public void setZGL(float zGL2) {
        this.zGL = zGL2;
    }

    private void adjustVertexBuffer() {
        if (this.drawingMode != 1) {
            return;
        }
        if (!this.vertexBufferCreated) {
            createVertexBuffer();
            return;
        }
        this.affineShiftX = getScaledX(this.xRip) - this.originalX;
        this.affineShiftY = getScaledY(this.yRip) - this.originalY;
    }

    public int getTextureGlobalIndex() {
        if (this.drawableTexture != null) {
            return this.drawableTextureIndex;
        }
        return -1;
    }

    public Point getPositionRip() {
        this.positionRip.set(this.xRip, this.yRip);
        return this.positionRip;
    }

    public Point getPositionPix() {
        this.positionPix.set(this.xPix, this.yPix);
        return this.positionPix;
    }

    public Rect getFrameInPix() {
        this.frameInPix.set(this.xPix, this.yPix, this.xPix + this.widthPix, this.yPix + this.heightPix);
        return this.frameInPix;
    }

    public Rect getFrameInRip() {
        this.frameInRip.set(this.xRip, this.yRip, this.xRip + this.widthRip, this.yRip + this.heightRip);
        return this.frameInRip;
    }

    public Point getCenterInRip() {
        this.centerInRip.set(this.xRip + (this.widthRip >> 1), this.yRip + (this.heightRip >> 1));
        return this.centerInRip;
    }

    public Point getCenterInPix() {
        this.centerInPix.set(this.xPix + (this.widthPix >> 1), this.yPix + (this.heightPix >> 1));
        return this.centerInPix;
    }

    public float getCoordsOES_X() {
        return this.xGL;
    }

    public float getCoordsOES_Y() {
        return this.yGL;
    }

    public float getCoordsOES_Z() {
        return this.zGL;
    }

    /* access modifiers changed from: protected */
    public int getDrawingMode() {
        return this.drawingMode;
    }

    public void setTransition(Transition transition2) {
        this.transition = transition2;
    }

    public Transition getTransition() {
        return this.transition;
    }

    public void startTransition() {
        if (this.transition != null) {
            this.transition.start();
        }
    }

    public void stopTransition() {
        if (this.transition != null) {
            this.transition.stop();
        }
    }

    public void resetTransition() {
        if (this.transition != null) {
            this.transition.reset();
        }
    }

    /* access modifiers changed from: protected */
    public void createTextureBuffer() {
        if (this.rectOnTexture != null) {
            float width = (float) this.drawableTexture.getWidth();
            float height = (float) this.drawableTexture.getHeight();
            float[] textCoords = {((float) this.rectOnTexture.left) / width, ((float) this.rectOnTexture.top) / height, ((float) this.rectOnTexture.left) / width, ((float) this.rectOnTexture.bottom) / height, ((float) this.rectOnTexture.right) / width, ((float) this.rectOnTexture.bottom) / height, ((float) this.rectOnTexture.right) / width, ((float) this.rectOnTexture.top) / height};
            ByteBuffer tbb = ByteBuffer.allocateDirect(textCoords.length * 4);
            tbb.order(ByteOrder.nativeOrder());
            this.textureBuffer = tbb.asFloatBuffer();
            this.textureBuffer.put(textCoords);
            this.textureBuffer.position(0);
        }
    }

    /* access modifiers changed from: protected */
    public void setTextureBuffer(FloatBuffer textureBuffer2) {
        this.textureBuffer = textureBuffer2;
    }

    private void createIndexBuffer() {
        ByteBuffer ibb = ByteBuffer.allocateDirect(this.quad_indices.length * 2);
        ibb.order(ByteOrder.nativeOrder());
        this.indexBuffer = ibb.asShortBuffer();
        this.indexBuffer.put(this.quad_indices);
        this.indexBuffer.position(0);
    }

    private void createVertexBuffer() {
        float[] vertices = {getScaledX(this.xRip), getScaledY(this.yRip), 0.0f, getScaledX(this.xRip), getScaledY(this.yRip + this.heightRip), 0.0f, getScaledX(this.xRip + this.widthRip), getScaledY(this.yRip + this.heightRip), 0.0f, getScaledX(this.xRip + this.widthRip), getScaledY(this.yRip), 0.0f};
        this.originalX = vertices[0];
        this.originalY = vertices[1];
        this.affineShiftX = 0.0f;
        this.affineShiftY = 0.0f;
        ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
        vbb.order(ByteOrder.nativeOrder());
        this.vertexBuffer = vbb.asFloatBuffer();
        this.vertexBuffer.put(vertices);
        this.vertexBuffer.position(0);
        this.vertexBufferCreated = true;
    }

    private float getScaledX(int xRip2) {
        return ((((float) xRip2) * (this.rangeX.y - this.rangeX.x)) / ((float) ScreenMetrics.screenWidthRip)) + this.rangeX.x;
    }

    private float getScaledY(int yRip2) {
        return ((((float) yRip2) * (this.rangeY.x - this.rangeY.y)) / ((float) ScreenMetrics.screenHeightRip)) - this.rangeY.x;
    }

    public FloatBuffer createVertexBufferForRect(Rect rect) {
        return BufferUtils.makeFloatBuffer(new float[]{getScaledX(rect.left), getScaledY(rect.top), 0.0f, getScaledX(rect.left), getScaledY(rect.bottom), 0.0f, getScaledX(rect.right), getScaledY(rect.bottom), 0.0f, getScaledX(rect.right), getScaledY(rect.top), 0.0f});
    }

    private void createDefaultRange() {
        this.rangeX = new PointF(-1.0f, 1.0f);
        this.rangeY = new PointF(-1.0f, 1.0f);
    }

    private void calculateRangeXY() {
        float screenRatio = ScreenMetrics.aspectRatioOriented;
        this.rangeY.y = (0.417f * (this.defaultViewingDistance - 1.4f)) + 0.583f;
        this.rangeY.x = -this.rangeY.y;
        this.rangeX.x = this.rangeY.x * screenRatio;
        this.rangeX.y = this.rangeY.y * screenRatio;
    }

    public void setRotationAngleDeg(double angleDeg) {
        this.rotationAngle = (float) angleDeg;
    }

    public float getRotationAngleDeg() {
        return this.rotationAngle;
    }

    public void setRotationAngleRad(double angleRad) {
        this.rotationAngle = (float) (57.29577951308232d * angleRad);
    }

    public void setPivotPointToCenterAndRotateByRad(double angleRad) {
        setPivotPointRip(this.centerRipX, this.centerRipY);
        setRotationAngleRad(angleRad);
    }

    public void setPivotPointToCenterAndRotateByDeg(double angleDeg) {
        setPivotPointRip(this.centerRipX, this.centerRipY);
        setRotationAngleDeg(angleDeg);
    }
}
