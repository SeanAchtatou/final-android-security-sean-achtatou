package com.boolbalabs.lib.game;

public interface ContentLoader {
    void loadContent();

    void onAfterLoad();

    void unloadContent();
}
