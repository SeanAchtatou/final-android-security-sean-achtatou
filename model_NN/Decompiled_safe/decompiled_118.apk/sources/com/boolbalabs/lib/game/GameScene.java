package com.boolbalabs.lib.game;

import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;
import java.util.Collections;
import javax.microedition.khronos.opengles.GL10;

public abstract class GameScene implements ActionPerformer {
    private Handler activityHandler;
    private ArrayList<ZNode> drawables = new ArrayList<>();
    private int id;
    private boolean isRegistered;
    private boolean isSceneInitialized;
    protected Game mainGame;
    private ArrayList<ZNode> touchables = new ArrayList<>();

    public GameScene(Game game, int sceneId) {
        this.mainGame = game;
        this.id = sceneId;
        onCreate();
        this.mainGame.registerGameScene(this);
    }

    public void registerGameComponent(ZNode zNode) {
        if (zNode != null && !this.drawables.contains(zNode)) {
            this.drawables.add(zNode);
            this.touchables.add(zNode);
            zNode.register(this);
        }
    }

    public void unregisterGameComponent(ZNode zNode) {
        if (zNode != null && this.drawables.contains(zNode)) {
            zNode.unregister();
            this.drawables.remove(zNode);
            this.touchables.remove(zNode);
        }
    }

    public void loadContent() {
        if (this.drawables != null && !this.drawables.isEmpty()) {
            int size = this.drawables.size();
            for (int i = 0; i < size; i++) {
                this.drawables.get(i).loadContent();
            }
        }
    }

    public void onAfterLoad() {
        adjustNodes();
        this.isSceneInitialized = true;
    }

    public void unloadContent() {
        if (this.drawables != null && !this.drawables.isEmpty()) {
            int size = this.drawables.size();
            for (int i = 0; i < size; i++) {
                this.drawables.get(i).unloadContent();
            }
        }
    }

    private void adjustNodes() {
        this.touchables.clear();
        this.touchables.addAll(this.drawables);
        Collections.reverse(this.touchables);
    }

    public void update() {
        if (this.isSceneInitialized) {
            synchronized (this.drawables) {
                int size = this.drawables.size();
                for (int i = 0; i < size; i++) {
                    this.drawables.get(i).protectedUpdate();
                }
            }
        }
    }

    public void draw(GL10 gl) {
        if (this.isSceneInitialized) {
            synchronized (this.drawables) {
                int size = this.drawables.size();
                for (int i = 0; i < size; i++) {
                    this.drawables.get(i).drawNode(gl);
                }
            }
        }
    }

    public ZNode findComponentByClass(Class c) {
        int size = this.drawables.size();
        for (int i = 0; i < size; i++) {
            ZNode zNode = this.drawables.get(i);
            if (c.isAssignableFrom(zNode.getClass())) {
                return zNode;
            }
        }
        return null;
    }

    public void askGameToSwitchGameScene(int newGameSceneId) {
        this.mainGame.switchGameScene(newGameSceneId);
    }

    /* access modifiers changed from: protected */
    public boolean onTouchDown(Point touchDownPoint) {
        touchDownAction(touchDownPoint);
        int size = this.touchables.size();
        for (int i = 0; i < size; i++) {
            ZNode cNode = this.touchables.get(i);
            if (cNode.userInteractionEnabled && cNode.onTouchDown(touchDownPoint)) {
                return true;
            }
        }
        return false;
    }

    public void touchDownAction(Point touchDownPoint) {
    }

    /* access modifiers changed from: protected */
    public boolean onTouchMove(Point touchDownPoint, Point currentPoint) {
        touchMoveAction(touchDownPoint, currentPoint);
        int size = this.touchables.size();
        for (int i = 0; i < size; i++) {
            ZNode cNode = this.touchables.get(i);
            if (cNode.userInteractionEnabled && cNode.onTouchMove(touchDownPoint, currentPoint)) {
                return true;
            }
        }
        return false;
    }

    public void touchMoveAction(Point touchDownPoint, Point currentPoint) {
    }

    /* access modifiers changed from: protected */
    public boolean onTouchUp(Point touchDownPoint, Point touchUpPoint) {
        touchUpAction(touchDownPoint, touchUpPoint);
        int size = this.touchables.size();
        for (int i = 0; i < size; i++) {
            ZNode cNode = this.touchables.get(i);
            if (cNode.userInteractionEnabled && cNode.onTouchUp(touchDownPoint, touchUpPoint)) {
                return true;
            }
        }
        return false;
    }

    public void touchUpAction(Point touchDownPoint, Point currentPoint) {
    }

    public void onCreate() {
    }

    public void onResume() {
    }

    public void onPause() {
    }

    public void onDestroy() {
    }

    public void onShow() {
    }

    public void onHide() {
    }

    public int getSceneId() {
        return this.id;
    }

    public boolean isSceneInitialized() {
        return this.isSceneInitialized;
    }

    public ArrayList<ZNode> getSceneDrawables() {
        return this.drawables;
    }

    public ArrayList<ZNode> getSceneTouchables() {
        return this.touchables;
    }

    public void setActivityHandler(Handler activityHandler2) {
        this.activityHandler = activityHandler2;
    }

    public void sendMessageToActivity(Message msg) {
        if (this.activityHandler != null) {
            try {
                this.activityHandler.sendMessage(msg);
            } catch (Exception e) {
            }
        }
    }

    public void sendEmptyMessageToActivity(int messageId) {
        if (this.activityHandler != null) {
            try {
                this.activityHandler.sendEmptyMessage(messageId);
            } catch (Exception e) {
            }
        }
    }
}
