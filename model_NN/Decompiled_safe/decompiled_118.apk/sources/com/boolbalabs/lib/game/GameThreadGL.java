package com.boolbalabs.lib.game;

import android.graphics.Point;
import android.util.Log;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import com.boolbalabs.lib.utils.DebugLog;
import java.lang.ref.WeakReference;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;

public class GameThreadGL extends Thread {
    private static Semaphore sEglSemaphore;
    private final int ITERATION_TIME = 16;
    private final int THREAD_INPUT_QUEUE_SIZE = 20;
    private DisplayServiceOpenGL displayService;
    private Game game;
    private WeakReference<GameViewGL> gameViewGLWeak;
    private Point lastMovePoint = new Point(0, 0);
    private Point lastTouchDownPoint = new Point(0, 0);
    private Point lastTouchUpPoint = new Point(0, 0);
    private boolean mDone;
    private boolean mPaused;
    private ArrayBlockingQueue<UserInputEvent> threadUserInputQueue = new ArrayBlockingQueue<>(20);
    private final Object threadUserInputQueueMutex = new Object();

    public GameThreadGL(GameViewGL gameViewGL) {
        this.gameViewGLWeak = new WeakReference<>(gameViewGL);
        sEglSemaphore = GameViewGL.getSemaphore();
        this.mDone = false;
        this.displayService = DisplayServiceOpenGL.getInstance();
    }

    public boolean isRunning() {
        return !this.mPaused;
    }

    public boolean isPaused() {
        return this.mPaused;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001e, code lost:
        com.boolbalabs.lib.game.GameThreadGL.sEglSemaphore.release();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0023, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0017, code lost:
        com.boolbalabs.lib.game.GameThreadGL.sEglSemaphore.release();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001d A[ExcHandler:  FINALLY, Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r3 = this;
            java.util.concurrent.Semaphore r1 = com.boolbalabs.lib.game.GameThreadGL.sEglSemaphore     // Catch:{ InterruptedException -> 0x000e, all -> 0x001d }
            r1.acquire()     // Catch:{ InterruptedException -> 0x000e, all -> 0x001d }
            r3.guardedRun()     // Catch:{ InterruptedException -> 0x0016, all -> 0x001d }
            java.util.concurrent.Semaphore r1 = com.boolbalabs.lib.game.GameThreadGL.sEglSemaphore
            r1.release()
        L_0x000d:
            return
        L_0x000e:
            r1 = move-exception
            r0 = r1
            java.util.concurrent.Semaphore r1 = com.boolbalabs.lib.game.GameThreadGL.sEglSemaphore
            r1.release()
            goto L_0x000d
        L_0x0016:
            r1 = move-exception
            java.util.concurrent.Semaphore r1 = com.boolbalabs.lib.game.GameThreadGL.sEglSemaphore
            r1.release()
            goto L_0x000d
        L_0x001d:
            r1 = move-exception
            java.util.concurrent.Semaphore r2 = com.boolbalabs.lib.game.GameThreadGL.sEglSemaphore
            r2.release()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.boolbalabs.lib.game.GameThreadGL.run():void");
    }

    public void feedInput(UserInputEvent input) {
        synchronized (this.threadUserInputQueueMutex) {
            try {
                this.threadUserInputQueue.put(input);
            } catch (InterruptedException e) {
                InterruptedException e2 = e;
                Log.e("EXCEPTION", e2.getMessage(), e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private void guardedRun() throws InterruptedException {
        while (!this.mDone) {
            if (this.mPaused) {
                try {
                    Thread.sleep(100);
                    if (this.mDone) {
                        return;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    this.mDone = true;
                }
            }
            if (!this.mDone) {
                long startTime = System.currentTimeMillis();
                processInput();
                this.game.update();
                try {
                    Thread.sleep(Math.max(16 - (System.currentTimeMillis() - startTime), 1L));
                } catch (InterruptedException e2) {
                }
            } else {
                return;
            }
        }
    }

    public void start() {
        this.mPaused = true;
        this.mDone = false;
        super.start();
    }

    public void setGame(Game game2) {
        this.game = game2;
    }

    public void pauseGame() {
        if (!this.mPaused) {
            DebugLog.i("THREAD", "pauseGame CALLED");
            this.mPaused = true;
            this.game.onPause();
        }
    }

    public void resumeGame() {
        if (this.mPaused) {
            DebugLog.i("THREAD", "resumeGame CALLED");
            this.mPaused = false;
            this.game.onResume();
        }
    }

    public void requestExitAndWait() {
        synchronized (this) {
            try {
                pauseGame();
                this.mDone = true;
                interrupt();
            } catch (Exception e) {
            }
        }
    }

    private void processInput() {
        synchronized (this.threadUserInputQueueMutex) {
            ArrayBlockingQueue<UserInputEvent> threadInputQueue = this.threadUserInputQueue;
            while (!threadInputQueue.isEmpty()) {
                try {
                    UserInputEvent inputEvent = threadInputQueue.take();
                    if (inputEvent.eventType == 1) {
                        processKeyEvent(inputEvent);
                    } else if (inputEvent.eventType == 2) {
                        processTouchEvent(inputEvent);
                    }
                    inputEvent.returnToPool();
                } catch (InterruptedException e) {
                    InterruptedException e2 = e;
                    Log.e("EXCEPTION", e2.getMessage(), e2);
                }
            }
        }
    }

    private void processKeyEvent(UserInputEvent inputEvent) {
    }

    private void processTouchEvent(UserInputEvent inputEvent) {
        int touchX = inputEvent.x;
        int touchY = inputEvent.y;
        if (inputEvent.action == 3) {
            this.lastTouchDownPoint.set(touchX, touchY);
            this.game.onTouchDown(this.lastTouchDownPoint);
        } else if (inputEvent.action == 4) {
            this.lastMovePoint.set(touchX, touchY);
            this.game.onTouchMove(this.lastTouchDownPoint, this.lastMovePoint);
        } else if (inputEvent.action == 5) {
            this.lastTouchUpPoint.set(touchX, touchY);
            this.game.onTouchUp(this.lastTouchDownPoint, this.lastTouchUpPoint);
        }
    }
}
