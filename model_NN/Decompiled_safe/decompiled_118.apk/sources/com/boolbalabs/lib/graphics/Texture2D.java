package com.boolbalabs.lib.graphics;

import android.graphics.Bitmap;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.util.Log;
import com.boolbalabs.lib.managers.BitmapManager;
import com.boolbalabs.lib.managers.TexturesManager;
import javax.microedition.khronos.opengles.GL10;

public class Texture2D {
    private int height;
    private TextureFilter maxFilter;
    private TextureFilter minFilter;
    private int resourceId;
    private TextureWrap sWrap;
    private TextureWrap tWrap;
    private int textureGlobalIndex = -1;
    private int width;

    public enum TextureFilter {
        Nearest,
        Linear,
        MipMap
    }

    public enum TextureWrap {
        ClampToEdge,
        Wrap
    }

    public Texture2D(int resId, TextureFilter minFilter2, TextureFilter maxFilter2, TextureWrap sWrap2, TextureWrap tWrap2) {
        this.resourceId = resId;
        this.minFilter = minFilter2;
        this.maxFilter = maxFilter2;
        this.sWrap = sWrap2;
        this.tWrap = tWrap2;
    }

    public void buildTexture(GL10 gl) {
        if (gl != null) {
            int[] textures = new int[1];
            gl.glGenTextures(1, textures, 0);
            this.textureGlobalIndex = textures[0];
            Bitmap image = BitmapManager.decodeBitmap(this.resourceId, TexturesManager.textureBitmapOptions);
            this.width = image.getWidth();
            this.height = image.getHeight();
            gl.glBindTexture(3553, this.textureGlobalIndex);
            gl.glTexParameterf(3553, 10241, (float) getTextureFilter(this.minFilter));
            gl.glTexParameterf(3553, 10240, (float) getTextureFilter(this.maxFilter));
            gl.glTexParameterf(3553, 10242, (float) getTextureWrap(this.sWrap));
            gl.glTexParameterf(3553, 10243, (float) getTextureWrap(this.tWrap));
            gl.glTexEnvf(8960, 8704, 7681.0f);
            GLUtils.texImage2D(3553, 0, image, 0);
            image.recycle();
            gl.glMatrixMode(5890);
            gl.glLoadIdentity();
            Log.i("TEXTURE", " TEXTURE #" + this.textureGlobalIndex + " created...");
            int error = gl.glGetError();
            if (error != 0) {
                Log.e("GLUtils", "ERROR: " + GLU.gluErrorString(error));
            }
        }
    }

    private int getTextureFilter(TextureFilter filter) {
        if (filter == TextureFilter.Linear) {
            return 9729;
        }
        if (filter == TextureFilter.Nearest) {
            return 9728;
        }
        return 9985;
    }

    private int getTextureWrap(TextureWrap wrap) {
        if (wrap == TextureWrap.ClampToEdge) {
            return 33071;
        }
        return 10497;
    }

    public void dispose(GL10 gl) {
        gl.glDeleteTextures(1, new int[]{this.textureGlobalIndex}, 0);
        this.textureGlobalIndex = 0;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public int getResourceId() {
        return this.resourceId;
    }

    public int getTextureGlobalIndex() {
        return this.textureGlobalIndex;
    }
}
