package com.boolbalabs.lib.graphics;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.managers.BitmapManager;
import com.boolbalabs.lib.utils.ScreenMetrics;

public class BitmapFrames {
    private BitmapManager bitmapManager = BitmapManager.getInstance();
    private int bitmapRef;
    private int bmHeightPix;
    private int bmWidthPix;
    private Rect drawFrameDestRectArdp = new Rect();
    private Rect drawFrameInRectPix_SourceRect = new Rect();
    private Rect drawFrameSourceRect = new Rect();
    private float frameHeightPix;
    private float frameWidthPix;
    private int horFrameCount;

    public BitmapFrames(int bitmapRef2, int bitmapWidth, int bitmapHeight, int horFrameCount2, int verFrameCount) {
        this.bitmapRef = bitmapRef2;
        this.bmWidthPix = bitmapWidth;
        this.bmHeightPix = bitmapHeight;
        this.horFrameCount = horFrameCount2;
        this.frameWidthPix = (float) (this.bmWidthPix / horFrameCount2);
        this.frameHeightPix = (float) (this.bmHeightPix / verFrameCount);
    }

    public void release() {
        this.bitmapManager = null;
    }

    private void getFrame(int index, Rect frame) {
        int frameRowIndex = index / this.horFrameCount;
        frame.left = (int) ((((float) (index % this.horFrameCount)) * this.frameWidthPix) + 0.0f);
        frame.top = (int) ((((float) frameRowIndex) * this.frameHeightPix) + 0.0f);
        frame.right = (int) (((float) frame.left) + this.frameWidthPix);
        frame.bottom = (int) (((float) frame.top) + this.frameHeightPix);
    }

    private void getFrameTopPart(int frameIndex, int partsToGetCount, int totalPartsCount, Rect frame) {
        getFrame(frameIndex, frame);
        frame.bottom = frame.top + ((int) ((this.frameHeightPix * ((float) partsToGetCount)) / ((float) totalPartsCount)));
    }

    private void getFrameBottomPart(int frameIndex, int partsToGetCount, int totalPartsCount, Rect frame) {
        getFrame(frameIndex, frame);
        frame.top = frame.bottom - ((int) ((this.frameHeightPix * ((float) partsToGetCount)) / ((float) totalPartsCount)));
    }

    public void drawFrame(Canvas canvas, int frameIndex, Point topLeftDip) {
        getFrame(frameIndex, this.drawFrameSourceRect);
        int drawFrameDestTopLeftPoint_X = ScreenMetrics.convertRipToPixel((float) topLeftDip.x);
        int drawFrameDestTopLeftPoint_Y = ScreenMetrics.convertRipToPixel((float) topLeftDip.y);
        this.drawFrameDestRectArdp.left = drawFrameDestTopLeftPoint_X;
        this.drawFrameDestRectArdp.top = drawFrameDestTopLeftPoint_Y;
        this.drawFrameDestRectArdp.right = ((int) this.frameWidthPix) + drawFrameDestTopLeftPoint_X;
        this.drawFrameDestRectArdp.bottom = ((int) this.frameHeightPix) + drawFrameDestTopLeftPoint_Y;
        this.bitmapManager.drawBitmap(canvas, this.bitmapRef, this.drawFrameSourceRect, this.drawFrameDestRectArdp);
    }

    public void drawFrameTopPart(Canvas canvas, int frameIndex, Point topLeftDip, int partsToDrawCount, int totalPartsCount) {
        getFrameTopPart(frameIndex, partsToDrawCount, totalPartsCount, this.drawFrameSourceRect);
        int drawFrameDestTopLeftPoint_X = ScreenMetrics.convertRipToPixel((float) topLeftDip.x);
        int drawFrameDestTopLeftPoint_Y = ScreenMetrics.convertRipToPixel((float) topLeftDip.y) + ((int) ((this.frameHeightPix * ((float) (totalPartsCount - partsToDrawCount))) / ((float) totalPartsCount)));
        int destFrameWidth = this.drawFrameSourceRect.right - this.drawFrameSourceRect.left;
        int destFrameHeight = this.drawFrameSourceRect.bottom - this.drawFrameSourceRect.top;
        this.drawFrameDestRectArdp.left = drawFrameDestTopLeftPoint_X;
        this.drawFrameDestRectArdp.top = drawFrameDestTopLeftPoint_Y;
        this.drawFrameDestRectArdp.right = drawFrameDestTopLeftPoint_X + destFrameWidth;
        this.drawFrameDestRectArdp.bottom = drawFrameDestTopLeftPoint_Y + destFrameHeight;
        this.bitmapManager.drawBitmap(canvas, this.bitmapRef, this.drawFrameSourceRect, this.drawFrameDestRectArdp);
    }

    public void drawFrameBottomPart(Canvas canvas, int frameIndex, Point topLeftDip, int partsToDrawCount, int totalPartsCount) {
        getFrameBottomPart(frameIndex, partsToDrawCount, totalPartsCount, this.drawFrameSourceRect);
        int drawFrameDestTopLeftPoint_X = ScreenMetrics.convertRipToPixel((float) topLeftDip.x);
        int drawFrameDestTopLeftPoint_Y = ScreenMetrics.convertRipToPixel((float) topLeftDip.y);
        int destFrameWidth = this.drawFrameSourceRect.right - this.drawFrameSourceRect.left;
        int destFrameHeight = this.drawFrameSourceRect.bottom - this.drawFrameSourceRect.top;
        this.drawFrameDestRectArdp.left = drawFrameDestTopLeftPoint_X;
        this.drawFrameDestRectArdp.top = drawFrameDestTopLeftPoint_Y;
        this.drawFrameDestRectArdp.right = drawFrameDestTopLeftPoint_X + destFrameWidth;
        this.drawFrameDestRectArdp.bottom = drawFrameDestTopLeftPoint_Y + destFrameHeight;
        this.bitmapManager.drawBitmap(canvas, this.bitmapRef, this.drawFrameSourceRect, this.drawFrameDestRectArdp);
    }

    public void drawFrameInRectPix(Canvas canvas, int frameIndex, Rect destRectPix) {
        getFrame(frameIndex, this.drawFrameInRectPix_SourceRect);
        this.bitmapManager.drawBitmap(canvas, this.bitmapRef, this.drawFrameInRectPix_SourceRect, destRectPix);
    }

    public int getSingleFrameWidthPix() {
        return (int) this.frameWidthPix;
    }

    public int getSingleFrameHeightPix() {
        return (int) this.frameHeightPix;
    }
}
