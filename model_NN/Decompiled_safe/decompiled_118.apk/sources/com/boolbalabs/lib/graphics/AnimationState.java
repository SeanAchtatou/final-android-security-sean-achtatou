package com.boolbalabs.lib.graphics;

import com.boolbalabs.lib.utils.DebugLog;

public class AnimationState {
    public int animationCycleLengthMs;
    public int[] delays;
    public int id;
    public int[] indices;
    public boolean isLooped;
    public int size;

    public AnimationState(int id2, int[] frameIndices, int[] frameDelays, boolean isLooped2) {
        if (frameIndices.length != frameDelays.length) {
            DebugLog.i("ZSprite2D", "frameIndices.length != frameDelays.length");
            return;
        }
        this.id = id2;
        this.indices = frameIndices;
        this.delays = frameDelays;
        this.size = this.indices.length;
        this.animationCycleLengthMs = this.delays[this.size - 1];
        this.isLooped = isLooped2;
    }

    public boolean isEqual(AnimationState anotherState) {
        return this.id == anotherState.id;
    }
}
