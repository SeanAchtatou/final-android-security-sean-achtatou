package com.boolbalabs.lib.graphics;

import android.graphics.Rect;
import android.os.SystemClock;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.BufferUtils;
import com.boolbalabs.lib.utils.DebugLog;
import com.boolbalabs.lib.utils.MathUtils;
import java.lang.reflect.Array;
import java.nio.FloatBuffer;
import java.util.HashMap;

public class ZSprite2D extends ZNode {
    private static final int maxStartDelayMs = 5000;
    private final int ANIMATION_DEFAULT_ID = 0;
    private int curFrameIndexToDraw;
    private AnimationState currentAnimationState;
    private AnimationState defaultAnimationState;
    private int defaultFrameIndexToDraw = -1;
    private FloatBuffer[] frameTextureBuffers;
    private String[] framesNames;
    private int[][] framesRectsOnTexture;
    private boolean isFinished = false;
    private boolean isStarted = false;
    private long pauseTime;
    private long resumeTime;
    private int startAnimationDelay = 0;
    private HashMap<Integer, AnimationState> states = new HashMap<>();
    private Rect tempRect = new Rect();
    private long timeAnimationStartedMs = -1;
    private int totalFramesCount;
    private boolean updatable;
    private int visibleDelay = 0;

    public ZSprite2D(int resourceId, int drawingMode) {
        super(resourceId, drawingMode);
    }

    public void initializeAnimation(String[] framesNames2, int[] frameIndices, int[] frameDelays, Rect screenRect, boolean isLooped) {
        if (framesNames2 == null || framesNames2.length == 0) {
            DebugLog.i("ZSprite2D", "frameNames is empty");
        } else if (MathUtils.getMaxElement(frameIndices) >= framesNames2.length) {
            DebugLog.i("ZSprite2D", "frameIndices length is bigger than framesNames.length");
        } else {
            this.framesNames = framesNames2;
            this.totalFramesCount = frameIndices.length;
            this.isFinished = false;
            this.isStarted = false;
            this.defaultAnimationState = new AnimationState(0, frameIndices, frameDelays, isLooped);
            this.currentAnimationState = this.defaultAnimationState;
            this.states.put(0, this.defaultAnimationState);
            setFrameInRip(screenRect);
        }
    }

    public void initializeAnimation(String[] framesNames2, int animationSpeed, Rect screenRect, boolean isLooped) {
        int length = framesNames2.length;
        int[] frameIndices = new int[length];
        for (int i = 0; i < length; i++) {
            frameIndices[i] = i;
        }
        int[] frameDelays = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            frameDelays[i2] = (i2 * animationSpeed) + 10;
        }
        initializeAnimation(framesNames2, frameIndices, frameDelays, screenRect, isLooped);
    }

    public void addState(AnimationState state) {
        if (state == null) {
            DebugLog.i("ZSprite2D", "ERROR: state is NULL");
        } else if (state.id == 0) {
            DebugLog.i("ZSprite2D", "defaultAnimationState.id cannot be equal to state.id");
        } else {
            this.states.put(Integer.valueOf(state.id), state);
        }
    }

    private void initRectsOnTexture() {
        TexturesManager texturesManager = TexturesManager.getInstance();
        int size = this.framesNames.length;
        this.framesRectsOnTexture = (int[][]) Array.newInstance(Integer.TYPE, size, 4);
        if (getDrawingMode() == 1) {
            this.frameTextureBuffers = new FloatBuffer[size];
        }
        for (int i = 0; i < size; i++) {
            Rect currentTextureRect = texturesManager.getRectByFrameName(this.framesNames[i]);
            if (currentTextureRect != null) {
                int frameWidthOnTexturePix = currentTextureRect.width();
                int frameHeightOnTexturePix = currentTextureRect.height();
                this.framesRectsOnTexture[i] = new int[]{currentTextureRect.left, currentTextureRect.bottom, frameWidthOnTexturePix, -frameHeightOnTexturePix};
                if (getDrawingMode() == 1) {
                    this.frameTextureBuffers[i] = BufferUtils.makeTextureBuffer(currentTextureRect, getTexture().getWidth(), getTexture().getHeight());
                }
            }
        }
    }

    public void onAfterLoad() {
        initRectsOnTexture();
        super.onAfterLoad();
    }

    public void setRectOnTexture(int[] cropRect) {
        this.tempRect.set(cropRect[0], cropRect[1] + cropRect[3], cropRect[0] + cropRect[2], cropRect[1]);
        if (getDrawingMode() == 0) {
            setRectOnTexture(this.tempRect);
        } else if (getDrawingMode() == 1) {
            setTextureBuffer(this.frameTextureBuffers[this.curFrameIndexToDraw]);
        }
    }

    public void setRectsForDefaultAnimationOES(int[][] framesRectsOnTexture2) {
        if (MathUtils.getMaxElement(this.defaultAnimationState.indices) < framesRectsOnTexture2.length) {
            this.framesRectsOnTexture = framesRectsOnTexture2;
        }
    }

    public void setFrameNames(String[] framesNames2) {
        if (MathUtils.getMaxElement(this.defaultAnimationState.indices) < framesNames2.length) {
            this.framesNames = framesNames2;
        }
    }

    public boolean isFinished() {
        return this.isFinished;
    }

    public boolean isStarted() {
        return this.isStarted;
    }

    public void startDefaultAnimation() {
        this.currentAnimationState = this.defaultAnimationState;
        this.isStarted = true;
        this.timeAnimationStartedMs = -1;
        onAnimationStarted(this.currentAnimationState.id);
    }

    public void stopCurrentAnimation() {
        this.isFinished = false;
        this.isStarted = false;
    }

    public void startAnimation() {
        startDefaultAnimation();
    }

    public void startAnimation(int stateId) {
        if (!this.states.containsKey(Integer.valueOf(stateId))) {
            DebugLog.i("ZSprite2D", "Cannot start animation: no such state " + stateId);
            return;
        }
        this.currentAnimationState = this.states.get(Integer.valueOf(stateId));
        if (MathUtils.getMaxElement(this.currentAnimationState.indices) >= this.framesNames.length) {
            DebugLog.i("ZSprite2D", "Cannot start animation: indices.length is bigger than framesNames.length");
            return;
        }
        this.isFinished = false;
        this.isStarted = true;
        this.timeAnimationStartedMs = -1;
        onAnimationStarted(stateId);
    }

    public void startWithRandomDelays(boolean visDelay, int maxVisibleDelayMs, boolean animDelay, int maxAnimationDelayMs) {
    }

    public void resetAnimation() {
        stopCurrentAnimation();
    }

    public void stopAnimation() {
        stopCurrentAnimation();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void update() {
        /*
            r12 = this;
            r11 = 1
            r10 = 0
            com.boolbalabs.lib.graphics.AnimationState r5 = r12.currentAnimationState
            monitor-enter(r5)
            boolean r6 = r12.isStarted     // Catch:{ all -> 0x0043 }
            if (r6 == 0) goto L_0x000d
            boolean r6 = r12.isFinished     // Catch:{ all -> 0x0043 }
            if (r6 == 0) goto L_0x0021
        L_0x000d:
            int r6 = r12.defaultFrameIndexToDraw     // Catch:{ all -> 0x0043 }
            r7 = -1
            if (r6 <= r7) goto L_0x001f
            int r6 = r12.defaultFrameIndexToDraw     // Catch:{ all -> 0x0043 }
            r12.curFrameIndexToDraw = r6     // Catch:{ all -> 0x0043 }
            int[][] r6 = r12.framesRectsOnTexture     // Catch:{ all -> 0x0043 }
            int r7 = r12.curFrameIndexToDraw     // Catch:{ all -> 0x0043 }
            r6 = r6[r7]     // Catch:{ all -> 0x0043 }
            r12.setRectOnTexture(r6)     // Catch:{ all -> 0x0043 }
        L_0x001f:
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
        L_0x0020:
            return
        L_0x0021:
            long r3 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x0043 }
            long r6 = r12.timeAnimationStartedMs     // Catch:{ all -> 0x0043 }
            r8 = -1
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 != 0) goto L_0x002f
            r12.timeAnimationStartedMs = r3     // Catch:{ all -> 0x0043 }
        L_0x002f:
            long r6 = r12.timeAnimationStartedMs     // Catch:{ all -> 0x0043 }
            long r0 = r3 - r6
            int r6 = r12.visibleDelay     // Catch:{ all -> 0x0043 }
            long r6 = (long) r6     // Catch:{ all -> 0x0043 }
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 < 0) goto L_0x0046
            r6 = r11
        L_0x003b:
            r12.updatable = r6     // Catch:{ all -> 0x0043 }
            boolean r6 = r12.updatable     // Catch:{ all -> 0x0043 }
            if (r6 != 0) goto L_0x0048
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x0020
        L_0x0043:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            throw r6
        L_0x0046:
            r6 = r10
            goto L_0x003b
        L_0x0048:
            int r6 = r12.startAnimationDelay     // Catch:{ all -> 0x0043 }
            long r6 = (long) r6     // Catch:{ all -> 0x0043 }
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 >= 0) goto L_0x0063
            com.boolbalabs.lib.graphics.AnimationState r6 = r12.currentAnimationState     // Catch:{ all -> 0x0043 }
            int[] r6 = r6.indices     // Catch:{ all -> 0x0043 }
            r7 = 0
            r6 = r6[r7]     // Catch:{ all -> 0x0043 }
            r12.curFrameIndexToDraw = r6     // Catch:{ all -> 0x0043 }
            int[][] r6 = r12.framesRectsOnTexture     // Catch:{ all -> 0x0043 }
            int r7 = r12.curFrameIndexToDraw     // Catch:{ all -> 0x0043 }
            r6 = r6[r7]     // Catch:{ all -> 0x0043 }
            r12.setRectOnTexture(r6)     // Catch:{ all -> 0x0043 }
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x0020
        L_0x0063:
            com.boolbalabs.lib.graphics.AnimationState r6 = r12.currentAnimationState     // Catch:{ all -> 0x0043 }
            int r6 = r6.animationCycleLengthMs     // Catch:{ all -> 0x0043 }
            int r7 = r12.startAnimationDelay     // Catch:{ all -> 0x0043 }
            int r6 = r6 + r7
            long r6 = (long) r6     // Catch:{ all -> 0x0043 }
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x008a
            com.boolbalabs.lib.graphics.AnimationState r6 = r12.currentAnimationState     // Catch:{ all -> 0x0043 }
            boolean r6 = r6.isLooped     // Catch:{ all -> 0x0043 }
            if (r6 == 0) goto L_0x009c
            r6 = 0
            r12.visibleDelay = r6     // Catch:{ all -> 0x0043 }
            int r6 = r12.startAnimationDelay     // Catch:{ all -> 0x0043 }
            long r6 = (long) r6     // Catch:{ all -> 0x0043 }
            int r8 = r12.startAnimationDelay     // Catch:{ all -> 0x0043 }
            long r8 = (long) r8     // Catch:{ all -> 0x0043 }
            long r8 = r0 - r8
            com.boolbalabs.lib.graphics.AnimationState r10 = r12.currentAnimationState     // Catch:{ all -> 0x0043 }
            int r10 = r10.animationCycleLengthMs     // Catch:{ all -> 0x0043 }
            long r10 = (long) r10     // Catch:{ all -> 0x0043 }
            long r8 = r8 % r10
            long r0 = r6 + r8
            r12.timeAnimationStartedMs = r3     // Catch:{ all -> 0x0043 }
        L_0x008a:
            r2 = 0
        L_0x008b:
            com.boolbalabs.lib.graphics.AnimationState r6 = r12.currentAnimationState     // Catch:{ all -> 0x0043 }
            int r6 = r6.size     // Catch:{ all -> 0x0043 }
            if (r2 < r6) goto L_0x00ac
            com.boolbalabs.lib.graphics.AnimationState r6 = r12.currentAnimationState     // Catch:{ all -> 0x0043 }
            int[] r6 = r6.indices     // Catch:{ all -> 0x0043 }
            r7 = 0
            r6 = r6[r7]     // Catch:{ all -> 0x0043 }
            r12.curFrameIndexToDraw = r6     // Catch:{ all -> 0x0043 }
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x0020
        L_0x009c:
            r6 = 1
            r12.isFinished = r6     // Catch:{ all -> 0x0043 }
            r6 = 0
            r12.isStarted = r6     // Catch:{ all -> 0x0043 }
            com.boolbalabs.lib.graphics.AnimationState r6 = r12.currentAnimationState     // Catch:{ all -> 0x0043 }
            int r6 = r6.id     // Catch:{ all -> 0x0043 }
            r12.onAnimationFinished(r6)     // Catch:{ all -> 0x0043 }
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x0020
        L_0x00ac:
            com.boolbalabs.lib.graphics.AnimationState r6 = r12.currentAnimationState     // Catch:{ all -> 0x0043 }
            int[] r6 = r6.delays     // Catch:{ all -> 0x0043 }
            r6 = r6[r2]     // Catch:{ all -> 0x0043 }
            long r6 = (long) r6     // Catch:{ all -> 0x0043 }
            int r8 = r12.startAnimationDelay     // Catch:{ all -> 0x0043 }
            long r8 = (long) r8     // Catch:{ all -> 0x0043 }
            long r8 = r0 - r8
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 <= 0) goto L_0x00d0
            com.boolbalabs.lib.graphics.AnimationState r6 = r12.currentAnimationState     // Catch:{ all -> 0x0043 }
            int[] r6 = r6.indices     // Catch:{ all -> 0x0043 }
            r6 = r6[r2]     // Catch:{ all -> 0x0043 }
            r12.curFrameIndexToDraw = r6     // Catch:{ all -> 0x0043 }
            int[][] r6 = r12.framesRectsOnTexture     // Catch:{ all -> 0x0043 }
            int r7 = r12.curFrameIndexToDraw     // Catch:{ all -> 0x0043 }
            r6 = r6[r7]     // Catch:{ all -> 0x0043 }
            r12.setRectOnTexture(r6)     // Catch:{ all -> 0x0043 }
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x0020
        L_0x00d0:
            int r2 = r2 + 1
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.boolbalabs.lib.graphics.ZSprite2D.update():void");
    }

    public void onAnimationStarted(int state) {
    }

    public void onAnimationFinished(int state) {
    }

    @Deprecated
    private static int[] getRandomizedFrameDelays(int[] frameDelaysArray) {
        int randomOffsetMs = MathUtils.rand.nextInt(maxStartDelayMs);
        int framesCount = frameDelaysArray.length;
        int[] result = new int[framesCount];
        for (int i = 0; i < framesCount; i++) {
            result[i] = frameDelaysArray[i] + randomOffsetMs;
        }
        return result;
    }

    public void setDefaultFrameIndexToDraw(int frameIndex) {
        this.defaultFrameIndexToDraw = frameIndex;
    }

    public int getCurrentAnimationStateId() {
        if (this.currentAnimationState != null) {
            return this.currentAnimationState.id;
        }
        return 0;
    }

    public void pause() {
        this.pauseTime = SystemClock.uptimeMillis();
    }

    public void resume() {
        this.resumeTime = SystemClock.uptimeMillis();
        this.timeAnimationStartedMs = (this.timeAnimationStartedMs + this.resumeTime) - this.pauseTime;
    }
}
