package com.boolbalabs.lib.graphics;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.SystemClock;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.ScreenMetrics;
import java.lang.reflect.Array;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

public class NumberView2D extends ZNode {
    public static int ALIGN_CENTER = 2;
    public static int ALIGN_LEFT = 0;
    public static int ALIGN_RIGHT = 1;
    public static final int MAX_DIGIT_COUNT = 10;
    public static final int NUMBER_OF_DIGITS = 10;
    private int align;
    private int[][] digitsCropWorkspace = ((int[][]) Array.newInstance(Integer.TYPE, 10, 4));
    private boolean digitsDrawingDirection = true;
    private String[] digitsFramesNames;
    public int[] digitsToDraw = new int[10];
    private int[] digits_width = new int[10];
    public NumberDrawingMode drawingMode = NumberDrawingMode.NDISCRETE;
    private int drawingSpeed = 50;
    private Rect fakeFrame;
    private int farRightPositionPix = 0;
    private Point fixedPointPix;
    private int incrementValue = 1;
    public int lastDigitIndex = 0;
    private int numberWidthPix = 0;
    protected int oesY;
    private int previousRealValue = 0;
    private long previousUpdateTime = 0;
    private boolean shouldCallUpdate = false;
    private int sizePix;
    private int sizeRip;
    private TexturesManager texturesManager;
    private int theNumberToDraw = 0;
    private int theRealValue = 0;

    public enum NumberDrawingMode {
        NDISCRETE,
        NCONTINUOUS
    }

    public NumberView2D(int resourceId) {
        super(resourceId, 0);
        this.userInteractionEnabled = false;
    }

    public void initNumberView(Point fixedPointRip, String[] digitsFramesNames2, int align2, int sizeRip2) {
        this.sizeRip = sizeRip2;
        this.sizePix = ScreenMetrics.convertRipToPixel((float) sizeRip2);
        setNumberToDraw(0);
        this.digitsFramesNames = digitsFramesNames2;
        setFixedPointRip(fixedPointRip);
        setAlignment(align2);
    }

    public void initialize() {
        this.texturesManager = TexturesManager.getInstance();
        initDigitsCropWorkspace();
        this.fakeFrame = new Rect(0, 0, this.sizePix, this.sizePix);
        super.setFrameInRip(this.fakeFrame);
    }

    private void initDigitsCropWorkspace() {
        setCorrectDigitsWidth();
        for (int i = 0; i < 10; i++) {
            Rect currentTextureRect = this.texturesManager.getRectByFrameName(this.digitsFramesNames[i]);
            if (currentTextureRect != null) {
                int frameWidthOnTexturePix = currentTextureRect.width();
                int frameHeightOnTexturePix = currentTextureRect.height();
                this.digitsCropWorkspace[i] = new int[]{currentTextureRect.left, currentTextureRect.bottom, frameWidthOnTexturePix, -frameHeightOnTexturePix};
            }
        }
    }

    private void setCorrectDigitsWidth() {
        for (int i = 0; i < 10; i++) {
            Rect currentTextureRect = this.texturesManager.getRectByFrameName(this.digitsFramesNames[i]);
            if (currentTextureRect != null) {
                int frameWidthOnTexturePix = currentTextureRect.width();
                this.digits_width[i] = (int) (((float) frameWidthOnTexturePix) * (((float) this.sizePix) / ((float) currentTextureRect.height())));
            }
        }
    }

    public void setNumberToDraw(int theNumberToDraw2) {
        this.theRealValue = Math.abs(theNumberToDraw2);
        if (this.drawingMode == NumberDrawingMode.NDISCRETE) {
            this.theNumberToDraw = this.theRealValue;
        } else if (this.previousRealValue != this.theRealValue || this.theRealValue == 0) {
            this.theNumberToDraw = this.previousRealValue;
            this.previousRealValue = this.theRealValue;
            this.shouldCallUpdate = true;
        }
        calculateDigits();
    }

    public int getRealValue() {
        return this.theRealValue;
    }

    public void increaseNumberToDrawBy(int increment) {
        setNumberToDraw(this.theNumberToDraw + increment);
    }

    public void increaseInstantly() {
        this.theNumberToDraw = this.theRealValue;
        this.previousRealValue = this.theRealValue;
        calculateDigits();
    }

    private void calculateDigits() {
        int curModulusDivider = 10;
        int curIntegerDivider = 1;
        this.numberWidthPix = 0;
        for (int i = 0; i < 10; i++) {
            this.digitsToDraw[i] = 0;
        }
        for (int i2 = 0; i2 < 10; i2++) {
            this.digitsToDraw[i2] = (this.theNumberToDraw % curModulusDivider) / curIntegerDivider;
            this.numberWidthPix += this.digits_width[this.digitsToDraw[i2]];
            if (curModulusDivider > this.theNumberToDraw) {
                this.lastDigitIndex = i2;
                return;
            }
            curModulusDivider *= 10;
            curIntegerDivider *= 10;
        }
        this.lastDigitIndex = 9;
    }

    public void setContinuousDrawingParameters(int drawingSpeed2, int incrementValue2) {
        this.drawingSpeed = drawingSpeed2;
        this.incrementValue = incrementValue2;
    }

    public void update() {
        if (this.shouldCallUpdate && this.drawingMode == NumberDrawingMode.NCONTINUOUS && SystemClock.uptimeMillis() - this.previousUpdateTime > ((long) this.drawingSpeed)) {
            this.previousUpdateTime = SystemClock.uptimeMillis();
            this.theNumberToDraw += this.incrementValue;
            if ((this.theNumberToDraw > this.theRealValue && this.incrementValue >= 0) || (this.theNumberToDraw < this.theRealValue && this.incrementValue < 0)) {
                this.theNumberToDraw = this.theRealValue;
                this.shouldCallUpdate = false;
            }
            calculateDigits();
        }
    }

    /* access modifiers changed from: protected */
    public void drawOES(GL10 gl) {
        gl.glBindTexture(3553, getTextureGlobalIndex());
        int currLeftPosX = 0;
        if (this.align == ALIGN_LEFT) {
            currLeftPosX = this.fixedPointPix.x;
            this.digitsDrawingDirection = true;
        } else if (this.align == ALIGN_RIGHT) {
            currLeftPosX = this.fixedPointPix.x;
            this.digitsDrawingDirection = false;
        } else if (this.align == ALIGN_CENTER) {
            this.digitsDrawingDirection = true;
            currLeftPosX = this.fixedPointPix.x - (this.numberWidthPix / 2);
        }
        if (this.digitsDrawingDirection) {
            for (int i = this.lastDigitIndex; i >= 0; i--) {
                int digitToDraw = this.digitsToDraw[i];
                ((GL11) gl).glTexParameteriv(3553, 35741, this.digitsCropWorkspace[digitToDraw], 0);
                ((GL11Ext) gl).glDrawTexfOES((float) currLeftPosX, (float) this.oesY, getCoordsOES_Z(), (float) this.digits_width[digitToDraw], (float) this.sizePix);
                currLeftPosX = this.digits_width[digitToDraw] + currLeftPosX + 1;
            }
            this.farRightPositionPix = currLeftPosX;
            return;
        }
        this.farRightPositionPix = currLeftPosX;
        for (int i2 = 0; i2 <= this.lastDigitIndex; i2++) {
            int digitToDraw2 = this.digitsToDraw[i2];
            currLeftPosX = (currLeftPosX - this.digits_width[digitToDraw2]) - 1;
            ((GL11) gl).glTexParameteriv(3553, 35741, this.digitsCropWorkspace[digitToDraw2], 0);
            ((GL11Ext) gl).glDrawTexfOES((float) currLeftPosX, (float) this.oesY, getCoordsOES_Z(), (float) this.digits_width[digitToDraw2], (float) this.sizePix);
        }
    }

    public int getFarRightPositionPix() {
        return this.farRightPositionPix;
    }

    public void setAlignment(int align2) {
        if (align2 < 0 || align2 > 2) {
            this.align = ALIGN_LEFT;
        } else {
            this.align = align2;
        }
    }

    public void setFixedPointRip(Point fixedPointRip) {
        setCenterInRip(fixedPointRip.x, fixedPointRip.y);
        this.fixedPointPix = getCenterInPix();
        this.oesY = (ScreenMetrics.screenHeightPix - this.fixedPointPix.y) - this.sizePix;
    }

    public void setFrameInRip(Rect frame) {
        this.sizePix = frame.height();
        setCorrectDigitsWidth();
        calculateDigits();
    }

    public void setFixedPointPix(Point fixedPPix) {
        this.fixedPointPix.set(fixedPPix.x, fixedPPix.y);
        this.oesY = (ScreenMetrics.screenHeightPix - this.fixedPointPix.y) - this.sizePix;
    }

    /* access modifiers changed from: protected */
    public int getFixedPointPix_Y() {
        return this.fixedPointPix.y;
    }

    /* access modifiers changed from: protected */
    public int getSizePix() {
        return this.sizePix;
    }

    /* access modifiers changed from: protected */
    public int getSizeRip() {
        return this.sizeRip;
    }
}
