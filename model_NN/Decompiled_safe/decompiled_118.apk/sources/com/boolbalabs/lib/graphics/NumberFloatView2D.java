package com.boolbalabs.lib.graphics;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import com.boolbalabs.lib.utils.MathUtils;
import com.boolbalabs.lib.utils.ScreenMetrics;
import javax.microedition.khronos.opengles.GL10;

public class NumberFloatView2D extends NumberView2D {
    private int digitsAfterPoint;
    private Point fixedFracPointPix = new Point();
    private NumberView2D fractionalPartView;
    private int pWidth;
    private String pointName;
    private Rect pointRectOnScreenRip = new Rect();
    private Rect pointRectOnTexture;
    private ZNode pointView;
    private int smallSkip;
    private TexturesManager texturesManager;

    public NumberFloatView2D(int resourceId) {
        super(resourceId);
        this.fractionalPartView = new NumberView2D(resourceId);
        this.pointView = new ZNode(resourceId, 0);
    }

    public void initNumberView(Point fixedPointRip, String[] digitsFramesNames, int align, int sizeRip) {
        throw new IllegalArgumentException("Don't use this method, use another initNumberView");
    }

    public void initNumberView(Point fixedPointRip, String[] digitsFramesNames, String pointName2, int align, int sizeRip, int digitsAfterPoint2) {
        this.texturesManager = TexturesManager.getInstance();
        this.pointName = pointName2;
        this.digitsAfterPoint = digitsAfterPoint2;
        super.initNumberView(fixedPointRip, digitsFramesNames, align, sizeRip);
        this.pointRectOnTexture = this.texturesManager.getRectByFrameName(pointName2);
        ScreenMetrics.convertRectPixToRip(this.pointRectOnTexture, this.pointRectOnScreenRip);
        this.pointView.initWithFrame(this.pointRectOnScreenRip, this.pointRectOnTexture);
        this.fractionalPartView.initNumberView(fixedPointRip, digitsFramesNames, NumberView2D.ALIGN_LEFT, sizeRip);
        this.smallSkip = getSizePix() - this.pointView.getFrameInPix().height();
        this.pWidth = this.pointView.getFrameInPix().width();
    }

    public void setNumberToDrawF(float numberToDraw) {
        super.setNumberToDraw((int) numberToDraw);
        this.fractionalPartView.setNumberToDraw(MathUtils.getFractionalPartAsInt(numberToDraw, this.digitsAfterPoint));
    }

    public void setNumberToDraw(int numberToDraw) {
        setNumberToDrawF((float) numberToDraw);
    }

    public void increaseNumberToDrawBy(int increment) {
        setNumberToDrawF((float) (getRealValue() + increment));
    }

    public void initialize() {
        super.initialize();
        this.fractionalPartView.initialize();
        this.pointView.initialize();
    }

    public void loadContent() {
        super.loadContent();
        this.fractionalPartView.loadContent();
        this.pointView.loadContent();
    }

    public void unloadContent() {
        this.fractionalPartView.unloadContent();
        this.pointView.unloadContent();
        super.unloadContent();
    }

    public void update() {
        super.update();
        this.fractionalPartView.update();
    }

    public void onAfterLoad() {
        super.onAfterLoad();
        this.pointView.setTexture(getTexture());
        this.fractionalPartView.setTexture(getTexture());
    }

    public void draw(GL10 gl) {
        super.draw(gl);
        this.pointView.setPositionInPix(getFarRightPositionPix() + 1, getFixedPointPix_Y() + this.smallSkip);
        this.pointView.draw(gl);
        this.fixedFracPointPix.set(getFarRightPositionPix() + this.pWidth + 1, getFixedPointPix_Y());
        this.fractionalPartView.setFixedPointPix(this.fixedFracPointPix);
        this.fractionalPartView.draw(gl);
    }
}
