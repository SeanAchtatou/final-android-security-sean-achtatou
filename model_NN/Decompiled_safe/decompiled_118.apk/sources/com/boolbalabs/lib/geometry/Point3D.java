package com.boolbalabs.lib.geometry;

import java.io.Serializable;

public class Point3D implements Serializable {
    private static final long serialVersionUID = 172642776147L;
    public float x = 0.0f;
    public float y = 0.0f;
    public float z = 0.0f;

    public Point3D() {
    }

    public Point3D(float x2, float y2, float z2) {
        this.x = x2;
        this.y = y2;
        this.z = z2;
    }

    public Point3D(Point3D arg) {
        this.x = arg.x;
        this.y = arg.y;
        this.z = arg.z;
    }

    public void set(Point3D arg) {
        this.x = arg.x;
        this.y = arg.y;
        this.z = arg.z;
    }

    public void set(float x2, float y2, float z2) {
        this.x = x2;
        this.y = y2;
        this.z = z2;
    }

    public double norm() {
        return Math.sqrt((double) ((this.x * this.x) + (this.y * this.y) + (this.z * this.z)));
    }

    public void normalise() {
        double norm = norm();
        if (norm != 0.0d) {
            this.x = (float) (((double) this.x) / norm);
            this.y = (float) (((double) this.y) / norm);
            this.z = (float) (((double) this.z) / norm);
        }
    }

    public float dot(Point3D p) {
        return (this.x * p.x) + (this.y * p.y) + (this.z * p.z);
    }

    public float distanceTo(Point3D p) {
        return (float) Math.sqrt((double) (((this.x - p.x) * (this.x - p.x)) + ((this.y - p.y) * (this.y - p.y)) + ((this.z - p.z) * (this.z - p.z))));
    }

    public void mapToAntipodal() {
        this.x *= -1.0f;
        this.y *= -1.0f;
        this.z *= -1.0f;
    }

    public String toString() {
        return "Coordinates: (" + this.x + "/" + this.y + "/" + this.z + ")";
    }
}
