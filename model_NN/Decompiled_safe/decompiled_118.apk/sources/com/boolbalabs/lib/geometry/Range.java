package com.boolbalabs.lib.geometry;

public class Range {
    private float leftBoundary;
    private float rangeEnd;
    private float rangeLength;
    private float rangeStart;
    private float rightBoundary;

    public Range() {
        this.rangeStart = 0.0f;
        this.rangeEnd = 0.0f;
        adjustBoundaries();
    }

    public Range(int start, int end) {
        this.rangeStart = (float) start;
        this.rangeEnd = (float) end;
        adjustBoundaries();
    }

    public Range(float start, float end) {
        this.rangeStart = start;
        this.rangeEnd = end;
        adjustBoundaries();
    }

    public void set(int start, int end) {
        this.rangeStart = (float) start;
        this.rangeEnd = (float) end;
        adjustBoundaries();
    }

    public void set(float start, float end) {
        this.rangeStart = start;
        this.rangeEnd = end;
        adjustBoundaries();
    }

    private void adjustBoundaries() {
        this.leftBoundary = Math.min(this.rangeStart, this.rangeEnd);
        this.rightBoundary = Math.max(this.rangeStart, this.rangeEnd);
        this.rangeLength = this.rightBoundary - this.leftBoundary;
    }

    public boolean isInRange(float value) {
        float a = this.rangeStart;
        float b = this.rangeEnd;
        if (this.rangeEnd < this.rangeStart) {
            a = this.rangeEnd;
            b = this.rangeStart;
        }
        return a <= value && value < b;
    }

    public boolean isInRange(int value) {
        return isInRange((float) value);
    }

    public float getRangeIntermediateValue(float alpha) {
        float a = alpha;
        if (a < 0.0f) {
            a = 0.0f;
        }
        if (a > 1.0f) {
            a = 1.0f;
        }
        return this.leftBoundary + (this.rangeLength * a);
    }

    public float getLength() {
        return this.rangeLength;
    }

    public float getLeftBoundary() {
        return this.leftBoundary;
    }

    public float getRightBoundary() {
        return this.rightBoundary;
    }
}
