package com.boolbalabs.lib.geometry;

import android.graphics.PointF;

public class Line {
    private float a;
    private float b;
    private float defaultValue;
    private float u;
    private float v;
    private PointF valuePoint;
    private float x0;
    private float y0;

    public Line(float a2, float b2) {
        commonInitialize();
        set(a2, b2);
    }

    public Line(float startX, float startY, float endX, float endY) {
        commonInitialize();
        set(startX, startY, endX, endY);
    }

    public Line(PointF start, PointF end) {
        commonInitialize();
        set(start.x, start.y, end.x, end.y);
    }

    private void commonInitialize() {
        this.valuePoint = new PointF(0.0f, 0.0f);
    }

    public float getValue(float xValue) {
        this.valuePoint.x = xValue;
        this.valuePoint.y = (this.a * xValue) + this.b;
        return this.valuePoint.y;
    }

    public float getInverseValue(float yValue) {
        this.valuePoint.y = yValue;
        if (this.a != 0.0f) {
            this.valuePoint.x = (yValue - this.b) / this.a;
        } else {
            this.valuePoint.x = this.defaultValue;
        }
        return this.valuePoint.x;
    }

    public void set(float startX, float startY, float endX, float endY) {
        if (startX == endX) {
            this.a = 0.0f;
            this.defaultValue = startX;
            this.b = this.defaultValue;
        } else {
            this.a = (endY - startY) / (endX - startX);
            this.b = startY - (this.a * startX);
        }
        adjustParametricRepresentationNormed(startX, startY, endX, endY);
    }

    public void set(float a2, float b2) {
        this.a = a2;
        this.b = b2;
        adjustParametricRepresentation();
    }

    public PointF getParametricValue(float t) {
        this.valuePoint.x = this.x0 + (this.u * t);
        this.valuePoint.y = this.y0 + (this.v * t);
        return this.valuePoint;
    }

    private void adjustParametricRepresentation() {
        this.x0 = 0.0f;
        this.u = 1.0f;
        this.y0 = this.a;
        this.v = this.b;
    }

    private void adjustParametricRepresentationNormed(float startX, float startY, float endX, float endY) {
        this.x0 = startX;
        this.y0 = startY;
        this.u = endX - startX;
        this.v = endY - startY;
    }

    public String toString() {
        return "Line parameters:" + Float.toString(this.a) + "x+" + Float.toString(this.b);
    }
}
