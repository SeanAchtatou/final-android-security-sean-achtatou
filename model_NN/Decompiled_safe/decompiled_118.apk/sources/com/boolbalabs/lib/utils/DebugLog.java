package com.boolbalabs.lib.utils;

import android.util.Log;

public class DebugLog {
    private static boolean mLoggingEnabled = true;

    public static void setDebugLogging(boolean enabled) {
        mLoggingEnabled = enabled;
    }

    public static int v(String tag, String msg) {
        if (mLoggingEnabled) {
            return Log.v(tag, msg);
        }
        return 0;
    }

    public static int v(String tag, String msg, Throwable tr) {
        if (mLoggingEnabled) {
            return Log.v(tag, msg, tr);
        }
        return 0;
    }

    public static int d(String tag, String msg) {
        if (mLoggingEnabled) {
            return Log.d(tag, msg);
        }
        return 0;
    }

    public static int d(String tag, String msg, Throwable tr) {
        if (mLoggingEnabled) {
            return Log.d(tag, msg, tr);
        }
        return 0;
    }

    public static int i(String tag, String msg) {
        if (mLoggingEnabled) {
            return Log.i(tag, msg);
        }
        return 0;
    }

    public static int i(String tag, String msg, Throwable tr) {
        if (mLoggingEnabled) {
            return Log.i(tag, msg, tr);
        }
        return 0;
    }

    public static int i(String tag, float msg) {
        if (mLoggingEnabled) {
            return Log.i(tag, " " + msg);
        }
        return 0;
    }

    public static int i(String tag, int msg) {
        if (mLoggingEnabled) {
            return Log.i(tag, " " + msg);
        }
        return 0;
    }

    public static int i(String tag, double msg) {
        if (mLoggingEnabled) {
            return Log.i(tag, " " + msg);
        }
        return 0;
    }

    public static int w(String tag, String msg) {
        if (mLoggingEnabled) {
            return Log.w(tag, msg);
        }
        return 0;
    }

    public static int w(String tag, String msg, Throwable tr) {
        if (mLoggingEnabled) {
            return Log.w(tag, msg, tr);
        }
        return 0;
    }

    public static int w(String tag, Throwable tr) {
        if (mLoggingEnabled) {
            return Log.w(tag, tr);
        }
        return 0;
    }

    public static int e(String tag, String msg) {
        if (mLoggingEnabled) {
            return Log.e(tag, msg);
        }
        return 0;
    }

    public static int e(String tag, String msg, Throwable tr) {
        if (mLoggingEnabled) {
            return Log.e(tag, msg, tr);
        }
        return 0;
    }
}
