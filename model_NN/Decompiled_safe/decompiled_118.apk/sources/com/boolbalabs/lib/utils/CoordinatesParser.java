package com.boolbalabs.lib.utils;

import android.content.res.Resources;
import android.graphics.Rect;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class CoordinatesParser {
    private static CoordinatesParser coordinatesParser;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    private Resources gameResources;

    public static CoordinatesParser getInstance() {
        if (coordinatesParser == null) {
            coordinatesParser = new CoordinatesParser();
        }
        return coordinatesParser;
    }

    public static void initialise(Resources gameResources2) {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                coordinatesParser = new CoordinatesParser();
                coordinatesParser.gameResources = gameResources2;
                isInitialised = true;
            }
        }
    }

    private CoordinatesParser() {
    }

    public HashMap<String, Rect> parseEntireFile(String fileName) {
        HashMap<String, Rect> allRectangles = new HashMap<>();
        try {
            InputStream inStream = this.gameResources.getAssets().open(String.valueOf(fileName) + ".plist");
            BufferedReader bufReader = new BufferedReader(new InputStreamReader(inStream));
            new String();
            String[] strArr = new String[4];
            while (true) {
                String frameName = bufReader.readLine();
                if (frameName == null) {
                    break;
                }
                String[] coordinates = bufReader.readLine().split(" ");
                allRectangles.put(new String(frameName), new Rect(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]), Integer.parseInt(coordinates[2]) + Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[3]) + Integer.parseInt(coordinates[1])));
            }
            inStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allRectangles;
    }

    public ArrayList<Rect> getRectsByFrameNames(String fileName, String[] frameNames) {
        return getRectangles(frameNames, getSourceText(fileName));
    }

    @Deprecated
    private ArrayList<Rect> getRectangles(String[] frameNames, String sourceText) {
        ArrayList<Rect> rectangles = new ArrayList<>();
        for (String indexOf : frameNames) {
            int framenemeIndex = sourceText.indexOf(indexOf);
            String[] coords = sourceText.substring(sourceText.indexOf("{{", framenemeIndex) + 2, sourceText.indexOf("}}", framenemeIndex)).split("\\D+");
            rectangles.add(new Rect(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2]) + Integer.parseInt(coords[0]), Integer.parseInt(coords[3]) + Integer.parseInt(coords[1])));
        }
        return rectangles;
    }

    @Deprecated
    private String getSourceText(String fileName) {
        try {
            InputStream inStream = this.gameResources.getAssets().open(String.valueOf(fileName) + ".plist");
            BufferedReader bufReader = new BufferedReader(new InputStreamReader(inStream));
            String sourceText = new String();
            boolean nextLineContainsCoordinates = false;
            while (true) {
                String temp = bufReader.readLine();
                if (temp == null) {
                    inStream.close();
                    return sourceText;
                } else if (nextLineContainsCoordinates) {
                    sourceText = String.valueOf(sourceText) + temp + 10;
                    nextLineContainsCoordinates = false;
                } else if (temp.contains(".png") || temp.contains(".jpg") || temp.contains(".jpeg")) {
                    sourceText = String.valueOf(sourceText) + temp + 10;
                } else if (temp.contains("textureRect")) {
                    nextLineContainsCoordinates = true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
