package com.boolbalabs.lib.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.boolbalabs.lib.geometry.Range;

public class ScreenMetrics {
    public static int GAME_ORIENTATION;
    public static int RESOLUTION_DEFAULT = RESOLUTION_HIGH;
    public static int RESOLUTION_HIGH = 2;
    public static int RESOLUTION_LOW = 0;
    public static int RESOLUTION_MEDIUM = 1;
    public static final Range RESOLUTION_RANGE_HIGH = new Range(330, 500);
    public static final Range RESOLUTION_RANGE_LOW = new Range(0, 250);
    public static final Range RESOLUTION_RANGE_MEDIUM = new Range(250, 330);
    public static final Range RESOLUTION_RANGE_XHIGH = new Range(500, 5000);
    public static int RESOLUTION_XHIGH = 3;
    public static int SCREEN_ORIENTATION;
    public static float aspectRatio;
    public static float aspectRatioOriented;
    public static float defaultViewingDistance = 3.0f;
    private static float pixToRipCoefficient;
    public static int resolution = RESOLUTION_DEFAULT;
    public static String resolution_postfix;
    private static float ripToPixCoefficient;
    @Deprecated
    public static int screenDensityX;
    public static int screenHeightPix;
    public static int screenHeightRip;
    public static int screenLargeSidePix;
    public static int screenLargeSideRip;
    public static int screenSmallSidePix;
    public static int screenSmallSideRip = 320;
    public static int screenWidthPix;
    public static int screenWidthRip;
    public static int[] viewport;

    public static void initScreenMetrics(Context context, int orientation) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        screenDensityX = ((int) metrics.density) * 160;
        Configuration configuration = context.getResources().getConfiguration();
        screenSmallSidePix = Math.min(metrics.widthPixels, metrics.heightPixels);
        screenLargeSidePix = Math.max(metrics.widthPixels, metrics.heightPixels);
        aspectRatio = ((float) screenSmallSidePix) / ((float) screenLargeSidePix);
        screenLargeSideRip = (int) (((float) screenSmallSideRip) / aspectRatio);
        GAME_ORIENTATION = orientation;
        if (GAME_ORIENTATION == 1) {
            screenWidthRip = screenSmallSideRip;
            screenHeightRip = screenLargeSideRip;
            screenWidthPix = screenSmallSidePix;
            screenHeightPix = screenLargeSidePix;
        } else {
            screenWidthRip = screenLargeSideRip;
            screenHeightRip = screenSmallSideRip;
            screenWidthPix = screenLargeSidePix;
            screenHeightPix = screenSmallSidePix;
        }
        aspectRatioOriented = ((float) screenWidthRip) / ((float) screenHeightRip);
        pixToRipCoefficient = ((float) screenSmallSideRip) / ((float) screenSmallSidePix);
        ripToPixCoefficient = 1.0f / pixToRipCoefficient;
        createResolutionPostfix();
        int[] iArr = new int[4];
        iArr[2] = screenSmallSidePix;
        iArr[3] = screenLargeSidePix;
        viewport = iArr;
    }

    private static void createResolutionPostfix() {
        if (RESOLUTION_RANGE_LOW.isInRange(screenSmallSidePix)) {
            resolution = RESOLUTION_LOW;
            resolution_postfix = "lres";
        } else if (RESOLUTION_RANGE_MEDIUM.isInRange(screenSmallSidePix)) {
            resolution = RESOLUTION_MEDIUM;
            resolution_postfix = "mres";
        } else if (RESOLUTION_RANGE_HIGH.isInRange(screenSmallSidePix)) {
            resolution = RESOLUTION_HIGH;
            resolution_postfix = "hres";
        } else if (RESOLUTION_RANGE_XHIGH.isInRange(screenSmallSidePix)) {
            resolution = RESOLUTION_XHIGH;
            resolution_postfix = "xhres";
        }
    }

    public static int convertRipToPixel(float ripValue) {
        return (int) (ripToPixCoefficient * ripValue);
    }

    public static int convertPixelToRip(int pixelValue) {
        return (int) (((float) pixelValue) * pixToRipCoefficient);
    }

    public static void convertRectRipToPix(Rect fromRip, Rect toPix) {
        if (fromRip != null && toPix != null) {
            toPix.set(convertRipToPixel((float) fromRip.left), convertRipToPixel((float) fromRip.top), convertRipToPixel((float) fromRip.right), convertRipToPixel((float) fromRip.bottom));
        }
    }

    public static void convertRectPixToRip(Rect fromPix, Rect toRip) {
        if (fromPix != null && toRip != null) {
            toRip.set(convertPixelToRip(fromPix.left), convertPixelToRip(fromPix.top), convertPixelToRip(fromPix.right), convertPixelToRip(fromPix.bottom));
        }
    }

    public static void convertPointRipToPix(Point fromRip, Point toPix) {
        if (toPix != null && fromRip != null) {
            toPix.set(convertRipToPixel((float) fromRip.x), convertRipToPixel((float) fromRip.y));
        }
    }

    public static void convertPointPixToRip(Point fromPix, Point toRip) {
        if (toRip != null && fromPix != null) {
            toRip.set(convertPixelToRip(fromPix.x), convertPixelToRip(fromPix.y));
        }
    }

    public static int getHeightProportionalToWidth(int width) {
        return (int) (((float) width) / aspectRatioOriented);
    }

    public static int getWidthProportionalToHeight(int height) {
        return (int) (((float) height) * aspectRatioOriented);
    }

    public static String description() {
        return "Screen pixels: " + screenSmallSidePix + "x" + screenLargeSidePix + "\n" + "Screen RIPs: " + screenSmallSideRip + "x" + screenLargeSideRip + "\n" + "Aspect ratio: " + aspectRatio;
    }
}
