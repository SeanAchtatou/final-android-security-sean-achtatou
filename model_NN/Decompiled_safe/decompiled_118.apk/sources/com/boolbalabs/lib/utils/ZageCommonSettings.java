package com.boolbalabs.lib.utils;

public class ZageCommonSettings {
    public static boolean musicEnabled = true;
    public static float openGLVertexModeDefaultViewDistance = 3.0f;
    public static boolean soundEnabled = true;
    public static boolean vibroEnabled = true;
}
