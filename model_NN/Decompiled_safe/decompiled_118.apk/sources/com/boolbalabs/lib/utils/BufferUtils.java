package com.boolbalabs.lib.utils;

import android.graphics.Bitmap;
import android.graphics.Rect;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

public class BufferUtils {
    public static FloatBuffer makeFloatBuffer(float[] arr) {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(arr);
        fb.position(0);
        return fb;
    }

    public static ShortBuffer makeShortBuffer(short[] arr) {
        ByteBuffer bb = ByteBuffer.allocateDirect(arr.length * 4);
        bb.order(ByteOrder.nativeOrder());
        ShortBuffer ib = bb.asShortBuffer();
        ib.put(arr);
        ib.position(0);
        return ib;
    }

    public static ByteBuffer makeByteBuffer(Bitmap bmp) {
        ByteBuffer bb = ByteBuffer.allocateDirect(bmp.getHeight() * bmp.getWidth() * 4);
        bb.order(ByteOrder.BIG_ENDIAN);
        IntBuffer ib = bb.asIntBuffer();
        for (int y = 0; y < bmp.getHeight(); y++) {
            for (int x = 0; x < bmp.getWidth(); x++) {
                int pix = bmp.getPixel(x, (bmp.getHeight() - y) - 1);
                byte red = (byte) ((pix >> 16) & 255);
                byte green = (byte) ((pix >> 8) & 255);
                byte blue = (byte) (pix & 255);
                ib.put(((red & 255) << 24) | ((green & 255) << 16) | ((blue & 255) << 8) | (((byte) ((pix >> 24) & 255)) & 255));
            }
        }
        ib.position(0);
        bb.position(0);
        return bb;
    }

    public static FloatBuffer makeTextureBuffer(Rect rectOnTexture, int width, int height) {
        return makeFloatBuffer(new float[]{((float) rectOnTexture.left) / ((float) width), ((float) rectOnTexture.top) / ((float) height), ((float) rectOnTexture.left) / ((float) width), ((float) rectOnTexture.bottom) / ((float) height), ((float) rectOnTexture.right) / ((float) width), ((float) rectOnTexture.bottom) / ((float) height), ((float) rectOnTexture.right) / ((float) width), ((float) rectOnTexture.top) / ((float) height)});
    }
}
