package com.boolbalabs.lib.utils;

import android.graphics.Point;
import android.graphics.PointF;
import java.util.Random;

public class MathUtils {
    public static Random rand = new Random();

    public static double flatAnglef(PointF start, PointF end) {
        int sign = 1;
        if (end.y > start.y) {
            sign = -1;
        }
        return ((double) sign) * Math.acos(((double) (end.x - start.x)) / vectorLength(start, end));
    }

    public static double flatAnglei(Point start, Point end) {
        int sign = 1;
        if (end.y > start.y) {
            sign = -1;
        }
        return ((double) sign) * Math.acos(((double) (end.x - start.x)) / vectorLength(start, end));
    }

    public static double centralFlatAnglePix(Point pointOnScreen) {
        int xCenter = ScreenMetrics.screenWidthPix / 2;
        int yCenter = ScreenMetrics.screenHeightPix / 2;
        int sign = 1;
        if (pointOnScreen.y > yCenter) {
            sign = -1;
        }
        return ((double) sign) * Math.acos(((double) (pointOnScreen.x - xCenter)) / Math.sqrt(Math.pow((double) (pointOnScreen.x - xCenter), 2.0d) + Math.pow((double) (pointOnScreen.y - yCenter), 2.0d)));
    }

    public static double centralFlatAngleRip(Point pointOnScreenRip) {
        int xCenter = ScreenMetrics.screenWidthRip / 2;
        int yCenter = ScreenMetrics.screenHeightRip / 2;
        int sign = 1;
        if (pointOnScreenRip.y > yCenter) {
            sign = -1;
        }
        return ((double) sign) * Math.acos(((double) (pointOnScreenRip.x - xCenter)) / Math.sqrt(Math.pow((double) (pointOnScreenRip.x - xCenter), 2.0d) + Math.pow((double) (pointOnScreenRip.y - yCenter), 2.0d)));
    }

    public static double vectorLength(PointF point1, PointF point2) {
        return Math.sqrt(Math.pow((double) (point1.x - point2.x), 2.0d) + Math.pow((double) (point1.y - point2.y), 2.0d));
    }

    public static double vectorLength(Point point1, Point point2) {
        return Math.sqrt(Math.pow((double) (point1.x - point2.x), 2.0d) + Math.pow((double) (point1.y - point2.y), 2.0d));
    }

    public static int getMaxElement(int[] array) {
        if (array.length == 0) {
            return 0;
        }
        int result = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] >= result) {
                result = array[i];
            }
        }
        return result;
    }

    public static int getFractionalPartAsInt(float floatNumber, int digitsAfterPoint) {
        int result = 0;
        float cfPart = floatNumber;
        for (int i = digitsAfterPoint; i > 0; i--) {
            result = (int) (((double) result) + (((double) getFirstFracDigit(cfPart)) * Math.pow(10.0d, (double) (i - 1))));
            cfPart *= 10.0f;
        }
        return result;
    }

    private static int getFirstFracDigit(float floatNumber) {
        return (int) (10.0f * (floatNumber - ((float) ((int) floatNumber))));
    }
}
