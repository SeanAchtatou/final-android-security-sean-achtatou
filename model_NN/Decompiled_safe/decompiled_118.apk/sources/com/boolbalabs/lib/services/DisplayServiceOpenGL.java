package com.boolbalabs.lib.services;

import android.app.Activity;
import android.graphics.Typeface;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.boolbalabs.lib.game.Game;
import com.boolbalabs.lib.managers.TexturesManager;
import javax.microedition.khronos.opengles.GL10;

public class DisplayServiceOpenGL {
    private static DisplayServiceOpenGL displayService;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    /* access modifiers changed from: private */
    public long currentTime;
    /* access modifiers changed from: private */
    public short fpsCount;
    /* access modifiers changed from: private */
    public TextView fpsTextView;
    private Game game;
    public GL10 gl;
    private final float[] lightAmbient = {0.2f, 0.2f, 0.2f, 1.0f};
    private final float[] lightDiffuse = {1.0f, 1.0f, 1.0f, 1.0f};
    private final float[] lightPos = {0.0f, 0.0f, 3.0f, 1.0f};
    private Activity mainActivity;
    private FrameLayout mainFrameLayout;
    private final float[] matAmbient = {1.0f, 1.0f, 1.0f, 1.0f};
    private final float[] matDiffuse = {1.0f, 1.0f, 1.0f, 1.0f};
    /* access modifiers changed from: private */
    public long previousTime;
    private boolean showFPS = false;
    private Runnable taskFPS;

    public static DisplayServiceOpenGL getInstance() {
        return displayService;
    }

    public static void initialise() {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                displayService = new DisplayServiceOpenGL();
                isInitialised = true;
            }
        }
    }

    public static void release() {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                if (displayService != null) {
                    displayService = null;
                }
                isInitialised = false;
                TexturesManager.release();
            }
        }
    }

    private DisplayServiceOpenGL() {
    }

    public void sizeChanged(int width, int height) {
        this.gl.glMatrixMode(5889);
        this.gl.glLoadIdentity();
        this.gl.glViewport(0, 0, width, height);
        GLU.gluPerspective(this.gl, 45.0f, ((float) width) / ((float) height), 1.0f, 100.0f);
        this.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        this.gl.glEnable(2896);
        this.gl.glEnable(16384);
        this.gl.glMaterialfv(1032, 4608, this.matAmbient, 0);
        this.gl.glMaterialfv(1032, 4609, this.matDiffuse, 0);
        this.gl.glLightfv(16384, 4608, this.lightAmbient, 0);
        this.gl.glLightfv(16384, 4609, this.lightDiffuse, 0);
        this.gl.glLightfv(16384, 4611, this.lightPos, 0);
        this.gl.glEnable(3042);
        this.gl.glDisable(2929);
        this.gl.glBlendFunc(770, 771);
        this.gl.glDepthFunc(515);
        this.gl.glEnable(3553);
        this.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        this.gl.glClearDepthf(1.0f);
        this.gl.glEnable(2884);
        this.gl.glShadeModel(7425);
    }

    public void initCustomOpenGLParameters(GL10 gl2, GLSurfaceView parentView) {
        if (this.game != null) {
            this.game.initCustomOpenGLParameters(gl2, parentView);
        }
    }

    public void surfaceCreated() {
        initOpenGL();
    }

    private void initOpenGL() {
        this.gl.glHint(3152, 4354);
        this.gl.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        this.gl.glShadeModel(7424);
        this.gl.glDisable(2929);
        this.gl.glEnable(3553);
        this.gl.glDisable(3024);
        this.gl.glDisable(2896);
        this.gl.glClear(16640);
        this.gl.glEnable(2848);
        this.gl.glHint(3155, 4354);
        this.gl.glEnable(2832);
        this.gl.glHint(3153, 4354);
    }

    public void setGame(Game game2) {
        this.game = game2;
    }

    public void drawSplashScreen() {
        if (this.gl != null) {
            this.game.drawSplashScreen();
        }
    }

    public void setParameters(FrameLayout mainFrameLayout2, Activity mainActivity2, boolean showFPS2) {
        this.showFPS = showFPS2;
        this.mainFrameLayout = mainFrameLayout2;
        this.mainActivity = mainActivity2;
        if (showFPS2) {
            this.fpsTextView = new TextView(mainFrameLayout2.getContext());
            this.fpsTextView.setTypeface(Typeface.SANS_SERIF, 1);
            this.fpsTextView.setTextSize(22.0f);
            this.fpsTextView.setTextColor(-65536);
            this.fpsCount = 0;
            this.fpsTextView.setText("FPS: " + ((int) this.fpsCount));
            mainFrameLayout2.addView(this.fpsTextView);
            this.previousTime = System.currentTimeMillis();
            this.taskFPS = new Runnable() {
                public void run() {
                    DisplayServiceOpenGL.this.currentTime = System.currentTimeMillis();
                    DisplayServiceOpenGL displayServiceOpenGL = DisplayServiceOpenGL.this;
                    displayServiceOpenGL.fpsCount = (short) (displayServiceOpenGL.fpsCount + 1);
                    if (DisplayServiceOpenGL.this.currentTime >= DisplayServiceOpenGL.this.previousTime + 1000) {
                        DisplayServiceOpenGL.this.fpsTextView.setText("FPS: " + ((int) DisplayServiceOpenGL.this.fpsCount));
                        DisplayServiceOpenGL.this.fpsCount = 0;
                        DisplayServiceOpenGL.this.previousTime = DisplayServiceOpenGL.this.currentTime;
                    }
                }
            };
        }
    }

    private void updateFPS() {
        if (this.showFPS) {
            this.mainActivity.runOnUiThread(this.taskFPS);
        }
    }

    public void draw() {
        this.gl.glClear(16640);
        this.gl.glMatrixMode(5888);
        this.game.draw(this.gl);
    }
}
