package com.boolbalabs.lib.services;

import com.boolbalabs.lib.game.ContentLoader;
import com.boolbalabs.lib.game.GameScene;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import java.util.ArrayList;
import java.util.HashMap;

public class ContentLoaderServiceOpenGL {
    private static ContentLoaderServiceOpenGL contentLoaderService;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    private ContentLoader contentLoader;
    private HashMap<Integer, GameScene> gameScenes;

    public static ContentLoaderServiceOpenGL getInstance() {
        return contentLoaderService;
    }

    public static void initialise() {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                contentLoaderService = new ContentLoaderServiceOpenGL();
                isInitialised = true;
            }
        }
    }

    public static void release() {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                if (contentLoaderService != null) {
                    contentLoaderService = null;
                }
                isInitialised = false;
            }
        }
    }

    private ContentLoaderServiceOpenGL() {
    }

    public void setContentLoader(ContentLoader contentLoader2) {
        this.contentLoader = contentLoader2;
    }

    public void setGameScenes(HashMap<Integer, GameScene> gameScenes2) {
        this.gameScenes = gameScenes2;
    }

    public void loadContent() {
        loadContentOfGameComponents();
        TexturesManager.getInstance().loadTextures();
        assignIndicesToViews();
    }

    public void assignIndicesToViews() {
        TexturesManager texturesManager = TexturesManager.getInstance();
        if (this.gameScenes != null && !this.gameScenes.isEmpty()) {
            for (Integer intValue : this.gameScenes.keySet()) {
                GameScene scene = this.gameScenes.get(Integer.valueOf(intValue.intValue()));
                ArrayList<ZNode> currentList = scene.getSceneDrawables();
                int size = currentList.size();
                for (int i = 0; i < size; i++) {
                    ZNode cNode = currentList.get(i);
                    cNode.setTexture(texturesManager.getTextureByResourceId(cNode.getResourceId()));
                    cNode.onAfterLoad();
                }
                scene.onAfterLoad();
            }
        }
        this.contentLoader.onAfterLoad();
    }

    private void loadContentOfGameComponents() {
        this.contentLoader.loadContent();
    }

    public void unloadTexture() {
        this.contentLoader.unloadContent();
    }
}
