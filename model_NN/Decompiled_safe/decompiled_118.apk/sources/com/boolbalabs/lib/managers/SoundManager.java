package com.boolbalabs.lib.managers;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import com.boolbalabs.lib.utils.Constants;
import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import java.io.IOException;
import java.util.Timer;

public class SoundManager {
    private static AudioManager audioManager;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    private static SoundManager soundManager;
    /* access modifiers changed from: private */
    public Handler activityHandler;
    private Context context;
    private float currentVolume;
    private MediaPlayer mediaPlayer;
    private SparseArray<Integer> playingSoundHandles;
    private final Object playingSoundHandlesLock;
    private SparseArray<Integer> soundLengths;
    private SoundPool soundPool;
    private SparseArray<Integer> soundPoolHandles;
    Timer soundTimer;

    public static SoundManager getInstance() {
        return soundManager;
    }

    public static void init(Context context2) {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                soundManager = new SoundManager(context2);
                audioManager = (AudioManager) context2.getSystemService("audio");
                isInitialised = true;
            }
        }
    }

    public void setHandler(Handler handler) {
        if (soundManager != null) {
            soundManager.activityHandler = handler;
        }
    }

    public static void release() {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                if (soundManager != null) {
                    soundManager.soundPool.release();
                    soundManager.soundPool = null;
                    soundManager.soundPoolHandles.clear();
                    soundManager.soundPoolHandles = null;
                    synchronized (soundManager.playingSoundHandlesLock) {
                        soundManager.playingSoundHandles.clear();
                        soundManager.playingSoundHandles = null;
                    }
                    soundManager.soundLengths.clear();
                    soundManager.soundLengths = null;
                    soundManager.soundTimer.cancel();
                    soundManager.soundTimer = null;
                    soundManager = null;
                }
                isInitialised = false;
            }
        }
    }

    private SoundManager(Context context2) {
        this.soundPoolHandles = new SparseArray<>();
        this.playingSoundHandles = new SparseArray<>();
        this.playingSoundHandlesLock = new Object();
        this.soundLengths = new SparseArray<>();
        this.currentVolume = 1.0f;
        this.mediaPlayer = null;
        this.soundPool = new SoundPool(4, 3, 0);
        this.soundPoolHandles = new SparseArray<>();
        this.playingSoundHandles = new SparseArray<>();
        this.soundLengths = new SparseArray<>();
        this.context = context2;
        AudioManager audioManager2 = (AudioManager) context2.getSystemService("audio");
        this.currentVolume = 0.5f;
        if (audioManager2 != null) {
            try {
                this.currentVolume = ((float) audioManager2.getStreamVolume(3)) / ((float) audioManager2.getStreamMaxVolume(3));
            } catch (Exception e) {
            }
        }
        if (this.currentVolume > 0.99f) {
            this.currentVolume = 0.99f;
        }
        this.soundTimer = new Timer();
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                if (SoundManager.this.activityHandler == null) {
                    return false;
                }
                Message msg = new Message();
                Bundle error = new Bundle();
                error.putString(TrackerEvents.LABEL_ERROR, "what: " + what + " extra: " + extra);
                msg.what = Constants.MESSAGE_RESTART_SOUND_MANAGER;
                msg.setData(error);
                SoundManager.this.activityHandler.sendMessage(msg);
                return false;
            }
        });
    }

    public void addShortSound(int resourceId, int soundLength) {
        this.soundPoolHandles.put(resourceId, Integer.valueOf(this.soundPool.load(this.context, resourceId, 1)));
        this.soundLengths.put(resourceId, Integer.valueOf(soundLength));
    }

    public void addLoopingSound(int resourceId) {
        AssetFileDescriptor afd = this.context.getResources().openRawResourceFd(resourceId);
        if (afd != null) {
            this.mediaPlayer.reset();
            try {
                this.mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
                this.mediaPlayer.setLooping(true);
                this.mediaPlayer.prepare();
            } catch (IOException e) {
                Log.d("SoungManager", "addLoopingSound failed:", e);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0010, code lost:
        if (r8.soundPool == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r7 = r8.soundPool.play(r8.soundPoolHandles.get(r9).intValue(), r8.currentVolume, r8.currentVolume, 1, 0, r10);
        r0 = r8.playingSoundHandlesLock;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r8.playingSoundHandles.put(r9, java.lang.Integer.valueOf(r7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0037, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000c, code lost:
        if (r8.soundPoolHandles == null) goto L_?;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void playShortSound(int r9, float r10) {
        /*
            r8 = this;
            java.lang.Object r0 = com.boolbalabs.lib.managers.SoundManager.isInitialisedLock
            monitor-enter(r0)
            boolean r2 = com.boolbalabs.lib.managers.SoundManager.isInitialised     // Catch:{ all -> 0x003e }
            if (r2 != 0) goto L_0x0009
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
        L_0x0008:
            return
        L_0x0009:
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            android.util.SparseArray<java.lang.Integer> r0 = r8.soundPoolHandles
            if (r0 == 0) goto L_0x0008
            android.media.SoundPool r0 = r8.soundPool
            if (r0 == 0) goto L_0x0008
            android.util.SparseArray<java.lang.Integer> r0 = r8.soundPoolHandles     // Catch:{ Exception -> 0x003c }
            java.lang.Object r0 = r0.get(r9)     // Catch:{ Exception -> 0x003c }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x003c }
            int r1 = r0.intValue()     // Catch:{ Exception -> 0x003c }
            android.media.SoundPool r0 = r8.soundPool     // Catch:{ Exception -> 0x003c }
            float r2 = r8.currentVolume     // Catch:{ Exception -> 0x003c }
            float r3 = r8.currentVolume     // Catch:{ Exception -> 0x003c }
            r4 = 1
            r5 = 0
            r6 = r10
            int r7 = r0.play(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x003c }
            java.lang.Object r0 = r8.playingSoundHandlesLock     // Catch:{ Exception -> 0x003c }
            monitor-enter(r0)     // Catch:{ Exception -> 0x003c }
            android.util.SparseArray<java.lang.Integer> r2 = r8.playingSoundHandles     // Catch:{ all -> 0x0039 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x0039 }
            r2.put(r9, r3)     // Catch:{ all -> 0x0039 }
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
            goto L_0x0008
        L_0x0039:
            r2 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
            throw r2     // Catch:{ Exception -> 0x003c }
        L_0x003c:
            r0 = move-exception
            goto L_0x0008
        L_0x003e:
            r2 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.boolbalabs.lib.managers.SoundManager.playShortSound(int, float):void");
    }

    public void playShortSound(int resourceId) {
        playShortSound(resourceId, 1.0f);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0010, code lost:
        if (r2.soundPool == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
        if (r2.playingSoundHandles.indexOfKey(r3) < 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001a, code lost:
        r2.soundPool.stop(r2.playingSoundHandles.get(r3).intValue());
        r2.playingSoundHandles.delete(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000c, code lost:
        if (r2.soundPoolHandles == null) goto L_?;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void stopShortSound(int r3) {
        /*
            r2 = this;
            java.lang.Object r0 = com.boolbalabs.lib.managers.SoundManager.isInitialisedLock
            monitor-enter(r0)
            boolean r1 = com.boolbalabs.lib.managers.SoundManager.isInitialised     // Catch:{ all -> 0x0031 }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r0)     // Catch:{ all -> 0x0031 }
        L_0x0008:
            return
        L_0x0009:
            monitor-exit(r0)     // Catch:{ all -> 0x0031 }
            android.util.SparseArray<java.lang.Integer> r0 = r2.soundPoolHandles
            if (r0 == 0) goto L_0x0008
            android.media.SoundPool r0 = r2.soundPool
            if (r0 == 0) goto L_0x0008
            android.util.SparseArray<java.lang.Integer> r0 = r2.playingSoundHandles
            int r0 = r0.indexOfKey(r3)
            if (r0 < 0) goto L_0x0008
            android.media.SoundPool r1 = r2.soundPool
            android.util.SparseArray<java.lang.Integer> r0 = r2.playingSoundHandles
            java.lang.Object r0 = r0.get(r3)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            r1.stop(r0)
            android.util.SparseArray<java.lang.Integer> r0 = r2.playingSoundHandles
            r0.delete(r3)
            goto L_0x0008
        L_0x0031:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0031 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.boolbalabs.lib.managers.SoundManager.stopShortSound(int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0010, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        if (r2.mediaPlayer.isPlaying() != false) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0019, code lost:
        r2.mediaPlayer.reset();
        addLoopingSound(r3);
        r2.mediaPlayer.start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000c, code lost:
        if (r2.mediaPlayer == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x000e, code lost:
        r0 = r2.mediaPlayer;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void playLoopingSound(int r3) {
        /*
            r2 = this;
            java.lang.Object r0 = com.boolbalabs.lib.managers.SoundManager.isInitialisedLock
            monitor-enter(r0)
            boolean r1 = com.boolbalabs.lib.managers.SoundManager.isInitialised     // Catch:{ all -> 0x002b }
            if (r1 != 0) goto L_0x0009
            monitor-exit(r0)     // Catch:{ all -> 0x002b }
        L_0x0008:
            return
        L_0x0009:
            monitor-exit(r0)     // Catch:{ all -> 0x002b }
            android.media.MediaPlayer r0 = r2.mediaPlayer
            if (r0 == 0) goto L_0x0008
            android.media.MediaPlayer r0 = r2.mediaPlayer
            monitor-enter(r0)
            android.media.MediaPlayer r1 = r2.mediaPlayer     // Catch:{ all -> 0x0028 }
            boolean r1 = r1.isPlaying()     // Catch:{ all -> 0x0028 }
            if (r1 != 0) goto L_0x0026
            android.media.MediaPlayer r1 = r2.mediaPlayer     // Catch:{ all -> 0x0028 }
            r1.reset()     // Catch:{ all -> 0x0028 }
            r2.addLoopingSound(r3)     // Catch:{ all -> 0x0028 }
            android.media.MediaPlayer r1 = r2.mediaPlayer     // Catch:{ all -> 0x0028 }
            r1.start()     // Catch:{ all -> 0x0028 }
        L_0x0026:
            monitor-exit(r0)     // Catch:{ all -> 0x0028 }
            goto L_0x0008
        L_0x0028:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0028 }
            throw r1
        L_0x002b:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002b }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.boolbalabs.lib.managers.SoundManager.playLoopingSound(int):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x000f, code lost:
        if (r6.playingSoundHandles == null) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0011, code lost:
        r2 = r6.playingSoundHandles.size();
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0018, code lost:
        if (r1 < r2) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001a, code lost:
        r6.playingSoundHandles.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001f, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0022, code lost:
        if (r6.mediaPlayer == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0024, code lost:
        r6.mediaPlayer.stop();
        r6.mediaPlayer.release();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r6.soundPool.stop(r6.playingSoundHandles.get(r6.playingSoundHandles.keyAt(r1)).intValue());
        r1 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r4 = r6.playingSoundHandlesLock;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000c, code lost:
        monitor-enter(r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void stopAllPlayingSounds() {
        /*
            r6 = this;
            java.lang.Object r3 = com.boolbalabs.lib.managers.SoundManager.isInitialisedLock
            monitor-enter(r3)
            boolean r4 = com.boolbalabs.lib.managers.SoundManager.isInitialised     // Catch:{ all -> 0x002f }
            if (r4 != 0) goto L_0x0009
            monitor-exit(r3)     // Catch:{ all -> 0x002f }
        L_0x0008:
            return
        L_0x0009:
            monitor-exit(r3)     // Catch:{ all -> 0x002f }
            java.lang.Object r4 = r6.playingSoundHandlesLock
            monitor-enter(r4)
            android.util.SparseArray<java.lang.Integer> r3 = r6.playingSoundHandles     // Catch:{ all -> 0x004c }
            if (r3 == 0) goto L_0x001f
            android.util.SparseArray<java.lang.Integer> r3 = r6.playingSoundHandles     // Catch:{ all -> 0x004c }
            int r2 = r3.size()     // Catch:{ all -> 0x004c }
            r1 = 0
        L_0x0018:
            if (r1 < r2) goto L_0x0032
            android.util.SparseArray<java.lang.Integer> r3 = r6.playingSoundHandles     // Catch:{ all -> 0x004c }
            r3.clear()     // Catch:{ all -> 0x004c }
        L_0x001f:
            monitor-exit(r4)     // Catch:{ all -> 0x004c }
            android.media.MediaPlayer r3 = r6.mediaPlayer
            if (r3 == 0) goto L_0x0008
            android.media.MediaPlayer r3 = r6.mediaPlayer
            r3.stop()
            android.media.MediaPlayer r3 = r6.mediaPlayer
            r3.release()
            goto L_0x0008
        L_0x002f:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x002f }
            throw r4
        L_0x0032:
            android.util.SparseArray<java.lang.Integer> r3 = r6.playingSoundHandles     // Catch:{ all -> 0x004c }
            int r0 = r3.keyAt(r1)     // Catch:{ all -> 0x004c }
            android.media.SoundPool r5 = r6.soundPool     // Catch:{ all -> 0x004c }
            android.util.SparseArray<java.lang.Integer> r3 = r6.playingSoundHandles     // Catch:{ all -> 0x004c }
            java.lang.Object r3 = r3.get(r0)     // Catch:{ all -> 0x004c }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ all -> 0x004c }
            int r3 = r3.intValue()     // Catch:{ all -> 0x004c }
            r5.stop(r3)     // Catch:{ all -> 0x004c }
            int r1 = r1 + 1
            goto L_0x0018
        L_0x004c:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x004c }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.boolbalabs.lib.managers.SoundManager.stopAllPlayingSounds():void");
    }

    public void pauseLoopingSounds() {
        if (this.mediaPlayer != null && this.mediaPlayer.isPlaying()) {
            this.mediaPlayer.pause();
        }
    }

    public void adjustVolume(int direction) {
        if ((direction == -1 || direction == 1) && audioManager != null) {
            audioManager.adjustStreamVolume(3, direction, 5);
        }
    }
}
