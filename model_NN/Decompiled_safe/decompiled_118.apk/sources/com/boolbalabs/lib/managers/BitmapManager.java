package com.boolbalabs.lib.managers;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import com.boolbalabs.lib.graphics.BitmapFrames;
import com.boolbalabs.lib.utils.ScreenMetrics;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;

public class BitmapManager {
    private static final float DENSITY_FACTOR = ((float) (ScreenMetrics.screenDensityX / 240));
    private static Resources appResources;
    private static BitmapManager bitmapManager;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    private static BitmapFactory.Options noScalingOptions = new BitmapFactory.Options();
    private SparseArray<BitmapFrames> bitmapFrames = new SparseArray<>();
    private SparseArray<Bitmap> bitmaps = new SparseArray<>();
    private Matrix identityMatrix = new Matrix();

    public static BitmapManager getInstance() {
        return bitmapManager;
    }

    public static void init(Resources applicationResources) {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                bitmapManager = new BitmapManager();
                appResources = applicationResources;
                isInitialised = true;
            }
        }
    }

    public static void release() {
        synchronized (isInitialisedLock) {
            if (isInitialised) {
                if (bitmapManager != null) {
                    bitmapManager.recycleBitmaps();
                    bitmapManager.bitmaps = null;
                    bitmapManager.bitmapFrames = null;
                    bitmapManager = null;
                }
                isInitialised = false;
            }
        }
    }

    private BitmapManager() {
        try {
            Field inDensityField = BitmapFactory.Options.class.getField("inDensity");
            Field inTargetDensityField = BitmapFactory.Options.class.getField("inTargetDensity");
            Field inScaledField = BitmapFactory.Options.class.getField("inScaled");
            inDensityField.setInt(noScalingOptions, 0);
            inTargetDensityField.setInt(noScalingOptions, 0);
            inScaledField.setBoolean(noScalingOptions, false);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap getScaledBitmap(int bitmapRef) {
        Bitmap originalBitmap = BitmapFactory.decodeResource(appResources, bitmapRef, noScalingOptions);
        if (DENSITY_FACTOR == 1.0f) {
            return originalBitmap;
        }
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(originalBitmap, (int) (((float) originalBitmap.getWidth()) * DENSITY_FACTOR), (int) (((float) originalBitmap.getHeight()) * DENSITY_FACTOR), true);
        originalBitmap.recycle();
        return scaledBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [int, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    public static Bitmap decodeBitmap(int bitmapRef, BitmapFactory.Options options) {
        TypedValue value = new TypedValue();
        appResources.getValue(bitmapRef, value, true);
        String file = value.string.toString();
        if (file.endsWith(".xml")) {
            int previousDensity = appResources.getDisplayMetrics().densityDpi;
            try {
                XmlResourceParser rp = appResources.getXml(bitmapRef);
                appResources.getDisplayMetrics().densityDpi = 160;
                BitmapDrawable dr = (BitmapDrawable) Drawable.createFromXml(appResources, rp);
                rp.close();
                Bitmap originalBitmap = dr.getBitmap();
                appResources.getDisplayMetrics().densityDpi = previousDensity;
                return originalBitmap;
            } catch (Exception e) {
                Resources.NotFoundException rnf = new Resources.NotFoundException("File " + file + " from drawable resource ID #0x" + Integer.toHexString(bitmapRef));
                rnf.initCause(e);
                throw rnf;
            }
        } else {
            InputStream is = appResources.openRawResource(bitmapRef);
            try {
                Bitmap originalBitmap2 = BitmapFactory.decodeStream(is, null, options);
                try {
                    return originalBitmap2;
                } catch (IOException e2) {
                    return originalBitmap2;
                }
            } finally {
                try {
                    is.close();
                } catch (IOException e3) {
                }
            }
        }
    }

    public void recycleBitmaps() {
        int bitmapsCount = this.bitmaps.size();
        for (int i = 0; i < bitmapsCount; i++) {
            Bitmap bmToRelease = this.bitmaps.get(this.bitmaps.keyAt(i));
            if (bmToRelease != null) {
                bmToRelease.recycle();
            }
        }
        this.bitmaps.clear();
        int bitmapsFramesCount = this.bitmapFrames.size();
        for (int i2 = 0; i2 < bitmapsFramesCount; i2++) {
            BitmapFrames bmfToRelease = this.bitmapFrames.get(this.bitmapFrames.keyAt(i2));
            if (bmfToRelease != null) {
                bmfToRelease.release();
            }
        }
        this.bitmapFrames.clear();
    }

    public void drawBitmap(Canvas c, int bitmapRef, int leftDip, int topDip) {
        if (this.bitmaps == null) {
            Log.i("BitmapManager", "error: drawing recycled bitmap");
            return;
        }
        Bitmap bm = this.bitmaps.get(bitmapRef);
        if (bm == null || bm.isRecycled()) {
            Log.i("BitmapManager", "error: drawing recycled bitmap");
        } else {
            c.drawBitmap(bm, (float) ScreenMetrics.convertRipToPixel((float) leftDip), (float) ScreenMetrics.convertRipToPixel((float) topDip), (Paint) null);
        }
    }

    public void drawBitmapPix(Canvas c, int bitmapRef, int leftPix, int topPix) {
        if (this.bitmaps != null) {
            Bitmap b = this.bitmaps.get(bitmapRef);
            if (b == null || b.isRecycled()) {
                Log.i("BitmapManager", "error: drawing recycled bitmap");
            } else {
                c.drawBitmap(b, (float) leftPix, (float) topPix, (Paint) null);
            }
        }
    }

    public void drawBitmap(Canvas c, int bitmapRef, Rect sourceRectPix, Rect destRectPix) {
        if (this.bitmaps != null) {
            Bitmap b = this.bitmaps.get(bitmapRef);
            if (b == null || b.isRecycled()) {
                Log.i("BitmapManager", "error: drawing recycled bitmap");
            } else {
                c.drawBitmap(b, sourceRectPix, destRectPix, (Paint) null);
            }
        }
    }

    public void drawBitmap(Canvas c, int bitmapRef, Matrix m, Point posPix) {
        c.setMatrix(m);
        Bitmap b = this.bitmaps.get(bitmapRef);
        if (b == null || b.isRecycled()) {
            Log.i("BitmapManager", "error: drawing recycled bitmap");
        } else {
            c.drawBitmap(b, (float) posPix.x, (float) posPix.y, (Paint) null);
        }
        c.setMatrix(this.identityMatrix);
    }

    public void drawBitmapFrame(Canvas c, int bitmapFrameRef, int frameIndex, Point posDip) {
        BitmapFrames bf;
        if (this.bitmapFrames != null && (bf = this.bitmapFrames.get(bitmapFrameRef)) != null) {
            bf.drawFrame(c, frameIndex, posDip);
        }
    }

    public void drawBitmapFrameTopPart(Canvas c, int bitmapFrameRef, int frameIndex, Point posDip, int partsToDrawCount, int totalPartsCount) {
        BitmapFrames bf = this.bitmapFrames.get(bitmapFrameRef);
        if (bf != null) {
            bf.drawFrameTopPart(c, frameIndex, posDip, partsToDrawCount, totalPartsCount);
        }
    }

    public void drawBitmapFrameBottomPart(Canvas c, int bitmapFrameRef, int frameIndex, Point posDip, int partsToDrawCount, int totalPartsCount) {
        BitmapFrames bf = this.bitmapFrames.get(bitmapFrameRef);
        if (bf != null) {
            bf.drawFrameBottomPart(c, frameIndex, posDip, partsToDrawCount, totalPartsCount);
        }
    }

    public void drawBitmapFrameInRectPix(Canvas c, int bitmapRef, int frameIndex, Rect rectToDrawInPix) {
        BitmapFrames bf = this.bitmapFrames.get(bitmapRef);
        if (bf != null) {
            bf.drawFrameInRectPix(c, frameIndex, rectToDrawInPix);
        }
    }

    public int getBitmapWidth(int bitmapRef) {
        return this.bitmaps.get(bitmapRef).getWidth();
    }

    public int getBitmapHeight(int bitmapRef) {
        return this.bitmaps.get(bitmapRef).getHeight();
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public int getSingleBitmapFrameWidth(int bitmapFramesRef) {
        if (this.bitmapFrames == null || this.bitmapFrames.get(bitmapFramesRef) == null) {
            return 0;
        }
        return this.bitmapFrames.get(bitmapFramesRef).getSingleFrameWidthPix();
    }

    public SparseArray<Bitmap> getBitmaps() {
        return this.bitmaps;
    }

    public SparseArray<BitmapFrames> getBitmapFrames() {
        return this.bitmapFrames;
    }

    public void addBitmap(int bitmapRef) {
        if (appResources != null && this.bitmaps != null) {
            this.bitmaps.put(bitmapRef, getScaledBitmap(bitmapRef));
        }
    }

    public void addBitmapFrame(int bitmapFrameRef, int columns, int rows) {
        int ref = bitmapFrameRef;
        Bitmap b = getScaledBitmap(ref);
        this.bitmaps.put(ref, b);
        this.bitmapFrames.put(ref, new BitmapFrames(ref, b.getWidth(), b.getHeight(), columns, rows));
    }

    public void deleteBitmap(int bitmapRef) {
        Bitmap bmToRelease = this.bitmaps.get(bitmapRef);
        if (bmToRelease != null) {
            bmToRelease.recycle();
        }
        this.bitmaps.delete(bitmapRef);
    }

    public void deleteBitmapFrame(int bitmapFrameRef) {
        Bitmap bmToRelease = this.bitmaps.get(bitmapFrameRef);
        if (bmToRelease != null) {
            bmToRelease.recycle();
        }
        this.bitmapFrames.delete(bitmapFrameRef);
        BitmapFrames bmfToRelease = this.bitmapFrames.get(bitmapFrameRef);
        if (bmfToRelease != null) {
            bmfToRelease.release();
        }
        this.bitmapFrames.delete(bitmapFrameRef);
    }

    public Bitmap getBitmap(int bitmapRef) {
        return this.bitmaps.get(bitmapRef, null);
    }

    public BitmapFrames getBitmapFrame(int bitmapRef) {
        return this.bitmapFrames.get(bitmapRef, null);
    }
}
