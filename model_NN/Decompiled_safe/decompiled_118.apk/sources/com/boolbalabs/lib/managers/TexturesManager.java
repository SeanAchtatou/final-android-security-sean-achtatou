package com.boolbalabs.lib.managers;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;
import android.util.SparseArray;
import com.boolbalabs.lib.graphics.Texture2D;
import com.boolbalabs.lib.services.DisplayServiceOpenGL;
import com.boolbalabs.lib.utils.CoordinatesParser;
import com.boolbalabs.lib.utils.DebugLog;
import java.util.HashMap;
import javax.microedition.khronos.opengles.GL10;

public class TexturesManager {
    private static Resources appResources;
    private static boolean isInitialised = false;
    private static final Object isInitialisedLock = new Object();
    public static BitmapFactory.Options textureBitmapOptions = new BitmapFactory.Options();
    private static TexturesManager texturesManager;
    private HashMap<String, Rect> allRectangles = new HashMap<>();
    private GL10 gl;
    private SparseArray<Integer> globalTextureIndices = new SparseArray<>();
    private SparseArray<Texture2D> texturesList = new SparseArray<>();

    private TexturesManager() {
    }

    public static TexturesManager getInstance() {
        return texturesManager;
    }

    public static void init(Resources applicationResources) {
        synchronized (isInitialisedLock) {
            if (!isInitialised) {
                texturesManager = new TexturesManager();
                appResources = applicationResources;
                isInitialised = true;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void release() {
        /*
            java.lang.Object r0 = com.boolbalabs.lib.managers.TexturesManager.isInitialisedLock
            monitor-enter(r0)
            boolean r1 = com.boolbalabs.lib.managers.TexturesManager.isInitialised     // Catch:{ all -> 0x0025 }
            if (r1 == 0) goto L_0x000d
            com.boolbalabs.lib.managers.TexturesManager r1 = com.boolbalabs.lib.managers.TexturesManager.texturesManager     // Catch:{ all -> 0x0025 }
            javax.microedition.khronos.opengles.GL10 r1 = r1.gl     // Catch:{ all -> 0x0025 }
            if (r1 != 0) goto L_0x000f
        L_0x000d:
            monitor-exit(r0)     // Catch:{ all -> 0x0025 }
        L_0x000e:
            return
        L_0x000f:
            com.boolbalabs.lib.managers.TexturesManager r1 = com.boolbalabs.lib.managers.TexturesManager.texturesManager     // Catch:{ all -> 0x0025 }
            if (r1 == 0) goto L_0x0020
            com.boolbalabs.lib.managers.TexturesManager r1 = com.boolbalabs.lib.managers.TexturesManager.texturesManager     // Catch:{ all -> 0x0025 }
            r2 = 0
            r1.texturesList = r2     // Catch:{ all -> 0x0025 }
            com.boolbalabs.lib.managers.TexturesManager r1 = com.boolbalabs.lib.managers.TexturesManager.texturesManager     // Catch:{ all -> 0x0025 }
            r2 = 0
            r1.globalTextureIndices = r2     // Catch:{ all -> 0x0025 }
            r1 = 0
            com.boolbalabs.lib.managers.TexturesManager.texturesManager = r1     // Catch:{ all -> 0x0025 }
        L_0x0020:
            r1 = 0
            com.boolbalabs.lib.managers.TexturesManager.isInitialised = r1     // Catch:{ all -> 0x0025 }
            monitor-exit(r0)     // Catch:{ all -> 0x0025 }
            goto L_0x000e
        L_0x0025:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0025 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.boolbalabs.lib.managers.TexturesManager.release():void");
    }

    public void loadCoordinatesFromFile(String filename) {
        this.allRectangles.putAll(CoordinatesParser.getInstance().parseEntireFile(filename));
    }

    public void loadTextures() {
        GL10 gl2 = DisplayServiceOpenGL.getInstance().gl;
        this.gl = gl2;
        textureBitmapOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        int numberOfTextures = this.texturesList.size();
        for (int i = 0; i < numberOfTextures; i++) {
            Texture2D currentTexture = this.texturesList.get(this.texturesList.keyAt(i));
            currentTexture.buildTexture(gl2);
            addTextureIndex(currentTexture);
        }
    }

    private void addTextureIndex(Texture2D texture) {
        if (this.globalTextureIndices != null) {
            this.globalTextureIndices.put(texture.getResourceId(), Integer.valueOf(texture.getTextureGlobalIndex()));
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public int getGlobalIndexByResourceId(int resId) {
        if (this.globalTextureIndices != null) {
            return this.globalTextureIndices.get(resId, -1).intValue();
        }
        return -1;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Texture2D getTextureByResourceId(int resId) {
        if (this.texturesList != null) {
            return this.texturesList.get(resId, null);
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Rect getRectByFrameName(String frameName) {
        if (this.allRectangles.containsKey(frameName)) {
            return this.allRectangles.get(frameName);
        }
        return null;
    }

    public void addTexture(Texture2D texture) {
        if (this.texturesList != null) {
            int id = texture.getResourceId();
            if (this.texturesList.get(id) == null) {
                DebugLog.i("ADDING TEXTURE", " " + id);
                this.texturesList.put(id, texture);
            }
        }
    }

    public void showTextureIds() {
        int numberOfTextures = this.texturesList.size();
        for (int i = 0; i < numberOfTextures; i++) {
            Log.i("TEXTURE ID", "ID: " + this.texturesList.get(this.texturesList.keyAt(i)).getTextureGlobalIndex());
        }
    }
}
