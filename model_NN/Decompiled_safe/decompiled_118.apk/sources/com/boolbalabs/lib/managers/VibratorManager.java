package com.boolbalabs.lib.managers;

import android.content.Context;
import android.os.Vibrator;

public class VibratorManager {
    private static boolean isInitialised = false;
    private static VibratorManager vibratorManager;
    private long[] pattern = new long[2];
    private Vibrator vibrator;

    private VibratorManager(Context context) {
        this.vibrator = (Vibrator) context.getSystemService("vibrator");
    }

    public static VibratorManager getInstance() {
        return vibratorManager;
    }

    public static void init(Context context) {
        if (!isInitialised) {
            isInitialised = true;
            vibratorManager = new VibratorManager(context);
        }
    }

    public static void release() {
        if (isInitialised) {
            if (vibratorManager != null) {
                vibratorManager.vibrator = null;
                vibratorManager = null;
            }
            isInitialised = false;
        }
    }

    public void vibrate(long milliseconds) {
        if (this.vibrator != null) {
            this.vibrator.vibrate(milliseconds);
        }
    }

    public void vibrate(long delayMs, long vibrateTimeMs) {
        this.pattern[0] = delayMs;
        this.pattern[1] = vibrateTimeMs;
        if (this.vibrator != null) {
            try {
                this.vibrator.vibrate(this.pattern, -1);
            } catch (Exception e) {
            }
        }
    }
}
