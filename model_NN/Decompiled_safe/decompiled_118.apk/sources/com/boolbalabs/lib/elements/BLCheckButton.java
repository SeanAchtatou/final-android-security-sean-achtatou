package com.boolbalabs.lib.elements;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.managers.TexturesManager;
import javax.microedition.khronos.opengles.GL10;

public class BLCheckButton extends ZNode {
    public boolean overlayDrawingMode;
    private ZNode overlayView;
    private Rect rectOnScreenOverlayRip;
    private Rect rectOnScreenRip;
    private Rect rectOnTexture;
    private Rect rectOnTextureOverlay;
    private Rect rectOnTexturePressed;
    private boolean selected;

    public BLCheckButton(int resourceId, int overlayResourceId) {
        super(resourceId, 0);
        this.overlayDrawingMode = false;
        this.overlayView = new ZNode(overlayResourceId, 0);
        this.userInteractionEnabled = true;
        this.selected = false;
    }

    public BLCheckButton(int resourceId) {
        this(resourceId, resourceId);
    }

    public void setRects(Rect rectOnScreen, Rect rectOnTexture2, Rect rectOnTexture_pressed) {
        this.rectOnScreenRip = rectOnScreen;
        this.rectOnTexture = rectOnTexture2;
        this.rectOnTexturePressed = rectOnTexture_pressed;
    }

    public void initialize() {
        initWithFrame(this.rectOnScreenRip, this.rectOnTexture);
        this.overlayView.visible = false;
        if (this.overlayDrawingMode) {
            this.overlayView.initWithFrame(this.rectOnScreenOverlayRip, this.rectOnTextureOverlay);
        }
    }

    public void onAfterLoad() {
        super.onAfterLoad();
        if (this.overlayDrawingMode) {
            this.overlayView.setTexture(TexturesManager.getInstance().getTextureByResourceId(this.overlayView.getResourceId()));
        }
    }

    public void touchDownAction(Point touchDownPoint) {
        setSelected(!this.selected);
    }

    private void afterSelectionChange(boolean selected2) {
        if (selected2) {
            setRectOnTexture(this.rectOnTexturePressed);
        } else {
            setRectOnTexture(this.rectOnTexture);
        }
        onSelectionChange(selected2);
    }

    public void setSelected(boolean selected2) {
        this.selected = selected2;
        if (this.overlayDrawingMode) {
            this.overlayView.visible = selected2;
        }
        afterSelectionChange(selected2);
    }

    public void onSelectionChange(boolean selected2) {
    }

    public void setOverlayVisible(boolean visible) {
        this.overlayView.visible = visible;
    }

    public void draw(GL10 gl) {
        super.draw(gl);
        if (this.overlayDrawingMode && this.overlayView.visible) {
            this.overlayView.draw(gl);
        }
    }

    public boolean isSelected() {
        return this.selected;
    }
}
