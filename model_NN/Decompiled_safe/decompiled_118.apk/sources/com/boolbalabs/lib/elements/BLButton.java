package com.boolbalabs.lib.elements;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.game.ZNode;
import com.boolbalabs.lib.graphics.NumberView2D;
import com.boolbalabs.lib.managers.TexturesManager;
import javax.microedition.khronos.opengles.GL10;

public class BLButton extends ZNode {
    private NumberView2D currentNumberView;
    private int digitsResourceId;
    private boolean enabled;
    private Point fixedPointOffset;
    private Point fixedPointRip;
    private boolean modeDisabledTextureAsMainTexture;
    private NumberView2D numberDisabledView;
    private boolean numberDrawingMode;
    private NumberView2D numberView;
    public boolean overlayDrawingMode;
    private ZNode overlayView;
    private Rect rectOnScreenOverlayRip;
    private Rect rectOnScreenRip;
    private Rect rectOnTexture;
    private Rect rectOnTextureDisabled;
    private Rect rectOnTextureOverlay;
    private Rect rectOnTexturePressed;

    public BLButton(int resourceId, int overlayResourceId, int digitsResourceId2) {
        super(resourceId, 0);
        this.overlayDrawingMode = false;
        this.numberDrawingMode = false;
        this.enabled = true;
        this.modeDisabledTextureAsMainTexture = false;
        this.overlayView = new ZNode(overlayResourceId, 0);
        this.userInteractionEnabled = true;
        this.digitsResourceId = digitsResourceId2;
    }

    public BLButton(int resourceId) {
        this(resourceId, resourceId, resourceId);
    }

    public void setRects(Rect rectOnScreen, Rect rectOnTexture2, Rect rectOnTexture_pressed, Rect rectOnTexture_disabled) {
        this.rectOnScreenRip = rectOnScreen;
        this.rectOnTexture = rectOnTexture2;
        this.rectOnTexturePressed = rectOnTexture_pressed;
        this.rectOnTextureDisabled = rectOnTexture_disabled;
    }

    public void setRectsOverlay(Rect rectOnScreenOverlayRip2, Rect rectOnTextureOverlay2) {
        this.rectOnScreenOverlayRip = rectOnScreenOverlayRip2;
        this.rectOnTextureOverlay = rectOnTextureOverlay2;
        this.overlayView.visible = false;
        if (this.overlayDrawingMode && rectOnTextureOverlay2 != null && rectOnScreenOverlayRip2 != null) {
            this.overlayView.initWithFrame(rectOnScreenOverlayRip2, rectOnTextureOverlay2);
        }
    }

    public void initialize() {
        initWithFrame(this.rectOnScreenRip, this.rectOnTexture);
    }

    public void setDisabledTextureAsMainTexture() {
        setRectOnTexture(this.rectOnTextureDisabled);
        this.modeDisabledTextureAsMainTexture = true;
        this.currentNumberView = this.numberDisabledView;
    }

    public void resumeNormalTextures() {
        setRectOnTexture(this.rectOnTexture);
        this.modeDisabledTextureAsMainTexture = false;
        this.currentNumberView = this.numberView;
    }

    public void onAfterLoad() {
        super.onAfterLoad();
        setEnabled(this.enabled);
        if (this.overlayDrawingMode) {
            this.overlayView.setTexture(TexturesManager.getInstance().getTextureByResourceId(this.overlayView.getResourceId()));
        }
        if (this.numberDrawingMode) {
            this.numberView.setTexture(TexturesManager.getInstance().getTextureByResourceId(this.numberView.getResourceId()));
            this.numberDisabledView.setTexture(TexturesManager.getInstance().getTextureByResourceId(this.numberDisabledView.getResourceId()));
        }
    }

    public void touchDownAction(Point touchDownPoint) {
        setRectOnTexture(this.rectOnTexturePressed);
        setOverlayVisible(true);
    }

    public void touchUpAction(Point touchDownPoint, Point touchUpPoint) {
        if (this.modeDisabledTextureAsMainTexture) {
            setRectOnTexture(this.rectOnTextureDisabled);
        } else {
            setRectOnTexture(this.rectOnTexture);
        }
    }

    public void setOverlayVisible(boolean visible) {
        if (this.overlayDrawingMode) {
            this.overlayView.visible = visible;
        }
    }

    public void setEnabled(boolean enabled2) {
        this.enabled = enabled2;
        this.userInteractionEnabled = enabled2;
        if (enabled2 && this.rectOnTexture != null) {
            setRectOnTexture(this.rectOnTexture);
            this.currentNumberView = this.numberView;
        } else if (!enabled2 && this.rectOnTextureDisabled != null) {
            setRectOnTexture(this.rectOnTextureDisabled);
            this.currentNumberView = this.numberDisabledView;
        }
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void draw(GL10 gl) {
        super.draw(gl);
        if (this.overlayDrawingMode && this.overlayView.visible) {
            this.overlayView.draw(gl);
        }
        if (this.numberDrawingMode && this.currentNumberView.visible) {
            this.currentNumberView.draw(gl);
        }
    }

    public void initializeNumberSubview(String[] digitsFramesNames, String[] disabledDigitsFramesNames, int sizeRip, Point numberPosition, int align) {
        this.numberView = new NumberView2D(this.digitsResourceId);
        this.numberDisabledView = new NumberView2D(this.digitsResourceId);
        this.fixedPointRip = numberPosition;
        if (this.fixedPointRip == null) {
            this.fixedPointRip = new Point(this.rectOnScreenRip.right - 5, (this.rectOnScreenRip.bottom - sizeRip) - 5);
        }
        this.fixedPointOffset = new Point(this.fixedPointRip.x - this.rectOnScreenRip.left, this.fixedPointRip.y - this.rectOnScreenRip.top);
        this.numberView.initNumberView(this.fixedPointRip, digitsFramesNames, align, sizeRip);
        this.numberView.initialize();
        this.numberDisabledView.initNumberView(this.fixedPointRip, disabledDigitsFramesNames, align, sizeRip);
        this.numberDisabledView.initialize();
        this.currentNumberView = this.numberView;
        this.numberDrawingMode = true;
    }

    public void setNumberOnButton(int numberOnButton) {
        boolean visible = numberOnButton >= 0;
        if (this.numberView != null && this.numberDisabledView != null) {
            this.numberView.visible = visible;
            this.numberDisabledView.visible = visible;
            if (visible) {
                this.numberView.setNumberToDraw(numberOnButton);
                this.numberDisabledView.setNumberToDraw(numberOnButton);
            }
        }
    }

    public void setPositionInRip(Point posInRip) {
        super.setPositionInRip(posInRip);
        if (this.numberDrawingMode) {
            this.fixedPointRip.set(posInRip.x + this.fixedPointOffset.x, posInRip.y + this.fixedPointOffset.y);
            this.numberView.setFixedPointRip(this.fixedPointRip);
            this.numberDisabledView.setFixedPointRip(this.fixedPointRip);
        }
    }

    public void setPositionInRip(int x, int y) {
        super.setPositionInRip(x, y);
        if (this.numberDrawingMode) {
            this.fixedPointRip.set(this.fixedPointOffset.x + x, this.fixedPointOffset.y + y);
            this.numberDisabledView.setFixedPointRip(this.fixedPointRip);
            this.numberView.setFixedPointRip(this.fixedPointRip);
        }
    }

    public void setZGL(float zGL) {
        super.setZGL(zGL);
        if (this.numberDrawingMode) {
            this.numberView.setZGL(zGL);
            this.numberDisabledView.setZGL(zGL);
        }
    }
}
