package com.boolbalabs.lib.transitions;

import com.boolbalabs.lib.game.ZDrawable;

public class TransitionRotation extends Transition {
    private float angleIncrementDeg;
    public float currentAngleDeg;
    private boolean fixedOnFinish;
    private long incrementTime;
    protected boolean isInitialized = false;
    public float originalAngleDeg;
    private long previousTime;

    public TransitionRotation(ZDrawable view) {
        super(view);
    }

    public void initialize(float angleIncrementDeg2, long incrementTime2, long durationMs, boolean fixedOnFinish2) {
        this.angleIncrementDeg = angleIncrementDeg2;
        this.durationMs = durationMs;
        this.fixedOnFinish = fixedOnFinish2;
        this.incrementTime = incrementTime2;
        this.isInitialized = true;
    }

    public void onTransitionStart() {
        this.originalAngleDeg = this.parentView.getRotationAngleDeg();
        this.previousTime = 0;
        update(0);
    }

    public void onTransitionStop() {
        if (!this.fixedOnFinish) {
            this.parentView.setPivotPointToCenterAndRotateByDeg((double) this.originalAngleDeg);
        }
        reset();
    }

    public void update(long timeSinceStartMs) {
        if (timeSinceStartMs - this.previousTime > this.incrementTime) {
            this.currentAngleDeg = this.parentView.getRotationAngleDeg();
            this.parentView.setPivotPointToCenterAndRotateByDeg((double) (this.currentAngleDeg + this.angleIncrementDeg));
            this.previousTime = timeSinceStartMs;
        }
    }

    public Transition createCopy(ZDrawable view) {
        if (!this.isInitialized) {
            return null;
        }
        TransitionRotation newTransition = new TransitionRotation(view);
        newTransition.initialize(this.angleIncrementDeg, this.incrementTime, this.durationMs, this.fixedOnFinish);
        return newTransition;
    }
}
