package com.boolbalabs.lib.transitions;

import android.view.animation.Interpolator;

public class SineInterpolator implements Interpolator {
    private EasingType type;

    public SineInterpolator(EasingType type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.IN) {
            return in(t);
        }
        if (this.type == EasingType.OUT) {
            return out(t);
        }
        if (this.type == EasingType.INOUT) {
            return inout(t);
        }
        return 0.0f;
    }

    private float in(float t) {
        return (float) ((-Math.cos(((double) t) * 1.5707963267948966d)) + 1.0d);
    }

    private float out(float t) {
        return (float) Math.sin(((double) t) * 1.5707963267948966d);
    }

    private float inout(float t) {
        return (float) (-0.5d * (Math.cos(3.141592653589793d * ((double) t)) - 1.0d));
    }
}
