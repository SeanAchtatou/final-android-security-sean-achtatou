package com.boolbalabs.lib.transitions;

import android.graphics.Point;
import android.graphics.Rect;
import com.boolbalabs.lib.game.ZDrawable;

public class TransitionZoom extends Transition {
    private float endSizeCoefficient;
    private boolean fixedOnFinish;
    private int halfHeight;
    private int halfWidth;
    protected boolean isInitialized = false;
    private Point originalCenter;
    public Rect originalRect = new Rect();
    private float originalSizeCoefficient;
    public Rect zoomRect = new Rect();

    public TransitionZoom(ZDrawable view) {
        super(view);
    }

    public void initialize(float originalSizeCoefficient2, float endSizeCoefficient2, long durationMs, boolean fixedOnFinish2) {
        this.endSizeCoefficient = endSizeCoefficient2;
        this.originalSizeCoefficient = originalSizeCoefficient2;
        this.durationMs = durationMs;
        this.fixedOnFinish = fixedOnFinish2;
        this.isInitialized = true;
    }

    public void onTransitionStart() {
        this.originalRect.set(this.parentView.getFrameInRip());
        this.originalCenter = this.parentView.getCenterInRip();
        this.halfWidth = this.originalRect.width() / 2;
        this.halfHeight = this.originalRect.height() / 2;
        update(0);
    }

    public void onTransitionStop() {
        if (!this.fixedOnFinish) {
            this.parentView.setFrameInRip(this.originalRect);
        }
        reset();
    }

    public void update(long timeSinceStartMs) {
        float alpha = ((float) timeSinceStartMs) / ((float) this.durationMs);
        if (alpha < 0.0f) {
            alpha = 0.0f;
        }
        if (alpha > 1.0f) {
            alpha = 1.0f;
        }
        float sizeCoefficient = (this.originalSizeCoefficient * (1.0f - alpha)) + (this.endSizeCoefficient * alpha);
        this.zoomRect.left = (int) (((float) this.originalCenter.x) - (((float) this.halfWidth) * sizeCoefficient));
        this.zoomRect.right = (int) (((float) this.originalCenter.x) + (((float) this.halfWidth) * sizeCoefficient));
        this.zoomRect.top = (int) (((float) this.originalCenter.y) - (((float) this.halfHeight) * sizeCoefficient));
        this.zoomRect.bottom = (int) (((float) this.originalCenter.y) + (((float) this.halfHeight) * sizeCoefficient));
        this.parentView.setFrameInRip(this.zoomRect);
    }

    public Transition createCopy(ZDrawable view) {
        if (!this.isInitialized) {
            return null;
        }
        TransitionZoom newTransition = new TransitionZoom(view);
        newTransition.initialize(this.originalSizeCoefficient, this.endSizeCoefficient, this.durationMs, this.fixedOnFinish);
        return newTransition;
    }
}
