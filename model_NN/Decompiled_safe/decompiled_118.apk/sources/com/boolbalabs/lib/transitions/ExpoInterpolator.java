package com.boolbalabs.lib.transitions;

import android.view.animation.Interpolator;

public class ExpoInterpolator implements Interpolator {
    private EasingType type;

    public ExpoInterpolator(EasingType type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.IN) {
            return in(t);
        }
        if (this.type == EasingType.OUT) {
            return out(t);
        }
        if (this.type == EasingType.INOUT) {
            return inout(t);
        }
        return 0.0f;
    }

    private float in(float t) {
        return (float) (t == 0.0f ? 0.0d : Math.pow(2.0d, (double) (10.0f * (t - 1.0f))));
    }

    private float out(float t) {
        return (float) (t >= 1.0f ? 1.0d : (-Math.pow(2.0d, (double) (-10.0f * t))) + 1.0d);
    }

    private float inout(float t) {
        if (t == 0.0f) {
            return 0.0f;
        }
        if (t >= 1.0f) {
            return 1.0f;
        }
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            return (float) (Math.pow(2.0d, (double) (10.0f * (t2 - 1.0f))) * 0.5d);
        }
        return (float) (((-Math.pow(2.0d, (double) (-10.0f * (t2 - 1.0f)))) + 2.0d) * 0.5d);
    }
}
