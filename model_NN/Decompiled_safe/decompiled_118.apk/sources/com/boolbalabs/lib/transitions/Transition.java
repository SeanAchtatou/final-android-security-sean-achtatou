package com.boolbalabs.lib.transitions;

import android.os.SystemClock;
import com.boolbalabs.lib.game.ZDrawable;

public abstract class Transition {
    protected long durationMs;
    private boolean isFinished = false;
    private boolean isStarted = false;
    protected ZDrawable parentView;
    protected long startTime;

    public abstract Transition createCopy(ZDrawable zDrawable);

    public abstract void update(long j);

    public Transition(ZDrawable view) {
        this.parentView = view;
    }

    public void start() {
        if (!this.isStarted) {
            onTransitionStart();
            this.isStarted = true;
            this.startTime = SystemClock.uptimeMillis();
        }
    }

    public void stop() {
        this.isFinished = true;
        onTransitionStop();
    }

    public void reset() {
        this.isStarted = false;
        this.isFinished = false;
        onTransitionReset();
    }

    public void onTransitionStart() {
    }

    public void onTransitionStop() {
    }

    public void onTransitionReset() {
    }

    public void innerUpdate() {
        if (this.isStarted && !this.isFinished) {
            long timeSinceStartMs = SystemClock.uptimeMillis() - this.startTime;
            if (timeSinceStartMs > this.durationMs) {
                stop();
            } else {
                update(timeSinceStartMs);
            }
        }
    }

    public boolean isFinished() {
        return this.isFinished;
    }

    public boolean isStarted() {
        return this.isStarted;
    }
}
