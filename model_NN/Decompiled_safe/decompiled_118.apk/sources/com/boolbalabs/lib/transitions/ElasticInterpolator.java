package com.boolbalabs.lib.transitions;

import android.view.animation.Interpolator;

public class ElasticInterpolator implements Interpolator {
    private float amplitude;
    private float period;
    private EasingType type;

    public ElasticInterpolator(EasingType type2, float amplitude2, float period2) {
        this.type = type2;
        this.amplitude = amplitude2;
        this.period = period2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.IN) {
            return in(t, this.amplitude, this.period);
        }
        if (this.type == EasingType.OUT) {
            return out(t, this.amplitude, this.period);
        }
        if (this.type == EasingType.INOUT) {
            return inout(t, this.amplitude, this.period);
        }
        return 0.0f;
    }

    private float in(float t, float a, float p) {
        float s;
        if (t == 0.0f) {
            return 0.0f;
        }
        if (t >= 1.0f) {
            return 1.0f;
        }
        if (p == 0.0f) {
            p = 0.3f;
        }
        if (a == 0.0f || a < 1.0f) {
            a = 1.0f;
            s = p / 4.0f;
        } else {
            s = (float) ((((double) p) / 6.283185307179586d) * Math.asin((double) (1.0f / a)));
        }
        float t2 = t - 1.0f;
        return (float) (-(((double) a) * Math.pow(2.0d, (double) (10.0f * t2)) * Math.sin((((double) (t2 - s)) * 6.283185307179586d) / ((double) p))));
    }

    private float out(float t, float a, float p) {
        float s;
        if (t == 0.0f) {
            return 0.0f;
        }
        if (t >= 1.0f) {
            return 1.0f;
        }
        if (p == 0.0f) {
            p = 0.3f;
        }
        if (a == 0.0f || a < 1.0f) {
            a = 1.0f;
            s = p / 4.0f;
        } else {
            s = (float) ((((double) p) / 6.283185307179586d) * Math.asin((double) (1.0f / a)));
        }
        return (float) ((((double) a) * Math.pow(2.0d, (double) (-10.0f * t)) * Math.sin((((double) (t - s)) * 6.283185307179586d) / ((double) p))) + 1.0d);
    }

    private float inout(float t, float a, float p) {
        float s;
        if (t == 0.0f) {
            return 0.0f;
        }
        if (t >= 1.0f) {
            return 1.0f;
        }
        if (p == 0.0f) {
            p = 0.45000002f;
        }
        if (a == 0.0f || a < 1.0f) {
            a = 1.0f;
            s = p / 4.0f;
        } else {
            s = (float) ((((double) p) / 6.283185307179586d) * Math.asin((double) (1.0f / a)));
        }
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            float t3 = t2 - 1.0f;
            return (float) (-0.5d * ((double) a) * Math.pow(2.0d, (double) (10.0f * t3)) * Math.sin((((double) (t3 - s)) * 6.283185307179586d) / ((double) p)));
        }
        float t4 = t2 - 1.0f;
        return (float) ((((double) a) * Math.pow(2.0d, (double) (-10.0f * t4)) * Math.sin((((double) (t4 - s)) * 6.283185307179586d) / ((double) p)) * 0.5d) + 1.0d);
    }
}
