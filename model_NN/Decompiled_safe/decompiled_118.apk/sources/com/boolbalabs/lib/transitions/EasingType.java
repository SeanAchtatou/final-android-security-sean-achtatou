package com.boolbalabs.lib.transitions;

public enum EasingType {
    IN,
    OUT,
    INOUT
}
