package com.boolbalabs.lib.transitions;

import android.view.animation.Interpolator;

public class QuintInterpolator implements Interpolator {
    private EasingType type;

    public QuintInterpolator(EasingType type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.IN) {
            return in(t);
        }
        if (this.type == EasingType.OUT) {
            return out(t);
        }
        if (this.type == EasingType.INOUT) {
            return inout(t);
        }
        return 0.0f;
    }

    private float in(float t) {
        return t * t * t * t * t;
    }

    private float out(float t) {
        float t2 = t - 1.0f;
        return (t2 * t2 * t2 * t2 * t2) + 1.0f;
    }

    private float inout(float t) {
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            return 0.5f * t2 * t2 * t2 * t2 * t2;
        }
        float t3 = t2 - 2.0f;
        return ((t3 * t3 * t3 * t3 * t3) + 2.0f) * 0.5f;
    }
}
