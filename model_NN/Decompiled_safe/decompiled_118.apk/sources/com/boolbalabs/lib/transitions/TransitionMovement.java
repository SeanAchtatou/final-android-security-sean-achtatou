package com.boolbalabs.lib.transitions;

import android.graphics.Point;
import com.boolbalabs.lib.game.ZDrawable;

public class TransitionMovement extends Transition {
    private float acceleration;
    private float accelerationSign;
    private boolean fixedOnFinish;
    private float initialVelocity;
    protected boolean isInitialized = false;
    public Point originalPosition = new Point();
    protected float shiftEndRip_X;
    protected float shiftEndRip_Y;
    protected float shiftStartRip_X;
    protected float shiftStartRip_Y;

    public TransitionMovement(ZDrawable view) {
        super(view);
    }

    public void initialize(float shiftStartRip_X2, float shiftEndRip_X2, float shiftStartRip_Y2, float shiftEndRip_Y2, float accelerationSign2, long durationMs, boolean fixedOnFinish2) {
        this.shiftStartRip_X = shiftStartRip_X2;
        this.shiftEndRip_X = shiftEndRip_X2;
        this.shiftStartRip_Y = shiftStartRip_Y2;
        this.shiftEndRip_Y = shiftEndRip_Y2;
        this.durationMs = durationMs;
        this.accelerationSign = accelerationSign2;
        this.fixedOnFinish = fixedOnFinish2;
        float tMax = ((float) durationMs) / 1000.0f;
        if (accelerationSign2 > 0.0f) {
            this.initialVelocity = 0.0f;
            this.acceleration = 2.0f / (tMax * tMax);
        } else if (accelerationSign2 < 0.0f) {
            this.initialVelocity = 5.0f;
            this.acceleration = (2.0f / (tMax * tMax)) - (this.initialVelocity / tMax);
        } else {
            this.initialVelocity = 1.0f / tMax;
            this.acceleration = 0.0f;
        }
        this.isInitialized = true;
    }

    public void onTransitionStart() {
        Point posInRip = this.parentView.getPositionRip();
        this.originalPosition.set(posInRip.x, posInRip.y);
        moveAlongPath(0);
    }

    public void onTransitionStop() {
        if (!this.fixedOnFinish && this.originalPosition != null) {
            this.parentView.setPositionInRip(this.originalPosition);
        }
        reset();
    }

    public void update(long timeSinceStartMs) {
        moveAlongPath(timeSinceStartMs);
    }

    private void moveAlongPath(long timeSinceStartMs) {
        float t = ((float) timeSinceStartMs) / 1000.0f;
        float alpha = (this.initialVelocity * t) + (((this.acceleration * t) * t) / 2.0f);
        if (alpha < 0.0f) {
            alpha = 0.0f;
        }
        if (alpha > 1.0f) {
            alpha = 1.0f;
        }
        this.parentView.setPositionInRip(this.originalPosition.x + ((int) ((this.shiftStartRip_X * (1.0f - alpha)) + (this.shiftEndRip_X * alpha))), this.originalPosition.y + ((int) ((this.shiftStartRip_Y * (1.0f - alpha)) + (this.shiftEndRip_Y * alpha))));
    }

    public Transition createCopy(ZDrawable view) {
        if (!this.isInitialized) {
            return null;
        }
        TransitionMovement newTransition = new TransitionMovement(view);
        newTransition.initialize(this.shiftStartRip_X, this.shiftEndRip_X, this.shiftStartRip_Y, this.shiftEndRip_Y, this.accelerationSign, this.durationMs, this.fixedOnFinish);
        return newTransition;
    }
}
