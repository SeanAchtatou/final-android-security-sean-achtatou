package com.boolbalabs.lib.transitions;

import android.view.animation.Interpolator;

public class QuadInterpolator implements Interpolator {
    private EasingType type;

    public QuadInterpolator(EasingType type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.IN) {
            return in(t);
        }
        if (this.type == EasingType.OUT) {
            return out(t);
        }
        if (this.type == EasingType.INOUT) {
            return inout(t);
        }
        return 0.0f;
    }

    private float in(float t) {
        return t * t;
    }

    private float out(float t) {
        return (-t) * (t - 2.0f);
    }

    private float inout(float t) {
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            return 0.5f * t2 * t2;
        }
        float t3 = t2 - 1.0f;
        return -0.5f * (((t3 - 2.0f) * t3) - 1.0f);
    }
}
