package com.boolbalabs.lib.transitions;

import android.view.animation.Interpolator;

public class BackInterpolator implements Interpolator {
    private float overshot;
    private EasingType type;

    public BackInterpolator(EasingType type2, float overshot2) {
        this.type = type2;
        this.overshot = overshot2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.IN) {
            return in(t, this.overshot);
        }
        if (this.type == EasingType.OUT) {
            return out(t, this.overshot);
        }
        if (this.type == EasingType.INOUT) {
            return inout(t, this.overshot);
        }
        return 0.0f;
    }

    private float in(float t, float o) {
        if (o == 0.0f) {
            o = 1.70158f;
        }
        return t * t * (((1.0f + o) * t) - o);
    }

    private float out(float t, float o) {
        if (o == 0.0f) {
            o = 1.70158f;
        }
        float t2 = t - 1.0f;
        return (t2 * t2 * (((o + 1.0f) * t2) + o)) + 1.0f;
    }

    private float inout(float t, float o) {
        if (o == 0.0f) {
            o = 1.70158f;
        }
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            float o2 = (float) (((double) o) * 1.525d);
            return t2 * t2 * (((o2 + 1.0f) * t2) - o2) * 0.5f;
        }
        float t3 = t2 - 2.0f;
        float o3 = (float) (((double) o) * 1.525d);
        return ((t3 * t3 * (((o3 + 1.0f) * t3) + o3)) + 2.0f) * 0.5f;
    }
}
