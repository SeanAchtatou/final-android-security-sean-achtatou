package com.boolbalabs.lib.transitions;

import android.view.animation.Interpolator;

public class CircInterpolator implements Interpolator {
    private EasingType type;

    public CircInterpolator(EasingType type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.IN) {
            return in(t);
        }
        if (this.type == EasingType.OUT) {
            return out(t);
        }
        if (this.type == EasingType.INOUT) {
            return inout(t);
        }
        return 0.0f;
    }

    private float in(float t) {
        return (float) (-(Math.sqrt((double) (1.0f - (t * t))) - 1.0d));
    }

    private float out(float t) {
        float t2 = t - 1.0f;
        return (float) Math.sqrt((double) (1.0f - (t2 * t2)));
    }

    private float inout(float t) {
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            return (float) (-0.5d * (Math.sqrt((double) (1.0f - (t2 * t2))) - 1.0d));
        }
        float t3 = t2 - 2.0f;
        return (float) (0.5d * (Math.sqrt((double) (1.0f - (t3 * t3))) + 1.0d));
    }
}
