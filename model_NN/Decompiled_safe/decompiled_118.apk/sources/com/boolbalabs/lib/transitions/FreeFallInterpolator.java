package com.boolbalabs.lib.transitions;

import android.view.animation.Interpolator;

public class FreeFallInterpolator implements Interpolator {
    private float g;
    private EasingType type;
    private float v0;

    public FreeFallInterpolator(EasingType type2, float g2, float v02) {
        this.type = type2;
        this.v0 = v02;
        this.g = g2;
    }

    public float getInterpolation(float t) {
        return in(t);
    }

    private float in(float t) {
        return (this.v0 * t) + (((this.g * t) * t) / 2.0f);
    }
}
