package com.scoreloop.client.android.ui;

import android.os.Bundle;
import com.scoreloop.client.android.ui.framework.ScreenActivity;

public class ProfileScreenActivity extends ScreenActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        display(StandardScoreloopManager.getFactory(ScoreloopManagerSingleton.get()).createProfileSettingsScreenDescription(null), savedInstanceState);
    }
}
