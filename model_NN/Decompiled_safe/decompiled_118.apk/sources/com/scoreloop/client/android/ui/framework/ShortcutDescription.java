package com.scoreloop.client.android.ui.framework;

public class ShortcutDescription {
    private final int _activeImageId;
    private final int _imageId;
    private final int _textId;

    public ShortcutDescription(int textId, int imageId, int activeImageId) {
        this._textId = textId;
        this._imageId = imageId;
        this._activeImageId = activeImageId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ShortcutDescription other = (ShortcutDescription) obj;
        if (this._activeImageId != other._activeImageId) {
            return false;
        }
        if (this._imageId != other._imageId) {
            return false;
        }
        if (this._textId != other._textId) {
            return false;
        }
        return true;
    }

    public int getActiveImageId() {
        return this._activeImageId;
    }

    public int getImageId() {
        return this._imageId;
    }

    public int getTextId() {
        return this._textId;
    }

    public int hashCode() {
        int i = 1 * 31;
        return ((((this._activeImageId + 31) * 31) + this._imageId) * 31) + this._textId;
    }
}
