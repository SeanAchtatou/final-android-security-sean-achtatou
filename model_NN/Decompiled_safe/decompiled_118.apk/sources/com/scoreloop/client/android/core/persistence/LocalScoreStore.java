package com.scoreloop.client.android.core.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.boolbalabs.rollit.settings.Settings;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.util.CryptoUtil;
import org.json.JSONException;

public class LocalScoreStore {
    private final CryptoUtil a;
    private final a b;
    private SQLiteDatabase c;
    private User d;

    private static class a extends SQLiteOpenHelper {
        a(Context context) {
            super(context, "scores.db", (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void a(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS scores;");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE scores (id TEXT NOT NULL PRIMARY KEY, mode TEXT, score TEXT NOT NULL, last_update INTEGER);");
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            a(sQLiteDatabase);
            onCreate(sQLiteDatabase);
        }
    }

    public LocalScoreStore(Context context, String str, String str2, User user) {
        this.b = new a(context);
        this.a = new CryptoUtil(str, str2);
        this.d = user;
    }

    private void a() {
        this.c.close();
        this.b.close();
    }

    private void b() {
        this.c = this.b.getWritableDatabase();
    }

    private ContentValues c(Score score) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", score.c());
        contentValues.put("mode", score.getMode());
        try {
            contentValues.put("score", this.a.a(score.d().toString()));
            contentValues.put("last_update", Long.valueOf(System.currentTimeMillis()));
            return contentValues;
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0078 A[SYNTHETIC, Splitter:B:22:0x0078] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.scoreloop.client.android.core.model.Score> a(java.lang.Integer r12) {
        /*
            r11 = this;
            r9 = 0
            monitor-enter(r11)
            java.util.ArrayList r8 = new java.util.ArrayList     // Catch:{ all -> 0x007f }
            r8.<init>()     // Catch:{ all -> 0x007f }
            r11.b()     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            if (r12 == 0) goto L_0x0082
            android.database.sqlite.SQLiteDatabase r0 = r11.c     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.lang.String r1 = "scores"
            r2 = 0
            java.lang.String r3 = "mode = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r5 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r6.<init>()     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.lang.String r7 = ""
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.lang.StringBuilder r6 = r6.append(r12)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r4[r5] = r6     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
        L_0x0033:
            if (r0 == 0) goto L_0x0091
            java.lang.String r1 = "id"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            java.lang.String r2 = "score"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
        L_0x0041:
            boolean r3 = r0.moveToNext()     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            if (r3 == 0) goto L_0x0091
            com.scoreloop.client.android.core.model.Score r3 = new com.scoreloop.client.android.core.model.Score     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            com.scoreloop.client.android.core.util.CryptoUtil r5 = r11.a     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            java.lang.String r6 = r0.getString(r2)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            java.lang.String r5 = r5.c(r6)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            r4.<init>(r5)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            r3.<init>(r4)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            java.lang.String r4 = r0.getString(r1)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            r3.a(r4)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            com.scoreloop.client.android.core.model.User r4 = r11.d     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            r3.a(r4)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            r8.add(r3)     // Catch:{ Exception -> 0x006b, all -> 0x009e }
            goto L_0x0041
        L_0x006b:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x006f:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x0075 }
            r2.<init>(r0)     // Catch:{ all -> 0x0075 }
            throw r2     // Catch:{ all -> 0x0075 }
        L_0x0075:
            r0 = move-exception
        L_0x0076:
            if (r1 == 0) goto L_0x007b
            r1.close()     // Catch:{ all -> 0x007f }
        L_0x007b:
            r11.a()     // Catch:{ all -> 0x007f }
            throw r0     // Catch:{ all -> 0x007f }
        L_0x007f:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x0082:
            android.database.sqlite.SQLiteDatabase r0 = r11.c     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            java.lang.String r1 = "scores"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00a3, all -> 0x009b }
            goto L_0x0033
        L_0x0091:
            if (r0 == 0) goto L_0x0096
            r0.close()     // Catch:{ all -> 0x007f }
        L_0x0096:
            r11.a()     // Catch:{ all -> 0x007f }
            monitor-exit(r11)
            return r8
        L_0x009b:
            r0 = move-exception
            r1 = r9
            goto L_0x0076
        L_0x009e:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0076
        L_0x00a3:
            r0 = move-exception
            r1 = r9
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.core.persistence.LocalScoreStore.a(java.lang.Integer):java.util.List");
    }

    public synchronized void a(Score score) {
        ContentValues c2 = c(score);
        b();
        this.c.insert("scores", null, c2);
        a();
    }

    public synchronized void b(Score score) {
        ContentValues c2 = c(score);
        b();
        this.c.update("scores", c2, "id = ?", new String[]{((String) Settings.FLURRY_ID_FULL) + score.c()});
        a();
    }
}
