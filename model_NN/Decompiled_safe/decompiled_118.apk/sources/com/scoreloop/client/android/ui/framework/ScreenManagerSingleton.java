package com.scoreloop.client.android.ui.framework;

public abstract class ScreenManagerSingleton {
    private static ScreenManager _singleton;

    public static ScreenManager get() {
        return _singleton;
    }

    public static void init(ScreenManager manager) {
        if (_singleton != null) {
            throw new IllegalStateException("ScreenManagerSingleton.init() can be called only once");
        }
        _singleton = manager;
    }

    public static void destroy() {
        _singleton = null;
    }
}
