package com.scoreloop.client.android.ui.component.profile;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;

public class ProfileListItem extends StandardListItem<Void> {
    public ProfileListItem(ComponentActivity activity, Drawable drawable, String title, String subTitle) {
        super(activity, drawable, title, subTitle, null);
    }

    public int getType() {
        return 17;
    }
}
