package com.scoreloop.client.android.core.controller;

import android.content.Context;
import android.database.Cursor;
import android.provider.Contacts;
import com.boolbalabs.rollit.settings.Settings;
import com.scoreloop.client.android.core.util.Base64;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

class AddressBook {
    private static final String[] a = {"data"};
    private static AddressBook b;
    private List<String> c;
    private String d;
    private MessageDigest e;
    private HashAlgorithm f;

    public enum HashAlgorithm {
        SHA1("SHA1"),
        MD5("MD5");
        
        private final String a;

        private HashAlgorithm(String str) {
            this.a = str;
        }

        private MessageDigest a() {
            try {
                return MessageDigest.getInstance(this.a);
            } catch (NoSuchAlgorithmException e) {
                throw new IllegalStateException("Failed to create a MessageDigest");
            }
        }
    }

    public AddressBook() {
        a(HashAlgorithm.SHA1);
    }

    public static AddressBook a() {
        if (b == null) {
            b = new AddressBook();
        }
        return b;
    }

    private String b(String str) {
        return str.trim().replaceAll("\t", Settings.FLURRY_ID_FULL).toLowerCase();
    }

    private void c(Context context) {
        d(context);
    }

    private void c(String str) {
        if (str == null) {
            throw new IllegalArgumentException();
        }
        this.e.reset();
        try {
            this.e.update(b(str).getBytes("UTF8"));
            if (this.d != null) {
                this.e.update(this.d.getBytes("UTF8"));
            }
            this.c.add(Base64.a(this.e.digest()));
        } catch (UnsupportedEncodingException e2) {
            throw new IllegalStateException("Unsupported encoding");
        }
    }

    private void d(Context context) {
        if (this.e == null) {
            this.e = HashAlgorithm.a(this.f);
        }
        this.c = new ArrayList();
        for (String c2 : b(context)) {
            c(c2);
        }
    }

    public List<String> a(Context context) {
        c(context);
        return this.c;
    }

    public void a(HashAlgorithm hashAlgorithm) {
        if (hashAlgorithm != this.f || this.e == null) {
            this.f = hashAlgorithm;
            this.e = null;
        }
    }

    public void a(String str) {
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public List<String> b(Context context) {
        Cursor query = context.getContentResolver().query(Contacts.ContactMethods.CONTENT_EMAIL_URI, a, null, null, null);
        ArrayList arrayList = new ArrayList();
        if (query != null) {
            int columnIndex = query.getColumnIndex("data");
            if (query.moveToFirst()) {
                do {
                    arrayList.add(query.getString(columnIndex));
                } while (query.moveToNext());
            }
        }
        return arrayList;
    }
}
