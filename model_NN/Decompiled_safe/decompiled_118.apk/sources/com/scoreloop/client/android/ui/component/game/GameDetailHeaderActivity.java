package com.scoreloop.client.android.ui.component.game;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.boolbalabs.rollit.R;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.PackageManager;
import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import com.scoreloop.client.android.ui.util.ImageDownloader;

public class GameDetailHeaderActivity extends ComponentHeaderActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.sl_header_game);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        setNeedsRefresh();
    }

    public void onRefresh(int flags) {
        Button controlButton = (Button) findViewById(R.id.sl_control_button);
        final Game game = getGame();
        ImageDownloader.downloadImage(game.getImageUrl(), getResources().getDrawable(R.drawable.sl_icon_games_loading), getImageView(), null);
        setTitle(game.getName());
        setSubTitle(game.getPublisherName());
        if (game.getPackageNames() != null) {
            if (PackageManager.isGameInstalled(this, game)) {
                controlButton.setText(getString(R.string.sl_launch));
                controlButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        GameDetailHeaderActivity.this.getTracker().trackEvent(TrackerEvents.CAT_NAVI, TrackerEvents.NAVI_HEADER_GAME_LAUNCH, game.getName(), 0);
                        PackageManager.launchGame(GameDetailHeaderActivity.this, game);
                    }
                });
            } else {
                controlButton.setText(getString(R.string.sl_get));
                controlButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        GameDetailHeaderActivity.this.getTracker().trackEvent(TrackerEvents.CAT_NAVI, TrackerEvents.NAVI_HEADER_GAME_GET, game.getName(), 0);
                        PackageManager.installGame(GameDetailHeaderActivity.this, game);
                    }
                });
            }
            controlButton.setVisibility(0);
            return;
        }
        controlButton.setVisibility(8);
    }
}
