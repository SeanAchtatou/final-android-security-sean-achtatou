package com.scoreloop.client.android.ui.component.news;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.boolbalabs.rollit.R;
import com.scoreloop.client.android.core.addon.RSSItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.EmptyListItem;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.framework.ValueStore;
import java.util.List;

public class NewsListActivity extends ComponentListActivity<BaseListItem> {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new BaseListAdapter(this));
        addObservedKeys(ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NEWS_FEED));
    }

    public void onListItemClick(BaseListItem item) {
        if (item.getType() == 15) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(((NewsListItem) item).getItem().getLinkUrlString())));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        List<RSSItem> feed = (List) getUserValues().getValue(Constant.NEWS_FEED);
        if (feed != null) {
            for (RSSItem item : feed) {
                item.setHasPersistentReadFlag(true);
            }
        }
        getUserValues().putValue(Constant.NEWS_NUMBER_UNREAD_ITEMS, 0);
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        BaseListAdapter<BaseListItem> adapter = getBaseListAdapter();
        adapter.clear();
        List<RSSItem> feed = (List) getUserValues().getValue(Constant.NEWS_FEED);
        if (feed == null || feed.size() <= 0) {
            adapter.add(new EmptyListItem(this, getString(R.string.sl_no_news)));
            return;
        }
        for (RSSItem item : feed) {
            adapter.add(new NewsListItem(this, item));
        }
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (Constant.NEWS_FEED.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_OLDER_THAN, Long.valueOf((long) Constant.NEWS_FEED_REFRESH_TIME));
        }
    }
}
