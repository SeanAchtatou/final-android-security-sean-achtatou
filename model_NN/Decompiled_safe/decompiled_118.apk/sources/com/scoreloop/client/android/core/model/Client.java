package com.scoreloop.client.android.core.model;

import android.app.Service;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import com.boolbalabs.rollit.settings.Settings;
import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__2_0_0;
import com.scoreloop.client.android.core.PublishedFor__2_2_0;
import com.scoreloop.client.android.core.PublishedFor__2_3_0;
import com.scoreloop.client.android.core.server.Server;
import com.scoreloop.client.android.core.util.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;

public class Client implements SessionObserver {
    private final BitSet a = new BitSet();
    private ClientObserver b;
    private Server c;
    private Session d;

    Client() {
    }

    @PublishedFor__2_3_0
    public Client(Context context, String str, ClientObserver clientObserver) {
        if (context == null) {
            throw new IllegalArgumentException("the application context must not be null");
        } else if (str == null) {
            throw new IllegalArgumentException("the secret must not be null");
        } else {
            Properties properties = getProperties(context);
            if (properties.getProperty("game.secret") != null) {
                throw new IllegalStateException("remove game.secret from the scoreloop.properties file!");
            }
            properties.setProperty("game.secret", str);
            a(context, clientObserver, properties, "https://api.scoreloop.com/bayeux");
        }
    }

    @PublishedFor__1_0_0
    public Client(Context context, String str, String str2, ClientObserver clientObserver) {
        if (context == null) {
            throw new IllegalArgumentException("the application context must not be null");
        }
        Properties properties = getProperties(context);
        if (!(str == null || str2 == null)) {
            properties.setProperty("game.id", str);
            properties.setProperty("game.secret", str2);
        }
        a(context, clientObserver, properties, "https://api.scoreloop.com/bayeux");
    }

    private Game a() {
        return this.d.getGame();
    }

    private static String a(Context context) {
        String str;
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 1);
            str = packageInfo.versionName != null ? packageName + "/" + packageInfo.versionName : packageName + "/" + packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            str = packageName + "/undetermined";
        }
        return (str + " android/" + Build.VERSION.RELEASE) + " core-sdk-version/2.3.2";
    }

    private static void a(Properties properties) {
        Integer num;
        Integer num2;
        a(properties, "game.id");
        a(properties, "game.secret");
        a(properties, "game.name", null);
        a(properties, "game.version", "1.0");
        a(properties, "currency.symbol", null);
        a(properties, "currency.code", null);
        a(properties, "currency.name.singular", null);
        a(properties, "currency.name.plural", null);
        a(properties, "game.score.comparator.result", null);
        a(properties, "game.score.comparator.minorResult", null);
        String property = properties.getProperty("game.mode.min");
        if (property != null) {
            try {
                num = Integer.valueOf(Integer.parseInt(property));
                if (num.intValue() < 0) {
                    throw new Exception();
                }
            } catch (Exception e) {
                throw new IllegalStateException("property game.mode.min must be a valid integer >= 0");
            }
        } else {
            num = null;
        }
        String property2 = properties.getProperty("game.mode.max");
        if (property2 != null) {
            try {
                num2 = Integer.valueOf(Integer.parseInt(property2));
                if (num2.intValue() < 1) {
                    throw new Exception();
                }
            } catch (Exception e2) {
                throw new IllegalStateException("property game.mode.max must be a valid integer >= 1");
            }
        } else {
            num2 = null;
        }
        if (!(num == null && num2 == null)) {
            if (num == null || num2 == null) {
                throw new IllegalStateException("you have to provide both game.mode.min and game.mode.max");
            } else if (num.intValue() >= num2.intValue()) {
                throw new IllegalStateException("game.mode.min must be less than game.mode.max");
            }
        }
        b(properties, "game.score.comparator.result");
        b(properties, "game.score.comparator.minorResult");
    }

    private static void a(Properties properties, String str) {
        if (properties.getProperty(str) == null) {
            throw new IllegalStateException("property " + str + " must not be null");
        }
    }

    private static void a(Properties properties, String str, String str2) {
        if (properties.getProperty(str) == null) {
            Logger.a("property " + str + " should not be null. Using default " + str2 + " instead");
            if (str2 != null) {
                properties.setProperty(str, str2);
            }
        }
    }

    private static Properties b(Context context) {
        try {
            InputStream open = context.getAssets().open("scoreloop.properties");
            try {
                Properties properties = new Properties();
                properties.load(open);
                return properties;
            } catch (IOException e) {
                Logger.c("Client", "Failed to load scoreloop.properties file");
                return null;
            }
        } catch (IOException e2) {
            Logger.c("Client", "No scoreloop.properties file found");
            return null;
        }
    }

    private static void b(Properties properties, String str) {
        String property = properties.getProperty(str);
        if (property != null && !"ascending".equalsIgnoreCase(property) && !"descending".equalsIgnoreCase(property)) {
            throw new IllegalStateException(str + " must be either of (" + "ascending" + "," + "descending" + ")");
        }
    }

    private static void c(Context context) {
        boolean z;
        try {
            String[] strArr = {"android.permission.READ_PHONE_STATE", "android.permission.INTERNET"};
            String[] strArr2 = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
            if (strArr2 != null) {
                HashSet hashSet = new HashSet();
                Collections.addAll(hashSet, strArr2);
                int i = 0;
                while (true) {
                    if (i >= strArr.length) {
                        z = true;
                        break;
                    } else if (!hashSet.contains(strArr[i])) {
                        z = false;
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                z = false;
            }
            if (!z) {
                StringBuilder sb = new StringBuilder();
                sb.append("\n  !!! ").append("Scoreloop currently requires you to add the following permissions to your AndroidManifest.xml file:");
                for (int i2 = 0; i2 < strArr.length; i2++) {
                    sb.append("\n  !!! ").append("<uses-permission android:name=\"" + strArr[i2] + "\" />");
                }
                throw new IllegalStateException(sb.toString());
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
    }

    private void d(Context context) {
        this.d.a(context);
        this.d.a().a(new b(context).a());
        this.c.a(a(context));
    }

    @PublishedFor__2_0_0
    public static Properties getProperties(Context context) {
        Properties b2 = b(context.getApplicationContext());
        return b2 == null ? new Properties() : b2;
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, ClientObserver clientObserver, Properties properties, String str) {
        a(properties);
        c(context);
        this.b = clientObserver;
        Game game = null;
        String property = properties.getProperty("game.id");
        String property2 = properties.getProperty("game.secret");
        if (!(property == null || property2 == null)) {
            game = new Game(property, property2);
            game.d(properties.getProperty("game.version"));
            game.c(properties.getProperty("game.name"));
            String property3 = properties.getProperty("game.mode.min");
            if (property3 != null) {
                game.d(Integer.valueOf(Integer.parseInt(property3)));
            }
            String property4 = properties.getProperty("game.mode.max");
            if (property4 != null) {
                game.b(Integer.valueOf(Integer.parseInt(property4)));
            }
            String property5 = properties.getProperty("game.score.comparator.result");
            if (property5 != null) {
                game.a(property5.equals("ascending"));
            }
            String property6 = properties.getProperty("game.score.comparator.minorResult");
            if (property6 != null) {
                game.b(property6.equals("ascending"));
            }
        }
        try {
            this.c = new Server(new URL(str));
            if (game != null) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("id", game.getIdentifier());
                    jSONObject2.put("secret", game.c());
                    jSONObject2.put("version", game.getVersion());
                    jSONObject.put(Game.a, jSONObject2);
                    this.c.a(jSONObject);
                } catch (JSONException e) {
                    throw new IllegalStateException(e);
                }
            }
            this.d = new Session(this, this.c);
            this.d.a(game);
            Session.a(this.d);
            Money.a(properties.getProperty("currency.code"));
            Money.b(properties.getProperty("currency.symbol"));
            Money.c(properties.getProperty("currency.name.singular"));
            Money.d(properties.getProperty("currency.name.plural"));
            if (context instanceof Service) {
                d(context);
            } else {
                d(context.getApplicationContext());
            }
        } catch (MalformedURLException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @PublishedFor__1_0_0
    public Range getGameLevels() {
        if (a() != null) {
            return !a().hasLevels() ? new Range(0, 1) : new Range(a().getMinLevel().intValue(), a().getLevelCount().intValue());
        }
        throw new IllegalStateException("canno access game levels without a game");
    }

    @PublishedFor__1_0_0
    public Range getGameModes() {
        if (a() != null) {
            return (a() == null || !a().hasModes()) ? new Range(0, 1) : new Range(a().getMinMode().intValue(), a().getModeCount().intValue());
        }
        throw new IllegalStateException("cannot access game modes without a game");
    }

    @PublishedFor__2_2_0
    public String getInfoString() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("version", "2.3.2");
            return jSONObject.toString();
        } catch (JSONException e) {
            return Settings.FLURRY_ID_FULL;
        }
    }

    @PublishedFor__1_0_0
    public Session getSession() {
        return this.d;
    }

    @PublishedFor__1_0_0
    public void setGameLevels(Range range) {
        if (a() == null) {
            throw new IllegalStateException("cannot access game levels without a game");
        }
        a().c(Integer.valueOf(range.a()));
        a().a(Integer.valueOf(range.b()));
    }

    @PublishedFor__1_0_0
    public void setGameModes(Range range) {
        if (a() == null) {
            throw new IllegalStateException("canno access game modes without a game");
        }
        a().d(Integer.valueOf(range.a()));
        a().b(Integer.valueOf(range.b()));
    }
}
