package com.scoreloop.client.android.core.controller;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchSpec {
    private final List<c> a;
    private final e b;

    public SearchSpec() {
        this(null);
    }

    public SearchSpec(e eVar) {
        this.a = new ArrayList();
        this.b = eVar;
    }

    public JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (this.b != null) {
            jSONObject.put("order_by", this.b.a());
            jSONObject.put("order_as", this.b.b());
        }
        if (this.a != null && this.a.size() > 0) {
            JSONObject jSONObject2 = new JSONObject();
            for (c next : this.a) {
                jSONObject2.put(next.a(), next.b());
            }
            jSONObject.put("conditions", jSONObject2);
        }
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("search", jSONObject);
        return jSONObject3;
    }

    public void a(c cVar) {
        this.a.add(cVar);
    }
}
