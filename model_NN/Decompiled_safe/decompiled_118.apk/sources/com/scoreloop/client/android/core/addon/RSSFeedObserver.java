package com.scoreloop.client.android.core.addon;

import com.scoreloop.client.android.core.PublishedFor__2_0_0;

public interface RSSFeedObserver {
    @PublishedFor__2_0_0
    void feedDidFailToReceiveNextItem(RSSFeed rSSFeed, Exception exc);

    @PublishedFor__2_0_0
    void feedDidReceiveNextItem(RSSFeed rSSFeed, RSSItem rSSItem);

    @PublishedFor__2_0_0
    void feedDidRequestNextItem(RSSFeed rSSFeed);
}
