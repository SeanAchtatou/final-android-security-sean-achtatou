package com.scoreloop.client.android.ui;

import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.component.post.PostOverlayActivity;

public class PostScoreOverlayActivity extends PostOverlayActivity {
    /* access modifiers changed from: protected */
    public Entity getMessageTarget() {
        StandardScoreloopManager manager = StandardScoreloopManager.getFactory(ScoreloopManagerSingleton.get());
        Entity target = manager.getLastChallenge();
        if (target == null || target.getIdentifier() == null) {
            return manager.getLastScore();
        }
        return target;
    }

    /* access modifiers changed from: protected */
    public String getPostText() {
        StandardScoreloopManager manager = StandardScoreloopManager.getFactory(ScoreloopManagerSingleton.get());
        Entity target = getMessageTarget();
        if (target instanceof Score) {
            return "Score: " + StringFormatter.formatSocialNetworkPostScore((Score) target, manager.getConfiguration());
        }
        return "Challenge";
    }
}
