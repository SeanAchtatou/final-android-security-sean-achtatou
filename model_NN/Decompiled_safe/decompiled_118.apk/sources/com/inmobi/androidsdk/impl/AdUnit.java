package com.inmobi.androidsdk.impl;

import com.boolbalabs.rollit.settings.Settings;

public class AdUnit {
    String CDATABlock;
    AdActionTypes adActionType = AdActionTypes.AdActionType_Web;
    AdTypes adType = AdTypes.AdType_None;
    String defaultTargetUrl;
    String deviceInfoUploadUrl;
    int height;
    boolean sendDeviceInfo;
    String targetUrl;
    int width;

    public enum AdTypes {
        AdType_None,
        AdType_Text,
        AdType_Banner,
        AdType_Search;

        public String toString() {
            return super.toString().replaceFirst("AdType_", Settings.FLURRY_ID_FULL).toLowerCase();
        }
    }

    public enum AdActionTypes {
        AdActionType_None,
        AdActionType_Web,
        AdActionType_SMS,
        AdActionType_Search,
        AdActionType_Call;

        public String toString() {
            return super.toString().replaceFirst("AdActionType_", Settings.FLURRY_ID_FULL).toLowerCase();
        }
    }

    public static AdActionTypes adActionTypefromString(String text) {
        AdActionTypes aatype = AdActionTypes.AdActionType_None;
        if (text == null) {
            return aatype;
        }
        if (text.equalsIgnoreCase("call")) {
            return AdActionTypes.AdActionType_Call;
        }
        if (text.equalsIgnoreCase("sms")) {
            return AdActionTypes.AdActionType_SMS;
        }
        if (text.equalsIgnoreCase("search")) {
            return AdActionTypes.AdActionType_Search;
        }
        return AdActionTypes.AdActionType_Web;
    }

    public static AdTypes adTypefromString(String text) {
        AdTypes aatype = AdTypes.AdType_None;
        if (text == null) {
            return aatype;
        }
        if (text.equalsIgnoreCase("banner")) {
            return AdTypes.AdType_Banner;
        }
        if (text.equalsIgnoreCase("text")) {
            return AdTypes.AdType_Text;
        }
        if (text.equalsIgnoreCase("search")) {
            return AdTypes.AdType_Search;
        }
        return aatype;
    }

    public AdActionTypes getAdActionType() {
        return this.adActionType;
    }

    public void setAdActionType(AdActionTypes adActionType2) {
        this.adActionType = adActionType2;
    }

    public AdTypes getAdType() {
        return this.adType;
    }

    public void setAdType(AdTypes adType2) {
        this.adType = adType2;
    }

    public String getTargetUrl() {
        return this.targetUrl;
    }

    public void setTargetUrl(String targetUrl2) {
        this.targetUrl = targetUrl2;
    }

    public boolean isSendDeviceInfo() {
        return this.sendDeviceInfo;
    }

    public void setSendDeviceInfo(boolean sendDeviceInfo2) {
        this.sendDeviceInfo = sendDeviceInfo2;
    }

    public String getDeviceInfoUploadUrl() {
        return this.deviceInfoUploadUrl;
    }

    public void setDeviceInfoUploadUrl(String deviceInfoUploadUrl2) {
        this.deviceInfoUploadUrl = deviceInfoUploadUrl2;
    }

    public String getCDATABlock() {
        return this.CDATABlock;
    }

    public void setCDATABlock(String cDATABlock) {
        this.CDATABlock = cDATABlock;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height2) {
        this.height = height2;
    }

    public String getDefaultTargetUrl() {
        return this.defaultTargetUrl;
    }

    public void setDefaultTargetUrl(String defaultTargetUrl2) {
        this.defaultTargetUrl = defaultTargetUrl2;
    }
}
