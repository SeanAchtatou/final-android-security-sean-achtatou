package com.inmobi.androidsdk.impl;

public final class Constants {
    public static final String AD_SERVER_CACHED_LIFE = "inmobicachedlife";
    public static final String AD_SERVER_CACHED_URL = "inmobicachedserver";
    public static final String AD_SERVER_URL = "http://i.w.inmobi.com/showad.asm";
    public static final String AD_TEST_SERVER_URL = "http://i.w.sandbox.inmobi.com/showad.asm";
    public static final long CACHED_AD_SERVER_LIFE = 43200000;
    public static final String CACHED_AD_SERVER_TIMESTAMP = "inmobi_cached_timestamp";
    public static final int HTTP_SUCCESS_CODE = 200;
    public static final String LOGGING_TAG = "InmobiAndroidSDK2.3";
    public static final String SDK_VERSION = "2.3";
}
