package com.inmobi.androidsdk.impl.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.inmobi.androidsdk.impl.AdConstructionException;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.UserInfo;
import com.inmobi.androidsdk.impl.XMLParser;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public final class RequestResponseManager {
    private Context context;
    private String newAdActionType = null;

    public enum ActionType {
        AdRequest,
        DeviceInfoUpload,
        AdClick
    }

    public RequestResponseManager(Context ctx) {
        this.context = ctx;
    }

    public AdUnit requestAd(String urlString, UserInfo clientInfo, ActionType requestType) throws ConnectionException, AdConstructionException {
        String postBody = HttpRequestBuilder.buildPostBody(clientInfo, requestType);
        Log.v(Constants.LOGGING_TAG, postBody);
        HttpURLConnection connection = setupConnection(urlString, clientInfo);
        postData(connection, postBody);
        try {
            return retrieveAd(connection, clientInfo);
        } catch (IOException e) {
            Log.w(Constants.LOGGING_TAG, "Exception occured while requesting an ad");
            return null;
        }
    }

    public String initiateClick(String urlString, UserInfo clientInfo, List<?> extraData) throws ConnectionException {
        String postBody = HttpRequestBuilder.buildPostBody(clientInfo, ActionType.AdClick);
        String adActionType = null;
        if (extraData != null && !extraData.isEmpty() && "x-mkhoj-adactiontype".equals(extraData.get(0))) {
            adActionType = (String) extraData.get(1);
        }
        HttpURLConnection connection = setupConnection(urlString, clientInfo);
        if (adActionType != null && !clientInfo.isTestMode()) {
            connection.setRequestProperty("x-mkhoj-adactionType", adActionType);
        }
        postData(connection, postBody.toString());
        return getRedirectionURLText(connection, urlString);
    }

    public void uploadDeviceInfo(String urlString, UserInfo clientInfo, ActionType requestType) throws ConnectionException {
        IOException ioException;
        String postBody = HttpRequestBuilder.buildPostBody(clientInfo, requestType);
        HttpURLConnection connection = setupConnection(urlString, clientInfo);
        postData(connection, postBody);
        BufferedReader reader = null;
        try {
            BufferedReader reader2 = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            try {
                connection.getResponseCode();
                closeResource(reader2);
            } catch (IOException e) {
                ioException = e;
                reader = reader2;
                try {
                    throw new ConnectionException("Exception retrieving http response ", ioException);
                } catch (Throwable th) {
                    th = th;
                    closeResource(reader);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                reader = reader2;
                closeResource(reader);
                throw th;
            }
        } catch (IOException e2) {
            ioException = e2;
            throw new ConnectionException("Exception retrieving http response ", ioException);
        }
    }

    private HttpURLConnection setupConnection(String urlString, UserInfo userInfo) throws ConnectionException {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(urlString).openConnection();
            setConnectionParams(connection, userInfo);
            return connection;
        } catch (IOException e) {
            throw new ConnectionException("Encountered exception setting up a connection with URL : " + urlString, e);
        }
    }

    private void postData(HttpURLConnection connection, String postBody) throws ConnectionException {
        IOException ioException;
        connection.setRequestProperty("Content-Length", Integer.toString(postBody.length()));
        BufferedWriter writer = null;
        try {
            BufferedWriter writer2 = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            try {
                writer2.write(postBody);
                closeResource(writer2);
            } catch (IOException e) {
                ioException = e;
                writer = writer2;
                try {
                    throw new ConnectionException("Error posting data over connection ", ioException);
                } catch (Throwable th) {
                    th = th;
                    closeResource(writer);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                writer = writer2;
                closeResource(writer);
                throw th;
            }
        } catch (IOException e2) {
            ioException = e2;
            throw new ConnectionException("Error posting data over connection ", ioException);
        }
    }

    private static void closeResource(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException exception) {
                Log.d(Constants.LOGGING_TAG, "Exception closing resource: " + resource, exception);
            }
        }
    }

    private static void setConnectionParams(HttpURLConnection connection, UserInfo userInfo) throws ProtocolException {
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setConnectTimeout(30000);
        connection.setReadTimeout(30000);
        connection.setRequestProperty("user-agent", userInfo.getUserAgent());
        connection.setRequestProperty("x-mkhoj-testmode", userInfo.isTestMode() ? "YES" : "NO");
        connection.setRequestProperty("x-mkhoj-siteid", userInfo.getSiteId());
        connection.setRequestProperty("x-inmobi-phone-useragent", userInfo.getPhoneDefaultUserAgent());
        connection.setRequestProperty("Cookie", "Android-uuid=" + userInfo.getDeviceId());
        connection.setRequestMethod("POST");
        connection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
        if (userInfo.isTestMode()) {
            connection.setRequestProperty("x-mkhoj-adactiontype", userInfo.getTestModeAdActionType() != null ? userInfo.getTestModeAdActionType() : "web");
        }
    }

    private AdUnit retrieveAd(HttpURLConnection connection, UserInfo userInfoRef) throws ConnectionException, AdConstructionException, IOException {
        IOException ioException;
        List<String> cachedUrlLifeInfo;
        List<String> extraInfo;
        String deviceInfoUpURL = null;
        BufferedReader reader = null;
        if (connection.getResponseCode() == 200) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                try {
                    Map<String, List<String>> responseHeader = connection.getHeaderFields();
                    if (responseHeader != null) {
                        if (responseHeader.containsKey("x-mkhoj-ph") && (extraInfo = responseHeader.get("x-mkhoj-ph")) != null && extraInfo.size() == 1) {
                            deviceInfoUpURL = ((String) extraInfo.get(0)).trim();
                        }
                        if (responseHeader.containsKey(Constants.AD_SERVER_CACHED_URL)) {
                            List<String> cachedUrlInfo = responseHeader.get(Constants.AD_SERVER_CACHED_URL);
                            String cachedUrlLife = null;
                            String cachedUrl = null;
                            if (cachedUrlInfo != null && cachedUrlInfo.size() == 1) {
                                cachedUrl = ((String) cachedUrlInfo.get(0)).trim();
                                if (responseHeader.containsKey(Constants.AD_SERVER_CACHED_LIFE) && (cachedUrlLifeInfo = responseHeader.get(Constants.AD_SERVER_CACHED_LIFE)) != null && cachedUrlLifeInfo.size() == 1) {
                                    cachedUrlLife = ((String) cachedUrlLifeInfo.get(0)).trim();
                                }
                            }
                            storeCachedUrl(cachedUrl, cachedUrlLife);
                        }
                    }
                    AdUnit newAd = new XMLParser().buildAdUnitFromResponseData(bufferedReader);
                    setAdditionalAdInfo(newAd, userInfoRef, deviceInfoUpURL);
                    closeResource(bufferedReader);
                    return newAd;
                } catch (IOException e) {
                    ioException = e;
                    reader = bufferedReader;
                    try {
                        throw new ConnectionException("Error requesting new Ad ", ioException);
                    } catch (Throwable th) {
                        th = th;
                        closeResource(reader);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    reader = bufferedReader;
                    closeResource(reader);
                    throw th;
                }
            } catch (IOException e2) {
                ioException = e2;
                throw new ConnectionException("Error requesting new Ad ", ioException);
            }
        } else {
            Log.w(Constants.LOGGING_TAG, "Invalid Request. Please check your app/site ID.");
            return null;
        }
    }

    private void setAdditionalAdInfo(AdUnit newAd, UserInfo userInfoRef, String deviceInfoUpURL) {
        if (newAd != null) {
            newAd.setSendDeviceInfo(true);
            newAd.setDeviceInfoUploadUrl(deviceInfoUpURL);
        }
    }

    private String getRedirectionURLText(HttpURLConnection conn, String urlString) {
        List<String> newAdActionList;
        String url = null;
        HttpURLConnection.setFollowRedirects(false);
        try {
            conn.getResponseCode();
            if (0 != 0) {
                urlString.equalsIgnoreCase(null);
            }
            url = conn.getURL().toString();
        } catch (IOException e) {
            Log.d(Constants.LOGGING_TAG, "Exception getting response code for redirection URL", e);
        }
        if (url == null || urlString.equalsIgnoreCase(url)) {
            url = conn.getHeaderField("location");
        }
        Map<String, List<String>> responseHeader = conn.getHeaderFields();
        if (responseHeader != null && responseHeader.containsKey("action-name") && (newAdActionList = responseHeader.get("action-name")) != null && newAdActionList.size() == 1) {
            setNewAdActionType(((String) newAdActionList.get(0)).trim());
        }
        return url;
    }

    private void storeCachedUrl(String cachedUrl, String cachedUrlLife) {
        long cachedUrlLifeNum;
        SharedPreferences.Editor editor = this.context.getSharedPreferences("InMobi_Prefs_key", 0).edit();
        if (cachedUrl != null) {
            editor.putString(Constants.AD_SERVER_CACHED_URL, cachedUrl);
        }
        if (cachedUrlLife != null) {
            try {
                cachedUrlLifeNum = Long.parseLong(cachedUrlLife) * 1000;
            } catch (NumberFormatException e) {
                cachedUrlLifeNum = Constants.CACHED_AD_SERVER_LIFE;
            }
            editor.putLong(Constants.AD_SERVER_CACHED_LIFE, cachedUrlLifeNum);
        } else {
            editor.putLong(Constants.AD_SERVER_CACHED_LIFE, Constants.CACHED_AD_SERVER_LIFE);
        }
        editor.putLong(Constants.CACHED_AD_SERVER_TIMESTAMP, Calendar.getInstance().getTimeInMillis());
        editor.commit();
    }

    public String getNewAdActionType() {
        return this.newAdActionType;
    }

    public void setNewAdActionType(String newAdActionType2) {
        this.newAdActionType = newAdActionType2;
    }
}
