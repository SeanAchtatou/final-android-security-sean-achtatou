package com.inmobi.androidsdk.impl.net;

public class ConnectionException extends Exception {
    public ConnectionException(String string) {
        super(string);
    }

    public ConnectionException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }
}
