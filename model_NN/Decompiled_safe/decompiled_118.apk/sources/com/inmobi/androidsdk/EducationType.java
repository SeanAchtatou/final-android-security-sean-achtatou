package com.inmobi.androidsdk;

public enum EducationType {
    Edu_None,
    Edu_HighSchool,
    Edu_SomeCollege,
    Edu_InCollege,
    Edu_BachelorsDegree,
    Edu_MastersDegree,
    Edu_DoctoralDegree,
    Edu_Other
}
