package com.inmobi.androidsdk;

import android.location.Location;
import com.inmobi.androidsdk.impl.InMobiAdView;
import java.util.Date;

public interface InMobiAdDelegate {
    public static final int INMOBI_AD_UNIT_120X600 = 13;
    public static final int INMOBI_AD_UNIT_300X250 = 10;
    public static final int INMOBI_AD_UNIT_320X48 = 9;
    public static final int INMOBI_AD_UNIT_468X60 = 12;
    public static final int INMOBI_AD_UNIT_728X90 = 11;
    public static final String INMOBI_INTERNAL_TAG = "ref-__in__rt";
    public static final String INMOBI_REF_TAG = "ref-tag";

    void adRequestCompleted(InMobiAdView inMobiAdView);

    void adRequestFailed(InMobiAdView inMobiAdView);

    int age();

    String areaCode();

    Location currentLocation();

    Date dateOfBirth();

    EducationType education();

    EthnicityType ethnicity();

    GenderType gender();

    int income();

    String interests();

    boolean isLocationInquiryAllowed();

    boolean isPublisherProvidingLocation();

    String keywords();

    String postalCode();

    String searchString();

    String siteId();

    boolean testMode();
}
