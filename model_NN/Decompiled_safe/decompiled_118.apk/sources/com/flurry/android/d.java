package com.flurry.android;

import com.boolbalabs.rollit.settings.Settings;
import java.lang.Thread;

final class d implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler a = Thread.getDefaultUncaughtExceptionHandler();

    d() {
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            FlurryAgent.b.a(th);
        } catch (Throwable th2) {
            Flog.b("FlurryAgent", Settings.FLURRY_ID_FULL, th2);
        }
        if (this.a != null) {
            this.a.uncaughtException(thread, th);
        }
    }
}
