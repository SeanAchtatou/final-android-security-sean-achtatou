package com.lu.ipd_3t;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.util.Random;

public class Inca_Pyramid_body extends Activity implements View.OnTouchListener {
    static final String HELP_STR = "The left (white) is always the first player. A click of left/right player on an empty grid will expend white/black piece(s) from left/right up to the clicked grid. The winner is the last player to fill the pyramid. [x P] - expend any # of pieces. [2 P] - only take up to 2 pieces (hinted grids)";
    int[] COL_X = new int[13];
    AssetManager m_assets = null;
    int m_cell_length;
    int m_corner_height;
    int m_corner_width;
    int m_max_cols;
    int m_max_pieces;
    int m_max_rows = 4;
    TextView m_move_mesg;
    PlayMaker m_play = new PlayMaker();
    String m_play_with;
    Random m_randomGenerator = new Random();
    Button m_undo = null;
    boolean m_use_col_x;
    int m_w_pixels;
    String m_which_move_first = "I";
    int m_which_set = 0;
    int move_c;
    int move_r;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(0);
        this.m_max_pieces = PlayMaker.MAX_PIECES;
        this.m_play_with = "phone";
        this.m_assets = getApplicationContext().getAssets();
        this.m_play.m_max_pieces = this.m_max_pieces;
        this.m_play.setGoodState_array(0);
        reset_layout();
    }

    public boolean onTouch(View view, MotionEvent me) {
        int ret;
        String s;
        String s2;
        if (me.getAction() == 1) {
            int end_x = (int) me.getX();
            int c = 0;
            int r = (((int) me.getY()) - this.m_corner_height) / this.m_cell_length;
            if (this.m_use_col_x) {
                int i = 0;
                while (true) {
                    if (i >= this.COL_X.length) {
                        break;
                    } else if (end_x < this.COL_X[i]) {
                        c = i;
                        break;
                    } else {
                        i++;
                    }
                }
                if (i == this.COL_X.length) {
                    c = i;
                }
            } else {
                c = (end_x - this.m_corner_width) / this.m_cell_length;
            }
            if (r < 0 || r > this.m_max_rows - 1) {
                return true;
            }
            if (c < 0 || c > this.m_max_cols - 1) {
                return true;
            }
            if (!this.m_play.m_board_cell[r][c].isMovable) {
                return true;
            }
            if (!this.m_play.m_board_cell[r][c].isHinted) {
                return true;
            }
            this.m_undo.setEnabled(true);
            if ("phone".equals(this.m_play_with)) {
                ret = this.m_play.make_play(r, c);
            } else {
                ret = this.m_play.make_a_play(r, c);
            }
            if (ret <= 0) {
                return true;
            }
            if (ret == 1 && this.m_play.m_isGameOver_1) {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
                if ("phone".equals(this.m_play_with)) {
                    s2 = "Congratulations, you won!";
                } else {
                    s2 = "Congratulations, left / white won!";
                }
                alertbox.setMessage(s2);
                alertbox.setNeutralButton("Have Fun!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                alertbox.show();
                this.m_undo.setEnabled(false);
                return true;
            } else if (ret == 2 && this.m_play.m_isGameOver_2) {
                AlertDialog.Builder alertbox2 = new AlertDialog.Builder(this);
                if ("phone".equals(this.m_play_with)) {
                    s = "Computer won.";
                } else {
                    s = "Congratulations, right / balck won!";
                }
                alertbox2.setMessage(s);
                alertbox2.setNeutralButton("Have Fun!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                alertbox2.show();
                this.m_undo.setEnabled(false);
                return true;
            } else if (!"phone".equals(this.m_play_with)) {
                if (this.m_play.m_next_move.equals("LEFT")) {
                    this.m_move_mesg.setText("White move");
                    this.m_move_mesg.setTextColor(-1);
                } else {
                    this.m_move_mesg.setText("Black move");
                    this.m_move_mesg.setTextColor(-16777216);
                }
            }
        }
        return true;
    }

    public void undo() {
        this.m_play.undo();
        this.m_undo.setEnabled(false);
    }

    public void reset_layout() {
        this.m_play.reset();
        this.m_play.m_max_pieces = this.m_max_pieces;
        this.m_play.setGoodState_array(this.m_which_set);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        this.m_max_rows = 5;
        this.m_max_cols = 13;
        this.m_play.m_max_rows = 5;
        this.m_play.m_max_cols = 13;
        int w = dm.widthPixels;
        int h = dm.heightPixels;
        if ((w != 800 || h != 480) && (w != 480 || h != 800)) {
            this.m_play.IMG_BLACK_HINT_ID = R.drawable.green_hint_black20;
            this.m_play.IMG_WHITE_HINT_ID = R.drawable.green_hint_white20;
            this.m_w_pixels = 320;
            setContentView(R.layout.main_body320);
            this.m_cell_length = 40;
            this.m_play.IMG_WHITE_ID = R.drawable.green_white22;
            this.m_play.IMG_BLACK_ID = R.drawable.green_black22;
            this.m_corner_width = 2;
            this.m_corner_height = 2;
            this.COL_X[0] = 20;
            for (int i = 1; i < 13; i++) {
                this.COL_X[i] = this.COL_X[i - 1] + 40;
            }
            this.m_use_col_x = true;
        } else if (dm.densityDpi < 200) {
            this.m_play.IMG_BLACK_HINT_ID = R.drawable.black_hint_38;
            this.m_play.IMG_WHITE_HINT_ID = R.drawable.white_hint_38;
            this.m_w_pixels = 480;
            this.m_cell_length = 60;
            setContentView(R.layout.main_body480);
            this.m_play.IMG_WHITE_ID = R.drawable.green_white39;
            this.m_play.IMG_BLACK_ID = R.drawable.green_black39;
            this.m_corner_width = 0;
            this.m_corner_height = 0;
            this.m_use_col_x = false;
        } else {
            this.m_play.IMG_BLACK_HINT_ID = R.drawable.black_hint_30;
            this.m_play.IMG_WHITE_HINT_ID = R.drawable.white_hint_30;
            this.m_w_pixels = 480;
            this.m_cell_length = 60;
            setContentView(R.layout.main_body320);
            this.m_play.IMG_WHITE_ID = R.drawable.green_white22;
            this.m_play.IMG_BLACK_ID = R.drawable.green_black22;
            this.m_corner_width = 0;
            this.m_corner_height = 0;
            this.COL_X[0] = 30;
            for (int i2 = 1; i2 < 13; i2++) {
                this.COL_X[i2] = this.COL_X[i2 - 1] + 60;
            }
            this.m_use_col_x = true;
        }
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        this.m_play.IMG_EMPTY_ID = R.drawable.empty;
        ((RelativeLayout) findViewById(R.id.layout_game)).setOnTouchListener(this);
        this.m_move_mesg = (TextView) findViewById(R.id.move_mesg);
        Button bt_play_computer = (Button) findViewById(R.id.play_computer);
        bt_play_computer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Inca_Pyramid_body.this.m_play_with = "phone";
                Inca_Pyramid_body.this.reset_layout();
            }
        });
        Button bt_two_players = (Button) findViewById(R.id.two_players);
        bt_two_players.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Inca_Pyramid_body.this.m_play_with = "friend";
                Inca_Pyramid_body.this.reset_layout();
            }
        });
        if (this.m_play_with.equals("phone")) {
            bt_play_computer.setEnabled(false);
        } else {
            bt_two_players.setEnabled(false);
        }
        ((Button) findViewById(R.id.restart)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Inca_Pyramid_body.this.reset_layout();
            }
        });
        ((Button) findViewById(R.id.help)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder helpbox = new AlertDialog.Builder(Inca_Pyramid_body.this);
                helpbox.setMessage(Inca_Pyramid_body.HELP_STR);
                helpbox.setPositiveButton("Have fun", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
                helpbox.show();
            }
        });
        this.m_undo = (Button) findViewById(R.id.undo);
        this.m_undo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if ("phone".equals(Inca_Pyramid_body.this.m_play_with)) {
                    Inca_Pyramid_body.this.m_play.undo();
                } else if (Inca_Pyramid_body.this.m_play.undo_a_play().equals("LEFT")) {
                    Inca_Pyramid_body.this.m_move_mesg.setText("White move");
                    Inca_Pyramid_body.this.m_move_mesg.setTextColor(-1);
                } else {
                    Inca_Pyramid_body.this.m_move_mesg.setText("Black move");
                    Inca_Pyramid_body.this.m_move_mesg.setTextColor(-16777216);
                }
                Inca_Pyramid_body.this.m_undo.setEnabled(false);
            }
        });
        this.m_undo.setEnabled(false);
        try {
            read_in_data(this.m_max_rows);
        } catch (Exception e) {
            Log.d("MAIN", e.toString());
        }
        this.m_play.remove_all_hint();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if ("phone".equals(this.m_play_with)) {
            this.m_move_mesg.setText("");
            builder.setMessage("Who makes move first, you or the computer? The first move always at left side with white.");
            builder.setPositiveButton("I", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    PlayMaker playMaker = Inca_Pyramid_body.this.m_play;
                    Inca_Pyramid_body.this.m_which_move_first = "I";
                    playMaker.m_which_move_first = "I";
                    Inca_Pyramid_body.this.m_play.m_next_move = "LEFT";
                    Inca_Pyramid_body.this.m_play.mark_move_hint();
                    Toast viewToast = Toast.makeText(Inca_Pyramid_body.this, "You are white moving first", 0);
                    viewToast.setGravity(51, 20, 140);
                    viewToast.show();
                }
            });
            builder.setNegativeButton("Computer", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    Toast viewToast = Toast.makeText(Inca_Pyramid_body.this, "You are black moving second", 0);
                    viewToast.setGravity(51, 200, 140);
                    viewToast.show();
                    PlayMaker playMaker = Inca_Pyramid_body.this.m_play;
                    Inca_Pyramid_body.this.m_which_move_first = "C";
                    playMaker.m_which_move_first = "C";
                    Inca_Pyramid_body.this.m_play.computer_first_move();
                }
            });
        } else {
            this.m_move_mesg.setText("White move");
            this.m_move_mesg.setTextColor(-1);
            this.m_play.m_next_move = "LEFT";
            this.m_play.mark_move_hint();
            builder.setMessage("The first player is always at left side with white color.");
            builder.setPositiveButton("Have fun", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            });
        }
        builder.show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int read_in_data(int r19) throws java.lang.Exception {
        /*
            r18 = this;
            java.lang.String r5 = "3t.txt"
            r7 = 0
            r0 = r18
            android.content.res.AssetManager r0 = r0.m_assets     // Catch:{ Exception -> 0x0092 }
            r14 = r0
            java.io.InputStream r9 = r14.open(r5)     // Catch:{ Exception -> 0x0092 }
            java.io.DataInputStream r8 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0092 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0092 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            java.io.InputStreamReader r14 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r14.<init>(r8)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r2.<init>(r14)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r10 = 0
            r12 = 0
            java.lang.String[] r12 = (java.lang.String[]) r12     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
        L_0x001f:
            java.lang.String r13 = r2.readLine()     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            if (r13 != 0) goto L_0x002d
        L_0x0025:
            if (r8 == 0) goto L_0x002a
            r8.close()
        L_0x002a:
            r14 = 1
            r7 = r8
        L_0x002c:
            return r14
        L_0x002d:
            r0 = r10
            r1 = r19
            if (r0 > r1) goto L_0x0025
            java.lang.String r13 = r13.trim()     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            int r14 = r13.length()     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r15 = 1
            if (r14 < r15) goto L_0x001f
            java.lang.String r14 = ","
            java.lang.String[] r12 = r13.split(r14)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            if (r12 == 0) goto L_0x001f
            int r14 = r12.length     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            if (r14 < 0) goto L_0x001f
            r6 = 0
        L_0x0049:
            int r14 = r12.length     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            if (r6 < r14) goto L_0x004f
            int r10 = r10 + 1
            goto L_0x001f
        L_0x004f:
            r14 = r12[r6]     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            java.lang.String r14 = r14.trim()     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            int r3 = java.lang.Integer.parseInt(r14)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            android.content.res.Resources r14 = r18.getResources()     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            java.lang.String r16 = "c"
            r15.<init>(r16)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            java.lang.StringBuilder r15 = r15.append(r10)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            java.lang.StringBuilder r15 = r15.append(r3)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            java.lang.String r16 = "id"
            java.lang.String r17 = "com.lu.ipd_3t"
            int r11 = r14.getIdentifier(r15, r16, r17)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r0 = r18
            com.lu.ipd_3t.PlayMaker r0 = r0.m_play     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r14 = r0
            com.lu.ipd_3t.Cell[][] r14 = r14.m_board_cell     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r14 = r14[r10]     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r14 = r14[r3]     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r0 = r18
            r1 = r11
            android.view.View r3 = r0.findViewById(r1)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            android.widget.ImageView r3 = (android.widget.ImageView) r3     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            r14.setM_iv(r3)     // Catch:{ Exception -> 0x00a8, all -> 0x00a5 }
            int r6 = r6 + 1
            goto L_0x0049
        L_0x0092:
            r14 = move-exception
            r4 = r14
        L_0x0094:
            r4.printStackTrace()     // Catch:{ all -> 0x009e }
            if (r7 == 0) goto L_0x009c
            r7.close()
        L_0x009c:
            r14 = 0
            goto L_0x002c
        L_0x009e:
            r14 = move-exception
        L_0x009f:
            if (r7 == 0) goto L_0x00a4
            r7.close()
        L_0x00a4:
            throw r14
        L_0x00a5:
            r14 = move-exception
            r7 = r8
            goto L_0x009f
        L_0x00a8:
            r14 = move-exception
            r4 = r14
            r7 = r8
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lu.ipd_3t.Inca_Pyramid_body.read_in_data(int):int");
    }
}
