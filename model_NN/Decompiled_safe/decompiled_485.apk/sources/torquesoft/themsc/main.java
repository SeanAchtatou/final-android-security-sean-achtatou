package torquesoft.themsc;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.B4AMenuItem;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.Msgbox;
import anywheresoftware.b4a.admobwrapper.AdViewWrapper;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.keywords.DateTime;
import anywheresoftware.b4a.keywords.LayoutValues;
import anywheresoftware.b4a.keywords.constants.Colors;
import anywheresoftware.b4a.keywords.constants.DialogResponse;
import anywheresoftware.b4a.keywords.constants.Gravity;
import anywheresoftware.b4a.keywords.constants.KeyCodes;
import anywheresoftware.b4a.objects.ActivityWrapper;
import anywheresoftware.b4a.objects.ButtonWrapper;
import anywheresoftware.b4a.objects.CompoundButtonWrapper;
import anywheresoftware.b4a.objects.ConcreteViewWrapper;
import anywheresoftware.b4a.objects.LabelWrapper;
import anywheresoftware.b4a.objects.ListViewWrapper;
import anywheresoftware.b4a.objects.PanelWrapper;
import anywheresoftware.b4a.objects.ScrollViewWrapper;
import anywheresoftware.b4a.objects.TabHostWrapper;
import anywheresoftware.b4a.objects.ViewWrapper;
import anywheresoftware.b4a.objects.WebViewWrapper;
import anywheresoftware.b4a.objects.collections.List;
import anywheresoftware.b4a.objects.drawable.CanvasWrapper;
import anywheresoftware.b4a.objects.streams.File;
import anywheresoftware.b4a.phone.PackageManagerWrapper;
import anywheresoftware.b4a.phone.Phone;
import anywheresoftware.b4a.sql.SQL;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class main extends Activity implements B4AActivity {
    public static String _db_ver = "";
    public static boolean _exitorreturn = false;
    public static int _inc = 0;
    public static int _pos = 0;
    public static int _scrollpos = 0;
    public static boolean _showtoast = false;
    public static int _speed = 0;
    public static SQL _sql1 = null;
    public static boolean _v_display_notifications = false;
    public static boolean _v_tablet = false;
    public static String _v_today_date = "";
    public static String _version = "";
    static boolean afterFirstLayout;
    private static final boolean fullScreen = false;
    private static final boolean includeTitle = false;
    static boolean isFirst = true;
    static main mostCurrent;
    public static WeakReference<Activity> previousOne;
    public static BA processBA;
    private static boolean processGlobalsRun = false;
    public Common __c = null;
    public PanelWrapper _about_panel = null;
    ActivityWrapper _activity;
    public AdViewWrapper _adview1 = null;
    public CanvasWrapper.BitmapWrapper _b1 = null;
    public ButtonWrapper _btnnext1 = null;
    public CanvasWrapper _can1 = null;
    public SQL.CursorWrapper _cursor = null;
    public LabelWrapper _label1 = null;
    public ListViewWrapper _listview2 = null;
    public ListViewWrapper _listview3 = null;
    public ListViewWrapper _listview4 = null;
    public ListViewWrapper _listviewsavefavs = null;
    public LayoutValues _lv = null;
    public PanelWrapper _options_panel = null;
    public ButtonWrapper _pb_about = null;
    public ButtonWrapper _pb_close_about_panel = null;
    public CompoundButtonWrapper.ToggleButtonWrapper _pb_showfavsfirst = null;
    public CanvasWrapper.RectWrapper _r = null;
    public ButtonWrapper _removefavourites = null;
    public ScrollViewWrapper _scrollview1 = null;
    public ScrollViewWrapper _scrollview2 = null;
    public List _selecteditems = null;
    public CanvasWrapper.RectWrapper _srcrect = null;
    public statemanager _statemanager = null;
    public TabHostWrapper _tabhost1 = null;
    public LabelWrapper _twitterlabel = null;
    public LabelWrapper _version_label = null;
    public LabelWrapper _versionlabel = null;
    public LabelWrapper _versiontextlabel = null;
    public LabelWrapper _web_label = null;
    public WebViewWrapper _webview1 = null;
    BA activityBA;
    BALayout layout;
    ArrayList<B4AMenuItem> menuItems;
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;

    public void onCreate(Bundle bundle) {
        Activity activity;
        super.onCreate(bundle);
        if (isFirst) {
            processBA = new BA(getApplicationContext(), null, null, "torquesoft.themsc", "main");
            processBA.loadHtSubs(getClass());
            BALayout.setDeviceScale(getApplicationContext().getResources().getDisplayMetrics().density);
        } else if (!(previousOne == null || (activity = previousOne.get()) == null || activity == this)) {
            Common.Log("Killing previous instance (main).");
            activity.finish();
        }
        getWindow().requestFeature(1);
        mostCurrent = this;
        processBA.activityBA = null;
        this.layout = new BALayout(this);
        setContentView(this.layout);
        afterFirstLayout = false;
        BA.handler.postDelayed(new WaitForLayout(), 5);
    }

    private static class WaitForLayout implements Runnable {
        private WaitForLayout() {
        }

        public void run() {
            if (!main.afterFirstLayout) {
                if (main.mostCurrent.layout.getWidth() == 0) {
                    BA.handler.postDelayed(this, 5);
                    return;
                }
                main.mostCurrent.layout.getLayoutParams().height = main.mostCurrent.layout.getHeight();
                main.mostCurrent.layout.getLayoutParams().width = main.mostCurrent.layout.getWidth();
                main.afterFirstLayout = true;
                main.mostCurrent.afterFirstLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    public void afterFirstLayout() {
        this.activityBA = new BA(this, this.layout, processBA, "torquesoft.themsc", "main");
        processBA.activityBA = new WeakReference<>(this.activityBA);
        this._activity = new ActivityWrapper(this.activityBA, "activity");
        Msgbox.isDismissing = false;
        initializeProcessGlobals();
        initializeGlobals();
        ViewWrapper.lastId = 0;
        Common.Log("** Activity (main) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, Boolean.valueOf(isFirst));
        isFirst = false;
        if (mostCurrent != null && mostCurrent == this) {
            processBA.setActivityPaused(false);
            Common.Log("** Activity (main) Resume **");
            processBA.raiseEvent(null, "activity_resume", new Object[0]);
        }
    }

    public void addMenuItem(B4AMenuItem b4AMenuItem) {
        if (this.menuItems == null) {
            this.menuItems = new ArrayList<>();
        }
        this.menuItems.add(b4AMenuItem);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.menuItems == null) {
            return false;
        }
        Iterator<B4AMenuItem> it = this.menuItems.iterator();
        while (it.hasNext()) {
            B4AMenuItem next = it.next();
            MenuItem add = menu.add(next.title);
            if (next.drawable != null) {
                add.setIcon(next.drawable);
            }
            add.setOnMenuItemClickListener(new B4AMenuItemsClickListener(next.eventName.toLowerCase(BA.cul)));
        }
        return true;
    }

    private class B4AMenuItemsClickListener implements MenuItem.OnMenuItemClickListener {
        private final String eventName;

        public B4AMenuItemsClickListener(String str) {
            this.eventName = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            main.processBA.raiseEvent(menuItem.getTitle(), this.eventName + "_click", new Object[0]);
            return true;
        }
    }

    public static Class<?> getObject() {
        return main.class;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.onKeySubExist == null) {
            this.onKeySubExist = Boolean.valueOf(processBA.subExists("activity_keypress"));
        }
        if (this.onKeySubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keypress", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.onKeyUpSubExist == null) {
            this.onKeyUpSubExist = Boolean.valueOf(processBA.subExists("activity_keyup"));
        }
        if (this.onKeyUpSubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keyup", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    public void onPause() {
        super.onPause();
        if (this._activity != null) {
            Msgbox.dismiss(true);
            Common.Log("** Activity (main) Pause, UserClosed = " + this.activityBA.activity.isFinishing() + " **");
            processBA.raiseEvent2(this._activity, true, "activity_pause", false, Boolean.valueOf(this.activityBA.activity.isFinishing()));
            processBA.setActivityPaused(true);
            mostCurrent = null;
            if (!this.activityBA.activity.isFinishing()) {
                previousOne = new WeakReference<>(this);
            }
            Msgbox.isDismissing = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        previousOne = null;
    }

    public void onResume() {
        super.onResume();
        mostCurrent = this;
        Msgbox.isDismissing = false;
        if (this.activityBA != null) {
            BA.handler.post(new ResumeMessage(mostCurrent));
        }
    }

    private static class ResumeMessage implements Runnable {
        private final WeakReference<Activity> activity;

        public ResumeMessage(Activity activity2) {
            this.activity = new WeakReference<>(activity2);
        }

        public void run() {
            if (main.mostCurrent != null && main.mostCurrent == this.activity.get()) {
                main.processBA.setActivityPaused(false);
                Common.Log("** Activity (main) Resume **");
                main.processBA.raiseEvent(main.mostCurrent._activity, "activity_resume", null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        processBA.onActivityResult(i, i2, intent);
    }

    private static void initializeGlobals() {
        processBA.raiseEvent2(null, true, "globals", false, null);
    }

    public static class _rowcol {
        int Col;
        boolean IsInitialized;
        int Row;

        public void Initialize() {
            this.IsInitialized = true;
            this.Row = 0;
            this.Col = 0;
        }

        public String toString() {
            return BA.TypeToString(this);
        }
    }

    public static String _activity_create(boolean z) throws Exception {
        _exitorreturn = true;
        _showtoast = true;
        mostCurrent._options_panel.Initialize(mostCurrent.activityBA, "Options_Panel");
        mostCurrent._options_panel.setVisible(true);
        mostCurrent._options_panel.setEnabled(true);
        mostCurrent._options_panel.BringToFront();
        mostCurrent._about_panel.Initialize(mostCurrent.activityBA, "About_Panel");
        mostCurrent._about_panel.setVisible(false);
        mostCurrent._about_panel.SendToBack();
        new Phone();
        int i = Common.GetDeviceLayoutValues(mostCurrent.activityBA).Height;
        _v_tablet = false;
        if (i > 800) {
            Phone.SetScreenOrientation(processBA, -1);
            _v_tablet = true;
        } else if (i == 800 && BA.NumberToString((double) Common.GetDeviceLayoutValues(mostCurrent.activityBA).Scale).equals("1.0")) {
            Phone.SetScreenOrientation(processBA, -1);
            _v_tablet = true;
        }
        mostCurrent._activity.setTitle("Motorsport Calendar Free");
        mostCurrent._activity.LoadLayout("Main", mostCurrent.activityBA);
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        File file = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap = Common.LoadBitmap(File.getDirAssets(), "all.png");
        File file2 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap2 = Common.LoadBitmap(File.getDirAssets(), "all_select.png");
        File file3 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap3 = Common.LoadBitmap(File.getDirAssets(), "champ.png");
        File file4 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap4 = Common.LoadBitmap(File.getDirAssets(), "champ_select.png");
        File file5 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap5 = Common.LoadBitmap(File.getDirAssets(), "fav.png");
        File file6 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap6 = Common.LoadBitmap(File.getDirAssets(), "fav_select.png");
        File file7 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap7 = Common.LoadBitmap(File.getDirAssets(), "settings.png");
        File file8 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap8 = Common.LoadBitmap(File.getDirAssets(), "settings_select.png");
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        new CanvasWrapper.BitmapWrapper();
        File file9 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap9 = Common.LoadBitmap(File.getDirAssets(), "logo_alms.png");
        File file10 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap10 = Common.LoadBitmap(File.getDirAssets(), "logo_aus_v8.png");
        File file11 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap11 = Common.LoadBitmap(File.getDirAssets(), "logo_btcc.png");
        File file12 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap12 = Common.LoadBitmap(File.getDirAssets(), "logo_britcar.png");
        File file13 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap13 = Common.LoadBitmap(File.getDirAssets(), "logo_f3.png");
        File file14 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap14 = Common.LoadBitmap(File.getDirAssets(), "logo_brit_renault.png");
        File file15 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap15 = Common.LoadBitmap(File.getDirAssets(), "logo_bgt.png");
        File file16 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap16 = Common.LoadBitmap(File.getDirAssets(), "logo_bsb.png");
        File file17 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap17 = Common.LoadBitmap(File.getDirAssets(), "logo_dtm.png");
        File file18 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap18 = Common.LoadBitmap(File.getDirAssets(), "logo_dakar.png");
        File file19 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap19 = Common.LoadBitmap(File.getDirAssets(), "logo_f3_euro.png");
        File file20 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap20 = Common.LoadBitmap(File.getDirAssets(), "logo_fia_gt1.png");
        File file21 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap21 = Common.LoadBitmap(File.getDirAssets(), "logo_f1.png");
        File file22 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap22 = Common.LoadBitmap(File.getDirAssets(), "logo_f2.png");
        File file23 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap23 = Common.LoadBitmap(File.getDirAssets(), "logo_bmw.png");
        File file24 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap24 = Common.LoadBitmap(File.getDirAssets(), "logo_f_nippon.png");
        File file25 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap25 = Common.LoadBitmap(File.getDirAssets(), "logo_gp2.png");
        File file26 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap26 = Common.LoadBitmap(File.getDirAssets(), "logo_gp2_asia.png");
        File file27 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap27 = Common.LoadBitmap(File.getDirAssets(), "logo_grand_am.png");
        File file28 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap28 = Common.LoadBitmap(File.getDirAssets(), "logo_indycar.png");
        File file29 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap29 = Common.LoadBitmap(File.getDirAssets(), "logo_ilmc.png");
        File file30 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap30 = Common.LoadBitmap(File.getDirAssets(), "logo_lm_series.png");
        File file31 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap31 = Common.LoadBitmap(File.getDirAssets(), "logo_motogp.png");
        File file32 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap32 = Common.LoadBitmap(File.getDirAssets(), "logo_nascar.png");
        File file33 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap33 = Common.LoadBitmap(File.getDirAssets(), "logo_porsche.png");
        File file34 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap34 = Common.LoadBitmap(File.getDirAssets(), "logo_24_lemans.png");
        File file35 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap35 = Common.LoadBitmap(File.getDirAssets(), "logo_supergt.png");
        File file36 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap36 = Common.LoadBitmap(File.getDirAssets(), "logo_wrc.png");
        File file37 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap37 = Common.LoadBitmap(File.getDirAssets(), "logo_ws_renault.png");
        File file38 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap38 = Common.LoadBitmap(File.getDirAssets(), "logo_sbk.png");
        File file39 = Common.File;
        CanvasWrapper.BitmapWrapper LoadBitmap39 = Common.LoadBitmap(File.getDirAssets(), "logo_wtcc.png");
        PackageManagerWrapper packageManagerWrapper = new PackageManagerWrapper();
        mostCurrent._versionlabel.Initialize(mostCurrent.activityBA, "VersionLabel");
        mostCurrent._versionlabel.setText(packageManagerWrapper.GetVersionName("torquesoft.themsc"));
        main main = mostCurrent;
        _db_ver = "0.9.1";
        File file40 = Common.File;
        File file41 = Common.File;
        if (!File.Exists(File.getDirInternal(), "themsc.db")) {
            File file42 = Common.File;
            File file43 = Common.File;
            String dirAssets = File.getDirAssets();
            File file44 = Common.File;
            File.Copy(dirAssets, "themsc.db", File.getDirInternal(), "themsc.db");
        } else {
            String text = mostCurrent._versionlabel.getText();
            main main2 = mostCurrent;
            if (!text.equals(_db_ver)) {
                mostCurrent._listviewsavefavs.Initialize(mostCurrent.activityBA, "ListViewSaveFavs");
                if (!_sql1.IsInitialized()) {
                    SQL sql = _sql1;
                    File file45 = Common.File;
                    sql.Initialize(File.getDirInternal(), "themsc.db", true);
                }
                mostCurrent._cursor.setObject(_sql1.ExecQuery("SELECT fk_event_id FROM favourite"));
                double rowCount = (double) (mostCurrent._cursor.getRowCount() - 1);
                int i2 = 0;
                for (int i3 = 0; ((double) i3) <= rowCount; i3 = (int) (((double) i3) + 1.0d)) {
                    mostCurrent._cursor.setPosition(i3);
                    mostCurrent._listviewsavefavs.AddSingleLine(BA.NumberToString(mostCurrent._cursor.GetInt("fk_event_id")));
                    i2++;
                }
                File file46 = Common.File;
                File file47 = Common.File;
                File.Delete(File.getDirInternal(), "themsc.db");
                File file48 = Common.File;
                File file49 = Common.File;
                String dirAssets2 = File.getDirAssets();
                File file50 = Common.File;
                File.Copy(dirAssets2, "themsc.db", File.getDirInternal(), "themsc.db");
                SQL sql2 = _sql1;
                File file51 = Common.File;
                sql2.Initialize(File.getDirInternal(), "themsc.db", true);
                double d = (double) (i2 - 1);
                for (int i4 = 0; ((double) i4) <= d; i4 = (int) (((double) i4) + 1.0d)) {
                    _sql1.ExecNonQuery2("INSERT INTO favourite VALUES (?,?)", Common.ArrayToList(new Object[]{Integer.valueOf(i4), mostCurrent._listviewsavefavs.GetItem(i4)}));
                }
            }
        }
        if (!_sql1.IsInitialized()) {
            SQL sql3 = _sql1;
            File file52 = Common.File;
            sql3.Initialize(File.getDirInternal(), "themsc.db", true);
        }
        mostCurrent._tabhost1.AddTabWithIcon(mostCurrent.activityBA, "All", (Bitmap) LoadBitmap.getObject(), (Bitmap) LoadBitmap2.getObject(), "page1");
        mostCurrent._tabhost1.AddTabWithIcon(mostCurrent.activityBA, "Series", (Bitmap) LoadBitmap3.getObject(), (Bitmap) LoadBitmap4.getObject(), "page2");
        mostCurrent._tabhost1.AddTabWithIcon(mostCurrent.activityBA, "Favourites", (Bitmap) LoadBitmap5.getObject(), (Bitmap) LoadBitmap6.getObject(), "page3");
        mostCurrent._tabhost1.AddTabWithIcon(mostCurrent.activityBA, "Options", (Bitmap) LoadBitmap7.getObject(), (Bitmap) LoadBitmap8.getObject(), "page4");
        new LabelWrapper();
        new LabelWrapper();
        String NumberToString = BA.NumberToString(Common.DipToCurrent(20));
        String NumberToString2 = BA.NumberToString(-Common.DipToCurrent(19));
        main main3 = mostCurrent;
        DateTime dateTime = Common.DateTime;
        DateTime dateTime2 = Common.DateTime;
        _v_today_date = DateTime.Date(DateTime.getNow());
        _sql1.BeginTransaction();
        mostCurrent._scrollview1.getPanel().setHeight(((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(*) FROM event"))) * Common.DipToCurrent(41));
        mostCurrent._cursor.setObject(_sql1.ExecQuery("SELECT _id, name, enddate, display_date FROM event order by startdate,enddate"));
        double rowCount2 = (double) (mostCurrent._cursor.getRowCount() - 1);
        int i5 = 0;
        while (((double) i5) <= rowCount2) {
            mostCurrent._cursor.setPosition(i5);
            LabelWrapper labelWrapper = new LabelWrapper();
            LabelWrapper labelWrapper2 = new LabelWrapper();
            String NumberToString3 = BA.NumberToString(Double.parseDouble(NumberToString2) + ((double) Common.DipToCurrent(21)));
            labelWrapper.Initialize(mostCurrent.activityBA, "lbl1");
            Colors colors = Common.Colors;
            labelWrapper.setColor(Colors.Black);
            int GetInt = mostCurrent._cursor.GetInt("_id");
            labelWrapper.setTag(Integer.valueOf(GetInt));
            labelWrapper.setTextSize(14.0f);
            mostCurrent._scrollview1.getPanel().AddView((View) labelWrapper.getObject(), 0, (int) Double.parseDouble(NumberToString3), Common.PerXToCurrent(100.0f, mostCurrent.activityBA), (int) Double.parseDouble(NumberToString));
            String NumberToString4 = BA.NumberToString(Double.parseDouble(NumberToString3) + ((double) Common.DipToCurrent(20)));
            labelWrapper2.Initialize(mostCurrent.activityBA, "lbl2");
            Colors colors2 = Common.Colors;
            labelWrapper2.setColor(Colors.Black);
            labelWrapper2.setTag(Integer.valueOf(GetInt));
            labelWrapper2.setTextSize(11.0f);
            mostCurrent._scrollview1.getPanel().AddView((View) labelWrapper2.getObject(), 0, (int) Double.parseDouble(NumberToString4), Common.PerXToCurrent(100.0f, mostCurrent.activityBA), (int) Double.parseDouble(NumberToString));
            String GetString = mostCurrent._cursor.GetString("enddate");
            DateTime dateTime3 = Common.DateTime;
            long DateParse = DateTime.DateParse(GetString);
            DateTime dateTime4 = Common.DateTime;
            main main4 = mostCurrent;
            if (DateParse < DateTime.DateParse(_v_today_date)) {
                Colors colors3 = Common.Colors;
                labelWrapper.setTextColor(Colors.Gray);
                Colors colors4 = Common.Colors;
                labelWrapper2.setTextColor(Colors.Gray);
                labelWrapper.setText(mostCurrent._cursor.GetString("name"));
                labelWrapper2.setText(mostCurrent._cursor.GetString("display_date"));
            } else {
                Colors colors5 = Common.Colors;
                labelWrapper.setTextColor(-1);
                Colors colors6 = Common.Colors;
                labelWrapper2.setTextColor(Colors.Gray);
                labelWrapper.setText(mostCurrent._cursor.GetString("name"));
                labelWrapper2.setText(mostCurrent._cursor.GetString("display_date"));
            }
            i5 = (int) (((double) i5) + 1.0d);
            NumberToString2 = NumberToString4;
        }
        _sql1.TransactionSuccessful();
        _sql1.EndTransaction();
        Common.DoEvents();
        mostCurrent._scrollview1.setScrollPosition(Common.DipToCurrent((int) (Double.parseDouble(_sql1.ExecQuerySingleResult("select count(_id) from event where enddate < date('now')")) * 40.5d)));
        mostCurrent._cursor.Close();
        SQL.CursorWrapper cursorWrapper = new SQL.CursorWrapper();
        mostCurrent._listview2.getTwoLinesLayout().Label.setTextSize(14.0f);
        Gravity gravity = Common.Gravity;
        mostCurrent._listview2.getTwoLinesLayout().Label.setGravity(0);
        mostCurrent._listview2.getTwoLinesLayout().Label.setHeight(Common.DipToCurrent(22));
        Colors colors7 = Common.Colors;
        mostCurrent._listview2.setScrollingBackgroundColor(0);
        mostCurrent._listview2.getTwoLinesLayout().setItemHeight(Common.DipToCurrent(22));
        cursorWrapper.setObject(_sql1.ExecQuery("SELECT distinct series FROM event order by series"));
        double rowCount3 = (double) (cursorWrapper.getRowCount() - 1);
        int i6 = 0;
        while (true) {
            int i7 = i6;
            if (((double) i7) > rowCount3) {
                break;
            }
            cursorWrapper.setPosition(i7);
            if (i7 == 0) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap9.getObject());
            } else if (i7 == 1) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap10.getObject());
            } else if (i7 == 2) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap11.getObject());
            } else if (i7 == 3) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap12.getObject());
            } else if (i7 == 4) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap13.getObject());
            } else if (i7 == 5) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap14.getObject());
            } else if (i7 == 6) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap15.getObject());
            } else if (i7 == 7) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap16.getObject());
            } else if (i7 == 8) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap17.getObject());
            } else if (i7 == 9) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap18.getObject());
            } else if (i7 == 10) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap19.getObject());
            } else if (i7 == 11) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap20.getObject());
            } else if (i7 == 12) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap21.getObject());
            } else if (i7 == 13) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap22.getObject());
            } else if (i7 == 14) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap23.getObject());
            } else if (i7 == 15) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap24.getObject());
            } else if (i7 == 16) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap25.getObject());
            } else if (i7 == 17) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap26.getObject());
            } else if (i7 == 18) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap27.getObject());
            } else if (i7 == 19) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap28.getObject());
            } else if (i7 == 20) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap29.getObject());
            } else if (i7 == 21) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap30.getObject());
            } else if (i7 == 22) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap31.getObject());
            } else if (i7 == 23) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap32.getObject());
            } else if (i7 == 24) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap33.getObject());
            } else if (i7 == 25) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap34.getObject());
            } else if (i7 == 26) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap35.getObject());
            } else if (i7 == 27) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap36.getObject());
            } else if (i7 == 28) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap37.getObject());
            } else if (i7 == 29) {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap38.getObject());
            } else {
                mostCurrent._listview2.AddTwoLinesAndBitmap(cursorWrapper.GetString("series"), "", (Bitmap) LoadBitmap39.getObject());
            }
            i6 = (int) (((double) i7) + 1.0d);
        }
        cursorWrapper.Close();
        mostCurrent._listview3.getTwoLinesLayout().Label.setTextSize(14.0f);
        mostCurrent._listview3.getTwoLinesLayout().SecondLabel.setTextSize(11.0f);
        Gravity gravity2 = Common.Gravity;
        mostCurrent._listview3.getTwoLinesLayout().Label.setGravity(Gravity.FILL);
        Gravity gravity3 = Common.Gravity;
        mostCurrent._listview3.getTwoLinesLayout().SecondLabel.setGravity(Gravity.FILL);
        mostCurrent._listview3.getTwoLinesLayout().SecondLabel.setTop(Common.DipToCurrent(19));
        mostCurrent._listview3.getTwoLinesLayout().Label.setHeight(Common.DipToCurrent(19));
        mostCurrent._listview3.getTwoLinesLayout().SecondLabel.setHeight(Common.DipToCurrent(19));
        Colors colors8 = Common.Colors;
        mostCurrent._listview3.setScrollingBackgroundColor(0);
        mostCurrent._listview3.getTwoLinesLayout().setItemHeight(Common.DipToCurrent(40));
        SQL.CursorWrapper cursorWrapper2 = new SQL.CursorWrapper();
        cursorWrapper2.setObject(_sql1.ExecQuery("SELECT name, startdate, display_date, event._id FROM event, favourite where event._id = favourite.fk_event_id order by date(startdate)"));
        double rowCount4 = (double) (cursorWrapper2.getRowCount() - 1);
        for (int i8 = 0; ((double) i8) <= rowCount4; i8 = (int) (((double) i8) + 1.0d)) {
            cursorWrapper2.setPosition(i8);
            mostCurrent._listview3.AddTwoLines2(cursorWrapper2.GetString("name"), cursorWrapper2.GetString("display_date"), Integer.valueOf(cursorWrapper2.GetInt("_id")));
        }
        cursorWrapper2.Close();
        if (_showtoast) {
            Common.ToastMessageShow("Single Click on items to see more information, Long Click on items to add them to Favourites", true);
        }
        statemanager statemanager = mostCurrent._statemanager;
        statemanager._restorestate(mostCurrent.activityBA, mostCurrent._activity, "main", 60);
        mostCurrent._adview1.Initialize(mostCurrent.activityBA, "Ad", "a14df91ed63aa80");
        mostCurrent._activity.AddView((View) mostCurrent._adview1.getObject(), Common.DipToCurrent(0), mostCurrent._activity.getHeight() - Common.DipToCurrent(50), Common.DipToCurrent(320), Common.DipToCurrent(50));
        mostCurrent._adview1.LoadAd();
        return "";
    }

    public static boolean _activity_keypress(int i) throws Exception {
        if (!_exitorreturn) {
            KeyCodes keyCodes = Common.KeyCodes;
            if (i == 4) {
                mostCurrent._webview1.setVisible(false);
                mostCurrent._webview1.Invalidate();
                mostCurrent._listview2.BringToFront();
                mostCurrent._listview2.setVisible(true);
                mostCurrent._about_panel.setVisible(true);
                _exitorreturn = true;
                return true;
            }
        }
        return false;
    }

    public static String _activity_pause(boolean z) throws Exception {
        statemanager statemanager = mostCurrent._statemanager;
        statemanager._savestate(mostCurrent.activityBA, mostCurrent._activity, "main");
        statemanager statemanager2 = mostCurrent._statemanager;
        statemanager._savesettings(mostCurrent.activityBA);
        statemanager statemanager3 = mostCurrent._statemanager;
        statemanager._savestate(mostCurrent.activityBA, mostCurrent._activity, "page4");
        statemanager statemanager4 = mostCurrent._statemanager;
        statemanager._savesettings(mostCurrent.activityBA);
        Common.Log("Activity_Pause");
        return "";
    }

    public static String _activity_resume() throws Exception {
        statemanager statemanager = mostCurrent._statemanager;
        if (!statemanager._restorestate(mostCurrent.activityBA, mostCurrent._activity, "main", 60)) {
        }
        statemanager statemanager2 = mostCurrent._statemanager;
        statemanager._restorestate(mostCurrent.activityBA, mostCurrent._activity, "page4", 60);
        return "";
    }

    public static String _ad_failedtoreceivead(String str) throws Exception {
        Common.Log("failed: " + str);
        return "";
    }

    public static String _ad_receivead() throws Exception {
        Common.Log("received");
        return "";
    }

    public static String _detail_display(int i) throws Exception {
        int i2;
        ConcreteViewWrapper concreteViewWrapper = new ConcreteViewWrapper();
        concreteViewWrapper.setObject((View) Common.Sender(mostCurrent.activityBA));
        if (i == 0) {
            i2 = (int) BA.ObjectToNumber(concreteViewWrapper.getTag());
        } else {
            i2 = i;
        }
        if (((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(detail.location) FROM event, detail where _id = '" + BA.NumberToString(i2) + "' and detail.location = event.location"))) == 0) {
            Common.ToastMessageShow("No detail to display", false);
        } else {
            if (!mostCurrent._b1.IsInitialized()) {
                CanvasWrapper.BitmapWrapper bitmapWrapper = mostCurrent._b1;
                File file = Common.File;
                bitmapWrapper.Initialize(File.getDirAssets(), "montage_circuits.png");
            }
            new CanvasWrapper();
            CanvasWrapper.BitmapWrapper bitmapWrapper2 = new CanvasWrapper.BitmapWrapper();
            bitmapWrapper2.InitializeMutable(200, 200);
            CanvasWrapper canvasWrapper = new CanvasWrapper();
            canvasWrapper.Initialize2((Bitmap) bitmapWrapper2.getObject());
            CanvasWrapper.RectWrapper rectWrapper = new CanvasWrapper.RectWrapper();
            rectWrapper.Initialize(0, 0, 200, 200);
            SQL.CursorWrapper cursorWrapper = new SQL.CursorWrapper();
            cursorWrapper.setObject(_sql1.ExecQuery("SELECT name, startdate, enddate, detail.location, web_address, col, row, length FROM event, detail where _id = '" + BA.NumberToString(i2) + "' and detail.location = event.location"));
            cursorWrapper.setPosition(0);
            String GetString = cursorWrapper.GetString("location");
            String GetString2 = cursorWrapper.GetString("length");
            String GetString3 = cursorWrapper.GetString("web_address");
            int GetInt = cursorWrapper.GetInt("col");
            int GetInt2 = cursorWrapper.GetInt("row");
            mostCurrent._srcrect.Initialize((GetInt - 1) * 200, (GetInt2 - 1) * 200, GetInt * 200, GetInt2 * 200);
            canvasWrapper.DrawBitmap((Bitmap) mostCurrent._b1.getObject(), (Rect) mostCurrent._srcrect.getObject(), (Rect) rectWrapper.getObject());
            BA.NumberToString(10);
            BA.NumberToString(10);
            String str = "";
            Common.Msgbox2("Length : " + GetString2 + "" + Common.CRLF + "" + GetString3 + "", "" + GetString + "", "Close", "", str, (Bitmap) bitmapWrapper2.getObject(), mostCurrent.activityBA);
        }
        return "";
    }

    public static void initializeProcessGlobals() {
        if (!processGlobalsRun) {
            processGlobalsRun = true;
            try {
                _process_globals();
                statemanager._process_globals();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static String _globals() throws Exception {
        mostCurrent._tabhost1 = new TabHostWrapper();
        mostCurrent._adview1 = new AdViewWrapper();
        mostCurrent._listview2 = new ListViewWrapper();
        mostCurrent._listview3 = new ListViewWrapper();
        mostCurrent._listview4 = new ListViewWrapper();
        mostCurrent._scrollview1 = new ScrollViewWrapper();
        mostCurrent._scrollview2 = new ScrollViewWrapper();
        mostCurrent._label1 = new LabelWrapper();
        mostCurrent._btnnext1 = new ButtonWrapper();
        mostCurrent._cursor = new SQL.CursorWrapper();
        _scrollpos = 0;
        mostCurrent._listviewsavefavs = new ListViewWrapper();
        _exitorreturn = false;
        _showtoast = false;
        main main = mostCurrent;
        _version = "";
        mostCurrent._versiontextlabel = new LabelWrapper();
        mostCurrent._versionlabel = new LabelWrapper();
        mostCurrent._version_label = new LabelWrapper();
        mostCurrent._pb_about = new ButtonWrapper();
        DateTime dateTime = Common.DateTime;
        DateTime.setDateFormat("yyyy-MM-dd");
        main main2 = mostCurrent;
        _v_today_date = "";
        mostCurrent._selecteditems = new List();
        mostCurrent._about_panel = new PanelWrapper();
        mostCurrent._pb_close_about_panel = new ButtonWrapper();
        mostCurrent._web_label = new LabelWrapper();
        mostCurrent._removefavourites = new ButtonWrapper();
        mostCurrent._webview1 = new WebViewWrapper();
        mostCurrent._pb_showfavsfirst = new CompoundButtonWrapper.ToggleButtonWrapper();
        main main3 = mostCurrent;
        _db_ver = "";
        mostCurrent._twitterlabel = new LabelWrapper();
        mostCurrent._options_panel = new PanelWrapper();
        mostCurrent._can1 = new CanvasWrapper();
        mostCurrent._b1 = new CanvasWrapper.BitmapWrapper();
        mostCurrent._r = new CanvasWrapper.RectWrapper();
        mostCurrent._srcrect = new CanvasWrapper.RectWrapper();
        mostCurrent._lv = new LayoutValues();
        _inc = 0;
        _pos = 0;
        _speed = 0;
        return "";
    }

    public static String _lbl1_click() throws Exception {
        _detail_display(0);
        return "";
    }

    public static String _lbl1_longclick() throws Exception {
        int parseDouble;
        ConcreteViewWrapper concreteViewWrapper = new ConcreteViewWrapper();
        concreteViewWrapper.setObject((View) Common.Sender(mostCurrent.activityBA));
        int ObjectToNumber = (int) BA.ObjectToNumber(concreteViewWrapper.getTag());
        if (((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(*) FROM favourite where fk_event_id = '" + BA.NumberToString(ObjectToNumber) + "'"))) == 0) {
            if (((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(_id) FROM favourite"))) == 0) {
                parseDouble = 1;
            } else {
                parseDouble = ((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT max(_id) FROM favourite"))) + 1;
            }
            _sql1.ExecNonQuery2("INSERT INTO favourite VALUES (?,?)", Common.ArrayToList(new Object[]{Integer.valueOf(parseDouble), Integer.valueOf(ObjectToNumber)}));
            _repopulatefavourites();
            Common.ToastMessageShow("Selected item added to your Favourites", false);
            return "";
        }
        Common.ToastMessageShow("Selected item is already in your Favourites", true);
        return "";
    }

    public static String _lbl2_click() throws Exception {
        _detail_display(0);
        return "";
    }

    public static String _lbl2_longclick() throws Exception {
        int parseDouble;
        ConcreteViewWrapper concreteViewWrapper = new ConcreteViewWrapper();
        concreteViewWrapper.setObject((View) Common.Sender(mostCurrent.activityBA));
        int ObjectToNumber = (int) BA.ObjectToNumber(concreteViewWrapper.getTag());
        if (((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(*) FROM favourite where fk_event_id = '" + BA.NumberToString(ObjectToNumber) + "'"))) == 0) {
            if (((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(_id) FROM favourite"))) == 0) {
                parseDouble = 1;
            } else {
                parseDouble = ((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT max(_id) FROM favourite"))) + 1;
            }
            _sql1.ExecNonQuery2("INSERT INTO favourite VALUES (?,?)", Common.ArrayToList(new Object[]{Integer.valueOf(parseDouble), Integer.valueOf(ObjectToNumber)}));
            _repopulatefavourites();
            Common.ToastMessageShow("Selected item added to your Favourites", false);
            return "";
        }
        Common.ToastMessageShow("Selected item is already in your Favourites", true);
        return "";
    }

    public static String _lbl3_click() throws Exception {
        _detail_display(0);
        return "";
    }

    public static String _lbl3_longclick() throws Exception {
        int parseDouble;
        ConcreteViewWrapper concreteViewWrapper = new ConcreteViewWrapper();
        concreteViewWrapper.setObject((View) Common.Sender(mostCurrent.activityBA));
        int ObjectToNumber = (int) BA.ObjectToNumber(concreteViewWrapper.getTag());
        if (((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(*) FROM favourite where fk_event_id = '" + BA.NumberToString(ObjectToNumber) + "'"))) == 0) {
            if (((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(_id) FROM favourite"))) == 0) {
                parseDouble = 1;
            } else {
                parseDouble = ((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT max(_id) FROM favourite"))) + 1;
            }
            _sql1.ExecNonQuery2("INSERT INTO favourite VALUES (?,?)", Common.ArrayToList(new Object[]{Integer.valueOf(parseDouble), Integer.valueOf(ObjectToNumber)}));
            _repopulatefavourites();
            Common.ToastMessageShow("Selected item added to your Favourites", false);
            return "";
        }
        Common.ToastMessageShow("Selected item is already in your Favourites", true);
        return "";
    }

    public static String _lbl4_click() throws Exception {
        _detail_display(0);
        return "";
    }

    public static String _lbl4_longclick() throws Exception {
        int parseDouble;
        ConcreteViewWrapper concreteViewWrapper = new ConcreteViewWrapper();
        concreteViewWrapper.setObject((View) Common.Sender(mostCurrent.activityBA));
        int ObjectToNumber = (int) BA.ObjectToNumber(concreteViewWrapper.getTag());
        if (((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(*) FROM favourite where fk_event_id = '" + BA.NumberToString(ObjectToNumber) + "'"))) == 0) {
            if (((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(_id) FROM favourite"))) == 0) {
                parseDouble = 1;
            } else {
                parseDouble = ((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT max(_id) FROM favourite"))) + 1;
            }
            _sql1.ExecNonQuery2("INSERT INTO favourite VALUES (?,?)", Common.ArrayToList(new Object[]{Integer.valueOf(parseDouble), Integer.valueOf(ObjectToNumber)}));
            _repopulatefavourites();
            Common.ToastMessageShow("Selected item added to your Favourites", false);
            return "";
        }
        Common.ToastMessageShow("Selected item is already in your Favourites", true);
        return "";
    }

    public static String _listview2_itemclick(int i, Object obj) throws Exception {
        _exitorreturn = false;
        if (mostCurrent._activity.getWidth() < mostCurrent._activity.getHeight()) {
            mostCurrent._listview2.SendToBack();
            mostCurrent._listview2.setVisible(false);
        }
        SQL.CursorWrapper cursorWrapper = new SQL.CursorWrapper();
        new LabelWrapper();
        new LabelWrapper();
        String NumberToString = BA.NumberToString(Common.DipToCurrent(20));
        String NumberToString2 = BA.NumberToString(-Common.DipToCurrent(19));
        _sql1.BeginTransaction();
        mostCurrent._scrollview2.getPanel().setHeight(((int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(*) FROM event where series = '" + String.valueOf(obj) + "'"))) * Common.DipToCurrent(41));
        cursorWrapper.setObject(_sql1.ExecQuery("SELECT name, display_date, startdate, enddate, event._id FROM event where series = '" + String.valueOf(obj) + "' order by date(startdate)"));
        double rowCount = (double) (cursorWrapper.getRowCount() - 1);
        int i2 = 0;
        while (((double) i2) <= rowCount) {
            cursorWrapper.setPosition(i2);
            LabelWrapper labelWrapper = new LabelWrapper();
            LabelWrapper labelWrapper2 = new LabelWrapper();
            String NumberToString3 = BA.NumberToString(Double.parseDouble(NumberToString2) + ((double) Common.DipToCurrent(21)));
            labelWrapper.Initialize(mostCurrent.activityBA, "lbl3");
            Colors colors = Common.Colors;
            labelWrapper.setColor(Colors.Black);
            int GetInt = cursorWrapper.GetInt("_id");
            labelWrapper.setTag(Integer.valueOf(GetInt));
            labelWrapper.setTextSize(14.0f);
            mostCurrent._scrollview2.getPanel().AddView((View) labelWrapper.getObject(), 0, (int) Double.parseDouble(NumberToString3), Common.PerXToCurrent(100.0f, mostCurrent.activityBA), (int) Double.parseDouble(NumberToString));
            String NumberToString4 = BA.NumberToString(Double.parseDouble(NumberToString3) + ((double) Common.DipToCurrent(20)));
            labelWrapper2.Initialize(mostCurrent.activityBA, "lbl4");
            Colors colors2 = Common.Colors;
            labelWrapper2.setColor(Colors.Black);
            labelWrapper2.setTag(Integer.valueOf(GetInt));
            labelWrapper2.setTextSize(11.0f);
            mostCurrent._scrollview2.getPanel().AddView((View) labelWrapper2.getObject(), 0, (int) Double.parseDouble(NumberToString4), Common.PerXToCurrent(100.0f, mostCurrent.activityBA), (int) Double.parseDouble(NumberToString));
            String GetString = cursorWrapper.GetString("enddate");
            DateTime dateTime = Common.DateTime;
            long DateParse = DateTime.DateParse(GetString);
            DateTime dateTime2 = Common.DateTime;
            main main = mostCurrent;
            if (DateParse < DateTime.DateParse(_v_today_date)) {
                Colors colors3 = Common.Colors;
                labelWrapper.setTextColor(Colors.Gray);
                Colors colors4 = Common.Colors;
                labelWrapper2.setTextColor(Colors.Gray);
                labelWrapper.setText(cursorWrapper.GetString("name"));
                labelWrapper2.setText(cursorWrapper.GetString("display_date"));
            } else {
                Colors colors5 = Common.Colors;
                labelWrapper.setTextColor(-1);
                Colors colors6 = Common.Colors;
                labelWrapper2.setTextColor(Colors.Gray);
                labelWrapper.setText(cursorWrapper.GetString("name"));
                labelWrapper2.setText(cursorWrapper.GetString("display_date"));
            }
            i2 = (int) (((double) i2) + 1.0d);
            NumberToString2 = NumberToString4;
        }
        _sql1.TransactionSuccessful();
        _sql1.EndTransaction();
        Common.DoEvents();
        mostCurrent._scrollview2.setScrollPosition(Common.DipToCurrent((int) (Double.parseDouble(_sql1.ExecQuerySingleResult("select count(_id) from event where series =  '" + String.valueOf(obj) + "' and event.enddate < date('now')")) * 40.25d)));
        cursorWrapper.Close();
        mostCurrent._scrollview2.setVisible(true);
        return "";
    }

    public static String _listview2_itemlongclick(int i, Object obj) throws Exception {
        SQL.CursorWrapper cursorWrapper = new SQL.CursorWrapper();
        cursorWrapper.setObject(_sql1.ExecQuery("SELECT _id FROM event where series = '" + String.valueOf(obj) + "' order by date(startdate)"));
        double rowCount = (double) (cursorWrapper.getRowCount() - 1);
        for (int i2 = 0; ((double) i2) <= rowCount; i2 = (int) (((double) i2) + 1.0d)) {
            cursorWrapper.setPosition(i2);
            _sql1.ExecNonQuery("DELETE FROM favourite where fk_event_id = '" + BA.NumberToString(cursorWrapper.GetInt("_id")) + "'");
        }
        cursorWrapper.Close();
        int parseDouble = (int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT count(_id) FROM favourite"));
        if (parseDouble != 0) {
            parseDouble = (int) Double.parseDouble(_sql1.ExecQuerySingleResult("SELECT max(_id) FROM favourite"));
        }
        SQL.CursorWrapper cursorWrapper2 = new SQL.CursorWrapper();
        cursorWrapper2.setObject(_sql1.ExecQuery("SELECT _id FROM event where series = '" + String.valueOf(obj) + "' order by date(startdate)"));
        double rowCount2 = (double) (cursorWrapper2.getRowCount() - 1);
        for (int i3 = 0; ((double) i3) <= rowCount2; i3 = (int) (((double) i3) + 1.0d)) {
            cursorWrapper2.setPosition(i3);
            parseDouble++;
            _sql1.ExecNonQuery2("INSERT INTO favourite VALUES (?,?)", Common.ArrayToList(new Object[]{Integer.valueOf(parseDouble), Integer.valueOf(cursorWrapper2.GetInt("_id"))}));
        }
        cursorWrapper2.Close();
        _repopulatefavourites();
        Common.ToastMessageShow("All future events for this Series have been added to your Favourites", false);
        return "";
    }

    public static String _listview3_itemclick(int i, Object obj) throws Exception {
        _detail_display((int) BA.ObjectToNumber(obj));
        return "";
    }

    public static String _listview3_itemlongclick(int i, Object obj) throws Exception {
        int Msgbox2 = Common.Msgbox2("Are you sure?", "Remove selected Favourite", "Yes", "", "No", (Bitmap) Common.Null, mostCurrent.activityBA);
        DialogResponse dialogResponse = Common.DialogResponse;
        if (Msgbox2 == -1) {
            _sql1.ExecNonQuery("DELETE FROM favourite where fk_event_id = '" + String.valueOf(obj) + "'");
            _repopulatefavourites();
        }
        return "";
    }

    public static String _pb_about_click() throws Exception {
        mostCurrent._versionlabel.setText(new PackageManagerWrapper().GetVersionName("torquesoft.themsc"));
        mostCurrent._options_panel.setVisible(false);
        mostCurrent._options_panel.SendToBack();
        _exitorreturn = false;
        mostCurrent._pb_close_about_panel.setVisible(true);
        mostCurrent._pb_close_about_panel.setEnabled(true);
        mostCurrent._about_panel.setVisible(true);
        mostCurrent._removefavourites.setEnabled(false);
        mostCurrent._web_label.setEnabled(true);
        mostCurrent._web_label.setVisible(true);
        mostCurrent._twitterlabel.setEnabled(true);
        mostCurrent._twitterlabel.setVisible(true);
        mostCurrent._version_label.setText("Application Version : " + mostCurrent._versionlabel.getText());
        return "";
    }

    public static String _pb_close_about_panel_click() throws Exception {
        _exitorreturn = true;
        mostCurrent._about_panel.setVisible(false);
        mostCurrent._about_panel.SendToBack();
        mostCurrent._removefavourites.setEnabled(true);
        mostCurrent._options_panel.setVisible(true);
        mostCurrent._options_panel.BringToFront();
        mostCurrent._pb_close_about_panel.SendToBack();
        mostCurrent._pb_close_about_panel.setVisible(false);
        mostCurrent._pb_close_about_panel.setEnabled(false);
        mostCurrent._web_label.setEnabled(false);
        mostCurrent._web_label.setVisible(false);
        mostCurrent._twitterlabel.setEnabled(false);
        mostCurrent._twitterlabel.setVisible(false);
        return "";
    }

    public static String _process_globals() throws Exception {
        _sql1 = new SQL();
        _v_display_notifications = false;
        _v_tablet = false;
        return "";
    }

    public static String _removefavourites_click() throws Exception {
        int Msgbox2 = Common.Msgbox2("Are you sure?", "Remove all Favourites", "Yes", "", "No", (Bitmap) Common.Null, mostCurrent.activityBA);
        DialogResponse dialogResponse = Common.DialogResponse;
        if (Msgbox2 == -1) {
            mostCurrent._listview3.Clear();
            _sql1.ExecNonQuery("DELETE FROM favourite");
        }
        return "";
    }

    public static String _repopulatefavourites() throws Exception {
        SQL.CursorWrapper cursorWrapper = new SQL.CursorWrapper();
        mostCurrent._listview3.getTwoLinesLayout().Label.setTextSize(14.0f);
        mostCurrent._listview3.getTwoLinesLayout().SecondLabel.setTextSize(11.0f);
        LabelWrapper labelWrapper = mostCurrent._listview3.getTwoLinesLayout().Label;
        Gravity gravity = Common.Gravity;
        labelWrapper.setGravity(Gravity.FILL);
        ListViewWrapper listViewWrapper = mostCurrent._listview3;
        Colors colors = Common.Colors;
        listViewWrapper.setScrollingBackgroundColor(0);
        mostCurrent._listview3.Clear();
        cursorWrapper.setObject(_sql1.ExecQuery("SELECT name, startdate, display_date, event._id FROM event, favourite where event._id = favourite.fk_event_id order by date(startdate)"));
        double rowCount = (double) (cursorWrapper.getRowCount() - 1);
        for (int i = 0; ((double) i) <= rowCount; i = (int) (((double) i) + 1.0d)) {
            cursorWrapper.setPosition(i);
            mostCurrent._listview3.AddTwoLines2(cursorWrapper.GetString("name"), cursorWrapper.GetString("display_date"), Integer.valueOf(cursorWrapper.GetInt("_id")));
        }
        cursorWrapper.Close();
        return "";
    }

    public static String _tabhost1_tabchanged() throws Exception {
        mostCurrent._listview2.BringToFront();
        SQL.CursorWrapper cursorWrapper = new SQL.CursorWrapper();
        cursorWrapper.setObject(_sql1.ExecQuery("SELECT event.enddate, event._id FROM event, favourite where event._id = favourite.fk_event_id and event.enddate < date('now')"));
        double rowCount = (double) (cursorWrapper.getRowCount() - 1);
        for (int i = 0; ((double) i) <= rowCount; i = (int) (((double) i) + 1.0d)) {
            cursorWrapper.setPosition(i);
            _sql1.ExecNonQuery("DELETE FROM favourite where fk_event_id = '" + BA.NumberToString(cursorWrapper.GetInt("_id")) + "'");
        }
        cursorWrapper.Close();
        _repopulatefavourites();
        return "";
    }

    public static String _twitterlabel_click() throws Exception {
        if (!_v_tablet) {
            return "";
        }
        _exitorreturn = false;
        mostCurrent._webview1.setVisible(true);
        mostCurrent._webview1.LoadUrl("http://twitter.com/#!/msportcalendar");
        return "";
    }

    public static String _web_label_click() throws Exception {
        _exitorreturn = false;
        mostCurrent._webview1.setVisible(true);
        mostCurrent._webview1.LoadUrl("http://www.themotorsportcalendar.com/");
        return "";
    }

    public static boolean _webview1_overrideurl(String str) throws Exception {
        return false;
    }

    public static String _webview1_pagefinished(String str) throws Exception {
        return "";
    }
}
