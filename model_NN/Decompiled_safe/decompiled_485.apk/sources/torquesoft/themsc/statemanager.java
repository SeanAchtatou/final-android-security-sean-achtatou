package torquesoft.themsc;

import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TabHost;
import android.widget.ToggleButton;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.keywords.DateTime;
import anywheresoftware.b4a.objects.ActivityWrapper;
import anywheresoftware.b4a.objects.CompoundButtonWrapper;
import anywheresoftware.b4a.objects.ConcreteViewWrapper;
import anywheresoftware.b4a.objects.EditTextWrapper;
import anywheresoftware.b4a.objects.PanelWrapper;
import anywheresoftware.b4a.objects.ScrollViewWrapper;
import anywheresoftware.b4a.objects.SeekBarWrapper;
import anywheresoftware.b4a.objects.SpinnerWrapper;
import anywheresoftware.b4a.objects.TabHostWrapper;
import anywheresoftware.b4a.objects.collections.List;
import anywheresoftware.b4a.objects.collections.Map;
import anywheresoftware.b4a.objects.streams.File;
import anywheresoftware.b4a.randomaccessfile.RandomAccessFile;

public class statemanager {
    public static int _listposition = 0;
    public static Map _settings = null;
    public static String _settingsfilename = "";
    public static Map _states = null;
    public static String _statesfilename = "";
    private static statemanager mostCurrent = new statemanager();
    public Common __c = null;
    public main _main = null;

    public static Object getObject() {
        throw new RuntimeException("Code module does not support this method.");
    }

    public static Object[] _getnextitem(BA ba, List list) throws Exception {
        _listposition++;
        return (Object[]) list.Get(_listposition);
    }

    public static String _getsetting(BA ba, String str) throws Exception {
        return _getsetting2(ba, str, "");
    }

    public static String _getsetting2(BA ba, String str, String str2) throws Exception {
        if (!_settings.IsInitialized()) {
            File file = Common.File;
            File file2 = Common.File;
            if (!File.Exists(File.getDirInternal(), _settingsfilename)) {
                return str2;
            }
            File file3 = Common.File;
            File file4 = Common.File;
            _settings = File.ReadMap(File.getDirInternal(), _settingsfilename);
        }
        return String.valueOf(_settings.GetDefault(str.toLowerCase(), str2));
    }

    public static String _innerrestorestate(BA ba, ConcreteViewWrapper concreteViewWrapper, List list) throws Exception {
        Object[] objArr = new Object[0];
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            objArr[i] = new Object();
        }
        if (concreteViewWrapper.getObjectOrNull() instanceof EditText) {
            EditTextWrapper editTextWrapper = new EditTextWrapper();
            editTextWrapper.setObject((EditText) concreteViewWrapper.getObject());
            Object[] _getnextitem = _getnextitem(ba, list);
            editTextWrapper.setText(_getnextitem[0]);
            editTextWrapper.setSelectionStart((int) BA.ObjectToNumber(_getnextitem[1]));
            return "";
        } else if (concreteViewWrapper.getObjectOrNull() instanceof SpinnerWrapper.B4ASpinner) {
            SpinnerWrapper spinnerWrapper = new SpinnerWrapper();
            spinnerWrapper.setObject((SpinnerWrapper.B4ASpinner) concreteViewWrapper.getObject());
            spinnerWrapper.setSelectedIndex((int) BA.ObjectToNumber(_getnextitem(ba, list)[0]));
            return "";
        } else if (concreteViewWrapper.getObjectOrNull() instanceof CheckBox) {
            CompoundButtonWrapper.CheckBoxWrapper checkBoxWrapper = new CompoundButtonWrapper.CheckBoxWrapper();
            checkBoxWrapper.setObject((CheckBox) concreteViewWrapper.getObject());
            checkBoxWrapper.setChecked(BA.ObjectToBoolean(_getnextitem(ba, list)[0]));
            return "";
        } else if (concreteViewWrapper.getObjectOrNull() instanceof RadioButton) {
            CompoundButtonWrapper.RadioButtonWrapper radioButtonWrapper = new CompoundButtonWrapper.RadioButtonWrapper();
            radioButtonWrapper.setObject((RadioButton) concreteViewWrapper.getObject());
            radioButtonWrapper.setChecked(BA.ObjectToBoolean(_getnextitem(ba, list)[0]));
            return "";
        } else if (concreteViewWrapper.getObjectOrNull() instanceof ToggleButton) {
            CompoundButtonWrapper.ToggleButtonWrapper toggleButtonWrapper = new CompoundButtonWrapper.ToggleButtonWrapper();
            toggleButtonWrapper.setObject((ToggleButton) concreteViewWrapper.getObject());
            toggleButtonWrapper.setChecked(BA.ObjectToBoolean(_getnextitem(ba, list)[0]));
            return "";
        } else if (concreteViewWrapper.getObjectOrNull() instanceof SeekBar) {
            SeekBarWrapper seekBarWrapper = new SeekBarWrapper();
            seekBarWrapper.setObject((SeekBar) concreteViewWrapper.getObject());
            seekBarWrapper.setValue((int) BA.ObjectToNumber(_getnextitem(ba, list)[0]));
            return "";
        } else if (concreteViewWrapper.getObjectOrNull() instanceof TabHost) {
            TabHostWrapper tabHostWrapper = new TabHostWrapper();
            tabHostWrapper.setObject((TabHost) concreteViewWrapper.getObject());
            tabHostWrapper.setCurrentTab((int) BA.ObjectToNumber(_getnextitem(ba, list)[0]));
            return "";
        } else if (concreteViewWrapper.getObjectOrNull() instanceof ScrollView) {
            ScrollViewWrapper scrollViewWrapper = new ScrollViewWrapper();
            scrollViewWrapper.setObject((ScrollView) concreteViewWrapper.getObject());
            scrollViewWrapper.setScrollPosition((int) BA.ObjectToNumber(_getnextitem(ba, list)[0]));
            return "";
        } else if (!(concreteViewWrapper.getObjectOrNull() instanceof ViewGroup)) {
            return "";
        } else {
            PanelWrapper panelWrapper = new PanelWrapper();
            panelWrapper.setObject((ViewGroup) concreteViewWrapper.getObject());
            double numberOfViews = (double) (panelWrapper.getNumberOfViews() - 1);
            for (int i2 = 0; ((double) i2) <= numberOfViews; i2 = (int) (((double) i2) + 1.0d)) {
                _innerrestorestate(ba, panelWrapper.GetView(i2), list);
            }
            return "";
        }
    }

    public static String _innersavestate(BA ba, ConcreteViewWrapper concreteViewWrapper, List list) throws Exception {
        Object[] objArr;
        Object[] objArr2 = new Object[0];
        int length = objArr2.length;
        for (int i = 0; i < length; i++) {
            objArr2[i] = new Object();
        }
        if (concreteViewWrapper.getObjectOrNull() instanceof EditText) {
            EditTextWrapper editTextWrapper = new EditTextWrapper();
            editTextWrapper.setObject((EditText) concreteViewWrapper.getObject());
            objArr = new Object[]{editTextWrapper.getText(), Integer.valueOf(editTextWrapper.getSelectionStart())};
        } else if (concreteViewWrapper.getObjectOrNull() instanceof SpinnerWrapper.B4ASpinner) {
            SpinnerWrapper spinnerWrapper = new SpinnerWrapper();
            spinnerWrapper.setObject((SpinnerWrapper.B4ASpinner) concreteViewWrapper.getObject());
            objArr = new Object[]{Integer.valueOf(spinnerWrapper.getSelectedIndex())};
        } else if (concreteViewWrapper.getObjectOrNull() instanceof CheckBox) {
            CompoundButtonWrapper.CheckBoxWrapper checkBoxWrapper = new CompoundButtonWrapper.CheckBoxWrapper();
            checkBoxWrapper.setObject((CheckBox) concreteViewWrapper.getObject());
            objArr = new Object[]{Boolean.valueOf(checkBoxWrapper.getChecked())};
        } else if (concreteViewWrapper.getObjectOrNull() instanceof RadioButton) {
            CompoundButtonWrapper.RadioButtonWrapper radioButtonWrapper = new CompoundButtonWrapper.RadioButtonWrapper();
            radioButtonWrapper.setObject((RadioButton) concreteViewWrapper.getObject());
            objArr = new Object[]{Boolean.valueOf(radioButtonWrapper.getChecked())};
        } else if (concreteViewWrapper.getObjectOrNull() instanceof ToggleButton) {
            CompoundButtonWrapper.ToggleButtonWrapper toggleButtonWrapper = new CompoundButtonWrapper.ToggleButtonWrapper();
            toggleButtonWrapper.setObject((ToggleButton) concreteViewWrapper.getObject());
            objArr = new Object[]{Boolean.valueOf(toggleButtonWrapper.getChecked())};
        } else if (concreteViewWrapper.getObjectOrNull() instanceof SeekBar) {
            SeekBarWrapper seekBarWrapper = new SeekBarWrapper();
            seekBarWrapper.setObject((SeekBar) concreteViewWrapper.getObject());
            objArr = new Object[]{Integer.valueOf(seekBarWrapper.getValue())};
        } else if (concreteViewWrapper.getObjectOrNull() instanceof TabHost) {
            TabHostWrapper tabHostWrapper = new TabHostWrapper();
            tabHostWrapper.setObject((TabHost) concreteViewWrapper.getObject());
            objArr = new Object[]{Integer.valueOf(tabHostWrapper.getCurrentTab())};
        } else if (concreteViewWrapper.getObjectOrNull() instanceof ScrollView) {
            ScrollViewWrapper scrollViewWrapper = new ScrollViewWrapper();
            scrollViewWrapper.setObject((ScrollView) concreteViewWrapper.getObject());
            objArr = new Object[]{Integer.valueOf(scrollViewWrapper.getScrollPosition())};
        } else if (concreteViewWrapper.getObjectOrNull() instanceof ViewGroup) {
            PanelWrapper panelWrapper = new PanelWrapper();
            panelWrapper.setObject((ViewGroup) concreteViewWrapper.getObject());
            double numberOfViews = (double) (panelWrapper.getNumberOfViews() - 1);
            for (int i2 = 0; ((double) i2) <= numberOfViews; i2 = (int) (((double) i2) + 1.0d)) {
                _innersavestate(ba, panelWrapper.GetView(i2), list);
            }
            objArr = objArr2;
        } else {
            objArr = objArr2;
        }
        if (objArr.length <= 0) {
            return "";
        }
        list.Add(objArr);
        return "";
    }

    public static String _loadstatefile(BA ba) throws Exception {
        if (_states.IsInitialized()) {
            return "";
        }
        File file = Common.File;
        File file2 = Common.File;
        if (File.Exists(File.getDirInternal(), _statesfilename)) {
            RandomAccessFile randomAccessFile = new RandomAccessFile();
            File file3 = Common.File;
            randomAccessFile.Initialize(File.getDirInternal(), _statesfilename, false);
            _states.setObject((Map.MyMap) randomAccessFile.ReadObject(0));
            randomAccessFile.Close();
        }
        return "";
    }

    public static String _process_globals() throws Exception {
        _states = new Map();
        _listposition = 0;
        _statesfilename = "";
        _settingsfilename = "";
        _statesfilename = "state.dat";
        _settingsfilename = "settings.properties";
        _settings = new Map();
        return "";
    }

    public static String _resetstate(BA ba, String str) throws Exception {
        _loadstatefile(ba);
        if (!_states.IsInitialized()) {
            return "";
        }
        _states.Remove(str.toLowerCase());
        _writestatetofile(ba);
        return "";
    }

    public static boolean _restorestate(BA ba, ActivityWrapper activityWrapper, String str, int i) throws Exception {
        try {
            _loadstatefile(ba);
            if (!_states.IsInitialized()) {
                return false;
            }
            List list = new List();
            list.setObject((java.util.List) _states.Get(str.toLowerCase()));
            if (!list.IsInitialized()) {
                return false;
            }
            long ObjectToNumber = (long) BA.ObjectToNumber(list.Get(0));
            if (i > 0) {
                DateTime dateTime = Common.DateTime;
                DateTime dateTime2 = Common.DateTime;
                if (ObjectToNumber + (((long) i) * DateTime.TicksPerMinute) < DateTime.getNow()) {
                    return false;
                }
            }
            _listposition = 0;
            double numberOfViews = (double) (activityWrapper.getNumberOfViews() - 1);
            for (int i2 = 0; ((double) i2) <= numberOfViews; i2 = (int) (((double) i2) + 1.0d)) {
                _innerrestorestate(ba, activityWrapper.GetView(i2), list);
            }
            return true;
        } catch (Exception e) {
            (ba.processBA == null ? ba : ba.processBA).setLastException(e);
            Common.Log("Error loading state.");
            Common.Log(Common.LastException(ba).getMessage());
            return false;
        }
    }

    public static String _savesettings(BA ba) throws Exception {
        if (!_settings.IsInitialized()) {
            return "";
        }
        File file = Common.File;
        File file2 = Common.File;
        File.WriteMap(File.getDirInternal(), _settingsfilename, _settings);
        return "";
    }

    public static String _savestate(BA ba, ActivityWrapper activityWrapper, String str) throws Exception {
        if (!_states.IsInitialized()) {
            _states.Initialize();
        }
        List list = new List();
        list.Initialize();
        DateTime dateTime = Common.DateTime;
        list.Add(Long.valueOf(DateTime.getNow()));
        double numberOfViews = (double) (activityWrapper.getNumberOfViews() - 1);
        for (int i = 0; ((double) i) <= numberOfViews; i = (int) (((double) i) + 1.0d)) {
            _innersavestate(ba, activityWrapper.GetView(i), list);
        }
        _states.Put(str.toLowerCase(), list.getObject());
        _writestatetofile(ba);
        return "";
    }

    public static String _setsetting(BA ba, String str, String str2) throws Exception {
        if (!_settings.IsInitialized()) {
            File file = Common.File;
            File file2 = Common.File;
            if (File.Exists(File.getDirInternal(), _settingsfilename)) {
                File file3 = Common.File;
                File file4 = Common.File;
                _settings = File.ReadMap(File.getDirInternal(), _settingsfilename);
            } else {
                _settings.Initialize();
            }
        }
        _settings.Put(str.toLowerCase(), str2);
        return "";
    }

    public static String _writestatetofile(BA ba) throws Exception {
        RandomAccessFile randomAccessFile = new RandomAccessFile();
        File file = Common.File;
        randomAccessFile.Initialize(File.getDirInternal(), _statesfilename, false);
        DateTime dateTime = Common.DateTime;
        BA.NumberToString(DateTime.getNow());
        randomAccessFile.WriteObject(_states.getObject(), true, randomAccessFile.CurrentPosition);
        randomAccessFile.Close();
        return "";
    }
}
