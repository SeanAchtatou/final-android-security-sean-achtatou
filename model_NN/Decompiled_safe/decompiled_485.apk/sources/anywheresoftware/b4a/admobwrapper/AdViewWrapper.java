package anywheresoftware.b4a.admobwrapper;

import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.ViewWrapper;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

@BA.ShortName("AdView")
@BA.ActivityObject
public class AdViewWrapper extends ViewWrapper<AdView> {
    public void Initialize(final BA ba, String EventName, String PublisherId) {
        setObject(new AdView(ba.activity, AdSize.BANNER, PublisherId));
        super.Initialize(ba, EventName);
        final String eventName = EventName.toLowerCase(BA.cul);
        ((AdView) getObject()).setAdListener(new AdListener() {
            public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode e) {
                ba.raiseEvent(AdViewWrapper.this.getObject(), String.valueOf(eventName) + "_failedtoreceivead", e.toString());
            }

            public void onReceiveAd(Ad ad) {
                ba.raiseEvent(AdViewWrapper.this.getObject(), String.valueOf(eventName) + "_receivead", new Object[0]);
            }

            public void onDismissScreen(Ad arg0) {
            }

            public void onLeaveApplication(Ad arg0) {
            }

            public void onPresentScreen(Ad arg0) {
            }
        });
    }

    @BA.Hide
    public void Initiailize(BA ba, String EventName) {
    }

    public void LoadAd() {
        AdRequest req = new AdRequest();
        req.setTesting(true);
        ((AdView) getObject()).loadAd(req);
    }
}
