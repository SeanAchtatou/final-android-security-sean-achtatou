package anywheresoftware.b4a.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.telephony.SmsMessage;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.IntentWrapper;
import anywheresoftware.b4a.phone.Phone;
import java.util.HashMap;
import java.util.Map;

@BA.ShortName("PhoneEvents")
public class PhoneEvents {
    /* access modifiers changed from: private */
    public BA ba;
    private BroadcastReceiver br;
    /* access modifiers changed from: private */
    public String ev;
    /* access modifiers changed from: private */
    public HashMap<String, ActionHandler> map = new HashMap<>();

    public PhoneEvents() {
        this.map.put("android.speech.tts.TTS_QUEUE_PROCESSING_COMPLETED", new ActionHandler() {
            {
                this.event = "_texttospeechfinish";
            }

            public void handle(Intent intent) {
                send(intent, null);
            }
        });
        this.map.put("android.net.conn.CONNECTIVITY_CHANGE", new ActionHandler() {
            {
                this.event = "_connectivitychanged";
            }

            public void handle(Intent intent) {
                NetworkInfo ni = (NetworkInfo) intent.getParcelableExtra("networkInfo");
                send(intent, new Object[]{ni.getTypeName(), ni.getState().toString()});
            }
        });
        this.map.put("android.intent.action.USER_PRESENT", new ActionHandler() {
            {
                this.event = "_userpresent";
            }

            public void handle(Intent intent) {
                send(intent, null);
            }
        });
        this.map.put("android.intent.action.ACTION_SHUTDOWN", new ActionHandler() {
            {
                this.event = "_shutdown";
            }

            public void handle(Intent intent) {
                send(intent, null);
            }
        });
        this.map.put("android.intent.action.SCREEN_ON", new ActionHandler() {
            {
                this.event = "_screenon";
            }

            public void handle(Intent intent) {
                send(intent, null);
            }
        });
        this.map.put("android.intent.action.SCREEN_OFF", new ActionHandler() {
            {
                this.event = "_screenoff";
            }

            public void handle(Intent intent) {
                send(intent, null);
            }
        });
        this.map.put("android.intent.action.PACKAGE_REMOVED", new ActionHandler() {
            {
                this.event = "_packageremoved";
            }

            public void handle(Intent intent) {
                send(intent, new Object[]{intent.getDataString()});
            }
        });
        this.map.put("android.intent.action.PACKAGE_ADDED", new ActionHandler() {
            {
                this.event = "_packageadded";
            }

            public void handle(Intent intent) {
                send(intent, new Object[]{intent.getDataString()});
            }
        });
        this.map.put("android.intent.action.DEVICE_STORAGE_LOW", new ActionHandler() {
            {
                this.event = "_devicestoragelow";
            }

            public void handle(Intent intent) {
                send(intent, null);
            }
        });
        this.map.put("android.intent.action.DEVICE_STORAGE_OK", new ActionHandler() {
            {
                this.event = "_devicestorageok";
            }

            public void handle(Intent intent) {
                send(intent, null);
            }
        });
        this.map.put("android.intent.action.BATTERY_CHANGED", new ActionHandler() {
            {
                this.event = "_batterychanged";
            }

            public void handle(Intent intent) {
                boolean plugged;
                int level = intent.getIntExtra("level", 0);
                int scale = intent.getIntExtra("scale", 1);
                if (intent.getIntExtra("plugged", 0) > 0) {
                    plugged = true;
                } else {
                    plugged = false;
                }
                send(intent, new Object[]{Integer.valueOf(level), Integer.valueOf(scale), Boolean.valueOf(plugged)});
            }
        });
        this.map.put("android.intent.action.AIRPLANE_MODE", new ActionHandler() {
            {
                this.event = "_airplanemodechanged";
            }

            public void handle(Intent intent) {
                send(intent, new Object[]{Boolean.valueOf(intent.getBooleanExtra("state", false))});
            }
        });
        for (Map.Entry<String, ActionHandler> e : this.map.entrySet()) {
            ((ActionHandler) e.getValue()).action = (String) e.getKey();
        }
    }

    public void InitializeWithPhoneState(BA ba2, String EventName, Phone.PhoneId PhoneId) {
        this.map.put("android.intent.action.PHONE_STATE", new ActionHandler() {
            {
                this.event = "_phonestatechanged";
            }

            public void handle(Intent intent) {
                String state = intent.getStringExtra("state");
                String incomingNumber = intent.getStringExtra("incoming_number");
                if (incomingNumber == null) {
                    incomingNumber = "";
                }
                send(intent, new Object[]{state, incomingNumber});
            }
        });
        this.map.get("android.intent.action.PHONE_STATE").action = "android.intent.action.PHONE_STATE";
        Initialize(ba2, EventName);
    }

    public void Initialize(BA ba2, String EventName) {
        this.ba = ba2;
        this.ev = EventName.toLowerCase(BA.cul);
        StopListening();
        this.br = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                ActionHandler ah;
                if (intent.getAction() != null && (ah = (ActionHandler) PhoneEvents.this.map.get(intent.getAction())) != null) {
                    ah.handle(intent);
                }
            }
        };
        IntentFilter f1 = new IntentFilter();
        IntentFilter f2 = null;
        for (ActionHandler ah : this.map.values()) {
            if (ba2.subExists(String.valueOf(this.ev) + ah.event)) {
                if (ah.action == "android.intent.action.PACKAGE_ADDED" || ah.action == "android.intent.action.PACKAGE_REMOVED") {
                    if (f2 == null) {
                        f2 = new IntentFilter();
                        f2.addDataScheme("package");
                    }
                    f2.addAction(ah.action);
                }
                f1.addAction(ah.action);
            }
        }
        BA.applicationContext.registerReceiver(this.br, f1);
        if (f2 != null) {
            BA.applicationContext.registerReceiver(this.br, f2);
        }
    }

    public void StopListening() {
        if (this.br != null) {
            BA.applicationContext.unregisterReceiver(this.br);
        }
    }

    private abstract class ActionHandler {
        public String action;
        public String event;

        public abstract void handle(Intent intent);

        private ActionHandler() {
        }

        /* synthetic */ ActionHandler(PhoneEvents phoneEvents, ActionHandler actionHandler) {
            this();
        }

        /* access modifiers changed from: protected */
        public void send(Intent intent, Object[] args) {
            final Object[] o;
            if (args == null) {
                o = new Object[1];
            } else {
                o = new Object[(args.length + 1)];
                System.arraycopy(args, 0, o, 0, args.length);
            }
            o[o.length - 1] = AbsObjectWrapper.ConvertToWrapper(new IntentWrapper(), intent);
            if (BA.debugMode) {
                BA.handler.post(new BA.B4ARunnable() {
                    public void run() {
                        PhoneEvents.this.ba.raiseEvent(this, String.valueOf(PhoneEvents.this.ev) + ActionHandler.this.event, o);
                    }
                });
            } else {
                PhoneEvents.this.ba.raiseEvent(this, String.valueOf(PhoneEvents.this.ev) + this.event, o);
            }
        }
    }

    @BA.ShortName("SmsInterceptor")
    public static class SMSInterceptor {
        private BroadcastReceiver br;

        public void Initialize(String EventName, final BA ba) {
            final String ev = String.valueOf(EventName.toLowerCase(BA.cul)) + "_messagereceived";
            this.br = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    Bundle bundle;
                    if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED") && (bundle = intent.getExtras()) != null) {
                        Object[] pduObj = (Object[]) bundle.get("pdus");
                        for (Object obj : pduObj) {
                            SmsMessage sm = SmsMessage.createFromPdu((byte[]) obj);
                            ba.raiseEvent(SMSInterceptor.this, ev, sm.getOriginatingAddress(), sm.getMessageBody());
                        }
                    }
                }
            };
            BA.applicationContext.registerReceiver(this.br, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        }

        public void StopListening() {
            if (this.br != null) {
                BA.applicationContext.unregisterReceiver(this.br);
            }
            this.br = null;
        }
    }
}
