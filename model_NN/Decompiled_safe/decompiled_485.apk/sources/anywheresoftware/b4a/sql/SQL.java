package anywheresoftware.b4a.sql;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.collections.List;
import java.io.File;

@BA.ShortName("SQL")
public class SQL {
    private SQLiteDatabase db;

    public void Initialize(String Dir, String FileName, boolean CreateIfNecessary) {
        this.db = SQLiteDatabase.openDatabase(new File(Dir, FileName).toString(), null, (CreateIfNecessary ? 268435456 : 0) | 16);
    }

    public static void LIBRARY_DOC() {
    }

    private void checkNull() {
        if (this.db == null) {
            throw new RuntimeException("Object should first be initialized.");
        }
    }

    public boolean IsInitialized() {
        if (this.db == null) {
            return false;
        }
        return this.db.isOpen();
    }

    public void ExecNonQuery(String Statement) {
        checkNull();
        this.db.execSQL(Statement);
    }

    public void ExecNonQuery2(String Statement, List Args) {
        checkNull();
        SQLiteStatement s = this.db.compileStatement(Statement);
        try {
            int numArgs = Args.getSize();
            for (int i = 0; i < numArgs; i++) {
                DatabaseUtils.bindObjectToProgram(s, i + 1, Args.Get(i));
            }
            s.execute();
        } finally {
            s.close();
        }
    }

    public Cursor ExecQuery(String Query) {
        checkNull();
        return ExecQuery2(Query, null);
    }

    public Cursor ExecQuery2(String Query, String[] StringArgs) {
        checkNull();
        return this.db.rawQuery(Query, StringArgs);
    }

    public String ExecQuerySingleResult(String Query) {
        return ExecQuerySingleResult2(Query, null);
    }

    /* JADX INFO: finally extract failed */
    public String ExecQuerySingleResult2(String Query, String[] StringArgs) {
        checkNull();
        Cursor cursor = this.db.rawQuery(Query, StringArgs);
        try {
            if (cursor.moveToFirst()) {
                if (cursor.getColumnCount() != 0) {
                    String string = cursor.getString(0);
                    cursor.close();
                    return string;
                }
            }
            cursor.close();
            return null;
        } catch (Throwable th) {
            cursor.close();
            throw th;
        }
    }

    public void BeginTransaction() {
        checkNull();
        this.db.beginTransaction();
    }

    public void TransactionSuccessful() {
        this.db.setTransactionSuccessful();
    }

    public void EndTransaction() {
        this.db.endTransaction();
    }

    public void Close() {
        if (this.db != null && this.db.isOpen()) {
            this.db.close();
        }
    }

    @BA.ShortName("Cursor")
    public static class CursorWrapper extends AbsObjectWrapper<Cursor> {
        public int getPosition() {
            return ((Cursor) getObject()).getPosition();
        }

        public void setPosition(int Value) {
            ((Cursor) getObject()).moveToPosition(Value);
        }

        public String GetColumnName(int Index) {
            return ((Cursor) getObject()).getColumnName(Index);
        }

        public int getRowCount() {
            return ((Cursor) getObject()).getCount();
        }

        public int getColumnCount() {
            return ((Cursor) getObject()).getColumnCount();
        }

        public int GetInt2(int Index) {
            return ((Cursor) getObject()).getInt(Index);
        }

        public int GetInt(String ColumnName) {
            return ((Cursor) getObject()).getInt(((Cursor) getObject()).getColumnIndexOrThrow(ColumnName));
        }

        public String GetString2(int Index) {
            return ((Cursor) getObject()).getString(Index);
        }

        public String GetString(String ColumnName) {
            return ((Cursor) getObject()).getString(((Cursor) getObject()).getColumnIndexOrThrow(ColumnName));
        }

        public Long GetLong2(int Index) {
            return Long.valueOf(((Cursor) getObject()).getLong(Index));
        }

        public Long GetLong(String ColumnName) {
            return Long.valueOf(((Cursor) getObject()).getLong(((Cursor) getObject()).getColumnIndexOrThrow(ColumnName)));
        }

        public Double GetDouble2(int Index) {
            return Double.valueOf(((Cursor) getObject()).getDouble(Index));
        }

        public Double GetDouble(String ColumnName) {
            return Double.valueOf(((Cursor) getObject()).getDouble(((Cursor) getObject()).getColumnIndexOrThrow(ColumnName)));
        }

        public byte[] GetBlob(String ColumnName) {
            return ((Cursor) getObject()).getBlob(((Cursor) getObject()).getColumnIndexOrThrow(ColumnName));
        }

        public byte[] GetBlob2(int Index) {
            return ((Cursor) getObject()).getBlob(Index);
        }

        public void Close() {
            ((Cursor) getObject()).close();
        }
    }
}
