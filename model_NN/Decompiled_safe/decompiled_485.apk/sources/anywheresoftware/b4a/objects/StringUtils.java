package anywheresoftware.b4a.objects;

import android.text.Layout;
import android.text.StaticLayout;
import android.widget.TextView;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.objects.collections.List;
import anywheresoftware.b4a.objects.streams.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.regex.Pattern;

@BA.ShortName("StringUtils")
public class StringUtils {
    public int MeasureMultilineTextHeight(TextView TextView, String Text) {
        StaticLayout sl = new StaticLayout(Text, TextView.getPaint(), (TextView.getLayoutParams().width - TextView.getPaddingLeft()) - TextView.getPaddingRight(), Layout.Alignment.ALIGN_NORMAL, 1.0f, Common.Density, true);
        return sl.getLineTop(sl.getLineCount());
    }

    public String EncodeBase64(byte[] Data) {
        return Base64.encodeBytes(Data);
    }

    public byte[] DecodeBase64(String Data) throws IOException {
        return Base64.decode(Data);
    }

    public String EncodeUrl(String Url, String CharSet) throws UnsupportedEncodingException {
        return URLEncoder.encode(Url, CharSet);
    }

    public String DecodeUrl(String Url, String CharSet) throws UnsupportedEncodingException {
        return URLDecoder.decode(Url, CharSet);
    }

    public static void SaveCSV(String Dir, String FileName, char SeparatorChar, List Table) throws IOException {
        SaveCSV2(Dir, FileName, SeparatorChar, Table, null);
    }

    public static void SaveCSV2(String Dir, String FileName, char SeparatorChar, List Table, List Headers) throws IOException {
        int colCount = ((String[]) Table.Get(0)).length;
        StringBuilder data = new StringBuilder();
        Pattern problemChars = Pattern.compile("[\"\\r\\n" + SeparatorChar + "]");
        if (Headers != null) {
            for (String Word : (java.util.List) Headers.getObject()) {
                data.append(Word(Word, problemChars, SeparatorChar));
            }
            data.setCharAt(data.length() - 1, 10);
        }
        for (int rowI = 0; rowI < Table.getSize(); rowI++) {
            String[] row = (String[]) Table.Get(rowI);
            for (int i = 0; i < colCount; i++) {
                data.append(Word(row[i], problemChars, SeparatorChar));
            }
            data.setCharAt(data.length() - 1, 10);
        }
        File.WriteString(Dir, FileName, data.toString());
    }

    private static String Word(String word, Pattern problemChars, char sep) {
        if (problemChars.matcher(word).find()) {
            word = Common.QUOTE + word + Common.QUOTE;
            int i = word.indexOf(34, 1);
            while (i > -1 && i < word.length() - 1) {
                word = String.valueOf(word.substring(0, i)) + Common.QUOTE + word.substring(i);
                i = word.indexOf(Common.QUOTE, i + 2);
            }
        }
        return String.valueOf(word) + sep;
    }

    public static List LoadCSV(String Dir, String FileName, char SeparatorChar) throws IOException {
        return LoadCSV2(Dir, FileName, SeparatorChar, null);
    }

    /* JADX INFO: Multiple debug info for r7v1 anywheresoftware.b4a.objects.collections.List: [D('Dir' java.lang.String), D('Table' anywheresoftware.b4a.objects.collections.List)] */
    /* JADX INFO: Multiple debug info for r1v11 java.lang.String: [D('i' int), D('ret' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v13 'i'  int: [D('i' int), D('ret' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v1 java.lang.String[]: [D('list' java.lang.String[]), D('Headers' anywheresoftware.b4a.objects.collections.List)] */
    /* JADX INFO: Multiple debug info for r0v3 java.lang.String[]: [D('list' java.lang.String[]), D('alFirstLine' java.util.ArrayList<java.lang.String>)] */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b4, code lost:
        if (r8.charAt(r2 + 1) != 13) goto L_0x0019;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b6, code lost:
        r4 = false;
        r3 = r1 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0019, code lost:
        r3 = r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static anywheresoftware.b4a.objects.collections.List LoadCSV2(java.lang.String r7, java.lang.String r8, char r9, anywheresoftware.b4a.objects.collections.List r10) throws java.io.IOException {
        /*
            r1 = 0
            java.lang.String r8 = anywheresoftware.b4a.objects.streams.File.ReadString(r7, r8)
            anywheresoftware.b4a.objects.collections.List r7 = new anywheresoftware.b4a.objects.collections.List
            r7.<init>()
            r7.Initialize()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r4 = 1
        L_0x0013:
            int r2 = r8.length()
            if (r1 < r2) goto L_0x0069
        L_0x0019:
            r3 = r1
        L_0x001a:
            int r1 = r8.length()
            r2 = 1
            int r1 = r1 - r2
            char r1 = r8.charAt(r1)
            r2 = 10
            if (r1 == r2) goto L_0x0184
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r8 = java.lang.String.valueOf(r8)
            r1.<init>(r8)
            if (r4 == 0) goto L_0x012f
            java.lang.String r8 = "\n"
        L_0x0035:
            java.lang.StringBuilder r8 = r1.append(r8)
            java.lang.String r8 = r8.toString()
            r2 = r8
        L_0x003e:
            int r1 = r0.size()
            if (r10 == 0) goto L_0x013e
            boolean r8 = r10.IsInitialized()
            if (r8 != 0) goto L_0x004d
            r10.Initialize()
        L_0x004d:
            java.util.Iterator r0 = r0.iterator()
        L_0x0051:
            boolean r8 = r0.hasNext()
            if (r8 != 0) goto L_0x0133
        L_0x0057:
            r8 = 1
            int[] r10 = new int[r8]
            r8 = 0
            r10[r8] = r3
        L_0x005d:
            r8 = 0
            r8 = r10[r8]
            int r0 = r2.length()
            r3 = 1
            int r0 = r0 - r3
            if (r8 < r0) goto L_0x0154
            return r7
        L_0x0069:
            char r2 = r8.charAt(r1)
            r3 = 34
            if (r2 != r3) goto L_0x00d9
            java.lang.String r2 = "\""
            int r3 = r1 + 1
            int r2 = r8.indexOf(r2, r3)
            r3 = 0
        L_0x007a:
            int r5 = r8.length()
            if (r2 >= r5) goto L_0x0083
            r5 = -1
            if (r2 > r5) goto L_0x00bd
        L_0x0083:
            int r1 = r1 + 1
            java.lang.String r1 = r8.substring(r1, r2)
            if (r3 == 0) goto L_0x0093
            java.lang.String r3 = "\"\""
            java.lang.String r5 = "\""
            java.lang.String r1 = r1.replace(r3, r5)
        L_0x0093:
            r0.add(r1)
            int r1 = r2 + 2
            int r3 = r2 + 1
            char r3 = r8.charAt(r3)
            r5 = 13
            if (r3 == r5) goto L_0x00ac
            int r3 = r2 + 1
            char r3 = r8.charAt(r3)
            r5 = 10
            if (r3 != r5) goto L_0x0013
        L_0x00ac:
            int r2 = r2 + 1
            char r2 = r8.charAt(r2)
            r3 = 13
            if (r2 != r3) goto L_0x0019
            r2 = 0
            int r1 = r1 + 1
            r4 = r2
            r3 = r1
            goto L_0x001a
        L_0x00bd:
            int r5 = r8.length()
            r6 = 1
            int r5 = r5 - r6
            if (r2 == r5) goto L_0x0083
            int r5 = r2 + 1
            char r5 = r8.charAt(r5)
            r6 = 34
            if (r5 != r6) goto L_0x0083
            r3 = 1
            java.lang.String r5 = "\""
            int r2 = r2 + 2
            int r2 = r8.indexOf(r5, r2)
            goto L_0x007a
        L_0x00d9:
            int r2 = r8.indexOf(r9, r1)
            r3 = 10
            int r3 = r8.indexOf(r3, r1)
            r5 = -1
            if (r3 != r5) goto L_0x00ff
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r8 = java.lang.String.valueOf(r8)
            r3.<init>(r8)
            java.lang.String r8 = "\n"
            java.lang.StringBuilder r8 = r3.append(r8)
            java.lang.String r8 = r8.toString()
            int r3 = r8.length()
            r5 = 1
            int r3 = r3 - r5
        L_0x00ff:
            if (r3 < r2) goto L_0x0104
            r5 = -1
            if (r2 != r5) goto L_0x0124
        L_0x0104:
            r2 = 1
            int r2 = r3 - r2
            char r2 = r8.charAt(r2)
            r5 = 13
            if (r2 != r5) goto L_0x0187
            r4 = 0
            int r2 = r3 + -1
            r3 = r4
        L_0x0113:
            java.lang.String r1 = r8.substring(r1, r2)
            r0.add(r1)
            if (r3 == 0) goto L_0x0122
            r1 = 1
        L_0x011d:
            int r1 = r1 + r2
            r4 = r3
            r3 = r1
            goto L_0x001a
        L_0x0122:
            r1 = 2
            goto L_0x011d
        L_0x0124:
            java.lang.String r1 = r8.substring(r1, r2)
            r0.add(r1)
            int r1 = r2 + 1
            goto L_0x0013
        L_0x012f:
            java.lang.String r8 = "\r\n"
            goto L_0x0035
        L_0x0133:
            java.lang.Object r8 = r0.next()
            java.lang.String r8 = (java.lang.String) r8
            r10.Add(r8)
            goto L_0x0051
        L_0x013e:
            java.lang.String[] r10 = new java.lang.String[r1]
            r8 = 0
        L_0x0141:
            int r5 = r10.length
            if (r8 < r5) goto L_0x0149
            r7.Add(r10)
            goto L_0x0057
        L_0x0149:
            java.lang.Object r5 = r0.get(r8)
            java.lang.String r5 = (java.lang.String) r5
            r10[r8] = r5
            int r8 = r8 + 1
            goto L_0x0141
        L_0x0154:
            java.lang.String[] r0 = new java.lang.String[r1]
            r8 = 0
        L_0x0157:
            r3 = 1
            int r3 = r1 - r3
            if (r8 < r3) goto L_0x0172
            if (r4 != 0) goto L_0x017b
            r3 = 13
            java.lang.String r3 = ReadWord(r2, r10, r3)
            r0[r8] = r3
            r8 = 0
            r3 = r10[r8]
            int r3 = r3 + 1
            r10[r8] = r3
        L_0x016d:
            r7.Add(r0)
            goto L_0x005d
        L_0x0172:
            java.lang.String r3 = ReadWord(r2, r10, r9)
            r0[r8] = r3
            int r8 = r8 + 1
            goto L_0x0157
        L_0x017b:
            r3 = 10
            java.lang.String r3 = ReadWord(r2, r10, r3)
            r0[r8] = r3
            goto L_0x016d
        L_0x0184:
            r2 = r8
            goto L_0x003e
        L_0x0187:
            r2 = r3
            r3 = r4
            goto L_0x0113
        */
        throw new UnsupportedOperationException("Method not decompiled: anywheresoftware.b4a.objects.StringUtils.LoadCSV2(java.lang.String, java.lang.String, char, anywheresoftware.b4a.objects.collections.List):anywheresoftware.b4a.objects.collections.List");
    }

    private static String ReadWord(String data, int[] ii, char sep) {
        if (data.charAt(ii[0]) == '\"') {
            int i2 = data.indexOf(Common.QUOTE, ii[0] + 1);
            boolean shouldReplaceQuotes = false;
            while (i2 < data.length() && i2 > -1 && i2 != data.length() - 1 && data.charAt(i2 + 1) == '\"') {
                shouldReplaceQuotes = true;
                i2 = data.indexOf(Common.QUOTE, i2 + 2);
            }
            String ret = data.substring(ii[0] + 1, i2);
            if (shouldReplaceQuotes) {
                ret = ret.replace("\"\"", Common.QUOTE);
            }
            ii[0] = i2 + 2;
            return ret;
        }
        int i22 = data.indexOf(sep, ii[0]);
        String ret2 = data.substring(ii[0], i22);
        ii[0] = i22 + 1;
        return ret2;
    }
}
