package anywheresoftware.b4a.objects;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.DynamicBuilder;
import anywheresoftware.b4a.keywords.LayoutBuilder;
import anywheresoftware.b4a.keywords.LayoutValues;
import java.util.HashMap;

@BA.ShortName("Panel")
@BA.ActivityObject
public class PanelWrapper extends ViewWrapper<ViewGroup> {
    @BA.Hide
    public void innerInitialize(final BA ba, final String eventName, boolean keepOldObject) {
        if (!keepOldObject) {
            setObject(new BALayout(ba.context));
        }
        super.innerInitialize(ba, eventName, true);
        if (ba.subExists(String.valueOf(eventName) + "_touch")) {
            ((ViewGroup) getObject()).setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    ba.raiseEvent(PanelWrapper.this.getObject(), String.valueOf(eventName) + "_touch", Integer.valueOf(event.getAction()), Float.valueOf(event.getX()), Float.valueOf(event.getY()));
                    return true;
                }
            });
        }
    }

    public void AddView(View View, int Left, int Top, int Width, int Height) {
        ((ViewGroup) getObject()).addView(View, new BALayout.LayoutParams(Left, Top, Width, Height));
    }

    public ConcreteViewWrapper GetView(int Index) {
        ConcreteViewWrapper c = new ConcreteViewWrapper();
        c.setObject(((ViewGroup) getObject()).getChildAt(Index));
        return c;
    }

    public void RemoveViewAt(int Index) {
        ((ViewGroup) getObject()).removeViewAt(Index);
    }

    public int getNumberOfViews() {
        return ((ViewGroup) getObject()).getChildCount();
    }

    public LayoutValues LoadLayout(String Layout, BA ba) throws Exception {
        return LayoutBuilder.loadLayout(Layout, ba, false, (ViewGroup) getObject());
    }

    @BA.Hide
    public static ViewGroup build(Object prev, HashMap<String, Object> props, boolean designer, Object tag) throws Exception {
        ViewGroup vg = (ViewGroup) prev;
        if (vg == null) {
            vg = new BALayout((Context) tag);
        }
        ViewGroup vg2 = (ViewGroup) ViewWrapper.build(vg, props, designer);
        vg2.setBackgroundDrawable((Drawable) DynamicBuilder.build(vg2, (HashMap) props.get("drawable"), designer, null));
        return vg2;
    }
}
