package anywheresoftware.b4a.keywords;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.ConnectorUtils;
import anywheresoftware.b4a.DynamicBuilder;
import anywheresoftware.b4a.objects.ViewWrapper;
import java.io.DataInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.HashMap;

@BA.Hide
public class LayoutBuilder {
    /* JADX INFO: Multiple debug info for r9v3 java.io.InputStream: [D('layout' java.lang.String), D('in' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r0v8 anywheresoftware.b4a.keywords.LayoutValues: [D('pos' int), D('device' anywheresoftware.b4a.keywords.LayoutValues)] */
    public static LayoutValues loadLayout(String layout, BA ba, boolean isActivity, ViewGroup parent) throws Exception {
        int variantIndex;
        String layout2 = layout.toLowerCase(BA.cul);
        if (!layout2.endsWith(".bal")) {
            layout2 = String.valueOf(layout2) + ".bal";
        }
        InputStream in = ba.activity.getAssets().open(layout2);
        DataInputStream din = new DataInputStream(in);
        int readInt = ConnectorUtils.readInt(din);
        for (int pos = ConnectorUtils.readInt(din); pos > 0; pos = (int) (((long) pos) - in.skip((long) pos))) {
        }
        int numberOfVariants = ConnectorUtils.readInt(din);
        LayoutValues chosen = null;
        LayoutValues device = Common.GetDeviceLayoutValues(ba);
        float distance = Float.MAX_VALUE;
        int i = 0;
        int variantIndex2 = 0;
        while (i < numberOfVariants) {
            LayoutValues test = LayoutValues.readFromStream(din);
            if (chosen == null) {
                chosen = test;
                distance = test.calcDistance(device);
                variantIndex = i;
            } else {
                float testDistance = test.calcDistance(device);
                if (testDistance < distance) {
                    chosen = test;
                    distance = testDistance;
                    variantIndex = i;
                } else {
                    variantIndex = variantIndex2;
                }
            }
            i++;
            variantIndex2 = variantIndex;
        }
        BALayout.setUserScale(chosen.Scale);
        loadLayoutHelper(ConnectorUtils.readMap(din), ba, ba.activity, parent, isActivity, "variant" + variantIndex2, true);
        din.close();
        return chosen;
    }

    /* JADX INFO: Multiple debug info for r14v6 java.lang.String: [D('act' android.view.ViewGroup), D('name' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r14v13 android.view.View: [D('firstCall' boolean), D('o' android.view.View)] */
    /* JADX INFO: Multiple debug info for r12v28 android.graphics.drawable.Drawable: [D('drawProps' java.util.HashMap<java.lang.String, java.lang.Object>), D('d' android.graphics.drawable.Drawable)] */
    private static void loadLayoutHelper(HashMap<String, Object> props, BA ba, Activity activity, ViewGroup parent, boolean root, String currentVariant, boolean firstCall) throws Exception {
        View o;
        HashMap<String, Object> variant = (HashMap) props.get(currentVariant);
        if (root || !firstCall) {
            ViewGroup act = root ? parent : null;
            props.put("left", variant.get("left"));
            props.put("top", variant.get("top"));
            props.put("width", variant.get("width"));
            props.put("height", variant.get("height"));
            View o2 = (View) DynamicBuilder.build(act, props, false, parent.getContext());
            if (!root) {
                String name = ((String) props.get("name")).toLowerCase(BA.cul);
                String cls = (String) props.get("type");
                if (cls.startsWith(".")) {
                    cls = "anywheresoftware.b4a.objects" + cls;
                }
                ViewWrapper ow = (ViewWrapper) Class.forName(cls).newInstance();
                try {
                    Field field = activity.getClass().getField("_" + name);
                    if (field != null) {
                        field.set(activity, ow);
                    }
                } catch (IllegalArgumentException e) {
                    throw new RuntimeException("Field " + name + " was declared with the wrong type.");
                } catch (NoSuchFieldException e2) {
                }
                ow.setObject(o2);
                ow.innerInitialize(ba, ((String) props.get("eventName")).toLowerCase(BA.cul), true);
                if (parent != null) {
                    parent.addView(o2, o2.getLayoutParams());
                    o = o2;
                }
            }
            o = o2;
        } else {
            parent.setBackgroundDrawable((Drawable) DynamicBuilder.build(parent, (HashMap) props.get("drawable"), false, null));
            o = parent;
        }
        HashMap<String, Object> kids = (HashMap) props.get(":kids");
        if (kids != null) {
            for (int i = 0; i < kids.size(); i++) {
                loadLayoutHelper((HashMap) kids.get(String.valueOf(i)), ba, activity, (ViewGroup) o, false, currentVariant, false);
            }
        }
    }
}
