package com.santaclauspuzzle.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class WallpaperDbAdapter {
    /* access modifiers changed from: private */
    public static final String DATABASE_CREATE = ("create table " + DATABASE_TABLE + " (" + ITEM_ID + " integer primary key);");
    private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "WallpaperPuzzle";
    private static final int DATABASE_VERSION = 1;
    public static final String ITEM_ID = "id";
    private static final String TAG = "WallpaperDbAdapter";
    private final Context mCtx;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, WallpaperDbAdapter.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(WallpaperDbAdapter.DATABASE_CREATE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(WallpaperDbAdapter.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS WallpaperPuzzle");
            onCreate(db);
        }
    }

    public WallpaperDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public WallpaperDbAdapter open() throws SQLException {
        this.mDbHelper = new DatabaseHelper(this.mCtx);
        this.mDb = this.mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.mDbHelper.close();
    }

    public long createItem(Integer itemId) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(ITEM_ID, itemId);
        return this.mDb.insert(DATABASE_TABLE, null, initialValues);
    }

    public boolean deleteItem(long rowId) {
        return this.mDb.delete(DATABASE_TABLE, new StringBuilder().append("id=").append(rowId).toString(), null) > 0;
    }

    public Cursor fetchAllItems() {
        return this.mDb.query(DATABASE_TABLE, new String[]{ITEM_ID}, null, null, null, null, null);
    }

    public Cursor fetchItem(long rowId) throws SQLException {
        Cursor mCursor = this.mDb.query(true, DATABASE_TABLE, new String[]{ITEM_ID}, "id=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }
}
