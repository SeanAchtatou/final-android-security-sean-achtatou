package com.santaclauspuzzle.game;

public enum GameStatus {
    beforeStart,
    started,
    completed,
    wallpaper
}
