package com.zeddev.rudeboard;

import android.app.Activity;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.util.HashMap;

public class thebeatboard extends Activity {
    public static final int SOUND1 = 1;
    public static final int SOUND10 = 10;
    public static final int SOUND11 = 11;
    public static final int SOUND12 = 12;
    public static final int SOUND13 = 13;
    public static final int SOUND14 = 14;
    public static final int SOUND15 = 15;
    public static final int SOUND2 = 2;
    public static final int SOUND3 = 3;
    public static final int SOUND4 = 4;
    public static final int SOUND5 = 5;
    public static final int SOUND6 = 6;
    public static final int SOUND7 = 7;
    public static final int SOUND8 = 8;
    public static final int SOUND9 = 9;
    Button btn1 = null;
    Button btn10 = null;
    Button btn11 = null;
    Button btn12 = null;
    Button btn13 = null;
    Button btn14 = null;
    Button btn15 = null;
    Button btn2 = null;
    Button btn3 = null;
    Button btn4 = null;
    Button btn5 = null;
    Button btn6 = null;
    Button btn7 = null;
    Button btn8 = null;
    Button btn9 = null;
    /* access modifiers changed from: private */
    public SoundPool soundPool;
    /* access modifiers changed from: private */
    public HashMap<Integer, Integer> soundPoolMap;
    ImageButton stopbut = null;

    public void onPause() {
        super.onPause();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        ((AdView) findViewById(R.id.adView2)).loadAd(new AdRequest());
        this.soundPool = new SoundPool(4, 3, 0);
        this.soundPoolMap = new HashMap<>();
        this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.belch, 1)));
        this.soundPoolMap.put(2, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.burp4, 1)));
        this.soundPoolMap.put(3, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.burp_3, 1)));
        this.soundPoolMap.put(4, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.burp_2, 1)));
        this.soundPoolMap.put(5, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.big_belch, 1)));
        this.soundPoolMap.put(6, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.fart, 1)));
        this.soundPoolMap.put(7, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.fart4, 1)));
        this.soundPoolMap.put(8, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.fart_2, 1)));
        this.soundPoolMap.put(9, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.fart_3, 1)));
        this.soundPoolMap.put(10, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.fart_1, 1)));
        this.soundPoolMap.put(11, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.puke, 1)));
        this.soundPoolMap.put(12, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.pukewet, 1)));
        this.soundPoolMap.put(13, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.vomiting_1, 1)));
        this.soundPoolMap.put(14, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.squelch1, 1)));
        this.soundPoolMap.put(15, Integer.valueOf(this.soundPool.load(getBaseContext(), R.raw.slurp, 1)));
        View.OnClickListener startListener = new View.OnClickListener() {
            public void onClick(View v) {
                if (v.equals(thebeatboard.this.btn1)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(1)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn2)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(2)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn3)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(3)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn4)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(4)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn5)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(5)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn6)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(6)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn7)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(7)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn8)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(8)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn9)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(9)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn10)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(10)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn11)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(11)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn12)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(12)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn13)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(13)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn14)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(14)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
                if (v.equals(thebeatboard.this.btn15)) {
                    thebeatboard.this.soundPool.play(((Integer) thebeatboard.this.soundPoolMap.get(15)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
                }
            }
        };
        this.btn1 = (Button) findViewById(R.id.Button01);
        this.btn1.setOnClickListener(startListener);
        this.btn2 = (Button) findViewById(R.id.Button02);
        this.btn2.setOnClickListener(startListener);
        this.btn3 = (Button) findViewById(R.id.Button03);
        this.btn3.setOnClickListener(startListener);
        this.btn4 = (Button) findViewById(R.id.Button04);
        this.btn4.setOnClickListener(startListener);
        this.btn5 = (Button) findViewById(R.id.Button05);
        this.btn5.setOnClickListener(startListener);
        this.btn6 = (Button) findViewById(R.id.Button06);
        this.btn6.setOnClickListener(startListener);
        this.btn7 = (Button) findViewById(R.id.Button07);
        this.btn7.setOnClickListener(startListener);
        this.btn8 = (Button) findViewById(R.id.Button08);
        this.btn8.setOnClickListener(startListener);
        this.btn9 = (Button) findViewById(R.id.Button09);
        this.btn9.setOnClickListener(startListener);
        this.btn10 = (Button) findViewById(R.id.Button10);
        this.btn10.setOnClickListener(startListener);
        this.btn11 = (Button) findViewById(R.id.Button11);
        this.btn11.setOnClickListener(startListener);
        this.btn12 = (Button) findViewById(R.id.Button12);
        this.btn12.setOnClickListener(startListener);
        this.btn13 = (Button) findViewById(R.id.Button13);
        this.btn13.setOnClickListener(startListener);
        this.btn14 = (Button) findViewById(R.id.Button14);
        this.btn14.setOnClickListener(startListener);
        this.btn15 = (Button) findViewById(R.id.Button15);
        this.btn15.setOnClickListener(startListener);
    }
}
