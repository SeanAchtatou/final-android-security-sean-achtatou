package defpackage;

import android.app.Activity;
import android.os.AsyncTask;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.InterstitialAd;
import defpackage.a;

/* renamed from: c  reason: default package */
public final class c extends AsyncTask<AdRequest, String, AdRequest.ErrorCode> {
    private static final String a;
    private static final String b = ("<html><head><script src=\"" + a + "\"></script><script>");
    private static final Object c = new Object();
    private static long d = 5000;
    private String e;
    private String f = null;
    private b g;
    private d h;
    private WebView i;
    private String j = null;
    private AdRequest.ErrorCode k = null;
    private boolean l = false;
    private boolean m = false;

    /* renamed from: c$a */
    private class a extends Exception {
        public a(String str) {
            super(str);
        }
    }

    static {
        String[] split = AdRequest.VERSION.split("\\.");
        String str = split[0];
        a = "http://www.gstatic.com/afma/sdk-core-v" + str + split[1] + ".js";
    }

    c(d dVar) {
        this.h = dVar;
        Activity c2 = dVar.c();
        if (c2 != null) {
            this.i = new WebView(c2.getApplicationContext());
            this.i.getSettings().setJavaScriptEnabled(true);
            this.i.setWebViewClient(new h(dVar, a.C0000a.URL_REQUEST_TYPE, false));
            this.g = new b(this, dVar, u.a(this.i));
            return;
        }
        this.i = null;
        this.g = null;
        t.e("activity was null while trying to create an AdLoader.");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.ads.AdRequest.ErrorCode doInBackground(com.google.ads.AdRequest... r14) {
        /*
            r13 = this;
            r12 = 0
            r10 = 0
            monitor-enter(r13)
            android.webkit.WebView r0 = r13.i     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x000c
            b r0 = r13.g     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x0015
        L_0x000c:
            java.lang.String r0 = "adRequestWebView was null while trying to load an ad."
            defpackage.t.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
        L_0x0014:
            return r0
        L_0x0015:
            r0 = 0
            r0 = r14[r0]     // Catch:{ all -> 0x0029 }
            d r1 = r13.h     // Catch:{ all -> 0x0029 }
            android.app.Activity r1 = r1.c()     // Catch:{ all -> 0x0029 }
            if (r1 != 0) goto L_0x002c
            java.lang.String r0 = "activity was null while forming an ad request."
            defpackage.t.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0029:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x002c:
            android.content.Context r2 = r1.getApplicationContext()     // Catch:{ a -> 0x0114 }
            java.util.Map r0 = r0.getRequestMap()     // Catch:{ a -> 0x0114 }
            d r3 = r13.h     // Catch:{ a -> 0x0114 }
            f r3 = r3.j()     // Catch:{ a -> 0x0114 }
            long r4 = r3.h()     // Catch:{ a -> 0x0114 }
            int r6 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x004b
            java.lang.String r6 = "prl"
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ a -> 0x0114 }
            r0.put(r6, r4)     // Catch:{ a -> 0x0114 }
        L_0x004b:
            java.lang.String r4 = r3.g()     // Catch:{ a -> 0x0114 }
            if (r4 == 0) goto L_0x0056
            java.lang.String r5 = "ppcl"
            r0.put(r5, r4)     // Catch:{ a -> 0x0114 }
        L_0x0056:
            java.lang.String r4 = r3.f()     // Catch:{ a -> 0x0114 }
            if (r4 == 0) goto L_0x0061
            java.lang.String r5 = "pcl"
            r0.put(r5, r4)     // Catch:{ a -> 0x0114 }
        L_0x0061:
            long r4 = r3.e()     // Catch:{ a -> 0x0114 }
            int r6 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x0072
            java.lang.String r6 = "pcc"
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ a -> 0x0114 }
            r0.put(r6, r4)     // Catch:{ a -> 0x0114 }
        L_0x0072:
            java.lang.String r4 = "preqs"
            long r5 = defpackage.f.i()     // Catch:{ a -> 0x0114 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ a -> 0x0114 }
            r0.put(r4, r5)     // Catch:{ a -> 0x0114 }
            java.lang.String r4 = r3.j()     // Catch:{ a -> 0x0114 }
            if (r4 == 0) goto L_0x008a
            java.lang.String r5 = "pai"
            r0.put(r5, r4)     // Catch:{ a -> 0x0114 }
        L_0x008a:
            boolean r4 = r3.k()     // Catch:{ a -> 0x0114 }
            if (r4 == 0) goto L_0x0097
            java.lang.String r4 = "aoi_timeout"
            java.lang.String r5 = "true"
            r0.put(r4, r5)     // Catch:{ a -> 0x0114 }
        L_0x0097:
            boolean r4 = r3.m()     // Catch:{ a -> 0x0114 }
            if (r4 == 0) goto L_0x00a4
            java.lang.String r4 = "aoi_nofill"
            java.lang.String r5 = "true"
            r0.put(r4, r5)     // Catch:{ a -> 0x0114 }
        L_0x00a4:
            java.lang.String r4 = r3.p()     // Catch:{ a -> 0x0114 }
            if (r4 == 0) goto L_0x00af
            java.lang.String r5 = "pit"
            r0.put(r5, r4)     // Catch:{ a -> 0x0114 }
        L_0x00af:
            r3.a()     // Catch:{ a -> 0x0114 }
            r3.d()     // Catch:{ a -> 0x0114 }
            d r3 = r13.h     // Catch:{ a -> 0x0114 }
            com.google.ads.Ad r3 = r3.d()     // Catch:{ a -> 0x0114 }
            boolean r3 = r3 instanceof com.google.ads.InterstitialAd     // Catch:{ a -> 0x0114 }
            if (r3 == 0) goto L_0x011f
            java.lang.String r3 = "format"
            java.lang.String r4 = "interstitial_mb"
            r0.put(r3, r4)     // Catch:{ a -> 0x0114 }
        L_0x00c6:
            java.lang.String r3 = "slotname"
            d r4 = r13.h     // Catch:{ a -> 0x0114 }
            java.lang.String r4 = r4.f()     // Catch:{ a -> 0x0114 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0114 }
            java.lang.String r3 = "js"
            java.lang.String r4 = "afma-sdk-a-v4.0.4"
            r0.put(r3, r4)     // Catch:{ a -> 0x0114 }
            java.lang.String r3 = "msid"
            java.lang.String r4 = r2.getPackageName()     // Catch:{ a -> 0x0114 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0114 }
            java.lang.String r3 = "app_name"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ a -> 0x0114 }
            r4.<init>()     // Catch:{ a -> 0x0114 }
            java.lang.String r5 = "4.0.4.android."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ a -> 0x0114 }
            java.lang.String r5 = r2.getPackageName()     // Catch:{ a -> 0x0114 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ a -> 0x0114 }
            java.lang.String r4 = r4.toString()     // Catch:{ a -> 0x0114 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0114 }
            java.lang.String r3 = "isu"
            java.lang.String r4 = defpackage.u.a(r2)     // Catch:{ a -> 0x0114 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0114 }
            java.lang.String r3 = defpackage.u.c(r2)     // Catch:{ a -> 0x0114 }
            if (r3 != 0) goto L_0x0157
            c$a r0 = new c$a     // Catch:{ a -> 0x0114 }
            java.lang.String r1 = "NETWORK_ERROR"
            r0.<init>(r1)     // Catch:{ a -> 0x0114 }
            throw r0     // Catch:{ a -> 0x0114 }
        L_0x0114:
            r0 = move-exception
            java.lang.String r0 = "Unable to connect to network."
            defpackage.t.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x011f:
            d r3 = r13.h     // Catch:{ a -> 0x0114 }
            com.google.ads.AdSize r3 = r3.i()     // Catch:{ a -> 0x0114 }
            java.lang.String r4 = r3.toString()     // Catch:{ a -> 0x0114 }
            if (r4 == 0) goto L_0x0131
            java.lang.String r3 = "format"
            r0.put(r3, r4)     // Catch:{ a -> 0x0114 }
            goto L_0x00c6
        L_0x0131:
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ a -> 0x0114 }
            r4.<init>()     // Catch:{ a -> 0x0114 }
            java.lang.String r5 = "w"
            int r6 = r3.getWidth()     // Catch:{ a -> 0x0114 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ a -> 0x0114 }
            r4.put(r5, r6)     // Catch:{ a -> 0x0114 }
            java.lang.String r5 = "h"
            int r3 = r3.getHeight()     // Catch:{ a -> 0x0114 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ a -> 0x0114 }
            r4.put(r5, r3)     // Catch:{ a -> 0x0114 }
            java.lang.String r3 = "ad_frame"
            r0.put(r3, r4)     // Catch:{ a -> 0x0114 }
            goto L_0x00c6
        L_0x0157:
            java.lang.String r4 = "net"
            r0.put(r4, r3)     // Catch:{ a -> 0x0114 }
            java.lang.String r3 = defpackage.u.d(r2)     // Catch:{ a -> 0x0114 }
            if (r3 == 0) goto L_0x016d
            int r4 = r3.length()     // Catch:{ a -> 0x0114 }
            if (r4 == 0) goto L_0x016d
            java.lang.String r4 = "cap"
            r0.put(r4, r3)     // Catch:{ a -> 0x0114 }
        L_0x016d:
            java.lang.String r3 = "u_audio"
            u$a r4 = defpackage.u.e(r2)     // Catch:{ a -> 0x0114 }
            int r4 = r4.ordinal()     // Catch:{ a -> 0x0114 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ a -> 0x0114 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0114 }
            java.lang.String r3 = "u_so"
            java.lang.String r2 = defpackage.u.f(r2)     // Catch:{ a -> 0x0114 }
            r0.put(r3, r2)     // Catch:{ a -> 0x0114 }
            android.util.DisplayMetrics r1 = defpackage.u.a(r1)     // Catch:{ a -> 0x0114 }
            java.lang.String r2 = "u_sd"
            float r3 = r1.density     // Catch:{ a -> 0x0114 }
            java.lang.Float r3 = java.lang.Float.valueOf(r3)     // Catch:{ a -> 0x0114 }
            r0.put(r2, r3)     // Catch:{ a -> 0x0114 }
            java.lang.String r2 = "u_h"
            int r3 = r1.heightPixels     // Catch:{ a -> 0x0114 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ a -> 0x0114 }
            r0.put(r2, r3)     // Catch:{ a -> 0x0114 }
            java.lang.String r2 = "u_w"
            int r1 = r1.widthPixels     // Catch:{ a -> 0x0114 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ a -> 0x0114 }
            r0.put(r2, r1)     // Catch:{ a -> 0x0114 }
            java.lang.String r1 = "hl"
            java.util.Locale r2 = java.util.Locale.getDefault()     // Catch:{ a -> 0x0114 }
            java.lang.String r2 = r2.getLanguage()     // Catch:{ a -> 0x0114 }
            r0.put(r1, r2)     // Catch:{ a -> 0x0114 }
            boolean r1 = defpackage.u.a()     // Catch:{ a -> 0x0114 }
            if (r1 == 0) goto L_0x01c9
            java.lang.String r1 = "simulator"
            r2 = 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ a -> 0x0114 }
            r0.put(r1, r2)     // Catch:{ a -> 0x0114 }
        L_0x01c9:
            com.google.gson.Gson r1 = new com.google.gson.Gson     // Catch:{ a -> 0x0114 }
            r1.<init>()     // Catch:{ a -> 0x0114 }
            java.lang.String r0 = r1.toJson(r0)     // Catch:{ a -> 0x0114 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ a -> 0x0114 }
            r1.<init>()     // Catch:{ a -> 0x0114 }
            java.lang.String r2 = defpackage.c.b     // Catch:{ a -> 0x0114 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ a -> 0x0114 }
            java.lang.String r2 = "AFMA_buildAdURL"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ a -> 0x0114 }
            java.lang.String r2 = "("
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ a -> 0x0114 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ a -> 0x0114 }
            java.lang.String r1 = ");"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x0114 }
            java.lang.String r1 = "</script></head><body></body></html>"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x0114 }
            java.lang.String r2 = r0.toString()     // Catch:{ a -> 0x0114 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ a -> 0x0114 }
            r0.<init>()     // Catch:{ a -> 0x0114 }
            java.lang.String r1 = "adRequestUrlHtml: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x0114 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ a -> 0x0114 }
            java.lang.String r0 = r0.toString()     // Catch:{ a -> 0x0114 }
            defpackage.t.c(r0)     // Catch:{ a -> 0x0114 }
            android.webkit.WebView r0 = r13.i     // Catch:{ all -> 0x0029 }
            r1 = 0
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0029 }
            long r6 = r13.b()     // Catch:{ all -> 0x0029 }
            long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            int r0 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x022d
            r13.wait(r6)     // Catch:{ InterruptedException -> 0x0236 }
        L_0x022d:
            com.google.ads.AdRequest$ErrorCode r0 = r13.k     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0252
            com.google.ads.AdRequest$ErrorCode r0 = r13.k     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0236:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r1.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the URL: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            defpackage.t.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0252:
            java.lang.String r0 = r13.j     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x0260
            java.lang.String r0 = "AdLoader timed out while getting the URL."
            defpackage.t.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0260:
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ all -> 0x0029 }
            r1 = 0
            java.lang.String r2 = r13.j     // Catch:{ all -> 0x0029 }
            r0[r1] = r2     // Catch:{ all -> 0x0029 }
            r13.publishProgress(r0)     // Catch:{ all -> 0x0029 }
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            long r0 = r0 - r8
            long r0 = r6 - r0
            int r2 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r2 <= 0) goto L_0x0279
            r13.wait(r0)     // Catch:{ InterruptedException -> 0x0282 }
        L_0x0279:
            com.google.ads.AdRequest$ErrorCode r0 = r13.k     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x029e
            com.google.ads.AdRequest$ErrorCode r0 = r13.k     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0282:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r1.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the HTML: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            defpackage.t.e(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x029e:
            java.lang.String r0 = r13.f     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x02ac
            java.lang.String r0 = "AdLoader timed out while getting the HTML."
            defpackage.t.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x02ac:
            d r0 = r13.h     // Catch:{ all -> 0x0029 }
            g r0 = r0.g()     // Catch:{ all -> 0x0029 }
            d r1 = r13.h     // Catch:{ all -> 0x0029 }
            h r1 = r1.h()     // Catch:{ all -> 0x0029 }
            r1.a()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = r13.e     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = r13.f     // Catch:{ all -> 0x0029 }
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0029 }
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            long r1 = r1 - r8
            long r1 = r6 - r1
            int r3 = (r1 > r10 ? 1 : (r1 == r10 ? 0 : -1))
            if (r3 <= 0) goto L_0x02d5
            r13.wait(r1)     // Catch:{ InterruptedException -> 0x02dd }
        L_0x02d5:
            boolean r1 = r13.m     // Catch:{ all -> 0x0029 }
            if (r1 == 0) goto L_0x02fc
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            r0 = r12
            goto L_0x0014
        L_0x02dd:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r2.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r3 = "AdLoader InterruptedException while loading the HTML: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0029 }
            defpackage.t.e(r1)     // Catch:{ all -> 0x0029 }
            r0.stopLoading()     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x02fc:
            r0.stopLoading()     // Catch:{ all -> 0x0029 }
            r0 = 1
            r13.l = r0     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = "AdLoader timed out while loading the HTML."
            defpackage.t.c(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.doInBackground(com.google.ads.AdRequest[]):com.google.ads.AdRequest$ErrorCode");
    }

    public static void a(long j2) {
        synchronized (c) {
            d = j2;
        }
    }

    private long b() {
        long j2;
        if (!(this.h.d() instanceof InterstitialAd)) {
            return 60000;
        }
        synchronized (c) {
            j2 = d;
        }
        return j2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.m = true;
        notify();
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.k = errorCode;
        notify();
    }

    public final synchronized void a(String str) {
        this.j = str;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.e = str2;
        this.f = str;
        notify();
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        t.a("AdLoader cancelled.");
        this.i.stopLoading();
        this.g.cancel(false);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object x0) {
        AdRequest.ErrorCode errorCode = (AdRequest.ErrorCode) x0;
        synchronized (this) {
            if (errorCode == null) {
                this.h.n();
            } else {
                this.i.stopLoading();
                this.g.cancel(false);
                if (this.l) {
                    this.h.g().setVisibility(8);
                }
                this.h.a(errorCode);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onProgressUpdate(Object[] x0) {
        this.g.execute(((String[]) x0)[0]);
    }
}
