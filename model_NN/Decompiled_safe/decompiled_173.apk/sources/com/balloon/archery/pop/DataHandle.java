package com.balloon.archery.pop;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

public class DataHandle extends Activity {
    String button_pressed = "";
    GameView gameview;
    int value = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.resume);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.value = extras.getInt("high_score");
        }
        String temp_score = Integer.toString(this.value);
        SQLiteDatabase db = openOrCreateDatabase("level.db", 0, null);
        db.execSQL("create table if not exists highscore(highest_score text not null);");
        String temp_high_score = "";
        Cursor c = db.rawQuery("select * from highscore;", null);
        if (c.getCount() <= 0) {
            ContentValues cv_h = new ContentValues();
            cv_h.put("highest_score", temp_score);
            db.insert("highscore", null, cv_h);
        } else {
            if (c.moveToFirst()) {
                do {
                    temp_high_score = c.getString(c.getColumnIndex("highest_score"));
                } while (c.moveToNext());
            }
            if (this.value >= Integer.parseInt(temp_high_score)) {
                String new_hs = Integer.toString(this.value);
                ContentValues cv_h2 = new ContentValues();
                cv_h2.put("highest_score", new_hs);
                db.update("highscore", cv_h2, "highest_score='" + temp_high_score + "'", null);
            }
        }
        c.close();
        db.close();
        finish();
    }
}
