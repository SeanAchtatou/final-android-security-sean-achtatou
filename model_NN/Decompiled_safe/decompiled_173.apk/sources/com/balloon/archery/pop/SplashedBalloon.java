package com.balloon.archery.pop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class SplashedBalloon {
    Bitmap bmp;
    GameView gameview;
    int life = 3;
    boolean life_over = false;
    float x;
    float y;

    public SplashedBalloon(GameView gameview2, Bitmap bmp2, float x2, float y2) {
        this.x = x2;
        this.y = y2;
        this.bmp = bmp2;
    }

    public void onDraw(Canvas canvas) {
        update();
        canvas.drawBitmap(this.bmp, this.x, this.y, (Paint) null);
    }

    private void update() {
        int i = this.life - 1;
        this.life = i;
        if (i < 1) {
            this.life_over = true;
        }
    }
}
