package com.balloon.archery.pop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class IncreaseLife {
    Bitmap bmp;
    GameView gameview;
    int life = 10;
    boolean lifeplus_life_over = false;
    private float scale;
    float x = 0.0f;
    float y = 0.0f;
    int ySpeed = 0;

    public IncreaseLife(GameView gameview2, Bitmap bmp2, float X, float Y) {
        this.gameview = gameview2;
        this.bmp = bmp2;
        this.x = X;
        this.y = Y;
        this.scale = gameview2.getResources().getDisplayMetrics().density;
    }

    public void update() {
        int i = this.life - 1;
        this.life = i;
        if (i < 1) {
            this.lifeplus_life_over = true;
        }
    }

    public void onDraw(Canvas canvas) {
        update();
        canvas.drawBitmap(this.bmp, this.x, this.y, (Paint) null);
    }
}
