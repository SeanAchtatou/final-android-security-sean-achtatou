package com.balloon.archery.pop;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int arrow = 2130837504;
        public static final int arrow_bird = 2130837505;
        public static final int background = 2130837506;
        public static final int background_1_new = 2130837507;
        public static final int background_2 = 2130837508;
        public static final int background_3 = 2130837509;
        public static final int background_new = 2130837510;
        public static final int ballon_logo = 2130837511;
        public static final int balloon_button = 2130837512;
        public static final int balloon_icon = 2130837513;
        public static final int bird_image = 2130837514;
        public static final int black_balloon = 2130837515;
        public static final int blue_10_1 = 2130837516;
        public static final int blue_15_1 = 2130837517;
        public static final int blue_20_1 = 2130837518;
        public static final int blue_5_1 = 2130837519;
        public static final int blue_splash_10 = 2130837520;
        public static final int blue_splashed_15 = 2130837521;
        public static final int blue_splashed_20 = 2130837522;
        public static final int bow_pulled = 2130837523;
        public static final int bow_pulled_bird = 2130837524;
        public static final int bow_regular = 2130837525;
        public static final int bow_regular_bird = 2130837526;
        public static final int game_over = 2130837527;
        public static final int green_10_1 = 2130837528;
        public static final int green_15_1 = 2130837529;
        public static final int green_20_1 = 2130837530;
        public static final int green_5_1 = 2130837531;
        public static final int green_splash_10 = 2130837532;
        public static final int green_splashed_15 = 2130837533;
        public static final int green_splashed_20 = 2130837534;
        public static final int high_scores_button = 2130837535;
        public static final int how_to_play_button = 2130837536;
        public static final int icon = 2130837537;
        public static final int increase_life = 2130837538;
        public static final int level_10_completed = 2130837539;
        public static final int level_11_completed = 2130837540;
        public static final int level_12_completed = 2130837541;
        public static final int level_13_completed = 2130837542;
        public static final int level_14_completed = 2130837543;
        public static final int level_15_completed = 2130837544;
        public static final int level_16_completed = 2130837545;
        public static final int level_17_completed = 2130837546;
        public static final int level_18_completed = 2130837547;
        public static final int level_19_completed = 2130837548;
        public static final int level_1_completed = 2130837549;
        public static final int level_20_completed = 2130837550;
        public static final int level_2_completed = 2130837551;
        public static final int level_3_completed = 2130837552;
        public static final int level_4_completed = 2130837553;
        public static final int level_5_completed = 2130837554;
        public static final int level_6_completed = 2130837555;
        public static final int level_7_completed = 2130837556;
        public static final int level_8_completed = 2130837557;
        public static final int level_9_completed = 2130837558;
        public static final int lifeheart = 2130837559;
        public static final int lifeheart_balloon = 2130837560;
        public static final int new_game_button = 2130837561;
        public static final int pink_10_1 = 2130837562;
        public static final int pink_15_1 = 2130837563;
        public static final int pink_20_1 = 2130837564;
        public static final int pink_5_1 = 2130837565;
        public static final int pink_splash_10 = 2130837566;
        public static final int pink_splashed_15 = 2130837567;
        public static final int pink_splashed_20 = 2130837568;
        public static final int red_10_1 = 2130837569;
        public static final int red_15_1 = 2130837570;
        public static final int red_20_1 = 2130837571;
        public static final int red_5_1 = 2130837572;
        public static final int red_splash_10 = 2130837573;
        public static final int red_splashed_15 = 2130837574;
        public static final int red_splashed_20 = 2130837575;
        public static final int resume_button = 2130837576;
        public static final int splash_blue_5 = 2130837577;
        public static final int splash_green_5 = 2130837578;
        public static final int splash_pink_5 = 2130837579;
        public static final int splash_red_5 = 2130837580;
        public static final int splash_yellow_5 = 2130837581;
        public static final int start_button = 2130837582;
        public static final int string_10_1 = 2130837583;
        public static final int string_15_1 = 2130837584;
        public static final int string_15_2 = 2130837585;
        public static final int string_20_3 = 2130837586;
        public static final int string_5_3 = 2130837587;
        public static final int yellow_10_1 = 2130837588;
        public static final int yellow_15_1 = 2130837589;
        public static final int yellow_20_1 = 2130837590;
        public static final int yellow_5_1 = 2130837591;
        public static final int yellow_splash_10 = 2130837592;
        public static final int yellow_splashed_15 = 2130837593;
        public static final int yellow_splashed_20 = 2130837594;
    }

    public static final class id {
        public static final int button_balloon = 2131099671;
        public static final int button_bird = 2131099672;
        public static final int button_glass = 2131099674;
        public static final int button_help = 2131099669;
        public static final int button_highscores = 2131099667;
        public static final int button_number = 2131099673;
        public static final int button_start = 2131099662;
        public static final int highScore_relative = 2131099657;
        public static final int high_score_text = 2131099658;
        public static final int linearLayouthelp = 2131099648;
        public static final int new_game_button = 2131099665;
        public static final int ok = 2131099656;
        public static final int resume_button = 2131099664;
        public static final int rule_1 = 2131099649;
        public static final int rule_1_desc = 2131099650;
        public static final int rule_2 = 2131099651;
        public static final int rule_2_desc = 2131099652;
        public static final int rule_3 = 2131099653;
        public static final int rule_3_desc = 2131099654;
        public static final int table = 2131099666;
        public static final int tableLayout1 = 2131099655;
        public static final int table_1 = 2131099661;
        public static final int table_2 = 2131099670;
        public static final int table_above_new = 2131099663;
        public static final int table_below_new = 2131099668;
        public static final int textView2 = 2131099659;
        public static final int title_table = 2131099660;
    }

    public static final class layout {
        public static final int help = 2130903040;
        public static final int high_scores = 2130903041;
        public static final int main = 2130903042;
        public static final int resume = 2130903043;
        public static final int stages = 2130903044;
    }

    public static final class raw {
        public static final int background_music = 2130968576;
        public static final int bow_fire = 2130968577;
        public static final int splash_sound = 2130968578;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
        public static final int ok = 2131034120;
        public static final int rule_1 = 2131034117;
        public static final int rule_1_desc = 2131034114;
        public static final int rule_2 = 2131034118;
        public static final int rule_2_desc = 2131034115;
        public static final int rule_3 = 2131034119;
        public static final int rule_3_desc = 2131034116;
    }
}
