package com.balloon.archery.pop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StageHandler extends Activity {
    Button balloon;
    Button bird;
    Button glass;
    Button number;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.stages);
        this.balloon = (Button) findViewById(R.id.button_balloon);
        this.balloon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                StageHandler.this.startActivityForResult(new Intent(StageHandler.this, Resume.class), 0);
            }
        });
        this.glass = (Button) findViewById(R.id.button_glass);
        this.glass.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.bird = (Button) findViewById(R.id.button_bird);
        this.bird.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.number = (Button) findViewById(R.id.button_number);
        this.number.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }
}
