package com.balloon.archery.pop;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class StartGame extends Activity {
    private float adParamSize = 50.0f;
    private AdView adView;
    GameView gameview;
    private RelativeLayout layout;
    private LinearLayout mainLayout;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mainLayout = new LinearLayout(this);
        this.mainLayout.setOrientation(1);
        this.layout = new RelativeLayout(this);
        this.adView = new AdView(this, AdSize.BANNER, "a14e2aa89718a81");
        this.adView.setVisibility(0);
        this.adView.loadAd(new AdRequest());
        LinearLayout.LayoutParams adparam = new LinearLayout.LayoutParams(-2, -2);
        adparam.height = (int) ((this.adParamSize * getResources().getDisplayMetrics().density) + 0.5f);
        this.adView.setLayoutParams(adparam);
        this.mainLayout.addView(this.adView);
        this.mainLayout.addView(this.layout);
        this.gameview = new GameView(this, 0, 0, 10);
        this.gameview.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        this.layout.addView(this.gameview);
        setContentView(this.mainLayout);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        new AlertDialog.Builder(this).setTitle("ALERT....!").setMessage("Do you want to save the current game ?").setCancelable(true).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SQLiteDatabase db = StartGame.this.openOrCreateDatabase("level.db", 0, null);
                db.execSQL("drop table if exists stagedata;");
                db.execSQL("create table if not exists stagedata(level_name text not null, score text,life text, PRIMARY KEY(level_name));");
                db.execSQL("create table if not exists highscore(highest_score text not null);");
                String temp_level = Integer.toString(StartGame.this.gameview.level);
                String temp_score = Integer.toString(StartGame.this.gameview.score);
                String temp_life = Integer.toString(StartGame.this.gameview.life);
                ContentValues cv = new ContentValues();
                cv.put("level_name", temp_level);
                cv.put("score", temp_score);
                cv.put("life", temp_life);
                db.insert("stagedata", null, cv);
                StartGame.this.setResult(-1, new Intent());
                String temp_high_score = "";
                Cursor c = db.rawQuery("select * from highscore;", null);
                if (c.getCount() <= 0) {
                    ContentValues cv_h = new ContentValues();
                    cv_h.put("highest_score", temp_score);
                    db.insert("highscore", null, cv_h);
                } else {
                    if (c.moveToFirst()) {
                        do {
                            temp_high_score = c.getString(c.getColumnIndex("highest_score"));
                        } while (c.moveToNext());
                    }
                    if (StartGame.this.gameview.score >= Integer.parseInt(temp_high_score)) {
                        String new_hs = Integer.toString(StartGame.this.gameview.score);
                        ContentValues cv_h2 = new ContentValues();
                        cv_h2.put("highest_score", new_hs);
                        db.update("highscore", cv_h2, "highest_score='" + temp_high_score + "'", null);
                    }
                }
                c.close();
                db.close();
                StartGame.this.finish();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SQLiteDatabase db = StartGame.this.openOrCreateDatabase("level.db", 0, null);
                db.execSQL("create table if not exists highscore(highest_score text not null);");
                String temp_score = Integer.toString(StartGame.this.gameview.score);
                String temp_high_score = "";
                Cursor c = db.rawQuery("select * from highscore;", null);
                if (c.getCount() <= 0) {
                    ContentValues cv_h = new ContentValues();
                    cv_h.put("highest_score", temp_score);
                    db.insert("highscore", null, cv_h);
                } else {
                    if (c.moveToFirst()) {
                        do {
                            temp_high_score = c.getString(c.getColumnIndex("highest_score"));
                        } while (c.moveToNext());
                    }
                    if (StartGame.this.gameview.score >= Integer.parseInt(temp_high_score)) {
                        String new_hs = Integer.toString(StartGame.this.gameview.score);
                        ContentValues cv_h2 = new ContentValues();
                        cv_h2.put("highest_score", new_hs);
                        db.update("highscore", cv_h2, "highest_score='" + temp_high_score + "'", null);
                    }
                }
                c.close();
                db.close();
                StartGame.this.setResult(-1, new Intent());
                StartGame.this.finish();
            }
        }).show();
        return true;
    }
}
