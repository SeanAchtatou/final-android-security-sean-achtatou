package com.balloon.archery.pop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import java.util.Random;

public class Arrow {
    float angle = 90.0f;
    int balloon_point;
    private Bitmap bmp;
    private GameView gameview;
    int height;
    int min_speed;
    Random rand = new Random();
    int random_height_top;
    int random_y_bottom;
    float scale;
    int stage;
    int width;
    int x;
    int xSpeed;
    int y;
    int ySpeed;

    public Arrow(GameView gameview2, Bitmap bmp2, int power, int stage_level, int x2, int y2) {
        this.gameview = gameview2;
        this.bmp = bmp2;
        this.scale = gameview2.getResources().getDisplayMetrics().density;
        this.width = bmp2.getWidth();
        this.height = bmp2.getHeight();
        this.x = x2;
        this.y = y2;
        this.xSpeed = (int) ((25.0f * this.scale) + 0.5f);
    }

    private void update() {
        this.x += this.xSpeed;
    }

    public void setX(int xCord) {
        this.x = xCord;
    }

    public void setY(int yCord) {
        this.y = yCord;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void move(float goX, float goY) {
        this.y = (int) (((float) this.y) + goY);
        if (((float) this.y) >= ((float) (this.gameview.getHeight() - (this.bmp.getHeight() / 2))) - ((this.scale * 50.0f) + 0.5f)) {
            this.y = (int) (((float) (this.gameview.getHeight() - (this.bmp.getHeight() / 2))) - ((this.scale * 50.0f) + 0.5f));
        }
        if (((float) this.y) <= ((this.scale * 50.0f) + 0.5f) - ((float) (this.bmp.getHeight() / 2))) {
            this.y = (int) (((this.scale * 50.0f) + 0.5f) - ((float) (this.bmp.getHeight() / 2)));
        }
    }

    public void setArrowAngle(float angle_x, float angle_y, Bitmap bmp2, float angle_degree, Canvas c) {
        Matrix matrix = new Matrix();
        this.angle = angle_degree;
        matrix.postRotate(this.angle, angle_x, angle_y);
        matrix.postTranslate(angle_x, angle_y);
        c.drawBitmap(bmp2, matrix, null);
    }

    public void onDraw(Canvas canvas, boolean moving) {
        if (moving) {
            update();
        }
        canvas.drawBitmap(this.bmp, (float) this.x, (float) this.y, (Paint) null);
    }
}
