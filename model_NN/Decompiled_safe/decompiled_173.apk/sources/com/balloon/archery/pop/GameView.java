package com.balloon.archery.pop;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.Random;

public class GameView extends SurfaceView {
    boolean LIFE_HAPPENED = false;
    boolean STRING_10_HAPPEND = false;
    boolean STRING_15_HAPPEND = false;
    boolean STRING_20_HAPPEND = false;
    boolean STRING_5_HAPPEND = false;
    private Arrow arrow;
    private boolean arrow_create = true;
    boolean arrow_created = false;
    private boolean arrow_left = false;
    private boolean arrow_pulled = false;
    MediaPlayer background_music;
    private Sprite balloon_10;
    private Sprite balloon_15;
    private Sprite balloon_20;
    private Sprite balloon_5;
    private Sprite balloon_black;
    private Sprite balloon_life;
    boolean black_created = false;
    private Bitmap blue_10_1;
    private Bitmap blue_15_1;
    private Bitmap blue_20_1;
    private Bitmap blue_5_1;
    boolean blue_created_10 = false;
    boolean blue_created_15 = false;
    boolean blue_created_20 = false;
    boolean blue_created_5 = false;
    private Bitmap bmp_arrow;
    private Bitmap bmp_background;
    private Bitmap bmp_background_2;
    private Bitmap bmp_background_3;
    private Bitmap bmp_background_4;
    Bitmap bmp_bird;
    Bitmap bmp_black_balloon;
    private Bitmap bmp_bow_normal;
    private Bitmap bmp_bow_pulled;
    Bitmap bmp_gameover;
    Bitmap bmp_increase_life;
    Bitmap bmp_level_1;
    Bitmap bmp_level_10;
    Bitmap bmp_level_11;
    Bitmap bmp_level_12;
    Bitmap bmp_level_13;
    Bitmap bmp_level_14;
    Bitmap bmp_level_15;
    Bitmap bmp_level_16;
    Bitmap bmp_level_17;
    Bitmap bmp_level_18;
    Bitmap bmp_level_19;
    Bitmap bmp_level_2;
    Bitmap bmp_level_20;
    Bitmap bmp_level_3;
    Bitmap bmp_level_4;
    Bitmap bmp_level_5;
    Bitmap bmp_level_6;
    Bitmap bmp_level_7;
    Bitmap bmp_level_8;
    Bitmap bmp_level_9;
    Bitmap bmp_life;
    Bitmap bmp_life_balloon;
    Bitmap bmp_string_10;
    Bitmap bmp_string_15;
    Bitmap bmp_string_20;
    Bitmap bmp_string_5;
    private Bow bow;
    MediaPlayer bow_fire_sound;
    private boolean firstTime = true;
    /* access modifiers changed from: private */
    public GameLoopThread gameLoopThread;
    boolean game_over = false;
    float gameoverX = 0.0f;
    float gameoverY = 0.0f;
    private Bitmap green_10_1;
    private Bitmap green_15_1;
    private Bitmap green_20_1;
    private Bitmap green_5_1;
    boolean green_created_10 = false;
    boolean green_created_15 = false;
    boolean green_created_20 = false;
    boolean green_created_5 = false;
    private SurfaceHolder holder;
    IncreaseLife increaseLife;
    float increaselifeX = 0.0f;
    float increaselifeY = 0.0f;
    int level = 0;
    LevelCompleted levelCompleted;
    float levelCompletedX = 0.0f;
    float levelCompletedY = 0.0f;
    boolean level_10_happened = false;
    boolean level_11_happened = false;
    boolean level_12_happened = false;
    boolean level_13_happened = false;
    boolean level_14_happened = false;
    boolean level_15_happened = false;
    boolean level_16_happened = false;
    boolean level_17_happened = false;
    boolean level_18_happened = false;
    boolean level_19_happened = false;
    boolean level_1_happened = true;
    boolean level_20_happened = false;
    boolean level_2_happened = false;
    boolean level_3_happened = false;
    boolean level_4_happened = false;
    boolean level_5_happened = false;
    boolean level_6_happened = false;
    boolean level_7_happened = false;
    boolean level_8_happened = false;
    boolean level_9_happened = false;
    boolean level_created = false;
    int life = 10;
    boolean life_balloon_created = false;
    Context main_context;
    int oldscore = 0;
    private Paint paint_regular;
    private Bitmap pink_10_1;
    private Bitmap pink_15_1;
    private Bitmap pink_20_1;
    private Bitmap pink_5_1;
    boolean pink_created_10 = false;
    boolean pink_created_15 = false;
    boolean pink_created_20 = false;
    boolean pink_created_5 = false;
    Random rand = new Random();
    int random = 0;
    private Bitmap red_10_1;
    private Bitmap red_15_1;
    private Bitmap red_20_1;
    private Bitmap red_5_1;
    boolean red_created_10 = false;
    boolean red_created_15 = false;
    boolean red_created_20 = false;
    boolean red_created_5 = false;
    private boolean retry = true;
    float scale;
    int score = 0;
    int score_from_resume = 0;
    MediaPlayer splash_with_balloon;
    private SplashedBalloon splashed_10;
    boolean splashed_10_happened = false;
    private SplashedBalloon splashed_15;
    boolean splashed_15_happened = false;
    private SplashedBalloon splashed_20;
    boolean splashed_20_happened = false;
    private SplashedBalloon splashed_5;
    boolean splashed_5_happened = false;
    Bitmap splsh_blue_10;
    Bitmap splsh_blue_15;
    Bitmap splsh_blue_20;
    Bitmap splsh_blue_5;
    Bitmap splsh_green_10;
    Bitmap splsh_green_15;
    Bitmap splsh_green_20;
    Bitmap splsh_green_5;
    Bitmap splsh_pink_10;
    Bitmap splsh_pink_15;
    Bitmap splsh_pink_20;
    Bitmap splsh_pink_5;
    Bitmap splsh_red_10;
    Bitmap splsh_red_15;
    Bitmap splsh_red_20;
    Bitmap splsh_red_5;
    Bitmap splsh_yellow_10;
    Bitmap splsh_yellow_15;
    Bitmap splsh_yellow_20;
    Bitmap splsh_yellow_5;
    StringsAfterSplash string_10;
    StringsAfterSplash string_15;
    StringsAfterSplash string_20;
    StringsAfterSplash string_5;
    Vibrator vibration;
    float y_touch_old = 0.0f;
    private Bitmap yellow_10_1;
    private Bitmap yellow_15_1;
    private Bitmap yellow_20_1;
    private Bitmap yellow_5_1;
    boolean yellow_created_10 = false;
    boolean yellow_created_15 = false;
    boolean yellow_created_20 = false;
    boolean yellow_created_5 = false;

    public GameView(Context context, int level_from, int score_from, int life_from) {
        super(context);
        this.splash_with_balloon = MediaPlayer.create(context, (int) R.raw.splash_sound);
        this.bow_fire_sound = MediaPlayer.create(context, (int) R.raw.bow_fire);
        this.background_music = MediaPlayer.create(context, (int) R.raw.background_music);
        this.life = life_from;
        this.level = level_from;
        if (this.level == 1) {
            this.level_1_happened = true;
        } else if (this.level == 2) {
            this.level_1_happened = false;
            this.level_2_happened = true;
        } else if (this.level == 3) {
            this.level_1_happened = false;
            this.level_3_happened = true;
        } else if (this.level == 4) {
            this.level_1_happened = false;
            this.level_4_happened = true;
        } else if (this.level == 5) {
            this.level_1_happened = false;
            this.level_5_happened = true;
        } else if (this.level == 6) {
            this.level_1_happened = false;
            this.level_6_happened = true;
        } else if (this.level == 7) {
            this.level_1_happened = false;
            this.level_7_happened = true;
        } else if (this.level == 8) {
            this.level_1_happened = false;
            this.level_8_happened = true;
        } else if (this.level == 9) {
            this.level_1_happened = false;
            this.level_9_happened = true;
        } else if (this.level == 10) {
            this.level_1_happened = false;
            this.level_10_happened = true;
        } else if (this.level == 11) {
            this.level_1_happened = false;
            this.level_11_happened = true;
        } else if (this.level == 12) {
            this.level_1_happened = false;
            this.level_12_happened = true;
        } else if (this.level == 13) {
            this.level_1_happened = false;
            this.level_13_happened = true;
        } else if (this.level == 14) {
            this.level_1_happened = false;
            this.level_14_happened = true;
        } else if (this.level == 15) {
            this.level_1_happened = false;
            this.level_15_happened = true;
        } else if (this.level == 16) {
            this.level_1_happened = false;
            this.level_16_happened = true;
        } else if (this.level == 17) {
            this.level_1_happened = false;
            this.level_17_happened = true;
        } else if (this.level == 18) {
            this.level_1_happened = false;
            this.level_18_happened = true;
        } else if (this.level == 19) {
            this.level_1_happened = false;
            this.level_19_happened = true;
        } else if (this.level == 20) {
            this.level_1_happened = false;
            this.level_20_happened = true;
        }
        this.score_from_resume = score_from;
        this.oldscore = (this.score_from_resume / 1000) * 1000;
        this.score = this.score_from_resume;
        this.main_context = context;
        this.scale = getResources().getDisplayMetrics().density;
        this.vibration = (Vibrator) this.main_context.getSystemService("vibrator");
        this.paint_regular = new Paint();
        this.paint_regular.setStrokeWidth(15.0f);
        this.paint_regular.setTextSize((float) ((int) ((this.scale * 15.0f) + 0.5f)));
        this.paint_regular.setTypeface(Typeface.create("Helvetica", 1));
        this.bmp_bird = BitmapFactory.decodeResource(getResources(), R.drawable.bird_image);
        this.bmp_gameover = BitmapFactory.decodeResource(getResources(), R.drawable.game_over);
        this.bmp_life_balloon = BitmapFactory.decodeResource(getResources(), R.drawable.lifeheart_balloon);
        this.bmp_increase_life = BitmapFactory.decodeResource(getResources(), R.drawable.increase_life);
        this.bmp_level_1 = BitmapFactory.decodeResource(getResources(), R.drawable.level_1_completed);
        this.bmp_level_2 = BitmapFactory.decodeResource(getResources(), R.drawable.level_2_completed);
        this.bmp_level_3 = BitmapFactory.decodeResource(getResources(), R.drawable.level_3_completed);
        this.bmp_level_4 = BitmapFactory.decodeResource(getResources(), R.drawable.level_4_completed);
        this.bmp_level_5 = BitmapFactory.decodeResource(getResources(), R.drawable.level_5_completed);
        this.bmp_level_6 = BitmapFactory.decodeResource(getResources(), R.drawable.level_6_completed);
        this.bmp_level_7 = BitmapFactory.decodeResource(getResources(), R.drawable.level_7_completed);
        this.bmp_level_8 = BitmapFactory.decodeResource(getResources(), R.drawable.level_8_completed);
        this.bmp_level_9 = BitmapFactory.decodeResource(getResources(), R.drawable.level_9_completed);
        this.bmp_level_10 = BitmapFactory.decodeResource(getResources(), R.drawable.level_10_completed);
        this.bmp_level_11 = BitmapFactory.decodeResource(getResources(), R.drawable.level_11_completed);
        this.bmp_level_12 = BitmapFactory.decodeResource(getResources(), R.drawable.level_12_completed);
        this.bmp_level_13 = BitmapFactory.decodeResource(getResources(), R.drawable.level_13_completed);
        this.bmp_level_14 = BitmapFactory.decodeResource(getResources(), R.drawable.level_14_completed);
        this.bmp_level_15 = BitmapFactory.decodeResource(getResources(), R.drawable.level_15_completed);
        this.bmp_level_16 = BitmapFactory.decodeResource(getResources(), R.drawable.level_16_completed);
        this.bmp_level_17 = BitmapFactory.decodeResource(getResources(), R.drawable.level_17_completed);
        this.bmp_level_18 = BitmapFactory.decodeResource(getResources(), R.drawable.level_18_completed);
        this.bmp_level_19 = BitmapFactory.decodeResource(getResources(), R.drawable.level_19_completed);
        this.bmp_level_20 = BitmapFactory.decodeResource(getResources(), R.drawable.level_20_completed);
        this.bmp_background = BitmapFactory.decodeResource(getResources(), R.drawable.background);
        this.bmp_background_2 = BitmapFactory.decodeResource(getResources(), R.drawable.background_1_new);
        this.bmp_background_3 = BitmapFactory.decodeResource(getResources(), R.drawable.background_2);
        this.bmp_background_4 = BitmapFactory.decodeResource(getResources(), R.drawable.background_3);
        this.bmp_bow_normal = BitmapFactory.decodeResource(getResources(), R.drawable.bow_regular);
        this.bmp_bow_pulled = BitmapFactory.decodeResource(getResources(), R.drawable.bow_pulled);
        this.bmp_arrow = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);
        this.bmp_life = BitmapFactory.decodeResource(getResources(), R.drawable.lifeheart);
        this.bmp_black_balloon = BitmapFactory.decodeResource(getResources(), R.drawable.black_balloon);
        this.blue_5_1 = BitmapFactory.decodeResource(getResources(), R.drawable.blue_5_1);
        this.red_5_1 = BitmapFactory.decodeResource(getResources(), R.drawable.red_5_1);
        this.green_5_1 = BitmapFactory.decodeResource(getResources(), R.drawable.green_5_1);
        this.yellow_5_1 = BitmapFactory.decodeResource(getResources(), R.drawable.yellow_5_1);
        this.pink_5_1 = BitmapFactory.decodeResource(getResources(), R.drawable.pink_5_1);
        this.blue_10_1 = BitmapFactory.decodeResource(getResources(), R.drawable.blue_10_1);
        this.red_10_1 = BitmapFactory.decodeResource(getResources(), R.drawable.red_10_1);
        this.green_10_1 = BitmapFactory.decodeResource(getResources(), R.drawable.green_10_1);
        this.yellow_10_1 = BitmapFactory.decodeResource(getResources(), R.drawable.yellow_10_1);
        this.pink_10_1 = BitmapFactory.decodeResource(getResources(), R.drawable.pink_10_1);
        this.blue_20_1 = BitmapFactory.decodeResource(getResources(), R.drawable.blue_20_1);
        this.red_20_1 = BitmapFactory.decodeResource(getResources(), R.drawable.red_20_1);
        this.green_20_1 = BitmapFactory.decodeResource(getResources(), R.drawable.green_20_1);
        this.yellow_20_1 = BitmapFactory.decodeResource(getResources(), R.drawable.yellow_20_1);
        this.pink_20_1 = BitmapFactory.decodeResource(getResources(), R.drawable.pink_20_1);
        this.blue_15_1 = BitmapFactory.decodeResource(getResources(), R.drawable.blue_15_1);
        this.red_15_1 = BitmapFactory.decodeResource(getResources(), R.drawable.red_15_1);
        this.green_15_1 = BitmapFactory.decodeResource(getResources(), R.drawable.green_15_1);
        this.yellow_15_1 = BitmapFactory.decodeResource(getResources(), R.drawable.yellow_15_1);
        this.pink_15_1 = BitmapFactory.decodeResource(getResources(), R.drawable.pink_15_1);
        this.splsh_red_5 = BitmapFactory.decodeResource(getResources(), R.drawable.splash_red_5);
        this.splsh_blue_5 = BitmapFactory.decodeResource(getResources(), R.drawable.splash_blue_5);
        this.splsh_green_5 = BitmapFactory.decodeResource(getResources(), R.drawable.splash_green_5);
        this.splsh_yellow_5 = BitmapFactory.decodeResource(getResources(), R.drawable.splash_yellow_5);
        this.splsh_pink_5 = BitmapFactory.decodeResource(getResources(), R.drawable.splash_pink_5);
        this.splsh_red_10 = BitmapFactory.decodeResource(getResources(), R.drawable.red_splash_10);
        this.splsh_blue_10 = BitmapFactory.decodeResource(getResources(), R.drawable.blue_splash_10);
        this.splsh_green_10 = BitmapFactory.decodeResource(getResources(), R.drawable.green_splash_10);
        this.splsh_yellow_10 = BitmapFactory.decodeResource(getResources(), R.drawable.yellow_splash_10);
        this.splsh_pink_10 = BitmapFactory.decodeResource(getResources(), R.drawable.pink_splash_10);
        this.splsh_red_15 = BitmapFactory.decodeResource(getResources(), R.drawable.red_splashed_15);
        this.splsh_blue_15 = BitmapFactory.decodeResource(getResources(), R.drawable.blue_splashed_15);
        this.splsh_green_15 = BitmapFactory.decodeResource(getResources(), R.drawable.green_splashed_15);
        this.splsh_yellow_15 = BitmapFactory.decodeResource(getResources(), R.drawable.yellow_splashed_15);
        this.splsh_pink_15 = BitmapFactory.decodeResource(getResources(), R.drawable.pink_splashed_15);
        this.splsh_red_20 = BitmapFactory.decodeResource(getResources(), R.drawable.red_splashed_20);
        this.splsh_blue_20 = BitmapFactory.decodeResource(getResources(), R.drawable.blue_splashed_20);
        this.splsh_green_20 = BitmapFactory.decodeResource(getResources(), R.drawable.green_splashed_20);
        this.splsh_yellow_20 = BitmapFactory.decodeResource(getResources(), R.drawable.yellow_splashed_20);
        this.splsh_pink_20 = BitmapFactory.decodeResource(getResources(), R.drawable.pink_splashed_20);
        this.bmp_string_5 = BitmapFactory.decodeResource(getResources(), R.drawable.string_5_3);
        this.bmp_string_10 = BitmapFactory.decodeResource(getResources(), R.drawable.string_10_1);
        this.bmp_string_15 = BitmapFactory.decodeResource(getResources(), R.drawable.string_15_1);
        this.bmp_string_20 = BitmapFactory.decodeResource(getResources(), R.drawable.string_20_3);
        this.gameLoopThread = new GameLoopThread(this);
        this.holder = getHolder();
        this.holder.addCallback(new SurfaceHolder.Callback() {
            public void surfaceDestroyed(SurfaceHolder holder) {
                GameView.this.background_music.stop();
                GameView.this.gameLoopThread.setRunning(false);
                GameView.this.gameLoopThread.stop();
            }

            public void surfaceCreated(SurfaceHolder holder) {
                GameView.this.createBow();
                GameView.this.createArrow();
                GameView.this.gameLoopThread.setRunning(true);
                GameView.this.gameLoopThread.start();
            }

            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.drawColor(-16777216);
        if (this.level == 0) {
            canvas.drawBitmap(this.bmp_background, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 1) {
            canvas.drawBitmap(this.bmp_background_2, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 2) {
            canvas.drawBitmap(this.bmp_background_3, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 3) {
            canvas.drawBitmap(this.bmp_background_4, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 4) {
            canvas.drawBitmap(this.bmp_background, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 5) {
            canvas.drawBitmap(this.bmp_background_2, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 6) {
            canvas.drawBitmap(this.bmp_background_3, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 7) {
            canvas.drawBitmap(this.bmp_background_4, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 8) {
            canvas.drawBitmap(this.bmp_background, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 9) {
            canvas.drawBitmap(this.bmp_background_2, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 10) {
            canvas.drawBitmap(this.bmp_background_3, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 11) {
            canvas.drawBitmap(this.bmp_background_4, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 12) {
            canvas.drawBitmap(this.bmp_background, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 13) {
            canvas.drawBitmap(this.bmp_background_2, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 14) {
            canvas.drawBitmap(this.bmp_background_3, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level == 15) {
            canvas.drawBitmap(this.bmp_background_4, 0.0f, 0.0f, (Paint) null);
        }
        if (this.level > 15) {
            canvas.drawBitmap(this.bmp_background_2, 0.0f, 0.0f, (Paint) null);
        }
        if (this.life <= 0) {
            if (this.background_music.isPlaying()) {
                this.background_music.pause();
            }
            this.game_over = true;
            this.gameLoopThread.setRunning(false);
            this.gameLoopThread.stop();
        }
        int screenwidth_score = (int) (((float) (getWidth() / 2)) - ((30.0f * this.scale) + 0.5f));
        int screenwidth_image_life = (int) (((float) getWidth()) - ((75.0f * this.scale) + 0.5f));
        int screenwidth_life_text = (int) (((float) getWidth()) - ((50.0f * this.scale) + 0.5f));
        int down_height_score_text = (int) ((20.0f * this.scale) + 0.5f);
        int down_height_level_text = (int) ((20.0f * this.scale) + 0.5f);
        int down_width_level_text = (int) ((15.0f * this.scale) + 0.5f);
        int height_life_text = (int) ((20.0f * this.scale) + 0.5f);
        int life_image_Y = (int) ((5.0f * this.scale) + 0.5f);
        String score_final = Integer.toString(this.score);
        String level1 = Integer.toString(this.level);
        canvas.drawText("Score : " + score_final, (float) screenwidth_score, (float) down_height_score_text, this.paint_regular);
        canvas.drawText("Level : " + level1, (float) down_width_level_text, (float) down_height_level_text, this.paint_regular);
        canvas.drawBitmap(this.bmp_life, (float) screenwidth_image_life, (float) life_image_Y, (Paint) null);
        canvas.drawText("X " + this.life, (float) screenwidth_life_text, (float) height_life_text, this.paint_regular);
        this.gameoverX = (float) ((getWidth() / 2) - (this.bmp_gameover.getWidth() / 2));
        this.gameoverY = (float) ((getHeight() / 2) - (this.bmp_gameover.getHeight() / 2));
        this.increaselifeX = (float) ((getWidth() / 2) - (this.bmp_increase_life.getWidth() / 2));
        this.increaselifeY = (float) ((getHeight() / 2) - (this.bmp_increase_life.getHeight() / 2));
        this.levelCompletedX = (float) ((getWidth() / 2) - (this.bmp_level_1.getWidth() / 2));
        this.levelCompletedY = (float) ((getHeight() / 2) - (this.bmp_level_1.getHeight() / 2));
        if (this.level_created) {
            if (this.levelCompleted.level_life_over) {
                this.level_created = false;
                if (this.level_1_happened) {
                    this.level_1_happened = false;
                    this.level_2_happened = true;
                } else if (this.level_2_happened) {
                    this.level_2_happened = false;
                    this.level_3_happened = true;
                } else if (this.level_3_happened) {
                    this.level_3_happened = false;
                    this.level_4_happened = true;
                } else if (this.level_4_happened) {
                    this.level_4_happened = false;
                    this.level_5_happened = true;
                } else if (this.level_5_happened) {
                    this.level_5_happened = false;
                    this.level_6_happened = true;
                } else if (this.level_6_happened) {
                    this.level_6_happened = false;
                    this.level_7_happened = true;
                } else if (this.level_7_happened) {
                    this.level_7_happened = false;
                    this.level_8_happened = true;
                } else if (this.level_8_happened) {
                    this.level_8_happened = false;
                    this.level_9_happened = true;
                } else if (this.level_9_happened) {
                    this.level_9_happened = false;
                    this.level_10_happened = true;
                } else if (this.level_10_happened) {
                    this.level_10_happened = false;
                    this.level_11_happened = true;
                } else if (this.level_11_happened) {
                    this.level_11_happened = false;
                    this.level_12_happened = true;
                } else if (this.level_12_happened) {
                    this.level_12_happened = false;
                    this.level_13_happened = true;
                } else if (this.level_13_happened) {
                    this.level_13_happened = false;
                    this.level_14_happened = true;
                } else if (this.level_14_happened) {
                    this.level_14_happened = false;
                    this.level_15_happened = true;
                } else if (this.level_15_happened) {
                    this.level_15_happened = false;
                    this.level_16_happened = true;
                } else if (this.level_16_happened) {
                    this.level_16_happened = false;
                    this.level_17_happened = true;
                } else if (this.level_17_happened) {
                    this.level_17_happened = false;
                    this.level_18_happened = true;
                } else if (this.level_18_happened) {
                    this.level_18_happened = false;
                    this.level_19_happened = true;
                } else if (this.level_19_happened) {
                    this.level_19_happened = false;
                    this.level_20_happened = true;
                }
            } else {
                this.levelCompleted.onDraw(canvas);
            }
        }
        if (!this.level_created) {
            if (this.level == 1 && this.level_1_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_1, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 2 && this.level_2_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_2, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 3 && this.level_3_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_3, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 4 && this.level_4_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_4, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 5 && this.level_5_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_5, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 6 && this.level_6_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_6, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 7 && this.level_7_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_7, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 8 && this.level_8_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_8, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 9 && this.level_9_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_9, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 10 && this.level_10_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_10, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 11 && this.level_11_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_11, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 12 && this.level_12_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_12, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 13 && this.level_13_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_13, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 14 && this.level_14_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_14, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 15 && this.level_15_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_15, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 16 && this.level_16_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_16, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 17 && this.level_17_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_17, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 18 && this.level_18_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_18, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 19 && this.level_19_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_19, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
            if (this.level == 20 && this.level_20_happened) {
                this.levelCompleted = null;
                this.levelCompleted = new LevelCompleted(this, this.bmp_level_20, this.levelCompletedX, this.levelCompletedY);
                this.levelCompleted.level_life_over = false;
                this.level_created = true;
            }
        }
        if (this.arrow_pulled) {
            this.bow.onDraw(canvas, this.bmp_bow_pulled);
        } else {
            this.bow.onDraw(canvas, this.bmp_bow_normal);
        }
        if (this.arrow_created) {
            this.arrow.onDraw(canvas, this.arrow_left);
        }
        if (this.arrow.getX() >= getWidth() - this.bmp_arrow.getWidth()) {
            this.arrow_create = true;
        }
        if (!this.life_balloon_created && this.score % 500 == 0 && this.level > 1) {
            createLifeBalloon();
        }
        if (this.life_balloon_created) {
            this.balloon_life.onDraw(canvas, this.bmp_life_balloon);
        }
        if (this.balloon_life != null && this.balloon_life.getY() + (this.bmp_life_balloon.getHeight() / 2) <= 0) {
            this.life_balloon_created = false;
            this.balloon_life = null;
        }
        if (!this.black_created) {
            if (this.score % 50 == 0 && this.level > 2) {
                createBlackBalloon();
            }
            if (this.score % 75 == 0 && this.score > 0) {
                createBlackBalloon();
            }
        }
        if (this.black_created) {
            this.balloon_black.onDraw(canvas, this.bmp_black_balloon);
        }
        if (this.balloon_black != null && this.balloon_black.getY() + (this.bmp_black_balloon.getHeight() / 2) <= 0) {
            this.black_created = false;
        }
        if (this.score >= this.oldscore + 1000) {
            this.oldscore = (this.score / 1000) * 1000;
            this.level = this.level + 1;
        }
        if (!this.red_created_5 && !this.green_created_5 && !this.blue_created_5 && !this.yellow_created_5 && !this.pink_created_5) {
            createBalloon_5();
        }
        if (this.red_created_5) {
            this.balloon_5.onDraw(canvas, this.red_5_1);
        }
        if (this.green_created_5) {
            this.balloon_5.onDraw(canvas, this.green_5_1);
        }
        if (this.blue_created_5) {
            this.balloon_5.onDraw(canvas, this.blue_5_1);
        }
        if (this.yellow_created_5) {
            this.balloon_5.onDraw(canvas, this.yellow_5_1);
        }
        if (this.pink_created_5) {
            this.balloon_5.onDraw(canvas, this.pink_5_1);
        }
        if (this.balloon_5.getY() + (this.red_5_1.getHeight() / 2) <= 0) {
            this.red_created_5 = false;
            this.green_created_5 = false;
            this.blue_created_5 = false;
            this.yellow_created_5 = false;
            this.pink_created_5 = false;
        }
        if (!this.red_created_10 && !this.green_created_10 && !this.blue_created_10 && !this.yellow_created_10 && !this.pink_created_10) {
            createBalloon_10();
        }
        if (this.red_created_10) {
            this.balloon_10.onDraw(canvas, this.red_10_1);
        }
        if (this.green_created_10) {
            this.balloon_10.onDraw(canvas, this.green_10_1);
        }
        if (this.blue_created_10) {
            this.balloon_10.onDraw(canvas, this.blue_10_1);
        }
        if (this.yellow_created_10) {
            this.balloon_10.onDraw(canvas, this.yellow_10_1);
        }
        if (this.pink_created_10) {
            this.balloon_10.onDraw(canvas, this.pink_10_1);
        }
        if (this.balloon_10.getY() + (this.red_10_1.getHeight() / 2) <= 0) {
            this.red_created_10 = false;
            this.green_created_10 = false;
            this.blue_created_10 = false;
            this.yellow_created_10 = false;
            this.pink_created_10 = false;
        }
        if (!this.red_created_15 && !this.green_created_15 && !this.blue_created_15 && !this.yellow_created_15 && !this.pink_created_15) {
            createBalloon_15();
        }
        if (this.red_created_15) {
            this.balloon_15.onDraw(canvas, this.red_15_1);
        }
        if (this.green_created_15) {
            this.balloon_15.onDraw(canvas, this.green_15_1);
        }
        if (this.blue_created_15) {
            this.balloon_15.onDraw(canvas, this.blue_15_1);
        }
        if (this.yellow_created_15) {
            this.balloon_15.onDraw(canvas, this.yellow_15_1);
        }
        if (this.pink_created_15) {
            this.balloon_15.onDraw(canvas, this.pink_15_1);
        }
        if (this.balloon_15.getY() + (this.red_15_1.getHeight() / 2) <= 0) {
            this.red_created_15 = false;
            this.green_created_15 = false;
            this.blue_created_15 = false;
            this.yellow_created_15 = false;
            this.pink_created_15 = false;
        }
        if (!this.red_created_20 && !this.green_created_20 && !this.blue_created_20 && !this.yellow_created_20 && !this.pink_created_20) {
            createBalloon_20();
        }
        if (this.red_created_20) {
            this.balloon_20.onDraw(canvas, this.red_20_1);
        }
        if (this.green_created_20) {
            this.balloon_20.onDraw(canvas, this.green_20_1);
        }
        if (this.blue_created_20) {
            this.balloon_20.onDraw(canvas, this.blue_20_1);
        }
        if (this.yellow_created_20) {
            this.balloon_20.onDraw(canvas, this.yellow_20_1);
        }
        if (this.pink_created_20) {
            this.balloon_20.onDraw(canvas, this.pink_20_1);
        }
        if (this.balloon_20.getY() + (this.red_20_1.getHeight() / 2) <= 0) {
            this.red_created_20 = false;
            this.green_created_20 = false;
            this.blue_created_20 = false;
            this.yellow_created_20 = false;
            this.pink_created_20 = false;
        }
        if (((float) this.arrow.getX()) + (75.0f * this.scale) + 0.5f >= ((float) getWidth())) {
            this.arrow_created = false;
        }
        if (this.LIFE_HAPPENED) {
            if (this.increaseLife.lifeplus_life_over) {
                this.LIFE_HAPPENED = false;
            } else {
                this.increaseLife.onDraw(canvas);
            }
        }
        if (this.splashed_5_happened) {
            if (this.splashed_5.life_over) {
                this.splashed_5_happened = false;
                this.score = this.score + 5;
            } else {
                this.splashed_5.onDraw(canvas);
            }
        }
        if (this.splashed_10_happened) {
            if (this.splashed_10.life_over) {
                this.splashed_10_happened = false;
                this.score = this.score + 10;
            } else {
                this.splashed_10.onDraw(canvas);
            }
        }
        if (this.splashed_15_happened) {
            if (this.splashed_15.life_over) {
                this.splashed_15_happened = false;
                this.score = this.score + 15;
            } else {
                this.splashed_15.onDraw(canvas);
            }
        }
        if (this.splashed_20_happened) {
            if (this.splashed_20.life_over) {
                this.splashed_20_happened = false;
                this.score = this.score + 20;
            } else {
                this.splashed_20.onDraw(canvas);
            }
        }
        if (this.STRING_5_HAPPEND) {
            if (this.string_5.string_life_over) {
                this.STRING_5_HAPPEND = false;
            } else {
                this.string_5.onDraw(canvas);
            }
        }
        if (this.STRING_10_HAPPEND) {
            if (this.string_10.string_life_over) {
                this.STRING_10_HAPPEND = false;
            } else {
                this.string_10.onDraw(canvas);
            }
        }
        if (this.STRING_15_HAPPEND) {
            if (this.string_15.string_life_over) {
                this.STRING_15_HAPPEND = false;
            } else {
                this.string_15.onDraw(canvas);
            }
        }
        if (this.STRING_20_HAPPEND) {
            if (this.string_20.string_life_over) {
                this.STRING_20_HAPPEND = false;
            } else {
                this.string_20.onDraw(canvas);
            }
        }
        if (this.life_balloon_created && ((float) this.arrow.getX()) + (75.0f * this.scale) + 0.5f >= ((float) this.balloon_life.getX()) + (8.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (17.0f * this.scale) + 0.5f >= ((float) this.balloon_life.getY()) + (0.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (1.0f * this.scale) + 0.5f <= ((float) this.balloon_life.getY()) + (60.0f * this.scale) + 0.5f && this.arrow_created) {
            this.life = this.life + 1;
            this.LIFE_HAPPENED = true;
            this.increaseLife = new IncreaseLife(this, this.bmp_increase_life, this.increaselifeX, this.increaselifeY);
            this.life_balloon_created = false;
            this.balloon_life = null;
        }
        if (this.black_created && ((float) this.arrow.getX()) + (75.0f * this.scale) + 0.5f >= ((float) this.balloon_black.getX()) + (12.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (17.0f * this.scale) + 0.5f >= ((float) this.balloon_black.getY()) + (1.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (1.0f * this.scale) + 0.5f <= ((float) this.balloon_black.getY()) + (73.0f * this.scale) + 0.5f && this.arrow_created) {
            this.life = this.life - 1;
            this.vibration.vibrate(100);
            this.arrow_created = false;
            this.black_created = false;
            this.arrow_create = true;
        }
        if (((float) this.arrow.getX()) + (75.0f * this.scale) + 0.5f >= ((float) this.balloon_5.getX()) + (8.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (17.0f * this.scale) + 0.5f >= ((float) this.balloon_5.getY()) + (5.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (1.0f * this.scale) + 0.5f <= ((float) this.balloon_5.getY()) + (80.0f * this.scale) + 0.5f && this.arrow_created) {
            if (!this.splash_with_balloon.isPlaying()) {
                this.splash_with_balloon.start();
            }
            this.string_5 = null;
            this.string_5 = new StringsAfterSplash(this, this.bmp_string_5, (float) (this.balloon_5.getX() + (this.red_5_1.getWidth() / 2)), (float) (this.balloon_5.getY() + (this.red_5_1.getHeight() / 2)));
            this.STRING_5_HAPPEND = true;
            this.string_5.string_life_over = false;
            if (this.red_created_5) {
                this.splashed_5 = null;
                this.splashed_5 = new SplashedBalloon(this, this.splsh_red_5, (float) this.balloon_5.getX(), (float) this.balloon_5.getY());
                this.splashed_5_happened = true;
                this.splashed_5.life_over = false;
            }
            if (this.green_created_5) {
                this.splashed_5 = null;
                this.splashed_5 = new SplashedBalloon(this, this.splsh_green_5, (float) this.balloon_5.getX(), (float) this.balloon_5.getY());
                this.splashed_5_happened = true;
                this.splashed_5.life_over = false;
            }
            if (this.blue_created_5) {
                this.splashed_5 = null;
                this.splashed_5 = new SplashedBalloon(this, this.splsh_blue_5, (float) this.balloon_5.getX(), (float) this.balloon_5.getY());
                this.splashed_5_happened = true;
                this.splashed_5.life_over = false;
            }
            if (this.yellow_created_5) {
                this.splashed_5 = null;
                this.splashed_5 = new SplashedBalloon(this, this.splsh_yellow_5, (float) this.balloon_5.getX(), (float) this.balloon_5.getY());
                this.splashed_5_happened = true;
                this.splashed_5.life_over = false;
            }
            if (this.pink_created_5) {
                this.splashed_5 = null;
                this.splashed_5 = new SplashedBalloon(this, this.splsh_pink_5, (float) this.balloon_5.getX(), (float) this.balloon_5.getY());
                this.splashed_5_happened = true;
                this.splashed_5.life_over = false;
            }
            this.red_created_5 = false;
            this.green_created_5 = false;
            this.blue_created_5 = false;
            this.yellow_created_5 = false;
            this.pink_created_5 = false;
        }
        if (((float) this.arrow.getX()) + (75.0f * this.scale) + 0.5f >= ((float) this.balloon_10.getX()) + (7.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (17.0f * this.scale) + 0.5f >= ((float) this.balloon_10.getY()) + (5.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (1.0f * this.scale) + 0.5f <= ((float) this.balloon_10.getY()) + (62.0f * this.scale) + 0.5f && this.arrow_created) {
            if (!this.splash_with_balloon.isPlaying()) {
                this.splash_with_balloon.start();
            }
            this.string_10 = null;
            this.string_10 = new StringsAfterSplash(this, this.bmp_string_10, (float) (this.balloon_10.getX() + (this.red_10_1.getWidth() / 2)), (float) (this.balloon_10.getY() + (this.red_10_1.getHeight() / 2)));
            this.STRING_10_HAPPEND = true;
            this.string_10.string_life_over = false;
            if (this.red_created_10) {
                this.splashed_10 = null;
                this.splashed_10 = new SplashedBalloon(this, this.splsh_red_10, (float) this.balloon_10.getX(), (float) this.balloon_10.getY());
                this.splashed_10_happened = true;
                this.splashed_10.life_over = false;
            }
            if (this.green_created_10) {
                this.splashed_10 = null;
                this.splashed_10 = new SplashedBalloon(this, this.splsh_green_10, (float) this.balloon_10.getX(), (float) this.balloon_10.getY());
                this.splashed_10_happened = true;
                this.splashed_10.life_over = false;
            }
            if (this.blue_created_10) {
                this.splashed_10 = null;
                this.splashed_10 = new SplashedBalloon(this, this.splsh_blue_10, (float) this.balloon_10.getX(), (float) this.balloon_10.getY());
                this.splashed_10_happened = true;
                this.splashed_10.life_over = false;
            }
            if (this.yellow_created_10) {
                this.splashed_10 = null;
                this.splashed_10 = new SplashedBalloon(this, this.splsh_yellow_10, (float) this.balloon_10.getX(), (float) this.balloon_10.getY());
                this.splashed_10_happened = true;
                this.splashed_10.life_over = false;
            }
            if (this.pink_created_10) {
                this.splashed_10 = null;
                this.splashed_10 = new SplashedBalloon(this, this.splsh_pink_10, (float) this.balloon_10.getX(), (float) this.balloon_10.getY());
                this.splashed_10_happened = true;
                this.splashed_10.life_over = false;
            }
            this.red_created_10 = false;
            this.green_created_10 = false;
            this.blue_created_10 = false;
            this.yellow_created_10 = false;
            this.pink_created_10 = false;
        }
        if (((float) this.arrow.getX()) + (75.0f * this.scale) + 0.5f >= ((float) this.balloon_15.getX()) + (6.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (17.0f * this.scale) + 0.5f >= ((float) this.balloon_15.getY()) + (4.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (1.0f * this.scale) + 0.5f <= ((float) this.balloon_15.getY()) + (52.0f * this.scale) + 0.5f && this.arrow_created) {
            if (!this.splash_with_balloon.isPlaying()) {
                this.splash_with_balloon.start();
            }
            this.string_15 = null;
            this.string_15 = new StringsAfterSplash(this, this.bmp_string_15, (float) (this.balloon_15.getX() + (this.red_15_1.getWidth() / 2)), (float) (this.balloon_15.getY() + (this.red_15_1.getHeight() / 2)));
            this.STRING_15_HAPPEND = true;
            this.string_15.string_life_over = false;
            if (this.red_created_15) {
                this.splashed_15 = null;
                this.splashed_15 = new SplashedBalloon(this, this.splsh_red_15, (float) this.balloon_15.getX(), (float) this.balloon_15.getY());
                this.splashed_15_happened = true;
                this.splashed_15.life_over = false;
            }
            if (this.green_created_15) {
                this.splashed_15 = null;
                this.splashed_15 = new SplashedBalloon(this, this.splsh_green_15, (float) this.balloon_15.getX(), (float) this.balloon_15.getY());
                this.splashed_15_happened = true;
                this.splashed_15.life_over = false;
            }
            if (this.blue_created_15) {
                this.splashed_15 = null;
                this.splashed_15 = new SplashedBalloon(this, this.splsh_blue_15, (float) this.balloon_15.getX(), (float) this.balloon_15.getY());
                this.splashed_15_happened = true;
                this.splashed_15.life_over = false;
            }
            if (this.yellow_created_15) {
                this.splashed_15 = null;
                this.splashed_15 = new SplashedBalloon(this, this.splsh_yellow_15, (float) this.balloon_15.getX(), (float) this.balloon_15.getY());
                this.splashed_15_happened = true;
                this.splashed_15.life_over = false;
            }
            if (this.pink_created_15) {
                this.splashed_15 = null;
                this.splashed_15 = new SplashedBalloon(this, this.splsh_pink_15, (float) this.balloon_15.getX(), (float) this.balloon_15.getY());
                this.splashed_15_happened = true;
                this.splashed_15.life_over = false;
            }
            this.red_created_15 = false;
            this.green_created_15 = false;
            this.blue_created_15 = false;
            this.yellow_created_15 = false;
            this.pink_created_15 = false;
        }
        if (((float) this.arrow.getX()) + (75.0f * this.scale) + 0.5f >= ((float) this.balloon_20.getX()) + (5.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (17.0f * this.scale) + 0.5f >= ((float) this.balloon_20.getY()) + (3.0f * this.scale) + 0.5f && ((float) this.arrow.getY()) + (1.0f * this.scale) + 0.5f <= ((float) this.balloon_20.getY()) + (41.0f * this.scale) + 0.5f && this.arrow_created) {
            if (!this.splash_with_balloon.isPlaying()) {
                this.splash_with_balloon.start();
            }
            this.string_20 = null;
            this.string_20 = new StringsAfterSplash(this, this.bmp_string_20, (float) (this.balloon_20.getX() + (this.red_20_1.getWidth() / 2)), (float) (this.balloon_20.getY() + (this.red_20_1.getHeight() / 2)));
            this.STRING_20_HAPPEND = true;
            this.string_20.string_life_over = false;
            if (this.red_created_20) {
                this.splashed_20 = null;
                this.splashed_20 = new SplashedBalloon(this, this.splsh_red_20, (float) this.balloon_20.getX(), (float) this.balloon_20.getY());
                this.splashed_20_happened = true;
                this.splashed_20.life_over = false;
            }
            if (this.green_created_20) {
                this.splashed_20 = null;
                this.splashed_20 = new SplashedBalloon(this, this.splsh_green_20, (float) this.balloon_20.getX(), (float) this.balloon_20.getY());
                this.splashed_20_happened = true;
                this.splashed_20.life_over = false;
            }
            if (this.blue_created_20) {
                this.splashed_20 = null;
                this.splashed_20 = new SplashedBalloon(this, this.splsh_blue_20, (float) this.balloon_20.getX(), (float) this.balloon_20.getY());
                this.splashed_20_happened = true;
                this.splashed_20.life_over = false;
            }
            if (this.yellow_created_20) {
                this.splashed_20 = null;
                this.splashed_20 = new SplashedBalloon(this, this.splsh_yellow_20, (float) this.balloon_20.getX(), (float) this.balloon_20.getY());
                this.splashed_20_happened = true;
                this.splashed_20.life_over = false;
            }
            if (this.pink_created_20) {
                this.splashed_20 = null;
                this.splashed_20 = new SplashedBalloon(this, this.splsh_pink_20, (float) this.balloon_20.getX(), (float) this.balloon_20.getY());
                this.splashed_20_happened = true;
                this.splashed_20.life_over = false;
            }
            this.red_created_20 = false;
            this.green_created_20 = false;
            this.blue_created_20 = false;
            this.yellow_created_20 = false;
            this.pink_created_20 = false;
        }
        if (this.life <= 0) {
            canvas.drawBitmap(this.bmp_gameover, this.gameoverX, this.gameoverY, (Paint) null);
        }
    }

    /* access modifiers changed from: protected */
    public void createBalloon_5() {
        this.random = this.rand.nextInt(10);
        if (this.random <= 2) {
            this.balloon_5 = null;
            this.balloon_5 = new Sprite(this, this.red_5_1, 5, this.level);
            this.red_created_5 = true;
            this.green_created_5 = false;
            this.blue_created_5 = false;
            this.yellow_created_5 = false;
            this.pink_created_5 = false;
        }
        if (this.random > 2 && this.random <= 4) {
            this.balloon_5 = null;
            this.balloon_5 = new Sprite(this, this.green_5_1, 5, this.level);
            this.red_created_5 = false;
            this.green_created_5 = true;
            this.blue_created_5 = false;
            this.yellow_created_5 = false;
            this.pink_created_5 = false;
        }
        if (this.random > 4 && this.random <= 6) {
            this.balloon_5 = null;
            this.balloon_5 = new Sprite(this, this.blue_5_1, 5, this.level);
            this.red_created_5 = false;
            this.green_created_5 = false;
            this.blue_created_5 = true;
            this.yellow_created_5 = false;
            this.pink_created_5 = false;
        }
        if (this.random > 6 && this.random <= 8) {
            this.balloon_5 = null;
            this.balloon_5 = new Sprite(this, this.yellow_5_1, 5, this.level);
            this.red_created_5 = false;
            this.green_created_5 = false;
            this.blue_created_5 = false;
            this.yellow_created_5 = true;
            this.pink_created_5 = false;
        }
        if (this.random > 8 && this.random <= 10) {
            this.balloon_5 = null;
            this.balloon_5 = new Sprite(this, this.pink_5_1, 5, this.level);
            this.red_created_5 = false;
            this.green_created_5 = false;
            this.blue_created_5 = false;
            this.yellow_created_5 = false;
            this.pink_created_5 = true;
        }
    }

    /* access modifiers changed from: protected */
    public void createBalloon_10() {
        this.random = this.rand.nextInt(10);
        if (this.random <= 2) {
            this.balloon_10 = null;
            this.balloon_10 = new Sprite(this, this.red_10_1, 10, this.level);
            this.red_created_10 = true;
            this.green_created_10 = false;
            this.blue_created_10 = false;
            this.yellow_created_10 = false;
            this.pink_created_10 = false;
        }
        if (this.random > 2 && this.random <= 4) {
            this.balloon_10 = null;
            this.balloon_10 = new Sprite(this, this.green_10_1, 10, this.level);
            this.red_created_10 = false;
            this.green_created_10 = true;
            this.blue_created_10 = false;
            this.yellow_created_10 = false;
            this.pink_created_10 = false;
        }
        if (this.random > 4 && this.random <= 6) {
            this.balloon_10 = null;
            this.balloon_10 = new Sprite(this, this.blue_10_1, 10, this.level);
            this.red_created_10 = false;
            this.green_created_10 = false;
            this.blue_created_10 = true;
            this.yellow_created_10 = false;
            this.pink_created_10 = false;
        }
        if (this.random > 6 && this.random <= 8) {
            this.balloon_10 = null;
            this.balloon_10 = new Sprite(this, this.yellow_10_1, 10, this.level);
            this.red_created_10 = false;
            this.green_created_10 = false;
            this.blue_created_10 = false;
            this.yellow_created_10 = true;
            this.pink_created_10 = false;
        }
        if (this.random > 8 && this.random <= 10) {
            this.balloon_10 = null;
            this.balloon_10 = new Sprite(this, this.pink_10_1, 10, this.level);
            this.red_created_10 = false;
            this.green_created_10 = false;
            this.blue_created_10 = false;
            this.yellow_created_10 = false;
            this.pink_created_10 = true;
        }
    }

    /* access modifiers changed from: protected */
    public void createBalloon_15() {
        this.random = this.rand.nextInt(10);
        if (this.random <= 2) {
            this.balloon_15 = null;
            this.balloon_15 = new Sprite(this, this.red_15_1, 15, this.level);
            this.red_created_15 = true;
            this.green_created_15 = false;
            this.blue_created_15 = false;
            this.yellow_created_15 = false;
            this.pink_created_15 = false;
        }
        if (this.random > 2 && this.random <= 4) {
            this.balloon_15 = null;
            this.balloon_15 = new Sprite(this, this.green_15_1, 15, this.level);
            this.red_created_15 = false;
            this.green_created_15 = true;
            this.blue_created_15 = false;
            this.yellow_created_15 = false;
            this.pink_created_15 = false;
        }
        if (this.random > 4 && this.random <= 6) {
            this.balloon_15 = null;
            this.balloon_15 = new Sprite(this, this.blue_15_1, 15, this.level);
            this.red_created_15 = false;
            this.green_created_15 = false;
            this.blue_created_15 = true;
            this.yellow_created_15 = false;
            this.pink_created_15 = false;
        }
        if (this.random > 6 && this.random <= 8) {
            this.balloon_15 = null;
            this.balloon_15 = new Sprite(this, this.yellow_15_1, 15, this.level);
            this.red_created_15 = false;
            this.green_created_15 = false;
            this.blue_created_15 = false;
            this.yellow_created_15 = true;
            this.pink_created_15 = false;
        }
        if (this.random > 8 && this.random <= 10) {
            this.balloon_15 = null;
            this.balloon_15 = new Sprite(this, this.pink_10_1, 15, this.level);
            this.red_created_15 = false;
            this.green_created_15 = false;
            this.blue_created_15 = false;
            this.yellow_created_15 = false;
            this.pink_created_15 = true;
        }
    }

    /* access modifiers changed from: protected */
    public void createBalloon_20() {
        this.random = this.rand.nextInt(10);
        if (this.random <= 2) {
            this.balloon_20 = null;
            this.balloon_20 = new Sprite(this, this.red_20_1, 20, this.level);
            this.red_created_20 = true;
            this.green_created_20 = false;
            this.blue_created_20 = false;
            this.yellow_created_20 = false;
            this.pink_created_20 = false;
        }
        if (this.random > 2 && this.random <= 4) {
            this.balloon_20 = null;
            this.balloon_20 = new Sprite(this, this.green_20_1, 20, this.level);
            this.red_created_20 = false;
            this.green_created_20 = true;
            this.blue_created_20 = false;
            this.yellow_created_20 = false;
            this.pink_created_20 = false;
        }
        if (this.random > 4 && this.random <= 6) {
            this.balloon_20 = null;
            this.balloon_20 = new Sprite(this, this.blue_20_1, 20, this.level);
            this.red_created_20 = false;
            this.green_created_20 = false;
            this.blue_created_20 = true;
            this.yellow_created_20 = false;
            this.pink_created_20 = false;
        }
        if (this.random > 6 && this.random <= 8) {
            this.balloon_20 = null;
            this.balloon_20 = new Sprite(this, this.yellow_20_1, 20, this.level);
            this.red_created_20 = false;
            this.green_created_20 = false;
            this.blue_created_20 = false;
            this.yellow_created_20 = true;
            this.pink_created_20 = false;
        }
        if (this.random > 8 && this.random <= 10) {
            this.balloon_20 = null;
            this.balloon_20 = new Sprite(this, this.pink_20_1, 20, this.level);
            this.red_created_20 = false;
            this.green_created_20 = false;
            this.blue_created_20 = false;
            this.yellow_created_20 = false;
            this.pink_created_20 = true;
        }
    }

    /* access modifiers changed from: protected */
    public void createLifeBalloon() {
        this.balloon_life = null;
        this.balloon_life = new Sprite(this, this.bmp_life_balloon, 1, this.level);
        this.life_balloon_created = true;
    }

    /* access modifiers changed from: protected */
    public void createBlackBalloon() {
        this.balloon_black = null;
        this.balloon_black = new Sprite(this, this.bmp_black_balloon, 0, this.level);
        this.black_created = true;
    }

    /* access modifiers changed from: protected */
    public void createBow() {
        this.background_music.start();
        this.background_music.setLooping(true);
        this.bow = null;
        this.bow = new Bow(this, this.bmp_bow_normal, 0, this.level);
    }

    /* access modifiers changed from: protected */
    public void createArrow() {
        this.arrow = null;
        this.arrow = new Arrow(this, this.bmp_arrow, 0, this.level, (int) ((5.0f * this.scale) + 0.5f), (int) ((((float) this.bow.getY()) + ((50.0f * this.scale) + 0.5f)) - ((float) (this.bmp_arrow.getHeight() / 2))));
        this.arrow_created = true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == 0) {
            synchronized (getHolder()) {
                float y_touch = event.getY();
                float x_touch = event.getX();
                if (y_touch >= ((float) this.bow.getY()) && y_touch <= ((float) (this.bow.getY() + 100)) && x_touch >= ((float) this.bow.getX()) && x_touch <= ((float) (this.bow.getX() + 50))) {
                    this.arrow_pulled = true;
                    this.y_touch_old = y_touch;
                    if (this.arrow_create) {
                        createArrow();
                        this.arrow_create = false;
                        this.arrow_left = false;
                    }
                }
            }
        }
        if (action == 1) {
            synchronized (getHolder()) {
                this.arrow_pulled = false;
                this.arrow_left = true;
                if (!this.arrow_create && this.arrow_left) {
                    this.bow_fire_sound.start();
                }
                if (this.game_over) {
                    if (((double) event.getX()) > ((double) this.gameoverX) + (((double) this.bmp_gameover.getWidth()) / 7.45d) && ((double) event.getX()) < ((double) this.gameoverX) + (((double) this.bmp_gameover.getWidth()) / 2.8d) && event.getY() > this.gameoverY + ((float) (this.bmp_gameover.getHeight() / 2)) && event.getY() < this.gameoverY + ((float) this.bmp_gameover.getHeight())) {
                        Intent intent = new Intent(this.main_context, DataHandle.class);
                        intent.putExtra("high_score", this.score);
                        this.main_context.startActivity(intent);
                        this.background_music.start();
                        this.background_music.setLooping(true);
                        this.gameLoopThread = null;
                        this.gameLoopThread = new GameLoopThread(this);
                        this.game_over = false;
                        this.score = 0;
                        this.oldscore = 0;
                        this.score_from_resume = 0;
                        this.life = 10;
                        this.level = 0;
                        this.y_touch_old = 0.0f;
                        this.red_created_5 = false;
                        this.green_created_5 = false;
                        this.yellow_created_5 = false;
                        this.pink_created_5 = false;
                        this.blue_created_5 = false;
                        this.red_created_10 = false;
                        this.green_created_10 = false;
                        this.yellow_created_10 = false;
                        this.pink_created_10 = false;
                        this.blue_created_10 = false;
                        this.red_created_20 = false;
                        this.green_created_20 = false;
                        this.yellow_created_20 = false;
                        this.pink_created_20 = false;
                        this.blue_created_20 = false;
                        this.red_created_15 = false;
                        this.green_created_15 = false;
                        this.yellow_created_15 = false;
                        this.pink_created_15 = false;
                        this.blue_created_15 = false;
                        this.splashed_5_happened = false;
                        this.splashed_10_happened = false;
                        this.splashed_15_happened = false;
                        this.splashed_20_happened = false;
                        this.black_created = false;
                        this.life_balloon_created = false;
                        this.arrow_created = false;
                        this.STRING_5_HAPPEND = false;
                        this.STRING_10_HAPPEND = false;
                        this.STRING_15_HAPPEND = false;
                        this.STRING_20_HAPPEND = false;
                        this.arrow_pulled = false;
                        this.arrow_left = false;
                        this.arrow_create = true;
                        this.firstTime = true;
                        this.level_1_happened = true;
                        this.level_2_happened = false;
                        this.level_3_happened = false;
                        this.level_4_happened = false;
                        this.level_5_happened = false;
                        this.level_6_happened = false;
                        this.level_7_happened = false;
                        this.level_8_happened = false;
                        this.level_9_happened = false;
                        this.level_10_happened = false;
                        this.level_11_happened = false;
                        this.level_12_happened = false;
                        this.level_13_happened = false;
                        this.level_14_happened = false;
                        this.level_15_happened = false;
                        this.level_16_happened = false;
                        this.level_17_happened = false;
                        this.level_18_happened = false;
                        this.level_19_happened = false;
                        this.level_20_happened = false;
                        this.level_created = false;
                        this.gameLoopThread.setRunning(true);
                        this.gameLoopThread.start();
                    }
                    if (((double) event.getX()) > ((double) this.gameoverX) + (((double) this.bmp_gameover.getWidth()) / 1.58d) && ((double) event.getX()) < ((double) this.gameoverX) + (((double) this.bmp_gameover.getWidth()) / 1.17d) && event.getY() > this.gameoverY + ((float) (this.bmp_gameover.getHeight() / 2)) && event.getY() < this.gameoverY + ((float) this.bmp_gameover.getHeight())) {
                        Intent intent2 = new Intent(this.main_context, DataHandle.class);
                        intent2.putExtra("high_score", this.score);
                        this.main_context.startActivity(intent2);
                        System.exit(0);
                    }
                }
            }
        }
        if (action != 2) {
            return true;
        }
        synchronized (getHolder()) {
            float y_touch2 = event.getY();
            float x_touch2 = event.getX();
            if (y_touch2 >= ((float) this.bow.getY()) && y_touch2 <= ((float) this.bow.getY()) + (100.0f * this.scale) + 0.5f && x_touch2 >= ((float) this.bow.getX()) && x_touch2 <= ((float) this.bow.getX()) + (50.0f * this.scale) + 0.5f) {
                this.bow.move(x_touch2, y_touch2 - this.y_touch_old);
                if (!this.arrow_left) {
                    this.arrow.move(x_touch2, y_touch2 - this.y_touch_old);
                }
                this.y_touch_old = y_touch2;
            }
        }
        return true;
    }
}
