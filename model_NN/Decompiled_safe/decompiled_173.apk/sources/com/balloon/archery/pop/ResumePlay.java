package com.balloon.archery.pop;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class ResumePlay extends Activity {
    private float adParamSize = 50.0f;
    private AdView adView;
    int current_level = 0;
    int current_life = 10;
    int current_score = 0;
    GameView gameview;
    private RelativeLayout layout;
    private LinearLayout mainLayout;
    private String temp_level = "";
    private String temp_life = "10";
    private String temp_score = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SQLiteDatabase db = openOrCreateDatabase("level.db", 0, null);
        db.execSQL("create table if not exists stagedata(level_name text not null, score text,life text, PRIMARY KEY(level_name));");
        Cursor c = db.rawQuery("select * from stagedata;", null);
        if (c.getCount() <= 0) {
            this.current_score = 0;
            this.current_level = 0;
            this.current_life = 10;
        } else if (c.moveToFirst()) {
            do {
                this.temp_score = c.getString(c.getColumnIndex("score"));
                this.current_score = Integer.parseInt(this.temp_score);
                this.temp_life = c.getString(c.getColumnIndex("life"));
                this.current_life = Integer.parseInt(this.temp_life);
                this.temp_level = c.getString(c.getColumnIndex("level_name"));
                this.current_level = Integer.parseInt(this.temp_level);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        this.mainLayout = new LinearLayout(this);
        this.mainLayout.setOrientation(1);
        this.layout = new RelativeLayout(this);
        this.adView = new AdView(this, AdSize.BANNER, "a14e2aa89718a81");
        this.adView.setVisibility(0);
        this.adView.loadAd(new AdRequest());
        LinearLayout.LayoutParams adparam = new LinearLayout.LayoutParams(-2, -2);
        adparam.height = (int) ((this.adParamSize * getResources().getDisplayMetrics().density) + 0.5f);
        this.adView.setLayoutParams(adparam);
        this.mainLayout.addView(this.adView);
        this.mainLayout.addView(this.layout);
        this.gameview = new GameView(this, this.current_level, this.current_score, this.current_life);
        this.gameview.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        this.layout.addView(this.gameview);
        setContentView(this.mainLayout);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        new AlertDialog.Builder(this).setTitle("ALERT....!").setMessage("Do you want to save the current game ?").setCancelable(true).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SQLiteDatabase db = ResumePlay.this.openOrCreateDatabase("level.db", 0, null);
                db.execSQL("drop table if exists stagedata;");
                db.execSQL("create table if not exists stagedata(level_name text not null, score text,life text, PRIMARY KEY(level_name));");
                db.execSQL("create table if not exists highscore(highest_score text not null);");
                String temp_level = Integer.toString(ResumePlay.this.gameview.level);
                String temp_score = Integer.toString(ResumePlay.this.gameview.score);
                String temp_life = Integer.toString(ResumePlay.this.gameview.life);
                ContentValues cv = new ContentValues();
                cv.put("level_name", temp_level);
                cv.put("score", temp_score);
                cv.put("life", temp_life);
                db.insert("stagedata", null, cv);
                ResumePlay.this.setResult(-1, new Intent());
                String temp_high_score = "";
                Cursor c = db.rawQuery("select * from highscore;", null);
                if (c.getCount() <= 0) {
                    ContentValues cv_h = new ContentValues();
                    cv_h.put("highest_score", temp_score);
                    db.insert("highscore", null, cv_h);
                } else {
                    if (c.moveToFirst()) {
                        do {
                            temp_high_score = c.getString(c.getColumnIndex("highest_score"));
                        } while (c.moveToNext());
                    }
                    if (ResumePlay.this.gameview.score >= Integer.parseInt(temp_high_score)) {
                        String new_hs = Integer.toString(ResumePlay.this.gameview.score);
                        ContentValues cv_h2 = new ContentValues();
                        cv_h2.put("highest_score", new_hs);
                        db.update("highscore", cv_h2, "highest_score='" + temp_high_score + "'", null);
                    }
                }
                c.close();
                db.close();
                ResumePlay.this.finish();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SQLiteDatabase db = ResumePlay.this.openOrCreateDatabase("level.db", 0, null);
                db.execSQL("create table if not exists highscore(highest_score text not null);");
                String temp_score = Integer.toString(ResumePlay.this.gameview.score);
                String temp_high_score = "";
                Cursor c = db.rawQuery("select * from highscore;", null);
                if (c.getCount() <= 0) {
                    ContentValues cv_h = new ContentValues();
                    cv_h.put("highest_score", temp_score);
                    db.insert("highscore", null, cv_h);
                } else {
                    if (c.moveToFirst()) {
                        do {
                            temp_high_score = c.getString(c.getColumnIndex("highest_score"));
                        } while (c.moveToNext());
                    }
                    if (ResumePlay.this.gameview.score >= Integer.parseInt(temp_high_score)) {
                        String new_hs = Integer.toString(ResumePlay.this.gameview.score);
                        ContentValues cv_h2 = new ContentValues();
                        cv_h2.put("highest_score", new_hs);
                        db.update("highscore", cv_h2, "highest_score='" + temp_high_score + "'", null);
                    }
                }
                c.close();
                db.close();
                ResumePlay.this.setResult(-1, new Intent());
                ResumePlay.this.finish();
            }
        }).show();
        return true;
    }
}
