package com.balloon.archery.pop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Resume<keyevent> extends Activity {
    int current_level = 0;
    int current_score = 0;
    Button help;
    Button high_scores;
    Button new_game;
    Button resume;
    String temp_level = "";
    String temp_score = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.resume);
        this.new_game = (Button) findViewById(R.id.new_game_button);
        this.new_game.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Resume.this.startActivityForResult(new Intent(Resume.this, StartGame.class), 0);
            }
        });
        this.resume = (Button) findViewById(R.id.resume_button);
        this.resume.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Resume.this.startActivityForResult(new Intent(Resume.this, ResumePlay.class), 0);
            }
        });
        this.help = (Button) findViewById(R.id.button_help);
        this.help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Resume.this.startActivityForResult(new Intent(Resume.this, Help.class), 0);
            }
        });
        this.high_scores = (Button) findViewById(R.id.button_highscores);
        this.high_scores.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Resume.this.startActivityForResult(new Intent(Resume.this, HighScore.class), 0);
            }
        });
    }
}
