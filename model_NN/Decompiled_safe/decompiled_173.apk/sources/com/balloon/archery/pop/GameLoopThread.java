package com.balloon.archery.pop;

import android.graphics.Canvas;

public class GameLoopThread extends Thread {
    static final long FPS = 10;
    private boolean running = false;
    private GameView view;

    public GameLoopThread(GameView view2) {
        this.view = view2;
    }

    public void setRunning(boolean run) {
        this.running = run;
    }

    public void run() {
        while (this.running) {
            long starttime = System.currentTimeMillis();
            Canvas c = null;
            try {
                c = this.view.getHolder().lockCanvas();
                synchronized (this.view.getHolder()) {
                    this.view.onDraw(c);
                }
                long sleeptime = 100 - (System.currentTimeMillis() - starttime);
                if (sleeptime > 0) {
                    try {
                        sleep(sleeptime);
                    } catch (Exception e) {
                    }
                } else {
                    sleep(FPS);
                }
            } finally {
                if (c != null) {
                    this.view.getHolder().unlockCanvasAndPost(c);
                }
            }
        }
    }
}
