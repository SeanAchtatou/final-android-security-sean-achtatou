package com.balloon.archery.pop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BalloonArchery extends Activity {
    Button Help;
    Button Start;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.Start = (Button) findViewById(R.id.button_start);
        this.Start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BalloonArchery.this.startActivityForResult(new Intent(BalloonArchery.this, StageHandler.class), 0);
            }
        });
    }
}
