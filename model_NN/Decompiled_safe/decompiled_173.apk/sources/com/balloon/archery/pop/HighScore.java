package com.balloon.archery.pop;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class HighScore extends Activity {
    TextView high_score_text;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.high_scores);
        AdView adView = new AdView(this, AdSize.BANNER, "a14e2aa89718a81");
        ((RelativeLayout) findViewById(R.id.highScore_relative)).addView(adView);
        adView.loadAd(new AdRequest());
        this.high_score_text = (TextView) findViewById(R.id.high_score_text);
        SQLiteDatabase db = openOrCreateDatabase("level.db", 0, null);
        db.execSQL("create table if not exists highscore(highest_score text not null);");
        String temp_high_score = "";
        Cursor c = db.rawQuery("select * from highscore;", null);
        if (c.getCount() <= 0) {
            this.high_score_text.setText("0");
        } else {
            if (c.moveToFirst()) {
                do {
                    temp_high_score = c.getString(c.getColumnIndex("highest_score"));
                } while (c.moveToNext());
            }
            this.high_score_text.setText(temp_high_score);
        }
        c.close();
        db.close();
    }
}
