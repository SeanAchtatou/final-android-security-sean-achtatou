package com.balloon.archery.pop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.Random;

public class Sprite {
    int balloon_point;
    private Bitmap bmp;
    private GameView gameview;
    int height;
    int min_speed;
    Random rand = new Random();
    int random_height_top;
    int random_y_bottom;
    float scale;
    int stage;
    int width;
    int x;
    int y;
    int ySpeed;

    public Sprite(GameView gameview2, Bitmap bmp2, int point_level, int stage_level) {
        this.gameview = gameview2;
        this.bmp = bmp2;
        this.scale = gameview2.getResources().getDisplayMetrics().density;
        this.width = bmp2.getWidth();
        this.height = bmp2.getHeight();
        this.stage = stage_level;
        this.balloon_point = point_level;
        this.min_speed = (int) ((4.0f * this.scale) + 0.5f);
        int x_offset_0 = (int) ((100.0f * this.scale) + 0.5f);
        int x_offset_1 = (int) ((90.0f * this.scale) + 0.5f);
        int x_offset_5 = (int) ((75.0f * this.scale) + 0.5f);
        int x_offset_10 = (int) ((45.0f * this.scale) + 0.5f);
        int x_offset_15 = (int) ((20.0f * this.scale) + 0.5f);
        int x_offset_20 = (int) ((0.0f * this.scale) + 0.5f);
        if (this.balloon_point == 0) {
            this.x = (gameview2.getWidth() - this.width) - x_offset_0;
        }
        if (this.balloon_point == 1) {
            this.x = (gameview2.getWidth() - this.width) - x_offset_1;
        }
        if (this.balloon_point == 5) {
            this.x = (gameview2.getWidth() - this.width) - x_offset_5;
        }
        if (this.balloon_point == 10) {
            this.x = (gameview2.getWidth() - this.width) - x_offset_10;
        }
        if (this.balloon_point == 15) {
            this.x = (gameview2.getWidth() - this.width) - x_offset_15;
        }
        if (this.balloon_point == 20) {
            this.x = (gameview2.getWidth() - this.width) - x_offset_20;
        }
        this.y = gameview2.getHeight();
        int temp_rd = (int) ((25.0f * this.scale) + 0.5f);
        int temp_rd_minus_5 = 0;
        int temp_rd_minus_10 = 0;
        int temp_rd_minus_15 = 0;
        int temp_rd_minus_20 = 0;
        if (this.stage <= 2) {
            temp_rd_minus_5 = (int) ((19.0f * this.scale) + 0.5f);
            temp_rd_minus_10 = (int) ((17.0f * this.scale) + 0.5f);
            temp_rd_minus_15 = (int) ((14.0f * this.scale) + 0.5f);
            temp_rd_minus_20 = (int) ((11.0f * this.scale) + 0.5f);
        } else if (this.stage <= 3 && this.stage > 2) {
            temp_rd_minus_5 = (int) ((17.0f * this.scale) + 0.5f);
            temp_rd_minus_10 = (int) ((15.0f * this.scale) + 0.5f);
            temp_rd_minus_15 = (int) ((13.0f * this.scale) + 0.5f);
            temp_rd_minus_20 = (int) ((9.0f * this.scale) + 0.5f);
        } else if (this.stage <= 4 && this.stage > 3) {
            temp_rd_minus_5 = (int) ((15.0f * this.scale) + 0.5f);
            temp_rd_minus_10 = (int) ((13.0f * this.scale) + 0.5f);
            temp_rd_minus_15 = (int) ((11.0f * this.scale) + 0.5f);
            temp_rd_minus_20 = (int) ((7.0f * this.scale) + 0.5f);
        } else if (this.stage <= 5 && this.stage > 4) {
            temp_rd_minus_5 = (int) ((13.0f * this.scale) + 0.5f);
            temp_rd_minus_10 = (int) ((11.0f * this.scale) + 0.5f);
            temp_rd_minus_15 = (int) ((9.0f * this.scale) + 0.5f);
            temp_rd_minus_20 = (int) ((5.0f * this.scale) + 0.5f);
        } else if (this.stage <= 6 && this.stage > 5) {
            temp_rd_minus_5 = (int) ((11.0f * this.scale) + 0.5f);
            temp_rd_minus_10 = (int) ((9.0f * this.scale) + 0.5f);
            temp_rd_minus_15 = (int) ((7.0f * this.scale) + 0.5f);
            temp_rd_minus_20 = (int) ((3.0f * this.scale) + 0.5f);
        } else if (this.stage <= 7 && this.stage > 6) {
            temp_rd_minus_5 = (int) ((9.0f * this.scale) + 0.5f);
            temp_rd_minus_10 = (int) ((7.0f * this.scale) + 0.5f);
            temp_rd_minus_15 = (int) ((5.0f * this.scale) + 0.5f);
            temp_rd_minus_20 = (int) ((1.0f * this.scale) + 0.5f);
        } else if (this.stage <= 8 && this.stage > 7) {
            temp_rd_minus_5 = (int) ((7.0f * this.scale) + 0.5f);
            temp_rd_minus_10 = (int) ((5.0f * this.scale) + 0.5f);
            temp_rd_minus_15 = (int) ((3.0f * this.scale) + 0.5f);
            temp_rd_minus_20 = (int) ((0.0f * this.scale) + 0.5f);
        } else if (this.stage >= 9) {
            temp_rd_minus_5 = (int) ((5.0f * this.scale) + 0.5f);
            temp_rd_minus_10 = (int) ((3.0f * this.scale) + 0.5f);
            temp_rd_minus_15 = (int) ((0.0f * this.scale) + 0.5f);
            temp_rd_minus_20 = (int) ((0.0f * this.scale) + 0.5f);
        }
        if (this.balloon_point == 0) {
            this.ySpeed = this.rand.nextInt(temp_rd) - temp_rd_minus_20;
        }
        if (this.balloon_point == 1) {
            this.ySpeed = (int) (((float) (this.rand.nextInt(temp_rd) - temp_rd_minus_20)) + (2.0f * this.scale) + 0.5f);
        }
        if (this.balloon_point == 5) {
            this.ySpeed = this.rand.nextInt(temp_rd) - temp_rd_minus_5;
        }
        if (this.balloon_point == 10) {
            this.ySpeed = this.rand.nextInt(temp_rd) - temp_rd_minus_10;
        }
        if (this.balloon_point == 15) {
            this.ySpeed = this.rand.nextInt(temp_rd) - temp_rd_minus_15;
        }
        if (this.balloon_point == 20) {
            this.ySpeed = this.rand.nextInt(temp_rd) - temp_rd_minus_20;
        }
        if (this.ySpeed <= this.min_speed) {
            this.ySpeed = this.min_speed;
        }
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    private void update() {
        this.y -= this.ySpeed;
    }

    public void onDraw(Canvas canvas, Bitmap current_bitmap) {
        update();
        canvas.drawBitmap(current_bitmap, (float) this.x, (float) this.y, (Paint) null);
    }
}
