package com.balloon.archery.pop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.Random;

public class Bow {
    private Bitmap bmp;
    private GameView gameview;
    Random rand = new Random();
    float scale;
    int x;
    int y;

    public Bow(GameView gameview2, Bitmap bmp2, int power, int stage_level) {
        this.gameview = gameview2;
        this.bmp = bmp2;
        this.scale = gameview2.getResources().getDisplayMetrics().density;
        this.x = (int) ((0.0f * this.scale) + 0.5f);
        this.y = (int) ((5.0f * this.scale) + 0.5f);
    }

    public void setX(int xCord) {
        this.x = xCord;
    }

    public void setY(int yCord) {
        this.y = yCord;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void move(float goX, float goY) {
        this.y = (int) (((float) this.y) + goY);
        if (this.y >= this.gameview.getHeight() - this.bmp.getHeight()) {
            this.y = this.gameview.getHeight() - this.bmp.getHeight();
        }
        if (((float) this.y) <= (this.scale * 5.0f) + 0.5f) {
            this.y = (int) ((this.scale * 5.0f) + 0.5f);
        }
    }

    public void onDraw(Canvas canvas, Bitmap current_bmp) {
        canvas.drawBitmap(current_bmp, (float) this.x, (float) this.y, (Paint) null);
    }
}
