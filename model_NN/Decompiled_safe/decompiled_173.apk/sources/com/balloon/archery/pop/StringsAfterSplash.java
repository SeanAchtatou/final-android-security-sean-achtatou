package com.balloon.archery.pop;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class StringsAfterSplash {
    Bitmap bmp;
    GameView gameview;
    int height = 0;
    float scale;
    boolean string_life_over = false;
    float x;
    float y;
    int ySpeed;

    public StringsAfterSplash(GameView gameview2, Bitmap bmp2, float x2, float y2) {
        this.x = x2;
        this.y = y2;
        this.bmp = bmp2;
        this.gameview = gameview2;
        this.height = gameview2.getHeight();
        this.scale = gameview2.getResources().getDisplayMetrics().density;
        this.ySpeed = (int) ((7.0f * this.scale) + 0.5f);
    }

    public void onDraw(Canvas canvas) {
        update();
        canvas.drawBitmap(this.bmp, this.x, this.y, (Paint) null);
    }

    private void update() {
        this.y += (float) this.ySpeed;
        if (this.y >= ((float) this.height)) {
            this.string_life_over = true;
        }
    }
}
