package com.balloon.archery.pop;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class Help extends Activity {
    Button okButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help);
        AdView adView = new AdView(this, AdSize.BANNER, "a14e2aa89718a81");
        ((LinearLayout) findViewById(R.id.linearLayouthelp)).addView(adView);
        adView.loadAd(new AdRequest());
        this.okButton = (Button) findViewById(R.id.ok);
        this.okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Help.this.finish();
            }
        });
    }
}
