package com.umeng.common.net;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.umeng.common.Log;
import com.umeng.common.a;
import com.umeng.common.net.DownloadingService;
import java.io.File;

class d extends Handler {
    final /* synthetic */ DownloadingService.a a;

    d(DownloadingService.a aVar) {
        this.a = aVar;
    }

    public void handleMessage(Message message) {
        try {
            String string = message.getData().getString("filename");
            DownloadingService.this.k.cancel(this.a.e);
            Log.c(DownloadingService.j, "Cancel old notification....");
            Notification notification = new Notification(17301634, a.l, System.currentTimeMillis());
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.addFlags(268435456);
            intent.setDataAndType(Uri.fromFile(new File(string)), "application/vnd.android.package-archive");
            notification.setLatestEventInfo(this.a.b, this.a.i.b, a.l, PendingIntent.getActivity(this.a.b, 0, intent, 134217728));
            notification.flags = 16;
            DownloadingService.this.k = (NotificationManager) DownloadingService.this.getSystemService("notification");
            DownloadingService.this.k.notify(this.a.e, notification);
            Log.c(DownloadingService.j, "Show new  notification....");
            boolean a2 = DownloadingService.b(this.a.b);
            Log.c(DownloadingService.j, String.format("isAppOnForeground = %1$B", Boolean.valueOf(a2)));
            if (a2) {
                DownloadingService.this.k.cancel(this.a.e);
                this.a.b.startActivity(intent);
            }
            Log.a(DownloadingService.j, String.format("%1$10s downloaded. Saved to: %2$s", this.a.i.b, string));
        } catch (Exception e) {
            Log.b(DownloadingService.j, "can not install. " + e.getMessage());
            DownloadingService.this.k.cancel(this.a.e);
        }
    }
}
