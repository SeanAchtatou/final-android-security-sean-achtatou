package com.umeng.common.net;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.animation.Animation;
import android.widget.ImageView;
import com.umeng.common.Log;
import com.umeng.common.b.g;
import com.umeng.common.net.h;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.Stack;

public class i {
    public static boolean a = false;
    /* access modifiers changed from: private */
    public static final String b = i.class.getName();
    private static final long c = 104857600;
    private static final long d = 10485760;

    public interface a {
        void a(h.a aVar);

        void a(b bVar);
    }

    public enum b {
        BIND_FORM_CACHE,
        BIND_FROM_NET
    }

    static class c extends AsyncTask<Object, Integer, Drawable> {
        private Context a;
        private String b;
        private ImageView c;
        private b d;
        private boolean e;
        private a f;
        private Animation g;

        public c(Context context, ImageView imageView, String str, b bVar, boolean z, a aVar, Animation animation) {
            this.a = context;
            this.b = str;
            this.f = aVar;
            this.d = bVar;
            this.e = z;
            this.g = animation;
            this.c = imageView;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Drawable doInBackground(Object... objArr) {
            if (i.a) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            i.a(this.a, this.b);
            try {
                File b2 = i.b(this.a, this.b);
                if (b2 == null || !b2.exists()) {
                    return null;
                }
                return i.c(b2.getAbsolutePath());
            } catch (IOException e3) {
                Log.e(i.b, e3.toString());
                return null;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Drawable drawable) {
            i.b(this.a, this.c, drawable, this.e, this.f, this.g);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            if (this.f != null) {
                this.f.a(this.d);
            }
        }
    }

    private static long a(File file) {
        long length;
        long j = 0;
        if (file == null || !file.exists() || !file.isDirectory()) {
            return 0;
        }
        Stack stack = new Stack();
        stack.clear();
        stack.push(file);
        while (true) {
            long j2 = j;
            if (stack.isEmpty()) {
                return j2;
            }
            File[] listFiles = ((File) stack.pop()).listFiles();
            j = j2;
            int i = 0;
            while (i < listFiles.length) {
                if (listFiles[i].isDirectory()) {
                    stack.push(listFiles[i]);
                    length = j;
                } else {
                    length = listFiles[i].length() + j;
                }
                i++;
                j = length;
            }
        }
    }

    public static String a(Context context, String str) {
        File file;
        String canonicalPath;
        long j;
        if (g.c(str)) {
            return null;
        }
        try {
            String str2 = String.valueOf(b(str)) + ".tmp";
            if (com.umeng.common.b.b()) {
                canonicalPath = Environment.getExternalStorageDirectory().getCanonicalPath();
                j = c;
            } else {
                canonicalPath = context.getCacheDir().getCanonicalPath();
                j = d;
            }
            File file2 = new File(String.valueOf(canonicalPath) + com.umeng.common.a.a);
            if (file2.exists()) {
                if (a(file2.getCanonicalFile()) > j) {
                    b(file2);
                }
            } else if (!file2.mkdirs()) {
                Log.b(b, "Failed to create directory" + file2.getAbsolutePath() + ". Check permission. Make sure WRITE_EXTERNAL_STORAGE is added in your Manifest.xml");
            }
            file = new File(file2, str2);
            try {
                file.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                InputStream inputStream = (InputStream) new URL(str).openConnection().getContent();
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        fileOutputStream.flush();
                        inputStream.close();
                        fileOutputStream.close();
                        File file3 = new File(file.getParent(), file.getName().replace(".tmp", ""));
                        file.renameTo(file3);
                        Log.a(b, "download img[" + str + "]  to " + file3.getCanonicalPath());
                        return file3.getCanonicalPath();
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            file = null;
            Log.a(b, String.valueOf(e.getStackTrace().toString()) + "\t url:\t" + g.a + str);
            if (file != null && file.exists()) {
                file.deleteOnExit();
            }
            return null;
        }
    }

    public static void a(Context context, ImageView imageView, String str, boolean z) {
        a(context, imageView, str, z, (a) null, (Animation) null);
    }

    public static void a(Context context, ImageView imageView, String str, boolean z, a aVar) {
        a(context, imageView, str, z, aVar, (Animation) null);
    }

    public static void a(Context context, ImageView imageView, String str, boolean z, a aVar, Animation animation) {
        if (imageView != null) {
            try {
                File b2 = b(context, str);
                if (b2 == null || !b2.exists() || a) {
                    new c(context, imageView, str, b.BIND_FROM_NET, z, aVar, animation).execute(new Object[0]);
                    return;
                }
                if (aVar != null) {
                    aVar.a(b.BIND_FORM_CACHE);
                }
                b(context, imageView, c(b2.getAbsolutePath()), z, aVar, animation);
            } catch (IOException e) {
                if (aVar != null) {
                    aVar.a(h.a.FAIL);
                }
            }
        }
    }

    protected static File b(Context context, String str) throws IOException {
        File file = new File(new File(String.valueOf(com.umeng.common.b.b() ? Environment.getExternalStorageDirectory().getCanonicalPath() : context.getCacheDir().getCanonicalPath()) + com.umeng.common.a.a), b(str));
        if (file.exists()) {
            return file;
        }
        return null;
    }

    private static String b(String str) {
        int lastIndexOf = str.lastIndexOf(".");
        String str2 = "";
        if (lastIndexOf >= 0) {
            str2 = str.substring(lastIndexOf);
        }
        return String.valueOf(g.a(str)) + str2;
    }

    /* access modifiers changed from: private */
    public static synchronized void b(Context context, ImageView imageView, Drawable drawable, boolean z, a aVar, Animation animation) {
        synchronized (i.class) {
            if (drawable == null || imageView == null) {
                if (aVar != null) {
                    aVar.a(h.a.FAIL);
                }
                Log.e(b, "bind drawable failed. drawable [" + drawable + "]  imageView[+" + imageView + "+]");
            } else {
                if (z) {
                    imageView.setBackgroundDrawable(drawable);
                } else {
                    imageView.setImageDrawable(drawable);
                }
                if (animation != null) {
                    imageView.startAnimation(animation);
                }
                if (aVar != null) {
                    aVar.a(h.a.SUCCESS);
                }
            }
        }
    }

    private static void b(File file) {
        if (file != null && file.exists() && file.canWrite() && file.isDirectory()) {
            File[] listFiles = file.listFiles();
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    b(listFiles[i]);
                } else if (new Date().getTime() - listFiles[i].lastModified() > 1800) {
                    listFiles[i].delete();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static Drawable c(String str) {
        try {
            return Drawable.createFromPath(str);
        } catch (OutOfMemoryError e) {
            Log.e(b, "Resutil fetchImage OutOfMemoryError:" + e.toString());
            return null;
        }
    }
}
