package com.umeng.common.net;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.RemoteViews;
import com.umeng.common.Log;
import com.umeng.common.b.g;
import com.umeng.common.net.a;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DownloadingService extends Service {
    static final int a = 3;
    static final int b = 4;
    static final int c = 5;
    static final int d = 0;
    static final int e = 1;
    static final int f = 100;
    static final String g = "filename";
    /* access modifiers changed from: private */
    public static final String j = DownloadingService.class.getName();
    Map<a.C0000a, Messenger> h = new HashMap();
    final Messenger i = new Messenger(new b());
    /* access modifiers changed from: private */
    public NotificationManager k;

    class a extends Thread {
        private static final long j = 30000;
        /* access modifiers changed from: private */
        public Context b;
        private String c;
        private Notification d;
        /* access modifiers changed from: private */
        public int e;
        private int f = 0;
        private int g = -1;
        private int h = -1;
        /* access modifiers changed from: private */
        public a.C0000a i;
        private Handler k = new d(this);

        public a(Context context, a.C0000a aVar) {
            try {
                this.b = context;
                this.i = aVar;
                if (com.umeng.common.b.b()) {
                    this.c = Environment.getExternalStorageDirectory().getCanonicalPath();
                    new File(this.c).mkdirs();
                } else {
                    this.c = this.b.getFilesDir().getAbsolutePath();
                }
                this.c = String.valueOf(this.c) + "/download/.um/apk";
                new File(this.c).mkdirs();
                this.d = new Notification(17301633, com.umeng.common.a.j, 1);
                RemoteViews remoteViews = new RemoteViews(this.b.getPackageName(), com.umeng.common.a.b.a(this.b));
                remoteViews.setProgressBar(com.umeng.common.a.a.c(this.b), DownloadingService.f, 0, false);
                remoteViews.setTextViewText(com.umeng.common.a.a.b(this.b), "0%");
                remoteViews.setTextViewText(com.umeng.common.a.a.d(this.b), com.umeng.common.a.j + this.i.b);
                remoteViews.setTextViewText(com.umeng.common.a.a.a(this.b), "");
                remoteViews.setImageViewResource(com.umeng.common.a.a.e(this.b), 17301633);
                this.d.contentView = remoteViews;
                this.d.contentIntent = PendingIntent.getActivity(this.b, 0, new Intent(), 134217728);
                this.e = (int) System.currentTimeMillis();
                if (this.e < 0) {
                    this.e = -this.e;
                }
                DownloadingService.this.k.notify(this.e, this.d);
            } catch (Exception e2) {
                Log.c(DownloadingService.j, e2.getMessage(), e2);
                DownloadingService.this.k.cancel(this.e);
            }
        }

        private void a(Exception exc) {
            Log.b(DownloadingService.j, "can not install. " + exc.getMessage());
            this.d.contentView.setTextViewText(com.umeng.common.a.a.d(this.b), String.valueOf(this.i.b) + com.umeng.common.a.i);
            DownloadingService.this.k.notify(this.e, this.d);
            DownloadingService.this.k.cancel(this.e);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        private void a(boolean z) {
            File fileStreamPath;
            boolean z2;
            InputStream inputStream = null;
            FileOutputStream fileOutputStream = null;
            try {
                String str = String.valueOf(g.a(this.i.c)) + ".apk.tmp";
                if (com.umeng.common.b.b()) {
                    File file = new File(this.c, str);
                    fileStreamPath = file;
                    fileOutputStream = new FileOutputStream(file, true);
                } else {
                    this.c = this.b.getFilesDir().getAbsolutePath();
                    fileOutputStream = this.b.openFileOutput(str, 32771);
                    fileStreamPath = this.b.getFileStreamPath(str);
                }
                Log.c(DownloadingService.j, String.format("saveAPK: url = %1$15s\t|\tfilename = %2$15s", this.i.c, fileStreamPath.getAbsolutePath()));
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.i.c).openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setRequestProperty("Accept-Encoding", "identity");
                httpURLConnection.addRequestProperty("Connection", "keep-alive");
                httpURLConnection.setConnectTimeout(5000);
                httpURLConnection.setReadTimeout(10000);
                if (fileStreamPath.exists() && fileStreamPath.length() > 0) {
                    httpURLConnection.setRequestProperty("Range", "bytes=" + fileStreamPath.length() + "-");
                }
                httpURLConnection.connect();
                inputStream = httpURLConnection.getInputStream();
                if (!z) {
                    this.g = 0;
                    this.h = httpURLConnection.getContentLength();
                    Log.c(DownloadingService.j, String.format("getContentLength: %1$15s", Integer.valueOf(this.h)));
                }
                byte[] bArr = new byte[4096];
                int i2 = 0;
                Log.c(DownloadingService.j, String.valueOf(this.i.b) + "saveAPK getContentLength " + String.valueOf(this.h));
                c.a(this.b).a(this.i.a, this.i.c);
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        z2 = true;
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                    this.g = read + this.g;
                    int i3 = i2 + 1;
                    if (i2 % 50 == 0) {
                        if (!com.umeng.common.b.m(this.b)) {
                            z2 = false;
                            break;
                        }
                        int i4 = (int) ((((float) this.g) * 100.0f) / ((float) this.h));
                        this.d.contentView.setProgressBar(com.umeng.common.a.a.c(this.b), DownloadingService.f, i4, false);
                        this.d.contentView.setTextViewText(com.umeng.common.a.a.b(this.b), String.valueOf(String.valueOf(i4)) + "%");
                        DownloadingService.this.k.notify(this.e, this.d);
                        String a2 = DownloadingService.j;
                        Object[] objArr = new Object[DownloadingService.a];
                        objArr[0] = Integer.valueOf(this.e);
                        objArr[1] = Integer.valueOf(i4);
                        objArr[2] = this.i.b;
                        Log.c(a2, String.format("%3$10s Notification: mNotificationId = %1$15s\t|\tprogress = %2$15s", objArr));
                        try {
                            if (DownloadingService.this.h.get(this.i) != null) {
                                DownloadingService.this.h.get(this.i).send(Message.obtain(null, DownloadingService.a, i4, 0));
                            }
                        } catch (DeadObjectException e2) {
                            Log.b(DownloadingService.j, String.format("Service Client for downloading %1$15s is dead. Removing messenger from the service", this.i.b));
                            DownloadingService.this.h.put(this.i, null);
                        }
                        c.a(this.b).a(this.i.a, this.i.c, i4);
                    }
                    i2 = i3;
                }
                inputStream.close();
                fileOutputStream.close();
                if (!z2) {
                    DownloadingService.this.k.cancel(this.e);
                    DownloadingService.this.h.get(this.i).send(Message.obtain(null, 5, 0, 0));
                    DownloadingService.this.h.remove(this.i);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e3) {
                            e3.printStackTrace();
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                    return;
                                } catch (IOException e4) {
                                    e4.printStackTrace();
                                    return;
                                }
                            } else {
                                return;
                            }
                        } catch (Throwable th) {
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e5) {
                                    e5.printStackTrace();
                                }
                            }
                            throw th;
                        }
                    }
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e6) {
                            e6.printStackTrace();
                        }
                    }
                } else {
                    this.d.contentView.setTextViewText(com.umeng.common.a.a.b(this.b), String.valueOf(String.valueOf((int) DownloadingService.f)) + "%");
                    c.a(this.b).a(this.i.a, this.i.c, (int) DownloadingService.f);
                    File file2 = new File(fileStreamPath.getParent(), fileStreamPath.getName().replace(".tmp", ""));
                    fileStreamPath.renameTo(file2);
                    String absolutePath = file2.getAbsolutePath();
                    Bundle bundle = new Bundle();
                    bundle.putString(DownloadingService.g, absolutePath);
                    Message obtain = Message.obtain();
                    obtain.what = 5;
                    obtain.arg1 = 1;
                    obtain.setData(bundle);
                    this.k.sendMessage(obtain);
                    Message obtain2 = Message.obtain();
                    obtain2.what = 5;
                    obtain2.arg1 = 1;
                    obtain2.setData(bundle);
                    if (DownloadingService.this.h.get(this.i) != null) {
                        DownloadingService.this.h.get(this.i).send(obtain2);
                    }
                    DownloadingService.this.h.remove(this.i);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e7) {
                            e7.printStackTrace();
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                    return;
                                } catch (IOException e8) {
                                    e8.printStackTrace();
                                    return;
                                }
                            } else {
                                return;
                            }
                        } catch (Throwable th2) {
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e9) {
                                    e9.printStackTrace();
                                }
                            }
                            throw th2;
                        }
                    }
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e10) {
                            e10.printStackTrace();
                        }
                    }
                }
            } catch (IOException e11) {
                Log.c(DownloadingService.j, e11.getMessage(), e11);
                int i5 = this.f + 1;
                this.f = i5;
                if (i5 > DownloadingService.a) {
                    a(e11);
                } else {
                    Log.c(DownloadingService.j, "wait for repeating Test network repeat count=" + this.f);
                    try {
                        Thread.sleep(j);
                        if (this.h < 1) {
                            a(false);
                        } else {
                            a(true);
                        }
                    } catch (InterruptedException e12) {
                        a(e12);
                    }
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e13) {
                        e13.printStackTrace();
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                                return;
                            } catch (IOException e14) {
                                e14.printStackTrace();
                                return;
                            }
                        } else {
                            return;
                        }
                    } catch (Throwable th3) {
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e15) {
                                e15.printStackTrace();
                            }
                        }
                        throw th3;
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e16) {
                        e16.printStackTrace();
                    }
                }
            } catch (RemoteException e17) {
                DownloadingService.this.h.remove(this.i);
                e17.printStackTrace();
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e18) {
                        e18.printStackTrace();
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                                return;
                            } catch (IOException e19) {
                                e19.printStackTrace();
                                return;
                            }
                        } else {
                            return;
                        }
                    } catch (Throwable th4) {
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e20) {
                                e20.printStackTrace();
                            }
                        }
                        throw th4;
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e21) {
                        e21.printStackTrace();
                    }
                }
            } catch (Throwable th5) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e22) {
                        e22.printStackTrace();
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e23) {
                                e23.printStackTrace();
                            }
                        }
                    } catch (Throwable th6) {
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e24) {
                                e24.printStackTrace();
                            }
                        }
                        throw th6;
                    }
                }
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e25) {
                        e25.printStackTrace();
                    }
                }
                throw th5;
            }
        }

        public void run() {
            this.f = 0;
            try {
                a(false);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    class b extends Handler {
        b() {
        }

        public void handleMessage(Message message) {
            Log.c(DownloadingService.j, "IncomingHandler(msg.what:" + message.what + " msg.arg1:" + message.arg1 + " msg.arg2:" + message.arg2 + " msg.replyTo:" + message.replyTo);
            switch (message.what) {
                case 4:
                    Bundle data = message.getData();
                    Log.c(DownloadingService.j, "IncomingHandler(msg.getData():" + data);
                    a.C0000a a2 = a.C0000a.a(data);
                    if (DownloadingService.this.b(a2)) {
                        Log.a(DownloadingService.j, String.valueOf(a2.b) + " is already in downloading list. ");
                        return;
                    }
                    DownloadingService.this.h.put(a2, message.replyTo);
                    DownloadingService.this.a(a2);
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(a.C0000a aVar) {
        Log.c(j, "startDownload([mComponentName:" + aVar.a + " mTitle:" + aVar.b + " mUrl:" + aVar.c + "])");
        new a(getApplicationContext(), aVar).start();
    }

    /* access modifiers changed from: private */
    public static boolean b(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return false;
        }
        String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.importance == f && next.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean b(a.C0000a aVar) {
        if (this.h == null) {
            return false;
        }
        for (a.C0000a aVar2 : this.h.keySet()) {
            if (aVar2.c.equals(aVar.c)) {
                return true;
            }
        }
        return false;
    }

    public IBinder onBind(Intent intent) {
        Log.c(j, "onBind ");
        return this.i.getBinder();
    }

    public void onCreate() {
        super.onCreate();
        Log.c(j, "onCreate ");
        this.k = (NotificationManager) getSystemService("notification");
    }

    public void onDestroy() {
        try {
            c.a(getApplicationContext()).a(259200);
            c.a(getApplicationContext()).finalize();
        } catch (Exception e2) {
            Log.b(j, e2.getMessage());
        }
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        Log.c(j, "onStart ");
        super.onStart(intent, i2);
    }
}
