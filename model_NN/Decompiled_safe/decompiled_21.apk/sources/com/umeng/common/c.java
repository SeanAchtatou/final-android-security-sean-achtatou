package com.umeng.common;

import android.content.Context;

public class c {
    private static final String a = c.class.getName();
    private static c b;
    private static Class d = null;
    private static Class e = null;
    private static Class f = null;
    private static Class g = null;
    private static Class h = null;
    private static Class i = null;
    private static Class j = null;
    private Context c;

    private c(Context context) {
        this.c = context;
        try {
            e = Class.forName(String.valueOf(this.c.getPackageName()) + ".R$drawable");
        } catch (ClassNotFoundException e2) {
            Log.b(a, e2.getMessage());
        }
        try {
            f = Class.forName(String.valueOf(this.c.getPackageName()) + ".R$layout");
        } catch (ClassNotFoundException e3) {
            Log.b(a, e3.getMessage());
        }
        try {
            d = Class.forName(String.valueOf(this.c.getPackageName()) + ".R$id");
        } catch (ClassNotFoundException e4) {
            Log.b(a, e4.getMessage());
        }
        try {
            g = Class.forName(String.valueOf(this.c.getPackageName()) + ".R$anim");
        } catch (ClassNotFoundException e5) {
            Log.b(a, e5.getMessage());
        }
        try {
            h = Class.forName(String.valueOf(this.c.getPackageName()) + ".R$style");
        } catch (ClassNotFoundException e6) {
            Log.b(a, e6.getMessage());
        }
        try {
            i = Class.forName(String.valueOf(this.c.getPackageName()) + ".R$string");
        } catch (ClassNotFoundException e7) {
            Log.b(a, e7.getMessage());
        }
        try {
            j = Class.forName(String.valueOf(this.c.getPackageName()) + ".R$array");
        } catch (ClassNotFoundException e8) {
            Log.b(a, e8.getMessage());
        }
    }

    private int a(Class<?> cls, String str) {
        if (cls == null) {
            Log.b(a, "getRes(null," + str + ")");
            throw new IllegalArgumentException("ResClass is not initialized.");
        }
        try {
            return cls.getField(str).getInt(str);
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.b(a, e2.getMessage());
            return -1;
        }
    }

    public static c a(Context context) {
        if (b == null) {
            b = new c(context);
        }
        return b;
    }

    public int a(String str) {
        return a(g, str);
    }

    public int b(String str) {
        return a(d, str);
    }

    public int c(String str) {
        return a(e, str);
    }

    public int d(String str) {
        return a(f, str);
    }

    public int e(String str) {
        return a(h, str);
    }

    public int f(String str) {
        return a(i, str);
    }

    public int g(String str) {
        return a(j, str);
    }
}
