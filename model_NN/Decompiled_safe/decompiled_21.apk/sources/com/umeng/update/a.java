package com.umeng.update;

import android.content.Context;
import android.content.DialogInterface;

class a implements DialogInterface.OnClickListener {
    private final /* synthetic */ Context a;
    private final /* synthetic */ UpdateResponse b;

    a(Context context, UpdateResponse updateResponse) {
        this.a = context;
        this.b = updateResponse;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        UmengUpdateAgent.b(this.a, this.b.path);
    }
}
