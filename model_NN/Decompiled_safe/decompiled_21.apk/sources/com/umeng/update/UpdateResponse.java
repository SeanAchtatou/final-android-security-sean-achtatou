package com.umeng.update;

import com.umeng.common.net.l;
import org.json.JSONObject;

public class UpdateResponse extends l {
    public boolean hasUpdate = false;
    public String path;
    public String updateLog = null;
    public String version = null;

    public UpdateResponse(JSONObject jSONObject) {
        super(jSONObject);
        try {
            if (jSONObject.has("update") && !jSONObject.getString("update").toLowerCase().equals("no")) {
                this.updateLog = jSONObject.getString("update_log");
                this.version = jSONObject.getString("version");
                this.path = jSONObject.getString("path");
                this.hasUpdate = true;
            }
        } catch (Exception e) {
        }
    }
}
