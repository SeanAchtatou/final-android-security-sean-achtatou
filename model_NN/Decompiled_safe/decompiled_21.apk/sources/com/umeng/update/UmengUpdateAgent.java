package com.umeng.update;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;
import com.umeng.common.Log;
import com.umeng.common.b.g;
import com.umeng.common.c;
import com.umeng.common.net.j;
import com.umeng.common.net.k;
import org.json.JSONObject;

public class UmengUpdateAgent {
    public static final String a = "1.0.0.20120516";
    private static boolean b = true;
    /* access modifiers changed from: private */
    public static boolean c = true;
    /* access modifiers changed from: private */
    public static String d = e;
    private static final String e = "update";
    private static UmengUpdateListener f = null;
    private static final String g = "umeng_last_update_time";
    private static final String h = "umeng_update_internal";
    /* access modifiers changed from: private */
    public static final String[] i = {"http://www.umeng.com/api/check_app_update", "http://www.umeng.co/api/check_app_update"};
    private static UmengUpdateAgent j = null;

    public class a extends j implements Runnable {
        Context a;

        public a(Context context) {
            this.a = context;
        }

        private JSONObject a(Context context) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("type", UmengUpdateAgent.e);
                jSONObject.put("appkey", com.umeng.common.b.p(context));
                jSONObject.put(com.umeng.common.a.f, com.umeng.common.b.d(context));
                jSONObject.put(com.umeng.common.a.c, com.umeng.common.b.t(context));
                jSONObject.put("sdk_version", UmengUpdateAgent.a);
                jSONObject.put(com.umeng.common.a.e, g.b(com.umeng.common.b.f(context)));
                jSONObject.put(com.umeng.common.a.d, com.umeng.common.b.s(context));
                return jSONObject;
            } catch (Exception e) {
                Log.b(UmengUpdateAgent.d, "exception in updateInternal", e);
                return null;
            }
        }

        private void b() {
            JSONObject a2 = a(this.a);
            UmengUpdateAgent b2 = UmengUpdateAgent.e();
            b2.getClass();
            b bVar = new b(a2);
            UpdateResponse updateResponse = null;
            for (String a3 : UmengUpdateAgent.i) {
                bVar.a(a3);
                updateResponse = (UpdateResponse) a(bVar, UpdateResponse.class);
                if (updateResponse != null) {
                    break;
                }
            }
            if (updateResponse == null) {
                UmengUpdateAgent.b(UpdateStatus.Timeout, (UpdateResponse) null);
            }
            Log.a(UmengUpdateAgent.d, "response : " + updateResponse.hasUpdate);
            if (updateResponse.hasUpdate) {
                UmengUpdateAgent.b(UpdateStatus.Yes, updateResponse);
                if (UmengUpdateAgent.c) {
                    UmengUpdateAgent.showUpdateDialog(this.a, updateResponse);
                    return;
                }
                return;
            }
            UmengUpdateAgent.b(UpdateStatus.No, (UpdateResponse) null);
        }

        public boolean a() {
            return false;
        }

        public void run() {
            try {
                Looper.prepare();
                b();
                Looper.loop();
            } catch (Exception e) {
                UmengUpdateAgent.b(UpdateStatus.No, (UpdateResponse) null);
                Log.a(UmengUpdateAgent.d, "reques update error", e);
            }
        }
    }

    public class b extends k {
        private JSONObject e;

        public b(JSONObject jSONObject) {
            super(null);
            this.e = jSONObject;
        }

        public JSONObject a() {
            return this.e;
        }

        public String b() {
            return this.c;
        }
    }

    private static SharedPreferences a(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("umeng_analytic_online_setting_");
        stringBuffer.append(com.umeng.common.b.t(context));
        return context.getSharedPreferences(stringBuffer.toString(), 0);
    }

    /* access modifiers changed from: private */
    public static void b(int i2, UpdateResponse updateResponse) {
        if (f != null) {
            f.onUpdateReturned(i2, updateResponse);
        }
    }

    /* access modifiers changed from: private */
    public static void b(Context context, String str) {
        Log.a(e, "url: " + str);
        new com.umeng.common.net.a(context, e, com.umeng.common.b.u(context), str, null).a();
    }

    /* access modifiers changed from: private */
    public static UmengUpdateAgent e() {
        if (j == null) {
            j = new UmengUpdateAgent();
        }
        return j;
    }

    public static void setUpdateAutoPopup(boolean z) {
        c = z;
    }

    public static void setUpdateListener(UmengUpdateListener umengUpdateListener) {
        f = umengUpdateListener;
    }

    public static void setUpdateOnlyWifi(boolean z) {
        b = z;
    }

    public static void showUpdateDialog(Context context, UpdateResponse updateResponse) {
        String str = "";
        try {
            if (!com.umeng.common.b.k(context)) {
                str = String.valueOf(context.getString(c.a(context).f("UMGprsCondition"))) + "\n";
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(str);
            stringBuffer.append(context.getString(c.a(context).f("UMNewVersion")));
            stringBuffer.append(updateResponse.version);
            stringBuffer.append("\n");
            stringBuffer.append(updateResponse.updateLog);
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getString(c.a(context).f("UMUpdateTitle"))).setMessage(stringBuffer.toString()).setPositiveButton(context.getString(c.a(context).f("UMUpdateNow")), new a(context, updateResponse)).setNegativeButton(context.getString(c.a(context).f("UMNotNow")), new b());
            builder.create().show();
        } catch (Exception e2) {
            Log.b(d, "Fail to create update dialog box.", e2);
        }
    }

    public static void update(Context context) {
        try {
            if (b && !com.umeng.common.b.k(context)) {
                b(UpdateStatus.NoneWifi, (UpdateResponse) null);
            } else if (context == null) {
                b(UpdateStatus.No, (UpdateResponse) null);
                Log.b(d, "unexpected null context in update");
            } else {
                UmengUpdateAgent e2 = e();
                e2.getClass();
                new Thread(new a(context)).start();
            }
        } catch (Exception e3) {
            Log.b(d, "Exception occurred in Mobclick.update(). ", e3);
        }
    }

    public static void update(Context context, long j2) {
        if (context == null) {
            Log.a(d, "unexpected null Context");
            return;
        }
        SharedPreferences a2 = a(context);
        long j3 = a2.getLong("umeng_last_update_time", 0);
        long j4 = a2.getLong("umeng_update_internal", j2);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - j3 > j4) {
            update(context);
            a2.edit().putLong("umeng_last_update_time", currentTimeMillis).commit();
        }
    }

    public static void update(Context context, String str) {
        com.umeng.common.a.m = str;
        update(context);
    }
}
