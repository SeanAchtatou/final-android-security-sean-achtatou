package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.umeng.common.Log;
import com.umeng.common.b.e;
import com.umeng.common.b.g;
import com.umeng.fb.f;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.microedition.khronos.opengles.GL10;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobclickAgent {
    private static final String A = "appkey";
    private static final String B = "body";
    private static final String C = "session_id";
    private static final String D = "date";
    private static final String E = "time";
    private static final String F = "start_millis";
    private static final String G = "end_millis";
    private static final String H = "duration";
    private static final String I = "activities";
    private static final String J = "header";
    private static final String K = "uptr";
    private static final String L = "dntr";
    private static final String M = "acc";
    private static final String N = "tag";
    private static final String O = "label";
    private static final String P = "id";
    private static final String Q = "ts";
    private static final String R = "du";
    private static final String S = "context";
    private static final String T = "last_config_time";
    private static final String U = "report_policy";
    private static final String V = "online_params";
    static String a = "";
    static String b = "";
    static boolean c = true;
    /* access modifiers changed from: private */
    public static final MobclickAgent d = new MobclickAgent();
    private static int e = 1;
    private static final int h = 0;
    private static final int i = 1;
    private static final int j = 2;
    private static final int k = 3;
    private static final int l = 4;
    private static final int m = 5;
    private static final int n = 6;
    private static UmengOnlineConfigureListener o = null;
    private static final String p = "type";
    private static final String q = "subtype";
    private static final String r = "error";
    private static final String s = "event";
    private static final String t = "ekv";
    private static final String u = "launch";
    private static final String v = "flush";
    private static final String w = "terminate";
    private static final String x = "online_config";
    private static final String y = "cache_error";
    private static final String z = "send_error";
    private Context f;
    private final Handler g;

    /* renamed from: com.umeng.analytics.MobclickAgent$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[Gender.values().length];

        static {
            try {
                a[Gender.Male.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Gender.Female.ordinal()] = MobclickAgent.j;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Gender.Unknown.ordinal()] = MobclickAgent.k;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    private static final class a extends Thread {
        private static final Object a = new Object();
        private Context b;
        private int c;
        private String d;
        private String e;
        private String f;
        private String g;
        private int h;
        private long i;
        private Map<String, String> j;
        private String k;

        a(Context context, int i2) {
            this.b = context;
            this.c = i2;
        }

        a(Context context, String str, int i2) {
            this.b = context;
            this.c = i2;
            this.d = str;
        }

        a(Context context, String str, String str2, int i2) {
            this.b = context;
            this.c = i2;
            this.d = str;
            this.e = str2;
        }

        a(Context context, String str, String str2, long j2, int i2, int i3) {
            this.b = context;
            this.f = str;
            this.g = str2;
            this.h = i2;
            this.c = i3;
            this.i = j2;
        }

        a(Context context, String str, Map<String, String> map, long j2, int i2) {
            this.b = context;
            this.f = str;
            this.j = map;
            this.c = i2;
            this.i = j2;
        }

        a(Context context, String str, Map<String, String> map, String str2, int i2) {
            this.b = context;
            this.f = str;
            this.j = map;
            this.k = str2;
            this.c = i2;
        }

        public void run() {
            try {
                synchronized (a) {
                    if (this.c == 0) {
                        try {
                            if (this.b == null) {
                                Log.b(f.p, "unexpected null context in invokehander flag=0");
                                return;
                            }
                            MobclickAgent.d.c(this.b);
                        } catch (Exception e2) {
                            Log.b(f.p, "unexpected null context in invokehander flag=0", e2);
                        }
                    } else if (this.c == 1) {
                        MobclickAgent.d.a(this.b, this.d, this.e);
                    } else if (this.c == MobclickAgent.j) {
                        MobclickAgent.d.a(this.b, this.d);
                    } else if (this.c == MobclickAgent.k) {
                        MobclickAgent.d.b(this.b, this.f, this.g, this.i, this.h);
                    } else if (this.c == 4) {
                        MobclickAgent.d.b(this.b, this.f, this.j, this.i);
                    } else if (this.c == 5) {
                        MobclickAgent.d.a(this.b, this.f, this.j, this.k);
                    } else if (this.c == 6) {
                        MobclickAgent.d.b(this.b, this.f, this.k);
                    }
                }
            } catch (Exception e3) {
                Log.b(f.p, "Exception occurred in invokehander.", e3);
            }
        }
    }

    private static final class b implements Runnable {
        private static final Object a = new Object();
        private MobclickAgent b = MobclickAgent.d;
        private Context c;
        private JSONObject d;

        b(MobclickAgent mobclickAgent, Context context, JSONObject jSONObject) {
            this.c = context;
            this.d = jSONObject;
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r4 = this;
                org.json.JSONObject r0 = r4.d     // Catch:{ Exception -> 0x002b }
                java.lang.String r1 = "type"
                java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x002b }
                java.lang.String r1 = "online_config"
                boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x002b }
                if (r0 == 0) goto L_0x001a
                com.umeng.analytics.MobclickAgent r0 = r4.b     // Catch:{ Exception -> 0x002b }
                android.content.Context r1 = r4.c     // Catch:{ Exception -> 0x002b }
                org.json.JSONObject r2 = r4.d     // Catch:{ Exception -> 0x002b }
                r0.c(r1, r2)     // Catch:{ Exception -> 0x002b }
            L_0x0019:
                return
            L_0x001a:
                java.lang.Object r1 = com.umeng.analytics.MobclickAgent.b.a     // Catch:{ Exception -> 0x002b }
                monitor-enter(r1)     // Catch:{ Exception -> 0x002b }
                com.umeng.analytics.MobclickAgent r0 = r4.b     // Catch:{ all -> 0x0028 }
                android.content.Context r2 = r4.c     // Catch:{ all -> 0x0028 }
                org.json.JSONObject r3 = r4.d     // Catch:{ all -> 0x0028 }
                r0.a(r2, r3)     // Catch:{ all -> 0x0028 }
                monitor-exit(r1)     // Catch:{ all -> 0x0028 }
                goto L_0x0019
            L_0x0028:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0028 }
                throw r0     // Catch:{ Exception -> 0x002b }
            L_0x002b:
                r0 = move-exception
                java.lang.String r1 = "MobclickAgent"
                java.lang.String r2 = "Exception occurred in ReportMessageHandler"
                com.umeng.common.Log.b(r1, r2)
                r0.printStackTrace()
                goto L_0x0019
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.analytics.MobclickAgent.b.run():void");
        }
    }

    private MobclickAgent() {
        HandlerThread handlerThread = new HandlerThread(f.p);
        handlerThread.start();
        this.g = new Handler(handlerThread.getLooper());
    }

    static SharedPreferences a(Context context) {
        return context.getSharedPreferences("mobclick_agent_user_" + context.getPackageName(), 0);
    }

    private String a(Context context, SharedPreferences sharedPreferences) {
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong(F, valueOf.longValue());
        edit.putLong(G, -1);
        edit.commit();
        return sharedPreferences.getString(C, null);
    }

    private String a(Context context, String str, long j2) {
        StringBuilder sb = new StringBuilder();
        sb.append(j2).append(str).append(g.b(com.umeng.common.b.f(context)));
        return g.a(sb.toString());
    }

    private String a(Context context, String str, SharedPreferences sharedPreferences) {
        c(context, sharedPreferences);
        long currentTimeMillis = System.currentTimeMillis();
        String a2 = a(context, str, currentTimeMillis);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("appkey", str);
        edit.putString(C, a2);
        edit.putLong(F, currentTimeMillis);
        edit.putLong(G, -1);
        edit.putLong(H, 0);
        edit.putString(I, "");
        edit.commit();
        b(context, sharedPreferences);
        return a2;
    }

    private static String a(Context context, JSONObject jSONObject, String str, boolean z2, String str2) {
        HttpPost httpPost = new HttpPost(str);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        httpPost.addHeader("X-Umeng-Sdk", getUmengHttpHeader(context));
        try {
            String a2 = b.a(context);
            if (a2 != null) {
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(a2, 80));
            }
            String jSONObject2 = jSONObject.toString();
            Log.a(f.p, jSONObject2);
            if (!a.s || z2) {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair(f.S, jSONObject2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, e.f));
            } else {
                byte[] a3 = com.umeng.common.b.f.a("content=" + jSONObject2, "utf-8");
                httpPost.addHeader("Content-Encoding", "deflate");
                httpPost.setEntity(new InputStreamEntity(new ByteArrayInputStream(a3), (long) com.umeng.common.b.f.a));
            }
            SharedPreferences.Editor edit = i(context).edit();
            Date date = new Date();
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            long time = new Date().getTime() - date.getTime();
            if (execute.getStatusLine().getStatusCode() == 200) {
                Log.a(f.p, "Sent message to " + str);
                edit.putLong("req_time", time);
                edit.commit();
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    return a(entity.getContent());
                }
                return null;
            }
            edit.putLong("req_time", -1);
            return null;
        } catch (ClientProtocolException e2) {
            Log.a(f.p, "ClientProtocolException,Failed to send message.", e2);
            return null;
        } catch (IOException e3) {
            Log.a(f.p, "IOException,Failed to send message.", e3);
            return null;
        }
    }

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e2) {
                        Log.b(f.p, "Caught IOException in convertStreamToString()", e2);
                        return null;
                    }
                }
            } catch (IOException e3) {
                Log.b(f.p, "Caught IOException in convertStreamToString()", e3);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e4) {
                    Log.b(f.p, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e5) {
                    Log.b(f.p, "Caught IOException in convertStreamToString()", e5);
                    return null;
                }
            }
        }
    }

    private JSONArray a(JSONObject jSONObject, JSONArray jSONArray) {
        boolean z2;
        try {
            String string = jSONObject.getString(N);
            String string2 = jSONObject.has(O) ? jSONObject.getString(O) : null;
            String string3 = jSONObject.getString(D);
            int length = jSONArray.length() - 1;
            while (true) {
                if (length < 0) {
                    z2 = false;
                    break;
                }
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(length);
                if (string2 == null && !jSONObject2.has(O)) {
                    if (string.equals(jSONObject2.get(N)) && string3.equals(jSONObject2.get(D))) {
                        jSONObject2.put(M, jSONObject2.getInt(M) + 1);
                        z2 = true;
                        break;
                    }
                } else if (string2 != null && jSONObject2.has(O) && string.equals(jSONObject2.get(N)) && string2.equals(jSONObject2.get(O)) && string3.equals(jSONObject2.get(D))) {
                    jSONObject2.put(M, jSONObject2.getInt(M) + 1);
                    z2 = true;
                    break;
                }
                length--;
            }
            if (!z2) {
                jSONArray.put(jSONObject);
            }
        } catch (Exception e2) {
            Log.a(f.p, "custom log merge error in tryToSendMessage", e2);
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }

    private static void a(Context context, int i2) {
        if (i2 < 0 || i2 > 5) {
            Log.b(f.p, "Illegal value of report policy");
            return;
        }
        SharedPreferences n2 = n(context);
        synchronized (a.n) {
            n2.edit().putInt(f.aM, i2).commit();
        }
    }

    private void a(Context context, SharedPreferences sharedPreferences, String str, String str2, long j2, int i2) {
        String string = sharedPreferences.getString(C, "");
        String b2 = b();
        String str3 = b2.split(" ")[0];
        String str4 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", s);
            jSONObject.put(C, string);
            jSONObject.put(D, str3);
            jSONObject.put(E, str4);
            jSONObject.put(N, str);
            if (str2 != null) {
                jSONObject.put(O, str2);
            }
            if (j2 > 0) {
                jSONObject.put(R, j2);
            }
            jSONObject.put(M, i2);
            this.g.post(new b(this, context, jSONObject));
        } catch (JSONException e2) {
            Log.a(f.p, "json error in emitCustomLogReport", e2);
        }
    }

    private void a(Context context, SharedPreferences sharedPreferences, String str, JSONObject jSONObject) {
        String string = sharedPreferences.getString(C, "");
        JSONObject jSONObject2 = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        try {
            jSONObject.put(P, str);
            jSONObject.put(Q, System.currentTimeMillis() / 1000);
            jSONArray.put(jSONObject);
            jSONObject2.put("type", t);
            jSONObject2.put(string, jSONArray);
            this.g.post(new b(this, context, jSONObject2));
        } catch (JSONException e2) {
            Log.a(f.p, "json error in emitCustomLogReport", e2);
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(Context context, String str) {
        String b2 = b(context);
        if (b2 != "" && b2.length() <= 10240) {
            c(context, b2, y);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(Context context, String str, String str2) {
        this.f = context;
        SharedPreferences k2 = k(context);
        if (k2 != null) {
            if (a(k2)) {
                Log.a(f.p, "Start new session: " + a(context, str, k2));
            } else {
                Log.a(f.p, "Extend current session: " + a(context, k2));
            }
        }
    }

    private static void a(Context context, String str, String str2, long j2, int i2) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str) && i2 > 0) {
                    new a(context, str, str2, j2, i2, k).start();
                    return;
                }
            } catch (Exception e2) {
                Log.b(f.p, "Exception occurred in Mobclick.onEvent(). ", e2);
                return;
            }
        }
        a("invalid params in onEvent");
    }

    private static void a(Context context, String str, Map<String, String> map, long j2) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    if (map == null || map.isEmpty()) {
                        a("map is null or empty in onEvent");
                        return;
                    } else {
                        new a(context, str, map, j2, 4).start();
                        return;
                    }
                }
            } catch (Exception e2) {
                Log.b(f.p, "Exception occurred in Mobclick.onEvent(). ", e2);
                return;
            }
        }
        a("invalid params in onKVEventEnd");
    }

    /* access modifiers changed from: private */
    public synchronized void a(Context context, String str, Map<String, String> map, String str2) {
        SharedPreferences k2 = k(context);
        if (k2 != null) {
            try {
                d(context, "_kvts" + str + str2);
                JSONObject jSONObject = new JSONObject();
                Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext() && i2 <= 10) {
                        break;
                    }
                    int i3 = i2 + 1;
                    Map.Entry next = it.next();
                    jSONObject.put((String) next.getKey(), (String) next.getValue());
                    i2 = i3;
                }
                k2.edit().putString("_kvvl" + str + str2, jSONObject.toString()).commit();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.analytics.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean, java.lang.String):java.lang.String
     arg types: [android.content.Context, org.json.JSONObject, java.lang.String, int, java.lang.String]
     candidates:
      com.umeng.analytics.MobclickAgent.a(android.content.Context, java.lang.String, java.lang.String, long, int):void
      com.umeng.analytics.MobclickAgent.a(com.umeng.analytics.MobclickAgent, android.content.Context, java.lang.String, java.util.Map, long):void
      com.umeng.analytics.MobclickAgent.a(com.umeng.analytics.MobclickAgent, android.content.Context, java.lang.String, java.util.Map, java.lang.String):void
      com.umeng.analytics.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean, java.lang.String):java.lang.String */
    /* access modifiers changed from: private */
    public void a(Context context, JSONObject jSONObject) {
        String str = null;
        SharedPreferences i2 = i(context);
        JSONObject e2 = e(context);
        long j2 = i2.getLong("req_time", 0);
        if (j2 != 0) {
            try {
                e2.put("req_time", j2);
            } catch (JSONException e3) {
                Log.a(f.p, "json error in tryToSendMessage", e3);
            }
        }
        i2.edit().putString(J, e2.toString()).commit();
        JSONObject g2 = g(context);
        JSONObject jSONObject2 = new JSONObject();
        try {
            String string = jSONObject.getString("type");
            if (string != null) {
                String str2 = jSONObject.has(q) ? (String) jSONObject.remove(q) : null;
                if (string != v) {
                    jSONObject.remove("type");
                    if (g2 == null) {
                        JSONObject jSONObject3 = new JSONObject();
                        JSONArray jSONArray = new JSONArray();
                        jSONArray.put(jSONObject);
                        jSONObject3.put(string, jSONArray);
                        g2 = jSONObject3;
                    } else if (g2.isNull(string)) {
                        JSONArray jSONArray2 = new JSONArray();
                        jSONArray2.put(jSONObject);
                        g2.put(string, jSONArray2);
                    } else {
                        JSONArray jSONArray3 = g2.getJSONArray(string);
                        if (t.equals(string)) {
                            b(jSONObject, jSONArray3);
                        } else {
                            jSONArray3.put(jSONObject);
                        }
                    }
                }
                if (g2 == null) {
                    Log.e(f.p, "No cache message to flush in tryToSendMessage");
                    return;
                }
                jSONObject2.put(J, e2);
                jSONObject2.put(B, g2);
                if (a(string, context, str2)) {
                    int i3 = 0;
                    while (i3 < a.p.length && (str = a(context, jSONObject2, a.p[i3], false, string)) == null) {
                        i3++;
                    }
                    if (str != null) {
                        Log.a(f.p, "send applog succeed :" + str);
                        h(context);
                        if (e == 4) {
                            SharedPreferences.Editor edit = j(context).edit();
                            edit.putString(com.umeng.common.b.c(), "true");
                            edit.commit();
                            return;
                        }
                        return;
                    }
                    Log.a(f.p, "send applog failed");
                }
                b(context, g2);
            }
        } catch (JSONException e4) {
            Log.b(f.p, "Fail to construct json message in tryToSendMessage.", e4);
            h(context);
        }
    }

    private static void a(String str) {
        Log.a(f.p, str);
    }

    private boolean a(SharedPreferences sharedPreferences) {
        return System.currentTimeMillis() - sharedPreferences.getLong(G, -1) > a.d;
    }

    private static boolean a(String str, Context context, String... strArr) {
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) == 0 && !com.umeng.common.b.m(context)) {
            return false;
        }
        if (str == x) {
            return true;
        }
        if (com.umeng.common.b.k(context)) {
            return true;
        }
        e = o(context);
        if (e == k) {
            return str == v;
        }
        if (str == "error") {
            return strArr == null || strArr.length <= 0 || !strArr[0].equals(z);
        }
        if (e == 1 && str == u) {
            return true;
        }
        if (e == j && str == w) {
            return true;
        }
        if (e == 0) {
            return true;
        }
        if (e == 4) {
            return !j(context).getString(com.umeng.common.b.c(), "false").equals("true") && str.equals(u);
        }
        if (e == 5) {
            return com.umeng.common.b.k(context);
        }
        return false;
    }

    private static String b() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    private static String b(Context context) {
        String str;
        Exception e2;
        try {
            String packageName = context.getPackageName();
            ArrayList arrayList = new ArrayList();
            arrayList.add("logcat");
            arrayList.add("-d");
            arrayList.add("-v");
            arrayList.add("raw");
            arrayList.add("-s");
            arrayList.add("AndroidRuntime:E");
            arrayList.add("-p");
            arrayList.add(packageName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((String[]) arrayList.toArray(new String[arrayList.size()])).getInputStream()), 1024);
            boolean z2 = false;
            String readLine = bufferedReader.readLine();
            str = "";
            boolean z3 = false;
            while (readLine != null) {
                String str2 = readLine.indexOf("thread attach failed") < 0 ? str + readLine + 10 : str;
                if (!z3 && readLine.toLowerCase().indexOf("exception") >= 0) {
                    z3 = true;
                }
                z2 = (z2 || readLine.indexOf(packageName) < 0) ? z2 : true;
                readLine = bufferedReader.readLine();
                str = str2;
            }
            if (str.length() <= 0 || !z3 || !z2) {
                str = "";
            }
            try {
                Runtime.getRuntime().exec("logcat -c");
            } catch (Exception e3) {
                try {
                    Log.b(f.p, "Failed to clear log in exec(logcat -c)", e3);
                } catch (Exception e4) {
                    e2 = e4;
                }
            }
        } catch (Exception e5) {
            Exception exc = e5;
            str = "";
            e2 = exc;
            Log.b(f.p, "Failed to catch error log in catchLogError", e2);
            return str;
        }
        return str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r6.put(r5);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.json.JSONArray b(org.json.JSONObject r5, org.json.JSONArray r6) {
        /*
            r4 = this;
            if (r6 == 0) goto L_0x0004
            if (r5 != 0) goto L_0x0005
        L_0x0004:
            return r6
        L_0x0005:
            java.util.Iterator r0 = r5.keys()     // Catch:{ Exception -> 0x0037 }
            java.lang.Object r0 = r0.next()     // Catch:{ Exception -> 0x0037 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0037 }
            int r1 = r6.length()     // Catch:{ Exception -> 0x0037 }
            int r1 = r1 + -1
            r2 = r1
        L_0x0016:
            if (r2 < 0) goto L_0x0044
            java.lang.Object r1 = r6.get(r2)     // Catch:{ Exception -> 0x0037 }
            org.json.JSONObject r1 = (org.json.JSONObject) r1     // Catch:{ Exception -> 0x0037 }
            boolean r3 = r1.has(r0)     // Catch:{ Exception -> 0x0037 }
            if (r3 == 0) goto L_0x0040
            org.json.JSONArray r2 = r5.getJSONArray(r0)     // Catch:{ Exception -> 0x0037 }
            org.json.JSONArray r1 = r1.getJSONArray(r0)     // Catch:{ Exception -> 0x0037 }
            r0 = 0
            java.lang.Object r0 = r2.get(r0)     // Catch:{ Exception -> 0x0037 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ Exception -> 0x0037 }
            r1.put(r0)     // Catch:{ Exception -> 0x0037 }
            goto L_0x0004
        L_0x0037:
            r0 = move-exception
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r2 = "custom log merge error in tryToSendMessage"
            com.umeng.common.Log.a(r1, r2, r0)
            goto L_0x0004
        L_0x0040:
            int r1 = r2 + -1
            r2 = r1
            goto L_0x0016
        L_0x0044:
            r6.put(r5)     // Catch:{ Exception -> 0x0037 }
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.analytics.MobclickAgent.b(org.json.JSONObject, org.json.JSONArray):org.json.JSONArray");
    }

    private void b(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString(C, null);
        if (string == null) {
            Log.a(f.p, "Missing session_id, ignore message");
            return;
        }
        String b2 = b();
        String str = b2.split(" ")[0];
        String str2 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", u);
            jSONObject.put(C, string);
            jSONObject.put(D, str);
            jSONObject.put(E, str2);
            this.g.post(new b(this, context, jSONObject));
        } catch (JSONException e2) {
            Log.b(f.p, "json error in emitNewSessionReport", e2);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void b(android.content.Context r6, java.lang.String r7) {
        /*
            android.content.SharedPreferences r2 = n(r6)
            java.lang.Object r3 = com.umeng.analytics.a.n
            monitor-enter(r3)
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x007c }
            r4.<init>(r7)     // Catch:{ Exception -> 0x007c }
            java.lang.String r0 = "last_config_time"
            boolean r0 = r4.has(r0)     // Catch:{ Exception -> 0x0089 }
            if (r0 == 0) goto L_0x0027
            android.content.SharedPreferences$Editor r0 = r2.edit()     // Catch:{ Exception -> 0x0089 }
            java.lang.String r1 = "umeng_last_config_time"
            java.lang.String r5 = "last_config_time"
            java.lang.String r5 = r4.getString(r5)     // Catch:{ Exception -> 0x0089 }
            android.content.SharedPreferences$Editor r0 = r0.putString(r1, r5)     // Catch:{ Exception -> 0x0089 }
            r0.commit()     // Catch:{ Exception -> 0x0089 }
        L_0x0027:
            java.lang.String r0 = "report_policy"
            boolean r0 = r4.has(r0)     // Catch:{ Exception -> 0x0092 }
            if (r0 == 0) goto L_0x0042
            android.content.SharedPreferences$Editor r0 = r2.edit()     // Catch:{ Exception -> 0x0092 }
            java.lang.String r1 = "umeng_net_report_policy"
            java.lang.String r5 = "report_policy"
            int r5 = r4.getInt(r5)     // Catch:{ Exception -> 0x0092 }
            android.content.SharedPreferences$Editor r0 = r0.putInt(r1, r5)     // Catch:{ Exception -> 0x0092 }
            r0.commit()     // Catch:{ Exception -> 0x0092 }
        L_0x0042:
            r0 = 0
            java.lang.String r1 = "online_params"
            boolean r1 = r4.has(r1)     // Catch:{ Exception -> 0x0072 }
            if (r1 == 0) goto L_0x00b7
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0072 }
            java.lang.String r0 = "online_params"
            java.lang.String r0 = r4.getString(r0)     // Catch:{ Exception -> 0x0072 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0072 }
            java.util.Iterator r4 = r1.keys()     // Catch:{ Exception -> 0x0072 }
            android.content.SharedPreferences$Editor r2 = r2.edit()     // Catch:{ Exception -> 0x0072 }
        L_0x005e:
            boolean r0 = r4.hasNext()     // Catch:{ Exception -> 0x0072 }
            if (r0 == 0) goto L_0x009b
            java.lang.Object r0 = r4.next()     // Catch:{ Exception -> 0x0072 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0072 }
            java.lang.String r5 = r1.getString(r0)     // Catch:{ Exception -> 0x0072 }
            r2.putString(r0, r5)     // Catch:{ Exception -> 0x0072 }
            goto L_0x005e
        L_0x0072:
            r0 = move-exception
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r2 = "save online config params"
            com.umeng.common.Log.a(r1, r2, r0)     // Catch:{ all -> 0x0086 }
        L_0x007a:
            monitor-exit(r3)     // Catch:{ all -> 0x0086 }
        L_0x007b:
            return
        L_0x007c:
            r0 = move-exception
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "not json string"
            com.umeng.common.Log.a(r0, r1)     // Catch:{ all -> 0x0086 }
            monitor-exit(r3)     // Catch:{ all -> 0x0086 }
            goto L_0x007b
        L_0x0086:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0086 }
            throw r0
        L_0x0089:
            r0 = move-exception
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r5 = "save online config time"
            com.umeng.common.Log.a(r1, r5, r0)     // Catch:{ all -> 0x0086 }
            goto L_0x0027
        L_0x0092:
            r0 = move-exception
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r5 = "save online config policy"
            com.umeng.common.Log.a(r1, r5, r0)     // Catch:{ all -> 0x0086 }
            goto L_0x0042
        L_0x009b:
            r2.commit()     // Catch:{ Exception -> 0x0072 }
            java.lang.String r0 = "MobclickAgent"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0072 }
            r2.<init>()     // Catch:{ Exception -> 0x0072 }
            java.lang.String r4 = "get online setting params: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0072 }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x0072 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0072 }
            com.umeng.common.Log.a(r0, r2)     // Catch:{ Exception -> 0x0072 }
            r0 = r1
        L_0x00b7:
            com.umeng.analytics.UmengOnlineConfigureListener r1 = com.umeng.analytics.MobclickAgent.o     // Catch:{ Exception -> 0x0072 }
            if (r1 == 0) goto L_0x007a
            com.umeng.analytics.UmengOnlineConfigureListener r1 = com.umeng.analytics.MobclickAgent.o     // Catch:{ Exception -> 0x0072 }
            r1.onDataReceived(r0)     // Catch:{ Exception -> 0x0072 }
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.analytics.MobclickAgent.b(android.content.Context, java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context, String str, String str2) {
        SharedPreferences k2 = k(context);
        if (k2 != null) {
            try {
                int e2 = e(context, "_kvts" + str + str2);
                if (e2 < 0) {
                    a("event duration less than 0 in ekvEvnetEnd");
                } else {
                    JSONObject jSONObject = new JSONObject(k2.getString("_kvvl" + str + str2, null));
                    jSONObject.put(R, e2);
                    a(context, k2, str, jSONObject);
                }
            } catch (Exception e3) {
                a("exception in onLogDurationInternalEnd");
            }
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context, String str, String str2, long j2, int i2) {
        SharedPreferences k2 = k(context);
        if (k2 != null) {
            a(context, k2, str, str2, j2, i2);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context, String str, Map<String, String> map, long j2) {
        SharedPreferences k2 = k(context);
        if (k2 != null) {
            try {
                JSONObject jSONObject = new JSONObject();
                Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext() && i2 <= 10) {
                        break;
                    }
                    int i3 = i2 + 1;
                    Map.Entry next = it.next();
                    jSONObject.put((String) next.getKey(), (String) next.getValue());
                    i2 = i3;
                }
                if (j2 > 0) {
                    jSONObject.put(R, j2);
                }
                a(context, k2, str, jSONObject);
            } catch (Exception e2) {
                Log.a(f.p, "exception when convert map to json");
            }
        }
        return;
    }

    private static void b(Context context, JSONObject jSONObject) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(m(context), 0);
            openFileOutput.write(jSONObject.toString().getBytes());
            openFileOutput.close();
        } catch (FileNotFoundException e2) {
            Log.b(f.p, "cache message error", e2);
        } catch (IOException e3) {
            Log.b(f.p, "cache message error", e3);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void c(Context context) {
        if (this.f != context) {
            Log.b(f.p, "onPause() called without context from corresponding onResume()");
        } else {
            this.f = context;
            SharedPreferences k2 = k(context);
            if (k2 != null) {
                long j2 = k2.getLong(F, -1);
                if (j2 == -1) {
                    Log.b(f.p, "onEndSession called before onStartSession");
                } else {
                    long currentTimeMillis = System.currentTimeMillis();
                    long j3 = currentTimeMillis - j2;
                    long j4 = k2.getLong(H, 0);
                    SharedPreferences.Editor edit = k2.edit();
                    if (a.i) {
                        String string = k2.getString(I, "");
                        String name = context.getClass().getName();
                        if (!"".equals(string)) {
                            string = string + ";";
                        }
                        edit.remove(I);
                        edit.putString(I, string + "[" + name + "," + (j3 / 1000) + "]");
                    }
                    edit.putLong(F, -1);
                    edit.putLong(G, currentTimeMillis);
                    edit.putLong(H, j3 + j4);
                    edit.commit();
                }
            }
        }
    }

    private void c(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString(C, null);
        if (string == null) {
            a("Missing session_id, ignore message in emitLastEndSessionReport");
            return;
        }
        Long valueOf = Long.valueOf(sharedPreferences.getLong(H, -1));
        if (valueOf.longValue() <= 0) {
            valueOf = 0L;
        }
        String b2 = b();
        String str = b2.split(" ")[0];
        String str2 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", w);
            jSONObject.put(C, string);
            jSONObject.put(D, str);
            jSONObject.put(E, str2);
            jSONObject.put(H, String.valueOf(valueOf.longValue() / 1000));
            if (a.i) {
                String string2 = sharedPreferences.getString(I, "");
                if (!"".equals(string2)) {
                    String[] split = string2.split(";");
                    JSONArray jSONArray = new JSONArray();
                    for (String jSONArray2 : split) {
                        jSONArray.put(new JSONArray(jSONArray2));
                    }
                    jSONObject.put(I, jSONArray);
                }
            }
            long[] d2 = d(context, sharedPreferences);
            if (d2 != null) {
                jSONObject.put(K, d2[1]);
                jSONObject.put(L, d2[0]);
            }
            this.g.post(new b(this, context, jSONObject));
        } catch (JSONException e2) {
            Log.b(f.p, "json error in emitLastEndSessionReport", e2);
        }
    }

    private synchronized void c(Context context, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", x);
            jSONObject.put("appkey", com.umeng.common.b.p(context));
            jSONObject.put(com.umeng.common.a.f, com.umeng.common.b.d(context));
            jSONObject.put(com.umeng.common.a.c, com.umeng.common.b.t(context));
            jSONObject.put("sdk_version", "4.0");
            jSONObject.put(com.umeng.common.a.e, g.b(com.umeng.common.b.f(context)));
            jSONObject.put(com.umeng.common.a.d, com.umeng.common.b.s(context));
            jSONObject.put(U, o(context));
            jSONObject.put(T, p(context));
            this.g.post(new b(this, context, jSONObject));
        } catch (Exception e2) {
            Log.b(f.p, "exception in onlineConfigInternal");
        }
        return;
    }

    private void c(Context context, String str, String str2) {
        String b2 = b();
        String str3 = b2.split(" ")[0];
        String str4 = b2.split(" ")[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", "error");
            jSONObject.put(S, str);
            jSONObject.put(D, str3);
            jSONObject.put(E, str4);
            jSONObject.put(q, str2);
            this.g.post(new b(this, context, jSONObject));
        } catch (JSONException e2) {
            Log.a(f.p, "json error in emitErrorReport", e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.analytics.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean, java.lang.String):java.lang.String
     arg types: [android.content.Context, org.json.JSONObject, java.lang.String, int, java.lang.String]
     candidates:
      com.umeng.analytics.MobclickAgent.a(android.content.Context, java.lang.String, java.lang.String, long, int):void
      com.umeng.analytics.MobclickAgent.a(com.umeng.analytics.MobclickAgent, android.content.Context, java.lang.String, java.util.Map, long):void
      com.umeng.analytics.MobclickAgent.a(com.umeng.analytics.MobclickAgent, android.content.Context, java.lang.String, java.util.Map, java.lang.String):void
      com.umeng.analytics.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean, java.lang.String):java.lang.String */
    /* access modifiers changed from: private */
    public void c(Context context, JSONObject jSONObject) {
        if (a(x, context, new String[0])) {
            Log.a(f.p, "start to check onlineConfig info ...");
            String a2 = a(context, jSONObject, f.s, true, x);
            if (a2 == null) {
                a2 = a(context, jSONObject, f.t, true, x);
            }
            if (a2 != null) {
                Log.a(f.p, "get onlineConfig info succeed !");
                b(context, a2);
                return;
            }
            Log.a(f.p, "get onlineConfig info failed !");
        }
    }

    private synchronized void d(Context context) {
        f(context);
    }

    private static void d(Context context, String str) {
        SharedPreferences k2 = k(context);
        String[] split = k2.getString(str, "").split(",");
        StringBuilder sb = new StringBuilder();
        if (split != null) {
            for (int i2 = 0; i2 < split.length; i2++) {
                if (i2 >= split.length - 3) {
                    sb.append(",");
                    sb.append(split[i2]);
                }
            }
        }
        sb.append(",");
        sb.append(System.currentTimeMillis());
        k2.edit().putString(str, sb.toString()).commit();
    }

    private long[] d(Context context, SharedPreferences sharedPreferences) {
        try {
            Class<?> cls = Class.forName("android.net.TrafficStats");
            Method method = cls.getMethod("getUidRxBytes", Integer.TYPE);
            Method method2 = cls.getMethod("getUidTxBytes", Integer.TYPE);
            int i2 = context.getApplicationInfo().uid;
            if (i2 == -1) {
                return null;
            }
            long[] jArr = new long[j];
            jArr[0] = ((Long) method.invoke(null, Integer.valueOf(i2))).longValue();
            jArr[1] = ((Long) method2.invoke(null, Integer.valueOf(i2))).longValue();
            if (jArr[0] <= 0 || jArr[1] <= 0) {
                return null;
            }
            long j2 = sharedPreferences.getLong("traffics_up", -1);
            long j3 = sharedPreferences.getLong("traffics_down", -1);
            sharedPreferences.edit().putLong("traffics_up", jArr[1]).putLong("traffics_down", jArr[0]).commit();
            if (j2 <= 0 || j3 <= 0) {
                return null;
            }
            jArr[0] = jArr[0] - j3;
            jArr[1] = jArr[1] - j2;
            if (jArr[0] <= 0 || jArr[1] <= 0) {
                return null;
            }
            return jArr;
        } catch (Exception e2) {
            a("sdk less than 2.2 has get no traffic");
            return null;
        }
    }

    private static int e(Context context, String str) {
        SharedPreferences k2 = k(context);
        try {
            String string = k2.getString(str, "");
            int lastIndexOf = string.lastIndexOf(",");
            if (lastIndexOf < 0) {
                return -1;
            }
            k2.edit().putString(str, string.substring(0, lastIndexOf)).commit();
            return (int) (System.currentTimeMillis() - Long.valueOf(string.substring(lastIndexOf + 1)).longValue());
        } catch (Exception e2) {
            Log.b(f.p, "exception in getting event duration", e2);
            return -1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException} */
    private static JSONObject e(Context context) {
        JSONObject q2;
        JSONObject jSONObject = new JSONObject();
        try {
            String f2 = com.umeng.common.b.f(context);
            if (f2 == null || f2.equals("")) {
                Log.b(f.p, "No device id");
                return null;
            }
            String p2 = com.umeng.common.b.p(context);
            if (p2 == null) {
                Log.b(f.p, "No appkey");
                return null;
            }
            jSONObject.put("device_id", f2);
            jSONObject.put(com.umeng.common.a.e, g.b(f2));
            jSONObject.put(f.ak, Build.MODEL);
            jSONObject.put("appkey", p2);
            jSONObject.put(com.umeng.common.a.d, a.l != null ? a.l : com.umeng.common.b.s(context));
            jSONObject.put(f.ai, com.umeng.common.b.e(context));
            jSONObject.put(com.umeng.common.a.f, com.umeng.common.b.d(context));
            jSONObject.put("sdk_type", "Android");
            jSONObject.put("sdk_version", "4.0");
            jSONObject.put("os", "Android");
            jSONObject.put(f.aj, Build.VERSION.RELEASE);
            jSONObject.put("timezone", com.umeng.common.b.n(context));
            String[] o2 = com.umeng.common.b.o(context);
            if (o2 != null) {
                jSONObject.put("country", o2[0]);
                jSONObject.put("language", o2[1]);
            }
            jSONObject.put("resolution", com.umeng.common.b.q(context));
            String[] j2 = com.umeng.common.b.j(context);
            if (j2 != null && j2[0].equals("2G/3G")) {
                jSONObject.put("access", j2[0]);
                jSONObject.put("access_subtype", j2[1]);
            } else if (j2 != null) {
                jSONObject.put("access", j2[0]);
            } else {
                jSONObject.put("access", "Unknown");
            }
            jSONObject.put("carrier", com.umeng.common.b.r(context));
            if (a.h) {
                Location l2 = com.umeng.common.b.l(context);
                if (l2 != null) {
                    jSONObject.put(f.ae, String.valueOf(l2.getLatitude()));
                    jSONObject.put(f.af, String.valueOf(l2.getLongitude()));
                } else {
                    jSONObject.put(f.ae, 0.0d);
                    jSONObject.put(f.af, 0.0d);
                }
            }
            jSONObject.put("cpu", com.umeng.common.b.a());
            if (!a.equals("")) {
                jSONObject.put("gpu_vender", a);
            }
            if (!b.equals("")) {
                jSONObject.put("gpu_renderer", b);
            }
            if (a.j && (q2 = q(context)) != null) {
                jSONObject.put("uinfo", q2);
            }
            jSONObject.put(com.umeng.common.a.c, com.umeng.common.b.t(context));
            return jSONObject;
        } catch (Exception e2) {
            Log.b(f.p, "getMessageHeader error", e2);
            return null;
        }
    }

    public static void enterPage(Context context, String str) {
        onEvent(context, "_PAGE_", str);
    }

    private void f(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("type", v);
            this.g.post(new b(this, context, jSONObject));
        } catch (JSONException e2) {
            Log.b(f.p, "json error in emitCache");
        }
    }

    public static void flush(Context context) {
        if (context == null) {
            try {
                Log.b(f.p, "unexpected null context in flush");
            } catch (Exception e2) {
                Log.b(f.p, "Exception occurred in Mobclick.flush(). ", e2);
                return;
            }
        }
        d.d(context);
    }

    private static JSONObject g(Context context) {
        try {
            FileInputStream openFileInput = context.openFileInput(m(context));
            byte[] bArr = new byte[16384];
            String str = "";
            while (true) {
                int read = openFileInput.read(bArr);
                if (read == -1) {
                    break;
                }
                str = str + new String(bArr, 0, read);
            }
            if (str.length() == 0) {
                return null;
            }
            try {
                return new JSONObject(str);
            } catch (JSONException e2) {
                openFileInput.close();
                h(context);
                e2.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException | IOException e3) {
            return null;
        }
    }

    public static String getConfigParams(Context context, String str) {
        return n(context).getString(str, "");
    }

    public static String getUmengHttpHeader(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Android");
        stringBuffer.append("/");
        stringBuffer.append("4.0");
        stringBuffer.append(" ");
        try {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(context.getPackageManager().getApplicationLabel(context.getApplicationInfo()).toString());
            stringBuffer2.append("/");
            stringBuffer2.append(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
            stringBuffer2.append(" ");
            stringBuffer2.append(Build.MODEL);
            stringBuffer2.append("/");
            stringBuffer2.append(Build.VERSION.RELEASE);
            stringBuffer2.append(" ");
            stringBuffer2.append(g.b(com.umeng.common.b.f(context)));
            stringBuffer.append(URLEncoder.encode(stringBuffer2.toString()));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return stringBuffer.toString();
    }

    private static void h(Context context) {
        context.deleteFile(l(context));
        context.deleteFile(m(context));
    }

    private static SharedPreferences i(Context context) {
        return context.getSharedPreferences("mobclick_agent_header_" + context.getPackageName(), 0);
    }

    private static SharedPreferences j(Context context) {
        return context.getSharedPreferences("mobclick_agent_update_" + context.getPackageName(), 0);
    }

    private static SharedPreferences k(Context context) {
        return context.getSharedPreferences("mobclick_agent_state_" + context.getPackageName(), 0);
    }

    private static String l(Context context) {
        return "mobclick_agent_header_" + context.getPackageName();
    }

    private static String m(Context context) {
        return "mobclick_agent_cached_" + context.getPackageName();
    }

    private static SharedPreferences n(Context context) {
        return context.getSharedPreferences("mobclick_agent_online_setting_" + context.getPackageName(), 0);
    }

    private static int o(Context context) {
        SharedPreferences n2 = n(context);
        return (!n2.contains(f.aN) || n2.getInt(f.aN, -1) == -1) ? n2.getInt(f.aM, e) : n2.getInt(f.aN, e);
    }

    public static void onError(Context context) {
        try {
            String p2 = com.umeng.common.b.p(context);
            if (p2 == null || p2.length() == 0) {
                Log.b(f.p, "unexpected empty appkey in onError");
            } else if (context == null) {
                Log.b(f.p, "unexpected null context in onError");
            } else {
                new a(context, p2, j).start();
            }
        } catch (Exception e2) {
            Log.b(f.p, "Exception occurred in Mobclick.onError()", e2);
        }
    }

    public static void onEvent(Context context, String str) {
        onEvent(context, str, 1);
    }

    public static void onEvent(Context context, String str, int i2) {
        a(context, str, (String) null, -1, i2);
    }

    public static void onEvent(Context context, String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            a("label is null or empty in onEvent(tag,label)");
        } else {
            a(context, str, str2, -1, 1);
        }
    }

    public static void onEvent(Context context, String str, String str2, int i2) {
        a(context, str, str2, -1, i2);
    }

    public static void onEvent(Context context, String str, Map<String, String> map) {
        a(context, str, map, -1);
    }

    public static void onEventBegin(Context context, String str) {
        if (context == null || TextUtils.isEmpty(str)) {
            a("invalid params in onEventBegin");
        } else {
            d(context, "_t" + str);
        }
    }

    public static void onEventBegin(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            a("invalid params in onEventBegin");
        } else {
            d(context, "_tl" + str + str2);
        }
    }

    public static void onEventDuration(Context context, String str, long j2) {
        if (j2 <= 0) {
            a("duration is not valid in onEventDuration");
        } else {
            a(context, str, (String) null, j2, 1);
        }
    }

    public static void onEventDuration(Context context, String str, String str2, long j2) {
        if (j2 <= 0) {
            a("duration is not valid in onEventDuration");
        } else if (TextUtils.isEmpty(str2)) {
            a("label is null or empty in onEventDuration");
        } else {
            a(context, str, str2, j2, 1);
        }
    }

    public static void onEventDuration(Context context, String str, Map<String, String> map, long j2) {
        if (j2 <= 0) {
            a("duration is not valid in onEventDuration(map)");
        } else {
            a(context, str, map, j2);
        }
    }

    public static void onEventEnd(Context context, String str) {
        int e2 = e(context, "_t" + str);
        if (e2 < 0) {
            a("event duration less than 0 in onEventEnd");
            return;
        }
        a(context, str, (String) null, (long) e2, 1);
    }

    public static void onEventEnd(Context context, String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            a("invalid params in onEventEnd");
            return;
        }
        int e2 = e(context, "_tl" + str + str2);
        if (e2 < 0) {
            a("event duration less than 0 in onEvnetEnd");
            return;
        }
        a(context, str, str2, (long) e2, 1);
    }

    public static void onKVEventBegin(Context context, String str, Map<String, String> map, String str2) {
        if (context == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            a("invalid params in onKVEventBegin");
        } else if (map == null || map.isEmpty()) {
            a("map is null or empty in onKVEventBegin");
        } else {
            new a(context, str, map, str2, 5).start();
        }
    }

    public static void onKVEventEnd(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            a("invalid params in onKVEventEnd");
        } else {
            new a(context, str, (Map<String, String>) null, str2, 6).start();
        }
    }

    public static void onPause(Context context) {
        if (context == null) {
            try {
                Log.b(f.p, "unexpected null context in onPause");
            } catch (Exception e2) {
                Log.b(f.p, "Exception occurred in Mobclick.onRause(). ", e2);
            }
        } else {
            new a(context, 0).start();
        }
    }

    public static void onResume(Context context) {
        onResume(context, com.umeng.common.b.p(context), com.umeng.common.b.s(context));
    }

    public static void onResume(Context context, String str, String str2) {
        try {
            a.l = str2;
            if (context == null) {
                Log.b(f.p, "unexpected null context in onResume");
            } else if (str == null || str.length() == 0) {
                Log.b(f.p, "unexpected empty appkey in onResume");
            } else {
                new a(context, str, str2, 1).start();
            }
        } catch (Exception e2) {
            Log.b(f.p, "Exception occurred in Mobclick.onResume(). ", e2);
        }
    }

    public static void openActivityDurationTrack(boolean z2) {
        a.i = z2;
    }

    private static String p(Context context) {
        return n(context).getString(f.aO, "");
    }

    private static JSONObject q(Context context) {
        JSONObject jSONObject = new JSONObject();
        SharedPreferences a2 = a(context);
        try {
            if (a2.getInt(f.Z, -1) != -1) {
                jSONObject.put(f.F, a2.getInt(f.Z, -1));
            }
            if (a2.getInt("age", -1) != -1) {
                jSONObject.put("age", a2.getInt("age", -1));
            }
            if (!"".equals(a2.getString(f.V, ""))) {
                jSONObject.put(P, a2.getString(f.V, ""));
            }
            if (!"".equals(a2.getString("id_source", ""))) {
                jSONObject.put("url", URLEncoder.encode(a2.getString("id_source", "")));
            }
            if (jSONObject.length() > 0) {
                return jSONObject;
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void reportError(Context context, String str) {
        if (str != null && str != "" && str.length() <= 10240) {
            if (context == null) {
                Log.b(f.p, "unexpected null context in reportError");
            } else {
                d.c(context, str, z);
            }
        }
    }

    public static void setAge(Context context, int i2) {
        SharedPreferences a2 = a(context);
        if (i2 < 0 || i2 > 200) {
            a("not a valid age!");
        } else {
            a2.edit().putInt("age", i2).commit();
        }
    }

    public static void setAutoLocation(boolean z2) {
        c = z2;
    }

    public static void setDebugMode(boolean z2) {
        a.g = z2;
    }

    public static void setDefaultReportPolicy(Context context, int i2) {
        if (i2 < 0 || i2 > 5) {
            Log.b(f.p, "Illegal value of report policy");
            return;
        }
        e = i2;
        a(context, i2);
    }

    public static void setGender(Context context, Gender gender) {
        int i2 = 0;
        SharedPreferences a2 = a(context);
        switch (AnonymousClass1.a[gender.ordinal()]) {
            case 1:
                i2 = 1;
                break;
            case j /*2*/:
                i2 = j;
                break;
        }
        a2.edit().putInt(f.Z, i2).commit();
    }

    public static void setOnlineConfigureListener(UmengOnlineConfigureListener umengOnlineConfigureListener) {
        o = umengOnlineConfigureListener;
    }

    public static void setOpenGLContext(GL10 gl10) {
        if (gl10 != null) {
            String[] a2 = com.umeng.common.b.a(gl10);
            if (a2.length == j) {
                a = a2[0];
                b = a2[1];
            }
        }
    }

    public static void setSessionContinueMillis(long j2) {
        a.d = j2;
    }

    public static void setUserID(Context context, String str, String str2) {
        SharedPreferences a2 = a(context);
        if (TextUtils.isEmpty(str)) {
            a("userID is null or empty");
            return;
        }
        a2.edit().putString(f.V, str).commit();
        if (TextUtils.isEmpty(str2)) {
            a("id source is null or empty");
        } else {
            a2.edit().putString("id_source", str2).commit();
        }
    }

    public static void updateOnlineConfig(Context context) {
        if (context == null) {
            try {
                Log.b(f.p, "unexpected null context in updateOnlineConfig");
            } catch (Exception e2) {
                Log.b(f.p, "exception in updateOnlineConfig");
            }
        } else {
            d.c(context, com.umeng.common.b.p(context));
        }
    }

    public static void updateOnlineConfig(Context context, String str) {
        a.l = str;
        updateOnlineConfig(context);
    }
}
