package com.umeng.fb.ui;

import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.umeng.fb.a.f;
import com.umeng.fb.b.e;
import com.umeng.fb.util.b;
import com.umeng.fb.util.c;
import org.json.JSONObject;

class a implements View.OnClickListener {
    final /* synthetic */ FeedbackConversation a;

    a(FeedbackConversation feedbackConversation) {
        this.a = feedbackConversation;
    }

    public void onClick(View view) {
        JSONObject jSONObject;
        String editable = this.a.h.getText().toString();
        if (editable != null && editable.trim().length() != 0) {
            if (editable.length() > 140) {
                Toast.makeText(this.a, this.a.getString(e.q(this.a)), 0).show();
                return;
            }
            try {
                jSONObject = b.a(this.a, editable, this.a.e.c);
            } catch (Exception e) {
                Toast.makeText(this.a, e.getMessage(), 0).show();
                c.d(this.a, (JSONObject) null);
                Log.d(FeedbackConversation.c, e.getMessage());
                jSONObject = null;
            }
            this.a.h.setText("");
            ((InputMethodManager) this.a.getSystemService("input_method")).hideSoftInputFromWindow(this.a.h.getWindowToken(), 0);
            c.c(this.a, jSONObject);
            this.a.e = c.b(this.a, this.a.e.c);
            this.a.f.a(this.a.e);
            this.a.f.notifyDataSetChanged();
            this.a.setSelection(this.a.f.getCount() - 1);
            FeedbackConversation.executorService.submit(new f(jSONObject, this.a));
        }
    }
}
