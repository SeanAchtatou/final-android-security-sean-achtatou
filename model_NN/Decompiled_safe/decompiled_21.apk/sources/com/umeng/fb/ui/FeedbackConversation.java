package com.umeng.fb.ui;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.umeng.fb.a;
import com.umeng.fb.b;
import com.umeng.fb.b.d;
import com.umeng.fb.b.e;
import com.umeng.fb.f;
import com.umeng.fb.util.ActivityStarter;
import com.umeng.fb.util.c;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FeedbackConversation extends ListActivity {
    static Context a = null;
    static final String c = FeedbackConversation.class.getSimpleName();
    static boolean d = true;
    public static ExecutorService executorService = Executors.newFixedThreadPool(3);
    boolean b = false;
    /* access modifiers changed from: private */
    public b e;
    /* access modifiers changed from: private */
    public b f;
    private TextView g;
    /* access modifiers changed from: private */
    public EditText h;
    /* access modifiers changed from: private */
    public Button i;
    private a j;

    private class a extends BroadcastReceiver {
        private a() {
        }

        /* synthetic */ a(FeedbackConversation feedbackConversation, a aVar) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            String stringExtra = intent.getStringExtra(f.W);
            if (FeedbackConversation.this.e.c.equalsIgnoreCase(stringExtra)) {
                FeedbackConversation.this.e = c.b(FeedbackConversation.this, stringExtra);
                FeedbackConversation.this.f.a(FeedbackConversation.this.e);
                FeedbackConversation.this.f.notifyDataSetChanged();
            }
            if (FeedbackConversation.this.e.b != b.a.Normal) {
                FeedbackConversation.this.h.setEnabled(false);
                FeedbackConversation.this.i.setEnabled(false);
                return;
            }
            FeedbackConversation.this.h.setEnabled(true);
            FeedbackConversation.this.i.setEnabled(true);
        }
    }

    private void a() {
        this.g.setText(getString(e.r(this)));
        this.i.setText(getString(e.s(this)));
    }

    public static void setUserContext(Context context) {
        a = context;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(d.d(this));
        String stringExtra = getIntent().getStringExtra(f.W);
        if (stringExtra != null) {
            this.e = c.b(this, stringExtra);
        }
        try {
            this.f = new b(this, this.e);
        } catch (Exception e2) {
            Log.e(c, "In Feedback class,fail to initialize feedback adapter.");
            finish();
        }
        setListAdapter(this.f);
        setSelection(this.f.getCount() - 1);
        this.g = (TextView) findViewById(com.umeng.fb.b.c.m(this));
        this.h = (EditText) findViewById(com.umeng.fb.b.c.n(this));
        this.i = (Button) findViewById(com.umeng.fb.b.c.o(this));
        this.i.setOnClickListener(new a(this));
        this.h.requestFocus();
        registerForContextMenu(getListView());
        a();
        if (this.e.b != b.a.Normal) {
            this.h.setEnabled(false);
            this.i.setEnabled(false);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (a != null) {
            ActivityStarter.openFeedbackListActivity(a);
        } else {
            ActivityStarter.openFeedbackListActivity(this);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        super.onListItemClick(listView, view, i2, j2);
        com.umeng.fb.a a2 = this.e.a(i2);
        if (a2.g != a.C0001a.Fail) {
            return;
        }
        if (a2.f == a.b.Starting) {
            ActivityStarter.openSendFeedbackActivity(this, this.e);
            finish();
            return;
        }
        this.h.setText(a2.a());
        this.h.setEnabled(true);
        this.i.setEnabled(true);
        c.a(this, this.e, i2);
        this.f.a(this.e);
        this.f.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.j = new a(this, null);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(f.aH);
        intentFilter.addAction(f.aA);
        registerReceiver(this.j, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        unregisterReceiver(this.j);
    }
}
