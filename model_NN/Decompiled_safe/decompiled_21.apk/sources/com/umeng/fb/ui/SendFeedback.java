package com.umeng.fb.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.umeng.common.b.g;
import com.umeng.fb.UMFeedbackService;
import com.umeng.fb.b.d;
import com.umeng.fb.b.e;
import com.umeng.fb.f;
import com.umeng.fb.util.ActivityStarter;
import com.umeng.fb.util.FeedBackListener;
import com.umeng.fb.util.c;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SendFeedback extends Activity {
    static boolean a = true;
    public static ExecutorService executorService = Executors.newFixedThreadPool(3);
    /* access modifiers changed from: private */
    public Spinner b;
    /* access modifiers changed from: private */
    public Spinner c;
    /* access modifiers changed from: private */
    public EditText d;
    private TextView e;
    private TextView f;
    private ImageButton g;
    /* access modifiers changed from: private */
    public FeedBackListener h;
    /* access modifiers changed from: private */
    public Map<String, String> i;
    /* access modifiers changed from: private */
    public Map<String, String> j;
    /* access modifiers changed from: private */
    public JSONObject k;

    private class a implements View.OnClickListener {
        private a() {
        }

        /* synthetic */ a(SendFeedback sendFeedback, a aVar) {
            this();
        }

        public void onClick(View view) {
            SendFeedback.this.finish();
            if (ActivityStarter.lastContext != null) {
                ((Activity) ActivityStarter.lastContext).finish();
            }
        }
    }

    private class b implements View.OnClickListener {
        private b() {
        }

        /* synthetic */ b(SendFeedback sendFeedback, b bVar) {
            this();
        }

        public void onClick(View view) {
            int i = -1;
            String editable = SendFeedback.this.d != null ? SendFeedback.this.d.getText().toString() : null;
            if (g.c(editable)) {
                Toast.makeText(SendFeedback.this, SendFeedback.this.getString(e.a(SendFeedback.this)), 0).show();
            } else if (editable.length() > 140) {
                Toast.makeText(SendFeedback.this, SendFeedback.this.getString(e.b(SendFeedback.this)), 0).show();
            } else {
                int selectedItemPosition = SendFeedback.this.b != null ? SendFeedback.this.b.getSelectedItemPosition() : -1;
                if (SendFeedback.this.c != null) {
                    i = SendFeedback.this.c.getSelectedItemPosition();
                }
                if (SendFeedback.this.h != null) {
                    SendFeedback.this.h.onSubmitFB(SendFeedback.this);
                    SendFeedback.this.i = ActivityStarter.contactMap;
                    SendFeedback.this.j = ActivityStarter.remarkMap;
                    JSONObject jSONObject = new JSONObject();
                    JSONObject jSONObject2 = new JSONObject();
                    JSONObject json = SendFeedback.this.i != null ? SendFeedback.getJSON(SendFeedback.this.i) : null;
                    JSONObject json2 = SendFeedback.this.j != null ? SendFeedback.getJSON(SendFeedback.this.j) : null;
                    if (json != null) {
                        try {
                            jSONObject.put(f.I, json);
                        } catch (Exception e) {
                        }
                    }
                    if (json2 != null) {
                        jSONObject2.put(f.J, json2);
                    }
                    SharedPreferences.Editor edit = SendFeedback.this.getSharedPreferences(f.A, 0).edit();
                    edit.putInt(f.E, selectedItemPosition);
                    edit.putInt(f.F, i);
                    if (jSONObject != null && jSONObject.length() > 0) {
                        edit.putString(f.G, jSONObject.toString());
                    }
                    if (jSONObject2 != null && jSONObject2.length() > 0) {
                        edit.putString(f.H, jSONObject2.toString());
                    }
                    edit.commit();
                    try {
                        SendFeedback.this.k = com.umeng.fb.util.b.a(SendFeedback.this, editable, selectedItemPosition, i, json, json2);
                    } catch (Exception e2) {
                        if (f.h) {
                            e2.printStackTrace();
                        }
                        c.d(SendFeedback.this, SendFeedback.this.k);
                        return;
                    }
                } else {
                    SharedPreferences.Editor edit2 = SendFeedback.this.getSharedPreferences(f.A, 0).edit();
                    edit2.putInt(f.E, selectedItemPosition);
                    edit2.putInt(f.F, i);
                    edit2.commit();
                    try {
                        SendFeedback.this.k = com.umeng.fb.util.b.a(SendFeedback.this, editable, selectedItemPosition, i, null, null);
                    } catch (Exception e3) {
                        if (f.h) {
                            e3.printStackTrace();
                        }
                        c.d(SendFeedback.this, SendFeedback.this.k);
                        return;
                    }
                }
                c.c(SendFeedback.this, SendFeedback.this.k);
                SendFeedback.executorService.submit(new com.umeng.fb.a.f(SendFeedback.this.k, SendFeedback.this));
                SendFeedback.this.startActivity(new Intent(SendFeedback.this, FeedbackConversations.class).setFlags(131072));
                ((InputMethodManager) SendFeedback.this.getSystemService("input_method")).hideSoftInputFromWindow(SendFeedback.this.d.getWindowToken(), 0);
                SendFeedback.this.finish();
            }
        }
    }

    private void a() {
        this.b = (Spinner) findViewById(com.umeng.fb.b.c.a(this));
        this.c = (Spinner) findViewById(com.umeng.fb.b.c.b(this));
        this.e = (TextView) findViewById(com.umeng.fb.b.c.c(this));
        if (ActivityStarter.useGoBackButton) {
            this.f = (TextView) findViewById(com.umeng.fb.b.c.d(this));
            this.f.setVisibility(0);
        } else {
            this.f = (TextView) findViewById(com.umeng.fb.b.c.d(this));
            this.f.setVisibility(4);
        }
        this.d = (EditText) findViewById(com.umeng.fb.b.c.e(this));
        this.g = (ImageButton) findViewById(com.umeng.fb.b.c.f(this));
        if (this.b != null) {
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, getResources().getStringArray(com.umeng.fb.b.a.a(this)));
            arrayAdapter.setDropDownViewResource(17367049);
            this.b.setAdapter((SpinnerAdapter) arrayAdapter);
        }
        if (this.c != null) {
            ArrayAdapter arrayAdapter2 = new ArrayAdapter(this, 17367048, getResources().getStringArray(com.umeng.fb.b.a.b(this)));
            arrayAdapter2.setDropDownViewResource(17367049);
            this.c.setAdapter((SpinnerAdapter) arrayAdapter2);
        }
        if (this.g != null) {
            this.g.setOnClickListener(new e(this));
        }
        b();
        c();
    }

    private void b() {
        if (this.d != null) {
            this.d.setHint(getString(e.d(this)));
        }
        if (this.e != null) {
            this.e.setText(getString(e.e(this)));
        }
        if (this.f != null) {
            this.f.setText(getString(e.f(this)));
        }
    }

    private void c() {
        int e2;
        int d2;
        String stringExtra = getIntent().getStringExtra(f.W);
        if (!(stringExtra == null || this.d == null)) {
            String string = getSharedPreferences(f.z, 0).getString(stringExtra, null);
            if (!g.c(string)) {
                try {
                    this.d.setText(new com.umeng.fb.a(new JSONArray(string).getJSONObject(0)).a());
                    c.a(this, f.z, stringExtra);
                } catch (Exception e3) {
                    if (f.h) {
                        e3.printStackTrace();
                    }
                }
            }
        }
        if (!(this.b == null || (d2 = d()) == -1)) {
            this.b.setSelection(d2);
        }
        if (!(this.c == null || (e2 = e()) == -1)) {
            this.c.setSelection(e2);
        }
        f();
    }

    private int d() {
        return getSharedPreferences(f.A, 0).getInt(f.E, -1);
    }

    private int e() {
        return getSharedPreferences(f.A, 0).getInt(f.F, -1);
    }

    private void f() {
        try {
            SharedPreferences sharedPreferences = getSharedPreferences(f.A, 0);
            String string = sharedPreferences.getString(f.G, "");
            String string2 = sharedPreferences.getString(f.H, "");
            if (string == null || string.length() <= 0) {
                this.i = null;
            } else {
                this.i = getMap(new JSONObject(string).getJSONObject(f.I));
            }
            if (string2 == null || string2.length() <= 0) {
                this.j = null;
            } else {
                this.j = getMap(new JSONObject(string2).getJSONObject(f.J));
            }
            if (this.h != null) {
                this.h.onResetFB(this, this.i, this.j);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public static JSONObject getJSON(Map<String, String> map) {
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry next : map.entrySet()) {
            try {
                jSONObject.put((String) next.getKey(), next.getValue().toString());
            } catch (Exception e2) {
                System.out.println(e2.getMessage());
            }
        }
        return jSONObject;
    }

    public static Map<String, String> getMap(JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            HashMap hashMap = new HashMap();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.get(next).toString());
            }
            return hashMap;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(d.a(this));
        setFBListener(UMFeedbackService.fbListener);
        a();
        if (this.e != null) {
            this.e.setOnClickListener(new b(this, null));
            if (this.d != null) {
                ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 0);
            }
        }
        if (this.f != null) {
            this.f.setOnClickListener(new a(this, null));
            ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 0);
        }
    }

    public void setFBListener(FeedBackListener feedBackListener) {
        this.h = feedBackListener;
    }
}
