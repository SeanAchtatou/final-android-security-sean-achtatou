package com.umeng.fb.util;

import android.content.Context;
import com.umeng.common.a;
import com.umeng.fb.f;
import com.umeng.fb.g;
import org.json.JSONException;
import org.json.JSONObject;

public class b {
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static synchronized JSONObject a(Context context, String str, int i, int i2, JSONObject jSONObject, JSONObject jSONObject2) {
        JSONObject c;
        synchronized (b.class) {
            c = g.c(context);
            try {
                c.put(f.V, c.getString(a.e));
                c.put(f.T, str);
                c.put(f.S, "Not supported on client yet");
                c.put(f.U, com.umeng.common.b.g.a());
                c.put(f.W, a.a("FB", c.getString("appkey"), c.getString(f.V)));
                c.put("type", f.ar);
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put(f.Y, i);
                switch (i2) {
                    case 1:
                        jSONObject3.put(f.Z, "male");
                        jSONObject3.put(f.K, jSONObject);
                        jSONObject3.put(f.L, jSONObject2);
                        c.put("userinfo", jSONObject3);
                        break;
                    case 2:
                        jSONObject3.put(f.Z, "female");
                        jSONObject3.put(f.K, jSONObject);
                        jSONObject3.put(f.L, jSONObject2);
                        c.put("userinfo", jSONObject3);
                        break;
                    default:
                        jSONObject3.put(f.K, jSONObject);
                        jSONObject3.put(f.L, jSONObject2);
                        c.put("userinfo", jSONObject3);
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    public static synchronized JSONObject a(Context context, String str, String str2) {
        JSONObject jSONObject;
        synchronized (b.class) {
            String p = com.umeng.common.b.p(context);
            String g = com.umeng.common.b.g(context);
            jSONObject = new JSONObject();
            try {
                jSONObject.put("type", f.aq);
                jSONObject.put("appkey", p);
                jSONObject.put(f.S, str);
                jSONObject.put(f.V, g);
                jSONObject.put(f.U, com.umeng.common.b.g.a());
                jSONObject.put(f.W, str2);
                jSONObject.put("reply_id", a.a());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jSONObject;
    }

    public static boolean a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return false;
        }
        try {
            return jSONObject.has(f.am) && "ok".equals(jSONObject.getString(f.am));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static synchronized boolean a(JSONObject jSONObject, String str, String str2) {
        boolean z;
        synchronized (b.class) {
            try {
                jSONObject.put(str, str2);
                z = true;
            } catch (JSONException e) {
                e.printStackTrace();
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean b(JSONObject jSONObject) {
        boolean z;
        synchronized (b.class) {
            if (jSONObject.has(f.am)) {
                jSONObject.remove(f.am);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean c(JSONObject jSONObject) {
        boolean a;
        synchronized (b.class) {
            a = a(jSONObject, f.am, "fail");
        }
        return a;
    }

    public static synchronized boolean d(JSONObject jSONObject) {
        boolean a;
        synchronized (b.class) {
            a = a(jSONObject, f.am, f.av);
        }
        return a;
    }

    public static synchronized boolean e(JSONObject jSONObject) {
        boolean a;
        synchronized (b.class) {
            a = a(jSONObject, f.am, "ok");
        }
        return a;
    }
}
