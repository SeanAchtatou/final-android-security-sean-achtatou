package com.umeng.fb.util;

import android.app.Activity;
import java.util.Map;

public interface FeedBackListener {
    void onResetFB(Activity activity, Map<String, String> map, Map<String, String> map2);

    void onSubmitFB(Activity activity);
}
