package com.umeng.fb;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.umeng.fb.b.b;
import com.umeng.fb.b.c;
import com.umeng.fb.b.d;
import com.umeng.fb.b.e;
import com.umeng.fb.ui.FeedbackConversations;
import com.umeng.fb.util.ActivityStarter;
import com.umeng.fb.util.FeedBackListener;
import java.util.Map;

public class UMFeedbackService {
    private static NotificationType a;
    /* access modifiers changed from: private */
    public static Context b;
    private static boolean c = false;
    public static FeedBackListener fbListener;

    /* access modifiers changed from: private */
    public static void b(String str) {
        CharSequence charSequence;
        if (a == NotificationType.NotificationBar) {
            NotificationManager notificationManager = (NotificationManager) b.getSystemService("notification");
            Notification notification = new Notification(b.d(b), b.getString(e.w(b)), System.currentTimeMillis());
            Intent intent = new Intent(b, FeedbackConversations.class);
            intent.setFlags(131072);
            PendingIntent activity = PendingIntent.getActivity(b, 0, intent, 0);
            PackageManager packageManager = b.getPackageManager();
            try {
                charSequence = packageManager.getApplicationLabel(packageManager.getApplicationInfo(b.getPackageName(), 128));
            } catch (PackageManager.NameNotFoundException e) {
                if (f.h) {
                    e.printStackTrace();
                }
                charSequence = null;
            }
            if (charSequence != null) {
                charSequence = String.valueOf(charSequence) + " : ";
            }
            notification.setLatestEventInfo(b, ((Object) charSequence) + b.getString(e.x(b)), b.getString(e.y(b)), activity);
            notification.flags = 16;
            notificationManager.notify(0, notification);
            return;
        }
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(b).inflate(d.f(b), (ViewGroup) null);
        TextView textView = (TextView) linearLayout.findViewById(c.w(b));
        textView.setText(str);
        AlertDialog create = new AlertDialog.Builder(b).create();
        create.show();
        create.setContentView(linearLayout);
        ((TextView) linearLayout.findViewById(c.v(b))).setText(b.getString(e.z(b)));
        ((Button) linearLayout.findViewById(c.x(b))).setOnClickListener(new c(create));
        d dVar = new d(create);
        ((Button) linearLayout.findViewById(c.y(b))).setOnClickListener(dVar);
        textView.setOnClickListener(dVar);
    }

    public static void enableNewReplyNotification(Context context, NotificationType notificationType) {
        e eVar = new e();
        b = context;
        a = notificationType;
        new com.umeng.fb.a.e(context, eVar).start();
        new com.umeng.fb.a.d(context).start();
        c = true;
    }

    public static boolean getHasCheckedReply() {
        return c;
    }

    public static void openUmengFeedbackSDK(Context context) {
        ActivityStarter.openSendFeedbackActivity(context);
    }

    public static void setContactMap(Map<String, String> map) {
        ActivityStarter.contactMap = map;
    }

    public static void setFeedBackListener(FeedBackListener feedBackListener) {
        fbListener = feedBackListener;
    }

    public static void setGoBackButtonVisible() {
        ActivityStarter.useGoBackButton = true;
    }

    public static void setRemarkMap(Map<String, String> map) {
        ActivityStarter.remarkMap = map;
    }
}
