package com.umeng.fb.b;

import android.content.Context;
import com.umeng.common.c;

public class e {
    public static int A(Context context) {
        return c.a(context).f("UMToast_IsUpdating");
    }

    public static int a(Context context) {
        return c.a(context).f("UMEmptyFbNotAllowed");
    }

    public static int b(Context context) {
        return c.a(context).f("UMContentTooLong");
    }

    public static int c(Context context) {
        return c.a(context).f("UMFeedbackUmengTitle");
    }

    public static int d(Context context) {
        return c.a(context).f("UMFeedbackContent");
    }

    public static int e(Context context) {
        return c.a(context).f("UMFeedbackSummit");
    }

    public static int f(Context context) {
        return c.a(context).f("UMFeedbackGoBack");
    }

    public static int g(Context context) {
        return c.a(context).f("UMFbList_ListItem_State_Sending");
    }

    public static int h(Context context) {
        return c.a(context).f("UMFbList_ListItem_State_Fail");
    }

    public static int i(Context context) {
        return c.a(context).f("UMFbList_ListItem_State_Resending");
    }

    public static int j(Context context) {
        return c.a(context).f("UMFbList_ListItem_State_ReSend");
    }

    public static int k(Context context) {
        return c.a(context).f("UMFeedbackListTitle");
    }

    public static int l(Context context) {
        return c.a(context).f("UMViewThread");
    }

    public static int m(Context context) {
        return c.a(context).f("UMDeleteThread");
    }

    public static int n(Context context) {
        return c.a(context).f("UMViewFeedback");
    }

    public static int o(Context context) {
        return c.a(context).f("UMDeleteFeedback");
    }

    public static int p(Context context) {
        return c.a(context).f("UMResendFeedback");
    }

    public static int q(Context context) {
        return c.a(context).f("UMContentTooLong");
    }

    public static int r(Context context) {
        return c.a(context).f("UMFeedbackConversationTitle");
    }

    public static int s(Context context) {
        return c.a(context).f("UMFeedbackSummit");
    }

    public static int t(Context context) {
        return c.a(context).f("UMFb_Atom_State_Sending");
    }

    public static int u(Context context) {
        return c.a(context).f("UMFb_Atom_State_Fail");
    }

    public static int v(Context context) {
        return c.a(context).f("UMFb_Atom_State_Resending");
    }

    public static int w(Context context) {
        return c.a(context).f("UMNewReplyFlick");
    }

    public static int x(Context context) {
        return c.a(context).f("UMNewReplyTitle");
    }

    public static int y(Context context) {
        return c.a(context).f("UMNewReplyHint");
    }

    public static int z(Context context) {
        return c.a(context).f("UMNewReplyAlertTitle");
    }
}
