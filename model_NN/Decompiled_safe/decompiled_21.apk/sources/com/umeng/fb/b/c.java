package com.umeng.fb.b;

import android.content.Context;

public class c {
    public static int a(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_age_spinner");
    }

    public static int b(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_gender_spinner");
    }

    public static int c(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_submit");
    }

    public static int d(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_goback_btn");
    }

    public static int e(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_content");
    }

    public static int f(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_see_list_btn");
    }

    public static int g(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_new_reply_notifier");
    }

    public static int h(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_feedbackpreview");
    }

    public static int i(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_dev_reply");
    }

    public static int j(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_state_or_date");
    }

    public static int k(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_imgBtn_submitFb");
    }

    public static int l(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_um_feedbacklist_title");
    }

    public static int m(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_conversation_title");
    }

    public static int n(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_editTxtFb");
    }

    public static int o(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_btnSendFb");
    }

    public static int p(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_atomLinearLayout");
    }

    public static int q(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_bubble");
    }

    public static int r(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_atomtxt");
    }

    public static int s(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_stateOrTime");
    }

    public static int t(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_atom_left_margin");
    }

    public static int u(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_atom_right_margin");
    }

    public static int v(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_new_reply_alert_title");
    }

    public static int w(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_new_dev_reply_box");
    }

    public static int x(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_exitBtn");
    }

    public static int y(Context context) {
        return com.umeng.common.c.a(context).b("umeng_fb_see_detail_btn");
    }
}
