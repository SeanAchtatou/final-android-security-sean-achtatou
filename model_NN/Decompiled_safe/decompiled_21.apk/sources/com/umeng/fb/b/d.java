package com.umeng.fb.b;

import android.content.Context;
import com.umeng.common.c;

public class d {
    public static int a(Context context) {
        return c.a(context).d("umeng_fb_send_feedback");
    }

    public static int b(Context context) {
        return c.a(context).d("umeng_fb_conversations_item");
    }

    public static int c(Context context) {
        return c.a(context).d("umeng_fb_conversations");
    }

    public static int d(Context context) {
        return c.a(context).d("umeng_fb_conversation");
    }

    public static int e(Context context) {
        return c.a(context).d("umeng_fb_conversation_item");
    }

    public static int f(Context context) {
        return c.a(context).d("umeng_fb_new_reply_alert_dialog");
    }
}
