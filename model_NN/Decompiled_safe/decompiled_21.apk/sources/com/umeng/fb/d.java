package com.umeng.fb;

import android.app.AlertDialog;
import android.view.View;
import com.umeng.fb.util.ActivityStarter;

class d implements View.OnClickListener {
    private final /* synthetic */ AlertDialog a;

    d(AlertDialog alertDialog) {
        this.a = alertDialog;
    }

    public void onClick(View view) {
        ActivityStarter.openFeedbackListActivity(UMFeedbackService.b);
        this.a.dismiss();
    }
}
