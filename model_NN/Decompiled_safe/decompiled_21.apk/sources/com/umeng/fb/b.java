package com.umeng.fb;

import com.umeng.fb.a;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class b implements Comparable<b> {
    public String a = b.class.getSimpleName();
    public a b = a.Normal;
    public String c;
    public a d;
    public a e;
    public List<a> f = new ArrayList();

    public enum a {
        PureSending,
        PureFail,
        HasFail,
        Normal
    }

    public b(JSONArray jSONArray) throws Exception {
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                a aVar = new a(jSONArray.getJSONObject(i));
                if (aVar.g == a.C0001a.Fail) {
                    this.b = a.HasFail;
                }
                this.f.add(aVar);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        if (!this.f.isEmpty()) {
            this.d = this.f.get(0);
            this.e = this.f.get(this.f.size() - 1);
            this.c = this.d.c;
            if (this.f.size() != 1) {
                return;
            }
            if (this.f.get(0).g == a.C0001a.Fail) {
                this.b = a.PureFail;
            } else if (this.f.get(0).g == a.C0001a.Sending) {
                this.b = a.PureSending;
            }
        }
    }

    /* renamed from: a */
    public int compareTo(b bVar) {
        Date date = this.e.e;
        Date date2 = bVar.e.e;
        if (date2 == null || date == null || date.equals(date2)) {
            return 0;
        }
        return date.after(date2) ? -1 : 1;
    }

    public a a(int i) {
        if (i < 0 || i > this.f.size() - 1) {
            return null;
        }
        return this.f.get(i);
    }

    public void b(int i) {
        if (i >= 0 && i <= this.f.size() - 1) {
            this.f.remove(i);
        }
    }
}
