package com.umeng.fb;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;
import com.umeng.common.a;
import com.umeng.common.b;
import com.umeng.common.b.e;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

public class g {
    public static int a(Date date, Date date2) {
        if (!date.after(date2)) {
            Date date3 = date2;
            date2 = date;
            date = date3;
        }
        return (int) ((date.getTime() - date2.getTime()) / 1000);
    }

    public static String a() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String a(JSONObject jSONObject) {
        try {
            if (jSONObject.has(a.d)) {
                jSONObject.put(a.d, URLEncoder.encode(jSONObject.getString(a.d), e.f));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public static Date a(String str) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
        } catch (Exception e) {
            return null;
        }
    }

    public static void a(Context context) {
        Toast.makeText(context, context.getString(com.umeng.fb.b.e.A(context)), 0).show();
    }

    public static void a(Context context, Date date) {
        SharedPreferences.Editor edit = context.getSharedPreferences("exchange_last_request_time", 0).edit();
        edit.putString("last_request_time", b.a(date));
        edit.commit();
    }

    public static String b(JSONObject jSONObject) {
        try {
            JSONObject optJSONObject = jSONObject.optJSONObject("header");
            if (optJSONObject != null) {
                if (optJSONObject.has("access_subtype")) {
                    optJSONObject.put("access_subtype", URLEncoder.encode(optJSONObject.getString("access_subtype"), e.f));
                }
                if (optJSONObject.has("cpu")) {
                    optJSONObject.put("cpu", URLEncoder.encode(optJSONObject.getString("cpu"), e.f));
                }
                if (optJSONObject.has(f.ai)) {
                    optJSONObject.put(f.ai, URLEncoder.encode(optJSONObject.getString(f.ai), e.f));
                }
                if (optJSONObject.has("country")) {
                    optJSONObject.put("country", URLEncoder.encode(optJSONObject.getString("country"), e.f));
                }
                if (optJSONObject.has(f.ak)) {
                    optJSONObject.put(f.ak, URLEncoder.encode(optJSONObject.getString(f.ak), e.f));
                }
                if (optJSONObject.has("carrier")) {
                    optJSONObject.put("carrier", URLEncoder.encode(optJSONObject.getString("carrier"), e.f));
                }
                if (optJSONObject.has("language")) {
                    optJSONObject.put("language", URLEncoder.encode(optJSONObject.getString("language"), e.f));
                }
                if (optJSONObject.has(a.d)) {
                    optJSONObject.put(a.d, URLEncoder.encode(optJSONObject.getString(a.d), e.f));
                }
            }
            JSONObject optJSONObject2 = jSONObject.optJSONObject("body");
            JSONArray optJSONArray = optJSONObject2.optJSONArray("event");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject3 = optJSONArray.optJSONObject(i);
                    if (optJSONObject3 != null) {
                        if (optJSONObject3.has("label")) {
                            optJSONObject3.put("label", URLEncoder.encode(optJSONObject3.getString("label")));
                        }
                        if (optJSONObject3.has("tag")) {
                            optJSONObject3.put("tag", URLEncoder.encode(optJSONObject3.getString("tag")));
                        }
                    }
                }
            }
            JSONArray optJSONArray2 = optJSONObject2.optJSONArray(f.an);
            if (optJSONArray2 != null) {
                for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                    JSONObject optJSONObject4 = optJSONArray2.optJSONObject(i2);
                    if (optJSONObject4 != null && optJSONObject4.has("context")) {
                        optJSONObject4.put("context", URLEncoder.encode(optJSONObject4.getString("context")));
                    }
                }
            }
            return jSONObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return jSONObject.toString();
        }
    }

    public static Date b(Context context) {
        return a(context.getSharedPreferences("exchange_last_request_time", 0).getString("last_request_time", "1900-01-01 00:00:00"));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0179, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x017c, code lost:
        if (com.umeng.fb.f.h != false) goto L_0x017e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x017e, code lost:
        android.util.Log.e(com.umeng.fb.f.p, "getMessageHeader error");
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONObject c(android.content.Context r6) {
        /*
            r2 = 0
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r6.getSystemService(r0)     // Catch:{ Exception -> 0x0179 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = com.umeng.common.b.g(r6)     // Catch:{ Exception -> 0x0179 }
            if (r1 == 0) goto L_0x001c
            java.lang.String r4 = ""
            boolean r4 = r1.equals(r4)     // Catch:{ Exception -> 0x0179 }
            if (r4 == 0) goto L_0x0029
        L_0x001c:
            boolean r0 = com.umeng.fb.f.h     // Catch:{ Exception -> 0x0179 }
            if (r0 == 0) goto L_0x0027
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "No device id"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x0179 }
        L_0x0027:
            r0 = r2
        L_0x0028:
            return r0
        L_0x0029:
            java.lang.String r4 = "device_id"
            java.lang.String r5 = com.umeng.common.b.f(r6)     // Catch:{ Exception -> 0x0179 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r4 = "idmd5"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = "device_model"
            java.lang.String r4 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0179 }
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = com.umeng.common.b.p(r6)     // Catch:{ Exception -> 0x0179 }
            if (r1 != 0) goto L_0x0051
            boolean r0 = com.umeng.fb.f.h     // Catch:{ Exception -> 0x0179 }
            if (r0 == 0) goto L_0x004f
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "No appkey"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x0179 }
        L_0x004f:
            r0 = r2
            goto L_0x0028
        L_0x0051:
            java.lang.String r4 = "appkey"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = com.umeng.fb.f.m     // Catch:{ Exception -> 0x0179 }
            if (r1 == 0) goto L_0x0154
            java.lang.String r1 = com.umeng.fb.f.m     // Catch:{ Exception -> 0x0179 }
        L_0x005c:
            java.lang.String r4 = "channel"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0179 }
            android.content.pm.PackageManager r1 = r6.getPackageManager()     // Catch:{ NameNotFoundException -> 0x015a }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ NameNotFoundException -> 0x015a }
            r5 = 0
            android.content.pm.PackageInfo r1 = r1.getPackageInfo(r4, r5)     // Catch:{ NameNotFoundException -> 0x015a }
            java.lang.String r4 = r1.versionName     // Catch:{ NameNotFoundException -> 0x015a }
            int r1 = r1.versionCode     // Catch:{ NameNotFoundException -> 0x015a }
            java.lang.String r5 = "app_version"
            r3.put(r5, r4)     // Catch:{ NameNotFoundException -> 0x015a }
            java.lang.String r4 = "version_code"
            r3.put(r4, r1)     // Catch:{ NameNotFoundException -> 0x015a }
        L_0x007c:
            java.lang.String r1 = "sdk_type"
            java.lang.String r4 = "Android"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = "sdk_version"
            java.lang.String r4 = "4.0.0.20120516"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = "os"
            java.lang.String r4 = "Android"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = "os_version"
            java.lang.String r4 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0179 }
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            android.content.res.Configuration r1 = new android.content.res.Configuration     // Catch:{ Exception -> 0x0179 }
            r1.<init>()     // Catch:{ Exception -> 0x0179 }
            android.content.ContentResolver r4 = r6.getContentResolver()     // Catch:{ Exception -> 0x0179 }
            android.provider.Settings.System.getConfiguration(r4, r1)     // Catch:{ Exception -> 0x0179 }
            if (r1 == 0) goto L_0x019d
            java.util.Locale r4 = r1.locale     // Catch:{ Exception -> 0x0179 }
            if (r4 == 0) goto L_0x019d
            java.lang.String r4 = "country"
            java.util.Locale r5 = r1.locale     // Catch:{ Exception -> 0x0179 }
            java.lang.String r5 = r5.getCountry()     // Catch:{ Exception -> 0x0179 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r4 = "language"
            java.util.Locale r5 = r1.locale     // Catch:{ Exception -> 0x0179 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0179 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0179 }
            java.util.Locale r1 = r1.locale     // Catch:{ Exception -> 0x0179 }
            java.util.Calendar r1 = java.util.Calendar.getInstance(r1)     // Catch:{ Exception -> 0x0179 }
            if (r1 == 0) goto L_0x0194
            java.util.TimeZone r1 = r1.getTimeZone()     // Catch:{ Exception -> 0x0179 }
            if (r1 == 0) goto L_0x018b
            java.lang.String r4 = "timezone"
            int r1 = r1.getRawOffset()     // Catch:{ Exception -> 0x0179 }
            r5 = 3600000(0x36ee80, float:5.044674E-39)
            int r1 = r1 / r5
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0179 }
        L_0x00db:
            android.util.DisplayMetrics r4 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x0219 }
            r4.<init>()     // Catch:{ Exception -> 0x0219 }
            java.lang.String r1 = "window"
            java.lang.Object r1 = r6.getSystemService(r1)     // Catch:{ Exception -> 0x0219 }
            android.view.WindowManager r1 = (android.view.WindowManager) r1     // Catch:{ Exception -> 0x0219 }
            android.view.Display r1 = r1.getDefaultDisplay()     // Catch:{ Exception -> 0x0219 }
            r1.getMetrics(r4)     // Catch:{ Exception -> 0x0219 }
            int r1 = r4.widthPixels     // Catch:{ Exception -> 0x0219 }
            int r4 = r4.heightPixels     // Catch:{ Exception -> 0x0219 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0219 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0219 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0219 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x0219 }
            java.lang.String r4 = "*"
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ Exception -> 0x0219 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x0219 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x0219 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0219 }
            java.lang.String r4 = "resolution"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0219 }
        L_0x0117:
            java.lang.String[] r1 = com.umeng.common.b.j(r6)     // Catch:{ Exception -> 0x0231 }
            java.lang.String r4 = "access"
            r5 = 0
            r5 = r1[r5]     // Catch:{ Exception -> 0x0231 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0231 }
            r4 = 0
            r4 = r1[r4]     // Catch:{ Exception -> 0x0231 }
            java.lang.String r5 = "2G/3G"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x0231 }
            if (r4 == 0) goto L_0x0136
            java.lang.String r4 = "access_subtype"
            r5 = 1
            r1 = r1[r5]     // Catch:{ Exception -> 0x0231 }
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0231 }
        L_0x0136:
            java.lang.String r1 = "carrier"
            java.lang.String r0 = r0.getNetworkOperatorName()     // Catch:{ Exception -> 0x0249 }
            r3.put(r1, r0)     // Catch:{ Exception -> 0x0249 }
        L_0x013f:
            java.lang.String r0 = com.umeng.common.b.a()     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = "cpu"
            r3.put(r1, r0)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r0 = r6.getPackageName()     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = "package"
            r3.put(r1, r0)     // Catch:{ Exception -> 0x0179 }
            r0 = r3
            goto L_0x0028
        L_0x0154:
            java.lang.String r1 = com.umeng.common.b.s(r6)     // Catch:{ Exception -> 0x0179 }
            goto L_0x005c
        L_0x015a:
            r1 = move-exception
            boolean r4 = com.umeng.fb.f.h     // Catch:{ Exception -> 0x0179 }
            if (r4 == 0) goto L_0x0169
            java.lang.String r4 = "MobclickAgent"
            java.lang.String r5 = "read version fail"
            android.util.Log.i(r4, r5)     // Catch:{ Exception -> 0x0179 }
            r1.printStackTrace()     // Catch:{ Exception -> 0x0179 }
        L_0x0169:
            java.lang.String r1 = "app_version"
            java.lang.String r4 = "unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = "version_code"
            java.lang.String r4 = "unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            goto L_0x007c
        L_0x0179:
            r0 = move-exception
            boolean r1 = com.umeng.fb.f.h
            if (r1 == 0) goto L_0x0188
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r3 = "getMessageHeader error"
            android.util.Log.e(r1, r3)
            r0.printStackTrace()
        L_0x0188:
            r0 = r2
            goto L_0x0028
        L_0x018b:
            java.lang.String r1 = "timezone"
            r4 = 8
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            goto L_0x00db
        L_0x0194:
            java.lang.String r1 = "timezone"
            r4 = 8
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            goto L_0x00db
        L_0x019d:
            java.util.Locale r1 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0179 }
            boolean r4 = com.umeng.fb.f.l     // Catch:{ Exception -> 0x0179 }
            if (r4 == 0) goto L_0x0202
            if (r1 == 0) goto L_0x0202
            java.lang.String r4 = r1.getCountry()     // Catch:{ Exception -> 0x0179 }
            boolean r5 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x0179 }
            if (r5 != 0) goto L_0x01e0
            java.lang.String r5 = "country"
            r3.put(r5, r4)     // Catch:{ Exception -> 0x0179 }
        L_0x01b6:
            java.lang.String r4 = r1.getLanguage()     // Catch:{ Exception -> 0x0179 }
            boolean r5 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x0179 }
            if (r5 != 0) goto L_0x01e8
            java.lang.String r5 = "language"
            r3.put(r5, r4)     // Catch:{ Exception -> 0x0179 }
        L_0x01c5:
            java.util.Calendar r1 = java.util.Calendar.getInstance(r1)     // Catch:{ Exception -> 0x0179 }
            if (r1 == 0) goto L_0x01f9
            java.util.TimeZone r1 = r1.getTimeZone()     // Catch:{ Exception -> 0x0179 }
            if (r1 == 0) goto L_0x01f0
            java.lang.String r4 = "timezone"
            int r1 = r1.getRawOffset()     // Catch:{ Exception -> 0x0179 }
            r5 = 3600000(0x36ee80, float:5.044674E-39)
            int r1 = r1 / r5
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0179 }
            goto L_0x00db
        L_0x01e0:
            java.lang.String r4 = "country"
            java.lang.String r5 = "Unknown"
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0179 }
            goto L_0x01b6
        L_0x01e8:
            java.lang.String r4 = "language"
            java.lang.String r5 = "Unknown"
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0179 }
            goto L_0x01c5
        L_0x01f0:
            java.lang.String r1 = "timezone"
            r4 = 8
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            goto L_0x00db
        L_0x01f9:
            java.lang.String r1 = "timezone"
            r4 = 8
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            goto L_0x00db
        L_0x0202:
            java.lang.String r1 = "country"
            java.lang.String r4 = "Unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = "language"
            java.lang.String r4 = "Unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r1 = "timezone"
            r4 = 8
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            goto L_0x00db
        L_0x0219:
            r1 = move-exception
            boolean r4 = com.umeng.fb.f.h     // Catch:{ Exception -> 0x0179 }
            if (r4 == 0) goto L_0x0228
            java.lang.String r4 = "MobclickAgent"
            java.lang.String r5 = "read resolution fail"
            android.util.Log.e(r4, r5)     // Catch:{ Exception -> 0x0179 }
            r1.printStackTrace()     // Catch:{ Exception -> 0x0179 }
        L_0x0228:
            java.lang.String r1 = "resolution"
            java.lang.String r4 = "Unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            goto L_0x0117
        L_0x0231:
            r1 = move-exception
            boolean r4 = com.umeng.fb.f.h     // Catch:{ Exception -> 0x0179 }
            if (r4 == 0) goto L_0x0240
            java.lang.String r4 = "MobclickAgent"
            java.lang.String r5 = "read access fail"
            android.util.Log.i(r4, r5)     // Catch:{ Exception -> 0x0179 }
            r1.printStackTrace()     // Catch:{ Exception -> 0x0179 }
        L_0x0240:
            java.lang.String r1 = "access"
            java.lang.String r4 = "Unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0179 }
            goto L_0x0136
        L_0x0249:
            r0 = move-exception
            boolean r1 = com.umeng.fb.f.h     // Catch:{ Exception -> 0x0179 }
            if (r1 == 0) goto L_0x0258
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r4 = "read carrier fail"
            android.util.Log.i(r1, r4)     // Catch:{ Exception -> 0x0179 }
            r0.printStackTrace()     // Catch:{ Exception -> 0x0179 }
        L_0x0258:
            java.lang.String r0 = "carrier"
            java.lang.String r1 = "Unknown"
            r3.put(r0, r1)     // Catch:{ Exception -> 0x0179 }
            goto L_0x013f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.fb.g.c(android.content.Context):org.json.JSONObject");
    }
}
