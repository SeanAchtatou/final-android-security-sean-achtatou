package com.umeng.fb.a;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.umeng.common.b.g;
import com.umeng.fb.a;
import com.umeng.fb.b;
import com.umeng.fb.f;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONArray;
import org.json.JSONException;

public class d extends Thread {
    private static ExecutorService b = Executors.newFixedThreadPool(3);
    private Context a;

    public d(Context context) {
        this.a = context;
    }

    public void run() {
        boolean z = false;
        ArrayList<b> arrayList = new ArrayList<>();
        SharedPreferences sharedPreferences = this.a.getSharedPreferences(f.z, 0);
        for (String string : sharedPreferences.getAll().keySet()) {
            String string2 = sharedPreferences.getString(string, null);
            if (!g.c(string2) && string2.indexOf("fail") != -1) {
                try {
                    arrayList.add(new b(new JSONArray(string2)));
                } catch (Exception e) {
                }
            }
        }
        for (b bVar : arrayList) {
            if (!(bVar.b == b.a.Normal || bVar.b == b.a.PureSending)) {
                boolean z2 = z;
                int i = -1;
                for (a next : bVar.f) {
                    i++;
                    if (next.g == a.C0001a.Fail) {
                        try {
                            JSONArray jSONArray = new JSONArray(sharedPreferences.getString(bVar.c, null));
                            jSONArray.put(i, next.h.put(f.am, a.C0001a.Resending));
                            sharedPreferences.edit().putString(bVar.c, jSONArray.toString()).commit();
                            f fVar = new f(next.h, this.a);
                            if (fVar != null) {
                                b.submit(fVar);
                                z2 = true;
                            }
                        } catch (JSONException e2) {
                        }
                    }
                }
                z = z2;
            }
        }
        if (z) {
            this.a.sendBroadcast(new Intent().setAction(f.aH));
        }
    }
}
