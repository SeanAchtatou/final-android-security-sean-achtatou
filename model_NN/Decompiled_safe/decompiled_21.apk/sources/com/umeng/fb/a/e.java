package com.umeng.fb.a;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.umeng.common.b;
import com.umeng.common.b.g;
import com.umeng.fb.f;
import com.umeng.fb.util.a;
import com.umeng.fb.util.c;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;

public class e extends Thread {
    static final String a = e.class.getSimpleName();
    Context b;
    String c;
    String d;
    String e;
    int f;
    Handler g;

    public e(Context context) {
        this.c = "http://feedback.whalecloud.com/feedback/reply";
        this.f = 0;
        this.b = context;
        this.d = b.p(context);
        this.e = b.g(context);
    }

    public e(Context context, Handler handler) {
        this(context);
        this.g = handler;
    }

    public void run() {
        String str;
        JSONArray jSONArray;
        String str2 = "";
        Iterator<String> it = this.b.getSharedPreferences(f.z, 0).getAll().keySet().iterator();
        while (true) {
            str = str2;
            if (!it.hasNext()) {
                break;
            }
            str2 = it.next();
            if (str.length() != 0) {
                str2 = String.valueOf(str) + "," + str2;
            }
        }
        String string = this.b.getSharedPreferences(f.A, 0).getString(f.C, "RP0");
        if (!g.c(str)) {
            this.c = String.valueOf(this.c) + "?appkey=" + this.d + "&feedback_id=" + str;
            if (!string.equals("RP0")) {
                this.c = String.valueOf(this.c) + "&startkey=" + string;
            }
            Log.d(a, this.c);
            String a2 = a.a(this.c);
            Intent intent = new Intent();
            intent.setAction(f.aA);
            if (a2 != null) {
                try {
                    jSONArray = new JSONArray(a2);
                } catch (JSONException e2) {
                    e2.printStackTrace();
                    jSONArray = null;
                }
                String a3 = c.a(this.b, jSONArray);
                Log.d(a, "newReplyIds :" + a3);
                if (a3.length() == 0 || a3.split(",").length == 0) {
                    intent.putExtra(f.aA, 0);
                } else {
                    intent.putExtra(f.aA, 1);
                    if (this.g != null) {
                        String[] split = a3.split(",");
                        com.umeng.fb.a aVar = c.b(this.b, split[split.length - 1]).e;
                        Message message = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putString("newReplyContent", aVar.a());
                        message.setData(bundle);
                        this.g.sendMessage(message);
                    }
                }
            } else {
                intent.putExtra(f.aA, -1);
            }
            this.b.sendBroadcast(intent);
        }
    }
}
