package com.umeng.fb.a;

import android.util.Log;
import com.umeng.common.b.e;
import com.umeng.common.net.j;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class a extends j {
    private static final String a = a.class.getName();
    private static final int b = 30000;

    public c a(b bVar) {
        HttpUriRequest httpGet;
        int nextInt = new Random().nextInt(1000);
        String str = bVar.f;
        String str2 = bVar.d;
        JSONObject jSONObject = bVar.e;
        if (!(bVar instanceof b)) {
            Log.e(a, "request type error, request must be type of FbReportRequest");
            return null;
        } else if (str.length() <= 1) {
            Log.e(a, String.valueOf(nextInt) + ":\tInvalid baseUrl.");
            return null;
        } else {
            Log.i(a, String.valueOf(nextInt) + ": post: " + str + " " + jSONObject.toString());
            Log.i(a, String.valueOf(nextInt) + ":\tget: " + str);
            if (str2 != null) {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair(str2, jSONObject.toString()));
                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(arrayList, e.f);
                    HttpPost httpPost = new HttpPost(str);
                    httpPost.addHeader(urlEncodedFormEntity.getContentType());
                    httpPost.setEntity(urlEncodedFormEntity);
                    httpGet = httpPost;
                } catch (UnsupportedEncodingException e) {
                    throw new AssertionError(e);
                }
            } else {
                httpGet = new HttpGet(str);
            }
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpParams params = defaultHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, b);
            HttpConnectionParams.setSoTimeout(params, b);
            ConnManagerParams.setTimeout(params, 30000);
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() != 200) {
                    return null;
                }
                String entityUtils = EntityUtils.toString(execute.getEntity());
                Log.i("FbClinet", "res :" + entityUtils);
                return new c(new JSONObject(entityUtils));
            } catch (ClientProtocolException e2) {
                Log.d(a, String.valueOf(nextInt) + ":\tClientProtocolException,Failed to send message." + str, e2);
                return null;
            } catch (Exception e3) {
                Log.d(a, String.valueOf(nextInt) + ":\tIOException,Failed to send message." + str, e3);
                return null;
            }
        }
    }
}
