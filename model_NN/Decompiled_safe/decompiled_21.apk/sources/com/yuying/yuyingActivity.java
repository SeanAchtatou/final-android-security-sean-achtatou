package com.yuying;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.adwo.adsdk.AdwoAdView;
import com.umeng.analytics.MobclickAgent;
import com.umeng.fb.UMFeedbackService;
import com.umeng.update.UmengUpdateAgent;
import igudi.com.ergushi.AppConnect;

public class yuyingActivity extends Activity {
    static AdwoAdView adView = null;
    public static RelativeLayout adwolayout;
    String Adwo_PID = "58c181a1f0c5414a8637eb1e91bc4a42";
    /* access modifiers changed from: private */
    public Handler handler;
    RelativeLayout.LayoutParams params = null;
    /* access modifiers changed from: private */
    public ProgressDialog pd;
    private WebView wv;

    public void onCreate(Bundle savedInstanceState) {
        MobclickAgent.updateOnlineConfig(this);
        UmengUpdateAgent.update(this);
        adView = new AdwoAdView(this, this.Adwo_PID, false, 30);
        this.params = new RelativeLayout.LayoutParams(-2, -2);
        AppConnect.getInstance("73fc2140e1d51a646a7ceeb10c0c30bd", "hiapk", this);
        AppConnect.getInstance(this).setAdViewClassName("igudi.com.ergushi.OffersWebView");
        AppConnect.getInstance(this).initPopAd(this);
        super.onCreate(savedInstanceState);
        MobclickAgent.onError(this);
        setContentView((int) R.layout.main);
        init();
        loadurl(this.wv, "file:///android_asset/html/default.html");
        if (MobclickAgent.getConfigParams(this, "hi11").equals("on")) {
            AppConnect.getInstance(this).showPopAd(this);
            adwolayout = (RelativeLayout) findViewById(R.id.adView);
            adwolayout.addView(adView, this.params);
        }
    }

    private void init() {
        this.pd = new ProgressDialog(this);
        this.pd.setProgressStyle(0);
        this.pd.setMessage("数据载入中，请稍候！");
        try {
            getWindow().addFlags(WindowManager.LayoutParams.class.getField("FLAG_NEEDS_MENU_KEY").getInt(null));
        } catch (IllegalAccessException | NoSuchFieldException e) {
        }
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                if (!Thread.currentThread().isInterrupted()) {
                    switch (msg.what) {
                        case 0:
                            yuyingActivity.this.pd.show();
                            break;
                        case 1:
                            yuyingActivity.this.pd.hide();
                            break;
                    }
                }
                super.handleMessage(msg);
            }
        };
        this.wv = (WebView) findViewById(R.id.wv);
        this.wv.addJavascriptInterface(new JavaScriptInterface(this), "JSInterface");
        this.wv.getSettings().setJavaScriptEnabled(true);
        this.wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.wv.getSettings().setPluginState(WebSettings.PluginState.ON);
        this.wv.getSettings().setPluginsEnabled(true);
        this.wv.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        this.wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.wv.setScrollBarStyle(0);
        this.wv.getSettings().setDatabaseEnabled(true);
        this.wv.getSettings().setDomStorageEnabled(true);
        this.wv.getSettings().setSaveFormData(true);
        this.wv.getSettings().setDatabasePath(getApplicationContext().getDir("database", 0).getPath());
        this.wv.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                yuyingActivity.this.loadurl(view, url);
                return true;
            }
        });
        this.wv.setWebChromeClient(new WebChromeClient() {
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                new AlertDialog.Builder(yuyingActivity.this).setTitle("恭喜你").setMessage(message).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        result.confirm();
                    }
                }).setCancelable(false).create().show();
                return true;
            }

            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    yuyingActivity.this.handler.sendEmptyMessage(1);
                }
                super.onProgressChanged(view, progress);
            }

            public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                quotaUpdater.updateQuota(2 * estimatedSize);
            }
        });
        this.wv.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                yuyingActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
            }
        });
    }

    public class JavaScriptInterface {
        private Activity activity;

        public JavaScriptInterface(Activity activiy) {
            this.activity = activiy;
        }

        public void startVideo(String videoAddress) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(videoAddress), "video/mp4");
            this.activity.startActivity(intent);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && this.wv.canGoBack()) {
            this.wv.goBack();
            return true;
        } else if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        } else {
            ConfirmExit();
            return true;
        }
    }

    public void ConfirmExit() {
        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle("退出软件");
        ad.setMessage("亲～这就要走了吗～");
        ad.setPositiveButton("拜拜了", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                yuyingActivity.this.finish();
            }
        });
        ad.setNegativeButton("再看看", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
            }
        });
        ad.show();
    }

    public void loadurl(final WebView view, final String url) {
        new Thread() {
            public void run() {
                yuyingActivity.this.handler.sendEmptyMessage(0);
                view.loadUrl(url);
            }
        }.start();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 1, "分享").setIcon(17301586);
        menu.add(0, 1, 2, "刷新").setIcon(17301581);
        menu.add(0, 2, 3, "反馈").setIcon(17301584);
        menu.add(0, 3, 4, "退出").setIcon(17301564);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String isClose = MobclickAgent.getConfigParams(this, "hi11");
        switch (item.getItemId()) {
            case 0:
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.SUBJECT", "分享");
                intent.putExtra("android.intent.extra.TEXT", "非常实用的孕婴饮食宝典，推荐给你 http://www.wandoujia.com/apps/com.yuying");
                startActivity(Intent.createChooser(intent, getTitle()));
                break;
            case 1:
                this.wv.reload();
                break;
            case 2:
                UMFeedbackService.openUmengFeedbackSDK(this);
                break;
            case 3:
                ConfirmExit();
                break;
        }
        if (!isClose.equals("on")) {
            return true;
        }
        AppConnect.getInstance(this).showPopAd(this);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        this.wv.pauseTimers();
        if (isFinishing()) {
            this.wv.loadUrl("about:blank");
            setContentView(new FrameLayout(this));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.wv.resumeTimers();
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AppConnect.getInstance(this).close();
    }
}
