package com.yuying;

public final class R {

    public static final class array {
        public static final int UMageList = 2131034112;
        public static final int UMgenderList = 2131034113;
    }

    public static final class attr {
        public static final int isTesting = 2130771968;
    }

    public static final class drawable {
        public static final int ic_launcher = 2130837504;
        public static final int icon = 2130837505;
        public static final int smsinfo = 2130837506;
        public static final int start = 2130837507;
        public static final int umeng_fb_bar_bg = 2130837508;
        public static final int umeng_fb_blank_selector = 2130837509;
        public static final int umeng_fb_bottom_banner = 2130837510;
        public static final int umeng_fb_dev_bubble = 2130837511;
        public static final int umeng_fb_gradient_green = 2130837512;
        public static final int umeng_fb_gradient_orange = 2130837513;
        public static final int umeng_fb_gray_frame = 2130837514;
        public static final int umeng_fb_list_item = 2130837515;
        public static final int umeng_fb_list_item_pressed = 2130837516;
        public static final int umeng_fb_list_item_selector = 2130837517;
        public static final int umeng_fb_point_new = 2130837518;
        public static final int umeng_fb_point_normal = 2130837519;
        public static final int umeng_fb_see_list_normal = 2130837520;
        public static final int umeng_fb_see_list_pressed = 2130837521;
        public static final int umeng_fb_see_list_selector = 2130837522;
        public static final int umeng_fb_statusbar_icon = 2130837523;
        public static final int umeng_fb_submit_selector = 2130837524;
        public static final int umeng_fb_top_banner = 2130837525;
        public static final int umeng_fb_user_bubble = 2130837526;
        public static final int umeng_fb_write_normal = 2130837527;
        public static final int umeng_fb_write_pressed = 2130837528;
        public static final int umeng_fb_write_selector = 2130837529;
        public static final int wapicon = 2130837530;
    }

    public static final class id {
        public static final int adView = 2131099649;
        public static final int umeng_analytics_app = 2131099650;
        public static final int umeng_analytics_appIcon = 2131099651;
        public static final int umeng_analytics_description = 2131099655;
        public static final int umeng_analytics_notification = 2131099653;
        public static final int umeng_analytics_progress_bar = 2131099656;
        public static final int umeng_analytics_progress_text = 2131099652;
        public static final int umeng_analytics_title = 2131099654;
        public static final int umeng_common_app = 2131099657;
        public static final int umeng_common_appIcon = 2131099658;
        public static final int umeng_common_description = 2131099662;
        public static final int umeng_common_notification = 2131099660;
        public static final int umeng_common_progress_bar = 2131099663;
        public static final int umeng_common_progress_text = 2131099659;
        public static final int umeng_common_title = 2131099661;
        public static final int umeng_fb_age_spinner = 2131099694;
        public static final int umeng_fb_app = 2131099679;
        public static final int umeng_fb_appIcon = 2131099680;
        public static final int umeng_fb_atomLinearLayout = 2131099664;
        public static final int umeng_fb_atom_left_margin = 2131099665;
        public static final int umeng_fb_atom_right_margin = 2131099669;
        public static final int umeng_fb_atomtxt = 2131099667;
        public static final int umeng_fb_bottom_sub = 2131099671;
        public static final int umeng_fb_btnSendFb = 2131099673;
        public static final int umeng_fb_bubble = 2131099666;
        public static final int umeng_fb_content = 2131099693;
        public static final int umeng_fb_conversation_title = 2131099670;
        public static final int umeng_fb_description = 2131099684;
        public static final int umeng_fb_dev_reply = 2131099677;
        public static final int umeng_fb_editTxtFb = 2131099672;
        public static final int umeng_fb_exitBtn = 2131099688;
        public static final int umeng_fb_feedbackpreview = 2131099676;
        public static final int umeng_fb_gender_spinner = 2131099695;
        public static final int umeng_fb_goback_btn = 2131099691;
        public static final int umeng_fb_imgBtn_submitFb = 2131099674;
        public static final int umeng_fb_new_dev_reply_box = 2131099687;
        public static final int umeng_fb_new_reply_alert_title = 2131099686;
        public static final int umeng_fb_new_reply_notifier = 2131099675;
        public static final int umeng_fb_notification = 2131099682;
        public static final int umeng_fb_progress_bar = 2131099685;
        public static final int umeng_fb_progress_text = 2131099681;
        public static final int umeng_fb_rootId = 2131099690;
        public static final int umeng_fb_see_detail_btn = 2131099689;
        public static final int umeng_fb_see_list_btn = 2131099692;
        public static final int umeng_fb_stateOrTime = 2131099668;
        public static final int umeng_fb_state_or_date = 2131099678;
        public static final int umeng_fb_submit = 2131099696;
        public static final int umeng_fb_title = 2131099683;
        public static final int wv = 2131099648;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int umeng_analytics_download_notification = 2130903041;
        public static final int umeng_common_download_notification = 2130903042;
        public static final int umeng_fb_atom = 2130903043;
        public static final int umeng_fb_conversation = 2130903044;
        public static final int umeng_fb_conversation_item = 2130903045;
        public static final int umeng_fb_conversations = 2130903046;
        public static final int umeng_fb_conversations_item = 2130903047;
        public static final int umeng_fb_download_notification = 2130903048;
        public static final int umeng_fb_list_item = 2130903049;
        public static final int umeng_fb_new_reply_alert_dialog = 2130903050;
        public static final int umeng_fb_send_feedback = 2130903051;
    }

    public static final class string {
        public static final int UMAppUpdate = 2130968611;
        public static final int UMBreak_Network = 2130968606;
        public static final int UMContentTooLong = 2130968579;
        public static final int UMDeleteFeedback = 2130968600;
        public static final int UMDeleteMsg = 2130968602;
        public static final int UMDeleteThread = 2130968598;
        public static final int UMDialog_InstallAPK = 2130968614;
        public static final int UMEmptyFbNotAllowed = 2130968578;
        public static final int UMFbList_ListItem_State_Fail = 2130968582;
        public static final int UMFbList_ListItem_State_ReSend = 2130968580;
        public static final int UMFbList_ListItem_State_Resending = 2130968586;
        public static final int UMFbList_ListItem_State_Sending = 2130968581;
        public static final int UMFb_Atom_State_Fail = 2130968583;
        public static final int UMFb_Atom_State_Resend = 2130968585;
        public static final int UMFb_Atom_State_Sending = 2130968584;
        public static final int UMFeedbackContent = 2130968591;
        public static final int UMFeedbackConversationTitle = 2130968589;
        public static final int UMFeedbackGoBack = 2130968593;
        public static final int UMFeedbackGotIt = 2130968594;
        public static final int UMFeedbackListTitle = 2130968588;
        public static final int UMFeedbackSeeDetail = 2130968595;
        public static final int UMFeedbackSummit = 2130968592;
        public static final int UMFeedbackTitle = 2130968590;
        public static final int UMFeedbackUmengTitle = 2130968587;
        public static final int UMGprsCondition = 2130968609;
        public static final int UMNewReplyAlertTitle = 2130968597;
        public static final int UMNewReplyFlick = 2130968603;
        public static final int UMNewReplyHint = 2130968605;
        public static final int UMNewReplyTitle = 2130968604;
        public static final int UMNewVersion = 2130968608;
        public static final int UMNotNow = 2130968612;
        public static final int UMResendFeedback = 2130968601;
        public static final int UMToast_IsUpdating = 2130968613;
        public static final int UMUpdateNow = 2130968610;
        public static final int UMUpdateTitle = 2130968607;
        public static final int UMViewFeedback = 2130968599;
        public static final int UMViewThread = 2130968596;
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }

    public static final class styleable {
        public static final int[] com_adwo_adsdk_AdwoAdView = {R.attr.isTesting};
        public static final int com_adwo_adsdk_AdwoAdView_isTesting = 0;
    }
}
