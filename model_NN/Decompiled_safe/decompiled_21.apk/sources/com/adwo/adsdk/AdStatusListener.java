package com.adwo.adsdk;

public interface AdStatusListener {
    void onAdResumeRequesting();

    void onAdStopRequesting();
}
