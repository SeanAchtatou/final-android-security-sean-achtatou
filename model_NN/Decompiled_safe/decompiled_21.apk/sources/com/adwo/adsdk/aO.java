package com.adwo.adsdk;

import android.app.Dialog;
import android.media.MediaPlayer;

final class aO implements MediaPlayer.OnErrorListener {
    private final /* synthetic */ Dialog a;

    aO(aI aIVar, Dialog dialog) {
        this.a = dialog;
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        if (this.a == null) {
            return true;
        }
        this.a.dismiss();
        return true;
    }
}
