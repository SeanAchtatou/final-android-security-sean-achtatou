package com.adwo.adsdk;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;
import android.widget.EditText;

final class aG implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsPromptResult a;
    private final /* synthetic */ EditText b;

    aG(aC aCVar, JsPromptResult jsPromptResult, EditText editText) {
        this.a = jsPromptResult;
        this.b = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.confirm(this.b.getText().toString());
    }
}
