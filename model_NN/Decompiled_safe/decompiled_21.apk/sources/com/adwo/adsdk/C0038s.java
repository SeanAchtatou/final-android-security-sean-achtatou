package com.adwo.adsdk;

import android.media.MediaPlayer;
import android.net.Uri;
import java.io.IOException;

/* renamed from: com.adwo.adsdk.s  reason: case insensitive filesystem */
final class C0038s implements Runnable {
    private final /* synthetic */ String a;

    C0038s(C0036q qVar, String str) {
        this.a = str;
    }

    public final void run() {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(3);
            mediaPlayer.setOnCompletionListener(new C0039t(this));
            mediaPlayer.setDataSource(AdDisplayer.m, Uri.parse(this.a));
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        }
    }
}
