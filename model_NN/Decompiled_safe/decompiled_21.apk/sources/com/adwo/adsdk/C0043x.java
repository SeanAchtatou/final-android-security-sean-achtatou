package com.adwo.adsdk;

import android.app.Activity;

/* renamed from: com.adwo.adsdk.x  reason: case insensitive filesystem */
final class C0043x implements Runnable {
    final /* synthetic */ C0036q a;
    private final /* synthetic */ String b;

    C0043x(C0036q qVar, String str) {
        this.a = qVar;
        this.b = str;
    }

    public final void run() {
        U.a(this.b, (Activity) AdDisplayer.m);
        try {
            this.a.a.h.post(new C0044y(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
