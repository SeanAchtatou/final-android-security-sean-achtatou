package com.adwo.adsdk;

import android.os.Vibrator;

final class aJ implements Runnable {
    private final /* synthetic */ Vibrator a;

    aJ(aI aIVar, Vibrator vibrator) {
        this.a = vibrator;
    }

    public final void run() {
        if (this.a != null) {
            this.a.cancel();
        }
        C0045z.a = false;
    }
}
