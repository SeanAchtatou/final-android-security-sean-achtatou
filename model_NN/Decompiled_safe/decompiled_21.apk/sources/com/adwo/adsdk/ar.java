package com.adwo.adsdk;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.umeng.common.b.e;

final class ar {
    static Context j;
    protected static FullScreenAdListener o;
    private static ar s;
    private int A = 600;
    private int B = 601;
    private int C = 602;
    private int D = 603;
    private int E = 0;
    PopupWindow a = null;
    C0012aj b;
    WebView c;
    RelativeLayout d;
    ImageView e;
    ImageView f;
    ImageView g;
    RelativeLayout h;
    ProgressBar i;
    int k = 30;
    int l = 50;
    Handler m = new as(this);
    boolean n = false;
    int p = -1;
    int q = 1;
    short r = 0;
    private int t = 602;
    private RelativeLayout u;
    private ImageView v;
    private DisplayMetrics w = null;
    private int x = 1;
    private int y = 0;
    private int z = 0;

    protected static ar a(Context context) {
        j = context;
        if (s == null) {
            s = new ar(context);
        }
        return s;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        try {
            if (this.a != null) {
                this.a.dismiss();
            }
            b();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private ar(Context context) {
        this.w = context.getResources().getDisplayMetrics();
        try {
            Rect rect = new Rect();
            ((Activity) context).getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
            if (rect.top == 0) {
                this.k = 30;
            } else {
                this.k = rect.top;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        a();
        int i2 = this.w.widthPixels;
        int i3 = this.w.heightPixels;
        a();
        this.d = new RelativeLayout(j);
        this.d.setGravity(16);
        this.c = new WebView(j);
        this.c.setId(this.t);
        this.c.setWebViewClient(new aI(this));
        this.c.setWebChromeClient(new aC(this));
        this.c.setScrollBarStyle(0);
        this.c.setVerticalScrollBarEnabled(false);
        this.c.setHorizontalScrollBarEnabled(false);
        CookieManager.getInstance().setAcceptCookie(true);
        WebSettings settings = this.c.getSettings();
        settings.setDefaultTextEncodingName(e.f);
        settings.setGeolocationEnabled(true);
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setJavaScriptEnabled(true);
        settings.setPluginsEnabled(true);
        settings.setSaveFormData(true);
        settings.setLightTouchEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setCacheMode(-1);
        settings.setAppCacheMaxSize(8388608);
        if (K.a == null) {
            K.a = "/data/data/" + j.getPackageName() + "/cache";
        }
        settings.setAppCachePath(K.a);
        this.c.setOnTouchListener(new av(this));
        this.c.addJavascriptInterface(new C0020aw(this), "adwo");
        this.d.addView(this.c);
        this.h = new RelativeLayout(j);
        this.h.setBackgroundResource(17301658);
        this.g = new ImageButton(j);
        this.g.setBackgroundResource(17301527);
        this.g.setOnClickListener(new ax(this));
        this.e = new ImageButton(j);
        this.e.setVisibility(4);
        this.e.setBackgroundResource(17301538);
        this.e.setOnClickListener(new ay(this));
        this.f = new ImageButton(j);
        this.f.setVisibility(4);
        this.f.setBackgroundResource(17301541);
        this.f.setOnClickListener(new az(this));
        this.v = new ImageButton(j);
        this.v.setBackgroundResource(17301599);
        this.v.setOnClickListener(new aA(this));
        int i4 = i3 <= i2 ? i2 / 16 : i2 / 8;
        int i5 = (i4 / 3) + i4;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i4, i4);
        layoutParams.addRule(11);
        this.g.setId(this.A);
        layoutParams.setMargins(0, 0, 0, 0);
        this.h.addView(this.g, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(i4, i4);
        layoutParams2.addRule(0, this.A);
        this.v.setId(this.C);
        layoutParams2.setMargins(i5, 0, i5, 0);
        this.h.addView(this.v, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(i4, i4);
        layoutParams3.addRule(0, this.C);
        this.e.setId(this.D);
        layoutParams3.setMargins(i5, 0, 0, 0);
        this.h.addView(this.e, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(i4, i4);
        this.f.setId(this.B);
        layoutParams4.addRule(0, this.D);
        layoutParams4.setMargins(0, 0, 0, 0);
        this.h.addView(this.f, layoutParams4);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, i4);
        layoutParams5.addRule(12);
        layoutParams5.addRule(3, this.t);
        this.d.addView(this.h, layoutParams5);
        RotateAnimation rotateAnimation = new RotateAnimation(-90.0f, 0.0f, 1, 1.0f, 1, 0.0f);
        rotateAnimation.setDuration(500);
        this.h.setAnimation(rotateAnimation);
        a(i2, i3);
    }

    private void a(int i2, int i3) {
        this.a = new PopupWindow(j);
        this.a.setOutsideTouchable(false);
        this.a.setClippingEnabled(false);
        this.a.setTouchable(true);
        this.a.setFocusable(true);
        this.a.setAnimationStyle(16973910);
        this.a.setBackgroundDrawable(new ColorDrawable(-1));
        this.a.setWidth(i2);
        this.a.setHeight(i3);
        if (this.u != null) {
            this.u.removeView(this.d);
        }
        this.u = new RelativeLayout(j);
        this.u.addView(this.d);
        this.a.setContentView(this.u);
        this.a.setOnDismissListener(new au(this));
    }

    /* access modifiers changed from: protected */
    public final void a(View view, String str, int i2, int i3, short s2) {
        this.p = i2;
        this.q = i3;
        this.r = s2;
        this.c.clearHistory();
        this.c.loadUrl(str);
        int i4 = this.w.widthPixels;
        int i5 = this.w.heightPixels - this.k;
        int i6 = i5 <= i4 ? i4 / 16 : i4 / 8;
        this.c.setLayoutParams(new RelativeLayout.LayoutParams(i4, i5 - i6));
        this.c.requestLayout();
        int i7 = (i6 / 3) + i6;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i6, i6);
        layoutParams.addRule(11);
        layoutParams.setMargins(0, 0, 0, 0);
        this.g.setLayoutParams(layoutParams);
        this.g.requestLayout();
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(i6, i6);
        layoutParams2.addRule(0, this.A);
        layoutParams2.setMargins(i7, 0, i7, 0);
        this.v.setLayoutParams(layoutParams2);
        this.v.requestLayout();
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(i6, i6);
        layoutParams3.addRule(0, this.C);
        layoutParams3.setMargins(i7, 0, 0, 0);
        this.e.setLayoutParams(layoutParams3);
        this.e.requestLayout();
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(i6, i6);
        layoutParams4.addRule(0, this.D);
        layoutParams4.setMargins(0, 0, 0, 0);
        this.f.setLayoutParams(layoutParams4);
        this.f.requestLayout();
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, i6);
        layoutParams5.addRule(12);
        this.h.setLayoutParams(layoutParams5);
        this.h.requestLayout();
        int i8 = this.w.widthPixels;
        int i9 = this.w.heightPixels;
        Configuration configuration = j.getResources().getConfiguration();
        try {
            if (this.x != configuration.orientation) {
                this.x = configuration.orientation;
                if (this.x == 1) {
                    this.c.loadUrl("javascript:adwoFSAdOrientationChanged(0);");
                } else {
                    this.c.loadUrl("javascript:adwoFSAdOrientationChanged(1);");
                }
            }
            view.post(new aB(this, view, 0, 0, i8, i9));
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Adwo Ad window Exception...");
            e2.printStackTrace();
            a(view, 0, 0, i8, i9);
        }
        this.n = true;
    }

    private void a(View view, int i2, int i3, int i4, int i5) {
        try {
            a(this.w.widthPixels, this.w.heightPixels);
            view.post(new at(this, view, i2, i3, i4, i5));
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Adwo Ad window Exception...");
            e2.printStackTrace();
            this.E++;
            if (this.E < 3) {
                a(view, i2, i3, i4, i5);
            } else {
                this.E = 0;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.b != null) {
            this.b.b();
            this.b = null;
        }
    }
}
