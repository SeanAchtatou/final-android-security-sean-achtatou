package com.adwo.adsdk;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public final class E extends WebViewClient {
    /* access modifiers changed from: private */
    public /* synthetic */ C0045z a;

    protected E(C0045z zVar) {
        this.a = zVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adwo.adsdk.U.a(android.webkit.WebView, int):void
     arg types: [com.adwo.adsdk.z, int]
     candidates:
      com.adwo.adsdk.U.a(android.content.Context, java.lang.String):int
      com.adwo.adsdk.U.a(java.lang.String, java.lang.String):java.lang.String
      com.adwo.adsdk.U.a(int, byte):void
      com.adwo.adsdk.U.a(android.content.Context, int):void
      com.adwo.adsdk.U.a(android.view.View, int):void
      com.adwo.adsdk.U.a(com.adwo.adsdk.FullScreenAd, java.lang.String):void
      com.adwo.adsdk.U.a(java.lang.String, android.app.Activity):void
      com.adwo.adsdk.U.a(android.webkit.WebView, int):void */
    public final void onPageFinished(WebView webView, String str) {
        try {
            this.a.setVisibility(0);
            if (this.a.e != null && this.a.e.b() != null && this.a.e.b().equalsIgnoreCase(str)) {
                U.a((WebView) this.a, this.a.e.a);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v75, types: [int] */
    /* JADX WARN: Type inference failed for: r1v96, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v97 */
    /* JADX WARN: Type inference failed for: r1v115 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean shouldOverrideUrlLoading(android.webkit.WebView r11, java.lang.String r12) {
        /*
            r10 = this;
            r8 = 4652007308841189376(0x408f400000000000, double:1000.0)
            r6 = 2
            r1 = 0
            r7 = 1
            if (r12 == 0) goto L_0x0875
            java.lang.String r0 = "http"
            boolean r0 = r12.startsWith(r0)
            if (r0 != 0) goto L_0x001a
            java.lang.String r0 = "https"
            boolean r0 = r12.startsWith(r0)
            if (r0 == 0) goto L_0x00c0
        L_0x001a:
            java.lang.String r0 = ".mp3"
            boolean r0 = r12.endsWith(r0)
            if (r0 != 0) goto L_0x0042
            java.lang.String r0 = ".wav"
            boolean r0 = r12.endsWith(r0)
            if (r0 != 0) goto L_0x0042
            java.lang.String r0 = ".mp4"
            boolean r0 = r12.endsWith(r0)
            if (r0 != 0) goto L_0x0042
            java.lang.String r0 = ".3gp"
            boolean r0 = r12.endsWith(r0)
            if (r0 != 0) goto L_0x0042
            java.lang.String r0 = ".mpeg"
            boolean r0 = r12.endsWith(r0)
            if (r0 == 0) goto L_0x004c
        L_0x0042:
            com.adwo.adsdk.z r0 = r10.a
            android.content.Context r0 = r0.getContext()
            com.adwo.adsdk.U.b(r0, r12)
        L_0x004b:
            return r7
        L_0x004c:
            java.lang.String r0 = ".apk"
            boolean r0 = r12.endsWith(r0)
            if (r0 == 0) goto L_0x0086
            com.adwo.adsdk.z r0 = r10.a
            com.adwo.adsdk.I r0 = r0.e
            byte r0 = r0.m
            r0 = r0 & 240(0xf0, float:3.36E-43)
            int r0 = r0 >> 4
            byte r0 = (byte) r0
            if (r0 <= 0) goto L_0x0878
            r0 = r7
        L_0x0064:
            com.adwo.adsdk.z r1 = r10.a
            android.content.Context r1 = r1.getContext()
            com.adwo.adsdk.z r2 = r10.a
            com.adwo.adsdk.I r2 = r2.e
            boolean r2 = r2.h
            com.adwo.adsdk.z r2 = r10.a
            com.adwo.adsdk.I r2 = r2.e
            int r2 = r2.a
            com.adwo.adsdk.z r3 = r10.a
            com.adwo.adsdk.I r3 = r3.e
            short r3 = r3.l
            com.adwo.adsdk.U.a(r12, r1, r2, r0, r3)
            goto L_0x004b
        L_0x0086:
            java.lang.String r0 = ".jpeg"
            boolean r0 = r12.endsWith(r0)
            if (r0 != 0) goto L_0x00ae
            java.lang.String r0 = ".gif"
            boolean r0 = r12.endsWith(r0)
            if (r0 != 0) goto L_0x00ae
            java.lang.String r0 = ".png"
            boolean r0 = r12.endsWith(r0)
            if (r0 != 0) goto L_0x00ae
            java.lang.String r0 = ".bmp"
            boolean r0 = r12.endsWith(r0)
            if (r0 != 0) goto L_0x00ae
            java.lang.String r0 = ".jpg"
            boolean r0 = r12.endsWith(r0)
            if (r0 == 0) goto L_0x00ba
        L_0x00ae:
            com.adwo.adsdk.z r0 = r10.a
            android.content.Context r0 = r0.getContext()
            android.app.Activity r0 = (android.app.Activity) r0
            com.adwo.adsdk.U.a(r12, r0)
            goto L_0x004b
        L_0x00ba:
            com.adwo.adsdk.z r0 = r10.a
            r0.loadUrl(r12)
            goto L_0x004b
        L_0x00c0:
            java.lang.String r0 = "tel"
            boolean r0 = r12.startsWith(r0)
            if (r0 == 0) goto L_0x00d3
            com.adwo.adsdk.z r0 = r10.a
            android.content.Context r0 = r0.getContext()
            com.adwo.adsdk.U.f(r0, r12)
            goto L_0x004b
        L_0x00d3:
            java.lang.String r0 = "sms"
            boolean r0 = r12.startsWith(r0)
            if (r0 == 0) goto L_0x00e6
            com.adwo.adsdk.z r0 = r10.a
            android.content.Context r0 = r0.getContext()
            com.adwo.adsdk.U.c(r0, r12)
            goto L_0x004b
        L_0x00e6:
            java.lang.String r0 = "market"
            boolean r0 = r12.startsWith(r0)
            if (r0 == 0) goto L_0x00f9
            com.adwo.adsdk.z r0 = r10.a
            android.content.Context r0 = r0.getContext()
            com.adwo.adsdk.U.d(r0, r12)
            goto L_0x004b
        L_0x00f9:
            java.lang.String r0 = "mailto"
            boolean r0 = r12.startsWith(r0)
            if (r0 == 0) goto L_0x010c
            com.adwo.adsdk.z r0 = r10.a
            android.content.Context r0 = r0.getContext()
            com.adwo.adsdk.U.e(r0, r12)
            goto L_0x004b
        L_0x010c:
            java.lang.String r0 = "adwo://"
            boolean r0 = r12.startsWith(r0)
            if (r0 == 0) goto L_0x0875
            android.view.animation.ScaleAnimation r0 = com.adwo.adsdk.C0045z.n
            r2 = 500(0x1f4, double:2.47E-321)
            r0.setDuration(r2)
            com.adwo.adsdk.z r0 = r10.a
            android.view.animation.ScaleAnimation r2 = com.adwo.adsdk.C0045z.n
            r0.startAnimation(r2)
            r0 = 7
            java.lang.String r0 = r12.substring(r0)     // Catch:{ Exception -> 0x0160 }
            java.util.StringTokenizer r3 = new java.util.StringTokenizer     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = "&"
            r3.<init>(r0, r2)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r4 = "adwoOpenURL"
            boolean r4 = r4.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r4 == 0) goto L_0x02cd
            boolean r2 = com.adwo.adsdk.K.P     // Catch:{ Exception -> 0x0160 }
            if (r2 == 0) goto L_0x0166
            com.adwo.adsdk.z r2 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.content.Context r2 = r2.getContext()     // Catch:{ Exception -> 0x0160 }
            int r2 = com.adwo.adsdk.U.c(r2)     // Catch:{ Exception -> 0x0160 }
            if (r2 != 0) goto L_0x0166
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.content.Context r0 = r0.getContext()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "网络不给力..."
            r2 = 0
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)     // Catch:{ Exception -> 0x0160 }
            r0.show()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0160:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004b
        L_0x0166:
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r4 = "_"
            int r4 = r2.indexOf(r4)     // Catch:{ Exception -> 0x0160 }
            int r4 = r4 + 1
            java.lang.String r2 = r2.substring(r4)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x0160 }
            int r4 = r2.intValue()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r5 = "_"
            int r5 = r2.indexOf(r5)     // Catch:{ Exception -> 0x0160 }
            int r5 = r5 + 1
            java.lang.String r2 = r2.substring(r5)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x0160 }
            r2.intValue()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r5 = "_"
            int r5 = r2.indexOf(r5)     // Catch:{ Exception -> 0x0160 }
            int r5 = r5 + 1
            java.lang.String r2 = r2.substring(r5)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x0160 }
            r2.intValue()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r5 = "_"
            int r5 = r2.indexOf(r5)     // Catch:{ Exception -> 0x0160 }
            int r5 = r5 + 1
            java.lang.String r2 = r2.substring(r5)     // Catch:{ Exception -> 0x0160 }
        L_0x01bc:
            boolean r5 = r3.hasMoreTokens()     // Catch:{ Exception -> 0x0160 }
            if (r5 != 0) goto L_0x020e
            if (r4 != r7) goto L_0x022a
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x0160 }
            r1.<init>()     // Catch:{ Exception -> 0x0160 }
            android.content.ComponentName r3 = new android.content.ComponentName     // Catch:{ Exception -> 0x01f1 }
            java.lang.String r4 = "com.android.browser"
            java.lang.String r5 = "com.android.browser.BrowserActivity"
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x01f1 }
            r1.setComponent(r3)     // Catch:{ Exception -> 0x01f1 }
            java.lang.String r3 = "android.intent.action.VIEW"
            r1.setAction(r3)     // Catch:{ Exception -> 0x01f1 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ Exception -> 0x01f1 }
            r1.setData(r2)     // Catch:{ Exception -> 0x01f1 }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x01f1 }
            com.adwo.adsdk.z r2 = r10.a     // Catch:{ Exception -> 0x01f1 }
            android.content.Context r2 = r2.getContext()     // Catch:{ Exception -> 0x01f1 }
            r2.startActivity(r1)     // Catch:{ Exception -> 0x01f1 }
            goto L_0x004b
        L_0x01f1:
            r1 = move-exception
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0160 }
            java.lang.String r3 = "javascript:adwoURLOpenFailed(+"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0160 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = ");"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0160 }
            r1.loadUrl(r0)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x020e:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x0160 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = "&"
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r5 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0160 }
            goto L_0x01bc
        L_0x022a:
            if (r4 != 0) goto L_0x0290
            byte[] r0 = com.adwo.adsdk.C0024e.a(r4)     // Catch:{ Exception -> 0x0160 }
            r1 = 1
            byte r1 = r0[r1]     // Catch:{ Exception -> 0x0160 }
            if (r1 == 0) goto L_0x024a
            r1 = 2
            byte r1 = r0[r1]     // Catch:{ Exception -> 0x0160 }
            r3 = 3
            byte r0 = r0[r3]     // Catch:{ Exception -> 0x0160 }
            short r0 = com.adwo.adsdk.C0024e.a(r1, r0)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.os.Handler r1 = r1.p     // Catch:{ Exception -> 0x0160 }
            r3 = 4
            long r4 = (long) r0     // Catch:{ Exception -> 0x0160 }
            r1.sendEmptyMessageDelayed(r3, r4)     // Catch:{ Exception -> 0x0160 }
        L_0x024a:
            r0 = 0
            com.adwo.adsdk.ar.o = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.content.Context r0 = r0.getContext()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.ar r0 = com.adwo.adsdk.ar.a(r0)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r1 = r1.e     // Catch:{ Exception -> 0x0160 }
            int r3 = r1.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r1 = r1.e     // Catch:{ Exception -> 0x0160 }
            byte r4 = r1.m     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r1 = r1.e     // Catch:{ Exception -> 0x0160 }
            short r5 = r1.l     // Catch:{ Exception -> 0x0160 }
            r1 = r11
            r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0160 }
            r0 = 0
            com.adwo.adsdk.C0045z.b = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r0 = r0.e     // Catch:{ Exception -> 0x0160 }
            int r0 = r0.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.C0045z.c = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r0 = r0.e     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r0.k     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.C0045z.d = r0     // Catch:{ Exception -> 0x0160 }
            r0 = 1
            com.adwo.adsdk.C0045z.a = r0     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0290:
            if (r4 != r6) goto L_0x004b
            java.lang.String r0 = ".apk"
            boolean r0 = r2.endsWith(r0)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x004b
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r0 = r0.e     // Catch:{ Exception -> 0x0160 }
            byte r0 = r0.m     // Catch:{ Exception -> 0x0160 }
            r0 = r0 & 240(0xf0, float:3.36E-43)
            int r0 = r0 >> 4
            byte r0 = (byte) r0     // Catch:{ Exception -> 0x0160 }
            if (r0 <= 0) goto L_0x02aa
            r1 = r7
        L_0x02aa:
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.content.Context r0 = r0.getContext()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r3 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r3 = r3.e     // Catch:{ Exception -> 0x0160 }
            boolean r3 = r3.h     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r3 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r3 = r3.e     // Catch:{ Exception -> 0x0160 }
            int r3 = r3.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r4 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r4 = r4.e     // Catch:{ Exception -> 0x0160 }
            short r4 = r4.l     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.U.a(r2, r0, r3, r1, r4)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x02cd:
            java.lang.String r0 = "adwoBannerExpand"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0389
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "javascript:adwoDoExpand();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0160 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = "_"
            int r2 = r1.indexOf(r2)     // Catch:{ Exception -> 0x0160 }
            int r2 = r2 + 1
            java.lang.String r1 = r1.substring(r2)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r1 = java.lang.Integer.decode(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r3 = "_"
            int r3 = r2.indexOf(r3)     // Catch:{ Exception -> 0x0160 }
            int r3 = r3 + 1
            java.lang.String r2 = r2.substring(r3)     // Catch:{ Exception -> 0x0160 }
            java.lang.Double r2 = java.lang.Double.valueOf(r2)     // Catch:{ Exception -> 0x0160 }
            double r2 = r2.doubleValue()     // Catch:{ Exception -> 0x0160 }
            double r2 = r2 * r8
            int r2 = (int) r2     // Catch:{ Exception -> 0x0160 }
            android.view.animation.TranslateAnimation r3 = new android.view.animation.TranslateAnimation     // Catch:{ Exception -> 0x0160 }
            r4 = 1065353216(0x3f800000, float:1.0)
            r5 = 1036831949(0x3dcccccd, float:0.1)
            r6 = 1065353216(0x3f800000, float:1.0)
            r8 = 1036831949(0x3dcccccd, float:0.1)
            r3.<init>(r4, r5, r6, r8)     // Catch:{ Exception -> 0x0160 }
            long r4 = (long) r2     // Catch:{ Exception -> 0x0160 }
            r3.setDuration(r4)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r2 = r10.a     // Catch:{ Exception -> 0x0160 }
            r2.setAnimation(r3)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r2 = r10.a     // Catch:{ Exception -> 0x0160 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0160 }
            r2.o = r3     // Catch:{ Exception -> 0x0160 }
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0160 }
            double r3 = (double) r0     // Catch:{ Exception -> 0x0160 }
            double r5 = com.adwo.adsdk.K.K     // Catch:{ Exception -> 0x0160 }
            double r3 = r3 * r5
            int r0 = (int) r3     // Catch:{ Exception -> 0x0160 }
            double r3 = (double) r1     // Catch:{ Exception -> 0x0160 }
            double r5 = com.adwo.adsdk.K.K     // Catch:{ Exception -> 0x0160 }
            double r3 = r3 * r5
            int r1 = (int) r3     // Catch:{ Exception -> 0x0160 }
            r2.<init>(r0, r1)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            r0.setLayoutParams(r2)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            r0.requestLayout()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "javascript:adwoDoExpandComplete();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r0 = r0.e     // Catch:{ Exception -> 0x0160 }
            r1 = 1
            r0.j = r1     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0383 }
            android.view.ViewParent r0 = r0.getParent()     // Catch:{ Exception -> 0x0383 }
            com.adwo.adsdk.AdwoAdView r0 = (com.adwo.adsdk.AdwoAdView) r0     // Catch:{ Exception -> 0x0383 }
            com.adwo.adsdk.AdStatusListener r0 = r0.c()     // Catch:{ Exception -> 0x0383 }
            if (r0 == 0) goto L_0x004b
            r0.onAdStopRequesting()     // Catch:{ Exception -> 0x0383 }
            goto L_0x004b
        L_0x0383:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0389:
            java.lang.String r0 = "adwoExpandRestore"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x03bf
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "javascript:adwoDoBannerRestore();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            java.lang.Double r0 = java.lang.Double.valueOf(r0)     // Catch:{ Exception -> 0x0160 }
            double r0 = r0.doubleValue()     // Catch:{ Exception -> 0x0160 }
            double r0 = r0 * r8
            int r0 = (int) r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.os.Handler r1 = r1.p     // Catch:{ Exception -> 0x0160 }
            r2 = 3
            long r3 = (long) r0     // Catch:{ Exception -> 0x0160 }
            r1.sendEmptyMessageDelayed(r2, r3)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x03bf:
            java.lang.String r0 = "adwoVibrate"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0443
            boolean r0 = com.adwo.adsdk.C0045z.a     // Catch:{ Exception -> 0x0160 }
            if (r0 != 0) goto L_0x004b
            r0 = 1
            com.adwo.adsdk.C0045z.a = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r2 = r10.a     // Catch:{ Exception -> 0x041a }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x041a }
            android.content.Context r0 = r0.getContext()     // Catch:{ Exception -> 0x041a }
            java.lang.String r4 = "vibrator"
            java.lang.Object r0 = r0.getSystemService(r4)     // Catch:{ Exception -> 0x041a }
            android.os.Vibrator r0 = (android.os.Vibrator) r0     // Catch:{ Exception -> 0x041a }
            r2.g = r0     // Catch:{ Exception -> 0x041a }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x041a }
            java.lang.String r2 = "_"
            int r2 = r0.indexOf(r2)     // Catch:{ Exception -> 0x041a }
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x041a }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x041a }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x041a }
            if (r0 <= r7) goto L_0x042f
            int r2 = r0 * 2
            long[] r3 = new long[r2]     // Catch:{ Exception -> 0x041a }
            r2 = r1
        L_0x0400:
            if (r1 < r0) goto L_0x0420
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x041a }
            android.os.Vibrator r1 = r1.g     // Catch:{ Exception -> 0x041a }
            r1.vibrate(r3, r0)     // Catch:{ Exception -> 0x041a }
        L_0x040b:
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x041a }
            android.os.Handler r1 = r1.p     // Catch:{ Exception -> 0x041a }
            r2 = 1
            int r0 = r0 * 1000
            long r3 = (long) r0     // Catch:{ Exception -> 0x041a }
            r1.sendEmptyMessageDelayed(r2, r3)     // Catch:{ Exception -> 0x041a }
            goto L_0x004b
        L_0x041a:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0420:
            r4 = 100
            r3[r2] = r4     // Catch:{ Exception -> 0x041a }
            int r2 = r2 + 1
            r4 = 500(0x1f4, double:2.47E-321)
            r3[r2] = r4     // Catch:{ Exception -> 0x041a }
            int r2 = r2 + 1
            int r1 = r1 + 1
            goto L_0x0400
        L_0x042f:
            r0 = 2
            long[] r0 = new long[r0]     // Catch:{ Exception -> 0x041a }
            r1 = 1
            r2 = 1000(0x3e8, double:4.94E-321)
            r0[r1] = r2     // Catch:{ Exception -> 0x041a }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x041a }
            android.os.Vibrator r1 = r1.g     // Catch:{ Exception -> 0x041a }
            r2 = 1
            r1.vibrate(r0, r2)     // Catch:{ Exception -> 0x041a }
            r0 = r7
            goto L_0x040b
        L_0x0443:
            java.lang.String r0 = "adwoPlayAudio"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x04a2
            boolean r0 = com.adwo.adsdk.C0045z.a     // Catch:{ Exception -> 0x0160 }
            if (r0 != 0) goto L_0x004b
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = "_"
            int r2 = r0.indexOf(r2)     // Catch:{ Exception -> 0x0160 }
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0160 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0160 }
            r1.k = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            int r0 = r0.k     // Catch:{ Exception -> 0x0160 }
            if (r0 > 0) goto L_0x047a
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            r1 = 1
            r0.k = r1     // Catch:{ Exception -> 0x0160 }
        L_0x047a:
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.os.Handler r1 = r1.p     // Catch:{ Exception -> 0x0160 }
            r2 = 2
            android.os.Message r1 = r1.obtainMessage(r2)     // Catch:{ Exception -> 0x0160 }
            r1.obj = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.os.Handler r0 = r0.p     // Catch:{ Exception -> 0x0160 }
            r0.dispatchMessage(r1)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x04a2:
            java.lang.String r0 = "adwoPlayVideo"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x05a1
            boolean r0 = com.adwo.adsdk.C0045z.a     // Catch:{ Exception -> 0x0160 }
            if (r0 != 0) goto L_0x004b
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.C0045z.g(r0)     // Catch:{ Exception -> 0x0160 }
            r0 = 1
            com.adwo.adsdk.C0045z.a = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.widget.VideoView r1 = new android.widget.VideoView     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r2 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.content.Context r2 = r2.getContext()     // Catch:{ Exception -> 0x0160 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0160 }
            r0.h = r1     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.widget.VideoView r0 = r0.h     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.F r1 = new com.adwo.adsdk.F     // Catch:{ Exception -> 0x0160 }
            r1.<init>(r10)     // Catch:{ Exception -> 0x0160 }
            r0.setOnCompletionListener(r1)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.widget.VideoView r0 = r0.h     // Catch:{ Exception -> 0x0160 }
            android.widget.MediaController r1 = new android.widget.MediaController     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r2 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.content.Context r2 = r2.getContext()     // Catch:{ Exception -> 0x0160 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0160 }
            r0.setMediaController(r1)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0160 }
            r0.intValue()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0160 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = "_"
            int r2 = r1.indexOf(r2)     // Catch:{ Exception -> 0x0160 }
            int r2 = r2 + 1
            java.lang.String r1 = r1.substring(r2)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r1 = java.lang.Integer.decode(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r4 = "_"
            int r4 = r2.indexOf(r4)     // Catch:{ Exception -> 0x0160 }
            int r4 = r4 + 1
            java.lang.String r2 = r2.substring(r4)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x0160 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r3 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r4 = "_"
            int r4 = r3.indexOf(r4)     // Catch:{ Exception -> 0x0160 }
            int r4 = r4 + 1
            java.lang.String r3 = r3.substring(r4)     // Catch:{ Exception -> 0x0160 }
            if (r3 == 0) goto L_0x004b
            com.adwo.adsdk.z r4 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.widget.VideoView r4 = r4.h     // Catch:{ Exception -> 0x0160 }
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x0160 }
            r4.setVideoURI(r3)     // Catch:{ Exception -> 0x0160 }
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0160 }
            double r4 = (double) r1     // Catch:{ Exception -> 0x0160 }
            double r8 = com.adwo.adsdk.K.K     // Catch:{ Exception -> 0x0160 }
            double r4 = r4 * r8
            int r1 = (int) r4     // Catch:{ Exception -> 0x0160 }
            double r4 = (double) r2     // Catch:{ Exception -> 0x0160 }
            double r8 = com.adwo.adsdk.K.K     // Catch:{ Exception -> 0x0160 }
            double r4 = r4 * r8
            int r2 = (int) r4     // Catch:{ Exception -> 0x0160 }
            r3.<init>(r1, r2)     // Catch:{ Exception -> 0x0160 }
            double r0 = (double) r0     // Catch:{ Exception -> 0x0160 }
            double r4 = com.adwo.adsdk.K.K     // Catch:{ Exception -> 0x0160 }
            double r0 = r0 * r4
            int r0 = (int) r0     // Catch:{ Exception -> 0x0160 }
            r3.topMargin = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.widget.VideoView r0 = r0.h     // Catch:{ Exception -> 0x0160 }
            r0.setLayoutParams(r3)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.view.ViewParent r0 = r0.getParent()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.AdwoAdView r0 = (com.adwo.adsdk.AdwoAdView) r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.widget.VideoView r1 = r1.h     // Catch:{ Exception -> 0x0160 }
            r0.addView(r1)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.widget.VideoView r0 = r0.h     // Catch:{ Exception -> 0x0160 }
            r0.start()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x05a1:
            java.lang.String r0 = "adwoCloseVideo"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x05b5
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.os.Handler r0 = r0.p     // Catch:{ Exception -> 0x0160 }
            r1 = 5
            r0.sendEmptyMessage(r1)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x05b5:
            java.lang.String r0 = "adwoFetchLocation"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x05ee
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            double r0 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.K.I = r0     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            double r0 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.K.J = r0     // Catch:{ Exception -> 0x0160 }
            r0 = 1
            com.adwo.adsdk.K.E = r0     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x05ee:
            java.lang.String r0 = "adwoPageDidLoad"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x05fe
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.C0045z.b = r0     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x05fe:
            java.lang.String r0 = "adwoAdClickCount"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x064b
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r0 = r0.e     // Catch:{ Exception -> 0x0160 }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.J r2 = new com.adwo.adsdk.J     // Catch:{ Exception -> 0x0160 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0160 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0160 }
            r1.start()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0645 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0645 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0645 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0645 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0645 }
            java.lang.String r3 = "javascript:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0645 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0645 }
            java.lang.String r2 = "();"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0645 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0645 }
            r1.loadUrl(r0)     // Catch:{ Exception -> 0x0645 }
            goto L_0x004b
        L_0x0645:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x064b:
            java.lang.String r0 = "adwoAddToAddressBook"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x065c
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "javascript:adwoDoGetAddressBookInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x065c:
            java.lang.String r0 = "adwoAddToCalendar"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x066d
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "javascript:adwoDoGetCalendarEventInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x066d:
            java.lang.String r0 = "adwoAddToReminder"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x067e
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "javascript:adwoDoGetReminderInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x067e:
            java.lang.String r0 = "adwoInitEssentialParams"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x06c1
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r1 = r1.e     // Catch:{ Exception -> 0x0160 }
            int r1 = r1.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.U.a(r11, r1)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            if (r1 == 0) goto L_0x004b
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0160 }
            java.lang.String r3 = "javascript:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x0160 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = "+();"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0160 }
            r1.loadUrl(r0)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x06c1:
            java.lang.String r0 = "adwoStartAudioRecorder"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x06ff
            com.adwo.adsdk.z r8 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.aj r0 = new com.adwo.adsdk.aj     // Catch:{ Exception -> 0x0160 }
            r1 = 4648488871632306176(0x4082c00000000000, double:600.0)
            r3 = 0
            r4 = 0
            com.adwo.adsdk.z r5 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r5 = r5.e     // Catch:{ Exception -> 0x0160 }
            int r6 = r5.a     // Catch:{ Exception -> 0x0160 }
            r5 = r11
            r0.<init>(r1, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0160 }
            r8.j = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.aj r0 = r0.j     // Catch:{ Exception -> 0x0160 }
            r0.a()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.os.Handler r0 = r0.p     // Catch:{ Exception -> 0x0160 }
            r1 = 6
            com.adwo.adsdk.z r2 = r10.a     // Catch:{ Exception -> 0x0160 }
            int r2 = r2.m     // Catch:{ Exception -> 0x0160 }
            long r2 = (long) r2     // Catch:{ Exception -> 0x0160 }
            r0.sendEmptyMessageDelayed(r1, r2)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x06ff:
            java.lang.String r0 = "adwoCloseAudioRecorder"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x070e
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            r0.c()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x070e:
            java.lang.String r0 = "adwoTakePhoto"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0748
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.content.Context r0 = r0.getContext()     // Catch:{ Exception -> 0x0160 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "android.hardware.camera"
            boolean r0 = r0.hasSystemFeature(r1)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0736
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.C0045z.a(r0, r3)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "javascript:adwoFetchCameraAssociatedImageInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0736:
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.content.Context r0 = r0.getContext()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "设备没有摄像头调用！"
            r2 = 0
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)     // Catch:{ Exception -> 0x0160 }
            r0.show()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0748:
            java.lang.String r0 = "adwoCloseCamera"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0769
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.T r0 = r0.i     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x004b
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.T r0 = r0.i     // Catch:{ Exception -> 0x0160 }
            r0.dismiss()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            r1 = 0
            r0.i = null     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0769:
            java.lang.String r0 = "adwoShareToWeibo"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0778
            java.lang.String r0 = "javascript:adwoSinaWeiboInfo();"
            r11.loadUrl(r0)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0778:
            java.lang.String r0 = "adwoSaveImg2Gallery"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x079f
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.G r2 = new com.adwo.adsdk.G     // Catch:{ Exception -> 0x0160 }
            r2.<init>(r10, r0)     // Catch:{ Exception -> 0x0160 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0160 }
            r1.start()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x079f:
            java.lang.String r0 = "adwoVoiceRecord"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0806
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            double r1 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r3 = "_"
            int r3 = r0.indexOf(r3)     // Catch:{ Exception -> 0x0160 }
            int r3 = r3 + 1
            java.lang.String r3 = r0.substring(r3)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r8 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.aj r0 = new com.adwo.adsdk.aj     // Catch:{ Exception -> 0x0160 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0160 }
            java.lang.String r5 = com.adwo.adsdk.K.N     // Catch:{ Exception -> 0x0160 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0160 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0160 }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0160 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r5 = "adwoRecord.mp4"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0160 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r5 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.I r5 = r5.e     // Catch:{ Exception -> 0x0160 }
            int r6 = r5.a     // Catch:{ Exception -> 0x0160 }
            r5 = r11
            r0.<init>(r1, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0160 }
            r8.j = r0     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.aj r0 = r0.j     // Catch:{ Exception -> 0x0160 }
            r0.a()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0806:
            java.lang.String r0 = "adwoVoiceRecordStop"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0840
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0160 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0160 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0160 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0839
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.aj r0 = r0.j     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0839
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.aj r0 = r0.j     // Catch:{ Exception -> 0x0160 }
            r1 = 0
            r0.a = r1     // Catch:{ Exception -> 0x0160 }
        L_0x0839:
            com.adwo.adsdk.z r0 = r10.a     // Catch:{ Exception -> 0x0160 }
            r0.c()     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0840:
            java.lang.String r0 = "adwoBrowserDisableScroll"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x0852
            r0 = 0
            r11.setVerticalScrollBarEnabled(r0)     // Catch:{ Exception -> 0x0160 }
            r0 = 0
            r11.setHorizontalScrollBarEnabled(r0)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0852:
            java.lang.String r0 = "adwoIntent"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x0160 }
            if (r0 == 0) goto L_0x004b
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.z r1 = r10.a     // Catch:{ Exception -> 0x0160 }
            android.content.Context r1 = r1.getContext()     // Catch:{ Exception -> 0x0160 }
            java.lang.String r2 = "_"
            int r2 = r0.indexOf(r2)     // Catch:{ Exception -> 0x0160 }
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x0160 }
            com.adwo.adsdk.U.g(r1, r0)     // Catch:{ Exception -> 0x0160 }
            goto L_0x004b
        L_0x0875:
            r7 = r1
            goto L_0x004b
        L_0x0878:
            r0 = r1
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.E.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
    }
}
