package com.adwo.adsdk;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;

/* renamed from: com.adwo.adsdk.p  reason: case insensitive filesystem */
final class C0035p implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsPromptResult a;

    C0035p(C0030k kVar, JsPromptResult jsPromptResult) {
        this.a = jsPromptResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.cancel();
    }
}
