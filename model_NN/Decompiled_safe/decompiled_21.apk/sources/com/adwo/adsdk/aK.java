package com.adwo.adsdk;

import android.media.MediaPlayer;
import android.net.Uri;

final class aK implements Runnable {
    private final /* synthetic */ String a;

    aK(aI aIVar, String str) {
        this.a = str;
    }

    public final void run() {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(3);
            mediaPlayer.setOnCompletionListener(new aL(this));
            mediaPlayer.setDataSource(ar.j, Uri.parse(this.a));
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
