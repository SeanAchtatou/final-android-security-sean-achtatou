package com.adwo.adsdk;

import android.content.Context;
import android.content.DialogInterface;

final class Y implements DialogInterface.OnClickListener {
    private final /* synthetic */ String a;
    private final /* synthetic */ Context b;

    Y(String str, Context context) {
        this.a = str;
        this.b = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        new Thread(new Z(this, this.a, this.b)).start();
    }
}
