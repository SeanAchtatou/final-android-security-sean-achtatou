package com.adwo.adsdk;

import android.content.Context;
import android.os.Handler;
import android.os.Vibrator;
import android.view.animation.ScaleAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.VideoView;
import java.util.StringTokenizer;

/* renamed from: com.adwo.adsdk.z  reason: case insensitive filesystem */
final class C0045z extends WebView {
    protected static boolean a = false;
    protected static long b = 0;
    protected static int c = -1;
    protected static String d = null;
    /* access modifiers changed from: private */
    public static ScaleAnimation n = new ScaleAnimation(1.0f, 1.5f, 1.0f, 1.5f, 1, 0.5f, 1, 0.5f);
    /* access modifiers changed from: private */
    public I e;
    private E f;
    /* access modifiers changed from: private */
    public Vibrator g;
    /* access modifiers changed from: private */
    public VideoView h;
    /* access modifiers changed from: private */
    public T i;
    /* access modifiers changed from: private */
    public C0012aj j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public int l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public long o;
    /* access modifiers changed from: private */
    public Handler p;

    static /* synthetic */ void a(C0045z zVar, StringTokenizer stringTokenizer) {
    }

    /* access modifiers changed from: protected */
    public final I a() {
        return this.e;
    }

    protected C0045z(I i2, Context context) {
        super(context);
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = 1;
        this.l = 0;
        this.m = 50;
        this.o = 0;
        this.p = new A(this);
        this.f = new E(this);
        this.e = i2;
        setFocusableInTouchMode(true);
        getSettings().setJavaScriptEnabled(true);
        setWebViewClient(this.f);
        getSettings().setLoadsImagesAutomatically(true);
        getSettings().setPluginsEnabled(true);
        getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        getSettings().setSaveFormData(false);
        getSettings().setSavePassword(false);
        getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        getSettings().setLightTouchEnabled(true);
        getSettings().setAppCacheEnabled(true);
        getSettings().setDatabaseEnabled(true);
        getSettings().setDomStorageEnabled(true);
        getSettings().setCacheMode(-1);
        getSettings().setAppCacheMaxSize(8388608);
        if (K.a == null) {
            K.a = "/data/data/" + getContext().getPackageName() + "/cache";
        }
        getSettings().setAppCachePath(K.a);
        getSettings().setBuiltInZoomControls(false);
        getSettings().setSupportZoom(false);
        getSettings().setGeolocationEnabled(true);
        requestFocusFromTouch();
        setAnimationCacheEnabled(true);
        setScrollBarStyle(0);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        setBackgroundColor(0);
        setInitialScale((int) (((((double) K.z) * K.K) / 320.0d) * 100.0d));
        addJavascriptInterface(new D(this), "adwo");
        if (this.e != null) {
            setFocusable(true);
            setClickable(true);
            setVisibility(8);
            try {
                String b2 = this.e.b();
                if (b2 != null) {
                    loadUrl(b2);
                } else {
                    setVisibility(8);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    static /* synthetic */ void g(C0045z zVar) {
        if (zVar.h != null) {
            zVar.h.stopPlayback();
            try {
                ((AdwoAdView) zVar.getParent()).removeView(zVar.h);
            } catch (Exception e2) {
            }
            zVar.h = null;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.j != null) {
            this.j.b();
            this.j = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            ar.a(getContext()).a();
            this.p.sendEmptyMessage(1);
            this.p.sendEmptyMessage(5);
            this.p.removeMessages(2);
            this.p = null;
            c();
            if (this.i != null) {
                this.i.dismiss();
                this.i = null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
