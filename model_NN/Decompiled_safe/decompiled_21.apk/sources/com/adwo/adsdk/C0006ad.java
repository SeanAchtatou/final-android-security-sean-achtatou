package com.adwo.adsdk;

import android.content.Context;
import android.widget.Toast;

/* renamed from: com.adwo.adsdk.ad  reason: case insensitive filesystem */
final class C0006ad implements Runnable {
    private final /* synthetic */ Context a;
    private final /* synthetic */ String b;

    C0006ad(C0004ab abVar, Context context, String str) {
        this.a = context;
        this.b = str;
    }

    public final void run() {
        Toast.makeText(this.a, String.valueOf(this.b) + "\n被下载到:" + K.N + " 目录下。", 1).show();
    }
}
