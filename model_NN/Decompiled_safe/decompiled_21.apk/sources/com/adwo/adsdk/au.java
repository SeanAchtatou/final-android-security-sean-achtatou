package com.adwo.adsdk;

import android.widget.PopupWindow;

final class au implements PopupWindow.OnDismissListener {
    private /* synthetic */ ar a;

    au(ar arVar) {
        this.a = arVar;
    }

    public final void onDismiss() {
        this.a.c.loadData("<html><body></body></html>", "text/html", null);
        this.a.c.clearHistory();
        this.a.c.clearView();
        if (ar.o != null) {
            ar.o.onAdDismiss();
            ar.o = null;
        }
        C0045z.a = false;
        if (this.a.n) {
            U.a(String.valueOf(C0045z.c) + "=" + C0045z.d, new StringBuilder(String.valueOf(System.currentTimeMillis() - C0045z.b)).toString(), ar.j);
            U.a(C0045z.d, String.valueOf(K.I) + "=" + K.J + "=" + C0045z.c, ar.j);
        }
        this.a.m.removeMessages(6);
    }
}
