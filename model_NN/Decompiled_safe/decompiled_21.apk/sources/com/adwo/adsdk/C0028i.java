package com.adwo.adsdk;

import android.view.View;

/* renamed from: com.adwo.adsdk.i  reason: case insensitive filesystem */
final class C0028i implements Runnable {
    private /* synthetic */ AdDisplayer a;
    private final /* synthetic */ View b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;
    private final /* synthetic */ int e;
    private final /* synthetic */ int f;

    C0028i(AdDisplayer adDisplayer, View view, int i, int i2, int i3, int i4) {
        this.a = adDisplayer;
        this.b = view;
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.update(int, int, int, int, boolean):void}
     arg types: [int, int, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.update(android.view.View, int, int, int, int):void}
      ClspMth{android.widget.PopupWindow.update(int, int, int, int, boolean):void} */
    public final void run() {
        this.a.e.showAtLocation(this.b, 0, this.c, this.d);
        this.a.e.update(this.c, this.d, this.e, this.f, true);
    }
}
