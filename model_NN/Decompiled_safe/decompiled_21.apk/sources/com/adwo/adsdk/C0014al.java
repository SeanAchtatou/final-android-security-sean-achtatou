package com.adwo.adsdk;

import android.media.MediaRecorder;

/* renamed from: com.adwo.adsdk.al  reason: case insensitive filesystem */
final class C0014al implements MediaRecorder.OnInfoListener {
    /* access modifiers changed from: private */
    public /* synthetic */ C0012aj a;

    C0014al(C0012aj ajVar) {
        this.a = ajVar;
    }

    public final void onInfo(MediaRecorder mediaRecorder, int i, int i2) {
        if (i == 800 || i == 801 || i == 1) {
            if (this.a.a != null) {
                new Thread(new C0015am(this)).start();
            } else if (this.a.c != null) {
                this.a.c.loadUrl("javascript:adwoVoiceRecordComplete(" + ((String) null) + ");");
            }
        }
        this.a.b();
    }
}
