package com.adwo.adsdk;

import android.content.DialogInterface;
import android.widget.VideoView;

final class aM implements DialogInterface.OnDismissListener {
    private final /* synthetic */ VideoView a;

    aM(aI aIVar, VideoView videoView) {
        this.a = videoView;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        if (this.a != null) {
            this.a.stopPlayback();
        }
    }
}
