package com.adwo.adsdk;

import android.content.DialogInterface;
import android.webkit.JsResult;

/* renamed from: com.adwo.adsdk.n  reason: case insensitive filesystem */
final class C0033n implements DialogInterface.OnCancelListener {
    private final /* synthetic */ JsResult a;

    C0033n(C0030k kVar, JsResult jsResult) {
        this.a = jsResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.a.cancel();
    }
}
