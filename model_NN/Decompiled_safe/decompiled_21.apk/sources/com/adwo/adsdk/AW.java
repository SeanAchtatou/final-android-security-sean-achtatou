package com.adwo.adsdk;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.PopupWindow;

final class AW extends PopupWindow {
    public AW() {
    }

    public AW(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public AW(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AW(Context context) {
        super(context);
    }
}
