package com.adwo.adsdk;

import android.media.MediaPlayer;

final class F implements MediaPlayer.OnCompletionListener {
    private /* synthetic */ E a;

    F(E e) {
        this.a = e;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        C0045z.g(this.a.a);
        C0045z.a = false;
        if (this.a.a != null) {
            this.a.a.loadUrl("javascript:adwoDoPlayVideoComplete();");
        }
    }
}
