package com.adwo.adsdk;

import java.io.File;
import java.util.Comparator;

final class N implements Comparator {
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        long lastModified = ((File) obj).lastModified() - ((File) obj2).lastModified();
        if (lastModified > 0) {
            return 1;
        }
        return lastModified == 0 ? 0 : -1;
    }

    N() {
    }

    public final boolean equals(Object obj) {
        return true;
    }
}
