package com.adwo.adsdk;

import android.app.Dialog;
import android.media.MediaPlayer;

/* renamed from: com.adwo.adsdk.w  reason: case insensitive filesystem */
final class C0042w implements MediaPlayer.OnErrorListener {
    private final /* synthetic */ Dialog a;

    C0042w(C0036q qVar, Dialog dialog) {
        this.a = dialog;
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        if (this.a == null) {
            return true;
        }
        this.a.dismiss();
        return true;
    }
}
