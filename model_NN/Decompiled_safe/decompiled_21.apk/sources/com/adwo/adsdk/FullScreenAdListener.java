package com.adwo.adsdk;

public interface FullScreenAdListener {
    void onAdDismiss();

    void onFailedToReceiveAd(ErrorCode errorCode);

    void onLoadAdComplete();

    void onReceiveAd();
}
