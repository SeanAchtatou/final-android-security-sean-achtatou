package com.adwo.adsdk;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.EditText;

/* renamed from: com.adwo.adsdk.k  reason: case insensitive filesystem */
public final class C0030k extends WebChromeClient {
    protected C0030k(AdDisplayer adDisplayer) {
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdDisplayer.m);
        builder.setMessage(str2).setPositiveButton("确定", (DialogInterface.OnClickListener) null);
        builder.setCancelable(false);
        builder.create().show();
        jsResult.confirm();
        return true;
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdDisplayer.m);
        builder.setMessage(str2).setPositiveButton("确定", new C0031l(this, jsResult)).setNeutralButton("取消", new C0032m(this, jsResult));
        builder.setOnCancelListener(new C0033n(this, jsResult));
        builder.create().show();
        return true;
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdDisplayer.m);
        builder.setMessage(str2);
        EditText editText = new EditText(webView.getContext());
        editText.setSingleLine();
        editText.setText(str3);
        builder.setView(editText).setPositiveButton("确定", new C0034o(this, jsPromptResult, editText)).setNeutralButton("取消", new C0035p(this, jsPromptResult));
        builder.create().show();
        return true;
    }
}
