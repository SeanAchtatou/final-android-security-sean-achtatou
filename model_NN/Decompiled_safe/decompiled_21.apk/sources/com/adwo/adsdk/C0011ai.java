package com.adwo.adsdk;

/* renamed from: com.adwo.adsdk.ai  reason: case insensitive filesystem */
final class C0011ai implements Runnable {
    private /* synthetic */ FullScreenAd a;

    C0011ai(FullScreenAd fullScreenAd) {
        this.a = fullScreenAd;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0043 A[SYNTHETIC, Splitter:B:11:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0048 A[Catch:{ Exception -> 0x0094 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[ADDED_TO_REGION, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r1 = 0
            com.adwo.adsdk.FullScreenAd r0 = r5.a
            boolean r0 = r0.isCharged
            if (r0 != 0) goto L_0x007b
            com.adwo.adsdk.FullScreenAd r0 = r5.a
            java.lang.String r0 = r0.showChargeURL
            if (r0 == 0) goto L_0x004b
            com.adwo.adsdk.FullScreenAd r0 = r5.a     // Catch:{ IOException -> 0x0060 }
            java.lang.String r0 = r0.showChargeURL     // Catch:{ IOException -> 0x0060 }
            java.net.URL r2 = new java.net.URL     // Catch:{ IOException -> 0x0060 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0060 }
            java.net.URLConnection r0 = r2.openConnection()     // Catch:{ IOException -> 0x0060 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0060 }
            int r2 = com.adwo.adsdk.U.a     // Catch:{ IOException -> 0x0096 }
            r0.setConnectTimeout(r2)     // Catch:{ IOException -> 0x0096 }
            int r2 = com.adwo.adsdk.U.a     // Catch:{ IOException -> 0x0096 }
            r0.setReadTimeout(r2)     // Catch:{ IOException -> 0x0096 }
            r0.connect()     // Catch:{ IOException -> 0x0096 }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ IOException -> 0x0096 }
            int r2 = r0.getResponseCode()     // Catch:{ IOException -> 0x0096 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x0041
            com.adwo.adsdk.FullScreenAd r2 = r5.a     // Catch:{ IOException -> 0x0096 }
            r3 = 1
            r2.isCharged = r3     // Catch:{ IOException -> 0x0096 }
            java.lang.String r2 = "Adwo SDK"
            java.lang.String r3 = "Charging successfully."
            android.util.Log.v(r2, r3)     // Catch:{ IOException -> 0x0096 }
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ Exception -> 0x0094 }
        L_0x0046:
            if (r0 == 0) goto L_0x004b
            r0.disconnect()     // Catch:{ Exception -> 0x0094 }
        L_0x004b:
            com.adwo.adsdk.FullScreenAd r0 = r5.a
            java.util.List r0 = r0.showBeaconList
            if (r0 == 0) goto L_0x005f
            com.adwo.adsdk.FullScreenAd r0 = r5.a
            java.util.List r0 = r0.showBeaconList
            int r2 = r0.size()
            if (r2 == 0) goto L_0x005f
            r0 = 0
            r1 = r0
        L_0x005d:
            if (r1 < r2) goto L_0x0083
        L_0x005f:
            return
        L_0x0060:
            r0 = move-exception
            r0 = r1
        L_0x0062:
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Could not determine final click destination URL.  Will try to follow anyway.  "
            r3.<init>(r4)
            com.adwo.adsdk.FullScreenAd r4 = r5.a
            java.lang.String r4 = r4.showChargeURL
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r2, r3)
            goto L_0x0041
        L_0x007b:
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "This ad had already been charged for showing. "
            android.util.Log.e(r0, r1)
            goto L_0x004b
        L_0x0083:
            com.adwo.adsdk.FullScreenAd r0 = r5.a
            java.util.List r0 = r0.showBeaconList
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            com.adwo.adsdk.U.a(r0)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x005d
        L_0x0094:
            r0 = move-exception
            goto L_0x004b
        L_0x0096:
            r2 = move-exception
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0011ai.run():void");
    }
}
