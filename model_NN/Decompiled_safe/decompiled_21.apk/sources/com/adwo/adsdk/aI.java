package com.adwo.adsdk;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public final class aI extends WebViewClient {
    /* access modifiers changed from: private */
    public /* synthetic */ ar a;

    protected aI(ar arVar) {
        this.a = arVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        try {
            if (this.a.c.canGoBack()) {
                this.a.h.setVisibility(0);
                if (this.a.f != null) {
                    this.a.f.setVisibility(0);
                }
            } else if (this.a.f != null) {
                this.a.f.setVisibility(4);
            }
            if (this.a.c.canGoForward()) {
                this.a.h.setVisibility(0);
                if (this.a.e != null) {
                    this.a.e.setVisibility(0);
                }
            } else if (this.a.e != null) {
                this.a.e.setVisibility(4);
            }
            if (str != null && str.equalsIgnoreCase(str)) {
                U.a(webView, this.a.p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v142, types: [int] */
    /* JADX WARN: Type inference failed for: r1v146, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v147 */
    /* JADX WARN: Type inference failed for: r1v151 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean shouldOverrideUrlLoading(android.webkit.WebView r10, java.lang.String r11) {
        /*
            r9 = this;
            r6 = 2
            r1 = 0
            r7 = 1
            if (r11 == 0) goto L_0x06c8
            java.lang.String r0 = "http"
            boolean r0 = r11.startsWith(r0)
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = "https"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x0099
        L_0x0015:
            java.lang.String r0 = ".mp3"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = ".wav"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = ".mp4"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = ".3gp"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = ".mpeg"
            boolean r0 = r11.endsWith(r0)
            if (r0 == 0) goto L_0x0043
        L_0x003d:
            android.content.Context r0 = com.adwo.adsdk.ar.j
            com.adwo.adsdk.U.b(r0, r11)
        L_0x0042:
            return r7
        L_0x0043:
            java.lang.String r0 = ".apk"
            boolean r0 = r11.endsWith(r0)
            if (r0 == 0) goto L_0x0065
            com.adwo.adsdk.ar r0 = r9.a
            int r0 = r0.q
            r0 = r0 & 240(0xf0, float:3.36E-43)
            int r0 = r0 >> 4
            byte r0 = (byte) r0
            if (r0 <= 0) goto L_0x06cb
            r0 = r7
        L_0x0057:
            android.content.Context r1 = com.adwo.adsdk.ar.j
            com.adwo.adsdk.ar r2 = r9.a
            int r2 = r2.p
            com.adwo.adsdk.ar r3 = r9.a
            short r3 = r3.r
            com.adwo.adsdk.U.a(r11, r1, r2, r0, r3)
            goto L_0x0042
        L_0x0065:
            java.lang.String r0 = ".jpeg"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x008d
            java.lang.String r0 = ".gif"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x008d
            java.lang.String r0 = ".png"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x008d
            java.lang.String r0 = ".bmp"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x008d
            java.lang.String r0 = ".jpg"
            boolean r0 = r11.endsWith(r0)
            if (r0 == 0) goto L_0x0095
        L_0x008d:
            android.content.Context r0 = com.adwo.adsdk.ar.j
            android.app.Activity r0 = (android.app.Activity) r0
            com.adwo.adsdk.U.a(r11, r0)
            goto L_0x0042
        L_0x0095:
            r10.loadUrl(r11)
            goto L_0x0042
        L_0x0099:
            java.lang.String r0 = "tel"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x00a7
            android.content.Context r0 = com.adwo.adsdk.ar.j
            com.adwo.adsdk.U.f(r0, r11)
            goto L_0x0042
        L_0x00a7:
            java.lang.String r0 = "sms"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x00b5
            android.content.Context r0 = com.adwo.adsdk.ar.j
            com.adwo.adsdk.U.c(r0, r11)
            goto L_0x0042
        L_0x00b5:
            java.lang.String r0 = "market"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x00c3
            android.content.Context r0 = com.adwo.adsdk.ar.j
            com.adwo.adsdk.U.d(r0, r11)
            goto L_0x0042
        L_0x00c3:
            java.lang.String r0 = "mailto"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x00d2
            android.content.Context r0 = com.adwo.adsdk.ar.j
            com.adwo.adsdk.U.e(r0, r11)
            goto L_0x0042
        L_0x00d2:
            java.lang.String r0 = "adwo://"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x06c8
            r0 = 7
            java.lang.String r2 = r11.substring(r0)     // Catch:{ Exception -> 0x0194 }
            java.util.StringTokenizer r3 = new java.util.StringTokenizer     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = "&"
            r3.<init>(r2, r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r4 = "adwoOpenURL"
            boolean r4 = r4.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r4 == 0) goto L_0x01db
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r4 = "_"
            int r4 = r0.indexOf(r4)     // Catch:{ Exception -> 0x0194 }
            int r4 = r4 + 1
            java.lang.String r0 = r0.substring(r4)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            int r4 = r0.intValue()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r5 = "_"
            int r5 = r0.indexOf(r5)     // Catch:{ Exception -> 0x0194 }
            int r5 = r5 + 1
            java.lang.String r0 = r0.substring(r5)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            r0.intValue()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r5 = "_"
            int r5 = r0.indexOf(r5)     // Catch:{ Exception -> 0x0194 }
            int r5 = r5 + 1
            java.lang.String r0 = r0.substring(r5)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            r0.intValue()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r5 = "_"
            int r5 = r0.indexOf(r5)     // Catch:{ Exception -> 0x0194 }
            int r5 = r5 + 1
            java.lang.String r0 = r0.substring(r5)     // Catch:{ Exception -> 0x0194 }
        L_0x0148:
            boolean r5 = r3.hasMoreTokens()     // Catch:{ Exception -> 0x0194 }
            if (r5 != 0) goto L_0x019a
            if (r4 != r7) goto L_0x01b6
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x0194 }
            r1.<init>()     // Catch:{ Exception -> 0x0194 }
            android.content.ComponentName r3 = new android.content.ComponentName     // Catch:{ Exception -> 0x0179 }
            java.lang.String r4 = "com.android.browser"
            java.lang.String r5 = "com.android.browser.BrowserActivity"
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x0179 }
            r1.setComponent(r3)     // Catch:{ Exception -> 0x0179 }
            java.lang.String r3 = "android.intent.action.VIEW"
            r1.setAction(r3)     // Catch:{ Exception -> 0x0179 }
            android.net.Uri r0 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x0179 }
            r1.setData(r0)     // Catch:{ Exception -> 0x0179 }
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r0)     // Catch:{ Exception -> 0x0179 }
            android.content.Context r0 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0179 }
            r0.startActivity(r1)     // Catch:{ Exception -> 0x0179 }
            goto L_0x0042
        L_0x0179:
            r0 = move-exception
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "javascript:adwoURLOpenFailed(+"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = ");"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0194 }
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0194:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0042
        L_0x019a:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x0194 }
            r5.<init>(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = "&"
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r5 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0194 }
            goto L_0x0148
        L_0x01b6:
            if (r4 != r6) goto L_0x0042
            java.lang.String r2 = ".apk"
            boolean r2 = r0.endsWith(r2)     // Catch:{ Exception -> 0x0194 }
            if (r2 == 0) goto L_0x0042
            com.adwo.adsdk.ar r2 = r9.a     // Catch:{ Exception -> 0x0194 }
            int r2 = r2.q     // Catch:{ Exception -> 0x0194 }
            r2 = r2 & 240(0xf0, float:3.36E-43)
            int r2 = r2 >> 4
            byte r2 = (byte) r2     // Catch:{ Exception -> 0x0194 }
            if (r2 <= 0) goto L_0x01cc
            r1 = r7
        L_0x01cc:
            android.content.Context r2 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r3 = r9.a     // Catch:{ Exception -> 0x0194 }
            int r3 = r3.p     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r4 = r9.a     // Catch:{ Exception -> 0x0194 }
            short r4 = r4.r     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.U.a(r0, r2, r3, r1, r4)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x01db:
            java.lang.String r2 = "adwoVibrate"
            boolean r2 = r2.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r2 == 0) goto L_0x0243
            boolean r0 = com.adwo.adsdk.C0045z.a     // Catch:{ Exception -> 0x0194 }
            if (r0 != 0) goto L_0x0042
            r0 = 1
            com.adwo.adsdk.C0045z.a = r0     // Catch:{ Exception -> 0x0194 }
            android.content.Context r0 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = "vibrator"
            java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ Exception -> 0x0194 }
            android.os.Vibrator r0 = (android.os.Vibrator) r0     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r3 = "_"
            int r3 = r2.indexOf(r3)     // Catch:{ Exception -> 0x0194 }
            int r3 = r3 + 1
            java.lang.String r2 = r2.substring(r3)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x0194 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0194 }
            if (r2 <= r7) goto L_0x0235
            int r3 = r2 * 2
            long[] r4 = new long[r3]     // Catch:{ Exception -> 0x0194 }
            r3 = r1
        L_0x0213:
            if (r3 < r2) goto L_0x0226
            r0.vibrate(r4, r2)     // Catch:{ Exception -> 0x0194 }
            r1 = r2
        L_0x0219:
            com.adwo.adsdk.aJ r2 = new com.adwo.adsdk.aJ     // Catch:{ Exception -> 0x0194 }
            r2.<init>(r9, r0)     // Catch:{ Exception -> 0x0194 }
            int r0 = r1 * 1000
            long r0 = (long) r0     // Catch:{ Exception -> 0x0194 }
            r10.postDelayed(r2, r0)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0226:
            r5 = 100
            r4[r1] = r5     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            r5 = 500(0x1f4, double:2.47E-321)
            r4[r1] = r5     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            int r3 = r3 + 1
            goto L_0x0213
        L_0x0235:
            r1 = 2
            long[] r1 = new long[r1]     // Catch:{ Exception -> 0x0194 }
            r2 = 1
            r3 = 1000(0x3e8, double:4.94E-321)
            r1[r2] = r3     // Catch:{ Exception -> 0x0194 }
            r2 = 1
            r0.vibrate(r1, r2)     // Catch:{ Exception -> 0x0194 }
            r1 = r7
            goto L_0x0219
        L_0x0243:
            java.lang.String r1 = "adwoCloseAd"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x028a
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r1 = r9.a     // Catch:{ Exception -> 0x0194 }
            switch(r0) {
                case 2: goto L_0x026f;
                case 3: goto L_0x0278;
                case 4: goto L_0x0268;
                case 5: goto L_0x0281;
                case 6: goto L_0x0281;
                default: goto L_0x0268;
            }     // Catch:{ Exception -> 0x0194 }
        L_0x0268:
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            r0.a()     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x026f:
            android.widget.PopupWindow r0 = r1.a     // Catch:{ Exception -> 0x0194 }
            r1 = 16973827(0x1030003, float:2.4060908E-38)
            r0.setAnimationStyle(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0268
        L_0x0278:
            android.widget.PopupWindow r0 = r1.a     // Catch:{ Exception -> 0x0194 }
            r1 = 16973910(0x1030056, float:2.406114E-38)
            r0.setAnimationStyle(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0268
        L_0x0281:
            android.widget.PopupWindow r0 = r1.a     // Catch:{ Exception -> 0x0194 }
            r1 = 16973826(0x1030002, float:2.4060906E-38)
            r0.setAnimationStyle(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0268
        L_0x028a:
            java.lang.String r1 = "adwoPlayAudio"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x02c8
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            r0.intValue()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aK r2 = new com.adwo.adsdk.aK     // Catch:{ Exception -> 0x0194 }
            r2.<init>(r9, r0)     // Catch:{ Exception -> 0x0194 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0194 }
            r1.start()     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x02c8:
            java.lang.String r1 = "adwoPlayVideo"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x039c
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            r0.intValue()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            r0.intValue()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = "_"
            int r2 = r1.indexOf(r2)     // Catch:{ Exception -> 0x0194 }
            int r2 = r2 + 1
            java.lang.String r1 = r1.substring(r2)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r1 = java.lang.Integer.decode(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r3 = "_"
            int r3 = r2.indexOf(r3)     // Catch:{ Exception -> 0x0194 }
            int r3 = r3 + 1
            java.lang.String r2 = r2.substring(r3)     // Catch:{ Exception -> 0x0194 }
            android.app.Dialog r3 = new android.app.Dialog     // Catch:{ Exception -> 0x0194 }
            android.content.Context r4 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0194 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0194 }
            r4 = 0
            r3.setCancelable(r4)     // Catch:{ Exception -> 0x0194 }
            r4 = 0
            r3.setCanceledOnTouchOutside(r4)     // Catch:{ Exception -> 0x0194 }
            android.view.Window r4 = r3.getWindow()     // Catch:{ Exception -> 0x0194 }
            r5 = 16777216(0x1000000, float:2.3509887E-38)
            r6 = 16777216(0x1000000, float:2.3509887E-38)
            r4.setFlags(r5, r6)     // Catch:{ Exception -> 0x0194 }
            r4 = 1
            r3.requestWindowFeature(r4)     // Catch:{ Exception -> 0x0194 }
            android.widget.VideoView r4 = new android.widget.VideoView     // Catch:{ Exception -> 0x0194 }
            android.content.Context r5 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0194 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aM r5 = new com.adwo.adsdk.aM     // Catch:{ Exception -> 0x0194 }
            r5.<init>(r9, r4)     // Catch:{ Exception -> 0x0194 }
            r3.setOnDismissListener(r5)     // Catch:{ Exception -> 0x0194 }
            android.widget.MediaController r5 = new android.widget.MediaController     // Catch:{ Exception -> 0x0194 }
            android.content.Context r6 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0194 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0194 }
            r4.setMediaController(r5)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aN r5 = new com.adwo.adsdk.aN     // Catch:{ Exception -> 0x0194 }
            r5.<init>(r9, r3, r10)     // Catch:{ Exception -> 0x0194 }
            r4.setOnCompletionListener(r5)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aO r5 = new com.adwo.adsdk.aO     // Catch:{ Exception -> 0x0194 }
            r5.<init>(r9, r3)     // Catch:{ Exception -> 0x0194 }
            r4.setOnErrorListener(r5)     // Catch:{ Exception -> 0x0194 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ Exception -> 0x0194 }
            r4.setVideoURI(r2)     // Catch:{ Exception -> 0x0194 }
            r4.start()     // Catch:{ Exception -> 0x0194 }
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0194 }
            r2.<init>(r0, r1)     // Catch:{ Exception -> 0x0194 }
            r3.setContentView(r4, r2)     // Catch:{ Exception -> 0x0194 }
            r3.show()     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x039c:
            java.lang.String r1 = "adwoCloseVideo"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x03a8
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x03a8:
            java.lang.String r1 = "adwoFetchLocation"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x03e1
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            double r0 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.K.I = r0     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            double r0 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.K.J = r0     // Catch:{ Exception -> 0x0194 }
            r0 = 1
            com.adwo.adsdk.K.E = r0     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x03e1:
            java.lang.String r1 = "adwoPageDidLoad"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x03f1
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.C0045z.b = r0     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x03f1:
            java.lang.String r1 = "adwoAdClickCount"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x0423
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = "javascript:"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "();"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0194 }
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0423:
            java.lang.String r1 = "adwoChangeBrowserDoneStatus"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x045f
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r1 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.ImageView r1 = r1.g     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x0042
            if (r0 != 0) goto L_0x0455
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.ImageView r0 = r0.g     // Catch:{ Exception -> 0x0194 }
            r1 = 0
            r0.setEnabled(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0455:
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.ImageView r0 = r0.g     // Catch:{ Exception -> 0x0194 }
            r1 = 1
            r0.setEnabled(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x045f:
            java.lang.String r1 = "adwoAddToAddressBook"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x0472
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.webkit.WebView r0 = r0.c     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "javascript:adwoDoGetAddressBookInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0472:
            java.lang.String r1 = "adwoAddToCalendar"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x0485
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.webkit.WebView r0 = r0.c     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "javascript:adwoDoGetCalendarEventInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0485:
            java.lang.String r1 = "adwoAddToReminder"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x0498
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.webkit.WebView r0 = r0.c     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "javascript:adwoDoGetReminderInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0498:
            java.lang.String r1 = "adwoInitEssentialParams"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x04d3
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r1 = r9.a     // Catch:{ Exception -> 0x0194 }
            int r1 = r1.p     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.U.a(r10, r1)     // Catch:{ Exception -> 0x0194 }
            if (r10 == 0) goto L_0x0042
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = "javascript:"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "+();"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0194 }
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x04d3:
            java.lang.String r1 = "adwoStartAudioRecorder"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x0506
            com.adwo.adsdk.ar r8 = r9.a     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aj r0 = new com.adwo.adsdk.aj     // Catch:{ Exception -> 0x0194 }
            r1 = 4648488871632306176(0x4082c00000000000, double:600.0)
            r3 = 0
            r4 = 0
            com.adwo.adsdk.ar r5 = r9.a     // Catch:{ Exception -> 0x0194 }
            int r6 = r5.p     // Catch:{ Exception -> 0x0194 }
            r5 = r10
            r0.<init>(r1, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0194 }
            r8.b = r0     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aj r0 = r0.b     // Catch:{ Exception -> 0x0194 }
            r0.a()     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.os.Handler r0 = r0.m     // Catch:{ Exception -> 0x0194 }
            r1 = 6
            com.adwo.adsdk.ar r2 = r9.a     // Catch:{ Exception -> 0x0194 }
            int r2 = r2.l     // Catch:{ Exception -> 0x0194 }
            long r2 = (long) r2     // Catch:{ Exception -> 0x0194 }
            r0.sendEmptyMessageDelayed(r1, r2)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0506:
            java.lang.String r1 = "adwoCloseAudioRecorder"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x051d
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            r0.b()     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.os.Handler r0 = r0.m     // Catch:{ Exception -> 0x0194 }
            r1 = 6
            r0.removeMessages(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x051d:
            java.lang.String r1 = "adwoTakePhoto"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x054a
            android.content.Context r0 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0194 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "android.hardware.camera"
            boolean r0 = r0.hasSystemFeature(r1)     // Catch:{ Exception -> 0x0194 }
            if (r0 == 0) goto L_0x053c
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = "javascript:adwoFetchCameraAssociatedImageInfo();"
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x053c:
            android.content.Context r0 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "设备没有摄像头调用！"
            r2 = 0
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)     // Catch:{ Exception -> 0x0194 }
            r0.show()     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x054a:
            java.lang.String r1 = "adwoCloseCamera"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x0556
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0556:
            java.lang.String r1 = "adwoSaveImg2Gallery"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x057d
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aP r2 = new com.adwo.adsdk.aP     // Catch:{ Exception -> 0x0194 }
            r2.<init>(r9, r0)     // Catch:{ Exception -> 0x0194 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0194 }
            r1.start()     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x057d:
            java.lang.String r1 = "adwoVoiceRecord"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x05dd
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            double r1 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r3 = "_"
            int r3 = r0.indexOf(r3)     // Catch:{ Exception -> 0x0194 }
            int r3 = r3 + 1
            java.lang.String r3 = r0.substring(r3)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r8 = r9.a     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aj r0 = new com.adwo.adsdk.aj     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r5 = com.adwo.adsdk.K.N     // Catch:{ Exception -> 0x0194 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0194 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0194 }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r5 = "adwoRecord.mp4"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r5 = r9.a     // Catch:{ Exception -> 0x0194 }
            int r6 = r5.p     // Catch:{ Exception -> 0x0194 }
            r5 = r10
            r0.<init>(r1, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0194 }
            r8.b = r0     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aj r0 = r0.b     // Catch:{ Exception -> 0x0194 }
            r0.a()     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x05dd:
            java.lang.String r1 = "adwoVoiceRecordStop"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x0613
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0194 }
            if (r0 == 0) goto L_0x060c
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aj r0 = r0.b     // Catch:{ Exception -> 0x0194 }
            if (r0 == 0) goto L_0x060c
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.aj r0 = r0.b     // Catch:{ Exception -> 0x0194 }
            r1 = 0
            r0.a = r1     // Catch:{ Exception -> 0x0194 }
        L_0x060c:
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            r0.b()     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0613:
            java.lang.String r1 = "adwoBrowserDisableScroll"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x062d
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.webkit.WebView r0 = r0.c     // Catch:{ Exception -> 0x0194 }
            r1 = 0
            r0.setVerticalScrollBarEnabled(r1)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.webkit.WebView r0 = r0.c     // Catch:{ Exception -> 0x0194 }
            r1 = 0
            r0.setHorizontalScrollBarEnabled(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x062d:
            java.lang.String r1 = "adwoBrowserShowIndicator"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x0660
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.ProgressBar r1 = new android.widget.ProgressBar     // Catch:{ Exception -> 0x0194 }
            android.content.Context r2 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0194 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0194 }
            r0.i = r1     // Catch:{ Exception -> 0x0194 }
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0194 }
            r1 = -2
            r2 = -2
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0194 }
            r1 = 13
            r0.addRule(r1)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r1 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.ProgressBar r1 = r1.i     // Catch:{ Exception -> 0x0194 }
            r1.setLayoutParams(r0)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.RelativeLayout r0 = r0.d     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r1 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.ProgressBar r1 = r1.i     // Catch:{ Exception -> 0x0194 }
            r0.addView(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x0660:
            java.lang.String r1 = "adwoBrowserHideIndicator"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x067b
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.RelativeLayout r0 = r0.d     // Catch:{ Exception -> 0x0194 }
            if (r0 == 0) goto L_0x0042
            com.adwo.adsdk.ar r0 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.RelativeLayout r0 = r0.d     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r1 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.ProgressBar r1 = r1.i     // Catch:{ Exception -> 0x0194 }
            r0.removeView(r1)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x067b:
            java.lang.String r1 = "adwoIntent"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r1 == 0) goto L_0x069a
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            android.content.Context r1 = com.adwo.adsdk.ar.j     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = "_"
            int r2 = r0.indexOf(r2)     // Catch:{ Exception -> 0x0194 }
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.U.g(r1, r0)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x069a:
            java.lang.String r1 = "adwoSetBackgroundColor"
            boolean r0 = r1.equalsIgnoreCase(r0)     // Catch:{ Exception -> 0x0194 }
            if (r0 == 0) goto L_0x0042
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x0194 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x0194 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0194 }
            com.adwo.adsdk.ar r1 = r9.a     // Catch:{ Exception -> 0x0194 }
            android.widget.PopupWindow r1 = r1.a     // Catch:{ Exception -> 0x0194 }
            android.graphics.drawable.ColorDrawable r2 = new android.graphics.drawable.ColorDrawable     // Catch:{ Exception -> 0x0194 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0194 }
            r1.setBackgroundDrawable(r2)     // Catch:{ Exception -> 0x0194 }
            goto L_0x0042
        L_0x06c8:
            r7 = r1
            goto L_0x0042
        L_0x06cb:
            r0 = r1
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.aI.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
    }
}
