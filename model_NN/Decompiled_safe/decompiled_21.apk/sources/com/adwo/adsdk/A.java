package com.adwo.adsdk;

import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import igudi.com.ergushi.AnimationType;

final class A extends Handler {
    /* access modifiers changed from: private */
    public /* synthetic */ C0045z a;

    A(C0045z zVar) {
        this.a = zVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adwo.adsdk.z.a(com.adwo.adsdk.z, long):void
     arg types: [com.adwo.adsdk.z, int]
     candidates:
      com.adwo.adsdk.z.a(com.adwo.adsdk.z, int):void
      com.adwo.adsdk.z.a(com.adwo.adsdk.z, android.os.Vibrator):void
      com.adwo.adsdk.z.a(com.adwo.adsdk.z, android.widget.VideoView):void
      com.adwo.adsdk.z.a(com.adwo.adsdk.z, com.adwo.adsdk.T):void
      com.adwo.adsdk.z.a(com.adwo.adsdk.z, com.adwo.adsdk.aj):void
      com.adwo.adsdk.z.a(com.adwo.adsdk.z, java.util.StringTokenizer):void
      com.adwo.adsdk.z.a(com.adwo.adsdk.z, long):void */
    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 1:
                if (this.a.g != null) {
                    this.a.g.cancel();
                    this.a.g = (Vibrator) null;
                }
                C0045z.a = false;
                return;
            case 2:
                C0045z.a = true;
                new B(this, (String) message.obj).start();
                C0045z zVar = this.a;
                zVar.l = zVar.l + 1;
                return;
            case 3:
                try {
                    AdwoAdView adwoAdView = (AdwoAdView) this.a.getParent();
                    int b = adwoAdView.b();
                    int a2 = adwoAdView.a();
                    AdStatusListener c = adwoAdView.c();
                    if (c != null) {
                        c.onAdResumeRequesting();
                    }
                    this.a.setLayoutParams(new RelativeLayout.LayoutParams(a2, b));
                    TranslateAnimation translateAnimation = new TranslateAnimation(0.1f, 1.0f, 0.1f, 1.0f);
                    translateAnimation.setDuration(1000);
                    this.a.setAnimation(translateAnimation);
                    this.a.requestLayout();
                    this.a.loadUrl("javascript:adwoDoBannerRestoreComplete();");
                    if (this.a.o != 0) {
                        U.a(String.valueOf(this.a.e.a) + "=" + this.a.e.k + "=0", new StringBuilder(String.valueOf(System.currentTimeMillis() - this.a.o)).toString(), this.a.getContext());
                        U.a(this.a.e.k, String.valueOf(K.I) + "=" + K.J + "=" + this.a.e.a, this.a.getContext());
                        this.a.o = 0L;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.a.e.j = false;
                return;
            case 4:
                AdDisplayer.getInstance(this.a.getContext()).dismissDisplayer();
                return;
            case 5:
                C0045z.g(this.a);
                this.a.setVisibility(0);
                C0045z.a = false;
                return;
            case AnimationType.TRANSLATE_FROM_RIGHT /*6*/:
                if (this.a.j != null) {
                    this.a.loadUrl("javascript:adwoFetchAudioPower(" + (((double) this.a.j.c()) / 32767.0d) + ");");
                    this.a.p.sendEmptyMessageDelayed(6, (long) this.a.m);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
