package com.adwo.adsdk;

import android.content.DialogInterface;
import android.widget.VideoView;

/* renamed from: com.adwo.adsdk.u  reason: case insensitive filesystem */
final class C0040u implements DialogInterface.OnDismissListener {
    private final /* synthetic */ VideoView a;

    C0040u(C0036q qVar, VideoView videoView) {
        this.a = videoView;
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        if (this.a != null) {
            this.a.stopPlayback();
        }
    }
}
