package com.adwo.adsdk;

import android.app.Activity;
import android.widget.PopupWindow;
import java.io.File;

/* renamed from: com.adwo.adsdk.b  reason: case insensitive filesystem */
final class C0021b implements PopupWindow.OnDismissListener {
    /* access modifiers changed from: private */
    public /* synthetic */ AdDisplayer a;

    C0021b(AdDisplayer adDisplayer) {
        this.a = adDisplayer;
    }

    public final void onDismiss() {
        File[] listFiles;
        if (ar.o == null && this.a.u != null) {
            this.a.u.onAdDismiss();
        }
        C0045z.a = false;
        if (AdDisplayer.m(this.a)) {
            U.a(String.valueOf(C0045z.c) + "=" + C0045z.d, new StringBuilder(String.valueOf(System.currentTimeMillis() - C0045z.b)).toString(), AdDisplayer.m);
            U.a(C0045z.d, String.valueOf(K.I) + "=" + K.J + "=" + C0045z.c, AdDisplayer.m);
        }
        this.a.s.removeMessages(6);
        if (!(this.a.c == null || (listFiles = new File(K.O).listFiles()) == null || listFiles.length <= 0)) {
            for (File file : listFiles) {
                if (!this.a.c.contains(file.getAbsolutePath())) {
                    U.a(file);
                }
            }
        }
        if (this.a.o > 0 && (AdDisplayer.m instanceof Activity)) {
            try {
                Activity activity = (Activity) AdDisplayer.m;
                if (activity != null) {
                    activity.runOnUiThread(new C0022c(this, activity));
                }
            } catch (Exception e) {
            }
        }
    }
}
