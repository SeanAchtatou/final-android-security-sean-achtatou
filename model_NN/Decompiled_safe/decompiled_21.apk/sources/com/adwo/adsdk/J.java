package com.adwo.adsdk;

final class J implements Runnable {
    private /* synthetic */ I a;

    J(I i) {
        this.a = i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0054 A[SYNTHETIC, Splitter:B:14:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0059 A[Catch:{ Exception -> 0x00ee }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[ADDED_TO_REGION, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r1 = 0
            r5 = 1
            com.adwo.adsdk.I r0 = r6.a
            boolean r0 = r0.i
            if (r0 != 0) goto L_0x00c3
            com.adwo.adsdk.I r0 = r6.a
            java.lang.String r0 = r0.c
            if (r0 == 0) goto L_0x005c
            com.adwo.adsdk.I r0 = r6.a     // Catch:{ IOException -> 0x00a8 }
            java.lang.String r0 = r0.c     // Catch:{ IOException -> 0x00a8 }
            com.adwo.adsdk.I r2 = r6.a     // Catch:{ IOException -> 0x00a8 }
            java.lang.String r2 = r2.c     // Catch:{ IOException -> 0x00a8 }
            java.lang.String r3 = "clk?p0"
            int r2 = r2.indexOf(r3)     // Catch:{ IOException -> 0x00a8 }
            if (r2 <= 0) goto L_0x0022
            com.adwo.adsdk.I r0 = r6.a     // Catch:{ IOException -> 0x00a8 }
            java.lang.String r0 = r0.c     // Catch:{ IOException -> 0x00a8 }
        L_0x0022:
            java.net.URL r2 = new java.net.URL     // Catch:{ IOException -> 0x00a8 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x00a8 }
            java.net.URLConnection r0 = r2.openConnection()     // Catch:{ IOException -> 0x00a8 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x00a8 }
            int r2 = com.adwo.adsdk.U.a     // Catch:{ IOException -> 0x00f1 }
            r0.setConnectTimeout(r2)     // Catch:{ IOException -> 0x00f1 }
            int r2 = com.adwo.adsdk.U.a     // Catch:{ IOException -> 0x00f1 }
            r0.setReadTimeout(r2)     // Catch:{ IOException -> 0x00f1 }
            r0.connect()     // Catch:{ IOException -> 0x00f1 }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ IOException -> 0x00f1 }
            int r2 = r0.getResponseCode()     // Catch:{ IOException -> 0x00f1 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x0052
            com.adwo.adsdk.I r2 = r6.a     // Catch:{ IOException -> 0x00f1 }
            r3 = 1
            r2.i = r3     // Catch:{ IOException -> 0x00f1 }
            java.lang.String r2 = "Adwo SDK"
            java.lang.String r3 = "Charging successfully."
            android.util.Log.v(r2, r3)     // Catch:{ IOException -> 0x00f1 }
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()     // Catch:{ Exception -> 0x00ee }
        L_0x0057:
            if (r0 == 0) goto L_0x005c
            r0.disconnect()     // Catch:{ Exception -> 0x00ee }
        L_0x005c:
            java.util.HashMap r0 = com.adwo.adsdk.K.v
            com.adwo.adsdk.I r1 = r6.a
            int r1 = r1.a
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x00cb
            java.util.HashMap r0 = com.adwo.adsdk.K.v
            com.adwo.adsdk.I r1 = r6.a
            int r1 = r1.a
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.Object r0 = r0.get(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            int r0 = r0 + 1
            java.util.HashMap r1 = com.adwo.adsdk.K.v
            com.adwo.adsdk.I r2 = r6.a
            int r2 = r2.a
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1.put(r2, r0)
        L_0x0093:
            com.adwo.adsdk.I r0 = r6.a
            java.util.List r0 = r0.e
            if (r0 == 0) goto L_0x00a7
            com.adwo.adsdk.I r0 = r6.a
            java.util.List r0 = r0.e
            int r2 = r0.size()
            if (r2 == 0) goto L_0x00a7
            r0 = 0
            r1 = r0
        L_0x00a5:
            if (r1 < r2) goto L_0x00dd
        L_0x00a7:
            return
        L_0x00a8:
            r0 = move-exception
            r0 = r1
        L_0x00aa:
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Could not determine final click destination URL.  Will try to follow anyway.  "
            r3.<init>(r4)
            com.adwo.adsdk.I r4 = r6.a
            java.lang.String r4 = r4.c
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r2, r3)
            goto L_0x0052
        L_0x00c3:
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "This ad had already been clicked. "
            android.util.Log.e(r0, r1)
            goto L_0x005c
        L_0x00cb:
            java.util.HashMap r0 = com.adwo.adsdk.K.v
            com.adwo.adsdk.I r1 = r6.a
            int r1 = r1.a
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)
            r0.put(r1, r2)
            goto L_0x0093
        L_0x00dd:
            com.adwo.adsdk.I r0 = r6.a
            java.util.List r0 = r0.e
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            com.adwo.adsdk.U.a(r0)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x00a5
        L_0x00ee:
            r0 = move-exception
            goto L_0x005c
        L_0x00f1:
            r2 = move-exception
            goto L_0x00aa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.J.run():void");
    }
}
