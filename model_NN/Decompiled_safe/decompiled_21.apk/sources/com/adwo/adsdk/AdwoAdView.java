package com.adwo.adsdk;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import java.util.Random;

public final class AdwoAdView extends RelativeLayout {
    public static int b = 1;
    private static int g = 0;
    private static int h = 0;
    /* access modifiers changed from: private */
    public static byte i = 0;
    private static boolean l = false;
    private static int m = 1;
    private static final int o = 100;
    protected AdStatusListener a;
    /* access modifiers changed from: private */
    public C0045z c;
    /* access modifiers changed from: private */
    public AdListener d;
    private volatile boolean e;
    /* access modifiers changed from: private */
    public volatile boolean f;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public Random k;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public Handler p;

    /* access modifiers changed from: protected */
    public final int a() {
        return g;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        g = i2;
    }

    /* access modifiers changed from: protected */
    public final int b() {
        return h;
    }

    /* access modifiers changed from: protected */
    public final void b(int i2) {
        h = i2;
    }

    public static void setAggChannelId(byte b2) {
        if (b2 == 1) {
            K.s = 24;
        } else if (b2 == 2) {
            K.s = 40;
        } else if (b2 == 3) {
            K.s = 56;
        } else {
            K.s = 8;
        }
    }

    public static void setBannerMatchScreenWidth(boolean z) {
        l = z;
    }

    public static void setDesiredBannerSize(int i2) {
        m = i2;
    }

    public final void setRequestInterval(int i2) {
        if (i2 == 0) {
            this.f = true;
        }
        if (C0017ao.a <= i2) {
            C0017ao.a = i2;
        } else {
            Log.e("Adwo SDK", "The request interval you just set is too short, we have set the interval at " + C0017ao.a);
        }
    }

    public final void setKeyWords(String str) {
        K.l = str;
    }

    public final void setAnimationType(int i2) {
        this.j = i2;
    }

    public AdwoAdView(Context context, String str, boolean z, int i2) {
        super(context, null, 0);
        this.f = false;
        this.j = 1;
        this.k = new Random();
        this.n = true;
        this.p = new O(this);
        K.b(str);
        if (U.d(context)) {
            this.e = false;
            setFocusable(true);
            setDescendantFocusability(262144);
            setClickable(true);
            if (z) {
                K.b(z);
            }
            setRequestInterval(i2);
            a(context);
            K.C = C0017ao.a(context);
        }
    }

    public AdwoAdView(Context context, String str, boolean z) {
        this(context, str, z, 30);
    }

    public AdwoAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        boolean attributeBooleanValue;
        this.f = false;
        this.j = 1;
        this.k = new Random();
        this.n = true;
        this.p = new O(this);
        if (U.d(context)) {
            this.e = false;
            setFocusable(true);
            setDescendantFocusability(262144);
            setClickable(true);
            if (attributeSet != null && (attributeBooleanValue = attributeSet.getAttributeBooleanValue("http://schemas.android.com/apk/res/" + context.getPackageName(), "testing", false))) {
                K.b(attributeBooleanValue);
            }
            K.b(K.a(context));
            a(context);
            K.C = C0017ao.a(context);
        }
    }

    public AdwoAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    private void a(Context context) {
        K.B = AdwoKey.a(context);
        new DisplayMetrics();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        if (displayMetrics.widthPixels > displayMetrics.heightPixels) {
            K.y = displayMetrics.heightPixels;
            K.x = displayMetrics.widthPixels;
        } else {
            K.y = displayMetrics.widthPixels;
            K.x = displayMetrics.heightPixels;
        }
        K.K = (double) displayMetrics.density;
        Log.i("Adwo SDK", "Version 4.0 width: " + displayMetrics.widthPixels + " height:" + displayMetrics.heightPixels + "   density:" + displayMetrics.density + "-dp:" + displayMetrics.xdpi + "-" + displayMetrics.ydpi);
        K.c(context);
        if (l) {
            int i2 = (int) (((double) K.y) / K.K);
            K.z = i2;
            K.A = (i2 * 50) / 320;
        } else {
            if (m == 0) {
                int i3 = (int) (((double) K.y) / K.K);
                if (i3 == 360) {
                    K.z = 360;
                    K.A = 56;
                } else if (i3 == 400) {
                    K.z = 400;
                    K.A = 62;
                }
            } else if (m != 1) {
                if (m == 2) {
                    K.z = 360;
                    K.A = 56;
                } else if (m == 3) {
                    K.z = 400;
                    K.A = 62;
                } else if (m == 4) {
                    K.z = 320;
                    K.A = 250;
                } else if (m == 5) {
                    K.z = 360;
                    K.A = 275;
                } else if (m == 6) {
                    K.z = 400;
                    K.A = 300;
                } else {
                    Log.e("Adwo SDK", "Disired banner size not defined. The default setting will be used.");
                    int i4 = (int) (((double) K.y) / K.K);
                    if (i4 == 360) {
                        K.z = 360;
                        K.A = 56;
                    } else if (i4 == 400) {
                        K.z = 400;
                        K.A = 62;
                    } else {
                        K.z = 320;
                        K.A = 50;
                    }
                }
            }
            K.z = 320;
            K.A = 50;
        }
        a((int) (((float) K.z) * displayMetrics.density));
        b((int) (displayMetrics.density * ((float) K.A)));
        Log.e("Adwo SDK", "AdwoAdView initiating...");
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.e) {
            if (!this.n) {
                ViewGroup viewGroup = (ViewGroup) getParent();
                if (viewGroup == null) {
                    Log.e("Adwo SDK", "The Adview's parent view does not exist.");
                    return;
                } else if (viewGroup.getWidth() < a() || viewGroup.getHeight() < b() || getWidth() < a() || getHeight() < b()) {
                    Log.e("Adwo SDK", "The Adview's size is not correctly defined. Please make sure that the width of adview or its parent view equals or greater than 320, and hight of adview or its parent view equals or greater than 50.");
                    return;
                }
            }
            if (K.a()) {
                Log.e("Adwo SDK", "The interval that this request from the last one is shorter than 15 seconds.In other word, ad requesting has been too frequent.");
                if (this.d != null) {
                    this.d.onFailedToReceiveAd(this, new ErrorCode(30, "ERR_UNKNOWN"));
                    return;
                }
                return;
            }
            new P(this).execute(new Void[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        if (z) {
            this.p.removeMessages(o);
            K.Q.removeMessages(0);
            K.Q = null;
            return;
        }
        this.p.removeMessages(o);
        f();
        if (!this.f) {
            this.p.sendEmptyMessageDelayed(o, (long) (C0017ao.a * 1000));
        }
    }

    public final void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                if (i2 == 4 && this.c != null) {
                    removeView(this.c);
                    this.c.destroy();
                    this.c = null;
                    removeAllViews();
                }
            }
        }
    }

    public final void setListener(AdListener adListener) {
        synchronized (this) {
            this.d = adListener;
        }
    }

    public final void setStatusListener(AdStatusListener adStatusListener) {
        synchronized (this) {
            this.a = adStatusListener;
        }
    }

    /* access modifiers changed from: protected */
    public final AdStatusListener c() {
        return this.a;
    }

    public final boolean d() {
        return this.c != null;
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        this.e = true;
        a(false);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        this.e = false;
        try {
            a(true);
        } catch (Exception e2) {
        }
        super.onDetachedFromWindow();
    }
}
