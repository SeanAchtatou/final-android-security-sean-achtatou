package com.adwo.adsdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.umeng.common.b.e;
import igudi.com.ergushi.AnimationType;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

final class K {
    protected static int A = 50;
    protected static AdwoKey B;
    protected static C0017ao C;
    protected static int[] D = new int[0];
    protected static boolean E = false;
    protected static boolean F = false;
    protected static boolean G = false;
    protected static boolean H = false;
    protected static double I = 0.0d;
    protected static double J = 0.0d;
    protected static double K = 1.0d;
    protected static SimpleDateFormat L;
    protected static String M;
    protected static String N = "/mnt/sdcard/adwo/";
    protected static String O = "/mnt/sdcard/adwo/entryad/";
    protected static boolean P = false;
    protected static Handler Q = null;
    protected static int R = 20;
    protected static int S = 0;
    protected static byte T = 0;
    protected static int U;
    private static TelephonyManager V;
    private static byte[] W = null;
    private static String X;
    private static byte Y = 2;
    private static byte Z = 0;
    protected static String a = null;
    private static HashMap aa = new HashMap();
    private static boolean ab = false;
    private static boolean ac = false;
    /* access modifiers changed from: private */
    public static int ad = 30;
    private static Location ae = null;
    private static long af = 0;
    private static long ag = 0;
    protected static byte[] b = null;
    protected static byte[] c = null;
    protected static byte[] d = null;
    protected static byte[] e = "unknown".getBytes();
    protected static byte[] f = null;
    protected static byte[] g = null;
    protected static String h = null;
    protected static String i = null;
    protected static String j = null;
    protected static String k = null;
    protected static String l = "";
    protected static String m = "UNKNOWN";
    protected static byte n = -1;
    protected static byte o = 0;
    protected static byte p = 0;
    protected static byte q = 0;
    protected static byte r = 0;
    protected static byte s = 8;
    protected static byte t = 64;
    protected static byte u = 0;
    protected static HashMap v = new HashMap();
    protected static HashMap w = new HashMap();
    protected static int x = 800;
    protected static int y = 480;
    protected static int z = 320;

    K() {
    }

    static {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        L = simpleDateFormat;
        M = simpleDateFormat.format(new Date());
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ea A[SYNTHETIC, Splitter:B:46:0x00ea] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00ef A[Catch:{ Exception -> 0x00f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00f4 A[Catch:{ Exception -> 0x00f9 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static synchronized com.adwo.adsdk.I a(android.content.Context r13, byte r14) {
        /*
            r11 = -1
            r8 = 0
            java.lang.Class<com.adwo.adsdk.K> r10 = com.adwo.adsdk.K.class
            monitor-enter(r10)
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r13.checkCallingOrSelfPermission(r0)     // Catch:{ all -> 0x0121 }
            if (r0 != r11) goto L_0x0015
            java.lang.String r0 = "Can not make an ad request without INTERNET permission! Please add:  <uses-permission android:name=\"android.permission.INTERNET\" /> inside the </manifest> tag of the 'manifest.xml' file."
            c(r0)     // Catch:{ all -> 0x0121 }
            r0 = r8
        L_0x0013:
            monitor-exit(r10)
            return r0
        L_0x0015:
            boolean r0 = com.adwo.adsdk.K.ab     // Catch:{ all -> 0x0121 }
            boolean r1 = com.adwo.adsdk.K.ab     // Catch:{ Exception -> 0x012e }
            if (r1 == 0) goto L_0x0051
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x012e }
            java.lang.String r2 = "http://adtest.adwo.com/adaweb32"
            r1.<init>(r2)     // Catch:{ Exception -> 0x012e }
            r9 = r1
        L_0x0023:
            double[] r1 = d()     // Catch:{ Exception -> 0x012e }
            if (r1 == 0) goto L_0x0033
            r2 = 0
            r2 = r1[r2]     // Catch:{ Exception -> 0x012e }
            com.adwo.adsdk.K.I = r2     // Catch:{ Exception -> 0x012e }
            r2 = 1
            r1 = r1[r2]     // Catch:{ Exception -> 0x012e }
            com.adwo.adsdk.K.J = r1     // Catch:{ Exception -> 0x012e }
        L_0x0033:
            java.lang.String r1 = com.adwo.adsdk.K.M     // Catch:{ Exception -> 0x012e }
            int r7 = com.adwo.adsdk.U.a(r13, r1)     // Catch:{ Exception -> 0x012e }
            boolean r1 = com.adwo.adsdk.K.E     // Catch:{ Exception -> 0x012e }
            double r2 = com.adwo.adsdk.K.J     // Catch:{ Exception -> 0x012e }
            double r4 = com.adwo.adsdk.K.I     // Catch:{ Exception -> 0x012e }
            r6 = 0
            byte[] r1 = a(r0, r1, r2, r4, r6, r7)     // Catch:{ Exception -> 0x012e }
            f(r13)     // Catch:{ Exception -> 0x012e }
            java.net.URLConnection r0 = r9.openConnection()     // Catch:{ Exception -> 0x012e }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x012e }
            if (r0 != 0) goto L_0x005e
            r0 = r8
            goto L_0x0013
        L_0x0051:
            com.adwo.adsdk.ao r1 = com.adwo.adsdk.K.C     // Catch:{ Exception -> 0x012e }
            java.lang.String r2 = r1.a()     // Catch:{ Exception -> 0x012e }
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x012e }
            r1.<init>(r2)     // Catch:{ Exception -> 0x012e }
            r9 = r1
            goto L_0x0023
        L_0x005e:
            int r2 = com.adwo.adsdk.U.a     // Catch:{ Exception -> 0x0133 }
            r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x0133 }
            int r2 = com.adwo.adsdk.U.a     // Catch:{ Exception -> 0x0133 }
            r0.setReadTimeout(r2)     // Catch:{ Exception -> 0x0133 }
            java.lang.String r2 = "POST"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x0133 }
            r2 = 1
            r0.setDoOutput(r2)     // Catch:{ Exception -> 0x0133 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r3 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r2, r3)     // Catch:{ Exception -> 0x0133 }
            if (r1 == 0) goto L_0x0084
            java.lang.String r2 = "Content-Length"
            int r3 = r1.length     // Catch:{ Exception -> 0x0133 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ Exception -> 0x0133 }
            r0.setRequestProperty(r2, r3)     // Catch:{ Exception -> 0x0133 }
        L_0x0084:
            r0.connect()     // Catch:{ Exception -> 0x0133 }
            java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ Exception -> 0x0133 }
            if (r1 == 0) goto L_0x0090
            r3.write(r1)     // Catch:{ Exception -> 0x013a }
        L_0x0090:
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x013a }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0100 }
            r1.<init>()     // Catch:{ Exception -> 0x0100 }
        L_0x0099:
            int r4 = r2.read()     // Catch:{ Exception -> 0x0100 }
            if (r4 != r11) goto L_0x00fc
            byte[] r1 = r1.toByteArray()     // Catch:{ Exception -> 0x0100 }
            int r4 = r1.length     // Catch:{ Exception -> 0x0100 }
            if (r4 <= 0) goto L_0x0124
            com.adwo.adsdk.I r4 = com.adwo.adsdk.C0024e.b(r1)     // Catch:{ Exception -> 0x0100 }
            com.adwo.adsdk.ErrorCode r1 = r4.b     // Catch:{ Exception -> 0x0100 }
            if (r1 != 0) goto L_0x00e6
            java.lang.String r1 = "Adwo SDK"
            java.lang.String r5 = "Get an ad from Adwo servers."
            android.util.Log.d(r1, r5)     // Catch:{ Exception -> 0x0100 }
            java.util.HashMap r1 = com.adwo.adsdk.K.aa     // Catch:{ Exception -> 0x0100 }
            int r5 = r4.a     // Catch:{ Exception -> 0x0100 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0100 }
            boolean r1 = r1.containsKey(r5)     // Catch:{ Exception -> 0x0100 }
            if (r1 == 0) goto L_0x0110
            java.util.HashMap r1 = com.adwo.adsdk.K.aa     // Catch:{ Exception -> 0x0100 }
            int r5 = r4.a     // Catch:{ Exception -> 0x0100 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0100 }
            java.lang.Object r1 = r1.get(r5)     // Catch:{ Exception -> 0x0100 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0100 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0100 }
            int r1 = r1 + 1
            java.util.HashMap r5 = com.adwo.adsdk.K.aa     // Catch:{ Exception -> 0x0100 }
            int r6 = r4.a     // Catch:{ Exception -> 0x0100 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0100 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0100 }
            r5.put(r6, r1)     // Catch:{ Exception -> 0x0100 }
        L_0x00e6:
            r1 = r0
            r0 = r4
        L_0x00e8:
            if (r2 == 0) goto L_0x00ed
            r2.close()     // Catch:{ Exception -> 0x00f9 }
        L_0x00ed:
            if (r3 == 0) goto L_0x00f2
            r3.close()     // Catch:{ Exception -> 0x00f9 }
        L_0x00f2:
            if (r1 == 0) goto L_0x0013
            r1.disconnect()     // Catch:{ Exception -> 0x00f9 }
            goto L_0x0013
        L_0x00f9:
            r1 = move-exception
            goto L_0x0013
        L_0x00fc:
            r1.write(r4)     // Catch:{ Exception -> 0x0100 }
            goto L_0x0099
        L_0x0100:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0104:
            r0.printStackTrace()     // Catch:{ all -> 0x0121 }
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r4 = "Could not get an ad from Adwo servers,Network Error!"
            android.util.Log.w(r0, r4)     // Catch:{ all -> 0x0121 }
            r0 = r8
            goto L_0x00e8
        L_0x0110:
            java.util.HashMap r1 = com.adwo.adsdk.K.aa     // Catch:{ Exception -> 0x0100 }
            int r5 = r4.a     // Catch:{ Exception -> 0x0100 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0100 }
            r6 = 1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0100 }
            r1.put(r5, r6)     // Catch:{ Exception -> 0x0100 }
            goto L_0x00e6
        L_0x0121:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x0124:
            java.lang.String r1 = "Adwo SDK"
            java.lang.String r4 = "Could not get an ad from Adwo servers."
            android.util.Log.w(r1, r4)     // Catch:{ Exception -> 0x0100 }
            r1 = r0
            r0 = r8
            goto L_0x00e8
        L_0x012e:
            r0 = move-exception
            r1 = r8
            r2 = r8
            r3 = r8
            goto L_0x0104
        L_0x0133:
            r1 = move-exception
            r2 = r8
            r3 = r8
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0104
        L_0x013a:
            r1 = move-exception
            r2 = r8
            r12 = r0
            r0 = r1
            r1 = r12
            goto L_0x0104
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.K.a(android.content.Context, byte):com.adwo.adsdk.I");
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x00eb A[SYNTHETIC, Splitter:B:46:0x00eb] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00f0 A[Catch:{ Exception -> 0x00fa }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00f5 A[Catch:{ Exception -> 0x00fa }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static synchronized com.adwo.adsdk.FullScreenAd b(android.content.Context r13, byte r14) {
        /*
            r11 = -1
            r8 = 0
            java.lang.Class<com.adwo.adsdk.K> r10 = com.adwo.adsdk.K.class
            monitor-enter(r10)
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r13.checkCallingOrSelfPermission(r0)     // Catch:{ all -> 0x0122 }
            if (r0 != r11) goto L_0x0015
            java.lang.String r0 = "Can not make an ad request without INTERNET permission! Please add:  <uses-permission android:name=\"android.permission.INTERNET\" /> inside the </manifest> tag of the 'manifest.xml' file."
            c(r0)     // Catch:{ all -> 0x0122 }
            r0 = r8
        L_0x0013:
            monitor-exit(r10)
            return r0
        L_0x0015:
            boolean r0 = com.adwo.adsdk.K.ac     // Catch:{ all -> 0x0122 }
            boolean r1 = com.adwo.adsdk.K.ac     // Catch:{ Exception -> 0x012f }
            if (r1 == 0) goto L_0x0051
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x012f }
            java.lang.String r2 = "http://adtest.adwo.com/adfsa32"
            r1.<init>(r2)     // Catch:{ Exception -> 0x012f }
            r9 = r1
        L_0x0023:
            double[] r1 = d()     // Catch:{ Exception -> 0x012f }
            if (r1 == 0) goto L_0x0033
            r2 = 0
            r2 = r1[r2]     // Catch:{ Exception -> 0x012f }
            com.adwo.adsdk.K.I = r2     // Catch:{ Exception -> 0x012f }
            r2 = 1
            r1 = r1[r2]     // Catch:{ Exception -> 0x012f }
            com.adwo.adsdk.K.J = r1     // Catch:{ Exception -> 0x012f }
        L_0x0033:
            java.lang.String r1 = com.adwo.adsdk.K.M     // Catch:{ Exception -> 0x012f }
            int r7 = com.adwo.adsdk.U.a(r13, r1)     // Catch:{ Exception -> 0x012f }
            boolean r1 = com.adwo.adsdk.K.E     // Catch:{ Exception -> 0x012f }
            double r2 = com.adwo.adsdk.K.J     // Catch:{ Exception -> 0x012f }
            double r4 = com.adwo.adsdk.K.I     // Catch:{ Exception -> 0x012f }
            r6 = 0
            byte[] r1 = a(r0, r1, r2, r4, r6, r7)     // Catch:{ Exception -> 0x012f }
            f(r13)     // Catch:{ Exception -> 0x012f }
            java.net.URLConnection r0 = r9.openConnection()     // Catch:{ Exception -> 0x012f }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x012f }
            if (r0 != 0) goto L_0x005f
            r0 = r8
            goto L_0x0013
        L_0x0051:
            com.adwo.adsdk.ao r1 = com.adwo.adsdk.K.C     // Catch:{ Exception -> 0x012f }
            r1.a()     // Catch:{ Exception -> 0x012f }
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x012f }
            java.lang.String r2 = com.adwo.adsdk.C0017ao.c     // Catch:{ Exception -> 0x012f }
            r1.<init>(r2)     // Catch:{ Exception -> 0x012f }
            r9 = r1
            goto L_0x0023
        L_0x005f:
            int r2 = com.adwo.adsdk.U.a     // Catch:{ Exception -> 0x0134 }
            r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x0134 }
            int r2 = com.adwo.adsdk.U.a     // Catch:{ Exception -> 0x0134 }
            r0.setReadTimeout(r2)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r2 = "POST"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x0134 }
            r2 = 1
            r0.setDoOutput(r2)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r3 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r2, r3)     // Catch:{ Exception -> 0x0134 }
            if (r1 == 0) goto L_0x0085
            java.lang.String r2 = "Content-Length"
            int r3 = r1.length     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ Exception -> 0x0134 }
            r0.setRequestProperty(r2, r3)     // Catch:{ Exception -> 0x0134 }
        L_0x0085:
            r0.connect()     // Catch:{ Exception -> 0x0134 }
            java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ Exception -> 0x0134 }
            if (r1 == 0) goto L_0x0091
            r3.write(r1)     // Catch:{ Exception -> 0x013b }
        L_0x0091:
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x013b }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0101 }
            r1.<init>()     // Catch:{ Exception -> 0x0101 }
        L_0x009a:
            int r4 = r2.read()     // Catch:{ Exception -> 0x0101 }
            if (r4 != r11) goto L_0x00fd
            byte[] r1 = r1.toByteArray()     // Catch:{ Exception -> 0x0101 }
            int r4 = r1.length     // Catch:{ Exception -> 0x0101 }
            if (r4 <= 0) goto L_0x0125
            com.adwo.adsdk.FullScreenAd r4 = com.adwo.adsdk.C0024e.a(r1)     // Catch:{ Exception -> 0x0101 }
            com.adwo.adsdk.ErrorCode r1 = r4.err     // Catch:{ Exception -> 0x0101 }
            if (r1 != 0) goto L_0x00e7
            java.lang.String r1 = "Adwo SDK"
            java.lang.String r5 = "Get a full screen ad from Adwo servers."
            android.util.Log.d(r1, r5)     // Catch:{ Exception -> 0x0101 }
            java.util.HashMap r1 = com.adwo.adsdk.K.aa     // Catch:{ Exception -> 0x0101 }
            int r5 = r4.adid     // Catch:{ Exception -> 0x0101 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0101 }
            boolean r1 = r1.containsKey(r5)     // Catch:{ Exception -> 0x0101 }
            if (r1 == 0) goto L_0x0111
            java.util.HashMap r1 = com.adwo.adsdk.K.aa     // Catch:{ Exception -> 0x0101 }
            int r5 = r4.adid     // Catch:{ Exception -> 0x0101 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0101 }
            java.lang.Object r1 = r1.get(r5)     // Catch:{ Exception -> 0x0101 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0101 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0101 }
            int r1 = r1 + 1
            java.util.HashMap r5 = com.adwo.adsdk.K.aa     // Catch:{ Exception -> 0x0101 }
            int r6 = r4.adid     // Catch:{ Exception -> 0x0101 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0101 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0101 }
            r5.put(r6, r1)     // Catch:{ Exception -> 0x0101 }
        L_0x00e7:
            r1 = r0
            r0 = r4
        L_0x00e9:
            if (r2 == 0) goto L_0x00ee
            r2.close()     // Catch:{ Exception -> 0x00fa }
        L_0x00ee:
            if (r3 == 0) goto L_0x00f3
            r3.close()     // Catch:{ Exception -> 0x00fa }
        L_0x00f3:
            if (r1 == 0) goto L_0x0013
            r1.disconnect()     // Catch:{ Exception -> 0x00fa }
            goto L_0x0013
        L_0x00fa:
            r1 = move-exception
            goto L_0x0013
        L_0x00fd:
            r1.write(r4)     // Catch:{ Exception -> 0x0101 }
            goto L_0x009a
        L_0x0101:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0105:
            r0.printStackTrace()     // Catch:{ all -> 0x0122 }
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r4 = "Could not get an ad from Adwo servers,Network Error!"
            android.util.Log.w(r0, r4)     // Catch:{ all -> 0x0122 }
            r0 = r8
            goto L_0x00e9
        L_0x0111:
            java.util.HashMap r1 = com.adwo.adsdk.K.aa     // Catch:{ Exception -> 0x0101 }
            int r5 = r4.adid     // Catch:{ Exception -> 0x0101 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0101 }
            r6 = 1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0101 }
            r1.put(r5, r6)     // Catch:{ Exception -> 0x0101 }
            goto L_0x00e7
        L_0x0122:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x0125:
            java.lang.String r1 = "Adwo SDK"
            java.lang.String r4 = "Could not get an ad from Adwo servers."
            android.util.Log.w(r1, r4)     // Catch:{ Exception -> 0x0101 }
            r1 = r0
            r0 = r8
            goto L_0x00e9
        L_0x012f:
            r0 = move-exception
            r1 = r8
            r2 = r8
            r3 = r8
            goto L_0x0105
        L_0x0134:
            r1 = move-exception
            r2 = r8
            r3 = r8
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0105
        L_0x013b:
            r1 = move-exception
            r2 = r8
            r12 = r0
            r0 = r1
            r1 = r12
            goto L_0x0105
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.K.b(android.content.Context, byte):com.adwo.adsdk.FullScreenAd");
    }

    private static byte[] a(boolean z2, boolean z3, double d2, double d3, byte b2, int i2) {
        byte[] bArr;
        byte[] bArr2 = null;
        if (C0017ao.b != null) {
            try {
                bArr2 = C0017ao.b.getBytes(e.f);
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            b = bArr2;
            bArr = bArr2;
        } else {
            bArr = b;
        }
        return C0024e.a((byte) 0, t, s, r, Z, Y, z2, bArr, c, q, W, (short) y, (short) x, (short) z, (short) A, l, K, d, e, f, g, n, p, z3, d2, d3, X, w, v, aa, i2);
    }

    private static String e(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.packageName;
            }
        } catch (Exception e2) {
        }
        return null;
    }

    protected static String a(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString("Adwo_PID");
            }
            return null;
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Could not read 'Adwo_PID meta-data' from the AndroidManifest.xml.");
            return null;
        }
    }

    protected static int b(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null) {
                return packageInfo.versionCode;
            }
            return 0;
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Could not read the versionCode of the package from the AndroidManifest.xml.");
            return 0;
        }
    }

    public static boolean a(String str) {
        return Pattern.compile("[0-9]*").matcher(str).matches();
    }

    /* JADX WARNING: Removed duplicated region for block: B:81:0x0228  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void c(android.content.Context r10) {
        /*
            r5 = 2
            r6 = 16
            r7 = 3
            r3 = 1
            r4 = 0
            java.io.File r0 = new java.io.File
            java.lang.String r1 = "/system/bin/su"
            r0.<init>(r1)
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x001c
            com.adwo.adsdk.K.p = r3
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "The device is rooted."
            android.util.Log.e(r0, r1)
        L_0x001c:
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0067
            com.adwo.adsdk.K.q = r3
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/adwo/"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.adwo.adsdk.K.N = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = com.adwo.adsdk.K.N
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.String r1 = "entryad/"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.adwo.adsdk.K.O = r0
            java.lang.Thread r0 = new java.lang.Thread
            com.adwo.adsdk.L r1 = new com.adwo.adsdk.L
            r1.<init>(r10)
            r0.<init>(r1)
            r0.start()
        L_0x0067:
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.K.V
            if (r0 != 0) goto L_0x022f
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r10.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            com.adwo.adsdk.K.V = r0
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = r10.checkCallingOrSelfPermission(r0)
            r1 = -1
            if (r0 != r1) goto L_0x0083
            java.lang.String r0 = "Cannot make an ad request without READ_PHONE_STATE permissions!  Please add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" /> in the AndroidManifest.xml file."
            c(r0)
        L_0x0083:
            r0 = 0
            java.lang.String r1 = com.adwo.adsdk.K.h
            if (r1 != 0) goto L_0x039e
            android.telephony.TelephonyManager r1 = com.adwo.adsdk.K.V
            if (r1 == 0) goto L_0x039e
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.K.V
            java.lang.String r0 = r0.getDeviceId()
            r1 = r0
        L_0x0093:
            r0 = 0
            android.content.ContentResolver r2 = r10.getContentResolver()     // Catch:{ Exception -> 0x0230 }
            java.lang.String r8 = "android_id"
            java.lang.String r0 = android.provider.Settings.System.getString(r2, r8)     // Catch:{ Exception -> 0x0230 }
            r2 = r0
        L_0x009f:
            r8 = 0
            java.lang.String r0 = "android.permission.ACCESS_WIFI_STATE"
            int r0 = r10.checkCallingOrSelfPermission(r0)
            if (r0 != 0) goto L_0x039b
            java.lang.String r0 = "wifi"
            java.lang.Object r0 = r10.getSystemService(r0)
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0
            android.net.wifi.WifiInfo r0 = r0.getConnectionInfo()
            if (r0 == 0) goto L_0x039b
            java.lang.String r0 = r0.getMacAddress()
            if (r0 == 0) goto L_0x00cc
            java.lang.String r8 = ":"
            java.lang.String r9 = ""
            java.lang.String r0 = r0.replace(r8, r9)     // Catch:{ UnsupportedEncodingException -> 0x038f }
            java.lang.String r8 = "UTF-8"
            byte[] r8 = r0.getBytes(r8)     // Catch:{ UnsupportedEncodingException -> 0x038f }
            com.adwo.adsdk.K.W = r8     // Catch:{ UnsupportedEncodingException -> 0x038f }
        L_0x00cc:
            if (r1 == 0) goto L_0x0234
            if (r2 != 0) goto L_0x00d1
            r2 = r1
        L_0x00d1:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            java.lang.String r1 = "|"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.adwo.adsdk.K.h = r0
        L_0x00ea:
            java.lang.String r0 = com.adwo.adsdk.K.h     // Catch:{ UnsupportedEncodingException -> 0x0275 }
            if (r0 == 0) goto L_0x026b
            java.lang.String r0 = com.adwo.adsdk.K.h     // Catch:{ UnsupportedEncodingException -> 0x0275 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x0275 }
            com.adwo.adsdk.K.c = r0     // Catch:{ UnsupportedEncodingException -> 0x0275 }
        L_0x00f8:
            java.lang.String r0 = com.adwo.adsdk.K.h
            java.lang.String r0 = r0.toUpperCase()
            java.lang.String r0 = com.adwo.adsdk.U.b(r0)
            com.adwo.adsdk.K.i = r0
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0398
            r0 = r3
        L_0x0111:
            android.content.res.Resources r1 = r10.getResources()
            android.content.res.Configuration r1 = r1.getConfiguration()
            java.util.Locale r1 = r1.locale
            java.lang.String r1 = r1.getLanguage()
            java.lang.String r2 = "en"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x027b
            if (r0 == 0) goto L_0x0278
            r3 = 18
        L_0x012b:
            com.adwo.adsdk.K.Y = r3
            java.lang.String r0 = e(r10)
            if (r0 != 0) goto L_0x0137
            java.lang.String r0 = r10.getPackageName()
        L_0x0137:
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x031b }
            com.adwo.adsdk.K.f = r0     // Catch:{ UnsupportedEncodingException -> 0x031b }
        L_0x013f:
            java.lang.String r0 = android.os.Build.VERSION.RELEASE
            if (r0 == 0) goto L_0x017b
            int r1 = r0.length()
            if (r1 < r7) goto L_0x017b
            r1 = 0
            r2 = 1
            java.lang.String r1 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x038c }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x038c }
            byte r1 = (byte) r1     // Catch:{ Exception -> 0x038c }
            com.adwo.adsdk.K.r = r1     // Catch:{ Exception -> 0x038c }
            r1 = 2
            r2 = 3
            java.lang.String r1 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x038c }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x038c }
            byte r1 = (byte) r1     // Catch:{ Exception -> 0x038c }
            int r2 = r0.length()     // Catch:{ Exception -> 0x038c }
            r3 = 5
            if (r2 != r3) goto L_0x0395
            r2 = 4
            r3 = 5
            java.lang.String r0 = r0.substring(r2, r3)     // Catch:{ Exception -> 0x038c }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x038c }
            byte r0 = (byte) r0     // Catch:{ Exception -> 0x038c }
        L_0x0173:
            int r1 = r1 << 4
            r0 = r0 & 255(0xff, float:3.57E-43)
            r0 = r0 | r1
            byte r0 = (byte) r0     // Catch:{ Exception -> 0x038c }
            com.adwo.adsdk.K.Z = r0     // Catch:{ Exception -> 0x038c }
        L_0x017b:
            java.lang.Class<android.os.Build> r0 = android.os.Build.class
            java.lang.String r1 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0389 }
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ Exception -> 0x0389 }
            java.lang.String r2 = "UTF-8"
            byte[] r1 = r1.getBytes(r2)     // Catch:{ Exception -> 0x0389 }
            com.adwo.adsdk.K.e = r1     // Catch:{ Exception -> 0x0389 }
            java.lang.String r1 = "MANUFACTURER"
            java.lang.reflect.Field r0 = r0.getField(r1)     // Catch:{ Exception -> 0x0389 }
            android.os.Build r1 = new android.os.Build     // Catch:{ Exception -> 0x0389 }
            r1.<init>()     // Catch:{ Exception -> 0x0389 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x0389 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0389 }
            if (r0 == 0) goto L_0x01b0
            int r1 = r0.length()     // Catch:{ Exception -> 0x0389 }
            if (r1 <= 0) goto L_0x01b0
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x0389 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ Exception -> 0x0389 }
            com.adwo.adsdk.K.d = r0     // Catch:{ Exception -> 0x0389 }
        L_0x01b0:
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.K.V     // Catch:{ Exception -> 0x0323 }
            java.lang.String r0 = r0.getNetworkOperator()     // Catch:{ Exception -> 0x0323 }
            r1 = 0
            r2 = 3
            java.lang.String r1 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0323 }
            r2 = 3
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x0323 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0323 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x0323 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0323 }
            java.lang.String r1 = "_"
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0323 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0323 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0323 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ Exception -> 0x0323 }
            com.adwo.adsdk.K.g = r0     // Catch:{ Exception -> 0x0323 }
        L_0x01e0:
            f(r10)
            r0 = 0
            java.lang.String r7 = "0000000000000000"
            java.lang.String r2 = com.adwo.adsdk.K.h     // Catch:{ NumberFormatException -> 0x0329 }
            if (r2 == 0) goto L_0x0392
            java.lang.String r2 = com.adwo.adsdk.K.h     // Catch:{ NumberFormatException -> 0x0329 }
            int r3 = r2.length()     // Catch:{ NumberFormatException -> 0x0329 }
            r2 = 8
            if (r3 <= r2) goto L_0x0210
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x0387 }
            java.lang.String r8 = com.adwo.adsdk.K.h     // Catch:{ NumberFormatException -> 0x0387 }
            int r9 = r3 + -7
            java.lang.String r8 = r8.substring(r9)     // Catch:{ NumberFormatException -> 0x0387 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ NumberFormatException -> 0x0387 }
            r2.<init>(r8)     // Catch:{ NumberFormatException -> 0x0387 }
            java.lang.String r8 = "00000000"
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ NumberFormatException -> 0x0387 }
            java.lang.String r7 = r2.toString()     // Catch:{ NumberFormatException -> 0x0387 }
        L_0x0210:
            java.lang.String r2 = r7.toUpperCase()     // Catch:{ NumberFormatException -> 0x0387 }
            r8 = 16
            long r0 = java.lang.Long.parseLong(r2, r8)     // Catch:{ NumberFormatException -> 0x0387 }
        L_0x021a:
            com.adwo.adsdk.AdwoKey r2 = com.adwo.adsdk.K.B
            java.lang.String r0 = r2.a(r0)
            com.adwo.adsdk.K.X = r0
            int r0 = r0.length()
            if (r0 >= r6) goto L_0x022c
            int r0 = 16 - r0
        L_0x022a:
            if (r4 < r0) goto L_0x036d
        L_0x022c:
            com.adwo.adsdk.U.b(r10)
        L_0x022f:
            return
        L_0x0230:
            r2 = move-exception
            r2 = r0
            goto L_0x009f
        L_0x0234:
            if (r0 == 0) goto L_0x0254
            if (r2 != 0) goto L_0x0239
            r2 = r0
        L_0x0239:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r1.<init>(r0)
            java.lang.String r0 = "|"
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.adwo.adsdk.K.h = r0
            goto L_0x00ea
        L_0x0254:
            if (r2 != 0) goto L_0x0258
            java.lang.String r2 = "9774d56d682e549c"
        L_0x0258:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "0|"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.adwo.adsdk.K.h = r0
            goto L_0x00ea
        L_0x026b:
            java.lang.String r0 = "000000000000000|1a9ef48939761a75"
            byte[] r0 = r0.getBytes()     // Catch:{ UnsupportedEncodingException -> 0x0275 }
            com.adwo.adsdk.K.c = r0     // Catch:{ UnsupportedEncodingException -> 0x0275 }
            goto L_0x00f8
        L_0x0275:
            r0 = move-exception
            goto L_0x00f8
        L_0x0278:
            r3 = r5
            goto L_0x012b
        L_0x027b:
            java.lang.String r2 = "zh"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x028b
            if (r0 == 0) goto L_0x0288
            r3 = r6
            goto L_0x012b
        L_0x0288:
            r3 = r4
            goto L_0x012b
        L_0x028b:
            java.lang.String r2 = "ko"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x029c
            if (r0 == 0) goto L_0x0299
            r3 = 21
            goto L_0x012b
        L_0x0299:
            r3 = 5
            goto L_0x012b
        L_0x029c:
            java.lang.String r2 = "fr"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x02ad
            if (r0 == 0) goto L_0x02aa
            r3 = 19
            goto L_0x012b
        L_0x02aa:
            r3 = r7
            goto L_0x012b
        L_0x02ad:
            java.lang.String r2 = "es"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x02bf
            if (r0 == 0) goto L_0x02bb
            r3 = 24
            goto L_0x012b
        L_0x02bb:
            r3 = 8
            goto L_0x012b
        L_0x02bf:
            java.lang.String r2 = "de"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x02d0
            if (r0 == 0) goto L_0x02cd
            r3 = 22
            goto L_0x012b
        L_0x02cd:
            r3 = 6
            goto L_0x012b
        L_0x02d0:
            java.lang.String r2 = "it"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x02e1
            if (r0 == 0) goto L_0x02de
            r3 = 23
            goto L_0x012b
        L_0x02de:
            r3 = 7
            goto L_0x012b
        L_0x02e1:
            java.lang.String r2 = "ja"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x02f2
            if (r0 == 0) goto L_0x02ef
            r3 = 20
            goto L_0x012b
        L_0x02ef:
            r3 = 4
            goto L_0x012b
        L_0x02f2:
            java.lang.String r2 = "ru"
            boolean r2 = r1.contains(r2)
            if (r2 == 0) goto L_0x0304
            if (r0 == 0) goto L_0x0300
            r3 = 25
            goto L_0x012b
        L_0x0300:
            r3 = 9
            goto L_0x012b
        L_0x0304:
            java.lang.String r2 = "pt"
            boolean r1 = r1.contains(r2)
            if (r1 == 0) goto L_0x0312
            if (r0 == 0) goto L_0x012b
            r3 = 17
            goto L_0x012b
        L_0x0312:
            if (r0 == 0) goto L_0x0318
            r3 = 18
            goto L_0x012b
        L_0x0318:
            r3 = r5
            goto L_0x012b
        L_0x031b:
            r0 = move-exception
            java.lang.String r0 = "Package Name ERROR:  Incorrect application pakage name.  "
            c(r0)
            goto L_0x013f
        L_0x0323:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01e0
        L_0x0329:
            r2 = move-exception
            r3 = r6
        L_0x032b:
            r2.printStackTrace()
            java.lang.String r2 = com.adwo.adsdk.K.h
            java.lang.String r5 = r7.substring(r4, r5)
            java.lang.String r7 = "00"
            java.lang.String r2 = r2.replace(r5, r7)
            com.adwo.adsdk.K.h = r2
            java.lang.String r2 = com.adwo.adsdk.K.h     // Catch:{ Exception -> 0x0384 }
            java.lang.String r5 = "UTF-8"
            byte[] r2 = r2.getBytes(r5)     // Catch:{ Exception -> 0x0384 }
            com.adwo.adsdk.K.c = r2     // Catch:{ Exception -> 0x0384 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0384 }
            java.lang.String r5 = com.adwo.adsdk.K.h     // Catch:{ Exception -> 0x0384 }
            int r3 = r3 + -7
            java.lang.String r3 = r5.substring(r3)     // Catch:{ Exception -> 0x0384 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0384 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0384 }
            java.lang.String r3 = "00000000"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0384 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0384 }
            java.lang.String r2 = r2.toUpperCase()     // Catch:{ Exception -> 0x0384 }
            r3 = 16
            long r0 = java.lang.Long.parseLong(r2, r3)     // Catch:{ Exception -> 0x0384 }
            goto L_0x021a
        L_0x036d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "0"
            r1.<init>(r2)
            java.lang.String r2 = com.adwo.adsdk.K.X
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.adwo.adsdk.K.X = r1
            int r4 = r4 + 1
            goto L_0x022a
        L_0x0384:
            r2 = move-exception
            goto L_0x021a
        L_0x0387:
            r2 = move-exception
            goto L_0x032b
        L_0x0389:
            r0 = move-exception
            goto L_0x01b0
        L_0x038c:
            r0 = move-exception
            goto L_0x017b
        L_0x038f:
            r8 = move-exception
            goto L_0x00cc
        L_0x0392:
            r3 = r6
            goto L_0x0210
        L_0x0395:
            r0 = r4
            goto L_0x0173
        L_0x0398:
            r0 = r4
            goto L_0x0111
        L_0x039b:
            r0 = r8
            goto L_0x00cc
        L_0x039e:
            r1 = r0
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.K.c(android.content.Context):void");
    }

    private static void c(String str) {
        Log.e("Adwo SDK", str);
    }

    protected static void b(String str) {
        if (str == null || str.length() != 32) {
            c("CONFIGURATION ERROR:  Incorrect Adwo publisher ID. It should be composed of 32 [a-z,0-9] characters:  " + b);
        }
        Log.d("Adwo SDK", "Adwo PID is " + str);
        try {
            b = str.getBytes(e.f);
        } catch (UnsupportedEncodingException e2) {
            c("CONFIGURATION ERROR:  Incorrect Adwo publisher ID. It should be composed of 32 [a-z,0-9] characters:  " + b);
        }
    }

    protected static void a(boolean z2) {
        ac = z2;
    }

    protected static void b(boolean z2) {
        ab = z2;
    }

    private static void f(Context context) {
        if (P) {
            try {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                if ("wifi".equalsIgnoreCase(activeNetworkInfo.getTypeName().toLowerCase())) {
                    n = 1;
                } else {
                    n = 0;
                }
                if (activeNetworkInfo == null) {
                    m = "UNKNOWN";
                    return;
                }
                int type = activeNetworkInfo.getType();
                int subtype = activeNetworkInfo.getSubtype();
                if (type == 1) {
                    m = "WIFI";
                }
                if (type == 6) {
                    m = "WIMAX";
                }
                if (type == 0) {
                    switch (subtype) {
                        case 1:
                            m = "GPRS";
                            return;
                        case 2:
                            m = "EDGE";
                            return;
                        case 3:
                            m = "UMTS";
                            return;
                        case 4:
                            m = "CDMA";
                            return;
                        case 5:
                            m = "EVDO_0";
                            return;
                        case AnimationType.TRANSLATE_FROM_RIGHT /*6*/:
                            m = "EVDO_A";
                            return;
                        case AnimationType.TRANSLATE_FROM_LEFT /*7*/:
                            m = "1xRTT";
                            return;
                        case 8:
                            m = "HSDPA";
                            return;
                        case 9:
                            m = "HSUPA";
                            return;
                        case 10:
                            m = "HSPA";
                            return;
                        case 11:
                            m = "IDEN";
                            return;
                        case 12:
                            m = "EVDO_B";
                            return;
                        case 13:
                            m = "LTE";
                            return;
                        case 14:
                            m = "EHRPD";
                            return;
                        case 15:
                            m = "HSPAP";
                            return;
                        default:
                            m = "MOBILE";
                            return;
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    protected static void d(Context context) {
        Location location;
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        List<String> providers = locationManager.getProviders(true);
        int size = providers.size() - 1;
        Location location2 = null;
        while (true) {
            if (size < 0) {
                location = location2;
                break;
            }
            String str = providers.get(size);
            if (locationManager.isProviderEnabled(str)) {
                location = locationManager.getLastKnownLocation(str);
                if (location != null) {
                    break;
                }
            } else {
                location = location2;
            }
            size--;
            location2 = location;
        }
        double[] dArr = location != null ? new double[]{location.getLatitude(), location.getLongitude(), (double) location.getAccuracy(), (double) location.getTime()} : null;
        if (dArr != null) {
            Location location3 = new Location("last_known_loc");
            location3.setLatitude(dArr[0]);
            location3.setLongitude(dArr[1]);
            location3.setAccuracy((float) dArr[2]);
            location3.setTime((long) dArr[3]);
            ae = location3;
        }
    }

    private static double[] d() {
        if (ae == null) {
            return null;
        }
        return new double[]{ae.getLatitude(), ae.getLongitude()};
    }

    protected static boolean a() {
        long currentTimeMillis = System.currentTimeMillis();
        if (af == 0) {
            af = currentTimeMillis;
            return false;
        } else if (currentTimeMillis - af < 15000) {
            Log.e("Adwo SDK", "Requests have been too frequent, Pleae make sure just one request after another within 15 seconds.");
            return true;
        } else {
            af = currentTimeMillis;
            return false;
        }
    }

    protected static boolean b() {
        long currentTimeMillis = System.currentTimeMillis();
        if (ag == 0) {
            ag = currentTimeMillis;
            return false;
        } else if (currentTimeMillis - ag < ((long) (R * 1000))) {
            Log.e("Adwo SDK", "Requests have been too frequent, Pleae make sure just one request after another within 20 seconds.");
            return true;
        } else {
            ag = currentTimeMillis;
            return false;
        }
    }
}
