package com.adwo.adsdk;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.MailTo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.Toast;
import igudi.com.ergushi.AnimationType;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

final class U {
    protected static int a = 15000;
    protected static int b = -1;
    protected static String c = "http://r2.adwo.com/adweb4";
    protected static String d = "http://r2.adwo.com/adfs3";
    static Handler e = new Handler();
    static LinkedList f = new LinkedList();
    private static Intent g = null;
    private static String h = null;
    private static final String[][] i = {new String[]{".3gp", "video/3gpp"}, new String[]{".apk", "application/vnd.android.package-archive"}, new String[]{".asf", "video/x-ms-asf"}, new String[]{".avi", "video/x-msvideo"}, new String[]{".bmp", "image/bmp"}, new String[]{".gif", "image/gif"}, new String[]{".htm", "text/html"}, new String[]{".html", "text/html"}, new String[]{".jpeg", "image/jpeg"}, new String[]{".jpg", "image/jpeg"}, new String[]{".m4v", "video/x-m4v"}, new String[]{".mov", "video/quicktime"}, new String[]{".mp2", "audio/x-mpeg"}, new String[]{".mp3", "audio/x-mpeg"}, new String[]{".mp4", "video/mp4"}, new String[]{".mpe", "video/mpeg"}, new String[]{".mpeg", "video/mpeg"}, new String[]{".mpg", "video/mpeg"}, new String[]{".mpg4", "video/mp4"}, new String[]{".mpga", "audio/mpeg"}, new String[]{".ogg", "audio/ogg"}, new String[]{".png", "image/png"}, new String[]{".rmvb", "audio/x-pn-realaudio"}, new String[]{".wav", "audio/x-wav"}, new String[]{".wma", "audio/x-ms-wma"}, new String[]{".wmv", "audio/x-ms-wmv"}, new String[]{"", "*/*"}};
    private static volatile boolean j = false;

    U() {
    }

    static {
        int[] iArr = {216, 320, 480, 640, 720};
        int[] iArr2 = {38, 50, 80, 100, 110};
    }

    private static String d(String str) {
        String lowerCase;
        String str2 = "*/*";
        int lastIndexOf = str.lastIndexOf(".");
        if (lastIndexOf >= 0 && (lowerCase = str.substring(lastIndexOf, str.length()).toLowerCase()) != "") {
            for (int i2 = 0; i2 < i.length; i2++) {
                if (lowerCase.equals(i[i2][0])) {
                    str2 = i[i2][1];
                }
            }
        }
        return str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0091 A[SYNTHETIC, Splitter:B:23:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0096 A[Catch:{ IOException -> 0x00a1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(java.lang.String r7, android.app.Activity r8) {
        /*
            r1 = 0
            java.lang.String r0 = "/"
            int r0 = r7.lastIndexOf(r0)
            int r0 = r0 + 1
            java.lang.String r0 = r7.substring(r0)
            java.lang.String r2 = android.os.Environment.getExternalStorageState()
            java.lang.String r3 = "mounted"
            boolean r3 = r2.equals(r3)
            if (r3 != 0) goto L_0x002e
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "ExternalStorageState = "
            r1.<init>(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
        L_0x002d:
            return
        L_0x002e:
            com.adwo.adsdk.V r2 = new com.adwo.adsdk.V
            r2.<init>(r8, r0)
            r8.runOnUiThread(r2)
            boolean r2 = com.adwo.adsdk.U.j
            if (r2 != 0) goto L_0x002d
            r2 = 1
            com.adwo.adsdk.U.j = r2
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient
            r4.<init>()
            org.apache.http.client.methods.HttpGet r2 = new org.apache.http.client.methods.HttpGet
            r2.<init>(r7)
            org.apache.http.HttpResponse r2 = r4.execute(r2)     // Catch:{ IOException -> 0x00d1 }
            org.apache.http.StatusLine r3 = r2.getStatusLine()     // Catch:{ IOException -> 0x00d1 }
            int r3 = r3.getStatusCode()     // Catch:{ IOException -> 0x00d1 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r3 != r5) goto L_0x00d4
            org.apache.http.HttpEntity r2 = r2.getEntity()     // Catch:{ IOException -> 0x00d1 }
            java.io.InputStream r3 = r2.getContent()     // Catch:{ IOException -> 0x00d1 }
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x008a }
            java.lang.String r5 = com.adwo.adsdk.K.N     // Catch:{ IOException -> 0x008a }
            r2.<init>(r5)     // Catch:{ IOException -> 0x008a }
            boolean r5 = r2.exists()     // Catch:{ IOException -> 0x008a }
            if (r5 != 0) goto L_0x006f
            r2.mkdir()     // Catch:{ IOException -> 0x008a }
        L_0x006f:
            java.io.File r5 = new java.io.File     // Catch:{ IOException -> 0x008a }
            java.lang.String r2 = com.adwo.adsdk.K.N     // Catch:{ IOException -> 0x008a }
            r5.<init>(r2, r0)     // Catch:{ IOException -> 0x008a }
            boolean r2 = r5.exists()     // Catch:{ IOException -> 0x008a }
            if (r2 != 0) goto L_0x00a3
            boolean r2 = r5.createNewFile()     // Catch:{ IOException -> 0x008a }
            if (r2 != 0) goto L_0x00a3
            org.apache.http.conn.ClientConnectionManager r0 = r4.getConnectionManager()     // Catch:{ IOException -> 0x008a }
            r0.shutdown()     // Catch:{ IOException -> 0x008a }
            goto L_0x002d
        L_0x008a:
            r0 = move-exception
            r2 = r3
        L_0x008c:
            r0.printStackTrace()
        L_0x008f:
            if (r2 == 0) goto L_0x0094
            r2.close()     // Catch:{ IOException -> 0x00a1 }
        L_0x0094:
            if (r1 == 0) goto L_0x0099
            r1.close()     // Catch:{ IOException -> 0x00a1 }
        L_0x0099:
            org.apache.http.conn.ClientConnectionManager r0 = r4.getConnectionManager()     // Catch:{ IOException -> 0x00a1 }
            r0.shutdown()     // Catch:{ IOException -> 0x00a1 }
            goto L_0x002d
        L_0x00a1:
            r0 = move-exception
            goto L_0x002d
        L_0x00a3:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x008a }
            r2.<init>(r5)     // Catch:{ IOException -> 0x008a }
        L_0x00a8:
            int r1 = r3.read()     // Catch:{ IOException -> 0x00cd }
            r6 = -1
            if (r1 != r6) goto L_0x00c9
            r3.close()     // Catch:{ IOException -> 0x00cd }
            r2.close()     // Catch:{ IOException -> 0x00cd }
            java.lang.String r1 = r5.getAbsolutePath()     // Catch:{ IOException -> 0x00cd }
            com.adwo.adsdk.W r5 = new com.adwo.adsdk.W     // Catch:{ IOException -> 0x00cd }
            r5.<init>(r8, r0, r1)     // Catch:{ IOException -> 0x00cd }
            r8.runOnUiThread(r5)     // Catch:{ IOException -> 0x00cd }
            r1 = r2
            r2 = r3
        L_0x00c3:
            r0 = 0
            com.adwo.adsdk.U.j = r0     // Catch:{ IOException -> 0x00c7 }
            goto L_0x008f
        L_0x00c7:
            r0 = move-exception
            goto L_0x008c
        L_0x00c9:
            r2.write(r1)     // Catch:{ IOException -> 0x00cd }
            goto L_0x00a8
        L_0x00cd:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x008c
        L_0x00d1:
            r0 = move-exception
            r2 = r1
            goto L_0x008c
        L_0x00d4:
            r2 = r1
            goto L_0x00c3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.U.a(java.lang.String, android.app.Activity):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x00a6 A[SYNTHETIC, Splitter:B:11:0x00a6] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(android.content.Context r8, int r9) {
        /*
            r2 = 0
            java.lang.String r0 = r8.getPackageName()     // Catch:{ Exception -> 0x00af }
            android.content.pm.PackageManager r1 = r8.getPackageManager()     // Catch:{ Exception -> 0x00af }
            r3 = 0
            android.content.pm.PackageInfo r0 = r1.getPackageInfo(r0, r3)     // Catch:{ Exception -> 0x00af }
            java.lang.String r1 = r0.packageName     // Catch:{ Exception -> 0x00af }
            java.lang.String r0 = r0.versionName     // Catch:{ Exception -> 0x00af }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00af }
            java.lang.String r4 = "http://static.adwo.com/tr/dl"
            r3.<init>(r4)     // Catch:{ Exception -> 0x00af }
            java.lang.String r4 = "?app="
            java.lang.StringBuffer r4 = r3.append(r4)     // Catch:{ Exception -> 0x00af }
            java.lang.String r5 = "UTF-8"
            java.lang.String r1 = java.net.URLEncoder.encode(r1, r5)     // Catch:{ Exception -> 0x00af }
            r4.append(r1)     // Catch:{ Exception -> 0x00af }
            java.lang.String r1 = "&ownid="
            java.lang.StringBuffer r1 = r3.append(r1)     // Catch:{ Exception -> 0x00af }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x00af }
            byte[] r5 = com.adwo.adsdk.K.c     // Catch:{ Exception -> 0x00af }
            java.lang.String r6 = "UTF-8"
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x00af }
            r1.append(r4)     // Catch:{ Exception -> 0x00af }
            java.lang.String r1 = "&version="
            java.lang.StringBuffer r1 = r3.append(r1)     // Catch:{ Exception -> 0x00af }
            java.lang.String r4 = "UTF-8"
            java.lang.String r0 = java.net.URLEncoder.encode(r0, r4)     // Catch:{ Exception -> 0x00af }
            r1.append(r0)     // Catch:{ Exception -> 0x00af }
            java.lang.String r0 = "&pid="
            java.lang.StringBuffer r0 = r3.append(r0)     // Catch:{ Exception -> 0x00af }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x00af }
            byte[] r4 = com.adwo.adsdk.K.b     // Catch:{ Exception -> 0x00af }
            java.lang.String r5 = "UTF-8"
            r1.<init>(r4, r5)     // Catch:{ Exception -> 0x00af }
            r0.append(r1)     // Catch:{ Exception -> 0x00af }
            java.lang.String r0 = "&adid="
            java.lang.StringBuffer r0 = r3.append(r0)     // Catch:{ Exception -> 0x00af }
            r0.append(r9)     // Catch:{ Exception -> 0x00af }
            byte r0 = com.adwo.adsdk.K.s     // Catch:{ Exception -> 0x00af }
            byte r1 = com.adwo.adsdk.K.t     // Catch:{ Exception -> 0x00af }
            short r0 = com.adwo.adsdk.C0024e.a(r0, r1)     // Catch:{ Exception -> 0x00af }
            java.lang.String r1 = "&sdv="
            java.lang.StringBuffer r1 = r3.append(r1)     // Catch:{ Exception -> 0x00af }
            r1.append(r0)     // Catch:{ Exception -> 0x00af }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x00af }
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x00af }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00af }
            java.net.URLConnection r0 = r1.openConnection()     // Catch:{ Exception -> 0x00af }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00af }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00b9 }
            int r1 = com.adwo.adsdk.U.a     // Catch:{ Exception -> 0x00b9 }
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00b9 }
            r0.connect()     // Catch:{ Exception -> 0x00b9 }
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x00b9 }
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00bf }
            r3 = 200(0xc8, float:2.8E-43)
            if (r1 != r3) goto L_0x00a4
            java.lang.String r1 = "Adwo SDK"
            java.lang.String r3 = "download tracking"
            android.util.Log.v(r1, r3)     // Catch:{ Exception -> 0x00bf }
        L_0x00a4:
            if (r2 == 0) goto L_0x00a9
            r2.close()     // Catch:{ IOException -> 0x00b7 }
        L_0x00a9:
            if (r0 == 0) goto L_0x00ae
            r0.disconnect()
        L_0x00ae:
            return
        L_0x00af:
            r0 = move-exception
            r1 = r2
        L_0x00b1:
            r0.printStackTrace()
            r0 = r2
            r2 = r1
            goto L_0x00a4
        L_0x00b7:
            r1 = move-exception
            goto L_0x00a9
        L_0x00b9:
            r1 = move-exception
            r7 = r1
            r1 = r2
            r2 = r0
            r0 = r7
            goto L_0x00b1
        L_0x00bf:
            r1 = move-exception
            r7 = r1
            r1 = r2
            r2 = r0
            r0 = r7
            goto L_0x00b1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.U.a(android.content.Context, int):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b A[SYNTHETIC, Splitter:B:11:0x003b] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(java.lang.String r6) {
        /*
            r2 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0044 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x0044 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0044 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0044 }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x004e }
            int r1 = com.adwo.adsdk.U.a     // Catch:{ Exception -> 0x004e }
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x004e }
            r0.connect()     // Catch:{ Exception -> 0x004e }
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x004e }
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0054 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r1 != r3) goto L_0x0039
            java.lang.String r1 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0054 }
            java.lang.String r4 = "Connect successfully to beacon: "
            r3.<init>(r4)     // Catch:{ Exception -> 0x0054 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0054 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0054 }
            android.util.Log.v(r1, r3)     // Catch:{ Exception -> 0x0054 }
        L_0x0039:
            if (r2 == 0) goto L_0x003e
            r2.close()     // Catch:{ IOException -> 0x004c }
        L_0x003e:
            if (r0 == 0) goto L_0x0043
            r0.disconnect()
        L_0x0043:
            return
        L_0x0044:
            r0 = move-exception
            r1 = r2
        L_0x0046:
            r0.printStackTrace()
            r0 = r2
            r2 = r1
            goto L_0x0039
        L_0x004c:
            r1 = move-exception
            goto L_0x003e
        L_0x004e:
            r1 = move-exception
            r5 = r1
            r1 = r2
            r2 = r0
            r0 = r5
            goto L_0x0046
        L_0x0054:
            r1 = move-exception
            r5 = r1
            r1 = r2
            r2 = r0
            r0 = r5
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.U.a(java.lang.String):void");
    }

    protected static void a(String str, String str2, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ADWO_STATISTIC", 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    protected static void a(String str, int i2, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ADWO_AD_COUNT", 0).edit();
        edit.putInt(str, i2);
        edit.commit();
    }

    protected static int a(Context context, String str) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("ADWO_AD_COUNT", 0);
        if (sharedPreferences.contains(str)) {
            return sharedPreferences.getInt(str, 1);
        }
        return 0;
    }

    protected static boolean a(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ADWO_AD_COUNT", 0).edit();
        edit.clear();
        return edit.commit();
    }

    protected static String b(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & 255;
                if (b3 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e2) {
            return null;
        }
    }

    protected static void b(Context context, String str) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse(str), d(str));
        intent.addFlags(268435456);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e2) {
            Log.e("Adwo SDK", "Could not intent to " + str, e2);
        }
    }

    protected static void c(Context context, String str) {
        String str2;
        String str3;
        String str4 = null;
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.SENDTO");
            String substring = str.substring(str.indexOf(":") + 1);
            if (substring.contains("?")) {
                String substring2 = substring.substring(substring.indexOf("?") + 1);
                str2 = substring.substring(0, substring.indexOf("?"));
                str3 = null;
                for (String str5 : substring2.split("&")) {
                    if (str5.contains("subject")) {
                        str4 = str5.substring(str5.indexOf("=") + 1);
                    } else if (str5.contains("body")) {
                        str3 = str5.substring(str5.indexOf("=") + 1);
                    }
                }
            } else {
                str2 = substring;
                str3 = null;
            }
            intent.setData(Uri.parse("smsto:" + str2));
            intent.putExtra("sms_body", str3);
            intent.putExtra("android.intent.extra.SUBJECT", str4);
            intent.addFlags(268435456);
            context.startActivity(intent);
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Could not intent to " + str, e2);
        }
    }

    protected static void d(Context context, String str) {
        try {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.android.browser", "com.android.browser.BrowserActivity"));
            intent.setAction("android.intent.action.VIEW");
            intent.setData(Uri.parse(str));
            intent.addFlags(268435456);
            context.startActivity(intent);
        } catch (NullPointerException e2) {
            Log.e("Adwo SDK", "Could not intent to " + str, e2);
        }
    }

    protected static void e(Context context, String str) {
        try {
            Intent intent = new Intent();
            MailTo parse = MailTo.parse(str);
            intent.setAction("android.intent.action.SEND");
            intent.setData(Uri.parse(str));
            intent.putExtra("android.intent.extra.EMAIL", new String[]{parse.getTo()});
            intent.putExtra("android.intent.extra.CC", parse.getCc());
            intent.putExtra("android.intent.extra.TEXT", parse.getBody());
            intent.putExtra("android.intent.extra.SUBJECT", parse.getSubject());
            intent.setType("message/rfc882");
            context.startActivity(Intent.createChooser(intent, "Select Email Application:"));
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Could not intent to " + str, e2);
        }
    }

    protected static void f(Context context, String str) {
        try {
            context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(str)));
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Could not intent to " + str, e2);
        }
    }

    protected static void b(Context context) {
        Map<String, ?> all = context.getSharedPreferences("ADWO_STATISTIC", 0).getAll();
        SharedPreferences.Editor edit = context.getSharedPreferences("ADWO_STATISTIC", 0).edit();
        edit.clear();
        edit.commit();
        new Thread(new X(all)).start();
    }

    protected static byte[] a(double d2) {
        long doubleToLongBits = Double.doubleToLongBits(d2);
        byte[] bArr = new byte[8];
        bArr[7] = (byte) ((int) ((doubleToLongBits >> 56) & 255));
        bArr[6] = (byte) ((int) ((doubleToLongBits >> 48) & 255));
        bArr[5] = (byte) ((int) ((doubleToLongBits >> 40) & 255));
        bArr[4] = (byte) ((int) ((doubleToLongBits >> 32) & 255));
        bArr[3] = (byte) ((int) ((doubleToLongBits >> 24) & 255));
        bArr[2] = (byte) ((int) ((doubleToLongBits >> 16) & 255));
        bArr[1] = (byte) ((int) ((doubleToLongBits >> 8) & 255));
        bArr[0] = (byte) ((int) (doubleToLongBits & 255));
        return bArr;
    }

    protected static byte[] a(int i2) {
        byte[] bArr = new byte[4];
        bArr[3] = (byte) (i2 >>> 24);
        bArr[2] = (byte) (i2 >>> 16);
        bArr[1] = (byte) (i2 >>> 8);
        bArr[0] = (byte) i2;
        return bArr;
    }

    protected static byte[] a(short s) {
        return new byte[]{(byte) (s & 255), (byte) ((65280 & s) >> 8)};
    }

    protected static void g(Context context, String str) {
        try {
            PackageManager packageManager = context.getPackageManager();
            if (str != null) {
                Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
                if (launchIntentForPackage != null) {
                    context.startActivity(launchIntentForPackage);
                    Log.e("Adwo SDK", "Intent to packageName.");
                    return;
                }
                Log.e("Adwo SDK", "Get the intent failed.");
            }
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Could not intent to destination.", e2);
        }
    }

    protected static void a(WebView webView, int i2) {
        if (K.j == null) {
            K.j = "javascript:function adwoDoGetShowClickMonitorLinkParams(){ var adwoParamSet = new Object();adwoParamSet.uid ='" + K.i + "'; adwoParamSet.cid='" + ((int) K.s) + "';adwoParamSet.uidtype=2;adwoParamSet.ts=" + System.currentTimeMillis() + ";";
        }
        webView.loadUrl(String.valueOf(K.j) + "adwoParamSet.adid=" + i2 + "; return adwoParamSet;};");
        String str = "000_000";
        if (K.g != null) {
            str = new String(K.g);
        }
        if (K.k == null) {
            K.k = "javascript:function adwoDoGetSDKAttributes(){ var obj = new Object();obj.platform='Android'; obj.version='4.0';obj.systemVersion='" + ((int) K.r) + "';obj.deviceModel='" + new String(K.e) + "'; obj.nettype='" + K.m + "';obj.mcc_mnc='" + str + "'; obj.macAddr='" + K.h + "';obj.appID='" + new String(K.f) + "';obj.hasSDK='" + ((int) K.q) + "';obj.isRooted='" + ((int) K.p) + "';obj.isValidloc='" + K.E + "';obj.hasCameraPermission='" + K.F + "';obj.hasRecordPermission='" + K.H + "';obj.hasVibratePermission='" + K.G + "'; return obj;};";
        }
        webView.loadUrl(K.k);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x00aa A[SYNTHETIC, Splitter:B:18:0x00aa] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00af A[Catch:{ IOException -> 0x00ca }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b4 A[Catch:{ IOException -> 0x00ca }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static java.lang.String a(java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, java.lang.String r13) {
        /*
            r2 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00cc }
            r0.<init>(r10)     // Catch:{ Exception -> 0x00cc }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00cc }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00cc }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ Exception -> 0x00d1 }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00d1 }
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r1 = "Charset"
            java.lang.String r3 = "utf-8"
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r1 = "content-type"
            java.lang.String r3 = "text/html"
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r1 = "Connection"
            java.lang.String r3 = "Keep-Alive"
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r1 = "adid"
            r0.setRequestProperty(r1, r11)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r1 = "pid"
            r0.setRequestProperty(r1, r12)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r1 = "devid"
            r0.setRequestProperty(r1, r13)     // Catch:{ Exception -> 0x00d1 }
            java.lang.String r1 = "platform"
            java.lang.String r3 = "Android"
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00d1 }
            java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x00d1 }
            java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x00d1 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x00d1 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00d8 }
            r3.<init>(r9)     // Catch:{ Exception -> 0x00d8 }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x00bd }
        L_0x005a:
            int r5 = r3.read(r1)     // Catch:{ Exception -> 0x00bd }
            r6 = -1
            if (r5 != r6) goto L_0x00b8
            r4.flush()     // Catch:{ Exception -> 0x00bd }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x00bd }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00bd }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00bd }
            java.lang.String r7 = "utf-8"
            r6.<init>(r1, r7)     // Catch:{ Exception -> 0x00bd }
            r5.<init>(r6)     // Catch:{ Exception -> 0x00bd }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bd }
            r1.<init>()     // Catch:{ Exception -> 0x00bd }
        L_0x0079:
            java.lang.String r6 = r5.readLine()     // Catch:{ Exception -> 0x00bd }
            if (r6 != 0) goto L_0x00c6
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x00bd }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00bd }
            r5.<init>(r1)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r1 = "mediaUrl"
            java.lang.String r2 = r5.getString(r1)     // Catch:{ Exception -> 0x00bd }
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Exception -> 0x00bd }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bd }
            java.lang.String r6 = "MediaFileUrl--->"
            r5.<init>(r6)     // Catch:{ Exception -> 0x00bd }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ Exception -> 0x00bd }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00bd }
            r1.println(r5)     // Catch:{ Exception -> 0x00bd }
            r4.close()     // Catch:{ Exception -> 0x00bd }
            r3.close()     // Catch:{ Exception -> 0x00bd }
        L_0x00a8:
            if (r4 == 0) goto L_0x00ad
            r4.close()     // Catch:{ IOException -> 0x00ca }
        L_0x00ad:
            if (r3 == 0) goto L_0x00b2
            r3.close()     // Catch:{ IOException -> 0x00ca }
        L_0x00b2:
            if (r0 == 0) goto L_0x00b7
            r0.disconnect()     // Catch:{ IOException -> 0x00ca }
        L_0x00b7:
            return r2
        L_0x00b8:
            r6 = 0
            r4.write(r1, r6, r5)     // Catch:{ Exception -> 0x00bd }
            goto L_0x005a
        L_0x00bd:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00c1:
            r0.printStackTrace()
            r0 = r1
            goto L_0x00a8
        L_0x00c6:
            r1.append(r6)     // Catch:{ Exception -> 0x00bd }
            goto L_0x0079
        L_0x00ca:
            r0 = move-exception
            goto L_0x00b7
        L_0x00cc:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
            goto L_0x00c1
        L_0x00d1:
            r1 = move-exception
            r3 = r2
            r4 = r2
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00c1
        L_0x00d8:
            r1 = move-exception
            r3 = r2
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x00c1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.U.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.lang.String");
    }

    protected static String a(String str, String str2) {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        try {
            HttpResponse execute = defaultHttpClient.execute(new HttpGet(str));
            if (execute.getStatusLine().getStatusCode() == 200) {
                byte[] bArr = new byte[4096];
                try {
                    ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(execute.getEntity().getContent()));
                    String str3 = null;
                    while (true) {
                        ZipEntry nextEntry = zipInputStream.getNextEntry();
                        if (nextEntry == null) {
                            return str3;
                        }
                        str3 = String.valueOf(str2) + File.separator + nextEntry.getName();
                        File file = new File(str3);
                        if (nextEntry.isDirectory()) {
                            file.mkdirs();
                        } else {
                            File parentFile = file.getParentFile();
                            if (!parentFile.exists()) {
                                parentFile.mkdirs();
                            }
                            FileOutputStream fileOutputStream = new FileOutputStream(file);
                            while (true) {
                                int read = zipInputStream.read(bArr);
                                if (read <= 0) {
                                    break;
                                }
                                fileOutputStream.write(bArr, 0, read);
                            }
                            fileOutputStream.close();
                        }
                        zipInputStream.closeEntry();
                    }
                } catch (IOException e2) {
                    Log.e("Adwo SDK", "An error has occurred during unzipping full ad file.");
                    e2.printStackTrace();
                }
            }
        } catch (Exception e3) {
            Log.e("Adwo SDK", "Downloading full ad failed.");
            e3.printStackTrace();
        }
        defaultHttpClient.getConnectionManager().closeExpiredConnections();
        defaultHttpClient.getConnectionManager().shutdown();
        return null;
    }

    protected static void a(FullScreenAd fullScreenAd, String str) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(str));
            objectOutputStream.writeObject(fullScreenAd);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    protected static FullScreenAd c(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            byte[] bArr = new byte[fileInputStream.available()];
            fileInputStream.read(bArr);
            return (FullScreenAd) new ObjectInputStream(new ByteArrayInputStream(bArr)).readObject();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    protected static void a(File file) {
        if (file.isDirectory()) {
            for (File a2 : file.listFiles()) {
                a(a2);
            }
            file.delete();
            return;
        }
        file.delete();
    }

    protected static void a(View view, int i2) {
        switch (i2) {
            case 2:
                TranslateAnimation translateAnimation = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation.setDuration(1000);
                view.setAnimation(translateAnimation);
                return;
            case 3:
                TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation2.setDuration(1000);
                view.setAnimation(translateAnimation2);
                return;
            case 4:
                TranslateAnimation translateAnimation3 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                translateAnimation3.setDuration(1000);
                view.setAnimation(translateAnimation3);
                return;
            case 5:
                TranslateAnimation translateAnimation4 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                translateAnimation4.setDuration(1000);
                view.setAnimation(translateAnimation4);
                return;
            case AnimationType.TRANSLATE_FROM_RIGHT /*6*/:
                ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.5f, 0.0f, 1.5f, 1, 0.5f, 1, 0.5f);
                scaleAnimation.setDuration(1500);
                view.setAnimation(scaleAnimation);
                return;
            case AnimationType.TRANSLATE_FROM_LEFT /*7*/:
                RotateAnimation rotateAnimation = new RotateAnimation(180.0f, 360.0f, 1, 0.5f, 1, 0.5f);
                rotateAnimation.setDuration(1000);
                view.setAnimation(rotateAnimation);
                return;
            case 8:
                TranslateAnimation translateAnimation5 = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                translateAnimation5.setDuration(1000);
                view.setAnimation(translateAnimation5);
                return;
            case 9:
                TranslateAnimation translateAnimation6 = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                translateAnimation6.setDuration(1000);
                view.setAnimation(translateAnimation6);
                return;
            case 10:
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration(1500);
                view.setAnimation(alphaAnimation);
                return;
            case 11:
                TranslateAnimation translateAnimation7 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                translateAnimation7.setDuration(1000);
                view.setAnimation(translateAnimation7);
                return;
            case 12:
                TranslateAnimation translateAnimation8 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                translateAnimation8.setDuration(1000);
                view.setAnimation(translateAnimation8);
                return;
            default:
                return;
        }
    }

    protected static int c(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return 0;
        }
        return 1;
    }

    protected static void a(Context context, String str, String str2) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setPositiveButton("下载更新", new Y(str2, context)).setNegativeButton("取消", new C0003aa()).setTitle("程序更新").setMessage(str);
            builder.create().show();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void a(String str, Context context, int i2, boolean z, short s) {
        if (b == -1 || b != i2) {
            a(str, context, true, i2, z, h, s);
            return;
        }
        try {
            if (g != null) {
                context.startActivity(g);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void a(String str, Context context, boolean z, int i2, boolean z2, String str2, short s) {
        new Thread(new C0004ab(str, z, z2, i2, str2, s, context)).start();
    }

    protected static void b(String str, String str2, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ADWO_DOWNLOAD_TMP_FILE", 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    protected static String h(Context context, String str) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("ADWO_DOWNLOAD_TMP_FILE", 0);
        if (sharedPreferences.contains(str)) {
            return sharedPreferences.getString(str, null);
        }
        return null;
    }

    protected static boolean i(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ADWO_DOWNLOAD_TMP_FILE", 0).edit();
        edit.remove(str);
        return edit.commit();
    }

    protected static boolean d(Context context) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        String a2;
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
            Log.e("Adwo SDK", "Must define permission : android.permission.INTERNET");
            Toast.makeText(context, "Must define permission : android.permission.INTERNET", 0).show();
            z = false;
        } else {
            z = true;
        }
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            Log.e("Adwo SDK", "Must define permission : android.permission.READ_PHONE_STATE");
            Toast.makeText(context, "Must define permission : android.permission.READ_PHONE_STATE", 0).show();
            z2 = false;
        } else {
            z2 = true;
        }
        if (context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == -1) {
            Log.e("Adwo SDK", "Must define permission : android.permission.WRITE_EXTERNAL_STORAGE");
            Toast.makeText(context, "Must define permission : android.permission.WRITE_EXTERNAL_STORAGE", 0).show();
            z3 = false;
        } else {
            z3 = true;
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != -1) {
            K.P = true;
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") != -1) {
            K.E = true;
            a(7, (byte) 1);
        }
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != -1) {
            K.E = true;
            a(6, (byte) 1);
        }
        if (context.checkCallingOrSelfPermission("android.permission.CAMERA") != -1) {
            K.F = true;
            a(5, (byte) 1);
        }
        if (context.checkCallingOrSelfPermission("android.permission.RECORD_AUDIO") != -1) {
            K.H = true;
            a(4, (byte) 1);
        }
        if (context.checkCallingOrSelfPermission("android.permission.VIBRATE") != -1) {
            K.G = true;
            a(3, (byte) 1);
        }
        if (K.E) {
            C0009ag agVar = new C0009ag(context);
            K.Q = agVar;
            agVar.sendEmptyMessageDelayed(0, 30000);
        }
        if (K.b != null || ((a2 = K.a(context)) != null && a2.length() == 32)) {
            z4 = true;
        } else {
            Log.e("Adwo SDK", "Incorrect Adwo_PID. Should 32 [a-z,0-9] characters: " + a2);
            z4 = false;
        }
        if (!z || !z2 || !z3 || !z4) {
            return false;
        }
        return true;
    }

    private static void a(int i2, byte b2) {
        if (i2 <= 7) {
            K.u = (byte) (((byte) (1 << i2)) | K.u);
        }
    }

    protected static boolean j(Context context, String str) {
        int indexOf = str.indexOf("|");
        if (indexOf > 0) {
            String lowerCase = str.substring(0, indexOf).toLowerCase();
            String lowerCase2 = str.substring(indexOf + 1, str.length()).toLowerCase();
            h = lowerCase;
            PackageManager packageManager = context.getPackageManager();
            List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(0);
            if (installedApplications == null || installedApplications.size() == 0) {
                return false;
            }
            ArrayList<ApplicationInfo> arrayList = new ArrayList<>();
            int size = installedApplications.size();
            for (int i2 = 0; i2 < size; i2++) {
                ApplicationInfo applicationInfo = installedApplications.get(i2);
                if ((applicationInfo.flags & 1) == 0) {
                    arrayList.add(applicationInfo);
                }
            }
            for (ApplicationInfo applicationInfo2 : arrayList) {
                String str2 = applicationInfo2.packageName;
                if (str2.toLowerCase().equals(lowerCase)) {
                    try {
                        PackageInfo packageInfo = packageManager.getPackageInfo(str2, 0);
                        if (!(packageInfo.versionName == null ? String.valueOf(packageInfo.versionCode) : packageInfo.versionName).toLowerCase().equals(lowerCase2)) {
                            return false;
                        }
                        g = packageManager.getLaunchIntentForPackage(str2);
                        return true;
                    } catch (PackageManager.NameNotFoundException e2) {
                        return false;
                    }
                }
            }
        }
        return false;
    }
}
