package com.adwo.adsdk;

import android.os.Handler;
import android.os.Message;
import igudi.com.ergushi.AnimationType;

/* renamed from: com.adwo.adsdk.a  reason: case insensitive filesystem */
final class C0002a extends Handler {
    private /* synthetic */ AdDisplayer a;

    C0002a(AdDisplayer adDisplayer) {
        this.a = adDisplayer;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case AnimationType.TRANSLATE_FROM_RIGHT /*6*/:
                if (this.a.g != null) {
                    this.a.h.loadUrl("javascript:adwoFetchAudioPower(" + (((double) this.a.g.c()) / 32767.0d) + ");");
                    this.a.s.sendEmptyMessageDelayed(6, (long) this.a.r);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
