package com.adwo.adsdk;

import android.media.MediaPlayer;

final class aL implements MediaPlayer.OnCompletionListener {
    aL(aK aKVar) {
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }
}
