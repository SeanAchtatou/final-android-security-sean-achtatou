package com.adwo.adsdk;

import android.app.Activity;

/* renamed from: com.adwo.adsdk.c  reason: case insensitive filesystem */
final class C0022c implements Runnable {
    private /* synthetic */ C0021b a;
    private final /* synthetic */ Activity b;

    C0022c(C0021b bVar, Activity activity) {
        this.a = bVar;
        this.b = activity;
    }

    public final void run() {
        this.b.setRequestedOrientation(this.a.a.p);
        this.b.getWindow().clearFlags(1024);
    }
}
