package com.adwo.adsdk;

import android.media.MediaPlayer;
import android.net.Uri;
import java.io.IOException;

final class B extends Thread {
    /* access modifiers changed from: private */
    public /* synthetic */ A a;
    private final /* synthetic */ String b;

    B(A a2, String str) {
        this.a = a2;
        this.b = str;
    }

    public final void run() {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(3);
            mediaPlayer.setOnCompletionListener(new C(this, this.b));
            mediaPlayer.setDataSource(this.a.a.getContext(), Uri.parse(this.b));
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        }
    }
}
