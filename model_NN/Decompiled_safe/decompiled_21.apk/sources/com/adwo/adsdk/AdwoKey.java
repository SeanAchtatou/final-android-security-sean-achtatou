package com.adwo.adsdk;

import android.content.Context;

public class AdwoKey {
    private static AdwoKey a;

    private native String getKey(long j);

    protected static AdwoKey a(Context context) {
        if (a == null) {
            a = new AdwoKey(context);
        }
        return a;
    }

    /* JADX WARN: Type inference failed for: r0v1 */
    /* JADX WARN: Type inference failed for: r0v3, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r0v12 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.InputStream] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0057 A[SYNTHETIC, Splitter:B:15:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005c A[Catch:{ Exception -> 0x0068 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
    private AdwoKey(android.content.Context r6) {
        /*
            r5 = this;
            r2 = 0
            r5.<init>()
            java.io.File r0 = r6.getCacheDir()     // Catch:{ Exception -> 0x0060 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0060 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x0060 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x0060 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0060 }
            java.lang.String r0 = "/"
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0060 }
            java.lang.String r1 = "libadwo40.png"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0060 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0060 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0060 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0060 }
            boolean r1 = r1.exists()     // Catch:{ Exception -> 0x0060 }
            if (r1 != 0) goto L_0x0051
            java.lang.Class r1 = r5.getClass()     // Catch:{ Exception -> 0x0060 }
            java.lang.String r3 = "libadwo.png"
            java.io.InputStream r3 = r1.getResourceAsStream(r3)     // Catch:{ Exception -> 0x0060 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x006d }
            r1.<init>(r0)     // Catch:{ Exception -> 0x006d }
            int r4 = r3.available()     // Catch:{ Exception -> 0x0071 }
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x0071 }
            r3.read(r4)     // Catch:{ Exception -> 0x0071 }
            r1.write(r4)     // Catch:{ Exception -> 0x0071 }
            r3.close()     // Catch:{ Exception -> 0x0071 }
            r1.close()     // Catch:{ Exception -> 0x0074 }
        L_0x0051:
            java.lang.System.load(r0)     // Catch:{ Exception -> 0x0060 }
            r0 = r2
        L_0x0055:
            if (r2 == 0) goto L_0x005a
            r2.close()     // Catch:{ Exception -> 0x0068 }
        L_0x005a:
            if (r0 == 0) goto L_0x005f
            r0.close()     // Catch:{ Exception -> 0x0068 }
        L_0x005f:
            return
        L_0x0060:
            r0 = move-exception
            r1 = r2
        L_0x0062:
            r0.printStackTrace()
            r0 = r2
            r2 = r1
            goto L_0x0055
        L_0x0068:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x005f
        L_0x006d:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0062
        L_0x0071:
            r0 = move-exception
            r2 = r3
            goto L_0x0062
        L_0x0074:
            r0 = move-exception
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.AdwoKey.<init>(android.content.Context):void");
    }

    /* access modifiers changed from: protected */
    public final String a(long j) {
        String key = getKey(j);
        return key.substring(2, key.length()).toUpperCase();
    }
}
