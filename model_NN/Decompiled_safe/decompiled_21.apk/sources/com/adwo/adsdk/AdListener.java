package com.adwo.adsdk;

public interface AdListener {
    void onFailedToReceiveAd(AdwoAdView adwoAdView, ErrorCode errorCode);

    void onReceiveAd(AdwoAdView adwoAdView);
}
