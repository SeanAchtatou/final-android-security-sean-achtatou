package com.adwo.adsdk;

import android.os.AsyncTask;
import java.io.File;

/* renamed from: com.adwo.adsdk.g  reason: case insensitive filesystem */
final class C0026g extends AsyncTask {
    private /* synthetic */ AdDisplayer a;
    private final /* synthetic */ boolean b;
    private final /* synthetic */ String c;

    C0026g(AdDisplayer adDisplayer, boolean z, String str) {
        this.a = adDisplayer;
        this.b = z;
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        File[] listFiles;
        Object[] objArr2 = objArr;
        String a2 = U.a((String) objArr2[0], (String) objArr2[1]);
        if (this.b) {
            FullScreenAd fullScreenAd = (FullScreenAd) objArr2[3];
            String str = String.valueOf((String) objArr2[1]) + ((String) objArr2[2]) + File.separator;
            File file = new File(str);
            if (!file.exists()) {
                file.mkdirs();
            }
            fullScreenAd.showURL = "file://" + str + "index.html?w=" + (((float) this.a.n.widthPixels) / this.a.n.density) + "&h=" + (((float) this.a.n.heightPixels) / this.a.n.density);
            U.a(fullScreenAd, String.valueOf(str) + "object.temp");
            this.a.c = (String) objArr2[1];
            if ((this.a.e == null || !this.a.e.isShowing()) && (listFiles = new File(K.O).listFiles()) != null && listFiles.length > 0) {
                for (File file2 : listFiles) {
                    if (!file2.getAbsolutePath().contains(new StringBuilder(String.valueOf(fullScreenAd.adid)).toString())) {
                        U.a(file2);
                    }
                }
            }
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        String str = (String) obj;
        if (this.b) {
            return;
        }
        if (str != null) {
            this.a.h.clearHistory();
            this.a.h.loadUrl(this.c);
        } else if (this.a.u != null) {
            this.a.u.onFailedToReceiveAd(new ErrorCode(36, "OTHER_ERROR_DOWNLOADING_AD_RESOURCES"));
        }
    }
}
