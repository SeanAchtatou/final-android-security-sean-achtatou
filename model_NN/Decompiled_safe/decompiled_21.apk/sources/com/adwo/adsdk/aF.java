package com.adwo.adsdk;

import android.content.DialogInterface;
import android.webkit.JsResult;

final class aF implements DialogInterface.OnCancelListener {
    private final /* synthetic */ JsResult a;

    aF(aC aCVar, JsResult jsResult) {
        this.a = jsResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.a.cancel();
    }
}
