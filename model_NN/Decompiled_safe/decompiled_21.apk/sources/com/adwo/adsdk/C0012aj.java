package com.adwo.adsdk;

import android.media.MediaRecorder;
import android.webkit.WebView;
import java.io.IOException;

/* renamed from: com.adwo.adsdk.aj  reason: case insensitive filesystem */
public final class C0012aj {
    String a = null;
    String b = "/dev/null";
    WebView c;
    int d;
    private MediaRecorder e;
    private double f = 0.0d;

    public C0012aj(double d2, String str, String str2, WebView webView, int i) {
        this.f = d2;
        this.a = str;
        this.c = webView;
        this.d = i;
        if (str2 != null) {
            this.b = str2;
        }
    }

    public final void a() {
        if (this.e == null) {
            this.e = new MediaRecorder();
        }
        try {
            this.e.setAudioSource(1);
            this.e.setOutputFormat(2);
            this.e.setAudioEncoder(0);
            this.e.setOnErrorListener(new C0013ak(this));
            this.e.setOnInfoListener(new C0014al(this));
            this.e.setOutputFile(this.b);
            this.e.setMaxDuration(((int) this.f) * 1000);
            this.e.prepare();
            this.e.getMaxAmplitude();
            this.e.start();
            if (this.c != null) {
                this.c.loadUrl("javascript:adwoVoiceStartRecording();");
            }
            this.e.getMaxAmplitude();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    public final void b() {
        if (this.e != null) {
            this.e.stop();
            this.e.reset();
            this.e.release();
            this.e = null;
        }
    }

    public final int c() {
        if (this.e != null) {
            return this.e.getMaxAmplitude();
        }
        return 0;
    }
}
