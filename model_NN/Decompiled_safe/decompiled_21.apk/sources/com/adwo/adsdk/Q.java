package com.adwo.adsdk;

import android.content.Context;

final class Q implements Runnable {
    private /* synthetic */ P a;
    private final /* synthetic */ C0045z b;
    private final /* synthetic */ I c;
    private final /* synthetic */ Context d;

    Q(P p, C0045z zVar, I i, Context context) {
        this.a = p;
        this.b = zVar;
        this.c = i;
        this.d = context;
    }

    public final void run() {
        this.a.a.addView(this.b);
        byte b2 = (byte) (this.c.m & 15);
        if (b2 > 1 && b2 < 16) {
            U.a(this.a.a, b2);
        } else if (this.a.a.j != 1) {
            U.a(this.a.a, b2);
        } else {
            int nextInt = this.a.a.k.nextInt(13);
            if (nextInt == 0 || nextInt == 1) {
                nextInt = 2;
            }
            U.a(this.a.a, nextInt);
        }
        if (K.w.containsKey(Integer.valueOf(this.c.a))) {
            K.w.put(Integer.valueOf(this.c.a), Integer.valueOf(((Integer) K.w.get(Integer.valueOf(this.c.a))).intValue() + 1));
        } else {
            K.w.put(Integer.valueOf(this.c.a), 1);
        }
        if (this.c.f != null) {
            new R(this, this.c).start();
        }
        if (this.c.g != null && this.c.g.size() > 0) {
            for (String j : this.c.g) {
                if (U.j(this.d, j)) {
                    U.b = this.c.a;
                }
            }
        }
        try {
            if (this.a.a.c != null) {
                this.a.a.removeView(this.a.a.c);
            }
        } catch (Exception e) {
        }
        this.a.a.c = this.b;
    }
}
