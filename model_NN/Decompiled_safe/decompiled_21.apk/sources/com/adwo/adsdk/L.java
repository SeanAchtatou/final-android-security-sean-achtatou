package com.adwo.adsdk;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.util.Arrays;

final class L implements Runnable {
    private final /* synthetic */ Context a;

    L(Context context) {
        this.a = context;
    }

    public final void run() {
        String h;
        boolean z;
        boolean z2;
        short s;
        File file = new File(K.N);
        if (file.exists()) {
            File[] listFiles = file.listFiles(new M(this));
            int length = listFiles.length;
            if (length > K.ad) {
                Arrays.sort(listFiles, new N());
                int c = length - K.ad;
                for (int i = 0; i < c; i++) {
                    U.a(listFiles[i]);
                }
            }
            for (File absolutePath : file.listFiles()) {
                String absolutePath2 = absolutePath.getAbsolutePath();
                if (absolutePath2.endsWith(".tmp") && (h = U.h(this.a, absolutePath2)) != null) {
                    try {
                        Log.e("Adwo SDK", "Unfinished Url-->" + h);
                        String[] split = h.split(",,,");
                        String str = split[0];
                        if ("0".equals(split[1])) {
                            z = false;
                        } else {
                            z = true;
                        }
                        int parseInt = Integer.parseInt(split[2]);
                        if ("0".equals(split[3])) {
                            z2 = false;
                        } else {
                            z2 = true;
                        }
                        if (!"0".equals(split[5])) {
                            s = Short.parseShort(split[5]);
                        } else {
                            s = 0;
                        }
                        U.a(str, this.a, z, parseInt, z2, split[4], s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return;
        }
        file.mkdirs();
    }
}
