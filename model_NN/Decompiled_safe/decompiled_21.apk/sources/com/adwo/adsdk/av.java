package com.adwo.adsdk;

import android.view.MotionEvent;
import android.view.View;

final class av implements View.OnTouchListener {
    private float a;
    private float b;
    private float c;
    private float d;
    private float e;
    private float f;
    private float g;
    private float h;
    private /* synthetic */ ar i;

    av(ar arVar) {
        this.i = arVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 1:
                if (motionEvent.getPointerCount() < 2) {
                    this.a = -1.0f;
                    this.b = -1.0f;
                    this.c = -1.0f;
                    this.d = -1.0f;
                    break;
                }
                break;
            case 2:
                if (motionEvent.getPointerCount() == 2 && !(this.a == -1.0f && this.c == -1.0f)) {
                    this.e = motionEvent.getX(0);
                    this.f = motionEvent.getY(0);
                    this.g = motionEvent.getX(1);
                    this.h = motionEvent.getY(1);
                    float sqrt = (float) Math.sqrt(Math.pow((double) (this.c - this.a), 2.0d) + Math.pow((double) (this.d - this.b), 2.0d));
                    float sqrt2 = (float) Math.sqrt(Math.pow((double) (this.g - this.e), 2.0d) + Math.pow((double) (this.h - this.f), 2.0d));
                    if (sqrt - sqrt2 >= 25.0f) {
                        this.i.c.zoomOut();
                    } else if (sqrt2 - sqrt >= 25.0f) {
                        this.i.c.zoomIn();
                    }
                    this.a = this.e;
                    this.c = this.g;
                    this.b = this.f;
                    this.d = this.h;
                    break;
                }
            case 261:
                if (motionEvent.getPointerCount() == 2) {
                    this.a = motionEvent.getX(0);
                    this.b = motionEvent.getY(0);
                    this.c = motionEvent.getX(1);
                    this.d = motionEvent.getY(1);
                    break;
                }
                break;
        }
        return false;
    }
}
