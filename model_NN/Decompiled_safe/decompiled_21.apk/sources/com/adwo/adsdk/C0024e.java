package com.adwo.adsdk;

import com.umeng.common.b.e;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.adwo.adsdk.e  reason: case insensitive filesystem */
class C0024e {
    C0024e(AdDisplayer adDisplayer) {
    }

    C0024e() {
    }

    protected static short a(byte b, byte b2) {
        return (short) ((b2 << 8) | (b & 255));
    }

    /* JADX WARN: Type inference failed for: r0v39, types: [int] */
    /* JADX WARN: Type inference failed for: r0v46, types: [int] */
    /* JADX WARN: Type inference failed for: r0v53, types: [int] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, short]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.adwo.adsdk.FullScreenAd a(byte[] r9) {
        /*
            r0 = 26
            r4 = 0
            com.adwo.adsdk.FullScreenAd r1 = new com.adwo.adsdk.FullScreenAd
            r1.<init>()
            int r2 = r9.length
            r3 = 1
            if (r2 != r3) goto L_0x0137
            byte r0 = r9[r4]
            r2 = -29
            if (r0 != r2) goto L_0x0026
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r2 = "Adwo_Pid inexist"
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_INEXIST_PID"
            r0.<init>(r2, r3)
        L_0x0022:
            r1.err = r0
            r0 = r1
        L_0x0025:
            return r0
        L_0x0026:
            byte r0 = r9[r4]
            r2 = -32
            if (r0 != r2) goto L_0x003d
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r2 = "Server busy"
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_SERVERBUSY"
            r0.<init>(r2, r3)
            goto L_0x0022
        L_0x003d:
            byte r0 = r9[r4]
            r2 = -28
            if (r0 != r2) goto L_0x0054
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r2 = "Adwo_Pid inactive"
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_INACTIVATED_PID"
            r0.<init>(r2, r3)
            goto L_0x0022
        L_0x0054:
            byte r0 = r9[r4]
            r2 = -31
            if (r0 == r2) goto L_0x0060
            byte r0 = r9[r4]
            r2 = -24
            if (r0 != r2) goto L_0x0080
        L_0x0060:
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "No ad available: "
            r2.<init>(r3)
            byte r3 = r9[r4]
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_NOAD_IP"
            r0.<init>(r2, r3)
            goto L_0x0022
        L_0x0080:
            byte r0 = r9[r4]
            r2 = -23
            if (r0 != r2) goto L_0x00a7
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "No ad available: "
            r2.<init>(r3)
            byte r3 = r9[r4]
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_NOAD_LOWRANK"
            r0.<init>(r2, r3)
            goto L_0x0022
        L_0x00a7:
            byte r0 = r9[r4]
            r2 = -25
            if (r0 != r2) goto L_0x00ce
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "No ad available: "
            r2.<init>(r3)
            byte r3 = r9[r4]
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_NOAD_IP"
            r0.<init>(r2, r3)
            goto L_0x0022
        L_0x00ce:
            byte r0 = r9[r4]
            r2 = -30
            if (r0 != r2) goto L_0x00e6
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r2 = "Unknown Error"
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_UNKNOWN"
            r0.<init>(r2, r3)
            goto L_0x0022
        L_0x00e6:
            byte r0 = r9[r4]
            r2 = -26
            if (r0 != r2) goto L_0x00fe
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r2 = "Error in receiving data"
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_INCOMM"
            r0.<init>(r2, r3)
            goto L_0x0022
        L_0x00fe:
            byte r0 = r9[r4]
            r2 = -27
            if (r0 != r2) goto L_0x0116
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r2 = "Error in request data"
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_REQDATA"
            r0.<init>(r2, r3)
            goto L_0x0022
        L_0x0116:
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Unknown Error "
            r2.<init>(r3)
            byte r3 = r9[r4]
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r0, r2)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "OTHER_ERROR"
            r0.<init>(r2, r3)
            goto L_0x0022
        L_0x0137:
            int r2 = r9.length
            r3 = 7
            if (r2 >= r3) goto L_0x0150
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r2 = r9[r4]
            java.lang.String r3 = "ERR_INCOMM"
            r0.<init>(r2, r3)
            java.lang.String r2 = "Adwo SDK"
            java.lang.String r3 = "Error in receiving data"
            android.util.Log.e(r2, r3)
            r1.err = r0
            r0 = r1
            goto L_0x0025
        L_0x0150:
            com.adwo.adsdk.FullScreenAd r2 = new com.adwo.adsdk.FullScreenAd
            r2.<init>()
            int r1 = a(r9, r4)
            r2.adid = r1
            java.lang.String r1 = new java.lang.String
            r3 = 4
            r5 = 19
            r1.<init>(r9, r3, r5)
            r2.serverStamp = r1
            r1 = 24
            byte r1 = r9[r1]
            r3 = 25
            byte r3 = r9[r3]
            short r1 = a(r1, r3)
            if (r1 == 0) goto L_0x017c
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r0, r1)
            r2.showURL = r3
            int r0 = r1 + 26
        L_0x017c:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r3 = 70
            if (r0 != r3) goto L_0x0301
            int r3 = r1 + 1
            byte r1 = r9[r1]
            int r0 = r3 + 1
            byte r3 = r9[r3]
            short r1 = a(r1, r3)
            if (r1 == 0) goto L_0x019a
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r0, r1)
            r2.clickChargeURL = r3
            int r0 = r0 + r1
        L_0x019a:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r3 = 69
            if (r0 != r3) goto L_0x02fe
            int r3 = r1 + 1
            byte r1 = r9[r1]
            int r0 = r3 + 1
            byte r3 = r9[r3]
            short r1 = a(r1, r3)
            if (r1 == 0) goto L_0x01b8
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r0, r1)
            r2.showChargeURL = r3
            int r0 = r0 + r1
        L_0x01b8:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r3 = 68
            if (r0 != r3) goto L_0x02fb
            int r0 = r1 + 1
            byte r1 = r9[r1]
            if (r1 == 0) goto L_0x01ce
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r0, r1)
            r2.updateDate = r3
            int r0 = r0 + r1
        L_0x01ce:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r3 = 66
            if (r0 != r3) goto L_0x02f8
            int r0 = r1 + 1
            byte r1 = r9[r1]
            if (r1 == 0) goto L_0x01f8
            int r1 = r0 + 1
            byte r0 = r9[r0]
            int r3 = r1 + 1
            byte r1 = r9[r1]
            short r0 = a(r0, r1)
            r2.width = r0
            int r1 = r3 + 1
            byte r3 = r9[r3]
            int r0 = r1 + 1
            byte r1 = r9[r1]
            short r1 = a(r3, r1)
            r2.height = r1
        L_0x01f8:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r3 = 73
            if (r0 != r3) goto L_0x02f5
            int r1 = r1 + 1
            int r0 = r1 + 1
            byte r1 = r9[r1]
            r2.isInhouseAd = r1
        L_0x0208:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r2.animationType = r0
            int r0 = r1 + 1
            byte r1 = r9[r1]
            r3 = 76
            if (r1 != r3) goto L_0x0227
            int r1 = r0 + 1
            byte r3 = r9[r0]
            int r0 = r1 + 1
            byte r1 = r9[r1]
            short r5 = a(r3, r1)
            if (r5 == 0) goto L_0x0227
            r1 = r4
        L_0x0225:
            if (r1 < r5) goto L_0x022d
        L_0x0227:
            int r1 = r9.length
            if (r0 < r1) goto L_0x024a
            r0 = r2
            goto L_0x0025
        L_0x022d:
            int r3 = r0 + 1
            byte r0 = r9[r0]
            int r6 = r3 + 1
            byte r3 = r9[r3]
            short r0 = a(r0, r3)
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r6, r0)
            java.util.List r7 = r2.beaconUrlList
            r7.add(r3)
            int r3 = r6 + r0
            int r0 = r1 + 1
            r1 = r0
            r0 = r3
            goto L_0x0225
        L_0x024a:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r3 = 83
            if (r0 != r3) goto L_0x02f2
            int r3 = r1 + 1
            byte r1 = r9[r1]
            int r0 = r3 + 1
            byte r3 = r9[r3]
            short r5 = a(r1, r3)
            if (r5 == 0) goto L_0x0263
            r1 = r4
        L_0x0261:
            if (r1 < r5) goto L_0x0269
        L_0x0263:
            int r1 = r9.length
            if (r0 < r1) goto L_0x0286
            r0 = r2
            goto L_0x0025
        L_0x0269:
            int r3 = r0 + 1
            byte r0 = r9[r0]
            int r6 = r3 + 1
            byte r3 = r9[r3]
            short r0 = a(r0, r3)
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r6, r0)
            java.util.List r7 = r2.showBeaconList
            r7.add(r3)
            int r3 = r6 + r0
            int r0 = r1 + 1
            r1 = r0
            r0 = r3
            goto L_0x0261
        L_0x0286:
            int r1 = r0 + 1
            byte r0 = r9[r0]     // Catch:{ Exception -> 0x02e8 }
            r3 = 80
            if (r0 != r3) goto L_0x02f0
            int r3 = r1 + 1
            byte r1 = r9[r1]     // Catch:{ Exception -> 0x02eb }
            int r0 = r3 + 1
            byte r3 = r9[r3]     // Catch:{ Exception -> 0x02ee }
            short r5 = a(r1, r3)     // Catch:{ Exception -> 0x02ee }
            if (r5 == 0) goto L_0x029f
            r3 = r4
        L_0x029d:
            if (r3 < r5) goto L_0x02a5
        L_0x029f:
            int r1 = r9.length
            if (r0 < r1) goto L_0x02d7
            r0 = r2
            goto L_0x0025
        L_0x02a5:
            int r1 = r0 + 1
            byte r4 = r9[r0]     // Catch:{ Exception -> 0x02e8 }
            int r0 = r1 + 1
            byte r1 = r9[r1]     // Catch:{ Exception -> 0x02ee }
            short r1 = a(r4, r1)     // Catch:{ Exception -> 0x02ee }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x02ee }
            java.lang.String r6 = "UTF-8"
            r4.<init>(r9, r0, r1, r6)     // Catch:{ Exception -> 0x02ee }
            java.io.PrintStream r6 = java.lang.System.err     // Catch:{ Exception -> 0x02ee }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ee }
            java.lang.String r8 = "full screen packageName "
            r7.<init>(r8)     // Catch:{ Exception -> 0x02ee }
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ Exception -> 0x02ee }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x02ee }
            r6.println(r7)     // Catch:{ Exception -> 0x02ee }
            java.util.List r6 = r2.packageList     // Catch:{ Exception -> 0x02ee }
            r6.add(r4)     // Catch:{ Exception -> 0x02ee }
            int r1 = r1 + r0
            int r0 = r3 + 1
            r3 = r0
            r0 = r1
            goto L_0x029d
        L_0x02d7:
            int r1 = r0 + 1
            byte r0 = r9[r0]     // Catch:{ Exception -> 0x02e6 }
            byte r1 = r9[r1]     // Catch:{ Exception -> 0x02e6 }
            short r0 = a(r0, r1)     // Catch:{ Exception -> 0x02e6 }
            r2.launchDelay = r0     // Catch:{ Exception -> 0x02e6 }
        L_0x02e3:
            r0 = r2
            goto L_0x0025
        L_0x02e6:
            r0 = move-exception
            goto L_0x02e3
        L_0x02e8:
            r0 = move-exception
            r0 = r1
            goto L_0x029f
        L_0x02eb:
            r0 = move-exception
            r0 = r3
            goto L_0x029f
        L_0x02ee:
            r1 = move-exception
            goto L_0x029f
        L_0x02f0:
            r0 = r1
            goto L_0x029f
        L_0x02f2:
            r0 = r1
            goto L_0x0263
        L_0x02f5:
            r0 = r1
            goto L_0x0208
        L_0x02f8:
            r0 = r1
            goto L_0x01f8
        L_0x02fb:
            r0 = r1
            goto L_0x01ce
        L_0x02fe:
            r0 = r1
            goto L_0x01b8
        L_0x0301:
            r0 = r1
            goto L_0x019a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0024e.a(byte[]):com.adwo.adsdk.FullScreenAd");
    }

    /* JADX WARN: Type inference failed for: r0v34, types: [int] */
    /* JADX WARN: Type inference failed for: r0v53, types: [int] */
    /* JADX WARN: Type inference failed for: r0v60, types: [int] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, short]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.adwo.adsdk.I b(byte[] r9) {
        /*
            r0 = 26
            r1 = 24
            r8 = 1
            r4 = 0
            com.adwo.adsdk.I r2 = new com.adwo.adsdk.I
            r2.<init>()
            int r3 = r9.length
            if (r3 != r8) goto L_0x013a
            byte r0 = r9[r4]
            r1 = -29
            if (r0 != r1) goto L_0x0029
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Adwo_Pid inexist"
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_INEXIST_PID"
            r0.<init>(r1, r3)
        L_0x0024:
            r2.a(r0)
            r0 = r2
        L_0x0028:
            return r0
        L_0x0029:
            byte r0 = r9[r4]
            r1 = -32
            if (r0 != r1) goto L_0x0040
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Server busy"
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_SERVERBUSY"
            r0.<init>(r1, r3)
            goto L_0x0024
        L_0x0040:
            byte r0 = r9[r4]
            r1 = -28
            if (r0 != r1) goto L_0x0057
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Adwo_Pid inactive"
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_INACTIVATED_PID"
            r0.<init>(r1, r3)
            goto L_0x0024
        L_0x0057:
            byte r0 = r9[r4]
            r1 = -31
            if (r0 == r1) goto L_0x0063
            byte r0 = r9[r4]
            r1 = -24
            if (r0 != r1) goto L_0x0083
        L_0x0063:
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "No ad available: "
            r1.<init>(r3)
            byte r3 = r9[r4]
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_NOAD_IP"
            r0.<init>(r1, r3)
            goto L_0x0024
        L_0x0083:
            byte r0 = r9[r4]
            r1 = -23
            if (r0 != r1) goto L_0x00aa
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "No ad available: "
            r1.<init>(r3)
            byte r3 = r9[r4]
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_NOAD_LOWRANK"
            r0.<init>(r1, r3)
            goto L_0x0024
        L_0x00aa:
            byte r0 = r9[r4]
            r1 = -25
            if (r0 != r1) goto L_0x00d1
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "No ad available: "
            r1.<init>(r3)
            byte r3 = r9[r4]
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_NOAD_IP"
            r0.<init>(r1, r3)
            goto L_0x0024
        L_0x00d1:
            byte r0 = r9[r4]
            r1 = -30
            if (r0 != r1) goto L_0x00e9
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Unknown Error"
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_UNKNOWN"
            r0.<init>(r1, r3)
            goto L_0x0024
        L_0x00e9:
            byte r0 = r9[r4]
            r1 = -26
            if (r0 != r1) goto L_0x0101
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Error in receiving data"
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_INCOMM"
            r0.<init>(r1, r3)
            goto L_0x0024
        L_0x0101:
            byte r0 = r9[r4]
            r1 = -27
            if (r0 != r1) goto L_0x0119
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Error in request data"
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_REQDATA"
            r0.<init>(r1, r3)
            goto L_0x0024
        L_0x0119:
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Unknown Error "
            r1.<init>(r3)
            byte r3 = r9[r4]
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "OTHER_ERROR"
            r0.<init>(r1, r3)
            goto L_0x0024
        L_0x013a:
            int r3 = r9.length
            r5 = 7
            if (r3 >= r5) goto L_0x0154
            com.adwo.adsdk.ErrorCode r0 = new com.adwo.adsdk.ErrorCode
            byte r1 = r9[r4]
            java.lang.String r3 = "ERR_INCOMM"
            r0.<init>(r1, r3)
            java.lang.String r1 = "Adwo SDK"
            java.lang.String r3 = "Error in receiving data"
            android.util.Log.e(r1, r3)
            r2.a(r0)
            r0 = r2
            goto L_0x0028
        L_0x0154:
            int r3 = a(r9, r4)
            r2.a = r3
            java.lang.String r3 = new java.lang.String
            r5 = 4
            r6 = 19
            r3.<init>(r9, r5, r6)
            r2.k = r3
            r3 = 23
            byte r3 = r9[r3]
            r5 = 65
            if (r3 != r5) goto L_0x02fa
            byte r1 = r9[r1]
            r3 = 25
            byte r3 = r9[r3]
            short r1 = a(r1, r3)
            if (r1 == 0) goto L_0x0181
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r0, r1)
            r2.d = r3
            int r0 = r1 + 26
        L_0x0181:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r3 = 70
            if (r0 != r3) goto L_0x02f7
            int r3 = r1 + 1
            byte r1 = r9[r1]
            int r0 = r3 + 1
            byte r3 = r9[r3]
            short r1 = a(r1, r3)
            if (r1 == 0) goto L_0x019f
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r0, r1)
            r2.c = r3
            int r0 = r0 + r1
        L_0x019f:
            int r1 = r9.length
            if (r0 < r1) goto L_0x01a5
            r0 = r2
            goto L_0x0028
        L_0x01a5:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r3 = 73
            if (r0 != r3) goto L_0x02f4
            int r0 = r1 + 1
            int r0 = r0 + 1
        L_0x01b1:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r2.m = r0
            int r0 = r1 + 1
            byte r1 = r9[r1]
            r3 = 76
            if (r1 != r3) goto L_0x01d0
            int r1 = r0 + 1
            byte r3 = r9[r0]
            int r0 = r1 + 1
            byte r1 = r9[r1]
            short r5 = a(r3, r1)
            if (r5 == 0) goto L_0x01d0
            r1 = r4
        L_0x01ce:
            if (r1 < r5) goto L_0x01d6
        L_0x01d0:
            int r1 = r9.length
            if (r0 < r1) goto L_0x01f3
            r0 = r2
            goto L_0x0028
        L_0x01d6:
            int r3 = r0 + 1
            byte r0 = r9[r0]
            int r6 = r3 + 1
            byte r3 = r9[r3]
            short r0 = a(r0, r3)
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r6, r0)
            java.util.List r7 = r2.e
            r7.add(r3)
            int r3 = r6 + r0
            int r0 = r1 + 1
            r1 = r0
            r0 = r3
            goto L_0x01ce
        L_0x01f3:
            int r1 = r0 + 1
            byte r0 = r9[r0]
            r3 = 83
            if (r0 != r3) goto L_0x02f1
            int r3 = r1 + 1
            byte r1 = r9[r1]
            int r0 = r3 + 1
            byte r3 = r9[r3]
            short r5 = a(r1, r3)
            if (r5 == 0) goto L_0x020c
            r1 = r4
        L_0x020a:
            if (r1 < r5) goto L_0x0212
        L_0x020c:
            int r1 = r9.length
            if (r0 < r1) goto L_0x022f
            r0 = r2
            goto L_0x0028
        L_0x0212:
            int r3 = r0 + 1
            byte r0 = r9[r0]
            int r6 = r3 + 1
            byte r3 = r9[r3]
            short r0 = a(r0, r3)
            java.lang.String r3 = new java.lang.String
            r3.<init>(r9, r6, r0)
            java.util.List r7 = r2.f
            r7.add(r3)
            int r3 = r6 + r0
            int r0 = r1 + 1
            r1 = r0
            r0 = r3
            goto L_0x020a
        L_0x022f:
            int r1 = r0 + 1
            byte r0 = r9[r0]     // Catch:{ Exception -> 0x02dc }
            r3 = 75
            if (r0 != r3) goto L_0x02ee
            int r3 = r1 + 1
            byte r1 = r9[r1]     // Catch:{ Exception -> 0x02df }
            int r0 = r3 + 1
            byte r3 = r9[r3]     // Catch:{ Exception -> 0x02e2 }
            short r5 = a(r1, r3)     // Catch:{ Exception -> 0x02e2 }
            if (r5 == 0) goto L_0x0261
            byte[] r6 = new byte[r5]     // Catch:{ Exception -> 0x02e5 }
            r3 = r4
        L_0x0248:
            int r1 = r6.length     // Catch:{ Exception -> 0x02e5 }
            if (r3 < r1) goto L_0x0267
            r1 = 2
            if (r5 != r1) goto L_0x0256
            r1 = 1
            byte r1 = r6[r1]     // Catch:{ Exception -> 0x02e5 }
            if (r1 != r8) goto L_0x0256
            r1 = 1
            r2.h = r1     // Catch:{ Exception -> 0x02e5 }
        L_0x0256:
            r1 = 3
            if (r5 != r1) goto L_0x0261
            r1 = 1
            byte r1 = r6[r1]     // Catch:{ Exception -> 0x02e5 }
            if (r1 != 0) goto L_0x0261
            r1 = 0
            r2.h = r1     // Catch:{ Exception -> 0x02e5 }
        L_0x0261:
            int r1 = r9.length
            if (r0 < r1) goto L_0x0272
            r0 = r2
            goto L_0x0028
        L_0x0267:
            int r1 = r0 + 1
            byte r0 = r9[r0]     // Catch:{ Exception -> 0x02e8 }
            r6[r3] = r0     // Catch:{ Exception -> 0x02e8 }
            int r0 = r3 + 1
            r3 = r0
            r0 = r1
            goto L_0x0248
        L_0x0272:
            int r1 = r0 + 1
            byte r0 = r9[r0]     // Catch:{ Exception -> 0x02d4 }
            r3 = 80
            if (r0 != r3) goto L_0x02ec
            int r3 = r1 + 1
            byte r1 = r9[r1]     // Catch:{ Exception -> 0x02d7 }
            int r0 = r3 + 1
            byte r3 = r9[r3]     // Catch:{ Exception -> 0x02da }
            short r5 = a(r1, r3)     // Catch:{ Exception -> 0x02da }
            if (r5 == 0) goto L_0x028b
            r3 = r4
        L_0x0289:
            if (r3 < r5) goto L_0x0291
        L_0x028b:
            int r1 = r9.length
            if (r0 < r1) goto L_0x02c3
            r0 = r2
            goto L_0x0028
        L_0x0291:
            int r1 = r0 + 1
            byte r4 = r9[r0]     // Catch:{ Exception -> 0x02d4 }
            int r0 = r1 + 1
            byte r1 = r9[r1]     // Catch:{ Exception -> 0x02da }
            short r1 = a(r4, r1)     // Catch:{ Exception -> 0x02da }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x02da }
            java.lang.String r6 = "UTF-8"
            r4.<init>(r9, r0, r1, r6)     // Catch:{ Exception -> 0x02da }
            java.io.PrintStream r6 = java.lang.System.out     // Catch:{ Exception -> 0x02da }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02da }
            java.lang.String r8 = "packageName "
            r7.<init>(r8)     // Catch:{ Exception -> 0x02da }
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ Exception -> 0x02da }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x02da }
            r6.println(r7)     // Catch:{ Exception -> 0x02da }
            java.util.List r6 = r2.g     // Catch:{ Exception -> 0x02da }
            r6.add(r4)     // Catch:{ Exception -> 0x02da }
            int r1 = r1 + r0
            int r0 = r3 + 1
            r3 = r0
            r0 = r1
            goto L_0x0289
        L_0x02c3:
            int r1 = r0 + 1
            byte r0 = r9[r0]     // Catch:{ Exception -> 0x02d2 }
            byte r1 = r9[r1]     // Catch:{ Exception -> 0x02d2 }
            short r0 = a(r0, r1)     // Catch:{ Exception -> 0x02d2 }
            r2.l = r0     // Catch:{ Exception -> 0x02d2 }
        L_0x02cf:
            r0 = r2
            goto L_0x0028
        L_0x02d2:
            r0 = move-exception
            goto L_0x02cf
        L_0x02d4:
            r0 = move-exception
            r0 = r1
            goto L_0x028b
        L_0x02d7:
            r0 = move-exception
            r0 = r3
            goto L_0x028b
        L_0x02da:
            r1 = move-exception
            goto L_0x028b
        L_0x02dc:
            r0 = move-exception
            r0 = r1
            goto L_0x0261
        L_0x02df:
            r0 = move-exception
            r0 = r3
            goto L_0x0261
        L_0x02e2:
            r1 = move-exception
            goto L_0x0261
        L_0x02e5:
            r1 = move-exception
            goto L_0x0261
        L_0x02e8:
            r0 = move-exception
            r0 = r1
            goto L_0x0261
        L_0x02ec:
            r0 = r1
            goto L_0x028b
        L_0x02ee:
            r0 = r1
            goto L_0x0261
        L_0x02f1:
            r0 = r1
            goto L_0x020c
        L_0x02f4:
            r0 = r1
            goto L_0x01b1
        L_0x02f7:
            r0 = r1
            goto L_0x019f
        L_0x02fa:
            r0 = r1
            goto L_0x0181
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0024e.b(byte[]):com.adwo.adsdk.I");
    }

    static int a(byte[] bArr, int i) {
        return ((bArr[3] & 255) << 24) + 0 + ((bArr[2] & 255) << 16) + ((bArr[1] & 255) << 8) + ((bArr[0] & 255) << 0);
    }

    static byte[] a(short s) {
        return new byte[]{(byte) (s & 255), (byte) ((65280 & s) >> 8)};
    }

    protected static byte[] a(double d) {
        long doubleToLongBits = Double.doubleToLongBits(d);
        byte[] bArr = new byte[8];
        bArr[7] = (byte) ((int) ((doubleToLongBits >> 56) & 255));
        bArr[6] = (byte) ((int) ((doubleToLongBits >> 48) & 255));
        bArr[5] = (byte) ((int) ((doubleToLongBits >> 40) & 255));
        bArr[4] = (byte) ((int) ((doubleToLongBits >> 32) & 255));
        bArr[3] = (byte) ((int) ((doubleToLongBits >> 24) & 255));
        bArr[2] = (byte) ((int) ((doubleToLongBits >> 16) & 255));
        bArr[1] = (byte) ((int) ((doubleToLongBits >> 8) & 255));
        bArr[0] = (byte) ((int) (doubleToLongBits & 255));
        return bArr;
    }

    protected static byte[] a(int i) {
        byte[] bArr = new byte[4];
        bArr[3] = (byte) (i >>> 24);
        bArr[2] = (byte) (i >>> 16);
        bArr[1] = (byte) (i >>> 8);
        bArr[0] = (byte) i;
        return bArr;
    }

    protected static byte[] a(byte b, byte b2, byte b3, byte b4, byte b5, byte b6, boolean z, byte[] bArr, byte[] bArr2, byte b7, byte[] bArr3, short s, short s2, short s3, short s4, String str, double d, byte[] bArr4, byte[] bArr5, byte[] bArr6, byte[] bArr7, byte b8, byte b9, boolean z2, double d2, double d3, String str2, HashMap hashMap, HashMap hashMap2, HashMap hashMap3, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byteArrayOutputStream.write(new byte[2]);
            byteArrayOutputStream.write(b);
            byteArrayOutputStream.write(b2);
            byteArrayOutputStream.write(b3);
            byteArrayOutputStream.write(b5);
            byteArrayOutputStream.write(b4);
            byteArrayOutputStream.write(K.u);
            byteArrayOutputStream.write(b6);
            if (z) {
                byteArrayOutputStream.write(0);
            } else {
                byteArrayOutputStream.write(1);
            }
            byteArrayOutputStream.write(bArr);
            byteArrayOutputStream.write(AdDisplayer.a);
            byteArrayOutputStream.write(b3);
            byteArrayOutputStream.write(73);
            if (bArr2 != null) {
                byteArrayOutputStream.write((byte) bArr2.length);
                byteArrayOutputStream.write(bArr2);
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(b7);
            byteArrayOutputStream.write(87);
            if (bArr3 != null) {
                byteArrayOutputStream.write((byte) bArr3.length);
                byteArrayOutputStream.write(bArr3);
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(81);
            byteArrayOutputStream.write(4);
            byteArrayOutputStream.write(a(s));
            byteArrayOutputStream.write(a(s2));
            byteArrayOutputStream.write(71);
            byteArrayOutputStream.write(4);
            byteArrayOutputStream.write(a(s3));
            byteArrayOutputStream.write(a(s4));
            byteArrayOutputStream.write(75);
            if (str != null) {
                byte[] bytes = str.getBytes(e.f);
                byteArrayOutputStream.write(a((short) bytes.length));
                byteArrayOutputStream.write(bytes);
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(83);
            byteArrayOutputStream.write(8);
            byteArrayOutputStream.write(a(d));
            byteArrayOutputStream.write(77);
            if (bArr4 != null) {
                byteArrayOutputStream.write((byte) bArr4.length);
                byteArrayOutputStream.write(bArr4);
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(65);
            if (bArr5 != null) {
                byteArrayOutputStream.write((byte) bArr5.length);
                byteArrayOutputStream.write(bArr5);
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(80);
            if (bArr6 != null) {
                byteArrayOutputStream.write((byte) bArr6.length);
                byteArrayOutputStream.write(bArr6);
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(67);
            if (bArr7 != null) {
                byteArrayOutputStream.write((byte) bArr7.length);
                byteArrayOutputStream.write(bArr7);
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(84);
            byteArrayOutputStream.write(1);
            byteArrayOutputStream.write(b8);
            byteArrayOutputStream.write(86);
            if (K.m != null) {
                byte[] bytes2 = K.m.getBytes(e.f);
                byteArrayOutputStream.write((byte) bytes2.length);
                byteArrayOutputStream.write(bytes2);
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(74);
            byteArrayOutputStream.write(1);
            byteArrayOutputStream.write(b9);
            byteArrayOutputStream.write(76);
            if (z2) {
                byteArrayOutputStream.write(16);
                byteArrayOutputStream.write(a(d2));
                byteArrayOutputStream.write(a(d3));
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(70);
            byteArrayOutputStream.write(1);
            byteArrayOutputStream.write(AdDisplayer.b);
            byteArrayOutputStream.write(88);
            byteArrayOutputStream.write(1);
            byteArrayOutputStream.write(K.T);
            byteArrayOutputStream.write(72);
            byteArrayOutputStream.write(4);
            byteArrayOutputStream.write(a(i));
            byteArrayOutputStream.write(82);
            if (str2 != null) {
                byte[] bytes3 = str2.getBytes(e.f);
                byteArrayOutputStream.write((byte) bytes3.length);
                byteArrayOutputStream.write(bytes3);
            } else {
                byteArrayOutputStream.write(0);
            }
            byteArrayOutputStream.write(66);
            byteArrayOutputStream.write(a(hashMap2.size()));
            for (Map.Entry entry : hashMap2.entrySet()) {
                int intValue = ((Integer) entry.getKey()).intValue();
                int intValue2 = ((Integer) entry.getValue()).intValue();
                byteArrayOutputStream.write(a(intValue));
                byteArrayOutputStream.write(a(intValue2));
            }
            byteArrayOutputStream.write(69);
            byteArrayOutputStream.write(a(hashMap3.size()));
            for (Map.Entry entry2 : hashMap3.entrySet()) {
                int intValue3 = ((Integer) entry2.getKey()).intValue();
                int intValue4 = ((Integer) entry2.getValue()).intValue();
                byteArrayOutputStream.write(a(intValue3));
                byteArrayOutputStream.write(a(intValue4));
            }
            byteArrayOutputStream.write(68);
            byteArrayOutputStream.write(a(hashMap.size()));
            for (Map.Entry entry3 : hashMap.entrySet()) {
                int intValue5 = ((Integer) entry3.getKey()).intValue();
                int intValue6 = ((Integer) entry3.getValue()).intValue();
                byteArrayOutputStream.write(a(intValue5));
                byteArrayOutputStream.write(a(intValue6));
            }
            byteArrayOutputStream.write(a(U.b));
            byteArrayOutputStream.write(90);
            short length = (short) K.D.length;
            byteArrayOutputStream.write(a(length));
            for (short s5 = 0; s5 < length; s5 = (short) (s5 + 1)) {
                byteArrayOutputStream.write(a(K.D[s5]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        short length2 = (short) (byteArray.length - 2);
        byteArray[1] = (byte) ((65280 & length2) >> 8);
        byteArray[0] = (byte) (length2 & 255);
        return byteArray;
    }
}
