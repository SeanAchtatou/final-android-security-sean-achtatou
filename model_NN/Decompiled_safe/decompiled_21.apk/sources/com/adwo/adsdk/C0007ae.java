package com.adwo.adsdk;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

/* renamed from: com.adwo.adsdk.ae  reason: case insensitive filesystem */
final class C0007ae implements Runnable {
    private final /* synthetic */ Context a;
    private final /* synthetic */ String b;

    C0007ae(C0004ab abVar, Context context, String str) {
        this.a = context;
        this.b = str;
    }

    public final void run() {
        Intent launchIntentForPackage;
        PackageManager packageManager = this.a.getPackageManager();
        if (this.b != null && (launchIntentForPackage = packageManager.getLaunchIntentForPackage(this.b)) != null) {
            this.a.startActivity(launchIntentForPackage);
        }
    }
}
