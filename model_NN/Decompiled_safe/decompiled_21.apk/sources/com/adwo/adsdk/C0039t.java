package com.adwo.adsdk;

import android.media.MediaPlayer;

/* renamed from: com.adwo.adsdk.t  reason: case insensitive filesystem */
final class C0039t implements MediaPlayer.OnCompletionListener {
    C0039t(C0038s sVar) {
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }
}
