package com.adwo.adsdk;

import android.content.Context;

/* renamed from: com.adwo.adsdk.ab  reason: case insensitive filesystem */
final class C0004ab implements Runnable {
    private final /* synthetic */ String a;
    private final /* synthetic */ boolean b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ int d;
    private final /* synthetic */ String e;
    private final /* synthetic */ short f;
    private final /* synthetic */ Context g;

    C0004ab(String str, boolean z, boolean z2, int i, String str2, short s, Context context) {
        this.a = str;
        this.b = z;
        this.c = z2;
        this.d = i;
        this.e = str2;
        this.f = s;
        this.g = context;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:127:0x0565, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0566, code lost:
        r6 = r5;
        r5 = r12;
        r26 = r2;
        r2 = r3;
        r3 = r26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x056f, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0570, code lost:
        r3.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0575, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x0576, code lost:
        r10 = r2;
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x033e, code lost:
        if (r6 != r9) goto L_0x0412;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0340, code lost:
        if (r4 == null) goto L_0x034b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0346, code lost:
        if (r4.isValid() == false) goto L_0x034b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        r4.release();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0565 A[Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }, ExcHandler: SocketTimeoutException (r3v64 'e' java.net.SocketTimeoutException A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:58:0x0333] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0575 A[ExcHandler: MalformedURLException (r3v63 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:58:0x0333] */
    /* JADX WARNING: Removed duplicated region for block: B:166:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x041b A[SYNTHETIC, Splitter:B:81:0x041b] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0420 A[Catch:{ IOException -> 0x0586 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0430 A[Catch:{ IOException -> 0x0586 }] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x043d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r27 = this;
            r0 = r27
            java.lang.String r2 = r0.a
            java.lang.String r3 = "/"
            int r2 = r2.lastIndexOf(r3)
            r0 = r27
            java.lang.String r3 = r0.a
            int r2 = r2 + 1
            java.lang.String r16 = r3.substring(r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = com.adwo.adsdk.K.N
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r2.<init>(r3)
            r0 = r16
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r17 = r2.toString()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r17)
            r2.<init>(r3)
            java.lang.String r3 = ".tmp"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r18 = r2.toString()
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            boolean r2 = r2.contains(r0)
            if (r2 == 0) goto L_0x005d
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "下载列表中已经存在正在下载中--->"
            r3.<init>(r4)
            r0 = r18
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
        L_0x005c:
            return
        L_0x005d:
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            r2.add(r0)
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "添加到下载列表中--->"
            r3.<init>(r4)
            r0 = r18
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
            java.lang.String r2 = "1"
            r0 = r27
            boolean r3 = r0.b
            if (r3 != 0) goto L_0x0084
            java.lang.String r2 = "0"
        L_0x0084:
            java.lang.String r3 = "1"
            r0 = r27
            boolean r4 = r0.c
            if (r4 != 0) goto L_0x008e
            java.lang.String r3 = "0"
        L_0x008e:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r0 = r27
            java.lang.String r5 = r0.a
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r4.<init>(r5)
            java.lang.String r5 = ",,,"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = ",,,"
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r27
            int r4 = r0.d
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ",,,"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ",,,"
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r27
            java.lang.String r3 = r0.e
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ",,,"
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r27
            short r3 = r0.f
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0 = r27
            android.content.Context r3 = r0.g
            r0 = r18
            com.adwo.adsdk.U.b(r0, r2, r3)
            java.lang.String r2 = android.os.Environment.getExternalStorageState()
            java.lang.String r3 = "mounted"
            boolean r3 = r2.equals(r3)
            if (r3 != 0) goto L_0x013f
            java.lang.String r3 = "Adwo SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "ExternalStorageState = "
            r4.<init>(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r3, r2)
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            boolean r2 = r2.contains(r0)
            if (r2 == 0) goto L_0x012d
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            r2.remove(r0)
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "从下载列表中删除--->"
            r3.<init>(r4)
            r0 = r18
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
        L_0x012d:
            android.os.Handler r2 = com.adwo.adsdk.U.e
            com.adwo.adsdk.ac r3 = new com.adwo.adsdk.ac
            r0 = r27
            android.content.Context r4 = r0.g
            r0 = r27
            r3.<init>(r0, r4)
            r2.post(r3)
            goto L_0x005c
        L_0x013f:
            java.io.File r2 = new java.io.File
            java.lang.String r3 = com.adwo.adsdk.K.N
            r2.<init>(r3)
            boolean r3 = r2.exists()
            if (r3 != 0) goto L_0x014f
            r2.mkdirs()
        L_0x014f:
            r3 = 0
            r2 = 1
            org.apache.http.impl.client.DefaultHttpClient r5 = new org.apache.http.impl.client.DefaultHttpClient
            r5.<init>()
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet
            r0 = r27
            java.lang.String r6 = r0.a
            r4.<init>(r6)
            org.apache.http.HttpResponse r4 = r5.execute(r4)     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            org.apache.http.StatusLine r6 = r4.getStatusLine()     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            int r6 = r6.getStatusCode()     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            r7 = 200(0xc8, float:2.8E-43)
            if (r6 == r7) goto L_0x01b3
            r2 = 0
            r9 = r3
        L_0x0171:
            org.apache.http.conn.ClientConnectionManager r3 = r5.getConnectionManager()
            r3.closeExpiredConnections()
            org.apache.http.conn.ClientConnectionManager r3 = r5.getConnectionManager()
            r3.shutdown()
            if (r2 == 0) goto L_0x0183
            if (r9 >= 0) goto L_0x01e2
        L_0x0183:
            java.lang.String r2 = "Adwo SDK"
            java.lang.String r3 = "Failed to get the file'size."
            android.util.Log.e(r2, r3)
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            boolean r2 = r2.contains(r0)
            if (r2 == 0) goto L_0x005c
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            r2.remove(r0)
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "从下载列表中删除--->"
            r3.<init>(r4)
            r0 = r18
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
            goto L_0x005c
        L_0x01b3:
            org.apache.http.HttpEntity r4 = r4.getEntity()     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            long r3 = r4.getContentLength()     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            int r3 = (int) r3     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            java.lang.String r4 = "Adwo SDK"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            java.lang.String r7 = "The file's size is "
            r6.<init>(r7)     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            java.lang.StringBuilder r6 = r6.append(r3)     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            java.lang.String r6 = r6.toString()     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            android.util.Log.e(r4, r6)     // Catch:{ ClientProtocolException -> 0x01d2, IOException -> 0x01da }
            r9 = r3
            goto L_0x0171
        L_0x01d2:
            r2 = move-exception
            r4 = r2
            r2 = 0
            r4.printStackTrace()
            r9 = r3
            goto L_0x0171
        L_0x01da:
            r2 = move-exception
            r4 = r2
            r2 = 0
            r4.printStackTrace()
            r9 = r3
            goto L_0x0171
        L_0x01e2:
            java.io.File r19 = new java.io.File
            r0 = r19
            r1 = r17
            r0.<init>(r1)
            boolean r2 = r19.exists()
            if (r2 == 0) goto L_0x021c
            long r2 = r19.length()
            long r4 = (long) r9
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x045c
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "上次下载不完整，重新下载--->"
            r3.<init>(r4)
            r0 = r18
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
            java.io.File r2 = new java.io.File
            r0 = r18
            r2.<init>(r0)
            r0 = r19
            r0.renameTo(r2)
        L_0x021c:
            java.lang.String r2 = "Adwo SDK"
            java.lang.String r3 = "Ready to download."
            android.util.Log.e(r2, r3)
            android.os.Handler r2 = com.adwo.adsdk.U.e
            com.adwo.adsdk.ad r3 = new com.adwo.adsdk.ad
            r0 = r27
            android.content.Context r4 = r0.g
            r0 = r27
            r1 = r16
            r3.<init>(r0, r4, r1)
            r2.post(r3)
            r0 = r27
            android.content.Context r2 = r0.g
            java.lang.String r3 = "notification"
            java.lang.Object r2 = r2.getSystemService(r3)
            r8 = r2
            android.app.NotificationManager r8 = (android.app.NotificationManager) r8
            android.app.Notification r20 = new android.app.Notification
            r2 = 17301633(0x1080081, float:2.4979616E-38)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r16)
            r3.<init>(r4)
            java.lang.String r4 = "下载中"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            long r4 = java.lang.System.currentTimeMillis()
            r0 = r20
            r0.<init>(r2, r3, r4)
            r2 = 2
            r0 = r20
            r0.flags = r2
            r2 = 16
            r0 = r20
            r0.flags = r2
            android.widget.RemoteViews r21 = new android.widget.RemoteViews
            r0 = r27
            android.content.Context r2 = r0.g
            java.lang.String r2 = r2.getPackageName()
            r3 = 17367040(0x1090000, float:2.5162926E-38)
            r0 = r21
            r0.<init>(r2, r3)
            r2 = 16908294(0x1020006, float:2.3877246E-38)
            r3 = 17301633(0x1080081, float:2.4979616E-38)
            r0 = r21
            r0.setImageViewResource(r2, r3)
            r0 = r27
            android.content.Context r2 = r0.g
            r3 = 0
            android.content.Intent r4 = new android.content.Intent
            r4.<init>()
            r5 = 0
            android.app.PendingIntent r22 = android.app.PendingIntent.getActivity(r2, r3, r4, r5)
            r13 = 0
            r3 = 0
            r11 = 0
            r10 = 0
            java.io.File r23 = new java.io.File
            r0 = r23
            r1 = r18
            r0.<init>(r1)
            r4 = 0
            boolean r2 = r23.exists()
            if (r2 == 0) goto L_0x05be
            long r4 = r23.length()
            r14 = r4
        L_0x02b3:
            java.io.RandomAccessFile r12 = new java.io.RandomAccessFile     // Catch:{ SocketTimeoutException -> 0x05aa, MalformedURLException -> 0x0599, IOException -> 0x057d }
            java.lang.String r2 = "rw"
            r0 = r23
            r12.<init>(r0, r2)     // Catch:{ SocketTimeoutException -> 0x05aa, MalformedURLException -> 0x0599, IOException -> 0x057d }
            java.nio.channels.FileChannel r2 = r12.getChannel()     // Catch:{ SocketTimeoutException -> 0x05b1, MalformedURLException -> 0x059e, IOException -> 0x0589 }
            r3 = 0
            r5 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r7 = 0
            java.nio.channels.FileLock r4 = r2.lock(r3, r5, r7)     // Catch:{ SocketTimeoutException -> 0x05b1, MalformedURLException -> 0x059e, IOException -> 0x0589 }
            java.net.URL r2 = new java.net.URL     // Catch:{ SocketTimeoutException -> 0x05b8, MalformedURLException -> 0x05a2, IOException -> 0x058d }
            r0 = r27
            java.lang.String r3 = r0.a     // Catch:{ SocketTimeoutException -> 0x05b8, MalformedURLException -> 0x05a2, IOException -> 0x058d }
            r2.<init>(r3)     // Catch:{ SocketTimeoutException -> 0x05b8, MalformedURLException -> 0x05a2, IOException -> 0x058d }
            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ SocketTimeoutException -> 0x05b8, MalformedURLException -> 0x05a2, IOException -> 0x058d }
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ SocketTimeoutException -> 0x05b8, MalformedURLException -> 0x05a2, IOException -> 0x058d }
            r3 = 40000(0x9c40, float:5.6052E-41)
            r2.setConnectTimeout(r3)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            r3 = 120000(0x1d4c0, float:1.68156E-40)
            r2.setReadTimeout(r3)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            r3 = 1
            r2.setAllowUserInteraction(r3)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.lang.String r3 = "Range"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.lang.String r6 = "bytes="
            r5.<init>(r6)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.lang.StringBuilder r5 = r5.append(r14)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.lang.String r6 = "-"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.lang.String r5 = r5.toString()     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            r2.setRequestProperty(r3, r5)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            int r3 = r2.getResponseCode()     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            r5 = 206(0xce, float:2.89E-43)
            if (r3 != r5) goto L_0x04b3
            java.lang.String r3 = "Adwo SDK"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.lang.String r6 = "断点下载--->"
            r5.<init>(r6)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.lang.StringBuilder r5 = r5.append(r14)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.lang.String r5 = r5.toString()     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            android.util.Log.e(r3, r5)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            r12.seek(r14)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
        L_0x0328:
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            java.io.InputStream r3 = r2.getInputStream()     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            r5.<init>(r3)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            r3 = 10240(0x2800, float:1.4349E-41)
            byte[] r10 = new byte[r3]     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            int r6 = (int) r14     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r3 = -1
        L_0x0337:
            int r7 = r5.read(r10)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r11 = -1
            if (r7 != r11) goto L_0x04d7
            if (r6 != r9) goto L_0x0412
            if (r4 == 0) goto L_0x034b
            boolean r3 = r4.isValid()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            if (r3 == 0) goto L_0x034b
            r4.release()     // Catch:{ IOException -> 0x056f, SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575 }
        L_0x034b:
            java.io.File r3 = new java.io.File     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r17
            r3.<init>(r0)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r23
            r0.renameTo(r3)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            android.net.Uri r3 = android.net.Uri.fromFile(r3)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            android.content.Intent r6 = new android.content.Intent     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r6.<init>()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r7 = "android.intent.action.VIEW"
            r6.setAction(r7)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r7 = "application/vnd.android.package-archive"
            r6.setDataAndType(r3, r7)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            android.content.Context r3 = r0.g     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r7 = 0
            r10 = 0
            android.app.PendingIntent r3 = android.app.PendingIntent.getActivity(r3, r7, r6, r10)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r20
            r0.contentIntent = r3     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r3 = 16908294(0x1020006, float:2.3877246E-38)
            r0 = r20
            r8.notify(r3, r0)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r3 = "Adwo SDK"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r8 = " fileSize->"
            r7.<init>(r8)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r7 = r7.toString()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            android.util.Log.e(r3, r7)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r3 = "Adwo SDK"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r8 = r23.getAbsolutePath()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r7.<init>(r8)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r8 = " 下载完成--->"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r8 = r19.getAbsolutePath()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.String r7 = r7.toString()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            android.util.Log.e(r3, r7)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            boolean r3 = r0.b     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            if (r3 == 0) goto L_0x03e6
            r0 = r27
            android.content.Context r3 = r0.g     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r3.startActivity(r6)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            short r3 = r0.f     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            if (r3 <= 0) goto L_0x03e6
            android.os.Handler r3 = com.adwo.adsdk.U.e     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            com.adwo.adsdk.ae r6 = new com.adwo.adsdk.ae     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            android.content.Context r7 = r0.g     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            java.lang.String r8 = r0.e     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            r6.<init>(r0, r7, r8)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            short r7 = r0.f     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            int r7 = r7 * 1000
            long r7 = (long) r7     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r3.postDelayed(r6, r7)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
        L_0x03e6:
            r0 = r27
            boolean r3 = r0.c     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            if (r3 == 0) goto L_0x03f7
            r0 = r27
            android.content.Context r3 = r0.g     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            int r6 = r0.d     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            com.adwo.adsdk.U.a(r3, r6)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
        L_0x03f7:
            r0 = r27
            android.content.Context r3 = r0.g     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r18
            com.adwo.adsdk.U.i(r3, r0)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            android.os.Handler r3 = com.adwo.adsdk.U.e     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            com.adwo.adsdk.af r6 = new com.adwo.adsdk.af     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            android.content.Context r7 = r0.g     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r0 = r27
            r1 = r16
            r6.<init>(r0, r7, r1)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r3.post(r6)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
        L_0x0412:
            r5.close()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r12.close()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r10 = r2
        L_0x0419:
            if (r5 == 0) goto L_0x041e
            r5.close()     // Catch:{ IOException -> 0x0586 }
        L_0x041e:
            if (r12 == 0) goto L_0x0423
            r12.close()     // Catch:{ IOException -> 0x0586 }
        L_0x0423:
            if (r4 == 0) goto L_0x042e
            boolean r2 = r4.isValid()     // Catch:{ IOException -> 0x0586 }
            if (r2 == 0) goto L_0x042e
            r4.release()     // Catch:{ IOException -> 0x0586 }
        L_0x042e:
            if (r10 == 0) goto L_0x0433
            r10.disconnect()     // Catch:{ IOException -> 0x0586 }
        L_0x0433:
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            boolean r2 = r2.contains(r0)
            if (r2 == 0) goto L_0x005c
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            r2.remove(r0)
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "从下载列表中删除--->"
            r3.<init>(r4)
            r0 = r18
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
            goto L_0x005c
        L_0x045c:
            r0 = r27
            boolean r2 = r0.b
            if (r2 == 0) goto L_0x0481
            android.net.Uri r2 = android.net.Uri.fromFile(r19)
            android.content.Intent r3 = new android.content.Intent
            r3.<init>()
            java.lang.String r4 = "android.intent.action.VIEW"
            r3.setAction(r4)
            r4 = 268435456(0x10000000, float:2.5243549E-29)
            r3.setFlags(r4)
            java.lang.String r4 = "application/vnd.android.package-archive"
            r3.setDataAndType(r2, r4)
            r0 = r27
            android.content.Context r2 = r0.g
            r2.startActivity(r3)
        L_0x0481:
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            boolean r2 = r2.contains(r0)
            if (r2 == 0) goto L_0x04a8
            java.util.LinkedList r2 = com.adwo.adsdk.U.f
            r0 = r18
            r2.remove(r0)
            java.lang.String r2 = "Adwo SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "从下载列表中删除--->"
            r3.<init>(r4)
            r0 = r18
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
        L_0x04a8:
            r0 = r27
            android.content.Context r2 = r0.g
            r0 = r18
            com.adwo.adsdk.U.i(r2, r0)
            goto L_0x005c
        L_0x04b3:
            r5 = 0
            int r3 = (r14 > r5 ? 1 : (r14 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x04c0
            java.lang.String r3 = "Adwo SDK"
            java.lang.String r5 = "不支持断点下载--->"
            android.util.Log.e(r3, r5)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
        L_0x04c0:
            r5 = 0
            r12.seek(r5)     // Catch:{ SocketTimeoutException -> 0x04c7, MalformedURLException -> 0x05a5, IOException -> 0x0590 }
            goto L_0x0328
        L_0x04c7:
            r3 = move-exception
            r5 = r12
            r6 = r13
            r26 = r3
            r3 = r2
            r2 = r26
        L_0x04cf:
            r2.printStackTrace()
            r10 = r3
            r12 = r5
            r5 = r6
            goto L_0x0419
        L_0x04d7:
            r11 = 0
            r12.write(r10, r11, r7)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            int r7 = r7 + r6
            double r13 = (double) r7     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r24 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r13 = r13 * r24
            double r0 = (double) r9     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            r24 = r0
            double r13 = r13 / r24
            int r6 = (int) r13     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            java.lang.Thread r11 = java.lang.Thread.currentThread()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            monitor-enter(r11)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            if (r7 != r9) goto L_0x0525
            r13 = 16908308(0x1020014, float:2.3877285E-38)
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x0562 }
            java.lang.String r15 = java.lang.String.valueOf(r16)     // Catch:{ all -> 0x0562 }
            r14.<init>(r15)     // Catch:{ all -> 0x0562 }
            java.lang.String r15 = "已下载完成: "
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ all -> 0x0562 }
            java.lang.StringBuilder r6 = r14.append(r6)     // Catch:{ all -> 0x0562 }
            java.lang.String r14 = "%"
            java.lang.StringBuilder r6 = r6.append(r14)     // Catch:{ all -> 0x0562 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0562 }
            r0 = r21
            r0.setTextViewText(r13, r6)     // Catch:{ all -> 0x0562 }
            r0 = r21
            r1 = r20
            r1.contentView = r0     // Catch:{ all -> 0x0562 }
            r6 = 16908294(0x1020006, float:2.3877246E-38)
            r0 = r20
            r8.notify(r6, r0)     // Catch:{ all -> 0x0562 }
        L_0x0521:
            monitor-exit(r11)     // Catch:{ all -> 0x0562 }
            r6 = r7
            goto L_0x0337
        L_0x0525:
            if (r3 == r6) goto L_0x0521
            r3 = 16908308(0x1020014, float:2.3877285E-38)
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x0562 }
            java.lang.String r14 = java.lang.String.valueOf(r16)     // Catch:{ all -> 0x0562 }
            r13.<init>(r14)     // Catch:{ all -> 0x0562 }
            java.lang.String r14 = "已下载完成: "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0562 }
            java.lang.StringBuilder r13 = r13.append(r6)     // Catch:{ all -> 0x0562 }
            java.lang.String r14 = "%"
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0562 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0562 }
            r0 = r21
            r0.setTextViewText(r3, r13)     // Catch:{ all -> 0x0562 }
            r0 = r21
            r1 = r20
            r1.contentView = r0     // Catch:{ all -> 0x0562 }
            r0 = r22
            r1 = r20
            r1.contentIntent = r0     // Catch:{ all -> 0x0562 }
            r3 = 16908294(0x1020006, float:2.3877246E-38)
            r0 = r20
            r8.notify(r3, r0)     // Catch:{ all -> 0x0562 }
            r3 = r6
            goto L_0x0521
        L_0x0562:
            r3 = move-exception
            monitor-exit(r11)     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            throw r3     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
        L_0x0565:
            r3 = move-exception
            r6 = r5
            r5 = r12
            r26 = r2
            r2 = r3
            r3 = r26
            goto L_0x04cf
        L_0x056f:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ SocketTimeoutException -> 0x0565, MalformedURLException -> 0x0575, IOException -> 0x0595 }
            goto L_0x034b
        L_0x0575:
            r3 = move-exception
            r10 = r2
            r2 = r3
        L_0x0578:
            r2.printStackTrace()
            goto L_0x0419
        L_0x057d:
            r2 = move-exception
            r4 = r11
            r12 = r3
            r5 = r13
        L_0x0581:
            r2.printStackTrace()
            goto L_0x0419
        L_0x0586:
            r2 = move-exception
            goto L_0x0433
        L_0x0589:
            r2 = move-exception
            r4 = r11
            r5 = r13
            goto L_0x0581
        L_0x058d:
            r2 = move-exception
            r5 = r13
            goto L_0x0581
        L_0x0590:
            r3 = move-exception
            r10 = r2
            r5 = r13
            r2 = r3
            goto L_0x0581
        L_0x0595:
            r3 = move-exception
            r10 = r2
            r2 = r3
            goto L_0x0581
        L_0x0599:
            r2 = move-exception
            r4 = r11
            r12 = r3
            r5 = r13
            goto L_0x0578
        L_0x059e:
            r2 = move-exception
            r4 = r11
            r5 = r13
            goto L_0x0578
        L_0x05a2:
            r2 = move-exception
            r5 = r13
            goto L_0x0578
        L_0x05a5:
            r3 = move-exception
            r10 = r2
            r5 = r13
            r2 = r3
            goto L_0x0578
        L_0x05aa:
            r2 = move-exception
            r4 = r11
            r5 = r3
            r6 = r13
            r3 = r10
            goto L_0x04cf
        L_0x05b1:
            r2 = move-exception
            r3 = r10
            r4 = r11
            r5 = r12
            r6 = r13
            goto L_0x04cf
        L_0x05b8:
            r2 = move-exception
            r3 = r10
            r5 = r12
            r6 = r13
            goto L_0x04cf
        L_0x05be:
            r14 = r4
            goto L_0x02b3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0004ab.run():void");
    }
}
