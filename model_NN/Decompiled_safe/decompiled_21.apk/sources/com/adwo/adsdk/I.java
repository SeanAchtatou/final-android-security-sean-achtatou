package com.adwo.adsdk;

import java.util.ArrayList;
import java.util.List;

final class I {
    protected int a = 0;
    protected ErrorCode b = null;
    protected String c = null;
    protected String d = null;
    protected List e = new ArrayList();
    protected List f = new ArrayList();
    protected List g = new ArrayList();
    protected boolean h = true;
    protected boolean i = false;
    protected boolean j = false;
    protected String k = null;
    protected short l = 0;
    protected byte m = 0;

    /* access modifiers changed from: protected */
    public final void a(ErrorCode errorCode) {
        this.b = errorCode;
    }

    public final ErrorCode a() {
        return this.b;
    }

    protected I() {
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof I)) {
            return false;
        }
        I i2 = (I) obj;
        if (this.d == null || i2.d == null || !this.d.equals(i2.d)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return toString().hashCode();
    }
}
