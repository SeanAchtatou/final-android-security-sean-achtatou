package com.adwo.adsdk;

import android.os.Vibrator;

/* renamed from: com.adwo.adsdk.r  reason: case insensitive filesystem */
final class C0037r implements Runnable {
    private final /* synthetic */ Vibrator a;

    C0037r(C0036q qVar, Vibrator vibrator) {
        this.a = vibrator;
    }

    public final void run() {
        if (this.a != null) {
            this.a.cancel();
        }
        C0045z.a = false;
    }
}
