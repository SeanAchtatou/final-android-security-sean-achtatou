package com.adwo.adsdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;
import com.umeng.common.b.e;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/* renamed from: com.adwo.adsdk.ao  reason: case insensitive filesystem */
final class C0017ao {
    protected static int a = 20;
    protected static String b = null;
    protected static String c = null;
    private static C0017ao d;
    /* access modifiers changed from: private */
    public static String h = "1";
    private static String i = "-1";
    private static String j = "ADWO_SHARED_DATA";
    /* access modifiers changed from: private */
    public int e = 1;
    /* access modifiers changed from: private */
    public HashMap f = new HashMap();
    /* access modifiers changed from: private */
    public ArrayList g = new ArrayList();
    private boolean k = false;

    protected static C0017ao a(Context context) {
        if (d == null) {
            d = new C0017ao(context);
        }
        return d;
    }

    private C0017ao(Context context) {
        boolean z = true;
        if (a("configure_version", context) != null) {
            Date date = new Date();
            if (!this.k) {
                String a2 = a("update_date", context);
                if (a2 == null || !a2.equalsIgnoreCase(K.L.format(date))) {
                    this.k = false;
                    z = false;
                } else {
                    this.k = true;
                }
            }
            if (z) {
                c(context);
                return;
            }
        }
        new C0018ap(this, context).execute(new String[0]);
    }

    /* access modifiers changed from: protected */
    public final String a() {
        try {
            if ("1".equals(h)) {
                b = ((C0019aq) this.g.get(0)).c;
                int i2 = ((C0019aq) this.g.get(0)).e;
                c = ((C0019aq) this.g.get(0)).d;
                return ((C0019aq) this.g.get(0)).b;
            }
            int intValue = ((Integer) this.f.get(Integer.valueOf(Double.valueOf(Math.random() * ((double) this.e)).intValue() + 1))).intValue();
            b = ((C0019aq) this.g.get(intValue)).c;
            int i3 = ((C0019aq) this.g.get(intValue)).e;
            c = ((C0019aq) this.g.get(intValue)).d;
            return ((C0019aq) this.g.get(intValue)).b;
        } catch (Exception e2) {
            b = new String(K.b);
            c = U.d;
            return U.c;
        }
    }

    /* access modifiers changed from: private */
    public static String b(Context context) {
        String packageName;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("publicid=");
        try {
            stringBuffer.append(new String(K.b, e.f));
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        if (K.f != null) {
            packageName = new String(K.f);
        } else {
            packageName = context.getPackageName();
        }
        stringBuffer.append("&package=");
        stringBuffer.append(packageName);
        stringBuffer.append("&verionCode=");
        stringBuffer.append(K.b(context));
        stringBuffer.append("&nettype=");
        stringBuffer.append((int) K.n);
        stringBuffer.append("&opertype=");
        stringBuffer.append(0);
        try {
            stringBuffer.append("&manu=");
            String str = "unknown";
            if (K.d != null) {
                str = new String(K.d, e.f);
            }
            stringBuffer.append(str);
            stringBuffer.append("&brand=");
            String str2 = "unknwon";
            if (K.e != null) {
                str2 = new String(K.e, e.f);
            }
            stringBuffer.append(str2);
            stringBuffer.append("&os=");
            stringBuffer.append((int) K.r);
            stringBuffer.append("&userid=");
            stringBuffer.append(new String(K.c, e.f));
            new DisplayMetrics();
            stringBuffer.append("&screenSize=" + K.y + "x" + K.x + "," + context.getResources().getDisplayMetrics().density);
            stringBuffer.append("&timestamp=");
            stringBuffer.append(new Date().toString());
            stringBuffer.append("&pversion=");
            stringBuffer.append(i);
            stringBuffer.append("&sdkversion=");
            stringBuffer.append(new StringBuilder(String.valueOf((int) K.t)).toString());
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
        }
        return stringBuffer.toString();
    }

    static /* synthetic */ void a(C0017ao aoVar, String str, String str2, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences(j, 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    private static String a(String str, Context context) {
        return context.getSharedPreferences(j, 0).getString(str, null);
    }

    /* access modifiers changed from: private */
    public void c(Context context) {
        Map<String, ?> all = context.getSharedPreferences(j, 0).getAll();
        if (all == null || all.size() <= 0) {
            c();
            return;
        }
        for (Map.Entry next : all.entrySet()) {
            if ("strategy".equals(next.getKey())) {
                h = (String) next.getValue();
            } else if ("requestInterval".equals(next.getKey())) {
                int parseInt = Integer.parseInt((String) next.getValue());
                if (parseInt >= 30) {
                    a = parseInt;
                }
            } else if ("fs_requestInterval".equals(next.getKey())) {
                int parseInt2 = Integer.parseInt((String) next.getValue());
                if (parseInt2 > 20) {
                    K.R = parseInt2;
                }
            } else if ("fs_request_limit".equals(next.getKey())) {
                int parseInt3 = Integer.parseInt((String) next.getValue());
                if (parseInt3 > 0) {
                    K.S = parseInt3;
                }
            } else if (!"update_date".equals(next.getKey()) && !"configure_version".equals(next.getKey())) {
                if ("filter_ad_list".equals(next.getKey())) {
                    String str = (String) next.getValue();
                    if ("".equals(str) || "0".equals(str)) {
                        K.D = new int[0];
                    } else {
                        String[] split = str.split(",");
                        K.D = new int[split.length];
                        for (int i2 = 0; i2 < split.length; i2++) {
                            K.D[i2] = Integer.parseInt(split[i2]);
                        }
                    }
                } else {
                    StringTokenizer stringTokenizer = new StringTokenizer((String) next.getValue(), "=");
                    C0019aq aqVar = new C0019aq();
                    aqVar.b = stringTokenizer.nextToken();
                    aqVar.a = Double.parseDouble(stringTokenizer.nextToken());
                    aqVar.c = stringTokenizer.nextToken();
                    try {
                        String nextToken = stringTokenizer.nextToken();
                        if (nextToken != null) {
                            aqVar.e = Integer.decode(nextToken).intValue();
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    try {
                        aqVar.d = stringTokenizer.nextToken();
                    } catch (Exception e3) {
                    }
                    this.g.add(aqVar);
                    this.e += (int) (aqVar.a * 10.0d);
                    for (int i3 = this.e; i3 < this.e; i3++) {
                        this.f.put(Integer.valueOf(i3), Integer.valueOf(this.g.size() - 1));
                    }
                }
            }
        }
        this.e--;
    }

    private void c() {
        h = "1";
        a = 30;
        C0019aq aqVar = new C0019aq();
        aqVar.b = U.c;
        aqVar.a = 1.0d;
        aqVar.c = new String(K.b);
        aqVar.d = U.d;
        this.g.add(aqVar);
        this.e = ((int) (aqVar.a * 10.0d)) + this.e;
        for (int i2 = this.e; i2 < this.e; i2++) {
            this.f.put(Integer.valueOf(i2), Integer.valueOf(this.g.size() - 1));
        }
    }
}
