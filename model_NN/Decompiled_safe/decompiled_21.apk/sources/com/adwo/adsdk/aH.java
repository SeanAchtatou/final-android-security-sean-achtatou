package com.adwo.adsdk;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;

final class aH implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsPromptResult a;

    aH(aC aCVar, JsPromptResult jsPromptResult) {
        this.a = jsPromptResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.cancel();
    }
}
