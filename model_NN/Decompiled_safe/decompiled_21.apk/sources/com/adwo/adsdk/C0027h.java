package com.adwo.adsdk;

import android.app.Activity;

/* renamed from: com.adwo.adsdk.h  reason: case insensitive filesystem */
final class C0027h implements Runnable {
    private /* synthetic */ AdDisplayer a;
    private final /* synthetic */ Activity b;

    C0027h(AdDisplayer adDisplayer, Activity activity) {
        this.a = adDisplayer;
        this.b = activity;
    }

    public final void run() {
        this.a.p = this.b.getRequestedOrientation();
        this.b.setRequestedOrientation(this.a.q);
        this.b.getWindow().setFlags(1024, 1024);
    }
}
