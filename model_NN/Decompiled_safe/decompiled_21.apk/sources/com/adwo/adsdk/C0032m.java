package com.adwo.adsdk;

import android.content.DialogInterface;
import android.webkit.JsResult;

/* renamed from: com.adwo.adsdk.m  reason: case insensitive filesystem */
final class C0032m implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsResult a;

    C0032m(C0030k kVar, JsResult jsResult) {
        this.a = jsResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.cancel();
    }
}
