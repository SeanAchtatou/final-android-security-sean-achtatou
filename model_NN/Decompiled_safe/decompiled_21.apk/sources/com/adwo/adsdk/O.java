package com.adwo.adsdk;

import android.os.Handler;
import android.os.Message;

final class O extends Handler {
    private /* synthetic */ AdwoAdView a;

    O(AdwoAdView adwoAdView) {
        this.a = adwoAdView;
    }

    public final void handleMessage(Message message) {
        if (this.a.hasWindowFocus()) {
            boolean z = false;
            if (!(this.a.c == null || this.a.c.a() == null)) {
                z = this.a.c.a().j;
            }
            if (!C0045z.a && !z) {
                this.a.f();
            }
        }
        if (!this.a.f) {
            sendEmptyMessageDelayed(100, (long) (C0017ao.a * 1000));
        }
    }
}
