package com.adwo.adsdk;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/* renamed from: com.adwo.adsdk.q  reason: case insensitive filesystem */
public final class C0036q extends WebViewClient {
    /* access modifiers changed from: private */
    public /* synthetic */ AdDisplayer a;

    protected C0036q(AdDisplayer adDisplayer) {
        this.a = adDisplayer;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        try {
            if (this.a.v == null) {
                return;
            }
            if (this.a.v.showURL.equalsIgnoreCase(str) || str.contains(K.N)) {
                U.a(webView, this.a.v.adid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v102, types: [int] */
    /* JADX WARN: Type inference failed for: r1v106, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v107 */
    /* JADX WARN: Type inference failed for: r1v113 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean shouldOverrideUrlLoading(android.webkit.WebView r10, java.lang.String r11) {
        /*
            r9 = this;
            r6 = 2
            r1 = 0
            r7 = 1
            if (r11 == 0) goto L_0x08af
            java.lang.String r0 = "http"
            boolean r0 = r11.startsWith(r0)
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = "https"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x00ab
        L_0x0015:
            java.lang.String r0 = ".mp3"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = ".wav"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = ".mp4"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = ".3gp"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x003d
            java.lang.String r0 = ".mpeg"
            boolean r0 = r11.endsWith(r0)
            if (r0 == 0) goto L_0x0045
        L_0x003d:
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m
            com.adwo.adsdk.U.b(r0, r11)
        L_0x0044:
            return r7
        L_0x0045:
            java.lang.String r0 = ".apk"
            boolean r0 = r11.endsWith(r0)
            if (r0 == 0) goto L_0x0075
            com.adwo.adsdk.AdDisplayer r0 = r9.a
            com.adwo.adsdk.FullScreenAd r0 = r0.v
            int r2 = r0.adid
            com.adwo.adsdk.AdDisplayer r0 = r9.a
            com.adwo.adsdk.FullScreenAd r0 = r0.v
            byte r0 = r0.animationType
            r0 = r0 & 240(0xf0, float:3.36E-43)
            int r0 = r0 >> 4
            byte r0 = (byte) r0
            if (r0 <= 0) goto L_0x08bf
            r0 = r7
        L_0x0065:
            android.content.Context r1 = com.adwo.adsdk.AdDisplayer.m
            com.adwo.adsdk.AdDisplayer r3 = r9.a
            com.adwo.adsdk.FullScreenAd r3 = r3.v
            short r3 = r3.launchDelay
            com.adwo.adsdk.U.a(r11, r1, r2, r0, r3)
            goto L_0x0044
        L_0x0075:
            java.lang.String r0 = ".jpeg"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x009d
            java.lang.String r0 = ".gif"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x009d
            java.lang.String r0 = ".png"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x009d
            java.lang.String r0 = ".bmp"
            boolean r0 = r11.endsWith(r0)
            if (r0 != 0) goto L_0x009d
            java.lang.String r0 = ".jpg"
            boolean r0 = r11.endsWith(r0)
            if (r0 == 0) goto L_0x00a7
        L_0x009d:
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m
            android.app.Activity r0 = (android.app.Activity) r0
            com.adwo.adsdk.U.a(r11, r0)
            goto L_0x0044
        L_0x00a7:
            r10.loadUrl(r11)
            goto L_0x0044
        L_0x00ab:
            java.lang.String r0 = "tel"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x00bb
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m
            com.adwo.adsdk.U.f(r0, r11)
            goto L_0x0044
        L_0x00bb:
            java.lang.String r0 = "sms"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x00cc
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m
            com.adwo.adsdk.U.c(r0, r11)
            goto L_0x0044
        L_0x00cc:
            java.lang.String r0 = "market"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x00dd
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m
            com.adwo.adsdk.U.d(r0, r11)
            goto L_0x0044
        L_0x00dd:
            java.lang.String r0 = "mailto"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x00ee
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m
            com.adwo.adsdk.U.e(r0, r11)
            goto L_0x0044
        L_0x00ee:
            java.lang.String r0 = "adwo://"
            boolean r0 = r11.startsWith(r0)
            if (r0 == 0) goto L_0x08af
            r0 = 7
            java.lang.String r0 = r11.substring(r0)     // Catch:{ Exception -> 0x01b2 }
            java.util.StringTokenizer r3 = new java.util.StringTokenizer     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = "&"
            r3.<init>(r0, r2)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r4 = "adwoOpenURL"
            boolean r4 = r4.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r4 == 0) goto L_0x023f
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r4 = "_"
            int r4 = r2.indexOf(r4)     // Catch:{ Exception -> 0x01b2 }
            int r4 = r4 + 1
            java.lang.String r2 = r2.substring(r4)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x01b2 }
            int r4 = r2.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r5 = "_"
            int r5 = r2.indexOf(r5)     // Catch:{ Exception -> 0x01b2 }
            int r5 = r5 + 1
            java.lang.String r2 = r2.substring(r5)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x01b2 }
            r2.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r5 = "_"
            int r5 = r2.indexOf(r5)     // Catch:{ Exception -> 0x01b2 }
            int r5 = r5 + 1
            java.lang.String r2 = r2.substring(r5)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x01b2 }
            r2.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r5 = "_"
            int r5 = r2.indexOf(r5)     // Catch:{ Exception -> 0x01b2 }
            int r5 = r5 + 1
            java.lang.String r2 = r2.substring(r5)     // Catch:{ Exception -> 0x01b2 }
        L_0x0164:
            boolean r5 = r3.hasMoreTokens()     // Catch:{ Exception -> 0x01b2 }
            if (r5 != 0) goto L_0x01b8
            if (r4 != r7) goto L_0x01d4
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x01b2 }
            r1.<init>()     // Catch:{ Exception -> 0x01b2 }
            android.content.ComponentName r3 = new android.content.ComponentName     // Catch:{ Exception -> 0x0197 }
            java.lang.String r4 = "com.android.browser"
            java.lang.String r5 = "com.android.browser.BrowserActivity"
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x0197 }
            r1.setComponent(r3)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r3 = "android.intent.action.VIEW"
            r1.setAction(r3)     // Catch:{ Exception -> 0x0197 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ Exception -> 0x0197 }
            r1.setData(r2)     // Catch:{ Exception -> 0x0197 }
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r2)     // Catch:{ Exception -> 0x0197 }
            android.content.Context r2 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x0197 }
            r2.startActivity(r1)     // Catch:{ Exception -> 0x0197 }
            goto L_0x0044
        L_0x0197:
            r1 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = "javascript:adwoURLOpenFailed(+"
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b2 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = ");"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01b2 }
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x01b2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0044
        L_0x01b8:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x01b2 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = "&"
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r5 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0164
        L_0x01d4:
            if (r4 != 0) goto L_0x020c
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAdListener r0 = r0.u     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.ar.o = r0     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            r0.dismissDisplayer()     // Catch:{ Exception -> 0x01b2 }
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.ar r0 = com.adwo.adsdk.ar.a(r0)     // Catch:{ Exception -> 0x01b2 }
            android.view.View r1 = com.adwo.adsdk.AdDisplayer.f     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r3 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r3 = r3.v     // Catch:{ Exception -> 0x01b2 }
            int r3 = r3.adid     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r4 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r4 = r4.v     // Catch:{ Exception -> 0x01b2 }
            byte r4 = r4.animationType     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r5 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r5 = r5.v     // Catch:{ Exception -> 0x01b2 }
            short r5 = r5.launchDelay     // Catch:{ Exception -> 0x01b2 }
            r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x020c:
            if (r4 != r6) goto L_0x0044
            java.lang.String r0 = ".apk"
            boolean r0 = r2.endsWith(r0)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0044
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r0 = r0.v     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.adid     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r3 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r3 = r3.v     // Catch:{ Exception -> 0x01b2 }
            byte r3 = r3.animationType     // Catch:{ Exception -> 0x01b2 }
            r3 = r3 & 240(0xf0, float:3.36E-43)
            int r3 = r3 >> 4
            byte r3 = (byte) r3     // Catch:{ Exception -> 0x01b2 }
            if (r3 <= 0) goto L_0x022e
            r1 = r7
        L_0x022e:
            android.content.Context r3 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r4 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r4 = r4.v     // Catch:{ Exception -> 0x01b2 }
            short r4 = r4.launchDelay     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.U.a(r2, r3, r0, r1, r4)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x023f:
            java.lang.String r0 = "adwoVibrate"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x02a9
            boolean r0 = com.adwo.adsdk.C0045z.a     // Catch:{ Exception -> 0x01b2 }
            if (r0 != 0) goto L_0x0044
            r0 = 1
            com.adwo.adsdk.C0045z.a = r0     // Catch:{ Exception -> 0x01b2 }
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = "vibrator"
            java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ Exception -> 0x01b2 }
            android.os.Vibrator r0 = (android.os.Vibrator) r0     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r3 = "_"
            int r3 = r2.indexOf(r3)     // Catch:{ Exception -> 0x01b2 }
            int r3 = r3 + 1
            java.lang.String r2 = r2.substring(r3)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x01b2 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x01b2 }
            if (r2 <= r7) goto L_0x029b
            int r3 = r2 * 2
            long[] r4 = new long[r3]     // Catch:{ Exception -> 0x01b2 }
            r3 = r1
        L_0x0279:
            if (r3 < r2) goto L_0x028c
            r0.vibrate(r4, r2)     // Catch:{ Exception -> 0x01b2 }
            r1 = r2
        L_0x027f:
            com.adwo.adsdk.r r2 = new com.adwo.adsdk.r     // Catch:{ Exception -> 0x01b2 }
            r2.<init>(r9, r0)     // Catch:{ Exception -> 0x01b2 }
            int r0 = r1 * 1000
            long r0 = (long) r0     // Catch:{ Exception -> 0x01b2 }
            r10.postDelayed(r2, r0)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x028c:
            r5 = 100
            r4[r1] = r5     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            r5 = 500(0x1f4, double:2.47E-321)
            r4[r1] = r5     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            int r3 = r3 + 1
            goto L_0x0279
        L_0x029b:
            r1 = 2
            long[] r1 = new long[r1]     // Catch:{ Exception -> 0x01b2 }
            r2 = 1
            r3 = 1000(0x3e8, double:4.94E-321)
            r1[r2] = r3     // Catch:{ Exception -> 0x01b2 }
            r2 = 1
            r0.vibrate(r1, r2)     // Catch:{ Exception -> 0x01b2 }
            r1 = r7
            goto L_0x027f
        L_0x02a9:
            java.lang.String r0 = "adwoCloseAd"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x02d5
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r1 = r9.a     // Catch:{ Exception -> 0x01b2 }
            r1.setAnimation(r0)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            r0.dismissDisplayer()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x02d5:
            java.lang.String r0 = "adwoPlayAudio"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0313
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.s r2 = new com.adwo.adsdk.s     // Catch:{ Exception -> 0x01b2 }
            r2.<init>(r9, r0)     // Catch:{ Exception -> 0x01b2 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b2 }
            r1.start()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x0313:
            java.lang.String r0 = "adwoPlayVideo"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x03ed
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = "_"
            int r2 = r1.indexOf(r2)     // Catch:{ Exception -> 0x01b2 }
            int r2 = r2 + 1
            java.lang.String r1 = r1.substring(r2)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r1 = java.lang.Integer.decode(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r3 = "_"
            int r3 = r2.indexOf(r3)     // Catch:{ Exception -> 0x01b2 }
            int r3 = r3 + 1
            java.lang.String r2 = r2.substring(r3)     // Catch:{ Exception -> 0x01b2 }
            android.app.Dialog r3 = new android.app.Dialog     // Catch:{ Exception -> 0x01b2 }
            android.content.Context r4 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x01b2 }
            r4 = 0
            r3.setCancelable(r4)     // Catch:{ Exception -> 0x01b2 }
            r4 = 0
            r3.setCanceledOnTouchOutside(r4)     // Catch:{ Exception -> 0x01b2 }
            android.view.Window r4 = r3.getWindow()     // Catch:{ Exception -> 0x01b2 }
            r5 = 16777216(0x1000000, float:2.3509887E-38)
            r6 = 16777216(0x1000000, float:2.3509887E-38)
            r4.setFlags(r5, r6)     // Catch:{ Exception -> 0x01b2 }
            r4 = 1
            r3.requestWindowFeature(r4)     // Catch:{ Exception -> 0x01b2 }
            android.widget.VideoView r4 = new android.widget.VideoView     // Catch:{ Exception -> 0x01b2 }
            android.content.Context r5 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.u r5 = new com.adwo.adsdk.u     // Catch:{ Exception -> 0x01b2 }
            r5.<init>(r9, r4)     // Catch:{ Exception -> 0x01b2 }
            r3.setOnDismissListener(r5)     // Catch:{ Exception -> 0x01b2 }
            android.widget.MediaController r5 = new android.widget.MediaController     // Catch:{ Exception -> 0x01b2 }
            android.content.Context r6 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x01b2 }
            r4.setMediaController(r5)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.v r5 = new com.adwo.adsdk.v     // Catch:{ Exception -> 0x01b2 }
            r5.<init>(r9, r3, r10)     // Catch:{ Exception -> 0x01b2 }
            r4.setOnCompletionListener(r5)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.w r5 = new com.adwo.adsdk.w     // Catch:{ Exception -> 0x01b2 }
            r5.<init>(r9, r3)     // Catch:{ Exception -> 0x01b2 }
            r4.setOnErrorListener(r5)     // Catch:{ Exception -> 0x01b2 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ Exception -> 0x01b2 }
            r4.setVideoURI(r2)     // Catch:{ Exception -> 0x01b2 }
            r4.start()     // Catch:{ Exception -> 0x01b2 }
            android.widget.RelativeLayout$LayoutParams r2 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x01b2 }
            r2.<init>(r0, r1)     // Catch:{ Exception -> 0x01b2 }
            r3.setContentView(r4, r2)     // Catch:{ Exception -> 0x01b2 }
            r3.show()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x03ed:
            java.lang.String r0 = "adwoCloseVideo"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x03f9
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x03f9:
            java.lang.String r0 = "adwoFetchLocation"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0432
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            double r0 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.K.I = r0     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            double r0 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.K.J = r0     // Catch:{ Exception -> 0x01b2 }
            r0 = 1
            com.adwo.adsdk.K.E = r0     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x0432:
            java.lang.String r0 = "adwoPageDidLoad"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0442
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.C0045z.b = r0     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x0442:
            java.lang.String r0 = "adwoAdClickCount"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x047d
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r0 = r0.v     // Catch:{ Exception -> 0x01b2 }
            r0.clickAction()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = "javascript:"
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b2 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "();"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01b2 }
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x047d:
            java.lang.String r0 = "adwoFSAdShowCount"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0490
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r0 = r0.v     // Catch:{ Exception -> 0x01b2 }
            r0.showAction()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x0490:
            java.lang.String r0 = "adwoChangeBrowserDoneStatus"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x04b1
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x04b1:
            java.lang.String r0 = "adwoAddToAddressBook"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x04c6
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.webkit.WebView r0 = r0.h     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "javascript:adwoDoGetAddressBookInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x04c6:
            java.lang.String r0 = "adwoAddToCalendar"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x04db
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.webkit.WebView r0 = r0.h     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "javascript:adwoDoGetCalendarEventInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x04db:
            java.lang.String r0 = "adwoAddToReminder"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x04f0
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.webkit.WebView r0 = r0.h     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "javascript:adwoDoGetReminderInfo();"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x04f0:
            java.lang.String r0 = "adwoInitEssentialParams"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0537
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r1 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r1 = r1.v     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1.adid     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.U.a(r10, r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "0"
            boolean r1 = r1.equals(r0)     // Catch:{ Exception -> 0x01b2 }
            if (r1 != 0) goto L_0x0044
            if (r10 == 0) goto L_0x0044
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = "javascript:"
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b2 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "+();"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01b2 }
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x0537:
            java.lang.String r0 = "adwoStartAudioRecorder"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0575
            com.adwo.adsdk.AdDisplayer r8 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.aj r0 = new com.adwo.adsdk.aj     // Catch:{ Exception -> 0x01b2 }
            r1 = 4648488871632306176(0x4082c00000000000, double:600.0)
            r3 = 0
            r4 = 0
            com.adwo.adsdk.AdDisplayer r5 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r5 = r5.v     // Catch:{ Exception -> 0x01b2 }
            int r6 = r5.adid     // Catch:{ Exception -> 0x01b2 }
            r5 = r10
            r0.<init>(r1, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01b2 }
            r8.g = r0     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.aj r0 = r0.g     // Catch:{ Exception -> 0x01b2 }
            r0.a()     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.os.Handler r0 = r0.s     // Catch:{ Exception -> 0x01b2 }
            r1 = 6
            com.adwo.adsdk.AdDisplayer r2 = r9.a     // Catch:{ Exception -> 0x01b2 }
            int r2 = r2.r     // Catch:{ Exception -> 0x01b2 }
            long r2 = (long) r2     // Catch:{ Exception -> 0x01b2 }
            r0.sendEmptyMessageDelayed(r1, r2)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x0575:
            java.lang.String r0 = "adwoCloseAudioRecorder"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x058e
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            r0.d()     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.os.Handler r0 = r0.s     // Catch:{ Exception -> 0x01b2 }
            r1 = 6
            r0.removeMessages(r1)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x058e:
            java.lang.String r0 = "adwoTakePhoto"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x05c9
            android.content.Context r0 = r10.getContext()     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer.m = r0     // Catch:{ Exception -> 0x01b2 }
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "android.hardware.camera"
            boolean r0 = r0.hasSystemFeature(r1)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x05b9
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer.a(r0, r3)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = "javascript:adwoFetchCameraAssociatedImageInfo();"
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x05b9:
            android.content.Context r0 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "设备没有摄像头调用！"
            r2 = 0
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)     // Catch:{ Exception -> 0x01b2 }
            r0.show()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x05c9:
            java.lang.String r0 = "adwoCloseCamera"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x05d8
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer.i(r0)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x05d8:
            java.lang.String r0 = "adwoSaveImg2Gallery"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x05ff
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Thread r1 = new java.lang.Thread     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.x r2 = new com.adwo.adsdk.x     // Catch:{ Exception -> 0x01b2 }
            r2.<init>(r9, r0)     // Catch:{ Exception -> 0x01b2 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b2 }
            r1.start()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x05ff:
            java.lang.String r0 = "adwoVoiceRecord"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0666
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            double r1 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r3 = "_"
            int r3 = r0.indexOf(r3)     // Catch:{ Exception -> 0x01b2 }
            int r3 = r3 + 1
            java.lang.String r3 = r0.substring(r3)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r8 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.aj r0 = new com.adwo.adsdk.aj     // Catch:{ Exception -> 0x01b2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r5 = com.adwo.adsdk.K.N     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x01b2 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x01b2 }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01b2 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r5 = "adwoRecord.mp4"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r5 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAd r5 = r5.v     // Catch:{ Exception -> 0x01b2 }
            int r6 = r5.adid     // Catch:{ Exception -> 0x01b2 }
            r5 = r10
            r0.<init>(r1, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01b2 }
            r8.g = r0     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.aj r0 = r0.g     // Catch:{ Exception -> 0x01b2 }
            r0.a()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x0666:
            java.lang.String r0 = "adwoVoiceRecordStop"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x06a0
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0699
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.aj r0 = r0.g     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0699
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.aj r0 = r0.g     // Catch:{ Exception -> 0x01b2 }
            r1 = 0
            r0.a = r1     // Catch:{ Exception -> 0x01b2 }
        L_0x0699:
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            r0.d()     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x06a0:
            java.lang.String r0 = "adwoFSShrink"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0780
            java.lang.String r0 = "javascript:adwoFSShrinkBegin();"
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = "_"
            int r2 = r0.indexOf(r2)     // Catch:{ Exception -> 0x01b2 }
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r4 = "_"
            int r4 = r2.indexOf(r4)     // Catch:{ Exception -> 0x01b2 }
            int r4 = r4 + 1
            java.lang.String r2 = r2.substring(r4)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x01b2 }
            int r4 = r2.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r5 = "_"
            int r5 = r2.indexOf(r5)     // Catch:{ Exception -> 0x01b2 }
            int r5 = r5 + 1
            java.lang.String r2 = r2.substring(r5)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r2 = java.lang.Integer.decode(r2)     // Catch:{ Exception -> 0x01b2 }
            int r5 = r2.intValue()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r3 = "_"
            int r3 = r2.indexOf(r3)     // Catch:{ Exception -> 0x01b2 }
            int r3 = r3 + 1
            java.lang.String r2 = r2.substring(r3)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Double.parseDouble(r2)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r2 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.widget.PopupWindow r2 = r2.e     // Catch:{ Exception -> 0x01b2 }
            if (r2 == 0) goto L_0x074c
            com.adwo.adsdk.AdDisplayer r2 = r9.a     // Catch:{ Exception -> 0x01b2 }
            r3 = 0
            r2.a(r4, r5, r3)     // Catch:{ Exception -> 0x01b2 }
            if (r0 != r7) goto L_0x0753
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.util.DisplayMetrics r0 = r0.n     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.heightPixels     // Catch:{ Exception -> 0x01b2 }
            if (r0 <= r5) goto L_0x08bc
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.util.DisplayMetrics r0 = r0.n     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.heightPixels     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0 - r5
            int r3 = r0 / 2
        L_0x072d:
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.util.DisplayMetrics r0 = r0.n     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.widthPixels     // Catch:{ Exception -> 0x01b2 }
            if (r0 <= r4) goto L_0x08b2
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.util.DisplayMetrics r0 = r0.n     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.widthPixels     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0 - r4
            int r1 = r0 / 2
            r2 = r1
        L_0x0743:
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.view.View r1 = com.adwo.adsdk.AdDisplayer.f     // Catch:{ Exception -> 0x01b2 }
            r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01b2 }
        L_0x074c:
            java.lang.String r0 = "javascript:adwoFSShrinkComplete();"
            r10.loadUrl(r0)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x0753:
            if (r0 < r6) goto L_0x08b8
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.util.DisplayMetrics r0 = r0.n     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.heightPixels     // Catch:{ Exception -> 0x01b2 }
            if (r0 <= r5) goto L_0x08b5
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.util.DisplayMetrics r0 = r0.n     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.heightPixels     // Catch:{ Exception -> 0x01b2 }
            int r3 = r0 - r5
        L_0x0769:
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.util.DisplayMetrics r0 = r0.n     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.widthPixels     // Catch:{ Exception -> 0x01b2 }
            if (r0 <= r4) goto L_0x08b2
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.util.DisplayMetrics r0 = r0.n     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.widthPixels     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0 - r4
            int r1 = r0 / 2
            r2 = r1
            goto L_0x0743
        L_0x0780:
            java.lang.String r0 = "adwoFSAdDidLoad"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x07e3
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r1 = r9.a     // Catch:{ Exception -> 0x01b2 }
            r1.setAnimation(r0)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAdListener r0 = r0.u     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x07b6
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.FullScreenAdListener r0 = r0.u     // Catch:{ Exception -> 0x01b2 }
            r0.onLoadAdComplete()     // Catch:{ Exception -> 0x01b2 }
        L_0x07b6:
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.webkit.WebView r0 = r0.h     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r0 = r0.getTitle()     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0044
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "app"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x01b2 }
            if (r1 == 0) goto L_0x0044
            java.lang.String r1 = "fun"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0044
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.webkit.WebView r0 = r0.h     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "javascript:window.adwo.getContentWidth(document.getElementById('box').offsetWidth+','+document.getElementById('box').offsetHeight);"
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x07e3:
            java.lang.String r0 = "adwoBrowserDisableScroll"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0801
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.webkit.WebView r0 = r0.h     // Catch:{ Exception -> 0x01b2 }
            r1 = 0
            r0.setVerticalScrollBarEnabled(r1)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.webkit.WebView r0 = r0.h     // Catch:{ Exception -> 0x01b2 }
            r1 = 0
            r0.setHorizontalScrollBarEnabled(r1)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x0801:
            java.lang.String r0 = "adwoBrowserShowIndicator"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x083d
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.widget.ProgressBar r1 = new android.widget.ProgressBar     // Catch:{ Exception -> 0x01b2 }
            android.content.Context r2 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b2 }
            r0.l = r1     // Catch:{ Exception -> 0x01b2 }
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x01b2 }
            r1 = -2
            r2 = -2
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x01b2 }
            r1 = 13
            r0.addRule(r1)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r1 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.widget.ProgressBar r1 = r1.l     // Catch:{ Exception -> 0x01b2 }
            r1.setLayoutParams(r0)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.widget.RelativeLayout r0 = r0.j     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r1 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.widget.ProgressBar r1 = r1.l     // Catch:{ Exception -> 0x01b2 }
            r0.addView(r1)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x083d:
            java.lang.String r0 = "adwoBrowserHideIndicator"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x085e
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.widget.RelativeLayout r0 = r0.j     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0044
            com.adwo.adsdk.AdDisplayer r0 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.widget.RelativeLayout r0 = r0.j     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r1 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.widget.ProgressBar r1 = r1.l     // Catch:{ Exception -> 0x01b2 }
            r0.removeView(r1)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x085e:
            java.lang.String r0 = "adwoIntent"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x087f
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            android.content.Context r1 = com.adwo.adsdk.AdDisplayer.m     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r2 = "_"
            int r2 = r0.indexOf(r2)     // Catch:{ Exception -> 0x01b2 }
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.U.g(r1, r0)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x087f:
            java.lang.String r0 = "adwoSetBackgroundColor"
            boolean r0 = r0.equalsIgnoreCase(r2)     // Catch:{ Exception -> 0x01b2 }
            if (r0 == 0) goto L_0x0044
            java.lang.String r0 = r3.nextToken()     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r1 = "_"
            int r1 = r0.indexOf(r1)     // Catch:{ Exception -> 0x01b2 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x01b2 }
            java.lang.Integer r0 = java.lang.Integer.decode(r0)     // Catch:{ Exception -> 0x01b2 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x01b2 }
            com.adwo.adsdk.AdDisplayer r1 = r9.a     // Catch:{ Exception -> 0x01b2 }
            android.widget.PopupWindow r1 = r1.e     // Catch:{ Exception -> 0x01b2 }
            android.graphics.drawable.ColorDrawable r2 = new android.graphics.drawable.ColorDrawable     // Catch:{ Exception -> 0x01b2 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x01b2 }
            r1.setBackgroundDrawable(r2)     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0044
        L_0x08af:
            r7 = r1
            goto L_0x0044
        L_0x08b2:
            r2 = r1
            goto L_0x0743
        L_0x08b5:
            r3 = r1
            goto L_0x0769
        L_0x08b8:
            r3 = r1
            r2 = r1
            goto L_0x0743
        L_0x08bc:
            r3 = r1
            goto L_0x072d
        L_0x08bf:
            r0 = r1
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0036q.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
    }
}
