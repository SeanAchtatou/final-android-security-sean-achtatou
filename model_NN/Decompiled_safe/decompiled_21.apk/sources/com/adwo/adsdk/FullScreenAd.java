package com.adwo.adsdk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

final class FullScreenAd implements Serializable {
    private static final long serialVersionUID = 4230721171425262662L;
    protected int adid = -1;
    protected byte animationType = 0;
    protected List beaconUrlList = new ArrayList();
    protected String clickChargeURL = null;
    protected ErrorCode err = null;
    protected int height = 480;
    protected boolean isCharged = false;
    protected boolean isClicked = false;
    protected byte isInhouseAd = 0;
    protected short launchDelay = 0;
    protected List packageList = new ArrayList();
    protected String serverStamp = null;
    protected List showBeaconList = new ArrayList();
    protected String showChargeURL = null;
    protected String showURL = null;
    protected String updateDate = null;
    protected int width = 320;

    protected FullScreenAd() {
    }

    /* access modifiers changed from: protected */
    public final void clickAction() {
        new Thread(new C0010ah(this)).start();
    }

    /* access modifiers changed from: protected */
    public final void showAction() {
        new Thread(new C0011ai(this)).start();
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof FullScreenAd)) {
            return false;
        }
        FullScreenAd fullScreenAd = (FullScreenAd) obj;
        if (this.showURL == null || fullScreenAd.showURL == null || !this.showURL.equals(fullScreenAd.showURL)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return toString().hashCode();
    }
}
