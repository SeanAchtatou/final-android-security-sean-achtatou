package com.adwo.adsdk;

import android.content.DialogInterface;
import android.webkit.JsResult;

/* renamed from: com.adwo.adsdk.l  reason: case insensitive filesystem */
final class C0031l implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsResult a;

    C0031l(C0030k kVar, JsResult jsResult) {
        this.a = jsResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.a.confirm();
    }
}
