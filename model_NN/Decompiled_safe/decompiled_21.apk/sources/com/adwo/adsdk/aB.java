package com.adwo.adsdk;

import android.view.View;

final class aB implements Runnable {
    private /* synthetic */ ar a;
    private final /* synthetic */ View b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;
    private final /* synthetic */ int e;
    private final /* synthetic */ int f;

    aB(ar arVar, View view, int i, int i2, int i3, int i4) {
        this.a = arVar;
        this.b = view;
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.update(int, int, int, int, boolean):void}
     arg types: [int, int, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.update(android.view.View, int, int, int, int):void}
      ClspMth{android.widget.PopupWindow.update(int, int, int, int, boolean):void} */
    public final void run() {
        this.a.a.showAtLocation(this.b, 0, this.c, this.d);
        this.a.a.update(this.c, this.d + this.a.k, this.e, this.f - this.a.k, true);
    }
}
