package com.adwo.adsdk;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.umeng.common.b.e;
import igudi.com.ergushi.AnimationType;
import java.io.File;
import java.util.Date;
import java.util.StringTokenizer;

public final class AdDisplayer {
    public static final byte ADWO_FS_ENTRY = 1;
    public static final byte ADWO_FS_INTERCEPT = 0;
    public static final byte ADWO_FS_TRANSPOSITION = 2;
    public static final byte ADWO_FS_TYPE_ALL = 0;
    public static final byte ADWO_FS_TYPE_APP_FUN = 1;
    public static final byte ADWO_FS_TYPE_NO_APP_FUN = 2;
    protected static byte a = 0;
    protected static byte b = 0;
    private static AdDisplayer d;
    /* access modifiers changed from: private */
    public static View f;
    /* access modifiers changed from: private */
    public static Context m;
    String c;
    /* access modifiers changed from: private */
    public PopupWindow e = null;
    /* access modifiers changed from: private */
    public C0012aj g;
    /* access modifiers changed from: private */
    public WebView h;
    private int i = 602;
    /* access modifiers changed from: private */
    public RelativeLayout j;
    private RelativeLayout k;
    /* access modifiers changed from: private */
    public ProgressBar l;
    /* access modifiers changed from: private */
    public DisplayMetrics n = null;
    /* access modifiers changed from: private */
    public int o = 0;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public int r;
    /* access modifiers changed from: private */
    public Handler s;
    private boolean t;
    /* access modifiers changed from: private */
    public FullScreenAdListener u;
    /* access modifiers changed from: private */
    public FullScreenAd v;
    private int w;

    public static AdDisplayer getInstance(Context context) {
        m = context;
        if (d == null) {
            d = new AdDisplayer(context);
        }
        return d;
    }

    public final void initParems(String str, boolean z, FullScreenAdListener fullScreenAdListener) {
        K.b(str);
        if (U.d(m)) {
            K.a(z);
            this.u = fullScreenAdListener;
            K.B = AdwoKey.a(m);
            K.C = C0017ao.a(m);
            K.c(m);
            File file = new File(K.O);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
    }

    public final void setDesireAdType(byte b2) {
        a = b2;
    }

    public final void setDesireAdForm(byte b2) {
        b = b2;
    }

    public final void requestFullScreenAd() {
        FullScreenAd fullScreenAd;
        if (K.b()) {
            Log.e("Adwo SDK", "The interval that this request from the last one is shorter than 20 seconds.In other word, ad requesting has been too frequent.");
        } else if (b != 1) {
            c();
        } else if (K.q == 0) {
            Log.e("Adwo SDK", "No external storage found on the device, so could not make an ad request.");
            if (this.u != null) {
                this.u.onFailedToReceiveAd(new ErrorCode(33, "OTHER_ERROR_NO_SDCARD"));
            }
        } else if (!K.P || U.c(m) != 0) {
            File file = new File(K.O);
            if (!file.exists()) {
                file.mkdirs();
            }
            File[] listFiles = file.listFiles();
            if (listFiles == null || listFiles.length == 0) {
                Log.e("Adwo SDK", "This is the first entry full screen ad or there is no ad available in the sdcard.");
                if (this.u != null) {
                    this.u.onFailedToReceiveAd(new ErrorCode(35, "OTHER_ERROR_NO_ENTRY_AD_ON_SDCARD"));
                }
            } else {
                File[] listFiles2 = listFiles[0].listFiles();
                if (listFiles2 == null || listFiles2.length <= 0) {
                    fullScreenAd = null;
                } else {
                    fullScreenAd = U.c(String.valueOf(listFiles2[0].getAbsolutePath()) + "/object.temp");
                }
                if (fullScreenAd != null) {
                    this.v = fullScreenAd;
                    listFiles[0].getAbsolutePath();
                    if (this.u != null) {
                        this.u.onReceiveAd();
                    }
                } else {
                    Log.e("Adwo SDK", "There is no entry full screen ad available in the sdcard.");
                    this.v = null;
                    if (this.u != null) {
                        this.u.onFailedToReceiveAd(new ErrorCode(35, "OTHER_ERROR_NO_ENTRY_AD_ON_SDCARD"));
                        return;
                    }
                    return;
                }
            }
            c();
        } else {
            Log.e("Adwo SDK", "The network is not available.");
            if (this.u != null) {
                this.u.onFailedToReceiveAd(new ErrorCode(30, "ERR_UNKNOWN"));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00e1, code lost:
        if (r2.parse(r8.v.updateDate).after(new java.util.Date(r3.lastModified())) != false) goto L_0x00e3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void preLoadFullScreenAd() {
        /*
            r8 = this;
            r0 = 1
            r1 = 0
            com.adwo.adsdk.FullScreenAd r2 = r8.v
            if (r2 != 0) goto L_0x000e
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "The ad object is null."
            android.util.Log.e(r0, r1)
        L_0x000d:
            return
        L_0x000e:
            android.webkit.WebView r2 = r8.h
            r2.clearHistory()
            byte r2 = com.adwo.adsdk.AdDisplayer.b
            if (r2 != r0) goto L_0x0021
            android.webkit.WebView r0 = r8.h
            com.adwo.adsdk.FullScreenAd r1 = r8.v
            java.lang.String r1 = r1.showURL
            r0.loadUrl(r1)
            goto L_0x000d
        L_0x0021:
            com.adwo.adsdk.FullScreenAd r2 = r8.v
            java.lang.String r2 = r2.showURL
            byte r3 = com.adwo.adsdk.K.q
            if (r3 == 0) goto L_0x013f
            java.lang.String r3 = ".zip"
            boolean r3 = r2.endsWith(r3)
            if (r3 == 0) goto L_0x013f
            java.lang.String r3 = "/"
            int r3 = r2.lastIndexOf(r3)
            java.lang.String r4 = ".zip"
            int r4 = r2.lastIndexOf(r4)
            int r3 = r3 + 1
            java.lang.String r2 = r2.substring(r3, r4)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = com.adwo.adsdk.K.N
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3.<init>(r4)
            com.adwo.adsdk.FullScreenAd r4 = r8.v
            int r4 = r4.adid
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = java.io.File.separator
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
            java.io.File r3 = new java.io.File
            r3.<init>(r2)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "file://"
            r4.<init>(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = java.io.File.separator
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "index.html?w="
            java.lang.StringBuilder r2 = r2.append(r4)
            android.util.DisplayMetrics r4 = r8.n
            int r4 = r4.widthPixels
            float r4 = (float) r4
            android.util.DisplayMetrics r5 = r8.n
            float r5 = r5.density
            float r4 = r4 / r5
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "&h="
            java.lang.StringBuilder r2 = r2.append(r4)
            android.util.DisplayMetrics r4 = r8.n
            int r4 = r4.heightPixels
            float r4 = (float) r4
            android.util.DisplayMetrics r5 = r8.n
            float r5 = r5.density
            float r4 = r4 / r5
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = r2.toString()
            boolean r2 = r3.exists()
            if (r2 == 0) goto L_0x011c
            com.adwo.adsdk.FullScreenAd r2 = r8.v
            java.lang.String r2 = r2.updateDate
            if (r2 == 0) goto L_0x0114
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.lang.String r5 = "yyyy-MM-dd hh:mm:ss"
            r2.<init>(r5)
            com.adwo.adsdk.FullScreenAd r5 = r8.v     // Catch:{ ParseException -> 0x0110 }
            java.lang.String r5 = r5.updateDate     // Catch:{ ParseException -> 0x0110 }
            int r5 = r5.length()     // Catch:{ ParseException -> 0x0110 }
            r6 = 10
            if (r5 != r6) goto L_0x00cc
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat     // Catch:{ ParseException -> 0x0110 }
            java.lang.String r5 = "yyyy-MM-dd"
            r2.<init>(r5)     // Catch:{ ParseException -> 0x0110 }
        L_0x00cc:
            com.adwo.adsdk.FullScreenAd r5 = r8.v     // Catch:{ ParseException -> 0x0110 }
            java.lang.String r5 = r5.updateDate     // Catch:{ ParseException -> 0x0110 }
            java.util.Date r2 = r2.parse(r5)     // Catch:{ ParseException -> 0x0110 }
            java.util.Date r5 = new java.util.Date     // Catch:{ ParseException -> 0x0110 }
            long r6 = r3.lastModified()     // Catch:{ ParseException -> 0x0110 }
            r5.<init>(r6)     // Catch:{ ParseException -> 0x0110 }
            boolean r2 = r2.after(r5)     // Catch:{ ParseException -> 0x0110 }
            if (r2 == 0) goto L_0x0114
        L_0x00e3:
            if (r0 == 0) goto L_0x0116
            com.adwo.adsdk.U.a(r3)
            com.adwo.adsdk.FullScreenAd r0 = r8.v
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = com.adwo.adsdk.K.N
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r2.<init>(r3)
            com.adwo.adsdk.FullScreenAd r3 = r8.v
            int r3 = r3.adid
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = java.io.File.separator
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r8.a(r0, r2, r4, r1)
        L_0x010a:
            com.adwo.adsdk.FullScreenAd r0 = r8.v
            r0.showURL = r4
            goto L_0x000d
        L_0x0110:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0114:
            r0 = r1
            goto L_0x00e3
        L_0x0116:
            android.webkit.WebView r0 = r8.h
            r0.loadUrl(r4)
            goto L_0x010a
        L_0x011c:
            com.adwo.adsdk.FullScreenAd r0 = r8.v
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = com.adwo.adsdk.K.N
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r2.<init>(r3)
            com.adwo.adsdk.FullScreenAd r3 = r8.v
            int r3 = r3.adid
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = java.io.File.separator
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r8.a(r0, r2, r4, r1)
            goto L_0x010a
        L_0x013f:
            android.webkit.WebView r0 = r8.h
            r0.clearHistory()
            android.webkit.WebView r0 = r8.h
            r0.loadUrl(r2)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.AdDisplayer.preLoadFullScreenAd():void");
    }

    public final void displayFullScreenAd(View view) {
        if (this.v == null) {
            Log.e("Adwo SDK", "The ad object is null.");
            return;
        }
        try {
            if (this.o == 0) {
                Rect rect = new Rect();
                ((Activity) m).getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
                if (rect.top == 0) {
                    this.o = 0;
                } else {
                    this.o = rect.top;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.e.setBackgroundDrawable(new ColorDrawable(0));
        a(view);
    }

    public final void dismissDisplayer() {
        try {
            if (this.e != null) {
                this.e.dismiss();
            }
            d();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void setKeyWords(String str) {
        K.l = str;
    }

    public final void setAnimation(int i2) {
        switch (i2) {
            case 2:
                this.e.setAnimationStyle(16973827);
                return;
            case 3:
                this.e.setAnimationStyle(16973910);
                return;
            case 4:
            default:
                return;
            case 5:
            case AnimationType.TRANSLATE_FROM_RIGHT /*6*/:
                this.e.setAnimationStyle(16973826);
                return;
        }
    }

    public final boolean isShowing() {
        if (this.e != null) {
            return this.e.isShowing();
        }
        return false;
    }

    public final void setFSContext(Context context) {
        m = context;
    }

    static /* synthetic */ boolean m(AdDisplayer adDisplayer) {
        return false;
    }

    private AdDisplayer(Context context) {
        new Handler();
        this.p = 1;
        this.q = 1;
        this.r = 50;
        this.s = new C0002a(this);
        this.t = false;
        this.c = null;
        this.w = 0;
        this.n = context.getResources().getDisplayMetrics();
        dismissDisplayer();
        a(this.n.widthPixels, this.n.heightPixels, true);
        if (this.n.widthPixels > this.n.heightPixels) {
            K.y = this.n.heightPixels;
            K.x = this.n.widthPixels;
        } else {
            K.y = this.n.widthPixels;
            K.x = this.n.heightPixels;
        }
        K.K = (double) this.n.density;
    }

    private void a(int i2, int i3) {
        this.e = new AW(m);
        this.e.setOutsideTouchable(false);
        this.e.setClippingEnabled(false);
        this.e.setTouchable(true);
        this.e.setFocusable(true);
        this.e.setAnimationStyle(16973910);
        this.e.setBackgroundDrawable(new BitmapDrawable());
        this.e.setWidth(i2);
        this.e.setHeight(i3);
        if (this.k != null) {
            this.k.removeView(this.j);
        }
        this.k = new RelativeLayout(m);
        this.k.addView(this.j);
        this.e.setContentView(this.k);
        this.e.setOnDismissListener(new C0021b(this));
    }

    private void b(int i2, int i3) {
        this.h.setLayoutParams(new RelativeLayout.LayoutParams(i2, i3));
        this.h.requestLayout();
    }

    private void c() {
        new C0025f(this).execute(new String[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00a8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void b(com.adwo.adsdk.AdDisplayer r8, com.adwo.adsdk.FullScreenAd r9) {
        /*
            r1 = 1
            java.lang.String r0 = r9.showURL
            java.lang.String r2 = ".zip"
            boolean r0 = r0.endsWith(r2)
            if (r0 == 0) goto L_0x0151
            java.lang.String r0 = r9.showURL
            java.lang.String r2 = "/"
            int r0 = r0.lastIndexOf(r2)
            java.lang.String r2 = r9.showURL
            java.lang.String r3 = ".zip"
            int r2 = r2.lastIndexOf(r3)
            java.lang.String r3 = r9.showURL
            int r0 = r0 + 1
            java.lang.String r3 = r3.substring(r0, r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = com.adwo.adsdk.K.O
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r0.<init>(r2)
            int r2 = r9.adid
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = java.io.File.separator
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.io.File r4 = new java.io.File
            r4.<init>(r0)
            boolean r0 = r4.exists()
            if (r0 == 0) goto L_0x0131
            r2 = 0
            java.lang.String r0 = r9.updateDate
            if (r0 == 0) goto L_0x00a6
            java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat
            java.lang.String r5 = "yyyy-MM-dd hh:mm:ss"
            r0.<init>(r5)
            com.adwo.adsdk.FullScreenAd r5 = r8.v     // Catch:{ ParseException -> 0x00a2 }
            java.lang.String r5 = r5.updateDate     // Catch:{ ParseException -> 0x00a2 }
            int r5 = r5.length()     // Catch:{ ParseException -> 0x00a2 }
            r6 = 10
            if (r5 != r6) goto L_0x0068
            java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat     // Catch:{ ParseException -> 0x00a2 }
            java.lang.String r5 = "yyyy-MM-dd"
            r0.<init>(r5)     // Catch:{ ParseException -> 0x00a2 }
        L_0x0068:
            java.lang.String r5 = r9.updateDate     // Catch:{ ParseException -> 0x00a2 }
            java.util.Date r0 = r0.parse(r5)     // Catch:{ ParseException -> 0x00a2 }
            java.util.Date r5 = new java.util.Date     // Catch:{ ParseException -> 0x00a2 }
            long r6 = r4.lastModified()     // Catch:{ ParseException -> 0x00a2 }
            r5.<init>(r6)     // Catch:{ ParseException -> 0x00a2 }
            boolean r0 = r0.after(r5)     // Catch:{ ParseException -> 0x00a2 }
            if (r0 == 0) goto L_0x00a6
            r0 = r1
        L_0x007e:
            if (r0 == 0) goto L_0x00a8
            com.adwo.adsdk.U.a(r4)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = com.adwo.adsdk.K.O
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r0.<init>(r2)
            int r2 = r9.adid
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = java.io.File.separator
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r8.a(r9, r0, r3, r1)
        L_0x00a1:
            return
        L_0x00a2:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00a6:
            r0 = r2
            goto L_0x007e
        L_0x00a8:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = com.adwo.adsdk.K.O
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r0.<init>(r1)
            int r1 = r9.adid
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = java.io.File.separator
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r1 = "/object.temp"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.io.File r1 = new java.io.File
            r1.<init>(r0)
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x00e1
            boolean r2 = r1.isFile()
            if (r2 == 0) goto L_0x00e1
            r1.delete()
        L_0x00e1:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "file://"
            r1.<init>(r2)
            java.lang.String r2 = com.adwo.adsdk.K.O
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r9.adid
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = java.io.File.separator
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r2 = "/index.html?w="
            java.lang.StringBuilder r1 = r1.append(r2)
            android.util.DisplayMetrics r2 = r8.n
            int r2 = r2.widthPixels
            float r2 = (float) r2
            android.util.DisplayMetrics r3 = r8.n
            float r3 = r3.density
            float r2 = r2 / r3
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&h="
            java.lang.StringBuilder r1 = r1.append(r2)
            android.util.DisplayMetrics r2 = r8.n
            int r2 = r2.heightPixels
            float r2 = (float) r2
            android.util.DisplayMetrics r3 = r8.n
            float r3 = r3.density
            float r2 = r2 / r3
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r9.showURL = r1
            com.adwo.adsdk.U.a(r9, r0)
            goto L_0x00a1
        L_0x0131:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = com.adwo.adsdk.K.O
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r0.<init>(r2)
            int r2 = r9.adid
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = java.io.File.separator
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r8.a(r9, r0, r3, r1)
            goto L_0x00a1
        L_0x0151:
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "The ad file's url is not valid."
            android.util.Log.e(r0, r1)
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.AdDisplayer.b(com.adwo.adsdk.AdDisplayer, com.adwo.adsdk.FullScreenAd):void");
    }

    private void a(FullScreenAd fullScreenAd, String str, String str2, boolean z) {
        new C0026g(this, z, str2).execute(fullScreenAd.showURL, str, str2, fullScreenAd);
    }

    private void a(View view) {
        f = view;
        setAnimation(this.v.animationType);
        b(this.n.widthPixels, this.n.heightPixels);
        a(view, 0, 0, this.n.widthPixels, this.n.heightPixels);
        if (K.w.containsKey(Integer.valueOf(this.v.adid))) {
            K.w.put(Integer.valueOf(this.v.adid), Integer.valueOf(((Integer) K.w.get(Integer.valueOf(this.v.adid))).intValue() + 1));
        } else {
            K.w.put(Integer.valueOf(this.v.adid), 1);
        }
        K.M = K.L.format(new Date());
        int a2 = U.a(m, K.M);
        if (a2 == 0) {
            U.a(m);
            U.a(K.M, 1, m);
        } else {
            a2++;
            U.a(K.M, a2, m);
        }
        if (K.S > 0 && a2 >= K.S) {
            K.T = 1;
        }
        if (this.v.packageList != null && this.v.packageList.size() > 0) {
            for (String j2 : this.v.packageList) {
                if (U.j(m, j2)) {
                    U.b = this.v.adid;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(View view, int i2, int i3, int i4, int i5) {
        this.q = m.getResources().getConfiguration().orientation;
        if (m instanceof Activity) {
            try {
                Activity activity = (Activity) m;
                if (activity != null) {
                    if (activity.isFinishing() || activity.isRestricted()) {
                        Log.e("Adwo SDK", "The context of activity is not valid anymore.");
                        return;
                    }
                    activity.runOnUiThread(new C0027h(this, activity));
                }
            } catch (Exception e2) {
            }
            try {
                view.post(new C0028i(this, view, i2, i3, i4, i5));
                if (this.q == 1) {
                    this.h.loadUrl("javascript:adwoFSAdOrientationChanged(0);");
                } else {
                    this.h.loadUrl("javascript:adwoFSAdOrientationChanged(1);");
                }
                int i6 = 0;
                try {
                    if (this.q == 2) {
                        i6 = 2;
                    }
                } catch (Exception e3) {
                }
                this.h.loadUrl("javascript:adwoFSAdHasShown(" + i6 + ");");
            } catch (Exception e4) {
                e4.printStackTrace();
                b(view, i2, i3, i4, i5);
            }
        } else {
            Log.e("Adwo SDK", "The context param must be an instance of Activity.");
        }
    }

    private void b(View view, int i2, int i3, int i4, int i5) {
        try {
            a(this.n.widthPixels, this.n.heightPixels);
            view.post(new C0029j(this, view, i2, i3, i4, i5));
        } catch (Exception e2) {
            e2.printStackTrace();
            this.w++;
            if (this.w < 3) {
                b(view, i2, i3, i4, i5);
            } else {
                this.w = 0;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3, boolean z) {
        dismissDisplayer();
        if (!z) {
            b(i2, i3);
        } else {
            this.j = new RelativeLayout(m);
            this.j.setGravity(16);
            this.h = new WebView(m);
            this.h.setId(this.i);
            this.h.setBackgroundColor(0);
            this.h.setWebViewClient(new C0036q(this));
            this.h.setWebChromeClient(new C0030k(this));
            this.h.setScrollBarStyle(0);
            this.h.setVerticalScrollBarEnabled(false);
            this.h.setHorizontalScrollBarEnabled(false);
            CookieManager.getInstance().setAcceptCookie(true);
            WebSettings settings = this.h.getSettings();
            settings.setDefaultTextEncodingName(e.f);
            settings.setGeolocationEnabled(true);
            settings.setSupportZoom(false);
            settings.setBuiltInZoomControls(false);
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            settings.setJavaScriptEnabled(true);
            settings.setPluginsEnabled(true);
            settings.setSaveFormData(true);
            settings.setLightTouchEnabled(true);
            if (K.q == 0) {
                settings.setAppCacheEnabled(true);
                settings.setDatabaseEnabled(true);
                settings.setDomStorageEnabled(true);
                settings.setCacheMode(-1);
                settings.setAppCacheMaxSize(8388608);
                if (K.a == null) {
                    K.a = "/data/data/" + m.getPackageName() + "/cache";
                }
                settings.setAppCachePath(K.a);
            }
            this.h.setOnTouchListener(new C0023d(this));
            this.h.addJavascriptInterface(new C0024e(this), "adwo");
            this.j.addView(this.h);
        }
        a(i2, i3);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.g != null) {
            this.g.b();
            this.g = null;
        }
    }

    static /* synthetic */ void i(AdDisplayer adDisplayer) {
    }

    static /* synthetic */ void a(AdDisplayer adDisplayer, StringTokenizer stringTokenizer) {
    }
}
