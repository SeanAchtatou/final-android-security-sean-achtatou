package com.adwo.adsdk;

import android.os.AsyncTask;

/* renamed from: com.adwo.adsdk.f  reason: case insensitive filesystem */
final class C0025f extends AsyncTask {
    private /* synthetic */ AdDisplayer a;

    C0025f(AdDisplayer adDisplayer) {
        this.a = adDisplayer;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return K.b(AdDisplayer.m, (byte) 0);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        FullScreenAd fullScreenAd = (FullScreenAd) obj;
        if (fullScreenAd != null) {
            this.a.v = fullScreenAd;
            if (fullScreenAd.err != null) {
                if (this.a.u != null) {
                    this.a.u.onFailedToReceiveAd(fullScreenAd.err);
                }
                this.a.v = (FullScreenAd) null;
            } else if (AdDisplayer.b == 1) {
                AdDisplayer.b(this.a, fullScreenAd);
            } else if (this.a.u != null) {
                this.a.u.onReceiveAd();
            }
        } else if (this.a.u != null) {
            this.a.u.onFailedToReceiveAd(new ErrorCode(30, "OTHER_ERROR_INTERNET"));
        }
    }
}
