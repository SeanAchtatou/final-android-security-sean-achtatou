package com.adwo.adsdk;

/* renamed from: com.adwo.adsdk.am  reason: case insensitive filesystem */
final class C0015am implements Runnable {
    final /* synthetic */ C0014al a;

    C0015am(C0014al alVar) {
        this.a = alVar;
    }

    public final void run() {
        String a2 = U.a(this.a.a.b, this.a.a.a, new StringBuilder(String.valueOf(this.a.a.d)).toString(), new String(K.b), K.h);
        if (this.a.a.c != null) {
            try {
                this.a.a.c.post(new C0016an(this, a2));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
