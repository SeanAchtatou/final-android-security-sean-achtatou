package com.adwo.adsdk;

import android.app.Activity;
import android.widget.Toast;

final class W implements Runnable {
    private final /* synthetic */ Activity a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    W(Activity activity, String str, String str2) {
        this.a = activity;
        this.b = str;
        this.c = str2;
    }

    public final void run() {
        Toast.makeText(this.a, "下载\n" + this.b + "\n至目录:" + this.c, 1).show();
    }
}
