package com.adwo.adsdk;

import android.content.Context;
import android.widget.Toast;

/* renamed from: com.adwo.adsdk.af  reason: case insensitive filesystem */
final class C0008af implements Runnable {
    private final /* synthetic */ Context a;
    private final /* synthetic */ String b;

    C0008af(C0004ab abVar, Context context, String str) {
        this.a = context;
        this.b = str;
    }

    public final void run() {
        Toast.makeText(this.a, String.valueOf(this.b) + "\n已经被下载到:" + K.N + " 目录下。", 1).show();
    }
}
