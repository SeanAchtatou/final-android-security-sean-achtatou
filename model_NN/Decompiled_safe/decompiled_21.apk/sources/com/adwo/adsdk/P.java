package com.adwo.adsdk;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.RelativeLayout;

final class P extends AsyncTask {
    /* access modifiers changed from: private */
    public /* synthetic */ AdwoAdView a;

    P(AdwoAdView adwoAdView) {
        this.a = adwoAdView;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return K.a(this.a.getContext(), AdwoAdView.i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.adwo.adsdk.AdwoAdView.a(com.adwo.adsdk.AdwoAdView, boolean):void
     arg types: [com.adwo.adsdk.AdwoAdView, int]
     candidates:
      com.adwo.adsdk.AdwoAdView.a(com.adwo.adsdk.AdwoAdView, com.adwo.adsdk.z):void
      com.adwo.adsdk.AdwoAdView.a(com.adwo.adsdk.AdwoAdView, boolean):void */
    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        I i = (I) obj;
        Context context = this.a.getContext();
        if (i != null) {
            if (i.a() == null) {
                synchronized (this) {
                    this.a.n = false;
                    int d = P.super.getVisibility();
                    C0045z zVar = new C0045z(i, context);
                    zVar.setVisibility(d);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.a.a(), this.a.b());
                    layoutParams.addRule(14);
                    zVar.setLayoutParams(layoutParams);
                    if (this.a.d != null) {
                        this.a.d.onReceiveAd(this.a);
                    }
                    this.a.p.post(new Q(this, zVar, i, context));
                }
            } else if (this.a.d == null) {
            } else {
                if (i != null) {
                    this.a.d.onFailedToReceiveAd(this.a, i.a());
                    return;
                }
                this.a.d.onFailedToReceiveAd(this.a, new ErrorCode(-22, "ERR_SOCKET_TIMEOUT"));
            }
        } else if (this.a.d == null) {
        } else {
            if (i != null) {
                this.a.d.onFailedToReceiveAd(this.a, i.a());
                return;
            }
            this.a.d.onFailedToReceiveAd(this.a, new ErrorCode(-22, "ERR_SOCKET_TIMEOUT"));
        }
    }
}
