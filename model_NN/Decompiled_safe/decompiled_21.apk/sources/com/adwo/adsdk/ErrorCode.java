package com.adwo.adsdk;

public final class ErrorCode {
    int errorCode;
    String errorString;

    protected ErrorCode(int i, String str) {
        this.errorCode = i;
        this.errorString = str;
    }

    public final int getErrorCode() {
        return this.errorCode;
    }

    /* access modifiers changed from: protected */
    public final void setErrorCode(int i) {
        this.errorCode = i;
    }

    public final String getErrorString() {
        return this.errorString;
    }

    /* access modifiers changed from: protected */
    public final void setErrorString(String str) {
        this.errorString = str;
    }

    public final String toString() {
        return this.errorString;
    }
}
