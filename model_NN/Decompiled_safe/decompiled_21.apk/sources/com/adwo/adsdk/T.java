package com.adwo.adsdk;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.ViewGroup;

public final class T extends AlertDialog {
    private S a;
    private int b;
    private int c;
    private int d;
    private ViewGroup.LayoutParams e;

    public final void dismiss() {
        super.dismiss();
        this.a = null;
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        super.onCreate(bundle);
        this.e = new ViewGroup.LayoutParams(this.b, this.c + this.d);
        setContentView(null, this.e);
    }
}
