package com.adwo.adsdk;

/* renamed from: com.adwo.adsdk.aq  reason: case insensitive filesystem */
final class C0019aq implements Comparable {
    protected double a = 0.0d;
    protected String b = null;
    protected String c = null;
    protected String d = null;
    protected int e = -1;

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        C0019aq aqVar = (C0019aq) obj;
        if (this.a > aqVar.a) {
            return 1;
        }
        return this.a < aqVar.a ? -1 : 0;
    }

    C0019aq() {
    }

    public final String toString() {
        return String.valueOf(this.a) + ":" + this.b + ":" + this.c;
    }
}
