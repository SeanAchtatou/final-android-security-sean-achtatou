package com.adwo.adsdk;

import android.media.MediaRecorder;
import android.util.Log;

/* renamed from: com.adwo.adsdk.ak  reason: case insensitive filesystem */
final class C0013ak implements MediaRecorder.OnErrorListener {
    private /* synthetic */ C0012aj a;

    C0013ak(C0012aj ajVar) {
        this.a = ajVar;
    }

    public final void onError(MediaRecorder mediaRecorder, int i, int i2) {
        Log.i("MediaRecorder", "setOnErrorListener " + i);
        this.a.b();
    }
}
