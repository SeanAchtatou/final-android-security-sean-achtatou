package com.adwo.adsdk;

import android.media.MediaPlayer;
import android.os.Message;

final class C implements MediaPlayer.OnCompletionListener {
    private /* synthetic */ B a;
    private final /* synthetic */ String b;

    C(B b2, String str) {
        this.a = b2;
        this.b = str;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        if (this.a.a.a.l < this.a.a.a.k) {
            Message obtainMessage = this.a.a.a.p.obtainMessage(2);
            obtainMessage.obj = this.b;
            this.a.a.a.p.dispatchMessage(obtainMessage);
            return;
        }
        this.a.a.a.k = 1;
        this.a.a.a.l = 0;
        C0045z.a = false;
    }
}
