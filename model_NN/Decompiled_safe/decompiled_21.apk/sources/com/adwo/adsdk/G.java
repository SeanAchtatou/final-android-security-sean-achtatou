package com.adwo.adsdk;

import android.app.Activity;

final class G implements Runnable {
    final /* synthetic */ E a;
    private final /* synthetic */ String b;

    G(E e, String str) {
        this.a = e;
        this.b = str;
    }

    public final void run() {
        U.a(this.b, (Activity) this.a.a.getContext());
        try {
            this.a.a.post(new H(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
