package com.adwo.adsdk;

import android.app.Dialog;
import android.media.MediaPlayer;
import android.webkit.WebView;

/* renamed from: com.adwo.adsdk.v  reason: case insensitive filesystem */
final class C0041v implements MediaPlayer.OnCompletionListener {
    private final /* synthetic */ Dialog a;
    private final /* synthetic */ WebView b;

    C0041v(C0036q qVar, Dialog dialog, WebView webView) {
        this.a = dialog;
        this.b = webView;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (this.a != null) {
            this.a.dismiss();
        }
        try {
            this.b.loadUrl("javascript:adwoDoPlayVideoComplete();");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
