package com.adwo.adsdk;

import android.content.Context;
import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

/* renamed from: com.adwo.adsdk.ap  reason: case insensitive filesystem */
final class C0018ap extends AsyncTask {
    private /* synthetic */ C0017ao a;
    private final /* synthetic */ Context b;

    C0018ap(C0017ao aoVar, Context context) {
        this.a = aoVar;
        this.b = context;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object... objArr) {
        return a();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        ArrayList arrayList = (ArrayList) obj;
        if (arrayList != null && arrayList.size() > 1) {
            try {
                C0017ao.h = (String) arrayList.get(0);
                C0017ao.a(this.a, "strategy", C0017ao.h, this.b);
                int parseInt = Integer.parseInt((String) arrayList.get(1));
                if (parseInt >= 20) {
                    C0017ao.a = parseInt;
                }
                C0017ao.a(this.a, "requestInterval", new StringBuilder().append(parseInt).toString(), this.b);
                int parseInt2 = Integer.parseInt((String) arrayList.get(2));
                if (parseInt2 > 20) {
                    K.R = parseInt2;
                }
                C0017ao.a(this.a, "fs_requestInterval", new StringBuilder().append(parseInt2).toString(), this.b);
                int parseInt3 = Integer.parseInt((String) arrayList.get(3));
                if (parseInt3 > 0) {
                    K.S = parseInt3;
                }
                C0017ao.a(this.a, "fs_request_limit", new StringBuilder().append(parseInt3).toString(), this.b);
                K.U = Integer.parseInt((String) arrayList.get(4));
                String str = (String) arrayList.get(5);
                if (!"".equals(str)) {
                    U.a(this.b, (String) arrayList.get(6), str);
                }
                String str2 = (String) arrayList.get(7);
                if ("".equals(str2) || "0".equals(str2)) {
                    K.D = new int[0];
                } else {
                    String[] split = str2.split(",");
                    K.D = new int[split.length];
                    for (int i = 0; i < split.length; i++) {
                        K.D[i] = Integer.parseInt(split[i]);
                    }
                }
                C0017ao.a(this.a, "filter_ad_list", str2, this.b);
                int size = arrayList.size();
                for (int i2 = 8; i2 < size; i2++) {
                    String str3 = (String) arrayList.get(i2);
                    StringTokenizer stringTokenizer = new StringTokenizer(str3, "=");
                    C0019aq aqVar = new C0019aq();
                    aqVar.b = stringTokenizer.nextToken();
                    aqVar.a = Double.parseDouble(stringTokenizer.nextToken());
                    aqVar.c = stringTokenizer.nextToken();
                    try {
                        String nextToken = stringTokenizer.nextToken();
                        if (nextToken != null) {
                            aqVar.e = Integer.decode(nextToken).intValue();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        aqVar.d = stringTokenizer.nextToken();
                    } catch (Exception e2) {
                    }
                    this.a.g.add(aqVar);
                    C0017ao.a(this.a, new StringBuilder(String.valueOf(i2)).toString(), str3, this.b);
                    C0017ao aoVar = this.a;
                    aoVar.e = aoVar.e + ((int) (aqVar.a * 10.0d));
                    for (int b2 = this.a.e; b2 < this.a.e; b2++) {
                        this.a.f.put(Integer.valueOf(b2), Integer.valueOf(this.a.g.size() - 1));
                    }
                }
                C0017ao aoVar2 = this.a;
                aoVar2.e = aoVar2.e - 1;
                C0017ao.a(this.a, "update_date", K.L.format(new Date()), this.b);
                C0017ao.a(this.a, "configure_version", "4.0", this.b);
                return;
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        this.a.c(this.b);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069 A[SYNTHETIC, Splitter:B:18:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006e A[Catch:{ IOException -> 0x008a }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[Catch:{ Exception -> 0x0097 }, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.ArrayList a() {
        /*
            r6 = this;
            r1 = 0
            java.lang.String r0 = "http://apiconfig.adwo.com/adwo/a2"
            java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x008c }
            r2.<init>(r0)     // Catch:{ Exception -> 0x008c }
            java.net.URLConnection r0 = r2.openConnection()     // Catch:{ Exception -> 0x008c }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x008c }
            r2 = 1
            r0.setDoOutput(r2)     // Catch:{ Exception -> 0x0091 }
            r2 = 1
            r0.setDoInput(r2)     // Catch:{ Exception -> 0x0091 }
            r2 = 10000(0x2710, float:1.4013E-41)
            r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x0091 }
            r2 = 10000(0x2710, float:1.4013E-41)
            r0.setReadTimeout(r2)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r2 = "POST"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x0091 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0091 }
            java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ Exception -> 0x0091 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0091 }
            com.adwo.adsdk.ao r3 = r6.a     // Catch:{ Exception -> 0x0091 }
            android.content.Context r4 = r6.b     // Catch:{ Exception -> 0x0091 }
            java.lang.String r3 = com.adwo.adsdk.C0017ao.b(r4)     // Catch:{ Exception -> 0x0091 }
            r2.writeBytes(r3)     // Catch:{ Exception -> 0x0091 }
            r2.flush()     // Catch:{ Exception -> 0x0091 }
            r2.close()     // Catch:{ Exception -> 0x0091 }
            r0.connect()     // Catch:{ Exception -> 0x0091 }
            java.io.InputStream r4 = r0.getInputStream()     // Catch:{ Exception -> 0x0091 }
            int r2 = r0.getResponseCode()     // Catch:{ Exception -> 0x0097 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 == r3) goto L_0x0050
            r0 = r1
        L_0x004f:
            return r0
        L_0x0050:
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0097 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0097 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0097 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0097 }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x009d }
            r2.<init>()     // Catch:{ Exception -> 0x009d }
        L_0x005f:
            java.lang.String r1 = r3.readLine()     // Catch:{ Exception -> 0x007b }
            if (r1 != 0) goto L_0x0077
            r1 = r0
            r0 = r2
        L_0x0067:
            if (r4 == 0) goto L_0x006c
            r4.close()     // Catch:{ IOException -> 0x008a }
        L_0x006c:
            if (r3 == 0) goto L_0x0071
            r3.close()     // Catch:{ IOException -> 0x008a }
        L_0x0071:
            if (r1 == 0) goto L_0x004f
            r1.disconnect()
            goto L_0x004f
        L_0x0077:
            r2.add(r1)     // Catch:{ Exception -> 0x007b }
            goto L_0x005f
        L_0x007b:
            r1 = move-exception
            r5 = r1
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r0
            r0 = r5
        L_0x0082:
            r0.printStackTrace()
            r0 = r1
            r1 = r4
            r4 = r3
            r3 = r2
            goto L_0x0067
        L_0x008a:
            r2 = move-exception
            goto L_0x0071
        L_0x008c:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
            goto L_0x0082
        L_0x0091:
            r2 = move-exception
            r3 = r1
            r4 = r0
            r0 = r2
            r2 = r1
            goto L_0x0082
        L_0x0097:
            r2 = move-exception
            r3 = r4
            r4 = r0
            r0 = r2
            r2 = r1
            goto L_0x0082
        L_0x009d:
            r2 = move-exception
            r5 = r2
            r2 = r3
            r3 = r4
            r4 = r0
            r0 = r5
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0018ap.a():java.util.ArrayList");
    }
}
