package com.adwo.adsdk;

import android.content.Context;
import android.widget.Toast;

/* renamed from: com.adwo.adsdk.ac  reason: case insensitive filesystem */
final class C0005ac implements Runnable {
    private final /* synthetic */ Context a;

    C0005ac(C0004ab abVar, Context context) {
        this.a = context;
    }

    public final void run() {
        Toast.makeText(this.a, "设备没有外部存储卡，不能支持下载。", 0).show();
    }
}
