package com.adwo.adsdk;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.EditText;

public final class aC extends WebChromeClient {
    protected aC(ar arVar) {
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ar.j);
        builder.setMessage(str2).setPositiveButton("确定", (DialogInterface.OnClickListener) null);
        builder.setCancelable(false);
        builder.create().show();
        jsResult.confirm();
        return true;
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ar.j);
        builder.setMessage(str2).setPositiveButton("确定", new aD(this, jsResult)).setNeutralButton("取消", new aE(this, jsResult));
        builder.setOnCancelListener(new aF(this, jsResult));
        builder.create().show();
        return true;
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ar.j);
        builder.setMessage(str2);
        EditText editText = new EditText(webView.getContext());
        editText.setSingleLine();
        editText.setText(str3);
        builder.setView(editText).setPositiveButton("确定", new aG(this, jsPromptResult, editText)).setNeutralButton("取消", new aH(this, jsPromptResult));
        builder.create().show();
        return true;
    }
}
