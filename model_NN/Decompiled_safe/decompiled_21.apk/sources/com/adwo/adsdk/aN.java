package com.adwo.adsdk;

import android.app.Dialog;
import android.media.MediaPlayer;
import android.webkit.WebView;

final class aN implements MediaPlayer.OnCompletionListener {
    private final /* synthetic */ Dialog a;
    private final /* synthetic */ WebView b;

    aN(aI aIVar, Dialog dialog, WebView webView) {
        this.a = dialog;
        this.b = webView;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (this.a != null) {
            this.a.dismiss();
        }
        try {
            this.b.loadUrl("javascript:adwoDoPlayVideoComplete();");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
