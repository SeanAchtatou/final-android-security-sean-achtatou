package igudi.com.ergushi;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Xml;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.umeng.common.a;
import com.umeng.common.b.e;
import com.umeng.fb.f;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

public final class AppConnect {
    /* access modifiers changed from: private */
    public static boolean L = true;
    public static final String LIBRARY_VERSION_NUMBER = "1.8.8";
    /* access modifiers changed from: private */
    public static boolean M = false;
    /* access modifiers changed from: private */
    public static String aA = ak.v();
    /* access modifiers changed from: private */
    public static String aB = "install";
    private static boolean aD = true;
    private static String aE = "";
    private static String aF = "";
    private static boolean aH = true;
    private static boolean aI = true;
    private static boolean aL = true;
    private static Map aM = null;
    private static boolean aN = false;
    /* access modifiers changed from: private */
    public static boolean aO = true;
    /* access modifiers changed from: private */
    public static boolean aP = false;
    private static o aQ;
    /* access modifiers changed from: private */
    public static String aR = "";
    private static r aU = null;
    private static boolean aX = true;
    private static boolean aY = true;
    private static final byte[] aZ = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    /* access modifiers changed from: private */
    public static UpdatePointsNotifier at;
    /* access modifiers changed from: private */
    public static String az = "";
    protected static String b = "";
    private static int ba = 0;
    private static String bb = "";
    protected static String c = "";
    protected static ComponentName d;
    protected static ActivityManager.RunningTaskInfo e;
    protected static Map f;
    public static boolean finalize_tag = false;
    protected static int g = Color.argb(50, 240, 240, 240);
    protected static int h = -1;
    protected static List i = null;
    protected static String j = "ADVIEW";
    protected static boolean k = false;
    protected static boolean m = true;
    protected static List n = null;
    protected static String o = "";
    private static AppConnect r = null;
    /* access modifiers changed from: private */
    public static af s = null;
    /* access modifiers changed from: private */
    public static String z = "";
    private String A = "";
    private String B = "";
    private String C = "";
    /* access modifiers changed from: private */
    public String D = "";
    /* access modifiers changed from: private */
    public String E = "";
    private String F = "";
    /* access modifiers changed from: private */
    public String G = "";
    private String H = "";
    private int I = 0;
    private int J = 0;
    /* access modifiers changed from: private */
    public String K = "";
    private String N = "";
    private String O = "";
    private String P = "";
    private String Q = "";
    private String R = be.c(ak.l);
    /* access modifiers changed from: private */
    public String S = "";
    private String T = "";
    private final String U = "root";
    private final String V = "base";
    private final String W = "mac";
    private final String X = "x";
    private final String Y = "y";
    private final String Z = "net";
    protected Context a = null;
    private int aC;
    private String aG = "";
    private String aJ = "";
    private String aK = "";
    /* access modifiers changed from: private */
    public String aS = "";
    /* access modifiers changed from: private */
    public String aT = "";
    /* access modifiers changed from: private */
    public String aV = "";
    /* access modifiers changed from: private */
    public String aW = "";
    private final String aa = "imsi";
    private final String ab = "udid";
    private final String ac = "device_name";
    private final String ad = "device_type";
    private final String ae = f.aj;
    private final String af = "country_code";
    private final String ag = "language";
    private final String ah = f.ai;
    private final String ai = "sdk_version";
    private final String aj = "act";
    private final String ak = a.d;
    private final String al = "device_brand";
    private final String am = "points";
    private final String an = "points";
    private final String ao = "install";
    private final String ap = "uninstall";
    private final String aq = "load";
    private final String ar = "device_width";
    private final String as = "device_height";
    private t au = null;
    private y av = null;
    private s aw = null;
    private w ax = null;
    /* access modifiers changed from: private */
    public v ay = null;
    /* access modifiers changed from: private */
    public boolean bc = true;
    private Dialog bd;
    private x be;
    /* access modifiers changed from: private */
    public List bf;
    private final Handler bg = new Handler();
    boolean l = true;
    private final ScheduledExecutorService p = Executors.newScheduledThreadPool(1);
    private z q = null;
    private String t = "";
    private String u = "";
    private String v = "";
    private String w = "";
    private String x = "";
    /* access modifiers changed from: private */
    public String y = "";

    public AppConnect() {
    }

    private AppConnect(Context context) {
        i(context);
        c = c(context);
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.q = new z(this, null);
        this.q.execute(new Void[0]);
        new ar().a(context);
    }

    private LinearLayout a(AdInfo adInfo, Context context, Dialog dialog) {
        LinearLayout linearLayout;
        Exception exc;
        LinearLayout.LayoutParams layoutParams;
        try {
            int displaySize = SDKUtils.getDisplaySize(context);
            LinearLayout linearLayout2 = new LinearLayout(context);
            try {
                linearLayout2.setGravity(17);
                int h2 = adInfo.h();
                if (h2 != -1) {
                    linearLayout2.setBackgroundColor(Color.argb(h2, 0, 0, 0));
                } else {
                    linearLayout2.setBackgroundColor(Color.argb(180, 0, 0, 0));
                }
                RelativeLayout relativeLayout = new RelativeLayout(context);
                relativeLayout.setGravity(17);
                ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(new float[]{6.0f, 6.0f, 6.0f, 6.0f, 6.0f, 6.0f, 6.0f, 6.0f}, null, null));
                shapeDrawable.getPaint().setARGB(0, 255, 255, 255);
                relativeLayout.setBackgroundDrawable(shapeDrawable);
                LinearLayout a2 = a(adInfo, context, dialog, 0, 0);
                a2.setId(1);
                LinearLayout linearLayout3 = new LinearLayout(context);
                linearLayout3.setGravity(5);
                ImageView imageView = new ImageView(context);
                if (displaySize == 320) {
                    linearLayout3.setPadding(0, 4, 4, 0);
                    layoutParams = new LinearLayout.LayoutParams(33, 33);
                } else if (displaySize == 240) {
                    linearLayout3.setPadding(0, 3, 3, 0);
                    layoutParams = new LinearLayout.LayoutParams(25, 25);
                } else if (displaySize == 720) {
                    linearLayout3.setPadding(0, 8, 8, 0);
                    layoutParams = new LinearLayout.LayoutParams(55, 55);
                } else if (displaySize == 1080) {
                    linearLayout3.setPadding(0, 10, 10, 0);
                    layoutParams = new LinearLayout.LayoutParams(80, 80);
                } else {
                    linearLayout3.setPadding(0, 5, 5, 0);
                    layoutParams = new LinearLayout.LayoutParams(40, 40);
                }
                imageView.setLayoutParams(layoutParams);
                Bitmap i2 = adInfo.i();
                if (i2 != null) {
                    imageView.setImageBitmap(i2);
                } else {
                    imageView.setImageResource(17301594);
                }
                imageView.setMinimumWidth(40);
                imageView.setMinimumHeight(40);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setVisibility(4);
                this.bg.postDelayed(new ab(this, imageView), (long) (adInfo.g() * 1000));
                linearLayout3.addView(imageView);
                linearLayout3.setOnClickListener(new j(this, dialog));
                RelativeLayout.LayoutParams layoutParams2 = displaySize == 320 ? new RelativeLayout.LayoutParams(270, 270) : displaySize == 240 ? new RelativeLayout.LayoutParams(180, 180) : new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(13);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.addRule(7, a2.getId());
                layoutParams3.addRule(6, a2.getId());
                relativeLayout.addView(a2, layoutParams2);
                relativeLayout.addView(linearLayout3, layoutParams3);
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.3f, 1.0f);
                alphaAnimation.setDuration(500);
                alphaAnimation.setFillAfter(true);
                alphaAnimation.setInterpolator(new DecelerateInterpolator());
                linearLayout2.startAnimation(alphaAnimation);
                linearLayout2.addView(relativeLayout);
                return linearLayout2;
            } catch (Exception e2) {
                exc = e2;
                linearLayout = linearLayout2;
                exc.printStackTrace();
                return linearLayout;
            }
        } catch (Exception e3) {
            Exception exc2 = e3;
            linearLayout = null;
            exc = exc2;
            exc.printStackTrace();
            return linearLayout;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0312 A[Catch:{ Exception -> 0x02cd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.widget.LinearLayout a(igudi.com.ergushi.AdInfo r23, android.content.Context r24, android.app.Dialog r25, int r26, int r27) {
        /*
            r22 = this;
            r4 = 0
            int r6 = igudi.com.ergushi.SDKUtils.getDisplaySize(r24)     // Catch:{ Exception -> 0x039f }
            android.widget.LinearLayout r16 = new android.widget.LinearLayout     // Catch:{ Exception -> 0x039f }
            r0 = r16
            r1 = r24
            r0.<init>(r1)     // Catch:{ Exception -> 0x039f }
            r3 = 17
            r0 = r16
            r0.setGravity(r3)     // Catch:{ Exception -> 0x02cd }
            android.widget.RelativeLayout r17 = new android.widget.RelativeLayout     // Catch:{ Exception -> 0x02cd }
            r0 = r17
            r1 = r24
            r0.<init>(r1)     // Catch:{ Exception -> 0x02cd }
            r3 = 17
            r0 = r17
            r0.setGravity(r3)     // Catch:{ Exception -> 0x02cd }
            android.widget.LinearLayout r9 = new android.widget.LinearLayout     // Catch:{ Exception -> 0x02cd }
            r0 = r24
            r9.<init>(r0)     // Catch:{ Exception -> 0x02cd }
            r3 = 1
            r9.setId(r3)     // Catch:{ Exception -> 0x02cd }
            r3 = 17
            r9.setGravity(r3)     // Catch:{ Exception -> 0x02cd }
            if (r26 == 0) goto L_0x0274
            if (r27 == 0) goto L_0x0274
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r0 = r26
            r1 = r27
            r3.<init>(r0, r1)     // Catch:{ Exception -> 0x02cd }
            r5 = r3
        L_0x0043:
            r3 = 13
            r5.addRule(r3)     // Catch:{ Exception -> 0x02cd }
            android.widget.ImageView r18 = new android.widget.ImageView     // Catch:{ Exception -> 0x02cd }
            r0 = r18
            r1 = r24
            r0.<init>(r1)     // Catch:{ Exception -> 0x02cd }
            android.widget.ImageView$ScaleType r3 = android.widget.ImageView.ScaleType.FIT_END     // Catch:{ Exception -> 0x02cd }
            r0 = r18
            r0.setScaleType(r3)     // Catch:{ Exception -> 0x02cd }
            if (r26 == 0) goto L_0x02be
            if (r27 == 0) goto L_0x02be
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r0 = r26
            r1 = r27
            r3.<init>(r0, r1)     // Catch:{ Exception -> 0x02cd }
        L_0x0065:
            r0 = r18
            r0.setLayoutParams(r3)     // Catch:{ Exception -> 0x02cd }
            java.lang.String r3 = "/Android/data/cache/popCache"
            java.lang.String r7 = r23.a()     // Catch:{ Exception -> 0x02cd }
            r4 = 0
            boolean r8 = igudi.com.ergushi.be.b(r7)     // Catch:{ Exception -> 0x02cd }
            if (r8 != 0) goto L_0x030f
            java.lang.String r8 = r23.getAdId()     // Catch:{ Exception -> 0x030b }
            r0 = r22
            r1 = r24
            android.graphics.Bitmap r3 = r0.b(r1, r7, r8, r3)     // Catch:{ Exception -> 0x030b }
        L_0x0083:
            if (r3 == 0) goto L_0x0312
            r0 = r18
            r0.setImageBitmap(r3)     // Catch:{ Exception -> 0x02cd }
            r0 = r18
            r9.addView(r0)     // Catch:{ Exception -> 0x02cd }
            android.widget.LinearLayout r7 = new android.widget.LinearLayout     // Catch:{ Exception -> 0x02cd }
            r0 = r24
            r7.<init>(r0)     // Catch:{ Exception -> 0x02cd }
            r3 = 4
            r7.setVisibility(r3)     // Catch:{ Exception -> 0x02cd }
            r3 = 20
            r4 = 320(0x140, float:4.48E-43)
            if (r6 != r4) goto L_0x0315
            r3 = 12
        L_0x00a2:
            r4 = 8
            float[] r4 = new float[r4]     // Catch:{ Exception -> 0x02cd }
            r8 = 0
            float r10 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r4[r8] = r10     // Catch:{ Exception -> 0x02cd }
            r8 = 1
            float r10 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r4[r8] = r10     // Catch:{ Exception -> 0x02cd }
            r8 = 2
            float r10 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r4[r8] = r10     // Catch:{ Exception -> 0x02cd }
            r8 = 3
            float r10 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r4[r8] = r10     // Catch:{ Exception -> 0x02cd }
            r8 = 4
            float r10 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r4[r8] = r10     // Catch:{ Exception -> 0x02cd }
            r8 = 5
            float r10 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r4[r8] = r10     // Catch:{ Exception -> 0x02cd }
            r8 = 6
            float r10 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r4[r8] = r10     // Catch:{ Exception -> 0x02cd }
            r8 = 7
            float r3 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r4[r8] = r3     // Catch:{ Exception -> 0x02cd }
            android.graphics.drawable.ShapeDrawable r3 = new android.graphics.drawable.ShapeDrawable     // Catch:{ Exception -> 0x02cd }
            android.graphics.drawable.shapes.RoundRectShape r8 = new android.graphics.drawable.shapes.RoundRectShape     // Catch:{ Exception -> 0x02cd }
            r10 = 0
            r11 = 0
            r8.<init>(r4, r10, r11)     // Catch:{ Exception -> 0x02cd }
            r3.<init>(r8)     // Catch:{ Exception -> 0x02cd }
            android.graphics.Paint r4 = r3.getPaint()     // Catch:{ Exception -> 0x02cd }
            r8 = 70
            r10 = 0
            r11 = 0
            r12 = 0
            int r8 = android.graphics.Color.argb(r8, r10, r11, r12)     // Catch:{ Exception -> 0x02cd }
            r4.setColor(r8)     // Catch:{ Exception -> 0x02cd }
            r7.setBackgroundDrawable(r3)     // Catch:{ Exception -> 0x02cd }
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r3 = -2
            r8 = -2
            r4.<init>(r3, r8)     // Catch:{ Exception -> 0x02cd }
            r3 = 13
            r4.addRule(r3)     // Catch:{ Exception -> 0x02cd }
            r3 = 6
            int r8 = r9.getId()     // Catch:{ Exception -> 0x02cd }
            r4.addRule(r3, r8)     // Catch:{ Exception -> 0x02cd }
            r3 = 5
            int r8 = r9.getId()     // Catch:{ Exception -> 0x02cd }
            r4.addRule(r3, r8)     // Catch:{ Exception -> 0x02cd }
            r3 = 8
            int r8 = r9.getId()     // Catch:{ Exception -> 0x02cd }
            r4.addRule(r3, r8)     // Catch:{ Exception -> 0x02cd }
            r3 = 7
            int r8 = r9.getId()     // Catch:{ Exception -> 0x02cd }
            r4.addRule(r3, r8)     // Catch:{ Exception -> 0x02cd }
            android.widget.LinearLayout r8 = new android.widget.LinearLayout     // Catch:{ Exception -> 0x02cd }
            r0 = r24
            r8.<init>(r0)     // Catch:{ Exception -> 0x02cd }
            r3 = 17
            r8.setGravity(r3)     // Catch:{ Exception -> 0x02cd }
            android.graphics.drawable.GradientDrawable r19 = new android.graphics.drawable.GradientDrawable     // Catch:{ Exception -> 0x02cd }
            android.graphics.drawable.GradientDrawable$Orientation r3 = android.graphics.drawable.GradientDrawable.Orientation.TOP_BOTTOM     // Catch:{ Exception -> 0x02cd }
            r10 = 4
            int[] r10 = new int[r10]     // Catch:{ Exception -> 0x02cd }
            r11 = 0
            java.lang.String r12 = "#FFC125"
            int r12 = android.graphics.Color.parseColor(r12)     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 1
            java.lang.String r12 = "#FFD700"
            int r12 = android.graphics.Color.parseColor(r12)     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 2
            java.lang.String r12 = "#FFB90F"
            int r12 = android.graphics.Color.parseColor(r12)     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 3
            java.lang.String r12 = "#FFB90F"
            int r12 = android.graphics.Color.parseColor(r12)     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r0 = r19
            r0.<init>(r3, r10)     // Catch:{ Exception -> 0x02cd }
            android.graphics.drawable.GradientDrawable r20 = new android.graphics.drawable.GradientDrawable     // Catch:{ Exception -> 0x02cd }
            android.graphics.drawable.GradientDrawable$Orientation r3 = android.graphics.drawable.GradientDrawable.Orientation.TOP_BOTTOM     // Catch:{ Exception -> 0x02cd }
            r10 = 4
            int[] r10 = new int[r10]     // Catch:{ Exception -> 0x02cd }
            r11 = 0
            java.lang.String r12 = "#cccccc"
            int r12 = android.graphics.Color.parseColor(r12)     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 1
            java.lang.String r12 = "#eeeeee"
            int r12 = android.graphics.Color.parseColor(r12)     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 2
            java.lang.String r12 = "#cccccc"
            int r12 = android.graphics.Color.parseColor(r12)     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 3
            java.lang.String r12 = "#cccccc"
            int r12 = android.graphics.Color.parseColor(r12)     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r0 = r20
            r0.<init>(r3, r10)     // Catch:{ Exception -> 0x02cd }
            r3 = 0
            r0 = r19
            r0.setShape(r3)     // Catch:{ Exception -> 0x02cd }
            r3 = 10
            r10 = 320(0x140, float:4.48E-43)
            if (r6 != r10) goto L_0x031d
            r3 = 6
        L_0x018b:
            r10 = 8
            float[] r10 = new float[r10]     // Catch:{ Exception -> 0x02cd }
            r11 = 0
            float r12 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 1
            float r12 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 2
            float r12 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 3
            float r12 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 4
            float r12 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 5
            float r12 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 6
            float r12 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r12     // Catch:{ Exception -> 0x02cd }
            r11 = 7
            float r3 = (float) r3     // Catch:{ Exception -> 0x02cd }
            r10[r11] = r3     // Catch:{ Exception -> 0x02cd }
            r0 = r19
            r0.setCornerRadii(r10)     // Catch:{ Exception -> 0x02cd }
            r0 = r20
            r0.setCornerRadii(r10)     // Catch:{ Exception -> 0x02cd }
            r0 = r19
            r8.setBackgroundDrawable(r0)     // Catch:{ Exception -> 0x02cd }
            android.widget.TextView r3 = new android.widget.TextView     // Catch:{ Exception -> 0x02cd }
            r0 = r24
            r3.<init>(r0)     // Catch:{ Exception -> 0x02cd }
            android.widget.LinearLayout$LayoutParams r10 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r11 = -1
            r12 = -1
            r10.<init>(r11, r12)     // Catch:{ Exception -> 0x02cd }
            r3.setLayoutParams(r10)     // Catch:{ Exception -> 0x02cd }
            r10 = 17
            r3.setGravity(r10)     // Catch:{ Exception -> 0x02cd }
            java.lang.String r10 = "立即免费下载"
            r3.setText(r10)     // Catch:{ Exception -> 0x02cd }
            r10 = 1097859072(0x41700000, float:15.0)
            r3.setTextSize(r10)     // Catch:{ Exception -> 0x02cd }
            r10 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r3.setTextColor(r10)     // Catch:{ Exception -> 0x02cd }
            r8.addView(r3)     // Catch:{ Exception -> 0x02cd }
            r3 = 320(0x140, float:4.48E-43)
            if (r6 != r3) goto L_0x0324
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r10 = 100
            r11 = 40
            r3.<init>(r10, r11)     // Catch:{ Exception -> 0x02cd }
        L_0x01f3:
            r10 = 14
            r3.addRule(r10)     // Catch:{ Exception -> 0x02cd }
            r10 = 8
            int r11 = r9.getId()     // Catch:{ Exception -> 0x02cd }
            r3.addRule(r10, r11)     // Catch:{ Exception -> 0x02cd }
            r10 = 5
            int r11 = r9.getId()     // Catch:{ Exception -> 0x02cd }
            r3.addRule(r10, r11)     // Catch:{ Exception -> 0x02cd }
            r10 = 7
            int r11 = r9.getId()     // Catch:{ Exception -> 0x02cd }
            r3.addRule(r10, r11)     // Catch:{ Exception -> 0x02cd }
            r10 = 320(0x140, float:4.48E-43)
            if (r6 != r10) goto L_0x035b
            r6 = 30
            r3.bottomMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 40
            r3.leftMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 40
            r3.rightMargin = r6     // Catch:{ Exception -> 0x02cd }
        L_0x0221:
            r6 = 4
            r8.setVisibility(r6)     // Catch:{ Exception -> 0x02cd }
            r0 = r17
            r0.addView(r9, r5)     // Catch:{ Exception -> 0x02cd }
            r0 = r17
            r0.addView(r7, r4)     // Catch:{ Exception -> 0x02cd }
            r0 = r17
            r0.addView(r8, r3)     // Catch:{ Exception -> 0x02cd }
            igudi.com.ergushi.k r3 = new igudi.com.ergushi.k     // Catch:{ Exception -> 0x02cd }
            r4 = r22
            r5 = r23
            r6 = r25
            r3.<init>(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x02cd }
            r8.setOnClickListener(r3)     // Catch:{ Exception -> 0x02cd }
            igudi.com.ergushi.l r9 = new igudi.com.ergushi.l     // Catch:{ Exception -> 0x02cd }
            r10 = r22
            r11 = r23
            r12 = r25
            r13 = r8
            r14 = r7
            r15 = r24
            r9.<init>(r10, r11, r12, r13, r14, r15)     // Catch:{ Exception -> 0x02cd }
            r0 = r18
            r0.setOnClickListener(r9)     // Catch:{ Exception -> 0x02cd }
            igudi.com.ergushi.m r3 = new igudi.com.ergushi.m     // Catch:{ Exception -> 0x02cd }
            r0 = r22
            r3.<init>(r0, r7, r8)     // Catch:{ Exception -> 0x02cd }
            r7.setOnClickListener(r3)     // Catch:{ Exception -> 0x02cd }
            igudi.com.ergushi.n r3 = new igudi.com.ergushi.n     // Catch:{ Exception -> 0x02cd }
            r0 = r22
            r1 = r20
            r2 = r19
            r3.<init>(r0, r8, r1, r2)     // Catch:{ Exception -> 0x02cd }
            r8.setOnTouchListener(r3)     // Catch:{ Exception -> 0x02cd }
            r16.addView(r17)     // Catch:{ Exception -> 0x02cd }
            r3 = r16
        L_0x0273:
            return r3
        L_0x0274:
            r3 = 320(0x140, float:4.48E-43)
            if (r6 != r3) goto L_0x0284
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = 270(0x10e, float:3.78E-43)
            r5 = 270(0x10e, float:3.78E-43)
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x02cd }
            r5 = r3
            goto L_0x0043
        L_0x0284:
            r3 = 240(0xf0, float:3.36E-43)
            if (r6 != r3) goto L_0x0294
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = 180(0xb4, float:2.52E-43)
            r5 = 180(0xb4, float:2.52E-43)
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x02cd }
            r5 = r3
            goto L_0x0043
        L_0x0294:
            r3 = 720(0x2d0, float:1.009E-42)
            if (r6 != r3) goto L_0x02a4
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = 600(0x258, float:8.41E-43)
            r5 = 600(0x258, float:8.41E-43)
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x02cd }
            r5 = r3
            goto L_0x0043
        L_0x02a4:
            r3 = 1080(0x438, float:1.513E-42)
            if (r6 != r3) goto L_0x02b4
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = 800(0x320, float:1.121E-42)
            r5 = 800(0x320, float:1.121E-42)
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x02cd }
            r5 = r3
            goto L_0x0043
        L_0x02b4:
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = -2
            r5 = -2
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x02cd }
            r5 = r3
            goto L_0x0043
        L_0x02be:
            r3 = 320(0x140, float:4.48E-43)
            if (r6 != r3) goto L_0x02d5
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = 270(0x10e, float:3.78E-43)
            r7 = 270(0x10e, float:3.78E-43)
            r3.<init>(r4, r7)     // Catch:{ Exception -> 0x02cd }
            goto L_0x0065
        L_0x02cd:
            r3 = move-exception
            r4 = r3
            r3 = r16
        L_0x02d1:
            r4.printStackTrace()
            goto L_0x0273
        L_0x02d5:
            r3 = 240(0xf0, float:3.36E-43)
            if (r6 != r3) goto L_0x02e4
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = 180(0xb4, float:2.52E-43)
            r7 = 180(0xb4, float:2.52E-43)
            r3.<init>(r4, r7)     // Catch:{ Exception -> 0x02cd }
            goto L_0x0065
        L_0x02e4:
            r3 = 720(0x2d0, float:1.009E-42)
            if (r6 != r3) goto L_0x02f3
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = 600(0x258, float:8.41E-43)
            r7 = 600(0x258, float:8.41E-43)
            r3.<init>(r4, r7)     // Catch:{ Exception -> 0x02cd }
            goto L_0x0065
        L_0x02f3:
            r3 = 1080(0x438, float:1.513E-42)
            if (r6 != r3) goto L_0x0302
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = 800(0x320, float:1.121E-42)
            r7 = 800(0x320, float:1.121E-42)
            r3.<init>(r4, r7)     // Catch:{ Exception -> 0x02cd }
            goto L_0x0065
        L_0x0302:
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r4 = -2
            r7 = -2
            r3.<init>(r4, r7)     // Catch:{ Exception -> 0x02cd }
            goto L_0x0065
        L_0x030b:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ Exception -> 0x02cd }
        L_0x030f:
            r3 = r4
            goto L_0x0083
        L_0x0312:
            r3 = 0
            goto L_0x0273
        L_0x0315:
            r4 = 240(0xf0, float:3.36E-43)
            if (r6 != r4) goto L_0x00a2
            r3 = 8
            goto L_0x00a2
        L_0x031d:
            r10 = 240(0xf0, float:3.36E-43)
            if (r6 != r10) goto L_0x018b
            r3 = 4
            goto L_0x018b
        L_0x0324:
            r3 = 240(0xf0, float:3.36E-43)
            if (r6 != r3) goto L_0x0333
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r10 = 100
            r11 = 30
            r3.<init>(r10, r11)     // Catch:{ Exception -> 0x02cd }
            goto L_0x01f3
        L_0x0333:
            r3 = 720(0x2d0, float:1.009E-42)
            if (r6 != r3) goto L_0x0342
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r10 = 100
            r11 = 90
            r3.<init>(r10, r11)     // Catch:{ Exception -> 0x02cd }
            goto L_0x01f3
        L_0x0342:
            r3 = 1080(0x438, float:1.513E-42)
            if (r6 != r3) goto L_0x0351
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r10 = 100
            r11 = 120(0x78, float:1.68E-43)
            r3.<init>(r10, r11)     // Catch:{ Exception -> 0x02cd }
            goto L_0x01f3
        L_0x0351:
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x02cd }
            r10 = -2
            r11 = 60
            r3.<init>(r10, r11)     // Catch:{ Exception -> 0x02cd }
            goto L_0x01f3
        L_0x035b:
            r10 = 240(0xf0, float:3.36E-43)
            if (r6 != r10) goto L_0x036d
            r6 = 20
            r3.bottomMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 20
            r3.leftMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 20
            r3.rightMargin = r6     // Catch:{ Exception -> 0x02cd }
            goto L_0x0221
        L_0x036d:
            r10 = 720(0x2d0, float:1.009E-42)
            if (r6 != r10) goto L_0x037f
            r6 = 50
            r3.bottomMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 130(0x82, float:1.82E-43)
            r3.leftMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 130(0x82, float:1.82E-43)
            r3.rightMargin = r6     // Catch:{ Exception -> 0x02cd }
            goto L_0x0221
        L_0x037f:
            r10 = 1080(0x438, float:1.513E-42)
            if (r6 != r10) goto L_0x0391
            r6 = 60
            r3.bottomMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 160(0xa0, float:2.24E-43)
            r3.leftMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 160(0xa0, float:2.24E-43)
            r3.rightMargin = r6     // Catch:{ Exception -> 0x02cd }
            goto L_0x0221
        L_0x0391:
            r6 = 30
            r3.bottomMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 70
            r3.leftMargin = r6     // Catch:{ Exception -> 0x02cd }
            r6 = 70
            r3.rightMargin = r6     // Catch:{ Exception -> 0x02cd }
            goto L_0x0221
        L_0x039f:
            r3 = move-exception
            r21 = r3
            r3 = r4
            r4 = r21
            goto L_0x02d1
        */
        throw new UnsupportedOperationException("Method not decompiled: igudi.com.ergushi.AppConnect.a(igudi.com.ergushi.AdInfo, android.content.Context, android.app.Dialog, int, int):android.widget.LinearLayout");
    }

    private AdInfo a(Context context, List list) {
        AdInfo a2;
        if (list == null) {
            return null;
        }
        try {
            if (list.isEmpty()) {
                return null;
            }
            for (int i2 = 0; i2 < list.size() && (a2 = a(list)) != null; i2++) {
                InputStream loadStreamFromLocal = new SDKUtils(context).loadStreamFromLocal(a2.getAdId(), "/Android/data/cache/popCache");
                if (loadStreamFromLocal == null) {
                    new aa(this, a2).execute(new Void[0]);
                } else if (BitmapFactory.decodeStream(loadStreamFromLocal) != null) {
                    return a2;
                } else {
                    new aa(this, a2).execute(new Void[0]);
                    loadStreamFromLocal.close();
                }
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private AdInfo a(List list) {
        return a(list, "PopAd_ShowTag");
    }

    /* access modifiers changed from: private */
    public String a(int i2, SharedPreferences sharedPreferences) {
        try {
            String str = c;
            String str2 = "";
            if (i2 != 0) {
                if (i2 > 0) {
                    str2 = ak.c() + ak.s();
                    str = str + "&points=" + i2;
                } else if (i2 < 0) {
                    str2 = ak.c() + ak.r();
                    str = str + "&points=" + (-i2);
                }
                String a2 = s.a(str2, str);
                if (!be.b(a2)) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putInt("UpdatePoints", 0);
                    edit.commit();
                    return a2;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    private String a(NodeList nodeList) {
        Element element = (Element) nodeList.item(0);
        if (element == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        String str = "";
        for (int i2 = 0; i2 < length; i2++) {
            Node item = childNodes.item(i2);
            if (item != null) {
                str = str + item.getNodeValue();
            }
        }
        if (!be.b(str)) {
            return str.trim();
        }
        return null;
    }

    protected static String a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(bArr);
            return a(instance.digest(), "");
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    protected static String a(byte[] bArr, String str) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                sb.append("0");
            }
            sb.append(hexString).append(str);
        }
        return sb.toString();
    }

    protected static void a(Context context) {
        try {
            if (aD) {
                SharedPreferences.Editor edit = context.getSharedPreferences("AppSettings", 0).edit();
                List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(2);
                if (runningTasks != null && !runningTasks.isEmpty()) {
                    ActivityManager.RunningTaskInfo runningTaskInfo = runningTasks.get(0);
                    if (!runningTaskInfo.baseActivity.getPackageName().equals(context.getPackageName())) {
                        runningTaskInfo = runningTasks.get(1);
                    }
                    d = runningTaskInfo.baseActivity;
                    e = runningTaskInfo;
                    if (!d.getShortClassName().equals(g(context)) && !d.getShortClassName().contains("opda")) {
                        String packageName = runningTaskInfo.baseActivity.getPackageName();
                        String className = runningTaskInfo.baseActivity.getClassName();
                        String shortClassName = runningTaskInfo.baseActivity.getShortClassName();
                        edit.putString("packageName", packageName);
                        edit.putString("className", className);
                        edit.putString("short_className", shortClassName);
                        edit.commit();
                    }
                }
                aD = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void a(Context context, String str, String str2) {
        try {
            Intent intent = new Intent(context, h(context));
            intent.setFlags(268435456);
            intent.putExtra("Offers_URL", str2);
            intent.putExtra("USER_ID", str);
            intent.putExtra("URL_PARAMS", c);
            intent.putExtra("CLIENT_PACKAGE", this.F);
            intent.putExtra("offers_webview_tag", "OffersWebView");
            context.startActivity(intent);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, List list, boolean z2, int i2) {
        AdInfo a2;
        if (list != null) {
            try {
                if (list.size() > 0 && z2 && (a2 = a(context, list)) != null) {
                    if (this.bd != null && this.bd.isShowing()) {
                        this.bd.cancel();
                    }
                    if (i2 != 0) {
                        this.bd = new Dialog(context, i2);
                    } else if ((((Activity) context).getWindow().getAttributes().flags & 1024) == 1024) {
                        this.bd = new Dialog(context, 16973841);
                    } else {
                        this.bd = new Dialog(context, 16973840);
                    }
                    LinearLayout a3 = a(a2, context, this.bd);
                    if (a3 != null) {
                        this.bd.setContentView(a3);
                        if (this.bd != null && !this.bd.isShowing()) {
                            this.bd.show();
                            this.be = new x(this, a2);
                            this.be.execute(new Void[0]);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(AdInfo adInfo) {
        if (adInfo != null) {
            try {
                String a2 = adInfo.a();
                if (!be.b(a2)) {
                    try {
                        b(this.a, a2, adInfo.getAdId(), "/Android/data/cache/popCache");
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, Dialog dialog) {
        boolean z2;
        if (n != null && !n.isEmpty()) {
            if (be.b(c)) {
                c(this.a);
            }
            String str2 = "&publisher_app_id=" + this.y + "&publisher_user_id=" + b + "&" + c;
            List list = n;
            int i2 = 0;
            int i3 = -1;
            while (i2 < list.size()) {
                try {
                    int i4 = ((AdInfo) list.get(i2)).getAdId().equals(str) ? i2 : i3;
                    i2++;
                    i3 = i4;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return;
                }
            }
            if (i3 != -1) {
                AdInfo adInfo = (AdInfo) list.get(i3);
                String adName = adInfo.getAdName();
                String adPackage = adInfo.getAdPackage();
                String d2 = adInfo.d();
                String b2 = adInfo.b();
                String c2 = adInfo.c();
                String str3 = adInfo.e() + str2;
                String str4 = b2 + str2;
                if ("cpa".equals(adInfo.j().toLowerCase())) {
                    String str5 = "";
                    String str6 = "";
                    try {
                        ApplicationInfo applicationInfo = this.a.getPackageManager().getApplicationInfo(this.a.getPackageName(), 128);
                        String packageName = this.a.getPackageName();
                        if (applicationInfo.metaData != null) {
                            str5 = applicationInfo.metaData.getString("CLIENT_PACKAGE");
                        }
                        if (be.b(str5)) {
                            str5 = packageName;
                        }
                        str6 = str5;
                    } catch (PackageManager.NameNotFoundException e3) {
                        e3.printStackTrace();
                    }
                    if (!be.b(adPackage)) {
                        Intent a2 = new be(this.a).a(this.a, adPackage);
                        if (a2 != null) {
                            a2.setFlags(268435456);
                            this.a.startActivity(a2);
                            z2 = false;
                        } else {
                            z2 = true;
                        }
                    } else {
                        z2 = true;
                    }
                    if (!z2) {
                        return;
                    }
                    if (!be.b(d2)) {
                        new SDKUtils(this.a).openUrlByBrowser(d2, str4);
                    } else if (be.b(c2) || c2.equals("false")) {
                        new ar().b(this.a, str3, adName);
                        if (dialog != null) {
                            dialog.cancel();
                        }
                    } else {
                        Intent intent = new Intent(this.a, h(this.a));
                        intent.putExtra("CLIENT_PACKAGE", str6);
                        intent.putExtra("offers_webview_tag", "OffersWebView");
                        intent.setFlags(268435456);
                        intent.putExtra("URL", str4);
                        this.a.startActivity(intent);
                    }
                } else if (!be.b(d2)) {
                    new SDKUtils(this.a).openUrlByBrowser(d2, str4);
                } else {
                    Intent intent2 = new Intent(this.a, h(this.a));
                    intent2.putExtra("CLIENT_PACKAGE", this.F);
                    intent2.putExtra("offers_webview_tag", "OffersWebView");
                    intent2.setFlags(268435456);
                    intent2.putExtra("URL", str4);
                    this.a.startActivity(intent2);
                }
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public List b(InputStream inputStream) {
        Exception e2;
        ArrayList arrayList;
        ArrayList arrayList2;
        Bitmap bitmap;
        int i2;
        String str = "";
        try {
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(inputStream, e.f);
            int eventType = newPullParser.getEventType();
            AdInfo adInfo = null;
            arrayList = null;
            while (eventType != 1) {
                switch (eventType) {
                    case 0:
                        arrayList2 = new ArrayList();
                        break;
                    case 1:
                    default:
                        arrayList2 = arrayList;
                        break;
                    case 2:
                        String nextText = "Close_btn".equals(newPullParser.getName()) ? newPullParser.nextText() : str;
                        AdInfo adInfo2 = "Ad".equals(newPullParser.getName()) ? new AdInfo() : adInfo;
                        if (adInfo2 != null) {
                            if ("Id".equals(newPullParser.getName())) {
                                adInfo2.a(newPullParser.nextText());
                            }
                            if ("Title".equals(newPullParser.getName())) {
                                adInfo2.h(newPullParser.nextText());
                            }
                            if ("Image".equals(newPullParser.getName())) {
                                adInfo2.b(newPullParser.nextText());
                            }
                            if ("ClickUrl".equals(newPullParser.getName())) {
                                adInfo2.d(newPullParser.nextText());
                            }
                            if ("Show_detail".equals(newPullParser.getName())) {
                                adInfo2.e(newPullParser.nextText());
                            }
                            if ("AdPackage".equals(newPullParser.getName())) {
                                adInfo2.f(newPullParser.nextText());
                            }
                            if ("NewBrowser".equals(newPullParser.getName())) {
                                adInfo2.g(newPullParser.nextText());
                            }
                            if ("DownUrl".equals(newPullParser.getName())) {
                                adInfo2.m(newPullParser.nextText());
                            }
                            if ("Show_notice_url".equals(newPullParser.getName())) {
                                adInfo2.p(newPullParser.nextText());
                            }
                            if ("Delay".equals(newPullParser.getName())) {
                                adInfo2.b(Integer.parseInt(newPullParser.nextText()));
                            }
                            if ("BgAlpha".equals(newPullParser.getName())) {
                                try {
                                    i2 = Integer.parseInt(newPullParser.nextText());
                                } catch (Exception e3) {
                                    e3.printStackTrace();
                                    i2 = 0;
                                }
                                adInfo2.c(i2);
                            }
                            if ("Ad_Type".equals(newPullParser.getName())) {
                                adInfo2.q(newPullParser.nextText());
                                adInfo = adInfo2;
                                arrayList2 = arrayList;
                                str = nextText;
                                break;
                            }
                        }
                        adInfo = adInfo2;
                        arrayList2 = arrayList;
                        str = nextText;
                        break;
                    case 3:
                        if ("Ad".equals(newPullParser.getName()) && adInfo != null && !be.b(adInfo.getAdId())) {
                            if (!be.b(str)) {
                                try {
                                    bitmap = b(this.a, str, "close_btn", "/Android/data/cache");
                                } catch (Exception e4) {
                                    e4.printStackTrace();
                                    bitmap = null;
                                }
                                try {
                                    adInfo.b(bitmap);
                                } catch (Exception e5) {
                                    e2 = e5;
                                    break;
                                }
                            }
                            arrayList.add(adInfo);
                            adInfo = null;
                            arrayList2 = arrayList;
                            break;
                        }
                        arrayList2 = arrayList;
                        break;
                }
                try {
                    arrayList = arrayList2;
                    eventType = newPullParser.next();
                } catch (Exception e6) {
                    Exception exc = e6;
                    arrayList = arrayList2;
                    e2 = exc;
                    e2.printStackTrace();
                    n = arrayList;
                    return arrayList;
                }
            }
            b(arrayList);
        } catch (Exception e7) {
            e2 = e7;
            arrayList = null;
            e2.printStackTrace();
            n = arrayList;
            return arrayList;
        }
        n = arrayList;
        return arrayList;
    }

    protected static void b(Context context) {
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences("Start_Tag", 3);
            if (sharedPreferences != null) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.clear();
                edit.commit();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(Context context, String str, String str2) {
        if (i != null && i.size() > 0) {
            List list = i;
            int i2 = -1;
            int i3 = 0;
            while (i3 < list.size()) {
                try {
                    int i4 = ((AdInfo) list.get(i3)).getAdId().equals(str) ? i3 : i2;
                    i3++;
                    i2 = i4;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return;
                }
            }
            if (i2 != -1) {
                AdInfo adInfo = (AdInfo) list.get(i2);
                String adPackage = adInfo.getAdPackage();
                String d2 = adInfo.d();
                String b2 = adInfo.b();
                String c2 = adInfo.c();
                String e3 = adInfo.e();
                String adName = adInfo.getAdName();
                String str3 = "";
                String str4 = "";
                try {
                    ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                    String packageName = context.getPackageName();
                    if (applicationInfo.metaData != null) {
                        str3 = applicationInfo.metaData.getString("CLIENT_PACKAGE");
                    }
                    if (be.b(str3)) {
                        str3 = packageName;
                    }
                    str4 = str3;
                } catch (PackageManager.NameNotFoundException e4) {
                    e4.printStackTrace();
                }
                boolean z2 = true;
                if (!be.b(adPackage)) {
                    Intent a2 = new be(context).a(context, adPackage);
                    if (a2 != null) {
                        a2.setFlags(268435456);
                        context.startActivity(a2);
                        getInstance(context).a(adPackage, 2);
                        z2 = false;
                    } else {
                        z2 = true;
                    }
                }
                if (!z2) {
                    return;
                }
                if (!be.b(d2)) {
                    new SDKUtils(context).openUrlByBrowser(d2, b2);
                } else if (be.b(str2) || !str2.equals("download")) {
                    Intent intent = new Intent(context, h(context));
                    intent.putExtra("URL", b2);
                    intent.putExtra("CLIENT_PACKAGE", str4);
                    intent.putExtra("offers_webview_tag", "OffersWebView");
                    intent.putExtra("MiniTitle", adName);
                    intent.setFlags(268435456);
                    if (be.b(c2) || c2.equals("false")) {
                        intent.putExtra("isFinshClose", "true");
                    }
                    context.startActivity(intent);
                } else {
                    new ar().b(context, e3, adName);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, int i2) {
        int i3 = 0;
        if (b() && c()) {
            try {
                int historyPoints = getHistoryPoints(this.a);
                String historyPointsName = getHistoryPointsName(this.a);
                if (str == null || !"spend".equals(str)) {
                    if (str != null) {
                        if ("award".equals(str)) {
                            historyPoints += i2;
                            i3 = i2;
                        }
                    }
                } else if (historyPoints >= i2) {
                    historyPoints -= i2;
                    i3 = -i2;
                } else {
                    at.getUpdatePointsFailed((String) f.get("failed_to_spend_points"));
                    return;
                }
                SharedPreferences sharedPreferences = this.a.getSharedPreferences("AppSettings", 0);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                int i4 = sharedPreferences.getInt("UpdatePoints", 0);
                if (i4 != 0) {
                    i3 += i4;
                }
                edit.putInt("UpdatePoints", i3);
                edit.putInt("AllPoints", historyPoints);
                edit.commit();
                at.getUpdatePoints(historyPointsName, historyPoints);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (str != null && "spend".equals(str)) {
            at.getUpdatePointsFailed((String) f.get("failed_to_spend_points"));
        } else if (str != null && "award".equals(str)) {
            at.getUpdatePointsFailed((String) f.get("failed_to_award_points"));
        }
    }

    /* access modifiers changed from: private */
    public void b(List list) {
        if (list != null && list.size() > 0) {
            SharedPreferences sharedPreferences = this.a.getSharedPreferences("AppSettings", 0);
            try {
                int i2 = sharedPreferences.getInt("PopAd_ShowTag", -1);
                if (i2 == -1) {
                    i2 = 0;
                }
                int i3 = i2 >= list.size() ? 0 : i2;
                String a2 = ((AdInfo) list.get(i3)).a();
                if (!be.b(a2)) {
                    try {
                        b(this.a, a2, ((AdInfo) list.get(i3)).getAdId(), "/Android/data/cache/popCache");
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (Exception e3) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putInt("PopAd_ShowTag", 0);
                edit.commit();
                e3.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean c(String str) {
        try {
            Document i2 = i(str);
            if (!(i2 == null || i2.getElementsByTagName("Result") == null)) {
                String a2 = a(i2.getElementsByTagName("Result"));
                String a3 = a(i2.getElementsByTagName("Success"));
                try {
                    String a4 = a(i2.getElementsByTagName("Pkg"));
                    if (!be.b(a4) && ao.g != null && ao.g.containsKey(a4)) {
                        ao.g.remove(a4);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                if (!be.b(a2)) {
                    this.aS = a2;
                }
                if (!be.b(a3)) {
                    this.aT = a3;
                }
                return true;
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return false;
    }

    protected static String d(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                if (!be.b(aE)) {
                    Log.d("APP_SDK", "AppId is setted by code,the value is: " + aE);
                    if (applicationInfo.metaData != null) {
                        String string = applicationInfo.metaData.getString(ak.f() + "_ID");
                        if (!be.b(string)) {
                            if (!aE.equals(string.trim())) {
                                Log.w("APP_SDK", "The AppId setted by code is not equals the value setted by manifest! Please check it!");
                            } else {
                                Log.d("APP_SDK", "The AppId in manifest is: " + string.trim());
                            }
                        }
                    }
                } else if (applicationInfo.metaData != null) {
                    String string2 = applicationInfo.metaData.getString(ak.f() + "_ID");
                    if (be.b(string2)) {
                        string2 = applicationInfo.metaData.getString("APP_ID");
                    }
                    if (!be.b(string2)) {
                        aE = string2.trim();
                        Log.d("APP_SDK", "AppId is setted by manifest, the value is: " + aE);
                    }
                }
                if (be.b(aE)) {
                    aE = context.getSharedPreferences("AppSettings", 0).getString("APP_ID", "");
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return aE;
    }

    /* access modifiers changed from: private */
    public boolean d(String str) {
        Document i2 = i(str);
        if (i2 != null) {
            String a2 = a(i2.getElementsByTagName("Success"));
            if (a2 != null && a2.equals("true")) {
                SharedPreferences.Editor edit = this.a.getSharedPreferences("AppSettings", 0).edit();
                String a3 = a(i2.getElementsByTagName("Points"));
                String a4 = a(i2.getElementsByTagName("CurrencyName"));
                if (!(a3 == null || a4 == null)) {
                    at.getUpdatePoints(a4, Integer.parseInt(a3));
                    if (!b()) {
                        return true;
                    }
                    edit.putInt("AllPoints", Integer.parseInt(a3));
                    edit.putString("PointName", a4);
                    edit.putLong("GetPointsTime", System.currentTimeMillis());
                    edit.commit();
                    return true;
                }
            } else if (a2 != null && a2.endsWith("false")) {
                at.getUpdatePointsFailed(a(i2.getElementsByTagName("Message")));
                return true;
            }
        }
        return false;
    }

    protected static Map e(Context context) {
        if (0 != 0) {
            return null;
        }
        try {
            return new SDKUtils(context).getLanguageCode().toLowerCase().equals("zh") ? aw.a() : aw.b();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public Map e(String str) {
        try {
            aM = new HashMap();
            String string = be.b(str) ? this.a.getSharedPreferences("AppSettings", 0).getString("AttrConfig", "") : str;
            if (string.endsWith("[;]")) {
                string = string.substring(0, string.lastIndexOf("[;]"));
            }
            if (string.indexOf("[;]") > 0) {
                String[] split = string.split("\\[;\\]");
                for (int i2 = 0; i2 < split.length; i2++) {
                    if (split[i2].trim().indexOf("[=]") > 0) {
                        String[] split2 = split[i2].trim().split("\\[=\\]");
                        if (split2.length == 2) {
                            aM.put(split2[0], split2[1]);
                        }
                    }
                }
            } else if (string.trim().indexOf("[=]") > 0) {
                String[] split3 = string.trim().split("\\[=\\]");
                if (split3.length == 2) {
                    aM.put(split3[0], split3[1]);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return aM;
    }

    protected static String f(Context context) {
        bb = context.getSharedPreferences("AppSettings", 0).getString("OfferName", "");
        return bb;
    }

    /* access modifiers changed from: private */
    public boolean f(String str) {
        try {
            Document i2 = i(str);
            if (!(i2 == null || i2.getElementsByTagName("Version") == null)) {
                String a2 = a(i2.getElementsByTagName("Success"));
                String a3 = a(i2.getElementsByTagName("Version"));
                String a4 = a(i2.getElementsByTagName("Clear"));
                String a5 = a(i2.getElementsByTagName("Update_tag"));
                this.aK = a(i2.getElementsByTagName("Update_tip"));
                String a6 = a(i2.getElementsByTagName("AppList"));
                this.aG = a(i2.getElementsByTagName("AD"));
                if (be.b(this.aG) || !this.aG.equals("0")) {
                    aH = true;
                } else {
                    aH = false;
                }
                this.aJ = a(i2.getElementsByTagName("MINI"));
                if (be.b(this.aJ) || !this.aJ.equals("0")) {
                    aI = true;
                } else {
                    aI = false;
                }
                SharedPreferences.Editor edit = this.a.getSharedPreferences("ShowAdFlag", 3).edit();
                edit.putBoolean("show_ad_flag", aH);
                edit.putBoolean("show_mini_flag", aI);
                edit.commit();
                try {
                    String a7 = a(i2.getElementsByTagName("ItemReturn"));
                    if (!be.b(a7) && a7.length() > 0) {
                        SharedPreferences.Editor edit2 = this.a.getSharedPreferences("AppSettings", 0).edit();
                        edit2.putString("AttrConfig", a7);
                        edit2.commit();
                        e(a7);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                String a8 = a(i2.getElementsByTagName("Exception"));
                try {
                    String a9 = a(i2.getElementsByTagName("DefUrl"));
                    if (!be.b(a9)) {
                        SharedPreferences.Editor edit3 = this.a.getSharedPreferences("AppSettings", 0).edit();
                        edit3.putString("def_url", a9);
                        edit3.commit();
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                String a10 = a(i2.getElementsByTagName("PopVersion"));
                String a11 = a(i2.getElementsByTagName("PopTipShow"));
                String a12 = a(i2.getElementsByTagName("InstallToast"));
                if (!be.b(a12)) {
                    SharedPreferences.Editor edit4 = this.a.getSharedPreferences("AppSettings", 0).edit();
                    edit4.putString("InstallToast", a12);
                    edit4.commit();
                }
                if (a2 != null && a2.equals("true")) {
                    if (!be.b(a5)) {
                        this.S = a5;
                    }
                    if (!be.b(a3)) {
                        this.K = a3;
                    }
                    if (!be.b(a4)) {
                        M = true;
                    }
                    if (!be.b(a6)) {
                        SharedPreferences.Editor edit5 = this.a.getSharedPreferences("Finalize_Flag", 3).edit();
                        edit5.putString("appList", a6);
                        edit5.commit();
                    }
                    if (!be.b(a8) && a8.equals("true")) {
                        aP = true;
                    }
                    if (!be.b(a10)) {
                        this.aV = a10;
                    }
                    if (be.b(a11)) {
                        return true;
                    }
                    this.aW = a11;
                    return true;
                }
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        return false;
    }

    protected static String g(Context context) {
        try {
            String f2 = f(context);
            return be.b(f2) ? be.c(context, j) : f2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: private */
    public boolean g(String str) {
        try {
            Document i2 = i(str);
            if (!(i2 == null || i2.getElementsByTagName("Version") == null)) {
                String a2 = a(i2.getElementsByTagName("Success"));
                String a3 = a(i2.getElementsByTagName("Version"));
                this.aK = a(i2.getElementsByTagName("Update_tip"));
                if (a2 != null && a2.equals("true")) {
                    if (!be.b(a3)) {
                        this.K = a3;
                    }
                    return true;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public static int getHistoryPoints(Context context) {
        return context.getSharedPreferences("AppSettings", 0).getInt("AllPoints", 0);
    }

    public static String getHistoryPointsName(Context context) {
        return context.getSharedPreferences("AppSettings", 0).getString("PointName", (String) f.get("points"));
    }

    public static AppConnect getInstance(Context context) {
        try {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            if (context != null) {
                if (s == null) {
                    s = new af(context);
                }
                if (r == null || "".equals(c)) {
                    try {
                        if (be.b(d(context))) {
                            Log.e("APP_SDK", "App_ID is not setted!");
                            return null;
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    r = new AppConnect(context);
                    if (aL) {
                        b(context);
                        aL = false;
                    }
                    a(context);
                }
                return r;
            }
            Log.w("APP_SDK", "The context is null");
            return r;
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public static AppConnect getInstance(String str, Context context) {
        aE = str;
        if (!be.b(aE)) {
            SharedPreferences.Editor edit = context.getSharedPreferences("AppSettings", 0).edit();
            edit.putString("APP_ID", aE);
            edit.commit();
        }
        r = getInstance(context);
        return r;
    }

    public static AppConnect getInstance(String str, String str2, Context context) {
        aF = str2;
        r = getInstance(str, context);
        return r;
    }

    protected static Class h(Context context) {
        try {
            return Class.forName(g(context));
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void h(String str) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
            builder.setTitle((CharSequence) f.get("update_version"));
            if (!be.b(this.aK)) {
                builder.setMessage(((String) f.get("new_version")) + this.K + ((String) f.get("version_is_found")) + "\n" + this.aK);
            } else {
                builder.setMessage(((String) f.get("new_version")) + this.K + ((String) f.get("version_is_found")));
            }
            builder.setPositiveButton((CharSequence) f.get("download"), new g(this, str));
            builder.setNegativeButton((CharSequence) f.get("next_time"), new h(this));
            builder.show();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private Document i(String str) {
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            return newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes(e.f)));
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.io.InputStream, java.lang.String, java.lang.String, boolean):void
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void */
    private void i(Context context) {
        try {
            String loadStringFromLocal = new SDKUtils(context).loadStringFromLocal("CacheTime.dat", "/Android/data/cache");
            if (be.b(loadStringFromLocal)) {
                new SDKUtils(context).saveDataToLocal(String.valueOf(System.currentTimeMillis()), "CacheTime.dat", "/Android/data/cache", true);
            } else if (!be.b(loadStringFromLocal)) {
                if (System.currentTimeMillis() - Long.parseLong(loadStringFromLocal.replaceAll("\n", "")) >= 2592000000L) {
                    try {
                        if (Environment.getExternalStorageState().equals("mounted")) {
                            new SDKUtils(context).deleteLocalFiles(new File(Environment.getExternalStorageDirectory().toString() + "/Android/data/cache"));
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    new SDKUtils(context).saveDataToLocal(System.currentTimeMillis() + "", "CacheTime.dat", "/Android/data/cache", true);
                }
            }
        } catch (NumberFormatException e3) {
            e3.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00f9, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00fa, code lost:
        r1.printStackTrace();
        r7 = r6;
        r6 = r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0130 A[SYNTHETIC, Splitter:B:54:0x0130] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0135 A[Catch:{ Exception -> 0x013e }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x013a A[Catch:{ Exception -> 0x013e }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0148 A[SYNTHETIC, Splitter:B:65:0x0148] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x014d A[Catch:{ Exception -> 0x0156 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0152 A[Catch:{ Exception -> 0x0156 }] */
    /* JADX WARNING: Removed duplicated region for block: B:97:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void p() {
        /*
            r11 = this;
            r5 = 0
            r2 = 0
            java.lang.String r0 = ""
            java.lang.String r1 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.String r3 = "mounted"
            boolean r1 = r1.equals(r3)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            if (r1 == 0) goto L_0x0176
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            r3.<init>()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.io.File r4 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.String r4 = "/Android/data/cache"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            r3.<init>()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.io.File r4 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.String r4 = "/Android/data/cache"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.String r4 = "AppPackage.dat"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            r6.<init>(r3)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            boolean r3 = r1.exists()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            if (r3 != 0) goto L_0x0065
            r1.mkdir()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
        L_0x0065:
            boolean r1 = r6.exists()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            if (r1 != 0) goto L_0x006e
            r6.createNewFile()     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
        L_0x006e:
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0128, all -> 0x0143 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0167, all -> 0x015b }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0167, all -> 0x015b }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0167, all -> 0x015b }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0167, all -> 0x015b }
            java.lang.String r1 = ""
            if (r3 == 0) goto L_0x0099
        L_0x0081:
            java.lang.String r1 = r3.readLine()     // Catch:{ Throwable -> 0x00f9 }
            if (r1 == 0) goto L_0x0099
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00f9 }
            r7.<init>()     // Catch:{ Throwable -> 0x00f9 }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ Throwable -> 0x00f9 }
            java.lang.StringBuilder r1 = r7.append(r1)     // Catch:{ Throwable -> 0x00f9 }
            java.lang.String r0 = r1.toString()     // Catch:{ Throwable -> 0x00f9 }
            goto L_0x0081
        L_0x0099:
            r7 = r6
            r6 = r0
        L_0x009b:
            android.content.Context r0 = r11.a     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            r1 = 0
            java.util.List r8 = r0.getInstalledPackages(r1)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            r1 = r5
        L_0x00a7:
            int r0 = r8.size()     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            if (r5 >= r0) goto L_0x0100
            java.lang.Object r0 = r8.get(r5)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            android.content.pm.PackageInfo r0 = (android.content.pm.PackageInfo) r0     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            android.content.pm.ApplicationInfo r9 = r0.applicationInfo     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            int r9 = r9.flags     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            android.content.pm.ApplicationInfo r10 = r0.applicationInfo     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            r9 = r9 & 1
            if (r9 > 0) goto L_0x00f3
            int r1 = r1 + 1
            java.lang.String r0 = r0.packageName     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            java.lang.String r9 = "com."
            boolean r9 = r0.startsWith(r9)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            if (r9 == 0) goto L_0x00d2
            r9 = 3
            int r10 = r0.length()     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            java.lang.String r0 = r0.substring(r9, r10)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
        L_0x00d2:
            boolean r9 = r6.contains(r0)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            if (r9 != 0) goto L_0x00f3
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            r9.<init>()     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            java.lang.String r10 = igudi.com.ergushi.AppConnect.az     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            java.lang.StringBuilder r0 = r9.append(r0)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            java.lang.String r9 = ";"
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            igudi.com.ergushi.AppConnect.az = r0     // Catch:{ Exception -> 0x016b, all -> 0x015e }
        L_0x00f3:
            r0 = r1
            int r1 = r5 + 1
            r5 = r1
            r1 = r0
            goto L_0x00a7
        L_0x00f9:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            r7 = r6
            r6 = r0
            goto L_0x009b
        L_0x0100:
            java.lang.String r0 = igudi.com.ergushi.AppConnect.az     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            if (r7 == 0) goto L_0x0174
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            r5 = 1
            r1.<init>(r7, r5)     // Catch:{ Exception -> 0x016b, all -> 0x015e }
            r1.write(r0)     // Catch:{ Exception -> 0x016f, all -> 0x0160 }
        L_0x0113:
            if (r1 == 0) goto L_0x0118
            r1.close()     // Catch:{ Exception -> 0x0123 }
        L_0x0118:
            if (r4 == 0) goto L_0x011d
            r4.close()     // Catch:{ Exception -> 0x0123 }
        L_0x011d:
            if (r3 == 0) goto L_0x0122
            r3.close()     // Catch:{ Exception -> 0x0123 }
        L_0x0122:
            return
        L_0x0123:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0122
        L_0x0128:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x012b:
            r0.printStackTrace()     // Catch:{ all -> 0x0163 }
            if (r2 == 0) goto L_0x0133
            r2.close()     // Catch:{ Exception -> 0x013e }
        L_0x0133:
            if (r3 == 0) goto L_0x0138
            r3.close()     // Catch:{ Exception -> 0x013e }
        L_0x0138:
            if (r1 == 0) goto L_0x0122
            r1.close()     // Catch:{ Exception -> 0x013e }
            goto L_0x0122
        L_0x013e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0122
        L_0x0143:
            r0 = move-exception
            r3 = r2
            r4 = r2
        L_0x0146:
            if (r2 == 0) goto L_0x014b
            r2.close()     // Catch:{ Exception -> 0x0156 }
        L_0x014b:
            if (r4 == 0) goto L_0x0150
            r4.close()     // Catch:{ Exception -> 0x0156 }
        L_0x0150:
            if (r3 == 0) goto L_0x0155
            r3.close()     // Catch:{ Exception -> 0x0156 }
        L_0x0155:
            throw r0
        L_0x0156:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0155
        L_0x015b:
            r0 = move-exception
            r3 = r2
            goto L_0x0146
        L_0x015e:
            r0 = move-exception
            goto L_0x0146
        L_0x0160:
            r0 = move-exception
            r2 = r1
            goto L_0x0146
        L_0x0163:
            r0 = move-exception
            r4 = r3
            r3 = r1
            goto L_0x0146
        L_0x0167:
            r0 = move-exception
            r1 = r2
            r3 = r4
            goto L_0x012b
        L_0x016b:
            r0 = move-exception
            r1 = r3
            r3 = r4
            goto L_0x012b
        L_0x016f:
            r0 = move-exception
            r2 = r1
            r1 = r3
            r3 = r4
            goto L_0x012b
        L_0x0174:
            r1 = r2
            goto L_0x0113
        L_0x0176:
            r6 = r0
            r3 = r2
            r4 = r2
            r7 = r2
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: igudi.com.ergushi.AppConnect.p():void");
    }

    private void q() {
        this.au = new t(this, null);
        this.au.execute(new Void[0]);
    }

    private void r() {
        this.av = new y(this, null);
        this.av.execute(new Void[0]);
    }

    private void s() {
        this.aw = new s(this, null);
        this.aw.execute(new Void[0]);
    }

    private void t() {
        this.ax = new w(this, this.H);
        this.ax.execute(new Void[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: igudi.com.ergushi.SDKUtils.saveDataToLocal(java.io.InputStream, java.lang.String, java.lang.String, boolean):void
     arg types: [java.io.InputStream, java.lang.String, java.lang.String, int]
     candidates:
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.io.InputStream, java.lang.String, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0056 A[SYNTHETIC, Splitter:B:29:0x0056] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap a(android.content.Context r6, java.lang.String r7, java.lang.String r8, java.lang.String r9) {
        /*
            r5 = this;
            r0 = 0
            igudi.com.ergushi.SDKUtils r1 = new igudi.com.ergushi.SDKUtils     // Catch:{ Exception -> 0x0044, all -> 0x0051 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0044, all -> 0x0051 }
            java.io.InputStream r2 = r1.loadStreamFromLocal(r8, r9)     // Catch:{ Exception -> 0x0044, all -> 0x0051 }
            if (r2 != 0) goto L_0x002f
            igudi.com.ergushi.SDKUtils r1 = new igudi.com.ergushi.SDKUtils     // Catch:{ Exception -> 0x0060 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0060 }
            java.io.InputStream r1 = r1.getNetDataToStream(r7)     // Catch:{ Exception -> 0x0060 }
            if (r1 != 0) goto L_0x001d
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ IOException -> 0x005a }
        L_0x001c:
            return r0
        L_0x001d:
            igudi.com.ergushi.SDKUtils r3 = new igudi.com.ergushi.SDKUtils     // Catch:{ Exception -> 0x0060 }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0060 }
            r4 = 0
            r3.saveDataToLocal(r1, r8, r9, r4)     // Catch:{ Exception -> 0x0060 }
            igudi.com.ergushi.SDKUtils r1 = new igudi.com.ergushi.SDKUtils     // Catch:{ Exception -> 0x0060 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0060 }
            java.io.InputStream r2 = r1.loadStreamFromLocal(r8, r9)     // Catch:{ Exception -> 0x0060 }
        L_0x002f:
            android.graphics.BitmapFactory$Options r1 = new android.graphics.BitmapFactory$Options     // Catch:{ Exception -> 0x0060 }
            r1.<init>()     // Catch:{ Exception -> 0x0060 }
            r3 = 1
            r1.inSampleSize = r3     // Catch:{ Exception -> 0x0060 }
            r3 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2, r3, r1)     // Catch:{ Exception -> 0x0060 }
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ IOException -> 0x0042 }
            goto L_0x001c
        L_0x0042:
            r1 = move-exception
            goto L_0x001c
        L_0x0044:
            r1 = move-exception
            r2 = r0
        L_0x0046:
            r1.printStackTrace()     // Catch:{ all -> 0x005e }
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ IOException -> 0x004f }
            goto L_0x001c
        L_0x004f:
            r1 = move-exception
            goto L_0x001c
        L_0x0051:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0054:
            if (r2 == 0) goto L_0x0059
            r2.close()     // Catch:{ IOException -> 0x005c }
        L_0x0059:
            throw r0
        L_0x005a:
            r1 = move-exception
            goto L_0x001c
        L_0x005c:
            r1 = move-exception
            goto L_0x0059
        L_0x005e:
            r0 = move-exception
            goto L_0x0054
        L_0x0060:
            r1 = move-exception
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: igudi.com.ergushi.AppConnect.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):android.graphics.Bitmap");
    }

    /* access modifiers changed from: protected */
    public AdInfo a(List list, String str) {
        Exception e2;
        AdInfo adInfo;
        try {
            if ("DiyAd_ShowTag".equals(str)) {
                if (list == null) {
                    list = getAdInfoList();
                }
            } else if (list == null) {
                list = null;
            }
            if (list == null || list.size() <= 0) {
                return null;
            }
            SharedPreferences sharedPreferences = this.a.getSharedPreferences("AppSettings", 0);
            int i2 = sharedPreferences.getInt(str, -1);
            if (i2 != -1) {
                ba = i2;
            }
            if (ba >= list.size()) {
                ba = 0;
            }
            adInfo = (list == null || list.size() <= 0) ? null : (AdInfo) list.get(ba);
            try {
                ba++;
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putInt(str, ba);
                edit.commit();
                return adInfo;
            } catch (Exception e3) {
                e2 = e3;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            adInfo = null;
            e2 = exc;
        }
        e2.printStackTrace();
        return adInfo;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public List a(InputStream inputStream) {
        Exception e2;
        ArrayList arrayList;
        ArrayList arrayList2;
        AdInfo adInfo;
        try {
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(inputStream, e.f);
            int eventType = newPullParser.getEventType();
            AdInfo adInfo2 = null;
            arrayList = null;
            while (eventType != 1) {
                switch (eventType) {
                    case 0:
                        try {
                            adInfo = adInfo2;
                            arrayList2 = new ArrayList();
                            break;
                        } catch (Exception e3) {
                            e2 = e3;
                            e2.printStackTrace();
                            return arrayList;
                        }
                    case 1:
                    default:
                        adInfo = adInfo2;
                        arrayList2 = arrayList;
                        break;
                    case 2:
                        if ("Ad".equals(newPullParser.getName())) {
                            adInfo2 = new AdInfo();
                        }
                        if (adInfo2 != null) {
                            if ("Id".equals(newPullParser.getName())) {
                                adInfo2.a(newPullParser.nextText());
                            }
                            if ("Title".equals(newPullParser.getName())) {
                                adInfo2.h(newPullParser.nextText());
                            }
                            if ("Image".equals(newPullParser.getName())) {
                                adInfo2.b(newPullParser.nextText());
                            }
                            if ("Content".equals(newPullParser.getName())) {
                                adInfo2.c(newPullParser.nextText());
                            }
                            if ("ClickUrl".equals(newPullParser.getName())) {
                                adInfo2.d(newPullParser.nextText());
                            }
                            if ("Show_detail".equals(newPullParser.getName())) {
                                adInfo2.e(newPullParser.nextText());
                            }
                            if ("AdPackage".equals(newPullParser.getName())) {
                                adInfo2.f(newPullParser.nextText());
                            }
                            if ("NewBrowser".equals(newPullParser.getName())) {
                                adInfo2.g(newPullParser.nextText());
                            }
                            if ("Points".equals(newPullParser.getName())) {
                                adInfo2.a(Integer.parseInt(newPullParser.nextText()));
                            }
                            if ("Description".equals(newPullParser.getName())) {
                                adInfo2.i(newPullParser.nextText());
                            }
                            if ("Version".equals(newPullParser.getName())) {
                                adInfo2.j(newPullParser.nextText());
                            }
                            if ("Filesize".equals(newPullParser.getName())) {
                                adInfo2.k(newPullParser.nextText());
                            }
                            if ("Provider".equals(newPullParser.getName())) {
                                adInfo2.l(newPullParser.nextText());
                            }
                            if ("ImageUrls".equals(newPullParser.getName())) {
                                adInfo2.a(newPullParser.nextText().split("\\|"));
                            }
                            if ("DownUrl".equals(newPullParser.getName())) {
                                adInfo2.m(newPullParser.nextText());
                            }
                            if ("Action".equals(newPullParser.getName())) {
                                adInfo2.n(newPullParser.nextText());
                            }
                            if ("Header".equals(newPullParser.getName())) {
                                adInfo2.o(newPullParser.nextText());
                                adInfo = adInfo2;
                                arrayList2 = arrayList;
                                break;
                            }
                        }
                        adInfo = adInfo2;
                        arrayList2 = arrayList;
                        break;
                    case 3:
                        if ("Ad".equals(newPullParser.getName()) && adInfo2 != null && !be.b(adInfo2.getAdId())) {
                            String a2 = adInfo2.a();
                            if (!be.b(a2)) {
                                adInfo2.a(a(this.a, a2, adInfo2.getAdId(), "/Android/data/cache/iconCache"));
                            }
                            arrayList.add(adInfo2);
                            arrayList2 = arrayList;
                            adInfo = null;
                            break;
                        }
                        adInfo = adInfo2;
                        arrayList2 = arrayList;
                        break;
                }
                try {
                    arrayList = arrayList2;
                    adInfo2 = adInfo;
                    eventType = newPullParser.next();
                } catch (Exception e4) {
                    Exception exc = e4;
                    arrayList = arrayList2;
                    e2 = exc;
                    e2.printStackTrace();
                    return arrayList;
                }
            }
            i = arrayList;
        } catch (Exception e5) {
            e2 = e5;
            arrayList = null;
            e2.printStackTrace();
            return arrayList;
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            if (f == null) {
                f = e(this.a);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        PackageManager packageManager = this.a.getPackageManager();
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.a.getSystemService("phone");
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.a.getPackageName(), 128);
            if (applicationInfo != null) {
                String str = "";
                this.y = d(this.a);
                if (!be.b(this.y)) {
                    this.F = this.a.getPackageName();
                    if (applicationInfo.metaData != null) {
                        str = applicationInfo.metaData.getString("CLIENT_PACKAGE");
                        if (!be.b(str)) {
                            this.F = str;
                        }
                    }
                    if (!be.b(aF)) {
                        this.B = aF;
                        Log.d("APP_SDK", "App_Pid is setted by code, the value is: " + this.B);
                    } else if (applicationInfo.metaData != null) {
                        Object obj = applicationInfo.metaData.get(ak.f() + "_PID");
                        if (obj != null) {
                            str = obj.toString();
                            if (!be.b(str)) {
                                this.B = str;
                                Log.d("APP_SDK", "App_Pid is setted by manifest, the value is: " + this.B);
                            }
                        } else {
                            Object obj2 = applicationInfo.metaData.get("APP_PID");
                            if (obj2 != null) {
                                str = obj2.toString();
                                if (!be.b(str)) {
                                    this.B = str;
                                    Log.d("APP_SDK", "App_Pid is setted by manifest, the value is: " + this.B);
                                }
                            }
                        }
                    }
                    if (be.b(this.B)) {
                        this.B = ak.f();
                        Log.d("APP_SDK", "App_Pid is setted by default, the value is: " + this.B);
                    }
                    z = packageManager.getPackageInfo(this.a.getPackageName(), 0).versionName;
                    this.u = "android";
                    this.t = Build.MODEL;
                    this.C = Build.BRAND;
                    this.v = Build.VERSION.RELEASE;
                    this.w = Locale.getDefault().getCountry();
                    this.x = Locale.getDefault().getLanguage();
                    this.N = telephonyManager.getSubscriberId();
                    try {
                        aN = new SDKUtils(this.a).hasThePermission("ACCESS_WIFI_STATE");
                        if (!aN) {
                            Log.w("APP_SDK", "Permission.ACCESS_WIFI_STATE is not found!");
                        } else if (new SDKUtils(this.a).isConnect()) {
                            WifiInfo connectionInfo = ((WifiManager) this.a.getSystemService("wifi")).getConnectionInfo();
                            if (connectionInfo != null) {
                                this.Q = connectionInfo.getMacAddress();
                                if (!be.b(this.Q)) {
                                    this.Q = "mac" + this.Q.replaceAll(":", "");
                                } else {
                                    Log.w("APP_SDK", "MAC_ADDRESS is not available.");
                                }
                            }
                        } else {
                            Log.w("APP_SDK", "Network is not connected.");
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    try {
                        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo();
                        if (activeNetworkInfo != null) {
                            if (!activeNetworkInfo.getTypeName().toLowerCase().equals("mobile")) {
                                this.O = activeNetworkInfo.getTypeName().toLowerCase();
                            } else {
                                this.O = activeNetworkInfo.getExtraInfo().toLowerCase();
                            }
                        }
                        Log.d("APP_SDK", "The net is: " + this.O);
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                    this.A = LIBRARY_VERSION_NUMBER;
                    SharedPreferences sharedPreferences = this.a.getSharedPreferences("appPrefrences", 0);
                    String string = applicationInfo.metaData != null ? applicationInfo.metaData.getString("DEVICE_ID") : str;
                    if (!be.b(string)) {
                        b = string;
                    } else if (telephonyManager != null) {
                        b = telephonyManager.getDeviceId();
                        if (be.b(b)) {
                            b = "0";
                        }
                        try {
                            b = b.toLowerCase();
                            if (b.equals("0") && this.Q.equals("")) {
                                StringBuffer stringBuffer = new StringBuffer();
                                stringBuffer.append("EMULATOR");
                                String string2 = sharedPreferences.getString("emulatorDeviceId", null);
                                if (!be.b(string2)) {
                                    b = string2;
                                } else {
                                    stringBuffer.append(new be(this.a).a("1234567890abcdefghijklmnopqrstuvw", 32));
                                    b = stringBuffer.toString().toLowerCase();
                                    SharedPreferences.Editor edit = sharedPreferences.edit();
                                    edit.putString("emulatorDeviceId", b);
                                    edit.commit();
                                }
                            } else if (b.equals("0") && !be.b(this.Q)) {
                                b = this.Q;
                            }
                        } catch (Exception e5) {
                            e5.printStackTrace();
                        }
                    }
                    this.P = a(("kingxiaoguang@gmail.com" + b + this.y).toLowerCase().getBytes()).toLowerCase();
                    try {
                        WindowManager windowManager = (WindowManager) this.a.getSystemService("window");
                        this.I = windowManager.getDefaultDisplay().getWidth();
                        this.J = windowManager.getDefaultDisplay().getHeight();
                    } catch (Exception e6) {
                        e6.printStackTrace();
                    }
                    this.aC = sharedPreferences.getInt("PrimaryColor", 0);
                    String string3 = sharedPreferences.getString("InstallReferral", null);
                    if (!be.b(string3)) {
                        this.G = string3;
                    }
                    this.T = be.a() + "";
                } else {
                    return;
                }
            }
            if (be.c(ak.l).contains(be.c(ak.m))) {
                this.R = be.c(ak.k);
            } else {
                this.R = be.c(ak.l);
            }
        } catch (Exception e7) {
            e7.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, int i2) {
        switch (i2) {
            case 0:
                aA = ak.v();
                aB = "install";
                break;
            case 1:
                aA = ak.w();
                aB = "load";
                break;
            case 2:
                aA = ak.x();
                aB = "load";
                break;
            case 3:
                aA = ak.u();
                aA = "uninstall";
                break;
            case 4:
                aA = ak.y();
                aB = "load";
                break;
            default:
                aA = ak.v();
                aB = "install";
                break;
        }
        this.H = str;
        if (r != null) {
            r.t();
        }
    }

    public void awardPoints(int i2, UpdatePointsNotifier updatePointsNotifier) {
        if (i2 >= 0) {
            this.E = "" + i2;
            if (r != null) {
                at = updatePointsNotifier;
                r.s();
            }
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap b(Context context, String str, String str2, String str3) {
        Exception e2;
        try {
            Bitmap a2 = a(context, str, str2, str3);
            if (a2 != null) {
                return a2;
            }
            try {
                new SDKUtils(context).deleteLocalFiles(new File((Environment.getExternalStorageDirectory().toString() + str3) + "/", str2));
                return a(context, str, str2, str3);
            } catch (Exception e3) {
                e2 = e3;
            }
        } catch (Exception e4) {
            e2 = e4;
        }
        e2.printStackTrace();
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return aX;
    }

    /* access modifiers changed from: protected */
    public String c(Context context) {
        this.a = context;
        if (be.b(c)) {
            a();
            c += "app_id=" + this.y + "&";
            c += "udid=" + b + "&";
            c += "imsi=" + this.N + "&";
            c += "net=" + this.O + "&";
            c += "base=" + this.R + "&";
            c += "app_version=" + z + "&";
            c += "sdk_version=" + this.A + "&";
            c += "device_name=" + this.t + "&";
            c += "device_brand=" + this.C + "&";
            c += "y=" + this.P + "&";
            c += "device_type=" + this.u + "&";
            c += "os_version=" + this.v + "&";
            c += "country_code=" + this.w + "&";
            c += "language=" + this.x + "&";
            c += "act=" + context.getPackageName();
            c += "root=" + this.T;
            if (!be.b(this.B)) {
                c += "&";
                c += "channel=" + this.B;
            }
            if (this.I > 0 && this.J > 0) {
                c += "&";
                c += "device_width=" + this.I + "&";
                c += "device_height=" + this.J;
            }
        }
        return c.replaceAll(" ", "%20");
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return aY;
    }

    public void checkUpdate(Context context) {
        aQ = new o(this, context);
        aQ.execute(new Void[0]);
    }

    public void clickAd(Context context, String str) {
        try {
            b(context, str, "");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void close() {
        try {
            if (this.a != null && ((Activity) this.a).isFinishing()) {
                SharedPreferences.Editor edit = this.a.getSharedPreferences("Start_Tag", 3).edit();
                edit.clear();
                edit.commit();
                OffersWebView.outsideFlag = false;
                aL = true;
                aO = true;
                aM = null;
                aU = null;
                m = true;
                if (r != null) {
                    r = null;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.io.InputStream, java.lang.String, java.lang.String, boolean):void
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public String d() {
        String str = "";
        String loadStringFromLocal = new SDKUtils(this.a).loadStringFromLocal("UnPackage.dat", "/Android/data/cache");
        try {
            String installed = new SDKUtils(this.a).getInstalled();
            if (!be.b(loadStringFromLocal)) {
                String[] split = loadStringFromLocal.replaceAll("\n", "").split(";");
                for (int i2 = 0; i2 < split.length; i2++) {
                    if (!installed.contains(split[i2])) {
                        if (split[i2].startsWith("com.")) {
                            str = str + split[i2].substring(3, split[i2].length()) + ";";
                        } else {
                            str = str + split[i2] + ";";
                        }
                    }
                }
                if (str.endsWith(";")) {
                    str.substring(0, str.length() - 1);
                }
            }
            if (be.b(installed)) {
                return str;
            }
            new SDKUtils(this.a).saveDataToLocal(installed, "UnPackage.dat", "/Android/data/cache", false);
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public void downloadAd(Context context, String str) {
        try {
            b(context, str, "download");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public AdInfo getAdInfo() {
        return a((List) null, "DiyAd_ShowTag");
    }

    public List getAdInfoList() {
        if (m) {
            initAdInfo();
        }
        if (i == null || i.size() <= 0) {
            return null;
        }
        return i;
    }

    public String getConfig(String str) {
        return getConfig(str, null);
    }

    public String getConfig(String str, String str2) {
        if (this.a.getSharedPreferences("AppSettings", 0).getString("AttrConfig", "").equals("")) {
            return str2 == null ? getConfig_Sync(str) : str2;
        }
        Map e2 = e("");
        if (e2 != null) {
            try {
                if (e2.size() > 0 && str != null && !be.b((String) e2.get(str))) {
                    return (String) e2.get(str);
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return "";
    }

    public Map getConfigMap() {
        Map e2;
        long currentTimeMillis = System.currentTimeMillis();
        while (true) {
            try {
                e2 = e("");
                if (e2 == null && System.currentTimeMillis() - currentTimeMillis <= 1000) {
                    Thread.sleep(100);
                    if (System.currentTimeMillis() - currentTimeMillis > 1000) {
                        return null;
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return e2;
    }

    public String getConfig_Sync(String str) {
        try {
            String a2 = s.a(ak.c() + ak.p(), c);
            if (!be.b(a2)) {
                Document i2 = i(a2);
                if (!(i2 == null || i2.getElementsByTagName("Version") == null)) {
                    try {
                        String a3 = a(i2.getElementsByTagName("ItemReturn"));
                        if (!be.b(a3) && a3.length() > 0) {
                            SharedPreferences.Editor edit = this.a.getSharedPreferences("AppSettings", 0).edit();
                            edit.putString("AttrConfig", a3);
                            edit.commit();
                            return (String) e(a3).get(str);
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                return "";
            }
            Map e3 = e("");
            return (e3 == null || e3.size() <= 0) ? "" : (String) e3.get(str);
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    public void getPoints(UpdatePointsNotifier updatePointsNotifier) {
        if (r != null) {
            at = updatePointsNotifier;
            r.q();
        }
    }

    public Dialog getPopAdDialog() {
        return this.bd;
    }

    public LinearLayout getPopAdView(Context context) {
        return getPopAdView(context, 0, 0);
    }

    public LinearLayout getPopAdView(Context context, int i2, int i3) {
        int i4;
        Exception e2;
        LinearLayout linearLayout;
        int i5 = 200;
        if (i2 == 0 || i3 == 0 || (i2 > 200 && i3 > 200)) {
            i5 = i3;
            i4 = i2;
        } else {
            i4 = 200;
        }
        try {
            AdInfo a2 = a(context, n);
            if (a2 == null) {
                return null;
            }
            o = a2.getAdId();
            linearLayout = a(a2, context, (Dialog) null, i4, i5);
            try {
                this.be = new x(this, a2);
                this.be.execute(new Void[0]);
                return linearLayout;
            } catch (Exception e3) {
                e2 = e3;
            }
        } catch (Exception e4) {
            e2 = e4;
            linearLayout = null;
            e2.printStackTrace();
            return linearLayout;
        }
    }

    public boolean hasPopAd(Context context) {
        InputStream loadStreamFromLocal;
        try {
            File file = "mounted".equals(Environment.getExternalStorageState()) ? new File(Environment.getExternalStorageDirectory().toString() + "/Android/data/cache/popCache") : context.getFilesDir();
            if (!file.exists() || !file.isDirectory()) {
                return false;
            }
            String[] list = file.list();
            if (list != null && list.length > 0) {
                for (int i2 = 0; i2 < list.length; i2++) {
                    if (n == null || n.isEmpty()) {
                        return false;
                    }
                    for (AdInfo adInfo : n) {
                        if (list[i2].equals(adInfo.getAdId()) && (loadStreamFromLocal = new SDKUtils(context).loadStreamFromLocal(adInfo.getAdId(), "/Android/data/cache/popCache")) != null) {
                            if (BitmapFactory.decodeStream(loadStreamFromLocal) != null) {
                                loadStreamFromLocal.close();
                                return true;
                            }
                            loadStreamFromLocal.close();
                        }
                    }
                }
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void initAdInfo() {
        m = false;
        if (i == null || i.isEmpty()) {
            aU = new r(this, null);
            aU.execute(new Void[0]);
        }
        new i(this).start();
    }

    public void initPopAd(Context context) {
        new u(this, context, false, 0).execute(new Void[0]);
    }

    public void setAdBackColor(int i2) {
        g = i2;
    }

    public void setAdForeColor(int i2) {
        h = i2;
    }

    public void setAdViewClassName(String str) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("AppSettings", 0).edit();
        edit.putString("OfferName", str);
        edit.commit();
    }

    public void setCrashReport(boolean z2) {
        aO = z2;
        if (aO) {
            al.a(this.a, s, c).a();
            aO = false;
        }
    }

    public void setPointsCache(boolean z2) {
        aX = z2;
    }

    public void setUpdatePointsFlag(boolean z2) {
        aY = z2;
    }

    public void setWebViewTransparent(boolean z2) {
        k = z2;
    }

    public void showAppOffers(Context context) {
        a(context, b, ak.c() + ak.l());
    }

    public void showAppOffers(Context context, String str) {
        a(context, str, ak.c() + ak.l());
    }

    public void showFeedback() {
        this.a.startActivity(showFeedback_forTab());
    }

    public Intent showFeedback_forTab() {
        Intent intent = new Intent();
        intent.setClass(this.a, h(this.a));
        intent.setFlags(268435456);
        intent.putExtra("UrlPath", ak.c() + ak.B());
        intent.putExtra("ACTIVITY_FLAG", f.z);
        intent.putExtra("URL_PARAMS", c);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void showGameOffers(Context context) {
        a(context, b, ak.c() + ak.k());
    }

    public void showGameOffers(Context context, String str) {
        a(context, str, ak.c() + ak.k());
    }

    public void showMore(Context context) {
        showMore(context, null);
    }

    public void showMore(Context context, String str) {
        try {
            Intent intent = new Intent(context, h(context));
            intent.putExtra("Offers_URL", ak.c() + ak.n());
            intent.putExtra("USER_ID", b);
            intent.putExtra("URL_PARAMS", c);
            intent.putExtra("CLIENT_PACKAGE", this.F);
            intent.putExtra("offers_webview_tag", "OffersWebView");
            if (!be.b(str)) {
                intent.putExtra("o_id", str);
            }
            context.startActivity(intent);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public Intent showMore_forTab(Context context) {
        return showMore_forTab(context, b);
    }

    public Intent showMore_forTab(Context context, String str) {
        Intent intent = new Intent(context, h(context));
        intent.putExtra("Offers_URL", ak.c() + ak.n());
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", c);
        intent.putExtra("CLIENT_PACKAGE", this.F);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void showOffers(Context context) {
        a(context, b, ak.c() + ak.m());
    }

    public void showOffers(Context context, String str) {
        a(context, str, ak.c() + ak.m());
    }

    public Intent showOffers_forTab(Context context) {
        return showOffers_forTab(context, b);
    }

    public Intent showOffers_forTab(Context context, String str) {
        Intent intent = new Intent(context, h(context));
        intent.setFlags(268435456);
        intent.putExtra("Offers_URL", ak.c() + ak.m());
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", c);
        intent.putExtra("CLIENT_PACKAGE", this.F);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: igudi.com.ergushi.AppConnect.a(android.content.Context, java.util.List, boolean, int):void
     arg types: [android.content.Context, java.util.List, int, int]
     candidates:
      igudi.com.ergushi.AppConnect.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):android.graphics.Bitmap
      igudi.com.ergushi.AppConnect.a(android.content.Context, java.util.List, boolean, int):void */
    public void showPopAd(Context context) {
        try {
            if (this.bc) {
                new u(this, context, true, 0).execute(new Void[0]);
            } else {
                a(context, this.bf, true, 0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: igudi.com.ergushi.AppConnect.a(android.content.Context, java.util.List, boolean, int):void
     arg types: [android.content.Context, java.util.List, int, int]
     candidates:
      igudi.com.ergushi.AppConnect.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):android.graphics.Bitmap
      igudi.com.ergushi.AppConnect.a(android.content.Context, java.util.List, boolean, int):void */
    public void showPopAd(Context context, int i2) {
        try {
            if (this.bc) {
                new u(this, context, true, i2).execute(new Void[0]);
            } else {
                a(context, this.bf, true, i2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void showTuanOffers(Context context) {
        a(context, b, ak.g());
    }

    public void showTuanOffers(Context context, String str) {
        a(context, str, ak.g());
    }

    public void spendPoints(int i2, UpdatePointsNotifier updatePointsNotifier) {
        if (i2 >= 0) {
            this.D = "" + i2;
            if (r != null) {
                at = updatePointsNotifier;
                r.r();
            }
        }
    }
}
