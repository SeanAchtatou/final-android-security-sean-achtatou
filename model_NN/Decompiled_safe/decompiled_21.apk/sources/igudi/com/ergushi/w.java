package igudi.com.ergushi;

import android.os.AsyncTask;
import android.widget.Toast;

class w extends AsyncTask {
    final /* synthetic */ AppConnect a;
    private String b = "";

    public w(AppConnect appConnect, String str) {
        this.a = appConnect;
        this.b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String str = ak.c() + AppConnect.aA;
        String a2 = AppConnect.s.a(str, (AppConnect.c + "&" + AppConnect.aB + "=" + this.b) + "&x=" + AppConnect.a(("kingxiaoguang@gmail.com" + AppConnect.b + this.a.y + this.b).toLowerCase().getBytes()).toLowerCase());
        if (!be.b(a2) && str.contains("receiver/install?")) {
            z = this.a.c(a2);
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        if (!"true".equals(this.a.aT) && !"false".equals(this.a.aT) && ae.d) {
            AppConnect.getInstance(this.a.a).a(this.b, 0);
            ae.d = false;
        }
        if (!be.b(this.a.aS)) {
            if (this.a.l) {
                Toast.makeText(this.a.a, this.a.aS, 1).show();
            }
            this.a.l = true;
        }
    }
}
