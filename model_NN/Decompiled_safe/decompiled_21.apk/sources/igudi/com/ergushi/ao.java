package igudi.com.ergushi;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.umeng.fb.f;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class ao extends Thread {
    protected static boolean d = false;
    protected static boolean e = false;
    protected static HashMap f;
    protected static HashMap g;
    protected static Handler h = null;
    protected static Integer i = 0;
    protected static boolean q = false;
    private static boolean x = true;
    public String a = "";
    protected int b;
    String c = "";
    double j = 0.0d;
    double k = 0.0d;
    NumberFormat l = new DecimalFormat("#0");
    InputStream m = null;
    FileOutputStream n = null;
    String o = "";
    String p = "/sdcard/download/";
    private String r;
    /* access modifiers changed from: private */
    public ac s;
    private ae t;
    /* access modifiers changed from: private */
    public Context u;
    private String v;
    private String w = "";
    private File y = null;

    protected ao() {
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
    }

    protected ao(Context context, String str, String str2) {
        a(context, str, str2, "", "");
    }

    protected ao(Context context, String str, String str2, String str3) {
        a(context, str, str2, str3, "");
    }

    protected ao(Context context, String str, String str2, String str3, String str4) {
        a(context, str, str2, str3, str4);
    }

    private File a(Context context, String str) {
        String str2 = "";
        if (!be.b(str) && str.endsWith(".apk")) {
            str2 = str.substring(0, str.lastIndexOf("."));
        }
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return context.getFileStreamPath(str);
        }
        File file = new File(this.p);
        if (file.exists() && file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                if (file2.getName().contains(str2)) {
                    return file2;
                }
            }
        }
        return null;
    }

    private void a(Context context, String str, String str2, String str3, String str4) {
        this.u = context;
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        if (!"".equals(str)) {
            this.r = str;
        }
        if (!"".equals(str2)) {
            this.v = str2;
        }
        if (!"".equals(str3)) {
            this.w = str3;
            if (!str3.endsWith(".apk")) {
                this.o = str3 + ".apk";
            } else {
                this.o = str3;
            }
        }
        if (!"".equals(str4)) {
            this.c = str4;
        }
        this.s = new ac(this.u);
        if (g == null) {
            g = new HashMap();
        }
        if (f == null) {
            f = new HashMap();
        }
        if (h == null) {
            h = new ap(this);
        }
    }

    private void a(Bundle bundle) {
        Message message = new Message();
        message.what = 1;
        message.setData(bundle);
        if (h != null) {
            h.sendMessage(message);
        }
    }

    private void a(String str) {
        try {
            this.b = (int) System.currentTimeMillis();
            try {
                String c2 = new ar().c(this.u, this.o);
                if (!"".equals(c2)) {
                    if (c2.endsWith(";")) {
                        c2 = c2.substring(0, c2.length() - 1);
                    }
                    int parseInt = Integer.parseInt(c2.split(";")[3]);
                    if (parseInt != 0) {
                        this.s.a(parseInt);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (be.b(this.o)) {
                this.o = ar.a(this.r);
                this.s.a((String) AppConnect.e(this.u).get("getting_filename"), this.b, "0%");
            } else {
                this.s.a(this.o.endsWith(".apk") ? this.o.substring(0, this.o.lastIndexOf(".")) : "", this.b, ((String) AppConnect.e(this.u).get("getting_filename")) + " 0%");
            }
            HttpURLConnection a2 = new be(this.u).a(this.r, null, null);
            a2.connect();
            if (a2.getResponseCode() == 200) {
                this.m = a2.getInputStream();
                this.j = (double) a2.getContentLength();
                if (this.j <= 0.0d) {
                    c("未知文件大小");
                } else {
                    a(a2, this.o, this.k, this.j, this.r, this.b, this.m, this.s, false);
                }
            } else if (a2.getResponseCode() == 404) {
                c("(404)下载文件不存在");
            } else if (a2.getResponseCode() == 500) {
                c("(500)服务端出现错误");
            } else {
                c("服务器无响应");
            }
            try {
                if (this.m != null) {
                    this.m.close();
                }
                if (this.n != null) {
                    this.n.close();
                }
            } catch (Exception e3) {
            }
        } catch (Exception e4) {
            e4.printStackTrace();
            try {
                if (this.m != null) {
                    this.m.close();
                }
                if (this.n != null) {
                    this.n.close();
                }
            } catch (Exception e5) {
            }
        } catch (Throwable th) {
            try {
                if (this.m != null) {
                    this.m.close();
                }
                if (this.n != null) {
                    this.n.close();
                }
            } catch (Exception e6) {
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.io.InputStream, java.lang.String, java.lang.String, boolean):void
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r44.a(r36, r42, null, (java.lang.String) igudi.com.ergushi.AppConnect.e(r0.u).get("failed_to_download"), false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x028f, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        r3.printStackTrace();
     */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0365 A[Catch:{ Exception -> 0x01aa }] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x048d  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x035a A[EDGE_INSN: B:182:0x035a->B:106:0x035a ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:192:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0218 A[Catch:{ Exception -> 0x04a0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.net.HttpURLConnection r35, java.lang.String r36, double r37, double r39, java.lang.String r41, int r42, java.io.InputStream r43, igudi.com.ergushi.ac r44, boolean r45) {
        /*
            r34 = this;
            java.lang.String r3 = ""
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
            r3.<init>()     // Catch:{ Exception -> 0x01aa }
            r0 = r36
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = ";"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01aa }
            r0 = r41
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = ";"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01aa }
            r0 = r39
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = ";"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01aa }
            r0 = r42
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = r3.toString()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r3 = ".apk"
            r0 = r36
            boolean r3 = r0.endsWith(r3)     // Catch:{ Exception -> 0x01aa }
            if (r3 == 0) goto L_0x0589
            r3 = 0
            java.lang.String r5 = "."
            r0 = r36
            int r5 = r0.lastIndexOf(r5)     // Catch:{ Exception -> 0x01aa }
            r0 = r36
            java.lang.String r3 = r0.substring(r3, r5)     // Catch:{ Exception -> 0x01aa }
            r22 = r3
        L_0x0050:
            igudi.com.ergushi.SDKUtils r3 = new igudi.com.ergushi.SDKUtils     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            android.content.Context r5 = r0.u     // Catch:{ Exception -> 0x01aa }
            r3.<init>(r5)     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
            r5.<init>()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r6 = "Down_"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x01aa }
            r0 = r22
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r6 = ".txt"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
            r6.<init>()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r7 = "/Android/data/cache/downloadCache/"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            android.content.Context r7 = r0.u     // Catch:{ Exception -> 0x01aa }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x01aa }
            r7 = 0
            r3.saveDataToLocal(r4, r5, r6, r7)     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            java.lang.String r3 = r0.w     // Catch:{ Exception -> 0x01aa }
            boolean r3 = igudi.com.ergushi.be.b(r3)     // Catch:{ Exception -> 0x01aa }
            if (r3 != 0) goto L_0x0585
            java.lang.String r3 = ".apk"
            r0 = r36
            boolean r3 = r0.endsWith(r3)     // Catch:{ Exception -> 0x01aa }
            if (r3 == 0) goto L_0x0585
            r3 = 0
            java.lang.String r4 = "."
            r0 = r36
            int r4 = r0.lastIndexOf(r4)     // Catch:{ Exception -> 0x01aa }
            r0 = r36
            java.lang.String r3 = r0.substring(r3, r4)     // Catch:{ Exception -> 0x01aa }
            r21 = r3
        L_0x00b8:
            java.lang.String r3 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = "mounted"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x01aa }
            if (r3 == 0) goto L_0x019a
            r0 = r39
            int r3 = (int) r0     // Catch:{ Exception -> 0x01aa }
            r4 = 0
            boolean r3 = igudi.com.ergushi.be.a(r3, r4)     // Catch:{ Exception -> 0x01aa }
            if (r3 != 0) goto L_0x010a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
            r3.<init>()     // Catch:{ Exception -> 0x01aa }
            java.io.File r4 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = "/sdcard/download/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01aa }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x01aa }
            r4.<init>(r3)     // Catch:{ Exception -> 0x01aa }
            java.io.File[] r4 = r4.listFiles()     // Catch:{ Exception -> 0x01aa }
            int r5 = r4.length     // Catch:{ Exception -> 0x01aa }
            r3 = 0
        L_0x00f4:
            if (r3 >= r5) goto L_0x010a
            r6 = r4[r3]     // Catch:{ Exception -> 0x01aa }
            java.lang.String r7 = r6.getName()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r8 = ".tmp"
            boolean r7 = r7.endsWith(r8)     // Catch:{ Exception -> 0x01aa }
            if (r7 != 0) goto L_0x0107
            r6.delete()     // Catch:{ Exception -> 0x01aa }
        L_0x0107:
            int r3 = r3 + 1
            goto L_0x00f4
        L_0x010a:
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            java.lang.String r4 = r0.p     // Catch:{ Exception -> 0x01aa }
            r3.<init>(r4)     // Catch:{ Exception -> 0x01aa }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            java.lang.String r5 = r0.p     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
            r6.<init>()     // Catch:{ Exception -> 0x01aa }
            r0 = r36
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r7 = ".tmp"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x01aa }
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x01aa }
            if (r45 != 0) goto L_0x0145
            boolean r5 = r3.exists()     // Catch:{ Exception -> 0x01aa }
            if (r5 != 0) goto L_0x013c
            r3.mkdir()     // Catch:{ Exception -> 0x01aa }
        L_0x013c:
            boolean r3 = r4.exists()     // Catch:{ Exception -> 0x01aa }
            if (r3 != 0) goto L_0x0145
            r4.createNewFile()     // Catch:{ Exception -> 0x01aa }
        L_0x0145:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x01aa }
            r5 = 1
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            r0.n = r3     // Catch:{ Exception -> 0x01aa }
        L_0x014f:
            r17 = 0
            long r23 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01aa }
            r15 = 0
            r5 = 0
            r3 = 0
            r13 = 0
            r11 = 0
            if (r43 == 0) goto L_0x0199
            java.util.HashMap r6 = igudi.com.ergushi.ao.f     // Catch:{ Exception -> 0x01aa }
            r7 = 1
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Exception -> 0x01aa }
            r0 = r36
            r6.put(r0, r7)     // Catch:{ Exception -> 0x01aa }
            r6 = 102400(0x19000, float:1.43493E-40)
            byte[] r0 = new byte[r6]     // Catch:{ Exception -> 0x01aa }
            r25 = r0
            r19 = r3
            r6 = r37
        L_0x0177:
            r0 = r43
            r1 = r25
            int r4 = r0.read(r1)     // Catch:{ Exception -> 0x056c }
            r3 = -1
            if (r4 == r3) goto L_0x01fc
            java.util.HashMap r3 = igudi.com.ergushi.ao.f     // Catch:{ Exception -> 0x056c }
            r0 = r36
            java.lang.Object r3 = r3.get(r0)     // Catch:{ Exception -> 0x056c }
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ Exception -> 0x056c }
            boolean r3 = r3.booleanValue()     // Catch:{ Exception -> 0x056c }
            if (r3 != 0) goto L_0x01af
            r0 = r44
            r1 = r42
            r0.a(r1)     // Catch:{ Exception -> 0x056c }
        L_0x0199:
            return
        L_0x019a:
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x01aa }
            r4 = 3
            r0 = r36
            java.io.FileOutputStream r3 = r3.openFileOutput(r0, r4)     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            r0.n = r3     // Catch:{ Exception -> 0x01aa }
            goto L_0x014f
        L_0x01aa:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0199
        L_0x01af:
            java.util.HashMap r3 = igudi.com.ergushi.ao.f     // Catch:{ Exception -> 0x056c }
            r0 = r36
            java.lang.Object r3 = r3.get(r0)     // Catch:{ Exception -> 0x056c }
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ Exception -> 0x056c }
            boolean r3 = r3.booleanValue()     // Catch:{ Exception -> 0x056c }
            if (r3 == 0) goto L_0x01c9
            r0 = r34
            java.io.FileOutputStream r3 = r0.n     // Catch:{ Exception -> 0x056c }
            r8 = 0
            r0 = r25
            r3.write(r0, r8, r4)     // Catch:{ Exception -> 0x056c }
        L_0x01c9:
            double r8 = (double) r4
            double r9 = r6 + r8
            double r3 = (double) r4
            double r19 = r19 + r3
            double r26 = r9 - r17
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0295 }
            long r3 = r6 - r23
            r28 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 / r28
            int r3 = (int) r3     // Catch:{ Exception -> 0x0295 }
            long r28 = r6 - r15
            r30 = 1000(0x3e8, double:4.94E-321)
            int r4 = (r28 > r30 ? 1 : (r28 == r30 ? 0 : -1))
            if (r4 >= 0) goto L_0x01e8
            int r4 = (r9 > r39 ? 1 : (r9 == r39 ? 0 : -1))
            if (r4 != 0) goto L_0x057d
        L_0x01e8:
            long r6 = r6 - r11
            r15 = 20000(0x4e20, double:9.8813E-320)
            int r4 = (r6 > r15 ? 1 : (r6 == r15 ? 0 : -1))
            if (r4 < 0) goto L_0x0579
            boolean r4 = igudi.com.ergushi.ao.q     // Catch:{ Exception -> 0x0295 }
            if (r4 != 0) goto L_0x0579
            r11 = 0
            int r4 = (r13 > r9 ? 1 : (r13 == r9 ? 0 : -1))
            if (r4 != 0) goto L_0x0237
            r3 = 1
            igudi.com.ergushi.ao.q = r3     // Catch:{ Exception -> 0x0295 }
        L_0x01fc:
            r4 = 0
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x04a0 }
            java.lang.String r5 = "activity"
            java.lang.Object r3 = r3.getSystemService(r5)     // Catch:{ Exception -> 0x04a0 }
            android.app.ActivityManager r3 = (android.app.ActivityManager) r3     // Catch:{ Exception -> 0x04a0 }
            r5 = 1
            java.util.List r3 = r3.getRunningTasks(r5)     // Catch:{ Exception -> 0x04a0 }
            java.util.Iterator r5 = r3.iterator()     // Catch:{ Exception -> 0x04a0 }
        L_0x0212:
            boolean r3 = r5.hasNext()     // Catch:{ Exception -> 0x04a0 }
            if (r3 == 0) goto L_0x0358
            java.lang.Object r3 = r5.next()     // Catch:{ Exception -> 0x04a0 }
            android.app.ActivityManager$RunningTaskInfo r3 = (android.app.ActivityManager.RunningTaskInfo) r3     // Catch:{ Exception -> 0x04a0 }
            android.content.ComponentName r3 = r3.topActivity     // Catch:{ Exception -> 0x04a0 }
            java.lang.String r3 = r3.getClassName()     // Catch:{ Exception -> 0x04a0 }
            java.lang.String r6 = "PackageInstallerActivity"
            boolean r6 = r3.contains(r6)     // Catch:{ Exception -> 0x04a0 }
            if (r6 != 0) goto L_0x0234
            java.lang.String r6 = "InstallAppProgress"
            boolean r3 = r3.contains(r6)     // Catch:{ Exception -> 0x04a0 }
            if (r3 == 0) goto L_0x0570
        L_0x0234:
            r3 = 1
        L_0x0235:
            r4 = r3
            goto L_0x0212
        L_0x0237:
            r6 = r11
            r11 = r9
        L_0x0239:
            r13 = 0
            if (r3 != 0) goto L_0x0576
            r3 = 1
            r8 = r3
        L_0x023f:
            double r3 = r9 / r39
            r15 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r3 = r3 * r15
            int r15 = (int) r3
            r3 = 4652218415073722368(0x4090000000000000, double:1024.0)
            double r3 = r26 / r3
            int r4 = (int) r3
            if (r5 >= r4) goto L_0x0573
            r3 = r4
        L_0x024d:
            double r0 = (double) r8
            r16 = r0
            double r16 = r19 / r16
            double r26 = r39 - r9
            double r16 = r26 / r16
            r0 = r16
            int r5 = (int) r0
            r0 = r34
            java.text.NumberFormat r0 = r0.l     // Catch:{ Exception -> 0x0295 }
            r16 = r0
            long r0 = (long) r15     // Catch:{ Exception -> 0x0295 }
            r17 = r0
            java.lang.String r16 = r16.format(r17)     // Catch:{ Exception -> 0x0295 }
            int r16 = java.lang.Integer.parseInt(r16)     // Catch:{ Exception -> 0x0295 }
            r17 = 100
            r0 = r16
            r1 = r17
            if (r0 <= r1) goto L_0x02d7
            r6 = 0
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x028f }
            java.util.Map r3 = igudi.com.ergushi.AppConnect.e(r3)     // Catch:{ Exception -> 0x028f }
            java.lang.String r4 = "failed_to_download"
            java.lang.Object r7 = r3.get(r4)     // Catch:{ Exception -> 0x028f }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ Exception -> 0x028f }
            r8 = 0
            r3 = r44
            r4 = r36
            r5 = r42
            r3.a(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x028f }
            goto L_0x01fc
        L_0x028f:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ Exception -> 0x0295 }
            goto L_0x01fc
        L_0x0295:
            r3 = move-exception
            r4 = r9
        L_0x0297:
            r3.printStackTrace()     // Catch:{ Exception -> 0x01aa }
            r3 = 1
            igudi.com.ergushi.ao.q = r3     // Catch:{ Exception -> 0x01aa }
            double r3 = r4 / r39
            r5 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r3 = r3 * r5
            int r4 = (int) r3     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x01aa }
            java.util.Map r3 = igudi.com.ergushi.AppConnect.e(r3)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r5 = "download_failed"
            java.lang.Object r3 = r3.get(r5)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x01aa }
            java.lang.String r5 = "%"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
            r6.<init>()     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r6 = "%"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r3 = r3.replace(r5, r4)     // Catch:{ Exception -> 0x01aa }
            r0 = r44
            r1 = r22
            r2 = r42
            r0.b(r1, r2, r3)     // Catch:{ Exception -> 0x01aa }
            goto L_0x01fc
        L_0x02d7:
            android.os.Bundle r16 = new android.os.Bundle     // Catch:{ Exception -> 0x0295 }
            r16.<init>()     // Catch:{ Exception -> 0x0295 }
            java.lang.String r17 = "notify_title"
            r0 = r16
            r1 = r17
            r2 = r21
            r0.putString(r1, r2)     // Catch:{ Exception -> 0x0295 }
            java.lang.String r17 = "notify_id"
            r0 = r16
            r1 = r17
            r2 = r42
            r0.putInt(r1, r2)     // Catch:{ Exception -> 0x0295 }
            java.lang.String r17 = "downSize"
            r0 = r16
            r1 = r17
            r0.putDouble(r1, r9)     // Catch:{ Exception -> 0x0295 }
            java.lang.String r17 = "downPercent"
            r0 = r16
            r1 = r17
            r0.putInt(r1, r15)     // Catch:{ Exception -> 0x0295 }
            java.lang.String r15 = "downSpeed"
            r0 = r16
            r0.putInt(r15, r4)     // Catch:{ Exception -> 0x0295 }
            java.lang.String r4 = "downMaxSpeed"
            r0 = r16
            r0.putInt(r4, r3)     // Catch:{ Exception -> 0x0295 }
            java.lang.String r4 = "fileSize"
            r0 = r16
            r1 = r39
            r0.putDouble(r4, r1)     // Catch:{ Exception -> 0x0295 }
            java.lang.String r4 = "usedTime"
            r0 = r16
            r0.putInt(r4, r8)     // Catch:{ Exception -> 0x0295 }
            java.lang.String r4 = "remainTime"
            r0 = r16
            r0.putInt(r4, r5)     // Catch:{ Exception -> 0x0295 }
            r0 = r34
            r1 = r16
            r0.a(r1)     // Catch:{ Exception -> 0x0295 }
            r32 = r6
            r7 = r11
            r5 = r13
            r11 = r3
            r12 = r9
            r3 = r32
        L_0x0338:
            r14 = 0
            int r14 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r14 != 0) goto L_0x0342
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0295 }
        L_0x0342:
            r14 = 0
            int r14 = (r3 > r14 ? 1 : (r3 == r14 ? 0 : -1))
            if (r14 != 0) goto L_0x034c
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0295 }
        L_0x034c:
            r14 = 1
            igudi.com.ergushi.ao.e = r14     // Catch:{ Exception -> 0x0295 }
            r15 = r5
            r17 = r12
            r5 = r11
            r13 = r7
            r11 = r3
            r6 = r9
            goto L_0x0177
        L_0x0358:
            if (r4 != 0) goto L_0x048d
        L_0x035a:
            r0 = r43
            r1 = r25
            int r3 = r0.read(r1)     // Catch:{ Exception -> 0x01aa }
            r4 = -1
            if (r3 != r4) goto L_0x0199
            r35.disconnect()     // Catch:{ Exception -> 0x01aa }
            r3 = 0
            igudi.com.ergushi.ao.e = r3     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x01aa }
            r0 = r22
            igudi.com.ergushi.ar.b(r3, r0)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r3 = ""
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
            r3.<init>()     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            java.lang.String r4 = r0.p     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01aa }
            r0 = r36
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r5 = "mounted"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x01aa }
            if (r4 == 0) goto L_0x04a6
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x01aa }
            r4.<init>(r3)     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            r0.y = r4     // Catch:{ Exception -> 0x01aa }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
            r5.<init>()     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r5 = ".tmp"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01aa }
            r4.<init>(r3)     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            java.io.File r3 = r0.y     // Catch:{ Exception -> 0x01aa }
            r4.renameTo(r3)     // Catch:{ Exception -> 0x01aa }
        L_0x03c1:
            r0 = r34
            java.io.File r3 = r0.y     // Catch:{ Exception -> 0x01aa }
            java.lang.String r3 = r3.getPath()     // Catch:{ Exception -> 0x01aa }
            r4 = 1
            igudi.com.ergushi.ao.d = r4     // Catch:{ Exception -> 0x01aa }
            igudi.com.ergushi.be r4 = new igudi.com.ergushi.be     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            android.content.Context r5 = r0.u     // Catch:{ Exception -> 0x01aa }
            r4.<init>(r5)     // Catch:{ Exception -> 0x01aa }
            java.lang.Object[] r6 = r4.e(r3)     // Catch:{ Exception -> 0x01aa }
            if (r6 == 0) goto L_0x04ed
            int r3 = r6.length     // Catch:{ Exception -> 0x01aa }
            r4 = 1
            if (r3 <= r4) goto L_0x04ed
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = "AppSettings"
            r5 = 0
            android.content.SharedPreferences r3 = r3.getSharedPreferences(r4, r5)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r4 = "InstallToast"
            java.lang.String r5 = ""
            java.lang.String r3 = r3.getString(r4, r5)     // Catch:{ Exception -> 0x01aa }
            boolean r4 = igudi.com.ergushi.be.b(r3)     // Catch:{ Exception -> 0x01aa }
            if (r4 != 0) goto L_0x041c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
            r4.<init>()     // Catch:{ Exception -> 0x01aa }
            java.lang.String r5 = "["
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01aa }
            r0 = r22
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r5 = "]"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01aa }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            r0.d(r3)     // Catch:{ Exception -> 0x01aa }
        L_0x041c:
            java.lang.String r3 = ".apk"
            r0 = r36
            boolean r3 = r0.endsWith(r3)     // Catch:{ Exception -> 0x04d2 }
            if (r3 == 0) goto L_0x04b6
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x04d2 }
            java.util.Map r3 = igudi.com.ergushi.AppConnect.e(r3)     // Catch:{ Exception -> 0x04d2 }
            java.lang.String r4 = "click_to_install"
            java.lang.Object r7 = r3.get(r4)     // Catch:{ Exception -> 0x04d2 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ Exception -> 0x04d2 }
            r8 = 1
            r3 = r44
            r4 = r21
            r5 = r42
            r3.a(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x04d2 }
        L_0x0440:
            android.content.Intent r3 = new android.content.Intent     // Catch:{ Exception -> 0x04d8 }
            r3.<init>()     // Catch:{ Exception -> 0x04d8 }
            java.lang.String r4 = "android.intent.action.VIEW"
            r3.setAction(r4)     // Catch:{ Exception -> 0x04d8 }
            r0 = r34
            java.io.File r4 = r0.y     // Catch:{ Exception -> 0x04d8 }
            android.net.Uri r4 = android.net.Uri.fromFile(r4)     // Catch:{ Exception -> 0x04d8 }
            java.lang.String r5 = "application/vnd.android.package-archive"
            r3.setDataAndType(r4, r5)     // Catch:{ Exception -> 0x04d8 }
            r4 = 268435456(0x10000000, float:2.5243549E-29)
            r3.setFlags(r4)     // Catch:{ Exception -> 0x04d8 }
            r0 = r34
            android.content.Context r4 = r0.u     // Catch:{ Exception -> 0x04d8 }
            r4.startActivity(r3)     // Catch:{ Exception -> 0x04d8 }
            igudi.com.ergushi.ae r3 = new igudi.com.ergushi.ae     // Catch:{ Exception -> 0x04d8 }
            r0 = r44
            r1 = r42
            r2 = r36
            r3.<init>(r0, r1, r2)     // Catch:{ Exception -> 0x04d8 }
            r0 = r34
            r0.t = r3     // Catch:{ Exception -> 0x04d8 }
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x04d8 }
            r0 = r34
            igudi.com.ergushi.ae r4 = r0.t     // Catch:{ Exception -> 0x04d8 }
            igudi.com.ergushi.ar.a(r3, r4)     // Catch:{ Exception -> 0x04d8 }
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x01aa }
            igudi.com.ergushi.ao.i = r3     // Catch:{ Exception -> 0x01aa }
        L_0x0484:
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x01aa }
            igudi.com.ergushi.AppConnect.getInstance(r3)     // Catch:{ Exception -> 0x01aa }
            goto L_0x0199
        L_0x048d:
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x04a0 }
            android.app.Activity r3 = (android.app.Activity) r3     // Catch:{ Exception -> 0x04a0 }
            boolean r3 = r3.isFinishing()     // Catch:{ Exception -> 0x04a0 }
            if (r3 != 0) goto L_0x035a
            r3 = 10000(0x2710, double:4.9407E-320)
            java.lang.Thread.sleep(r3)     // Catch:{ Exception -> 0x04a0 }
            goto L_0x01fc
        L_0x04a0:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ Exception -> 0x01aa }
            goto L_0x01fc
        L_0x04a6:
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x01aa }
            r0 = r36
            java.io.File r3 = r3.getFileStreamPath(r0)     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            r0.y = r3     // Catch:{ Exception -> 0x01aa }
            goto L_0x03c1
        L_0x04b6:
            r0 = r34
            android.content.Context r3 = r0.u     // Catch:{ Exception -> 0x04d2 }
            java.util.Map r3 = igudi.com.ergushi.AppConnect.e(r3)     // Catch:{ Exception -> 0x04d2 }
            java.lang.String r4 = "download_complete"
            java.lang.Object r7 = r3.get(r4)     // Catch:{ Exception -> 0x04d2 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ Exception -> 0x04d2 }
            r8 = 1
            r3 = r44
            r4 = r21
            r5 = r42
            r3.a(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x04d2 }
            goto L_0x0440
        L_0x04d2:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ Exception -> 0x01aa }
            goto L_0x0440
        L_0x04d8:
            r3 = move-exception
            r3.printStackTrace()     // Catch:{ all -> 0x04e4 }
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x01aa }
            igudi.com.ergushi.ao.i = r3     // Catch:{ Exception -> 0x01aa }
            goto L_0x0484
        L_0x04e4:
            r3 = move-exception
            r4 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x01aa }
            igudi.com.ergushi.ao.i = r4     // Catch:{ Exception -> 0x01aa }
            throw r3     // Catch:{ Exception -> 0x01aa }
        L_0x04ed:
            igudi.com.ergushi.SDKUtils r3 = new igudi.com.ergushi.SDKUtils     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            android.content.Context r4 = r0.u     // Catch:{ Exception -> 0x01aa }
            r3.<init>(r4)     // Catch:{ Exception -> 0x01aa }
            boolean r3 = r3.isCmwap()     // Catch:{ Exception -> 0x01aa }
            if (r3 != 0) goto L_0x0484
            java.lang.Integer r3 = igudi.com.ergushi.ao.i     // Catch:{ Exception -> 0x01aa }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x01aa }
            r4 = 1
            if (r3 >= r4) goto L_0x0550
            java.lang.Integer r3 = igudi.com.ergushi.ao.i     // Catch:{ Exception -> 0x01aa }
            java.lang.Integer r3 = igudi.com.ergushi.ao.i     // Catch:{ Exception -> 0x01aa }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x01aa }
            int r3 = r3 + 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x01aa }
            igudi.com.ergushi.ao.i = r3     // Catch:{ Exception -> 0x01aa }
            java.lang.String r7 = "解析包错误，尝试重新下载..."
            r8 = 0
            r3 = r44
            r4 = r21
            r5 = r42
            r3.a(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x01aa }
            r3 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r3)     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            java.io.File r3 = r0.y     // Catch:{ Exception -> 0x01aa }
            boolean r3 = r3.exists()     // Catch:{ Exception -> 0x01aa }
            if (r3 == 0) goto L_0x0537
            r0 = r34
            java.io.File r3 = r0.y     // Catch:{ Exception -> 0x01aa }
            r3.delete()     // Catch:{ Exception -> 0x01aa }
        L_0x0537:
            r0 = r44
            r1 = r42
            r0.a(r1)     // Catch:{ Exception -> 0x01aa }
            igudi.com.ergushi.ar r3 = new igudi.com.ergushi.ar     // Catch:{ Exception -> 0x01aa }
            r3.<init>()     // Catch:{ Exception -> 0x01aa }
            r0 = r34
            android.content.Context r4 = r0.u     // Catch:{ Exception -> 0x01aa }
            r0 = r41
            r1 = r36
            r3.b(r4, r0, r1)     // Catch:{ Exception -> 0x01aa }
            goto L_0x0484
        L_0x0550:
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x01aa }
            igudi.com.ergushi.ao.i = r3     // Catch:{ Exception -> 0x01aa }
            r0 = r44
            r1 = r42
            r0.a(r1)     // Catch:{ Exception -> 0x01aa }
            java.lang.String r7 = "解析包失败，点击查看"
            r8 = 0
            r3 = r44
            r4 = r21
            r5 = r42
            r3.a(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x01aa }
            goto L_0x0484
        L_0x056c:
            r3 = move-exception
            r4 = r6
            goto L_0x0297
        L_0x0570:
            r3 = r4
            goto L_0x0235
        L_0x0573:
            r3 = r5
            goto L_0x024d
        L_0x0576:
            r8 = r3
            goto L_0x023f
        L_0x0579:
            r6 = r11
            r11 = r13
            goto L_0x0239
        L_0x057d:
            r3 = r11
            r7 = r13
            r11 = r5
            r12 = r17
            r5 = r15
            goto L_0x0338
        L_0x0585:
            r21 = r36
            goto L_0x00b8
        L_0x0589:
            r22 = r36
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: igudi.com.ergushi.ao.a(java.net.HttpURLConnection, java.lang.String, double, double, java.lang.String, int, java.io.InputStream, igudi.com.ergushi.ac, boolean):void");
    }

    private void b(String str) {
        try {
            if (str.endsWith(";")) {
                str = str.substring(0, str.length() - 1);
            }
            String[] split = str.split(";");
            this.o = split[0];
            String str2 = split[1];
            double parseDouble = Double.parseDouble(split[2]);
            this.b = Integer.parseInt(split[3]);
            if (this.s == null) {
                this.s = new ac(this.u);
            }
            if (be.b(this.o)) {
                this.o = ar.a(str2);
            }
            File a2 = a(this.u, this.o);
            double length = (a2 == null || !a2.isFile() || !a2.exists()) ? 0.0d : (double) a2.length();
            ArrayList arrayList = new ArrayList();
            if (length != 0.0d) {
                arrayList.add("Range;bytes=" + ((int) length) + "-" + (((int) parseDouble) - 1));
            } else {
                arrayList = null;
            }
            HttpURLConnection a3 = new be(this.u).a(str2, arrayList, null);
            a3.connect();
            int responseCode = a3.getResponseCode();
            if (responseCode == 200 || responseCode == 206) {
                this.m = a3.getInputStream();
                if (responseCode != 206) {
                    parseDouble = (double) a3.getContentLength();
                }
                if (parseDouble <= 0.0d) {
                    c("未知文件大小");
                } else {
                    a(a3, this.o, length, parseDouble, str2, this.b, this.m, this.s, true);
                }
            } else if (a3.getResponseCode() == 416) {
                ar.b(this.u, this.o);
            } else if (a3.getResponseCode() == 404) {
                c("(404)下载文件不存在");
            } else if (a3.getResponseCode() == 500) {
                c("(500)服务端出现错误");
            } else {
                c("服务器无响应");
            }
            try {
                if (this.m != null) {
                    this.m.close();
                }
                if (this.n != null) {
                    this.n.close();
                }
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            try {
                if (this.m != null) {
                    this.m.close();
                }
                if (this.n != null) {
                    this.n.close();
                }
            } catch (Exception e4) {
            }
        } catch (Throwable th) {
            try {
                if (this.m != null) {
                    this.m.close();
                }
                if (this.n != null) {
                    this.n.close();
                }
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    private void c(String str) {
        Message message = new Message();
        message.what = 0;
        Bundle bundle = new Bundle();
        bundle.putString(f.an, str);
        this.a = str;
        message.setData(bundle);
        if (h != null) {
            h.sendMessage(message);
        }
    }

    private void d(String str) {
        Message message = new Message();
        message.what = 2;
        Bundle bundle = new Bundle();
        bundle.putString("installToast", str);
        message.setData(bundle);
        if (h != null) {
            h.sendMessage(message);
        }
    }

    public void run() {
        try {
            if (!be.b(this.v) && g != null) {
                g.put(this.v, "");
            }
            if (x) {
                x = false;
                new aq(this).start();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        AppConnect.getInstance(this.u);
        if ("".equals(this.c)) {
            a(this.r);
        } else {
            String c2 = new ar().c(this.u, this.c);
            if (!"".equals(c2)) {
                b(c2);
            }
        }
        super.run();
    }
}
