package igudi.com.ergushi;

import android.content.SharedPreferences;
import android.os.AsyncTask;

class s extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private s(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ s(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        if (new SDKUtils(this.a.a).isConnect()) {
            SharedPreferences sharedPreferences = this.a.a.getSharedPreferences("AppSettings", 0);
            int i = sharedPreferences.getInt("UpdatePoints", 0);
            if (i != 0) {
                String unused = this.a.a(i, sharedPreferences);
            }
            String a2 = AppConnect.s.a(ak.c() + ak.s(), AppConnect.c + "&points=" + this.a.E);
            boolean e = !be.b(a2) ? this.a.d(a2) : false;
            if (!e) {
                AppConnect.at.getUpdatePointsFailed((String) AppConnect.f.get("failed_to_award_points"));
            }
            z = e;
        } else {
            this.a.b("award", Integer.valueOf(this.a.E).intValue());
        }
        return Boolean.valueOf(z);
    }
}
