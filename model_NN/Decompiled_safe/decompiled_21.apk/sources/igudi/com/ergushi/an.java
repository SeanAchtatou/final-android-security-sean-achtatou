package igudi.com.ergushi;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import java.io.File;

class an extends AsyncTask {
    final /* synthetic */ al a;

    private an(al alVar) {
        this.a = alVar;
    }

    /* synthetic */ an(al alVar, am amVar) {
        this(alVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        String a2 = this.a.d();
        if (!be.b(a2)) {
            try {
                String b = al.e.b(ak.c() + ak.z() + this.a.f, a2);
                if (!be.b(b) && "ok".equals(b.trim())) {
                    Log.w("APP_SDK", "Send error log!");
                    File file = new File(Environment.getExternalStorageDirectory().toString() + "/Android/data/cache" + "/" + this.a.h);
                    File fileStreamPath = al.c.getFileStreamPath(this.a.h);
                    if (Environment.getExternalStorageState().equals("mounted") && file.exists()) {
                        file.delete();
                    }
                    if (fileStreamPath.exists()) {
                        fileStreamPath.delete();
                    }
                }
            } catch (Exception e) {
            }
        }
        return true;
    }
}
