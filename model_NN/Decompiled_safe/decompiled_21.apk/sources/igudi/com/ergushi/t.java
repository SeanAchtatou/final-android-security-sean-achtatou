package igudi.com.ergushi;

import android.content.SharedPreferences;
import android.os.AsyncTask;

class t extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private t(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ t(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        int i;
        String a2;
        boolean z = false;
        SharedPreferences sharedPreferences = this.a.a.getSharedPreferences("AppSettings", 0);
        long j = sharedPreferences.getLong("GetPointsTime", 0);
        long currentTimeMillis = System.currentTimeMillis();
        String str = null;
        if (this.a.b()) {
            i = AppConnect.getHistoryPoints(this.a.a);
            str = AppConnect.getHistoryPointsName(this.a.a);
            if (!new SDKUtils(this.a.a).isConnect() && i >= 0 && str != null) {
                AppConnect.at.getUpdatePoints(str, i);
                return true;
            }
        } else {
            i = 0;
        }
        if (j == 0 || (j != 0 && currentTimeMillis - j >= 20000)) {
            int i2 = sharedPreferences.getInt("UpdatePoints", 0);
            if (i2 != 0) {
                try {
                    a2 = this.a.a(i2, sharedPreferences);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    a2 = AppConnect.s.a(ak.c() + ak.q(), AppConnect.c);
                }
            } else {
                a2 = AppConnect.s.a(ak.c() + ak.q(), AppConnect.c);
            }
            if (!be.b(a2)) {
                z = this.a.d(a2);
            } else if (!this.a.b()) {
                AppConnect.at.getUpdatePointsFailed((String) AppConnect.f.get("failed_to_update_points"));
            } else if (i >= 0 && str != null) {
                AppConnect.at.getUpdatePoints(str, i);
                return true;
            }
            if (!z) {
                AppConnect.at.getUpdatePointsFailed((String) AppConnect.f.get("failed_to_update_points"));
            }
            return Boolean.valueOf(z);
        } else if (!this.a.b() || i < 0 || str == null) {
            return false;
        } else {
            AppConnect.at.getUpdatePoints(str, i);
            return true;
        }
    }
}
