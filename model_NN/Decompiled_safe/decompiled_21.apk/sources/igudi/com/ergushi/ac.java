package igudi.com.ergushi;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.io.File;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ac {
    protected static boolean b = false;
    public static boolean c = true;
    public static String d = "";
    static long e;
    public Notification a;
    private NotificationManager f;
    private Context g;
    private String h = "";
    private Map i;
    private final ScheduledExecutorService j = Executors.newScheduledThreadPool(1);

    public ac(Context context) {
        this.g = context;
        e = System.currentTimeMillis();
    }

    private ImageView a(View view) {
        try {
            if (view instanceof ViewGroup) {
                for (int childCount = ((ViewGroup) view).getChildCount(); childCount > 0; childCount--) {
                    ImageView a2 = a(((ViewGroup) view).getChildAt(childCount - 1));
                    if (a2 != null) {
                        return a2;
                    }
                }
            }
            if (view instanceof ImageView) {
                return (ImageView) view;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public void a(int i2) {
        this.f = (NotificationManager) this.g.getSystemService("notification");
        this.f.cancel(i2);
    }

    public void a(int i2, String str) {
        this.f.cancel(i2);
        if (!Environment.getExternalStorageState().equals("mounted")) {
            this.j.schedule(new ad(this, this.g, str), 600, TimeUnit.SECONDS);
        }
    }

    public void a(String str, int i2, String str2) {
        if (this.i == null || this.i.size() == 0) {
            this.i = AppConnect.e(this.g);
        }
        this.a = new Notification();
        this.a.icon = 17301633;
        this.a.tickerText = (CharSequence) this.i.get("downloading");
        this.a.when = e;
        this.a.flags = 16;
        this.a.setLatestEventInfo(this.g, str, str2 + "", PendingIntent.getActivity(this.g, 100, new Intent(), 268435456));
        this.f = (NotificationManager) this.g.getSystemService("notification");
        this.f.notify(i2, this.a);
    }

    public void a(String str, int i2, Object[] objArr, String str2, boolean z) {
        Bitmap decodeResource;
        try {
            if (this.i == null || this.i.size() == 0) {
                this.i = AppConnect.e(this.g);
            }
            String str3 = !str.endsWith(".apk") ? str + ".apk" : str;
            this.a = new Notification();
            this.a.icon = 17301634;
            this.a.tickerText = "";
            this.a.when = System.currentTimeMillis();
            if (z) {
                this.a.defaults = 1;
                this.a.flags = 1;
            } else {
                this.a.flags = 16;
            }
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.setFlags(268435456);
            if (Environment.getExternalStorageState().equals("mounted")) {
                intent.setDataAndType(Uri.fromFile(new File("/sdcard/download/" + str3)), "application/vnd.android.package-archive");
            } else {
                intent.setDataAndType(Uri.fromFile(this.g.getFileStreamPath(str3)), "application/vnd.android.package-archive");
            }
            this.a.setLatestEventInfo(this.g, str, str2, PendingIntent.getActivity(this.g, 100, intent, 0));
            if (objArr != null) {
                try {
                    if (objArr.length > 1 && (decodeResource = BitmapFactory.decodeResource((Resources) objArr[1], ((Integer) objArr[0]).intValue())) != null) {
                        this.a.contentView.setImageViewBitmap(a(((LayoutInflater) this.g.getSystemService("layout_inflater")).inflate(this.a.contentView.getLayoutId(), (ViewGroup) null)).getId(), decodeResource);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            this.f = (NotificationManager) this.g.getSystemService("notification");
            this.f.notify(i2, this.a);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str, int i2, String str2) {
        if (this.i == null || this.i.size() == 0) {
            this.i = AppConnect.e(this.g);
        }
        this.a = new Notification();
        this.a.icon = 17301633;
        this.a.tickerText = (CharSequence) this.i.get("downloading");
        this.a.when = e;
        this.a.flags = 16;
        this.a.setLatestEventInfo(this.g, str, str2 + "", PendingIntent.getActivity(this.g, 100, AppConnect.getInstance(this.g).showOffers_forTab(this.g), 268435456));
        this.f = (NotificationManager) this.g.getSystemService("notification");
        this.f.notify(i2, this.a);
    }
}
