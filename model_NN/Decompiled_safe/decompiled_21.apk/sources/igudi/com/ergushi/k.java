package igudi.com.ergushi;

import android.app.Dialog;
import android.view.View;
import android.widget.LinearLayout;

class k implements View.OnClickListener {
    final /* synthetic */ AdInfo a;
    final /* synthetic */ Dialog b;
    final /* synthetic */ LinearLayout c;
    final /* synthetic */ LinearLayout d;
    final /* synthetic */ AppConnect e;

    k(AppConnect appConnect, AdInfo adInfo, Dialog dialog, LinearLayout linearLayout, LinearLayout linearLayout2) {
        this.e = appConnect;
        this.a = adInfo;
        this.b = dialog;
        this.c = linearLayout;
        this.d = linearLayout2;
    }

    public void onClick(View view) {
        this.e.a(this.a.getAdId(), this.b);
        this.c.setVisibility(4);
        this.d.setVisibility(4);
    }
}
