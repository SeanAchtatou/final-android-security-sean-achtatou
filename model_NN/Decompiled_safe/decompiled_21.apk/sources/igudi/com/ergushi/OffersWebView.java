package igudi.com.ergushi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import java.io.File;
import java.util.List;
import java.util.Map;

public class OffersWebView extends Activity {
    /* access modifiers changed from: private */
    public static Map F;
    public static boolean outsideFlag = false;
    private String A = "";
    private String B = "";
    private String C = "";
    private String D = "";
    private String E = "";
    private boolean G = false;
    private String H = "";
    private String I = "";
    private String J = "";
    private String K = "";
    private String L = "";
    private String M = "";
    ao a;
    String b = "";
    String c = "";
    String d;
    String e;
    LinearLayout f;
    String g = "";
    String[] h = null;
    ac i = null;
    String j = "";
    File k = null;
    AlertDialog l = null;
    private WebView m = null;
    /* access modifiers changed from: private */
    public String n = null;
    /* access modifiers changed from: private */
    public ProgressBar o;
    private String p = "";
    private String q = "";
    private String r = "";
    private String s = "";
    private String t = "";
    private String u = "false";
    private String v = "";
    /* access modifiers changed from: private */
    public String w;
    /* access modifiers changed from: private */
    public String x;
    /* access modifiers changed from: private */
    public String y = "";
    /* access modifiers changed from: private */
    public boolean z = true;

    private void a(Context context) {
        int i2 = 0;
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        String str = "";
        while (true) {
            int i3 = i2;
            if (i3 < installedPackages.size()) {
                str = str + installedPackages.get(i3).packageName;
                i2 = i3 + 1;
            } else {
                SharedPreferences.Editor edit = context.getSharedPreferences("Package_Name", 3).edit();
                edit.putString("Package_Names", str);
                edit.commit();
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, ao aoVar, String str, String str2, String str3) {
        try {
            new ar().a(context, aoVar, str, str3, (int) ar.a(context, str), str2);
            if (this.u != null && "true".equals(this.u)) {
                finish();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void a(SharedPreferences sharedPreferences) {
        if (sharedPreferences != null) {
            this.H = sharedPreferences.getString("Notify_UrlPath", "");
            this.J = sharedPreferences.getString("offers_webview_tag", "");
            this.K = sharedPreferences.getString("NotifyAd_Tag", "");
            if (this.K != null && this.K.equals("true")) {
                this.L = sharedPreferences.getString("Notify_Show_detail", "");
            }
            if (!be.b(this.M)) {
                this.M = sharedPreferences.getString("Notify_Ad_Package", "");
            }
            if (this.H.contains("down_type")) {
                this.I = sharedPreferences.getString("Notify_UrlParams", "");
                this.I += "&at=" + System.currentTimeMillis();
            }
        }
    }

    private void a(Bundle bundle) {
        if (bundle != null) {
            if (bundle.getString("Offers_URL") != null) {
                this.r = bundle.getString("Offers_URL");
            }
            if (bundle.getString("URL") != null) {
                this.r = bundle.getString("URL");
            }
            if (bundle.getString("UrlPath") != null) {
                this.A = bundle.getString("Notify_Id");
                this.B = bundle.getString("UrlPath");
                this.C = bundle.getString("ACTIVITY_FLAG");
                this.D = bundle.getString("SHWO_FLAG");
                if (this.B.contains("down_type")) {
                    this.E = bundle.getString("Notify_Url_Params");
                }
            }
            this.s = bundle.getString("URL_PARAMS");
            if (be.b(this.s)) {
                this.s = "&" + this.s;
            }
            this.q = bundle.getString("CLIENT_PACKAGE");
            this.t = bundle.getString("USER_ID");
            this.u = bundle.getString("isFinshClose");
            this.s += "&publisher_user_id=" + this.t;
            try {
                this.v = bundle.getString("o_id");
                if (!be.b(this.v)) {
                    this.s += "&o_id=" + this.v;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.s += "&at=" + System.currentTimeMillis();
            this.p = bundle.getString("offers_webview_tag");
            this.j = bundle.getString("name");
            if (bundle.getString("MiniTitle") != null) {
                this.y = bundle.getString("MiniTitle");
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0229, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x022a, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            r1 = 0
            r5 = -1
            super.onCreate(r8)
            java.util.Map r0 = igudi.com.ergushi.OffersWebView.F     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x0011
            java.util.Map r0 = igudi.com.ergushi.OffersWebView.F     // Catch:{ Exception -> 0x0229 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0229 }
            if (r0 != 0) goto L_0x0017
        L_0x0011:
            java.util.Map r0 = igudi.com.ergushi.AppConnect.e(r7)     // Catch:{ Exception -> 0x0229 }
            igudi.com.ergushi.OffersWebView.F = r0     // Catch:{ Exception -> 0x0229 }
        L_0x0017:
            android.content.Intent r0 = r7.getIntent()     // Catch:{ Exception -> 0x0229 }
            android.os.Bundle r0 = r0.getExtras()     // Catch:{ Exception -> 0x0229 }
            r7.a(r0)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = "Notify"
            r2 = 3
            android.content.SharedPreferences r0 = r7.getSharedPreferences(r0, r2)     // Catch:{ Exception -> 0x0229 }
            r7.a(r0)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = "activity"
            java.lang.Object r0 = r7.getSystemService(r0)     // Catch:{ Exception -> 0x0229 }
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0     // Catch:{ Exception -> 0x0229 }
            r2 = 2
            java.util.List r0 = r0.getRunningTasks(r2)     // Catch:{ Exception -> 0x0229 }
            r2 = 0
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Exception -> 0x0229 }
            android.app.ActivityManager$RunningTaskInfo r0 = (android.app.ActivityManager.RunningTaskInfo) r0     // Catch:{ Exception -> 0x0229 }
            android.content.ComponentName r0 = r0.baseActivity     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.getShortClassName()     // Catch:{ Exception -> 0x0229 }
            r7.d = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = "AppSettings"
            r2 = 1
            android.content.SharedPreferences r0 = r7.getSharedPreferences(r0, r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "short_className"
            java.lang.String r3 = ""
            java.lang.String r2 = r0.getString(r2, r3)     // Catch:{ Exception -> 0x0229 }
            r7.e = r2     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = igudi.com.ergushi.AppConnect.g(r7)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = "."
            int r3 = r2.lastIndexOf(r3)     // Catch:{ Exception -> 0x0229 }
            int r4 = r2.length()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r2.substring(r3, r4)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = r7.e     // Catch:{ Exception -> 0x0229 }
            boolean r3 = igudi.com.ergushi.be.b(r3)     // Catch:{ Exception -> 0x0229 }
            if (r3 != 0) goto L_0x007e
            java.lang.String r3 = r7.d     // Catch:{ Exception -> 0x0229 }
            boolean r2 = r3.contains(r2)     // Catch:{ Exception -> 0x0229 }
            if (r2 != 0) goto L_0x007e
            r2 = 0
            igudi.com.ergushi.OffersWebView.outsideFlag = r2     // Catch:{ Exception -> 0x0229 }
        L_0x007e:
            r2 = 1048576(0x100000, float:1.469368E-39)
            android.content.Intent r3 = r7.getIntent()     // Catch:{ Exception -> 0x0229 }
            int r3 = r3.getFlags()     // Catch:{ Exception -> 0x0229 }
            r2 = r2 & r3
            r3 = 1048576(0x100000, float:1.469368E-39)
            if (r2 != r3) goto L_0x00d4
            boolean r2 = igudi.com.ergushi.OffersWebView.outsideFlag     // Catch:{ Exception -> 0x0229 }
            if (r2 == 0) goto L_0x00d4
            java.lang.String r2 = "packageName"
            java.lang.String r3 = ""
            java.lang.String r2 = r0.getString(r2, r3)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = "className"
            java.lang.String r4 = ""
            java.lang.String r0 = r0.getString(r3, r4)     // Catch:{ Exception -> 0x0229 }
            super.onCreate(r8)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = ""
            java.lang.String r4 = r2.trim()     // Catch:{ Exception -> 0x0229 }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0229 }
            if (r3 != 0) goto L_0x00d4
            java.lang.String r3 = ""
            java.lang.String r4 = r0.trim()     // Catch:{ Exception -> 0x0229 }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0229 }
            if (r3 != 0) goto L_0x00d4
            java.lang.String r3 = "OffersWebView"
            boolean r0 = r0.contains(r3)     // Catch:{ Exception -> 0x0229 }
            if (r0 != 0) goto L_0x00d4
            r7.finish()     // Catch:{ Exception -> 0x0229 }
            android.content.pm.PackageManager r0 = r7.getPackageManager()     // Catch:{ Exception -> 0x0223 }
            android.content.Intent r0 = r0.getLaunchIntentForPackage(r2)     // Catch:{ Exception -> 0x0223 }
            if (r0 == 0) goto L_0x00d4
            r7.startActivity(r0)     // Catch:{ Exception -> 0x0223 }
        L_0x00d4:
            java.lang.String r0 = "feedback"
            java.lang.String r2 = r7.C     // Catch:{ Exception -> 0x0229 }
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x022e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r0.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.B     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "?"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.s     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0229 }
            r7.n = r0     // Catch:{ Exception -> 0x0229 }
        L_0x00fb:
            java.lang.String r0 = r7.n     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = " "
            java.lang.String r3 = "%20"
            java.lang.String r0 = r0.replaceAll(r2, r3)     // Catch:{ Exception -> 0x0229 }
            r7.n = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r7.p     // Catch:{ Exception -> 0x0229 }
            boolean r0 = igudi.com.ergushi.be.b(r0)     // Catch:{ Exception -> 0x0229 }
            if (r0 != 0) goto L_0x0222
            java.lang.String r0 = r7.p     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "OffersWebView"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x0222
            r0 = 1
            r7.requestWindowFeature(r0)     // Catch:{ Exception -> 0x0229 }
            android.widget.RelativeLayout r2 = new android.widget.RelativeLayout     // Catch:{ Exception -> 0x0229 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r0 = new android.webkit.WebView     // Catch:{ Exception -> 0x0229 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x0229 }
            r7.m = r0     // Catch:{ Exception -> 0x0229 }
            android.widget.ProgressBar r0 = new android.widget.ProgressBar     // Catch:{ Exception -> 0x0229 }
            r3 = 0
            r4 = 16843399(0x1010287, float:2.369537E-38)
            r0.<init>(r7, r3, r4)     // Catch:{ Exception -> 0x0229 }
            r7.o = r0     // Catch:{ Exception -> 0x0229 }
            android.widget.ProgressBar r0 = r7.o     // Catch:{ Exception -> 0x0229 }
            r3 = 1
            r0.setEnabled(r3)     // Catch:{ Exception -> 0x0229 }
            android.widget.ProgressBar r0 = r7.o     // Catch:{ Exception -> 0x0229 }
            r3 = 0
            r0.setVisibility(r3)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebSettings r3 = r0.getSettings()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r7.C     // Catch:{ Exception -> 0x0229 }
            boolean r0 = igudi.com.ergushi.be.b(r0)     // Catch:{ Exception -> 0x0229 }
            if (r0 != 0) goto L_0x0345
            java.lang.String r0 = "notify"
            java.lang.String r4 = r7.C     // Catch:{ Exception -> 0x0229 }
            boolean r0 = r0.equals(r4)     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x039e
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r0 = -1
            r4 = -2
            r1.<init>(r0, r4)     // Catch:{ Exception -> 0x0229 }
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r4 = -1
            r5 = -1
            r0.<init>(r4, r5)     // Catch:{ Exception -> 0x0229 }
        L_0x0166:
            java.lang.String r4 = "feedback"
            java.lang.String r5 = r7.C     // Catch:{ Exception -> 0x0229 }
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x0229 }
            if (r4 == 0) goto L_0x0182
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r0 = -1
            r4 = -2
            r1.<init>(r0, r4)     // Catch:{ Exception -> 0x0229 }
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r4 = -1
            r5 = -1
            r0.<init>(r4, r5)     // Catch:{ Exception -> 0x0229 }
            r4 = 1
            r7.setRequestedOrientation(r4)     // Catch:{ Exception -> 0x0229 }
        L_0x0182:
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r5 = -2
            r6 = -2
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r5 = r7.m     // Catch:{ Exception -> 0x0229 }
            r6 = 2
            r5.setId(r6)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r5 = r7.m     // Catch:{ Exception -> 0x0229 }
            r5.setLayoutParams(r0)     // Catch:{ Exception -> 0x0229 }
            android.widget.ProgressBar r5 = r7.o     // Catch:{ Exception -> 0x0229 }
            r5.setLayoutParams(r4)     // Catch:{ Exception -> 0x0229 }
            r5 = 12
            r1.addRule(r5)     // Catch:{ Exception -> 0x0229 }
            r1 = 2
            r5 = 1
            r0.addRule(r1, r5)     // Catch:{ Exception -> 0x0229 }
            r1 = 13
            r4.addRule(r1)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r1 = r7.m     // Catch:{ Exception -> 0x0229 }
            r2.addView(r1, r0)     // Catch:{ Exception -> 0x0229 }
            android.widget.ProgressBar r0 = r7.o     // Catch:{ Exception -> 0x0229 }
            r2.addView(r0, r4)     // Catch:{ Exception -> 0x0229 }
            r7.setContentView(r2)     // Catch:{ Exception -> 0x0229 }
        L_0x01b5:
            boolean r0 = igudi.com.ergushi.AppConnect.k     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x01bf
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            r1 = 0
            r0.setBackgroundColor(r1)     // Catch:{ Exception -> 0x0229 }
        L_0x01bf:
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            igudi.com.ergushi.bd r1 = new igudi.com.ergushi.bd     // Catch:{ Exception -> 0x0229 }
            r2 = 0
            r1.<init>(r7, r2)     // Catch:{ Exception -> 0x0229 }
            r0.setWebViewClient(r1)     // Catch:{ Exception -> 0x0229 }
            igudi.com.ergushi.SDKUtils r0 = new igudi.com.ergushi.SDKUtils     // Catch:{ Exception -> 0x0229 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r1 = r7.m     // Catch:{ Exception -> 0x039b }
            java.lang.String r2 = "SDKUtils"
            r1.addJavascriptInterface(r0, r2)     // Catch:{ Exception -> 0x039b }
            r0 = 1
            r3.setJavaScriptEnabled(r0)     // Catch:{ Exception -> 0x039b }
        L_0x01da:
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0395 }
            android.webkit.WebView.enablePlatformNotifications()     // Catch:{ Exception -> 0x0395 }
        L_0x01df:
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            r1 = 0
            r0.setScrollBarStyle(r1)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            r1 = 0
            r0.setVerticalScrollBarEnabled(r1)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            r1 = 0
            r0.setHorizontalScrollBarEnabled(r1)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebChromeClient r1 = new android.webkit.WebChromeClient     // Catch:{ Exception -> 0x0229 }
            r1.<init>()     // Catch:{ Exception -> 0x0229 }
            r0.setWebChromeClient(r1)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = r7.n     // Catch:{ Exception -> 0x0229 }
            r0.loadUrl(r1)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            igudi.com.ergushi.bc r1 = new igudi.com.ergushi.bc     // Catch:{ Exception -> 0x0229 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x0229 }
            r0.setDownloadListener(r1)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = "Package_Name"
            r1 = 3
            android.content.SharedPreferences r0 = r7.getSharedPreferences(r0, r1)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = "Package_Names"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.getString(r1, r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = ""
            if (r0 != r1) goto L_0x0222
            r7.a(r7)     // Catch:{ Exception -> 0x0229 }
        L_0x0222:
            return
        L_0x0223:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0229 }
            goto L_0x00d4
        L_0x0229:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0222
        L_0x022e:
            java.lang.String r0 = r7.p     // Catch:{ Exception -> 0x0229 }
            boolean r0 = igudi.com.ergushi.be.b(r0)     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x0303
            java.lang.String r0 = r7.H     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "down_type"
            boolean r0 = r0.contains(r2)     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x02ce
            java.lang.String r0 = r7.K     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "true"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x029d
            java.lang.String r0 = r7.J     // Catch:{ Exception -> 0x0229 }
            r7.p = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r0.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.H     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "&publisher_user_id="
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.t     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "&"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "at"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "="
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0229 }
            r7.n = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r7.L     // Catch:{ Exception -> 0x0229 }
            boolean r0 = igudi.com.ergushi.be.b(r0)     // Catch:{ Exception -> 0x0229 }
            if (r0 != 0) goto L_0x0297
            java.lang.String r0 = r7.L     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "false"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x00fb
        L_0x0297:
            java.lang.String r0 = "true"
            r7.u = r0     // Catch:{ Exception -> 0x0229 }
            goto L_0x00fb
        L_0x029d:
            java.lang.String r0 = r7.K     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "false"
            boolean r0 = r0.equals(r2)     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x00fb
            java.lang.String r0 = r7.J     // Catch:{ Exception -> 0x0229 }
            r7.p = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r0.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.H     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "&"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.I     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0229 }
            r7.n = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = "true"
            r7.u = r0     // Catch:{ Exception -> 0x0229 }
            goto L_0x00fb
        L_0x02ce:
            java.lang.String r0 = r7.J     // Catch:{ Exception -> 0x0229 }
            r7.p = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r0.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.H     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "?"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "nyid"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "="
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.A     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.I     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0229 }
            r7.n = r0     // Catch:{ Exception -> 0x0229 }
            goto L_0x00fb
        L_0x0303:
            java.lang.String r0 = r7.r     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "?"
            int r0 = r0.indexOf(r2)     // Catch:{ Exception -> 0x0229 }
            if (r0 <= r5) goto L_0x0326
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r0.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.r     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.s     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0229 }
            r7.n = r0     // Catch:{ Exception -> 0x0229 }
            goto L_0x00fb
        L_0x0326:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r0.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.r     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = "?a=1"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r7.s     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0229 }
            r7.n = r0     // Catch:{ Exception -> 0x0229 }
            goto L_0x00fb
        L_0x0345:
            android.webkit.WebView r0 = r7.m     // Catch:{ Exception -> 0x0229 }
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r4 = -1
            r5 = -2
            r1.<init>(r4, r5)     // Catch:{ Exception -> 0x0229 }
            r0.setLayoutParams(r1)     // Catch:{ Exception -> 0x0229 }
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r1 = -1
            r4 = -1
            r0.<init>(r1, r4)     // Catch:{ Exception -> 0x0229 }
            android.widget.LinearLayout r1 = new android.widget.LinearLayout     // Catch:{ Exception -> 0x0229 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x0229 }
            r4 = 1
            r1.setOrientation(r4)     // Catch:{ Exception -> 0x0229 }
            android.view.ViewGroup$LayoutParams r4 = new android.view.ViewGroup$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r5 = -1
            r6 = -1
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x0229 }
            r1.setLayoutParams(r4)     // Catch:{ Exception -> 0x0229 }
            r4 = 17
            r2.setGravity(r4)     // Catch:{ Exception -> 0x0229 }
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r5 = -2
            r6 = -2
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x0229 }
            r5 = 13
            r4.addRule(r5)     // Catch:{ Exception -> 0x0229 }
            android.webkit.WebView r5 = r7.m     // Catch:{ Exception -> 0x0229 }
            r2.addView(r5, r0)     // Catch:{ Exception -> 0x0229 }
            android.widget.ProgressBar r0 = r7.o     // Catch:{ Exception -> 0x0229 }
            r2.addView(r0, r4)     // Catch:{ Exception -> 0x0229 }
            android.view.ViewGroup$LayoutParams r0 = new android.view.ViewGroup$LayoutParams     // Catch:{ Exception -> 0x0229 }
            r4 = -1
            r5 = -1
            r0.<init>(r4, r5)     // Catch:{ Exception -> 0x0229 }
            r1.addView(r2, r0)     // Catch:{ Exception -> 0x0229 }
            r7.setContentView(r1)     // Catch:{ Exception -> 0x0229 }
            goto L_0x01b5
        L_0x0395:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0229 }
            goto L_0x01df
        L_0x039b:
            r0 = move-exception
            goto L_0x01da
        L_0x039e:
            r0 = r1
            goto L_0x0166
        */
        throw new UnsupportedOperationException("Method not decompiled: igudi.com.ergushi.OffersWebView.onCreate(android.os.Bundle):void");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 1, "返回首页").setIcon(17301580);
        menu.add(0, 2, 2, "关闭").setIcon(17301560);
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            if (this.m != null) {
                this.m.clearHistory();
                this.m.clearCache(true);
                this.m.destroy();
                this.G = false;
            }
            F = null;
            finish();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.m != null) {
            this.m.clearCache(false);
            if (i2 == 4 && !this.e.equals(this.d)) {
                outsideFlag = true;
            }
            if (i2 == 4 && this.m.canGoBack()) {
                if (!this.z) {
                    finish();
                    this.z = true;
                }
                this.m.goBack();
                ac.c = true;
                return true;
            } else if (i2 == 4 && !this.m.canGoBack()) {
                finish();
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                if (this.m != null && !be.b(this.n)) {
                    this.m.loadUrl(this.n);
                    break;
                }
            case 2:
                finish();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.G = true;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.G) {
            this.G = false;
            if (this.m != null && !be.b(this.m.getOriginalUrl()) && (this.m.getOriginalUrl().contains(ak.h() + "/") || this.m.getOriginalUrl().contains(ak.i() + "/"))) {
                this.m.loadUrl(this.n);
            }
        }
        super.onResume();
    }
}
