package igudi.com.ergushi;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import java.net.HttpURLConnection;
import java.util.Map;

class av extends AsyncTask {
    Context a;
    String b;
    String c;
    int d = 0;
    String e = "";
    final /* synthetic */ ar f;

    av(ar arVar, Context context, String str, String str2) {
        this.f = arVar;
        this.a = context;
        this.b = str;
        this.c = str2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        try {
            if (this.b.contains("pkg=")) {
                this.e = this.b.substring(this.b.indexOf("pkg=") + 4);
                if (this.e.contains("&")) {
                    this.e = this.e.substring(0, this.e.indexOf("&"));
                }
            }
            HttpURLConnection a2 = new be(this.a).a(this.b, null, null);
            a2.connect();
            int responseCode = a2.getResponseCode();
            if (responseCode == 200 || responseCode == 206) {
                if (responseCode != 206) {
                    this.d = a2.getContentLength();
                }
                this.b = a2.getURL().toString();
                return false;
            }
            if (a2.getResponseCode() == 404) {
                String unused = this.f.a = "(404)下载文件不存在";
            } else if (a2.getResponseCode() == 500) {
                String unused2 = this.f.a = "(500)服务端出现错误";
            } else {
                String unused3 = this.f.a = "服务器无响应";
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: igudi.com.ergushi.ar.a(igudi.com.ergushi.ar, boolean):boolean
     arg types: [igudi.com.ergushi.ar, int]
     candidates:
      igudi.com.ergushi.ar.a(android.content.Context, java.lang.String):long
      igudi.com.ergushi.ar.a(igudi.com.ergushi.ar, igudi.com.ergushi.ae):igudi.com.ergushi.ae
      igudi.com.ergushi.ar.a(igudi.com.ergushi.ar, java.lang.String):java.lang.String
      igudi.com.ergushi.ar.a(android.content.Context, igudi.com.ergushi.ae):void
      igudi.com.ergushi.ar.a(igudi.com.ergushi.ar, boolean):boolean */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        try {
            if (ar.d == null || ar.d.size() == 0) {
                Map unused = ar.d = AppConnect.e(this.a);
            }
            if (this.d >= 0) {
                boolean unused2 = this.f.h = true;
                this.f.a(this.a, this.f.a(this.a, this.b, this.e, this.c), this.b, this.c, this.d, this.e);
            } else if (this.f.h) {
                new av(this.f, this.a, this.b, this.c).execute(new Void[0]);
                boolean unused3 = this.f.h = false;
            } else {
                Toast.makeText(this.a, this.f.a + "，" + ((String) ar.d.get("failed_to_download")), 1).show();
                boolean unused4 = this.f.h = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
