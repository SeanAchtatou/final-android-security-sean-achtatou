package igudi.com.ergushi;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SDKUtils {
    static String a = "";
    /* access modifiers changed from: private */
    public Context b;
    private PackageManager c;
    private ApplicationInfo d;
    private String e = "";
    private PackageInfo f;
    private Handler g;
    /* access modifiers changed from: private */
    public RelativeLayout h;
    /* access modifiers changed from: private */
    public LinearLayout i;
    /* access modifiers changed from: private */
    public AdViewCloseListener j;

    public SDKUtils(Context context) {
        this.b = context;
    }

    public SDKUtils(Context context, Handler handler, RelativeLayout relativeLayout, LinearLayout linearLayout, AdViewCloseListener adViewCloseListener) {
        this.b = context;
        this.g = handler;
        this.h = relativeLayout;
        this.i = linearLayout;
        this.j = adViewCloseListener;
    }

    public static int getDisplaySize(Context context) {
        int width = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
        int height = ((Activity) context).getWindowManager().getDefaultDisplay().getHeight();
        if (width < height) {
            if (width == 320) {
                return 320;
            }
            if (width < 320) {
                return 240;
            }
            if (width >= 720 && width < 1080) {
                return 720;
            }
            if (width >= 1080) {
                return 1080;
            }
        } else if (height == 320) {
            return 320;
        } else {
            if (height < 320) {
                return 240;
            }
            if (height >= 720 && height < 1080) {
                return 720;
            }
            if (height >= 1080) {
                return 1080;
            }
        }
        return 480;
    }

    public void callTel(String str) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:" + str));
        this.b.startActivity(intent);
    }

    public void close() {
        ((Activity) this.b).finish();
    }

    public void closeAd() {
        try {
            this.g.post(new bh(this));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void closeOfDialog(String str) {
        submit((String) AppConnect.e(this.b).get("message_title"), str);
    }

    public void closeSubmit(String str) {
        Toast.makeText(this.b, str, 1).show();
        ((Activity) this.b).finish();
    }

    public void deleteLocalFiles(File file) {
        try {
            if (!file.exists()) {
                return;
            }
            if (file.isFile()) {
                file.delete();
            } else if (file.isDirectory()) {
                for (File deleteLocalFiles : file.listFiles()) {
                    deleteLocalFiles(deleteLocalFiles);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void full_screen() {
        this.g.post(new bk(this));
    }

    public String[] getAllPermissions() {
        try {
            return this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), 4096).requestedPermissions;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public Map getAppInfoMap(String str) {
        try {
            HashMap hashMap = new HashMap();
            PackageManager packageManager = this.b.getPackageManager();
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 1);
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= queryIntentActivities.size()) {
                    break;
                }
                ResolveInfo resolveInfo = queryIntentActivities.get(i3);
                if (resolveInfo.activityInfo.packageName.equals(str)) {
                    String obj = resolveInfo.loadLabel(packageManager).toString();
                    int i4 = resolveInfo.activityInfo.applicationInfo.icon;
                    String str2 = resolveInfo.activityInfo.name;
                    if (str2 != null && !"".equals(str2.trim())) {
                        hashMap.put("appName", obj);
                        hashMap.put("appIcon", Integer.valueOf(i4));
                        hashMap.put("activityName", str2);
                        return hashMap;
                    }
                }
                i2 = i3 + 1;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public String getAppName() {
        try {
            return (String) this.b.getApplicationInfo().loadLabel(this.b.getPackageManager());
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public String getAppVersion(String str) {
        try {
            Context createPackageContext = this.b.createPackageContext(str, 3);
            this.c = createPackageContext.getPackageManager();
            this.f = this.c.getPackageInfo(createPackageContext.getPackageName(), 0);
            if (this.f != null) {
                String str2 = this.f.versionName;
                if (str2 != null && !"".equals(str2.trim())) {
                    return str2;
                }
                Log.i("APP_SDK", "The app is not exist.");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    public String getBrowserPackageName(String str) {
        try {
            String installed = getInstalled();
            if (str == null || "".equals(str.trim())) {
                return "";
            }
            if (str.indexOf(";") >= 0) {
                String[] split = str.split(";");
                for (int i2 = 0; i2 < split.length; i2++) {
                    if (installed.contains(split[i2])) {
                        return split[i2];
                    }
                }
            } else if (installed.contains(str)) {
                return str;
            }
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public String getCountryCode() {
        return Locale.getDefault().getCountry();
    }

    public String getDeviceName() {
        return Build.MODEL;
    }

    public int getDeviceOSVersion() {
        return Integer.parseInt(Build.VERSION.SDK);
    }

    public void getHtml(String str) {
        a = str;
    }

    public String getImsi() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.b.getSystemService("phone");
            if (telephonyManager != null) {
                return telephonyManager.getSubscriberId();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    public String getInstalled() {
        String str = "";
        try {
            this.c = this.b.getPackageManager();
            List<PackageInfo> installedPackages = this.c.getInstalledPackages(0);
            int i2 = 0;
            while (i2 < installedPackages.size()) {
                PackageInfo packageInfo = installedPackages.get(i2);
                int i3 = packageInfo.applicationInfo.flags;
                ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                i2++;
                str = (i3 & 1) <= 0 ? str + packageInfo.packageName + ";" : str;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return str;
    }

    public String getLanguageCode() {
        return Locale.getDefault().getLanguage();
    }

    public List getList(String str) {
        ArrayList arrayList = new ArrayList();
        if (str == null || "".equals(str) || str.indexOf("[;]") < 0) {
            arrayList.add(str);
        } else {
            String[] split = str.split("\\[;\\]");
            for (String add : split) {
                arrayList.add(add);
            }
        }
        return arrayList;
    }

    public String getMac_Address() {
        try {
            if (hasThePermission("ACCESS_WIFI_STATE")) {
                WifiInfo connectionInfo = ((WifiManager) this.b.getSystemService("wifi")).getConnectionInfo();
                if (connectionInfo != null) {
                    String macAddress = connectionInfo.getMacAddress();
                    if (macAddress != null && !"".equals(macAddress.trim())) {
                        return macAddress;
                    }
                    Log.i("APP_SDK", "The mac address is not found!");
                } else {
                    Log.i("APP_SDK", "Pleass check the Network connection!");
                }
                return "";
            }
            Log.i("APP_SDK", "Permission.ACCESS_WIFI_STATE is not found or the device is Emulator, Please check it!");
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public InputStream getNetDataToStream(String str) {
        try {
            HttpURLConnection a2 = new be(this.b).a(str, null, null);
            a2.connect();
            return a2.getInputStream();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public String getNodeTrimValues(NodeList nodeList) {
        String str;
        String str2 = "";
        int i2 = 0;
        while (i2 < nodeList.getLength()) {
            Element element = (Element) nodeList.item(i2);
            if (element != null) {
                NodeList childNodes = element.getChildNodes();
                if (childNodes.getLength() > 0) {
                    str = str2;
                    for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                        Node item = childNodes.item(i3);
                        if (item != null) {
                            str = str + item.getNodeValue() + "[;]";
                        }
                    }
                } else {
                    str = str2 + "a[;]";
                }
            } else {
                str = str2;
            }
            i2++;
            str2 = str;
        }
        if (str2 == null || str2.equals("")) {
            return null;
        }
        return str2.substring(0, str2.length() - 3).trim();
    }

    public String getParams() {
        return AppConnect.getInstance(this.b).c(this.b);
    }

    public String getResponseResult(HttpResponse httpResponse) {
        try {
            return EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public String getRunningAppPackageNames() {
        String str = "";
        try {
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) this.b.getSystemService("activity")).getRunningAppProcesses()) {
                str = getInstalled().contains(next.processName) ? str + next.processName + ";" : str;
            }
            if (str != null && !"".equals(str.trim()) && str.endsWith(";")) {
                return str.substring(0, str.length() - 1);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return "";
    }

    public String getSDKVersion() {
        return AppConnect.LIBRARY_VERSION_NUMBER;
    }

    public String getScreenStatus() {
        try {
            return this.b.getResources().getConfiguration().orientation == 1 ? "true" : this.b.getResources().getConfiguration().orientation == 2 ? "false" : "";
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public String getUdid() {
        Exception e2;
        String str;
        TelephonyManager telephonyManager;
        try {
            String str2 = AppConnect.b;
            if (!be.b(str2) || (telephonyManager = (TelephonyManager) this.b.getSystemService("phone")) == null) {
                return str2;
            }
            str = telephonyManager.getDeviceId();
            try {
                return be.b(str) ? "mac" + getMac_Address().replaceAll(":", "") : str;
            } catch (Exception e3) {
                e2 = e3;
                e2.printStackTrace();
                return str;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            str = "";
            e2 = exc;
            e2.printStackTrace();
            return str;
        }
    }

    public String getWAPS_ID() {
        return AppConnect.d(this.b);
    }

    public String getWAPS_PID() {
        Object obj;
        this.c = this.b.getPackageManager();
        try {
            this.d = this.c.getApplicationInfo(this.b.getPackageName(), 128);
            if (this.d == null || this.d.metaData == null || (obj = this.d.metaData.get("WAPS_PID")) == null) {
                return "";
            }
            this.e = obj.toString();
            return (this.e == null || this.e.equals("")) ? "" : this.e;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public void goToTargetBrowser(String str, String str2, PackageManager packageManager) {
        try {
            this.b.startActivity(goToTargetBrowser_Intent(str, str2, packageManager));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public Intent goToTargetBrowser_Intent(String str, String str2, PackageManager packageManager) {
        try {
            Intent goToTargetBrowser_Intent = goToTargetBrowser_Intent(str, "", str2, packageManager);
            if (goToTargetBrowser_Intent != null) {
                return goToTargetBrowser_Intent;
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public Intent goToTargetBrowser_Intent(String str, String str2, String str3, PackageManager packageManager) {
        try {
            if (getInstalled().contains(str)) {
                Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
                if (str2 != null && !"".equals(str2.trim())) {
                    launchIntentForPackage = new Intent();
                    launchIntentForPackage.setClassName(str, str2);
                }
                launchIntentForPackage.setAction("android.intent.action.VIEW");
                launchIntentForPackage.addCategory("android.intent.category.DEFAULT");
                launchIntentForPackage.setData(Uri.parse(str3));
                return launchIntentForPackage;
            }
            new Intent("android.intent.action.VIEW", Uri.parse(str3)).setFlags(268435456);
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean hasThePermission(String str) {
        try {
            String[] allPermissions = getAllPermissions();
            if (allPermissions == null || allPermissions.length <= 0) {
                return false;
            }
            for (String str2 : allPermissions) {
                if (!be.b(str) && str2.contains(str)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public void hideAd() {
        try {
            this.g.post(new bi(this));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public int initAdWidth() {
        try {
            if (this.b.getResources().getConfiguration().orientation == 1) {
                return ((Activity) this.b).getWindowManager().getDefaultDisplay().getWidth();
            }
            if (this.b.getResources().getConfiguration().orientation == 2) {
                return ((Activity) this.b).getWindowManager().getDefaultDisplay().getHeight();
            }
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean isCmwap() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.b.getSystemService("connectivity")).getActiveNetworkInfo();
        return (activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().toLowerCase().contains(ak.j())) ? false : true;
    }

    public boolean isConnect() {
        try {
            return ((ConnectivityManager) this.b.getSystemService("connectivity")).getActiveNetworkInfo() != null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public String isInstalled(String str) {
        try {
            return this.b.getPackageManager().getLaunchIntentForPackage(str) != null ? "true" : "false";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public boolean isTimeLimited(String str, String str2) {
        String str3;
        try {
            Date date = new Date(System.currentTimeMillis());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("HH:mm:ss");
            try {
                str3 = simpleDateFormat2.format(simpleDateFormat3.parse(str)).equals("1970-01-01") ? simpleDateFormat2.format(date) + " " + str : str;
            } catch (Exception e2) {
                str3 = str;
            }
            try {
                if (simpleDateFormat2.format(simpleDateFormat3.parse(str)).equals("1970-01-01")) {
                    str2 = simpleDateFormat2.format(date) + " " + str2;
                }
            } catch (Exception e3) {
            }
            return date.after(simpleDateFormat.parse(str3)) && date.before(simpleDateFormat.parse(str2));
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    public String isVisible() {
        return ((Activity) this.b).hasWindowFocus() ? "true" : "false";
    }

    public boolean isWapNetwork() {
        return !be.b(Proxy.getDefaultHost());
    }

    public boolean isWifi() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.b.getSystemService("connectivity")).getActiveNetworkInfo();
            return "wifi".equals(activeNetworkInfo != null ? !activeNetworkInfo.getTypeName().toLowerCase().equals("mobile") ? activeNetworkInfo.getTypeName().toLowerCase() : activeNetworkInfo.getExtraInfo().toLowerCase() : "");
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public void load(String str) {
        if (str != null) {
            try {
                if (!"".equals(str)) {
                    this.c = this.b.getPackageManager();
                    Intent launchIntentForPackage = this.c.getLaunchIntentForPackage(str);
                    if (launchIntentForPackage != null) {
                        this.b.startActivity(launchIntentForPackage);
                    } else {
                        Log.i("APP_SDK", "The app is not exist.");
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public InputStream loadStreamFromLocal(String str, String str2) {
        try {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                File file = new File((Environment.getExternalStorageDirectory().toString() + str2) + "/" + str);
                if (file.exists() && file.length() > 0) {
                    return new FileInputStream(file);
                }
            }
            File fileStreamPath = this.b.getFileStreamPath(str);
            if (fileStreamPath.exists() && fileStreamPath.length() > 0) {
                return new FileInputStream(fileStreamPath);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:31:0x00b0 */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r2v8 */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARN: Type inference failed for: r2v11 */
    /* JADX WARN: Type inference failed for: r2v12 */
    /* JADX WARN: Type inference failed for: r2v15 */
    /* JADX WARN: Type inference failed for: r2v17 */
    /* JADX WARN: Type inference failed for: r2v18 */
    /* JADX WARN: Type inference failed for: r2v19 */
    /* JADX WARN: Type inference failed for: r2v22 */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0127, code lost:
        r0 = th;
        r2 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0142, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0143, code lost:
        r1 = r2;
        r2 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00be A[LOOP:1: B:35:0x00be->B:38:0x00c4, LOOP_START, PHI: r0 
      PHI: (r0v13 java.lang.String) = (r0v0 java.lang.String), (r0v19 java.lang.String) binds: [B:34:0x00bc, B:38:0x00c4] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:35:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0107 A[SYNTHETIC, Splitter:B:60:0x0107] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x010c A[Catch:{ IOException -> 0x0110 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0119 A[SYNTHETIC, Splitter:B:69:0x0119] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x011e A[Catch:{ IOException -> 0x0122 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0127 A[ExcHandler: all (th java.lang.Throwable), PHI: r2 r3 
      PHI: (r2v4 ?) = (r2v5 ?), (r2v5 ?), (r2v0 ?), (r2v0 ?) binds: [B:31:0x00b0, B:32:?, B:9:0x0059, B:10:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r3v4 java.io.FileInputStream) = (r3v9 java.io.FileInputStream), (r3v9 java.io.FileInputStream), (r3v14 java.io.FileInputStream), (r3v14 java.io.FileInputStream) binds: [B:31:0x00b0, B:32:?, B:9:0x0059, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0059] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String loadStringFromLocal(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            r7 = 0
            r2 = 0
            java.lang.String r0 = ""
            java.lang.String r1 = "mounted"
            java.lang.String r3 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            boolean r1 = r1.equals(r3)     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            if (r1 == 0) goto L_0x0149
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            r1.<init>()     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.lang.StringBuilder r1 = r1.append(r12)     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            r3.<init>()     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.lang.StringBuilder r1 = r1.append(r11)     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            boolean r1 = r4.exists()     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            if (r1 == 0) goto L_0x0149
            long r5 = r4.length()     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            int r1 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r1 <= 0) goto L_0x0149
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0100, all -> 0x0115 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0136, all -> 0x0127 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0136, all -> 0x0127 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0136, all -> 0x0127 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0136, all -> 0x0127 }
            java.lang.String r2 = ""
            if (r1 == 0) goto L_0x0095
        L_0x0067:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x013a, all -> 0x0129 }
            if (r2 == 0) goto L_0x0085
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013a, all -> 0x0129 }
            r4.<init>()     // Catch:{ Exception -> 0x013a, all -> 0x0129 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x013a, all -> 0x0129 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x013a, all -> 0x0129 }
            java.lang.String r2 = "\n"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x013a, all -> 0x0129 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x013a, all -> 0x0129 }
            goto L_0x0067
        L_0x0085:
            if (r3 == 0) goto L_0x008a
            r3.close()     // Catch:{ IOException -> 0x0090 }
        L_0x008a:
            if (r1 == 0) goto L_0x008f
            r1.close()     // Catch:{ IOException -> 0x0090 }
        L_0x008f:
            return r0
        L_0x0090:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x008f
        L_0x0095:
            r2 = r1
            r1 = r3
        L_0x0097:
            android.content.Context r3 = r10.b     // Catch:{ Exception -> 0x013d, all -> 0x012c }
            java.io.File r4 = r3.getFileStreamPath(r11)     // Catch:{ Exception -> 0x013d, all -> 0x012c }
            boolean r3 = r4.exists()     // Catch:{ Exception -> 0x013d, all -> 0x012c }
            if (r3 == 0) goto L_0x00ee
            long r5 = r4.length()     // Catch:{ Exception -> 0x013d, all -> 0x012c }
            int r3 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r3 <= 0) goto L_0x00ee
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x013d, all -> 0x012c }
            r3.<init>(r4)     // Catch:{ Exception -> 0x013d, all -> 0x012c }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0142, all -> 0x0127 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0142, all -> 0x0127 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0142, all -> 0x0127 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0142, all -> 0x0127 }
            java.lang.String r2 = ""
            if (r1 == 0) goto L_0x00ec
        L_0x00be:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0146, all -> 0x012f }
            if (r2 == 0) goto L_0x00dc
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0146, all -> 0x012f }
            r4.<init>()     // Catch:{ Exception -> 0x0146, all -> 0x012f }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x0146, all -> 0x012f }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0146, all -> 0x012f }
            java.lang.String r2 = "\n"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0146, all -> 0x012f }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0146, all -> 0x012f }
            goto L_0x00be
        L_0x00dc:
            if (r3 == 0) goto L_0x00e1
            r3.close()     // Catch:{ IOException -> 0x00e7 }
        L_0x00e1:
            if (r1 == 0) goto L_0x008f
            r1.close()     // Catch:{ IOException -> 0x00e7 }
            goto L_0x008f
        L_0x00e7:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x008f
        L_0x00ec:
            r2 = r1
            r1 = r3
        L_0x00ee:
            if (r1 == 0) goto L_0x00f3
            r1.close()     // Catch:{ IOException -> 0x00fb }
        L_0x00f3:
            if (r2 == 0) goto L_0x00f8
            r2.close()     // Catch:{ IOException -> 0x00fb }
        L_0x00f8:
            java.lang.String r0 = ""
            goto L_0x008f
        L_0x00fb:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00f8
        L_0x0100:
            r0 = move-exception
            r1 = r2
        L_0x0102:
            r0.printStackTrace()     // Catch:{ all -> 0x0132 }
            if (r2 == 0) goto L_0x010a
            r2.close()     // Catch:{ IOException -> 0x0110 }
        L_0x010a:
            if (r1 == 0) goto L_0x00f8
            r1.close()     // Catch:{ IOException -> 0x0110 }
            goto L_0x00f8
        L_0x0110:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00f8
        L_0x0115:
            r0 = move-exception
            r3 = r2
        L_0x0117:
            if (r3 == 0) goto L_0x011c
            r3.close()     // Catch:{ IOException -> 0x0122 }
        L_0x011c:
            if (r2 == 0) goto L_0x0121
            r2.close()     // Catch:{ IOException -> 0x0122 }
        L_0x0121:
            throw r0
        L_0x0122:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0121
        L_0x0127:
            r0 = move-exception
            goto L_0x0117
        L_0x0129:
            r0 = move-exception
            r2 = r1
            goto L_0x0117
        L_0x012c:
            r0 = move-exception
            r3 = r1
            goto L_0x0117
        L_0x012f:
            r0 = move-exception
            r2 = r1
            goto L_0x0117
        L_0x0132:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x0117
        L_0x0136:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0102
        L_0x013a:
            r0 = move-exception
            r2 = r3
            goto L_0x0102
        L_0x013d:
            r0 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x0102
        L_0x0142:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0102
        L_0x0146:
            r0 = move-exception
            r2 = r3
            goto L_0x0102
        L_0x0149:
            r1 = r2
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: igudi.com.ergushi.SDKUtils.loadStringFromLocal(java.lang.String, java.lang.String):java.lang.String");
    }

    public void openAd() {
        openAd("");
    }

    public void openAd(String str) {
        try {
            this.g.post(new bj(this, str));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void openUrlByBrowser(String str, String str2) {
        try {
            String browserPackageName = getBrowserPackageName(str);
            if (browserPackageName == null || "".equals(browserPackageName.trim())) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
                intent.setFlags(268435456);
                this.b.startActivity(intent);
                return;
            }
            goToTargetBrowser(browserPackageName, str2, this.b.getPackageManager());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public Intent openUrlByBrowser_Intent(String str, String str2) {
        try {
            String browserPackageName = getBrowserPackageName(str);
            if (browserPackageName != null && !"".equals(browserPackageName.trim())) {
                return goToTargetBrowser_Intent(browserPackageName, str2, this.b.getPackageManager());
            }
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
            intent.setFlags(268435456);
            return intent;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public String replaceData(String str) {
        return (str.equals("") || !str.equals("a")) ? str : str.replace("a", "");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076 A[SYNTHETIC, Splitter:B:21:0x0076] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00af A[SYNTHETIC, Splitter:B:39:0x00af] */
    /* JADX WARNING: Removed duplicated region for block: B:58:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void saveDataToLocal(java.io.InputStream r9, java.lang.String r10, java.lang.String r11, boolean r12) {
        /*
            r8 = this;
            r6 = -1
            r0 = 0
            r1 = 0
            r2 = 10240(0x2800, float:1.4349E-41)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0094 }
            java.lang.String r3 = "mounted"
            java.lang.String r4 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x0094 }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0094 }
            if (r3 == 0) goto L_0x0096
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0094 }
            r0.<init>()     // Catch:{ Exception -> 0x0094 }
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x0094 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0094 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x0094 }
            java.lang.StringBuilder r0 = r0.append(r11)     // Catch:{ Exception -> 0x0094 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0094 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0094 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0094 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0094 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0094 }
            r5.<init>()     // Catch:{ Exception -> 0x0094 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x0094 }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x0094 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0094 }
            r4.<init>(r0, r10)     // Catch:{ Exception -> 0x0094 }
            boolean r0 = r3.exists()     // Catch:{ Exception -> 0x0094 }
            if (r0 != 0) goto L_0x0052
            r3.mkdirs()     // Catch:{ Exception -> 0x0094 }
        L_0x0052:
            boolean r0 = r4.exists()     // Catch:{ Exception -> 0x0094 }
            if (r0 != 0) goto L_0x005b
            r4.createNewFile()     // Catch:{ Exception -> 0x0094 }
        L_0x005b:
            if (r4 == 0) goto L_0x007a
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0094 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0094 }
        L_0x0062:
            int r1 = r9.read(r2)     // Catch:{ Exception -> 0x006d, all -> 0x00ca }
            if (r1 == r6) goto L_0x007b
            r3 = 0
            r0.write(r2, r3, r1)     // Catch:{ Exception -> 0x006d, all -> 0x00ca }
            goto L_0x0062
        L_0x006d:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0071:
            r0.printStackTrace()     // Catch:{ all -> 0x00ac }
            if (r1 == 0) goto L_0x0079
            r1.close()     // Catch:{ IOException -> 0x00c0 }
        L_0x0079:
            return
        L_0x007a:
            r0 = r1
        L_0x007b:
            r1 = r0
        L_0x007c:
            if (r12 == 0) goto L_0x00b5
            android.content.Context r0 = r8.b     // Catch:{ Exception -> 0x0094 }
            r2 = 0
            java.io.FileOutputStream r1 = r0.openFileOutput(r10, r2)     // Catch:{ Exception -> 0x0094 }
            r0 = 10240(0x2800, float:1.4349E-41)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0094 }
        L_0x0089:
            int r2 = r9.read(r0)     // Catch:{ Exception -> 0x0094 }
            if (r2 == r6) goto L_0x00b5
            r3 = 0
            r1.write(r0, r3, r2)     // Catch:{ Exception -> 0x0094 }
            goto L_0x0089
        L_0x0094:
            r0 = move-exception
            goto L_0x0071
        L_0x0096:
            android.content.Context r2 = r8.b     // Catch:{ Exception -> 0x0094 }
            r3 = 0
            java.io.FileOutputStream r1 = r2.openFileOutput(r10, r3)     // Catch:{ Exception -> 0x0094 }
            r2 = 10240(0x2800, float:1.4349E-41)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0094 }
        L_0x00a1:
            int r3 = r9.read(r2)     // Catch:{ Exception -> 0x0094 }
            if (r3 == r6) goto L_0x00b3
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ Exception -> 0x0094 }
            goto L_0x00a1
        L_0x00ac:
            r0 = move-exception
        L_0x00ad:
            if (r1 == 0) goto L_0x00b2
            r1.close()     // Catch:{ IOException -> 0x00c5 }
        L_0x00b2:
            throw r0
        L_0x00b3:
            r12 = r0
            goto L_0x007c
        L_0x00b5:
            if (r1 == 0) goto L_0x0079
            r1.close()     // Catch:{ IOException -> 0x00bb }
            goto L_0x0079
        L_0x00bb:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0079
        L_0x00c0:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0079
        L_0x00c5:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b2
        L_0x00ca:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: igudi.com.ergushi.SDKUtils.saveDataToLocal(java.io.InputStream, java.lang.String, java.lang.String, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0094 A[SYNTHETIC, Splitter:B:30:0x0094] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00a0 A[SYNTHETIC, Splitter:B:36:0x00a0] */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void saveDataToLocal(java.lang.String r8, java.lang.String r9, java.lang.String r10, boolean r11) {
        /*
            r7 = this;
            r0 = 0
            r1 = 0
            java.lang.String r2 = "UTF-8"
            byte[] r2 = r8.getBytes(r2)     // Catch:{ Exception -> 0x008e }
            java.lang.String r3 = "mounted"
            java.lang.String r4 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x008e }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x008e }
            if (r3 == 0) goto L_0x007d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008e }
            r0.<init>()     // Catch:{ Exception -> 0x008e }
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x008e }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x008e }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x008e }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ Exception -> 0x008e }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x008e }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x008e }
            r3.<init>(r0)     // Catch:{ Exception -> 0x008e }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x008e }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008e }
            r5.<init>()     // Catch:{ Exception -> 0x008e }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x008e }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x008e }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ Exception -> 0x008e }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x008e }
            r4.<init>(r0)     // Catch:{ Exception -> 0x008e }
            boolean r0 = r3.exists()     // Catch:{ Exception -> 0x008e }
            if (r0 != 0) goto L_0x0057
            r3.mkdirs()     // Catch:{ Exception -> 0x008e }
        L_0x0057:
            boolean r0 = r4.exists()     // Catch:{ Exception -> 0x008e }
            if (r0 != 0) goto L_0x0060
            r4.createNewFile()     // Catch:{ Exception -> 0x008e }
        L_0x0060:
            if (r4 == 0) goto L_0x00b3
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x008e }
            r0.<init>(r4)     // Catch:{ Exception -> 0x008e }
            r0.write(r2)     // Catch:{ Exception -> 0x00ae, all -> 0x00a9 }
        L_0x006a:
            r1 = r0
        L_0x006b:
            if (r11 == 0) goto L_0x0077
            android.content.Context r0 = r7.b     // Catch:{ Exception -> 0x008e }
            r3 = 0
            java.io.FileOutputStream r1 = r0.openFileOutput(r9, r3)     // Catch:{ Exception -> 0x008e }
            r1.write(r2)     // Catch:{ Exception -> 0x008e }
        L_0x0077:
            if (r1 == 0) goto L_0x007c
            r1.close()     // Catch:{ IOException -> 0x0089 }
        L_0x007c:
            return
        L_0x007d:
            android.content.Context r3 = r7.b     // Catch:{ Exception -> 0x008e }
            r4 = 0
            java.io.FileOutputStream r1 = r3.openFileOutput(r9, r4)     // Catch:{ Exception -> 0x008e }
            r1.write(r2)     // Catch:{ Exception -> 0x008e }
            r11 = r0
            goto L_0x006b
        L_0x0089:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x007c
        L_0x008e:
            r0 = move-exception
        L_0x008f:
            r0.printStackTrace()     // Catch:{ all -> 0x009d }
            if (r1 == 0) goto L_0x007c
            r1.close()     // Catch:{ IOException -> 0x0098 }
            goto L_0x007c
        L_0x0098:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x007c
        L_0x009d:
            r0 = move-exception
        L_0x009e:
            if (r1 == 0) goto L_0x00a3
            r1.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x00a3:
            throw r0
        L_0x00a4:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a3
        L_0x00a9:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x009e
        L_0x00ae:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x008f
        L_0x00b3:
            r0 = r1
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void");
    }

    public void sendSMS(String str, String str2) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SENDTO");
        intent.setData(Uri.parse("smsto:" + str));
        intent.putExtra("sms_body", str2);
        this.b.startActivity(intent);
    }

    public void showToast(String str) {
        Toast.makeText(this.b, str, 1).show();
    }

    public String[] splitString(String str, String str2, String str3) {
        if (str2 != null) {
            try {
                if (!"".equals(str2.trim())) {
                    if (str3 == null || str3.equals("")) {
                        str3 = str2;
                    }
                    if (str != null && !"".equals(str.trim())) {
                        if (str.endsWith(str2)) {
                            str = str.substring(0, str.lastIndexOf(str2));
                        }
                        if (str.indexOf(str2) > 0) {
                            return str.split(str3);
                        }
                        if (str.indexOf(str2) == 0) {
                            return new String[]{str.substring(1)};
                        }
                        return new String[]{str};
                    }
                    return null;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return new String[]{str};
    }

    public void submit(String str, String str2) {
        if (str2 == null || "".equals(str2)) {
            ((Activity) this.b).finish();
        } else {
            new AlertDialog.Builder(this.b).setTitle(str).setMessage(str2).setPositiveButton("确定", new bg(this)).create().show();
        }
    }
}
