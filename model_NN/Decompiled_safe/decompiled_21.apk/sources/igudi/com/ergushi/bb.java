package igudi.com.ergushi;

import android.content.Context;
import android.view.View;

public final class bb implements View.OnClickListener {
    int a = 0;
    Context b;
    final /* synthetic */ MiniAdView c;

    public bb(MiniAdView miniAdView, Context context, int i) {
        this.c = miniAdView;
        this.a = i;
        this.b = context;
    }

    public void onClick(View view) {
        try {
            AppConnect.getInstance(this.b).clickAd(this.b, ((AdInfo) AppConnect.i.get(this.a)).getAdId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
