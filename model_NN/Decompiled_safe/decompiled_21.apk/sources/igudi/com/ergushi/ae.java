package igudi.com.ergushi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class ae extends BroadcastReceiver {
    public static boolean c = false;
    public static boolean d = false;
    private static String e = "";
    int a;
    ac b;
    private String f;
    private SharedPreferences g;
    private SharedPreferences.Editor h;

    public ae(ac acVar, int i, String str) {
        this.a = i;
        this.b = acVar;
        this.f = str;
    }

    public ae(String str) {
        this.f = str;
    }

    public void onReceive(Context context, Intent intent) {
        try {
            if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
                e = intent.getDataString().substring(8);
                AppConnect.getInstance(context).a(e, 0);
                d = true;
                try {
                    if (this.b != null) {
                        this.b.a(this.a, this.f);
                    }
                    Intent a2 = new be(context).a(context, e);
                    if (a2 != null) {
                        context.startActivity(a2);
                    }
                    c = true;
                    if (ao.d) {
                        this.g = context.getSharedPreferences("DownLoadSave", 3);
                        this.h = this.g.edit();
                        this.h.putString(this.g.getAll().size() + "", e);
                        this.h.commit();
                        c = true;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
                AppConnect.getInstance(context).a(intent.getDataString().substring(8), 3);
            }
            context.unregisterReceiver(this);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
