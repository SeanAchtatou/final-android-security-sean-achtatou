package igudi.com.ergushi;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.net.Proxy;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.umeng.common.b.e;
import java.io.File;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

public class be {
    private Context a;
    private SDKUtils b;

    protected be(Context context) {
        this.a = context;
        this.b = new SDKUtils(context);
    }

    protected static String a(Context context) {
        String installed = new SDKUtils(context).getInstalled();
        StringBuffer stringBuffer = new StringBuffer();
        for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (installed.contains(next.processName)) {
                stringBuffer.append(next.processName);
                stringBuffer.append(";");
            }
        }
        return stringBuffer.toString();
    }

    private String a(String str, String str2) {
        try {
            if (!b(str)) {
                if (str.endsWith(";")) {
                    str = str.substring(0, str.length() - 1);
                }
                if (str.contains(";")) {
                    for (String split : str.split(";")) {
                        String[] split2 = split.split(",");
                        if (split2[0].equals(str2)) {
                            return split2[1];
                        }
                    }
                } else {
                    String[] split3 = str.split(",");
                    if (split3[0].equals(str2)) {
                        return split3[1];
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean a() {
        String[] strArr = {"/system/bin/", "/system/xbin/", "/system/sbin/", "/sbin/", "/vendor/bin/"};
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            File file = new File(strArr[i] + "su");
            if (file != null && file.exists()) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(int i, int i2) {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return false;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((long) i) < ((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) >> i2);
    }

    protected static String b(Context context, String str) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                String string = applicationInfo.metaData.getString(str);
                if (!b(string)) {
                    return string;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    protected static boolean b(String str) {
        return str == null || "".equals(str.trim());
    }

    protected static String c(Context context, String str) {
        String str2 = "";
        try {
            if (!b(str)) {
                str2 = b(context, str);
            }
            return b(str2) ? OffersWebView.class.getCanonicalName() : str2;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String c(String str) {
        try {
            return new String(ag.a(str, 8));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String d(String str) {
        try {
            return ag.b(str.getBytes(), 8);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public Intent a(Context context, String str) {
        Exception e;
        Intent intent;
        try {
            intent = context.getPackageManager().getLaunchIntentForPackage(str);
            if (intent != null) {
                try {
                    String string = context.getSharedPreferences("AppSettings", 0).getString("def_url", "");
                    if (!b(string)) {
                        String a2 = a(string, str);
                        if (!b(a2)) {
                            intent.setAction("android.intent.action.VIEW");
                            intent.addCategory("android.intent.category.DEFAULT");
                            intent.setData(Uri.parse(a2));
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return intent;
                }
            }
        } catch (Exception e3) {
            Exception exc = e3;
            intent = null;
            e = exc;
        }
        return intent;
    }

    /* access modifiers changed from: protected */
    public String a(String str, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            if (!b(str) && i > 0) {
                for (int i2 = 0; i2 < i; i2++) {
                    stringBuffer.append(str.charAt(((int) (Math.random() * 100.0d)) % 30));
                }
                return stringBuffer.toString().toLowerCase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection a(String str) {
        try {
            if (b(Proxy.getDefaultHost()) || !new SDKUtils(this.a).isCmwap()) {
                return (HttpURLConnection) new URL(str).openConnection();
            }
            return (HttpURLConnection) new URL(str).openConnection(new java.net.Proxy(Proxy.Type.HTTP, new InetSocketAddress(android.net.Proxy.getDefaultHost(), android.net.Proxy.getDefaultPort())));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection a(String str, List list, byte[] bArr) {
        HttpURLConnection httpURLConnection;
        Exception exc;
        String str2 = bArr != null ? "POST" : "GET";
        try {
            HttpURLConnection a2 = a(str);
            try {
                a2.setConnectTimeout(15000);
                a2.setReadTimeout(15000);
                a2.setRequestMethod(str2);
                a2.setRequestProperty("Accept", "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
                a2.setRequestProperty("Accept-Language", "zh-CN");
                a2.setRequestProperty("Referer", str);
                a2.setRequestProperty("Charset", e.f);
                a2.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)");
                a2.setRequestProperty("Connection", "Keep-Alive");
                if (list != null && list.size() > 0) {
                    Iterator it = list.iterator();
                    while (it.hasNext()) {
                        String[] split = ((String) it.next()).split(";");
                        a2.setRequestProperty(split[0], split[1]);
                    }
                }
                if ("POST".equals(str2)) {
                    a2.setDoOutput(true);
                    OutputStream outputStream = a2.getOutputStream();
                    outputStream.write(bArr);
                    outputStream.flush();
                    outputStream.close();
                }
                return a2;
            } catch (Exception e) {
                Exception exc2 = e;
                httpURLConnection = a2;
                exc = exc2;
                exc.printStackTrace();
                return httpURLConnection;
            }
        } catch (Exception e2) {
            Exception exc3 = e2;
            httpURLConnection = null;
            exc = exc3;
            exc.printStackTrace();
            return httpURLConnection;
        }
    }

    public boolean a(WebView webView, String str) {
        String str2;
        try {
            SharedPreferences.Editor edit = this.a.getSharedPreferences("AppSettings", 0).edit();
            if (!str.contains(ak.h()) || !str.contains(ak.i())) {
                edit.putBoolean("isRefresh", false);
                edit.commit();
            }
            if (str.startsWith("tel:")) {
                this.b.callTel(str.substring(str.indexOf("tel:") + 4));
                return true;
            } else if (str.startsWith("wtai://")) {
                this.b.callTel(str.substring(str.lastIndexOf(";")));
                return true;
            } else if (str.startsWith("http://")) {
                if (str.contains("noRefresh=")) {
                    edit.putBoolean("isRefresh", false);
                    edit.commit();
                }
                if (str.contains("browser=")) {
                    String substring = str.substring(str.indexOf("browser=") + 8);
                    if (substring.indexOf("&") > 0) {
                        substring = substring.substring(0, substring.indexOf("&"));
                    }
                    new SDKUtils(this.a).openUrlByBrowser(substring, str);
                    return true;
                }
                webView.loadUrl(str);
                return true;
            } else {
                if (str.contains(";http://")) {
                    String substring2 = str.substring(0, str.indexOf(";http://"));
                    String substring3 = str.substring(str.indexOf("http://"));
                    str = substring2;
                    str2 = substring3;
                } else {
                    str2 = "";
                }
                if (!b(str)) {
                    if (str.startsWith("load://")) {
                        String substring4 = str.substring("load://".length());
                        if (substring4 != "") {
                            Intent a2 = a(this.a, substring4);
                            if (a2 != null) {
                                this.a.startActivity(a2);
                                try {
                                    AppConnect.getInstance(this.a).a(substring4, 1);
                                    return true;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    return true;
                                }
                            } else {
                                if (str2.contains("noRefresh=")) {
                                    edit.putBoolean("isRefresh", false);
                                    edit.commit();
                                }
                                if (str2.contains("browser=")) {
                                    String substring5 = str2.substring(str2.indexOf("browser=") + 8);
                                    if (substring5.indexOf("&") > 0) {
                                        substring5 = substring5.substring(0, substring5.indexOf("&"));
                                    }
                                    new SDKUtils(this.a).openUrlByBrowser(substring5, str2);
                                } else {
                                    webView.loadUrl(str2);
                                }
                            }
                        }
                    } else if (str.startsWith("market://")) {
                        try {
                            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + str.split("id=")[1])));
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            try {
                                new AlertDialog.Builder(this.a).setTitle("").setMessage("Android market is unavailable at this device. To view this link install market.").setPositiveButton((CharSequence) AppConnect.e(this.a).get("ok"), new bf(this)).create().show();
                            } catch (Exception e3) {
                                e3.printStackTrace();
                            }
                        }
                    } else {
                        if (str2.contains("noRefresh=")) {
                            edit.putBoolean("isRefresh", false);
                            edit.commit();
                        }
                        if (str.contains("browser=")) {
                            String substring6 = str.substring(str.indexOf("browser=") + 8);
                            if (substring6.indexOf("&") > 0) {
                                substring6 = substring6.substring(0, substring6.indexOf("&"));
                            }
                            new SDKUtils(this.a).openUrlByBrowser(substring6, str);
                            return true;
                        }
                        webView.loadUrl(str);
                        return true;
                    }
                }
                edit.commit();
                return false;
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    public Object[] e(String str) {
        try {
            Class<?> cls = Class.forName("android.content.pm.PackageParser");
            Object newInstance = cls.getConstructor(String.class).newInstance(str);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            displayMetrics.setToDefaults();
            Object invoke = cls.getDeclaredMethod("parsePackage", File.class, String.class, DisplayMetrics.class, Integer.TYPE).invoke(newInstance, new File(str), str, displayMetrics, 0);
            ApplicationInfo applicationInfo = (ApplicationInfo) invoke.getClass().getDeclaredField("applicationInfo").get(invoke);
            Class<?> cls2 = Class.forName("android.content.res.AssetManager");
            Object newInstance2 = cls2.getConstructor(null).newInstance(null);
            cls2.getDeclaredMethod("addAssetPath", String.class).invoke(newInstance2, str);
            Resources resources = this.a.getResources();
            Resources newInstance3 = Resources.class.getConstructor(newInstance2.getClass(), resources.getDisplayMetrics().getClass(), resources.getConfiguration().getClass()).newInstance(newInstance2, resources.getDisplayMetrics(), resources.getConfiguration());
            if (applicationInfo.icon != 0) {
                return new Object[]{Integer.valueOf(applicationInfo.icon), newInstance3};
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
