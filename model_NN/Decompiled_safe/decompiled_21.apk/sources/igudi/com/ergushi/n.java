package igudi.com.ergushi;

import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

class n implements View.OnTouchListener {
    final /* synthetic */ LinearLayout a;
    final /* synthetic */ GradientDrawable b;
    final /* synthetic */ GradientDrawable c;
    final /* synthetic */ AppConnect d;

    n(AppConnect appConnect, LinearLayout linearLayout, GradientDrawable gradientDrawable, GradientDrawable gradientDrawable2) {
        this.d = appConnect;
        this.a = linearLayout;
        this.b = gradientDrawable;
        this.c = gradientDrawable2;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.a.setBackgroundDrawable(this.b);
                return false;
            case 1:
                this.a.setBackgroundDrawable(this.c);
                return false;
            case 2:
            default:
                return false;
            case 3:
                this.a.setBackgroundDrawable(this.c);
                return false;
        }
    }
}
