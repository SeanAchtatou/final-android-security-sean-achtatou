package igudi.com.ergushi;

import java.util.HashMap;
import java.util.Map;

public class aw {
    protected static Map a() {
        HashMap hashMap = new HashMap();
        hashMap.put("failed_to_update_points", "无法更新积分");
        hashMap.put("failed_to_spend_points", "消费积分失败");
        hashMap.put("failed_to_award_points", "奖励积分失败");
        hashMap.put("update_version", "新版本提示");
        hashMap.put("is_newest_version", "您使用的已经是最新版本");
        hashMap.put("new_version", "有新版本（");
        hashMap.put("version_is_found", "），是否下载？");
        hashMap.put("download", "下载");
        hashMap.put("next_time", "下次再说");
        hashMap.put("getting_filename", "正在获取下载资源");
        hashMap.put("failed_to_download", "下载失败，请重新下载");
        hashMap.put("click_to_install", "下载完成，点击安装");
        hashMap.put("download_complete", "下载完成！");
        hashMap.put("new_app_is_found", "发现新应用");
        hashMap.put("new_app_install", "您有新应用安装，点击查看！");
        hashMap.put("new_app", "新安装应用");
        hashMap.put("downloading", "正在下载");
        hashMap.put("finish_download", "正在下载，已完成");
        hashMap.put("exception", "程序异常：");
        hashMap.put("reminder", "温馨提示");
        hashMap.put("app_already_exists", "该安装文件已存在于/sdcard/download/目录下，您可以直接安装或重新下载！");
        hashMap.put("install", "安装");
        hashMap.put("download_again", "重新下载");
        hashMap.put("prepare_to_download", "正在准备下载,请稍候...");
        hashMap.put("cancel", "取消");
        hashMap.put("waitting_for_download", "正在下载,请稍候...");
        hashMap.put("network_links_failure", "网络连接失败，请检查网络。");
        hashMap.put("ok", "确定");
        hashMap.put("close", "关闭");
        hashMap.put("new_app_list", "新安装应用列表");
        hashMap.put("message_title", "提示");
        hashMap.put("wrong_url", "未获取到正确的下载地址！");
        hashMap.put("points", "积分");
        hashMap.put("download_failed", "下载中断！已完成%");
        hashMap.put("ad_version", "版本:");
        hashMap.put("ad_size", "大小:");
        hashMap.put("Try", "试试看");
        hashMap.put("Other", "换一个");
        return hashMap;
    }

    protected static Map b() {
        HashMap hashMap = new HashMap();
        hashMap.put("failed_to_update_points", "Failed to update points.");
        hashMap.put("failed_to_spend_points", "Failed to spend points.");
        hashMap.put("failed_to_award_points", "Failed to award points.");
        hashMap.put("update_version", "Update For Version");
        hashMap.put("is_newest_version", "The version is newest.");
        hashMap.put("new_version", "New version(");
        hashMap.put("version_is_found", ")is found. Download now?");
        hashMap.put("download", "Download");
        hashMap.put("next_time", "Next time");
        hashMap.put("getting_filename", "File is getting.");
        hashMap.put("failed_to_download", "Failed to download!Please try again!");
        hashMap.put("click_to_install", "The download is complete.Click to install!");
        hashMap.put("download_complete", "The download is complete.");
        hashMap.put("new_app_is_found", "New Application is found!");
        hashMap.put("new_app_install", "New Application is installed.Click to check.");
        hashMap.put("new_app", "New Application");
        hashMap.put("downloading", "Downloading");
        hashMap.put("finish_download", "The download is finish");
        hashMap.put("exception", "Exception");
        hashMap.put("reminder", "Reminder");
        hashMap.put("app_already_exists", "The application is already exists in \"/sdcard/download/\",you can install or download again.");
        hashMap.put("install", "Install");
        hashMap.put("download_again", "Download");
        hashMap.put("prepare_to_download", "Prepare to download,please wait...");
        hashMap.put("cancel", "Cancel");
        hashMap.put("waitting_for_download", "Downloading,please wait...");
        hashMap.put("network_links_failure", "The network links failure,please check.");
        hashMap.put("ok", "OK");
        hashMap.put("close", "Close");
        hashMap.put("new_app_list", "New Application List");
        hashMap.put("message_title", "Message");
        hashMap.put("wrong_url", "The url of download is wrong!");
        hashMap.put("points", "Points");
        hashMap.put("download_failed", "Failed at %,Click to contiue");
        hashMap.put("ad_version", "ver:");
        hashMap.put("ad_size", "size:");
        hashMap.put("Try", "Try");
        hashMap.put("Other", "Other");
        return hashMap;
    }
}
