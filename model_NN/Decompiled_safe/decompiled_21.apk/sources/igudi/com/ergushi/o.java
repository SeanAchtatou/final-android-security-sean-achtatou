package igudi.com.ergushi;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

class o extends AsyncTask {
    Context a;
    final /* synthetic */ AppConnect b;

    o(AppConnect appConnect, Context context) {
        this.b = appConnect;
        this.a = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        try {
            String str = AppConnect.c;
            if (be.b(AppConnect.aR)) {
                String unused = AppConnect.aR = AppConnect.s.a(ak.c() + ak.p(), str);
            }
            if (!be.b(AppConnect.aR)) {
                z = this.b.g(AppConnect.aR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        try {
            if (be.b(this.b.K)) {
                new AlertDialog.Builder(this.a).setTitle((CharSequence) AppConnect.f.get("update_version")).setMessage((CharSequence) AppConnect.f.get("network_links_failure")).setPositiveButton((CharSequence) AppConnect.f.get("ok"), new q(this)).create().show();
            } else if (this.b.K.compareTo(AppConnect.z) > 0) {
                this.b.h(ak.c() + ak.t() + AppConnect.c);
            } else {
                new AlertDialog.Builder(this.a).setTitle((CharSequence) AppConnect.f.get("update_version")).setMessage((CharSequence) AppConnect.f.get("is_newest_version")).setPositiveButton((CharSequence) AppConnect.f.get("ok"), new p(this)).create().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
