package igudi.com.ergushi;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

class l implements View.OnClickListener {
    final /* synthetic */ AdInfo a;
    final /* synthetic */ Dialog b;
    final /* synthetic */ LinearLayout c;
    final /* synthetic */ LinearLayout d;
    final /* synthetic */ Context e;
    final /* synthetic */ AppConnect f;

    l(AppConnect appConnect, AdInfo adInfo, Dialog dialog, LinearLayout linearLayout, LinearLayout linearLayout2, Context context) {
        this.f = appConnect;
        this.a = adInfo;
        this.b = dialog;
        this.c = linearLayout;
        this.d = linearLayout2;
        this.e = context;
    }

    public void onClick(View view) {
        if ("0".equals(this.f.aW)) {
            this.f.a(this.a.getAdId(), this.b);
        } else if ("1".equals(this.f.aW)) {
            this.c.setVisibility(0);
            this.d.setVisibility(0);
        } else if (new SDKUtils(this.e).isWifi()) {
            this.f.a(this.a.getAdId(), this.b);
        } else {
            this.c.setVisibility(0);
            this.d.setVisibility(0);
        }
    }
}
