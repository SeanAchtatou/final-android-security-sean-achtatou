package igudi.com.ergushi;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AnimationType {
    public static final int ALPHA = 5;
    public static final int MINI_RANDOM = -2;
    public static final int NONE = -1;
    public static final int RANDOM = 0;
    public static final int ROTATE = 4;
    public static final int SCALE_CENTER = 1;
    public static final int TRANSLATE_FROM_LEFT = 7;
    public static final int TRANSLATE_FROM_RIGHT = 6;
    private int[] a;
    private int b;

    public AnimationType() {
    }

    public AnimationType(int i) {
        this.b = i;
    }

    public AnimationType(int[] iArr) {
        this.a = iArr;
    }

    private static Map getAnimation(View view) {
        float f = ((float) view.getLayoutParams().width) / 2.0f;
        float f2 = ((float) view.getLayoutParams().height) / 2.0f;
        HashMap hashMap = new HashMap();
        hashMap.put("1", new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, f, f2));
        hashMap.put("4", new f(270.0f, 360.0f, f, f2, 0.0f, true));
        hashMap.put("5", new AlphaAnimation(0.0f, 1.0f));
        hashMap.put("6", new TranslateAnimation((float) view.getLayoutParams().width, 0.0f, 0.0f, 0.0f));
        hashMap.put("7", new TranslateAnimation((float) (-view.getLayoutParams().width), 0.0f, 0.0f, 0.0f));
        return hashMap;
    }

    private List getAnimationList(View view) {
        Map animation = getAnimation(view);
        ArrayList arrayList = new ArrayList();
        String str = "";
        for (int i = 0; i < this.a.length; i++) {
            str = str + this.a[i];
        }
        if (str.contains("0")) {
            arrayList.add(animation.get("1"));
            arrayList.add(animation.get("4"));
            arrayList.add(animation.get("5"));
            arrayList.add(animation.get("6"));
            arrayList.add(animation.get("7"));
            return arrayList;
        } else if (str.contains("-2")) {
            arrayList.add(animation.get("6"));
            arrayList.add(animation.get("7"));
            return arrayList;
        } else if (str.contains("-1")) {
            return arrayList;
        } else {
            for (int i2 = 0; i2 < this.a.length; i2++) {
                if (this.a[i2] >= 9 || this.a.length > 7) {
                    arrayList.add(animation.get("1"));
                    arrayList.add(animation.get("4"));
                    arrayList.add(animation.get("5"));
                    arrayList.add(animation.get("6"));
                    arrayList.add(animation.get("7"));
                    return arrayList;
                }
                arrayList.add(animation.get(this.a[i2] + ""));
            }
            return arrayList;
        }
    }

    private List getAnimationList2(View view) {
        Map animation = getAnimation(view);
        ArrayList arrayList = new ArrayList();
        switch (this.b) {
            case MINI_RANDOM /*-2*/:
                arrayList.add(animation.get("6"));
                arrayList.add(animation.get("7"));
                break;
            case 1:
                arrayList.add(animation.get("1"));
                break;
            case 4:
                arrayList.add(animation.get("4"));
                break;
            case 5:
                arrayList.add(animation.get("5"));
                break;
            case TRANSLATE_FROM_RIGHT /*6*/:
                arrayList.add(animation.get("6"));
                break;
            case TRANSLATE_FROM_LEFT /*7*/:
                arrayList.add(animation.get("7"));
                break;
        }
        return arrayList;
    }

    private static void startRotation(float f, float f2, View view) {
        f fVar = new f(f, f2, ((float) view.getWidth()) / 2.0f, ((float) view.getHeight()) / 2.0f, 0.0f, true);
        fVar.setDuration(2000);
        fVar.setFillAfter(true);
        fVar.setInterpolator(new DecelerateInterpolator());
        view.startAnimation(fVar);
    }

    public void startAnimation(View view) {
        List animationList2 = this.a == null ? getAnimationList2(view) : getAnimationList(view);
        Random random = new Random();
        if (animationList2.size() != 0) {
            Animation animation = (Animation) animationList2.get(random.nextInt(animationList2.size()));
            animation.setDuration(2000);
            animation.setInterpolator(new DecelerateInterpolator());
            view.startAnimation(animation);
        }
    }

    /* access modifiers changed from: protected */
    public void startMiniAdAnimation(int i, View view) {
        if (i == -2) {
            Map animation = getAnimation(view);
            ArrayList arrayList = new ArrayList();
            arrayList.add(animation.get("6"));
            arrayList.add(animation.get("7"));
            Random random = new Random();
            if (arrayList.size() != 0) {
                Animation animation2 = (Animation) arrayList.get(random.nextInt(arrayList.size()));
                animation2.setDuration(2000);
                animation2.setInterpolator(new DecelerateInterpolator());
                view.startAnimation(animation2);
            }
        }
    }
}
