package igudi.com.ergushi;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.umeng.fb.f;
import java.text.DecimalFormat;

class ap extends Handler {
    final /* synthetic */ ao a;

    ap(ao aoVar) {
        this.a = aoVar;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        Bundle data = message.getData();
        switch (message.what) {
            case 0:
                String string = data.getString(f.an);
                if (!be.b(string)) {
                    Toast.makeText(this.a.u, string, 1).show();
                    return;
                }
                return;
            case 1:
                String string2 = data.getString("notify_title");
                int i = data.getInt("notify_id");
                double d = data.getDouble("downSize");
                double d2 = data.getDouble("fileSize");
                int i2 = data.getInt("downSpeed");
                int i3 = data.getInt("remainTime");
                String str = i3 >= 60 ? (i3 / 60) + "分钟" : i3 + "秒";
                int i4 = (int) ((d / d2) * 100.0d);
                if (i4 == 100) {
                    this.a.s.a(string2, i, "下载完成，大小" + new DecimalFormat("#.00").format((d2 / 1024.0d) / 1024.0d) + "M");
                    return;
                } else {
                    this.a.s.a(string2, i, i2 > 1024 ? "已完成" + i4 + "%  " + (i2 / 1024) + "M/秒  " + "剩余" + str : "已完成" + i4 + "%  " + i2 + "K/秒  " + "剩余" + str);
                    return;
                }
            case 2:
                Toast.makeText(this.a.u, data.getString("installToast"), 1).show();
                return;
            default:
                return;
        }
    }
}
