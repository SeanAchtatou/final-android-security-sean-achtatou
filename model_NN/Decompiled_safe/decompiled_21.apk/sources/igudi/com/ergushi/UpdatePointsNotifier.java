package igudi.com.ergushi;

public interface UpdatePointsNotifier {
    void getUpdatePoints(String str, int i);

    void getUpdatePointsFailed(String str);
}
