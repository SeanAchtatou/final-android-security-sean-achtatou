package igudi.com.ergushi;

public interface AdViewCloseListener {
    void onAdViewClosed();
}
