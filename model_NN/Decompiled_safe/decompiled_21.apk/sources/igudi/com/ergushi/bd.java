package igudi.com.ergushi;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.umeng.fb.f;
import java.net.URLDecoder;

class bd extends WebViewClient {
    final /* synthetic */ OffersWebView a;

    private bd(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    /* synthetic */ bd(OffersWebView offersWebView, bc bcVar) {
        this(offersWebView);
    }

    public void onPageFinished(WebView webView, String str) {
        this.a.o.setVisibility(8);
        if (this.a.f != null) {
            this.a.f.setVisibility(0);
        }
        try {
            if (str.contains("androidTuan/list?")) {
                webView.loadUrl("javascript:window.SDKUtils.getHtml(document.head.innerHTML);");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.a.o.setVisibility(0);
        super.onPageStarted(webView, str, bitmap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: igudi.com.ergushi.OffersWebView.a(igudi.com.ergushi.OffersWebView, boolean):boolean
     arg types: [igudi.com.ergushi.OffersWebView, int]
     candidates:
      igudi.com.ergushi.OffersWebView.a(igudi.com.ergushi.OffersWebView, java.lang.String):java.lang.String
      igudi.com.ergushi.OffersWebView.a(igudi.com.ergushi.OffersWebView, boolean):boolean */
    public void onReceivedError(WebView webView, int i, String str, String str2) {
        try {
            webView.setScrollBarStyle(0);
            if (!str2.contains("market://")) {
                webView.loadDataWithBaseURL("", "<html><body bgcolor=\"000000\" align=\"center\"><br/><font color=\"ffffff\">" + ((String) OffersWebView.F.get("network_links_failure")) + "</font>" + "<br/></body></html>", "text/html", "utf-8", "");
            } else {
                webView.loadDataWithBaseURL("", "", "text/html", "utf-8", "");
            }
            boolean unused = this.a.z = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String unused = this.a.w = str;
        try {
            if (be.b(this.a.y) && !be.b(this.a.w) && this.a.w.contains("app_name")) {
                String unused2 = this.a.y = this.a.w.substring(this.a.w.indexOf("app_name=") + "app_name=".length());
                if (!be.b(this.a.y)) {
                    if (this.a.y.contains("&")) {
                        String unused3 = this.a.y = this.a.y.substring(0, this.a.y.indexOf("&"));
                    }
                    String unused4 = this.a.y = URLDecoder.decode(this.a.y, "UTF8");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (be.b(this.a.g)) {
                this.a.g = SDKUtils.a;
                if (!be.b(this.a.g)) {
                    this.a.g = this.a.g.substring(this.a.g.indexOf("info=\"") + "info=\"".length(), this.a.g.indexOf("\">"));
                    if (!be.b(this.a.g)) {
                        if (this.a.g.contains(";")) {
                            this.a.h = this.a.g.split(";");
                        } else {
                            this.a.h = new String[]{this.a.g};
                        }
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if ("http://m.dianping.com/".equalsIgnoreCase(str) || "http://m.dianping.com/tuan".equalsIgnoreCase(str) || str.startsWith("http://wap.dianping.com/") || str.startsWith("http://t.dianping.com/")) {
            str = this.a.n;
        } else if (str.startsWith("http://m.dianping.com/tuan/")) {
            String substring = str.substring("http://m.dianping.com/tuan/".length());
            if (!be.b(substring) && !substring.contains(f.z) && !substring.contains("receiptlist") && !substring.contains("orderlist") && !substring.contains("mobileBinding") && !substring.contains("usediscount") && !substring.contains("/") && !substring.contains("?")) {
                try {
                    if (this.a.h == null || this.a.h.length <= 0) {
                        str = this.a.n;
                    } else {
                        boolean z = false;
                        for (String contains : this.a.h) {
                            if (substring.contains(contains)) {
                                z = true;
                            }
                        }
                        if (!z) {
                            str = this.a.n;
                        }
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                    str = this.a.n;
                }
            }
        }
        if (this.a.w.contains("pkg=") && !this.a.w.contains(".apk")) {
            String unused5 = this.a.x = this.a.w.substring(this.a.w.indexOf("pkg=") + 4);
            if (this.a.x.contains("&")) {
                String unused6 = this.a.x = this.a.x.substring(0, this.a.x.indexOf("&"));
            }
        }
        return new be(this.a).a(webView, str);
    }
}
