package igudi.com.ergushi;

import android.graphics.Bitmap;

public class AdInfo {
    private String a = "";
    private String b = "";
    private String c = "";
    private int d = 0;
    private String e = "";
    private String f = "";
    private String g = "";
    private String h = "";
    private String[] i = null;
    private String j = "";
    private String k = "";
    private Bitmap l = null;
    private String m = "";
    private String n = "";
    private String o = "";
    private String p = "";
    private String q = "";
    private String r = "";
    private String s = "";
    private String t = "";
    private String u = "";
    private String v = "";
    private int w = 0;
    private int x = 0;
    private Bitmap y = null;
    private String z = "";

    /* access modifiers changed from: protected */
    public String a() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        this.d = i2;
    }

    /* access modifiers changed from: protected */
    public void a(Bitmap bitmap) {
        this.l = bitmap;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public void a(String[] strArr) {
        this.i = strArr;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return this.o;
    }

    /* access modifiers changed from: protected */
    public void b(int i2) {
        this.w = i2;
    }

    /* access modifiers changed from: protected */
    public void b(Bitmap bitmap) {
        this.y = bitmap;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.n = str;
    }

    /* access modifiers changed from: protected */
    public String c() {
        return this.p;
    }

    /* access modifiers changed from: protected */
    public void c(int i2) {
        this.x = i2;
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public String d() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public void d(String str) {
        this.o = str;
    }

    /* access modifiers changed from: protected */
    public String e() {
        return this.m;
    }

    /* access modifiers changed from: protected */
    public void e(String str) {
        this.p = str;
    }

    /* access modifiers changed from: protected */
    public String f() {
        return this.v;
    }

    /* access modifiers changed from: protected */
    public void f(String str) {
        this.j = str;
    }

    /* access modifiers changed from: protected */
    public int g() {
        return this.w;
    }

    /* access modifiers changed from: protected */
    public void g(String str) {
        this.q = str;
    }

    public String getAction() {
        return this.k;
    }

    public Bitmap getAdIcon() {
        return this.l;
    }

    public String getAdId() {
        return this.a;
    }

    public String getAdName() {
        return this.b;
    }

    public String getAdPackage() {
        return this.j;
    }

    public int getAdPoints() {
        return this.d;
    }

    public String getAdText() {
        return this.c;
    }

    public String getDescription() {
        return this.e;
    }

    public String getFilesize() {
        return this.g;
    }

    public String[] getImageUrls() {
        return this.i;
    }

    public String getProvider() {
        return this.h;
    }

    public String getVersion() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public int h() {
        return this.x;
    }

    /* access modifiers changed from: protected */
    public void h(String str) {
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public Bitmap i() {
        return this.y;
    }

    /* access modifiers changed from: protected */
    public void i(String str) {
        this.e = str;
    }

    /* access modifiers changed from: protected */
    public String j() {
        return this.z;
    }

    /* access modifiers changed from: protected */
    public void j(String str) {
        this.f = str;
    }

    /* access modifiers changed from: protected */
    public void k(String str) {
        this.g = str;
    }

    /* access modifiers changed from: protected */
    public void l(String str) {
        this.h = str;
    }

    /* access modifiers changed from: protected */
    public void m(String str) {
        this.m = str;
    }

    /* access modifiers changed from: protected */
    public void n(String str) {
        this.k = str;
    }

    /* access modifiers changed from: protected */
    public void o(String str) {
        this.u = str;
    }

    /* access modifiers changed from: protected */
    public void p(String str) {
        this.v = str;
    }

    /* access modifiers changed from: protected */
    public void q(String str) {
        this.z = str;
    }
}
