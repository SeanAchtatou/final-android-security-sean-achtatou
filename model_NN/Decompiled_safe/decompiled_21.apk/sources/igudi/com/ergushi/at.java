package igudi.com.ergushi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import java.io.File;

class at implements DialogInterface.OnClickListener {
    final /* synthetic */ Context a;
    final /* synthetic */ ar b;

    at(ar arVar, Context context) {
        this.b = arVar;
        this.a = context;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        ao.d = true;
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(this.b.f + this.b.e)), "application/vnd.android.package-archive");
        this.a.startActivity(intent);
        ae unused = this.b.g = new ae(this.b.e);
        ar.a(this.a, this.b.g);
    }
}
