package igudi.com.ergushi;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.net.URLDecoder;
import java.util.Map;

public class AdView {
    private static Map o;
    protected boolean a = true;
    protected Thread b = null;
    /* access modifiers changed from: private */
    public Context c;
    private LinearLayout d;
    /* access modifiers changed from: private */
    public long e;
    /* access modifiers changed from: private */
    public RelativeLayout f = null;
    /* access modifiers changed from: private */
    public int g = 1;
    /* access modifiers changed from: private */
    public boolean h = false;
    private WebView i;
    /* access modifiers changed from: private */
    public boolean j = true;
    /* access modifiers changed from: private */
    public int k = 0;
    private float l = 6.4f;
    /* access modifiers changed from: private */
    public String m = (ak.b() + ak.D());
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    private String n = "";
    private AdViewCloseListener p;

    public AdView(Context context, LinearLayout linearLayout) {
        a(context, linearLayout, null);
    }

    public AdView(Context context, LinearLayout linearLayout, AdViewCloseListener adViewCloseListener) {
        a(context, linearLayout, adViewCloseListener);
    }

    private WebView a() {
        WebView webView = new WebView(this.c);
        WebSettings settings = webView.getSettings();
        webView.setLayoutParams(new ViewGroup.LayoutParams(new SDKUtils(this.c).initAdWidth(), -2));
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new b(this, null));
        try {
            webView.addJavascriptInterface(new SDKUtils(this.c, this.mHandler, this.f, this.d, this.p), "SDKUtils");
            settings.setJavaScriptEnabled(true);
        } catch (Exception e2) {
        }
        settings.setDatabaseEnabled(true);
        WebView.enablePlatformNotifications();
        webView.setScrollBarStyle(0);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setBackgroundColor(0);
        this.e = System.currentTimeMillis();
        this.b = new a(this, webView);
        this.b.start();
        return webView;
    }

    private void a(Context context, LinearLayout linearLayout, AdViewCloseListener adViewCloseListener) {
        this.c = context;
        this.d = linearLayout;
        this.n = AppConnect.c;
        if (be.b(this.n)) {
            this.n = new AppConnect().c(context);
        }
        this.m += this.n;
        if (o == null || o.size() == 0) {
            o = AppConnect.e(context);
        }
        if (adViewCloseListener != null) {
            this.p = adViewCloseListener;
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        String str2;
        if (str.contains("down?")) {
            if (str.contains("&name=")) {
                str2 = str.substring(str.indexOf("&name=") + "&name=".length());
                if (str2.contains("&")) {
                    str2 = str2.substring(0, str2.indexOf("&"));
                }
            } else {
                str2 = "App" + System.currentTimeMillis();
            }
            try {
                if (str.contains("app_name")) {
                    str2 = str.substring(str.indexOf("app_name=") + "app_name=".length());
                    if (!be.b(str2)) {
                        String substring = str2.contains("&") ? str2.substring(0, str2.indexOf("&")) : str2;
                        try {
                            str2 = URLDecoder.decode(substring, "UTF8");
                        } catch (Exception e2) {
                            Exception exc = e2;
                            str2 = substring;
                            e = exc;
                        }
                    }
                }
            } catch (Exception e3) {
                e = e3;
                e.printStackTrace();
                new ar().b(this.c, str, str2);
                Toast.makeText(this.c, (CharSequence) o.get("prepare_to_download"), 1).show();
                return;
            }
            new ar().b(this.c, str, str2);
            Toast.makeText(this.c, (CharSequence) o.get("prepare_to_download"), 1).show();
            return;
        }
        Intent intent = new Intent(this.c, AppConnect.h(this.c));
        intent.putExtra("URL", str);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        if (str.contains("down?")) {
            intent.putExtra("isFinshClose", "true");
        }
        this.c.startActivity(intent);
    }

    public void DisplayAd() {
        DisplayAd(this.g);
    }

    public void DisplayAd(int i2) {
        if (this.c.getSharedPreferences("ShowAdFlag", 3).getBoolean("show_ad_flag", true)) {
            try {
                this.f = new RelativeLayout(this.c);
                this.f.setLayoutParams(new LinearLayout.LayoutParams(-2, 0));
                this.i = a();
                this.f.addView(this.i);
                this.d.addView(this.f);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
