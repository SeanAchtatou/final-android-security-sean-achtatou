package igudi.com.ergushi;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.File;

public class MiniAdView {
    private static final long DELETE_ICON_LIMIT_TIME = 604800000;
    static long e = 0;
    static int f = 5;
    /* access modifiers changed from: private */
    public static int r = 0;
    LinearLayout a;
    boolean b = false;
    View c;
    Context d;
    TextView g;
    private int[] h;
    private int i;
    private AnimationType j;
    private int k = -1;
    private String l = "";
    /* access modifiers changed from: private */
    public int m = AppConnect.h;
    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new ax(this);
    /* access modifiers changed from: private */
    public boolean n = true;
    /* access modifiers changed from: private */
    public int o = 0;
    private String p;
    private int q;
    private String s = "/Android/data/cache/iconCache";
    private String t = "CacheTime";
    private SharedPreferences u = null;
    private SharedPreferences.Editor v = null;
    private AdInfo w = null;
    private String x = ak.E();

    public MiniAdView() {
    }

    public MiniAdView(Context context) {
        this.d = context;
    }

    public MiniAdView(Context context, LinearLayout linearLayout) {
        this.d = context;
        a(this.d, this.s);
        this.a = linearLayout;
        this.a.setBackgroundColor(AppConnect.g);
        this.h = new int[]{-2};
        this.i = -2;
        this.k = 0;
        this.u = context.getSharedPreferences("AppSettings", 0);
        this.v = this.u.edit();
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(new float[]{5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f}, null, null));
        shapeDrawable.getPaint().setColor(AppConnect.g);
        this.a.setBackgroundDrawable(shapeDrawable);
    }

    private LinearLayout a(Context context, Bitmap bitmap, String str, int i2) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        linearLayout.setBackgroundColor(Color.argb(0, 0, 0, 0));
        linearLayout.setPadding(5, 0, 0, 0);
        int i3 = (int) (((float) i2) / 16.0f);
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new ViewGroup.LayoutParams((int) (((float) i2) / 16.0f), i3));
        if (bitmap != null) {
            if (bitmap.getWidth() > i3) {
                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            } else {
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageResource(17301516);
        }
        this.g = new TextView(context);
        this.g.setText(str);
        this.g.setTextSize(15.0f);
        this.g.setTextColor(this.m);
        this.g.setPadding(5, 0, 0, 0);
        linearLayout.addView(imageView);
        linearLayout.addView(this.g);
        return linearLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.io.InputStream, java.lang.String, java.lang.String, boolean):void
      igudi.com.ergushi.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String, boolean):void */
    private void a(Context context, String str) {
        try {
            String loadStringFromLocal = new SDKUtils(context).loadStringFromLocal(this.t, str);
            if (be.b(loadStringFromLocal)) {
                new SDKUtils(context).saveDataToLocal(String.valueOf(System.currentTimeMillis()), this.t, str, true);
            } else if (!be.b(loadStringFromLocal) && System.currentTimeMillis() - Long.parseLong(loadStringFromLocal.replaceAll("\n", "")) >= DELETE_ICON_LIMIT_TIME) {
                try {
                    if (Environment.getExternalStorageState().equals("mounted")) {
                        new SDKUtils(context).deleteLocalFiles(new File(Environment.getExternalStorageDirectory().toString() + str));
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                new SDKUtils(context).saveDataToLocal(System.currentTimeMillis() + "", this.t, str, true);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        try {
            this.a.removeView(this.a.getChildAt(0));
            this.a.removeAllViews();
            if (this.c != null && this.b) {
                if (r == 0) {
                    this.c.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
                    return;
                }
                this.a.refreshDrawableState();
                this.a.setAlwaysDrawnWithCacheEnabled(true);
                this.a.clearFocus();
                this.a.clearDisappearingChildren();
                this.a.addView(this.c);
                this.l = Build.VERSION.SDK;
                this.a.clearAnimation();
                this.c.clearAnimation();
                if (this.k == 0) {
                    this.j = new AnimationType(this.h);
                } else if (this.k == 1) {
                    this.j = new AnimationType(this.i);
                } else if (this.k == 2) {
                    this.j = new AnimationType(this.h);
                }
                this.j.startAnimation(this.c);
                this.b = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        try {
            this.q = i2;
            if (r == 0) {
                this.b = true;
                this.mHandler.post(this.mUpdateResults);
                return;
            }
            this.w = (AdInfo) AppConnect.i.get(this.q);
            if (this.w != null) {
                this.p = this.w.getAdText();
                Bitmap adIcon = this.w.getAdIcon();
                int initAdWidth = new SDKUtils(this.d).initAdWidth();
                this.c = a(this.d, adIcon, this.p, initAdWidth);
                this.c.setLayoutParams(new ViewGroup.LayoutParams(initAdWidth, (int) (((float) initAdWidth) / 13.333333f)));
                this.mHandler.post(new az(this));
                this.c.setOnClickListener(getMiniAdClickListener(this.d, this.q));
                if (this.c != null) {
                    this.b = true;
                    this.mHandler.post(this.mUpdateResults);
                }
                this.v.putInt(this.x, this.q);
                this.v.commit();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ int c(MiniAdView miniAdView) {
        int i2 = miniAdView.o;
        miniAdView.o = i2 + 1;
        return i2;
    }

    private void c() {
        e = System.currentTimeMillis();
        this.u = this.d.getSharedPreferences("AppSettings", 0);
        int i2 = this.u.getInt(this.x, -1);
        if (i2 != -1) {
            this.o = i2 + 1;
        }
        if (this.o >= r) {
            this.o = 0;
        }
        b(this.o);
        new ay(this).start();
    }

    public void DisplayAd() {
        DisplayAd(f);
    }

    public void DisplayAd(int i2) {
        f = i2;
        if (i2 < 5) {
            f = 5;
        }
        if (this.d.getSharedPreferences("ShowAdFlag", 3).getBoolean("show_mini_flag", true)) {
            c();
        }
    }

    public View.OnClickListener getMiniAdClickListener(Context context, int i2) {
        return new bb(this, context, i2);
    }

    public MiniAdView setAnimationType(int i2) {
        this.i = i2;
        this.k = 1;
        return this;
    }

    public MiniAdView setAnimationType(int[] iArr) {
        this.h = iArr;
        this.k = 2;
        return this;
    }
}
