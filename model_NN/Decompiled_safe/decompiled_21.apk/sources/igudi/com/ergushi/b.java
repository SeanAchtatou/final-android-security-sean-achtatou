package igudi.com.ergushi;

import android.app.Activity;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class b extends WebViewClient {
    final /* synthetic */ AdView a;

    private b(AdView adView) {
        this.a = adView;
    }

    /* synthetic */ b(AdView adView, a aVar) {
        this(adView);
    }

    public void onPageFinished(WebView webView, String str) {
        try {
            if (webView.getTitle() == null || !webView.getTitle().toLowerCase().contains("ad")) {
                this.a.mHandler.post(new d(this));
                super.onPageFinished(webView, str);
            }
            this.a.mHandler.post(new e(this, webView));
            super.onPageFinished(webView, str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (((Activity) this.a.c).isFinishing()) {
            webView.loadUrl("");
        }
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        webView.setScrollBarStyle(0);
        try {
            webView.loadDataWithBaseURL("", "<html></html>", "text/html", "utf-8", "");
            this.a.mHandler.post(new c(this));
            boolean unused = this.a.h = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onReceivedError(webView, i, str, str2);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            if (be.b(str)) {
                return false;
            }
            if (str.contains("_self")) {
                webView.loadUrl(str);
                return true;
            } else if (str.contains("?")) {
                String str2 = "";
                String str3 = "";
                if (str.contains("browser")) {
                    str3 = str.substring(str.indexOf("browser=") + 8);
                    if (str3.contains("&")) {
                        str3 = str3.substring(0, str3.indexOf("&"));
                    }
                }
                if (str.contains("ad_type")) {
                    String substring = str.substring(str.indexOf("ad_type=") + 8);
                    str2 = substring.substring(0, substring.indexOf("&"));
                }
                if (!str2.toLowerCase().equals("cpc")) {
                    this.a.a(str);
                } else if (str3.equals("")) {
                    this.a.a(str);
                } else {
                    new SDKUtils(this.a.c).openUrlByBrowser(str3, str);
                }
                return true;
            } else {
                this.a.a(str);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
