package igudi.com.ergushi;

import android.webkit.DownloadListener;
import android.widget.Toast;

class bc implements DownloadListener {
    final /* synthetic */ OffersWebView a;

    bc(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        if (be.b(this.a.w)) {
            Toast.makeText(this.a, (CharSequence) OffersWebView.F.get("wrong_url"), 1).show();
            this.a.finish();
            return;
        }
        if (!be.b(this.a.y)) {
            this.a.a = new ao(this.a, this.a.w, this.a.x, this.a.y);
        } else {
            this.a.a = new ao(this.a, this.a.w, this.a.x);
        }
        this.a.a(this.a, this.a.a, this.a.w, this.a.x, this.a.y);
        String unused = this.a.y = "";
    }
}
