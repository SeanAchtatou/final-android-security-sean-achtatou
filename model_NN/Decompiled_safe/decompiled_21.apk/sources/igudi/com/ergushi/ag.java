package igudi.com.ergushi;

import com.umeng.common.b.e;
import java.io.UnsupportedEncodingException;

public class ag {
    static final /* synthetic */ boolean a = (!ag.class.desiredAssertionStatus());

    private ag() {
    }

    public static byte[] a(String str, int i) {
        return a(str.getBytes(), i);
    }

    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        ai aiVar = new ai(i3, new byte[((i2 * 3) / 4)]);
        if (!aiVar.a(bArr, i, i2, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (aiVar.b == aiVar.a.length) {
            return aiVar.a;
        } else {
            byte[] bArr2 = new byte[aiVar.b];
            System.arraycopy(aiVar.a, 0, bArr2, 0, aiVar.b);
            return bArr2;
        }
    }

    public static String b(byte[] bArr, int i) {
        try {
            return new String(c(bArr, i), e.b);
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        aj ajVar = new aj(i3, null);
        int i4 = (i2 / 3) * 4;
        if (!ajVar.d) {
            switch (i2 % 3) {
                case 1:
                    i4 += 2;
                    break;
                case 2:
                    i4 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (ajVar.e && i2 > 0) {
            i4 += (ajVar.f ? 2 : 1) * (((i2 - 1) / 57) + 1);
        }
        ajVar.a = new byte[i4];
        ajVar.a(bArr, i, i2, true);
        if (a || ajVar.b == i4) {
            return ajVar.a;
        }
        throw new AssertionError();
    }

    public static byte[] c(byte[] bArr, int i) {
        return b(bArr, 0, bArr.length, i);
    }
}
