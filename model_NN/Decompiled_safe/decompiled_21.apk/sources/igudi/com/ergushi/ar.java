package igudi.com.ergushi;

import android.app.AlertDialog;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Environment;
import android.widget.Toast;
import com.umeng.common.a;
import java.io.File;
import java.net.HttpURLConnection;
import java.util.Map;

public class ar {
    /* access modifiers changed from: private */
    public static Map d;
    /* access modifiers changed from: private */
    public String a = "";
    /* access modifiers changed from: private */
    public File b = null;
    private AlertDialog c = null;
    /* access modifiers changed from: private */
    public String e = "";
    /* access modifiers changed from: private */
    public String f = "";
    /* access modifiers changed from: private */
    public ae g;
    /* access modifiers changed from: private */
    public boolean h = true;
    private Thread i;

    protected static long a(Context context, String str) {
        try {
            HttpURLConnection a2 = new be(context).a(str, null, null);
            a2.connect();
            return (long) a2.getContentLength();
        } catch (Exception e2) {
            return 0;
        }
    }

    protected static String a(String str) {
        try {
            return str.substring(str.lastIndexOf("/") + 1, str.indexOf(".apk") + 4);
        } catch (Exception e2) {
            return "";
        }
    }

    protected static void a(Context context, ae aeVar) {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
            intentFilter.addDataScheme(a.c);
            context.registerReceiver(aeVar, intentFilter);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    protected static void b(Context context) {
        if (new SDKUtils(context).isConnect() && ao.q) {
            String[] strArr = null;
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(Environment.getExternalStorageDirectory().toString() + "/Android/data/cache/downloadCache" + "/" + context.getPackageName());
                if (file.exists() && file.isDirectory()) {
                    strArr = file.list();
                }
            } else {
                strArr = context.getFilesDir().list();
            }
            for (String str : strArr) {
                if (str.startsWith("Down_")) {
                    String substring = str.substring("Down_".length(), str.lastIndexOf("."));
                    new ar().a(context, substring, substring);
                }
            }
            ao.q = false;
        }
    }

    protected static void b(Context context, String str) {
        File fileStreamPath;
        if (!be.b(str)) {
            if (str.endsWith(".apk")) {
                str = str.substring(0, str.lastIndexOf("."));
            }
            if (Environment.getExternalStorageState().equals("mounted")) {
                fileStreamPath = new File((Environment.getExternalStorageDirectory().toString() + "/Android/data/cache/downloadCache" + "/" + context.getPackageName()) + "/" + "Down_" + str + ".txt");
            } else {
                fileStreamPath = context.getFileStreamPath("Down_" + str + ".txt");
            }
            new SDKUtils(context).deleteLocalFiles(fileStreamPath);
        }
    }

    /* access modifiers changed from: protected */
    public ao a(Context context, String str, String str2, String str3) {
        return new ao(context, str, str2, str3);
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        try {
            if (this.i == null) {
                this.i = new au(this, context);
                this.i.start();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a(Context context, ao aoVar, String str, String str2, int i2, String str3) {
        try {
            if (d == null || d.size() == 0) {
                d = AppConnect.e(context);
            }
            if (be.b(str2)) {
                this.e = a(str);
            } else {
                this.e = str2 + ".apk";
            }
            this.f = "/sdcard/download/";
            File file = 0 == 0 ? new File(this.f, this.e + ".tmp") : null;
            if (Environment.getExternalStorageState().equals("mounted")) {
                this.b = new File(this.f, this.e);
                if (this.b == null || i2 == 0 || this.b.length() != ((long) i2)) {
                    if (file.length() >= ((long) i2) || file.length() == 0) {
                        if (ao.i.intValue() == 0) {
                            Toast.makeText(context, (CharSequence) d.get("prepare_to_download"), 0).show();
                        }
                        if (file.exists()) {
                            file.delete();
                        }
                        if (this.b.exists()) {
                            this.b.delete();
                        }
                        aoVar.start();
                        return;
                    }
                    Toast.makeText(context, (CharSequence) d.get("waitting_for_download"), 0).show();
                    if (be.b(c(context, str2))) {
                        file.delete();
                        aoVar.start();
                    } else if (ao.f == null) {
                    } else {
                        if (ao.f.get(this.e) == null || !((Boolean) ao.f.get(this.e)).booleanValue()) {
                            ao.f.put(this.e, false);
                            ao.q = false;
                            Thread.sleep(500);
                            new ao(context, str, str3, this.e, this.e).start();
                        }
                    }
                } else if ((System.currentTimeMillis() - this.b.lastModified()) / 1000 > 172800) {
                    Toast.makeText(context, (CharSequence) d.get("prepare_to_download"), 0).show();
                    this.b.delete();
                    aoVar.start();
                } else {
                    this.c = new AlertDialog.Builder(context).setTitle((CharSequence) d.get("reminder")).setIcon(17301618).setMessage((CharSequence) d.get("app_already_exists")).setPositiveButton((CharSequence) d.get("install"), new at(this, context)).setNeutralButton((CharSequence) d.get("download_again"), new as(this, context, aoVar)).create();
                    this.c.show();
                }
            } else {
                this.b = context.getFileStreamPath(this.e);
                if (this.b != null && this.b.length() == ((long) i2)) {
                    Toast.makeText(context, (CharSequence) d.get("prepare_to_download"), 0).show();
                    this.b.delete();
                    aoVar.start();
                } else if (this.b != null && this.b.length() != 0) {
                    Toast.makeText(context, (CharSequence) d.get("waitting_for_download"), 0).show();
                    if (be.b(c(context, str2))) {
                        this.b.delete();
                        aoVar.start();
                    } else if (ao.f == null) {
                    } else {
                        if (ao.f.get(this.e) == null || !((Boolean) ao.f.get(this.e)).booleanValue()) {
                            this.b.delete();
                            aoVar.start();
                        }
                    }
                } else if (this.b != null && this.b.length() == 0) {
                    Toast.makeText(context, (CharSequence) d.get("prepare_to_download"), 0).show();
                    aoVar.start();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void a(Context context, String str, String str2) {
        new ao(context, "", "", str, str2).start();
    }

    /* access modifiers changed from: protected */
    public void b(Context context, String str, String str2) {
        if (d == null || d.size() == 0) {
            d = AppConnect.e(context);
        }
        new av(this, context, str, str2).execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public String c(Context context, String str) {
        Exception e2;
        String str2;
        try {
            if (str.endsWith(".apk")) {
                str = str.substring(0, str.lastIndexOf("."));
            }
            str2 = new SDKUtils(context).loadStringFromLocal("Down_" + str + ".txt", "/Android/data/cache/downloadCache/" + context.getPackageName());
            try {
                return str2.endsWith("\n") ? str2.replace("\n", "") : str2;
            } catch (Exception e3) {
                e2 = e3;
                e2.printStackTrace();
                return str2;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            str2 = "";
            e2 = exc;
            e2.printStackTrace();
            return str2;
        }
    }
}
