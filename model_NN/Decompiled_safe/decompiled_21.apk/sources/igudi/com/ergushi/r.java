package igudi.com.ergushi;

import android.os.AsyncTask;
import com.umeng.common.b.e;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

class r extends AsyncTask {
    String a;
    final /* synthetic */ AppConnect b;

    private r(AppConnect appConnect) {
        this.b = appConnect;
        this.a = "";
    }

    /* synthetic */ r(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        this.a = AppConnect.s.a(ak.d() + ak.C(), AppConnect.c);
        try {
            if (!be.b(this.a)) {
                this.b.a(new ByteArrayInputStream(this.a.getBytes(e.f)));
                z = true;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Boolean.valueOf(z);
    }
}
