package igudi.com.ergushi;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

class as implements DialogInterface.OnClickListener {
    final /* synthetic */ Context a;
    final /* synthetic */ ao b;
    final /* synthetic */ ar c;

    as(ar arVar, Context context, ao aoVar) {
        this.c = arVar;
        this.a = context;
        this.b = aoVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Toast.makeText(this.a, (CharSequence) ar.d.get("prepare_to_download"), 0).show();
        this.c.b.delete();
        this.b.start();
    }
}
