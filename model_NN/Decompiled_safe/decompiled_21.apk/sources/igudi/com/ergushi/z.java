package igudi.com.ergushi;

import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import java.util.Map;

class z extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private z(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ z(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z;
        String str = AppConnect.c;
        if (!this.a.G.equals("")) {
            str = str + "&" + this.a.G;
        }
        String a2 = AppConnect.s.a(ak.c() + ak.p(), str);
        if (new SDKUtils(this.a.a).isConnect() && (a2 == null || !a2.contains("Success"))) {
            ak.a();
            AppConnect.c = "";
        }
        if (!be.b(a2)) {
            z = this.a.f(a2);
        } else {
            try {
                String string = this.a.a.getSharedPreferences("AppSettings", 0).getString("AttrConfig", "");
                if (!be.b(string)) {
                    Map unused = this.a.e(string);
                }
                z = false;
            } catch (Exception e) {
                e.printStackTrace();
                z = false;
            }
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        try {
            if ("true".equals(this.a.S) && AppConnect.L && !be.b(this.a.K) && this.a.K.compareTo(AppConnect.z) > 0) {
                this.a.h(ak.c() + ak.t() + AppConnect.c);
                boolean unused = AppConnect.L = false;
            }
            if (AppConnect.M && Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(Environment.getExternalStorageDirectory().toString() + "/Android/data/cache" + "/" + "AppPackage.dat");
                if (file.exists()) {
                    file.delete();
                }
            }
            if (be.b(AppConnect.az)) {
                v unused2 = this.a.ay = new v(this.a, null);
                this.a.ay.execute(new Void[0]);
            }
            if (AppConnect.aP) {
                if (AppConnect.aO) {
                    this.a.setCrashReport(AppConnect.aO);
                }
                boolean unused3 = AppConnect.aP = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            boolean unused4 = AppConnect.M = false;
        }
    }
}
