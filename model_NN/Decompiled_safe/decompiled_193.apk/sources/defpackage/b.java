package defpackage;

import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.util.StringTokenizer;

/* renamed from: b  reason: default package */
public final class b extends AsyncTask<String, Void, Void> {
    private c a;
    private d b;
    private String c;

    b(c cVar, d dVar, String str) {
        this.a = cVar;
        this.b = dVar;
        this.c = str;
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.StringBuilder.toString():java.lang.String in method: b.a(java.lang.String[]):java.lang.Void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.StringBuilder.toString():java.lang.String
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    /* renamed from: a */
    public java.lang.Void doInBackground(java.lang.String... r1) {
        /*
            r5 = this;
            r0 = 0
            r4 = 0
            r0 = r6[r0]
            r1 = r0
        L_0x0005:
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = r5.c     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.setRequestProperty(r2, r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r2 = 0
            r0.setInstanceFollowRedirects(r2)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.connect()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            int r2 = r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r3 = 302(0x12e, float:4.23E-43)
            if (r2 != r3) goto L_0x0058
            java.lang.String r1 = "Location"
            java.lang.String r1 = r0.getHeaderField(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            if (r1 != 0) goto L_0x003c
            java.lang.String r0 = "Could not get redirect location from a 302 redirect."
            defpackage.t.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            c r0 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
        L_0x003b:
            return r0
        L_0x003c:
            r5.b(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r5.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            a(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r5.d(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            goto L_0x0005
        L_0x0049:
            r0 = move-exception
            java.lang.String r1 = "Received malformed ad url from javascript."
            defpackage.t.a(r1, r0)
            c r0 = r5.a
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR
            r0.a(r1)
            r0 = r4
            goto L_0x003b
        L_0x0058:
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x00e7
            r5.b(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r5.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            a(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r5.d(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r3.<init>(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
        L_0x007b:
            java.lang.String r3 = r2.readLine()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            if (r3 == 0) goto L_0x0099
            r0.append(r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r3 = "\n"
            r0.append(r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            goto L_0x007b
        L_0x008a:
            r0 = move-exception
            java.lang.String r1 = "IOException connecting to ad url."
            defpackage.t.b(r1, r0)
            c r0 = r5.a
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR
            r0.a(r1)
            r0 = r4
            goto L_0x003b
        L_0x0099:
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r3 = "Response content is: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            defpackage.t.a(r2)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            if (r0 == 0) goto L_0x00bf
            java.lang.String r2 = r0.trim()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            int r2 = r2.length()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            if (r2 > 0) goto L_0x00df
        L_0x00bf:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r1.<init>()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r2 = "Response message is null or zero length: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            defpackage.t.a(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            c r0 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.NO_FILL     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
            goto L_0x003b
        L_0x00df:
            c r2 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r2.a(r0, r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
            goto L_0x003b
        L_0x00e7:
            r0 = 400(0x190, float:5.6E-43)
            if (r2 != r0) goto L_0x00fa
            java.lang.String r0 = "Bad request"
            defpackage.t.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            c r0 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.INVALID_REQUEST     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
            goto L_0x003b
        L_0x00fa:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.<init>()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r1 = "Invalid response code: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            java.lang.String r0 = r0.toString()     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            defpackage.t.c(r0)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            c r0 = r5.a     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            com.google.ads.AdRequest$ErrorCode r1 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0049, IOException -> 0x008a }
            r0 = r4
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b.doInBackground(java.lang.String[]):java.lang.Void");
    }

    private static void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Timeout");
        if (headerField != null) {
            c.a((long) (Float.parseFloat(headerField) * 1000.0f));
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.a(stringTokenizer.nextToken());
            }
        }
    }

    private void c(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField != null) {
            this.b.a(Float.parseFloat(headerField));
            if (!this.b.m()) {
                this.b.b();
            }
        }
    }

    private void d(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField == null) {
            return;
        }
        if (headerField.equals("portrait")) {
            this.b.a(1);
        } else if (headerField.equals("landscape")) {
            this.b.a(0);
        }
    }
}
