package com.x25.app;

import java.io.UnsupportedEncodingException;

public class Ag {
    static final char[] a = "0123456789ABCDEF".toCharArray();
    private static String b;
    private static final char[] c = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-".toCharArray();

    private static byte[] a(String paramString) {
        try {
            return paramString.getBytes("UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    private static final byte[] a(String paramString1, String paramString2) throws UnsupportedEncodingException {
        byte[] arrayOfByte1 = a(paramString2);
        if (arrayOfByte1 == null) {
            throw new UnsupportedEncodingException("key should be null");
        }
        byte[] arrayOfByte2 = paramString1.getBytes("UTF-8");
        byte[] arrayOfByte3 = new byte[arrayOfByte2.length];
        int i = arrayOfByte1.length;
        int j = ((arrayOfByte2.length - 1) / arrayOfByte1.length) + 1;
        int k = 0;
        for (int n = 0; n < j; n++) {
            for (int i1 = 0; i1 < i; i1++) {
                int m = k + i1;
                if (m >= arrayOfByte3.length) {
                    break;
                }
                arrayOfByte3[m] = (byte) (arrayOfByte2[m] ^ arrayOfByte1[i1]);
            }
            k += i;
        }
        return arrayOfByte3;
    }

    static final String a(String paramString, int paramInt) throws UnsupportedEncodingException {
        String str1 = Encrypt.md5(String.valueOf(a()) + paramInt);
        StringBuffer localStringBuffer = new StringBuffer();
        int i = str1.length();
        int j = paramString.length();
        int k = ((paramString.length() - 1) / i) + 1;
        int m = 0;
        for (int i2 = 0; i2 < k; i2++) {
            int n = m + i;
            if (n > j) {
                n = j;
            }
            byte[] arrayOfByte = a(paramString.substring(m, n), str1);
            for (int i3 = 0; i3 < arrayOfByte.length; i3++) {
                localStringBuffer.append(a[(arrayOfByte[i3] >>> 4) & 15]);
                localStringBuffer.append(a[arrayOfByte[i3] & 15]);
            }
            m += i;
        }
        return a(localStringBuffer.toString(), localStringBuffer);
    }

    static String a() {
        if (b != null) {
            return b;
        }
        try {
            if (b == null) {
                b = Encrypt.hash("59587142510671244a4410427c0f1132", "0359204502517f2316154f427a024269");
                b = b.trim();
            }
            if (b == null) {
                return "";
            }
        } catch (Exception e) {
        }
        return b;
    }

    static final String a(String paramString, StringBuffer paramStringBuffer) {
        int n;
        paramStringBuffer.delete(0, paramStringBuffer.length());
        int i = paramString.length();
        int j = 0;
        paramStringBuffer.append(c[i % 3]);
        for (int i1 = 0; i1 < i; i1 += 3) {
            if (i1 + 3 < i) {
                n = i1 + 3;
            } else {
                n = i;
            }
            int k = Integer.parseInt(paramString.substring(i1, n), 16);
            paramStringBuffer.append(c[(((byte) (k < 64 ? 0 : k >>> 6)) + j) % 64]);
            int j2 = j + 1;
            paramStringBuffer.append(c[(k + j2) % 64]);
            j = j2 + 1;
        }
        return paramStringBuffer.toString();
    }
}
