package com.x25.apps.BrainTrainer.Util;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import com.x25.apps.BrainTrainer.R;
import java.util.HashMap;

public class SoundManager {
    public static final int BUTTON_CLICK = 1;
    public static final int BUTTON_NO = 2;
    public static final int BUTTON_OK = 3;
    public static final int BUTTON_START = 4;
    private AudioManager audioManager;
    private Context context;
    private MediaPlayer mediaPlayer;
    private SettingManager settingManager;
    private SoundPool soundPool = new SoundPool(4, 3, 100);
    private HashMap<Integer, Integer> soundPoolMap = new HashMap<>();
    private int streamVolume;

    public SoundManager(Context context2) {
        this.context = context2;
        this.audioManager = (AudioManager) context2.getSystemService("audio");
        this.streamVolume = this.audioManager.getStreamVolume(3);
        this.settingManager = new SettingManager(this.context);
        loadSounds();
    }

    public void loadSounds() {
        this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(this.context, R.raw.click, 1)));
        this.soundPoolMap.put(2, Integer.valueOf(this.soundPool.load(this.context, R.raw.no, 2)));
        this.soundPoolMap.put(3, Integer.valueOf(this.soundPool.load(this.context, R.raw.ok, 3)));
        this.soundPoolMap.put(4, Integer.valueOf(this.soundPool.load(this.context, R.raw.start, 4)));
    }

    public void play(int sound) {
        try {
            if (this.settingManager.isSoundOn()) {
                this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), (float) this.streamVolume, (float) this.streamVolume, 1, 0, 1.0f);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopSound(int sound) {
        try {
            if (this.settingManager.isSoundOn()) {
                this.soundPool.stop(this.soundPoolMap.get(Integer.valueOf(sound)).intValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playBgSound() {
        try {
            if (this.settingManager.isMusicOn()) {
                this.mediaPlayer = MediaPlayer.create(this.context, (int) R.raw.bg);
                this.mediaPlayer.setLooping(true);
                this.mediaPlayer.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopBgSound() {
        try {
            if (this.settingManager.isMusicOn()) {
                this.mediaPlayer.stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cleanup() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.stop();
            this.mediaPlayer.release();
        }
        this.soundPool.release();
        this.soundPool = null;
        this.soundPoolMap.clear();
        this.audioManager.unloadSoundEffects();
    }
}
