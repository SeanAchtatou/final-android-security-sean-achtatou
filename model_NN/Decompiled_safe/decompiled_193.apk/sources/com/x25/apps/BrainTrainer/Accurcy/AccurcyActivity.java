package com.x25.apps.BrainTrainer.Accurcy;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;
import com.mt.airad.AirAD;
import com.waps.AnimationType;
import com.x25.apps.BrainTrainer.Ads;
import com.x25.apps.BrainTrainer.R;
import com.x25.apps.BrainTrainer.Util.Charts;
import com.x25.apps.BrainTrainer.Util.ScoreManager;
import com.x25.apps.BrainTrainer.Util.SoundManager;
import com.x25.apps.BrainTrainer.Util.Utils;

public class AccurcyActivity extends Activity implements View.OnClickListener, View.OnTouchListener, Runnable {
    private Ads ads;
    private Animation animation;
    private int balls = 3;
    private BoardView board;
    private int boardHeight;
    private int boardWidth;
    private TextView btnHowTo;
    private Button btnStart;
    private Cell[] cells;
    private ImageView cooldown;
    private AnimationDrawable cooldownAnimation;
    /* access modifiers changed from: private */
    public int currentGameState = 999;
    private RefreshHandler handler;
    private int maxBalls = 12;
    private int radius = 35;
    private int score = 100;
    private ScoreManager scoreManager;
    private SoundManager soundManager;
    private int speed = 4;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.accurcy);
        this.handler = new RefreshHandler();
        this.animation = AnimationUtils.loadAnimation(this, R.anim.shake);
        this.soundManager = new SoundManager(this);
        this.btnStart = (Button) findViewById(R.id.btnAccurcyStart);
        this.btnStart.setOnClickListener(this);
        this.board = (BoardView) findViewById(R.id.accurcyBoard);
        this.board.setOnTouchListener(this);
        this.btnHowTo = (TextView) findViewById(R.id.textAccurcyHowto);
        this.scoreManager = new ScoreManager(this);
        this.ads = new Ads(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAccurcyStart /*2131296259*/:
                this.btnHowTo.setVisibility(8);
                this.soundManager.play(4);
                startGame();
                return;
            default:
                return;
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0 && this.cells != null) {
            int i = 0;
            while (true) {
                if (i >= this.cells.length) {
                    break;
                } else if (!this.cells[i].isVisible() || !this.cells[i].contains(event.getX(), event.getY())) {
                    i++;
                } else if (isCorrectBall(this.cells[i])) {
                    this.cells[i].setVisible(false);
                    this.soundManager.play(3);
                } else {
                    this.soundManager.play(2);
                    this.score--;
                }
            }
            if (!isBallsInVisible()) {
                nextLevel();
            }
            this.board.setCells(this.cells);
            this.board.invalidate();
        }
        return true;
    }

    private boolean isCorrectBall(Cell cell) {
        for (int i = 0; i < this.cells.length; i++) {
            if (this.cells[i].isVisible() && this.cells[i].getValue() < cell.getValue()) {
                return false;
            }
        }
        return true;
    }

    private boolean isBallsInVisible() {
        for (Cell isVisible : this.cells) {
            if (isVisible.isVisible()) {
                return true;
            }
        }
        return false;
    }

    public void startGame() {
        if (this.currentGameState == 3) {
            ScoreManager score2 = new ScoreManager(this);
            Charts charts = new Charts(this);
            charts.setTitle(getResources().getString(R.string.btnAccuracy));
            charts.setXTitle(getResources().getString(R.string.textChartTimes));
            charts.setYTitle(getResources().getString(R.string.textChartScore));
            charts.setTitles(new String[]{getResources().getString(R.string.btnAccuracy)});
            charts.setValues(new double[][]{score2.getAccurcyScore()});
            startActivity(charts.getIntent());
            finish();
            return;
        }
        this.ads.getAd().getWb();
        this.btnStart.setEnabled(false);
        this.handler.removeCallbacks(this);
        this.currentGameState = 0;
        showCooldown();
        this.handler.sleep(3000);
    }

    public void playGame() {
        closeCooldown();
        this.currentGameState = 1;
        this.cells = genCells();
        this.board.setCells(this.cells);
        this.board.invalidate();
        this.handler.sleep(300);
    }

    public void nextLevel() {
        this.ads.clear();
        this.btnStart.setText((int) R.string.btnNextLevel);
        this.btnStart.setEnabled(true);
        this.handler.postDelayed(this, 1000);
        this.balls++;
        if (this.balls > this.maxBalls) {
            finishGame();
        }
    }

    public void moveBalls() {
        this.boardWidth = this.board.getWidth();
        this.boardHeight = this.board.getHeight();
        for (int i = 0; i < this.cells.length; i++) {
            if (this.cells[i].isVisible()) {
                this.cells[i].move(this.boardWidth, this.boardHeight);
            }
        }
        this.board.setCells(this.cells);
        this.board.invalidate();
        this.handler.sleep(300);
    }

    public void finishGame() {
        this.ads.clear();
        this.currentGameState = 3;
        this.btnStart.setEnabled(true);
        this.btnStart.setText((int) R.string.btnSummary);
        this.scoreManager.addAccurcyScore(this.score);
        this.handler.postDelayed(this, 1000);
    }

    public void showCooldown() {
        this.board.removeAllViews();
        this.cooldown = new ImageView(this);
        this.cooldown.setImageResource(R.drawable.cooldown);
        this.boardWidth = this.board.getWidth();
        this.boardHeight = this.board.getHeight();
        int cooldownSize = Utils.dipToPx(this, 200);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(cooldownSize, cooldownSize);
        layoutParams.setMargins((this.boardWidth - cooldownSize) / 2, (this.boardHeight - cooldownSize) / 2, 0, 0);
        this.cooldown.setLayoutParams(layoutParams);
        this.board.addView(this.cooldown);
        this.cooldownAnimation = (AnimationDrawable) this.cooldown.getDrawable();
        this.cooldownAnimation.start();
    }

    public void closeCooldown() {
        this.cooldown.setVisibility(8);
        this.cooldownAnimation.stop();
        this.board.removeView(this.cooldown);
    }

    private Cell[] genCells() {
        Cell[] cells2 = new Cell[this.balls];
        this.boardWidth = this.board.getWidth();
        this.boardHeight = this.board.getHeight();
        int i = 0;
        while (i < this.balls) {
            int x = Utils.getRandom(0, this.boardWidth - (this.radius * 2));
            int y = Utils.getRandom(0, this.boardHeight - (this.radius * 2));
            Cell cell = new Cell(this.radius + x, this.radius + y, this.radius, Utils.getRandom(0, this.speed) + 1);
            cell.setValue(Utils.getRandom(-99, 99));
            boolean isAdd = true;
            if (i > 0) {
                int k = 0;
                while (true) {
                    if (k >= i) {
                        break;
                    } else if (cell.getValue() == cells2[k].getValue() || ((cells2[k].x - x) * (cells2[k].x - x)) + ((cells2[k].y - y) * (cells2[k].y - y)) < this.radius * this.radius * 4) {
                        isAdd = false;
                    } else {
                        k++;
                    }
                }
                isAdd = false;
            }
            if (isAdd) {
                cell.color = Math.abs(cell.getValue()) % 9;
                this.board.installBitmap(cell.color, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), getResourceByIndex(cell.color)), cell.radius * 2, cell.radius * 2, true));
                cells2[i] = cell;
                i++;
            }
        }
        return cells2;
    }

    public static int getResourceByIndex(int index) {
        switch (index) {
            case 0:
                return R.drawable.blue;
            case 1:
                return R.drawable.gay;
            case 2:
                return R.drawable.gray;
            case 3:
                return R.drawable.green;
            case 4:
                return R.drawable.orange;
            case AnimationType.ALPHA:
                return R.drawable.purple;
            case AnimationType.TRANSLATE_FROM_RIGHT:
                return R.drawable.red;
            case AnimationType.TRANSLATE_FROM_LEFT:
                return R.drawable.violet;
            case AirAD.ANIMATION_FIXED:
                return R.drawable.yellow;
            default:
                return R.drawable.green;
        }
    }

    public void run() {
        this.btnStart.startAnimation(this.animation);
        this.handler.postDelayed(this, 5000);
    }

    public void onPause() {
        super.onPause();
        this.handler.removeCallbacks(this);
        this.soundManager.stopBgSound();
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        this.soundManager.playBgSound();
        if (this.btnStart.isEnabled()) {
            this.handler.removeCallbacks(this);
            this.handler.post(this);
        }
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }

    class RefreshHandler extends Handler {
        public RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            if (AccurcyActivity.this.currentGameState == 0) {
                AccurcyActivity.this.playGame();
            } else if (AccurcyActivity.this.currentGameState == 1) {
                AccurcyActivity.this.moveBalls();
            }
        }

        public void sleep(long paramLong) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), paramLong);
        }
    }
}
