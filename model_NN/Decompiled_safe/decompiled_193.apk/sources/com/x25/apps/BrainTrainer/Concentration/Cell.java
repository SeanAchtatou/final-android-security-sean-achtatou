package com.x25.apps.BrainTrainer.Concentration;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.mt.airad.AirAD;
import com.waps.AnimationType;
import com.x25.apps.BrainTrainer.R;

public class Cell extends ImageView {
    private int value;

    public Cell(Context context, int val) {
        super(context);
        setValue(val);
        setImageResource(getResId());
    }

    public void setValue(int val) {
        this.value = val;
    }

    public int getValue() {
        return this.value;
    }

    public void remove() {
        setVisibility(4);
    }

    public void setParams(int x, int y, int size) {
        LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(size, size);
        localLayoutParams.setMargins(x, y, 0, 0);
        setLayoutParams(localLayoutParams);
    }

    private int getResId() {
        switch (this.value) {
            case 1:
                return R.drawable.blue;
            case 2:
                return R.drawable.blue_o;
            case 3:
                return R.drawable.gay;
            case 4:
                return R.drawable.gay_o;
            case AnimationType.ALPHA:
                return R.drawable.gray;
            case AnimationType.TRANSLATE_FROM_RIGHT:
                return R.drawable.gray_o;
            case AnimationType.TRANSLATE_FROM_LEFT:
                return R.drawable.green;
            case AirAD.ANIMATION_FIXED:
                return R.drawable.green_o;
            case 9:
                return R.drawable.orange;
            case 10:
                return R.drawable.orange_o;
            case AirAD.POSITION_BOTTOM:
                return R.drawable.purple;
            case AirAD.POSITION_TOP:
                return R.drawable.purple_o;
            case 13:
                return R.drawable.red;
            case 14:
                return R.drawable.red_o;
            case 15:
                return R.drawable.violet;
            case 16:
                return R.drawable.violet_o;
            case 17:
                return R.drawable.yellow;
            case 18:
                return R.drawable.yellow_o;
            default:
                return 0;
        }
    }
}
