package com.x25.apps.BrainTrainer.Util;

import android.content.Context;

public class SettingManager {
    private StoreManager storeManager;

    public SettingManager(Context context) {
        this.storeManager = new StoreManager(context);
    }

    public void musicOn() {
        this.storeManager.set("music", 0);
    }

    public void musicOff() {
        this.storeManager.set("music", 1);
    }

    public boolean isMusicOn() {
        if (this.storeManager.getInt("music") == 1) {
            return false;
        }
        return true;
    }

    public void soundOn() {
        this.storeManager.set("sound", 0);
    }

    public void soundOff() {
        this.storeManager.set("sound", 1);
    }

    public boolean isSoundOn() {
        if (this.storeManager.getInt("sound") == 1) {
            return false;
        }
        return true;
    }
}
