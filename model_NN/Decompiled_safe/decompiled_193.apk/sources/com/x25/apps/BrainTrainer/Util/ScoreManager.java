package com.x25.apps.BrainTrainer.Util;

import android.content.Context;

public class ScoreManager {
    private static final String ACCURCY = "ACCURCY";
    private static final String CONCENTRATION = "CONCENTRATION";
    private static final String MEMORY = "MEMORY";
    private static final String SPEED = "SPEED";
    private StoreManager store;

    public ScoreManager(Context context) {
        this.store = new StoreManager(context);
    }

    public void addAccurcyScore(int score) {
        AddScore(ACCURCY, score);
    }

    public void addMemoryScore(int score) {
        AddScore(MEMORY, score);
    }

    public void addSpeedScore(int score) {
        AddScore(SPEED, score);
    }

    public void addConcentrationScore(int score) {
        AddScore(CONCENTRATION, score);
    }

    public double[] getAccurcyScore() {
        return getScore(ACCURCY);
    }

    public double[] getMemoryScore() {
        return getScore(MEMORY);
    }

    public double[] getSpeedScore() {
        return getScore(SPEED);
    }

    public double[] getConcentrationScore() {
        return getScore(CONCENTRATION);
    }

    private void AddScore(String tag, int score) {
        String strScore;
        String strScore2 = this.store.get(tag);
        if (strScore2.length() > 0) {
            strScore = String.valueOf(strScore2) + ":" + score;
        } else {
            strScore = new StringBuilder(String.valueOf(score)).toString();
        }
        this.store.set(tag, strScore);
    }

    private double[] getScore(String tag) {
        String strScore = this.store.get(tag);
        if (strScore.length() <= 0) {
            return new double[0];
        }
        double[] scores = new double[strScore.length()];
        String[] scoreList = strScore.split(":");
        if (scoreList != null) {
            for (int i = 0; i < scoreList.length; i++) {
                scores[i] = Double.valueOf(Double.parseDouble(scoreList[i])).doubleValue();
            }
        }
        return scores;
    }
}
