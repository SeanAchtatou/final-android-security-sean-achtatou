package com.x25.apps.BrainTrainer.Util;

import android.content.Context;
import java.util.Random;

public class Utils {
    public static int dipToPx(Context context, int dip) {
        return (int) ((((float) dip) * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int getRandom(int min, int max) {
        int max2 = max + 1;
        if (min <= max2) {
            return new Random().nextInt(max2 - min) + min;
        }
        try {
            return new Random().nextInt(min - max2) + max2;
        } catch (Exception e) {
            e.printStackTrace();
            return 0 + min;
        }
    }
}
