package com.x25.apps.BrainTrainer;

public final class R {

    public static final class anim {
        public static final int cycle = 2130968576;
        public static final int shake = 2130968577;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int bkg = 2131099658;
        public static final int color_black = 2131099656;
        public static final int color_brown = 2131099655;
        public static final int color_chart_plot = 2131099659;
        public static final int color_green = 2131099653;
        public static final int color_orange = 2131099652;
        public static final int color_red = 2131099654;
        public static final int color_white = 2131099657;
        public static final int color_yellow = 2131099651;
        public static final int dialog_button_text_color = 2131099648;
        public static final int label_text_color = 2131099649;
        public static final int values_text_color = 2131099650;
    }

    public static final class drawable {
        public static final int blue = 2130837504;
        public static final int blue_block = 2130837505;
        public static final int blue_o = 2130837506;
        public static final int btn_black = 2130837507;
        public static final int btn_custom = 2130837508;
        public static final int btn_orange = 2130837509;
        public static final int btn_red = 2130837510;
        public static final int cell_1 = 2130837511;
        public static final int cell_10 = 2130837512;
        public static final int cell_2 = 2130837513;
        public static final int cell_3 = 2130837514;
        public static final int cell_4 = 2130837515;
        public static final int cell_5 = 2130837516;
        public static final int cell_6 = 2130837517;
        public static final int cell_7 = 2130837518;
        public static final int cell_8 = 2130837519;
        public static final int cell_9 = 2130837520;
        public static final int cell_bg = 2130837521;
        public static final int cell_mask = 2130837522;
        public static final int cell_unmask_bg = 2130837523;
        public static final int cooldown = 2130837524;
        public static final int cooldown_1 = 2130837525;
        public static final int cooldown_2 = 2130837526;
        public static final int cooldown_3 = 2130837527;
        public static final int exit = 2130837528;
        public static final int gay = 2130837529;
        public static final int gay_o = 2130837530;
        public static final int gray = 2130837531;
        public static final int gray_o = 2130837532;
        public static final int green = 2130837533;
        public static final int green_block = 2130837534;
        public static final int green_o = 2130837535;
        public static final int icon = 2130837536;
        public static final int main_bg = 2130837537;
        public static final int more = 2130837538;
        public static final int orange = 2130837539;
        public static final int orange_o = 2130837540;
        public static final int purple = 2130837541;
        public static final int purple_o = 2130837542;
        public static final int red = 2130837543;
        public static final int red_o = 2130837544;
        public static final int setting = 2130837545;
        public static final int violet = 2130837546;
        public static final int violet_o = 2130837547;
        public static final int yellow = 2130837548;
        public static final int yellow_o = 2130837549;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131296279;
        public static final int OfferProgressBar = 2131296282;
        public static final int RelativeLayout01 = 2131296280;
        public static final int accurcyBoard = 2131296256;
        public static final int accurcyBtnBoard = 2131296258;
        public static final int app = 2131296293;
        public static final int appIcon = 2131296294;
        public static final int board = 2131296276;
        public static final int btnAccuracy = 2131296268;
        public static final int btnAccurcyStart = 2131296259;
        public static final int btnConcentration = 2131296271;
        public static final int btnConcentrationStart = 2131296261;
        public static final int btnLeft = 2131296266;
        public static final int btnMemory = 2131296269;
        public static final int btnMore = 2131296275;
        public static final int btnProgress = 2131296272;
        public static final int btnRight = 2131296267;
        public static final int btnSetting = 2131296273;
        public static final int btnShare = 2131296274;
        public static final int btnSpeed = 2131296270;
        public static final int btnSpeedStart = 2131296292;
        public static final int btnStart = 2131296278;
        public static final int concentrationBoard = 2131296265;
        public static final int description = 2131296298;
        public static final int notification = 2131296296;
        public static final int offersWebView = 2131296281;
        public static final int proTV = 2131296283;
        public static final int progress_bar = 2131296299;
        public static final int progress_text = 2131296295;
        public static final int speedBoard = 2131296289;
        public static final int speedBtnBoard = 2131296291;
        public static final int speedInfo = 2131296286;
        public static final int speedScore = 2131296287;
        public static final int speedTime = 2131296288;
        public static final int targetCell = 2131296262;
        public static final int textAccurcyHowto = 2131296257;
        public static final int textConcentrationHowto = 2131296260;
        public static final int textMemoryHowto = 2131296277;
        public static final int textSpeedHowto = 2131296290;
        public static final int timeProgress = 2131296263;
        public static final int title = 2131296297;
        public static final int toggleMusic = 2131296284;
        public static final int toggleSound = 2131296285;
        public static final int txtProgress = 2131296264;
    }

    public static final class layout {
        public static final int accurcy = 2130903040;
        public static final int concentration = 2130903041;
        public static final int concentration_board = 2130903042;
        public static final int main = 2130903043;
        public static final int memory = 2130903044;
        public static final int offers_web_view = 2130903045;
        public static final int setting = 2130903046;
        public static final int speed = 2130903047;
        public static final int umeng_download_notification = 2130903048;
    }

    public static final class raw {
        public static final int bg = 2131034112;
        public static final int click = 2131034113;
        public static final int no = 2131034114;
        public static final int ok = 2131034115;
        public static final int start = 2131034116;
    }

    public static final class string {
        public static final int app_des = 2131165186;
        public static final int app_name = 2131165185;
        public static final int btnAccuracy = 2131165187;
        public static final int btnAccurcyStart = 2131165203;
        public static final int btnConcentration = 2131165190;
        public static final int btnConcentrationStart = 2131165205;
        public static final int btnMemory = 2131165188;
        public static final int btnMore = 2131165193;
        public static final int btnNextLevel = 2131165195;
        public static final int btnProgress = 2131165192;
        public static final int btnReplay = 2131165196;
        public static final int btnShare = 2131165191;
        public static final int btnSpeed = 2131165189;
        public static final int btnSpeedStart = 2131165199;
        public static final int btnStart = 2131165194;
        public static final int btnSummary = 2131165197;
        public static final int cancel = 2131165216;
        public static final int exit = 2131165213;
        public static final int exit_msg = 2131165214;
        public static final int lang = 2131165184;
        public static final int more = 2131165212;
        public static final int ok = 2131165215;
        public static final int setting = 2131165209;
        public static final int textAccurcyHowto = 2131165204;
        public static final int textChartScore = 2131165207;
        public static final int textChartTimes = 2131165208;
        public static final int textConcentrationHowto = 2131165206;
        public static final int textMemoryHowto = 2131165198;
        public static final int textMusic = 2131165210;
        public static final int textScore = 2131165201;
        public static final int textSound = 2131165211;
        public static final int textSpeedHowto = 2131165200;
        public static final int textTime = 2131165202;
    }

    public static final class style {
        public static final int Button = 2131230723;
        public static final int PontiflexLandscapeMargin = 2131230720;
        public static final int PontiflexLandscapePadding = 2131230721;
        public static final int app_theme = 2131230722;
    }
}
