package com.x25.apps.BrainTrainer.Speed;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;
import com.x25.apps.BrainTrainer.Ads;
import com.x25.apps.BrainTrainer.R;
import com.x25.apps.BrainTrainer.Util.Charts;
import com.x25.apps.BrainTrainer.Util.ScoreManager;
import com.x25.apps.BrainTrainer.Util.SoundManager;
import com.x25.apps.BrainTrainer.Util.Utils;
import java.util.Arrays;
import java.util.Collections;

public class SpeedActivity extends Activity implements View.OnClickListener, View.OnTouchListener, Runnable {
    private Ads ads;
    private Animation animation;
    private LinearLayout board;
    private int boardHeight;
    private int boardWidth;
    private TextView btnHowTo;
    private Button btnStart;
    private Cell[] cells;
    private int col = 4;
    private ImageView cooldown;
    private AnimationDrawable cooldownAnimation;
    private Cell curCell;
    /* access modifiers changed from: private */
    public int currentGameState = 999;
    private RefreshHandler handler;
    private boolean isCanTouch = false;
    private int row = 6;
    private int score = 0;
    private ScoreManager scoreManager;
    private SoundManager soundManager;
    private LinearLayout speedBtnBoard;
    private LinearLayout speedInfo;
    private TextView speedScore;
    private TextView speedTime;
    /* access modifiers changed from: private */
    public int time = 60;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.speed);
        this.handler = new RefreshHandler();
        this.animation = AnimationUtils.loadAnimation(this, R.anim.shake);
        this.soundManager = new SoundManager(this);
        this.btnStart = (Button) findViewById(R.id.btnSpeedStart);
        this.btnStart.setOnClickListener(this);
        this.board = (LinearLayout) findViewById(R.id.speedBoard);
        this.board.setOnTouchListener(this);
        this.speedInfo = (LinearLayout) findViewById(R.id.speedInfo);
        this.speedBtnBoard = (LinearLayout) findViewById(R.id.speedBtnBoard);
        this.btnHowTo = (TextView) findViewById(R.id.textSpeedHowto);
        this.speedScore = (TextView) findViewById(R.id.speedScore);
        this.speedTime = (TextView) findViewById(R.id.speedTime);
        this.scoreManager = new ScoreManager(this);
        this.ads = new Ads(this);
    }

    private void setScoreShow() {
        this.speedScore.setText(String.valueOf(getResources().getString(R.string.textScore)) + " " + this.score);
    }

    private void setTimeShow() {
        this.speedTime.setText(String.valueOf(getResources().getString(R.string.textTime)) + " " + this.time);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSpeedStart:
                this.btnHowTo.setVisibility(8);
                this.soundManager.play(4);
                startGame();
                return;
            default:
                return;
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0 && (v instanceof Cell)) {
            if (!this.isCanTouch) {
                return true;
            }
            if (v == this.curCell) {
                this.soundManager.play(3);
                this.score++;
                this.isCanTouch = false;
            } else {
                this.soundManager.play(2);
            }
        }
        return true;
    }

    public void startGame() {
        if (this.currentGameState == 3) {
            ScoreManager score2 = new ScoreManager(this);
            Charts charts = new Charts(this);
            charts.setTitle(getResources().getString(R.string.btnSpeed));
            charts.setXTitle(getResources().getString(R.string.textChartTimes));
            charts.setYTitle(getResources().getString(R.string.textChartScore));
            charts.setTitles(new String[]{getResources().getString(R.string.btnSpeed)});
            charts.setValues(new double[][]{score2.getSpeedScore()});
            startActivity(charts.getIntent());
            finish();
            return;
        }
        this.ads.getAd().getWb();
        this.btnStart.setEnabled(false);
        this.handler.removeCallbacks(this);
        this.currentGameState = 0;
        showCooldown();
        this.handler.sleep(3000);
    }

    public void initGame() {
        int x;
        int y;
        closeCooldown();
        this.speedBtnBoard.setVisibility(8);
        this.speedInfo.setVisibility(0);
        this.currentGameState = 1;
        this.cells = genCells(this.col * this.row);
        this.board.removeAllViews();
        this.boardWidth = this.board.getWidth();
        this.boardHeight = this.board.getHeight();
        int width = this.boardWidth / this.col;
        int height = (((this.boardHeight - Utils.dipToPx(this, 48)) / this.row) - this.row) + 2;
        int i = 0;
        for (Cell cell : this.cells) {
            int k = i % this.col;
            if (k == 0) {
                x = 0;
                y = 0;
            } else {
                x = k * width;
                y = (height * -1) + 1;
            }
            cell.setParams(x, y, width, height);
            cell.setOnTouchListener(this);
            this.board.addView(cell);
            i++;
        }
        this.handler.sleep(1000);
    }

    public void playGame() {
        setScoreShow();
        setTimeShow();
        this.isCanTouch = true;
        if (this.curCell != null) {
            this.curCell.reset();
        }
        this.curCell = this.cells[Utils.getRandom(0, this.cells.length - 1)];
        this.curCell.highLight();
        this.handler.sleep(650);
    }

    public void timeCooldown() {
        if (this.time <= 0) {
            finishGame();
        }
        this.time--;
        this.handler.sendEmptyMessageDelayed(1, 1000);
    }

    public void finishGame() {
        this.ads.clear();
        this.isCanTouch = false;
        this.currentGameState = 3;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.speedBtnBoard.getMeasuredWidth(), this.speedBtnBoard.getMeasuredHeight());
        layoutParams.setMargins(0, -this.speedBtnBoard.getMeasuredHeight(), 0, 0);
        this.speedBtnBoard.setLayoutParams(layoutParams);
        this.speedBtnBoard.setVisibility(0);
        this.btnStart.setEnabled(true);
        this.btnStart.setText((int) R.string.btnSummary);
        this.scoreManager.addSpeedScore(this.score);
        this.handler.postDelayed(this, 1000);
    }

    public void showCooldown() {
        this.board.removeAllViews();
        this.cooldown = new ImageView(this);
        this.cooldown.setImageResource(R.drawable.cooldown);
        this.boardWidth = this.board.getWidth();
        this.boardHeight = this.board.getHeight();
        int cooldownSize = Utils.dipToPx(this, 200);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(cooldownSize, cooldownSize);
        layoutParams.setMargins((this.boardWidth - cooldownSize) / 2, (this.boardHeight - cooldownSize) / 2, 0, 0);
        this.cooldown.setLayoutParams(layoutParams);
        this.board.addView(this.cooldown);
        this.cooldownAnimation = (AnimationDrawable) this.cooldown.getDrawable();
        this.cooldownAnimation.start();
    }

    public void closeCooldown() {
        this.cooldown.setVisibility(8);
        this.cooldownAnimation.stop();
        this.board.removeView(this.cooldown);
    }

    private Cell[] genCells(int num) {
        Cell[] cells2 = new Cell[num];
        for (int i = 1; i <= num; i++) {
            cells2[i - 1] = new Cell(this);
        }
        Collections.shuffle(Arrays.asList(cells2));
        return cells2;
    }

    public void run() {
        this.btnStart.startAnimation(this.animation);
        this.handler.postDelayed(this, 5000);
    }

    public void onPause() {
        super.onPause();
        this.handler.removeCallbacks(this);
        this.soundManager.stopBgSound();
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        this.soundManager.playBgSound();
        if (this.btnStart.isEnabled()) {
            this.handler.removeCallbacks(this);
            this.handler.post(this);
        }
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }

    class RefreshHandler extends Handler {
        public RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            if (SpeedActivity.this.currentGameState == 0) {
                SpeedActivity.this.initGame();
            } else if (SpeedActivity.this.currentGameState == 1) {
                if (msg.what != 1) {
                    SpeedActivity.this.playGame();
                }
                if (SpeedActivity.this.time == 60 || msg.what == 1) {
                    SpeedActivity.this.timeCooldown();
                }
            }
        }

        public void sleep(long paramLong) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), paramLong);
        }
    }
}
