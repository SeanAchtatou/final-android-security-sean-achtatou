package com.x25.apps.BrainTrainer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ToggleButton;
import com.mobclick.android.MobclickAgent;
import com.x25.apps.BrainTrainer.Util.SettingManager;
import com.x25.apps.BrainTrainer.Util.SoundManager;

public class SettingActivity extends Activity implements View.OnClickListener {
    private Ads ads;
    private SettingManager settingManager;
    private SoundManager soundManager;
    private ToggleButton toggleMusic;
    private ToggleButton toggleSound;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.setting);
        this.settingManager = new SettingManager(this);
        this.soundManager = new SoundManager(this);
        this.toggleMusic = (ToggleButton) findViewById(R.id.toggleMusic);
        this.toggleMusic.setOnClickListener(this);
        if (this.settingManager.isMusicOn()) {
            this.toggleMusic.setChecked(true);
        }
        this.toggleSound = (ToggleButton) findViewById(R.id.toggleSound);
        this.toggleSound.setOnClickListener(this);
        if (this.settingManager.isSoundOn()) {
            this.toggleSound.setChecked(true);
        }
        MobclickAgent.update(this);
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toggleMusic:
                this.soundManager.play(1);
                if (this.toggleMusic.isChecked()) {
                    this.settingManager.musicOn();
                    this.soundManager.playBgSound();
                    return;
                }
                this.soundManager.stopBgSound();
                this.settingManager.musicOff();
                return;
            case R.id.toggleSound:
                if (this.toggleSound.isChecked()) {
                    this.settingManager.soundOn();
                    this.soundManager.play(1);
                    return;
                }
                this.settingManager.soundOff();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.soundManager.stopBgSound();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.soundManager.playBgSound();
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }
}
