package com.x25.apps.BrainTrainer.Speed;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.x25.apps.BrainTrainer.R;

public class Cell extends ImageView {
    public Cell(Context context) {
        super(context);
        setImageResource(R.drawable.blue_block);
    }

    public void setParams(int x, int y, int width, int height) {
        LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(width, height);
        localLayoutParams.setMargins(x, y, 0, 0);
        setLayoutParams(localLayoutParams);
    }

    public void highLight() {
        setImageResource(R.drawable.green_block);
        invalidate();
    }

    public void reset() {
        setImageResource(R.drawable.blue_block);
        invalidate();
    }
}
