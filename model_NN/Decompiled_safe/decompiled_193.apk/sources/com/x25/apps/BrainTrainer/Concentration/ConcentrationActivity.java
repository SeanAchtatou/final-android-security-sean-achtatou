package com.x25.apps.BrainTrainer.Concentration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import com.mobclick.android.MobclickAgent;
import com.x25.apps.BrainTrainer.R;
import com.x25.apps.BrainTrainer.Util.SoundManager;

public class ConcentrationActivity extends Activity implements View.OnClickListener, Runnable {
    private Animation animation;
    private Button btnStart;
    private Handler handler;
    private SoundManager soundManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.concentration);
        this.handler = new Handler();
        this.animation = AnimationUtils.loadAnimation(this, R.anim.shake);
        this.soundManager = new SoundManager(this);
        this.btnStart = (Button) findViewById(R.id.btnConcentrationStart);
        this.btnStart.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnConcentrationStart /*2131296261*/:
                this.soundManager.play(4);
                startActivity(new Intent(this, GameActivity.class));
                finish();
                return;
            default:
                return;
        }
    }

    public void run() {
        this.btnStart.startAnimation(this.animation);
        this.handler.postDelayed(this, 5000);
    }

    public void onPause() {
        super.onPause();
        this.handler.removeCallbacks(this);
        this.soundManager.stopBgSound();
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        this.soundManager.playBgSound();
        if (this.btnStart.isEnabled()) {
            this.handler.removeCallbacks(this);
            this.handler.post(this);
        }
        MobclickAgent.onResume(this);
    }
}
