package com.x25.apps.BrainTrainer.Concentration;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;
import com.mt.airad.AirAD;
import com.waps.AnimationType;
import com.x25.apps.BrainTrainer.Ads;
import com.x25.apps.BrainTrainer.R;
import com.x25.apps.BrainTrainer.Util.Charts;
import com.x25.apps.BrainTrainer.Util.ScoreManager;
import com.x25.apps.BrainTrainer.Util.SoundManager;
import com.x25.apps.BrainTrainer.Util.Utils;
import java.util.Arrays;
import java.util.Collections;

public class GameActivity extends Activity implements View.OnClickListener, View.OnTouchListener {
    private Ads ads;
    private LinearLayout board;
    private int boardHeight;
    private int boardWidth;
    private Button btnLeft;
    private Button btnRight;
    private CellVal[] cellVals;
    private Cell[] cells;
    private int col = 6;
    private int curVal;
    /* access modifiers changed from: private */
    public int currentGameState = 999;
    private RefreshHandler handler;
    private boolean isCanTouch = false;
    private int oldVal;
    private int progress;
    private int row = 8;
    private int score = 0;
    private ScoreManager scoreManager;
    private SoundManager soundManager;
    private ImageView targetCell;
    private ProgressBar timeProgress;
    private TextView txtProgress;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.concentration_board);
        this.handler = new RefreshHandler();
        this.soundManager = new SoundManager(this);
        this.board = (LinearLayout) findViewById(R.id.concentrationBoard);
        this.board.setOnTouchListener(this);
        this.timeProgress = (ProgressBar) findViewById(R.id.timeProgress);
        this.txtProgress = (TextView) findViewById(R.id.txtProgress);
        this.targetCell = (ImageView) findViewById(R.id.targetCell);
        this.btnLeft = (Button) findViewById(R.id.btnLeft);
        this.btnRight = (Button) findViewById(R.id.btnRight);
        this.btnLeft.setOnClickListener(this);
        this.btnRight.setOnClickListener(this);
        this.scoreManager = new ScoreManager(this);
        this.ads = new Ads(this);
        startGame();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLeft /*2131296266*/:
                this.soundManager.play(4);
                startGame();
                return;
            case R.id.btnRight /*2131296267*/:
                this.soundManager.play(4);
                this.scoreManager.addConcentrationScore(Math.min(100, this.score));
                ScoreManager score2 = new ScoreManager(this);
                Charts charts = new Charts(this);
                charts.setTitle(getResources().getString(R.string.btnConcentration));
                charts.setXTitle(getResources().getString(R.string.textChartTimes));
                charts.setYTitle(getResources().getString(R.string.textChartScore));
                charts.setTitles(new String[]{getResources().getString(R.string.btnConcentration)});
                charts.setValues(new double[][]{score2.getConcentrationScore()});
                startActivity(charts.getIntent());
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0 && (v instanceof Cell)) {
            int value = ((Cell) v).getValue();
            if (!this.isCanTouch || value != this.cellVals[this.curVal].value + 1) {
                this.soundManager.play(2);
                return true;
            }
            this.soundManager.play(3);
            this.cellVals[this.curVal].num--;
            this.score += 2;
            ((Cell) v).remove();
        }
        return true;
    }

    public void startGame() {
        this.progress = 5;
        this.timeProgress.setMax(this.progress);
        this.currentGameState = 0;
        this.curVal = 0;
        this.btnLeft.setVisibility(8);
        this.btnRight.setVisibility(8);
        this.ads.getAd().getWb();
        this.handler.sleep(300);
    }

    public void playGame() {
        int size;
        int i;
        int x;
        int y;
        this.currentGameState = 1;
        this.cells = genCells(this.row * this.col);
        this.board.removeAllViews();
        this.boardWidth = this.board.getWidth();
        this.boardHeight = this.board.getHeight();
        int width = this.boardWidth / this.col;
        int height = this.boardHeight / this.row;
        int i2 = 0;
        if (height > width) {
            size = width;
        } else {
            size = height;
        }
        for (Cell cell : this.cells) {
            int k = i2 % this.col;
            if (k == 0) {
                x = 0;
                y = 0;
            } else {
                if (size > width) {
                    i = size;
                } else {
                    i = width;
                }
                x = k * i;
                y = size * -1;
            }
            cell.setParams(x, y, size);
            cell.setOnTouchListener(this);
            this.board.addView(cell);
            i2++;
        }
        this.board.invalidate();
        this.isCanTouch = true;
        this.handler.sleep(0);
    }

    public void onPlayGame() {
        this.oldVal = this.curVal;
        if (setCurVal()) {
            this.score = 100;
            finishGame();
            this.ads.clear();
            this.btnRight.setVisibility(0);
        }
        if (this.oldVal != this.curVal) {
            this.progress = 5;
        }
        this.timeProgress.setProgress(this.progress);
        this.txtProgress.setText(new StringBuilder(String.valueOf(this.progress)).toString());
        if (this.progress <= 0) {
            finishGame();
            this.ads.clear();
            this.btnLeft.setVisibility(0);
            this.btnRight.setVisibility(0);
            return;
        }
        this.progress--;
        this.handler.sleep(1000);
    }

    public void finishGame() {
        this.isCanTouch = false;
        this.currentGameState = 3;
    }

    public boolean setCurVal() {
        if (this.curVal == 17 && this.cellVals[this.curVal].num == 0) {
            return true;
        }
        if (this.cellVals[this.curVal].num == 0) {
            int i = this.curVal;
            while (i < 18) {
                this.curVal = i;
                if (this.cellVals[this.curVal].num > 0) {
                    break;
                } else if (this.curVal == 17 && this.cellVals[this.curVal].num == 0) {
                    return true;
                } else {
                    i++;
                }
            }
        }
        this.targetCell.setImageResource(getResId(this.cellVals[this.curVal].value + 1));
        this.targetCell.setVisibility(0);
        return false;
    }

    private Cell[] genCells(int num) {
        this.cellVals = new CellVal[18];
        for (int k = 0; k < 18; k++) {
            this.cellVals[k] = new CellVal(k, 0);
        }
        Cell[] cells2 = new Cell[num];
        for (int i = 1; i <= num; i++) {
            int val = Utils.getRandom(1, 18);
            cells2[i - 1] = new Cell(this, val);
            this.cellVals[val - 1].num++;
        }
        Collections.shuffle(Arrays.asList(cells2));
        Collections.shuffle(Arrays.asList(this.cellVals));
        return cells2;
    }

    private int getResId(int value) {
        switch (value) {
            case 1:
                return R.drawable.blue;
            case 2:
                return R.drawable.blue_o;
            case 3:
                return R.drawable.gay;
            case 4:
                return R.drawable.gay_o;
            case AnimationType.ALPHA:
                return R.drawable.gray;
            case AnimationType.TRANSLATE_FROM_RIGHT:
                return R.drawable.gray_o;
            case AnimationType.TRANSLATE_FROM_LEFT:
                return R.drawable.green;
            case AirAD.ANIMATION_FIXED:
                return R.drawable.green_o;
            case 9:
                return R.drawable.orange;
            case 10:
                return R.drawable.orange_o;
            case AirAD.POSITION_BOTTOM:
                return R.drawable.purple;
            case AirAD.POSITION_TOP:
                return R.drawable.purple_o;
            case 13:
                return R.drawable.red;
            case 14:
                return R.drawable.red_o;
            case 15:
                return R.drawable.violet;
            case 16:
                return R.drawable.violet_o;
            case 17:
                return R.drawable.yellow;
            case 18:
                return R.drawable.yellow_o;
            default:
                return 0;
        }
    }

    public void onPause() {
        super.onPause();
        this.soundManager.stopBgSound();
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        this.soundManager.playBgSound();
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }

    class RefreshHandler extends Handler {
        public RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            if (GameActivity.this.currentGameState == 0) {
                GameActivity.this.playGame();
            } else if (GameActivity.this.currentGameState == 1) {
                GameActivity.this.onPlayGame();
            }
        }

        public void sleep(long paramLong) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), paramLong);
        }
    }
}
