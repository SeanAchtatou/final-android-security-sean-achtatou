package com.x25.apps.BrainTrainer.Accurcy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import java.util.HashMap;

public class BoardView extends LinearLayout {
    private Cell[] cells;
    public HashMap<Integer, Bitmap> colorBalls;
    private Paint paint = new Paint();
    private float regTextSize = 35.0f;

    public BoardView(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
        this.paint.setColor(-65536);
        this.paint.setAntiAlias(true);
        this.paint.setTextSize(this.regTextSize);
        if (this.colorBalls == null) {
            this.colorBalls = new HashMap<>();
        }
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.cells != null) {
            for (int i = 0; i < this.cells.length; i++) {
                if (this.cells[i].isVisible()) {
                    this.paint.setStyle(Paint.Style.FILL);
                    canvas.drawBitmap(this.colorBalls.get(Integer.valueOf(this.cells[i].color)), this.cells[i].left, this.cells[i].top, this.paint);
                    String value = String.valueOf(this.cells[i].getValue());
                    Rect rect = new Rect();
                    this.paint.getTextBounds(value, 0, value.length(), rect);
                    canvas.drawText(value, ((float) this.cells[i].x) - (this.paint.measureText(value, 0, value.length()) / 2.0f), (float) (this.cells[i].y + (Math.abs(rect.height()) / 2)), this.paint);
                }
            }
        }
    }

    public void installBitmap(int color, Bitmap bitmap) {
        this.colorBalls.put(Integer.valueOf(color), bitmap);
    }

    public void setCells(Cell[] cells2) {
        this.cells = cells2;
    }
}
