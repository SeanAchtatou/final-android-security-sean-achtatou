package com.x25.apps.BrainTrainer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import com.mobclick.android.MobclickAgent;
import com.waps.AnimationType;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;
import com.x25.apps.BrainTrainer.Accurcy.AccurcyActivity;
import com.x25.apps.BrainTrainer.Concentration.ConcentrationActivity;
import com.x25.apps.BrainTrainer.Memory.MemoryActivity;
import com.x25.apps.BrainTrainer.Speed.SpeedActivity;
import com.x25.apps.BrainTrainer.Util.Charts;
import com.x25.apps.BrainTrainer.Util.ScoreManager;
import com.x25.apps.BrainTrainer.Util.SoundManager;
import com.x25.apps.BrainTrainer.Util.StoreManager;

public class MainActivity extends Activity implements View.OnClickListener, Runnable, UpdatePointsNotifier {
    private static final int MENU_MORE = 2;
    private static final int MENU_QUIT = 3;
    private static final int MENU_SETTING = 1;
    private int accuracyPoint = 50;
    private Ads ads;
    private Animation animation;
    private Button btnAccuracy;
    private Button btnConcentration;
    private Button btnMemory;
    private Button btnMore;
    private Button btnProgress;
    private Button btnSetting;
    private Button btnShare;
    private Button btnSpeed;
    private int counter = 0;
    private Handler handler;
    private int memoryPoint = 80;
    private int needPoint = 100;
    private int point = 0;
    private SoundManager soundManager;
    private StoreManager store;
    private int vipPoint = 200;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        Global.disableWaps("2011-08-19 18:00:00");
        this.handler = new Handler();
        this.animation = AnimationUtils.loadAnimation(this, R.anim.shake);
        this.soundManager = new SoundManager(this);
        this.store = new StoreManager(this);
        this.btnAccuracy = (Button) findViewById(R.id.btnAccuracy);
        this.btnAccuracy.setOnClickListener(this);
        this.btnConcentration = (Button) findViewById(R.id.btnConcentration);
        this.btnConcentration.setOnClickListener(this);
        this.btnMemory = (Button) findViewById(R.id.btnMemory);
        this.btnMemory.setOnClickListener(this);
        this.btnProgress = (Button) findViewById(R.id.btnProgress);
        this.btnProgress.setOnClickListener(this);
        this.btnShare = (Button) findViewById(R.id.btnShare);
        this.btnShare.setOnClickListener(this);
        this.btnSpeed = (Button) findViewById(R.id.btnSpeed);
        this.btnSpeed.setOnClickListener(this);
        this.btnMore = (Button) findViewById(R.id.btnMore);
        this.btnMore.setOnClickListener(this);
        this.btnSetting = (Button) findViewById(R.id.btnSetting);
        this.btnSetting.setOnClickListener(this);
        if (Global.isWaps(this)) {
            if (this.store.getInt("vip") == 1) {
                this.btnShare.setText("告诉朋友");
            }
        } else if (Global.isChinese(this)) {
            this.btnShare.setText("告诉朋友");
            this.btnMore.setText("更多应用");
        }
        MobclickAgent.update(this);
        AppConnect.getInstance(this);
        this.ads = new Ads(this);
        this.ads.getAd().getWbs();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAccuracy /*2131296268*/:
                this.soundManager.play(1);
                if (!Global.isWaps(this) || this.store.getInt("isBuyAccurcy") != 0) {
                    startActivity(new Intent(this, AccurcyActivity.class));
                    return;
                } else if (this.point < this.accuracyPoint) {
                    new AlertDialog.Builder(this).setTitle("积分不足").setMessage("请先激活，永久开启此游戏需要 " + this.accuracyPoint + " 积分，你当前积分余额为：" + this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。为确保积分到账，安装后请启动一次！").setPositiveButton((int) R.string.btnMore, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                        }
                    }).show();
                    return;
                } else {
                    this.point -= this.accuracyPoint;
                    AppConnect.getInstance(this).spendPoints(this.accuracyPoint, this);
                    this.store.set("isBuyAccurcy", 1);
                    new AlertDialog.Builder(this).setTitle("开启游戏成功").setMessage("恭喜您，你已经成功永久开启此游戏，重新点击进入游戏吧！").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                        }
                    }).show();
                    return;
                }
            case R.id.btnMemory /*2131296269*/:
                this.soundManager.play(1);
                if (!Global.isWaps(this) || this.store.getInt("isBuyMemory") != 0) {
                    startActivity(new Intent(this, MemoryActivity.class));
                    return;
                } else if (this.point < this.memoryPoint) {
                    new AlertDialog.Builder(this).setTitle("积分不足").setMessage("请先激活，永久开启此游戏需要 " + this.memoryPoint + " 积分，你当前积分余额为：" + this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。为确保积分到账，安装后请启动一次！").setPositiveButton((int) R.string.btnMore, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                        }
                    }).show();
                    return;
                } else {
                    this.point -= this.memoryPoint;
                    AppConnect.getInstance(this).spendPoints(this.memoryPoint, this);
                    this.store.set("isBuyMemory", 1);
                    new AlertDialog.Builder(this).setTitle("开启游戏成功").setMessage("恭喜您，你已经成功永久开启此游戏，重新点击进入游戏吧！").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                        }
                    }).show();
                    return;
                }
            case R.id.btnSpeed /*2131296270*/:
                this.soundManager.play(1);
                if (!Global.isWaps(this) || this.store.getInt("isBuySpeed") != 0) {
                    startActivity(new Intent(this, SpeedActivity.class));
                    return;
                } else if (this.point < this.needPoint) {
                    new AlertDialog.Builder(this).setTitle("积分不足").setMessage("请先激活，永久开启此游戏需要 " + this.needPoint + " 积分，你当前积分余额为：" + this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。为确保积分到账，安装后请启动一次！").setPositiveButton((int) R.string.btnMore, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                        }
                    }).show();
                    return;
                } else {
                    this.point -= this.needPoint;
                    AppConnect.getInstance(this).spendPoints(this.needPoint, this);
                    this.store.set("isBuySpeed", 1);
                    new AlertDialog.Builder(this).setTitle("开启游戏成功").setMessage("恭喜您，你已经成功永久开启此游戏，重新点击进入游戏吧！").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                        }
                    }).show();
                    return;
                }
            case R.id.btnConcentration /*2131296271*/:
                this.soundManager.play(1);
                if (!Global.isWaps(this) || this.store.getInt("isBuy") != 0) {
                    startActivity(new Intent(this, ConcentrationActivity.class));
                    return;
                } else if (this.point < this.needPoint) {
                    new AlertDialog.Builder(this).setTitle("积分不足").setMessage("请先激活，永久开启此游戏需要 " + this.needPoint + " 积分，你当前积分余额为：" + this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。为确保积分到账，安装后请启动一次！").setPositiveButton((int) R.string.btnMore, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                        }
                    }).show();
                    return;
                } else {
                    this.point -= this.needPoint;
                    AppConnect.getInstance(this).spendPoints(this.needPoint, this);
                    this.store.set("isBuy", 1);
                    new AlertDialog.Builder(this).setTitle("开启游戏成功").setMessage("恭喜您，你已经成功永久开启此游戏，重新点击进入游戏吧！").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                        }
                    }).show();
                    return;
                }
            case R.id.btnProgress /*2131296272*/:
                this.soundManager.play(1);
                ScoreManager score = new ScoreManager(this);
                Charts charts = new Charts(this);
                charts.setTitle(getResources().getString(R.string.app_name));
                charts.setXTitle(getResources().getString(R.string.textChartTimes));
                charts.setYTitle(getResources().getString(R.string.textChartScore));
                charts.setTitles(new String[]{getResources().getString(R.string.btnAccuracy), getResources().getString(R.string.btnMemory), getResources().getString(R.string.btnSpeed), getResources().getString(R.string.btnConcentration)});
                charts.setValues(new double[][]{score.getAccurcyScore(), score.getMemoryScore(), score.getSpeedScore(), score.getConcentrationScore()});
                startActivity(charts.getIntent());
                return;
            case R.id.btnSetting /*2131296273*/:
                setting();
                return;
            case R.id.btnShare /*2131296274*/:
                this.soundManager.play(1);
                if (!Global.isWaps(this) || this.store.getInt("vip") != 0) {
                    Intent intent = new Intent("android.intent.action.SEND");
                    intent.setType("text/plain");
                    intent.putExtra("android.intent.extra.SUBJECT", getResources().getString(R.string.app_name));
                    intent.putExtra("android.intent.extra.TEXT", "https://market.android.com/details?id=" + getPackageName());
                    Intent.createChooser(intent, "Share");
                    startActivity(intent);
                    return;
                } else if (this.point < this.vipPoint) {
                    new AlertDialog.Builder(this).setTitle("积分不足").setMessage("永久免费去广告需要 " + this.vipPoint + " 积分，你当前积分余额为：" + this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。").setPositiveButton((int) R.string.btnMore, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            AppConnect.getInstance(MainActivity.this).showOffers(MainActivity.this);
                        }
                    }).show();
                    return;
                } else {
                    this.point -= this.vipPoint;
                    AppConnect.getInstance(this).spendPoints(this.vipPoint, this);
                    this.store.set("vip", 1);
                    new AlertDialog.Builder(this).setTitle("去广告成功").setMessage("恭喜您，你已经成功去除所有广告，如果发现还有广告，请重新启动即可！").setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                        }
                    }).show();
                    this.btnShare.setText("告诉朋友");
                    return;
                }
            case R.id.btnMore /*2131296275*/:
                more();
                return;
            default:
                return;
        }
    }

    public void run() {
        switch (this.counter % 8) {
            case 0:
                this.btnAccuracy.startAnimation(this.animation);
                break;
            case 1:
                this.btnMemory.startAnimation(this.animation);
                break;
            case 2:
                this.btnSpeed.startAnimation(this.animation);
                break;
            case 3:
                this.btnConcentration.startAnimation(this.animation);
                break;
            case 4:
                this.btnProgress.startAnimation(this.animation);
                break;
            case AnimationType.ALPHA:
                this.btnSetting.startAnimation(this.animation);
                break;
            case AnimationType.TRANSLATE_FROM_RIGHT:
                this.btnShare.startAnimation(this.animation);
                break;
            case AnimationType.TRANSLATE_FROM_LEFT:
                this.btnMore.startAnimation(this.animation);
                break;
        }
        this.handler.postDelayed(this, 5000);
        if (this.counter < 7) {
            this.counter++;
        } else {
            this.counter = 0;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.setting).setIcon((int) R.drawable.setting);
        menu.add(0, 2, 0, (int) R.string.more).setIcon((int) R.drawable.more);
        menu.add(0, 3, 0, (int) R.string.exit).setIcon((int) R.drawable.exit);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                setting();
                return true;
            case 2:
                more();
                return true;
            case 3:
                exit();
                return true;
            default:
                return true;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        exit();
        return false;
    }

    private void setting() {
        this.soundManager.play(1);
        startActivity(new Intent(this, SettingActivity.class));
    }

    private void more() {
        this.soundManager.play(1);
        if (Global.isWaps(this)) {
            AppConnect.getInstance(this).showOffers(this);
        } else {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"AppTeam Lab\"")));
        }
    }

    private void exit() {
        new AlertDialog.Builder(this).setTitle((int) R.string.exit).setMessage((int) R.string.exit_msg).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MainActivity.this.finish();
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.handler.removeCallbacks(this);
        this.soundManager.stopBgSound();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.handler.postDelayed(this, 1000);
        this.soundManager.playBgSound();
        MobclickAgent.onResume(this);
        AppConnect.getInstance(this).getPoints(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AppConnect.getInstance(this).finalize();
        this.ads.finalize();
    }

    public void getUpdatePoints(String currencyName, int pointTotal) {
        this.point = pointTotal;
    }

    public void getUpdatePointsFailed(String error) {
        this.point = 0;
    }
}
