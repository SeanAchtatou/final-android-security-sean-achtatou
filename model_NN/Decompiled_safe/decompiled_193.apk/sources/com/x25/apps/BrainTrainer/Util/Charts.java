package com.x25.apps.BrainTrainer.Util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import java.util.ArrayList;
import java.util.List;
import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

public class Charts {
    private int[] colors = {-16776961, -16711936, -16711681, -256};
    private Context context;
    private PointStyle[] styles = {PointStyle.CIRCLE, PointStyle.DIAMOND, PointStyle.TRIANGLE, PointStyle.SQUARE};
    private String title;
    private String[] titles;
    private double[][] values;
    private String xTitle;
    private String yTitle;

    public Charts(Context context2) {
        this.context = context2;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public void setXTitle(String xTitle2) {
        this.xTitle = xTitle2;
    }

    public void setYTitle(String yTitle2) {
        this.yTitle = yTitle2;
    }

    public void setTitles(String[] titles2) {
        this.titles = titles2;
    }

    public void setValues(double[][] values2) {
        this.values = values2;
    }

    private XYMultipleSeriesDataset buildDataset(String[] titles2, List<double[]> xValues, List<double[]> yValues) {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        int length = titles2.length;
        for (int i = 0; i < length; i++) {
            XYSeries series = new XYSeries(titles2[i]);
            double[] xV = xValues.get(i);
            double[] yV = yValues.get(i);
            int seriesLength = xV.length;
            for (int k = 0; k < seriesLength; k++) {
                series.add(xV[k], yV[k]);
            }
            dataset.addSeries(series);
        }
        return dataset;
    }

    private XYMultipleSeriesRenderer buildRenderer(int[] colors2, PointStyle[] styles2) {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        renderer.setAxisTitleTextSize(16.0f);
        renderer.setChartTitleTextSize(20.0f);
        renderer.setLabelsTextSize(15.0f);
        renderer.setLegendTextSize(15.0f);
        renderer.setPointSize(5.0f);
        int[] iArr = new int[4];
        iArr[0] = 30;
        iArr[1] = 45;
        iArr[2] = 20;
        renderer.setMargins(iArr);
        int length = colors2.length;
        for (int i = 0; i < length; i++) {
            XYSeriesRenderer r = new XYSeriesRenderer();
            r.setColor(colors2[i]);
            r.setPointStyle(styles2[i]);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    private void setChartSettings(XYMultipleSeriesRenderer renderer, String title2, String xTitle2, String yTitle2, double xMin, double xMax, double yMin, double yMax, int axesColor, int labelsColor) {
        renderer.setChartTitle(title2);
        renderer.setXTitle(xTitle2);
        renderer.setYTitle(yTitle2);
        renderer.setXAxisMin(xMin);
        renderer.setXAxisMax(xMax);
        renderer.setYAxisMin(yMin);
        renderer.setYAxisMax(yMax);
        renderer.setAxesColor(axesColor);
        renderer.setLabelsColor(labelsColor);
    }

    public Intent getIntent() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        int[] colors2 = new int[this.titles.length];
        PointStyle[] styles2 = new PointStyle[this.titles.length];
        for (int i = 0; i < this.titles.length; i++) {
            double[] xTemp = new double[this.values[i].length];
            for (int k = 0; k < this.values[i].length; k++) {
                xTemp[k] = (double) (k + 1);
            }
            arrayList.add(xTemp);
            arrayList2.add(this.values[i]);
            colors2[i] = this.colors[i];
            styles2[i] = this.styles[i];
        }
        XYMultipleSeriesRenderer renderer = buildRenderer(colors2, styles2);
        int length = renderer.getSeriesRendererCount();
        for (int i2 = 0; i2 < length; i2++) {
            ((XYSeriesRenderer) renderer.getSeriesRendererAt(i2)).setFillPoints(true);
        }
        setChartSettings(renderer, this.title, this.xTitle, this.yTitle, 0.0d, (double) Math.max(10, this.values[0].length), 0.0d, 100.0d, DefaultRenderer.TEXT_COLOR, DefaultRenderer.TEXT_COLOR);
        renderer.setXLabels(12);
        renderer.setYLabels(10);
        renderer.setShowGrid(true);
        renderer.setXLabelsAlign(Paint.Align.RIGHT);
        renderer.setYLabelsAlign(Paint.Align.RIGHT);
        renderer.setPanLimits(new double[]{0.0d, 0.0d, 0.0d, 0.0d});
        renderer.setZoomLimits(new double[]{0.0d, 0.0d, 0.0d, 0.0d});
        return ChartFactory.getLineChartIntent(this.context, buildDataset(this.titles, arrayList, arrayList2), renderer, this.title);
    }
}
