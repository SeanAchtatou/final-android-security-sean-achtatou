package com.x25.apps.BrainTrainer.Memory;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.x25.apps.BrainTrainer.R;

public class Cell extends ImageView {
    private int imgResId;
    private int value;

    public Cell(Context context, int val) {
        super(context);
        this.imgResId = context.getResources().getIdentifier("cell_" + val, "drawable", context.getPackageName());
        setVisibility(0);
        setValue(val);
        setImageResource(this.imgResId);
        setBackgroundResource(R.drawable.cell_bg);
    }

    public void setValue(int val) {
        this.value = val;
    }

    public int getValue() {
        return this.value;
    }

    public void setXY(int x, int y, int tileSize) {
        LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(tileSize, tileSize);
        localLayoutParams.setMargins(x, y, 0, 0);
        setLayoutParams(localLayoutParams);
    }

    public void mask() {
        setImageResource(R.drawable.cell_mask);
        invalidate();
    }

    public void unmask() {
        setImageResource(this.imgResId);
        setBackgroundResource(R.drawable.cell_unmask_bg);
        invalidate();
    }
}
