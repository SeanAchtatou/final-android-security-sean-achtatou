package com.x25.apps.BrainTrainer.Memory;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;
import com.x25.apps.BrainTrainer.Ads;
import com.x25.apps.BrainTrainer.R;
import com.x25.apps.BrainTrainer.Util.Charts;
import com.x25.apps.BrainTrainer.Util.ScoreManager;
import com.x25.apps.BrainTrainer.Util.SoundManager;
import com.x25.apps.BrainTrainer.Util.Utils;
import java.util.Arrays;
import java.util.Collections;

public class MemoryActivity extends Activity implements View.OnClickListener, View.OnTouchListener, Runnable {
    private Ads ads;
    private Animation animation;
    private int balls = 3;
    private LinearLayout board;
    private int boardHeight;
    private int boardWidth;
    private TextView btnHowTo;
    private Button btnStart;
    private Cell[] cells;
    private ImageView cooldown;
    private AnimationDrawable cooldownAnimation;
    private int curVal = 0;
    /* access modifiers changed from: private */
    public int currentGameState = 999;
    private RefreshHandler handler;
    private boolean isCanTouch = false;
    private int maxBalls = 9;
    private int score = 0;
    private ScoreManager scoreManager;
    private SoundManager soundManager;
    private int tileSize;
    private int timerTime = 3;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.memory);
        this.tileSize = Utils.dipToPx(this, 50);
        this.handler = new RefreshHandler();
        this.animation = AnimationUtils.loadAnimation(this, R.anim.shake);
        this.soundManager = new SoundManager(this);
        this.btnStart = (Button) findViewById(R.id.btnStart);
        this.btnStart.setOnClickListener(this);
        this.btnStart.setText(((Object) this.btnStart.getText()) + "(Attempts left:" + this.timerTime + ")");
        this.board = (LinearLayout) findViewById(R.id.board);
        this.board.setOnTouchListener(this);
        this.btnHowTo = (TextView) findViewById(R.id.textMemoryHowto);
        this.scoreManager = new ScoreManager(this);
        this.ads = new Ads(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                this.btnHowTo.setVisibility(8);
                this.soundManager.play(4);
                startGame();
                return;
            default:
                return;
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0 && (v instanceof Cell)) {
            int value = ((Cell) v).getValue();
            if (!this.isCanTouch || value <= this.curVal) {
                return true;
            }
            ((Cell) v).unmask();
            if (value == this.curVal + 1) {
                this.soundManager.play(3);
                this.curVal++;
                if (this.balls == this.maxBalls) {
                    this.score += 2;
                }
                if (this.curVal == this.cells.length) {
                    nextLevel();
                }
            } else {
                this.soundManager.play(2);
                for (Cell cell : this.cells) {
                    cell.unmask();
                }
                this.timerTime--;
                if (this.timerTime > 0) {
                    replayGame();
                } else {
                    finishGame();
                }
            }
        }
        return true;
    }

    public void startGame() {
        if (this.currentGameState == 3) {
            ScoreManager score2 = new ScoreManager(this);
            Charts charts = new Charts(this);
            charts.setTitle(getResources().getString(R.string.btnMemory));
            charts.setXTitle(getResources().getString(R.string.textChartTimes));
            charts.setYTitle(getResources().getString(R.string.textChartScore));
            charts.setTitles(new String[]{getResources().getString(R.string.btnMemory)});
            charts.setValues(new double[][]{score2.getMemoryScore()});
            startActivity(charts.getIntent());
            finish();
            return;
        }
        this.ads.getAd().getWb();
        this.btnStart.setEnabled(false);
        this.handler.removeCallbacks(this);
        this.currentGameState = 0;
        this.curVal = 0;
        showCooldown();
        this.handler.sleep(3000);
    }

    public void playGame() {
        int y;
        closeCooldown();
        this.currentGameState = 1;
        this.cells = genCells(this.balls);
        this.board.removeAllViews();
        this.boardWidth = this.board.getWidth();
        this.boardHeight = this.board.getHeight();
        int i = 1;
        int offset = Utils.getRandom(0, this.maxBalls - this.balls);
        for (Cell cell : this.cells) {
            cell.setOnTouchListener(this);
            int x = Utils.getRandom(this.tileSize, this.boardWidth - this.tileSize);
            if (i == 1) {
                y = Utils.getRandom(0, this.tileSize * offset);
            } else if (this.balls == this.maxBalls) {
                y = Utils.getRandom(-10, 0);
            } else {
                y = Utils.getRandom(0, (((this.maxBalls - this.balls) - offset) / (this.balls + 1)) * this.tileSize);
            }
            cell.setXY(x, y, this.tileSize);
            this.board.addView(cell);
            i++;
        }
        this.handler.sleep(1500);
    }

    public void nextLevel() {
        this.ads.clear();
        this.isCanTouch = false;
        this.btnStart.setText((int) R.string.btnNextLevel);
        this.btnStart.setText(((Object) this.btnStart.getText()) + "(Attempts left:" + this.timerTime + ")");
        this.btnStart.setEnabled(true);
        this.handler.postDelayed(this, 1000);
        this.score += this.balls * 2;
        this.balls++;
        if (this.balls > this.maxBalls) {
            if (this.timerTime == 3) {
                this.score = 100;
            }
            finishGame();
        }
    }

    public void replayGame() {
        this.ads.clear();
        this.score -= Math.min(this.score, 5);
        this.isCanTouch = false;
        this.btnStart.setText((int) R.string.btnReplay);
        this.btnStart.setText(((Object) this.btnStart.getText()) + "(Attempts left:" + this.timerTime + ")");
        this.btnStart.setEnabled(true);
        this.handler.postDelayed(this, 1000);
    }

    public void mask() {
        this.currentGameState = 2;
        this.isCanTouch = true;
        for (Cell cell : this.cells) {
            cell.mask();
        }
    }

    public void finishGame() {
        this.ads.clear();
        this.isCanTouch = false;
        this.currentGameState = 3;
        this.btnStart.setEnabled(true);
        this.btnStart.setText((int) R.string.btnSummary);
        this.btnStart.setText(((Object) this.btnStart.getText()) + "(Attempts left:" + this.timerTime + ")");
        this.scoreManager.addMemoryScore(Math.min(100, this.score));
        this.handler.postDelayed(this, 1000);
    }

    public void showCooldown() {
        this.board.removeAllViews();
        this.cooldown = new ImageView(this);
        this.cooldown.setImageResource(R.drawable.cooldown);
        this.boardWidth = this.board.getWidth();
        this.boardHeight = this.board.getHeight();
        int cooldownSize = Utils.dipToPx(this, 200);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(cooldownSize, cooldownSize);
        layoutParams.setMargins((this.boardWidth - cooldownSize) / 2, (this.boardHeight - cooldownSize) / 2, 0, 0);
        this.cooldown.setLayoutParams(layoutParams);
        this.board.addView(this.cooldown);
        this.cooldownAnimation = (AnimationDrawable) this.cooldown.getDrawable();
        this.cooldownAnimation.start();
    }

    public void closeCooldown() {
        this.cooldown.setVisibility(8);
        this.cooldownAnimation.stop();
        this.board.removeView(this.cooldown);
    }

    private Cell[] genCells(int num) {
        Cell[] cells2 = new Cell[num];
        for (int i = 1; i <= num; i++) {
            cells2[i - 1] = new Cell(this, i);
        }
        Collections.shuffle(Arrays.asList(cells2));
        return cells2;
    }

    public void run() {
        this.btnStart.startAnimation(this.animation);
        this.handler.postDelayed(this, 5000);
    }

    public void onPause() {
        super.onPause();
        this.handler.removeCallbacks(this);
        this.soundManager.stopBgSound();
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        this.soundManager.playBgSound();
        if (this.btnStart.isEnabled()) {
            this.handler.removeCallbacks(this);
            this.handler.post(this);
        }
        MobclickAgent.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }

    class RefreshHandler extends Handler {
        public RefreshHandler() {
        }

        public void handleMessage(Message msg) {
            if (MemoryActivity.this.currentGameState == 0) {
                MemoryActivity.this.playGame();
            } else if (MemoryActivity.this.currentGameState == 1) {
                MemoryActivity.this.mask();
            }
        }

        public void sleep(long paramLong) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), paramLong);
        }
    }
}
