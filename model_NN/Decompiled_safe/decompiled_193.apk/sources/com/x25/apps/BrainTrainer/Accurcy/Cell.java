package com.x25.apps.BrainTrainer.Accurcy;

import android.graphics.Point;
import com.x25.apps.BrainTrainer.Util.Utils;

public class Cell {
    public float bottom;
    public int color;
    public int dx = 0;
    public int dy = 0;
    public float left;
    public int radius;
    public float right;
    public float top;
    public int value;
    private boolean visible;
    public int x;
    public int y;

    public Cell(int x2, int y2, int radius2, int speed) {
        this.x = x2;
        this.y = y2;
        this.radius = radius2;
        this.left = (float) (x2 - radius2);
        this.top = (float) (y2 - radius2);
        this.right = (float) (x2 + radius2);
        this.bottom = (float) (y2 + radius2);
        if (Utils.getRandom(0, 1) == 0) {
            this.dx = speed;
        } else {
            this.dx = speed * -1;
        }
        if (Utils.getRandom(0, 1) == 0) {
            this.dy = speed * -1;
        } else {
            this.dy = speed;
        }
        setVisible(true);
    }

    public boolean contains(float x2, float y2) {
        if (((((float) this.x) - x2) * (((float) this.x) - x2)) + ((((float) this.y) - y2) * (((float) this.y) - y2)) <= ((float) (this.radius * this.radius))) {
            return true;
        }
        return false;
    }

    public void move(int width, int height) {
        if (this.y < this.radius || this.y > height - this.radius) {
            this.dy *= -1;
        }
        if (this.x < this.radius || this.x > width - this.radius) {
            this.dx *= -1;
        }
        this.x += this.dx;
        this.y += this.dy;
        this.left = (float) (this.x - this.radius);
        this.top = (float) (this.y - this.radius);
        this.right = (float) (this.x + this.radius);
        this.bottom = (float) (this.y + this.radius);
    }

    public Point simualateMove() {
        return new Point(this.x + this.dx, this.y + this.dy);
    }

    public void setValue(int val) {
        this.value = val;
    }

    public int getValue() {
        return this.value;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean visible2) {
        this.visible = visible2;
    }
}
