package com.adwo.adsdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.view.animation.AlphaAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import org.achartengine.renderer.DefaultRenderer;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/* renamed from: com.adwo.adsdk.j  reason: case insensitive filesystem */
final class C0011j extends FrameLayout {
    private static String o;
    protected WebView a;
    private NotificationManager b;
    private Notification c = null;
    private volatile boolean d = false;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public Button f;
    private Button g;
    /* access modifiers changed from: private */
    public RelativeLayout h;
    private RelativeLayout i;
    /* access modifiers changed from: private */
    public String j;
    private Drawable k;
    private Drawable l;
    private Drawable m;
    private WeakReference n;

    C0011j(Activity activity, int i2, long j2, String str, boolean z, boolean z2, boolean z3, boolean z4) {
        super(activity);
        String str2;
        new C0012k(this);
        this.n = new WeakReference(activity);
        setId(15062);
        Activity activity2 = (Activity) this.n.get();
        if (activity2 != null) {
            this.b = (NotificationManager) activity2.getSystemService("notification");
            this.c = new Notification(17301598, "程序下载完成", System.currentTimeMillis());
            this.c.defaults = 1;
            activity2.setTheme(16973840);
            setWillNotDraw(false);
            Integer valueOf = Integer.valueOf((int) (activity2.getResources().getDisplayMetrics().density * 0.0625f * ((float) i2)));
            setPadding(valueOf.intValue(), valueOf.intValue(), valueOf.intValue(), valueOf.intValue());
            this.i = new RelativeLayout(activity2);
            this.i.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            Activity activity3 = (Activity) this.n.get();
            if (activity3 != null) {
                this.a = new WebView(activity3);
                this.a.setId(200);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                this.a.setLayoutParams(layoutParams);
                this.a.setWebViewClient(new w(this));
                this.a.setWebChromeClient(new v(this));
                this.a.addJavascriptInterface(new u(this), "interface");
                WebSettings settings = this.a.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setDefaultTextEncodingName("UTF-8");
                settings.setGeolocationEnabled(true);
                settings.setBuiltInZoomControls(true);
                settings.setAllowFileAccess(true);
                o = settings.getUserAgentString();
                if (z) {
                    layoutParams.addRule(3, 100);
                }
                this.i.addView(this.a);
                if (z4) {
                    this.a.setBackgroundColor(0);
                    this.i.setBackgroundColor(0);
                } else {
                    this.a.setBackgroundColor(-1);
                    this.i.setBackgroundColor(-1);
                }
                this.h = new RelativeLayout(activity3);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, 40);
                layoutParams2.addRule(12);
                this.h.setBackgroundColor(DefaultRenderer.TEXT_COLOR);
                this.h.setId(300);
                this.g = new Button(activity3);
                this.g.setBackgroundColor(DefaultRenderer.BACKGROUND_COLOR);
                AssetManager assets = activity3.getAssets();
                try {
                    this.m = Drawable.createFromStream(assets.open("adwo_x.png"), "adwo_x.png");
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                c(z3);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.addRule(11);
                layoutParams3.addRule(15);
                this.h.addView(this.g, layoutParams3);
                this.e = new Button(activity3);
                try {
                    this.k = Drawable.createFromStream(assets.open("adwo_right_arrow.png"), "adwo_right_arrow.png");
                } catch (IOException e3) {
                    this.e.setBackgroundColor(DefaultRenderer.TEXT_COLOR);
                    this.e.setText(">>");
                    this.e.setTextColor((int) DefaultRenderer.BACKGROUND_COLOR);
                    e3.printStackTrace();
                }
                a(z3);
                this.f = new Button(activity3);
                try {
                    this.l = Drawable.createFromStream(assets.open("adwo_left_arrow.png"), "adwo_left_arrow.png");
                } catch (IOException e4) {
                    this.f.setBackgroundColor(DefaultRenderer.TEXT_COLOR);
                    this.f.setText("<<");
                    this.f.setTextColor((int) DefaultRenderer.BACKGROUND_COLOR);
                    e4.printStackTrace();
                }
                this.f.setId(301);
                b(z3);
                this.f.setId(302);
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams4.addRule(15);
                this.h.addView(this.f, layoutParams4);
                this.e.setId(303);
                RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams5.addRule(1, this.f.getId());
                layoutParams5.addRule(15);
                this.h.addView(this.e, layoutParams5);
                this.f.setVisibility(4);
                this.e.setVisibility(4);
                this.i.addView(this.h, layoutParams2);
                if (z2) {
                    this.h.setVisibility(0);
                } else {
                    this.h.setVisibility(4);
                }
                addView(this.i);
                if (str == null) {
                    str2 = "toptobottom";
                } else {
                    str2 = str;
                }
                if (str2.equals("toptobottom")) {
                    TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                    translateAnimation.setDuration(j2);
                    translateAnimation.setAnimationListener(new C0014m(this));
                    startAnimation(translateAnimation);
                } else if (str2.equals("explode")) {
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.1f, 0.9f, 0.1f, 0.9f, 1, 0.5f, 1, 0.5f);
                    scaleAnimation.setDuration(j2);
                    scaleAnimation.setAnimationListener(new C0015n(this));
                    startAnimation(scaleAnimation);
                } else {
                    TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                    translateAnimation2.setDuration(j2);
                    translateAnimation2.setAnimationListener(new C0016o(this));
                    startAnimation(translateAnimation2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        new Rect(new Rect(canvas.getClipBounds())).inset(0, 0);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.j = str;
        new Thread(new C0017p(this)).start();
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        if (this.e != null && z) {
            this.e.setBackgroundDrawable(this.k);
            this.e.setOnClickListener(new C0018q(this));
            this.e.setEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(boolean z) {
        if (this.f != null && z) {
            this.f.setBackgroundDrawable(this.l);
            this.f.setOnClickListener(new C0019r(this));
            this.f.setEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    public final void c(boolean z) {
        if (this.g != null && z) {
            this.g.setBackgroundDrawable(this.m);
            this.g.setOnClickListener(new C0020s(this));
            this.g.setEnabled(true);
        }
    }

    static /* synthetic */ void a(C0011j jVar, boolean z) {
        Activity activity = (Activity) jVar.n.get();
        if (activity == null) {
            return;
        }
        if (1 != 0) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(200);
            activity.finish();
            jVar.startAnimation(alphaAnimation);
            return;
        }
        activity.finish();
    }

    /* access modifiers changed from: package-private */
    public final Activity a() {
        return (Activity) this.n.get();
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, Activity activity) {
        String str2;
        CharSequence charSequence;
        String substring = str.substring(str.lastIndexOf("/") + 1);
        String externalStorageState = Environment.getExternalStorageState();
        if (!externalStorageState.equals("mounted")) {
            if (externalStorageState.equals("shared")) {
                str2 = "SD 卡正忙。要允许下载，请在通知中选择\"关闭USB 存储\"";
                charSequence = "SD 卡不可用";
            } else {
                str2 = "需要有 SD 卡才能下载" + substring;
                charSequence = "无 SD 卡";
            }
            new AlertDialog.Builder(a()).setTitle(charSequence).setIcon(17301543).setMessage(str2).setPositiveButton("OK", (DialogInterface.OnClickListener) null).show();
            return;
        }
        activity.runOnUiThread(new C0021t(this, activity, substring));
        if (!this.d) {
            this.d = true;
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(str);
            httpGet.setHeader("User-Agent", o);
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() == 200) {
                    InputStream content = execute.getEntity().getContent();
                    File file = new File(Environment.getExternalStorageDirectory() + "/adwo/");
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    File file2 = new File(Environment.getExternalStorageDirectory() + "/adwo/", substring);
                    if (file2.exists() || file2.createNewFile()) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file2);
                        while (true) {
                            int read = content.read();
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(read);
                        }
                        content.close();
                        fileOutputStream.close();
                        Uri fromFile = Uri.fromFile(file2);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setFlags(268435456);
                        intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
                        PendingIntent activity2 = PendingIntent.getActivity(activity, 0, intent, 268435456);
                        if (!(this.c == null || activity2 == null)) {
                            this.c.setLatestEventInfo(activity, String.valueOf(substring) + "下载完成", String.valueOf(substring) + "下载完成,请安装使用", activity2);
                        }
                        this.b.notify(0, this.c);
                    } else {
                        return;
                    }
                }
                this.d = false;
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
            } finally {
                this.d = false;
            }
        }
    }

    static /* synthetic */ void a(C0011j jVar, String str, Activity activity) {
        String str2;
        CharSequence charSequence;
        String substring = str.substring(str.lastIndexOf("/") + 1);
        String externalStorageState = Environment.getExternalStorageState();
        if (!externalStorageState.equals("mounted")) {
            if (externalStorageState.equals("shared")) {
                str2 = "SD 卡正忙。要允许下载，请在通知中选择\"关闭USB 存储\"";
                charSequence = "SD 卡不可用";
            } else {
                str2 = "需要有 SD 卡才能下载" + substring;
                charSequence = "无 SD 卡";
            }
            new AlertDialog.Builder(jVar.a()).setTitle(charSequence).setIcon(17301543).setMessage(str2).setPositiveButton("OK", (DialogInterface.OnClickListener) null).show();
            return;
        }
        activity.runOnUiThread(new C0013l(jVar, activity, substring));
        if (!jVar.d) {
            jVar.d = true;
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(str);
            httpGet.setHeader("User-Agent", o);
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() == 200) {
                    InputStream content = execute.getEntity().getContent();
                    File file = new File(Environment.getExternalStorageDirectory() + "/adwo/");
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    File file2 = new File(Environment.getExternalStorageDirectory() + "/adwo/", substring);
                    if (file2.exists() || file2.createNewFile()) {
                        FileOutputStream fileOutputStream = new FileOutputStream(file2);
                        while (true) {
                            int read = content.read();
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(read);
                        }
                        content.close();
                        fileOutputStream.close();
                        Uri fromFile = Uri.fromFile(file2);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setFlags(268435456);
                        intent.setDataAndType(fromFile, "image/bmp");
                        intent.setDataAndType(fromFile, "image/gif");
                        intent.setDataAndType(fromFile, "image/jpeg");
                        PendingIntent activity2 = PendingIntent.getActivity(activity, 0, intent, 268435456);
                        if (!(jVar.c == null || activity2 == null)) {
                            jVar.c.setLatestEventInfo(activity, String.valueOf(substring) + "下载完成", String.valueOf(substring) + "下载完成,请安装使用", activity2);
                        }
                        jVar.b.notify(0, jVar.c);
                    } else {
                        return;
                    }
                }
                jVar.d = false;
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
            } finally {
                jVar.d = false;
            }
        }
    }
}
