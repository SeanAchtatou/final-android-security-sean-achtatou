package com.vpon.adon.android;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.vpon.adon.android.entity.Ad;
import com.vpon.adon.android.utils.VPLog;
import com.vpon.adon.android.webClientHandler.AndroidMarketHandler;
import com.vpon.adon.android.webClientHandler.MailHandler;
import com.vpon.adon.android.webClientHandler.RedirectHandler;
import com.vpon.adon.android.webClientHandler.SmsHandler;
import com.vpon.adon.android.webClientHandler.TelHandler;
import com.vpon.adon.android.webClientHandler.WebAndMapHandler;

final class AdDisplayWebViewClient extends WebViewClient {
    private Ad ad;
    private AdView adView;
    private Context context;
    private boolean firstCallBack = true;

    public AdDisplayWebViewClient(AdView adView2, Ad ad2, Context context2) {
        VPLog.i("AdDisplayWebViewClient", "AdDisplayWebViewClient");
        this.adView = adView2;
        this.ad = ad2;
        this.context = context2;
    }

    public void onPageFinished(WebView view, String url) {
        VPLog.i("AdDisplayWebViewClient", "onPageFinished");
        if (this.firstCallBack) {
            this.adView.onRecivedAd();
            this.firstCallBack = false;
        }
        super.onPageFinished(view, url);
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (this.ad.getClickStatus()) {
            return new TelHandler(new SmsHandler(new MailHandler(new AndroidMarketHandler(new WebAndMapHandler(new RedirectHandler(null)))))).doNext(this.context, this.ad, url);
        }
        this.ad.setClicked();
        this.adView.performAdManagerAddClickResp();
        return new TelHandler(new SmsHandler(new MailHandler(new AndroidMarketHandler(new WebAndMapHandler(new RedirectHandler(null)))))).doNext(this.context, this.ad, url);
    }
}
