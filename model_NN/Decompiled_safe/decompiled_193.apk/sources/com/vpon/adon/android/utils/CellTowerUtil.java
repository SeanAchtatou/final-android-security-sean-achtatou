package com.vpon.adon.android.utils;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

public class CellTowerUtil {
    private static CellTowerUtil instance;
    private String cellId;
    private String lac;
    private String mcc;
    private String mnc;

    private CellTowerUtil(Context context) {
        try {
            TelephonyManager telephoneManager = (TelephonyManager) context.getSystemService("phone");
            String networkOperator = telephoneManager.getNetworkOperator();
            this.mcc = networkOperator.substring(0, 3);
            this.mnc = networkOperator.substring(3);
            GsmCellLocation gsmCellLocation = (GsmCellLocation) telephoneManager.getCellLocation();
            this.cellId = String.valueOf(gsmCellLocation.getCid());
            this.lac = String.valueOf(gsmCellLocation.getLac());
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static CellTowerUtil instance(Context context) {
        if (instance == null) {
            instance = new CellTowerUtil(context);
        }
        return instance;
    }

    public String getMcc() {
        return this.mcc;
    }

    public String getMnc() {
        return this.mnc;
    }

    public String getCellId() {
        return this.cellId;
    }

    public String getLac() {
        return this.lac;
    }
}
