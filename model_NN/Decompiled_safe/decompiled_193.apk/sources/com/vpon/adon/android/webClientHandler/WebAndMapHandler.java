package com.vpon.adon.android.webClientHandler;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.vpon.adon.android.WebInApp;
import com.vpon.adon.android.entity.Ad;

public class WebAndMapHandler extends WebClientHandler {
    public WebAndMapHandler(WebClientHandler next) {
        super(next);
    }

    public boolean handle(Context context, Ad ad, String url) {
        if (!url.startsWith("http://")) {
            return doNext(context, ad, url);
        }
        try {
            if (url.contains("&t=o")) {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url.split("&t=o")[0])));
            } else if (ad != null) {
                String url2 = url.split("&t=i")[0];
                Intent intent = new Intent(context, WebInApp.class);
                Bundle bundle = new Bundle();
                bundle.putString("url", url2);
                bundle.putInt("adWidth", ad.getAdWidth());
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return true;
    }
}
