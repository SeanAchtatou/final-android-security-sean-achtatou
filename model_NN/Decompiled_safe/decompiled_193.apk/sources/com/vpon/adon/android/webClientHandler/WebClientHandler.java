package com.vpon.adon.android.webClientHandler;

import android.content.Context;
import com.vpon.adon.android.entity.Ad;

public abstract class WebClientHandler {
    protected WebClientHandler next;

    public abstract boolean handle(Context context, Ad ad, String str);

    public WebClientHandler(WebClientHandler next2) {
        this.next = next2;
    }

    public boolean doNext(Context context, Ad ad, String url) {
        if (this.next != null) {
            return this.next.handle(context, ad, url);
        }
        return false;
    }
}
