package com.mobclick.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.achartengine.ChartFactory;

public class a {
    /* access modifiers changed from: private */
    public Context a;
    private int b = 50;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    private String e;
    private Notification f;
    private NotificationManager g;
    private int h;
    private String i = "未联网，更新失败";
    private String j = "Please make sure you are connected to internet, update failed";
    /* access modifiers changed from: private */
    public boolean k = true;
    /* access modifiers changed from: private */
    public boolean l = true;
    /* access modifiers changed from: private */
    public int m = d.a;
    private Handler n = new b(this);

    public a(Context context, String str, String str2, String str3, String str4) {
        try {
            a(context, str, str2, str3, str4);
        } catch (Exception e2) {
            Log.e("initialization error", e2.getMessage());
            this.k = false;
        }
    }

    private int a(String str, String str2) {
        try {
            Field field = Class.forName(String.valueOf(this.a.getPackageName()) + ".R$" + str).getField(str2);
            return Integer.parseInt(field.get(field.getName()).toString());
        } catch (Exception e2) {
            Log.e("getIdByReflection error", e2.getMessage());
            return 0;
        }
    }

    private void a(Context context, String str, String str2, String str3, String str4) {
        this.a = context;
        e();
        if (this.m == d.b) {
            this.k = false;
            return;
        }
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        this.c = String.valueOf(externalStorageDirectory.getParent()) + "/" + externalStorageDirectory.getName() + "/download";
        this.e = str;
        this.h = 17301633;
        this.f = new Notification(this.h, str2, 1);
        this.f.flags |= 2;
        RemoteViews remoteViews = new RemoteViews(this.a.getPackageName(), a("layout", "umeng_download_notification"));
        remoteViews.setProgressBar(a("id", "progress_bar"), 100, 0, false);
        remoteViews.setTextViewText(a("id", "progress_text"), "0%");
        remoteViews.setTextViewText(a("id", ChartFactory.TITLE), str3);
        remoteViews.setTextViewText(a("id", "description"), str4);
        remoteViews.setImageViewResource(a("id", "appIcon"), this.h);
        this.f.contentView = remoteViews;
        Intent intent = new Intent();
        intent.setClassName(this.a.getPackageName(), this.a.getClass().getName());
        this.f.contentIntent = PendingIntent.getActivity(this.a, 0, intent, 134217728);
        this.g = (NotificationManager) this.a.getSystemService("notification");
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            byte[] c2 = c(this.e);
            if (!this.k) {
                this.g.cancel(0);
                return;
            }
            this.d = String.valueOf(String.valueOf(System.currentTimeMillis()) + ".apk");
            FileOutputStream fileOutputStream = new FileOutputStream(new File(this.c, this.d));
            fileOutputStream.write(c2, 0, c2.length);
            fileOutputStream.close();
            this.g.cancel(0);
            this.n.sendEmptyMessage(0);
        } catch (Exception e2) {
            Log.e("cannot save to sd card", e2.getMessage());
            this.k = false;
        }
    }

    private byte[] c(String str) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.e).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            this.g.notify(0, this.f);
            byte[] bArr = new byte[1024];
            int contentLength = httpURLConnection.getContentLength();
            ArrayList arrayList = new ArrayList(contentLength);
            int i2 = 0;
            int i3 = 0;
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                i2 += read;
                for (int i4 = 0; i4 < read; i4++) {
                    arrayList.add(Byte.valueOf(bArr[i4]));
                }
                int i5 = i3 + 1;
                if (i3 % this.b == 0) {
                    if (!f()) {
                        this.k = false;
                        break;
                    }
                    int i6 = (int) ((((float) i2) * 100.0f) / ((float) contentLength));
                    this.f.contentView.setProgressBar(a("id", "progress_bar"), 100, i6, false);
                    this.f.contentView.setTextViewText(a("id", "progress_text"), String.valueOf(String.valueOf(i6)) + "%");
                    this.g.notify(0, this.f);
                }
                i3 = i5;
            }
            inputStream.close();
            int size = arrayList.size();
            byte[] bArr2 = new byte[size];
            for (int i7 = 0; i7 < size; i7++) {
                bArr2[i7] = ((Byte) arrayList.get(i7)).byteValue();
            }
            return bArr2;
        } catch (Exception e2) {
            Log.e("can not save to memory", e2.getMessage());
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        try {
            this.d = String.valueOf(String.valueOf(System.currentTimeMillis()) + ".apk");
            byte[] c2 = c(this.e);
            if (!this.k) {
                this.g.cancel(0);
                return;
            }
            this.c = this.a.getFilesDir().getAbsolutePath();
            FileOutputStream openFileOutput = this.a.openFileOutput(this.d, 3);
            openFileOutput.write(c2);
            openFileOutput.close();
            this.g.cancel(0);
            this.n.sendEmptyMessage(0);
        } catch (Exception e2) {
            Log.e("can not save to memory", e2.getMessage());
            this.k = false;
        }
    }

    private void d(String str) {
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    private void e() {
        if (!f()) {
            Toast.makeText(this.a, h(), 3).show();
            this.m = d.b;
        } else if (!g()) {
            this.m = d.c;
        } else {
            this.m = d.a;
        }
    }

    private boolean f() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                return activeNetworkInfo.isConnectedOrConnecting();
            }
            return false;
        } catch (Exception e2) {
            return true;
        }
    }

    private boolean g() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    private String h() {
        return this.a.getResources().getConfiguration().locale.toString().equals("zh_CN") ? this.i : this.j;
    }

    public void a(int i2) {
        this.h = i2;
        this.f.icon = i2;
        this.f.contentView.setImageViewResource(a("id", "appIcon"), i2);
    }

    public void a(String str) {
        this.f.contentView.setTextViewText(a("id", ChartFactory.TITLE), str);
    }

    public void a(boolean z) {
        this.l = z;
    }

    public boolean a() {
        return this.k;
    }

    public void b() {
        if (this.k) {
            try {
                d(this.c);
                new c(this).start();
            } catch (Exception e2) {
                Log.e("MobclickAgent", e2.getMessage());
            }
        }
    }

    public void b(String str) {
        this.c = str;
    }
}
