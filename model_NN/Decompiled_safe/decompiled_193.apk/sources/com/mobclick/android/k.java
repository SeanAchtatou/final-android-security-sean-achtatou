package com.mobclick.android;

import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.achartengine.chart.TimeChart;

public class k {
    private static String A = "Not now";
    public static String a = "last_send_time";
    private static String b = "用户反馈";
    private static String c = "Feedback";
    private static String d = "欢迎您提出宝贵的意见和建议，您留下的每个字都将用来改善我们的服务。";
    private static String e = "Any comments and suggestions are welcome, we believe every word you write will benefit us";
    private static String f = "请输入您的反馈意见（字数500以内）";
    private static String g = "Input your suggestions here";
    private static String h = "提交反馈";
    private static String i = "Submit suggestions";
    private static String j = "请正确选择年龄和性别再提交";
    private static String k = "Please fill in a correct age and gender before submitting";
    private static String[] l = {"年龄", "18岁以下", "18-24岁", "25-30岁", "31-35岁", "36-40岁", "41-50岁", "51-59岁", "60岁及60岁以上"};
    private static String[] m = {"Age", "<18", "18~24", "25~30", "31~35", "36~40", "41~50", "51~59", ">=60"};
    private static String[] n = {"性别", "男", "女"};
    private static String[] o = {"Gender", "Male", "Female"};
    private static String p = "应用程序有新版本更新";
    private static String q = "New version found";
    private static String r = "最新版本: ";
    private static String s = "Latest version: ";
    private static String t = "立即更新";
    private static String u = "Update now";
    private static String v = "应用更新";
    private static String w = "App updating";
    private static String x = "正在更新应用程序...";
    private static String y = "Updating application...";
    private static String z = "以后再说";

    public static String A() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static int a(Date date, Date date2) {
        Date date3;
        Date date4;
        if (date.after(date2)) {
            date3 = date;
            date4 = date2;
        } else {
            date3 = date2;
            date4 = date;
        }
        return (int) ((date3.getTime() - date4.getTime()) / TimeChart.DAY);
    }

    public static String a() {
        return b;
    }

    public static String a(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? b : c;
    }

    public static Date a(String str) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(str);
        } catch (Exception e2) {
            return null;
        }
    }

    public static String b() {
        return c;
    }

    public static String b(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? d : e;
    }

    public static String c() {
        return d;
    }

    public static String c(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? f : g;
    }

    public static String d() {
        return e;
    }

    public static String d(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? h : i;
    }

    public static String e() {
        return f;
    }

    public static String e(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? j : k;
    }

    public static String f() {
        return g;
    }

    public static String[] f(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? l : m;
    }

    public static String g() {
        return h;
    }

    public static String[] g(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? n : o;
    }

    public static String h() {
        return i;
    }

    public static String h(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? p : q;
    }

    public static String i() {
        return j;
    }

    public static String i(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? r : s;
    }

    public static String j() {
        return k;
    }

    public static String j(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? t : u;
    }

    public static String k(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? v : w;
    }

    public static String[] k() {
        return l;
    }

    public static String l(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? x : y;
    }

    public static String[] l() {
        return m;
    }

    public static String m(Context context) {
        return context.getResources().getConfiguration().locale.toString().equals("zh_CN") ? z : A;
    }

    public static String[] m() {
        return n;
    }

    public static String[] n() {
        return o;
    }

    public static String o() {
        return p;
    }

    public static String p() {
        return q;
    }

    public static String q() {
        return r;
    }

    public static String r() {
        return s;
    }

    public static String s() {
        return t;
    }

    public static String t() {
        return u;
    }

    public static String u() {
        return v;
    }

    public static String v() {
        return w;
    }

    public static String w() {
        return x;
    }

    public static String x() {
        return y;
    }

    public static String y() {
        return z;
    }

    public static String z() {
        return A;
    }
}
