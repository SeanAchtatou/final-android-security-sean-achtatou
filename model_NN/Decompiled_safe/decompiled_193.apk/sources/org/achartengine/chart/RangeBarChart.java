package org.achartengine.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

public class RangeBarChart extends BarChart {
    public RangeBarChart(XYMultipleSeriesDataset xYMultipleSeriesDataset, XYMultipleSeriesRenderer xYMultipleSeriesRenderer, BarChart.Type type) {
        super(xYMultipleSeriesDataset, xYMultipleSeriesRenderer, type);
    }

    public void drawSeries(Canvas canvas, Paint paint, float[] fArr, SimpleSeriesRenderer simpleSeriesRenderer, float f, int i) {
        int seriesCount = this.mDataset.getSeriesCount();
        int length = fArr.length;
        paint.setColor(simpleSeriesRenderer.getColor());
        paint.setStyle(Paint.Style.FILL);
        float halfDiffX = getHalfDiffX(fArr, length, seriesCount);
        for (int i2 = 0; i2 < length; i2 += 4) {
            float f2 = fArr[i2];
            float f3 = fArr[i2 + 1];
            float f4 = fArr[i2 + 2];
            float f5 = fArr[i2 + 3];
            if (this.mType == BarChart.Type.STACKED) {
                canvas.drawRect(f2 - halfDiffX, f5, f4 + halfDiffX, f3, paint);
            } else {
                float f6 = (((float) (i * 2)) * halfDiffX) + (f2 - (((float) seriesCount) * halfDiffX));
                canvas.drawRect(f6, f5, f6 + (2.0f * halfDiffX), f3, paint);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void drawChartValuesText(Canvas canvas, XYSeries xYSeries, Paint paint, float[] fArr, int i) {
        int seriesCount = this.mDataset.getSeriesCount();
        float halfDiffX = getHalfDiffX(fArr, fArr.length, seriesCount);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < fArr.length) {
                float f = fArr[i3];
                if (this.mType == BarChart.Type.DEFAULT) {
                    f += (((float) (i * 2)) * halfDiffX) - ((((float) seriesCount) - 1.5f) * halfDiffX);
                }
                float f2 = f;
                drawText(canvas, getLabel(xYSeries.getY((i3 / 2) + 1)), f2, fArr[i3 + 3] - 3.0f, paint, 0.0f);
                drawText(canvas, getLabel(xYSeries.getY(i3 / 2)), f2, fArr[i3 + 1] + 7.5f, paint, 0.0f);
                i2 = i3 + 4;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public float getCoeficient() {
        return 0.5f;
    }
}
