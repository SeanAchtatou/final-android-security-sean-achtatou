package org.achartengine.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.waps.AnimationType;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

public class ScatterChart extends XYChart {
    private static final int SHAPE_WIDTH = 10;
    private static final float SIZE = 3.0f;
    private float size = SIZE;

    public ScatterChart(XYMultipleSeriesDataset xYMultipleSeriesDataset, XYMultipleSeriesRenderer xYMultipleSeriesRenderer) {
        super(xYMultipleSeriesDataset, xYMultipleSeriesRenderer);
        this.size = xYMultipleSeriesRenderer.getPointSize();
    }

    public void drawSeries(Canvas canvas, Paint paint, float[] fArr, SimpleSeriesRenderer simpleSeriesRenderer, float f, int i) {
        XYSeriesRenderer xYSeriesRenderer = (XYSeriesRenderer) simpleSeriesRenderer;
        paint.setColor(xYSeriesRenderer.getColor());
        if (xYSeriesRenderer.isFillPoints()) {
            paint.setStyle(Paint.Style.FILL);
        } else {
            paint.setStyle(Paint.Style.STROKE);
        }
        int length = fArr.length;
        switch (AnonymousClass1.$SwitchMap$org$achartengine$chart$PointStyle[xYSeriesRenderer.getPointStyle().ordinal()]) {
            case 1:
                for (int i2 = 0; i2 < length; i2 += 2) {
                    drawX(canvas, paint, fArr[i2], fArr[i2 + 1]);
                }
                return;
            case 2:
                for (int i3 = 0; i3 < length; i3 += 2) {
                    drawCircle(canvas, paint, fArr[i3], fArr[i3 + 1]);
                }
                return;
            case 3:
                float[] fArr2 = new float[6];
                for (int i4 = 0; i4 < length; i4 += 2) {
                    drawTriangle(canvas, paint, fArr2, fArr[i4], fArr[i4 + 1]);
                }
                return;
            case 4:
                for (int i5 = 0; i5 < length; i5 += 2) {
                    drawSquare(canvas, paint, fArr[i5], fArr[i5 + 1]);
                }
                return;
            case AnimationType.ALPHA:
                float[] fArr3 = new float[8];
                for (int i6 = 0; i6 < length; i6 += 2) {
                    drawDiamond(canvas, paint, fArr3, fArr[i6], fArr[i6 + 1]);
                }
                return;
            case AnimationType.TRANSLATE_FROM_RIGHT:
                canvas.drawPoints(fArr, paint);
                return;
            default:
                return;
        }
    }

    /* renamed from: org.achartengine.chart.ScatterChart$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$achartengine$chart$PointStyle = new int[PointStyle.values().length];

        static {
            try {
                $SwitchMap$org$achartengine$chart$PointStyle[PointStyle.X.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$achartengine$chart$PointStyle[PointStyle.CIRCLE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$achartengine$chart$PointStyle[PointStyle.TRIANGLE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$achartengine$chart$PointStyle[PointStyle.SQUARE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$achartengine$chart$PointStyle[PointStyle.DIAMOND.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$achartengine$chart$PointStyle[PointStyle.POINT.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
        }
    }

    public int getLegendShapeWidth() {
        return SHAPE_WIDTH;
    }

    public void drawLegendShape(Canvas canvas, SimpleSeriesRenderer simpleSeriesRenderer, float f, float f2, Paint paint) {
        if (((XYSeriesRenderer) simpleSeriesRenderer).isFillPoints()) {
            paint.setStyle(Paint.Style.FILL);
        } else {
            paint.setStyle(Paint.Style.STROKE);
        }
        switch (AnonymousClass1.$SwitchMap$org$achartengine$chart$PointStyle[((XYSeriesRenderer) simpleSeriesRenderer).getPointStyle().ordinal()]) {
            case 1:
                drawX(canvas, paint, f + 10.0f, f2);
                return;
            case 2:
                drawCircle(canvas, paint, f + 10.0f, f2);
                return;
            case 3:
                drawTriangle(canvas, paint, new float[6], f + 10.0f, f2);
                return;
            case 4:
                drawSquare(canvas, paint, f + 10.0f, f2);
                return;
            case AnimationType.ALPHA:
                drawDiamond(canvas, paint, new float[8], f + 10.0f, f2);
                return;
            case AnimationType.TRANSLATE_FROM_RIGHT:
                canvas.drawPoint(f + 10.0f, f2, paint);
                return;
            default:
                return;
        }
    }

    private void drawX(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawLine(f - this.size, f2 - this.size, f + this.size, f2 + this.size, paint);
        canvas.drawLine(f + this.size, f2 - this.size, f - this.size, f2 + this.size, paint);
    }

    private void drawCircle(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawCircle(f, f2, this.size, paint);
    }

    private void drawTriangle(Canvas canvas, Paint paint, float[] fArr, float f, float f2) {
        fArr[0] = f;
        fArr[1] = (f2 - this.size) - (this.size / 2.0f);
        fArr[2] = f - this.size;
        fArr[3] = this.size + f2;
        fArr[4] = this.size + f;
        fArr[5] = fArr[3];
        drawPath(canvas, fArr, paint, true);
    }

    private void drawSquare(Canvas canvas, Paint paint, float f, float f2) {
        canvas.drawRect(f - this.size, f2 - this.size, f + this.size, f2 + this.size, paint);
    }

    private void drawDiamond(Canvas canvas, Paint paint, float[] fArr, float f, float f2) {
        fArr[0] = f;
        fArr[1] = f2 - this.size;
        fArr[2] = f - this.size;
        fArr[3] = f2;
        fArr[4] = f;
        fArr[5] = this.size + f2;
        fArr[6] = this.size + f;
        fArr[7] = f2;
        drawPath(canvas, fArr, paint, true);
    }
}
