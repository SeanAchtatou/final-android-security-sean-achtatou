package org.achartengine.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.List;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.util.MathHelper;

public abstract class XYChart extends AbstractChart {
    private double[] calcRange = new double[4];
    private PointF mCenter;
    protected XYMultipleSeriesDataset mDataset;
    protected XYMultipleSeriesRenderer mRenderer;
    private float mScale;
    private float mTranslate;
    private Rect screenR;

    public abstract void drawSeries(Canvas canvas, Paint paint, float[] fArr, SimpleSeriesRenderer simpleSeriesRenderer, float f, int i);

    public XYChart(XYMultipleSeriesDataset xYMultipleSeriesDataset, XYMultipleSeriesRenderer xYMultipleSeriesRenderer) {
        this.mDataset = xYMultipleSeriesDataset;
        this.mRenderer = xYMultipleSeriesRenderer;
    }

    /* JADX WARNING: Removed duplicated region for block: B:104:0x05d9  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x05d7 A[EDGE_INSN: B:146:0x05d7->B:103:0x05d7 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0496  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r54, int r55, int r56, int r57, int r58, android.graphics.Paint r59) {
        /*
            r53 = this;
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            boolean r5 = r5.isAntialiasing()
            r0 = r59
            r1 = r5
            r0.setAntiAlias(r1)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getLegendHeight()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            boolean r6 = r6.isShowLegend()
            if (r6 == 0) goto L_0x07f5
            if (r5 != 0) goto L_0x07f5
            int r5 = r58 / 5
            r31 = r5
        L_0x0029:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int[] r32 = r5.getMargins()
            r5 = 1
            r5 = r32[r5]
            int r33 = r55 + r5
            r5 = 0
            r5 = r32[r5]
            int r34 = r56 + r5
            int r5 = r55 + r57
            r6 = 3
            r6 = r32[r6]
            int r15 = r5 - r6
            int r5 = r56 + r58
            r6 = 2
            r6 = r32[r6]
            int r5 = r5 - r6
            int r16 = r5 - r31
            r0 = r53
            android.graphics.Rect r0 = r0.screenR
            r5 = r0
            if (r5 != 0) goto L_0x005c
            android.graphics.Rect r5 = new android.graphics.Rect
            r5.<init>()
            r0 = r5
            r1 = r53
            r1.screenR = r0
        L_0x005c:
            r0 = r53
            android.graphics.Rect r0 = r0.screenR
            r5 = r0
            r0 = r5
            r1 = r33
            r2 = r34
            r3 = r15
            r4 = r16
            r0.set(r1, r2, r3, r4)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            r13 = 0
            r14 = 0
            r5 = r53
            r7 = r54
            r8 = r55
            r9 = r56
            r10 = r57
            r11 = r58
            r12 = r59
            r5.drawBackground(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            android.graphics.Typeface r5 = r59.getTypeface()
            if (r5 == 0) goto L_0x00b4
            android.graphics.Typeface r5 = r59.getTypeface()
            java.lang.String r5 = r5.toString()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            java.lang.String r6 = r6.getTextTypefaceName()
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x00b4
            android.graphics.Typeface r5 = r59.getTypeface()
            int r5 = r5.getStyle()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            int r6 = r6.getTextTypefaceStyle()
            if (r5 == r6) goto L_0x00d0
        L_0x00b4:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            java.lang.String r5 = r5.getTextTypefaceName()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            int r6 = r6.getTextTypefaceStyle()
            android.graphics.Typeface r5 = android.graphics.Typeface.create(r5, r6)
            r0 = r59
            r1 = r5
            r0.setTypeface(r1)
        L_0x00d0:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r13 = r5.getOrientation()
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r5 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.VERTICAL
            if (r13 != r5) goto L_0x07ef
            int r5 = r15 - r31
            r6 = 20
            int r6 = r31 - r6
            int r6 = r6 + r16
            r18 = r6
            r35 = r5
        L_0x00e9:
            int r36 = r13.getAngle()
            r5 = 90
            r0 = r36
            r1 = r5
            if (r0 != r1) goto L_0x01e6
            r5 = 1
            r37 = r5
        L_0x00f7:
            r0 = r58
            float r0 = (float) r0
            r5 = r0
            r0 = r57
            float r0 = (float) r0
            r6 = r0
            float r5 = r5 / r6
            r0 = r5
            r1 = r53
            r1.mScale = r0
            int r5 = r57 - r58
            int r5 = java.lang.Math.abs(r5)
            int r5 = r5 / 2
            float r5 = (float) r5
            r0 = r5
            r1 = r53
            r1.mTranslate = r0
            r0 = r53
            float r0 = r0.mScale
            r5 = r0
            r6 = 1065353216(0x3f800000, float:1.0)
            int r5 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r5 >= 0) goto L_0x012b
            r0 = r53
            float r0 = r0.mTranslate
            r5 = r0
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            float r5 = r5 * r6
            r0 = r5
            r1 = r53
            r1.mTranslate = r0
        L_0x012b:
            android.graphics.PointF r5 = new android.graphics.PointF
            int r6 = r55 + r57
            int r6 = r6 / 2
            float r6 = (float) r6
            int r7 = r56 + r58
            int r7 = r7 / 2
            float r7 = (float) r7
            r5.<init>(r6, r7)
            r0 = r5
            r1 = r53
            r1.mCenter = r0
            if (r37 == 0) goto L_0x014f
            r0 = r36
            float r0 = (float) r0
            r5 = r0
            r6 = 0
            r0 = r53
            r1 = r54
            r2 = r5
            r3 = r6
            r0.transform(r1, r2, r3)
        L_0x014f:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            double r5 = r5.getXAxisMin()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r7 = r0
            double r7 = r7.getXAxisMax()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r9 = r0
            double r9 = r9.getYAxisMin()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r11 = r0
            double r11 = r11.getYAxisMax()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r14 = r0
            boolean r14 = r14.isMinXSet()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r15 = r0
            boolean r15 = r15.isMaxXSet()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r16 = r0
            boolean r16 = r16.isMinYSet()
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r17 = r0
            boolean r17 = r17.isMaxYSet()
            r19 = 0
            r21 = 0
            r0 = r53
            org.achartengine.model.XYMultipleSeriesDataset r0 = r0.mDataset
            r23 = r0
            int r23 = r23.getSeriesCount()
            r0 = r23
            java.lang.String[] r0 = new java.lang.String[r0]
            r38 = r0
            r24 = 0
            r39 = r11
            r41 = r9
            r43 = r7
            r29 = r5
            r5 = r24
        L_0x01b9:
            r0 = r5
            r1 = r23
            if (r0 >= r1) goto L_0x0259
            r0 = r53
            org.achartengine.model.XYMultipleSeriesDataset r0 = r0.mDataset
            r6 = r0
            org.achartengine.model.XYSeries r6 = r6.getSeriesAt(r5)
            java.lang.String r7 = r6.getTitle()
            r38[r5] = r7
            int r7 = r6.getItemCount()
            if (r7 != 0) goto L_0x01eb
            r6 = r39
            r8 = r41
            r10 = r43
            r24 = r29
        L_0x01db:
            int r5 = r5 + 1
            r39 = r6
            r41 = r8
            r43 = r10
            r29 = r24
            goto L_0x01b9
        L_0x01e6:
            r5 = 0
            r37 = r5
            goto L_0x00f7
        L_0x01eb:
            if (r14 != 0) goto L_0x07eb
            double r7 = r6.getMinX()
            r0 = r29
            r2 = r7
            double r7 = java.lang.Math.min(r0, r2)
            r0 = r53
            double[] r0 = r0.calcRange
            r9 = r0
            r10 = 0
            r9[r10] = r7
        L_0x0200:
            if (r15 != 0) goto L_0x07e7
            double r9 = r6.getMaxX()
            r0 = r43
            r2 = r9
            double r9 = java.lang.Math.max(r0, r2)
            r0 = r53
            double[] r0 = r0.calcRange
            r11 = r0
            r12 = 1
            r11[r12] = r9
        L_0x0215:
            if (r16 != 0) goto L_0x07e3
            double r11 = r6.getMinY()
            float r11 = (float) r11
            double r11 = (double) r11
            r0 = r41
            r2 = r11
            double r11 = java.lang.Math.min(r0, r2)
            r0 = r53
            double[] r0 = r0.calcRange
            r24 = r0
            r25 = 2
            r24[r25] = r11
        L_0x022e:
            if (r17 != 0) goto L_0x07d8
            double r24 = r6.getMaxY()
            r0 = r24
            float r0 = (float) r0
            r6 = r0
            r0 = r6
            double r0 = (double) r0
            r24 = r0
            r0 = r39
            r2 = r24
            double r24 = java.lang.Math.max(r0, r2)
            r0 = r53
            double[] r0 = r0.calcRange
            r6 = r0
            r26 = 3
            r6[r26] = r24
            r49 = r24
            r24 = r7
            r6 = r49
            r51 = r9
            r8 = r11
            r10 = r51
            goto L_0x01db
        L_0x0259:
            double r5 = r43 - r29
            r7 = 0
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 == 0) goto L_0x07d4
            int r5 = r35 - r33
            double r5 = (double) r5
            double r7 = r43 - r29
            double r5 = r5 / r7
            r45 = r5
        L_0x0269:
            double r5 = r39 - r41
            r7 = 0
            int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r5 == 0) goto L_0x07d0
            int r5 = r18 - r34
            double r5 = (double) r5
            double r7 = r39 - r41
            double r5 = r5 / r7
            float r5 = (float) r5
            double r5 = (double) r5
            r47 = r5
        L_0x027b:
            r5 = 0
            r6 = 0
            r12 = r6
        L_0x027e:
            r0 = r12
            r1 = r23
            if (r0 >= r1) goto L_0x033a
            r0 = r53
            org.achartengine.model.XYMultipleSeriesDataset r0 = r0.mDataset
            r6 = r0
            org.achartengine.model.XYSeries r6 = r6.getSeriesAt(r12)
            int r7 = r6.getItemCount()
            if (r7 != 0) goto L_0x0296
        L_0x0292:
            int r6 = r12 + 1
            r12 = r6
            goto L_0x027e
        L_0x0296:
            r14 = 1
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            org.achartengine.renderer.SimpleSeriesRenderer r10 = r5.getSeriesRendererAt(r12)
            int r5 = r6.getItemCount()
            int r15 = r5 * 2
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            r5 = 0
            r16 = r5
        L_0x02ae:
            r0 = r16
            r1 = r15
            if (r0 >= r1) goto L_0x0318
            int r5 = r16 / 2
            double r7 = r6.getY(r5)
            r19 = 9218868437227405311(0x7fefffffffffffff, double:1.7976931348623157E308)
            int r11 = (r7 > r19 ? 1 : (r7 == r19 ? 0 : -1))
            if (r11 == 0) goto L_0x02f4
            r0 = r33
            double r0 = (double) r0
            r19 = r0
            double r21 = r6.getX(r5)
            double r21 = r21 - r29
            double r21 = r21 * r45
            double r19 = r19 + r21
            r0 = r19
            float r0 = (float) r0
            r5 = r0
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
            r9.add(r5)
            r0 = r18
            double r0 = (double) r0
            r19 = r0
            double r7 = r7 - r41
            double r7 = r7 * r47
            double r7 = r19 - r7
            float r5 = (float) r7
            java.lang.Float r5 = java.lang.Float.valueOf(r5)
            r9.add(r5)
        L_0x02ef:
            int r5 = r16 + 2
            r16 = r5
            goto L_0x02ae
        L_0x02f4:
            int r5 = r9.size()
            if (r5 <= 0) goto L_0x02ef
            r0 = r18
            float r0 = (float) r0
            r5 = r0
            r0 = r18
            double r0 = (double) r0
            r7 = r0
            double r19 = r47 * r41
            double r7 = r7 + r19
            float r7 = (float) r7
            float r11 = java.lang.Math.min(r5, r7)
            r5 = r53
            r7 = r54
            r8 = r59
            r5.drawSeries(r6, r7, r8, r9, r10, r11, r12, r13)
            r9.clear()
            goto L_0x02ef
        L_0x0318:
            int r5 = r9.size()
            if (r5 <= 0) goto L_0x0337
            r0 = r18
            float r0 = (float) r0
            r5 = r0
            r0 = r18
            double r0 = (double) r0
            r7 = r0
            double r15 = r47 * r41
            double r7 = r7 + r15
            float r7 = (float) r7
            float r11 = java.lang.Math.min(r5, r7)
            r5 = r53
            r7 = r54
            r8 = r59
            r5.drawSeries(r6, r7, r8, r9, r10, r11, r12, r13)
        L_0x0337:
            r5 = r14
            goto L_0x0292
        L_0x033a:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r15 = r0
            int r20 = r58 - r18
            r22 = 1
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            int r23 = r6.getMarginsColor()
            r14 = r53
            r16 = r54
            r17 = r55
            r19 = r57
            r21 = r59
            r14.drawBackground(r15, r16, r17, r18, r19, r20, r21, r22, r23)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r20 = r0
            r6 = 0
            r25 = r32[r6]
            r27 = 1
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            int r28 = r6.getMarginsColor()
            r19 = r53
            r21 = r54
            r22 = r55
            r23 = r56
            r24 = r57
            r26 = r59
            r19.drawBackground(r20, r21, r22, r23, r24, r25, r26, r27, r28)
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r6 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.HORIZONTAL
            if (r13 != r6) goto L_0x051e
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r20 = r0
            int r24 = r33 - r55
            int r25 = r58 - r56
            r27 = 1
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            int r28 = r6.getMarginsColor()
            r19 = r53
            r21 = r54
            r22 = r55
            r23 = r56
            r26 = r59
            r19.drawBackground(r20, r21, r22, r23, r24, r25, r26, r27, r28)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r20 = r0
            r6 = 3
            r24 = r32[r6]
            int r25 = r58 - r56
            r27 = 1
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            int r28 = r6.getMarginsColor()
            r19 = r53
            r21 = r54
            r22 = r35
            r23 = r56
            r26 = r59
            r19.drawBackground(r20, r21, r22, r23, r24, r25, r26, r27, r28)
        L_0x03c5:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            boolean r6 = r6.isShowLabels()
            if (r6 == 0) goto L_0x0568
            if (r5 == 0) goto L_0x0568
            r5 = 1
            r11 = r5
        L_0x03d4:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            boolean r12 = r5.isShowGrid()
            if (r11 != 0) goto L_0x03e1
            if (r12 == 0) goto L_0x0686
        L_0x03e1:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getXLabels()
            r0 = r29
            r2 = r43
            r4 = r5
            java.util.List r5 = org.achartengine.util.MathHelper.getLabels(r0, r2, r4)
            r0 = r53
            r1 = r5
            java.util.List r20 = r0.getValidLabels(r1)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getYLabels()
            r0 = r41
            r2 = r39
            r4 = r5
            java.util.List r5 = org.achartengine.util.MathHelper.getLabels(r0, r2, r4)
            r0 = r53
            r1 = r5
            java.util.List r14 = r0.getValidLabels(r1)
            if (r11 == 0) goto L_0x07cc
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getLabelsColor()
            r0 = r59
            r1 = r5
            r0.setColor(r1)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            float r5 = r5.getLabelsTextSize()
            r0 = r59
            r1 = r5
            r0.setTextSize(r1)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            android.graphics.Paint$Align r5 = r5.getXLabelsAlign()
            r0 = r59
            r1 = r5
            r0.setTextAlign(r1)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            android.graphics.Paint$Align r5 = r5.getXLabelsAlign()
            android.graphics.Paint$Align r6 = android.graphics.Paint.Align.LEFT
            if (r5 != r6) goto L_0x07cc
            r0 = r33
            float r0 = (float) r0
            r5 = r0
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            float r6 = r6.getLabelsTextSize()
            r7 = 1082130432(0x40800000, float:4.0)
            float r6 = r6 / r7
            float r5 = r5 + r6
            int r5 = (int) r5
            r24 = r5
        L_0x0463:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            java.lang.Double[] r21 = r5.getXTextLabelLocations()
            r19 = r53
            r22 = r54
            r23 = r59
            r25 = r34
            r26 = r18
            r27 = r45
            r19.drawXLabels(r20, r21, r22, r23, r24, r25, r26, r27, r29)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            android.graphics.Paint$Align r5 = r5.getYLabelsAlign()
            r0 = r59
            r1 = r5
            r0.setTextAlign(r1)
            int r15 = r14.size()
            r5 = 0
            r16 = r5
        L_0x0491:
            r0 = r16
            r1 = r15
            if (r0 >= r1) goto L_0x05d7
            r0 = r14
            r1 = r16
            java.lang.Object r5 = r0.get(r1)
            java.lang.Double r5 = (java.lang.Double) r5
            double r19 = r5.doubleValue()
            r0 = r18
            double r0 = (double) r0
            r5 = r0
            double r7 = r19 - r41
            double r7 = r7 * r47
            double r5 = r5 - r7
            float r7 = (float) r5
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r5 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.HORIZONTAL
            if (r13 != r5) goto L_0x056c
            if (r11 == 0) goto L_0x04f7
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getLabelsColor()
            r0 = r59
            r1 = r5
            r0.setColor(r1)
            r5 = 4
            int r5 = r33 - r5
            float r6 = (float) r5
            r0 = r33
            float r0 = (float) r0
            r8 = r0
            r5 = r54
            r9 = r7
            r10 = r59
            r5.drawLine(r6, r7, r8, r9, r10)
            r0 = r53
            r1 = r19
            java.lang.String r21 = r0.getLabel(r1)
            r5 = 2
            int r5 = r33 - r5
            r0 = r5
            float r0 = (float) r0
            r22 = r0
            r5 = 1073741824(0x40000000, float:2.0)
            float r23 = r7 - r5
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            float r25 = r5.getYLabelsAngle()
            r19 = r53
            r20 = r54
            r24 = r59
            r19.drawText(r20, r21, r22, r23, r24, r25)
        L_0x04f7:
            if (r12 == 0) goto L_0x0518
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getGridColor()
            r0 = r59
            r1 = r5
            r0.setColor(r1)
            r0 = r33
            float r0 = (float) r0
            r6 = r0
            r0 = r35
            float r0 = (float) r0
            r8 = r0
            r5 = r54
            r9 = r7
            r10 = r59
            r5.drawLine(r6, r7, r8, r9, r10)
        L_0x0518:
            int r5 = r16 + 1
            r16 = r5
            goto L_0x0491
        L_0x051e:
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r6 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.VERTICAL
            if (r13 != r6) goto L_0x03c5
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r20 = r0
            int r24 = r57 - r35
            int r25 = r58 - r56
            r27 = 1
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            int r28 = r6.getMarginsColor()
            r19 = r53
            r21 = r54
            r22 = r35
            r23 = r56
            r26 = r59
            r19.drawBackground(r20, r21, r22, r23, r24, r25, r26, r27, r28)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r20 = r0
            int r24 = r33 - r55
            int r25 = r58 - r56
            r27 = 1
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            int r28 = r6.getMarginsColor()
            r19 = r53
            r21 = r54
            r22 = r55
            r23 = r56
            r26 = r59
            r19.drawBackground(r20, r21, r22, r23, r24, r25, r26, r27, r28)
            goto L_0x03c5
        L_0x0568:
            r5 = 0
            r11 = r5
            goto L_0x03d4
        L_0x056c:
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r5 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.VERTICAL
            if (r13 != r5) goto L_0x0518
            if (r11 == 0) goto L_0x05b4
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getLabelsColor()
            r0 = r59
            r1 = r5
            r0.setColor(r1)
            int r5 = r35 + 4
            float r6 = (float) r5
            r0 = r35
            float r0 = (float) r0
            r8 = r0
            r5 = r54
            r9 = r7
            r10 = r59
            r5.drawLine(r6, r7, r8, r9, r10)
            r0 = r53
            r1 = r19
            java.lang.String r21 = r0.getLabel(r1)
            int r5 = r35 + 10
            r0 = r5
            float r0 = (float) r0
            r22 = r0
            r5 = 1073741824(0x40000000, float:2.0)
            float r23 = r7 - r5
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            float r25 = r5.getYLabelsAngle()
            r19 = r53
            r20 = r54
            r24 = r59
            r19.drawText(r20, r21, r22, r23, r24, r25)
        L_0x05b4:
            if (r12 == 0) goto L_0x0518
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getGridColor()
            r0 = r59
            r1 = r5
            r0.setColor(r1)
            r0 = r35
            float r0 = (float) r0
            r6 = r0
            r0 = r33
            float r0 = (float) r0
            r8 = r0
            r5 = r54
            r9 = r7
            r10 = r59
            r5.drawLine(r6, r7, r8, r9, r10)
            goto L_0x0518
        L_0x05d7:
            if (r11 == 0) goto L_0x0686
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getLabelsColor()
            r0 = r59
            r1 = r5
            r0.setColor(r1)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            float r12 = r5.getAxisTitleTextSize()
            r0 = r59
            r1 = r12
            r0.setTextSize(r1)
            android.graphics.Paint$Align r5 = android.graphics.Paint.Align.CENTER
            r0 = r59
            r1 = r5
            r0.setTextAlign(r1)
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r5 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.HORIZONTAL
            if (r13 != r5) goto L_0x0704
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            java.lang.String r7 = r5.getXTitle()
            int r5 = r57 / 2
            int r5 = r5 + r55
            float r8 = (float) r5
            r0 = r18
            float r0 = (float) r0
            r5 = r0
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            float r6 = r6.getLabelsTextSize()
            r9 = 1082130432(0x40800000, float:4.0)
            float r6 = r6 * r9
            r9 = 1077936128(0x40400000, float:3.0)
            float r6 = r6 / r9
            float r5 = r5 + r6
            float r9 = r5 + r12
            r11 = 0
            r5 = r53
            r6 = r54
            r10 = r59
            r5.drawText(r6, r7, r8, r9, r10, r11)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            java.lang.String r7 = r5.getYTitle()
            r0 = r55
            float r0 = (float) r0
            r5 = r0
            float r8 = r5 + r12
            int r5 = r58 / 2
            int r5 = r5 + r56
            float r9 = (float) r5
            r11 = -1028390912(0xffffffffc2b40000, float:-90.0)
            r5 = r53
            r6 = r54
            r10 = r59
            r5.drawText(r6, r7, r8, r9, r10, r11)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            float r5 = r5.getChartTitleTextSize()
            r0 = r59
            r1 = r5
            r0.setTextSize(r1)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            java.lang.String r7 = r5.getChartTitle()
            int r5 = r57 / 2
            int r5 = r5 + r55
            float r8 = (float) r5
            r0 = r56
            float r0 = (float) r0
            r5 = r0
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r6 = r0
            float r6 = r6.getChartTitleTextSize()
            float r9 = r5 + r6
            r11 = 0
            r5 = r53
            r6 = r54
            r10 = r59
            r5.drawText(r6, r7, r8, r9, r10, r11)
        L_0x0686:
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r5 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.HORIZONTAL
            if (r13 != r5) goto L_0x0770
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r21 = r0
            r19 = r53
            r20 = r54
            r22 = r38
            r23 = r33
            r24 = r35
            r25 = r56
            r26 = r57
            r27 = r58
            r28 = r31
            r29 = r59
            r19.drawLegend(r20, r21, r22, r23, r24, r25, r26, r27, r28, r29)
        L_0x06a7:
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            boolean r5 = r5.isShowAxes()
            if (r5 == 0) goto L_0x06f3
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            int r5 = r5.getAxesColor()
            r0 = r59
            r1 = r5
            r0.setColor(r1)
            r0 = r33
            float r0 = (float) r0
            r6 = r0
            r0 = r18
            float r0 = (float) r0
            r7 = r0
            r0 = r35
            float r0 = (float) r0
            r8 = r0
            r0 = r18
            float r0 = (float) r0
            r9 = r0
            r5 = r54
            r10 = r59
            r5.drawLine(r6, r7, r8, r9, r10)
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r5 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.HORIZONTAL
            if (r13 != r5) goto L_0x07af
            r0 = r33
            float r0 = (float) r0
            r6 = r0
            r0 = r34
            float r0 = (float) r0
            r7 = r0
            r0 = r33
            float r0 = (float) r0
            r8 = r0
            r0 = r18
            float r0 = (float) r0
            r9 = r0
            r5 = r54
            r10 = r59
            r5.drawLine(r6, r7, r8, r9, r10)
        L_0x06f3:
            if (r37 == 0) goto L_0x0703
            r0 = r36
            float r0 = (float) r0
            r5 = r0
            r6 = 1
            r0 = r53
            r1 = r54
            r2 = r5
            r3 = r6
            r0.transform(r1, r2, r3)
        L_0x0703:
            return
        L_0x0704:
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r5 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.VERTICAL
            if (r13 != r5) goto L_0x0686
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            java.lang.String r7 = r5.getXTitle()
            int r5 = r57 / 2
            int r5 = r5 + r55
            float r8 = (float) r5
            int r5 = r56 + r58
            float r5 = (float) r5
            float r9 = r5 - r12
            r11 = -1028390912(0xffffffffc2b40000, float:-90.0)
            r5 = r53
            r6 = r54
            r10 = r59
            r5.drawText(r6, r7, r8, r9, r10, r11)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            java.lang.String r7 = r5.getYTitle()
            int r5 = r35 + 20
            float r8 = (float) r5
            int r5 = r58 / 2
            int r5 = r5 + r56
            float r9 = (float) r5
            r11 = 0
            r5 = r53
            r6 = r54
            r10 = r59
            r5.drawText(r6, r7, r8, r9, r10, r11)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            float r5 = r5.getChartTitleTextSize()
            r0 = r59
            r1 = r5
            r0.setTextSize(r1)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r5 = r0
            java.lang.String r7 = r5.getChartTitle()
            r0 = r55
            float r0 = (float) r0
            r5 = r0
            float r8 = r5 + r12
            int r5 = r58 / 2
            int r5 = r5 + r34
            float r9 = (float) r5
            r11 = 0
            r5 = r53
            r6 = r54
            r10 = r59
            r5.drawText(r6, r7, r8, r9, r10, r11)
            goto L_0x0686
        L_0x0770:
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r5 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.VERTICAL
            if (r13 != r5) goto L_0x06a7
            r0 = r36
            float r0 = (float) r0
            r5 = r0
            r6 = 1
            r0 = r53
            r1 = r54
            r2 = r5
            r3 = r6
            r0.transform(r1, r2, r3)
            r0 = r53
            org.achartengine.renderer.XYMultipleSeriesRenderer r0 = r0.mRenderer
            r21 = r0
            r19 = r53
            r20 = r54
            r22 = r38
            r23 = r33
            r24 = r35
            r25 = r56
            r26 = r57
            r27 = r58
            r28 = r31
            r29 = r59
            r19.drawLegend(r20, r21, r22, r23, r24, r25, r26, r27, r28, r29)
            r0 = r36
            float r0 = (float) r0
            r5 = r0
            r6 = 0
            r0 = r53
            r1 = r54
            r2 = r5
            r3 = r6
            r0.transform(r1, r2, r3)
            goto L_0x06a7
        L_0x07af:
            org.achartengine.renderer.XYMultipleSeriesRenderer$Orientation r5 = org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation.VERTICAL
            if (r13 != r5) goto L_0x06f3
            r0 = r35
            float r0 = (float) r0
            r6 = r0
            r0 = r34
            float r0 = (float) r0
            r7 = r0
            r0 = r35
            float r0 = (float) r0
            r8 = r0
            r0 = r18
            float r0 = (float) r0
            r9 = r0
            r5 = r54
            r10 = r59
            r5.drawLine(r6, r7, r8, r9, r10)
            goto L_0x06f3
        L_0x07cc:
            r24 = r33
            goto L_0x0463
        L_0x07d0:
            r47 = r21
            goto L_0x027b
        L_0x07d4:
            r45 = r19
            goto L_0x0269
        L_0x07d8:
            r24 = r7
            r6 = r39
            r49 = r9
            r8 = r11
            r10 = r49
            goto L_0x01db
        L_0x07e3:
            r11 = r41
            goto L_0x022e
        L_0x07e7:
            r9 = r43
            goto L_0x0215
        L_0x07eb:
            r7 = r29
            goto L_0x0200
        L_0x07ef:
            r18 = r16
            r35 = r15
            goto L_0x00e9
        L_0x07f5:
            r31 = r5
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: org.achartengine.chart.XYChart.draw(android.graphics.Canvas, int, int, int, int, android.graphics.Paint):void");
    }

    private List<Double> getValidLabels(List<Double> list) {
        ArrayList arrayList = new ArrayList(list);
        for (Double next : list) {
            if (next.isNaN()) {
                arrayList.remove(next);
            }
        }
        return arrayList;
    }

    private void drawSeries(XYSeries xYSeries, Canvas canvas, Paint paint, List<Float> list, SimpleSeriesRenderer simpleSeriesRenderer, float f, int i, XYMultipleSeriesRenderer.Orientation orientation) {
        ScatterChart pointsChart;
        float[] floats = MathHelper.getFloats(list);
        drawSeries(canvas, paint, floats, simpleSeriesRenderer, f, i);
        if (isRenderPoints(simpleSeriesRenderer) && (pointsChart = getPointsChart()) != null) {
            pointsChart.drawSeries(canvas, paint, floats, simpleSeriesRenderer, 0.0f, i);
        }
        paint.setTextSize(this.mRenderer.getChartValuesTextSize());
        if (orientation == XYMultipleSeriesRenderer.Orientation.HORIZONTAL) {
            paint.setTextAlign(Paint.Align.CENTER);
        } else {
            paint.setTextAlign(Paint.Align.LEFT);
        }
        if (this.mRenderer.isDisplayChartValues()) {
            drawChartValuesText(canvas, xYSeries, paint, floats, i);
        }
    }

    /* access modifiers changed from: protected */
    public void drawChartValuesText(Canvas canvas, XYSeries xYSeries, Paint paint, float[] fArr, int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < fArr.length) {
                drawText(canvas, getLabel(xYSeries.getY(i3 / 2)), fArr[i3], fArr[i3 + 1] - 3.5f, paint, 0.0f);
                i2 = i3 + 2;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void drawText(Canvas canvas, String str, float f, float f2, Paint paint, float f3) {
        float f4 = ((float) (-this.mRenderer.getOrientation().getAngle())) + f3;
        if (f4 != 0.0f) {
            canvas.rotate(f4, f, f2);
        }
        canvas.drawText(str, f, f2, paint);
        if (f4 != 0.0f) {
            canvas.rotate(-f4, f, f2);
        }
    }

    private void transform(Canvas canvas, float f, boolean z) {
        if (z) {
            canvas.scale(1.0f / this.mScale, this.mScale);
            canvas.translate(this.mTranslate, -this.mTranslate);
            canvas.rotate(-f, this.mCenter.x, this.mCenter.y);
            return;
        }
        canvas.rotate(f, this.mCenter.x, this.mCenter.y);
        canvas.translate(-this.mTranslate, this.mTranslate);
        canvas.scale(this.mScale, 1.0f / this.mScale);
    }

    /* access modifiers changed from: protected */
    public String getLabel(double d) {
        if (d == ((double) Math.round(d))) {
            return Math.round(d) + "";
        }
        return d + "";
    }

    /* access modifiers changed from: protected */
    public void drawXLabels(List<Double> list, Double[] dArr, Canvas canvas, Paint paint, int i, int i2, int i3, double d, double d2) {
        int size = list.size();
        boolean isShowLabels = this.mRenderer.isShowLabels();
        boolean isShowGrid = this.mRenderer.isShowGrid();
        for (int i4 = 0; i4 < size; i4++) {
            double doubleValue = list.get(i4).doubleValue();
            float f = (float) (((double) i) + ((doubleValue - d2) * d));
            if (isShowLabels) {
                paint.setColor(this.mRenderer.getLabelsColor());
                canvas.drawLine(f, (float) i3, f, ((float) i3) + (this.mRenderer.getLabelsTextSize() / 3.0f), paint);
                drawText(canvas, getLabel(doubleValue), f, ((float) i3) + ((this.mRenderer.getLabelsTextSize() * 4.0f) / 3.0f), paint, this.mRenderer.getXLabelsAngle());
            }
            if (isShowGrid) {
                paint.setColor(this.mRenderer.getGridColor());
                canvas.drawLine(f, (float) i3, f, (float) i2, paint);
            }
        }
        if (isShowLabels) {
            paint.setColor(this.mRenderer.getLabelsColor());
            for (Double d3 : dArr) {
                float doubleValue2 = (float) (((double) i) + ((d3.doubleValue() - d2) * d));
                canvas.drawLine(doubleValue2, (float) i3, doubleValue2, (float) (i3 + 4), paint);
                drawText(canvas, this.mRenderer.getXTextLabel(d3), doubleValue2, ((float) i3) + this.mRenderer.getLabelsTextSize(), paint, this.mRenderer.getXLabelsAngle());
            }
        }
    }

    public XYMultipleSeriesRenderer getRenderer() {
        return this.mRenderer;
    }

    public XYMultipleSeriesDataset getDataset() {
        return this.mDataset;
    }

    public double[] getCalcRange() {
        return this.calcRange;
    }

    public PointF toRealPoint(float f, float f2) {
        double xAxisMin = this.mRenderer.getXAxisMin();
        double xAxisMax = this.mRenderer.getXAxisMax();
        double yAxisMin = this.mRenderer.getYAxisMin();
        return new PointF((float) (xAxisMin + (((xAxisMax - xAxisMin) * ((double) (f - ((float) this.screenR.left)))) / ((double) this.screenR.width()))), (float) (((((double) (((float) (this.screenR.top + this.screenR.height())) - f2)) * (this.mRenderer.getYAxisMax() - yAxisMin)) / ((double) this.screenR.height())) + yAxisMin));
    }

    public PointF toScreenPoint(PointF pointF) {
        double xAxisMin = this.mRenderer.getXAxisMin();
        double xAxisMax = this.mRenderer.getXAxisMax();
        double yAxisMin = this.mRenderer.getYAxisMin();
        double yAxisMax = this.mRenderer.getYAxisMax();
        return new PointF((float) ((((((double) pointF.x) - xAxisMin) * ((double) this.screenR.width())) / (xAxisMax - xAxisMin)) + ((double) this.screenR.left)), (float) ((((yAxisMax - ((double) pointF.y)) * ((double) this.screenR.height())) / (yAxisMax - yAxisMin)) + ((double) this.screenR.top)));
    }

    public boolean isRenderPoints(SimpleSeriesRenderer simpleSeriesRenderer) {
        return false;
    }

    public double getDefaultMinimum() {
        return Double.MAX_VALUE;
    }

    public ScatterChart getPointsChart() {
        return null;
    }
}
