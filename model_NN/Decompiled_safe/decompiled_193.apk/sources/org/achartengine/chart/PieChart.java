package org.achartengine.chart;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

public class PieChart extends AbstractChart {
    private static final int SHAPE_WIDTH = 10;
    private CategorySeries mDataset;
    private DefaultRenderer mRenderer;

    public PieChart(CategorySeries categorySeries, DefaultRenderer defaultRenderer) {
        this.mDataset = categorySeries;
        this.mRenderer = defaultRenderer;
    }

    public void draw(Canvas canvas, int i, int i2, int i3, int i4, Paint paint) {
        int i5;
        float f;
        float f2;
        float f3;
        int i6;
        int i7;
        float f4;
        int i8;
        paint.setAntiAlias(this.mRenderer.isAntialiasing());
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(this.mRenderer.getLabelsTextSize());
        int legendHeight = this.mRenderer.getLegendHeight();
        if (!this.mRenderer.isShowLegend() || legendHeight != 0) {
            i5 = legendHeight;
        } else {
            i5 = i4 / 5;
        }
        int i9 = i + 15;
        int i10 = i2 + 5;
        int i11 = (i + i3) - 5;
        int i12 = (i2 + i4) - i5;
        drawBackground(this.mRenderer, canvas, i, i2, i3, i4, paint, false, 0);
        int itemCount = this.mDataset.getItemCount();
        String[] strArr = new String[itemCount];
        double d = 0.0d;
        int i13 = 0;
        while (i13 < itemCount) {
            strArr[i13] = this.mDataset.getCategory(i13);
            i13++;
            d = this.mDataset.getValue(i13) + d;
        }
        int min = (int) (((double) Math.min(Math.abs(i11 - i9), Math.abs(i12 - i10))) * 0.35d);
        int i14 = (i9 + i11) / 2;
        int i15 = (i12 + i10) / 2;
        float f5 = ((float) min) * 0.9f;
        float f6 = ((float) min) * 1.1f;
        RectF rectF = new RectF((float) (i14 - min), (float) (i15 - min), (float) (i14 + min), (float) (min + i15));
        int i16 = 0;
        float f7 = 1.0f;
        float f8 = 0.0f;
        float f9 = 0.0f;
        float f10 = 0.0f;
        while (i16 < itemCount) {
            paint.setColor(this.mRenderer.getSeriesRendererAt(i16).getColor());
            float value = (float) ((((double) ((float) this.mDataset.getValue(i16))) / d) * 360.0d);
            canvas.drawArc(rectF, f10, value, true, paint);
            if (this.mRenderer.isShowLabels()) {
                paint.setColor(this.mRenderer.getLabelsColor());
                double radians = Math.toRadians((double) (90.0f - ((value / 2.0f) + f10)));
                double sin = Math.sin(radians);
                double cos = Math.cos(radians);
                int round = Math.round(((float) i14) + ((float) (((double) f5) * sin)));
                int round2 = Math.round(((float) i15) + ((float) (((double) f5) * cos)));
                int round3 = Math.round(((float) i14) + ((float) (((double) f6) * sin)));
                int round4 = Math.round(((float) i15) + ((float) (((double) f6) * cos)));
                if (Math.sqrt((double) (((((float) round4) - f8) * (((float) round4) - f8)) + ((((float) round3) - f9) * (((float) round3) - f9)))) <= ((double) 20.0f)) {
                    float f11 = (float) (((double) f7) * 1.1d);
                    int round5 = Math.round(((float) i14) + ((float) (sin * ((double) (f6 * f11)))));
                    i6 = Math.round(((float) (cos * ((double) (f6 * f11)))) + ((float) i15));
                    i7 = round5;
                    f4 = f11;
                } else {
                    i6 = round4;
                    i7 = round3;
                    f4 = 1.0f;
                }
                canvas.drawLine((float) round, (float) round2, (float) i7, (float) i6, paint);
                paint.setTextAlign(Paint.Align.LEFT);
                if (round > i7) {
                    i8 = -SHAPE_WIDTH;
                    paint.setTextAlign(Paint.Align.RIGHT);
                } else {
                    i8 = SHAPE_WIDTH;
                }
                canvas.drawLine((float) i7, (float) i6, (float) (i7 + i8), (float) i6, paint);
                canvas.drawText(this.mDataset.getCategory(i16), (float) (i8 + i7), (float) (i6 + 5), paint);
                f2 = (float) i6;
                f3 = (float) i7;
                f = f4;
            } else {
                f = f7;
                f2 = f8;
                f3 = f9;
            }
            f10 += value;
            i16++;
            f7 = f;
            f8 = f2;
            f9 = f3;
        }
        drawLegend(canvas, this.mRenderer, strArr, i9, i11, i2, i3, i4, i5, paint);
    }

    public int getLegendShapeWidth() {
        return SHAPE_WIDTH;
    }

    public void drawLegendShape(Canvas canvas, SimpleSeriesRenderer simpleSeriesRenderer, float f, float f2, Paint paint) {
        canvas.drawRect(f, f2 - 5.0f, f + 10.0f, f2 + 5.0f, paint);
    }
}
