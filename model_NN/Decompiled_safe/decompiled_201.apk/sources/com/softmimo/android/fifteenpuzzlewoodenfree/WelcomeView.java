package com.softmimo.android.fifteenpuzzlewoodenfree;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.ImageView;
import com.softmimo.android.fifteenpuzzlewoodenlibrary.FifteenPuzzleView;
import com.softmimo.android.fifteenpuzzlewoodenlibrary.ListView;

public class WelcomeView extends Activity {
    ImageView a;
    ImageView b;
    int c = 255;
    int d = 0;
    boolean e = true;
    private Handler f = new Handler();

    private void b() {
        if (this.e) {
            this.a = (ImageView) findViewById(R.id.logo);
            this.a.setAlpha(this.c);
            this.b = (ImageView) findViewById(R.id.logofrank);
            this.b.setAlpha(this.c);
            new Thread(new a(this)).start();
            this.f = new b(this);
            return;
        }
        startActivity(new Intent(this, ListView.class));
        finish();
    }

    public void a() {
        this.c -= 5;
        if (this.c <= 0) {
            this.d = 2;
            startActivity(new Intent(this, ListView.class));
            finish();
        }
        this.f.sendMessage(this.f.obtainMessage());
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        FifteenPuzzleView.H = true;
        this.e = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("checkbox_splashscreen", true);
        setContentView((int) R.layout.welcome);
        b();
    }
}
