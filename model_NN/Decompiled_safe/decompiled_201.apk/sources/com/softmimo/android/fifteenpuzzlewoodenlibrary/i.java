package com.softmimo.android.fifteenpuzzlewoodenlibrary;

import android.content.SharedPreferences;
import android.view.View;
import com.softmimo.android.fifteenpuzzlewoodenfree.R;

class i implements View.OnClickListener {
    final /* synthetic */ FifteenPuzzlePreferences a;

    i(FifteenPuzzlePreferences fifteenPuzzlePreferences) {
        this.a = fifteenPuzzlePreferences;
    }

    public void onClick(View view) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("FifteenPuzzlePrefsFile", 0).edit();
        edit.clear();
        edit.commit();
        FifteenPuzzlePreferences.a(view.getContext(), "Game records are removed.", R.drawable.icon_wooden);
    }
}
