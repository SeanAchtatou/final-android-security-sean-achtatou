package com.softmimo.android.fifteenpuzzlewoodenlibrary;

import android.os.SystemClock;

class j implements Runnable {
    final /* synthetic */ FifteenPuzzleView a;

    j(FifteenPuzzleView fifteenPuzzleView) {
        this.a = fifteenPuzzleView;
    }

    public void run() {
        this.a.X = ((int) (SystemClock.uptimeMillis() - this.a.W)) / 1000;
        int b = this.a.X;
        int i = b / 60;
        int i2 = b % 60;
        if (i2 < 10) {
            this.a.S.setText("Current Steps: " + Integer.toString(this.a.G) + "    Time Used: " + i + ":0" + i2 + FifteenPuzzleView.M);
        } else {
            this.a.S.setText("Current Steps: " + Integer.toString(this.a.G) + "    Time Used: " + i + ":" + i2 + FifteenPuzzleView.M);
        }
        this.a.V.postAtTime(this, ((long) ((i2 + (i * 60) + 1) * 1000)) + this.a.W);
    }
}
