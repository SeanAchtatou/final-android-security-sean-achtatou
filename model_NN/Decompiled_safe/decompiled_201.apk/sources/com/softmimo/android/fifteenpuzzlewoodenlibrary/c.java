package com.softmimo.android.fifteenpuzzlewoodenlibrary;

class c {
    int[] a = new int[this.b];
    int b;
    final /* synthetic */ a c;

    c(a aVar, int i, int i2) {
        this.c = aVar;
        this.b = (i2 - i) + 1;
        int i3 = i;
        for (int i4 = 0; i4 < this.b; i4++) {
            this.a[i4] = i3;
            i3++;
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        if (this.b == 0) {
            return -1;
        }
        if (this.b == 1) {
            this.b = 0;
            return this.a[0];
        }
        int random = (int) (Math.random() * ((double) this.b));
        int i = this.a[random];
        this.b--;
        while (random < this.b) {
            this.a[random] = this.a[random + 1];
            random++;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public int a(int i) {
        boolean z;
        int i2 = -1;
        int i3 = 0;
        while (true) {
            if (i3 >= this.b) {
                z = false;
                break;
            } else if (this.a[i3] == i) {
                z = true;
                break;
            } else {
                i3++;
            }
        }
        if (z) {
            i2 = this.a[i3];
            if (this.b == 1) {
                this.b = 0;
            } else {
                this.b--;
                while (i3 < this.b) {
                    this.a[i3] = this.a[i3 + 1];
                    i3++;
                }
            }
        }
        return i2;
    }
}
