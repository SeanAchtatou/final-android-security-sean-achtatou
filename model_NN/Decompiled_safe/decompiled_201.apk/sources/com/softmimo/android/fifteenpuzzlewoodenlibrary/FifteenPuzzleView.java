package com.softmimo.android.fifteenpuzzlewoodenlibrary;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import com.softmimo.android.fifteenpuzzlewoodenfree.R;

public class FifteenPuzzleView extends View {
    public static int A = R.drawable.number12;
    public static int B = R.drawable.number13;
    public static int C = R.drawable.number14;
    public static int D = R.drawable.number15;
    public static int E = R.drawable.number16;
    public static int F;
    public static boolean H = false;
    public static boolean I = false;
    public static int J = 480;
    public static int K = 854;
    public static float L = 1.5f;
    public static String M = "      ";
    public static boolean b = true;
    public static boolean c = true;
    public static int p = R.drawable.number1;
    public static int q = R.drawable.number2;
    public static int r = R.drawable.number3;
    public static int s = R.drawable.number4;
    public static int t = R.drawable.number5;
    public static int u = R.drawable.number6;
    public static int v = R.drawable.number7;
    public static int w = R.drawable.number8;
    public static int x = R.drawable.number9;
    public static int y = R.drawable.number10;
    public static int z = R.drawable.number11;
    int G = 0;
    private Context N;
    private Bitmap O;
    private Canvas P;
    /* access modifiers changed from: private */
    public a Q;
    private Button R;
    /* access modifiers changed from: private */
    public Button S;
    /* access modifiers changed from: private */
    public boolean T = false;
    /* access modifiers changed from: private */
    public int U = 0;
    /* access modifiers changed from: private */
    public Handler V = new Handler();
    /* access modifiers changed from: private */
    public long W = 0;
    /* access modifiers changed from: private */
    public int X = 0;
    private int Y = 5999;
    private boolean Z = false;
    Rect a = new Rect();
    private Runnable aa = new j(this);
    int d = 40;
    int e = 10;
    int f = 40;
    int g = 10;
    int h;
    int i;
    int j;
    int k;
    int l = 5;
    int m = 5;
    int n = 100;
    int o = 150;

    public FifteenPuzzleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.N = context;
        a();
        i();
        this.Q = new a(1, this.m - 1, this.l - 1, this.G);
        setBackgroundColor(Color.rgb(4, 92, 116));
        setBackgroundDrawable(getResources().getDrawable(R.drawable.background_wooden));
        b();
        context.sendBroadcast(new Intent("com.softmimo.android.fifteenpuzzlewoodenlibrary.ACTION_LOADFINISH"));
    }

    /* access modifiers changed from: private */
    public void i() {
        this.W = SystemClock.uptimeMillis();
        this.X = 0;
        this.V.removeCallbacks(this.aa);
        this.V.postDelayed(this.aa, 100);
    }

    private void j() {
        this.V.removeCallbacks(this.aa);
        f();
        g();
        a(false);
    }

    public int a(int i2) {
        for (int i3 = 0; i3 < this.m - 1; i3++) {
            if (i2 >= this.d + (this.n * i3) && i2 <= this.d + ((i3 + 1) * this.n)) {
                return i3;
            }
        }
        return -1;
    }

    public void a() {
        int i2 = !H ? (int) (150.0f * L) : (int) (210.0d * ((double) L));
        int i3 = J <= K ? J : K;
        int i4 = J <= K ? K : J;
        this.n = (int) (((double) i3) / 4.8d);
        this.o = (int) (((double) (i4 - i2)) / 4.8d);
        if (this.n < this.o) {
            this.o = this.n;
            this.d = (int) (((double) this.n) * 0.4d);
            this.e = ((i4 - i2) - (this.o * 4)) / 2;
            this.f = 0;
            this.g = ((i4 - i2) - ((int) (((double) (this.o * 4)) * 1.2d))) / 2;
            return;
        }
        this.n = this.o;
        this.d = (i3 - (this.n * 4)) / 2;
        this.e = (int) (((double) this.o) * 0.4d);
        this.f = (i3 - ((int) (((double) (this.n * 4)) * 1.2d))) / 2;
        this.g = 0;
    }

    public void a(int i2, int i3, int i4) {
        int i5 = (this.n * i2) + this.d + (this.n / 2);
        int i6 = (this.o * i3) + this.e + (this.o / 2);
        int i7 = 0;
        int min = (Math.min(this.n, this.o) / 2) - 2;
        if (i4 > 0 && i4 <= 16) {
            switch (i4) {
                case 1:
                    i7 = p;
                    break;
                case 2:
                    i7 = q;
                    break;
                case 3:
                    i7 = r;
                    break;
                case 4:
                    i7 = s;
                    break;
                case 5:
                    i7 = t;
                    break;
                case 6:
                    i7 = u;
                    break;
                case 7:
                    i7 = v;
                    break;
                case 8:
                    i7 = w;
                    break;
                case 9:
                    i7 = x;
                    break;
                case 10:
                    i7 = y;
                    break;
                case 11:
                    i7 = z;
                    break;
                case 12:
                    i7 = A;
                    break;
                case 13:
                    i7 = B;
                    break;
                case 14:
                    i7 = C;
                    break;
                case 15:
                    i7 = D;
                    break;
                case 16:
                    i7 = E;
                    break;
            }
            Drawable drawable = this.N.getResources().getDrawable(i7);
            drawable.setBounds((i5 - min) - 2, (i6 - min) - 2, i5 + min + 2, min + i6 + 2);
            drawable.draw(this.P);
        }
        this.a.set(i5, i6, i5 + 60, i6 + 60);
        invalidate(this.a);
    }

    public void a(int i2, int i3, int i4, Button button, Button button2) {
        F = i2;
        if (F == 0) {
            F = 9999;
        }
        this.Y = i3;
        if (this.Y == 0) {
            this.Y = 5999;
        }
        this.R = button;
        this.S = button2;
        this.U = i4;
        this.Q.a(this.U);
        a(true);
        c();
    }

    public void a(boolean z2) {
        if (z2) {
            try {
                h();
            } catch (Exception e2) {
                return;
            }
        }
        this.S.setTextColor(-16777216);
        this.S.setOnClickListener(new h(this));
    }

    public int b(int i2) {
        for (int i3 = 0; i3 < this.l - 1; i3++) {
            if (i2 > this.e + (this.o * i3) && i2 < this.e + ((i3 + 1) * this.o)) {
                return i3;
            }
        }
        return -1;
    }

    public void b() {
        Bitmap createBitmap = Bitmap.createBitmap((this.n * (this.m - 1)) + (this.d * 2), (this.o * (this.l - 1)) + (this.e * 2), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        if (this.O != null) {
            canvas.drawBitmap(this.O, 0.0f, 0.0f, (Paint) null);
        }
        this.O = createBitmap;
        this.P = canvas;
        Paint paint = new Paint();
        Rect rect = new Rect(this.d + 1, this.e + 1, this.d + ((this.m - 1) * this.n), this.e + ((this.l - 1) * this.o));
        paint.setColor(0);
        this.P.drawRect(rect, paint);
        for (int i2 = 0; i2 < this.l - 1; i2++) {
            for (int i3 = 0; i3 < this.m - 1; i3++) {
                a(i3, i2, this.Q.a()[i3][i2]);
            }
        }
        if (this.l % 2 == 0) {
            this.h = this.d + (this.n * ((this.m / 2) - 1));
            this.j = this.m / 2;
        } else {
            this.h = (this.d + ((this.n * (this.m - 1)) / 2)) - (this.n / 2);
            this.j = (this.m + 1) / 2;
        }
        if (this.m % 2 == 0) {
            this.i = this.e + (this.o * ((this.l / 2) - 1));
            this.k = this.l / 2;
            return;
        }
        this.i = (this.e + ((this.o * (this.l - 1)) / 2)) - (this.o / 2);
        this.k = (this.l + 1) / 2;
    }

    public void c() {
        invalidate();
        Paint paint = new Paint();
        Drawable drawable = this.N.getResources().getDrawable(R.drawable.frame);
        Rect rect = new Rect(this.f, this.g, this.f + ((this.m - 1) * ((int) (((double) this.n) * 1.2d))), this.g + ((this.l - 1) * ((int) (((double) this.o) * 1.2d))));
        drawable.setBounds(rect);
        drawable.draw(this.P);
        Rect rect2 = new Rect(this.d + 1, this.e + 1, this.d + ((this.m - 1) * this.n), this.e + ((this.l - 1) * this.o));
        paint.setColor(0);
        this.P.drawRect(rect2, paint);
        for (int i2 = 0; i2 < this.l - 1; i2++) {
            for (int i3 = 0; i3 < this.m - 1; i3++) {
                a(i3, i2, this.Q.a()[i3][i2]);
            }
        }
        rect.set(0, 0, this.n * (this.m - 1), this.o * (this.l - 1));
        invalidate(rect);
        this.h = this.d + (this.Q.b() * this.n);
        this.i = this.e + (this.Q.c() * this.o);
    }

    public void d() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.N);
        if (this.Z) {
            builder.setTitle("NEW RECORD!");
        } else {
            builder.setTitle("Congratulations!");
        }
        builder.setMessage("You solve the puzzle by " + this.G + " steps in " + this.X + " seconds!");
        builder.setNeutralButton("OK", new e(this));
        builder.show();
    }

    public void e() {
        this.T = true;
        this.S.setText("--> Start New Game <--" + M);
        this.S.setTextColor(-65281);
        this.G = 0;
        this.Z = false;
        this.S.setOnClickListener(new f(this));
    }

    public void f() {
        if (this.G != 0) {
            int i2 = F;
            if (this.G < F) {
                this.Z = true;
            }
            F = Math.min(F, this.G);
            if (F != i2 && F != 0 && F != 9999) {
                SharedPreferences.Editor edit = this.N.getSharedPreferences("FifteenPuzzlePrefsFile", 0).edit();
                switch (this.U) {
                    case 1:
                        edit.putInt("easybeststeps", F);
                        break;
                    case 2:
                        edit.putInt("normalbeststeps", F);
                        break;
                    case 3:
                        edit.putInt("hardbeststeps", F);
                        break;
                }
                edit.commit();
            }
        }
    }

    public void g() {
        if (this.X != 0) {
            int i2 = this.Y;
            if (this.X < this.Y) {
                this.Z = true;
            }
            this.Y = Math.min(this.Y, this.X);
            if (this.Y != i2 && this.Y != 0 && this.Y != 9999) {
                SharedPreferences.Editor edit = this.N.getSharedPreferences("FifteenPuzzlePrefsFile", 0).edit();
                switch (this.U) {
                    case 1:
                        edit.putInt("easybesttime", this.Y);
                        break;
                    case 2:
                        edit.putInt("normalbesttime", this.Y);
                        break;
                    case 3:
                        edit.putInt("hardbesttime", this.Y);
                        break;
                }
                edit.commit();
            }
        }
    }

    public void h() {
        if (F == 0) {
            F = 9999;
        }
        if (this.Y == 0) {
            this.Y = 6039;
        }
        int i2 = this.Y;
        int i3 = i2 / 60;
        int i4 = i2 % 60;
        if (i4 < 10) {
            this.R.setText("Best Steps: " + Integer.toString(F) + "    Best Time: " + i3 + ":0" + i4 + M);
        } else {
            this.R.setText("Best Steps: " + Integer.toString(F) + "    Best Time: " + i3 + ":" + i4 + M);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.O != null) {
            canvas.drawBitmap(this.O, 0.0f, 0.0f, (Paint) null);
        }
        super.onDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.T) {
            return super.onTouchEvent(motionEvent);
        }
        int a2 = a((int) motionEvent.getX());
        int b2 = b((int) motionEvent.getY());
        if (-1 == a2 || -1 == b2) {
            return false;
        }
        if (this.Q.b(a2, b2)) {
            c();
            if (this.Q.c(a2, b2)) {
                this.G++;
                j();
                d();
                return false;
            }
            this.G++;
            a(false);
        }
        return super.onTouchEvent(motionEvent);
    }
}
