package com.softmimo.android.fifteenpuzzlewoodenlibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class b extends BroadcastReceiver {
    final /* synthetic */ ListView a;

    private b(ListView listView) {
        this.a = listView;
    }

    /* synthetic */ b(ListView listView, b bVar) {
        this(listView);
    }

    public void onReceive(Context context, Intent intent) {
        this.a.setProgressBarIndeterminateVisibility(false);
        if (this.a.g != null) {
            this.a.g.dismiss();
        }
    }
}
