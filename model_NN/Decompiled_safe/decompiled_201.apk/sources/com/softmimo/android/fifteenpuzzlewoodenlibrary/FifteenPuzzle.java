package com.softmimo.android.fifteenpuzzlewoodenlibrary;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.ads.AdView;
import com.google.ads.c;
import com.google.ads.d;
import com.softmimo.android.fifteenpuzzlewoodenfree.R;
import java.util.HashSet;

public class FifteenPuzzle extends Activity {
    private FifteenPuzzleView a;

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        int i;
        int i2;
        super.onCreate(bundle);
        setContentView((int) R.layout.wooden_main);
        this.a = (FifteenPuzzleView) findViewById(R.id.tv);
        Button button = (Button) findViewById(R.id.highestScore);
        Button button2 = (Button) findViewById(R.id.currentScore);
        int parseInt = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getString("list_difficulty", "1"));
        SharedPreferences sharedPreferences = getSharedPreferences("FifteenPuzzlePrefsFile", 0);
        Resources resources = getResources();
        CharSequence text = resources.getText(resources.getIdentifier("app_name", "string", getPackageName()));
        switch (parseInt) {
            case 2:
                int i3 = sharedPreferences.getInt("normalbeststeps", 9999);
                int i4 = sharedPreferences.getInt("normalbesttime", 5999);
                setTitle(((Object) text) + " Normal Mode");
                i = i3;
                i2 = i4;
                break;
            case 3:
                int i5 = sharedPreferences.getInt("hardbeststeps", 9999);
                int i6 = sharedPreferences.getInt("hardbesttime", 5999);
                setTitle(((Object) text) + " Hard Mode");
                i = i5;
                i2 = i6;
                break;
            default:
                int i7 = sharedPreferences.getInt("easybeststeps", 9999);
                int i8 = sharedPreferences.getInt("easybesttime", 5999);
                setTitle(((Object) text) + " Easy Mode");
                i = i7;
                i2 = i8;
                break;
        }
        this.a.a(i, i2, parseInt, button, button2);
        if (FifteenPuzzleView.H) {
            AdView adView = new AdView(this, d.a, "a14deba1bd65e7e");
            ((LinearLayout) findViewById(R.id.linearLayout)).addView(adView);
            c cVar = new c();
            cVar.a(false);
            HashSet hashSet = new HashSet();
            hashSet.add("free");
            hashSet.add("app");
            hashSet.add("game");
            hashSet.add("best");
            cVar.a(hashSet);
            adView.a(cVar);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        this.a.onKeyDown(i, keyEvent);
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
