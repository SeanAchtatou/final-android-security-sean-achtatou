package com.softmimo.android.fifteenpuzzlewoodenlibrary;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import com.softmimo.android.fifteenpuzzlewoodenfree.R;

public class ListView extends Activity implements View.OnClickListener {
    public static double a = 1.0d;
    private ImageView b;
    private ImageView c;
    private ImageView d;
    private ImageView e;
    private Intent f;
    /* access modifiers changed from: private */
    public ProgressDialog g = null;
    private BroadcastReceiver h = new b(this, null);

    private String b() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void a() {
        try {
            unregisterReceiver(this.h);
        } catch (IllegalArgumentException e2) {
        }
        finish();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4 || keyEvent.getAction() != 0) {
            return super.dispatchKeyEvent(keyEvent);
        }
        a();
        return true;
    }

    public void onClick(View view) {
        if (view == this.b) {
            setProgressBarIndeterminateVisibility(true);
            showDialog(1);
            this.f = new Intent(this, FifteenPuzzle.class);
            startActivity(this.f);
        }
        if (view == this.e) {
            this.f = new Intent(this, FifteenPuzzlePreferences.class);
            startActivity(this.f);
        }
        if (view == this.d) {
            this.f = new Intent(this, FifteenPuzzleHelp.class);
            startActivity(this.f);
        }
        if (view == this.c) {
            a();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(5);
        registerReceiver(this.h, new IntentFilter("com.softmimo.android.fifteenpuzzlewoodenlibrary.ACTION_LOADFINISH"));
        setContentView((int) R.layout.wooden_list_view);
        this.b = (ImageView) findViewById(R.id.start);
        this.c = (ImageView) findViewById(R.id.exit);
        this.d = (ImageView) findViewById(R.id.help);
        this.e = (ImageView) findViewById(R.id.options);
        this.b.setOnClickListener(this);
        this.c.setOnClickListener(this);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        FifteenPuzzleView.J = defaultDisplay.getWidth();
        FifteenPuzzleView.K = defaultDisplay.getHeight();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        FifteenPuzzleView.L = displayMetrics.density;
        String b2 = b();
        if (b2 != null) {
            a = Double.parseDouble(b2);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 1:
                this.g = new ProgressDialog(this);
                this.g.setMessage("Game Loading...");
                this.g.setIndeterminate(true);
                this.g.setCancelable(true);
                return this.g;
            default:
                return null;
        }
    }
}
