package com.softmimo.android.fifteenpuzzlewoodenlibrary;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

class d implements View.OnClickListener {
    final /* synthetic */ FifteenPuzzleHelp a;
    private final /* synthetic */ String b;

    d(FifteenPuzzleHelp fifteenPuzzleHelp, String str) {
        this.a = fifteenPuzzleHelp;
        this.b = str;
    }

    public void onClick(View view) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        if (FifteenPuzzleView.I) {
            intent.setData(Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=" + this.b));
        } else {
            intent.setData(Uri.parse("market://search?q=pub:\"Frank Android Software\""));
        }
        this.a.startActivity(intent);
    }
}
