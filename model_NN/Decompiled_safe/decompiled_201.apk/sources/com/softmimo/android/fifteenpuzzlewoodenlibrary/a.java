package com.softmimo.android.fifteenpuzzlewoodenlibrary;

import java.lang.reflect.Array;
import java.util.Random;

public class a {
    private static int i = 0;
    private int a;
    private int b;
    private int c;
    private int d = 0;
    private int e = 0;
    private int[][] f;
    private boolean g;
    private int h = 0;
    private String j = "123456789ABCDEF";

    public a() {
    }

    public a(int i2, int i3, int i4, int i5) {
        if (i2 == 1) {
            a(i3, i4, i2);
        }
    }

    private void a(int i2, int i3, int i4) {
        this.a = i2;
        this.b = i3;
        this.c = i4;
        this.f = (int[][]) Array.newInstance(Integer.TYPE, i2, i3);
        int i5 = 1;
        int i6 = 0;
        while (i6 < i2) {
            int i7 = i5;
            for (int i8 = 0; i8 < i3; i8++) {
                this.f[i6][i8] = i7;
                i7++;
            }
            i6++;
            i5 = i7;
        }
    }

    private boolean d(int i2, int i3) {
        if (i2 >= this.a || i2 < 0) {
            return true;
        }
        return i3 >= this.b || i3 < 0;
    }

    private boolean e() {
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.b) {
            int i4 = i3;
            int i5 = 0;
            while (i5 < this.a) {
                int i6 = this.f[i5][i2];
                int i7 = i5;
                int i8 = i4;
                int i9 = i2;
                while (i9 < this.b) {
                    int i10 = i8;
                    int i11 = i7;
                    while (i7 < this.a) {
                        if (this.f[i7][i9] < i6) {
                            i10++;
                        }
                        i11 = (i11 + 1) % this.a;
                        i7++;
                    }
                    i9++;
                    i7 = i11;
                    i8 = i10;
                }
                i5++;
                i4 = i8;
            }
            i2++;
            i3 = i4;
        }
        return true == (i3 % 2 == 0);
    }

    private void f() {
        this.j = "";
        for (int i2 = 0; i2 < this.b; i2++) {
            for (int i3 = 0; i3 < this.a; i3++) {
                if (i3 != this.a - 1 || i2 != this.b - 1) {
                    this.j = String.valueOf(this.j) + Integer.toHexString(this.f[i3][i2]);
                }
            }
        }
    }

    public void a(int i2) {
        this.h = i2;
        d();
    }

    public boolean a(int i2, int i3) {
        boolean z = false;
        if (i2 > 0 && this.f[i2 - 1][i3] == 16) {
            z = true;
        }
        if (i2 < this.a - 1 && this.f[i2 + 1][i3] == 16) {
            z = true;
        }
        if (i3 > 0 && this.f[i2][i3 - 1] == 16) {
            z = true;
        }
        if (i3 >= this.b - 1 || this.f[i2][i3 + 1] != 16) {
            return z;
        }
        return true;
    }

    public int[][] a() {
        return this.f;
    }

    public int b() {
        return this.d;
    }

    public boolean b(int i2, int i3) {
        if (d(i2, i3)) {
            return false;
        }
        if (!a(i2, i3)) {
            this.g = false;
            return false;
        }
        this.g = true;
        int i4 = 0;
        int i5 = 0;
        boolean z = false;
        while (i5 < this.a) {
            i4 = 0;
            while (true) {
                if (i4 >= this.b) {
                    break;
                } else if (this.f[i5][i4] == 16) {
                    z = true;
                    break;
                } else {
                    i4++;
                }
            }
            if (z) {
                break;
            }
            i5++;
        }
        if (!z) {
            return false;
        }
        this.f[i5][i4] = this.f[i2][i3];
        this.f[i2][i3] = 16;
        return true;
    }

    public int c() {
        return this.e;
    }

    public boolean c(int i2, int i3) {
        boolean z;
        boolean z2;
        int i4 = 0;
        int i5 = 1;
        boolean z3 = false;
        boolean z4 = true;
        while (i4 < this.b) {
            int i6 = i5;
            int i7 = 0;
            while (true) {
                if (i7 >= this.a) {
                    z = z3;
                    z2 = z4;
                    break;
                } else if (this.f[i7][i4] != i6) {
                    z = true;
                    z2 = false;
                    break;
                } else {
                    i6++;
                    i7++;
                }
            }
            if (z) {
                return z2;
            }
            i4++;
            z4 = z2;
            z3 = z;
            i5 = i6;
        }
        return z4;
    }

    public void d() {
        switch (this.h) {
            case 2:
                boolean z = false;
                while (!z) {
                    c cVar = new c(this, 1, 15);
                    if (new Random().nextInt(2) == 0) {
                        int i2 = 0;
                        int i3 = 1;
                        while (i2 < this.b) {
                            int i4 = i3;
                            for (int i5 = 0; i5 < this.a; i5++) {
                                if (i5 == 0) {
                                    this.f[i5][i2] = i4;
                                    cVar.a(i4);
                                }
                                i4++;
                            }
                            i2++;
                            i3 = i4;
                        }
                        for (int i6 = 0; i6 < this.b; i6++) {
                            for (int i7 = 0; i7 < this.a; i7++) {
                                if (i7 != 0) {
                                    if (i7 == this.a - 1 && i6 == this.b - 1) {
                                        this.f[i7][i6] = 16;
                                    } else {
                                        this.f[i7][i6] = cVar.a();
                                    }
                                }
                            }
                        }
                    } else {
                        int i8 = 0;
                        int i9 = 1;
                        while (i8 < this.b) {
                            int i10 = i9;
                            for (int i11 = 0; i11 < this.a; i11++) {
                                if (i8 == 0) {
                                    this.f[i11][i8] = i10;
                                    cVar.a(i10);
                                }
                                i10++;
                            }
                            i8++;
                            i9 = i10;
                        }
                        for (int i12 = 0; i12 < this.b; i12++) {
                            for (int i13 = 0; i13 < this.a; i13++) {
                                if (i12 != 0) {
                                    if (i13 == this.a - 1 && i12 == this.b - 1) {
                                        this.f[i13][i12] = 16;
                                    } else {
                                        this.f[i13][i12] = cVar.a();
                                    }
                                }
                            }
                        }
                    }
                    z = e();
                }
                f();
                i = this.h;
                return;
            case 3:
                boolean z2 = false;
                while (!z2) {
                    c cVar2 = new c(this, 1, 15);
                    for (int i14 = 0; i14 < this.b; i14++) {
                        for (int i15 = 0; i15 < this.a; i15++) {
                            if (i15 == this.a - 1 && i14 == this.b - 1) {
                                this.f[i15][i14] = 16;
                            } else {
                                this.f[i15][i14] = cVar2.a();
                            }
                        }
                    }
                    z2 = e();
                }
                f();
                i = this.h;
                return;
            default:
                boolean z3 = false;
                while (!z3) {
                    c cVar3 = new c(this, 1, 15);
                    int i16 = 0;
                    int i17 = 1;
                    while (i16 < this.b) {
                        int i18 = i17;
                        for (int i19 = 0; i19 < this.a; i19++) {
                            if (i19 == 0 || i16 == 0) {
                                this.f[i19][i16] = i18;
                                cVar3.a(i18);
                            }
                            i18++;
                        }
                        i16++;
                        i17 = i18;
                    }
                    for (int i20 = 0; i20 < this.b; i20++) {
                        for (int i21 = 0; i21 < this.a; i21++) {
                            if (!(i21 == 0 || i20 == 0)) {
                                if (i21 == this.a - 1 && i20 == this.b - 1) {
                                    this.f[i21][i20] = 16;
                                } else {
                                    this.f[i21][i20] = cVar3.a();
                                }
                            }
                        }
                    }
                    z3 = e();
                }
                f();
                i = this.h;
                return;
        }
    }
}
