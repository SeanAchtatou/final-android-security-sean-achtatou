package com.softmimo.android.fifteenpuzzlewoodenlibrary;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.softmimo.android.fifteenpuzzlewoodenfree.R;

public class FifteenPuzzlePreferences extends PreferenceActivity {
    public static void a(Context context, String str, int i) {
        Toast makeText = Toast.makeText(context, str, 1);
        View view = makeText.getView();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        ImageView imageView = new ImageView(context);
        imageView.setImageResource(i);
        linearLayout.addView(imageView);
        linearLayout.addView(view);
        makeText.setView(linearLayout);
        makeText.show();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences_wooden);
        setContentView((int) R.layout.preferencelayout_wooden);
        setTheme((int) R.style.Theme_DarkText);
        ((ImageView) findViewById(R.id.clearrecord)).setOnClickListener(new i(this));
        ((ImageView) findViewById(R.id.ok)).setOnClickListener(new g(this));
    }
}
