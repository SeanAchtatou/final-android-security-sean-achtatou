package defpackage;

import android.webkit.WebView;
import com.google.ads.g;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
/* renamed from: a  reason: default package */
final class a implements Runnable {
    final /* synthetic */ k a;
    private final h b;
    private final WebView c;
    private final j d;
    private final g e;
    private final boolean f;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Object.<init>():void in method: a.<init>(k, h, android.webkit.WebView, j, com.google.ads.g, boolean):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Object.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public a(defpackage.k r1, defpackage.h r2, android.webkit.WebView r3, defpackage.j r4, com.google.ads.g r5, boolean r6) {
        /*
            r0 = this;
            r0.a = r1
            r0.<init>()
            r0.b = r2
            r0.c = r3
            r0.d = r4
            r0.e = r5
            r0.f = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.a.<init>(k, h, android.webkit.WebView, j, com.google.ads.g, boolean):void");
    }

    public final void run() {
        this.c.stopLoading();
        this.c.destroy();
        this.d.a();
        if (this.f) {
            g h = this.b.h();
            h.stopLoading();
            h.setVisibility(8);
        }
        this.b.a(this.e);
    }
}
