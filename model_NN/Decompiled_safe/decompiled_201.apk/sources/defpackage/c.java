package defpackage;

import android.webkit.WebView;

/* renamed from: c  reason: default package */
final class c implements Runnable {
    final /* synthetic */ k a;
    private final String b;
    private final String c;
    private final WebView d;

    public c(k kVar, WebView webView, String str, String str2) {
        this.a = kVar;
        this.d = webView;
        this.b = str;
        this.c = str2;
    }

    public final void run() {
        this.d.loadDataWithBaseURL(this.b, this.c, "text/html", "utf-8", null);
    }
}
