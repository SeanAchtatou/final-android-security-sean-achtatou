package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.AdActivity;
import com.google.ads.AdView;
import com.google.ads.a;
import com.google.ads.b;
import com.google.ads.c;
import com.google.ads.d;
import com.google.ads.f;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: h  reason: default package */
public final class h {
    private static final Object a = new Object();
    private WeakReference b;
    private b c;
    private f d = null;
    private k e = null;
    private c f = null;
    private d g;
    private f h = new f();
    private String i;
    private g j;
    private s k;
    private Handler l = new Handler();
    private long m;
    private boolean n = false;
    private boolean o = false;
    private SharedPreferences p;
    private long q = 0;
    private ac r;
    private boolean s;
    private LinkedList t;
    private LinkedList u;
    private int v;

    public h(Activity activity, b bVar, d dVar, String str, boolean z) {
        this.b = new WeakReference(activity);
        this.c = bVar;
        this.g = dVar;
        this.i = str;
        this.s = z;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            if (z) {
                long j2 = this.p.getLong("Timeout" + str, -1);
                if (j2 < 0) {
                    this.m = 5000;
                } else {
                    this.m = j2;
                }
            } else {
                this.m = 60000;
            }
        }
        this.r = new ac(this);
        this.t = new LinkedList();
        this.u = new LinkedList();
        a();
        AdUtil.g(activity.getApplicationContext());
    }

    private synchronized boolean w() {
        return this.e != null;
    }

    private synchronized void x() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.d.e("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator it = this.u.iterator();
            while (it.hasNext()) {
                new Thread(new u((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    public final synchronized void a() {
        Activity d2 = d();
        if (d2 == null) {
            com.google.ads.util.d.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new g(d2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new s(this, l.b, true, false);
            } else {
                this.k = new s(this, l.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.v = i2;
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("Timeout" + this.i, j2);
            edit.commit();
            if (this.s) {
                this.m = j2;
            }
        }
    }

    public final synchronized void a(c cVar) {
        if (w()) {
            com.google.ads.util.d.e("loadAd called while the ad is already loading, so aborting.");
        } else if (AdActivity.c()) {
            com.google.ads.util.d.e("loadAd called while an interstitial or landing page is displayed, so aborting");
        } else {
            Activity d2 = d();
            if (d2 == null) {
                com.google.ads.util.d.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(d2.getApplicationContext()) && AdUtil.b(d2.getApplicationContext())) {
                this.n = false;
                this.t.clear();
                this.f = cVar;
                this.e = new k(this);
                this.e.a(cVar);
            }
        }
    }

    public final synchronized void a(g gVar) {
        this.e = null;
        if (this.c instanceof a) {
            if (gVar == g.NO_FILL) {
                this.h.n();
            } else if (gVar == g.NETWORK_ERROR) {
                this.h.l();
            }
        }
        com.google.ads.util.d.c("onFailedToReceiveAd(" + gVar + ")");
        if (this.d != null) {
            this.d.a(this.c, gVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.l.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        com.google.ads.util.d.a("Adding a tracking URL: " + str);
        this.t.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList linkedList) {
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            com.google.ads.util.d.a("Adding a click tracking URL: " + ((String) it.next()));
        }
        this.u = linkedList;
    }

    public final synchronized void b() {
        if (this.o) {
            com.google.ads.util.d.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            com.google.ads.util.d.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void c() {
        if (!(this.c instanceof AdView)) {
            com.google.ads.util.d.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            com.google.ads.util.d.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            com.google.ads.util.d.a("Refreshing is already enabled.");
        }
    }

    public final Activity d() {
        return (Activity) this.b.get();
    }

    public final b e() {
        return this.c;
    }

    public final synchronized k f() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        return this.i;
    }

    public final synchronized g h() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized s i() {
        return this.k;
    }

    public final d j() {
        return this.g;
    }

    public final f k() {
        return this.h;
    }

    public final synchronized int l() {
        return this.v;
    }

    public final long m() {
        return this.m;
    }

    public final synchronized boolean n() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void o() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            t();
        }
        com.google.ads.util.d.c("onReceiveAd()");
        if (this.d != null) {
            this.d.a(this.c);
        }
    }

    public final synchronized void p() {
        this.h.o();
        com.google.ads.util.d.c("onDismissScreen()");
        if (this.d != null) {
            this.d.c(this.c);
        }
    }

    public final synchronized void q() {
        com.google.ads.util.d.c("onPresentScreen()");
        if (this.d != null) {
            this.d.b(this.c);
        }
    }

    public final synchronized void r() {
        com.google.ads.util.d.c("onLeaveApplication()");
        if (this.d != null) {
            this.d.d(this.c);
        }
    }

    public final void s() {
        this.h.b();
        x();
    }

    public final synchronized void t() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.d.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new u((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean u() {
        return !this.u.isEmpty();
    }

    public final synchronized void v() {
        if (this.f == null) {
            com.google.ads.util.d.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.d()) {
                com.google.ads.util.d.a("Not refreshing because the ad is not visible.");
            } else {
                com.google.ads.util.d.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            com.google.ads.util.d.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }
}
