package com.hourdb.volumelocker.util;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;
import java.util.List;

public class ServiceUtils {
    private static final String PROCESS_VLSERVICE = "com.hourdb.volumelocker.VLService";
    private static final String TAG = "ServiceUtils";

    public static boolean isServiceRunning(Context context) {
        List<ActivityManager.RunningServiceInfo> runningServices = null;
        try {
            runningServices = ((ActivityManager) context.getSystemService("activity")).getRunningServices(100);
            if (runningServices != null) {
                for (ActivityManager.RunningServiceInfo rsi : runningServices) {
                    if (rsi.service.getClassName().equalsIgnoreCase(PROCESS_VLSERVICE)) {
                        if (runningServices != null) {
                            runningServices.clear();
                        }
                        return true;
                    }
                }
            }
            if (runningServices != null) {
                runningServices.clear();
            }
        } catch (SecurityException e) {
            Log.e(TAG, "Security exception retrieving service info... " + e.getMessage());
            if (runningServices != null) {
                runningServices.clear();
            }
        } catch (Throwable th) {
            if (runningServices != null) {
                runningServices.clear();
            }
            throw th;
        }
        return false;
    }
}
