package com.hourdb.volumelocker.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVLServiceCallback extends IInterface {
    void valueChanged(boolean z, String str, int i) throws RemoteException;

    public static abstract class Stub extends Binder implements IVLServiceCallback {
        private static final String DESCRIPTOR = "com.hourdb.volumelocker.aidl.IVLServiceCallback";
        static final int TRANSACTION_valueChanged = 1;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IVLServiceCallback asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof IVLServiceCallback)) {
                return new Proxy(obj);
            }
            return (IVLServiceCallback) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean _arg0;
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    if (data.readInt() != 0) {
                        _arg0 = true;
                    } else {
                        _arg0 = false;
                    }
                    valueChanged(_arg0, data.readString(), data.readInt());
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }

        private static class Proxy implements IVLServiceCallback {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public void valueChanged(boolean terminate, String changed, int counter) throws RemoteException {
                Parcel _data = Parcel.obtain();
                try {
                    _data.writeInterfaceToken(Stub.DESCRIPTOR);
                    _data.writeInt(terminate ? 1 : 0);
                    _data.writeString(changed);
                    _data.writeInt(counter);
                    this.mRemote.transact(1, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }
        }
    }
}
