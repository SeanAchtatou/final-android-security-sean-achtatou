package com.hourdb.volumelocker.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import com.hourdb.volumelocker.R;
import com.hourdb.volumelocker.helper.PreferencesHelper;
import com.hourdb.volumelocker.receiver.EnableOrDisableBroadcastReceiver;
import com.hourdb.volumelocker.util.ServiceUtils;

public class EnableDisableWidget extends AppWidgetProvider {
    private static final String TAG = "EnableDisableWidget";

    public void onEnabled(Context context) {
        Log.d(TAG, "Widget Enabled...");
        Intent active = new Intent();
        active.setAction(EnableOrDisableBroadcastReceiver.ACTION_ENABLE_OR_DISABLE_VL);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, active, 134217728);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.enable_disable_widget_layout);
        remoteViews.setOnClickPendingIntent(R.id.EnableDisableWidgetLayout, pendingIntent);
        AppWidgetManager.getInstance(context).updateAppWidget(new ComponentName(context.getPackageName(), EnableDisableWidget.class.getName()), remoteViews);
    }

    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getAction() == null) {
            super.onReceive(context, intent);
            return;
        }
        String intentAction = intent.getAction();
        Log.d(TAG, "Received broadcast to widget: " + intentAction);
        if ("android.appwidget.action.APPWIDGET_DELETED".equals(intentAction)) {
            int appWidgetId = intent.getExtras().getInt("appWidgetId", 0);
            if (appWidgetId != 0) {
                onDeleted(context, new int[]{appWidgetId});
                return;
            }
            return;
        }
        super.onReceive(context, intent);
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d(TAG, "Updating EnableDisable widgets: " + n);
        boolean currentlyEnabled = new PreferencesHelper(context, false).getEnableVLS() && ServiceUtils.isServiceRunning(context);
        for (int appWidgetId : appWidgetIds) {
            Intent clickIntent = new Intent();
            clickIntent.putExtra("appWidgetId", appWidgetId);
            clickIntent.setAction(EnableOrDisableBroadcastReceiver.ACTION_ENABLE_OR_DISABLE_VL);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, clickIntent, 134217728);
            RemoteViews widgetView = new RemoteViews(context.getPackageName(), (int) R.layout.enable_disable_widget_layout);
            widgetView.setOnClickPendingIntent(R.id.EnableDisableWidgetLayout, pendingIntent);
            if (currentlyEnabled) {
                widgetView.setImageViewResource(R.id.EnableDisableWidgetIcon, R.drawable.enabled_icon);
            } else {
                widgetView.setImageViewResource(R.id.EnableDisableWidgetIcon, R.drawable.disabled_icon);
            }
            appWidgetManager.updateAppWidget(appWidgetId, widgetView);
        }
    }
}
