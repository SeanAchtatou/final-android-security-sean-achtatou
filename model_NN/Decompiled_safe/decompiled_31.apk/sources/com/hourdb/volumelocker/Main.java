package com.hourdb.volumelocker;

import android.content.ComponentName;
import android.content.Intent;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.Log;
import com.hourdb.volumelocker.helper.PreferencesHelper;

public class Main extends PreferenceActivity implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {
    private static final int RINGTONE_SELECTION = 123;
    private static final String TAG = "Main";
    private PreferencesHelper pHelper;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        this.pHelper = new PreferencesHelper(this, true);
        restoreSettings();
        startService();
    }

    private void restoreSettings() {
        boolean enableVLSChecked = this.pHelper.getEnableVLS();
        CheckBoxPreference checkbox = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_ENABLE_VLS);
        checkbox.setOnPreferenceChangeListener(this);
        checkbox.setChecked(enableVLSChecked);
        boolean lockAlarmVolumeChecked = this.pHelper.getLockAlarmVolume();
        CheckBoxPreference checkbox2 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_LOCK_ALARM_VOLUME);
        checkbox2.setOnPreferenceChangeListener(this);
        checkbox2.setChecked(lockAlarmVolumeChecked);
        boolean lockMusicVolumeChecked = this.pHelper.getLockMusicVolume();
        CheckBoxPreference checkbox3 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_LOCK_MUSIC_VOLUME);
        checkbox3.setOnPreferenceChangeListener(this);
        checkbox3.setChecked(lockMusicVolumeChecked);
        boolean disableMusicVolumeLockWhileHeadphonesInChecked = this.pHelper.getDisableMusicVolumeLockWhileHeadphonesIn();
        CheckBoxPreference checkbox4 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_MUSIC_VOLUME_LOCK_WHILE_HEADPHONES_IN);
        checkbox4.setOnPreferenceChangeListener(this);
        checkbox4.setChecked(disableMusicVolumeLockWhileHeadphonesInChecked);
        boolean lockNotificationVolumeChecked = this.pHelper.getLockNotificationVolume();
        CheckBoxPreference checkbox5 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_LOCK_NOTIFICATION_VOLUME);
        checkbox5.setOnPreferenceChangeListener(this);
        checkbox5.setChecked(lockNotificationVolumeChecked);
        boolean lockRingerVolumeChecked = this.pHelper.getLockRingerVolume();
        CheckBoxPreference checkbox6 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_LOCK_RINGER_VOLUME);
        checkbox6.setOnPreferenceChangeListener(this);
        checkbox6.setChecked(lockRingerVolumeChecked);
        boolean disableRingerVolumeLockWhileRingingChecked = this.pHelper.getDisableRingerVolumeLockWhileRinging();
        CheckBoxPreference checkbox7 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_RINGER_VOLUME_LOCK_WHILE_RINGING);
        checkbox7.setOnPreferenceChangeListener(this);
        checkbox7.setChecked(disableRingerVolumeLockWhileRingingChecked);
        boolean lockSystemVolumeChecked = this.pHelper.getLockSystemVolume();
        CheckBoxPreference checkbox8 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_LOCK_SYSTEM_VOLUME);
        checkbox8.setOnPreferenceChangeListener(this);
        checkbox8.setChecked(lockSystemVolumeChecked);
        boolean lockDTMFChecked = this.pHelper.getLockDTMFVolume();
        CheckBoxPreference checkbox9 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_LOCK_DTMF_VOLUME);
        checkbox9.setOnPreferenceChangeListener(this);
        checkbox9.setChecked(lockDTMFChecked);
        try {
            checkbox9.setEnabled(((AudioManager) getSystemService("audio")).getClass().getField("STREAM_DTMF") != null);
        } catch (Exception e) {
            checkbox9.setEnabled(false);
        }
        boolean lockVoiceCallVolumeChecked = this.pHelper.getLockVoiceCallVolume();
        CheckBoxPreference checkbox10 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_LOCK_VOICE_CALL_VOLUME);
        checkbox10.setOnPreferenceChangeListener(this);
        checkbox10.setChecked(lockVoiceCallVolumeChecked);
        boolean disableVoiceCallVolumeLockDuringCallChecked = this.pHelper.getDisableVoiceCallVolumeLockDuringCall();
        CheckBoxPreference checkbox11 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_VOICE_CALL_VOLUME_LOCK_DURING_CALL);
        checkbox11.setOnPreferenceChangeListener(this);
        checkbox11.setChecked(disableVoiceCallVolumeLockDuringCallChecked);
        boolean disableDTMFVolumeLockDuringCallChecked = this.pHelper.getDisableDTMFVolumeLockDuringCall();
        CheckBoxPreference checkbox12 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_DTMF_VOLUME_LOCK_DURING_CALL);
        checkbox12.setOnPreferenceChangeListener(this);
        checkbox12.setChecked(disableDTMFVolumeLockDuringCallChecked);
        String notificationPopupStr = this.pHelper.getNotificationPopup() ? "1" : "0";
        ListPreference listPreference = (ListPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_NOTIFICATION_POPUP);
        listPreference.setOnPreferenceChangeListener(this);
        listPreference.setValue(notificationPopupStr);
        String notificationTimeoutStr = new StringBuilder(String.valueOf(this.pHelper.getNotificationTimeout())).toString();
        ListPreference listPreference2 = (ListPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_NOTIFICATION_TIMEOUT);
        listPreference2.setOnPreferenceChangeListener(this);
        listPreference2.setValue(notificationTimeoutStr);
        String notificationLEDColorStr = new StringBuilder(String.valueOf(this.pHelper.getNotificationLEDColor())).toString();
        ListPreference listPreference3 = (ListPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_NOTIFICATION_LED_COLOR);
        listPreference3.setOnPreferenceChangeListener(this);
        listPreference3.setValue(notificationLEDColorStr);
        boolean notificationVibrateChecked = this.pHelper.getNotificationVibrate();
        CheckBoxPreference checkbox13 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_NOTIFICATION_VIBRATE);
        checkbox13.setOnPreferenceChangeListener(this);
        checkbox13.setChecked(notificationVibrateChecked);
        boolean notificationRingVibrateOnlyUnlockedChecked = this.pHelper.getNotificationRingVibrateOnlyUnlocked();
        CheckBoxPreference checkbox14 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_NOTIFICATION_RING_VIBRATE_ONLY_UNLOCKED);
        checkbox14.setOnPreferenceChangeListener(this);
        checkbox14.setChecked(notificationRingVibrateOnlyUnlockedChecked);
        boolean disableDuringScreenLockChecked = this.pHelper.getDisableDuringScreenLock();
        CheckBoxPreference checkbox15 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_DURING_SCREEN_LOCK);
        checkbox15.setOnPreferenceChangeListener(this);
        checkbox15.setChecked(disableDuringScreenLockChecked);
        boolean disableDuringScreenLockSaveChangesChecked = this.pHelper.getDisableDuringScreenLockSaveChanges();
        CheckBoxPreference checkbox16 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_DURING_SCREEN_LOCK_SAVE_CHANGES);
        checkbox16.setOnPreferenceChangeListener(this);
        checkbox16.setChecked(disableDuringScreenLockSaveChangesChecked);
        boolean disableDuringCameraUsageChecked = this.pHelper.getDisableDuringCameraUsage();
        CheckBoxPreference checkbox17 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_DURING_CAMERA_USAGE);
        checkbox17.setOnPreferenceChangeListener(this);
        checkbox17.setChecked(disableDuringCameraUsageChecked);
        boolean disableDuringCameraUsageDelayChecked = this.pHelper.getDisableDuringCameraUsageDelay();
        CheckBoxPreference checkbox18 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_DURING_CAMERA_USAGE_DELAY);
        checkbox18.setOnPreferenceChangeListener(this);
        checkbox18.setChecked(disableDuringCameraUsageDelayChecked);
        boolean disableDuringGoogleVoiceChecked = this.pHelper.getDisableDuringGoogleVoice();
        CheckBoxPreference checkbox19 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_DURING_GOOGLE_VOICE);
        checkbox19.setOnPreferenceChangeListener(this);
        checkbox19.setChecked(disableDuringGoogleVoiceChecked);
        boolean disableDuringGoogleNavigationChecked = this.pHelper.getDisableDuringGoogleNavigation();
        CheckBoxPreference checkbox20 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_DURING_GOOGLE_NAVIGATION);
        checkbox20.setOnPreferenceChangeListener(this);
        checkbox20.setChecked(disableDuringGoogleNavigationChecked);
        boolean disableDuringBedsideChecked = this.pHelper.getDisableDuringBedside();
        CheckBoxPreference checkbox21 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_DISABLE_DURING_BEDSIDE);
        checkbox21.setOnPreferenceChangeListener(this);
        checkbox21.setChecked(disableDuringBedsideChecked);
        boolean allowOtherAppsToOverrideChecked = this.pHelper.getAllowOtherAppsToOverride();
        CheckBoxPreference checkbox22 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_ALLOW_OTHER_APPS_TO_OVERRIDE);
        checkbox22.setOnPreferenceChangeListener(this);
        checkbox22.setChecked(allowOtherAppsToOverrideChecked);
        boolean autoSaveSilentModeChecked = this.pHelper.getAutoSaveSilentMode();
        CheckBoxPreference checkbox23 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_AUTO_SAVE_SILENT_MODE);
        checkbox23.setOnPreferenceChangeListener(this);
        checkbox23.setChecked(autoSaveSilentModeChecked);
        boolean runAtHigherPriorityChecked = this.pHelper.getRunAtHigherPriority();
        CheckBoxPreference checkbox24 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_RUN_AT_HIGHER_PRIORITY);
        checkbox24.setOnPreferenceChangeListener(this);
        checkbox24.setChecked(runAtHigherPriorityChecked);
        boolean persistVolumeLevelsChecked = this.pHelper.getPersistVolumeLevels();
        CheckBoxPreference checkbox25 = (CheckBoxPreference) getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_PERSIST_VOLUME_LEVELS);
        checkbox25.setOnPreferenceChangeListener(this);
        checkbox25.setChecked(persistVolumeLevelsChecked);
        getPreferenceScreen().findPreference(PreferencesHelper.PREFERENCE_NOTIFICATION_RINGTONE_URI).setOnPreferenceClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    private void restartService() {
        Log.d(TAG, "Restarting service...");
        stopService();
        startService();
    }

    private void stopService() {
        Log.d(TAG, "Stopping the service...");
        ComponentName comp = new ComponentName(getPackageName(), VLService.class.getName());
        if (!stopService(new Intent().setComponent(comp))) {
            Log.d(TAG, "Could not stop service " + comp.toString());
        }
    }

    private void startService() {
        if (this.pHelper != null && this.pHelper.getEnableVLS()) {
            Log.d(TAG, "Starting the service...");
            ComponentName comp = new ComponentName(getPackageName(), VLService.class.getName());
            if (startService(new Intent().setComponent(comp)) == null) {
                Log.d(TAG, "Could not start service " + comp.toString());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void callRingtoneSelection() {
        Intent intent = new Intent("android.intent.action.RINGTONE_PICKER");
        String notificationRingtoneURI = this.pHelper.getNotificationRingtoneURI();
        if (notificationRingtoneURI != null && notificationRingtoneURI.length() > 0) {
            intent.putExtra("android.intent.extra.ringtone.EXISTING_URI", Uri.parse(notificationRingtoneURI));
        }
        intent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", true);
        intent.putExtra("android.intent.extra.ringtone.DEFAULT_URI", RingtoneManager.getDefaultUri(2));
        intent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", true);
        intent.putExtra("android.intent.extra.ringtone.TYPE", 2);
        startActivityForResult(intent, RINGTONE_SELECTION);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Result found");
        if (requestCode != RINGTONE_SELECTION) {
            return;
        }
        if (resultCode != -1) {
            Log.d(TAG, "Ringtone URI: Bad result code");
        } else if (data != null) {
            Uri uri = (Uri) data.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
            this.pHelper.edit();
            String notificationRingtoneURI = uri != null ? uri.toString() : "";
            this.pHelper.setNotificationRingtoneURI(notificationRingtoneURI);
            this.pHelper.commit();
            Log.d(TAG, "Ringtone URI:" + notificationRingtoneURI);
        } else {
            Log.d(TAG, "Ringtone URI: No data to save?");
        }
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();
        Log.d(TAG, "Preference changed: " + key);
        this.pHelper.edit();
        if (newValue instanceof String) {
            String val = (String) newValue;
            if (key.equals(PreferencesHelper.PREFERENCE_NOTIFICATION_POPUP)) {
                this.pHelper.setBoolean(key, val.equalsIgnoreCase("1"));
            } else if (key.equals(PreferencesHelper.PREFERENCE_NOTIFICATION_TIMEOUT) || key.equals(PreferencesHelper.PREFERENCE_NOTIFICATION_LED_COLOR)) {
                try {
                    this.pHelper.setInt(key, Integer.parseInt(val));
                } catch (NumberFormatException e) {
                }
            } else {
                this.pHelper.setString(key, val);
            }
        } else if (newValue instanceof Boolean) {
            this.pHelper.setBoolean(key, ((Boolean) newValue).booleanValue());
        } else {
            Log.d(TAG, "newValue is...? " + newValue.toString());
        }
        this.pHelper.commit();
        restartService();
        return true;
    }

    public boolean onPreferenceClick(Preference preference) {
        callRingtoneSelection();
        return false;
    }
}
