package com.hourdb.volumelocker;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.RemoteViews;
import com.hourdb.volumelocker.aidl.IVLService;
import com.hourdb.volumelocker.aidl.IVLServiceCallback;
import com.hourdb.volumelocker.helper.PreferencesHelper;
import com.hourdb.volumelocker.model.VolumeBean;
import com.hourdb.volumelocker.receiver.HeadphonesBroadcastReceiver;
import com.hourdb.volumelocker.widget.EnableDisableWidget;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class VLService extends Service {
    private static final int BROADCAST_RECEIVED_TIMEOUT = 7;
    public static final String BUNDLE_EXTRA_HEADPHONES_ON = "HeadphonesOn";
    public static final String BUNDLE_ITEM_CHANGED = "Changed";
    public static final String BUNDLE_ITEM_COUNTER = "Counter";
    public static final String BUNDLE_ITEM_TERMINATE = "Terminate";
    public static final String BUNDLE_REVERT_VOLUME_LEVEL_CHANGES = "RevertVolumeLevelChanges";
    public static final String BUNDLE_SAVE_VOLUME_LEVEL_CHANGES = "SaveVolumeLevelChanges";
    public static final String BUNDLE_VL_IGNORE_CHANGE_RECEIVED = "IgnoreChangeReceived";
    private static final int CAMERA_CLOSED_DELAY_TIMEOUT = 2;
    public static final int HANDLER_NOTIFICATION_UPDATE = 1;
    private static final int LOCK_SKIP_FINISHED = 2;
    private static final int LOCK_SKIP_INITIALIZED = 0;
    private static final int LOCK_SKIP_STARTED = 1;
    private static final String NOTIFICATIONS_USE_RING_VOLUME = "notifications_use_ring_volume";
    public static final int NOTIFICATION_VOLUME_LOCKER_ID = 1;
    private static final String PROCESS_BEDSIDE_CLASS_NAME_POST = "net.geekherd.bedsidepro2.services.NotificationsService";
    private static final String PROCESS_BEDSIDE_CLASS_NAME_PRE = "net.geekherd.bedsidepro2.services.NotificationsServiceCupcake";
    private static final String PROCESS_CAMERA_REGEX = "^(com\\.sonyericsson\\.android\\.camera|com\\.google\\.android\\.camera|com\\.android\\.camera)$";
    private static final String PROCESS_GOOGLE_VOICE_PROCESS_NAME = "com.google.android.apps.googlevoice";
    private static final String PROCESS_NAVIGATION_PROCESS_NAME = "com.google.android.apps.maps:driveabout";
    private static final String PROCESS_NAVIGATION_SERVICE_COMPONENT_NAME = "com.google.android.maps.driveabout.app.NavigationService";
    private static final String TAG = "VLService";
    private static final int TIMEOUT_COUNTER_OFF = -5;
    private static final long UPDATE_INTERVAL = 1000;
    private static final int UPDATE_INTERVAL_IN_SECONDS = 1;
    private static final String VIBRATE_IN_SILENT = "vibrate_in_silent";
    private ActivityManager activityManager = null;
    private AudioManager audioManager = null;
    private AtomicInteger broadcastChangesReceived = new AtomicInteger(LOCK_SKIP_INITIALIZED);
    private int broadcastReceivedDelayCounter = LOCK_SKIP_INITIALIZED;
    private int cameraClosedDelayCounter = LOCK_SKIP_INITIALIZED;
    private Pattern cameraRegexPattern = null;
    private AtomicBoolean headphonesOn = new AtomicBoolean(false);
    private HeadphonesBroadcastReceiver headphonesReceiver = null;
    private KeyguardManager keyguardManager = null;
    private int lockSkipOccurred = LOCK_SKIP_INITIALIZED;
    private final IVLService.Stub mBinder = new IVLService.Stub() {
        public void registerCallback(IVLServiceCallback cb) throws RemoteException {
            Log.d(VLService.TAG, "Registering callback...");
            if (cb != null) {
                VLService.this.mCallbacks.register(cb);
            }
        }

        public void unregisterCallback(IVLServiceCallback cb) throws RemoteException {
            Log.d(VLService.TAG, "Unregistering callback...");
            if (cb != null) {
                VLService.this.mCallbacks.unregister(cb);
            }
        }
    };
    /* access modifiers changed from: private */
    public final RemoteCallbackList<IVLServiceCallback> mCallbacks = new RemoteCallbackList<>();
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (VLService.this.mCallbacks != null) {
                        int callbacksTotal = VLService.this.mCallbacks.beginBroadcast();
                        for (int i = VLService.LOCK_SKIP_INITIALIZED; i < callbacksTotal; i++) {
                            try {
                                Bundle tmpBundle = msg.getData();
                                ((IVLServiceCallback) VLService.this.mCallbacks.getBroadcastItem(i)).valueChanged(tmpBundle.getBoolean(VLService.BUNDLE_ITEM_TERMINATE), tmpBundle.getString(VLService.BUNDLE_ITEM_CHANGED), tmpBundle.getInt(VLService.BUNDLE_ITEM_COUNTER));
                            } catch (RemoteException e) {
                            }
                        }
                        VLService.this.mCallbacks.finishBroadcast();
                        return;
                    }
                    return;
                default:
                    super.handleMessage(msg);
                    return;
            }
        }
    };
    private Notification notification = null;
    private PendingIntent notificationContentIntent = null;
    private int notificationLEDColor = -1;
    private int notificationLEDColorSetting = LOCK_SKIP_INITIALIZED;
    private NotificationManager notificationManager = null;
    private boolean notificationPopped = false;
    private String notificationRingerURI = null;
    private int notificationTimeoutDurationSetting = 30;
    private PreferencesHelper pHelper = null;
    private boolean persistInitLoadedVolumeMapping = false;
    private PowerManager powerManager = null;
    private AtomicBoolean revertVolumeLevelChanges = new AtomicBoolean(false);
    private AtomicBoolean saveVolumeLevelChanges = new AtomicBoolean(false);
    private AtomicBoolean serviceRunning = new AtomicBoolean(false);
    private String stringNotificationChangedAlarm = null;
    private String stringNotificationChangedDTMF = null;
    private String stringNotificationChangedDelimiter = null;
    private String stringNotificationChangedMusic = null;
    private String stringNotificationChangedNotification = null;
    private String stringNotificationChangedRinger = null;
    private String stringNotificationChangedSystem = null;
    private String stringNotificationChangedVoiceCall = null;
    private String stringNotificationContentLeadText = null;
    private String stringNotificationContentTitleEnd = null;
    private String stringNotificationContentTitleStart = null;
    private String stringNotificationTickerText = null;
    private boolean supportsDTMF = false;
    private int systemTimeoutCounter = TIMEOUT_COUNTER_OFF;
    private int systemTotalChangedCounter = LOCK_SKIP_INITIALIZED;
    private TelephonyManager telephonyManager = null;
    private Timer timer = null;
    private Vibrator vibratorService = null;
    private HashMap<Integer, VolumeBean> volumeMapping = new HashMap<>();
    private PowerManager.WakeLock wakeLock = null;

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public void onCreate() {
        Log.d(TAG, "Creating service...");
        this.pHelper = new PreferencesHelper(this, true);
        Log.d(TAG, "EnableVLS: " + this.pHelper.getEnableVLS() + "; ServiceRunning: " + this.serviceRunning.get());
        if (!this.pHelper.getEnableVLS() || this.serviceRunning.get()) {
            Log.d(TAG, "VLService is diabled via settings, or is already running...");
            return;
        }
        this.serviceRunning.getAndSet(true);
        if (this.pHelper.getRunAtHigherPriority()) {
            Log.d(TAG, "Running VLService at a higher priority...");
            try {
                Process.setThreadPriority(-16);
            } catch (IllegalArgumentException e) {
                Log.i(TAG, "Thread priority change arg exception: " + e.getMessage());
            } catch (SecurityException e2) {
                Log.i(TAG, "Thread priority change security exception: " + e2.getMessage());
            }
        }
        notifyWidgetOfUpdate(true);
        Log.d(TAG, "Populating defaults and services information...");
        this.systemTimeoutCounter = TIMEOUT_COUNTER_OFF;
        this.systemTotalChangedCounter = LOCK_SKIP_INITIALIZED;
        this.cameraClosedDelayCounter = LOCK_SKIP_INITIALIZED;
        this.broadcastReceivedDelayCounter = LOCK_SKIP_INITIALIZED;
        this.lockSkipOccurred = LOCK_SKIP_INITIALIZED;
        this.pHelper.getEnableVLS();
        this.pHelper.getLockAlarmVolume();
        this.pHelper.getLockMusicVolume();
        this.pHelper.getLockDTMFVolume();
        this.pHelper.getDisableMusicVolumeLockWhileHeadphonesIn();
        this.pHelper.getLockNotificationVolume();
        this.pHelper.getLockRingerVolume();
        this.pHelper.getDisableRingerVolumeLockWhileRinging();
        this.pHelper.getLockSystemVolume();
        this.pHelper.getLockVoiceCallVolume();
        this.pHelper.getDisableVoiceCallVolumeLockDuringCall();
        this.pHelper.getDisableDTMFVolumeLockDuringCall();
        this.pHelper.getDisableDuringScreenLock();
        this.pHelper.getDisableDuringScreenLockSaveChanges();
        this.pHelper.getNotificationPopup();
        this.pHelper.getNotificationVibrate();
        this.pHelper.getNotificationRingVibrateOnlyUnlocked();
        this.pHelper.getDisableDuringCameraUsage();
        this.pHelper.getDisableDuringCameraUsageDelay();
        this.pHelper.getDisableDuringGoogleVoice();
        this.pHelper.getDisableDuringGoogleNavigation();
        this.pHelper.getDisableDuringBedside();
        this.pHelper.getAllowOtherAppsToOverride();
        this.pHelper.getAutoSaveSilentMode();
        this.notificationRingerURI = this.pHelper.getNotificationRingtoneURI();
        this.notificationTimeoutDurationSetting = this.pHelper.getNotificationTimeout();
        this.notificationLEDColorSetting = this.pHelper.getNotificationLEDColor();
        this.notificationLEDColor = parseNotificationLEDColor(this.notificationLEDColorSetting);
        this.notificationManager = (NotificationManager) getSystemService("notification");
        this.powerManager = (PowerManager) getSystemService("power");
        this.audioManager = (AudioManager) getSystemService("audio");
        if (this.pHelper.getNotificationVibrate()) {
            this.vibratorService = (Vibrator) getSystemService("vibrator");
        } else {
            this.vibratorService = null;
        }
        if ((!this.pHelper.getLockVoiceCallVolume() || !this.pHelper.getDisableVoiceCallVolumeLockDuringCall()) && ((!this.pHelper.getLockRingerVolume() || !this.pHelper.getDisableRingerVolumeLockWhileRinging()) && (!this.pHelper.getLockDTMFVolume() || !this.pHelper.getDisableDTMFVolumeLockDuringCall()))) {
            this.telephonyManager = null;
        } else {
            this.telephonyManager = (TelephonyManager) getSystemService("phone");
        }
        if (this.pHelper.getDisableDuringScreenLock() || this.pHelper.getNotificationRingVibrateOnlyUnlocked()) {
            this.keyguardManager = (KeyguardManager) getSystemService("keyguard");
        } else {
            this.keyguardManager = null;
        }
        if (this.pHelper.getDisableDuringGoogleNavigation() || this.pHelper.getDisableDuringBedside() || this.pHelper.getDisableDuringCameraUsage() || this.pHelper.getDisableDuringGoogleVoice()) {
            this.activityManager = (ActivityManager) getSystemService("activity");
            if (this.pHelper.getDisableDuringCameraUsage()) {
                this.cameraRegexPattern = Pattern.compile(PROCESS_CAMERA_REGEX, 74);
            }
        } else {
            this.activityManager = null;
        }
        if (this.pHelper.getLockMusicVolume() && this.pHelper.getDisableMusicVolumeLockWhileHeadphonesIn()) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.HEADSET_PLUG");
            this.headphonesReceiver = new HeadphonesBroadcastReceiver();
            registerReceiver(this.headphonesReceiver, intentFilter);
        }
        this.stringNotificationTickerText = getString(R.string.notification_ticker_text);
        this.stringNotificationContentTitleStart = getString(R.string.notification_content_title_start);
        this.stringNotificationContentTitleEnd = getString(R.string.notification_content_title_end);
        this.stringNotificationContentLeadText = String.valueOf(getString(R.string.notification_content_lead_text)) + " ";
        this.stringNotificationChangedAlarm = getString(R.string.notification_changed_alarm);
        this.stringNotificationChangedRinger = getString(R.string.notification_changed_ringer);
        this.stringNotificationChangedMusic = getString(R.string.notification_changed_music);
        this.stringNotificationChangedNotification = getString(R.string.notification_changed_notification);
        this.stringNotificationChangedSystem = getString(R.string.notification_changed_system);
        this.stringNotificationChangedVoiceCall = getString(R.string.notification_changed_voice_call);
        this.stringNotificationChangedDTMF = getString(R.string.notification_changed_dtmf);
        this.stringNotificationChangedDelimiter = ", ";
        try {
            this.supportsDTMF = this.audioManager.getClass().getField("STREAM_DTMF") != null ? true : LOCK_SKIP_INITIALIZED;
        } catch (SecurityException e3) {
            this.supportsDTMF = false;
        } catch (NoSuchFieldException e4) {
            this.supportsDTMF = false;
        }
        if (this.pHelper.getPersistVolumeLevels()) {
            this.volumeMapping = this.pHelper.getPersistedVolumeLevels();
            if (this.volumeMapping.size() == 0) {
                this.persistInitLoadedVolumeMapping = true;
            } else {
                this.persistInitLoadedVolumeMapping = false;
            }
        }
        startTimer();
        try {
            cancelNotification(true);
        } catch (Exception e5) {
            Log.e(TAG, "Error cancelling the notification during start up...");
        }
        Log.d(TAG, "VLService Started");
    }

    public void onStart(Intent intent, int startId) {
        Log.d(TAG, "Starting service... pre-2.0 caller");
        startService(intent, startId);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting service... 2.0+ caller");
        startService(intent, startId);
        return 1;
    }

    private void startService(Intent intent, int startId) {
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            int changes = LOCK_SKIP_INITIALIZED;
            if (bundle.containsKey(BUNDLE_SAVE_VOLUME_LEVEL_CHANGES)) {
                this.saveVolumeLevelChanges.getAndSet(true);
                this.revertVolumeLevelChanges.getAndSet(false);
                changes = LOCK_SKIP_INITIALIZED + 1;
            }
            if (bundle.containsKey(BUNDLE_REVERT_VOLUME_LEVEL_CHANGES)) {
                this.revertVolumeLevelChanges.getAndSet(true);
                this.saveVolumeLevelChanges.getAndSet(false);
            }
            if (bundle.containsKey(BUNDLE_EXTRA_HEADPHONES_ON)) {
                this.headphonesOn.getAndSet(bundle.getBoolean(BUNDLE_EXTRA_HEADPHONES_ON));
                changes++;
            }
            if (bundle.containsKey(BUNDLE_VL_IGNORE_CHANGE_RECEIVED)) {
                if (this.broadcastChangesReceived == null || this.serviceRunning == null || !this.serviceRunning.get()) {
                    Log.d(TAG, "BroadcastChangesRecieved but ignored since the service isn't running.");
                } else {
                    Log.d(TAG, "BroadcastChangesRecieved: " + this.broadcastChangesReceived.incrementAndGet());
                }
                changes++;
            }
            if (changes > 0) {
                Log.d(TAG, "Recieved intent messaging, ignoring start...");
            }
        } else if (!this.serviceRunning.get()) {
            onCreate();
        }
    }

    private void stopTimer() {
        if (this.timer != null) {
            try {
                this.timer.cancel();
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "IllegalArgumentException trying to stop the timer..." + e.getMessage());
                stopTimer();
            } catch (IllegalStateException e2) {
                Log.e(TAG, "IllegalStateException trying to stop the timer..." + e2.getMessage());
                stopTimer();
            } finally {
                this.timer = null;
            }
        }
    }

    private void startTimer() {
        if (this.timer == null) {
            try {
                this.timer = new Timer();
                this.timer.schedule(new TimerTask() {
                    public void run() {
                        VLService.this.runBackgroundTask();
                    }
                }, 20, (long) UPDATE_INTERVAL);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "IllegalArgumentException trying to start the timer..." + e.getMessage());
                stopTimer();
            } catch (IllegalStateException e2) {
                Log.e(TAG, "IllegalStateException trying to start the timer..." + e2.getMessage());
                stopTimer();
            }
            Log.d(TAG, "Timer Starting");
        }
    }

    public void onDestroy() {
        Log.d(TAG, "Service shutting down...");
        notifyWidgetOfUpdate(false);
        stopTimer();
        try {
            resetVolumeToBeans();
        } catch (Exception e) {
            Log.e(TAG, "Error resetting volume to beans in shutdown...");
        }
        wakeLockStop();
        if (this.volumeMapping != null) {
            this.volumeMapping.clear();
        }
        try {
            cancelNotification(true);
        } catch (Exception e2) {
            Log.e(TAG, "Error cancelling the notification during shutdown...");
        }
        this.persistInitLoadedVolumeMapping = false;
        this.supportsDTMF = false;
        this.notification = null;
        this.notificationContentIntent = null;
        this.notificationPopped = false;
        this.audioManager = null;
        this.activityManager = null;
        this.notificationManager = null;
        this.telephonyManager = null;
        this.keyguardManager = null;
        this.vibratorService = null;
        this.powerManager = null;
        resetSystemTimeoutCounter();
        this.notificationTimeoutDurationSetting = 30;
        this.notificationLEDColorSetting = LOCK_SKIP_INITIALIZED;
        this.notificationLEDColor = -1;
        this.notificationRingerURI = null;
        try {
            if (this.headphonesReceiver != null) {
                unregisterReceiver(this.headphonesReceiver);
            }
        } catch (Exception e3) {
            Log.e(TAG, "Error removing headphones receiver during shutdown...");
        } finally {
            this.headphonesReceiver = null;
        }
        this.systemTimeoutCounter = TIMEOUT_COUNTER_OFF;
        this.systemTotalChangedCounter = LOCK_SKIP_INITIALIZED;
        this.lockSkipOccurred = LOCK_SKIP_INITIALIZED;
        this.broadcastReceivedDelayCounter = LOCK_SKIP_INITIALIZED;
        this.cameraClosedDelayCounter = LOCK_SKIP_INITIALIZED;
        this.saveVolumeLevelChanges.getAndSet(false);
        this.revertVolumeLevelChanges.getAndSet(false);
        this.broadcastChangesReceived.getAndSet(LOCK_SKIP_INITIALIZED);
        this.headphonesOn.getAndSet(false);
        if (this.mCallbacks != null) {
            this.mCallbacks.kill();
        }
        if (this.mHandler != null) {
            this.mHandler.removeMessages(1);
        }
        this.serviceRunning.getAndSet(false);
        Log.d(TAG, "Service Shut Down");
        super.onDestroy();
    }

    private void notifyWidgetOfUpdate(boolean enabled) {
        RemoteViews views = new RemoteViews(getPackageName(), (int) R.layout.enable_disable_widget_layout);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        if (enabled) {
            views.setImageViewResource(R.id.EnableDisableWidgetIcon, R.drawable.enabled_icon);
        } else {
            views.setImageViewResource(R.id.EnableDisableWidgetIcon, R.drawable.disabled_icon);
        }
        appWidgetManager.updateAppWidget(new ComponentName(getPackageName(), EnableDisableWidget.class.getName()), views);
    }

    private void wakeLockStart() {
        if (this.wakeLock == null && this.powerManager != null) {
            this.wakeLock = this.powerManager.newWakeLock(268435457, TAG);
            this.wakeLock.acquire();
            Log.d(TAG, "Wake Lock Acquired");
        }
    }

    private void wakeLockStop() {
        if (this.wakeLock != null) {
            Log.d(TAG, "Releasing Wake Lock...");
            this.wakeLock.release();
            this.wakeLock = null;
            Log.d(TAG, "Wake Lock Released");
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void runBackgroundTask() {
        /*
            r57 = this;
            r0 = r57
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.serviceRunning     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x0020
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x0020
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getEnableVLS()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x002b
        L_0x0020:
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Background task shutting down the service..."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r57.stopSelf()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x002a:
            return
        L_0x002b:
            r0 = r57
            java.util.concurrent.atomic.AtomicInteger r0 = r0.broadcastChangesReceived     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x0055
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getAllowOtherAppsToOverride()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x0156
            r0 = r57
            java.util.concurrent.atomic.AtomicInteger r0 = r0.broadcastChangesReceived     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 0
            r52.getAndSet(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Broadcast receiver ignored due to settings."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0055:
            r0 = r57
            android.app.KeyguardManager r0 = r0.keyguardManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x01d3
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableDuringScreenLock()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x01d3
            r0 = r57
            android.app.KeyguardManager r0 = r0.keyguardManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.inKeyguardRestrictedInputMode()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x01d3
            r52 = 1
            r26 = r52
        L_0x0079:
            if (r26 == 0) goto L_0x01d9
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableDuringScreenLockSaveChanges()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x01d9
            r0 = r57
            int r0 = r0.systemTimeoutCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = -5
            r0 = r52
            r1 = r53
            if (r0 == r1) goto L_0x002a
            r0 = r57
            int r0 = r0.lockSkipOccurred     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 2
            r0 = r52
            r1 = r53
            if (r0 == r1) goto L_0x00ab
            r52 = 2
            r0 = r52
            r1 = r57
            r1.lockSkipOccurred = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x00ab:
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 != 0) goto L_0x00cb
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Background Task -- Audio Manager was null, re-requesting."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "audio"
            r0 = r57
            r1 = r52
            java.lang.Object r5 = r0.getSystemService(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            android.media.AudioManager r5 = (android.media.AudioManager) r5     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r5
            r1 = r57
            r1.audioManager = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x00cb:
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x002a
            r0 = r57
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.saveVolumeLevelChanges     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x00eb
            r0 = r57
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.revertVolumeLevelChanges     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x0204
        L_0x00eb:
            r52 = 0
            r0 = r57
            r1 = r52
            r0.cancelNotification(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r26 == 0) goto L_0x010c
            r0 = r57
            int r0 = r0.lockSkipOccurred     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 1
            r0 = r52
            r1 = r53
            if (r0 != r1) goto L_0x010c
            r52 = 2
            r0 = r52
            r1 = r57
            r1.lockSkipOccurred = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x010c:
            r0 = r57
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.saveVolumeLevelChanges     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x01ff
            r57.resetBeansToVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x011b:
            r0 = r57
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.saveVolumeLevelChanges     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 0
            r52.getAndSet(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.revertVolumeLevelChanges     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 0
            r52.getAndSet(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r57.wakeLockStop()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x002a
        L_0x0136:
            r52 = move-exception
            r34 = r52
            java.lang.String r52 = "VLService"
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = "NullPointer Exception! "
            r53.<init>(r54)
            java.lang.String r54 = r34.getMessage()
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r53 = r53.toString()
            android.util.Log.e(r52, r53)
            r34.printStackTrace()
            goto L_0x002a
        L_0x0156:
            r57.wakeLockStart()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.broadcastReceivedDelayCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52 + 1
            r0 = r52
            r1 = r57
            r1.broadcastReceivedDelayCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.broadcastReceivedDelayCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 7
            r0 = r52
            r1 = r53
            if (r0 <= r1) goto L_0x0055
            r0 = r57
            java.util.concurrent.atomic.AtomicInteger r0 = r0.broadcastChangesReceived     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x018a
            r0 = r57
            java.util.concurrent.atomic.AtomicInteger r0 = r0.broadcastChangesReceived     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r52.decrementAndGet()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x018a:
            r52 = 0
            r0 = r52
            r1 = r57
            r1.broadcastReceivedDelayCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.concurrent.atomic.AtomicInteger r0 = r0.broadcastChangesReceived     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x01ca
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Broadcast receiver timeout occurred."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r57.wakeLockStop()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x0055
        L_0x01aa:
            r52 = move-exception
            r24 = r52
            java.lang.String r52 = "VLService"
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = "Runtime Exception! "
            r53.<init>(r54)
            java.lang.String r54 = r24.getMessage()
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r53 = r53.toString()
            android.util.Log.e(r52, r53)
            r24.printStackTrace()
            goto L_0x002a
        L_0x01ca:
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Broadcast receiver timeout occurred, however another is lined up. Restarting timeout..."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x0055
        L_0x01d3:
            r52 = 0
            r26 = r52
            goto L_0x0079
        L_0x01d9:
            if (r26 == 0) goto L_0x01ed
            r0 = r57
            int r0 = r0.lockSkipOccurred     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 != 0) goto L_0x00ab
            r52 = 1
            r0 = r52
            r1 = r57
            r1.lockSkipOccurred = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x00ab
        L_0x01ed:
            r0 = r57
            int r0 = r0.lockSkipOccurred     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x00ab
            r52 = 0
            r0 = r52
            r1 = r57
            r1.lockSkipOccurred = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x00ab
        L_0x01ff:
            r57.resetVolumeToBeans()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x011b
        L_0x0204:
            java.lang.StringBuffer r14 = new java.lang.StringBuffer     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r14.<init>()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 0
            r0 = r52
            r1 = r57
            r1.systemTotalChangedCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r9 = 0
            r10 = 0
            r38 = 0
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getLockRingerVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x032f
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 2
            int r40 = r52.getStreamVolume(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r39 = r52.getRingerMode()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 2
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.Object r37 = r52.get(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            com.hourdb.volumelocker.model.VolumeBean r37 = (com.hourdb.volumelocker.model.VolumeBean) r37     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r37 != 0) goto L_0x026f
            com.hourdb.volumelocker.model.VolumeBean r37 = new com.hourdb.volumelocker.model.VolumeBean     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 2
            r0 = r37
            r1 = r52
            r2 = r40
            r3 = r39
            r0.<init>(r1, r2, r3)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 2
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r52
            r1 = r53
            r2 = r37
            r0.put(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x026f:
            r0 = r37
            r1 = r40
            r2 = r39
            boolean r52 = r0.hasChanged(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x032f
            r36 = 1
            if (r40 != 0) goto L_0x02e0
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getAutoSaveSilentMode()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x02e0
            r49 = 0
            android.content.Context r52 = r57.getApplicationContext()     // Catch:{ Exception -> 0x07ce }
            android.content.ContentResolver r52 = r52.getContentResolver()     // Catch:{ Exception -> 0x07ce }
            java.lang.String r53 = "vibrate_in_silent"
            r54 = 0
            int r49 = android.provider.Settings.System.getInt(r52, r53, r54)     // Catch:{ Exception -> 0x07ce }
        L_0x029d:
            if (r39 == 0) goto L_0x02af
            r52 = 1
            r0 = r39
            r1 = r52
            if (r0 != r1) goto L_0x02e0
            r52 = 1
            r0 = r49
            r1 = r52
            if (r0 != r1) goto L_0x02e0
        L_0x02af:
            r0 = r37
            r1 = r40
            r0.setStreamVolume(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r37
            r1 = r39
            r0.setStreamMode(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            android.content.Context r52 = r57.getApplicationContext()     // Catch:{ Exception -> 0x07dd }
            android.content.ContentResolver r52 = r52.getContentResolver()     // Catch:{ Exception -> 0x07dd }
            java.lang.String r53 = "notifications_use_ring_volume"
            r54 = 1
            int r27 = android.provider.Settings.System.getInt(r52, r53, r54)     // Catch:{ Exception -> 0x07dd }
            r52 = 1
            r0 = r27
            r1 = r52
            if (r0 != r1) goto L_0x02d6
            r9 = 1
        L_0x02d6:
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Auto Saved Ringer Volume -- Auto Save Silent Mode Setting Enabled"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r10 = 1
            r36 = 0
        L_0x02e0:
            if (r36 == 0) goto L_0x030c
            r0 = r57
            android.telephony.TelephonyManager r0 = r0.telephonyManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x030c
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableRingerVolumeLockWhileRinging()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x030c
            r0 = r57
            android.telephony.TelephonyManager r0 = r0.telephonyManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52.getCallState()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r53 = 1
            r0 = r52
            r1 = r53
            if (r0 != r1) goto L_0x030c
            r36 = 0
            r38 = 1
        L_0x030c:
            if (r36 == 0) goto L_0x032f
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedRinger     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.systemTotalChangedCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52 + 1
            r0 = r52
            r1 = r57
            r1.systemTotalChangedCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Updating the ringer changed"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x032f:
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getLockAlarmVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x03b9
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 4
            int r6 = r52.getStreamVolume(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 4
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.Object r5 = r52.get(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            com.hourdb.volumelocker.model.VolumeBean r5 = (com.hourdb.volumelocker.model.VolumeBean) r5     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r5 != 0) goto L_0x037a
            com.hourdb.volumelocker.model.VolumeBean r5 = new com.hourdb.volumelocker.model.VolumeBean     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 4
            r0 = r5
            r1 = r52
            r2 = r6
            r0.<init>(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 4
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r52
            r1 = r53
            r2 = r5
            r0.put(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x037a:
            r52 = -1
            r0 = r5
            r1 = r6
            r2 = r52
            boolean r52 = r0.hasChanged(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x03b9
            int r52 = r14.length()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x0398
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedDelimiter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0398:
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedAlarm     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.systemTotalChangedCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52 + 1
            r0 = r52
            r1 = r57
            r1.systemTotalChangedCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Updating the alarm changed counter"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x03b9:
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getLockMusicVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x0460
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 3
            int r30 = r52.getStreamVolume(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 3
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.Object r29 = r52.get(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            com.hourdb.volumelocker.model.VolumeBean r29 = (com.hourdb.volumelocker.model.VolumeBean) r29     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r29 != 0) goto L_0x0407
            com.hourdb.volumelocker.model.VolumeBean r29 = new com.hourdb.volumelocker.model.VolumeBean     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 3
            r0 = r29
            r1 = r52
            r2 = r30
            r0.<init>(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 3
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r52
            r1 = r53
            r2 = r29
            r0.put(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0407:
            r52 = -1
            r0 = r29
            r1 = r30
            r2 = r52
            boolean r52 = r0.hasChanged(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x0460
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableMusicVolumeLockWhileHeadphonesIn()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x042d
            r0 = r57
            java.util.concurrent.atomic.AtomicBoolean r0 = r0.headphonesOn     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x07ec
        L_0x042d:
            int r52 = r14.length()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x043f
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedDelimiter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x043f:
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedMusic     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.systemTotalChangedCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52 + 1
            r0 = r52
            r1 = r57
            r1.systemTotalChangedCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Updating the music changed counter"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0460:
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getLockNotificationVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x04ec
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 5
            int r33 = r52.getStreamVolume(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 5
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.Object r31 = r52.get(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            com.hourdb.volumelocker.model.VolumeBean r31 = (com.hourdb.volumelocker.model.VolumeBean) r31     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r31 != 0) goto L_0x04ae
            com.hourdb.volumelocker.model.VolumeBean r31 = new com.hourdb.volumelocker.model.VolumeBean     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 5
            r0 = r31
            r1 = r52
            r2 = r33
            r0.<init>(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 5
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r52
            r1 = r53
            r2 = r31
            r0.put(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x04ae:
            r52 = -1
            r0 = r31
            r1 = r33
            r2 = r52
            boolean r52 = r0.hasChanged(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x04ec
            r36 = 1
            if (r9 != 0) goto L_0x04dc
            if (r38 == 0) goto L_0x04dc
            android.content.Context r52 = r57.getApplicationContext()     // Catch:{ Exception -> 0x07f5 }
            android.content.ContentResolver r52 = r52.getContentResolver()     // Catch:{ Exception -> 0x07f5 }
            java.lang.String r53 = "notifications_use_ring_volume"
            r54 = 1
            int r27 = android.provider.Settings.System.getInt(r52, r53, r54)     // Catch:{ Exception -> 0x07f5 }
            r52 = 1
            r0 = r27
            r1 = r52
            if (r0 != r1) goto L_0x04dc
            r36 = 0
        L_0x04dc:
            if (r9 == 0) goto L_0x0804
            r0 = r31
            r1 = r33
            r0.setStreamVolume(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Auto Saved Notification Volume -- Auto Save Silent Mode Setting Enabled"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x04ec:
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getLockSystemVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x054d
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 1
            int r45 = r52.getStreamVolume(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 1
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.Object r44 = r52.get(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            com.hourdb.volumelocker.model.VolumeBean r44 = (com.hourdb.volumelocker.model.VolumeBean) r44     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r44 != 0) goto L_0x053a
            com.hourdb.volumelocker.model.VolumeBean r44 = new com.hourdb.volumelocker.model.VolumeBean     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 1
            r0 = r44
            r1 = r52
            r2 = r45
            r0.<init>(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 1
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r52
            r1 = r53
            r2 = r44
            r0.put(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x053a:
            r52 = -1
            r0 = r44
            r1 = r45
            r2 = r52
            boolean r52 = r0.hasChanged(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x054d
            if (r10 == 0) goto L_0x083b
            r44.setStreamVolume(r45)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x054d:
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getLockVoiceCallVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x05f6
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 0
            int r51 = r52.getStreamVolume(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 0
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.Object r50 = r52.get(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            com.hourdb.volumelocker.model.VolumeBean r50 = (com.hourdb.volumelocker.model.VolumeBean) r50     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r50 != 0) goto L_0x059b
            com.hourdb.volumelocker.model.VolumeBean r50 = new com.hourdb.volumelocker.model.VolumeBean     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 0
            r0 = r50
            r1 = r52
            r2 = r51
            r0.<init>(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 0
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r52
            r1 = r53
            r2 = r50
            r0.put(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x059b:
            r52 = -1
            boolean r52 = r50.hasChanged(r51, r52)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x05f6
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableVoiceCallVolumeLockDuringCall()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x05c3
            r0 = r57
            android.telephony.TelephonyManager r0 = r0.telephonyManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x0870
            r0 = r57
            android.telephony.TelephonyManager r0 = r0.telephonyManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52.getCallState()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x0870
        L_0x05c3:
            int r52 = r14.length()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x05d5
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedDelimiter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x05d5:
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedVoiceCall     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.systemTotalChangedCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52 + 1
            r0 = r52
            r1 = r57
            r1.systemTotalChangedCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Updating the voice call changed counter"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x05f6:
            r0 = r57
            boolean r0 = r0.supportsDTMF     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x06ad
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getLockDTMFVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x06ad
            r0 = r57
            android.media.AudioManager r0 = r0.audioManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 8
            int r23 = r52.getStreamVolume(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 8
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.Object r22 = r52.get(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            com.hourdb.volumelocker.model.VolumeBean r22 = (com.hourdb.volumelocker.model.VolumeBean) r22     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r22 != 0) goto L_0x064c
            com.hourdb.volumelocker.model.VolumeBean r22 = new com.hourdb.volumelocker.model.VolumeBean     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 8
            r0 = r22
            r1 = r52
            r2 = r23
            r0.<init>(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 8
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r52
            r1 = r53
            r2 = r22
            r0.put(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x064c:
            r52 = -1
            r0 = r22
            r1 = r23
            r2 = r52
            boolean r52 = r0.hasChanged(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x06ad
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableDTMFVolumeLockDuringCall()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x067a
            r0 = r57
            android.telephony.TelephonyManager r0 = r0.telephonyManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x0879
            r0 = r57
            android.telephony.TelephonyManager r0 = r0.telephonyManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52.getCallState()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x0879
        L_0x067a:
            int r52 = r14.length()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x068c
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedDelimiter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x068c:
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedDTMF     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.systemTotalChangedCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52 + 1
            r0 = r52
            r1 = r57
            r1.systemTotalChangedCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Updating the DTMF changed counter"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x06ad:
            r0 = r57
            boolean r0 = r0.persistInitLoadedVolumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x06e5
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Persisting volume mapping..."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r52.edit()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r57
            java.util.HashMap<java.lang.Integer, com.hourdb.volumelocker.model.VolumeBean> r0 = r0.volumeMapping     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r53 = r0
            r52.setPersistedVolumeLevels(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r52.commit()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 0
            r0 = r52
            r1 = r57
            r1.persistInitLoadedVolumeMapping = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x06e5:
            r0 = r57
            int r0 = r0.systemTotalChangedCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 <= 0) goto L_0x0dd8
            r0 = r57
            android.app.ActivityManager r0 = r0.activityManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x0741
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableDuringGoogleNavigation()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x070d
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableDuringBedside()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x0741
        L_0x070d:
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r20 = r52.getDisableDuringGoogleNavigation()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r17 = r52.getDisableDuringBedside()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r43 = 0
            r0 = r57
            android.app.ActivityManager r0 = r0.activityManager     // Catch:{ SecurityException -> 0x08e4 }
            r52 = r0
            r53 = 100
            java.util.List r43 = r52.getRunningServices(r53)     // Catch:{ SecurityException -> 0x08e4 }
            if (r43 == 0) goto L_0x073c
            r15 = 0
            java.util.Iterator r52 = r43.iterator()     // Catch:{ SecurityException -> 0x08e4 }
        L_0x0736:
            boolean r53 = r52.hasNext()     // Catch:{ SecurityException -> 0x08e4 }
            if (r53 != 0) goto L_0x0882
        L_0x073c:
            if (r43 == 0) goto L_0x0741
            r43.clear()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0741:
            r0 = r57
            android.app.ActivityManager r0 = r0.activityManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x09c2
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableDuringCameraUsage()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x0761
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getDisableDuringGoogleVoice()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x09c2
        L_0x0761:
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r18 = r52.getDisableDuringCameraUsage()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r19 = r52.getDisableDuringCameraUsageDelay()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r21 = r52.getDisableDuringGoogleVoice()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r8 = 0
            r0 = r57
            android.app.ActivityManager r0 = r0.activityManager     // Catch:{ SecurityException -> 0x0992 }
            r52 = r0
            java.util.List r8 = r52.getRunningAppProcesses()     // Catch:{ SecurityException -> 0x0992 }
            r13 = 0
            r12 = 0
            r25 = 0
            if (r8 == 0) goto L_0x079a
            java.util.Iterator r52 = r8.iterator()     // Catch:{ SecurityException -> 0x0992 }
        L_0x0794:
            boolean r53 = r52.hasNext()     // Catch:{ SecurityException -> 0x0992 }
            if (r53 != 0) goto L_0x0911
        L_0x079a:
            if (r13 != 0) goto L_0x07b5
            r0 = r57
            int r0 = r0.cameraClosedDelayCounter     // Catch:{ SecurityException -> 0x0992 }
            r52 = r0
            if (r52 <= 0) goto L_0x07b5
            r0 = r57
            int r0 = r0.cameraClosedDelayCounter     // Catch:{ SecurityException -> 0x0992 }
            r52 = r0
            r53 = 1
            int r52 = r52 - r53
            r0 = r52
            r1 = r57
            r1.cameraClosedDelayCounter = r0     // Catch:{ SecurityException -> 0x0992 }
            r13 = 1
        L_0x07b5:
            if (r13 == 0) goto L_0x09bd
            r52 = 0
            r0 = r57
            r1 = r52
            r0.cancelNotification(r1)     // Catch:{ SecurityException -> 0x0992 }
            r57.resetSystemTimeoutCounter()     // Catch:{ SecurityException -> 0x0992 }
            r57.wakeLockStop()     // Catch:{ SecurityException -> 0x0992 }
            if (r8 == 0) goto L_0x002a
            r8.clear()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r8 = 0
            goto L_0x002a
        L_0x07ce:
            r52 = move-exception
            r24 = r52
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Error occurred trying to get the linked ringer/notification settings."
            android.util.Log.e(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r24.printStackTrace()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x029d
        L_0x07dd:
            r52 = move-exception
            r24 = r52
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Error occurred trying to get the linked ringer/notification settings."
            android.util.Log.e(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r24.printStackTrace()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x02d6
        L_0x07ec:
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Couldn't update music changed counter, headphone jack is in use"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x0460
        L_0x07f5:
            r52 = move-exception
            r24 = r52
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Error occurred trying to get the linked ringer/notification settings."
            android.util.Log.e(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r24.printStackTrace()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x04dc
        L_0x0804:
            if (r36 == 0) goto L_0x04ec
            int r52 = r14.length()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x0818
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedDelimiter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0818:
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedNotification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.systemTotalChangedCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52 + 1
            r0 = r52
            r1 = r57
            r1.systemTotalChangedCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Updating the notification changed counter"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x04ec
        L_0x083b:
            int r52 = r14.length()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x084d
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedDelimiter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x084d:
            r0 = r57
            java.lang.String r0 = r0.stringNotificationChangedSystem     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r14
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.systemTotalChangedCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52 + 1
            r0 = r52
            r1 = r57
            r1.systemTotalChangedCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Updating the system changed counter"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x054d
        L_0x0870:
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Couldn't update voice call changed counter, call in progress"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x05f6
        L_0x0879:
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Couldn't update DTMF changed counter, call in progress"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x06ad
        L_0x0882:
            java.lang.Object r42 = r52.next()     // Catch:{ SecurityException -> 0x08e4 }
            android.app.ActivityManager$RunningServiceInfo r42 = (android.app.ActivityManager.RunningServiceInfo) r42     // Catch:{ SecurityException -> 0x08e4 }
            r0 = r42
            android.content.ComponentName r0 = r0.service     // Catch:{ SecurityException -> 0x08e4 }
            r53 = r0
            java.lang.String r15 = r53.getClassName()     // Catch:{ SecurityException -> 0x08e4 }
            if (r20 == 0) goto L_0x08ad
            r0 = r42
            java.lang.String r0 = r0.process     // Catch:{ SecurityException -> 0x08e4 }
            r53 = r0
            java.lang.String r54 = "com.google.android.apps.maps:driveabout"
            boolean r53 = r53.equalsIgnoreCase(r54)     // Catch:{ SecurityException -> 0x08e4 }
            if (r53 == 0) goto L_0x08ad
            java.lang.String r53 = "com.google.android.maps.driveabout.app.NavigationService"
            r0 = r15
            r1 = r53
            boolean r53 = r0.equalsIgnoreCase(r1)     // Catch:{ SecurityException -> 0x08e4 }
            if (r53 != 0) goto L_0x08c5
        L_0x08ad:
            if (r17 == 0) goto L_0x0736
            java.lang.String r53 = "net.geekherd.bedsidepro2.services.NotificationsServiceCupcake"
            r0 = r15
            r1 = r53
            boolean r53 = r0.equalsIgnoreCase(r1)     // Catch:{ SecurityException -> 0x08e4 }
            if (r53 != 0) goto L_0x08c5
            java.lang.String r53 = "net.geekherd.bedsidepro2.services.NotificationsService"
            r0 = r15
            r1 = r53
            boolean r53 = r0.equalsIgnoreCase(r1)     // Catch:{ SecurityException -> 0x08e4 }
            if (r53 == 0) goto L_0x0736
        L_0x08c5:
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Navigation service in progress...cancelling out."
            android.util.Log.d(r52, r53)     // Catch:{ SecurityException -> 0x08e4 }
            r52 = 0
            r0 = r57
            r1 = r52
            r0.cancelNotification(r1)     // Catch:{ SecurityException -> 0x08e4 }
            r57.resetSystemTimeoutCounter()     // Catch:{ SecurityException -> 0x08e4 }
            r57.wakeLockStop()     // Catch:{ SecurityException -> 0x08e4 }
            if (r43 == 0) goto L_0x002a
            r43.clear()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r43 = 0
            goto L_0x002a
        L_0x08e4:
            r52 = move-exception
            r24 = r52
            java.lang.String r52 = "VLService"
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ all -> 0x0908 }
            java.lang.String r54 = "Security exception retrieving service info... "
            r53.<init>(r54)     // Catch:{ all -> 0x0908 }
            java.lang.String r54 = r24.getMessage()     // Catch:{ all -> 0x0908 }
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ all -> 0x0908 }
            java.lang.String r53 = r53.toString()     // Catch:{ all -> 0x0908 }
            android.util.Log.e(r52, r53)     // Catch:{ all -> 0x0908 }
            if (r43 == 0) goto L_0x002a
            r43.clear()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r43 = 0
            goto L_0x002a
        L_0x0908:
            r52 = move-exception
            if (r43 == 0) goto L_0x0910
            r43.clear()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r43 = 0
        L_0x0910:
            throw r52     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0911:
            java.lang.Object r7 = r52.next()     // Catch:{ SecurityException -> 0x0992 }
            android.app.ActivityManager$RunningAppProcessInfo r7 = (android.app.ActivityManager.RunningAppProcessInfo) r7     // Catch:{ SecurityException -> 0x0992 }
            r0 = r7
            java.lang.String r0 = r0.processName     // Catch:{ SecurityException -> 0x0992 }
            r53 = r0
            if (r53 == 0) goto L_0x0794
            if (r18 == 0) goto L_0x0988
            r0 = r57
            java.util.regex.Pattern r0 = r0.cameraRegexPattern     // Catch:{ SecurityException -> 0x0992 }
            r53 = r0
            if (r53 == 0) goto L_0x0988
            r0 = r57
            java.util.regex.Pattern r0 = r0.cameraRegexPattern     // Catch:{ SecurityException -> 0x0992 }
            r53 = r0
            r0 = r7
            java.lang.String r0 = r0.processName     // Catch:{ SecurityException -> 0x0992 }
            r54 = r0
            java.util.regex.Matcher r53 = r53.matcher(r54)     // Catch:{ SecurityException -> 0x0992 }
            boolean r53 = r53.matches()     // Catch:{ SecurityException -> 0x0992 }
            if (r53 == 0) goto L_0x0988
            r53 = 1
            r12 = r53
        L_0x0941:
            if (r21 == 0) goto L_0x098d
            r0 = r7
            java.lang.String r0 = r0.processName     // Catch:{ SecurityException -> 0x0992 }
            r53 = r0
            java.lang.String r54 = "com.google.android.apps.googlevoice"
            boolean r53 = r53.equalsIgnoreCase(r54)     // Catch:{ SecurityException -> 0x0992 }
            if (r53 == 0) goto L_0x098d
            r53 = 1
            r25 = r53
        L_0x0954:
            if (r12 != 0) goto L_0x0958
            if (r25 == 0) goto L_0x0794
        L_0x0958:
            r0 = r7
            int r0 = r0.importance     // Catch:{ SecurityException -> 0x0992 }
            r53 = r0
            r54 = 100
            r0 = r53
            r1 = r54
            if (r0 == r1) goto L_0x0972
            r0 = r7
            int r0 = r0.importance     // Catch:{ SecurityException -> 0x0992 }
            r53 = r0
            r54 = 200(0xc8, float:2.8E-43)
            r0 = r53
            r1 = r54
            if (r0 != r1) goto L_0x0794
        L_0x0972:
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Google Voice or Camera app is in foreground...cancelling out."
            android.util.Log.d(r52, r53)     // Catch:{ SecurityException -> 0x0992 }
            if (r19 == 0) goto L_0x0985
            if (r12 == 0) goto L_0x0985
            r52 = 2
            r0 = r52
            r1 = r57
            r1.cameraClosedDelayCounter = r0     // Catch:{ SecurityException -> 0x0992 }
        L_0x0985:
            r13 = 1
            goto L_0x079a
        L_0x0988:
            r53 = 0
            r12 = r53
            goto L_0x0941
        L_0x098d:
            r53 = 0
            r25 = r53
            goto L_0x0954
        L_0x0992:
            r52 = move-exception
            r24 = r52
            java.lang.String r52 = "VLService"
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ all -> 0x09b5 }
            java.lang.String r54 = "Security exception retrieving running applications... "
            r53.<init>(r54)     // Catch:{ all -> 0x09b5 }
            java.lang.String r54 = r24.getMessage()     // Catch:{ all -> 0x09b5 }
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ all -> 0x09b5 }
            java.lang.String r53 = r53.toString()     // Catch:{ all -> 0x09b5 }
            android.util.Log.e(r52, r53)     // Catch:{ all -> 0x09b5 }
            if (r8 == 0) goto L_0x002a
            r8.clear()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r8 = 0
            goto L_0x002a
        L_0x09b5:
            r52 = move-exception
            if (r8 == 0) goto L_0x09bc
            r8.clear()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r8 = 0
        L_0x09bc:
            throw r52     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x09bd:
            if (r8 == 0) goto L_0x09c2
            r8.clear()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x09c2:
            r0 = r57
            java.util.concurrent.atomic.AtomicInteger r0 = r0.broadcastChangesReceived     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x0a2e
            r52 = 0
            r0 = r57
            r1 = r52
            r0.cancelNotification(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r57.resetBeansToVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Saved the volume settings due to broadcast receiver."
            android.util.Log.i(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.util.concurrent.atomic.AtomicInteger r0 = r0.broadcastChangesReceived     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52.get()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r53 = 1
            r0 = r52
            r1 = r53
            if (r0 <= r1) goto L_0x002a
            r0 = r57
            java.util.concurrent.atomic.AtomicInteger r0 = r0.broadcastChangesReceived     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r47 = r52.decrementAndGet()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r54 = "Decremented the broadcast receiver counter to: "
            r53.<init>(r54)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r53
            r1 = r47
            java.lang.StringBuilder r53 = r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r53 = r53.toString()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 1
            r0 = r47
            r1 = r52
            if (r0 != r1) goto L_0x002a
            r52 = 0
            r0 = r52
            r1 = r57
            r1.broadcastReceivedDelayCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Reset the broadcast received delay counter, since 1 remains..."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x002a
        L_0x0a2e:
            if (r26 == 0) goto L_0x0a53
            r0 = r57
            int r0 = r0.systemTimeoutCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = -5
            r0 = r52
            r1 = r53
            if (r0 != r1) goto L_0x0a53
            r0 = r57
            int r0 = r0.lockSkipOccurred     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 1
            r0 = r52
            r1 = r53
            if (r0 != r1) goto L_0x0c8f
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Skipping, lock skip occurring..."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0a53:
            r57.wakeLockStart()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.notificationTimeoutDurationSetting     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 <= 0) goto L_0x0dca
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Showing Notification"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r48 = r52.getNotificationPopup()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 != 0) goto L_0x0bb2
            r0 = r57
            boolean r0 = r0.notificationPopped     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 != 0) goto L_0x0bb2
            if (r48 != 0) goto L_0x0d03
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Inializing Notification..."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            android.app.Notification r52 = new android.app.Notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r53 = 2130837507(0x7f020003, float:1.727997E38)
            r0 = r57
            java.lang.String r0 = r0.stringNotificationTickerText     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r54 = r0
            long r55 = java.lang.System.currentTimeMillis()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52.<init>(r53, r54, r55)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r52
            r1 = r57
            r1.notification = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.notificationLEDColorSetting     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 <= 0) goto L_0x0cd0
            r0 = r57
            int r0 = r0.notificationLEDColorSetting     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 1
            r0 = r52
            r1 = r53
            if (r0 <= r1) goto L_0x0caa
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r57
            int r0 = r0.notificationLEDColor     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r53 = r0
            r0 = r53
            r1 = r52
            r1.ledARGB = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Notification: Custom LED Color On"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0acf:
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 500(0x1f4, float:7.0E-43)
            r0 = r53
            r1 = r52
            r1.ledOnMS = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 500(0x1f4, float:7.0E-43)
            r0 = r53
            r1 = r52
            r1.ledOffMS = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 3
            r0 = r53
            r1 = r52
            r1.flags = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0af9:
            android.content.Intent r32 = new android.content.Intent     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.Class<com.hourdb.volumelocker.CommitVolumes> r52 = com.hourdb.volumelocker.CommitVolumes.class
            r0 = r32
            r1 = r57
            r2 = r52
            r0.<init>(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 1350893568(0x50850000, float:1.78509578E10)
            r0 = r32
            r1 = r52
            r0.addFlags(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 0
            r53 = 0
            r0 = r57
            r1 = r52
            r2 = r32
            r3 = r53
            android.app.PendingIntent r52 = android.app.PendingIntent.getActivity(r0, r1, r2, r3)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r52
            r1 = r57
            r1.notificationContentIntent = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0b25:
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getNotificationRingVibrateOnlyUnlocked()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x0b51
            r0 = r57
            android.app.KeyguardManager r0 = r0.keyguardManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x0bb2
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getNotificationRingVibrateOnlyUnlocked()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x0bb2
            r0 = r57
            android.app.KeyguardManager r0 = r0.keyguardManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.inKeyguardRestrictedInputMode()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 != 0) goto L_0x0bb2
        L_0x0b51:
            r0 = r57
            android.os.Vibrator r0 = r0.vibratorService     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x0b77
            r0 = r57
            com.hourdb.volumelocker.helper.PreferencesHelper r0 = r0.pHelper     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            boolean r52 = r52.getNotificationVibrate()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 == 0) goto L_0x0b77
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Vibrating..."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.os.Vibrator r0 = r0.vibratorService     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 200(0xc8, double:9.9E-322)
            r52.vibrate(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0b77:
            r0 = r57
            java.lang.String r0 = r0.notificationRingerURI     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x0bb2
            r0 = r57
            java.lang.String r0 = r0.notificationRingerURI     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            int r52 = r52.length()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r52 <= 0) goto L_0x0bb2
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Ringing..."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.lang.String r0 = r0.notificationRingerURI     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            android.net.Uri r52 = android.net.Uri.parse(r52)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            r1 = r52
            android.media.Ringtone r41 = android.media.RingtoneManager.getRingtone(r0, r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r41 == 0) goto L_0x0bb2
            r52 = 5
            r0 = r41
            r1 = r52
            r0.setStreamType(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r41.play()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0bb2:
            r0 = r57
            int r0 = r0.systemTimeoutCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = -5
            r0 = r52
            r1 = r53
            if (r0 != r1) goto L_0x0d53
            r0 = r57
            int r0 = r0.notificationTimeoutDurationSetting     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r52
            r1 = r57
            r1.systemTimeoutCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0bcc:
            if (r48 != 0) goto L_0x0d65
            java.lang.StringBuilder r52 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.lang.String r0 = r0.stringNotificationContentLeadText     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r53 = r0
            java.lang.String r53 = java.lang.String.valueOf(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52.<init>(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r53 = r14.toString()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.StringBuilder r52 = r52.append(r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r16 = r52.toString()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.lang.String r0 = r0.stringNotificationContentTitleStart     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r46
            r1 = r52
            r0.<init>(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.systemTimeoutCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r46
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            java.lang.String r0 = r0.stringNotificationContentTitleEnd     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r46
            r1 = r52
            r0.append(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            android.content.Context r53 = r57.getApplicationContext()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r54 = r46.toString()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.app.PendingIntent r0 = r0.notificationContentIntent     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r55 = r0
            r0 = r52
            r1 = r53
            r2 = r54
            r3 = r16
            r4 = r55
            r0.setLatestEventInfo(r1, r2, r3, r4)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.app.NotificationManager r0 = r0.notificationManager     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 1
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r54 = r0
            r52.notify(r53, r54)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0c44:
            java.lang.String r52 = "VLService"
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r54 = "SystemTimeoutCounter: "
            r53.<init>(r54)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            int r0 = r0.systemTimeoutCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r53 = r53.toString()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0c5e:
            r0 = r57
            int r0 = r0.systemTimeoutCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 > 0) goto L_0x002a
            r52 = 0
            r0 = r57
            r1 = r52
            r0.cancelNotification(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r57.resetVolumeToBeans()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            if (r26 == 0) goto L_0x0c8a
            r0 = r57
            int r0 = r0.lockSkipOccurred     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 1
            r0 = r52
            r1 = r53
            if (r0 != r1) goto L_0x0c8a
            r52 = 2
            r0 = r52
            r1 = r57
            r1.lockSkipOccurred = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0c8a:
            r57.wakeLockStop()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x002a
        L_0x0c8f:
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Keyguard set, saving changes"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r57.wakeLockStart()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 0
            r0 = r57
            r1 = r52
            r0.cancelNotification(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r57.resetBeansToVolume()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r57.wakeLockStop()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x002a
        L_0x0caa:
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 4
            r0 = r53
            r1 = r52
            r1.defaults = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = -16711936(0xffffffffff00ff00, float:-1.7146522E38)
            r0 = r53
            r1 = r52
            r1.ledARGB = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Notification: Default LED Color On"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x0acf
        L_0x0cd0:
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 0
            r0 = r53
            r1 = r52
            r1.ledOnMS = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 0
            r0 = r53
            r1 = r52
            r1.ledOffMS = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.app.Notification r0 = r0.notification     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 2
            r0 = r53
            r1 = r52
            r1.flags = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Notification: No LED Flashing"
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x0af9
        L_0x0d03:
            r0 = r57
            boolean r0 = r0.notificationPopped     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 != 0) goto L_0x0b25
            android.content.Intent r35 = new android.content.Intent     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.Class<com.hourdb.volumelocker.PopupActivity> r52 = com.hourdb.volumelocker.PopupActivity.class
            r0 = r35
            r1 = r57
            r2 = r52
            r0.<init>(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 1350565892(0x50800004, float:1.71798774E10)
            r0 = r35
            r1 = r52
            r0.setFlags(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "Counter"
            r0 = r57
            int r0 = r0.notificationTimeoutDurationSetting     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r53 = r0
            r0 = r35
            r1 = r52
            r2 = r53
            r0.putExtra(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "Changed"
            java.lang.String r53 = r14.toString()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r35
            r1 = r52
            r2 = r53
            r0.putExtra(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            r1 = r35
            r0.startActivity(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 1
            r0 = r52
            r1 = r57
            r1.notificationPopped = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x0b25
        L_0x0d53:
            r0 = r57
            int r0 = r0.systemTimeoutCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 1
            int r52 = r52 - r53
            r0 = r52
            r1 = r57
            r1.systemTimeoutCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x0bcc
        L_0x0d65:
            r0 = r57
            android.os.Handler r0 = r0.mHandler     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            if (r52 == 0) goto L_0x0c44
            r0 = r57
            android.os.Handler r0 = r0.mHandler     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            android.os.Message r28 = r52.obtainMessage()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 1
            r0 = r52
            r1 = r28
            r1.what = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            android.os.Bundle r11 = new android.os.Bundle     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r11.<init>()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "Terminate"
            r53 = 0
            r0 = r11
            r1 = r52
            r2 = r53
            r0.putBoolean(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "Changed"
            java.lang.String r53 = r14.toString()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r11
            r1 = r52
            r2 = r53
            r0.putString(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "Counter"
            r0 = r57
            int r0 = r0.systemTimeoutCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r53 = r0
            r0 = r11
            r1 = r52
            r2 = r53
            r0.putInt(r1, r2)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r28
            r1 = r11
            r0.setData(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r0 = r57
            android.os.Handler r0 = r0.mHandler     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r52
            r1 = r28
            r0.sendMessage(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            java.lang.String r52 = "VLService"
            java.lang.String r53 = "Sending notification popup new information.."
            android.util.Log.d(r52, r53)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x0c44
        L_0x0dca:
            r0 = r57
            int r0 = r0.notificationTimeoutDurationSetting     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r0 = r52
            r1 = r57
            r1.systemTimeoutCounter = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x0c5e
        L_0x0dd8:
            r0 = r57
            int r0 = r0.systemTimeoutCounter     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = -5
            r0 = r52
            r1 = r53
            if (r0 <= r1) goto L_0x0df5
            r57.resetSystemTimeoutCounter()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = 0
            r0 = r57
            r1 = r52
            r0.cancelNotification(r1)     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r57.wakeLockStop()     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
        L_0x0df5:
            if (r26 == 0) goto L_0x002a
            r0 = r57
            int r0 = r0.lockSkipOccurred     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            r52 = r0
            r53 = 2
            r0 = r52
            r1 = r53
            if (r0 == r1) goto L_0x002a
            r52 = 2
            r0 = r52
            r1 = r57
            r1.lockSkipOccurred = r0     // Catch:{ NullPointerException -> 0x0136, RuntimeException -> 0x01aa }
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.hourdb.volumelocker.VLService.runBackgroundTask():void");
    }

    private void cancelNotification(boolean force) {
        Log.d(TAG, "Notification: Canceling...");
        if (this.notification != null || force) {
            if (this.notificationManager != null) {
                this.notificationManager.cancel(1);
            }
            this.notification = null;
            this.notificationContentIntent = null;
        }
        if (this.mHandler != null && this.notificationPopped) {
            Message msg = this.mHandler.obtainMessage();
            msg.what = 1;
            Bundle b = new Bundle();
            b.putBoolean(BUNDLE_ITEM_TERMINATE, true);
            b.putString(BUNDLE_ITEM_CHANGED, "");
            b.putInt(BUNDLE_ITEM_COUNTER, -1);
            msg.setData(b);
            this.mHandler.sendMessage(msg);
            this.notificationPopped = false;
        }
    }

    private int parseNotificationLEDColor(int position) {
        switch (position) {
            case 2:
                return -16711681;
            case 3:
                return -16711936;
            case 4:
                return -65281;
            case 5:
                return -65536;
            case 6:
            default:
                return -1;
            case BROADCAST_RECEIVED_TIMEOUT /*7*/:
                return -256;
        }
    }

    private void resetBeansToVolume() {
        Log.d(TAG, "Resetting beans to the current volume levels...");
        if (this.pHelper.getLockRingerVolume()) {
            int ringerVolume = this.audioManager.getStreamVolume(2);
            int ringerMode = this.audioManager.getRingerMode();
            VolumeBean ringerBean = this.volumeMapping.get(2);
            if (ringerBean != null) {
                ringerBean.setStreamVolume(ringerVolume);
                ringerBean.setStreamMode(ringerMode);
            }
        }
        if (this.pHelper.getLockAlarmVolume()) {
            int alarmVolume = this.audioManager.getStreamVolume(4);
            VolumeBean alarmBean = this.volumeMapping.get(4);
            if (alarmBean != null) {
                alarmBean.setStreamVolume(alarmVolume);
            }
        }
        if (this.pHelper.getLockMusicVolume()) {
            int musicVolume = this.audioManager.getStreamVolume(3);
            VolumeBean musicBean = this.volumeMapping.get(3);
            if (musicBean != null) {
                musicBean.setStreamVolume(musicVolume);
            }
        }
        if (this.pHelper.getLockNotificationVolume()) {
            int notificationVolume = this.audioManager.getStreamVolume(5);
            VolumeBean notificationBean = this.volumeMapping.get(5);
            if (notificationBean != null) {
                notificationBean.setStreamVolume(notificationVolume);
            }
        }
        if (this.pHelper.getLockSystemVolume()) {
            int systemVolume = this.audioManager.getStreamVolume(1);
            VolumeBean systemBean = this.volumeMapping.get(1);
            if (systemBean != null) {
                systemBean.setStreamVolume(systemVolume);
            }
        }
        if (this.pHelper.getLockVoiceCallVolume()) {
            int voiceCallVolume = this.audioManager.getStreamVolume(LOCK_SKIP_INITIALIZED);
            VolumeBean voiceCallBean = this.volumeMapping.get(Integer.valueOf((int) LOCK_SKIP_INITIALIZED));
            if (voiceCallBean != null) {
                voiceCallBean.setStreamVolume(voiceCallVolume);
            }
        }
        if (this.supportsDTMF && this.pHelper.getLockDTMFVolume()) {
            int dtmfVolume = this.audioManager.getStreamVolume(8);
            VolumeBean dtmfBean = this.volumeMapping.get(8);
            if (dtmfBean != null) {
                dtmfBean.setStreamVolume(dtmfVolume);
            }
        }
        if (this.pHelper.getPersistVolumeLevels()) {
            Log.d(TAG, "Persisting volume mapping...");
            this.pHelper.edit();
            this.pHelper.setPersistedVolumeLevels(this.volumeMapping);
            this.pHelper.commit();
        }
        resetSystemTimeoutCounter();
    }

    private void resetVolumeToBeans() {
        VolumeBean dtmfCallBean;
        VolumeBean voiceCallBean;
        VolumeBean systemBean;
        VolumeBean notificationBean;
        VolumeBean musicBean;
        VolumeBean alarmBean;
        VolumeBean ringerBean;
        Log.d(TAG, "Resetting stream volumes to beans...");
        if (this.audioManager != null) {
            if (this.pHelper.getLockRingerVolume() && (ringerBean = this.volumeMapping.get(2)) != null) {
                this.audioManager.setStreamVolume(2, ringerBean.getStreamVolume(), 8);
                this.audioManager.setRingerMode(ringerBean.getStreamMode());
            }
            if (this.pHelper.getLockAlarmVolume() && (alarmBean = this.volumeMapping.get(4)) != null) {
                this.audioManager.setStreamVolume(4, alarmBean.getStreamVolume(), 8);
            }
            if (this.pHelper.getLockMusicVolume() && (musicBean = this.volumeMapping.get(3)) != null) {
                this.audioManager.setStreamVolume(3, musicBean.getStreamVolume(), 8);
            }
            if (this.pHelper.getLockNotificationVolume() && (notificationBean = this.volumeMapping.get(5)) != null) {
                this.audioManager.setStreamVolume(5, notificationBean.getStreamVolume(), 8);
            }
            if (this.pHelper.getLockSystemVolume() && (systemBean = this.volumeMapping.get(1)) != null) {
                this.audioManager.setStreamVolume(1, systemBean.getStreamVolume(), 8);
            }
            if (this.pHelper.getLockVoiceCallVolume() && (voiceCallBean = this.volumeMapping.get(Integer.valueOf((int) LOCK_SKIP_INITIALIZED))) != null) {
                this.audioManager.setStreamVolume(LOCK_SKIP_INITIALIZED, voiceCallBean.getStreamVolume(), 8);
            }
            if (this.supportsDTMF && this.pHelper.getLockDTMFVolume() && (dtmfCallBean = this.volumeMapping.get(8)) != null) {
                this.audioManager.setStreamVolume(8, dtmfCallBean.getStreamVolume(), 8);
            }
            resetSystemTimeoutCounter();
        }
    }

    private void resetSystemTimeoutCounter() {
        this.systemTimeoutCounter = TIMEOUT_COUNTER_OFF;
        this.systemTotalChangedCounter = LOCK_SKIP_INITIALIZED;
    }
}
