package com.hourdb.volumelocker.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.hourdb.volumelocker.model.VolumeBean;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class PreferencesHelper {
    private static final String PREFERENCES_NAME = "CurrentUser";
    public static final String PREFERENCE_ALLOW_OTHER_APPS_TO_OVERRIDE = "AllowOtherAppsToOverride";
    public static final String PREFERENCE_AUTO_SAVE_SILENT_MODE = "AutoSaveSilentMode";
    public static final String PREFERENCE_DISABLE_DTMF_VOLUME_LOCK_DURING_CALL = "DisableDTMFVolumeLockDuringCall";
    public static final String PREFERENCE_DISABLE_DURING_BEDSIDE = "DisableDuringBedside";
    public static final String PREFERENCE_DISABLE_DURING_CAMERA_USAGE = "DisableDuringCameraUsage";
    public static final String PREFERENCE_DISABLE_DURING_CAMERA_USAGE_DELAY = "DisableDuringCameraUsageDelay";
    public static final String PREFERENCE_DISABLE_DURING_GOOGLE_NAVIGATION = "DisableDuringGoogleNavigation";
    public static final String PREFERENCE_DISABLE_DURING_GOOGLE_VOICE = "DisableDuringGoogleVoice";
    public static final String PREFERENCE_DISABLE_DURING_SCREEN_LOCK = "DisableDuringScreenLock";
    public static final String PREFERENCE_DISABLE_DURING_SCREEN_LOCK_SAVE_CHANGES = "DisableDuringScreenLockSaveChanges";
    public static final String PREFERENCE_DISABLE_MUSIC_VOLUME_LOCK_WHILE_HEADPHONES_IN = "DisableMusicVolumeLockWhileHeadphonesIn";
    public static final String PREFERENCE_DISABLE_RINGER_VOLUME_LOCK_WHILE_RINGING = "DisableRingerVolumeLockWhileRinging";
    public static final String PREFERENCE_DISABLE_VOICE_CALL_VOLUME_LOCK_DURING_CALL = "DisableVoiceCallVolumeLockDuringCall";
    public static final String PREFERENCE_ENABLE_VLS = "EnableVLS";
    public static final String PREFERENCE_LOCK_ALARM_VOLUME = "LockAlarmVolume";
    public static final String PREFERENCE_LOCK_DTMF_VOLUME = "LockDTMFVolume";
    public static final String PREFERENCE_LOCK_MUSIC_VOLUME = "LockMusicVolume";
    public static final String PREFERENCE_LOCK_NOTIFICATION_VOLUME = "LockNotificationVolume";
    public static final String PREFERENCE_LOCK_RINGER_VOLUME = "LockRingerVolume";
    public static final String PREFERENCE_LOCK_SYSTEM_VOLUME = "LockSystemVolume";
    public static final String PREFERENCE_LOCK_VOICE_CALL_VOLUME = "LockVoiceCallVolume";
    public static final String PREFERENCE_NOTIFICATION_LED_COLOR = "NotificationLEDColor";
    public static final String PREFERENCE_NOTIFICATION_POPUP = "NotificationPopup";
    public static final String PREFERENCE_NOTIFICATION_RINGTONE_URI = "NotificationRingtoneURI";
    public static final String PREFERENCE_NOTIFICATION_RING_VIBRATE_ONLY_UNLOCKED = "NotificationRingVibrateOnlyUnlocked";
    public static final String PREFERENCE_NOTIFICATION_TIMEOUT = "NotificationTimeout";
    public static final String PREFERENCE_NOTIFICATION_VIBRATE = "NotificationVibrate";
    public static final String PREFERENCE_PERSISTED_VOLUME_LEVELS = "PersistedVolumeLevels";
    public static final String PREFERENCE_PERSIST_VOLUME_LEVELS = "PersistVolumeLevels";
    public static final String PREFERENCE_RUN_AT_HIGHER_PRIORITY = "RunAtHigherPriority";
    private static final String TAG = "PreferencesHelper";
    private HashMap<String, Boolean> booleanCache = null;
    private final boolean cache;
    private HashMap<String, Integer> intCache = null;
    private SharedPreferences.Editor mEditor = null;
    private final SharedPreferences mPreferences;
    private HashMap<String, String> stringCache = null;

    public PreferencesHelper(Context ctx, boolean cache2) {
        this.mPreferences = ctx.getSharedPreferences(PREFERENCES_NAME, 0);
        this.cache = cache2;
        if (this.cache) {
            this.stringCache = new HashMap<>();
            this.intCache = new HashMap<>();
            this.booleanCache = new HashMap<>();
        }
    }

    public void edit() {
        if (this.mEditor == null) {
            this.mEditor = this.mPreferences.edit();
        } else {
            Log.e(TAG, "Editor already opened!");
        }
    }

    public void commit() {
        if (this.mEditor == null) {
            Log.e(TAG, "Editor never opened!");
            return;
        }
        this.mEditor.commit();
        this.mEditor = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getEnableVLS() {
        return getValue(PREFERENCE_ENABLE_VLS, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getLockAlarmVolume() {
        return getValue(PREFERENCE_LOCK_ALARM_VOLUME, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getLockDTMFVolume() {
        return getValue(PREFERENCE_LOCK_DTMF_VOLUME, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getLockMusicVolume() {
        return getValue(PREFERENCE_LOCK_MUSIC_VOLUME, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableMusicVolumeLockWhileHeadphonesIn() {
        return getValue(PREFERENCE_DISABLE_MUSIC_VOLUME_LOCK_WHILE_HEADPHONES_IN, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getLockNotificationVolume() {
        return getValue(PREFERENCE_LOCK_NOTIFICATION_VOLUME, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getLockRingerVolume() {
        return getValue(PREFERENCE_LOCK_RINGER_VOLUME, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableRingerVolumeLockWhileRinging() {
        return getValue(PREFERENCE_DISABLE_RINGER_VOLUME_LOCK_WHILE_RINGING, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getLockSystemVolume() {
        return getValue(PREFERENCE_LOCK_SYSTEM_VOLUME, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getLockVoiceCallVolume() {
        return getValue(PREFERENCE_LOCK_VOICE_CALL_VOLUME, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableVoiceCallVolumeLockDuringCall() {
        return getValue(PREFERENCE_DISABLE_VOICE_CALL_VOLUME_LOCK_DURING_CALL, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableDTMFVolumeLockDuringCall() {
        return getValue(PREFERENCE_DISABLE_DTMF_VOLUME_LOCK_DURING_CALL, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableDuringScreenLock() {
        return getValue(PREFERENCE_DISABLE_DURING_SCREEN_LOCK, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableDuringScreenLockSaveChanges() {
        return getValue(PREFERENCE_DISABLE_DURING_SCREEN_LOCK_SAVE_CHANGES, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getNotificationPopup() {
        return getValue(PREFERENCE_NOTIFICATION_POPUP, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getNotificationVibrate() {
        return getValue(PREFERENCE_NOTIFICATION_VIBRATE, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getNotificationRingVibrateOnlyUnlocked() {
        return getValue(PREFERENCE_NOTIFICATION_RING_VIBRATE_ONLY_UNLOCKED, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableDuringCameraUsage() {
        return getValue(PREFERENCE_DISABLE_DURING_CAMERA_USAGE, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableDuringCameraUsageDelay() {
        return getValue(PREFERENCE_DISABLE_DURING_CAMERA_USAGE_DELAY, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableDuringGoogleVoice() {
        return getValue(PREFERENCE_DISABLE_DURING_GOOGLE_VOICE, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableDuringGoogleNavigation() {
        return getValue(PREFERENCE_DISABLE_DURING_GOOGLE_NAVIGATION, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getDisableDuringBedside() {
        return getValue(PREFERENCE_DISABLE_DURING_BEDSIDE, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getAllowOtherAppsToOverride() {
        return getValue(PREFERENCE_ALLOW_OTHER_APPS_TO_OVERRIDE, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getAutoSaveSilentMode() {
        return getValue(PREFERENCE_AUTO_SAVE_SILENT_MODE, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getPersistVolumeLevels() {
        return getValue(PREFERENCE_PERSIST_VOLUME_LEVELS, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, int):int
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, java.lang.String):java.lang.String
      com.hourdb.volumelocker.helper.PreferencesHelper.getValue(java.lang.String, boolean):boolean */
    public boolean getRunAtHigherPriority() {
        return getValue(PREFERENCE_RUN_AT_HIGHER_PRIORITY, true);
    }

    public String getNotificationRingtoneURI() {
        return getValue(PREFERENCE_NOTIFICATION_RINGTONE_URI, (String) null);
    }

    public int getNotificationTimeout() {
        return getValue(PREFERENCE_NOTIFICATION_TIMEOUT, 30);
    }

    public int getNotificationLEDColor() {
        return getValue(PREFERENCE_NOTIFICATION_LED_COLOR, 2);
    }

    public HashMap<Integer, VolumeBean> getPersistedVolumeLevels() {
        HashMap<Integer, VolumeBean> toRet = new HashMap<>();
        String persistedVal = getValue(PREFERENCE_PERSISTED_VOLUME_LEVELS, (String) null);
        if (persistedVal != null) {
            try {
                JSONObject obj = new JSONObject(persistedVal);
                Iterator<String> itr = obj.keys();
                if (itr != null) {
                    while (itr.hasNext()) {
                        String key = itr.next();
                        JSONObject subObj = obj.getJSONObject(key);
                        toRet.put(Integer.valueOf(Integer.parseInt(key)), new VolumeBean(subObj.getInt(VolumeBean.JSON_KEY_STREAM_TYPE), subObj.getInt(VolumeBean.JSON_KEY_STREAM_VOLUME), subObj.getInt(VolumeBean.JSON_KEY_STREAM_MODE)));
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                edit();
                clearPersistedVolumeLevels();
                commit();
            }
        }
        return toRet;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    private boolean getValue(String key, boolean defValue) {
        if (this.cache && this.booleanCache.containsKey(key)) {
            return this.booleanCache.get(key).booleanValue();
        }
        boolean value = this.mPreferences.getBoolean(key, defValue);
        if (this.cache) {
            this.booleanCache.put(key, Boolean.valueOf(value));
        }
        return value;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    private int getValue(String key, int defValue) {
        if (this.cache && this.intCache.containsKey(key)) {
            return this.intCache.get(key).intValue();
        }
        int value = this.mPreferences.getInt(key, defValue);
        if (this.cache) {
            this.intCache.put(key, Integer.valueOf(value));
        }
        return value;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    private String getValue(String key, String defValue) {
        if (this.cache && this.stringCache.containsKey(key)) {
            return this.stringCache.get(key);
        }
        String value = this.mPreferences.getString(key, defValue);
        if (this.cache) {
            this.stringCache.put(key, value);
        }
        return value;
    }

    private void setValue(String key, String value) {
        if (this.mEditor != null) {
            if (this.cache) {
                this.stringCache.remove(key);
            }
            this.mEditor.putString(key, value);
            return;
        }
        Log.e(TAG, "The editor was not opened!");
    }

    private void setValue(String key, int value) {
        if (this.mEditor != null) {
            if (this.cache) {
                this.intCache.remove(key);
            }
            this.mEditor.putInt(key, value);
            return;
        }
        Log.e(TAG, "The editor was not opened!");
    }

    private void setValue(String key, boolean value) {
        if (this.mEditor != null) {
            if (this.cache) {
                this.booleanCache.remove(key);
            }
            this.mEditor.putBoolean(key, value);
            return;
        }
        Log.e(TAG, "The editor was not opened!");
    }

    public void setNotificationLEDColor(int notificationLEDColor) {
        setValue(PREFERENCE_NOTIFICATION_LED_COLOR, notificationLEDColor);
    }

    public void setNotificationTimeout(int notificationTimeout) {
        setValue(PREFERENCE_NOTIFICATION_TIMEOUT, notificationTimeout);
    }

    public void setNotificationRingtoneURI(String notificationRingtoneURI) {
        setValue(PREFERENCE_NOTIFICATION_RINGTONE_URI, notificationRingtoneURI);
    }

    public void setRunAtHigherPriority(boolean runAtHigherPriority) {
        setValue(PREFERENCE_RUN_AT_HIGHER_PRIORITY, runAtHigherPriority);
    }

    public void setAutoSaveSilentMode(boolean autoSaveSilentMode) {
        setValue(PREFERENCE_AUTO_SAVE_SILENT_MODE, autoSaveSilentMode);
    }

    public void setPersistVolumeLevels(boolean persistVolumeLevels) {
        clearPersistedVolumeLevels();
        setValue(PREFERENCE_PERSIST_VOLUME_LEVELS, persistVolumeLevels);
    }

    public void setAllowOtherAppsToOverride(boolean allowOtherAppsToOverride) {
        setValue(PREFERENCE_ALLOW_OTHER_APPS_TO_OVERRIDE, allowOtherAppsToOverride);
    }

    public void setDisableDuringBedside(boolean disableDuringBedside) {
        setValue(PREFERENCE_DISABLE_DURING_BEDSIDE, disableDuringBedside);
    }

    public void setDisableDuringGoogleNavigation(boolean disableDuringGoogleNavigation) {
        setValue(PREFERENCE_DISABLE_DURING_GOOGLE_NAVIGATION, disableDuringGoogleNavigation);
    }

    public void setDisableDuringGoogleVoice(boolean disableDuringGoogleVoice) {
        setValue(PREFERENCE_DISABLE_DURING_GOOGLE_VOICE, disableDuringGoogleVoice);
    }

    public void setDisableDuringCameraUsage(boolean disableDuringCameraUsage) {
        setValue(PREFERENCE_DISABLE_DURING_CAMERA_USAGE, disableDuringCameraUsage);
    }

    public void setDisableDuringCameraUsageDelay(boolean disableDuringCameraUsageDelay) {
        setValue(PREFERENCE_DISABLE_DURING_CAMERA_USAGE_DELAY, disableDuringCameraUsageDelay);
    }

    public void setNotificationRingVibrateOnlyUnlocked(boolean notificationRingVibrateOnlyUnlocked) {
        setValue(PREFERENCE_NOTIFICATION_RING_VIBRATE_ONLY_UNLOCKED, notificationRingVibrateOnlyUnlocked);
    }

    public void setNotificationVibrate(boolean notificationVibrate) {
        setValue(PREFERENCE_NOTIFICATION_VIBRATE, notificationVibrate);
    }

    public void setNotificationPopup(boolean notificationPopup) {
        setValue(PREFERENCE_NOTIFICATION_POPUP, notificationPopup);
    }

    public void setDisableDuringScreenLockSaveChanges(boolean disableDuringScreenLockSaveChanges) {
        setValue(PREFERENCE_DISABLE_DURING_SCREEN_LOCK_SAVE_CHANGES, disableDuringScreenLockSaveChanges);
    }

    public void setDisableDuringScreenLock(boolean disableDuringScreenLock) {
        setValue(PREFERENCE_DISABLE_DURING_SCREEN_LOCK, disableDuringScreenLock);
    }

    public void setDisableVoiceCallVolumeLockDuringCall(boolean disableVoiceCallVolumeLockDuringCall) {
        setValue(PREFERENCE_DISABLE_VOICE_CALL_VOLUME_LOCK_DURING_CALL, disableVoiceCallVolumeLockDuringCall);
    }

    public void setDisableDTMFVolumeLockDuringCall(boolean disableDTMFVolumeLockDuringCall) {
        setValue(PREFERENCE_DISABLE_DTMF_VOLUME_LOCK_DURING_CALL, disableDTMFVolumeLockDuringCall);
    }

    public void setLockVoiceCallVolume(boolean lockVoiceCallVolume) {
        clearPersistedVolumeLevels();
        setValue(PREFERENCE_LOCK_VOICE_CALL_VOLUME, lockVoiceCallVolume);
    }

    public void setLockSystemVolume(boolean lockSystemVolume) {
        clearPersistedVolumeLevels();
        setValue(PREFERENCE_LOCK_SYSTEM_VOLUME, lockSystemVolume);
    }

    public void setDisableRingerVolumeLockWhileRinging(boolean disableRingerVolumeLockWhileRinging) {
        setValue(PREFERENCE_DISABLE_RINGER_VOLUME_LOCK_WHILE_RINGING, disableRingerVolumeLockWhileRinging);
    }

    public void setLockRingerVolume(boolean lockRingerVolume) {
        clearPersistedVolumeLevels();
        setValue(PREFERENCE_LOCK_RINGER_VOLUME, lockRingerVolume);
    }

    public void setLockNotificationVolume(boolean lockNotificationVolume) {
        clearPersistedVolumeLevels();
        setValue(PREFERENCE_LOCK_NOTIFICATION_VOLUME, lockNotificationVolume);
    }

    public void setDisableMusicVolumeLockWhileHeadphonesIn(boolean disableMusicVolumeLockWhileHeadphonesIn) {
        setValue(PREFERENCE_DISABLE_MUSIC_VOLUME_LOCK_WHILE_HEADPHONES_IN, disableMusicVolumeLockWhileHeadphonesIn);
    }

    public void setLockMusicVolume(boolean lockMusicVolume) {
        clearPersistedVolumeLevels();
        setValue(PREFERENCE_LOCK_MUSIC_VOLUME, lockMusicVolume);
    }

    public void setLockAlarmValue(boolean lockAlarmVolume) {
        clearPersistedVolumeLevels();
        setValue(PREFERENCE_LOCK_ALARM_VOLUME, lockAlarmVolume);
    }

    public void setLockDTMFValue(boolean lockDTMFVolume) {
        clearPersistedVolumeLevels();
        setValue(PREFERENCE_LOCK_DTMF_VOLUME, lockDTMFVolume);
    }

    public void setEnableVLS(boolean enableVLS) {
        clearPersistedVolumeLevels();
        setValue(PREFERENCE_ENABLE_VLS, enableVLS);
    }

    public void setPersistedVolumeLevels(HashMap<Integer, VolumeBean> values) {
        if (this.mEditor == null) {
            Log.e(TAG, "The editor was not opened!");
        } else if (values == null) {
            Log.e(TAG, "The values are not defined!");
        } else {
            JSONObject obj = new JSONObject();
            try {
                for (Map.Entry<Integer, VolumeBean> entry : values.entrySet()) {
                    obj.put(Integer.toString(((Integer) entry.getKey()).intValue()), ((VolumeBean) entry.getValue()).toJSON());
                }
                this.mEditor.putString(PREFERENCE_PERSISTED_VOLUME_LEVELS, obj.toString());
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public void clearPersistedVolumeLevels() {
        if (this.mEditor == null) {
            Log.e(TAG, "The editor was not opened!");
            return;
        }
        Log.d(TAG, "Clearing persisted volume levels!");
        this.mEditor.remove(PREFERENCE_PERSISTED_VOLUME_LEVELS);
    }

    public void setBoolean(String key, boolean value) {
        if (key == null) {
            Log.e(TAG, "No key specified!");
        } else if (this.mEditor == null) {
            Log.e(TAG, "The editor was not opened!");
        } else {
            if (this.cache) {
                this.booleanCache.remove(key);
            }
            if (key.equalsIgnoreCase(PREFERENCE_ENABLE_VLS) || key.equalsIgnoreCase(PREFERENCE_LOCK_ALARM_VOLUME) || key.equalsIgnoreCase(PREFERENCE_LOCK_DTMF_VOLUME) || key.equalsIgnoreCase(PREFERENCE_LOCK_MUSIC_VOLUME) || key.equalsIgnoreCase(PREFERENCE_LOCK_NOTIFICATION_VOLUME) || key.equalsIgnoreCase(PREFERENCE_LOCK_RINGER_VOLUME) || key.equalsIgnoreCase(PREFERENCE_LOCK_SYSTEM_VOLUME) || key.equalsIgnoreCase(PREFERENCE_LOCK_VOICE_CALL_VOLUME)) {
                clearPersistedVolumeLevels();
            }
            this.mEditor.putBoolean(key, value);
        }
    }

    public void setInt(String key, int value) {
        if (key == null) {
            Log.e(TAG, "No key specified!");
        } else if (this.mEditor == null) {
            Log.e(TAG, "The editor was not opened!");
        } else {
            if (this.cache) {
                this.intCache.remove(key);
            }
            this.mEditor.putInt(key, value);
        }
    }

    public void setString(String key, String value) {
        if (key == null) {
            Log.e(TAG, "No key specified!");
        } else if (this.mEditor == null) {
            Log.e(TAG, "The editor was not opened!");
        } else {
            if (this.cache) {
                this.stringCache.remove(key);
            }
            this.mEditor.putString(key, value);
        }
    }
}
