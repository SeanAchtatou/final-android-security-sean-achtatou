package com.hourdb.volumelocker.model;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class VolumeBean {
    public static final String JSON_KEY_STREAM_MODE = "streamMode";
    public static final String JSON_KEY_STREAM_TYPE = "streamType";
    public static final String JSON_KEY_STREAM_VOLUME = "streamVolume";
    public static final int NO_MODE = -1;
    private static final String TAG = "VolumeBean";
    private int streamMode = -1;
    private int streamType = -1;
    private int streamVolume = -1;

    public VolumeBean() {
    }

    public VolumeBean(int streamType2) {
        this.streamType = streamType2;
    }

    public VolumeBean(int streamType2, int streamVolume2) {
        this.streamType = streamType2;
        this.streamVolume = streamVolume2;
    }

    public VolumeBean(int streamType2, int streamVolume2, int streamMode2) {
        this.streamType = streamType2;
        this.streamVolume = streamVolume2;
        this.streamMode = streamMode2;
    }

    public int getStreamType() {
        return this.streamType;
    }

    public void setStreamType(int streamType2) {
        this.streamType = streamType2;
    }

    public int getStreamVolume() {
        return this.streamVolume;
    }

    public void setStreamVolume(int streamVolume2) {
        this.streamVolume = streamVolume2;
    }

    public int getStreamMode() {
        return this.streamMode;
    }

    public void setStreamMode(int streamMode2) {
        this.streamMode = streamMode2;
    }

    public boolean hasChanged(int streamVolume2, int streamMode2) {
        return (this.streamVolume == streamVolume2 && this.streamMode == streamMode2) ? false : true;
    }

    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        try {
            obj.put(JSON_KEY_STREAM_TYPE, this.streamType);
            obj.put(JSON_KEY_STREAM_VOLUME, this.streamVolume);
            obj.put(JSON_KEY_STREAM_MODE, this.streamMode);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return obj;
    }
}
