package com.hourdb.volumelocker.receiver;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.hourdb.volumelocker.VLService;
import com.hourdb.volumelocker.helper.PreferencesHelper;
import com.hourdb.volumelocker.util.ServiceUtils;

public class EnableOrDisableBroadcastReceiver extends BroadcastReceiver {
    public static final String ACTION_ENABLE_OR_DISABLE_VL = "com.hourdb.volumelocker.action_enable_or_disable";
    public static final String EXTRA_VALUE_ENABLED = "Enabled";
    private static final String TAG = "EnableOrDisableBroadcastReceiver";

    public void onReceive(Context context, Intent intent) {
        boolean broadcastEnabled;
        boolean z;
        if (intent != null && intent.getAction() != null && intent.getAction().equals(ACTION_ENABLE_OR_DISABLE_VL)) {
            Log.d(TAG, "Volume Locker Enable/Disable Request Received!");
            PreferencesHelper pHelper = new PreferencesHelper(context, true);
            boolean skipSave = false;
            Bundle extras = intent.getExtras();
            if (extras == null || !extras.containsKey(EXTRA_VALUE_ENABLED)) {
                broadcastEnabled = !pHelper.getEnableVLS() || !ServiceUtils.isServiceRunning(context);
            } else {
                if (pHelper.getEnableVLS()) {
                    z = false;
                } else {
                    z = true;
                }
                broadcastEnabled = extras.getBoolean(EXTRA_VALUE_ENABLED, z);
            }
            if (pHelper.getEnableVLS() == broadcastEnabled) {
                skipSave = true;
            }
            if (!skipSave) {
                Log.d(TAG, "Skipping setEnableVLS save due to current value...");
                pHelper.edit();
                pHelper.setEnableVLS(broadcastEnabled);
                pHelper.commit();
            }
            if (!broadcastEnabled) {
                stopService(context);
            } else {
                startService(context, pHelper.getEnableVLS());
            }
        }
    }

    private void stopService(Context ctx) {
        Log.d(TAG, "Stopping the service...");
        ComponentName comp = new ComponentName(ctx.getPackageName(), VLService.class.getName());
        if (!ctx.stopService(new Intent().setComponent(comp))) {
            Log.d(TAG, "Could not stop service " + comp.toString());
        }
    }

    private void startService(Context ctx, boolean prefEnableVLSValue) {
        if (prefEnableVLSValue) {
            Log.d(TAG, "Starting the service...");
            ComponentName comp = new ComponentName(ctx.getPackageName(), VLService.class.getName());
            if (ctx.startService(new Intent().setComponent(comp)) == null) {
                Log.d(TAG, "Could not start service " + comp.toString());
            }
        }
    }
}
