package com.hourdb.volumelocker.receiver;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.hourdb.volumelocker.VLService;

public class VolumeLockerBroadcastReceiver extends BroadcastReceiver {
    private static final String BROADCAST_INTENT_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    private static final String TAG = "VolumeLockerBroadcastReceiver";

    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Starting Volume Locker Service");
        if (intent == null || intent.getAction() == null || !intent.getAction().equals(BROADCAST_INTENT_BOOT_COMPLETED)) {
            Log.d(TAG, "Received unexpected intent " + intent.toString());
            return;
        }
        ComponentName comp = new ComponentName(context.getPackageName(), VLService.class.getName());
        if (context.startService(new Intent().setComponent(comp)) == null) {
            Log.e(TAG, "Could not start service " + comp.toString());
        }
    }
}
