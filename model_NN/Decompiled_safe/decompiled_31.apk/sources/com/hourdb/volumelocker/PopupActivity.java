package com.hourdb.volumelocker;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.hourdb.volumelocker.aidl.IVLService;
import com.hourdb.volumelocker.aidl.IVLServiceCallback;

public class PopupActivity extends Activity {
    private static final String TAG = "PopupActivity";
    private TextView changedValuesTextView = null;
    private boolean connectionIsBound = false;
    private TextView countdownTimerTextView = null;
    /* access modifiers changed from: private */
    public IVLServiceCallback mCallback = null;
    private ServiceConnection mConnection = null;
    /* access modifiers changed from: private */
    public Handler mHandler = null;
    /* access modifiers changed from: private */
    public IVLService mService = null;
    private boolean safeFinishing = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.popup);
        this.changedValuesTextView = (TextView) findViewById(R.id.PopupChangedValuesTextView);
        this.countdownTimerTextView = (TextView) findViewById(R.id.PopupCountdownTextView);
        Button saveButton = (Button) findViewById(R.id.PopupSaveChangesButton);
        Button revertButton = (Button) findViewById(R.id.PopupRevertChangesButton);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            showCountdownTimerValue(extras.getInt(VLService.BUNDLE_ITEM_COUNTER));
            showChangedValues(extras.getString(VLService.BUNDLE_ITEM_CHANGED));
        }
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        Bundle data = msg.getData();
                        if (data.getBoolean(VLService.BUNDLE_ITEM_TERMINATE)) {
                            PopupActivity.this.terminate();
                            return;
                        }
                        PopupActivity.this.showChangedValues(data.getString(VLService.BUNDLE_ITEM_CHANGED));
                        PopupActivity.this.showCountdownTimerValue(data.getInt(VLService.BUNDLE_ITEM_COUNTER));
                        return;
                    default:
                        super.handleMessage(msg);
                        return;
                }
            }
        };
        this.mCallback = new IVLServiceCallback.Stub() {
            public void valueChanged(boolean terminate, String changed, int counter) throws RemoteException {
                Message msg = PopupActivity.this.mHandler.obtainMessage(1);
                Bundle b = new Bundle();
                b.putBoolean(VLService.BUNDLE_ITEM_TERMINATE, terminate);
                b.putString(VLService.BUNDLE_ITEM_CHANGED, changed);
                b.putInt(VLService.BUNDLE_ITEM_COUNTER, counter);
                msg.setData(b);
                PopupActivity.this.mHandler.sendMessage(msg);
            }
        };
        this.mConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                PopupActivity.this.mService = IVLService.Stub.asInterface(service);
                try {
                    PopupActivity.this.mService.registerCallback(PopupActivity.this.mCallback);
                } catch (RemoteException e) {
                }
                Log.d(PopupActivity.TAG, "Connected");
            }

            public void onServiceDisconnected(ComponentName className) {
                PopupActivity.this.mService = null;
                Log.d(PopupActivity.TAG, "Disconnected...");
            }
        };
        try {
            bindService(new Intent(IVLService.class.getName()), this.mConnection, 1);
            this.connectionIsBound = true;
        } catch (SecurityException e) {
            Log.e(TAG, "Couldn't establish IVLService connection: " + e.getMessage());
        }
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PopupActivity.this.commitVolumeLevelChanges(true);
                PopupActivity.this.terminate();
            }
        });
        revertButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PopupActivity.this.commitVolumeLevelChanges(false);
                PopupActivity.this.terminate();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (this.changedValuesTextView != null) {
            savedInstanceState.putString("changedValues", this.changedValuesTextView.getText().toString());
        }
        if (this.countdownTimerTextView != null) {
            savedInstanceState.putString("countdownTimer", this.countdownTimerTextView.getText().toString());
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (this.changedValuesTextView != null && savedInstanceState.containsKey("changedValues")) {
            this.changedValuesTextView.setText(savedInstanceState.getString("changedValues"));
        }
        if (this.countdownTimerTextView != null && savedInstanceState.containsKey("countdownTimer")) {
            this.countdownTimerTextView.setText(savedInstanceState.getString("countdownTimer"));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void commitVolumeLevelChanges(boolean save) {
        Intent serviceIntent = new Intent().setComponent(new ComponentName(getPackageName(), VLService.class.getName()));
        if (save) {
            serviceIntent.putExtra(VLService.BUNDLE_SAVE_VOLUME_LEVEL_CHANGES, true);
        } else {
            serviceIntent.putExtra(VLService.BUNDLE_REVERT_VOLUME_LEVEL_CHANGES, true);
        }
        startService(serviceIntent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (isFinishing() && !this.safeFinishing) {
            commitVolumeLevelChanges(false);
        }
        if (this.connectionIsBound) {
            if (this.mService != null) {
                try {
                    this.mService.unregisterCallback(this.mCallback);
                } catch (RemoteException e) {
                }
            }
            unbindService(this.mConnection);
            Log.d(TAG, "Service connection is now disconnected...");
        }
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void terminate() {
        this.safeFinishing = true;
        finish();
    }

    /* access modifiers changed from: private */
    public void showChangedValues(String values) {
        this.changedValuesTextView.setText(values);
    }

    /* access modifiers changed from: private */
    public void showCountdownTimerValue(int value) {
        this.countdownTimerTextView.setText(String.valueOf(value) + "s");
    }
}
