package com.hourdb.volumelocker;

public final class R {

    public static final class array {
        public static final int notification_led_color = 2131034115;
        public static final int notification_led_color_values = 2131034114;
        public static final int notification_timeout = 2131034112;
        public static final int notification_timeout_values = 2131034113;
        public static final int notification_types = 2131034116;
        public static final int notification_values = 2131034117;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int popup_changed_text_color = 2131165186;
        public static final int popup_text_color = 2131165184;
        public static final int popup_title_color = 2131165185;
        public static final int transparent = 2131165187;
    }

    public static final class drawable {
        public static final int disabled_icon = 2130837504;
        public static final int enabled_icon = 2130837505;
        public static final int icon = 2130837506;
        public static final int speaker = 2130837507;
    }

    public static final class id {
        public static final int EnableDisableWidgetIcon = 2131296257;
        public static final int EnableDisableWidgetLayout = 2131296256;
        public static final int PopupButtonLayout = 2131296263;
        public static final int PopupChangedValuesTextView = 2131296262;
        public static final int PopupCountdownTextView = 2131296260;
        public static final int PopupLeadingTextView = 2131296261;
        public static final int PopupLinearLayout = 2131296258;
        public static final int PopupRevertChangesButton = 2131296265;
        public static final int PopupSaveChangesButton = 2131296264;
        public static final int PopupTitle = 2131296259;
    }

    public static final class layout {
        public static final int enable_disable_widget_layout = 2130903040;
        public static final int popup = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int enable_disable_widget_title = 2131099734;
        public static final int main_additional_dtmf_settings_label = 2131099657;
        public static final int main_additional_music_settings_label = 2131099655;
        public static final int main_additional_ringer_settings_label = 2131099654;
        public static final int main_additional_voice_settings_label = 2131099656;
        public static final int main_allow_apps_to_override = 2131099713;
        public static final int main_allow_apps_to_override_disclaimer = 2131099714;
        public static final int main_auto_save_silent_mode = 2131099715;
        public static final int main_auto_save_silent_mode_note = 2131099716;
        public static final int main_disable_additional_settings = 2131099698;
        public static final int main_disable_dtmf_volume_lock_during_call_label = 2131099668;
        public static final int main_disable_dtmf_volume_lock_during_call_summary = 2131099669;
        public static final int main_disable_during_bedside = 2131099705;
        public static final int main_disable_during_bedside_summary = 2131099706;
        public static final int main_disable_during_camera_usage = 2131099703;
        public static final int main_disable_during_camera_usage_delay = 2131099707;
        public static final int main_disable_during_camera_usage_delay_summary = 2131099708;
        public static final int main_disable_during_camera_usage_summary = 2131099704;
        public static final int main_disable_during_google_navigation = 2131099699;
        public static final int main_disable_during_google_navigation_summary = 2131099700;
        public static final int main_disable_during_google_voice = 2131099701;
        public static final int main_disable_during_google_voice_summary = 2131099702;
        public static final int main_disable_during_screen_lock = 2131099690;
        public static final int main_disable_during_screen_lock_save_changes = 2131099692;
        public static final int main_disable_during_screen_lock_save_changes_disclaimer = 2131099693;
        public static final int main_disable_during_screen_lock_summary = 2131099691;
        public static final int main_disable_music_volume_lock_while_headphones_in = 2131099663;
        public static final int main_disable_music_volume_lock_while_headphones_in_summary = 2131099664;
        public static final int main_disable_music_volume_lock_while_ringing = 2131099659;
        public static final int main_disable_music_volume_lock_while_ringing_summary = 2131099660;
        public static final int main_disable_voice_call_volume_lock_during_call_label = 2131099671;
        public static final int main_disable_voice_call_volume_lock_during_call_summary = 2131099672;
        public static final int main_disable_while = 2131099697;
        public static final int main_enable_alarm_label = 2131099661;
        public static final int main_enable_dtmf_volume_label = 2131099667;
        public static final int main_enable_music_label = 2131099662;
        public static final int main_enable_notification_label = 2131099665;
        public static final int main_enable_ringer_volume_label = 2131099658;
        public static final int main_enable_system_label = 2131099666;
        public static final int main_enable_voice_call_label = 2131099670;
        public static final int main_enable_volume_locker_label = 2131099649;
        public static final int main_enable_volume_locker_summary = 2131099650;
        public static final int main_notification_led_color_label = 2131099681;
        public static final int main_notification_led_color_summary = 2131099682;
        public static final int main_notification_popup_label = 2131099678;
        public static final int main_notification_ring_vibrate_only_unlocked_label = 2131099688;
        public static final int main_notification_ring_vibrate_only_unlocked_summary = 2131099689;
        public static final int main_notification_ringtone_button = 2131099685;
        public static final int main_notification_ringtone_enable_vibrate_label = 2131099686;
        public static final int main_notification_ringtone_enable_vibrate_summary = 2131099687;
        public static final int main_notification_ringtone_label = 2131099683;
        public static final int main_notification_ringtone_summary = 2131099684;
        public static final int main_notification_timeout_label = 2131099679;
        public static final int main_notification_timeout_summary = 2131099680;
        public static final int main_notification_tray_label = 2131099677;
        public static final int main_notification_type_summary = 2131099676;
        public static final int main_notification_type_title = 2131099675;
        public static final int main_persist_volume_levels = 2131099711;
        public static final int main_persist_volume_levels_disclaimer = 2131099712;
        public static final int main_run_higher_priority = 2131099717;
        public static final int main_run_higher_priority_note = 2131099718;
        public static final int main_settings_label = 2131099653;
        public static final int main_title_disable_during_settings = 2131099695;
        public static final int main_title_disable_during_summary = 2131099696;
        public static final int main_title_locked_volume_settings = 2131099651;
        public static final int main_title_locked_volume_settings_summary = 2131099652;
        public static final int main_title_misc_settings = 2131099694;
        public static final int main_title_notification_settings = 2131099673;
        public static final int main_title_notification_settings_summary = 2131099674;
        public static final int main_title_service_settings = 2131099709;
        public static final int main_title_service_settings_summary = 2131099710;
        public static final int notification_changed_alarm = 2131099723;
        public static final int notification_changed_dtmf = 2131099729;
        public static final int notification_changed_music = 2131099725;
        public static final int notification_changed_notification = 2131099726;
        public static final int notification_changed_ringer = 2131099724;
        public static final int notification_changed_system = 2131099727;
        public static final int notification_changed_voice_call = 2131099728;
        public static final int notification_content_lead_text = 2131099722;
        public static final int notification_content_title_end = 2131099721;
        public static final int notification_content_title_start = 2131099720;
        public static final int notification_ticker_text = 2131099719;
        public static final int popup_leading_text = 2131099731;
        public static final int popup_revert_button_text = 2131099733;
        public static final int popup_save_button_text = 2131099732;
        public static final int popup_title_text = 2131099730;
    }

    public static final class style {
        public static final int Theme_Transparent = 2131230720;
    }

    public static final class xml {
        public static final int enable_disable_widget = 2130968576;
        public static final int preferences = 2130968577;
    }
}
