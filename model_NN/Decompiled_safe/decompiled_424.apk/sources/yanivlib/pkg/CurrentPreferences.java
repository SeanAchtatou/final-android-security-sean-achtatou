package yanivlib.pkg;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.io.Serializable;

public class CurrentPreferences implements Serializable {
    protected static final String GAME_PREFERENCES = "GamePrefs";
    protected static final String GAME_PREFERENCES_ALERT_EXIT_GAME = "AlertExitGame";
    protected static final String GAME_PREFERENCES_ALERT_EXIT_PROGRAM = "AlertExitProgram";
    protected static final String GAME_PREFERENCES_ALLOW_MULLA_SLAPDOWN = "AllowMullaSlapDown";
    protected static final String GAME_PREFERENCES_CARDS_4COLOURS = "Cards4Colours";
    protected static final String GAME_PREFERENCES_CARD_ANIMATION = "CardAnimation";
    protected static final String GAME_PREFERENCES_CURRENT_SKILL_LEVEL = "CurrentSkillLevel";
    protected static final String GAME_PREFERENCES_DEBUG_MODE = "DebugMode";
    protected static final String GAME_PREFERENCES_FLASH_CARD_FROM_DISCARD = "FlashCardFromDiscard";
    protected static final String GAME_PREFERENCES_GAME_END_SCORE = "GameEndScore";
    protected static final String GAME_PREFERENCES_HALF_SCORES = "HalfScoresFactor";
    protected static final String GAME_PREFERENCES_NUMBER_OF_DECKS = "NumberOfDecks";
    protected static final String GAME_PREFERENCES_NUMBER_OF_PLAYERS = "NumberOfPlayers";
    protected static final String GAME_PREFERENCES_PAUSE_TIME_BETWEEN_TURNS = "PauseTimeBetweenTurns";
    protected static final String GAME_PREFERENCES_SKILL_LEVEL = "SkillLevel";
    protected static final String GAME_PREFERENCES_YANIV_AMOUNT = "YanivAmount";
    private static boolean alertExitGame = true;
    private static boolean alertExitProgram = true;
    private static boolean allowMullaSlapDown = false;
    private static int assafFine = 30;
    private static int assafedPlayerScore = 0;
    private static boolean cardAnimation = true;
    private static float cardAnimationTime = 0.7f;
    private static boolean cards4colours = false;
    private static CurrentPreferences currentPreferences = null;
    private static int currentSkillLevel = 3;
    private static boolean debugMode = false;
    private static boolean flashCardFromDiscard = true;
    private static int gameEndScore = gameEndScoreBase;
    public static final int gameEndScoreBase = 200;
    private static int halfScoresFactor = 100000;
    private static int levelRequired_CheckAverageRemainingCardsValue = 4;
    private static int levelRequired_CheckBeforeYaniv = 3;
    private static int levelRequired_LookAhead = 2;
    private static int levelRequired_LookAheadForStrFlush = 3;
    private static int levelRequired_TakePreviousPlayersPickupIntoAccount = 4;
    private static SharedPreferences mGamePreferences = null;
    private static int numberOfDecks = 4;
    private static int numberOfPlayers = 4;
    private static int pauseTimeBetweenTurns = 2;
    private static final long serialVersionUID = 7822796199978760103L;
    private static int skillLevel = 3;
    private static int winnerPlayerScore = 0;
    private static int yanivAmount = 7;

    private CurrentPreferences() {
    }

    public static CurrentPreferences getCurrentPreferences(Context context) {
        try {
            if (currentPreferences != null) {
                return currentPreferences;
            }
            currentPreferences = new CurrentPreferences();
            mGamePreferences = PreferenceManager.getDefaultSharedPreferences(context);
            currentPreferences.refresh();
            return currentPreferences;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refresh() {
        currentSkillLevel = Integer.valueOf(mGamePreferences.getString(GAME_PREFERENCES_CURRENT_SKILL_LEVEL, "0")).intValue();
        if (currentSkillLevel == 0) {
            setCurrentSkillLevel(skillLevel, true);
            setSkillLevel(0, true);
        } else {
            skillLevel = Integer.valueOf(mGamePreferences.getString(GAME_PREFERENCES_SKILL_LEVEL, "0")).intValue();
        }
        currentSkillLevel = Integer.valueOf(mGamePreferences.getString(GAME_PREFERENCES_CURRENT_SKILL_LEVEL, "1")).intValue();
        yanivAmount = Integer.valueOf(mGamePreferences.getString(GAME_PREFERENCES_YANIV_AMOUNT, "7")).intValue();
        numberOfDecks = Integer.valueOf(mGamePreferences.getString(GAME_PREFERENCES_NUMBER_OF_DECKS, "1")).intValue();
        numberOfPlayers = Integer.valueOf(mGamePreferences.getString(GAME_PREFERENCES_NUMBER_OF_PLAYERS, "4")).intValue();
        cardAnimation = mGamePreferences.getBoolean(GAME_PREFERENCES_CARD_ANIMATION, true);
        flashCardFromDiscard = mGamePreferences.getBoolean(GAME_PREFERENCES_FLASH_CARD_FROM_DISCARD, true);
        pauseTimeBetweenTurns = Integer.valueOf(mGamePreferences.getString(GAME_PREFERENCES_PAUSE_TIME_BETWEEN_TURNS, "3")).intValue();
        cards4colours = mGamePreferences.getBoolean(GAME_PREFERENCES_CARDS_4COLOURS, false);
        alertExitGame = mGamePreferences.getBoolean(GAME_PREFERENCES_ALERT_EXIT_GAME, true);
        alertExitProgram = mGamePreferences.getBoolean(GAME_PREFERENCES_ALERT_EXIT_PROGRAM, true);
        debugMode = mGamePreferences.getBoolean(GAME_PREFERENCES_DEBUG_MODE, false);
        allowMullaSlapDown = mGamePreferences.getBoolean(GAME_PREFERENCES_ALLOW_MULLA_SLAPDOWN, false);
        gameEndScore = Integer.valueOf(mGamePreferences.getString(GAME_PREFERENCES_GAME_END_SCORE, "200")).intValue();
    }

    public void initialise() {
        setSkillLevel(skillLevel, true);
        setCurrentSkillLevel(currentSkillLevel, true);
        setYanivAmount(yanivAmount, true);
        setgameEndScore(gameEndScore, true);
        setNumberOfDecks(numberOfDecks, true);
        setNumberOfPlayers(numberOfPlayers, true);
        setCardAnimation(cardAnimation, true);
        setFlashCardFromDiscard(flashCardFromDiscard, true);
        setPauseTimeBetweenTurns(pauseTimeBetweenTurns, true);
        setDebugMode(debugMode, true);
    }

    public int getCurrentSkillLevel() {
        return currentSkillLevel;
    }

    public void setCurrentSkillLevel(int inCurrentSkillLevel, boolean save) {
        currentSkillLevel = inCurrentSkillLevel;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putString(GAME_PREFERENCES_SKILL_LEVEL, Integer.toString(skillLevel));
            editor.commit();
        }
    }

    public int getSkillLevel() {
        return skillLevel;
    }

    public void setSkillLevel(int inSkillLevel, boolean save) {
        skillLevel = inSkillLevel;
        currentSkillLevel = inSkillLevel;
        setCurrentSkillLevel(currentSkillLevel, save);
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putString(GAME_PREFERENCES_SKILL_LEVEL, Integer.toString(skillLevel));
            editor.commit();
        }
    }

    public void setSkillLevelFromCurrentSkillLevel(boolean save) {
        setCurrentSkillLevel(currentSkillLevel, save);
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putString(GAME_PREFERENCES_SKILL_LEVEL, Integer.toString(currentSkillLevel));
            editor.commit();
        }
    }

    public int getYanivAmount() {
        return yanivAmount;
    }

    public void setYanivAmount(int inYanivAmount, boolean save) {
        yanivAmount = inYanivAmount;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putString(GAME_PREFERENCES_YANIV_AMOUNT, Integer.toString(yanivAmount));
            editor.commit();
        }
    }

    public int getNumberOfDecks() {
        return numberOfDecks;
    }

    public void setNumberOfDecks(int inNumberOfDecks, boolean save) {
        numberOfDecks = inNumberOfDecks;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putString(GAME_PREFERENCES_NUMBER_OF_DECKS, Integer.toString(numberOfDecks));
            editor.commit();
        }
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(int inNumberOfPlayers, boolean save) {
        numberOfPlayers = inNumberOfPlayers;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putString(GAME_PREFERENCES_NUMBER_OF_PLAYERS, Integer.toString(numberOfPlayers));
            editor.commit();
        }
    }

    public boolean getCardAnimation() {
        return cardAnimation;
    }

    public void setCardAnimation(boolean inCardAnimation, boolean save) {
        cardAnimation = inCardAnimation;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putBoolean(GAME_PREFERENCES_CARD_ANIMATION, cardAnimation);
            editor.commit();
        }
    }

    public int getCardAnimationTime() {
        if (cardAnimation) {
            return (int) (cardAnimationTime * 1000.0f);
        }
        return 0;
    }

    public boolean getFlashCardFromDiscard() {
        return flashCardFromDiscard;
    }

    public void setFlashCardFromDiscard(boolean inFlashCardFromDiscard, boolean save) {
        flashCardFromDiscard = inFlashCardFromDiscard;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putBoolean(GAME_PREFERENCES_FLASH_CARD_FROM_DISCARD, flashCardFromDiscard);
            editor.commit();
        }
    }

    public boolean getAlertExitGame() {
        return alertExitGame;
    }

    public void setAlertExitGame(boolean inAlertExitGame, boolean save) {
        alertExitGame = inAlertExitGame;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putBoolean(GAME_PREFERENCES_ALERT_EXIT_GAME, alertExitGame);
            editor.commit();
        }
    }

    public boolean getAlertExitProgram() {
        return alertExitProgram;
    }

    public void setAlertExitProgram(boolean inAlertExitProgram, boolean save) {
        alertExitProgram = inAlertExitProgram;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putBoolean(GAME_PREFERENCES_ALERT_EXIT_PROGRAM, alertExitProgram);
            editor.commit();
        }
    }

    public boolean getDebugMode() {
        return debugMode;
    }

    public void setDebugMode(boolean inDebugMode, boolean save) {
        debugMode = inDebugMode;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putBoolean(GAME_PREFERENCES_DEBUG_MODE, debugMode);
            editor.commit();
        }
    }

    public boolean getCards4colours() {
        return cards4colours;
    }

    public void setCards4colours(boolean inCards4colours, boolean save) {
        cards4colours = inCards4colours;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putBoolean(GAME_PREFERENCES_CARDS_4COLOURS, cards4colours);
            editor.commit();
        }
    }

    public boolean getAllowMullaSlapdown() {
        return allowMullaSlapDown;
    }

    public void setAllMullaSlapDown(boolean inAllowMullaSlapDown, boolean save) {
        allowMullaSlapDown = inAllowMullaSlapDown;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putBoolean(GAME_PREFERENCES_ALLOW_MULLA_SLAPDOWN, allowMullaSlapDown);
            editor.commit();
        }
    }

    public int getGameEndScore() {
        return gameEndScore;
    }

    public void setgameEndScore(int inGameEndScore, boolean save) {
        gameEndScore = inGameEndScore;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putString(GAME_PREFERENCES_GAME_END_SCORE, Integer.toString(gameEndScore));
            editor.commit();
        }
    }

    public void setPauseTimeBetweenTurns(int inPauseTimeBetweenTurns, boolean save) {
        pauseTimeBetweenTurns = inPauseTimeBetweenTurns;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putString(GAME_PREFERENCES_PAUSE_TIME_BETWEEN_TURNS, Integer.toString(pauseTimeBetweenTurns));
            editor.commit();
        }
    }

    public int getHalfScoresFactor() {
        return halfScoresFactor;
    }

    public void setHalfScoresFactor(int inHalfScoresFactor, boolean save) {
        halfScoresFactor = inHalfScoresFactor;
        if (save) {
            SharedPreferences.Editor editor = mGamePreferences.edit();
            editor.putString(GAME_PREFERENCES_HALF_SCORES, Integer.toString(halfScoresFactor));
            editor.commit();
        }
    }

    public int getPauseTimeBetweenTurns() {
        return pauseTimeBetweenTurns;
    }

    public int getAssafFine() {
        return assafFine;
    }

    public int getWinnerPlayerScore() {
        return winnerPlayerScore;
    }

    public int getAssafedPlayerScore() {
        return assafedPlayerScore;
    }

    public boolean shouldLookAhead() {
        return skillLevel >= levelRequired_LookAhead;
    }

    public boolean shouldLookAheadForStrFlush() {
        return skillLevel >= levelRequired_LookAheadForStrFlush;
    }

    public boolean shouldCheckBeforeYaniv() {
        return skillLevel >= levelRequired_CheckBeforeYaniv;
    }

    public boolean shouldTakePreviousPlayersPickupIntoAccount() {
        return skillLevel >= levelRequired_TakePreviousPlayersPickupIntoAccount;
    }

    public boolean shouldCheckAverageRemainingCardsValue() {
        return skillLevel >= levelRequired_CheckAverageRemainingCardsValue;
    }
}
