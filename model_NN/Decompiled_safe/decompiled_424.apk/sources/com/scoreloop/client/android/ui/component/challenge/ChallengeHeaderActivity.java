package com.scoreloop.client.android.ui.component.challenge;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.framework.ValueStore;
import il.co.anykey.games.yaniv.lite.R;

public class ChallengeHeaderActivity extends ComponentHeaderActivity implements View.OnClickListener {
    public void onClick(View view) {
        if (getSession().isAuthenticated()) {
            display(getFactory().createChallengePaymentScreenDescription());
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.sl_header_default);
        setCaption(getGame().getName());
        if (((Integer) getActivityArguments().getValue(Constant.CHALLENGE_HEADER_MODE, 0)).intValue() == 0) {
            getImageView().setImageDrawable(getResources().getDrawable(R.drawable.sl_header_icon_challenges));
            setTitle(getResources().getString(R.string.sl_challenges));
            showControlIcon(R.drawable.sl_button_add_coins);
        } else {
            getImageView().setImageDrawable(getResources().getDrawable(R.drawable.sl_header_icon_add_coins));
            setTitle(getResources().getString(R.string.sl_add_coins));
        }
        addObservedKeys(ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.USER_BALANCE));
    }

    private void showControlIcon(int resId) {
        ImageView icon = (ImageView) findViewById(R.id.sl_control_icon);
        icon.setImageResource(resId);
        icon.setEnabled(true);
        icon.setOnClickListener(this);
    }

    public void onStart() {
        super.onStart();
        getUserValues().setDirty(Constant.USER_BALANCE);
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        setSubTitle(StringFormatter.getBalanceSubTitle(this, getUserValues(), getConfiguration()));
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (Constant.USER_BALANCE.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
    }
}
