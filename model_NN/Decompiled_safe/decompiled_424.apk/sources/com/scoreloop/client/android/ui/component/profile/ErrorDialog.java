package com.scoreloop.client.android.ui.component.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import il.co.anykey.games.yaniv.lite.R;

public class ErrorDialog extends BaseDialog {
    private String _text;
    private String _title;

    public ErrorDialog(Context context) {
        super(context);
        setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public int getContentViewLayoutId() {
        return R.layout.sl_dialog_error;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.sl_button_ok) {
            dismiss();
        }
    }

    public void setTitle(String title) {
        this._title = title;
        updateUi();
    }

    public void setText(String text) {
        this._text = text;
        updateUi();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
        updateUi();
    }

    private void updateUi() {
        ((TextView) findViewById(R.id.sl_title)).setText(this._title);
        ((TextView) findViewById(R.id.sl_error_message)).setText(this._text);
    }
}
