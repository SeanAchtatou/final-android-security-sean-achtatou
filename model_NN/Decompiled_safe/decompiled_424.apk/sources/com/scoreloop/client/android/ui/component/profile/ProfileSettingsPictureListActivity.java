package com.scoreloop.client.android.ui.component.profile;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.ImageSource;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.util.Base64;
import com.scoreloop.client.android.ui.util.ImageHelper;
import com.scoreloop.client.android.ui.util.LocalImageStorage;
import il.co.anykey.games.yaniv.lite.R;
import java.io.ByteArrayOutputStream;

public class ProfileSettingsPictureListActivity extends ComponentListActivity<BaseListItem> implements RequestControllerObserver, SocialProviderControllerObserver {
    private static final int IMAGE_SIZE = 144;
    private static final int PICK_PICTURE = 1;
    private Runnable _continuation;
    /* access modifiers changed from: private */
    public ProfilePictureListItem _deviceLibraryItem;
    /* access modifiers changed from: private */
    public ProfilePictureListItem _facebookItem;
    /* access modifiers changed from: private */
    public ProfilePictureListItem _myspaceItem;
    /* access modifiers changed from: private */
    public ProfilePictureListItem _setDefaultItem;
    /* access modifiers changed from: private */
    public ProfilePictureListItem _twitterItem;
    private User _user;
    private UserController _userController;

    class PictureListAdapter extends BaseListAdapter<BaseListItem> {
        public PictureListAdapter(Context context) {
            super(context);
            add(new CaptionListItem(context, null, ProfileSettingsPictureListActivity.this.getString(R.string.sl_change_picture)));
            add(ProfileSettingsPictureListActivity.this._deviceLibraryItem);
            add(ProfileSettingsPictureListActivity.this._facebookItem);
            add(ProfileSettingsPictureListActivity.this._twitterItem);
            add(ProfileSettingsPictureListActivity.this._myspaceItem);
            add(ProfileSettingsPictureListActivity.this._setDefaultItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (data != null && data.getData() != null && !data.getData().toString().trim().equals("")) {
            getHandler().post(new Runnable() {
                public void run() {
                    Uri localImageUri = data.getData();
                    ProfileSettingsPictureListActivity.this.startSubmitPicture(ImageHelper.createThumbnail(localImageUri, ProfileSettingsPictureListActivity.this.getContentResolver(), ProfileSettingsPictureListActivity.IMAGE_SIZE, ImageHelper.getExifOrientation(localImageUri, ProfileSettingsPictureListActivity.this.getContentResolver(), ProfileSettingsPictureListActivity.this.getApplicationContext())), localImageUri);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void startSubmitPicture(Bitmap bitmap, Uri localImageUri) {
        showSpinnerFor(this._userController);
        String imageUrl = localImageUri.toString();
        if (LocalImageStorage.putBitmap(this, imageUrl, bitmap)) {
            getUserValues().putValue(Constant.USER_IMAGE_URL, imageUrl);
        }
        this._user.setImageSource(ImageSource.IMAGE_SOURCE_SCORELOOP);
        this._user.setImageMimeType("image/png");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
        this._user.setImageData(Base64.encodeBytes(out.toByteArray()));
        this._userController.submitUser();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Resources res = getResources();
        this._deviceLibraryItem = new ProfilePictureListItem(this, res.getDrawable(R.drawable.sl_icon_device), getString(R.string.sl_device_library));
        this._facebookItem = new ProfilePictureListItem(this, res.getDrawable(R.drawable.sl_icon_facebook), getString(R.string.sl_facebook));
        this._twitterItem = new ProfilePictureListItem(this, res.getDrawable(R.drawable.sl_icon_twitter), getString(R.string.sl_twitter));
        this._myspaceItem = new ProfilePictureListItem(this, res.getDrawable(R.drawable.sl_icon_myspace), getString(R.string.sl_myspace));
        this._setDefaultItem = new ProfilePictureListItem(this, res.getDrawable(R.drawable.sl_icon_user), getString(R.string.sl_set_default));
        setListAdapter(new PictureListAdapter(this));
        this._user = getSessionUser();
        this._userController = new UserController(this);
        this._userController.setUser(this._user);
    }

    public void onListItemClick(BaseListItem item) {
        if (item == this._deviceLibraryItem) {
            pickDeviceLibraryPicture();
        } else if (item == this._facebookItem) {
            withConnectedProvider(SocialProvider.FACEBOOK_IDENTIFIER, new Runnable() {
                public void run() {
                    ProfileSettingsPictureListActivity.this.pickFacebookPicture();
                }
            });
        } else if (item == this._twitterItem) {
            withConnectedProvider(SocialProvider.TWITTER_IDENTIFIER, new Runnable() {
                public void run() {
                    ProfileSettingsPictureListActivity.this.pickTwitterPicture();
                }
            });
        } else if (item == this._myspaceItem) {
            withConnectedProvider(SocialProvider.MYSPACE_IDENTIFIER, new Runnable() {
                public void run() {
                    ProfileSettingsPictureListActivity.this.pickMyspacePicture();
                }
            });
        } else if (item == this._setDefaultItem) {
            pickDefaultPicture();
        }
    }

    private void pickDefaultPicture() {
        this._user.setImageSource(ImageSource.IMAGE_SOURCE_DEFAULT);
        this._user.setImageMimeType(null);
        this._user.setImageData(null);
        showSpinnerFor(this._userController);
        this._userController.submitUser();
    }

    private void pickDeviceLibraryPicture() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction("android.intent.action.GET_CONTENT");
        intent.putExtra("windowTitle", getString(R.string.sl_choose_photo));
        try {
            startActivityForResult(intent, 1);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void pickFacebookPicture() {
        this._user.setImageSource(SocialProvider.getSocialProviderForIdentifier(SocialProvider.FACEBOOK_IDENTIFIER));
        this._user.setImageMimeType(null);
        this._user.setImageData(null);
        showSpinnerFor(this._userController);
        this._userController.submitUser();
    }

    /* access modifiers changed from: private */
    public void pickMyspacePicture() {
        this._user.setImageSource(SocialProvider.getSocialProviderForIdentifier(SocialProvider.MYSPACE_IDENTIFIER));
        this._user.setImageMimeType(null);
        this._user.setImageData(null);
        showSpinnerFor(this._userController);
        this._userController.submitUser();
    }

    /* access modifiers changed from: private */
    public void pickTwitterPicture() {
        this._user.setImageSource(SocialProvider.getSocialProviderForIdentifier(SocialProvider.TWITTER_IDENTIFIER));
        this._user.setImageMimeType(null);
        this._user.setImageData(null);
        showSpinnerFor(this._userController);
        this._userController.submitUser();
    }

    /* access modifiers changed from: protected */
    public void requestControllerDidFailSafe(RequestController aRequestController, Exception anException) {
        super.requestControllerDidFailSafe(aRequestController, anException);
        getUserValues().putValue(Constant.USER_IMAGE_URL, this._user.getImageUrl());
    }

    public void requestControllerDidReceiveResponseSafe(RequestController controller) {
        getUserValues().putValue(Constant.USER_IMAGE_URL, this._user.getImageUrl());
        hideSpinnerFor(controller);
    }

    public void socialProviderControllerDidCancel(SocialProviderController controller) {
        hideSpinnerFor(controller);
    }

    public void socialProviderControllerDidEnterInvalidCredentials(SocialProviderController controller) {
        socialProviderControllerDidFail(controller, new RuntimeException("Invalid Credentials"));
    }

    public void socialProviderControllerDidFail(SocialProviderController controller, Throwable error) {
        hideSpinnerFor(controller);
        showToast(String.format(getString(R.string.sl_format_connect_failed), controller.getSocialProvider().getName()));
    }

    public void socialProviderControllerDidSucceed(SocialProviderController controller) {
        hideSpinnerFor(controller);
        if (!isPaused() && this._continuation != null) {
            this._continuation.run();
        }
    }

    private void withConnectedProvider(String socialProviderIdentifier, Runnable runnable) {
        SocialProvider socialProvider = SocialProvider.getSocialProviderForIdentifier(socialProviderIdentifier);
        if (socialProvider.isUserConnected(getSessionUser())) {
            runnable.run();
            return;
        }
        SocialProviderController socialProviderController = SocialProviderController.getSocialProviderController(getSession(), this, socialProvider);
        this._continuation = runnable;
        showSpinnerFor(socialProviderController);
        socialProviderController.connect(this);
    }
}
