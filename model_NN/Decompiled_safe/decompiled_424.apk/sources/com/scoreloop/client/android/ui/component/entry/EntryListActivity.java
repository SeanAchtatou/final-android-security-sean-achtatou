package com.scoreloop.client.android.ui.component.entry;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Configuration;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.Factory;
import com.scoreloop.client.android.ui.component.base.PackageManager;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.framework.ValueStore;
import il.co.anykey.games.yaniv.lite.R;

public class EntryListActivity extends ComponentListActivity<BaseListItem> {
    /* access modifiers changed from: private */
    public EntryListItem achievementsItem;
    /* access modifiers changed from: private */
    public EntryListItem challengesItem;
    /* access modifiers changed from: private */
    public EntryListItem leaderboardsItem;
    /* access modifiers changed from: private */
    public EntryListItem newsItem;

    class EntryListAdapter extends BaseListAdapter<BaseListItem> {
        public EntryListAdapter(Context context) {
            super(context);
            Resources res = context.getResources();
            Configuration configuration = EntryListActivity.this.getConfiguration();
            add(new CaptionListItem(context, null, EntryListActivity.this.getGame().getName()));
            EntryListActivity.this.leaderboardsItem = new EntryListItem(EntryListActivity.this, res.getDrawable(R.drawable.sl_icon_leaderboards), context.getString(R.string.sl_leaderboards), null);
            add(EntryListActivity.this.leaderboardsItem);
            if (configuration.isFeatureEnabled(Configuration.Feature.ACHIEVEMENT)) {
                EntryListActivity.this.achievementsItem = new EntryListItem(EntryListActivity.this, res.getDrawable(R.drawable.sl_icon_achievements), context.getString(R.string.sl_achievements), null);
                add(EntryListActivity.this.achievementsItem);
            }
            if (configuration.isFeatureEnabled(Configuration.Feature.CHALLENGE)) {
                EntryListActivity.this.challengesItem = new EntryListItem(EntryListActivity.this, res.getDrawable(R.drawable.sl_icon_challenges), context.getString(R.string.sl_challenges), null);
                add(EntryListActivity.this.challengesItem);
            }
            if (configuration.isFeatureEnabled(Configuration.Feature.NEWS)) {
                EntryListActivity.this.newsItem = new EntryListItem(EntryListActivity.this, res.getDrawable(R.drawable.sl_icon_news_closed), context.getString(R.string.sl_news), null);
                add(EntryListActivity.this.newsItem);
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new EntryListAdapter(this));
        addObservedKeys(ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_ACHIEVEMENTS), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_CHALLENGES_WON), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NEWS_NUMBER_UNREAD_ITEMS));
    }

    public void onListItemClick(BaseListItem item) {
        Factory factory = getFactory();
        if (item == this.leaderboardsItem) {
            display(factory.createScoreScreenDescription(getGame(), null, null));
        } else if (item == this.challengesItem) {
            display(factory.createChallengeScreenDescription(getUser()));
        } else if (item == this.achievementsItem) {
            display(factory.createAchievementScreenDescription(getUser()));
        } else if (item == this.newsItem) {
            display(factory.createNewsScreenDescription());
        }
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        if (isValueChangedFor(key, Constant.NUMBER_ACHIEVEMENTS, oldValue, newValue)) {
            this.achievementsItem.setSubTitle(StringFormatter.getAchievementsSubTitle(this, getUserValues(), false));
            getBaseListAdapter().notifyDataSetChanged();
        } else if (isValueChangedFor(key, Constant.NUMBER_CHALLENGES_WON, oldValue, newValue)) {
            this.challengesItem.setSubTitle(StringFormatter.getChallengesSubTitle(this, getUserValues()));
            getBaseListAdapter().notifyDataSetChanged();
        } else if (isValueChangedFor(key, Constant.NEWS_NUMBER_UNREAD_ITEMS, oldValue, newValue)) {
            this.newsItem.setSubTitle(StringFormatter.getNewsSubTitle(this, getUserValues()));
            this.newsItem.setDrawable(StringFormatter.getNewsDrawable(this, getUserValues(), false));
            getBaseListAdapter().notifyDataSetChanged();
        }
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        Configuration configuration = getConfiguration();
        if (configuration.isFeatureEnabled(Configuration.Feature.ACHIEVEMENT) && key.equals(Constant.NUMBER_ACHIEVEMENTS)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
        if (configuration.isFeatureEnabled(Configuration.Feature.CHALLENGE) && key.equals(Constant.NUMBER_CHALLENGES_WON)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
        if (configuration.isFeatureEnabled(Configuration.Feature.NEWS) && key.equals(Constant.NEWS_NUMBER_UNREAD_ITEMS)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_OLDER_THAN, Long.valueOf((long) Constant.NEWS_FEED_REFRESH_TIME));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        hideFooter();
        if (!PackageManager.isScoreloopAppInstalled(this)) {
            showFooter(new StandardListItem(this, getResources().getDrawable(R.drawable.sl_icon_scoreloop), getString(R.string.sl_slapp_title), getString(R.string.sl_slapp_subtitle), null));
        }
    }

    /* access modifiers changed from: protected */
    public void onFooterItemClick(BaseListItem footerItem) {
        hideFooter();
        PackageManager.installScoreloopApp(this);
    }
}
