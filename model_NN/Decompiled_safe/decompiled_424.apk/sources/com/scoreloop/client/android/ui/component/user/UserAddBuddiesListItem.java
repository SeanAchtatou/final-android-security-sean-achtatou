package com.scoreloop.client.android.ui.component.user;

import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import il.co.anykey.games.yaniv.lite.R;

public class UserAddBuddiesListItem extends StandardListItem<Void> {
    public UserAddBuddiesListItem(ComponentActivity activity) {
        super(activity, null, activity.getString(R.string.sl_add_friends), null, null);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_user_add_buddies;
    }

    public int getType() {
        return 25;
    }
}
