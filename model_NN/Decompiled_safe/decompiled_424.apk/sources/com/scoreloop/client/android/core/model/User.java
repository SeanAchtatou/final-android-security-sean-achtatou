package com.scoreloop.client.android.core.model;

import com.google.ads.AdActivity;
import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__2_0_0;
import com.scoreloop.client.android.core.PublishedFor__2_1_0;
import com.scoreloop.client.android.core.util.Formats;
import com.scoreloop.client.android.core.util.SetterIntent;
import com.scoreloop.client.android.ui.component.base.Constant;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class User extends BaseEntity implements MessageTargetInterface {
    public static String a = Constant.USER;
    private static Map<String, b> c = new HashMap();
    private List<SearchList> A;
    private Map<String, JSONObject> B;
    private Activity C;
    private String D;
    private String E;
    private String F;
    private String G;
    private String H;
    private Integer I;
    private Integer J;
    private Integer K;
    private boolean L;
    private a d;
    private boolean e;
    private String f;
    private String g;
    private String h;
    private Integer i;
    private String j;
    private List<User> k;
    private Map<String, Object> l;
    private Long m;
    private Details n;
    private Date o;
    private String p;
    private String q;
    private Gender r;
    private a s;
    private Date t;
    private String u;
    private String v;
    private Integer w;
    private b x;
    private a y;
    private List<SearchList> z;

    public class Details {
        private Double b;
        private Integer c;
        private Integer d;

        public Details() {
        }

        /* access modifiers changed from: package-private */
        public void a(JSONObject jSONObject) throws JSONException {
            SetterIntent setterIntent = new SetterIntent();
            if (setterIntent.c(jSONObject, "winning_probability", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                this.b = (Double) setterIntent.a();
            }
            if (setterIntent.d(jSONObject, "challenges_lost", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                this.d = (Integer) setterIntent.a();
            }
            if (setterIntent.d(jSONObject, "challenges_won", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
                this.c = (Integer) setterIntent.a();
            }
        }

        @PublishedFor__1_0_0
        public Integer getChallengesLost() {
            return this.d;
        }

        @PublishedFor__1_0_0
        public Integer getChallengesWon() {
            return this.c;
        }

        @PublishedFor__1_0_0
        public Double getWinningProbability() {
            return this.b;
        }
    }

    public enum Gender {
        FEMALE("f"),
        MALE(AdActivity.TYPE_PARAM),
        UNKNOWN("?");
        
        private String a;

        private Gender(String str) {
            this.a = str;
        }

        public String getJSONString() {
            return this.a;
        }
    }

    static class a {
        private final String a;
        private final int b;
        private final int c;

        public a(JSONObject jSONObject) throws JSONException {
            SetterIntent setterIntent = new SetterIntent();
            this.a = setterIntent.d(jSONObject, "decoration", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
            this.b = setterIntent.a(jSONObject, "threshold", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE).intValue();
            this.c = setterIntent.a(jSONObject, "value", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE).intValue();
        }
    }

    private enum b {
        anonymous,
        active,
        deleted,
        passive,
        pending,
        suspended
    }

    static {
        c.put("anonymous", b.anonymous);
        c.put("active", b.active);
        c.put("deleted", b.deleted);
        c.put("passive", b.passive);
        c.put("pending", b.pending);
        c.put("suspended", b.suspended);
    }

    public User() {
        this.r = Gender.UNKNOWN;
        this.B = new HashMap();
        this.L = false;
        this.n = new Details();
    }

    private User(String str) {
        super(str);
        this.r = Gender.UNKNOWN;
        this.B = new HashMap();
        this.L = false;
        this.n = new Details();
    }

    public User(JSONObject jSONObject) throws JSONException {
        this();
        a(jSONObject);
    }

    public static Entity a(Session session, String str) {
        if (session != null) {
            User user = session.getUser();
            if (str != null && str.equals(user.getIdentifier())) {
                return user;
            }
        }
        return new User(str);
    }

    private ImageSource e(String str) {
        if (str == null || str.equals(ImageSource.IMAGE_SOURCE_DEFAULT.getName())) {
            return ImageSource.IMAGE_SOURCE_DEFAULT;
        }
        if (str.equals(ImageSource.IMAGE_SOURCE_SCORELOOP.getName())) {
            return ImageSource.IMAGE_SOURCE_SCORELOOP;
        }
        for (SocialProvider next : SocialProvider.getSupportedProviders()) {
            if (str.equals(next.getName())) {
                return next;
            }
        }
        return null;
    }

    private Integer i() {
        return this.i;
    }

    private String j() {
        return this.j;
    }

    private b k() {
        return this.x;
    }

    public String a() {
        return a;
    }

    public void a(Long l2) {
        this.m = l2;
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
        String login = getLogin();
        if (login == null) {
            login = getIdentifier() == null ? "[empty user]" : getIdentifier();
        }
        a(obj, "name", login);
    }

    public void a(String str) {
        this.p = str;
    }

    public void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        SetterIntent setterIntent = new SetterIntent();
        if (setterIntent.h(jSONObject, "login", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            setLogin((String) setterIntent.a());
        }
        if (setterIntent.h(jSONObject, "email", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.q = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "state", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            String lowerCase = ((String) setterIntent.a()).toLowerCase();
            if (!c.containsKey(lowerCase)) {
                throw new IllegalStateException("could not parse json representation of User due to unknown state given: '" + lowerCase + "'");
            }
            this.x = c.get(lowerCase);
        }
        if (setterIntent.h(jSONObject, "device_id", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.p = (String) setterIntent.a();
        }
        if (setterIntent.h(jSONObject, "gender", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            String str = (String) setterIntent.a();
            if (AdActivity.TYPE_PARAM.equalsIgnoreCase(str)) {
                this.r = Gender.MALE;
            } else if ("f".equalsIgnoreCase(str)) {
                this.r = Gender.FEMALE;
            } else {
                this.r = Gender.UNKNOWN;
            }
        }
        if (setterIntent.b(jSONObject, "date_of_birth", SetterIntent.ValueMode.ALLOWS_NULL_VALUE)) {
            this.o = (Date) setterIntent.a();
        }
        if (setterIntent.f(jSONObject, "balance", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            JSONObject jSONObject2 = (JSONObject) setterIntent.a();
            this.i = setterIntent.a(jSONObject2, "amount", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
            this.j = setterIntent.d(jSONObject2, "currency", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        }
        if (setterIntent.f(jSONObject, "avatar", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            JSONObject jSONObject3 = (JSONObject) setterIntent.a();
            this.h = setterIntent.d(jSONObject3, "head", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
            this.g = setterIntent.d(jSONObject3, "hair", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
            this.f = setterIntent.d(jSONObject3, "body", SetterIntent.KeyMode.THROWS_WHEN_NO_KEY, SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE);
        }
        SocialProvider.b(this, jSONObject);
        if (setterIntent.f(jSONObject, "skill", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.w = setterIntent.a((JSONObject) setterIntent.a(), "value", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
        }
        if (setterIntent.f(jSONObject, "agility", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.d = new a((JSONObject) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "strategy", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.y = new a((JSONObject) setterIntent.a());
        }
        if (setterIntent.f(jSONObject, "knowledge", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.s = new a((JSONObject) setterIntent.a());
        }
        if (setterIntent.e(jSONObject, "score_lists", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            JSONArray jSONArray = (JSONArray) setterIntent.a();
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                arrayList.add(new SearchList(jSONArray.getJSONObject(i2)));
            }
            this.z = arrayList;
        }
        if (setterIntent.e(jSONObject, "challenge_lists", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            JSONArray jSONArray2 = (JSONArray) setterIntent.a();
            ArrayList arrayList2 = new ArrayList();
            for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                arrayList2.add(new SearchList(jSONArray2.getJSONObject(i3)));
            }
            this.A = arrayList2;
        }
        if (setterIntent.b(jSONObject, "last_active_at", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.t = (Date) setterIntent.a();
        }
        if (setterIntent.f(jSONObject, "last_activity", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.C = new Activity(null, (JSONObject) setterIntent.a());
        }
        if (setterIntent.h(jSONObject, "nationality", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.D = (String) setterIntent.a();
        }
        if (setterIntent.e(jSONObject, "buddies", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            JSONArray jSONArray3 = (JSONArray) setterIntent.a();
            ArrayList arrayList3 = new ArrayList();
            for (int i4 = 0; i4 < jSONArray3.length(); i4++) {
                arrayList3.add(new User(jSONArray3.getJSONObject(i4)));
            }
            this.k = arrayList3;
        }
        JSONObject b2 = setterIntent.b(jSONObject, "image", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
        if (b2 != null) {
            this.E = setterIntent.d(b2, "source", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
            this.F = setterIntent.d(b2, "url", SetterIntent.KeyMode.USE_NULL_WHEN_NO_KEY, SetterIntent.ValueMode.ALLOWS_NULL_VALUE);
        }
        if ((this.L && b2 == null) || getImageSource() == ImageSource.IMAGE_SOURCE_DEFAULT) {
            this.E = null;
            this.F = null;
        }
        this.L = false;
        this.n.a(jSONObject);
        if (setterIntent.d(jSONObject, "games_counter", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.I = (Integer) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "buddies_counter", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.J = (Integer) setterIntent.a();
        }
        if (setterIntent.d(jSONObject, "global_achievements_counter", SetterIntent.ValueMode.REQUIRES_NON_NULL_VALUE)) {
            this.K = (Integer) setterIntent.a();
        }
    }

    public void a(JSONObject jSONObject, String str) {
        if (jSONObject != null) {
            this.B.put(str, jSONObject);
        } else {
            this.B.remove(str);
        }
    }

    public void a(JSONObject jSONObject, boolean z2) throws JSONException {
        this.L = z2;
        a(jSONObject);
    }

    public void a(boolean z2) {
        this.e = z2;
    }

    public void b(String str) {
        super.b(str);
    }

    public Money c() {
        return (i() == null || j() == null) ? new Money("SLD", new BigDecimal(0)) : new Money(j(), new BigDecimal(i().intValue()));
    }

    public boolean c(String str) {
        return this.B.containsKey(str);
    }

    public JSONObject d() throws JSONException {
        JSONObject d2 = super.d();
        d2.put("login", this.u);
        d2.put("device_id", this.p);
        d2.put("password", this.v);
        d2.put("password_confirmation", this.v);
        d2.put("email", this.q);
        if (this.r != Gender.UNKNOWN) {
            d2.put("gender", this.r.getJSONString());
        }
        if (this.o != null) {
            d2.put("date_of_birth", Formats.b.format(this.o));
        }
        if (!(this.f == null && this.g == null && this.f == null)) {
            JSONObject jSONObject = new JSONObject();
            d2.put("avatar", jSONObject);
            if (this.f != null) {
                jSONObject.put("hair", this.g);
            }
            if (this.h != null) {
                jSONObject.put("head", this.h);
            }
            if (this.f != null) {
                jSONObject.put("body", this.f);
            }
        }
        if (this.D != null) {
            d2.put("nationality", this.D);
        }
        JSONObject jSONObject2 = new JSONObject();
        ImageSource imageSource = getImageSource();
        if (imageSource == ImageSource.IMAGE_SOURCE_SCORELOOP) {
            if (!(this.G == null || this.H == null)) {
                jSONObject2.put("source", imageSource.getName());
                jSONObject2.put("data", this.G);
                jSONObject2.put("mime_type", this.H);
                d2.put("image", jSONObject2);
                this.G = null;
                this.H = null;
            }
        } else if (imageSource != null) {
            jSONObject2.put("source", imageSource.getName());
            d2.put("image", jSONObject2);
        }
        SocialProvider.c(this, d2);
        return d2;
    }

    public JSONObject d(String str) {
        return this.B.get(str);
    }

    public String e() {
        return this.p;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof User)) {
            return super.equals(obj);
        }
        User user = (User) obj;
        if (getIdentifier() != null && user.getIdentifier() != null) {
            return super.equals(obj);
        }
        String login = getLogin();
        String login2 = user.getLogin();
        return (login == null || login2 == null || !login.equalsIgnoreCase(login2)) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public List<SearchList> f() {
        return this.z;
    }

    public JSONObject g() {
        return this.B.get(SocialProvider.FACEBOOK_IDENTIFIER);
    }

    @PublishedFor__2_0_0
    public Integer getBuddiesCounter() {
        return this.J;
    }

    @PublishedFor__1_0_0
    public List<User> getBuddyUsers() {
        return this.k;
    }

    @PublishedFor__1_0_0
    public Map<String, Object> getContext() {
        return this.l;
    }

    @PublishedFor__1_0_0
    public Date getDateOfBirth() {
        return this.o;
    }

    @PublishedFor__1_0_0
    public Details getDetail() {
        return this.n;
    }

    @PublishedFor__1_0_0
    public String getDisplayName() {
        if (this.u != null && !this.u.equals("")) {
            return this.u;
        }
        if (g() == null) {
            return (this.q == null || this.q.equals("")) ? "unknown" : this.q;
        }
        try {
            return String.format("%s %s", g().getString("first_name"), g().getString("last_name"));
        } catch (JSONException e2) {
            return "unknown";
        }
    }

    @PublishedFor__1_0_0
    public String getEmailAddress() {
        return this.q;
    }

    @PublishedFor__2_0_0
    public Integer getGamesCounter() {
        return this.I;
    }

    @PublishedFor__2_0_0
    public Integer getGlobalAchievementsCounter() {
        return this.K;
    }

    @PublishedFor__2_0_0
    public ImageSource getImageSource() {
        return e(this.E);
    }

    @PublishedFor__2_0_0
    public String getImageUrl() {
        return this.F;
    }

    @PublishedFor__1_0_0
    public Date getLastActiveAt() {
        return this.t;
    }

    @PublishedFor__1_0_0
    public Activity getLastActivity() {
        return this.C;
    }

    @PublishedFor__1_0_0
    public String getLogin() {
        return this.u;
    }

    @PublishedFor__1_0_0
    public Integer getSkillValue() {
        return this.w;
    }

    public Long h() {
        return this.m;
    }

    public int hashCode() {
        if (getIdentifier() != null) {
            return super.hashCode();
        }
        String login = getLogin();
        if (login == null) {
            login = "";
        }
        return login.hashCode();
    }

    @PublishedFor__1_0_0
    public boolean isActive() {
        return b.active.equals(k());
    }

    @PublishedFor__1_0_0
    public boolean isAnonymous() {
        return b.anonymous.equals(k());
    }

    @PublishedFor__1_0_0
    public boolean isAuthenticated() {
        return this.e;
    }

    @PublishedFor__1_0_0
    public boolean isChallengable() {
        return isAnonymous() || isPassive() || isPending() || isActive();
    }

    @PublishedFor__1_0_0
    @Deprecated
    public boolean isConnectedToSocialProviderWithIdentifier(String str) {
        SocialProvider socialProviderForIdentifier = SocialProvider.getSocialProviderForIdentifier(str);
        if (socialProviderForIdentifier != null) {
            return socialProviderForIdentifier.isUserConnected(this);
        }
        throw new IllegalArgumentException("could not find provider for id: '" + str + "'");
    }

    @PublishedFor__1_0_0
    public boolean isPassive() {
        return b.passive.equals(k());
    }

    @PublishedFor__1_0_0
    public boolean isPending() {
        return b.pending.equals(k());
    }

    @PublishedFor__2_0_0
    @Deprecated
    public boolean ownsSession(Session session) {
        return session.getUser().equals(this);
    }

    @PublishedFor__1_0_0
    public void setContext(Map<String, Object> map) {
        this.l = map;
    }

    @PublishedFor__1_0_0
    public void setDateOfBirth(Date date) {
        this.o = date;
    }

    @PublishedFor__1_0_0
    public void setEmailAddress(String str) {
        this.q = str;
    }

    @PublishedFor__2_0_0
    public void setImageData(String str) {
        this.G = str;
    }

    @PublishedFor__2_1_0
    public void setImageMimeType(String str) {
        this.H = str;
    }

    @PublishedFor__2_0_0
    public void setImageSource(ImageSource imageSource) {
        if (imageSource == null) {
            this.E = ImageSource.IMAGE_SOURCE_DEFAULT.getName();
        } else {
            this.E = imageSource.getName();
        }
    }

    @PublishedFor__2_1_0
    public void setImageSource(String str) {
        this.E = str;
    }

    @PublishedFor__1_0_0
    public void setLogin(String str) {
        this.u = str;
    }

    @PublishedFor__1_0_0
    public void setPassword(String str) {
        this.v = str;
    }
}
