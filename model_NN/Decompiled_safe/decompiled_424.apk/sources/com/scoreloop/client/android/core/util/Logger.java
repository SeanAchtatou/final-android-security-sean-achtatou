package com.scoreloop.client.android.core.util;

import java.io.File;
import org.json.JSONArray;

public abstract class Logger {
    private static final String a = (File.separator + "sdcard" + File.separator + "scoreloop" + File.separator);

    public static void a(String str) {
        a("Scoreloop", str);
    }

    public static void a(String str, String str2) {
    }

    public static void a(String str, String str2, Exception exc) {
    }

    public static void a(String str, String str2, Object obj) {
    }

    public static void a(JSONArray jSONArray) {
    }

    public static void b(String str) {
        b("Scoreloop", str);
    }

    public static void b(String str, String str2) {
    }

    public static void c(String str) {
        c("Scoreloop", str);
    }

    public static void c(String str, String str2) {
    }
}
