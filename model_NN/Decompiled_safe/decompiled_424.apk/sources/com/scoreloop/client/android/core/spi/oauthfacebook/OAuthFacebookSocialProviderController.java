package com.scoreloop.client.android.core.spi.oauthfacebook;

import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.facebook.android.a;
import com.facebook.android.b;
import com.facebook.android.c;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.Session;

public final class OAuthFacebookSocialProviderController extends SocialProviderController {
    private static final String[] b = {"publish_stream", "read_stream", "offline_access"};

    public OAuthFacebookSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        super(session, socialProviderControllerObserver);
    }

    /* access modifiers changed from: private */
    public OAuthFacebookSocialProvider f() {
        return (OAuthFacebookSocialProvider) getSocialProvider();
    }

    /* access modifiers changed from: protected */
    public void a() {
        CookieSyncManager.createInstance(c_());
        CookieManager.getInstance().removeAllCookie();
        final b bVar = new b("75418507755");
        bVar.a(c_(), b, -1, new b.a() {
            public void a() {
                OAuthFacebookSocialProviderController.this.d_().socialProviderControllerDidCancel(OAuthFacebookSocialProviderController.this);
            }

            public void a(Bundle bundle) {
                String b2 = bVar.b();
                if (b2 != null) {
                    OAuthFacebookSocialProviderController.this.f().a(OAuthFacebookSocialProviderController.this.e(), b2, null, null);
                    OAuthFacebookSocialProviderController.this.a(SocialProviderController.UpdateMode.SUBMIT);
                    return;
                }
                OAuthFacebookSocialProviderController.this.d_().socialProviderControllerDidFail(OAuthFacebookSocialProviderController.this, new IllegalStateException("completed w/o token"));
            }

            public void a(a aVar) {
                OAuthFacebookSocialProviderController.this.d_().socialProviderControllerDidFail(OAuthFacebookSocialProviderController.this, aVar);
            }

            public void a(c cVar) {
                OAuthFacebookSocialProviderController.this.d_().socialProviderControllerDidFail(OAuthFacebookSocialProviderController.this, cVar);
            }
        });
    }
}
