package com.scoreloop.client.android.ui.component.base;

import com.scoreloop.client.android.ui.framework.ValueStore;

public interface Tracker {
    void trackEvent(String str, String str2, String str3, int i);

    void trackPageView(String str, ValueStore valueStore);
}
