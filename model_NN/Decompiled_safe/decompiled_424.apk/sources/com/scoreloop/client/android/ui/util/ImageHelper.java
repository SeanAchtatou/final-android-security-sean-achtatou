package com.scoreloop.client.android.ui.util;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import java.io.File;
import java.io.FileNotFoundException;

public class ImageHelper {
    private static Bitmap decodeFile(Uri imageUri, ContentResolver contentResolver, int targetSize) throws FileNotFoundException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(contentResolver.openInputStream(imageUri), null, options);
        int scale = 1;
        int sampleSize = targetSize * 2;
        if (options.outHeight > sampleSize || options.outWidth > sampleSize) {
            scale = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(((double) sampleSize) / ((double) Math.max(options.outHeight, options.outWidth))) / Math.log(0.5d))));
        }
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inSampleSize = scale;
        return BitmapFactory.decodeStream(contentResolver.openInputStream(imageUri), null, options2);
    }

    /* JADX INFO: Multiple debug info for r8v2 int: [D('orgHeight' int), D('contentResolver' android.content.ContentResolver)] */
    /* JADX INFO: Multiple debug info for r7v2 int: [D('cropSize' int), D('imageUri' android.net.Uri)] */
    /* JADX INFO: Multiple debug info for r7v4 float: [D('scale' float), D('cropSize' int)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap createThumbnail(Uri imageUri, ContentResolver contentResolver, int targetSize, String orientation) {
        System.gc();
        try {
            Bitmap bitmap = decodeFile(imageUri, contentResolver, targetSize);
            int orgWidth = bitmap.getWidth();
            int orgHeight = bitmap.getHeight();
            int cropSize = Math.min(orgWidth, orgHeight);
            int cropDx = (orgWidth - cropSize) / 2;
            int cropDy = (orgHeight - cropSize) / 2;
            float scale = ((float) targetSize) / ((float) cropSize);
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            if (orientation != null && (orientation.equals("90") || orientation.equals("180") || orientation.equals("270"))) {
                matrix.postRotate((float) Integer.valueOf(orientation).intValue());
            }
            Bitmap thumbnail = Bitmap.createBitmap(bitmap, cropDx, cropDy, orgWidth - (cropDx * 2), orgHeight - (cropDy * 2), matrix, true);
            bitmap.recycle();
            return thumbnail;
        } catch (FileNotFoundException e) {
            throw new RuntimeException("unhandled checked exception", e);
        }
    }

    public static String getExifOrientation(Uri imageUri, ContentResolver contentResolver, Context context) {
        int sdk_int;
        String orientation = "0";
        try {
            sdk_int = Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (Exception e) {
            sdk_int = 3;
        }
        if (sdk_int >= 5 && LocalImageStorage.isStorageWritable()) {
            File file = null;
            try {
                file = LocalImageStorage.putStream(context, imageUri.toString(), contentResolver.openInputStream(imageUri));
                Class<?> exifInterfaceClass = Class.forName("android.media.ExifInterface");
                Object exif = exifInterfaceClass.getConstructor(String.class).newInstance(file.getAbsolutePath());
                String exifOrientation = (String) exifInterfaceClass.getMethod("getAttribute", String.class).invoke(exif, "Orientation");
                if (exifOrientation != null) {
                    if (exifOrientation.equals("1")) {
                        orientation = "0";
                    } else if (exifOrientation.equals("3")) {
                        orientation = "180";
                    } else if (exifOrientation.equals("6")) {
                        orientation = "90";
                    } else if (exifOrientation.equals("8")) {
                        orientation = "270";
                    }
                }
                if (file != null && file.exists()) {
                    file.delete();
                }
            } catch (Exception e2) {
                if (file != null && file.exists()) {
                    file.delete();
                }
            } catch (Throwable th) {
                if (file != null && file.exists()) {
                    file.delete();
                }
                throw th;
            }
        }
        return orientation;
    }
}
