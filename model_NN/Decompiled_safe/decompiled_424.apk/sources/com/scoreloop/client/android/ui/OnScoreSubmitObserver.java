package com.scoreloop.client.android.ui;

public interface OnScoreSubmitObserver {
    public static final int STATUS_ERROR_BALANCE = 5;
    public static final int STATUS_ERROR_NETWORK = 4;
    public static final int STATUS_SUCCESS_CHALLENGE = 3;
    public static final int STATUS_SUCCESS_LOCAL_SCORE = 2;
    public static final int STATUS_SUCCESS_SCORE = 1;
    public static final int STATUS_UNDEFINED = 0;

    void onScoreSubmit(int i, Exception exc);
}
