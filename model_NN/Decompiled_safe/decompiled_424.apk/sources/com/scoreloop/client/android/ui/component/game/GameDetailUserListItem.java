package com.scoreloop.client.android.ui.component.game;

import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import il.co.anykey.games.yaniv.lite.R;

public class GameDetailUserListItem extends StandardListItem<User> {
    public GameDetailUserListItem(ComponentActivity context, User user) {
        super(context, context.getResources().getDrawable(R.drawable.sl_icon_user), user.getDisplayName(), null, user);
    }

    /* access modifiers changed from: protected */
    public String getImageUrl() {
        return ((User) getTarget()).getImageUrl();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_user;
    }

    public int getType() {
        return 14;
    }
}
