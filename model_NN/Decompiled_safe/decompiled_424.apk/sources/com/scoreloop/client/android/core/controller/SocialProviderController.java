package com.scoreloop.client.android.core.controller;

import android.app.Activity;
import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import java.lang.reflect.InvocationTargetException;

public abstract class SocialProviderController {
    static final /* synthetic */ boolean a = (!SocialProviderController.class.desiredAssertionStatus());
    private Activity b;
    private final SocialProviderControllerObserver c;
    private SocialProvider d;
    private f e;
    private RequestControllerObserver f;
    private final Session g;
    private UserController h;

    protected enum UpdateMode {
        SUBMIT,
        LOAD
    }

    class a implements RequestControllerObserver {
        a() {
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            SocialProviderController.this.getSocialProvider().a(SocialProviderController.this.e());
            SocialProviderController.this.d_().socialProviderControllerDidFail(SocialProviderController.this, exc);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            if (requestController.getClass().isAssignableFrom(f.class)) {
                SocialProviderController.this.f();
            }
        }
    }

    protected SocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        this.g = session;
        this.c = socialProviderControllerObserver;
    }

    private void a(SocialProvider socialProvider) {
        this.d = socialProvider;
    }

    /* access modifiers changed from: private */
    public void f() {
        if (getSocialProvider().isUserConnected(e())) {
            d_().socialProviderControllerDidSucceed(this);
        } else {
            a();
        }
    }

    @PublishedFor__1_0_0
    public static SocialProviderController getSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver, SocialProvider socialProvider) {
        if (socialProviderControllerObserver == null || socialProvider == null) {
            throw new IllegalArgumentException("observer and provider argumetns must not be null");
        }
        Session currentSession = session == null ? Session.getCurrentSession() : session;
        if (a || currentSession != null) {
            Class<?> b2 = socialProvider.b();
            if (b2 == null) {
                throw new IllegalArgumentException();
            }
            try {
                try {
                    SocialProviderController socialProviderController = (SocialProviderController) b2.getConstructor(Session.class, SocialProviderControllerObserver.class).newInstance(currentSession, socialProviderControllerObserver);
                    socialProviderController.a(socialProvider);
                    return socialProviderController;
                } catch (IllegalArgumentException e2) {
                    throw new IllegalStateException(e2);
                } catch (InstantiationException e3) {
                    throw new IllegalStateException(e3);
                } catch (IllegalAccessException e4) {
                    throw new IllegalStateException(e4);
                } catch (InvocationTargetException e5) {
                    throw new IllegalStateException(e5);
                }
            } catch (SecurityException e6) {
                throw new IllegalStateException(e6);
            } catch (NoSuchMethodException e7) {
                throw new IllegalStateException(e7);
            }
        } else {
            throw new AssertionError();
        }
    }

    @PublishedFor__1_0_0
    public static SocialProviderController getSocialProviderController(String str, SocialProviderControllerObserver socialProviderControllerObserver) {
        return getSocialProviderController(null, socialProviderControllerObserver, SocialProvider.getSocialProviderForIdentifier(str));
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public void a(UpdateMode updateMode) {
        if (this.h == null) {
            this.h = new UserController(new RequestControllerObserver() {
                public void requestControllerDidFail(RequestController requestController, Exception exc) {
                    SocialProviderController.this.a(new IllegalStateException("submit user failed"));
                }

                public void requestControllerDidReceiveResponse(RequestController requestController) {
                    SocialProviderController.this.a((Exception) null);
                }
            });
        }
        this.h.setUser(e());
        switch (updateMode) {
            case SUBMIT:
                this.h.submitUser();
                return;
            case LOAD:
                this.h.loadUser();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Exception exc) {
        if (exc == null) {
            d_().socialProviderControllerDidSucceed(this);
        } else {
            d_().socialProviderControllerDidFail(this, exc);
        }
    }

    /* access modifiers changed from: protected */
    public Activity c_() {
        return this.b;
    }

    @PublishedFor__1_0_0
    public final void connect(Activity activity) {
        this.b = activity;
        this.f = new a();
        if (d() == null || !d().isAuthenticated()) {
            if (this.e == null) {
                this.e = new f(d(), this.f);
            }
            this.e.b();
            return;
        }
        f();
    }

    /* access modifiers changed from: protected */
    public Session d() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public SocialProviderControllerObserver d_() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public User e() {
        return this.g.getUser();
    }

    @PublishedFor__1_0_0
    public SocialProvider getSocialProvider() {
        return this.d;
    }
}
