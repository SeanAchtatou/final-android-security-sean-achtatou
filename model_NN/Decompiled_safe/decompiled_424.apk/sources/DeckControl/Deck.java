package DeckControl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import ui.YanivGameActivity;

public final class Deck implements Serializable {
    private static Deck deck = null;
    public static final int fromDeck = 1;
    public static final int fromDiscard = 2;
    public static final int fromUndecided = 0;
    private static final long serialVersionUID = -3085427575625532536L;
    public static final String[] whereFromArray = {"", "deck", "discard pile"};
    private ArrayList<Card> allDiscards;
    private ArrayList<Card> availableDiscards;
    private int cardsInDeck;
    private int currentCard;
    private ArrayList<Card> currentDiscards;
    private int deckValue;
    public long seed;
    private Card[] theDeck;

    public Deck clone() {
        deck.theDeck = (Card[]) this.theDeck.clone();
        deck.allDiscards = (ArrayList) this.allDiscards.clone();
        deck.currentDiscards = (ArrayList) this.currentDiscards.clone();
        deck.availableDiscards = (ArrayList) this.availableDiscards.clone();
        deck.currentCard = this.currentCard;
        deck.cardsInDeck = this.cardsInDeck;
        deck.cardsInDeck = this.cardsInDeck;
        return deck;
    }

    private Deck() {
    }

    public static Deck getDeck() {
        if (deck == null) {
            deck = new Deck();
        }
        return deck;
    }

    private Object readResolve() {
        clone();
        return getDeck();
    }

    public void initialiseDeck() {
        try {
            int numberOfDecks = YanivGameActivity.settings.getNumberOfDecks();
            this.deckValue = 0;
            this.theDeck = new Card[((numberOfDecks * 54) + 1)];
            for (int d = 0; d < numberOfDecks; d++) {
                for (int s = 0; s < 4; s++) {
                    for (int r = 1; r < 14; r++) {
                        this.theDeck[(((d * 54) + (s * 13)) + r) - 1] = new Card(Card.suits[s], Card.ranks[r]);
                        this.deckValue += this.theDeck[(((d * 54) + (s * 13)) + r) - 1].getCardValue();
                    }
                }
                this.theDeck[(d * 54) + 52] = new Card(0, 0);
                this.theDeck[(d * 54) + 53] = new Card(0, 0);
            }
            this.theDeck[numberOfDecks * 54] = null;
            this.cardsInDeck = numberOfDecks * 54;
            shuffle();
        } catch (Exception e) {
        }
    }

    public void shuffle() {
        this.seed = new Random().nextLong();
        Random randomiser = new Random(this.seed);
        this.currentCard = -1;
        for (int swap1 = 0; swap1 < this.cardsInDeck; swap1++) {
            int swap2 = randomiser.nextInt(this.cardsInDeck);
            Card temp = this.theDeck[swap1];
            this.theDeck[swap1] = this.theDeck[swap2];
            this.theDeck[swap2] = temp;
        }
        this.allDiscards = new ArrayList<>();
    }

    public void reshuffle() {
        try {
            this.allDiscards.toArray(this.theDeck);
            this.cardsInDeck = this.allDiscards.size();
            this.deckValue = 0;
            for (Card next : this.theDeck) {
                if (next == null) {
                    break;
                }
                this.deckValue += next.getCardValue();
                next.wasPreviouslyDiscarded = false;
            }
            shuffle();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Card nextCard() {
        try {
            this.currentCard++;
            if (this.currentCard < this.cardsInDeck) {
                return this.theDeck[this.currentCard];
            }
            YanivGameActivity.thisActivity.announceReshuffle();
            reshuffle();
            this.currentCard++;
            return this.theDeck[this.currentCard];
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void pushCard(Card card) {
        this.currentCard--;
        this.theDeck[this.currentCard] = card;
    }

    public Card peek() {
        Card peekCard = nextCard();
        pushCard(peekCard);
        return peekCard;
    }

    public void setDiscard(Card discard) {
        this.currentDiscards = new ArrayList<>();
        this.currentDiscards.add(discard);
        this.availableDiscards = new ArrayList<>();
        this.availableDiscards.add(discard);
        this.deckValue -= discard.getCardValue();
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public void discard(ArrayList<Card> inDiscards) {
        this.allDiscards.addAll(this.currentDiscards);
        Iterator<Card> it = inDiscards.iterator();
        while (it.hasNext()) {
            Card next = it.next();
            if (!next.wasPreviouslyDiscarded) {
                this.deckValue -= next.getCardValue();
                next.wasPreviouslyDiscarded = true;
            }
        }
        this.currentDiscards = new ArrayList<>();
        this.currentDiscards.addAll(inDiscards);
        this.availableDiscards = new ArrayList<>();
        this.availableDiscards.add(inDiscards.get(0));
        if (inDiscards.size() > 1) {
            this.availableDiscards.add(inDiscards.get(inDiscards.size() - 1));
        }
    }

    public Card getAvailableDiscard(int whichOne) {
        return this.availableDiscards.get(whichOne);
    }

    public ArrayList<Card> getCurrentDiscards() {
        return this.currentDiscards;
    }

    public String getCurrentDiscardsAsString() {
        String currentDiscardsString = "";
        Iterator<Card> it = this.currentDiscards.iterator();
        while (it.hasNext()) {
            currentDiscardsString = String.valueOf(currentDiscardsString) + it.next().toString();
        }
        return currentDiscardsString;
    }

    public int currentDiscardSize() {
        return this.currentDiscards.size();
    }

    public int availableDiscardSize() {
        return this.availableDiscards.size();
    }

    public Card firstAvailableDiscard() {
        return this.availableDiscards.get(0);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Card secondAvailableDiscard() {
        if (this.availableDiscards.size() == 2) {
            return this.availableDiscards.get(1);
        }
        return null;
    }

    public int averageRemainingCardsValues() {
        return this.deckValue / ((this.cardsInDeck - this.currentCard) - 1);
    }

    public String getCrashData() {
        String errorMessage = String.valueOf("") + "Seed: " + String.valueOf(this.seed) + "<BR> ";
        if (this.currentDiscards == null) {
            return errorMessage;
        }
        String errorMessage2 = String.valueOf(errorMessage) + "Current Discards: ";
        Iterator<Card> it = this.currentDiscards.iterator();
        while (it.hasNext()) {
            errorMessage2 = String.valueOf(errorMessage2) + it.next().toString() + " ";
        }
        return String.valueOf(errorMessage2) + "<BR> ";
    }
}
