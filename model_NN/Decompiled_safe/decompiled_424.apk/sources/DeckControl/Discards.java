package DeckControl;

import HandControl.PlayerHand;
import HandControl.TurnDetails;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Discards implements Serializable {
    private static final int BETTER_THAN = 2;
    private static final int SAME_AS = 1;
    private static final int WORSE_THAN = 0;
    private static final long serialVersionUID = 1;
    private ArrayList<Card> cardsToDiscard = new ArrayList<>();
    private int finalCardCount = 0;
    private int finalHandValue = 0;
    private int intermediateHandValue = 0;
    private int jokerCount = 0;
    private int totalCardValue = 0;

    public boolean isBetterThan(Discards compare, DiscardsCompareRules discardsCompareRules) {
        int compareResult = 0;
        if (compare == null) {
            compareResult = 2;
        } else {
            if (this.finalHandValue == 0 || compare.finalHandValue == 0) {
                if (this.totalCardValue > compare.totalCardValue) {
                    compareResult = 2;
                }
            } else if (this.finalHandValue < compare.finalHandValue) {
                compareResult = 2;
            } else if (this.finalHandValue == compare.finalHandValue) {
                compareResult = 1;
                if (discardsCompareRules.checkIntermediateValues) {
                    compareResult = this.intermediateHandValue < compare.intermediateHandValue ? 2 : this.intermediateHandValue == compare.intermediateHandValue ? 1 : 0;
                }
            }
            if (compareResult == 1 && discardsCompareRules.checkJokers && this.jokerCount < compare.jokerCount) {
                compareResult = 2;
            }
            if (compareResult == 1 && discardsCompareRules.checkFinalCardCount && this.finalCardCount < compare.finalCardCount) {
                compareResult = 2;
            }
        }
        if (compareResult == 2) {
            return true;
        }
        return false;
    }

    public void add(Card card) {
        if (card.isJoker) {
            this.jokerCount++;
        }
        if (card.getCardRank() != 14 || card.isJoker) {
            this.cardsToDiscard.add(card);
            this.totalCardValue += card.getCardValue();
            return;
        }
        this.cardsToDiscard.add(card.equivalentAceLow);
        this.totalCardValue += card.equivalentAceLow.getCardValue();
    }

    public void addAll(Discards discards) {
        this.cardsToDiscard.addAll(discards.getCardsToDiscard());
        this.totalCardValue += discards.getTotalCardValue();
    }

    public void addAll(PlayerHand hand) {
        this.cardsToDiscard.addAll(hand.getCards());
        this.totalCardValue += hand.handValue;
    }

    public void addAll(TurnDetails discardList) {
        Iterator<Card> it = discardList.cardsToDiscard.iterator();
        while (it.hasNext()) {
            add(it.next());
        }
    }

    public int getSize() {
        return this.cardsToDiscard.size();
    }

    public int getJokerCount() {
        return this.jokerCount;
    }

    public int getNonJokerCount() {
        return this.cardsToDiscard.size() - this.jokerCount;
    }

    public int getTotalCardValue() {
        return this.totalCardValue;
    }

    public void clear() {
        this.cardsToDiscard.clear();
        this.totalCardValue = 0;
        this.intermediateHandValue = 0;
        this.finalHandValue = 0;
    }

    public ArrayList<Card> getCardsToDiscard() {
        return this.cardsToDiscard;
    }

    public void setIntermediateHandValue(int intermediateHandValue2) {
        this.intermediateHandValue = intermediateHandValue2;
    }

    public void setFinalHandValue(int finalHandValue2) {
        this.finalHandValue = finalHandValue2;
    }

    public void setFinalCardCount(int finalCardCount2) {
        this.finalCardCount = finalCardCount2;
    }
}
