package DeckControl;

import java.io.Serializable;

public class DiscardsCompareRules implements Serializable {
    private static final long serialVersionUID = 8509121012963659418L;
    public boolean checkFinalCardCount;
    public boolean checkIntermediateValues;
    public boolean checkJokers;

    public DiscardsCompareRules(boolean checkJokers2, boolean checkIntermediateValues2, boolean checkFinalCardCount2) {
        this.checkJokers = checkJokers2;
        this.checkIntermediateValues = checkIntermediateValues2;
        this.checkFinalCardCount = checkFinalCardCount2;
    }
}
