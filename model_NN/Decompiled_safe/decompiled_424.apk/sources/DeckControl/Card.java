package DeckControl;

import il.co.anykey.games.yaniv.lite.R;
import java.io.Serializable;
import ui.YanivGameActivity;

public class Card implements Serializable {
    public static final int ace = 1;
    public static final int aceHigh = 14;
    private static final int[] cardImagesL = {R.drawable.cal, R.drawable.c2l, R.drawable.c3l, R.drawable.c4l, R.drawable.c5l, R.drawable.c6l, R.drawable.c7l, R.drawable.c8l, R.drawable.c9l, R.drawable.ctl, R.drawable.cjl, R.drawable.cql, R.drawable.ckl, R.drawable.cal, R.drawable.dal, R.drawable.d2l, R.drawable.d3l, R.drawable.d4l, R.drawable.d5l, R.drawable.d6l, R.drawable.d7l, R.drawable.d8l, R.drawable.d9l, R.drawable.dtl, R.drawable.djl, R.drawable.dql, R.drawable.dkl, R.drawable.dal, R.drawable.hal, R.drawable.h2l, R.drawable.h3l, R.drawable.h4l, R.drawable.h5l, R.drawable.h6l, R.drawable.h7l, R.drawable.h8l, R.drawable.h9l, R.drawable.htl, R.drawable.hjl, R.drawable.hql, R.drawable.hkl, R.drawable.hal, R.drawable.sal, R.drawable.s2l, R.drawable.s3l, R.drawable.s4l, R.drawable.s5l, R.drawable.s6l, R.drawable.s7l, R.drawable.s8l, R.drawable.s9l, R.drawable.stl, R.drawable.sjl, R.drawable.sql, R.drawable.skl, R.drawable.hal, R.drawable.rjl, R.drawable.rjl};
    private static final int[] cardImagesL_4Colour = {R.drawable.cal_4colour, R.drawable.c2l_4colour, R.drawable.c3l_4colour, R.drawable.c4l_4colour, R.drawable.c5l_4colour, R.drawable.c6l_4colour, R.drawable.c7l_4colour, R.drawable.c8l_4colour, R.drawable.c9l_4colour, R.drawable.ctl_4colour, R.drawable.cjl_4colour, R.drawable.cql_4colour, R.drawable.ckl_4colour, R.drawable.cal_4colour, R.drawable.dal_4colour, R.drawable.d2l_4colour, R.drawable.d3l_4colour, R.drawable.d4l_4colour, R.drawable.d5l_4colour, R.drawable.d6l_4colour, R.drawable.d7l_4colour, R.drawable.d8l_4colour, R.drawable.d9l_4colour, R.drawable.dtl_4colour, R.drawable.djl_4colour, R.drawable.dql_4colour, R.drawable.dkl_4colour, R.drawable.dal_4colour, R.drawable.hal, R.drawable.h2l, R.drawable.h3l, R.drawable.h4l, R.drawable.h5l, R.drawable.h6l, R.drawable.h7l, R.drawable.h8l, R.drawable.h9l, R.drawable.htl, R.drawable.hjl, R.drawable.hql, R.drawable.hkl, R.drawable.hal, R.drawable.sal, R.drawable.s2l, R.drawable.s3l, R.drawable.s4l, R.drawable.s5l, R.drawable.s6l, R.drawable.s7l, R.drawable.s8l, R.drawable.s9l, R.drawable.stl, R.drawable.sjl, R.drawable.sql, R.drawable.skl, R.drawable.hal, R.drawable.rjl, R.drawable.rjl};
    public static final int club = 0;
    public static final int diamond = 1;
    public static final int eight = 8;
    public static final int five = 5;
    public static final int four = 4;
    public static final int heart = 2;
    public static final int jack = 11;
    public static final int joker = 0;
    public static final int king = 13;
    public static final int nine = 9;
    public static final int queen = 12;
    public static final int[] ranks;
    private static final long serialVersionUID = 1;
    public static final int seven = 7;
    public static final int six = 6;
    public static final int spade = 3;
    public static final int[] suits;
    public static final int ten = 10;
    public static final int three = 3;
    public static final int two = 2;
    public static final int[] values;
    public Card equivalentAceHigh;
    public Card equivalentAceLow;
    public boolean isJoker;
    private int rank;
    private int suit;
    private int value;
    public boolean wasPreviouslyDiscarded;

    static {
        int[] iArr = new int[4];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        suits = iArr;
        int[] iArr2 = new int[15];
        iArr2[1] = 1;
        iArr2[2] = 2;
        iArr2[3] = 3;
        iArr2[4] = 4;
        iArr2[5] = 5;
        iArr2[6] = 6;
        iArr2[7] = 7;
        iArr2[8] = 8;
        iArr2[9] = 9;
        iArr2[10] = 10;
        iArr2[11] = 11;
        iArr2[12] = 12;
        iArr2[13] = 13;
        iArr2[14] = 14;
        ranks = iArr2;
        int[] iArr3 = new int[15];
        iArr3[1] = 1;
        iArr3[2] = 2;
        iArr3[3] = 3;
        iArr3[4] = 4;
        iArr3[5] = 5;
        iArr3[6] = 6;
        iArr3[7] = 7;
        iArr3[8] = 8;
        iArr3[9] = 9;
        iArr3[10] = 10;
        iArr3[11] = 10;
        iArr3[12] = 10;
        iArr3[13] = 10;
        iArr3[14] = 1;
        values = iArr3;
    }

    public Card(int inSuit, int inRank) {
        this.suit = 0;
        this.rank = 0;
        this.value = 0;
        this.wasPreviouslyDiscarded = false;
        this.suit = inSuit;
        this.rank = inRank;
        this.value = values[inRank];
        this.isJoker = inRank == 0;
        if (inRank == 1) {
            this.equivalentAceHigh = new Card(inSuit, 14);
            this.equivalentAceHigh.equivalentAceLow = this;
            this.value = 1;
        }
    }

    public Card(String cardString) {
        this("CDHS".indexOf(cardString.substring(0, 1)), "XA23456789TJQK".indexOf(cardString.substring(1, 2)));
    }

    public boolean sameSuitAs(Card inCard) {
        return this.suit == inCard.suit;
    }

    public boolean sameRankAs(Card inCard) {
        return this.rank == inCard.rank;
    }

    public int getCardImage() {
        if (this.isJoker) {
            return cardImagesL_4Colour[56];
        }
        if (YanivGameActivity.settings.getCards4colours()) {
            return cardImagesL_4Colour[((this.suit * 14) + this.rank) - 1];
        }
        return cardImagesL[((this.suit * 14) + this.rank) - 1];
    }

    public int getCardImageL() {
        if (this.isJoker) {
            return cardImagesL_4Colour[56];
        }
        if (YanivGameActivity.settings.getCards4colours()) {
            return cardImagesL_4Colour[((this.suit * 14) + this.rank) - 1];
        }
        return cardImagesL[((this.suit * 14) + this.rank) - 1];
    }

    public int getCardRank() {
        return this.rank;
    }

    public int getCardSuit() {
        return this.suit;
    }

    public int getCardValue() {
        return this.value;
    }

    public String toString() {
        if (this.isJoker) {
            return "Joker";
        }
        return String.valueOf("A23456789TJQKA".substring(this.rank - 1, this.rank)) + "cdhs".substring(this.suit, this.suit + 1);
    }

    public String toShortString() {
        if (this.isJoker) {
            return "XX";
        }
        return String.valueOf("A23456789TJQKA".substring(this.rank - 1, this.rank)) + "cdhs".substring(this.suit, this.suit + 1);
    }

    public boolean immediatelyFollows(Card inCard) {
        return this.suit == inCard.getCardSuit() && this.rank == inCard.getCardRank() - 1;
    }

    public void setRankForJoker(int rank2) {
        this.rank = rank2;
    }

    public void setValuesForJoker(int suit2, int rank2) {
        this.suit = suit2;
        this.rank = rank2;
    }

    public void resetJoker() {
        this.suit = 0;
        this.rank = 0;
    }
}
