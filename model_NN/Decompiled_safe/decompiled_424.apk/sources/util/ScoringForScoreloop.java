package util;

import PlayerControl.Player;
import android.content.Context;
import ui.ApplicationControllerBase;
import ui.YanivGameActivity;
import yanivlib.pkg.CurrentPreferences;

public class ScoringForScoreloop {
    private static double GAMEENDSCOREMULTIPLIER = 0.0d;
    private static final int MULTIPLIER = 6;
    private static final int POINTSDIFFERENCEMULTIPLIER = 10;
    private static final int SCORE_FOR_ASSAF = 500;
    private static final int SCORE_FOR_ASSAFED = -500;
    private static final int[] SCORE_FOR_SAME_RANK;
    private static final int[] SCORE_FOR_STR_FLUSH;
    private static final int SCORE_FOR_WINNING = 1800;
    private static final int SCORE_FOR_YANIV = 300;
    private static CurrentPreferences currentPreferences;
    private static int currentScore = 0;

    static {
        int[] iArr = new int[8];
        iArr[1] = 10;
        iArr[2] = 23;
        iArr[3] = 33;
        iArr[4] = 56;
        iArr[5] = 89;
        iArr[6] = 145;
        iArr[7] = 234;
        SCORE_FOR_SAME_RANK = iArr;
        int[] iArr2 = new int[8];
        iArr2[3] = 73;
        iArr2[4] = 99;
        iArr2[5] = 172;
        iArr2[6] = 271;
        iArr2[7] = 343;
        SCORE_FOR_STR_FLUSH = iArr2;
    }

    public static void initialise(Context context) {
        currentPreferences = CurrentPreferences.getCurrentPreferences(context);
        GAMEENDSCOREMULTIPLIER = new Integer((int) CurrentPreferences.gameEndScoreBase).doubleValue() / new Integer(currentPreferences.getGameEndScore()).doubleValue();
    }

    public static void addSameRankScore(int numberOfCards) {
        currentScore = (int) (((double) currentScore) + (((double) (SCORE_FOR_SAME_RANK[numberOfCards] * 6)) * GAMEENDSCOREMULTIPLIER * Math.log((double) ApplicationControllerBase.currentPreferences.getYanivAmount())));
        YanivGameActivity.thisActivity.showCurrentScore((double) currentScore);
    }

    public static void addStrFlushScore(int numberOfCards) {
        currentScore = (int) (((double) currentScore) + (((double) (SCORE_FOR_STR_FLUSH[numberOfCards] * 6)) * Math.log((double) ApplicationControllerBase.currentPreferences.getYanivAmount())));
        YanivGameActivity.thisActivity.showCurrentScore((double) currentScore);
    }

    public static void addYanivScore() {
        currentScore = (int) (((double) currentScore) + (1800.0d * Math.log((double) ApplicationControllerBase.currentPreferences.getYanivAmount())));
        YanivGameActivity.thisActivity.showCurrentScore((double) currentScore);
    }

    public static void addAssafScore() {
        currentScore = (int) (((double) currentScore) + (3000.0d * Math.log((double) ApplicationControllerBase.currentPreferences.getYanivAmount())));
        YanivGameActivity.thisActivity.showCurrentScore((double) currentScore);
    }

    public static void addAssafedScore() {
        currentScore = (int) (((double) currentScore) + (-3000.0d * Math.log((double) ApplicationControllerBase.currentPreferences.getYanivAmount())));
        YanivGameActivity.thisActivity.showCurrentScore((double) currentScore);
    }

    public static void addWinningScore() {
        currentScore = (int) (((double) currentScore) + (10800.0d * Math.log((double) ApplicationControllerBase.currentPreferences.getYanivAmount())));
        YanivGameActivity.thisActivity.showCurrentScore((double) currentScore);
    }

    public static void addPointDifferenceScore(Player[] players) {
        int pointsDifference = 0;
        switch (CurrentPreferences.getCurrentPreferences(YanivGameActivity.thisActivity).getNumberOfPlayers()) {
            case 2:
                pointsDifference = players[0].getHandValue() - players[1].getHandValue();
                break;
            case 3:
                pointsDifference = Math.max(players[1].getHandValue(), players[2].getHandValue()) - players[0].getHandValue();
                break;
            case 4:
                pointsDifference = Math.max(Math.max(players[1].getHandValue(), players[2].getHandValue()), players[3].getHandValue()) - players[0].getHandValue();
                break;
        }
        if (pointsDifference > 0) {
            currentScore = (int) (((double) currentScore) + (((double) (pointsDifference * 10 * 6)) * Math.log((double) ApplicationControllerBase.currentPreferences.getYanivAmount())));
        }
        YanivGameActivity.thisActivity.showCurrentScore((double) currentScore);
    }

    public static void resetScore() {
        currentScore = 0;
    }

    public static int getScore() {
        return currentScore;
    }

    public static void setScore(int newScore) {
        currentScore = newScore;
        YanivGameActivity.thisActivity.showCurrentScore((double) currentScore);
    }
}
