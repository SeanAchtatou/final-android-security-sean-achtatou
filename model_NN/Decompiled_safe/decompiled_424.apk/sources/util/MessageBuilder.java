package util;

import android.content.Context;
import android.widget.TextView;

public class MessageBuilder {
    public static String buildMessage(Context context, int stringId, String... parm) {
        String message = context.getString(stringId);
        for (String replaceFirst : parm) {
            message = message.replaceFirst("@", replaceFirst);
        }
        return message;
    }

    public static void buildMessage(Context context, TextView targetView, int stringId, String... parm) {
        targetView.setText(buildMessage(context, stringId, parm));
    }

    public static void buildMessage(Context context, TextView targetView, String message, String... parm) {
        for (String replace : parm) {
            message = message.replace("@", replace);
        }
        targetView.setText(message);
    }
}
