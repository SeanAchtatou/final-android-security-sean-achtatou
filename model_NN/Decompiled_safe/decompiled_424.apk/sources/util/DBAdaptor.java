package util;

import PlayerControl.Player;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import yanivlib.pkg.CurrentPreferences;

public class DBAdaptor {
    private static final String DATABASE_CREATE = "create table scores(_id integer primary key autoincrement, player0Score integer not null, player1Score integer not null, player2Score integer, player3Score integer );";
    private static final String DATABASE_DROP = "DROP TABLE if exists scores;";
    private static final String DATABASE_NAME = "yaniv";
    private static final String DATABASE_TABLE = "scores";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = "DBAdapter";
    private final Context context;
    private SQLiteDatabase db;
    private DatabaseHelper dbHelper;

    public DBAdaptor(Context ctx) {
        this.context = ctx.getApplicationContext();
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DBAdaptor.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DBAdaptor.DATABASE_CREATE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(DBAdaptor.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
            db.execSQL(DBAdaptor.DATABASE_DROP);
            onCreate(db);
        }
    }

    public DBAdaptor open() throws SQLException {
        this.dbHelper = new DatabaseHelper(this.context);
        this.db = this.dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.dbHelper.close();
    }

    public void clearScores() {
        this.db.execSQL(DATABASE_DROP);
        this.db.execSQL(DATABASE_CREATE);
    }

    public long insertScores(int[] playerScore) {
        try {
            ContentValues initialValues = new ContentValues();
            initialValues.put("player0Score", Integer.valueOf(playerScore[0]));
            initialValues.put("player1Score", Integer.valueOf(playerScore[1]));
            if (playerScore[2] != -1) {
                initialValues.put("player2Score", Integer.valueOf(playerScore[2]));
            } else {
                initialValues.putNull("player2Score");
            }
            if (playerScore[3] != -1) {
                initialValues.put("player3Score", Integer.valueOf(playerScore[3]));
            } else {
                initialValues.putNull("player3Score");
            }
            return this.db.insertOrThrow(DATABASE_TABLE, null, initialValues);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public Cursor getScores() throws Exception {
        try {
            return this.db.query(DATABASE_TABLE, new String[]{"_id", "player0Score", "player1Score", "player2Score", "player3Score"}, null, null, null, null, null, null);
        } catch (Exception e) {
            throw e;
        }
    }

    public Cursor getTotalScores() {
        return this.db.rawQuery("Select 0 as _id, player0Total, player1Total, player2Total, player3Total, min(player0Total, player1Total, player2Total, player3Total) as lowestScore,max(player0Total, player1Total, player2Total, player3Total) as highestScore from (SELECT 0 as _id, SUM(player0Score) as player0Total, SUM(player1Score) as player1Total, SUM(player2Score) as player2Total, SUM(player3Score) as player3Total FROM scores) as temp", null);
    }

    public void addScoresYaniv(Player yaniver, ArrayList<Player> losers) {
        int[] playerScores = {-1, -1, -1, -1};
        if (CurrentPreferences.getCurrentPreferences(this.context).getWinnerPlayerScore() == 0) {
            playerScores[yaniver.playerId] = 0;
        } else {
            playerScores[yaniver.playerId] = yaniver.getHandValue();
        }
        for (int i = 0; i < losers.size(); i++) {
            playerScores[losers.get(i).playerId] = losers.get(i).getHandValue();
        }
        insertScores(playerScores);
    }

    public void addScoresAssaf(ArrayList<Player> assafers, Player yaniver, ArrayList<Player> losers) {
        int[] playerScores = {-1, -1, -1, -1};
        CurrentPreferences settings = CurrentPreferences.getCurrentPreferences(this.context);
        for (int i = 0; i < assafers.size(); i++) {
            if (settings.getWinnerPlayerScore() == 0) {
                playerScores[assafers.get(i).playerId] = 0;
            } else {
                playerScores[assafers.get(i).playerId] = assafers.get(i).getHandValue();
            }
        }
        playerScores[yaniver.playerId] = yaniver.getHandValue() + settings.getAssafFine();
        for (int i2 = 0; i2 < losers.size(); i2++) {
            playerScores[losers.get(i2).playerId] = losers.get(i2).getHandValue();
        }
        insertScores(playerScores);
    }
}
