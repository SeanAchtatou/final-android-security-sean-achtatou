package util;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import java.lang.Thread;
import ui.ApplicationControllerBase;
import ui.PreferencesActivity;

public class YanivUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    public void uncaughtException(Thread thread, Throwable ex) {
        try {
            StackTraceElement[] ste = ex.getStackTrace();
            Intent emailIntent = new Intent("android.intent.action.SEND");
            String error = "<BR>Problem details:<BR> OS Version  : " + System.getProperty("os.version") + "(" + Build.VERSION.INCREMENTAL + ")" + "<BR> OS API Level: " + Build.VERSION.SDK + "<BR> Device      : " + Build.DEVICE + "<BR> Model and Product: " + Build.MODEL + " (" + Build.PRODUCT + ")";
            for (int i = 0; i < ste.length; i++) {
                error = String.valueOf(error) + "<BR>" + ste[i].toString();
            }
            emailIntent.setType("plain/text").putExtra("android.intent.extra.EMAIL", new String[]{"jb@mail.anykey.co.il"}).putExtra("android.intent.extra.SUBJECT", "Exception in Yaniv!").putExtra("android.intent.extra.TEXT", "test");
            new Thread() {
                public void run() {
                    Context context = ApplicationControllerBase.getMyApplicationContext();
                    context.startActivity(new Intent(context, PreferencesActivity.class).addFlags(268435456));
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
