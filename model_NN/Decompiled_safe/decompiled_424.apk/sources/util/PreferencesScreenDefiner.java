package util;

import android.preference.PreferenceActivity;
import il.co.anykey.games.yaniv.lite.R;

public class PreferencesScreenDefiner {
    public static void loadPreferencesScreen(PreferenceActivity pref, boolean inGame, boolean fromMainMenu) {
        try {
            pref.addPreferencesFromResource(R.xml.preferences_top);
            if (fromMainMenu || inGame) {
                pref.addPreferencesFromResource(R.xml.preferences_middle);
            }
            pref.addPreferencesFromResource(R.xml.preferences_bottom);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
