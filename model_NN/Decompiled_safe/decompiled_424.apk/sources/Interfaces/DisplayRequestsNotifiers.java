package Interfaces;

import DeckControl.Card;
import PlayerControl.Player;
import java.io.Serializable;
import java.util.ArrayList;

public interface DisplayRequestsNotifiers extends Serializable {
    void announceReshuffle();

    void clearLastMoveMessage();

    void declareAssaf(Player player, ArrayList<Player> arrayList);

    void declareYaniv(Player player);

    void displayPlayerAction(String str);

    void redrawCards(Player player, int i, Card card);

    void showHideCont(int i);

    void showHideRightTap(int i);

    void showHideYaniv(int i);
}
