package Interfaces;

import android.app.Activity;

public interface MultiPlayerGameInterface {
    boolean allowedMultiPlayerGame();

    boolean startNewMultiplayerGame(Activity activity);
}
