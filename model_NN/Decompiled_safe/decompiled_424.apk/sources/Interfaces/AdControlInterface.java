package Interfaces;

import com.google.ads.AdView;

public interface AdControlInterface {
    void showHideAd(AdView adView);
}
