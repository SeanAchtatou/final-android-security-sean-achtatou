package Interfaces;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public interface PreferencesScreenDefinerInterface {
    void loadPreferencesScreen(PreferenceActivity preferenceActivity, boolean z, Bundle bundle);
}
