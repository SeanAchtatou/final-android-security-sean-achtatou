package ui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import il.co.anykey.games.yaniv.lite.R;

public class YanivScoresActivity extends Yaniv {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.scores);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        setRequestedOrientation(1);
        super.onConfigurationChanged(newConfig);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
