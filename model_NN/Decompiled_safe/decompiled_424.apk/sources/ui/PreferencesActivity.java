package ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import il.co.anykey.games.yaniv.lite.R;
import java.util.Map;
import yanivlib.pkg.CurrentPreferences;

public class PreferencesActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        if (extras.getBoolean("MultiUser")) {
            PreferenceManager prefMgr = getPreferenceManager();
            prefMgr.setSharedPreferencesName("multi_user_game_preferences");
            prefMgr.setSharedPreferencesMode(0);
        }
        CurrentPreferences.getCurrentPreferences(getApplicationContext()).initialise();
        ApplicationControllerBase.preferencesScreenDefiner.loadPreferencesScreen(this, Yaniv.inGame, extras);
        for (Map.Entry<String, ?> entry : getPreferenceScreen().getSharedPreferences().getAll().entrySet()) {
            Preference pref = findPreference((CharSequence) entry.getKey());
            if (pref instanceof ListPreference) {
                changeSummary(pref);
            }
        }
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        CurrentPreferences.getCurrentPreferences(getApplicationContext()).refresh();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        if (pref.getTitle().equals(getString(R.string.preferences_NumberOfPlayersTitle)) || pref.getTitle().equals(getString(R.string.preferences_NumberOfDecksTitle)) || pref.getTitle().equals(getString(R.string.preferences_YanivAmountTitle)) || pref.getTitle().equals(getString(R.string.preferences_SkillLevelTitle)) || pref.getTitle().equals(getString(R.string.preferences_GameEndScoreTitle)) || pref.getTitle().equals(getString(R.string.preferences_HalfScoreFactorTitle))) {
            Intent intent = getIntent();
            intent.putExtra("cancelGame", true);
            setResult(-1, intent);
            Yaniv.inGame = false;
        }
        if (pref instanceof ListPreference) {
            changeSummary(pref);
        }
    }

    private void changeSummary(Preference pref) {
        try {
            ListPreference listPref = (ListPreference) pref;
            String currentSummary = (String) pref.getSummary();
            int index1 = currentSummary.indexOf(getString(R.string.preferences_CurrentValueDisplay).substring(0, getString(R.string.preferences_CurrentValueDisplay).indexOf("@")));
            if (index1 != -1) {
                currentSummary = currentSummary.substring(0, index1);
            }
            pref.setSummary(String.valueOf(currentSummary) + " " + getString(R.string.preferences_CurrentValueDisplay).replace("@", (String) listPref.getEntry()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
