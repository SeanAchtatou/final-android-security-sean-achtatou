package ui;

import Interfaces.AdControlInterface;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class AdControl implements AdControlInterface {
    public void showHideAd(AdView adView) {
        AdRequest request = new AdRequest();
        request.addTestDevice(AdRequest.TEST_EMULATOR);
        request.addTestDevice("5789DE4130E2DE42967EC3EFF17949D1");
        adView.loadAd(request);
    }
}
