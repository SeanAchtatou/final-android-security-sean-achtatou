package ui;

import DeckControl.Card;
import DeckControl.Deck;
import GameControl.GameControl;
import HandControl.HandControl;
import Interfaces.DisplayRequestsNotifiers;
import PlayerControl.AutomatedPlayer;
import PlayerControl.HumanPlayer;
import PlayerControl.Player;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.scoreloop.client.android.ui.OnScoreSubmitObserver;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.scoreloop.client.android.ui.ShowResultOverlayActivity;
import il.co.anykey.games.yaniv.lite.R;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import util.MessageBuilder;
import util.ScoringForScoreloop;
import yanivlib.pkg.CurrentPreferences;

public class YanivGameActivity extends Yaniv implements DisplayRequestsNotifiers, OnScoreSubmitObserver {
    public static final int ANIMATE_ALL = 3;
    public static final int ANIMATE_NONE = 0;
    public static final int ANIMATE_ONE_FROM_DECK = 1;
    public static final int ANIMATE_ONE_FROM_DISCARD = 2;
    private static transient Handler handler = null;
    private static final long serialVersionUID = 6597212088019282794L;
    public static CurrentPreferences settings;
    public static YanivGameActivity thisActivity = null;
    /* access modifiers changed from: private */
    public int cardBackResource = R.drawable.b1fv;
    /* access modifiers changed from: private */
    public boolean contListenerEnabled = true;
    /* access modifiers changed from: private */
    public Deck deck = Deck.getDeck();
    /* access modifiers changed from: private */
    public boolean inAnimation = false;
    public boolean inHand = false;
    private View.OnClickListener lsn_btnNext_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                if (YanivGameActivity.this.contListenerEnabled && !YanivGameActivity.this.inAnimation) {
                    YanivGameActivity.this.contListenerEnabled = false;
                    YanivGameActivity.getCurrentPlayer().doTurn();
                }
            } catch (Exception e) {
                YanivGameActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_btnYaniv_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                if (YanivGameActivity.this.yanivListenerEnabled && !YanivGameActivity.this.inAnimation) {
                    YanivGameActivity.this.yanivListenerEnabled = false;
                    YanivGameActivity.getCurrentPlayer().yaniv();
                }
            } catch (Exception e) {
                YanivGameActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_cardViews_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            int whichCard = 0;
            while (YanivGameActivity.this.cardViews[whichCard] != v) {
                try {
                    whichCard++;
                } catch (Exception e) {
                    YanivGameActivity.this.exceptionHandler(e);
                    return;
                }
            }
            if (YanivGameActivity.getCurrentPlayer().isHuman()) {
                ((HumanPlayer) YanivGameActivity.getCurrentPlayer()).selectDeselectCard(whichCard);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) YanivGameActivity.this.cardViews[whichCard].getLayoutParams();
                int temp = lp.bottomMargin;
                lp.bottomMargin = lp.topMargin;
                lp.topMargin = temp;
                YanivGameActivity.this.cardViews[whichCard].setLayoutParams(lp);
            } else if (YanivGameActivity.settings.getAllowMullaSlapdown() && YanivGameActivity.game.getPlayer(0).getNextPlayer() == YanivGameActivity.getCurrentPlayer()) {
                ((HumanPlayer) YanivGameActivity.game.getPlayer(0)).selectDeselectCard(whichCard);
                if (!YanivGameActivity.game.getPlayer(0).mullaSlapDown()) {
                    ((HumanPlayer) YanivGameActivity.game.getPlayer(0)).selectDeselectCard(whichCard);
                }
            }
        }
    };
    private View.OnClickListener lsn_vwDeck_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                if (YanivGameActivity.getCurrentPlayer().isHuman() && !YanivGameActivity.this.inAnimation) {
                    YanivGameActivity.getCurrentPlayer().discardAndPickupFromDeck();
                }
            } catch (Exception e) {
                YanivGameActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_vwDiscard_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                if (YanivGameActivity.getCurrentPlayer().isHuman() && !YanivGameActivity.this.inAnimation) {
                    YanivGameActivity.getCurrentPlayer().discardAndPickupFromDiscard(0);
                }
            } catch (Exception e) {
                YanivGameActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_vwRightTap_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                if (YanivGameActivity.getCurrentPlayer().isHuman() && !YanivGameActivity.this.inAnimation) {
                    YanivGameActivity.getCurrentPlayer().discardAndPickupFromDiscard(YanivGameActivity.this.deck.availableDiscardSize() - 1);
                }
            } catch (Exception e) {
                YanivGameActivity.this.exceptionHandler(e);
            }
        }
    };
    private ProgressDialog progressDialog;
    private transient Runnable startNextPlayerTurn;
    private transient Runnable unFlash;
    /* access modifiers changed from: private */
    public boolean yanivListenerEnabled = true;

    final class MyAnimation implements Animation.AnimationListener {
        private int animationDelay;
        private int animationDuration;
        private ImageView animationSource;
        private ImageView animationTarget;
        private int cardResource;

        MyAnimation(ImageView animationSource2, ImageView animationTarget2, int cardResource2, int animationDuration2, int animationDelay2) {
            this.animationSource = animationSource2;
            this.animationTarget = animationTarget2;
            this.cardResource = cardResource2;
            this.animationDuration = animationDuration2;
            this.animationDelay = animationDelay2;
        }

        public void animate() {
            int[] sourceCoords = new int[2];
            int[] targetCoords = new int[2];
            this.animationSource.getLocationOnScreen(sourceCoords);
            this.animationTarget.getLocationOnScreen(targetCoords);
            TranslateAnimation cardDealingAnimation = new TranslateAnimation(0.0f, (float) (targetCoords[0] - sourceCoords[0]), 0.0f, (float) (targetCoords[1] - sourceCoords[1]));
            cardDealingAnimation.setAnimationListener(this);
            cardDealingAnimation.setDuration((long) this.animationDuration);
            cardDealingAnimation.setStartOffset((long) this.animationDelay);
            this.animationSource.startAnimation(cardDealingAnimation);
        }

        public void onAnimationEnd(Animation animation) {
            this.animationTarget.setImageResource(this.cardResource);
            this.animationTarget.setVisibility(0);
            YanivGameActivity.this.inAnimation = false;
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
            YanivGameActivity.this.inAnimation = true;
            YanivGameActivity.this.redrawDiscards();
        }
    }

    private void animate(ImageView animationSource, ImageView animationTarget, int cardResource, int animationDuration, int animationDelay) {
        new MyAnimation(animationSource, animationTarget, cardResource, animationDuration, animationDelay).animate();
    }

    private void flashCard(final ImageView target) {
        this.unFlash = new Runnable() {
            public void run() {
                target.setImageResource(YanivGameActivity.this.cardBackResource);
            }
        };
        if (handler == null) {
            handler = new Handler();
        } else {
            handler.removeCallbacks(this.startNextPlayerTurn);
        }
        handler.postDelayed(this.unFlash, 1000);
    }

    public void redrawPlayerCards(Player player, int inAnimateDeal, boolean showCards) {
        int cardResource;
        int animateDeal = inAnimateDeal;
        if (!settings.getCardAnimation()) {
            animateDeal = 0;
        }
        Card[] hand = player.playerHand.getCardsAsArray();
        int handSize = hand.length;
        boolean showCards2 = showCards || player.playerId == 0;
        int cardPoint = player.playerId * 5;
        for (int j = handSize - 1; j < 5; j++) {
            this.cardViews[(player.playerId * 5) + j].setVisibility(8);
        }
        int loopEnd = handSize - 1;
        int j2 = 0;
        while (j2 < loopEnd) {
            try {
                Card nextCard = hand[j2];
                if (nextCard.getCardRank() != 14) {
                    if (player.playerId == 0) {
                        cardResource = nextCard.getCardImageL();
                    } else if (showCards2) {
                        cardResource = nextCard.getCardImage();
                    } else {
                        cardResource = this.cardBackResource;
                    }
                    this.cardViews[cardPoint].setImageResource(cardResource);
                    this.cardViews[cardPoint].setVisibility(0);
                }
                cardPoint++;
                j2++;
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
        Card nextCard2 = hand[handSize - 1];
        if (nextCard2.getCardRank() != 14) {
            int cardResource2 = nextCard2.getCardImageL();
            switch (animateDeal) {
                case 0:
                    this.cardViews[cardPoint].setVisibility(0);
                    if (player.isHuman() || showCards2) {
                        this.cardViews[cardPoint].setImageResource(cardResource2);
                    } else if (!settings.getFlashCardFromDiscard() || inAnimateDeal != 2) {
                        this.cardViews[cardPoint].setImageResource(this.cardBackResource);
                    } else {
                        this.cardViews[cardPoint].setImageResource(cardResource2);
                        flashCard(this.cardViews[cardPoint]);
                    }
                    redrawDiscards();
                    break;
                case 1:
                    if (!player.isHuman() && !showCards2) {
                        animate(this.ivwDeck, this.cardViews[cardPoint], this.cardBackResource, settings.getCardAnimationTime(), 0);
                        break;
                    } else {
                        animate(this.ivwDeck, this.cardViews[cardPoint], cardResource2, settings.getCardAnimationTime(), 0);
                        break;
                    }
                case 2:
                    if (!player.isHuman() && !showCards2) {
                        animate(this.ivwDiscardDummy, this.cardViews[cardPoint], this.cardBackResource, settings.getCardAnimationTime(), 0);
                        break;
                    } else {
                        animate(this.ivwDiscardDummy, this.cardViews[cardPoint], cardResource2, settings.getCardAnimationTime(), 0);
                        break;
                    }
                case 3:
                    int animateDelay = 0 + 1;
                    animate(this.ivwDeck, this.cardViews[cardPoint], cardResource2, 300, 0 * 150);
                    break;
            }
        }
        if (player.getPlayerType() == 1) {
            for (int j3 = 0; j3 < 5; j3++) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) this.cardViews[(player.playerId * 5) + j3].getLayoutParams();
                lp.bottomMargin = 0;
                lp.topMargin = this.cardViewMargin;
                this.cardViews[(player.playerId * 5) + j3].setLayoutParams(lp);
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    /* access modifiers changed from: private */
    public void redrawDiscards() {
        ArrayList<Card> currentDiscards = this.deck.getCurrentDiscards();
        int discardSize = this.deck.currentDiscardSize();
        for (int point = 0; point <= discardSize - 1; point++) {
            this.ivwDiscard[4 - point].setImageResource(currentDiscards.get(point).getCardImageL());
            this.ivwDiscard[4 - point].setVisibility(0);
        }
        for (int point2 = 4 - discardSize; point2 >= 0; point2--) {
            this.ivwDiscard[point2].setImageResource(R.drawable.empty);
            this.ivwDiscard[point2].setVisibility(4);
        }
        if (discardSize > 1) {
            ((ImageView) findViewById(R.id.ivwRightTap)).setVisibility(0);
        } else {
            ((ImageView) findViewById(R.id.ivwRightTap)).setVisibility(4);
        }
    }

    public void redrawCards(Player player, int animateDeal, Card cardTaken) {
        if (cardTaken != null) {
            this.ivwDiscardDummy.setImageResource(cardTaken.getCardImageL());
        }
        redrawPlayerCards(player, animateDeal, settings.getDebugMode());
    }

    public void redrawAllCards(int animateDeal) {
        for (int i = 0; i < settings.getNumberOfPlayers(); i++) {
            redrawPlayerCards(game.getPlayer(i), animateDeal, settings.getDebugMode());
        }
        redrawDiscards();
    }

    private void displayAllCards(Player[] players) {
        int i = 1;
        while (i < settings.getNumberOfPlayers() && players[i] != null) {
            redrawPlayerCards(players[i], 0, true);
            i++;
        }
    }

    private void startGame() {
        try {
            if (inGame) {
                redrawAllCards(0);
                game.continueGame(this.inHand);
                this.inHand = true;
                return;
            }
            inGame = true;
            game = new GameControl();
            game.startGame();
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    public void announceReshuffle() {
        try {
            Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.message_reshufflingCards), 0);
            toast.setGravity(16, 0, 0);
            toast.show();
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    public void declareAssaf(Player yanivPlayer, ArrayList<Player> assafPlayers) {
        int messageId;
        String buttonText;
        boolean assafByHuman = false;
        try {
            this.inHand = false;
            game.setPreviousWinner(assafPlayers.get(0));
            displayAllCards(game.getPlayers());
            int i = 0;
            while (true) {
                if (i >= assafPlayers.size()) {
                    break;
                } else if (assafPlayers.get(i).getPlayerType() == 1) {
                    assafByHuman = true;
                    ScoringForScoreloop.addAssafScore();
                    break;
                } else {
                    i++;
                }
            }
            String assafPlayersString = "";
            switch (assafPlayers.size()) {
                case 1:
                    assafPlayersString = assafPlayers.get(0).playerName;
                    break;
                case 2:
                    assafPlayersString = String.valueOf(assafPlayers.get(0).playerName) + getString(R.string.message_and) + assafPlayers.get(1).playerName;
                    break;
                case 3:
                    assafPlayersString = String.valueOf(assafPlayers.get(0).playerName) + ", " + assafPlayers.get(1).playerName + getString(R.string.message_and) + assafPlayers.get(2).playerName;
                    break;
            }
            this.previousWinner = assafPlayers.get(0);
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle((int) R.string.assaf);
            if (yanivPlayer.isHuman()) {
                ScoringForScoreloop.addAssafedScore();
                alertDialog.setMessage(MessageBuilder.buildMessage(this, R.string.message_youDeclaredYanivButLost, assafPlayersString));
                buttonText = getString(R.string.sadFace);
            } else {
                if (assafByHuman) {
                    if (assafPlayers.size() == 1) {
                        messageId = R.string.message_youDeclaredAssafAlone;
                    } else {
                        messageId = R.string.message_youAndOthersDeclaredAssaf;
                    }
                    buttonText = getString(R.string.happyFace);
                } else {
                    messageId = R.string.message_othersDeclaredAssafed;
                    buttonText = getString(R.string.neutralFace);
                }
                alertDialog.setMessage(MessageBuilder.buildMessage(this, messageId, yanivPlayer.getPlayerName(), assafPlayersString));
            }
            alertDialog.setButton(-1, buttonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        YanivGameActivity.this.showCurrentScores(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    public void declareYaniv(Player player) {
        String buttonText;
        try {
            this.inHand = false;
            game.setPreviousWinner(player);
            displayAllCards(game.getPlayers());
            AlertDialog alertDialog = new AlertDialog.Builder(thisActivity).create();
            if (player.isHuman()) {
                alertDialog.setTitle((int) R.string.yaniv);
                alertDialog.setMessage(MessageBuilder.buildMessage(thisActivity, R.string.message_youDeclaredYaniv, new String[0]));
                buttonText = getString(R.string.happyFace);
                ScoringForScoreloop.addYanivScore();
            } else {
                alertDialog.setTitle((int) R.string.yaniv);
                alertDialog.setMessage(MessageBuilder.buildMessage(thisActivity, R.string.message_otherDeclaredYaniv, player.getPlayerName()));
                buttonText = getString(R.string.sadFace);
            }
            ScoringForScoreloop.addPointDifferenceScore(game.getPlayers());
            alertDialog.setButton(-1, buttonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        YanivGameActivity.this.showCurrentScores(false);
                    } catch (Exception e) {
                        YanivGameActivity.this.exceptionHandler(e);
                    }
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
            this.previousWinner = player;
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    public void endGame() {
        Yaniv.inGame = false;
        new AlertDialog.Builder(this).setTitle((int) R.string.submitScoreTitle).setMessage(MessageBuilder.buildMessage(this, R.string.submitScoreMessage, Double.toString((double) ScoringForScoreloop.getScore()))).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                YanivGameActivity.this.submitScore();
            }
        }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                YanivGameActivity.this.finish();
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void showCurrentScores(boolean inHand2) throws Exception {
        game.showCurrentScores(inHand2);
    }

    public void displayPlayerAction(String message) {
        this.tvwMoveMessage.setText(message);
    }

    public void showHideYaniv(int visibility) {
        this.btnYaniv.setVisibility(visibility);
        this.yanivListenerEnabled = visibility == 0;
    }

    public void showHideRightTap(int visibility) {
        this.ivwRightTap.setVisibility(visibility);
    }

    public void showHideCont(int visibility) {
        this.btnNext.setVisibility(visibility);
        this.contListenerEnabled = visibility == 0;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(1);
    }

    private void setViewPointers() {
        this.cardViews[0] = (ImageView) findViewById(R.id.ivwCard00);
        this.cardViews[1] = (ImageView) findViewById(R.id.ivwCard01);
        this.cardViews[2] = (ImageView) findViewById(R.id.ivwCard02);
        this.cardViews[3] = (ImageView) findViewById(R.id.ivwCard03);
        this.cardViews[4] = (ImageView) findViewById(R.id.ivwCard04);
        this.cardViews[5] = (ImageView) findViewById(R.id.ivwCard05);
        this.cardViews[6] = (ImageView) findViewById(R.id.ivwCard06);
        this.cardViews[7] = (ImageView) findViewById(R.id.ivwCard07);
        this.cardViews[8] = (ImageView) findViewById(R.id.ivwCard08);
        this.cardViews[9] = (ImageView) findViewById(R.id.ivwCard09);
        this.cardViews[10] = (ImageView) findViewById(R.id.ivwCard10);
        this.cardViews[11] = (ImageView) findViewById(R.id.ivwCard11);
        this.cardViews[12] = (ImageView) findViewById(R.id.ivwCard12);
        this.cardViews[13] = (ImageView) findViewById(R.id.ivwCard13);
        this.cardViews[14] = (ImageView) findViewById(R.id.ivwCard14);
        this.cardViews[15] = (ImageView) findViewById(R.id.ivwCard15);
        this.cardViews[16] = (ImageView) findViewById(R.id.ivwCard16);
        this.cardViews[17] = (ImageView) findViewById(R.id.ivwCard17);
        this.cardViews[18] = (ImageView) findViewById(R.id.ivwCard18);
        this.cardViews[19] = (ImageView) findViewById(R.id.ivwCard19);
        this.ivwDeck = (ImageView) findViewById(R.id.ivwDeck);
        this.ivwDeckDummy = (ImageView) findViewById(R.id.ivwDeckDummy);
        this.ivwDiscard[0] = (ImageView) findViewById(R.id.ivwDiscard0);
        this.ivwDiscard[1] = (ImageView) findViewById(R.id.ivwDiscard1);
        this.ivwDiscard[2] = (ImageView) findViewById(R.id.ivwDiscard2);
        this.ivwDiscard[3] = (ImageView) findViewById(R.id.ivwDiscard3);
        this.ivwDiscard[4] = (ImageView) findViewById(R.id.ivwDiscard4);
        this.ivwDiscardDummy = (ImageView) findViewById(R.id.ivwDiscardDummy);
        this.btnYaniv = (Button) findViewById(R.id.btnYaniv);
        this.btnNext = (Button) findViewById(R.id.btnNext);
        this.ivwRightTap = (ImageView) findViewById(R.id.ivwRightTap);
        this.tvwMoveMessage = (TextView) findViewById(R.id.tvwMoveMessage);
        this.tvwMoveMessage.setSelected(true);
        this.tvwSeed = (TextView) findViewById(R.id.tvwSeed);
        this.ivwDeck.setImageResource(R.drawable.b1fvl);
        this.ivwDeckDummy.setImageResource(R.drawable.b1fvl);
        for (ImageView onClickListener : this.cardViews) {
            onClickListener.setOnClickListener(this.lsn_cardViews_onClick);
        }
        this.ivwDeck.setOnClickListener(this.lsn_vwDeck_onClick);
        this.ivwDiscard[0].setOnClickListener(this.lsn_vwDiscard_onClick);
        this.ivwDiscard[1].setOnClickListener(this.lsn_vwDiscard_onClick);
        this.ivwDiscard[2].setOnClickListener(this.lsn_vwDiscard_onClick);
        this.ivwDiscard[3].setOnClickListener(this.lsn_vwDiscard_onClick);
        this.ivwDiscard[4].setOnClickListener(this.lsn_vwDiscard_onClick);
        this.btnYaniv.setOnClickListener(this.lsn_btnYaniv_onClick);
        this.btnNext.setOnClickListener(this.lsn_btnNext_onClick);
        this.ivwRightTap.setOnClickListener(this.lsn_vwRightTap_onClick);
    }

    /* access modifiers changed from: private */
    public static Player getCurrentPlayer() {
        return HandControl.getCurrentPlayer();
    }

    public void onCreate(Bundle savedInstanceState) {
        try {
            setContentView((int) R.layout.game);
            super.onCreate(savedInstanceState);
            setViewPointers();
            settings = CurrentPreferences.getCurrentPreferences(getApplicationContext());
            ScoringForScoreloop.initialise(getApplicationContext());
            ((TextView) findViewById(R.id.tvwYanivAmountRequired)).setText("Yaniv Amount: " + String.valueOf(settings.getYanivAmount()));
            MessageBuilder.buildMessage(this, (TextView) findViewById(R.id.tvwSkillLevel), (int) R.string.skillLevelPrefix, getResources().getStringArray(R.array.preferences_CurrentSkillLevelText)[settings.getCurrentSkillLevel() - 1]);
            scoreloopInit();
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            super.onPause();
            if (!HandControl.getCurrentPlayer().isHuman()) {
                ((AutomatedPlayer) HandControl.getCurrentPlayer()).pauseTurn();
            }
            if (inGame) {
                saveData();
            } else {
                clearSavedData();
            }
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        try {
            super.onResume();
            boolean reset = false;
            Bundle bundle = getIntent().getExtras();
            thisActivity = this;
            if (bundle != null) {
                reset = bundle.getBoolean("reset");
                getIntent().removeExtra("reset");
            }
            if (reset) {
                inGame = false;
            } else {
                restoreData();
                if (inGame) {
                    game.getPlayer(0).playerHand.clearSelectedCards();
                }
                this.deck = Deck.getDeck();
            }
            startGame();
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    private void restoreData() {
        boolean z;
        FileInputStream restoreFile = null;
        try {
            FileInputStream restoreFile2 = openFileInput(this.STATE_SAVE_FILE_NAME);
            try {
                ObjectInput input = new ObjectInputStream(restoreFile2);
                game = (GameControl) input.readObject();
                ScoringForScoreloop.setScore(input.readInt());
                if (game != null) {
                    z = true;
                } else {
                    z = false;
                }
                inGame = z;
                try {
                    restoreFile2.close();
                    super.clearSavedData();
                } catch (Exception e) {
                    inGame = false;
                }
            } catch (Exception e2) {
                inGame = false;
                try {
                    restoreFile.close();
                    super.clearSavedData();
                } catch (Exception e3) {
                    inGame = false;
                }
            } catch (Throwable th) {
                try {
                    restoreFile.close();
                    super.clearSavedData();
                    throw th;
                } catch (Exception e4) {
                    inGame = false;
                }
            }
        } catch (FileNotFoundException e5) {
            inGame = false;
            try {
                restoreFile.close();
                super.clearSavedData();
            } catch (Exception e6) {
                inGame = false;
            }
        }
    }

    private void saveData() {
        FileOutputStream saveFile = null;
        try {
            saveFile = openFileOutput(this.STATE_SAVE_FILE_NAME, 0);
            ObjectOutput output = new ObjectOutputStream(saveFile);
            game.preSave();
            output.writeObject(game);
            output.writeInt(ScoringForScoreloop.getScore());
            output.close();
            try {
                saveFile.close();
            } catch (Exception ex) {
                exceptionHandler(ex);
            }
        } catch (Exception e) {
            exceptionHandler(e);
            try {
                saveFile.close();
            } catch (Exception ex2) {
                exceptionHandler(ex2);
            }
        } catch (Throwable th) {
            try {
                saveFile.close();
            } catch (Exception ex3) {
                exceptionHandler(ex3);
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.gameoptions, menu);
        menu.findItem(R.id.help_menu_item).setIntent(new Intent(this, YanivRulesActivity.class));
        menu.findItem(R.id.controls_menu_item).setIntent(new Intent(this, YanivControlsActivity.class));
        menu.findItem(R.id.settings_menu_item).setIntent(new Intent(this, PreferencesActivity.class).putExtra("FromMainMenu", false));
        menu.findItem(R.id.cancelgame_menu_item).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                YanivGameActivity.inGame = false;
                YanivGameActivity.this.clearSavedData();
                YanivGameActivity.this.finish();
                return false;
            }
        });
        menu.findItem(R.id.currentscores_menu_item).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                YanivGameActivity.game.showCurrentScores(true);
                return false;
            }
        });
        menu.findItem(R.id.reportproblem_menu_item).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                YanivGameActivity.this.reportProblem();
                return false;
            }
        });
        return true;
    }

    public void onStop() {
        super.onStop();
    }

    public void clearLastMoveMessage() {
        this.tvwMoveMessage.setText("");
    }

    public void onDestroy() {
        try {
            super.onDestroy();
        } catch (Exception ex) {
            exceptionHandler(ex);
        }
    }

    public void showCurrentScore(double currentScore) {
        ((TextView) findViewById(R.id.tvwCurrentScore)).setText(new Long(new Double(currentScore).longValue()).toString());
    }

    private void scoreloopInit() {
        ScoreloopManagerSingleton.get().setOnScoreSubmitObserver(this);
    }

    /* access modifiers changed from: private */
    public void submitScore() {
        Double scoreResult = Double.valueOf(Integer.valueOf(ScoringForScoreloop.getScore()).doubleValue());
        this.progressDialog = ProgressDialog.show(this, "", "Please wait ...", true);
        ScoreloopManagerSingleton.get().onGamePlayEnded(scoreResult, new Integer(settings.getCurrentSkillLevel() - 1));
    }

    public void onScoreSubmit(int status, Exception error) {
        startActivity(new Intent(this, ShowResultOverlayActivity.class).putExtra("mode", settings.getCurrentSkillLevel() - 1));
        this.progressDialog.dismiss();
        finish();
    }
}
