package ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.scoreloop.client.android.ui.LeaderboardsScreenActivity;
import il.co.anykey.games.yaniv.lite.R;
import yanivlib.pkg.CurrentPreferences;

public class YanivMenuActivity extends Yaniv {
    boolean cancelGame = false;
    boolean inOption = false;
    private View.OnClickListener lsn_btnControls_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                YanivMenuActivity.this.startActivity(new Intent(YanivMenuActivity.this, YanivControlsActivity.class));
            } catch (Exception e) {
                YanivMenuActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_btnLeaderBoard_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                YanivMenuActivity.this.startActivity(new Intent(YanivMenuActivity.this, LeaderboardsScreenActivity.class).putExtra("mode", CurrentPreferences.getCurrentPreferences(YanivMenuActivity.this).getCurrentSkillLevel() - 1));
            } catch (Exception e) {
                YanivMenuActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_btnPreferences_onClick = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View v) {
            try {
                YanivMenuActivity.this.startActivityForResult(new Intent(YanivMenuActivity.this, PreferencesActivity.class).putExtra("FromMainMenu", true), 1);
            } catch (Exception e) {
                YanivMenuActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_btnReportProblem_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                YanivMenuActivity.this.reportProblem();
            } catch (Exception e) {
                YanivMenuActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_btnResumeGame_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                YanivMenuActivity.this.startActivity(new Intent(YanivMenuActivity.this, YanivGameActivity.class));
            } catch (Exception e) {
                YanivMenuActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_btnRules_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                YanivMenuActivity.this.startActivity(new Intent(YanivMenuActivity.this, YanivRulesActivity.class));
            } catch (Exception e) {
                YanivMenuActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_btnScores_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                YanivMenuActivity.this.startActivity(new Intent(YanivMenuActivity.this, YanivScoresActivity.class));
            } catch (Exception e) {
                YanivMenuActivity.this.exceptionHandler(e);
            }
        }
    };
    private View.OnClickListener lsn_btnStartGame_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (!YanivMenuActivity.this.inOption) {
                YanivMenuActivity.this.inOption = true;
                if (!YanivMenuActivity.inGame) {
                    YanivMenuActivity.this.startNewGame();
                    YanivMenuActivity.this.inOption = false;
                } else {
                    final AlertDialog alertDialog = new AlertDialog.Builder(YanivMenuActivity.this).create();
                    alertDialog.setTitle(YanivMenuActivity.this.getString(R.string.activeGame));
                    alertDialog.setMessage(YanivMenuActivity.this.getString(R.string.inMiddleOfGame));
                    alertDialog.setButton(-2, YanivMenuActivity.this.getString(R.string.startNewGame), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            YanivMenuActivity.this.startNewGame();
                            alertDialog.dismiss();
                            YanivMenuActivity.this.inOption = false;
                        }
                    });
                    alertDialog.setButton(-1, YanivMenuActivity.this.getString(R.string.continueOldGame), new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                         arg types: [java.lang.String, int]
                         candidates:
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                        public void onClick(DialogInterface arg0, int arg1) {
                            alertDialog.dismiss();
                            YanivMenuActivity.this.startActivity(new Intent(YanivMenuActivity.this, YanivGameActivity.class).putExtra("reset", false));
                            YanivMenuActivity.this.inOption = false;
                        }
                    });
                    alertDialog.show();
                }
            }
            YanivMenuActivity.this.inOption = false;
        }
    };
    private View.OnClickListener lsn_btnStartMultiPlayerGame_onClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                if (ApplicationControllerBase.multiPlayerGameControl.startNewMultiplayerGame(YanivMenuActivity.this)) {
                    YanivMenuActivity.this.finish();
                }
            } catch (Exception e) {
                YanivMenuActivity.this.exceptionHandler(e);
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView((int) R.layout.menu);
            this.inOption = false;
            ((Button) findViewById(R.id.btnStartGame)).setOnClickListener(this.lsn_btnStartGame_onClick);
            ((Button) findViewById(R.id.btnResumeGame)).setOnClickListener(this.lsn_btnResumeGame_onClick);
            if (ApplicationControllerBase.multiPlayerGameControl.allowedMultiPlayerGame()) {
                ((Button) findViewById(R.id.btnStartMultiPlayer)).setOnClickListener(this.lsn_btnStartMultiPlayerGame_onClick);
                ((Button) findViewById(R.id.btnStartMultiPlayer)).setVisibility(0);
            } else {
                ((Button) findViewById(R.id.btnStartMultiPlayer)).setVisibility(8);
            }
            ((Button) findViewById(R.id.btnRules)).setOnClickListener(this.lsn_btnRules_onClick);
            ((Button) findViewById(R.id.btnControls)).setOnClickListener(this.lsn_btnControls_onClick);
            ((Button) findViewById(R.id.btnSettings)).setOnClickListener(this.lsn_btnPreferences_onClick);
            ((Button) findViewById(R.id.btnScores)).setOnClickListener(this.lsn_btnScores_onClick);
            ((Button) findViewById(R.id.btnLeaderBoard)).setOnClickListener(this.lsn_btnLeaderBoard_onClick);
            ((Button) findViewById(R.id.btnReportProblem)).setOnClickListener(this.lsn_btnReportProblem_onClick);
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    /* access modifiers changed from: private */
    public void startNewGame() {
        inGame = false;
        CurrentPreferences currentPreferences = CurrentPreferences.getCurrentPreferences(this);
        if (currentPreferences.getSkillLevel() != 0) {
            currentPreferences.setCurrentSkillLevel(currentPreferences.getSkillLevel(), false);
            startActivity(new Intent(this, YanivGameActivity.class));
            return;
        }
        startActivity(new Intent(this, NewGamePreferencesActivity.class));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == -1 && requestCode == 1) {
                this.cancelGame = data.getBooleanExtra("cancelGame", false);
            }
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        try {
            setRequestedOrientation(1);
            super.onConfigurationChanged(newConfig);
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    public void onBackPressed() {
        try {
            if (CurrentPreferences.getCurrentPreferences(getApplicationContext()).getAlertExitProgram()) {
                new AlertDialog.Builder(this).setTitle((int) R.string.exitProgramTitle).setMessage((int) R.string.exitProgramMessage).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        YanivMenuActivity.this.finish();
                    }
                }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).create().show();
            } else {
                finish();
            }
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        try {
            super.onResume();
            currentGameType = 1;
            this.inOption = false;
            if (this.cancelGame) {
                inGame = false;
                super.clearSavedData();
            }
            setResumeButtonVisibility();
        } catch (Exception e) {
            exceptionHandler(e);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    /* access modifiers changed from: private */
    public void setResumeButtonVisibility() {
        if (inGame) {
            ((Button) findViewById(R.id.btnResumeGame)).setVisibility(0);
        } else {
            ((Button) findViewById(R.id.btnResumeGame)).setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            super.onPause();
        } catch (Exception ex) {
            exceptionHandler(ex);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.gameoptions, menu);
        menu.findItem(R.id.help_menu_item).setIntent(new Intent(this, YanivRulesActivity.class));
        menu.findItem(R.id.controls_menu_item).setIntent(new Intent(this, YanivControlsActivity.class));
        menu.findItem(R.id.settings_menu_item).setIntent(new Intent(this, PreferencesActivity.class).putExtra("FromMainMenu", true));
        menu.findItem(R.id.cancelgame_menu_item).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                YanivMenuActivity.inGame = false;
                YanivMenuActivity.this.clearSavedData();
                YanivMenuActivity.this.setResumeButtonVisibility();
                return false;
            }
        });
        menu.findItem(R.id.currentscores_menu_item).setVisible(false).setEnabled(false);
        menu.findItem(R.id.reportproblem_menu_item).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                YanivMenuActivity.this.reportProblem();
                return false;
            }
        });
        return true;
    }
}
