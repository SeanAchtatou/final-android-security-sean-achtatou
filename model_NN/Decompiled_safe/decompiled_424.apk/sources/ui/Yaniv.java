package ui;

import GameControl.GameControl;
import PlayerControl.Player;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.UUID;

public class Yaniv extends Activity {
    public static final int GAMETYPEMULTIPLAYER = 2;
    public static final int GAMETYPESINGLEPLAYER = 1;
    public static final UUID MY_UUID = UUID.fromString("fa87aaaa-afac-11de-8a39-0800200c9a66");
    public static int currentGameType;
    protected static GameControl game = null;
    protected static boolean inGame = false;
    protected String STATE_SAVE_FILE_NAME = "yanivStateSave";
    protected Button btnNext;
    protected Button btnYaniv;
    protected int cardViewMargin = 14;
    protected ImageView[] cardViews = new ImageView[20];
    protected ImageView ivwDeck;
    protected ImageView ivwDeckDummy;
    protected ImageView[] ivwDiscard = new ImageView[5];
    protected ImageView ivwDiscardDummy;
    protected ImageView ivwRightTap;
    protected Player previousWinner = null;
    protected TextView tvwMoveMessage;
    protected TextView tvwSeed;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        inGame = true;
        try {
            FileInputStream restoreFile = openFileInput(this.STATE_SAVE_FILE_NAME);
        } catch (FileNotFoundException e) {
            inGame = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void clearSavedData() {
        deleteFile(this.STATE_SAVE_FILE_NAME);
    }

    public void exceptionHandler(Exception ex) {
        try {
            throw ex;
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    private void sendExceptionMail(Exception ex) {
        sendErrorEmail(String.valueOf(preparePhoneDetails()) + prepareErrorDetails(ex));
    }

    /* access modifiers changed from: protected */
    public void reportProblem() {
        sendErrorEmail(String.valueOf(addMoreDetailsArea()) + preparePhoneDetails() + prepareErrorDetails(null));
    }

    private String addMoreDetailsArea() {
        return "<BR><BR>Please add problem details between the 2 lines below<BR><BR>--------------------------------<BR><BR>--------------------------------<BR><BR>";
    }

    private String preparePhoneDetails() {
        String yanivVersion;
        try {
            PackageInfo pi = getPackageManager().getPackageInfo("il.co.anykey.games.yaniv.lite", 128);
            yanivVersion = "Name:" + pi.versionName + ", Code:" + pi.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            yanivVersion = "Could not find package for debug info";
        }
        return "<BR>Details:<BR> OS Version  : " + System.getProperty("os.version") + "(" + Build.VERSION.INCREMENTAL + ")" + "<BR> OS API Level: " + Build.VERSION.SDK + "<BR> Device      : " + Build.DEVICE + "<BR> Model and Product: " + Build.MODEL + " (" + Build.PRODUCT + ") <BR>" + "<BR> Version: " + yanivVersion;
    }

    private String prepareErrorDetails(Exception ex) {
        String errorMessage;
        String errorMessage2 = preparePhoneDetails();
        if (game != null) {
            errorMessage2 = String.valueOf(errorMessage2) + game.getCrashData();
        }
        if (ex != null) {
            errorMessage = String.valueOf(errorMessage) + ex.toString();
            StackTraceElement[] stackTrace = ex.getStackTrace();
            for (int i = 0; i < stackTrace.length; i++) {
                errorMessage = String.valueOf(errorMessage) + "<BR>" + stackTrace[i].toString();
            }
        }
        return errorMessage;
    }

    private void sendErrorEmail(String errorMessage) {
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("text/html").putExtra("android.intent.extra.EMAIL", new String[]{"yaniv@anykey.co.il"}).putExtra("android.intent.extra.SUBJECT", "A problem occurred in Yaniv!").putExtra("android.intent.extra.TEXT", Html.fromHtml("The following error occurred:" + errorMessage));
        startActivity(Intent.createChooser(emailIntent, "Send mail...").addFlags(268435456));
    }
}
