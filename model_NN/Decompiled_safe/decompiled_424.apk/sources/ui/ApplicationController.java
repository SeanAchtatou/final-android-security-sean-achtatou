package ui;

import GameControl.MultiPlayerGameControl;

public class ApplicationController extends ApplicationControllerBase {
    public void onCreate() {
        super.onCreate();
        preferencesScreenDefiner = new PreferencesScreenDefiner();
        adController = new AdControl();
        multiPlayerGameControl = new MultiPlayerGameControl();
    }

    public void onTerminate() {
        super.onTerminate();
    }
}
