package ui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;
import il.co.anykey.games.yaniv.lite.R;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class YanivRulesActivity extends Yaniv {
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView((int) R.layout.help);
            InputStream iFile = getResources().openRawResource(R.raw.yaniv_rules);
            TextView helpText = (TextView) findViewById(R.id.TextView_HelpText);
            if (Locale.getDefault().getLanguage().equals("iw")) {
                helpText.setText(getString(R.string.gameRules));
                helpText.setGravity(5);
                return;
            }
            helpText.setText(inputStreamToString(iFile));
            helpText.setGravity(3);
        } catch (Exception e) {
        }
    }

    public String inputStreamToString(InputStream is) throws IOException {
        StringBuffer sBuffer = new StringBuffer();
        DataInputStream dataIO = new DataInputStream(is);
        while (true) {
            String strLine = dataIO.readLine();
            if (strLine == null) {
                dataIO.close();
                is.close();
                return sBuffer.toString();
            }
            sBuffer.append(String.valueOf(strLine) + "\n");
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        setRequestedOrientation(1);
        super.onConfigurationChanged(newConfig);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
