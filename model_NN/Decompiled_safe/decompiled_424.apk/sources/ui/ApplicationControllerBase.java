package ui;

import Interfaces.AdControlInterface;
import Interfaces.MultiPlayerGameInterface;
import Interfaces.PreferencesScreenDefinerInterface;
import android.app.Application;
import android.content.Context;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import yanivlib.pkg.CurrentPreferences;

public class ApplicationControllerBase extends Application {
    public static AdControlInterface adController;
    public static CurrentPreferences currentPreferences;
    public static MultiPlayerGameInterface multiPlayerGameControl;
    public static Context myApplicationContext;
    public static PreferencesScreenDefinerInterface preferencesScreenDefiner;

    public void onCreate() {
        super.onCreate();
        ScoreloopManagerSingleton.init(this, "AgYhjzf07vnBusLP9krUtvc+rnd2KiCnhORATIC1aUvsF3kvGZIZdQ==");
        currentPreferences = CurrentPreferences.getCurrentPreferences(this);
        myApplicationContext = this;
    }

    public void onTerminate() {
        super.onTerminate();
        ScoreloopManagerSingleton.destroy();
    }

    public static Context getMyApplicationContext() {
        return myApplicationContext;
    }
}
