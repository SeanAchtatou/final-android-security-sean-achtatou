package ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import il.co.anykey.games.yaniv.lite.R;
import java.util.Map;
import yanivlib.pkg.CurrentPreferences;

public class NewGamePreferencesActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        final CurrentPreferences currentPreferences = CurrentPreferences.getCurrentPreferences(this);
        Bundle extras = getIntent().getExtras();
        addPreferencesFromResource(R.xml.preferences_new_game);
        setContentView((int) R.layout.preferenceswithbutton);
        for (Map.Entry<String, ?> entry : getPreferenceScreen().getSharedPreferences().getAll().entrySet()) {
            changeSummary(findPreference((CharSequence) entry.getKey()));
        }
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        ((Button) findViewById(R.id.btnStartGame)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                NewGamePreferencesActivity.this.startActivity(new Intent(NewGamePreferencesActivity.this, YanivGameActivity.class));
                CheckBox dontAskAgain = (CheckBox) NewGamePreferencesActivity.this.findViewById(R.id.dontAskAgain);
                if (dontAskAgain.isChecked()) {
                    currentPreferences.setSkillLevelFromCurrentSkillLevel(true);
                    dontAskAgain.setChecked(false);
                }
                NewGamePreferencesActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        CurrentPreferences.getCurrentPreferences(getApplicationContext()).refresh();
    }

    private void changeSummary(Preference pref) {
        try {
            ListPreference listPref = (ListPreference) pref;
            String currentSummary = (String) pref.getSummary();
            int index1 = currentSummary.indexOf(getString(R.string.preferences_CurrentValueDisplay).substring(0, getString(R.string.preferences_CurrentValueDisplay).indexOf("@")));
            if (index1 != -1) {
                currentSummary = currentSummary.substring(0, index1);
            }
            pref.setSummary(String.valueOf(currentSummary) + " " + getString(R.string.preferences_CurrentValueDisplay).replace("@", (String) listPref.getEntry()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        changeSummary(findPreference(key));
    }
}
