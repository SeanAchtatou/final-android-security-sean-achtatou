package ui;

import Interfaces.PreferencesScreenDefinerInterface;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import il.co.anykey.games.yaniv.lite.R;

public class PreferencesScreenDefiner implements PreferencesScreenDefinerInterface {
    public void loadPreferencesScreen(PreferenceActivity pref, boolean inGame, Bundle extras) {
        try {
            pref.addPreferencesFromResource(R.xml.preferences_top);
            if (extras.getBoolean("FromMainMenu") || !inGame) {
                pref.addPreferencesFromResource(R.xml.preferences_middle);
            }
            pref.addPreferencesFromResource(R.xml.preferences_bottom);
            if ((pref.getApplicationInfo().flags & 2) != 0) {
                pref.addPreferencesFromResource(R.xml.preferences_debug_mode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
