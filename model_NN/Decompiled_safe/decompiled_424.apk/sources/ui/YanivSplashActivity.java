package ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import il.co.anykey.games.yaniv.lite.R;

public class YanivSplashActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
        setContentView((int) R.layout.splash);
        ((TextView) findViewById(R.id.txwSplashTitle)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        Animation fade2 = AnimationUtils.loadAnimation(this, R.anim.fade_in2);
        ((TextView) findViewById(R.id.txwSplashTrailer)).startAnimation(fade2);
        fade2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                YanivSplashActivity.this.startActivity(new Intent(YanivSplashActivity.this, YanivMenuActivity.class));
                YanivSplashActivity.this.finish();
            }

            public void onAnimationRepeat(Animation arg0) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        LayoutAnimationController controller = new LayoutAnimationController(AnimationUtils.loadAnimation(this, R.anim.custom_anim));
        TableLayout table = (TableLayout) findViewById(R.id.tableLayout1);
        for (int i = 0; i < table.getChildCount(); i++) {
            ((TableRow) table.getChildAt(i)).setLayoutAnimation(controller);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        ((TextView) findViewById(R.id.txwSplashTitle)).clearAnimation();
        ((TextView) findViewById(R.id.txwSplashTrailer)).clearAnimation();
        TableLayout table = (TableLayout) findViewById(R.id.tableLayout1);
        for (int i = 0; i < table.getChildCount(); i++) {
            ((TableRow) table.getChildAt(i)).clearAnimation();
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(1);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
