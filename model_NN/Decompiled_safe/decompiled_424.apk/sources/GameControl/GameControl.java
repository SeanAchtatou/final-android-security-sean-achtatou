package GameControl;

import DeckControl.Deck;
import HandControl.HandControl;
import PlayerControl.AutomatedPlayer;
import PlayerControl.AutomatedPlayerLevel1;
import PlayerControl.AutomatedPlayerLevel2;
import PlayerControl.AutomatedPlayerLevel3;
import PlayerControl.HumanPlayer;
import PlayerControl.Player;
import android.app.Dialog;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import il.co.anykey.games.yaniv.lite.R;
import java.io.Serializable;
import java.util.Random;
import ui.YanivGameActivity;
import util.DBAdaptor;
import util.MessageBuilder;
import util.ScoringForScoreloop;
import yanivlib.pkg.CurrentPreferences;

public class GameControl implements Serializable {
    private static final long serialVersionUID = -1223788546109850295L;
    transient DBAdaptor dbAdaptor = new DBAdaptor(YanivGameActivity.thisActivity);
    boolean gameOver;
    protected HandControl hand;
    Player[] players = new Player[4];
    private CurrentPreferences preferences;
    Player previousWinner;
    Player startingPlayer;
    int startingPlayerId;

    public GameControl() {
        this.dbAdaptor.open();
        this.dbAdaptor.clearScores();
        this.dbAdaptor.close();
        this.preferences = CurrentPreferences.getCurrentPreferences(YanivGameActivity.thisActivity);
    }

    private AutomatedPlayer newAutomatedPlayer(int playerId, String playerName) {
        switch (this.preferences.getCurrentSkillLevel()) {
            case 1:
                return new AutomatedPlayerLevel1(playerId, playerName);
            case 2:
                return new AutomatedPlayerLevel2(playerId, playerName);
            case 3:
                return new AutomatedPlayerLevel3(playerId, playerName);
            default:
                return null;
        }
    }

    public void startGame() {
        ScoringForScoreloop.resetScore();
        YanivGameActivity.thisActivity.showHideYaniv(4);
        YanivGameActivity.thisActivity.showHideCont(4);
        YanivGameActivity.thisActivity.showHideRightTap(4);
        this.players[0] = new HumanPlayer(0, YanivGameActivity.thisActivity.getString(R.string.you));
        this.startingPlayer = this.players[0];
        switch (this.preferences.getNumberOfPlayers()) {
            case 2:
                this.players[1] = newAutomatedPlayer(2, YanivGameActivity.thisActivity.getString(R.string.player2));
                this.players[0].setNextPlayer(this.players[1]);
                this.players[1].setNextPlayer(this.players[0]);
                if (this.previousWinner != null && this.previousWinner.playerId == 2) {
                    this.startingPlayer = this.players[1];
                    break;
                }
            case 3:
                this.players[1] = newAutomatedPlayer(1, YanivGameActivity.thisActivity.getString(R.string.player2));
                this.players[2] = newAutomatedPlayer(2, YanivGameActivity.thisActivity.getString(R.string.player3));
                this.players[0].setNextPlayer(this.players[1]);
                this.players[1].setNextPlayer(this.players[2]);
                this.players[2].setNextPlayer(this.players[0]);
                if (this.previousWinner != null) {
                    this.startingPlayer = this.players[this.previousWinner.playerId];
                    break;
                }
                break;
            case 4:
                this.players[1] = newAutomatedPlayer(1, YanivGameActivity.thisActivity.getString(R.string.player2));
                this.players[2] = newAutomatedPlayer(2, YanivGameActivity.thisActivity.getString(R.string.player3));
                this.players[3] = newAutomatedPlayer(3, YanivGameActivity.thisActivity.getString(R.string.player4));
                this.players[0].setNextPlayer(this.players[1]);
                this.players[1].setNextPlayer(this.players[2]);
                this.players[2].setNextPlayer(this.players[3]);
                this.players[3].setNextPlayer(this.players[0]);
                if (this.previousWinner != null) {
                    this.startingPlayer = this.players[this.previousWinner.playerId];
                    break;
                }
                break;
        }
        this.previousWinner = null;
        YanivGameActivity.thisActivity.clearLastMoveMessage();
        startHand();
    }

    public void showCurrentScores(boolean inHand) {
        String message;
        try {
            this.dbAdaptor = new DBAdaptor(YanivGameActivity.thisActivity);
            this.dbAdaptor.open();
            Cursor scoresCursor = this.dbAdaptor.getScores();
            Cursor totalsCursor = this.dbAdaptor.getTotalScores();
            YanivGameActivity.thisActivity.startManagingCursor(scoresCursor);
            YanivGameActivity.thisActivity.startManagingCursor(totalsCursor);
            Dialog dialog = new Dialog(YanivGameActivity.thisActivity, 16973835);
            dialog.setContentView((int) R.layout.scores);
            ListView lvw = (ListView) dialog.getWindow().findViewById(R.id.lvwScores);
            View footerView = YanivGameActivity.thisActivity.getLayoutInflater().inflate(R.layout.playerscorestotals, (ViewGroup) null);
            totalsCursor.moveToFirst();
            ((TextView) footerView.findViewById(R.id.tvwPlayer0Total)).setText(String.valueOf(totalsCursor.getInt(totalsCursor.getColumnIndex("player0Total"))));
            ((TextView) dialog.findViewById(R.id.tvwPlayer0Name)).setText(this.players[0].playerName);
            switch (this.preferences.getNumberOfPlayers()) {
                case 2:
                    ((TextView) footerView.findViewById(R.id.tvwPlayer2Total)).setText(String.valueOf(totalsCursor.getInt(totalsCursor.getColumnIndex("player2Total"))));
                    lvw.addFooterView(footerView);
                    ((TextView) dialog.findViewById(R.id.tvwPlayer2Name)).setText(this.players[1].playerName);
                    lvw.setAdapter((ListAdapter) new SimpleCursorAdapter(YanivGameActivity.thisActivity, R.layout.playerscoresrow, scoresCursor, new String[]{"_id", "player0Score", "player2Score"}, new int[]{R.id.tvwGameNumber, R.id.tvwPlayer0Score, R.id.tvwPlayer2Score}));
                    break;
                case 3:
                    ((TextView) footerView.findViewById(R.id.tvwPlayer1Total)).setText(String.valueOf(totalsCursor.getInt(totalsCursor.getColumnIndex("player1Total"))));
                    ((TextView) footerView.findViewById(R.id.tvwPlayer2Total)).setText(String.valueOf(totalsCursor.getInt(totalsCursor.getColumnIndex("player2Total"))));
                    lvw.addFooterView(footerView);
                    ((TextView) dialog.findViewById(R.id.tvwPlayer1Name)).setText(this.players[1].playerName);
                    ((TextView) dialog.findViewById(R.id.tvwPlayer2Name)).setText(this.players[2].playerName);
                    lvw.setAdapter((ListAdapter) new SimpleCursorAdapter(YanivGameActivity.thisActivity, R.layout.playerscoresrow, scoresCursor, new String[]{"_id", "player0Score", "player1Score", "player2Score"}, new int[]{R.id.tvwGameNumber, R.id.tvwPlayer0Score, R.id.tvwPlayer1Score, R.id.tvwPlayer2Score}));
                    break;
                case 4:
                    ((TextView) footerView.findViewById(R.id.tvwPlayer1Total)).setText(String.valueOf(totalsCursor.getInt(totalsCursor.getColumnIndex("player1Total"))));
                    ((TextView) footerView.findViewById(R.id.tvwPlayer2Total)).setText(String.valueOf(totalsCursor.getInt(totalsCursor.getColumnIndex("player2Total"))));
                    ((TextView) footerView.findViewById(R.id.tvwPlayer3Total)).setText(String.valueOf(totalsCursor.getInt(totalsCursor.getColumnIndex("player3Total"))));
                    lvw.addFooterView(footerView);
                    ((TextView) dialog.findViewById(R.id.tvwPlayer1Name)).setText(this.players[1].playerName);
                    ((TextView) dialog.findViewById(R.id.tvwPlayer2Name)).setText(this.players[2].playerName);
                    ((TextView) dialog.findViewById(R.id.tvwPlayer3Name)).setText(this.players[3].playerName);
                    lvw.setAdapter((ListAdapter) new SimpleCursorAdapter(YanivGameActivity.thisActivity, R.layout.playerscoresrow, scoresCursor, new String[]{"_id", "player0Score", "player1Score", "player2Score", "player3Score"}, new int[]{R.id.tvwGameNumber, R.id.tvwPlayer0Score, R.id.tvwPlayer1Score, R.id.tvwPlayer2Score, R.id.tvwPlayer3Score}));
                    break;
            }
            this.gameOver = false;
            totalsCursor.moveToFirst();
            int[] playerTotals = {totalsCursor.getInt(totalsCursor.getColumnIndex("player0Total")), totalsCursor.getInt(totalsCursor.getColumnIndex("player1Total")), totalsCursor.getInt(totalsCursor.getColumnIndex("player2Total")), totalsCursor.getInt(totalsCursor.getColumnIndex("player3Total"))};
            int lowestScore = totalsCursor.getInt(totalsCursor.getColumnIndex("lowestScore"));
            boolean wonByHuman = false;
            if (totalsCursor.getInt(totalsCursor.getColumnIndex("highestScore")) >= this.preferences.getGameEndScore()) {
                String[] winners = new String[3];
                String winner = "";
                int current = 0;
                for (int i = 0; i < 4; i++) {
                    if (playerTotals[i] == lowestScore) {
                        winners[current] = this.players[i].getPlayerName();
                        if (this.players[i].isHuman()) {
                            wonByHuman = true;
                            ScoringForScoreloop.addWinningScore();
                        }
                        current++;
                    }
                }
                switch (current) {
                    case 1:
                        winner = winners[0];
                        break;
                    case 2:
                        winner = String.valueOf(winners[0]) + YanivGameActivity.thisActivity.getString(R.string.message_and) + winners[1];
                        break;
                    case 3:
                        winner = String.valueOf(winners[0]) + ", " + winners[1] + YanivGameActivity.thisActivity.getString(R.string.message_and) + winners[2];
                        break;
                }
                if (!wonByHuman) {
                    message = MessageBuilder.buildMessage(YanivGameActivity.thisActivity, R.string.message_gameOverOthersWon, winner);
                } else if (current == 1) {
                    message = YanivGameActivity.thisActivity.getString(R.string.message_gameOverYouWonAlone);
                } else {
                    message = MessageBuilder.buildMessage(YanivGameActivity.thisActivity, R.string.message_gameOverYouWithOthers, winner);
                }
                ((TextView) dialog.findViewById(R.id.tvwGameOver)).setText(message);
                ((TextView) dialog.findViewById(R.id.tvwGameOver)).setVisibility(0);
                this.gameOver = true;
            }
            dialog.setTitle("Scores");
            final Dialog dialog2 = dialog;
            final boolean z = inHand;
            ((Button) dialog.findViewById(R.id.btnOK)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    dialog2.dismiss();
                    if (GameControl.this.gameOver) {
                        YanivGameActivity.thisActivity.endGame();
                    } else if (!z) {
                        GameControl.this.startHand();
                    }
                }
            });
            dialog.setCancelable(false);
            dialog.show();
            this.dbAdaptor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void preSave() {
        this.startingPlayerId = HandControl.getCurrentPlayer().getPlayerId();
    }

    public void continueGame(boolean inHand) {
        this.startingPlayer = this.players[this.startingPlayerId];
        YanivGameActivity.thisActivity.showHideYaniv(4);
        YanivGameActivity.thisActivity.showHideRightTap(4);
        if (this.startingPlayer == null) {
            this.startingPlayer = this.players[0];
        }
        if (this.startingPlayer.isHuman()) {
            YanivGameActivity.thisActivity.showHideCont(4);
        }
        HandControl.setCurrentPlayer(this.startingPlayer);
        if (inHand) {
            this.hand.continueHand();
        } else {
            startHand();
        }
    }

    public void startHand() {
        if (this.previousWinner != null) {
            this.startingPlayer = this.previousWinner;
        } else {
            this.startingPlayer = this.players[new Random().nextInt(4)];
        }
        this.hand = new HandControl(this.startingPlayer, this.players);
        for (int i = 0; i < this.preferences.getNumberOfPlayers(); i++) {
            this.players[i].reset();
        }
        YanivGameActivity.thisActivity.showHideYaniv(4);
        YanivGameActivity.thisActivity.showHideRightTap(4);
        YanivGameActivity.thisActivity.showHideCont(4);
        this.hand.startHand();
    }

    public Player[] getPlayers() {
        return this.players;
    }

    public Player getPlayer(int i) {
        return this.players[i];
    }

    public void setPreviousWinner(Player previousWinner2) {
        this.previousWinner = previousWinner2;
    }

    public String getCrashData() {
        String errorMessage = String.valueOf("") + Deck.getDeck().getCrashData();
        if (this.hand != null) {
            errorMessage = String.valueOf(errorMessage) + HandControl.getCrashData();
        }
        for (int i = 0; i < this.preferences.getNumberOfPlayers(); i++) {
            if (this.players[i] != null) {
                errorMessage = String.valueOf(errorMessage) + this.players[i].getCrashData();
            }
        }
        if (this.startingPlayer != null) {
            return String.valueOf(errorMessage) + "<BR>Starting Player = " + this.startingPlayer.getPlayerName();
        }
        return errorMessage;
    }
}
