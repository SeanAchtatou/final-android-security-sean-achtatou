package HandControl;

import DeckControl.DiscardsCompareRules;

public class PlayerHandAutomatedLevel2 extends PlayerHandAutomated {
    private static final long serialVersionUID = 820257210496602389L;

    public PlayerHandAutomatedLevel2() {
        this.discardsCompareRules = new DiscardsCompareRules(true, true, false);
    }

    /* access modifiers changed from: protected */
    public PlayerHandAutomatedLevel2 clone() {
        PlayerHandAutomatedLevel2 newHand = new PlayerHandAutomatedLevel2();
        copyCards(newHand);
        return newHand;
    }

    /* access modifiers changed from: protected */
    public boolean shouldLookAhead() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean shouldLookAtIntermediateValue() {
        return true;
    }
}
