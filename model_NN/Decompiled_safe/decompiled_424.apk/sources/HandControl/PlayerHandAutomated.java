package HandControl;

import DeckControl.Card;
import DeckControl.Discards;
import DeckControl.DiscardsCompareRules;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import ui.YanivGameActivity;

public abstract class PlayerHandAutomated extends PlayerHand implements Serializable {
    private static final long serialVersionUID = 700262766892402801L;
    protected int discardValue = 0;
    private int discardedRank = -1;
    protected DiscardsCompareRules discardsCompareRules;
    private int numberOfCardsDiscarded = 0;
    protected int whereToTakeFrom;
    protected int whichDiscardToTake;

    /* access modifiers changed from: protected */
    public abstract PlayerHandAutomated clone();

    /* access modifiers changed from: protected */
    public abstract boolean shouldLookAhead();

    /* access modifiers changed from: protected */
    public abstract boolean shouldLookAtIntermediateValue();

    private Discards findStraightFlush() {
        Discards newList = new Discards();
        Discards bestList = new Discards();
        int cardPoint = 0;
        if (this.cardCount >= 3) {
            addAceHighs();
            ArrayList<Card> sortedList = sortBySuitAndRank(this.cards);
            for (int suit = 0; suit <= 3; suit++) {
                switch (this.cardsPerSuit[suit] + Math.max(this.jokersCount, 1)) {
                    case 0:
                        break;
                    case 1:
                        cardPoint = (cardPoint + 1) - Math.max(this.jokersCount, 1);
                        break;
                    case 2:
                        cardPoint = (cardPoint + 2) - Math.max(this.jokersCount, 1);
                        break;
                    default:
                        Card currentCard = sortedList.get(cardPoint);
                        newList.add(currentCard);
                        cardPoint++;
                        int cardInSuit = 1;
                        boolean previousCardWasJoker = false;
                        int remainingJokers = this.jokersCount;
                        while (cardPoint < sortedList.size()) {
                            Card nextCard = sortedList.get(cardPoint);
                            if (!nextCard.isJoker) {
                                if (!nextCard.immediatelyFollows(currentCard)) {
                                    if (remainingJokers == 0) {
                                        if (newList.getSize() >= 3 && newList.getTotalCardValue() > bestList.getTotalCardValue()) {
                                            Discards temp = bestList;
                                            bestList = newList;
                                            newList = temp;
                                        }
                                        newList.clear();
                                        previousCardWasJoker = false;
                                        if ((this.cardsPerSuit[suit] - cardInSuit) + 1 < 3) {
                                            break;
                                        }
                                    } else {
                                        previousCardWasJoker = true;
                                    }
                                }
                                if (!previousCardWasJoker) {
                                    newList.add(nextCard);
                                    currentCard = nextCard;
                                    cardInSuit++;
                                    cardPoint++;
                                } else {
                                    Card tempCard = (Card) this.jokers.get(remainingJokers - 1);
                                    tempCard.setValuesForJoker(currentCard.getCardSuit(), currentCard.getCardRank() - 1);
                                    newList.add(tempCard);
                                    remainingJokers--;
                                    previousCardWasJoker = false;
                                    currentCard = tempCard;
                                }
                            } else {
                                cardInSuit++;
                                cardPoint++;
                            }
                        }
                        break;
                }
            }
            if (newList.getTotalCardValue() > bestList.getTotalCardValue()) {
                bestList = newList;
            }
            removeAceHighs();
        }
        if (bestList.getSize() < 3) {
            bestList = null;
        } else if (bestList.getNonJokerCount() < 2) {
            bestList = null;
        }
        Iterator it = this.jokers.iterator();
        while (it.hasNext()) {
            ((Card) it.next()).resetJoker();
        }
        return bestList;
    }

    private Discards findSameRanks(int doNotDiscardRank) {
        ArrayList<Card> sortedList = sortByRank(this.cards);
        Discards bestList = new Discards();
        Discards newList = new Discards();
        int currentRank = -1;
        Iterator<Card> it = sortedList.iterator();
        while (it.hasNext()) {
            Card nextCard = it.next();
            if (nextCard.getCardRank() != doNotDiscardRank && !nextCard.isJoker) {
                if (nextCard.getCardRank() != currentRank) {
                    if (newList.getTotalCardValue() > bestList.getTotalCardValue()) {
                        Discards temp = bestList;
                        bestList = newList;
                        newList = temp;
                    }
                    newList.clear();
                    currentRank = nextCard.getCardRank();
                }
                newList.add(nextCard);
            }
        }
        if (newList.getTotalCardValue() > bestList.getTotalCardValue()) {
            return newList;
        }
        return bestList;
    }

    /* access modifiers changed from: protected */
    public void decideWhatToPickUp() {
        this.whereToTakeFrom = 1;
        int cardValue = this.deck.firstAvailableDiscard().getCardValue();
        this.discardValue = cardValue;
        if (cardValue <= 3) {
            this.whichDiscardToTake = 0;
            this.whereToTakeFrom = 2;
        }
        if (this.deck.availableDiscardSize() == 2) {
            int cardValue2 = this.deck.secondAvailableDiscard().getCardValue();
            this.discardValue = cardValue2;
            if (cardValue2 <= 3 && this.deck.secondAvailableDiscard().getCardValue() < this.deck.firstAvailableDiscard().getCardValue()) {
                this.whichDiscardToTake = 1;
                this.whereToTakeFrom = 2;
            }
        }
    }

    public TurnDetails calculateTurn(boolean withPickups) {
        Discards bestList;
        Card[] availableDiscards = {this.deck.firstAvailableDiscard(), this.deck.secondAvailableDiscard()};
        this.whereToTakeFrom = 0;
        if (!shouldLookAhead()) {
            withPickups = false;
        }
        boolean allSameRank = true;
        if (this.cardCount > 1) {
            int rank = ((Card) this.cards.get(0)).getCardRank();
            int i = 0;
            while (true) {
                if (i >= this.cardCount) {
                    break;
                } else if (((Card) this.cards.get(i)).getCardRank() != rank) {
                    allSameRank = false;
                    break;
                } else {
                    i++;
                }
            }
        }
        if (allSameRank) {
            bestList = new Discards();
            bestList.addAll(this);
            if (withPickups) {
                decideWhatToPickUp();
            }
        } else {
            bestList = findStraightFlush();
            if (bestList == null || bestList.getSize() != this.cardCount) {
                if (withPickups) {
                    if (bestList != null) {
                        PlayerHandAutomated tempHand = clone();
                        tempHand.removeCards(bestList.getCardsToDiscard());
                        bestList.setIntermediateHandValue(tempHand.handValue);
                        tempHand.removeCards(tempHand.calculateTurn(false).cardsToDiscard);
                        bestList.setFinalHandValue(tempHand.handValue);
                        bestList.setFinalCardCount(tempHand.cardCount + 2);
                    }
                    if (bestList != null) {
                        PlayerHandAutomated tempHand2 = clone();
                        Discards newList = new Discards();
                        newList.addAll(bestList);
                        tempHand2.removeCards(bestList.getCardsToDiscard());
                        newList.setIntermediateHandValue(tempHand2.handValue);
                        for (int i2 = 0; i2 < this.deck.availableDiscardSize(); i2++) {
                            tempHand2.addCard(availableDiscards[i2], false);
                            tempHand2.removeCards(tempHand2.calculateTurn(false).cardsToDiscard);
                            newList.setFinalHandValue(tempHand2.handValue);
                            newList.setFinalCardCount(tempHand2.cardCount + 1);
                            if (newList.getSize() >= 3 && newList.isBetterThan(bestList, this.discardsCompareRules)) {
                                bestList = newList;
                                this.whereToTakeFrom = 2;
                                this.whichDiscardToTake = i2;
                            }
                        }
                    }
                    for (int i3 = 0; i3 < this.deck.availableDiscardSize(); i3++) {
                        PlayerHandAutomated tempHand3 = clone();
                        tempHand3.addCard(availableDiscards[i3], false);
                        Discards newList2 = tempHand3.findStraightFlush();
                        if (newList2 != null && (bestList == null || newList2.getSize() > bestList.getSize())) {
                            tempHand3.removeCards(newList2.getCardsToDiscard());
                            TurnDetails tempDiscardList = tempHand3.calculateTurn(false);
                            tempHand3.removeCards(tempDiscardList.cardsToDiscard);
                            newList2.setFinalHandValue(tempHand3.handValue);
                            newList2.setFinalCardCount(tempHand3.cardCount + 1);
                            tempHand3.addCards(newList2.getCardsToDiscard(), false);
                            newList2.setIntermediateHandValue(tempHand3.handValue);
                            newList2.clear();
                            newList2.addAll(tempDiscardList);
                            if (bestList == null || newList2.isBetterThan(bestList, this.discardsCompareRules)) {
                                bestList = newList2;
                                this.whereToTakeFrom = 2;
                                this.whichDiscardToTake = i3;
                            }
                        }
                    }
                }
                if (!withPickups) {
                    Discards newList3 = clone().findSameRanks(0);
                    if (newList3.isBetterThan(bestList, this.discardsCompareRules)) {
                        bestList = newList3;
                    }
                } else {
                    PlayerHandAutomated tempHand4 = clone();
                    Discards newList4 = tempHand4.findSameRanks(0);
                    tempHand4.removeCards(newList4.getCardsToDiscard());
                    newList4.setIntermediateHandValue(tempHand4.handValue);
                    tempHand4.removeCards(tempHand4.calculateTurn(false).cardsToDiscard);
                    newList4.setFinalHandValue(tempHand4.handValue);
                    newList4.setFinalCardCount(tempHand4.cardCount + 2);
                    if (newList4.isBetterThan(bestList, this.discardsCompareRules)) {
                        bestList = newList4;
                    }
                    for (int i4 = 0; i4 < this.deck.availableDiscardSize(); i4++) {
                        PlayerHandAutomated tempHand5 = clone();
                        Discards newList5 = tempHand5.findSameRanks(availableDiscards[i4].getCardRank());
                        tempHand5.removeCards(newList5.getCardsToDiscard());
                        newList5.setIntermediateHandValue(tempHand5.handValue);
                        tempHand5.addCard(availableDiscards[i4], false);
                        tempHand5.removeCards(tempHand5.calculateTurn(false).cardsToDiscard);
                        newList5.setFinalHandValue(tempHand5.handValue);
                        newList5.setFinalCardCount(tempHand5.cardCount + 1);
                        if (newList5.isBetterThan(bestList, this.discardsCompareRules)) {
                            bestList = newList5;
                            this.whereToTakeFrom = 2;
                            this.whichDiscardToTake = i4;
                        }
                    }
                }
            } else if (withPickups) {
                decideWhatToPickUp();
            }
        }
        if (this.whereToTakeFrom == 0) {
            decideWhatToPickUp();
        }
        TurnDetails turnDetails = new TurnDetails();
        turnDetails.cardsToDiscard = bestList.getCardsToDiscard();
        turnDetails.whereToTakeFrom = this.whereToTakeFrom;
        turnDetails.whichDiscardToTake = this.whichDiscardToTake;
        return turnDetails;
    }

    public boolean canYaniv() {
        return this.handValue <= YanivGameActivity.settings.getYanivAmount();
    }

    public Card[] getCardsAsArray() {
        Card[] cardsArray = new Card[this.cardCount];
        this.cards.toArray(cardsArray);
        return cardsArray;
    }

    public int getNumberOfCards() {
        return this.cardCount;
    }

    public void setNumberOfCardsDiscarded(int numberOfCardsDiscarded2) {
        this.numberOfCardsDiscarded = numberOfCardsDiscarded2;
    }

    public int getNumberOfCardsDiscarded() {
        return this.numberOfCardsDiscarded;
    }

    public ArrayList<Card> getCards() {
        return this.cards;
    }

    public void setDiscardedRank(int discardedRank2) {
        this.discardedRank = discardedRank2;
    }

    public int getDiscardedRank() {
        return this.discardedRank;
    }
}
