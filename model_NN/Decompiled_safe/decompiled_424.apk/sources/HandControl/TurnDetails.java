package HandControl;

import DeckControl.Card;
import java.io.Serializable;
import java.util.ArrayList;

public class TurnDetails implements Serializable {
    private static final long serialVersionUID = 1;
    public ArrayList<Card> cardsToDiscard;
    public int whereToTakeFrom;
    public int whichDiscardToTake;
    public boolean yaniv = false;
}
