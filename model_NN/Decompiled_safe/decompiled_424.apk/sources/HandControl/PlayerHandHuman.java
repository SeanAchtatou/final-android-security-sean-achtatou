package HandControl;

import DeckControl.Card;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import ui.YanivGameActivity;
import util.ScoringForScoreloop;

public class PlayerHandHuman extends PlayerHand implements Serializable {
    private static final long serialVersionUID = 700262766892402801L;
    public static final int sortHand = 1;
    public static final int sortSelectedCards = 2;
    protected int[] cardsPerSuit = new int[4];
    private int discardedRank = -1;
    private int numberOfCardsDiscarded = 0;

    public PlayerHandHuman clone() {
        PlayerHandHuman newHand = new PlayerHandHuman();
        copyCards(newHand);
        return newHand;
    }

    public void selectDeselectCard(int cardPoint) {
        Card card = (Card) this.cards.get(cardPoint);
        if (!this.selectedCards.contains(card)) {
            this.selectedCount++;
            this.selectedCards.add(card);
            if (card.isJoker) {
                this.jokersSelectedCount++;
                this.selectedJokers.add(card);
                return;
            }
            return;
        }
        this.selectedCount--;
        this.selectedCards.remove(card);
        if (card.isJoker) {
            this.jokersSelectedCount--;
        }
    }

    public Iterator<Card> iterator() {
        return this.cards.iterator();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    public boolean checkSelectedCards() {
        boolean valid;
        boolean isStraightFlush = false;
        this.discardedRank = -1;
        switch (this.selectedCount) {
            case 0:
                valid = false;
                break;
            case 1:
                valid = true;
                break;
            case 2:
                if (this.jokersSelectedCount <= 0) {
                    if (((Card) this.selectedCards.get(0)).getCardRank() != ((Card) this.selectedCards.get(1)).getCardRank()) {
                        valid = false;
                        break;
                    } else {
                        valid = true;
                        break;
                    }
                } else {
                    valid = true;
                    break;
                }
            default:
                int cardSuit = -1;
                int[] cardsPerSuit2 = new int[4];
                int numberOfDifferentSuits = 0;
                int[] cardsPerRank = new int[15];
                int numberOfDifferentRanks = 0;
                BitSet[] cardsBitsPerSuit = {new BitSet(16), new BitSet(16), new BitSet(16), new BitSet(16)};
                for (int i = 0; i < this.selectedCount; i++) {
                    Card card = (Card) this.selectedCards.get(i);
                    if (!card.isJoker) {
                        cardSuit = card.getCardSuit();
                        int cardRank = card.getCardRank();
                        cardsPerSuit2[cardSuit] = cardsPerSuit2[cardSuit] + 1;
                        if (cardsPerSuit2[cardSuit] == 1) {
                            numberOfDifferentSuits++;
                        }
                        cardsBitsPerSuit[card.getCardSuit()].set(cardRank);
                        cardsPerRank[cardRank] = cardsPerRank[cardRank] + 1;
                        if (cardsPerRank[cardRank] == 1) {
                            numberOfDifferentRanks++;
                        }
                    }
                }
                if (numberOfDifferentRanks != 1) {
                    if (numberOfDifferentSuits == 1 && numberOfDifferentRanks == this.selectedCount - this.jokersSelectedCount) {
                        valid = checkStraightFlush(cardsBitsPerSuit[cardSuit]);
                        if (!valid) {
                            if (cardsBitsPerSuit[cardSuit].get(1)) {
                                cardsBitsPerSuit[cardSuit].set(1, false);
                                cardsBitsPerSuit[cardSuit].set(14, true);
                                valid = checkStraightFlush(cardsBitsPerSuit[cardSuit]);
                            } else {
                                valid = false;
                            }
                        }
                    } else {
                        valid = false;
                    }
                    isStraightFlush = valid;
                    break;
                } else {
                    valid = true;
                    break;
                }
                break;
        }
        if (valid) {
            if (isStraightFlush) {
                this.selectedCards = sortByRank(this.selectedCards);
                ScoringForScoreloop.addStrFlushScore(this.selectedCards.size());
            } else {
                ScoringForScoreloop.addSameRankScore(this.selectedCards.size());
                this.discardedRank = ((Card) this.selectedCards.get(0)).getCardRank();
            }
        }
        return valid;
    }

    private boolean checkStraightFlush(BitSet cardsBits) {
        int lowestBit = cardsBits.nextSetBit(0);
        int highestBit = cardsBits.nextClearBit(lowestBit);
        if (highestBit - lowestBit == this.selectedCount) {
            return true;
        }
        int i = cardsBits.nextSetBit(0);
        while (i >= 0) {
            highestBit = i + 1;
            i = cardsBits.nextSetBit(i + 1);
        }
        if (highestBit - lowestBit > this.selectedCount) {
            return false;
        }
        int nextJoker = 0;
        for (int bit = lowestBit + 1; bit < highestBit; bit++) {
            if (!cardsBits.get(bit)) {
                ((Card) this.selectedJokers.get(nextJoker)).setRankForJoker(bit);
                nextJoker++;
            }
        }
        if (nextJoker == this.jokersSelectedCount) {
            return true;
        }
        int remainingJokers = this.jokersSelectedCount - nextJoker;
        if (lowestBit >= remainingJokers) {
            int bit2 = lowestBit - 1;
            while (remainingJokers > 0) {
                ((Card) this.selectedJokers.get(nextJoker)).setRankForJoker(bit2);
                bit2--;
                remainingJokers--;
            }
            return true;
        }
        int bit3 = highestBit;
        while (remainingJokers > 0) {
            ((Card) this.selectedJokers.get(nextJoker)).setRankForJoker(bit3);
            bit3++;
            remainingJokers--;
        }
        return true;
    }

    public boolean canYaniv() {
        return this.handValue <= YanivGameActivity.settings.getYanivAmount();
    }

    public ArrayList<Card> getSelectedCards() {
        return this.selectedCards;
    }

    public Card[] getCardsAsArray() {
        Card[] cardsArray = new Card[this.cardCount];
        this.cards.toArray(cardsArray);
        return cardsArray;
    }

    public int getNumberOfCards() {
        return this.cardCount;
    }

    public void setNumberOfCardsDiscarded(int numberOfCardsDiscarded2) {
        this.numberOfCardsDiscarded = numberOfCardsDiscarded2;
    }

    public int getNumberOfCardsDiscarded() {
        return this.numberOfCardsDiscarded;
    }

    public ArrayList<Card> getCards() {
        return this.cards;
    }

    public void setDiscardedRank(int discardedRank2) {
        this.discardedRank = discardedRank2;
    }

    public int getDiscardedRank() {
        return this.discardedRank;
    }

    private ArrayList<Card> sortByRankDescending(int whatToSort) {
        ArrayList<Card> sortByRank = new ArrayList<>();
        if (whatToSort == 1) {
            sortByRank.addAll(this.cards);
        } else {
            sortByRank.addAll(this.selectedCards);
        }
        Collections.sort(sortByRank, new Comparator<Card>() {
            public int compare(Card c1, Card c2) {
                return c1.getCardRank() - c2.getCardRank();
            }
        });
        return sortByRank;
    }
}
