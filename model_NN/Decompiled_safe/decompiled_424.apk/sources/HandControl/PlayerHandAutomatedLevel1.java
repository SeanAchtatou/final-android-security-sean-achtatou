package HandControl;

import DeckControl.DiscardsCompareRules;

public class PlayerHandAutomatedLevel1 extends PlayerHandAutomated {
    private static final long serialVersionUID = 820257210496602389L;

    public PlayerHandAutomatedLevel1() {
        this.discardsCompareRules = new DiscardsCompareRules(false, false, false);
    }

    /* access modifiers changed from: protected */
    public PlayerHandAutomatedLevel1 clone() {
        PlayerHandAutomatedLevel1 newHand = new PlayerHandAutomatedLevel1();
        copyCards(newHand);
        return newHand;
    }

    /* access modifiers changed from: protected */
    public boolean shouldLookAhead() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean shouldLookAtIntermediateValue() {
        return false;
    }
}
