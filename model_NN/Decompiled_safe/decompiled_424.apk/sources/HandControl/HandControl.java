package HandControl;

import DeckControl.Card;
import DeckControl.Deck;
import PlayerControl.Player;
import java.io.Serializable;
import ui.YanivGameActivity;

public class HandControl implements Serializable {
    private static Player currentPlayer = null;
    private static final long serialVersionUID = -7913710083724945295L;
    private Deck deck = Deck.getDeck();
    private Player[] players;
    private Player previousWinner;

    private HandControl() {
    }

    public HandControl(Player previousWinner2, Player[] players2) {
        if (previousWinner2 == null) {
            this.previousWinner = players2[0];
        } else {
            this.previousWinner = previousWinner2;
        }
        this.players = players2;
    }

    public void startHand() {
        dealCards();
        currentPlayer = this.previousWinner;
        currentPlayer.awaitTurn();
    }

    public void continueHand() {
        currentPlayer.awaitTurn();
    }

    public void dealCards() {
        Card nextCard;
        this.deck.initialiseDeck();
        int i = 0;
        while (i < 4) {
            try {
                if (this.players[i] == null) {
                    break;
                }
                for (int j = 0; j < 5 && (nextCard = this.deck.nextCard()) != null; j++) {
                    this.players[i].addCard(nextCard, false);
                }
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
        this.deck.setDiscard(this.deck.nextCard());
        YanivGameActivity.thisActivity.redrawAllCards(0);
    }

    public static void setCurrentPlayer(Player in_currentPlayer) {
        currentPlayer = in_currentPlayer;
    }

    public static Player getCurrentPlayer() {
        return currentPlayer;
    }

    public static String getCrashData() {
        if (currentPlayer != null) {
            return String.valueOf("") + "Current Player: " + currentPlayer.getPlayerName() + "<BR>";
        }
        return "";
    }
}
