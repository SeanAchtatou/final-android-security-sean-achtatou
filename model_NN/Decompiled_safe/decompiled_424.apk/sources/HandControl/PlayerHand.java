package HandControl;

import DeckControl.Card;
import DeckControl.Deck;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import ui.YanivGameActivity;

public abstract class PlayerHand implements Serializable {
    private static final long serialVersionUID = 700262766892402801L;
    public static final int sortHand = 1;
    public static final int sortSelectedCards = 2;
    protected ArrayList<Card> aceHighs = new ArrayList<>();
    protected int cardCount = 0;
    protected ArrayList<Card> cards = new ArrayList<>();
    protected int[] cardsPerSuit = new int[4];
    protected Deck deck = Deck.getDeck();
    private int discardedRank = -1;
    public int handValue = 0;
    protected ArrayList<Card> jokers = new ArrayList<>();
    protected int jokersCount = 0;
    protected int jokersSelectedCount = 0;
    protected HashSet<Card> knownCards = new HashSet<>();
    private int numberOfCardsDiscarded = 0;
    protected int numberOfKnownCards = 0;
    protected ArrayList<Card> selectedCards = new ArrayList<>();
    protected int selectedCount = 0;
    protected ArrayList<Card> selectedJokers = new ArrayList<>();
    protected int valueOfKnownCards = 0;

    /* access modifiers changed from: protected */
    public abstract PlayerHand clone();

    /* access modifiers changed from: protected */
    public void copyCards(PlayerHand newHand) {
        for (int i = 0; i < this.cardCount; i++) {
            newHand.addCard(this.cards.get(i), false);
        }
    }

    public void addCard(Card newCard, boolean known) {
        int cardSuit = newCard.getCardSuit();
        int cardRank = newCard.getCardRank();
        this.cards.add(newCard);
        this.handValue += newCard.getCardValue();
        this.cardCount++;
        if (newCard.isJoker) {
            this.jokersCount++;
            this.jokers.add(newCard);
        }
        int[] iArr = this.cardsPerSuit;
        iArr[cardSuit] = iArr[cardSuit] + 1;
        if (known) {
            this.numberOfKnownCards++;
            this.valueOfKnownCards += newCard.getCardValue();
            this.knownCards.add(newCard);
        }
        if (cardRank == 1 && !newCard.isJoker) {
            this.aceHighs.add(newCard.equivalentAceHigh);
        }
    }

    public void addCards(ArrayList<Card> newCards, boolean known) {
        for (int i = 0; i < newCards.size(); i++) {
            addCard(newCards.get(i), known);
        }
    }

    public void removeCard(Card inOldCard) {
        Card oldCard = inOldCard;
        if (oldCard.getCardRank() == 14) {
            this.cards.remove(inOldCard);
            this.aceHighs.remove(inOldCard);
            this.aceHighs.trimToSize();
            oldCard = inOldCard.equivalentAceLow;
        } else {
            this.aceHighs.remove(oldCard.equivalentAceHigh);
        }
        int cardSuit = oldCard.getCardSuit();
        this.cards.remove(oldCard);
        this.handValue -= oldCard.getCardValue();
        this.cardCount--;
        if (oldCard.isJoker) {
            this.jokersCount--;
        }
        int[] iArr = this.cardsPerSuit;
        iArr[cardSuit] = iArr[cardSuit] - 1;
        if (this.knownCards.remove(oldCard)) {
            this.numberOfKnownCards--;
            this.valueOfKnownCards -= oldCard.getCardValue();
        }
        this.cards.trimToSize();
    }

    public void removeCards(ArrayList<Card> oldCards) {
        for (int i = 0; i < oldCards.size(); i++) {
            removeCard(oldCards.get(i));
        }
    }

    public void addAceHighs() {
        Iterator<Card> it = this.aceHighs.iterator();
        while (it.hasNext()) {
            Card nextCard = it.next();
            this.cards.add(nextCard);
            int[] iArr = this.cardsPerSuit;
            int cardSuit = nextCard.getCardSuit();
            iArr[cardSuit] = iArr[cardSuit] + 1;
        }
    }

    public void removeAceHighs() {
        int numberOfAces = this.aceHighs.size();
        for (int i = 0; i < numberOfAces; i++) {
            Card nextCard = this.aceHighs.get(i);
            this.cards.remove(nextCard);
            int[] iArr = this.cardsPerSuit;
            int cardSuit = nextCard.getCardSuit();
            iArr[cardSuit] = iArr[cardSuit] - 1;
        }
    }

    public Iterator<Card> iterator() {
        return this.cards.iterator();
    }

    public void discardAndPickupFromDeck() {
        addCard(this.deck.nextCard(), false);
        discard();
    }

    public Card discardAndPickupFromDiscard(int whichDiscard) {
        Card discardedCard = this.deck.getAvailableDiscard(whichDiscard);
        addCard(discardedCard, true);
        discard();
        return discardedCard;
    }

    public void discardWithoutPickup() {
        discard();
    }

    /* access modifiers changed from: protected */
    public void discard() {
        this.numberOfCardsDiscarded = this.selectedCards.size();
        removeCards(this.selectedCards);
        this.deck.discard(this.selectedCards);
        clearSelectedCards();
    }

    public void clearSelectedCards() {
        this.selectedCards.clear();
        this.selectedJokers.clear();
        this.jokersSelectedCount = 0;
        this.selectedCount = 0;
    }

    /* access modifiers changed from: protected */
    public ArrayList<Card> sortByRank(ArrayList<Card> whatToSort) {
        ArrayList<Card> sortByRank = new ArrayList<>();
        sortByRank.addAll(whatToSort);
        Collections.sort(sortByRank, new Comparator<Card>() {
            public int compare(Card c1, Card c2) {
                return c2.getCardRank() - c1.getCardRank();
            }
        });
        return sortByRank;
    }

    /* access modifiers changed from: protected */
    public ArrayList<Card> sortBySuitAndRank(ArrayList<Card> whatToSort) {
        ArrayList<Card> sortBySuitAndRank = new ArrayList<>();
        sortBySuitAndRank.addAll(whatToSort);
        Collections.sort(sortBySuitAndRank, new Comparator<Card>() {
            public int compare(Card c1, Card c2) {
                return (((c1.getCardSuit() - c2.getCardSuit()) * 100) + c2.getCardRank()) - c1.getCardRank();
            }
        });
        return sortBySuitAndRank;
    }

    public boolean canYaniv() {
        return this.handValue <= YanivGameActivity.settings.getYanivAmount();
    }

    public void sortSelfByRank() {
        Collections.sort(this.cards, new Comparator<Card>() {
            public int compare(Card c1, Card c2) {
                return c2.getCardRank() - c1.getCardRank();
            }
        });
    }

    public void selectAll(ArrayList<Card> inCards) {
        this.selectedCards.clear();
        this.selectedCards.addAll(inCards);
        this.selectedCount = this.selectedCards.size();
        Iterator<Card> it = this.selectedCards.iterator();
        while (it.hasNext()) {
            if (it.next().isJoker) {
                this.jokersSelectedCount++;
            }
        }
    }

    public ArrayList<Card> getSelectedCards() {
        return this.selectedCards;
    }

    public boolean knowAllCards() {
        return this.numberOfKnownCards == this.cardCount;
    }

    public int getValueOfKnownCards() {
        return this.valueOfKnownCards;
    }

    public Card[] getCardsAsArray() {
        Card[] cardsArray = new Card[this.cardCount];
        this.cards.toArray(cardsArray);
        return cardsArray;
    }

    public String toString() {
        String handAsString = Integer.toString(this.cards.size());
        Iterator<Card> it = this.cards.iterator();
        while (it.hasNext()) {
            handAsString = String.valueOf(handAsString) + it.next().toShortString();
        }
        return handAsString;
    }

    public int getNumberOfCards() {
        return this.cardCount;
    }

    public void setNumberOfCardsDiscarded(int numberOfCardsDiscarded2) {
        this.numberOfCardsDiscarded = numberOfCardsDiscarded2;
    }

    public int getNumberOfCardsDiscarded() {
        return this.numberOfCardsDiscarded;
    }

    public ArrayList<Card> getCards() {
        return this.cards;
    }

    public void setDiscardedRank(int discardedRank2) {
        this.discardedRank = discardedRank2;
    }

    public int getDiscardedRank() {
        return this.discardedRank;
    }

    public String getCrashData() {
        String errorMessage = String.valueOf("") + "Cards: ";
        Iterator<Card> it = this.cards.iterator();
        while (it.hasNext()) {
            errorMessage = String.valueOf(errorMessage) + it.next().toString() + " ";
        }
        return String.valueOf(errorMessage) + "<BR>";
    }
}
