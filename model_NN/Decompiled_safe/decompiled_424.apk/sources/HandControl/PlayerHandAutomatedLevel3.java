package HandControl;

import DeckControl.DiscardsCompareRules;

public class PlayerHandAutomatedLevel3 extends PlayerHandAutomated {
    private static final long serialVersionUID = 820257210496602389L;

    public PlayerHandAutomatedLevel3() {
        this.discardsCompareRules = new DiscardsCompareRules(true, true, true);
    }

    /* access modifiers changed from: protected */
    public boolean shouldLookAhead() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean shouldLookAtIntermediateValue() {
        return true;
    }

    public PlayerHandAutomatedLevel3 clone() {
        PlayerHandAutomatedLevel3 newHand = new PlayerHandAutomatedLevel3();
        copyCards(newHand);
        return newHand;
    }

    /* access modifiers changed from: protected */
    public void decideWhatToPickUp() {
        this.whereToTakeFrom = 1;
        int cardValue = this.deck.firstAvailableDiscard().getCardValue();
        this.discardValue = cardValue;
        if (cardValue <= 2 || this.discardValue <= this.deck.firstAvailableDiscard().getCardValue() / 2) {
            this.whichDiscardToTake = 0;
            this.whereToTakeFrom = 2;
        }
        if (this.deck.availableDiscardSize() == 2) {
            int cardValue2 = this.deck.secondAvailableDiscard().getCardValue();
            this.discardValue = cardValue2;
            if ((cardValue2 <= 2 || this.discardValue <= this.deck.firstAvailableDiscard().getCardValue() / 2) && this.deck.secondAvailableDiscard().getCardValue() < this.deck.firstAvailableDiscard().getCardValue()) {
                this.whichDiscardToTake = 1;
                this.whereToTakeFrom = 2;
            }
        }
        if (this.whereToTakeFrom == 2 && this.deck.averageRemainingCardsValues() < this.discardValue) {
            this.whereToTakeFrom = 1;
        }
    }
}
