package PlayerControl;

import HandControl.PlayerHandAutomated;
import HandControl.TurnDetails;
import android.os.Handler;
import il.co.anykey.games.yaniv.lite.R;
import java.io.Serializable;
import ui.YanivGameActivity;
import util.MessageBuilder;

public abstract class AutomatedPlayer extends Player implements Serializable {
    private static transient Handler handler = null;
    private static final long serialVersionUID = 1;
    private transient Runnable startNextPlayerTurn;

    public abstract void reset();

    /* access modifiers changed from: protected */
    public abstract boolean shouldYaniv(int i);

    public AutomatedPlayer(int playerId, String playerName) {
        super(2, playerId, playerName);
    }

    public void pauseTurn() {
        if (handler != null) {
            handler.removeCallbacks(this.startNextPlayerTurn);
        }
    }

    public void awaitTurn() {
        YanivGameActivity.thisActivity.showHideCont(0);
        if (YanivGameActivity.settings.getPauseTimeBetweenTurns() > 0) {
            this.startNextPlayerTurn = new Runnable() {
                public void run() {
                    AutomatedPlayer.this.doTurn();
                }
            };
            if (handler == null) {
                handler = new Handler();
            } else {
                handler.removeCallbacks(this.startNextPlayerTurn);
            }
            handler.postDelayed(this.startNextPlayerTurn, (long) ((YanivGameActivity.settings.getPauseTimeBetweenTurns() * 1000) + YanivGameActivity.settings.getCardAnimationTime()));
            this.startedTurn = false;
        }
    }

    public void finishTurn() {
        this.playerHand.sortSelfByRank();
        YanivGameActivity.thisActivity.showHideCont(4);
        super.finishTurn();
    }

    public void doTurn() {
        this.startedTurn = true;
        if (this.startNextPlayerTurn != null) {
            handler.removeCallbacks(this.startNextPlayerTurn);
        }
        if (!this.playerHand.canYaniv() || !shouldYaniv(this.playerHand.handValue)) {
            TurnDetails turnDetails = ((PlayerHandAutomated) this.playerHand).calculateTurn(true);
            this.playerHand.selectAll(turnDetails.cardsToDiscard);
            String discards = "";
            for (int i = 0; i < turnDetails.cardsToDiscard.size(); i++) {
                discards = String.valueOf(discards) + turnDetails.cardsToDiscard.get(i).toString() + ", ";
            }
            String discards2 = discards.substring(0, discards.length() - 2);
            switch (turnDetails.whereToTakeFrom) {
                case 1:
                    YanivGameActivity.thisActivity.displayPlayerAction(MessageBuilder.buildMessage(YanivGameActivity.thisActivity, R.string.message_pickedUpFromDeck, this.playerName, discards2));
                    discardAndPickupFromDeck();
                    return;
                case 2:
                    YanivGameActivity.thisActivity.displayPlayerAction(MessageBuilder.buildMessage(YanivGameActivity.thisActivity, R.string.message_pickedUpFromDiscards, this.playerName, discards2, this.deck.getAvailableDiscard(turnDetails.whichDiscardToTake).toString()));
                    discardAndPickupFromDiscard(turnDetails.whichDiscardToTake);
                    return;
                default:
                    return;
            }
        } else {
            yaniv();
        }
    }
}
