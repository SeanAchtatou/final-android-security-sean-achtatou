package PlayerControl;

import HandControl.PlayerHandAutomatedLevel2;
import java.io.Serializable;

public class AutomatedPlayerLevel2 extends AutomatedPlayer implements Serializable {
    private static final long serialVersionUID = 1;

    public AutomatedPlayerLevel2(int playerId, String playerName) {
        super(playerId, playerName);
        this.playerHand = new PlayerHandAutomatedLevel2();
    }

    public void reset() {
        this.playerHand = new PlayerHandAutomatedLevel2();
    }

    /* access modifiers changed from: protected */
    public boolean shouldYaniv(int handValue) {
        return true;
    }
}
