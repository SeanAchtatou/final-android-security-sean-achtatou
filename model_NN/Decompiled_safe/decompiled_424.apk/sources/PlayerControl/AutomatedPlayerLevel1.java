package PlayerControl;

import HandControl.PlayerHandAutomatedLevel1;
import java.io.Serializable;

public class AutomatedPlayerLevel1 extends AutomatedPlayer implements Serializable {
    private static final long serialVersionUID = 1;

    public AutomatedPlayerLevel1(int playerId, String playerName) {
        super(playerId, playerName);
        this.playerHand = new PlayerHandAutomatedLevel1();
    }

    public void reset() {
        this.playerHand = new PlayerHandAutomatedLevel1();
    }

    /* access modifiers changed from: protected */
    public boolean shouldYaniv(int handValue) {
        return true;
    }
}
