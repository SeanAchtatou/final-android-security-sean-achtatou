package PlayerControl;

import HandControl.PlayerHandHuman;
import java.io.Serializable;

public class HumanPlayer extends LocalPlayer implements Serializable {
    private static final long serialVersionUID = 1;

    public HumanPlayer(int playerId, String playerName) {
        super(1, playerId, playerName);
        this.playerHand = new PlayerHandHuman();
    }

    public void selectDeselectCard(int cardPoint) {
        ((PlayerHandHuman) this.playerHand).selectDeselectCard(cardPoint);
    }

    public void reset() {
        this.playerHand = new PlayerHandHuman();
    }

    public void discardAndPickupFromDiscard(int whichOne) {
        if (((PlayerHandHuman) this.playerHand).checkSelectedCards()) {
            super.discardAndPickupFromDiscard(whichOne);
        }
    }

    public void discardAndPickupFromDeck() {
        if (((PlayerHandHuman) this.playerHand).checkSelectedCards()) {
            super.discardAndPickupFromDeck();
        }
    }
}
