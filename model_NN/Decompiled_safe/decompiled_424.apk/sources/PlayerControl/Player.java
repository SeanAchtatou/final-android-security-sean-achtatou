package PlayerControl;

import DeckControl.Card;
import DeckControl.Deck;
import HandControl.HandControl;
import HandControl.PlayerHand;
import java.io.Serializable;
import java.util.ArrayList;
import ui.Yaniv;
import ui.YanivGameActivity;
import util.DBAdaptor;

public abstract class Player implements Serializable {
    public static final int bluetoothLocal = 3;
    public static final int bluetoothRemote = 4;
    public static final int computer = 2;
    public static final int human = 1;
    private static final long serialVersionUID = 1;
    protected Deck deck = Deck.getDeck();
    protected Player nextPlayer;
    public PlayerHand playerHand;
    public int playerId;
    public String playerName;
    protected int playerType;
    private boolean previouslyPickedFromDeck = false;
    protected boolean startedTurn = false;

    public abstract void awaitTurn();

    public abstract void doTurn();

    public abstract void reset();

    public Player(int in_PlayerType, int in_PlayerId, String in_playerName) {
        this.playerType = in_PlayerType;
        this.playerId = in_PlayerId;
        this.playerName = in_playerName;
    }

    public boolean hasNotStartedTurn() {
        return !this.startedTurn;
    }

    public void redrawCards(int whichToAnimate, Card cardTaken) {
        YanivGameActivity.thisActivity.redrawCards(this, whichToAnimate, cardTaken);
    }

    public int getPlayerType() {
        return this.playerType;
    }

    public void addCard(Card newCard, boolean known) {
        this.playerHand.addCard(newCard, known);
    }

    public void removeCard(Card newCard) {
        this.playerHand.removeCard(newCard);
    }

    /* access modifiers changed from: protected */
    public void sendMessage(String message) {
    }

    public void sendToOtherPlayers() {
        if (Yaniv.currentGameType == 2) {
            Player nextPlayer2 = this.nextPlayer;
            while (nextPlayer2 != this) {
                if (nextPlayer2.playerType == 4) {
                    nextPlayer2.sendMessage("");
                }
            }
        }
    }

    public void finishTurn() {
        HandControl.setCurrentPlayer(this.nextPlayer);
        sendToOtherPlayers();
        this.nextPlayer.awaitTurn();
    }

    public void removeCards(ArrayList<Card> cards) {
        this.playerHand.removeCards(cards);
    }

    public boolean isHuman() {
        return this.playerType == 1;
    }

    public boolean canYaniv(int handValue) {
        return handValue <= YanivGameActivity.settings.getYanivAmount();
    }

    public int canAssaf(int yanivedAmount) {
        if (this.playerHand.handValue <= yanivedAmount) {
            return this.playerHand.handValue;
        }
        return -1;
    }

    public int getHandValue() {
        return this.playerHand.handValue;
    }

    public void discardAndPickupFromDeck() {
        this.playerHand.discardAndPickupFromDeck();
        redrawCards(1, null);
        this.previouslyPickedFromDeck = true;
        finishTurn();
    }

    public void discardAndPickupFromDiscard(int whichDiscard) {
        redrawCards(2, this.playerHand.discardAndPickupFromDiscard(whichDiscard));
        this.previouslyPickedFromDeck = false;
        finishTurn();
    }

    public boolean mullaSlapDown() {
        if (!this.previouslyPickedFromDeck || this.playerHand.getSelectedCards().get(0).getCardRank() != this.playerHand.getDiscardedRank()) {
            this.playerHand.clearSelectedCards();
            return false;
        }
        this.playerHand.discardWithoutPickup();
        redrawCards(0, null);
        return true;
    }

    public void setNextPlayer(Player player) {
        this.nextPlayer = player;
    }

    public Player getNextPlayer() {
        return this.nextPlayer;
    }

    public String getPlayerName() {
        return this.playerName;
    }

    public void yaniv() {
        ArrayList<Player> assafPlayers = new ArrayList<>();
        ArrayList<Player> losingPlayers = new ArrayList<>();
        int currentYanivAmount = this.playerHand.handValue;
        for (Player nextPlayer2 = this.nextPlayer; nextPlayer2 != this; nextPlayer2 = nextPlayer2.nextPlayer) {
            if (nextPlayer2.playerHand.handValue < currentYanivAmount) {
                losingPlayers.addAll(assafPlayers);
                assafPlayers = new ArrayList<>();
                assafPlayers.add(nextPlayer2);
                currentYanivAmount = nextPlayer2.playerHand.handValue;
            } else if (nextPlayer2.playerHand.handValue == currentYanivAmount) {
                assafPlayers.add(nextPlayer2);
            } else {
                losingPlayers.add(nextPlayer2);
            }
        }
        DBAdaptor dbAdaptor = new DBAdaptor(YanivGameActivity.thisActivity);
        dbAdaptor.open();
        if (assafPlayers.size() != 0) {
            dbAdaptor.addScoresAssaf(assafPlayers, this, losingPlayers);
            YanivGameActivity.thisActivity.declareAssaf(this, assafPlayers);
        } else {
            dbAdaptor.addScoresYaniv(this, losingPlayers);
            YanivGameActivity.thisActivity.declareYaniv(this);
        }
        dbAdaptor.close();
    }

    public int getPlayerId() {
        return this.playerId;
    }

    public String getCrashData() {
        return String.valueOf(String.valueOf("") + this.playerName + "<BR> ") + this.playerHand.getCrashData();
    }
}
