package PlayerControl;

import HandControl.PlayerHandAutomatedLevel3;
import java.io.Serializable;
import ui.YanivGameActivity;

public class AutomatedPlayerLevel3 extends AutomatedPlayer implements Serializable {
    private static final long serialVersionUID = 1;

    public AutomatedPlayerLevel3(int playerId, String playerName) {
        super(playerId, playerName);
        this.playerHand = new PlayerHandAutomatedLevel3();
    }

    public void reset() {
        this.playerHand = new PlayerHandAutomatedLevel3();
    }

    /* access modifiers changed from: protected */
    public boolean shouldYaniv(int handValue) {
        if (!YanivGameActivity.settings.shouldCheckBeforeYaniv()) {
            return true;
        }
        boolean reply = true;
        for (Player next = this.nextPlayer; next != this; next = next.nextPlayer) {
            if (next.playerHand.knowAllCards() && next.playerHand.getValueOfKnownCards() <= handValue) {
                reply = false;
            }
        }
        return reply;
    }
}
