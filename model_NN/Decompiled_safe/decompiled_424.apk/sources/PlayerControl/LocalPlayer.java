package PlayerControl;

import DeckControl.Deck;
import HandControl.PlayerHandHuman;
import java.io.Serializable;
import ui.YanivGameActivity;

public class LocalPlayer extends Player implements Serializable {
    private static final long serialVersionUID = 1;

    public LocalPlayer(int in_PlayerType, int in_PlayerId, String in_playerName) {
        super(in_PlayerType, in_PlayerId, in_playerName);
    }

    public void reset() {
        this.playerHand = new PlayerHandHuman();
    }

    public void doTurn() {
    }

    public void awaitTurn() {
        YanivGameActivity.thisActivity.showHideCont(4);
        if (Deck.getDeck().currentDiscardSize() > 1) {
            YanivGameActivity.thisActivity.showHideRightTap(0);
        }
        if (this.playerHand.canYaniv()) {
            YanivGameActivity.thisActivity.showHideYaniv(0);
        } else {
            YanivGameActivity.thisActivity.showHideYaniv(4);
        }
    }

    public void finishTurn() {
        YanivGameActivity.thisActivity.showHideYaniv(4);
        YanivGameActivity.thisActivity.showHideRightTap(4);
        super.finishTurn();
    }
}
