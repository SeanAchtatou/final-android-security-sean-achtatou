package mc.com.de.learn.findnum;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import mc.com.de.learn.R;
import mc.com.de.learn.inter.SoundFiles;
import mc.com.de.learn.util.RandomCreator;

public class Panel extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener, SoundFiles {
    CanvasThread canvasthread;
    Context context;
    int height = 0;
    private AudioManager mAudioManager;
    private SoundPool mSoundPool;
    private int[] mSoundPoolIds = new int[mSoundIdsAll.length];
    private int mStreamVolume;
    int[] numbers = {R.drawable.n1, R.drawable.n2, R.drawable.n3, R.drawable.n4, R.drawable.n5, R.drawable.n6, R.drawable.n7, R.drawable.n8, R.drawable.n9, R.drawable.n10};
    PositionXY[] posXY = new PositionXY[(this.randomNumbersCount + 1)];
    int[] randomNumber = new int[this.randomNumbersCount];
    int randomNumbersCount = 5;
    int selectedNumber = 0;
    boolean toporbottom = false;
    int width = 0;

    public Panel(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        getHolder().addCallback(this);
        this.canvasthread = new CanvasThread(getHolder(), this);
        setFocusable(true);
        setOnTouchListener(this);
        this.mAudioManager = (AudioManager) context2.getSystemService("audio");
        this.mStreamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool = new SoundPool(5, 3, 0);
        int i = mSoundIdsAll.length;
        while (true) {
            i--;
            if (i >= 0) {
                this.mSoundPoolIds[i] = this.mSoundPool.load(context2, mSoundIdsAll[i], 1);
            } else {
                return;
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width2, int height2) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        setBackgroundColor(-1);
        this.width = getWidth();
        this.height = getHeight();
        this.randomNumber = randomNumbers();
        this.posXY = randomPosition();
        this.canvasthread.setRunning(true);
        this.canvasthread.start();
    }

    public int[] randomNumbers() {
        this.selectedNumber = RandomCreator.getRandomInt(1, 10);
        int i = 0;
        while (i < 5) {
            this.randomNumber[i] = RandomCreator.getRandomInt(1, 10);
            if (this.randomNumber[i] != this.selectedNumber && ((i > 0 && this.randomNumber[i] != this.randomNumber[i - 1]) || i == 0)) {
                i++;
            }
        }
        return this.randomNumber;
    }

    public PositionXY[] randomPosition() {
        Bitmap bmNumber = BitmapFactory.decodeResource(getResources(), this.numbers[1]);
        PositionXY[] posXYloc = new PositionXY[(this.randomNumbersCount + 1)];
        int i = 0;
        while (i < this.randomNumbersCount + 1) {
            int randomPosX1 = RandomCreator.getRandomInt(0, this.width);
            int randomPosY1 = RandomCreator.getRandomInt(0, this.height);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int randomPosX2 = randomPosX1 + bmNumber.getWidth();
            int randomPosY2 = randomPosY1 + bmNumber.getHeight();
            boolean ok = false;
            if (randomPosX2 < this.width && randomPosY2 < this.height) {
                if (i != 0) {
                    ok = true;
                    int j = 0;
                    while (true) {
                        if (j < i) {
                            if (randomPosX1 <= posXYloc[j].getX1() || randomPosY1 <= posXYloc[j].getY1() || randomPosX1 >= posXYloc[j].getX2() || randomPosY1 >= posXYloc[j].getY2()) {
                                if (randomPosX2 <= posXYloc[j].getX1() || randomPosY1 <= posXYloc[j].getY1() || randomPosX2 >= posXYloc[j].getX2() || randomPosY1 >= posXYloc[j].getY2()) {
                                    if (randomPosX1 <= posXYloc[j].getX1() || randomPosY2 <= posXYloc[j].getY1() || randomPosX1 >= posXYloc[j].getX2() || randomPosY2 >= posXYloc[j].getY2()) {
                                        if (randomPosX2 > posXYloc[j].getX1() && randomPosY2 > posXYloc[j].getY1() && randomPosX2 < posXYloc[j].getX2() && randomPosY2 < posXYloc[j].getY2()) {
                                            ok = false;
                                            break;
                                        }
                                        j++;
                                    } else {
                                        ok = false;
                                        break;
                                    }
                                } else {
                                    ok = false;
                                    break;
                                }
                            } else {
                                ok = false;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                } else {
                    ok = true;
                }
            }
            if (ok) {
                posXYloc[i] = new PositionXY();
                posXYloc[i].setX1(randomPosX1);
                posXYloc[i].setY1(randomPosY1);
                posXYloc[i].setX2(randomPosX2);
                posXYloc[i].setY2(randomPosY2);
                i++;
            }
        }
        this.mSoundPool.play(this.mSoundPoolIds[this.selectedNumber - 1], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
        return posXYloc;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this.canvasthread.setRunning(false);
        while (retry) {
            try {
                this.canvasthread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public void onDraw(Canvas canvas) {
        for (int i = 0; i < this.randomNumbersCount; i++) {
            canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), this.numbers[this.randomNumber[i] - 1]), (float) this.posXY[i].getX1(), (float) this.posXY[i].getY1(), (Paint) null);
        }
        canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), this.numbers[this.selectedNumber - 1]), (float) this.posXY[this.randomNumbersCount].getX1(), (float) this.posXY[this.randomNumbersCount].getY1(), (Paint) null);
    }

    public boolean onTouch(View v, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        if (event.getAction() == 0) {
            if (x <= ((float) this.posXY[this.randomNumbersCount].getX1()) || x >= ((float) this.posXY[this.randomNumbersCount].getX2()) || y <= ((float) this.posXY[this.randomNumbersCount].getY1()) || y >= ((float) this.posXY[this.randomNumbersCount].getY2())) {
                this.mSoundPool.play(this.mSoundPoolIds[11], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
            } else {
                this.mSoundPool.play(this.mSoundPoolIds[10], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.randomNumber = randomNumbers();
            this.posXY = randomPosition();
            setBackgroundColor(-1);
        }
        return true;
    }
}
