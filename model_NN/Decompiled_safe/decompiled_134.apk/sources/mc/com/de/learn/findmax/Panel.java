package mc.com.de.learn.findmax;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import mc.com.de.learn.R;
import mc.com.de.learn.inter.SoundFiles;
import mc.com.de.learn.util.RandomCreator;

public class Panel extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener, SoundFiles {
    CanvasThread canvasthread;
    Context context;
    int fruitBottomX = 0;
    int fruitBottomY = 0;
    int fruitTopX = 0;
    int fruitTopY = 0;
    int[] fruits = {R.drawable.apple, R.drawable.banana, R.drawable.grape, R.drawable.orange, R.drawable.papaya, R.drawable.pineapple, R.drawable.strawberry, R.drawable.watermelon};
    int fruitsBottom = 0;
    int fruitsBottomCount = 0;
    int fruitsBottomFirstX = 0;
    int fruitsBottomFirstY = 0;
    int fruitsTop = 0;
    int fruitsTopCount = 0;
    int fruitsTopFirstX = 0;
    int fruitsTopFirstY = 0;
    int height = 0;
    private AudioManager mAudioManager;
    private SoundPool mSoundPool;
    private int[] mSoundPoolIds = new int[mSoundIds.length];
    private int mStreamVolume;
    boolean toporbottom = false;
    int width = 0;

    public Panel(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        getHolder().addCallback(this);
        this.canvasthread = new CanvasThread(getHolder(), this);
        setFocusable(true);
        setOnTouchListener(this);
        this.mAudioManager = (AudioManager) context2.getSystemService("audio");
        this.mStreamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool = new SoundPool(5, 3, 0);
        int i = mSoundIds.length;
        while (true) {
            i--;
            if (i >= 0) {
                this.mSoundPoolIds[i] = this.mSoundPool.load(context2, mSoundIds[i], 1);
            } else {
                return;
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width2, int height2) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        setBackgroundColor(-1);
        this.width = getWidth();
        this.height = getHeight();
        this.fruitsTopFirstX = 0;
        this.fruitsTopFirstY = 0;
        this.fruitTopX = this.fruitsTopFirstX;
        this.fruitTopY = this.fruitsTopFirstY;
        this.fruitsBottomFirstX = 0;
        this.fruitsBottomFirstY = getHeight() / 2;
        this.fruitBottomX = this.fruitsBottomFirstX;
        this.fruitBottomY = this.fruitsBottomFirstY;
        this.toporbottom = randomNumbers();
        this.canvasthread.setRunning(true);
        this.canvasthread.start();
    }

    public boolean randomNumbers() {
        try {
            this.fruitsTop = RandomCreator.getRandomInt(0, 7);
            this.fruitsTopCount = RandomCreator.getRandomInt(1, 10);
            Thread.sleep(1000);
            this.fruitsBottom = RandomCreator.getRandomInt(0, 7);
            this.fruitsBottomCount = this.fruitsTopCount;
            while (this.fruitsBottomCount == this.fruitsTopCount) {
                this.fruitsBottomCount = RandomCreator.getRandomInt(1, 10);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (this.fruitsTopCount > this.fruitsBottomCount) {
            return true;
        }
        return false;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this.canvasthread.setRunning(false);
        while (retry) {
            try {
                this.canvasthread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public void onDraw(Canvas canvas) {
        Bitmap fruitTop = BitmapFactory.decodeResource(getResources(), this.fruits[this.fruitsTop]);
        Bitmap fruitBottom = BitmapFactory.decodeResource(getResources(), this.fruits[this.fruitsBottom]);
        int fruitTopWidth = fruitTop.getWidth();
        int fruitTopHeight = fruitTop.getHeight();
        int fruitTopLineCount = 0;
        for (int i = 0; i < this.fruitsTopCount; i++) {
            this.fruitTopX = fruitTopLineCount * fruitTopWidth;
            if (this.fruitTopX + fruitTopWidth > getWidth()) {
                fruitTopLineCount = 0;
                this.fruitTopX = this.fruitsTopFirstX;
                this.fruitTopY = this.fruitTopY + fruitTopHeight;
            }
            canvas.drawBitmap(fruitTop, (float) this.fruitTopX, (float) this.fruitTopY, (Paint) null);
            fruitTopLineCount++;
        }
        this.fruitTopX = 0;
        this.fruitTopY = 0;
        int fruitBottomWidth = fruitBottom.getWidth();
        int fruitBottomHeight = fruitBottom.getHeight();
        int fruitBottomLineCount = 0;
        for (int i2 = 0; i2 < this.fruitsBottomCount; i2++) {
            this.fruitBottomX = fruitBottomLineCount * fruitBottomWidth;
            if (this.fruitBottomX + fruitBottomWidth > getWidth()) {
                this.fruitBottomX = this.fruitsBottomFirstX;
                this.fruitBottomY = this.fruitBottomY + fruitBottomHeight;
                fruitBottomLineCount = 0;
            }
            canvas.drawBitmap(fruitBottom, (float) this.fruitBottomX, (float) this.fruitBottomY, (Paint) null);
            fruitBottomLineCount++;
        }
        this.fruitBottomX = 0;
        this.fruitBottomY = getHeight() / 2;
        Paint paint = new Paint();
        paint.setPathEffect(new DashPathEffect(new float[]{20.0f, 5.0f}, 1.0f));
        paint.setStrokeWidth(8.0f);
        canvas.drawLine(0.0f, (float) (getHeight() / 2), (float) getWidth(), (float) (getHeight() / 2), paint);
    }

    public boolean onTouch(View v, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        if (event.getAction() == 0) {
            if (x <= 0.0f || y <= 0.0f || x >= ((float) getWidth()) || y >= ((float) (getHeight() / 2))) {
                if (!this.toporbottom) {
                    this.mSoundPool.play(this.mSoundPoolIds[0], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
                } else {
                    this.mSoundPool.play(this.mSoundPoolIds[1], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
                }
            } else if (this.toporbottom) {
                this.mSoundPool.play(this.mSoundPoolIds[0], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
            } else {
                this.mSoundPool.play(this.mSoundPoolIds[1], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
            }
            this.toporbottom = randomNumbers();
            setBackgroundColor(-1);
        }
        return true;
    }
}
