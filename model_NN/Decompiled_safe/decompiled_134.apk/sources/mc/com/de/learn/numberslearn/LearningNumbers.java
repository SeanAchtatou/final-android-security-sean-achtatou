package mc.com.de.learn.numberslearn;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import java.util.Hashtable;
import mc.com.de.learn.R;
import mc.com.de.learn.inter.SoundFiles;

public class LearningNumbers extends Activity implements SoundFiles {
    public static Hashtable<String, int[]> MyResources = new Hashtable<>();
    public static Hashtable<String, SoundPool> MySoundPool = new Hashtable<>();
    public static Hashtable<String, Integer> MyStreamVolume = new Hashtable<>();
    private AudioManager mAudioManager;
    private SoundPool mSoundPool;
    private int[] mSoundPoolIds = new int[mSoundIds.length];
    private int mStreamVolume;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.numbers);
        this.mAudioManager = (AudioManager) getSystemService("audio");
        this.mStreamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool = new SoundPool(5, 3, 0);
        int i = mSoundIds.length;
        while (true) {
            i--;
            if (i < 0) {
                MyResources.put("mSoundPoolIds", this.mSoundPoolIds);
                MySoundPool.put("mSoundPool", this.mSoundPool);
                MyStreamVolume.put("mStreamVolume", new Integer(this.mStreamVolume));
                return;
            }
            this.mSoundPoolIds[i] = this.mSoundPool.load(this, mSoundIds[i], 1);
        }
    }
}
