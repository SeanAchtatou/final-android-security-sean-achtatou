package mc.com.de.learn.util;

import java.util.Random;

public final class RandomCreator {
    public static int getRandomInt(int min, int max) {
        return new Random(System.currentTimeMillis()).nextInt((max - min) + 1) + min;
    }
}
