package mc.com.de.learn;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import mc.com.de.learn.addition.Addition;
import mc.com.de.learn.count.KidsNumber;
import mc.com.de.learn.exer.Exercies;
import mc.com.de.learn.findmax.FindMaxNumber;
import mc.com.de.learn.findnum.FindNumber;
import mc.com.de.learn.memory.Manager;
import mc.com.de.learn.numberslearn.LearningNumbers;
import mc.com.de.learn.subtr.Subtract;

public class Startsite extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        AdView adView = new AdView(this, AdSize.BANNER, "a14e24112f341da");
        ((LinearLayout) findViewById(R.id.linearLayout)).addView(adView);
        AdRequest request = new AdRequest();
        request.setTesting(true);
        adView.loadAd(request);
    }

    public void onNavButtonClick(View view) {
        switch (view.getId()) {
            case R.id.sf_numbers:
                startActivity(new Intent(this, LearningNumbers.class));
                return;
            case R.id.sf_kidsnum:
                startActivity(new Intent(this, KidsNumber.class));
                return;
            case R.id.sf_findmax:
                startActivity(new Intent(this, FindMaxNumber.class));
                return;
            case R.id.sf_findnum:
                startActivity(new Intent(this, FindNumber.class));
                return;
            case R.id.sf_memory:
                startActivity(new Intent(this, Manager.class));
                return;
            case R.id.sf_addition:
                startActivity(new Intent(this, Addition.class));
                return;
            case R.id.sf_subtraction:
                startActivity(new Intent(this, Subtract.class));
                return;
            case R.id.sf_exercises:
                startActivity(new Intent(this, Exercies.class));
                return;
            case R.id.sf_buythefullversion:
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://details?id=mc.com.de.learnfull"));
                startActivity(intent);
                return;
            default:
                return;
        }
    }
}
