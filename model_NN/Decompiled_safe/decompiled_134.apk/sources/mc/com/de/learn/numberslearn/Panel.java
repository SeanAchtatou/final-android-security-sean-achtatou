package mc.com.de.learn.numberslearn;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import mc.com.de.learn.inter.SoundFiles;

public class Panel extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener, SoundFiles {
    CanvasThread canvasthread;
    Context context;
    int farbe = -16776961;
    int height = 0;
    private AudioManager mAudioManager;
    private SoundPool mSoundPool;
    private int[] mSoundPoolIds = new int[mSoundIdsAll.length];
    private int mStreamVolume;
    MediaPlayer mpNumbers;
    int number = 1;
    int numberX;
    int numberY;
    int triangleHeight = 60;
    int triangleMiddle = (this.triangleHeight / 2);
    int triangleWidth = 30;
    int width = 0;

    public Panel(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        getHolder().addCallback(this);
        this.canvasthread = new CanvasThread(getHolder(), this);
        setFocusable(true);
        setOnTouchListener(this);
        Context context3 = getContext();
        this.mAudioManager = (AudioManager) context3.getSystemService("audio");
        this.mStreamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool = new SoundPool(5, 3, 0);
        int i = mSoundIdsAll.length;
        while (true) {
            i--;
            if (i >= 0) {
                this.mSoundPoolIds[i] = this.mSoundPool.load(context3, mSoundIdsAll[i], 1);
            } else {
                return;
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width2, int height2) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.width = getWidth();
        this.height = getHeight();
        this.numberX = this.width / 2;
        this.numberY = this.height / 2;
        this.canvasthread.setRunning(true);
        this.canvasthread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this.canvasthread.setRunning(false);
        while (retry) {
            try {
                this.canvasthread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public void onDraw(Canvas canvas) {
        this.width = canvas.getWidth();
        this.height = canvas.getHeight();
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-1);
        canvas.drawPaint(paint);
        paint.setColor(-65536);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(5.0f);
        Path path = new Path();
        path.moveTo(0.0f, 0.0f);
        path.lineTo((float) this.triangleWidth, (float) this.triangleMiddle);
        path.lineTo(0.0f, (float) this.triangleHeight);
        path.close();
        path.offset((float) (this.width - this.triangleWidth), (float) (this.height / 3));
        canvas.drawPath(path, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(2.0f);
        Path path2 = new Path();
        path2.moveTo(0.0f, 0.0f);
        path2.lineTo((float) (-this.triangleWidth), (float) this.triangleMiddle);
        path2.lineTo(0.0f, (float) this.triangleHeight);
        path2.close();
        path2.offset((float) this.triangleWidth, (float) (this.height / 3));
        canvas.drawPath(path2, paint);
        paint.setColor(-16776961);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(30.0f);
        paint.setTextSize(150.0f);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(this.farbe);
        canvas.drawText(new StringBuilder().append(this.number).toString(), (float) this.numberX, (float) this.numberY, paint);
        System.out.println("OK");
    }

    public boolean onTouch(View v, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        if (event.getAction() == 0) {
            if (x > ((float) (this.width - this.triangleWidth)) && x < ((float) this.width) && y > ((float) (this.height / 3)) && y < ((float) ((this.height / 3) + this.triangleHeight))) {
                this.number++;
                if (this.number > 10) {
                    this.number = 1;
                }
            }
            if (x > 0.0f && x < ((float) this.triangleWidth) && y > ((float) (this.height / 3)) && y < ((float) ((this.height / 3) + this.triangleHeight))) {
                this.number--;
                if (this.number < 1) {
                    this.number = 10;
                }
            }
            this.mSoundPool.play(this.mSoundPoolIds[this.number - 1], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
        }
        return true;
    }
}
