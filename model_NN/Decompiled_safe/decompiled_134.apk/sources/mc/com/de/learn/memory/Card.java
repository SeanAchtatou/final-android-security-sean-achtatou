package mc.com.de.learn.memory;

import android.widget.Button;

public class Card {
    public Button button;
    public int x;
    public int y;

    public Card(Button button2, int x2, int y2) {
        this.x = x2;
        this.y = y2;
        this.button = button2;
    }
}
