package mc.com.de.learn.count;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import mc.com.de.learn.R;
import mc.com.de.learn.inter.SoundFiles;
import mc.com.de.learn.util.NumberImageInfo;
import mc.com.de.learn.util.RandomCreator;

public class Panel extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener, SoundFiles {
    CanvasThread canvasthread;
    int countNumberPos = -1;
    int displayCountNumber = 4;
    int fruitCount = 0;
    int fruitTopX = 0;
    int fruitTopY = 0;
    int[] fruits = {R.drawable.apple, R.drawable.banana, R.drawable.grape, R.drawable.orange, R.drawable.papaya, R.drawable.pineapple, R.drawable.strawberry, R.drawable.watermelon};
    int fruitsTopFirstX = 0;
    int height = 0;
    private AudioManager mAudioManager;
    private SoundPool mSoundPool;
    private int[] mSoundPoolIds = new int[mSoundIds.length];
    private int mStreamVolume;
    NumberImageInfo[] nii;
    Bitmap[] numberbmp = new Bitmap[4];
    int width = 0;

    public Panel(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        this.canvasthread = new CanvasThread(getHolder(), this);
        setFocusable(true);
        setOnTouchListener(this);
        this.mAudioManager = (AudioManager) context.getSystemService("audio");
        this.mStreamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool = new SoundPool(5, 3, 0);
        int i = mSoundIds.length;
        while (true) {
            i--;
            if (i >= 0) {
                this.mSoundPoolIds[i] = this.mSoundPool.load(context, mSoundIds[i], 1);
            } else {
                return;
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width2, int height2) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        setBackgroundColor(-1);
        this.width = getWidth();
        this.height = getHeight();
        this.fruitCount = RandomCreator.getRandomInt(1, 10);
        this.nii = new NumberImageInfo[this.displayCountNumber];
        for (int i = 0; i < this.displayCountNumber; i++) {
            this.nii[i] = new NumberImageInfo();
        }
        randomNumbers();
        this.canvasthread.setRunning(true);
        this.canvasthread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this.canvasthread.setRunning(false);
        while (retry) {
            try {
                this.canvasthread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public void onDraw(Canvas canvas) {
        Bitmap kangoo = BitmapFactory.decodeResource(getResources(), this.fruits[RandomCreator.getRandomInt(0, 7)]);
        int fruitTopWidth = kangoo.getWidth();
        int fruitTopHeight = kangoo.getHeight();
        int fruitTopLineCount = 0;
        for (int i = 0; i < this.fruitCount; i++) {
            this.fruitTopX = fruitTopLineCount * fruitTopWidth;
            if (this.fruitTopX + fruitTopWidth > getWidth()) {
                fruitTopLineCount = 0;
                this.fruitTopX = this.fruitsTopFirstX;
                this.fruitTopY += fruitTopHeight;
            }
            canvas.drawBitmap(kangoo, (float) this.fruitTopX, (float) this.fruitTopY, (Paint) null);
            fruitTopLineCount++;
        }
        this.fruitTopX = 0;
        this.fruitTopY = 0;
        for (int i2 = 1; i2 <= this.numberbmp.length; i2++) {
            int widthNumberbmp = this.numberbmp[i2 - 1].getWidth();
            int numberbmpX = (((this.width - (this.numberbmp.length * widthNumberbmp)) / (this.numberbmp.length + 1)) * i2) + ((i2 - 1) * widthNumberbmp);
            int numberbmpY = (this.height * 4) / 5;
            canvas.drawBitmap(this.numberbmp[i2 - 1], (float) numberbmpX, (float) numberbmpY, (Paint) null);
            this.nii[i2 - 1].setX1(numberbmpX);
            this.nii[i2 - 1].setY1(numberbmpY);
            this.nii[i2 - 1].setX2(numberbmpX + widthNumberbmp);
            this.nii[i2 - 1].setY2(this.numberbmp[i2 - 1].getHeight() + numberbmpY);
        }
    }

    public void randomNumbers() {
        int[] randomInt = new int[4];
        for (int i = 0; i < this.numberbmp.length; i++) {
            randomInt[i] = RandomCreator.getRandomInt(0, 9);
            while (randomInt[i] == this.fruitCount - 1) {
                randomInt[i] = RandomCreator.getRandomInt(0, 9);
            }
            this.numberbmp[i] = BitmapFactory.decodeResource(getResources(), numbers[randomInt[i]]);
        }
        int fruitCountNumberPosition = RandomCreator.getRandomInt(0, 3);
        this.numberbmp[fruitCountNumberPosition] = BitmapFactory.decodeResource(getResources(), numbers[this.fruitCount - 1]);
        this.countNumberPos = fruitCountNumberPosition;
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            this.fruitCount = RandomCreator.getRandomInt(1, 10);
            for (int i = 0; i < this.nii.length; i++) {
                if (event.getX() > ((float) this.nii[i].getX1()) && event.getX() < ((float) this.nii[i].getX2()) && event.getY() > ((float) this.nii[i].getY1()) && event.getY() < ((float) this.nii[i].getY2())) {
                    try {
                        if (this.countNumberPos == i) {
                            this.mSoundPool.play(this.mSoundPoolIds[0], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
                            Thread.sleep(2);
                            setBackgroundColor(-1);
                        } else {
                            Thread.sleep(2);
                            this.mSoundPool.play(this.mSoundPoolIds[1], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
                            setBackgroundColor(-1);
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            randomNumbers();
        } else {
            event.getAction();
        }
        return true;
    }
}
