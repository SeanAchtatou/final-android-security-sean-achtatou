package mc.com.de.learn;

public final class R {

    public static final class array {
        public static final int type = 2131230720;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int blau = 2131034117;
        public static final int blau_halbtransparent = 2131034118;
        public static final int dunkelblau = 2131034120;
        public static final int dunkelgrau = 2131034121;
        public static final int gelb = 2131034115;
        public static final int gelb_transparent = 2131034119;
        public static final int gruen = 2131034116;
        public static final int rot = 2131034114;
        public static final int schwarz = 2131034112;
        public static final int textfarbe = 2131034122;
        public static final int weiss = 2131034113;
    }

    public static final class dimen {
        public static final int icon_normal = 2131099652;
        public static final int text_extragross = 2131099651;
        public static final int text_gross = 2131099650;
        public static final int text_klein = 2131099648;
        public static final int text_normal = 2131099649;
    }

    public static final class drawable {
        public static final int android_button = 2130837504;
        public static final int android_focused = 2130837505;
        public static final int android_normal = 2130837506;
        public static final int android_pressed = 2130837507;
        public static final int apple = 2130837508;
        public static final int background = 2130837509;
        public static final int banana = 2130837510;
        public static final int card1 = 2130837511;
        public static final int card10 = 2130837512;
        public static final int card11 = 2130837513;
        public static final int card12 = 2130837514;
        public static final int card13 = 2130837515;
        public static final int card14 = 2130837516;
        public static final int card15 = 2130837517;
        public static final int card16 = 2130837518;
        public static final int card17 = 2130837519;
        public static final int card18 = 2130837520;
        public static final int card19 = 2130837521;
        public static final int card2 = 2130837522;
        public static final int card20 = 2130837523;
        public static final int card21 = 2130837524;
        public static final int card3 = 2130837525;
        public static final int card4 = 2130837526;
        public static final int card5 = 2130837527;
        public static final int card6 = 2130837528;
        public static final int card7 = 2130837529;
        public static final int card8 = 2130837530;
        public static final int card9 = 2130837531;
        public static final int grape = 2130837532;
        public static final int icon = 2130837533;
        public static final int n1 = 2130837534;
        public static final int n10 = 2130837535;
        public static final int n2 = 2130837536;
        public static final int n3 = 2130837537;
        public static final int n4 = 2130837538;
        public static final int n5 = 2130837539;
        public static final int n6 = 2130837540;
        public static final int n7 = 2130837541;
        public static final int n8 = 2130837542;
        public static final int n9 = 2130837543;
        public static final int orange = 2130837544;
        public static final int papaya = 2130837545;
        public static final int pineapple = 2130837546;
        public static final int star = 2130837547;
        public static final int strawberry = 2130837548;
        public static final int watermelon = 2130837549;
    }

    public static final class id {
        public static final int ImageView01 = 2131361814;
        public static final int LinearLayout01 = 2131361813;
        public static final int Spinner01 = 2131361805;
        public static final int TableLayout03 = 2131361803;
        public static final int TableRow01 = 2131361808;
        public static final int TableRow02 = 2131361810;
        public static final int TableRow03 = 2131361812;
        public static final int TableRow04 = 2131361804;
        public static final int TableRow05 = 2131361806;
        public static final int background = 2131361792;
        public static final int linearLayout = 2131361793;
        public static final int mainTitle = 2131361809;
        public static final int myWebSite = 2131361811;
        public static final int sf_addition = 2131361799;
        public static final int sf_buythefullversion = 2131361802;
        public static final int sf_exercises = 2131361801;
        public static final int sf_findmax = 2131361796;
        public static final int sf_findnum = 2131361797;
        public static final int sf_kidsnum = 2131361795;
        public static final int sf_memory = 2131361798;
        public static final int sf_numbers = 2131361794;
        public static final int sf_subtraction = 2131361800;
        public static final int tv1 = 2131361807;
    }

    public static final class layout {
        public static final int addition = 2130903040;
        public static final int count = 2130903041;
        public static final int exercies = 2130903042;
        public static final int findmax = 2130903043;
        public static final int findnum = 2130903044;
        public static final int main = 2130903045;
        public static final int memory = 2130903046;
        public static final int numbers = 2130903047;
        public static final int subtract = 2130903048;
    }

    public static final class raw {
        public static final int acht = 2130968576;
        public static final int achtzehn = 2130968577;
        public static final int applaude = 2130968578;
        public static final int drei = 2130968579;
        public static final int dreizehn = 2130968580;
        public static final int eins = 2130968581;
        public static final int elf = 2130968582;
        public static final int funf = 2130968583;
        public static final int funfzehn = 2130968584;
        public static final int iki = 2130968585;
        public static final int neun = 2130968586;
        public static final int neunzehn = 2130968587;
        public static final int sechs = 2130968588;
        public static final int sechszehn = 2130968589;
        public static final int sekiz = 2130968590;
        public static final int sibzehn = 2130968591;
        public static final int sieben = 2130968592;
        public static final int vier = 2130968593;
        public static final int vierzehn = 2130968594;
        public static final int yanlis = 2130968595;
        public static final int zehn = 2130968596;
        public static final int zwanzig = 2130968597;
        public static final int zwei = 2130968598;
        public static final int zwolf = 2130968599;
    }

    public static final class string {
        public static final int app_name = 2131165185;
        public static final int hello = 2131165184;
        public static final int sf_addition = 2131165186;
        public static final int sf_buythefullversion = 2131165194;
        public static final int sf_exercises = 2131165188;
        public static final int sf_findmax = 2131165189;
        public static final int sf_findnum = 2131165190;
        public static final int sf_kidsnum = 2131165191;
        public static final int sf_memory = 2131165193;
        public static final int sf_numbers = 2131165192;
        public static final int sf_subtraction = 2131165187;
        public static final int tx_startseite_titel = 2131165195;
    }

    public static final class style {
        public static final int Label = 2131296259;
        public static final int TextGross = 2131296257;
        public static final int TextNormal = 2131296256;
        public static final int TextUeberschrift = 2131296258;
    }
}
