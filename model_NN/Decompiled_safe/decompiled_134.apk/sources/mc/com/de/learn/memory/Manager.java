package mc.com.de.learn.memory;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import mc.com.de.learn.R;
import mc.com.de.learn.inter.SoundFiles;

public class Manager extends Activity implements SoundFiles {
    private static int COL_COUNT = -1;
    private static int ROW_COUNT = -1;
    /* access modifiers changed from: private */
    public static Object lock = new Object();
    /* access modifiers changed from: private */
    public Drawable backImage;
    private ButtonListener buttonListener;
    /* access modifiers changed from: private */
    public int[][] cards;
    private Context context;
    /* access modifiers changed from: private */
    public Card firstCard;
    /* access modifiers changed from: private */
    public UpdateCardsHandler handler;
    /* access modifiers changed from: private */
    public List<Drawable> images;
    private final int mApplaudeId = R.raw.applaude;
    private AudioManager mAudioManager;
    /* access modifiers changed from: private */
    public int mSoundApplaude;
    /* access modifiers changed from: private */
    public SoundPool mSoundPool;
    /* access modifiers changed from: private */
    public int[] mSoundPoolIds = new int[mSoundIdsAll.length];
    /* access modifiers changed from: private */
    public int mStreamVolume;
    private TableLayout mainTable;
    /* access modifiers changed from: private */
    public Card seconedCard;
    int turns;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        this.mAudioManager = (AudioManager) getSystemService("audio");
        this.mStreamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool = new SoundPool(5, 3, 0);
        int i = mSoundIdsAll.length;
        while (true) {
            i--;
            if (i < 0) {
                this.mSoundApplaude = this.mSoundPool.load(this, R.raw.applaude, 1);
                this.handler = new UpdateCardsHandler();
                loadImages();
                setContentView((int) R.layout.memory);
                Linkify.addLinks((TextView) findViewById(R.id.myWebSite), 1);
                this.backImage = getResources().getDrawable(R.drawable.star);
                this.buttonListener = new ButtonListener(this);
                this.mainTable = (TableLayout) findViewById(R.id.TableLayout03);
                this.context = this.mainTable.getContext();
                Spinner s = (Spinner) findViewById(R.id.Spinner01);
                ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.type, 17367048);
                adapter.setDropDownViewResource(17367049);
                s.setAdapter((SpinnerAdapter) adapter);
                s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
                        int x;
                        int y;
                        ((Spinner) Manager.this.findViewById(R.id.Spinner01)).setSelection(0);
                        switch (pos) {
                            case 1:
                                x = 4;
                                y = 4;
                                break;
                            case 2:
                                x = 4;
                                y = 5;
                                break;
                            case 3:
                                x = 4;
                                y = 6;
                                break;
                            case 4:
                                x = 5;
                                y = 6;
                                break;
                            case 5:
                                x = 6;
                                y = 6;
                                break;
                            default:
                                return;
                        }
                        Manager.this.newGame(x, y);
                    }

                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });
                return;
            }
            this.mSoundPoolIds[i] = this.mSoundPool.load(this, mSoundIdsAll[i], 1);
        }
    }

    /* access modifiers changed from: private */
    public void newGame(int c, int r) {
        ROW_COUNT = r;
        COL_COUNT = c;
        this.cards = (int[][]) Array.newInstance(Integer.TYPE, COL_COUNT, ROW_COUNT);
        this.mainTable.removeView(findViewById(R.id.TableRow01));
        this.mainTable.removeView(findViewById(R.id.TableRow02));
        TableRow tr = (TableRow) findViewById(R.id.TableRow03);
        tr.removeAllViews();
        this.mainTable = new TableLayout(this.context);
        tr.addView(this.mainTable);
        for (int y = 0; y < ROW_COUNT; y++) {
            this.mainTable.addView(createRow(y));
        }
        this.firstCard = null;
        loadCards();
        this.turns = 0;
        ((TextView) findViewById(R.id.tv1)).setText("Versuche: " + this.turns);
    }

    private void loadImages() {
        this.images = new ArrayList();
        this.images.add(getResources().getDrawable(R.drawable.card1));
        this.images.add(getResources().getDrawable(R.drawable.card2));
        this.images.add(getResources().getDrawable(R.drawable.card3));
        this.images.add(getResources().getDrawable(R.drawable.card4));
        this.images.add(getResources().getDrawable(R.drawable.card5));
        this.images.add(getResources().getDrawable(R.drawable.card6));
        this.images.add(getResources().getDrawable(R.drawable.card7));
        this.images.add(getResources().getDrawable(R.drawable.card8));
        this.images.add(getResources().getDrawable(R.drawable.card9));
        this.images.add(getResources().getDrawable(R.drawable.card10));
        this.images.add(getResources().getDrawable(R.drawable.card11));
        this.images.add(getResources().getDrawable(R.drawable.card12));
        this.images.add(getResources().getDrawable(R.drawable.card13));
        this.images.add(getResources().getDrawable(R.drawable.card14));
        this.images.add(getResources().getDrawable(R.drawable.card15));
        this.images.add(getResources().getDrawable(R.drawable.card16));
        this.images.add(getResources().getDrawable(R.drawable.card17));
        this.images.add(getResources().getDrawable(R.drawable.card18));
        this.images.add(getResources().getDrawable(R.drawable.card19));
        this.images.add(getResources().getDrawable(R.drawable.card20));
        this.images.add(getResources().getDrawable(R.drawable.card21));
    }

    private void loadCards() {
        try {
            int size = ROW_COUNT * COL_COUNT;
            Log.i("loadCards()", "size=" + size);
            ArrayList<Integer> list = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                list.add(new Integer(i));
            }
            Random r = new Random();
            for (int i2 = size - 1; i2 >= 0; i2--) {
                int t = 0;
                if (i2 > 0) {
                    t = r.nextInt(i2);
                }
                this.cards[i2 % COL_COUNT][i2 / COL_COUNT] = ((Integer) list.remove(t)).intValue() % (size / 2);
                Log.i("loadCards()", "card[" + (i2 % COL_COUNT) + "][" + (i2 / COL_COUNT) + "]=" + this.cards[i2 % COL_COUNT][i2 / COL_COUNT]);
            }
        } catch (Exception e) {
            Log.e("loadCards()", new StringBuilder().append(e).toString());
        }
    }

    private TableRow createRow(int y) {
        TableRow row = new TableRow(this.context);
        row.setHorizontalGravity(17);
        for (int x = 0; x < COL_COUNT; x++) {
            row.addView(createImageButton(x, y));
        }
        return row;
    }

    private View createImageButton(int x, int y) {
        Button button = new Button(this.context);
        button.setBackgroundDrawable(this.backImage);
        button.setId((x * 100) + y);
        button.setOnClickListener(this.buttonListener);
        return button;
    }

    class ButtonListener implements View.OnClickListener {
        Manager m;

        ButtonListener(Manager m2) {
            this.m = m2;
        }

        public void onClick(View v) {
            synchronized (Manager.lock) {
                if (Manager.this.firstCard == null || Manager.this.seconedCard == null) {
                    int id = v.getId();
                    int x = id / 100;
                    int y = id % 100;
                    Manager.this.mSoundPool.play(Manager.this.mSoundPoolIds[Manager.this.cards[x][y]], (float) Manager.this.mStreamVolume, (float) Manager.this.mStreamVolume, 1, 0, 1.0f);
                    turnCard((Button) v, x, y);
                }
            }
        }

        private void turnCard(Button button, int x, int y) {
            button.setBackgroundDrawable((Drawable) Manager.this.images.get(Manager.this.cards[x][y]));
            if (Manager.this.firstCard == null) {
                Manager.this.firstCard = new Card(button, x, y);
            } else if (Manager.this.firstCard.x != x || Manager.this.firstCard.y != y) {
                Manager.this.seconedCard = new Card(button, x, y);
                Manager.this.turns++;
                ((TextView) Manager.this.findViewById(R.id.tv1)).setText("Versuche: " + Manager.this.turns);
                new Timer(false).schedule(new TimerTask() {
                    public void run() {
                        try {
                            synchronized (Manager.lock) {
                                Manager.this.handler.sendEmptyMessage(0);
                            }
                        } catch (Exception e) {
                            Log.e("E1", e.getMessage());
                        }
                    }
                }, 1300);
            }
        }
    }

    class UpdateCardsHandler extends Handler {
        UpdateCardsHandler() {
        }

        public void handleMessage(Message msg) {
            synchronized (Manager.lock) {
                checkCards();
            }
        }

        public void checkCards() {
            if (Manager.this.cards[Manager.this.seconedCard.x][Manager.this.seconedCard.y] == Manager.this.cards[Manager.this.firstCard.x][Manager.this.firstCard.y]) {
                Manager.this.firstCard.button.setVisibility(4);
                Manager.this.seconedCard.button.setVisibility(4);
                Manager.this.mSoundPool.play(Manager.this.mSoundApplaude, (float) Manager.this.mStreamVolume, (float) Manager.this.mStreamVolume, 1, 0, 1.0f);
            } else {
                Manager.this.seconedCard.button.setBackgroundDrawable(Manager.this.backImage);
                Manager.this.firstCard.button.setBackgroundDrawable(Manager.this.backImage);
            }
            Manager.this.firstCard = null;
            Manager.this.seconedCard = null;
        }
    }
}
