package mc.com.de.learn.inter;

import mc.com.de.learn.R;

public interface SoundFiles {
    public static final int[] mSoundIds = {R.raw.applaude, R.raw.yanlis};
    public static final int[] mSoundIdsAll = {R.raw.eins, R.raw.zwei, R.raw.drei, R.raw.vier, R.raw.funf, R.raw.sechs, R.raw.sieben, R.raw.acht, R.raw.neun, R.raw.zehn, R.raw.applaude, R.raw.yanlis};
    public static final int[] numbers = {R.drawable.n1, R.drawable.n2, R.drawable.n3, R.drawable.n4, R.drawable.n5, R.drawable.n6, R.drawable.n7, R.drawable.n8, R.drawable.n9, R.drawable.n10};
}
