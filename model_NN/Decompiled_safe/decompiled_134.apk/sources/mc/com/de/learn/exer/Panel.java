package mc.com.de.learn.exer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import mc.com.de.learn.R;
import mc.com.de.learn.inter.SoundFiles;
import mc.com.de.learn.util.NumberImageInfo;
import mc.com.de.learn.util.RandomCreator;

public class Panel extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener, SoundFiles {
    int additionTextX = 100;
    int additionTextY = 50;
    CanvasThread canvasthread;
    Context context;
    int countNumberPos = -1;
    int displayCountNumber = 4;
    int farbe = -16777216;
    int fruitAdd = 0;
    int fruitBottomX = 0;
    int fruitBottomY = 0;
    int fruitLastCount = 0;
    int fruitTopX = 0;
    int fruitTopY = 0;
    int fruitsBottom = 0;
    int fruitsBottomCount = 0;
    int fruitsBottomFirstX = 0;
    int fruitsBottomFirstY = 0;
    int fruitsTop = 0;
    int fruitsTopCount = 0;
    int fruitsTopFirstX = 0;
    int fruitsTopFirstY = 60;
    int height = 0;
    private AudioManager mAudioManager;
    private SoundPool mSoundPool;
    private int[] mSoundPoolIds = new int[mSoundIds.length];
    private int mStreamVolume;
    NumberImageInfo[] nii;
    int number1 = 5;
    int number2 = 3;
    Bitmap[] numberbmp = new Bitmap[4];
    int[] numbers = {R.drawable.n1, R.drawable.n2, R.drawable.n3, R.drawable.n4, R.drawable.n5, R.drawable.n6, R.drawable.n7, R.drawable.n8, R.drawable.n9, R.drawable.n10};
    int[] randomInt = new int[4];
    int width = 0;

    public Panel(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        getHolder().addCallback(this);
        this.canvasthread = new CanvasThread(getHolder(), this);
        setFocusable(true);
        setOnTouchListener(this);
        this.mAudioManager = (AudioManager) context2.getSystemService("audio");
        this.mStreamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool = new SoundPool(5, 3, 0);
        int i = mSoundIds.length;
        while (true) {
            i--;
            if (i >= 0) {
                this.mSoundPoolIds[i] = this.mSoundPool.load(context2, mSoundIds[i], 1);
            } else {
                return;
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width2, int height2) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        setBackgroundColor(-1);
        this.width = getWidth();
        this.height = getHeight();
        this.fruitTopX = this.fruitsTopFirstX;
        this.fruitTopY = this.fruitsTopFirstY;
        this.fruitsBottomFirstX = 0;
        this.fruitsBottomFirstY = getHeight() / 2;
        this.fruitBottomX = this.fruitsBottomFirstX;
        this.nii = new NumberImageInfo[this.displayCountNumber];
        for (int i = 0; i < this.displayCountNumber; i++) {
            this.nii[i] = new NumberImageInfo();
        }
        randomNumbers();
        this.canvasthread.setRunning(true);
        this.canvasthread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this.canvasthread.setRunning(false);
        while (retry) {
            try {
                this.canvasthread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public void randomNumbers() {
        try {
            this.fruitsTop = RandomCreator.getRandomInt(0, 7);
            this.fruitsTopCount = RandomCreator.getRandomInt(1, 9);
            Thread.sleep(1000);
            this.fruitsBottom = RandomCreator.getRandomInt(0, 7);
            this.fruitsBottomCount = this.fruitsTopCount;
            while (true) {
                if (this.fruitsBottomCount + this.fruitsTopCount <= 10 && this.fruitsBottomCount != this.fruitsTopCount) {
                    break;
                }
                this.fruitsBottomCount = RandomCreator.getRandomInt(1, 9);
            }
            this.fruitLastCount = RandomCreator.getRandomInt(1, 9);
            this.fruitAdd = this.fruitsBottomCount + this.fruitsTopCount;
            while (true) {
                if (this.fruitLastCount < this.fruitAdd && this.fruitsBottomCount != this.fruitLastCount) {
                    break;
                }
                this.fruitLastCount = RandomCreator.getRandomInt(1, 9);
            }
            this.fruitAdd = (this.fruitsBottomCount + this.fruitsTopCount) - this.fruitLastCount;
            for (int i = 0; i < this.numberbmp.length; i++) {
                this.randomInt[i] = RandomCreator.getRandomInt(0, 9);
                while (this.randomInt[i] == this.fruitAdd - 1) {
                    this.randomInt[i] = RandomCreator.getRandomInt(0, 9);
                }
                this.numberbmp[i] = BitmapFactory.decodeResource(getResources(), this.numbers[this.randomInt[i]]);
            }
            int fruitCountNumberPosition = RandomCreator.getRandomInt(0, 3);
            this.numberbmp[fruitCountNumberPosition] = BitmapFactory.decodeResource(getResources(), this.numbers[this.fruitAdd - 1]);
            this.countNumberPos = fruitCountNumberPosition;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-16776961);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(30.0f);
        paint.setTextSize(50.0f);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(this.farbe);
        canvas.drawText(this.fruitsTopCount + " + " + this.fruitsBottomCount + " - " + this.fruitLastCount + " = ?", (float) (this.additionTextX + 50), (float) (this.height / 2), paint);
        int numberbmpY = (this.height * 4) / 5;
        for (int i = 1; i <= this.numberbmp.length; i++) {
            int widthNumberbmp = this.numberbmp[i - 1].getWidth();
            int numberbmpX = (((this.width - (this.numberbmp.length * widthNumberbmp)) / (this.numberbmp.length + 1)) * i) + ((i - 1) * widthNumberbmp);
            canvas.drawBitmap(this.numberbmp[i - 1], (float) numberbmpX, (float) numberbmpY, (Paint) null);
            this.nii[i - 1].setX1(numberbmpX);
            this.nii[i - 1].setY1(numberbmpY);
            this.nii[i - 1].setX2(numberbmpX + widthNumberbmp);
            this.nii[i - 1].setY2(this.numberbmp[i - 1].getHeight() + numberbmpY);
        }
        paint.setPathEffect(new DashPathEffect(new float[]{20.0f, 5.0f}, 1.0f));
        paint.setStrokeWidth(8.0f);
        canvas.drawLine(0.0f, (float) (numberbmpY - 5), (float) getWidth(), (float) (numberbmpY - 5), paint);
    }

    public boolean onTouch(View v, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        if (event.getAction() == 0) {
            for (int i = 0; i < this.nii.length; i++) {
                if (event.getX() > ((float) this.nii[i].getX1()) && event.getX() < ((float) this.nii[i].getX2()) && event.getY() > ((float) this.nii[i].getY1()) && event.getY() < ((float) this.nii[i].getY2())) {
                    try {
                        if (this.countNumberPos == i) {
                            this.mSoundPool.play(this.mSoundPoolIds[0], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
                            Thread.sleep(2);
                            setBackgroundColor(-1);
                        } else {
                            Thread.sleep(2);
                            this.mSoundPool.play(this.mSoundPoolIds[1], (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
                            setBackgroundColor(-1);
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                    randomNumbers();
                }
            }
        }
        setBackgroundColor(-1);
        return true;
    }
}
