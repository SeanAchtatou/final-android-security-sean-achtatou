package mc.com.de.learn.findnum;

public class PositionXY {
    int x1;
    int x2;
    int y1;
    int y2;

    public int getX1() {
        return this.x1;
    }

    public void setX1(int x12) {
        this.x1 = x12;
    }

    public int getX2() {
        return this.x2;
    }

    public void setX2(int x22) {
        this.x2 = x22;
    }

    public int getY1() {
        return this.y1;
    }

    public void setY1(int y12) {
        this.y1 = y12;
    }

    public int getY2() {
        return this.y2;
    }

    public void setY2(int y22) {
        this.y2 = y22;
    }
}
