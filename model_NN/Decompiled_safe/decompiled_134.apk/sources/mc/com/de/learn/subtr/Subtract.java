package mc.com.de.learn.subtr;

import android.app.Activity;
import android.os.Bundle;
import mc.com.de.learn.R;

public class Subtract extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setRequestedOrientation(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.subtract);
    }
}
