package mc.com.de.learn.addition;

import android.app.Activity;
import android.os.Bundle;
import mc.com.de.learn.R;

public class Addition extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
        setContentView((int) R.layout.addition);
    }
}
