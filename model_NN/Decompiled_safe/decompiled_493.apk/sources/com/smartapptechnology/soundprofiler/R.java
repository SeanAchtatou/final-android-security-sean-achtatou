package com.smartapptechnology.soundprofiler;

public final class R {

    public static final class array {
        public static final int alarm_help = 2131034114;
        public static final int contact_help = 2131034113;
        public static final int profile_help = 2131034115;
        public static final int release_notes = 2131034116;
        public static final int sound_help = 2131034112;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int txtview_ringtone = 2131099648;
    }

    public static final class drawable {
        public static final int appicon = 2130837504;
        public static final int appicon_lite = 2130837505;
        public static final int assign_tone = 2130837506;
        public static final int clearall = 2130837507;
        public static final int contact = 2130837508;
        public static final int contact_pict = 2130837509;
        public static final int contactall = 2130837510;
        public static final int delete = 2130837511;
        public static final int exit = 2130837512;
        public static final int help = 2130837513;
        public static final int play = 2130837514;
        public static final int tab_contact_active = 2130837515;
        public static final int tab_contact_drawable = 2130837516;
        public static final int tab_contact_inactive = 2130837517;
        public static final int tab_notifier_drawable = 2130837518;
        public static final int tab_sound_active = 2130837519;
        public static final int tab_sound_drawable = 2130837520;
        public static final int tab_sound_inactive = 2130837521;
        public static final int tab_tone_active = 2130837522;
        public static final int tab_tone_inactive = 2130837523;
        public static final int tone = 2130837524;
    }

    public static final class id {
        public static final int alarm_lbl = 2131230753;
        public static final int alarm_skb = 2131230754;
        public static final int btn_dialog_rate_later = 2131230727;
        public static final int btn_dialog_rate_never = 2131230728;
        public static final int btn_dialog_rate_now = 2131230726;
        public static final int btn_save_Ringtone = 2131230735;
        public static final int dialog_help_table = 2131230721;
        public static final int dialog_name_edtxt = 2131230742;
        public static final int dialog_release_note_table = 2131230732;
        public static final int imgview_contact = 2131230738;
        public static final int layout_contact_ringtone = 2131230733;
        public static final int layout_dialog_help = 2131230720;
        public static final int layout_dialog_rate = 2131230724;
        public static final int layout_dialog_release = 2131230729;
        public static final int layout_dialog_save = 2131230741;
        public static final int lbl_checkRingtoneStatus = 2131230736;
        public static final int lbl_dialog_help_contact = 2131230722;
        public static final int lbl_dialog_help_devname = 2131230723;
        public static final int lbl_dialog_rate_comment = 2131230725;
        public static final int lbl_dialog_release_new_in_release = 2131230731;
        public static final int lbl_dialog_release_user_agree = 2131230730;
        public static final int list_ringtone = 2131230737;
        public static final int media_lbl = 2131230751;
        public static final int media_skb = 2131230752;
        public static final int menu_clearall = 2131230766;
        public static final int menu_close = 2131230769;
        public static final int menu_contacts = 2131230767;
        public static final int menu_contactsall = 2131230768;
        public static final int menu_context_assignTone = 2131230763;
        public static final int menu_context_delete = 2131230765;
        public static final int menu_context_playtone = 2131230764;
        public static final int menu_delete = 2131230760;
        public static final int menu_exit = 2131230761;
        public static final int menu_help = 2131230762;
        public static final int mobFoxView = 2131230759;
        public static final int notif_lbl = 2131230749;
        public static final int notif_skb = 2131230750;
        public static final int ringtone_lbl = 2131230747;
        public static final int ringtone_skb = 2131230748;
        public static final int save_btn = 2131230744;
        public static final int sound_tone = 2131230746;
        public static final int spinner = 2131230743;
        public static final int spinner_Ringtone = 2131230734;
        public static final int system_lbl = 2131230755;
        public static final int system_skb = 2131230756;
        public static final int txtview_contact = 2131230739;
        public static final int txtview_ringtone = 2131230740;
        public static final int vibrate_lbl = 2131230745;
        public static final int voicecall_lbl = 2131230757;
        public static final int voicecall_skb = 2131230758;
    }

    public static final class layout {
        public static final int layout_auto_complete_list_item = 2130903040;
        public static final int layout_help_dialog = 2130903041;
        public static final int layout_rate_dialog = 2130903042;
        public static final int layout_release_dialog = 2130903043;
        public static final int layout_ringtone = 2130903044;
        public static final int layout_ringtone_row = 2130903045;
        public static final int layout_save_dialog = 2130903046;
        public static final int layout_sound = 2130903047;
        public static final int layout_sound_tone = 2130903048;
        public static final int layout_tab = 2130903049;
    }

    public static final class menu {
        public static final int app_menu = 2131165184;
        public static final int ringtone_context = 2131165185;
        public static final int ringtone_menu = 2131165186;
    }

    public static final class string {
        public static final int accept = 2130968623;
        public static final int alarm = 2130968583;
        public static final int app_name = 2130968576;
        public static final int app_name_lite = 2130968577;
        public static final int cancel = 2130968588;
        public static final int checking = 2130968614;
        public static final int default_str = 2130968598;
        public static final int empty = 2130968580;
        public static final int help_title = 2130968628;
        public static final int instr_contact = 2130968629;
        public static final int is_active = 2130968613;
        public static final int limit_entity = 2130968626;
        public static final int limit_profile = 2130968627;
        public static final int list_item_alarm = 2130968608;
        public static final int list_item_notif = 2130968609;
        public static final int lite = 2130968635;
        public static final int loading = 2130968616;
        public static final int media = 2130968582;
        public static final int menu_context_item_assigntone = 2130968605;
        public static final int menu_context_item_playtone = 2130968606;
        public static final int menu_item_clearall = 2130968604;
        public static final int menu_item_close = 2130968603;
        public static final int menu_item_contacts = 2130968601;
        public static final int menu_item_contactsall = 2130968602;
        public static final int menu_item_delete = 2130968600;
        public static final int menu_item_exit = 2130968599;
        public static final int msg_ringtone_could_not_load = 2130968607;
        public static final int new_in_release = 2130968625;
        public static final int new_profile_name = 2130968578;
        public static final int no = 2130968611;
        public static final int no_list = 2130968597;
        public static final int not_accept = 2130968624;
        public static final int notif = 2130968584;
        public static final int profile = 2130968579;
        public static final int profile_current = 2130968590;
        public static final int profile_loud = 2130968589;
        public static final int profile_mute = 2130968592;
        public static final int profile_quiet = 2130968591;
        public static final int rate_later = 2130968632;
        public static final int rate_never = 2130968633;
        public static final int rate_now = 2130968631;
        public static final int rate_this_app = 2130968630;
        public static final int rating_website = 2130968634;
        public static final int removed = 2130968595;
        public static final int replaced = 2130968596;
        public static final int ringtone = 2130968581;
        public static final int save_new = 2130968587;
        public static final int setting = 2130968615;
        public static final int system = 2130968585;
        public static final int tab_title_alerts = 2130968620;
        public static final int tab_title_contact = 2130968618;
        public static final int tab_title_sound = 2130968619;
        public static final int to_make_active = 2130968612;
        public static final int updated = 2130968617;
        public static final int user_agree = 2130968621;
        public static final int user_agree_note = 2130968622;
        public static final int vibrate_off = 2130968593;
        public static final int vibrate_on = 2130968594;
        public static final int voicecall = 2130968586;
        public static final int yes = 2130968610;
    }
}
