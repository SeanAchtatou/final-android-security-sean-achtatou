package com.smartapptechnology.soundprofiler;

import android.content.Context;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class SoundViewsAdapter {
    public static final int PROGRESS_NONE = -1;
    private SoundManagerActivity mActivity;
    private Integer[] mStreamTypes;
    private Integer[] mViewsLbls;
    private Integer[] mViewsSkbars;

    public SoundViewsAdapter(Context context, Integer[] viewLbls, Integer[] viewSkbarIds, Integer[] streamTypes, SeekBar.OnSeekBarChangeListener onChangelistener, View.OnClickListener onClickListener) {
        if (viewSkbarIds == null || streamTypes == null || viewLbls == null || context == null) {
            throw new IllegalArgumentException("Illegal null paramter value passed.");
        } else if (viewSkbarIds.length == streamTypes.length && viewLbls.length == streamTypes.length) {
            this.mActivity = (SoundManagerActivity) context;
            this.mViewsLbls = viewLbls;
            this.mViewsSkbars = viewSkbarIds;
            this.mStreamTypes = streamTypes;
            init(onChangelistener, onClickListener);
        } else {
            throw new IllegalArgumentException("Size of array paramters don't match");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer
     arg types: [java.lang.Integer, int]
     candidates:
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Object, boolean):java.lang.Object
      com.smartapptechnology.soundprofiler.IActivity.getSubComponent(java.lang.Object, boolean):T
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer */
    private void init(SeekBar.OnSeekBarChangeListener onChangelistener, View.OnClickListener onClickListener) {
        TextView txtLabel = (TextView) this.mActivity.findViewById(R.id.vibrate_lbl);
        txtLabel.setOnClickListener(onClickListener);
        flipVibTxt(txtLabel);
        for (int i = 0; i < this.mViewsSkbars.length; i++) {
            SeekBar seekBar = (SeekBar) this.mActivity.findViewById(this.mViewsSkbars[i].intValue());
            int volume = adjustStreamBar(seekBar, this.mStreamTypes[i].intValue(), -1);
            seekBar.setMax(this.mActivity.getSubComponent(this.mStreamTypes[i], true).intValue());
            seekBar.setKeyProgressIncrement(1);
            seekBar.setOnSeekBarChangeListener(onChangelistener);
            TextView txtLabel2 = (TextView) this.mActivity.findViewById(this.mViewsLbls[i].intValue());
            txtLabel2.setOnClickListener(onClickListener);
            modifyLabel(txtLabel2, volume);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer
     arg types: [java.lang.Integer, int]
     candidates:
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Object, boolean):java.lang.Object
      com.smartapptechnology.soundprofiler.IActivity.getSubComponent(java.lang.Object, boolean):T
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer */
    public void refresh() {
        flipVibTxt(null);
        for (int i = 0; i < this.mViewsSkbars.length; i++) {
            adjustStreamBar(this.mViewsSkbars[i].intValue(), this.mStreamTypes[i].intValue(), -1);
            modifyLabel((TextView) this.mActivity.findViewById(this.mViewsLbls[i].intValue()), this.mActivity.getSubComponent(this.mStreamTypes[i], false).intValue());
        }
    }

    private void adjustStreamBar(int viewId, int streamType, int progress) {
        adjustStreamBar((SeekBar) this.mActivity.findViewById(viewId), streamType, progress);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer
     arg types: [java.lang.Integer, int]
     candidates:
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Object, boolean):java.lang.Object
      com.smartapptechnology.soundprofiler.IActivity.getSubComponent(java.lang.Object, boolean):T
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer */
    private int adjustStreamBar(SeekBar seekBar, int streamType, int progress) {
        if (progress <= -1) {
            progress = this.mActivity.getSubComponent(Integer.valueOf(streamType), false).intValue();
        }
        seekBar.setProgress(progress);
        return progress;
    }

    public void modifyLabel(TextView label, int volume) {
        if (volume < 1) {
            label.setTextColor(-7829368);
        } else {
            label.setTextColor(-16711936);
        }
    }

    public void flipVibTxt(TextView txt) {
        if (txt == null) {
            txt = (TextView) this.mActivity.findViewById(R.id.vibrate_lbl);
        }
        int vibSetting = this.mActivity.getVibrateSetting();
        int mode = this.mActivity.getRingerMode();
        if (mode != 1 && (vibSetting == 0 || vibSetting == 2)) {
            txt.setText((int) R.string.vibrate_off);
            vibSetting = 0;
        } else if (mode == 1 || this.mActivity.getVibrateSetting() == 1) {
            txt.setText((int) R.string.vibrate_on);
            vibSetting = 1;
        }
        modifyLabel(txt, vibSetting);
    }

    public void modLblByStrmType(int streamType, int volume) {
        for (int i = 0; i < this.mStreamTypes.length; i++) {
            if (streamType == this.mStreamTypes[i].intValue()) {
                modifyLabel((TextView) this.mActivity.findViewById(this.mViewsLbls[i].intValue()), volume);
                return;
            }
        }
    }
}
