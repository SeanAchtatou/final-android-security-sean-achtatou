package com.smartapptechnology.soundprofiler;

import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.smartapptechnology.soundprofiler.mgr.EntityTone;
import com.smartapptechnology.soundprofiler.mgr.NotifierLookup;
import com.smartapptechnology.soundprofiler.mgr.Profile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NotifierRingtoneActivity extends AbstractRingtoneActivity {
    private static final int ID_ALARM = 202020;
    private static final int ID_NOTIF = 101010;

    /* access modifiers changed from: protected */
    public Profile onCreateMyListFromProfile(List<Profile> profiles) {
        final Profile profile = determineActiveProfile(profiles);
        this.mLookup = new NotifierLookup(this);
        this.mlistView = (ListView) findViewById(R.id.list_ringtone);
        AsyncTask<Void, Void, Void> loadTask = new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... params) {
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Void result) {
                EntityToneListAdapter adapter = NotifierRingtoneActivity.this.createListAdapter(profile, NotifierRingtoneActivity.this.mLookup);
                if (adapter == null) {
                    adapter = NotifierRingtoneActivity.this.createPredefined();
                }
                NotifierRingtoneActivity.this.setListAdapter(adapter, NotifierRingtoneActivity.this.mlistView, NotifierRingtoneActivity.this);
                NotifierRingtoneActivity.this.checkProfile(false);
                NotifierRingtoneActivity.this.mCommonViewListener.cancelProgressDialog();
                NotifierRingtoneActivity.this.removeTask(this, "LOADNOTIFTASK");
                NotifierRingtoneActivity.this.continueOnCreate(profile);
            }
        };
        showDialog(4);
        addTask(loadTask, "LOADNOTIFTASK");
        loadTask.execute(new Void[0]);
        return profile;
    }

    /* access modifiers changed from: private */
    public EntityToneListAdapter createPredefined() {
        ArrayList<EntityTone> dataList = new ArrayList<>();
        EntityTone entityTone = this.mLookup.getRecyclable(101010, getString(R.string.list_item_notif), null, this);
        if (entityTone.getValue(EntityTone.ENTITY_TONE_TYPE.NAME_RINGTONE) == null) {
            entityTone.setValue(EntityTone.ENTITY_TONE_TYPE.NAME_RINGTONE, getString(R.string.profile_current));
        }
        dataList.add(entityTone);
        EntityTone entityTone2 = this.mLookup.getRecyclable(202020, getString(R.string.list_item_alarm), null, this);
        if (entityTone2.getValue(EntityTone.ENTITY_TONE_TYPE.NAME_RINGTONE) == null) {
            entityTone2.setValue(EntityTone.ENTITY_TONE_TYPE.NAME_RINGTONE, getString(R.string.profile_current));
        }
        dataList.add(entityTone2);
        Profile dummy = new Profile();
        dummy.setValueList(dataList);
        return createListAdapter(dummy, this.mLookup);
    }

    public ToneType getToneType(EntityTone entityTone) {
        ToneType type = entityTone.getToneType();
        if (type != null) {
            return type;
        }
        if (entityTone.getId() == 101010) {
            type = ToneType.NOTIFICATION;
        } else if (entityTone.getId() == 202020) {
            type = ToneType.ALARM;
        }
        entityTone.setToneType(type);
        return type;
    }

    public void buildHelpContent(TableLayout tableLayout) {
        String[] alarmInstr = getResources().getStringArray(R.array.alarm_help);
        String[] profInstr = getResources().getStringArray(R.array.profile_help);
        ArrayList<String> helpInstrList = new ArrayList<>();
        helpInstrList.addAll(Arrays.asList(alarmInstr));
        helpInstrList.addAll(Arrays.asList(profInstr));
        String[] helpInstr = (String[]) helpInstrList.toArray(new String[1]);
        int size = helpInstr.length;
        for (int i = 0; i < size; i++) {
            TableRow tableRow = new TableRow(getApplicationContext());
            tableRow.setPadding(0, 10, 0, 0);
            TextView textView = new TextView(getApplicationContext());
            textView.setPadding(2, 2, 2, 2);
            textView.setGravity(3);
            textView.setText(String.valueOf(i + 1) + ".");
            tableRow.addView(textView);
            TextView textView2 = new TextView(getApplicationContext());
            textView2.setPadding(2, 2, 2, 2);
            textView2.setGravity(3);
            textView2.setText(helpInstr[i]);
            tableRow.addView(textView2);
            tableLayout.addView(tableRow);
        }
    }
}
