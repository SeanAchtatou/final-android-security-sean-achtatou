package com.smartapptechnology.soundprofiler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.smartapptechnology.soundprofiler.mgr.EntityTone;
import java.util.Collections;
import java.util.List;

public class EntityToneListAdapter extends BaseAdapter {
    private Context mContext;
    private List<EntityTone> mDataList;
    private EntityTone.ENTITY_TONE_TYPE[] mFromLabels;
    private LayoutInflater mInflater;
    private int mResource;
    private int[] mToFieldIds;

    public EntityToneListAdapter(Context context, int resource, EntityTone.ENTITY_TONE_TYPE[] from, int[] to, List<EntityTone> list) {
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.mResource = resource;
        this.mDataList = list;
        this.mToFieldIds = to;
        this.mFromLabels = from;
    }

    public Context getContext() {
        return this.mContext;
    }

    public int getPosition(EntityTone contactTone) {
        return this.mDataList.indexOf(contactTone);
    }

    public boolean contains(EntityTone contactTone) {
        return this.mDataList.contains(contactTone);
    }

    public boolean add(EntityTone contactTone) {
        boolean bool = this.mDataList.add(contactTone);
        Collections.sort(this.mDataList);
        notifyDataSetChanged();
        return bool;
    }

    public EntityTone remove(int idx) {
        EntityTone obj = this.mDataList.remove(idx);
        notifyDataSetChanged();
        return obj;
    }

    public void removeAll() {
        this.mDataList.clear();
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.mDataList.size();
    }

    public EntityTone getItem(int location) {
        return this.mDataList.get(location);
    }

    public long getItemId(int location) {
        return (long) location;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return createViewFromResource(position, convertView, parent, this.mResource);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View createViewFromResource(int position, View convertView, ViewGroup parent, int resource) {
        View v;
        if (convertView == null) {
            v = this.mInflater.inflate(resource, parent, false);
        } else {
            v = convertView;
        }
        bind2View(position, v);
        return v;
    }

    private void bind2View(int position, View view) {
        EntityTone entityTone = this.mDataList.get(position);
        if (entityTone != null) {
            EntityTone.ENTITY_TONE_TYPE[] from = this.mFromLabels;
            int[] to = this.mToFieldIds;
            int count = to.length;
            for (int i = 0; i < count; i++) {
                View v = view.findViewById(to[i]);
                if (v != null) {
                    if (v instanceof ImageView) {
                        ImageView imgView = (ImageView) v;
                        imgView.setImageBitmap(entityTone.getBitmap());
                        imgView.getLayoutParams().height = 55;
                        imgView.getLayoutParams().width = 50;
                        imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    } else {
                        String text = entityTone.getValue(from[i]);
                        if (text == null) {
                            return;
                        }
                        if (v instanceof TextView) {
                            ((TextView) v).setText(text);
                        } else {
                            throw new IllegalStateException(String.valueOf(v.getClass().getName()) + " is not a " + " view that can be bounds by this " + getClass().getSimpleName());
                        }
                    }
                }
            }
        }
    }

    public List<EntityTone> getDataList() {
        return this.mDataList;
    }
}
