package com.smartapptechnology.soundprofiler.graphics;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;
import java.util.Arrays;

public class LEDSeekBar extends SeekBar {
    private static final int COLOR_TXT_HIGHL = Color.parseColor("#FFFF00");
    private static final int COLOR_TXT_NORMAL = -1;
    private static final int COLOR_UNPAINTED = Color.parseColor("#E0E0E0");
    private static final int MIN_NUM_RECT = 24;
    private static final int RECTPAD = 1;
    private static final int RECT_HEIGHT = 30;
    private static final int RECT_WIDTH = 7;
    private static final Shader[] mSectColorShaders = {new LinearGradient(0.0f, 0.0f, 0.0f, 30.0f, -256, Color.parseColor("#F0F000"), Shader.TileMode.MIRROR), new LinearGradient(0.0f, 0.0f, 0.0f, 30.0f, -16711936, Color.parseColor("#00F000"), Shader.TileMode.MIRROR), new LinearGradient(0.0f, 0.0f, 0.0f, 30.0f, -65536, Color.parseColor("#F00000"), Shader.TileMode.MIRROR), new LinearGradient(0.0f, 0.0f, 0.0f, 30.0f, Color.parseColor("#F0F0F0"), COLOR_UNPAINTED, Shader.TileMode.MIRROR)};
    private int mAscent;
    private int mClipWdth;
    private int mHgth;
    private Paint mRectPaint;
    private int[] mRectPerProg = null;
    private Paint mTxtPaint;
    private int mWdth;

    public LEDSeekBar(Context context) {
        super(context);
        init();
    }

    public LEDSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LEDSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        this.mTxtPaint = new Paint(1);
        this.mTxtPaint.setTextSize(12.0f);
        this.mRectPaint = new Paint(1);
        setPadding(2, 2, 2, 2);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        int leftMost;
        int topMost;
        int rghtMost;
        int botMost;
        Rect clipArea = new Rect();
        if (!canvas.getClipBounds(clipArea)) {
            if (getLeft() + getPaddingLeft() > 10) {
                leftMost = 10;
            } else {
                leftMost = getLeft() + getPaddingLeft();
            }
            if (getTop() + getPaddingTop() > 10) {
                topMost = 10;
            } else {
                topMost = getTop() + getPaddingTop();
            }
            if (getRight() - getPaddingRight() < 110) {
                rghtMost = 110;
            } else {
                rghtMost = getRight() - getPaddingRight();
            }
            if (getRight() - getPaddingRight() < 80) {
                botMost = 80;
            } else {
                botMost = getBottom() - getPaddingBottom();
            }
            clipArea.set(leftMost, topMost, rghtMost, botMost);
        }
        int left = clipArea.left + getPaddingLeft();
        int rght = clipArea.right - getPaddingRight();
        int recTop = clipArea.top + getPaddingTop();
        int recBtm = recTop + 30;
        int txtTop = recBtm - this.mAscent;
        int numRect = (int) Math.floor(((double) rght) / 8.0d);
        int numProgLevels = getMax();
        int curProgLevel = getProgress();
        int startLft = clipArea.left + getPaddingLeft();
        int startRgh = RECT_WIDTH;
        int rectPerSect = (int) Math.floor(((double) numRect) / ((double) 3));
        int rectPerProg = (int) Math.floor(((double) numRect) / ((double) numProgLevels));
        int numRect2Paint = 0;
        int[] progRect = this.mRectPerProg;
        String curr = String.valueOf(curProgLevel);
        String max = String.valueOf(numProgLevels);
        if (this.mClipWdth != rght) {
            this.mClipWdth = rght;
            progRect = buildProgRectArray(numRect, rectPerProg, numProgLevels);
            this.mRectPerProg = progRect;
        }
        this.mTxtPaint.setColor(-1);
        if (isFocused()) {
            this.mTxtPaint.setColor(COLOR_TXT_HIGHL);
            Paint paint2 = new Paint();
            paint2.setColor(COLOR_TXT_HIGHL);
            paint2.setStyle(Paint.Style.STROKE);
            canvas.drawRect((float) (startLft - 1), (float) (recTop - 1), (float) (numRect * 8), (float) (recBtm + 1), paint2);
        }
        canvas.drawText("0", (float) left, (float) txtTop, this.mTxtPaint);
        canvas.drawText(curr, (float) (((left + rght) / 2) - (((int) Math.ceil((double) this.mTxtPaint.measureText(curr))) / 2)), (float) txtTop, this.mTxtPaint);
        canvas.drawText(max, (((float) rght) - this.mTxtPaint.measureText(max)) - 3.0f, (float) txtTop, this.mTxtPaint);
        if (curProgLevel > 0) {
            numRect2Paint = generateNumOfRect2Paint(curProgLevel, progRect);
        }
        int idx = -1;
        int counter = rectPerSect + 1;
        for (int i = 0; i < numRect; i++) {
            counter++;
            if (counter >= rectPerSect && i < numRect2Paint) {
                idx++;
                if (idx >= 3) {
                    idx = 2;
                }
                this.mRectPaint.setShader(mSectColorShaders[idx]);
                counter = 0;
            } else if (i >= numRect2Paint) {
                this.mRectPaint.setShader(mSectColorShaders[3]);
            }
            canvas.drawRect((float) startLft, (float) recTop, (float) startRgh, (float) recBtm, this.mRectPaint);
            startLft += 8;
            startRgh += 8;
        }
    }

    private int[] buildProgRectArray(int numRect, int rectPerProg, int numProgLevels) {
        double dNumRect = (double) numRect;
        double dNumProgLevels = (double) numProgLevels;
        int arrayMaxIdx = numProgLevels - 1;
        int arrayHalfIdx = arrayMaxIdx / 2;
        int[] progArray = new int[numProgLevels];
        Arrays.fill(progArray, rectPerProg);
        if (dNumRect % dNumProgLevels != 0.0d) {
            int diff = numRect - ((int) (Math.floor(dNumRect / dNumProgLevels) * ((double) numProgLevels)));
            if (diff > 0) {
                progArray[arrayHalfIdx] = progArray[arrayHalfIdx] + 1;
            }
            if (diff > 1) {
                progArray[arrayMaxIdx] = progArray[arrayMaxIdx] + 1;
            }
            if (diff > 2) {
                for (int i = 2; i <= diff; i++) {
                    if (!((arrayMaxIdx - i) + 1 == arrayHalfIdx || (arrayMaxIdx - i) + 1 == arrayMaxIdx)) {
                        int i2 = (arrayMaxIdx - i) + 1;
                        progArray[i2] = progArray[i2] + 1;
                    }
                }
            }
        }
        return progArray;
    }

    private int generateNumOfRect2Paint(int curProgress, int[] progRect) {
        int total = 0;
        for (int i = 0; i < curProgress; i++) {
            total += progRect[i];
        }
        return total;
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                return true;
            default:
                return super.onTouchEvent(event);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this.mWdth = measureWidth(widthMeasureSpec);
        this.mHgth = measureHeight(heightMeasureSpec);
        setMeasuredDimension(this.mWdth, this.mHgth);
    }

    private int measureWidth(int measureSpec) {
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        int sugWidth = getSuggestedMinimumWidth();
        if (specMode == 1073741824) {
            return specSize;
        }
        int result = getPaddingLeft() + 192 + getPaddingRight();
        if (result < sugWidth) {
            return sugWidth;
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        this.mAscent = (int) this.mTxtPaint.ascent();
        return ((int) (((float) (-this.mAscent)) + this.mTxtPaint.descent())) + getPaddingTop() + getPaddingBottom() + 30;
    }
}
