package com.smartapptechnology.soundprofiler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class SoundManagerReceiver extends BroadcastReceiver {
    private static SoundManagerActivity mActivity;

    public void registerActivity(SoundManagerActivity activity) {
        mActivity = activity;
    }

    public void unRegisterActivity() {
        mActivity = null;
    }

    public void onReceive(Context arg0, Intent arg1) {
        if (mActivity != null && !arg1.getBooleanExtra(SoundManagerActivity.SOUNDPROFILE, false) && !mActivity.hasWindowFocus()) {
            mActivity.refreshViews(true);
        }
    }
}
