package com.smartapptechnology.soundprofiler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.mgr.Profile;
import com.smartapptechnology.soundprofiler.mgr.ProfileManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class CommonViewListeners {
    public static final String BYPASS_DEBUG = "67666666323330493562";
    public static final String BYPASS_LONG = "6766666647412946484043423562";
    public static final String BYPASS_NAME = "676666664229413362";
    public static final String BYPASS_PREF = "67666666";
    public static final String BYPASS_SHARED_PREF_NAME = "BYPASS_SHARED_PREF_NAME";
    public static final String BYPASS_THIS = "67666666474129464843423362";
    public static final int DIALOG4NAME = 0;
    public static final int DIALOGALERT = 2;
    public static final int DIALOGBYPASS = 5;
    public static final int DIALOGHELP = 1;
    public static final int DIALOGPROGRESSUNDETERMINE = 4;
    public static final int DIALOGRATEAPP = 6;
    public static final int DIALOGUSERAGREEMENT = 3;
    public static boolean ISDEBUG = false;
    public static final int RATING_LEAVE_ME_ALONE = -5;
    public static final String SETTING_BYPASS_LONG = "setting_bypass_long";
    public static final String SETTING_RATE_APP = "setting_rate_app";
    public static final String SETTING_VERSION = "version";
    private static StringBuffer mAlertDislogMsg;
    /* access modifiers changed from: private */
    public static List<String> mAutoCompList;
    public static StringBuffer mBypassKeyEntry = null;
    private static SimpleDateFormat mDateFormat;
    public static boolean mIsAppLite = false;
    public static boolean mIsByPassShowName = false;
    public static String mPackageName;
    public static boolean mTriggerBypassMode = false;
    public static int mVersionCode;
    public static String mVersionName;
    /* access modifiers changed from: private */
    public IActivity mActivity;
    private ProgressDialog mDialogProgress;
    /* access modifiers changed from: private */
    public ProfileManager mProfileManager;

    public CommonViewListeners(IActivity iActivity, ProfileManager manager) {
        this.mActivity = iActivity;
        this.mProfileManager = manager;
        if (mBypassKeyEntry == null) {
            mBypassKeyEntry = new StringBuffer();
        }
        if (mDateFormat == null) {
            mDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        }
        if (mAutoCompList == null) {
            mAutoCompList = new ArrayList();
        }
        if (mAlertDislogMsg == null) {
            mAlertDislogMsg = new StringBuffer();
        }
    }

    public void destroy() {
        mIsByPassShowName = false;
        mTriggerBypassMode = false;
        mIsAppLite = false;
        ISDEBUG = false;
        mVersionCode = -1;
        mVersionName = null;
        this.mProfileManager = null;
        mAlertDislogMsg = null;
        this.mActivity = null;
        mBypassKeyEntry = null;
        mDateFormat = null;
        if (mAutoCompList != null) {
            mAutoCompList.clear();
        }
        mAutoCompList = null;
    }

    public AdapterView.OnItemSelectedListener getItemSelectedListener() {
        return new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                final Profile profile = (Profile) parent.getItemAtPosition(pos);
                if (pos >= 1) {
                    AsyncTask<Void, Void, Integer> loadTask = new AsyncTask<Void, Void, Integer>() {
                        /* access modifiers changed from: protected */
                        public Integer doInBackground(Void... arg0) {
                            return null;
                        }

                        /* access modifiers changed from: protected */
                        public void onPostExecute(Integer result) {
                            if (CommonViewListeners.this.mActivity.processSpinnerItemSelected(profile)) {
                                CommonViewListeners.this.mActivity.enableStoreBtn(false);
                            }
                            CommonViewListeners.this.cancelProgressDialog();
                            CommonViewListeners.this.mActivity.removeTask(this, "LOADTASK");
                        }
                    };
                    ((Activity) CommonViewListeners.this.mActivity).showDialog(4);
                    CommonViewListeners.this.mActivity.addTask(loadTask, "LOADTASK");
                    loadTask.execute(new Void[0]);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        };
    }

    public View.OnClickListener getOnClickListener() {
        return new View.OnClickListener() {
            /* Debug info: failed to restart local var, previous not found, register: 1 */
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_save_Ringtone /*2131230735*/:
                    case R.id.save_btn /*2131230744*/:
                        ((Activity) CommonViewListeners.this.mActivity).showDialog(0);
                        return;
                    default:
                        CommonViewListeners.this.mActivity.processOnClick(v);
                        return;
                }
            }
        };
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public boolean processOnCreateOptionMenu(Menu menu, int resource, Activity act) {
        Activity activity;
        if (act == null) {
            activity = (Activity) this.mActivity;
        } else {
            activity = act;
        }
        activity.getMenuInflater().inflate(resource, menu);
        if (!(activity instanceof NotifierRingtoneActivity) || resource != R.menu.ringtone_menu) {
            return true;
        }
        menu.removeItem(R.id.menu_contacts);
        menu.removeItem(R.id.menu_contactsall);
        menu.removeItem(R.id.menu_clearall);
        return true;
    }

    public boolean processOnPrepareOptionsMenu(Menu menu, Button storeBtn, Spinner spinner) {
        Activity act = (Activity) this.mActivity;
        MenuItem delMenuItem = menu.findItem(R.id.menu_delete);
        Profile profile = (Profile) spinner.getSelectedItem();
        if (storeBtn.isEnabled() || profile.getName().indexOf(act.getString(R.string.profile_current)) != -1) {
            delMenuItem.setEnabled(false);
            delMenuItem.setTitle(act.getString(R.string.menu_item_delete));
        } else {
            delMenuItem.setEnabled(true);
            delMenuItem.setTitle(String.valueOf(act.getString(R.string.menu_item_delete)) + " " + act.getString(R.string.profile) + " '" + profile.getName() + "'");
            Intent i = new Intent();
            i.putExtra("com.smartapptechnology.profile.NAME", profile.getName());
            delMenuItem.setIntent(i);
        }
        MenuItem clearAllMenuItem = menu.findItem(R.id.menu_clearall);
        if (clearAllMenuItem != null) {
            if (this.mActivity.getMainComponents() == null) {
                clearAllMenuItem.setEnabled(false);
            } else {
                clearAllMenuItem.setEnabled(true);
            }
        }
        return true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    public boolean processOnOptionsItemSelected(MenuItem item) {
        int idx;
        Activity act = (Activity) this.mActivity;
        switch (item.getItemId()) {
            case R.id.menu_delete /*2131230760*/:
                Intent i = item.getIntent();
                if (i != null && (idx = this.mProfileManager.findProfilePosInList(i.getStringExtra("com.smartapptechnology.profile.NAME"), true)) > -1) {
                    Profile profile = this.mProfileManager.removeProfile(idx);
                    if (profile.getName().indexOf(act.getString(R.string.profile_current)) > -1) {
                        return true;
                    }
                    if (profile != null) {
                        Toast.makeText(act, String.valueOf(act.getString(R.string.profile)) + " '" + profile.getName() + "' " + act.getString(R.string.removed), 0).show();
                    }
                    this.mActivity.checkProfile(false);
                }
                return true;
            case R.id.menu_exit /*2131230761*/:
            default:
                return false;
            case R.id.menu_help /*2131230762*/:
                ((Activity) this.mActivity).showDialog(1);
                return true;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void processOnCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo, int resource, Activity act) {
        Activity activity;
        if (act == null) {
            activity = (Activity) this.mActivity;
        } else {
            activity = act;
        }
        activity.getMenuInflater().inflate(resource, menu);
        if ((activity instanceof NotifierRingtoneActivity) && resource == R.menu.ringtone_context) {
            menu.removeItem(R.id.menu_context_delete);
        }
    }

    public Dialog processOnCreateDialog(int id, Activity paramActivity) {
        final Activity activity;
        String string;
        if (paramActivity == null) {
            activity = (Activity) this.mActivity;
        } else {
            activity = paramActivity;
        }
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService("layout_inflater");
        switch (id) {
            case 0:
                View layout = layoutInflater.inflate((int) R.layout.layout_save_dialog, (ViewGroup) activity.findViewById(R.id.layout_dialog_save));
                AlertDialog.Builder dialogSave = new AlertDialog.Builder(activity);
                dialogSave.setView(layout);
                dialogSave.setTitle((int) R.string.new_profile_name);
                dialogSave.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                dialogSave.setPositiveButton((int) R.string.save_new, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Profile profile;
                        Dialog dlg = (Dialog) dialog;
                        AutoCompleteTextView autoComplFld = (AutoCompleteTextView) dlg.findViewById(R.id.dialog_name_edtxt);
                        String name = autoComplFld.getText().toString().trim();
                        String values = CommonViewListeners.this.mProfileManager.deflateCurrentMainNSub(CommonViewListeners.this.mActivity.getMainComponents());
                        if (!"".equals(name) && values != null && !"".equals(values)) {
                            boolean willAdd = true;
                            int location = CommonViewListeners.this.mProfileManager.findProfilePosInList(name, true);
                            if (location > -1) {
                                profile = CommonViewListeners.this.mProfileManager.getProfiles().get(location);
                                Toast.makeText(activity, String.valueOf(activity.getString(R.string.replaced)) + " " + activity.getString(R.string.profile) + " '" + name + "'", 0).show();
                            } else {
                                profile = new Profile();
                                profile.setName(name);
                                willAdd = CommonViewListeners.this.mProfileManager.addProfile(profile);
                                if (autoComplFld.getAdapter() == null) {
                                    CommonViewListeners.this.createAutoComplete(autoComplFld);
                                }
                                CommonViewListeners.add2AutoComplete(CommonViewListeners.mAutoCompList, profile.getName());
                            }
                            profile.setValues(values);
                            if (willAdd) {
                                CommonViewListeners.this.mActivity.preSaveOp(profile);
                            }
                            CommonViewListeners.this.mActivity.checkProfile(false);
                            dlg.dismiss();
                        }
                    }
                });
                return dialogSave.create();
            case 1:
                View layout2 = layoutInflater.inflate((int) R.layout.layout_help_dialog, (ViewGroup) activity.findViewById(R.id.layout_dialog_help));
                AlertDialog.Builder dialogHelp = new AlertDialog.Builder(activity);
                dialogHelp.setView(layout2);
                dialogHelp.setTitle((int) R.string.help_title);
                StringBuffer ver = new StringBuffer();
                if (mIsAppLite) {
                    string = activity.getString(R.string.app_name_lite);
                } else {
                    string = activity.getString(R.string.app_name);
                }
                ver.append(string).append(" (version " + mVersionName + ").\n");
                TextView cntView = (TextView) layout2.findViewById(R.id.lbl_dialog_help_contact);
                ver.append(cntView.getText().toString());
                cntView.setText(ver.toString());
                this.mActivity.buildHelpContent((TableLayout) layout2.findViewById(R.id.dialog_help_table));
                dialogHelp.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                return dialogHelp.create();
            case 2:
                AlertDialog.Builder dialogAlert = new AlertDialog.Builder(activity);
                dialogAlert.setCancelable(true).setMessage(mAlertDislogMsg.toString()).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                return dialogAlert.create();
            case DIALOGUSERAGREEMENT /*3*/:
            default:
                return null;
            case DIALOGPROGRESSUNDETERMINE /*4*/:
                this.mDialogProgress = new ProgressDialog(activity);
                this.mDialogProgress.setIndeterminate(true);
                this.mDialogProgress.setCancelable(false);
                this.mDialogProgress.setMessage(activity.getText(R.string.loading));
                return this.mDialogProgress;
            case DIALOGBYPASS /*5*/:
                View layout3 = layoutInflater.inflate((int) R.layout.layout_save_dialog, (ViewGroup) activity.findViewById(R.id.layout_dialog_save));
                AlertDialog.Builder dialogBypass = new AlertDialog.Builder(activity);
                dialogBypass.setView(layout3);
                dialogBypass.setTitle("Bypass");
                dialogBypass.setCancelable(true);
                dialogBypass.setPositiveButton((int) R.string.save_new, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String value = ((AutoCompleteTextView) ((Dialog) dialog).findViewById(R.id.dialog_name_edtxt)).getText().toString().trim();
                        if (!"".equals(value)) {
                            try {
                                ProfileManager.storePref(activity, CommonViewListeners.BYPASS_SHARED_PREF_NAME, CommonViewListeners.SETTING_BYPASS_LONG, CommonViewListeners.addToDate(Integer.parseInt(value)));
                                CommonViewListeners.mIsAppLite = false;
                                dialog.dismiss();
                            } catch (NumberFormatException e) {
                                ProfileManager.removePref(activity, CommonViewListeners.BYPASS_SHARED_PREF_NAME, CommonViewListeners.SETTING_BYPASS_LONG);
                                CommonViewListeners.mIsAppLite = true;
                                dialog.dismiss();
                                activity.finish();
                            }
                        }
                    }
                });
                return dialogBypass.create();
        }
    }

    public void processOnPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 0:
                AutoCompleteTextView edFld = (AutoCompleteTextView) dialog.findViewById(R.id.dialog_name_edtxt);
                edFld.setText((int) R.string.empty);
                createAutoComplete(edFld);
                return;
            case 1:
                if (mIsByPassShowName) {
                    ((TextView) dialog.findViewById(R.id.lbl_dialog_help_devname)).setText("Designed and Developed by Terence O. Moore, Jr for SmartApp Technology, LLC");
                    mIsByPassShowName = false;
                    return;
                }
                return;
            case 2:
                ((AlertDialog) dialog).setMessage(mAlertDislogMsg.toString());
                newAlertDialogMsg("");
                return;
            default:
                return;
        }
    }

    public void cancelProgressDialog() {
        if (this.mDialogProgress != null) {
            this.mDialogProgress.cancel();
        }
    }

    /* access modifiers changed from: package-private */
    public <T> void check4Profile(T[] typeArr, Spinner spinner) {
        int position = this.mProfileManager.findProfilePosInList(this.mProfileManager.deflateCurrentMainNSub(typeArr), false);
        if (position <= 0) {
            position = 0;
            this.mActivity.enableStoreBtn(true);
        } else {
            this.mActivity.enableStoreBtn(false);
        }
        spinner.setSelection(position);
    }

    public static void newAlertDialogMsg(String msg) {
        synchronized (mAlertDislogMsg) {
            mAlertDislogMsg.delete(0, mAlertDislogMsg.length());
        }
        appendAlertDialogMsg(msg);
    }

    public static void appendAlertDialogMsg(String msg) {
        mAlertDislogMsg.append("\n" + msg);
    }

    public void byPassOps(Activity activity) {
        String msg = "Sorry, Nothing";
        if (BYPASS_NAME.equals(mBypassKeyEntry.toString())) {
            mIsByPassShowName = true;
            msg = "Bypass Code: Name Enabled";
        } else if (BYPASS_DEBUG.equals(mBypassKeyEntry.toString())) {
            ISDEBUG = true;
            msg = "Bypass Code: Debug Enabled";
        } else if (BYPASS_THIS.equals(mBypassKeyEntry.toString())) {
            mIsAppLite = false;
            msg = "Bypass Code: Single Use Enabled";
        } else if (BYPASS_LONG.equals(mBypassKeyEntry.toString())) {
            msg = "Bypass Code: Multiple Use Enabled";
            activity.showDialog(5);
        }
        Toast.makeText(activity, msg, 1).show();
    }

    /* access modifiers changed from: private */
    public void createAutoComplete(AutoCompleteTextView autoComplFld) {
        ArrayAdapter<String> la;
        ListAdapter dummyListAdap = autoComplFld.getAdapter();
        if (dummyListAdap != null) {
            la = (ArrayAdapter) dummyListAdap;
        } else {
            la = new ArrayAdapter<>(((Activity) this.mActivity).getApplication(), (int) R.layout.layout_auto_complete_list_item, mAutoCompList);
        }
        if (dummyListAdap == null) {
            autoComplFld.setAdapter(la);
        }
        for (Profile profile : this.mProfileManager.getProfiles()) {
            add2AutoComplete(mAutoCompList, profile.toString());
        }
    }

    public static void add2AutoComplete(List<String> list, String newItem) {
        if (list != null && !list.contains(newItem)) {
            list.add(newItem);
        }
    }

    public static Date parseDate(String date) {
        try {
            return mDateFormat.parse(date);
        } catch (Exception e) {
            ErrorLog.log(e, ErrorLog.Levels.LOW);
            return null;
        }
    }

    public static String addToDate(int numOfDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(5, numOfDays);
        return mDateFormat.format(calendar.getTime());
    }

    public static String removeNonDigit(String strDate) {
        StringBuffer sb = new StringBuffer();
        int length = strDate.length();
        for (int i = 0; i < length; i++) {
            char ch = strDate.charAt(i);
            if (ch >= '0' && ch <= '9') {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    public static void captureIntentInfo(String preMsg, Intent intent, boolean append) {
        StringBuffer sb = new StringBuffer(String.valueOf(preMsg) + "\nIntent...");
        try {
            sb.append("\ngetData= " + intent.getData()).append("\nPar-PK_URI= " + intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI")).append("\nPar-EX_URI= " + intent.getParcelableExtra("android.intent.extra.ringtone.EXISTING_URI")).append("\nPar-TYPE= " + intent.getParcelableExtra("android.intent.extra.ringtone.TYPE")).append("\nPar-INCLUDE DRM= " + intent.getParcelableExtra("android.intent.extra.ringtone.INCLUDE_DRM")).append("\nPar-TITLE= " + intent.getParcelableExtra("android.intent.extra.ringtone.TITLE"));
            Bundle extras = intent.getExtras();
            if (extras != null) {
                sb.append("\nExt-PK_URI= " + extras.get("android.intent.extra.ringtone.PICKED_URI")).append("\nExt-EX_URI= " + extras.get("android.intent.extra.ringtone.EXISTING_URI")).append("\nExt-TYPE= " + extras.getInt("android.intent.extra.ringtone.TYPE")).append("\nExt-TONETYPE= " + ToneType.valueOf(extras.getInt("android.intent.extra.ringtone.TYPE"))).append("\nExt-INCLUDE DRM= " + extras.getBoolean("android.intent.extra.ringtone.INCLUDE_DRM")).append("\nExt-TITLE= " + extras.getString("android.intent.extra.ringtone.TITLE"));
            }
        } catch (Exception e) {
            ErrorLog.log(e, ErrorLog.Levels.DEBUG);
        }
        if (append) {
            appendAlertDialogMsg(sb.toString());
        } else {
            newAlertDialogMsg(sb.toString());
        }
    }
}
