package com.smartapptechnology.soundprofiler.mgr;

import android.graphics.Bitmap;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.ToneType;
import java.util.Collection;
import java.util.HashMap;

public class EntityTone implements Comparable<EntityTone> {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$EntityTone$ENTITY_TONE_TYPE;
    private Exception ex;
    private boolean isDisplayable = true;
    private Bitmap mBitmap;
    private long mId;
    private StringBuffer mSharedNameContainer;
    private ToneType mToneType;
    private HashMap<String, String> mValuesMap = new HashMap<>();

    public enum ENTITY_TONE_TYPE {
        NAME,
        NAME_RINGTONE,
        URI_RINGTONE,
        BITMAP
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$EntityTone$ENTITY_TONE_TYPE() {
        int[] iArr = $SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$EntityTone$ENTITY_TONE_TYPE;
        if (iArr == null) {
            iArr = new int[ENTITY_TONE_TYPE.values().length];
            try {
                iArr[ENTITY_TONE_TYPE.BITMAP.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ENTITY_TONE_TYPE.NAME.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ENTITY_TONE_TYPE.NAME_RINGTONE.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ENTITY_TONE_TYPE.URI_RINGTONE.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$EntityTone$ENTITY_TONE_TYPE = iArr;
        }
        return iArr;
    }

    EntityTone() {
    }

    public long getId() {
        return this.mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public void setMId_Str(String longValue) {
        if (longValue != null && !longValue.equals("")) {
            try {
                this.mId = Long.parseLong(longValue);
            } catch (NumberFormatException e) {
                ErrorLog.log(e, ErrorLog.Levels.LOW);
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public String setValue(ENTITY_TONE_TYPE type, String value) {
        if (type == null) {
            return null;
        }
        switch ($SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$EntityTone$ENTITY_TONE_TYPE()[type.ordinal()]) {
            case 1:
                if (this.mSharedNameContainer == null) {
                    this.mSharedNameContainer = new StringBuffer();
                }
                String oldValue = this.mSharedNameContainer.toString();
                this.mSharedNameContainer.delete(0, this.mSharedNameContainer.length());
                this.mSharedNameContainer.append(value);
                return oldValue;
            default:
                return this.mValuesMap.put(type.name(), value);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public String getValue(ENTITY_TONE_TYPE type) {
        switch ($SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$EntityTone$ENTITY_TONE_TYPE()[type.ordinal()]) {
            case 1:
                if (this.mSharedNameContainer == null) {
                    return null;
                }
                return this.mSharedNameContainer.toString();
            default:
                return this.mValuesMap.get(type.name());
        }
    }

    public Exception getException() {
        return this.ex;
    }

    public void setException(Exception ex2) {
        this.ex = ex2;
    }

    public boolean isDisplayable() {
        return this.isDisplayable;
    }

    public void setDisplayable(boolean isDisplayable2) {
        this.isDisplayable = isDisplayable2;
    }

    public ToneType getToneType() {
        return this.mToneType;
    }

    public void setToneType(ToneType toneType) {
        this.mToneType = toneType;
    }

    public Bitmap getBitmap() {
        return this.mBitmap;
    }

    public void setBitmap(Bitmap icon) {
        this.mBitmap = icon;
    }

    public String toString() {
        if (this.mSharedNameContainer == null) {
            return null;
        }
        return this.mSharedNameContainer.toString();
    }

    public StringBuffer getSharedNameContainer() {
        return this.mSharedNameContainer;
    }

    public void setSharedNameContainer(StringBuffer sharedNameContainer) {
        this.mSharedNameContainer = sharedNameContainer;
    }

    public int compareTo(EntityTone another) {
        String oName;
        if (another == null || (oName = another.toString()) == null) {
            return -1;
        }
        String name = toString();
        if (name == null) {
            return 1;
        }
        return name.toLowerCase().compareTo(oName.toLowerCase());
    }

    public boolean equals(Object o) {
        return this.mId == ((EntityTone) o).mId && getClass() == o.getClass() && hashCode() == o.hashCode();
    }

    public int hashCode() {
        Collection<String> collection = this.mValuesMap.values();
        StringBuilder sb = new StringBuilder();
        if (collection != null) {
            for (String s : collection) {
                sb.append(s);
            }
        }
        return sb.toString().hashCode() + ((int) this.mId);
    }

    public EntityTone deepClone() throws CloneNotSupportedException {
        return (EntityTone) clone();
    }

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        EntityTone obj = new EntityTone();
        obj.setDisplayable(this.isDisplayable);
        obj.setException(this.ex);
        obj.setId(this.mId);
        obj.setToneType(this.mToneType);
        obj.setBitmap(this.mBitmap);
        obj.setSharedNameContainer(this.mSharedNameContainer);
        for (String key : this.mValuesMap.keySet()) {
            obj.setValue(ENTITY_TONE_TYPE.valueOf(key), this.mValuesMap.get(key));
        }
        return obj;
    }

    public void destroy() {
        this.ex = null;
        if (this.mValuesMap != null) {
            this.mValuesMap.clear();
        }
        this.mValuesMap = null;
        this.mToneType = null;
        this.mBitmap = null;
        this.mSharedNameContainer = null;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        destroy();
        super.finalize();
    }
}
