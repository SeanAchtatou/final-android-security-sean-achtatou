package com.smartapptechnology.soundprofiler.mgr;

import android.app.Activity;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import com.smartapptechnology.soundprofiler.CommonViewListeners;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.IActivity;
import com.smartapptechnology.soundprofiler.R;
import com.smartapptechnology.soundprofiler.ToneType;
import com.smartapptechnology.soundprofiler.mgr.EntityTone;
import java.util.ArrayList;

public final class RingToneHandler {
    /* access modifiers changed from: private */
    public static ArrayList<Runnable> mPlayList = new ArrayList<>();
    private static final Intent mRingtonePickerIntent = new Intent("android.intent.action.RINGTONE_PICKER");

    public static class RingtoneException extends Exception {
        private static final long serialVersionUID = 1;
        String msg;

        public RingtoneException(String msg2) {
            this(msg2, null);
        }

        public RingtoneException(String msg2, Exception ex) {
            super(msg2, ex);
            this.msg = msg2;
        }

        public String getMessage() {
            return this.msg;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static Intent getPickerIntent(ToneType toneType, Uri uri, IActivity activity) {
        if (uri == null) {
            uri = RingtoneManager.getDefaultUri(toneType.getCode());
        }
        mRingtonePickerIntent.putExtra("android.intent.extra.ringtone.EXISTING_URI", uri);
        mRingtonePickerIntent.putExtra("android.intent.extra.ringtone.TYPE", toneType.getCode());
        mRingtonePickerIntent.putExtra("android.intent.extra.ringtone.INCLUDE_DRM", true);
        mRingtonePickerIntent.putExtra("android.intent.extra.ringtone.TITLE", toneType.toString());
        if (CommonViewListeners.ISDEBUG) {
            CommonViewListeners.captureIntentInfo("\nPick Tone...", mRingtonePickerIntent, true);
        }
        return mRingtonePickerIntent;
    }

    public static ToneType determineIntentType(Intent invokingIntent) {
        if (invokingIntent == null) {
            return null;
        }
        try {
            Uri dataUri = invokingIntent.getData();
            Bundle extras = invokingIntent.getExtras();
            if (CommonViewListeners.ISDEBUG) {
                CommonViewListeners.captureIntentInfo("\nDetermine", invokingIntent, true);
            }
            if (dataUri != null && RingtoneManager.getDefaultUri(ToneType.SOUND.getCode()).compareTo(dataUri) == 0) {
                return ToneType.SOUND;
            }
            if ((dataUri != null && RingtoneManager.getDefaultUri(ToneType.RINGTONE.getCode()).compareTo(dataUri) == 0) || (extras != null && ToneType.RINGTONE.getCode() == extras.getInt("android.intent.extra.ringtone.TYPE"))) {
                return ToneType.RINGTONE;
            }
            if ((dataUri != null && RingtoneManager.getDefaultUri(ToneType.NOTIFICATION.getCode()).compareTo(dataUri) == 0) || (extras != null && ToneType.NOTIFICATION.getCode() == extras.getInt("android.intent.extra.ringtone.TYPE"))) {
                return ToneType.NOTIFICATION;
            }
            if ((dataUri != null && RingtoneManager.getDefaultUri(ToneType.ALARM.getCode()).compareTo(dataUri) == 0) || (extras != null && ToneType.ALARM.getCode() == extras.getInt("android.intent.extra.ringtone.TYPE"))) {
                return ToneType.ALARM;
            }
            return null;
        } catch (Exception e) {
            ErrorLog.log(e, ErrorLog.Levels.LOW);
        }
    }

    public static void checkNSetRingTone(EntityTone entityTone, String toneUriAsStr, Activity activityInstance) {
        ToneType type = entityTone.getToneType();
        if (type == ToneType.OTHERS || type == null) {
            type = ((IActivity) activityInstance).getToneType(entityTone);
        }
        Uri ringtoneUri = convert(toneUriAsStr, type.getCode(), activityInstance);
        Ringtone ringTone = getRingtone(ringtoneUri, activityInstance);
        if (ringTone != null) {
            entityTone.setValue(EntityTone.ENTITY_TONE_TYPE.NAME_RINGTONE, ringTone.getTitle(activityInstance));
            entityTone.setValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE, Utility.showParsingConflicts(ringtoneUri.toString()));
            return;
        }
        String err = activityInstance.getText(R.string.msg_ringtone_could_not_load).toString().replace("#", entityTone.getValue(EntityTone.ENTITY_TONE_TYPE.NAME));
        entityTone.setDisplayable(false);
        entityTone.setException(new RingtoneException(err));
    }

    public static Uri convert(String toneUriAsStr, int ringToneType, Activity activity) {
        if (toneUriAsStr == null || "".equals(toneUriAsStr)) {
            toneUriAsStr = null;
        }
        String nToneUriAsStr = Utility.showParsingConflicts(toneUriAsStr);
        if (nToneUriAsStr != null) {
            return Uri.parse(nToneUriAsStr);
        }
        return getDefault(activity, ringToneType);
    }

    public static Uri getDefault(Activity activity, int ringToneType) {
        try {
            return RingtoneManager.getActualDefaultRingtoneUri(activity, ringToneType);
        } catch (Exception ex) {
            ErrorLog.log(ex, ErrorLog.Levels.LOW);
            return RingtoneManager.getDefaultUri(ringToneType);
        }
    }

    protected static Ringtone getRingtone(Uri ringtoneUri, Activity activityInstance) {
        if (ringtoneUri == null) {
            return null;
        }
        return RingtoneManager.getRingtone(activityInstance, ringtoneUri);
    }

    public static void setRingtone(Activity activity, int type, Uri ringtoneUri) {
        RingtoneManager.setActualDefaultRingtoneUri(activity, type, ringtoneUri);
    }

    public static void playTone(String toneUriAsStr, int ringToneType, Activity activityInstance) {
        Runnable r;
        boolean bool;
        Ringtone ringTone = getRingtone(convert(toneUriAsStr, ringToneType, activityInstance), activityInstance);
        if (!ringTone.isPlaying()) {
            if (mPlayList == null) {
                mPlayList = new ArrayList<>();
            }
            synchronized (mPlayList) {
                ArrayList<Runnable> arrayList = mPlayList;
                r = createKillThread(ringTone);
                bool = arrayList.add(r);
                mPlayList.notifyAll();
            }
            if (bool && r != null) {
                ringTone.play();
                Thread t = new Thread(r, "Thread playing tone " + ringTone.getTitle(activityInstance));
                t.setDaemon(true);
                t.start();
            }
        }
    }

    private static Runnable createKillThread(final Ringtone ringtone) {
        return new Runnable() {
            public void run() {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                }
                ringtone.stop();
                synchronized (RingToneHandler.mPlayList) {
                    RingToneHandler.mPlayList.remove(this);
                    RingToneHandler.mPlayList.notifyAll();
                }
            }
        };
    }

    public static void destroy() {
        if (mPlayList != null) {
            mPlayList.clear();
        }
        mPlayList = null;
    }
}
