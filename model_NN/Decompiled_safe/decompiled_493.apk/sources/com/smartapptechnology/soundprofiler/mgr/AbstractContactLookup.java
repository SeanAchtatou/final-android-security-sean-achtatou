package com.smartapptechnology.soundprofiler.mgr;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import com.smartapptechnology.soundprofiler.CommonViewListeners;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.IActivity;
import com.smartapptechnology.soundprofiler.ToneType;
import com.smartapptechnology.soundprofiler.mgr.EntityTone;
import java.util.HashMap;
import java.util.List;

public abstract class AbstractContactLookup extends AbstractLookup {
    protected static IActivity mActivityInstance;
    private static AbstractContactLookup mInstance;

    /* access modifiers changed from: protected */
    public abstract Uri buildContactRingtoneUri(Uri uri, String str);

    public abstract EntityTone load(ContentResolver contentResolver, Uri uri);

    public static AbstractContactLookup getInstance(IActivity activity) {
        String className;
        if (mInstance == null) {
            if (Integer.parseInt(Build.VERSION.SDK) < 5) {
                className = "com.smartapptechnology.soundprofiler.mgr.ContactLookupSdk3_4";
            } else {
                className = "com.smartapptechnology.soundprofiler.mgr.ContactLookupSdk5";
            }
            try {
                mInstance = (AbstractContactLookup) Class.forName(className).asSubclass(AbstractContactLookup.class).newInstance();
            } catch (Exception e) {
                ErrorLog.log(e, ErrorLog.Levels.SEVERE);
            }
            mActivityInstance = activity;
        }
        return mInstance;
    }

    public void destroy() {
        super.destroy();
        mActivityInstance = null;
        mInstance = null;
    }

    /* access modifiers changed from: protected */
    public final int updateToneRecord(ContentResolver contentResolver, Uri contactUri, List<EntityTone> entityToneList, String column_customRingtone) {
        int rowUpdated = 0;
        if (entityToneList != null && entityToneList.size() > 0) {
            ContentValues values = new ContentValues();
            try {
                for (EntityTone e : entityToneList) {
                    String toneAsStr = e.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE);
                    String id = String.valueOf(e.getId());
                    if (CommonViewListeners.ISDEBUG) {
                        CommonViewListeners.appendAlertDialogMsg("\nCntct Tone Update...\nId= " + e.getId() + "\nName= " + e.getValue(EntityTone.ENTITY_TONE_TYPE.NAME) + "\nToneName= " + e.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE) + "\nUri= " + toneAsStr);
                    }
                    Uri ringToneUri = buildContactRingtoneUri(contactUri, id);
                    values.clear();
                    values.put(column_customRingtone, toneAsStr);
                    rowUpdated += contentResolver.update(ringToneUri, values, null, null);
                }
            } catch (Exception e2) {
                ErrorLog.log(e2, ErrorLog.Levels.SEVERE);
            }
        }
        return rowUpdated;
    }

    /* Debug info: failed to restart local var, previous not found, register: 20 */
    /* access modifiers changed from: protected */
    public final boolean areRingtonesActive(ContentResolver contentResolver, Uri contactUri, List<EntityTone> entityToneList, String column_id, String column_customRingtone, String column_name, String columns4Selection) {
        boolean bool = false;
        if (entityToneList != null && entityToneList.size() > 0) {
            Cursor cursor = null;
            try {
                HashMap<Long, String> checkMap = new HashMap<>();
                StringBuffer sb = new StringBuffer();
                for (EntityTone e : entityToneList) {
                    if (sb.length() > 0) {
                        sb.append(Utility.SEP);
                    }
                    sb.append(e.getId());
                    checkMap.put(Long.valueOf(e.getId()), e.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE));
                }
                if (columns4Selection == null) {
                    columns4Selection = column_id;
                }
                cursor = contentResolver.query(contactUri, new String[]{column_id, column_customRingtone, column_name}, String.valueOf(columns4Selection) + " in (" + sb.toString() + ")", null, null);
                if (cursor.moveToFirst()) {
                    bool = true;
                    while (true) {
                        long id = cursor.getLong(0);
                        String dbTone = cursor.getString(1);
                        String name = cursor.getString(2);
                        String curTone = (String) checkMap.get(Long.valueOf(id));
                        if (dbTone == null) {
                            if (RingToneHandler.getDefault((Activity) mActivityInstance, ToneType.RINGTONE.getCode()) != null) {
                                dbTone = RingToneHandler.getDefault((Activity) mActivityInstance, ToneType.RINGTONE.getCode()).toString();
                            } else {
                                dbTone = null;
                            }
                        }
                        if (CommonViewListeners.ISDEBUG) {
                            CommonViewListeners.appendAlertDialogMsg("\nCntct Tone Check...\nTone Active ?\nId= " + id + "\nName=" + name + "\ndbTone= " + dbTone + "\ncrTone= " + curTone);
                        }
                        if (dbTone != null && dbTone.equalsIgnoreCase(curTone)) {
                            if (!cursor.moveToNext()) {
                                break;
                            }
                        } else {
                            bool = false;
                        }
                    }
                    bool = false;
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e2) {
                bool = false;
                ErrorLog.log(e2, ErrorLog.Levels.SEVERE);
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return bool;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.smartapptechnology.soundprofiler.mgr.EntityTone> loadAll(android.content.ContentResolver r12, android.net.Uri r13, java.lang.String r14, java.lang.String r15, java.lang.String r16) {
        /*
            r11 = this;
            r6 = 0
            r8 = 0
            r0 = 3
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ Exception -> 0x005f }
            r0 = 0
            r2[r0] = r14     // Catch:{ Exception -> 0x005f }
            r0 = 1
            r2[r0] = r15     // Catch:{ Exception -> 0x005f }
            r0 = 2
            r2[r0] = r16     // Catch:{ Exception -> 0x005f }
            r3 = 0
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005f }
            java.lang.String r1 = java.lang.String.valueOf(r15)     // Catch:{ Exception -> 0x005f }
            r0.<init>(r1)     // Catch:{ Exception -> 0x005f }
            java.lang.String r1 = " ASC"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x005f }
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x005f }
            r0 = r12
            r1 = r13
            android.database.Cursor r6 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x005f }
            boolean r0 = r6.moveToFirst()     // Catch:{ Exception -> 0x005f }
            if (r0 == 0) goto L_0x0058
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ Exception -> 0x005f }
            r9.<init>()     // Catch:{ Exception -> 0x005f }
        L_0x0034:
            r0 = 0
            long r1 = r6.getLong(r0)     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
            r0 = 1
            java.lang.String r3 = r6.getString(r0)     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
            r0 = 2
            java.lang.String r4 = r6.getString(r0)     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
            com.smartapptechnology.soundprofiler.IActivity r5 = com.smartapptechnology.soundprofiler.mgr.AbstractContactLookup.mActivityInstance     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
            android.app.Activity r5 = (android.app.Activity) r5     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
            r0 = r11
            com.smartapptechnology.soundprofiler.mgr.EntityTone r7 = r0.getRecyclable(r1, r3, r4, r5)     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
            if (r7 == 0) goto L_0x0051
            r9.add(r7)     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
        L_0x0051:
            boolean r0 = r6.moveToNext()     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
            if (r0 != 0) goto L_0x0034
            r8 = r9
        L_0x0058:
            if (r6 == 0) goto L_0x005d
            r6.close()
        L_0x005d:
            r6 = 0
        L_0x005e:
            return r8
        L_0x005f:
            r0 = move-exception
            r10 = r0
        L_0x0061:
            com.smartapptechnology.soundprofiler.ErrorLog$Levels r0 = com.smartapptechnology.soundprofiler.ErrorLog.Levels.SEVERE     // Catch:{ all -> 0x006d }
            com.smartapptechnology.soundprofiler.ErrorLog.log(r10, r0)     // Catch:{ all -> 0x006d }
            if (r6 == 0) goto L_0x006b
            r6.close()
        L_0x006b:
            r6 = 0
            goto L_0x005e
        L_0x006d:
            r0 = move-exception
        L_0x006e:
            if (r6 == 0) goto L_0x0073
            r6.close()
        L_0x0073:
            r6 = 0
            throw r0
        L_0x0075:
            r0 = move-exception
            r8 = r9
            goto L_0x006e
        L_0x0078:
            r0 = move-exception
            r10 = r0
            r8 = r9
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.smartapptechnology.soundprofiler.mgr.AbstractContactLookup.loadAll(android.content.ContentResolver, android.net.Uri, java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }

    public EntityTone getRefreshed(long id, String name, String toneUriAsStr, Activity mActivityInstance2) {
        return load(mActivityInstance2.getContentResolver(), getAppendedUri(String.valueOf(id)));
    }
}
