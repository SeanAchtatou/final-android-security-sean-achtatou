package com.smartapptechnology.soundprofiler.mgr;

public final class Utility {
    public static final String DEL = ":";
    public static final String DEL4CONTID = "~";
    public static final String DEL4CONTID_COUNTER = "!";
    public static final String DEL_COUNTER = ">";
    public static final String DEL_FLD = "-";
    public static final String DEL_FLD_COUNTER = "_";
    public static final String DEL_PROF = ";";
    public static final String NULL_STRING = "null";
    public static final String SEP = ",";

    public static String hideParsingConficts(String value) {
        if (value == null) {
            return null;
        }
        return value.replace(DEL, DEL_COUNTER).replace(DEL4CONTID, DEL4CONTID_COUNTER).replace(DEL_FLD, DEL_FLD_COUNTER);
    }

    public static String showParsingConflicts(String value) {
        if (value == null) {
            return null;
        }
        return value.replace(DEL_COUNTER, DEL).replace(DEL4CONTID_COUNTER, DEL4CONTID).replace(DEL_FLD_COUNTER, DEL_FLD);
    }
}
