package com.smartapptechnology.soundprofiler.mgr;

import android.app.Activity;
import com.smartapptechnology.soundprofiler.CommonViewListeners;
import com.smartapptechnology.soundprofiler.R;
import java.util.List;

public final class LimitEnforcer {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$LimitEnforcer$Feature = null;
    private static final int LIMIT_ENTITIES_LITE = 5;
    private static final int LIMIT_PROFILES_LITE = 1;

    public enum Feature {
        ADD_ENTITY,
        SAVE_PROFILE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$LimitEnforcer$Feature() {
        int[] iArr = $SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$LimitEnforcer$Feature;
        if (iArr == null) {
            iArr = new int[Feature.values().length];
            try {
                iArr[Feature.ADD_ENTITY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Feature.SAVE_PROFILE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$LimitEnforcer$Feature = iArr;
        }
        return iArr;
    }

    public static final boolean hasReachedLimit(Feature feature, List list, Activity activity) {
        if (!CommonViewListeners.mIsAppLite) {
            return false;
        }
        if (feature == Feature.ADD_ENTITY && list != null) {
            return list.size() + 1 > 5;
        }
        if (feature != Feature.SAVE_PROFILE || list == null) {
            return true;
        }
        return checkProfileSize(list, activity) > 1;
    }

    private static final int checkProfileSize(List list, Activity activity) {
        List<Profile> list2 = list;
        String unDef = activity.getText(R.string.profile_current).toString();
        int size = list2.size();
        for (Profile p : list2) {
            if (p.getName().equalsIgnoreCase(unDef)) {
                return size - 1;
            }
        }
        return size;
    }

    public static final boolean checkNClip(Feature feature, List list, Activity activity) {
        boolean limitReached = hasReachedLimit(feature, list, activity);
        if (limitReached) {
            switch ($SWITCH_TABLE$com$smartapptechnology$soundprofiler$mgr$LimitEnforcer$Feature()[feature.ordinal()]) {
                case 1:
                    List list2 = list;
                    while (true) {
                        int size = list2.size();
                        if (size <= 5) {
                            break;
                        } else {
                            list2.remove(size - 1);
                        }
                    }
                case 2:
                    List list3 = list;
                    String unDef = activity.getText(R.string.profile_current).toString();
                    boolean found = false;
                    Profile profileB4 = null;
                    int allowableLimit = 1;
                    for (int i = list3.size() - 1; i >= 1; i--) {
                        int size2 = list3.size();
                        Profile profile2Remove = (Profile) list3.get(i);
                        if (i > 0) {
                            profileB4 = (Profile) list3.get(i - 1);
                        }
                        if (profileB4.getName().equalsIgnoreCase(unDef)) {
                            allowableLimit = 2;
                        }
                        if (!found && profile2Remove.getName().equalsIgnoreCase(unDef)) {
                            found = true;
                            allowableLimit = 2;
                        } else if (size2 > allowableLimit) {
                            list3.remove(i);
                        }
                    }
                    break;
            }
        }
        return limitReached;
    }
}
