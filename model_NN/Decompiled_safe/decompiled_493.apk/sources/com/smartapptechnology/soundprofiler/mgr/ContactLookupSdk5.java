package com.smartapptechnology.soundprofiler.mgr;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.R;
import java.util.List;

class ContactLookupSdk5 extends AbstractContactLookup {
    public ContactLookupSdk5() {
        this.mPlaceholderImageResource = R.drawable.contact_pict;
    }

    public Intent getPickerIntent() {
        return new Intent("android.intent.action.PICK", getContentURI());
    }

    public Uri getContentURI() {
        return ContactsContract.Contacts.CONTENT_URI;
    }

    public EntityTone load(ContentResolver contentResolver, Uri contactUri) {
        RuntimeException ex;
        String name;
        long id;
        String toneUriAsStr;
        Cursor cursor = contentResolver.query(contactUri, new String[]{"_id", "display_name", "custom_ringtone"}, null, null, null);
        try {
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
                try {
                    name = cursor.getString(1);
                    try {
                        toneUriAsStr = cursor.getString(2);
                    } catch (RuntimeException e) {
                        ex = e;
                    }
                } catch (RuntimeException e2) {
                    ex = e2;
                    name = null;
                    try {
                        ErrorLog.log(ex, ErrorLog.Levels.MEDIUM);
                        cursor.close();
                        toneUriAsStr = null;
                        return getRecyclable(id, name, toneUriAsStr, (Activity) mActivityInstance);
                    } catch (Throwable th) {
                        th = th;
                        cursor.close();
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor.close();
                    throw th;
                }
            } else {
                id = -1;
                toneUriAsStr = null;
                name = null;
            }
            cursor.close();
        } catch (RuntimeException e3) {
            ex = e3;
            id = -1;
            name = null;
            ErrorLog.log(ex, ErrorLog.Levels.MEDIUM);
            cursor.close();
            toneUriAsStr = null;
            return getRecyclable(id, name, toneUriAsStr, (Activity) mActivityInstance);
        } catch (Throwable th3) {
            th = th3;
            cursor.close();
            throw th;
        }
        return getRecyclable(id, name, toneUriAsStr, (Activity) mActivityInstance);
    }

    public Bitmap loadEntityPhoto(Context context, Uri person, int placeholderImageResource, BitmapFactory.Options options) {
        try {
            Bitmap bitmap = BitmapFactory.decodeStream(ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(), person));
            if (bitmap != null && bitmap.getHeight() > 0 && bitmap.getWidth() > 0) {
                return bitmap;
            }
        } catch (Exception e) {
            ErrorLog.log(e, ErrorLog.Levels.LOW);
        }
        return BitmapFactory.decodeResource(context.getResources(), placeholderImageResource);
    }

    public int update(ContentResolver contentResolver, List<EntityTone> entityToneList) {
        return updateToneRecord(contentResolver, ContactsContract.Contacts.CONTENT_URI, entityToneList, "custom_ringtone");
    }

    public boolean isListRingtoneActive(ContentResolver contentResolver, List<EntityTone> entityToneList) {
        if (entityToneList == null) {
            return false;
        }
        return areRingtonesActive(contentResolver, ContactsContract.Contacts.CONTENT_URI, entityToneList, "_id", "custom_ringtone", "display_name", null);
    }

    /* access modifiers changed from: protected */
    public Uri buildContactRingtoneUri(Uri uri, String appendValue) {
        return Uri.withAppendedPath(uri, appendValue);
    }

    public List<EntityTone> loadAllEntities(ContentResolver contentResolver) {
        return loadAll(contentResolver, ContactsContract.Contacts.CONTENT_URI, "_id", "display_name", "custom_ringtone");
    }
}
