package com.smartapptechnology.soundprofiler.mgr;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractLookup {
    protected Map<Long, StringBuffer> mEntityNameMap;
    protected Map<Long, Bitmap> mEntityPhotoMap;
    protected int mPlaceholderImageResource;

    public abstract Uri getContentURI();

    public abstract Intent getPickerIntent();

    public abstract EntityTone getRefreshed(long j, String str, String str2, Activity activity);

    public abstract boolean isListRingtoneActive(ContentResolver contentResolver, List<EntityTone> list);

    public abstract List<EntityTone> loadAllEntities(ContentResolver contentResolver);

    public abstract Bitmap loadEntityPhoto(Context context, Uri uri, int i, BitmapFactory.Options options);

    public abstract int update(ContentResolver contentResolver, List<EntityTone> list);

    public void destroy() {
        if (this.mEntityPhotoMap != null) {
            this.mEntityPhotoMap.clear();
        }
        this.mEntityPhotoMap = null;
        if (this.mEntityNameMap != null) {
            this.mEntityNameMap.clear();
        }
        this.mEntityNameMap = null;
    }

    public EntityTone getRecyclable(long id, String name, String toneUriAsStr, Activity mActivityInstance) {
        boolean shouldAddName;
        if (id < 0 || name == null) {
            return null;
        }
        if (this.mEntityPhotoMap == null) {
            this.mEntityPhotoMap = new HashMap();
        }
        if (this.mEntityNameMap == null) {
            this.mEntityNameMap = new HashMap();
        }
        EntityTone entityTone = new EntityTone();
        entityTone.setId(id);
        StringBuffer tempSB = this.mEntityNameMap.get(Long.valueOf(id));
        if (tempSB == null) {
            tempSB = new StringBuffer();
            this.mEntityNameMap.put(Long.valueOf(id), tempSB);
            shouldAddName = true;
        } else {
            shouldAddName = !tempSB.toString().equalsIgnoreCase(name);
            if (shouldAddName) {
                tempSB.delete(0, tempSB.length());
            }
        }
        if (shouldAddName) {
            tempSB.append(name);
        }
        entityTone.setSharedNameContainer(tempSB);
        RingToneHandler.checkNSetRingTone(entityTone, toneUriAsStr, mActivityInstance);
        Bitmap icon = this.mEntityPhotoMap.get(Long.valueOf(id));
        if (icon == null) {
            icon = loadEntityPhoto(mActivityInstance, getAppendedUri(String.valueOf(id)), this.mPlaceholderImageResource, null);
            this.mEntityPhotoMap.put(Long.valueOf(id), icon);
        }
        entityTone.setBitmap(icon);
        return entityTone;
    }

    public Uri getAppendedUri(String appendWith) {
        if (getContentURI() == null) {
            return null;
        }
        return Uri.withAppendedPath(getContentURI(), appendWith);
    }
}
