package com.smartapptechnology.soundprofiler.mgr;

import android.app.Activity;
import android.content.SharedPreferences;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.IActivity;
import com.smartapptechnology.soundprofiler.R;
import com.smartapptechnology.soundprofiler.mgr.LimitEnforcer;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProfileManager {
    public static final String SETTING_PROFILES = "profiles";
    private IActivity mActivity;
    private List<Profile> mProfiles;

    public ProfileManager(IActivity context) {
        this.mActivity = context;
    }

    public void destroy() {
        if (this.mProfiles != null) {
            this.mProfiles.clear();
        }
        this.mProfiles = null;
        this.mActivity = null;
    }

    public List<Profile> getProfiles() {
        if (this.mProfiles == null) {
            this.mProfiles = retrieveProfiles();
        }
        sort(this.mProfiles);
        return this.mProfiles;
    }

    public void sort(List list) {
        Collections.sort(list);
    }

    private List<Profile> retrieveProfiles() {
        Activity act = (Activity) this.mActivity;
        List<Profile> profList = inflateProfiles(retrieve(act, act.getClass().getSimpleName(), SETTING_PROFILES));
        if (profList == null) {
            profList = this.mActivity.createDefaultProfiles();
        }
        this.mActivity.checkLimit(LimitEnforcer.Feature.SAVE_PROFILE, profList, true);
        this.mProfiles = profList;
        return profList;
    }

    public static String retrieve(Activity activity, String sharedPrefName, String key) {
        return activity.getSharedPreferences(sharedPrefName, 0).getString(key, null);
    }

    public <T> String deflateCurrentMainNSub(T[] mainComp) {
        if (mainComp == null || mainComp[0] == null) {
            return null;
        }
        StringBuilder sblder = new StringBuilder();
        int limit = mainComp.length;
        for (int i = 0; i < limit; i++) {
            sblder.append(String.valueOf(Utility.hideParsingConficts(String.valueOf(mainComp[i]))) + Utility.DEL).append(this.mActivity.getSubComponent(mainComp[i], false)).append(Utility.SEP);
        }
        return sblder.toString();
    }

    public int[][] parseStreamVol(String streamVal) {
        if (streamVal == null || Utility.NULL_STRING.equalsIgnoreCase(streamVal) || streamVal.length() < 3) {
            return null;
        }
        String[] streams = streamVal.split(Utility.SEP);
        int limit = streams.length;
        int[][] streamsNIndex = (int[][]) Array.newInstance(Integer.TYPE, limit, 2);
        for (int i = 0; i < limit; i++) {
            String[] temp = streams[i].split(Utility.DEL);
            if (temp[0] != null && !"".equals(temp[0])) {
                try {
                    streamsNIndex[i][0] = Integer.parseInt(temp[0]);
                    if (temp.length <= 1 || temp[1] == null || "".equals(temp[1])) {
                        streamsNIndex[i][1] = -1;
                    } else {
                        streamsNIndex[i][1] = Integer.parseInt(temp[1]);
                    }
                } catch (NumberFormatException e) {
                    ErrorLog.log(e, ErrorLog.Levels.LOW);
                }
            }
        }
        return streamsNIndex;
    }

    public String[][] parseStream(String streamVal) {
        if (streamVal == null || Utility.NULL_STRING.equalsIgnoreCase(streamVal) || streamVal.length() < 3) {
            return null;
        }
        String[] streams = streamVal.split(Utility.SEP);
        int limit = streams.length;
        String[][] streamsNIndex = (String[][]) Array.newInstance(String.class, limit, 2);
        for (int i = 0; i < limit; i++) {
            String[] temp = streams[i].split(Utility.DEL);
            if (temp[0] != null && !"".equals(temp[0])) {
                streamsNIndex[i][0] = temp[0];
                if (temp.length <= 1 || temp[1] == null) {
                    streamsNIndex[i][1] = "";
                } else {
                    streamsNIndex[i][1] = temp[1];
                }
            }
        }
        return streamsNIndex;
    }

    public int findProfilePosInList(String val, boolean matchByName) {
        if (val == null) {
            return -1;
        }
        List<Profile> profiles = this.mProfiles;
        if (profiles == null) {
            profiles = getProfiles();
        }
        int idx = -1;
        for (Profile profile : profiles) {
            idx++;
            if ((matchByName && profile.matchName(val)) || (!matchByName && profile.matchValues(val))) {
                return idx;
            }
        }
        return -1;
    }

    /* Debug info: failed to restart local var, previous not found, register: 12 */
    private List<Profile> inflateProfiles(String profAsString) {
        if (profAsString == null || profAsString.length() < 1) {
            return null;
        }
        String[] sProfiles = profAsString.split(Utility.DEL_PROF);
        if (sProfiles == null) {
            return null;
        }
        boolean defaultFound = false;
        String defName = ((Activity) this.mActivity).getText(R.string.profile_current).toString();
        List<Profile> profileList = null;
        for (String split : sProfiles) {
            String[] name_stream = split.split(Utility.DEL_FLD);
            if (name_stream != null && !"".equals(name_stream[0].trim())) {
                if (defName.equalsIgnoreCase(name_stream[0])) {
                    defaultFound = true;
                }
                Profile profile = new Profile();
                profile.setName(name_stream[0]);
                if (name_stream.length <= 1 || Utility.NULL_STRING.equalsIgnoreCase(name_stream[1])) {
                    profile.setValues(null);
                } else {
                    profile.setValues(name_stream[1]);
                }
                if (profileList == null) {
                    profileList = new ArrayList<>();
                }
                profileList.add(profile);
            }
        }
        if (!defaultFound) {
            Profile profile2 = new Profile();
            profile2.setName(defName);
            profile2.setValues(null);
            profileList.add(0, profile2);
        }
        return profileList;
    }

    public void storeProfiles() {
        String flatProfs = deflateProfiles();
        Activity act = (Activity) this.mActivity;
        storePref(act, act.getClass().getSimpleName(), SETTING_PROFILES, flatProfs);
    }

    public static void storePref(Activity activity, String activityName, String key, String value) {
        SharedPreferences.Editor editor = activity.getSharedPreferences(activityName, 0).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String removePref(Activity activity, String activityName, String key) {
        SharedPreferences profSettings = activity.getSharedPreferences(activityName, 0);
        String value = profSettings.getString(key, null);
        SharedPreferences.Editor editor = profSettings.edit();
        editor.remove(key);
        editor.commit();
        return value;
    }

    private String deflateProfiles() {
        List<Profile> profiles = this.mProfiles;
        if (profiles == null) {
            profiles = getProfiles();
        }
        StringBuilder sBlder = new StringBuilder();
        for (Profile profile : profiles) {
            sBlder.append(String.valueOf(profile.getName()) + Utility.DEL_FLD + profile.getValues() + Utility.DEL_PROF);
        }
        return sBlder.toString();
    }

    public boolean addProfile(Profile profile) {
        if (this.mProfiles == null) {
            this.mProfiles = getProfiles();
        }
        boolean bool = this.mProfiles.add(profile);
        if (this.mActivity.checkLimit(LimitEnforcer.Feature.SAVE_PROFILE, this.mProfiles, true)) {
            return false;
        }
        sort(this.mProfiles);
        return bool;
    }

    public Profile removeProfile(int idx) {
        if (this.mProfiles == null) {
            this.mProfiles = getProfiles();
        }
        return this.mProfiles.remove(idx);
    }
}
