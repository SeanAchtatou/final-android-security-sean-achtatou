package com.smartapptechnology.soundprofiler.mgr;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Contacts;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.R;
import java.util.List;

class ContactLookupSdk3_4 extends AbstractContactLookup {
    ContactLookupSdk3_4() {
        this.mPlaceholderImageResource = R.drawable.contact_pict;
    }

    public Intent getPickerIntent() {
        return new Intent("android.intent.action.PICK", getContentURI());
    }

    public Uri getContentURI() {
        return Contacts.People.CONTENT_URI;
    }

    public EntityTone load(ContentResolver contentResolver, Uri contactUri) {
        String name = null;
        String toneUriAsStr = null;
        long id = -1;
        Cursor cursor = contentResolver.query(contactUri, new String[]{"_id", "display_name"}, null, null, null);
        try {
            if (cursor.moveToFirst()) {
                id = cursor.getLong(0);
                name = cursor.getString(1);
            }
        } catch (RuntimeException e) {
            ErrorLog.log(e, ErrorLog.Levels.MEDIUM);
        } finally {
            cursor.close();
        }
        Cursor cursor2 = contentResolver.query(Uri.withAppendedPath(contactUri, "contact_methods"), new String[]{"custom_ringtone"}, null, null, null);
        try {
            if (cursor2.moveToFirst()) {
                toneUriAsStr = cursor2.getString(0);
            }
        } catch (RuntimeException e2) {
            ErrorLog.log(e2, ErrorLog.Levels.MEDIUM);
        } finally {
            cursor2.close();
        }
        return getRecyclable(id, name, toneUriAsStr, (Activity) mActivityInstance);
    }

    public Bitmap loadEntityPhoto(Context context, Uri person, int placeholderImageResource, BitmapFactory.Options options) {
        try {
            Bitmap bitmap = Contacts.People.loadContactPhoto(context, person, placeholderImageResource, options);
            if (bitmap != null && bitmap.getHeight() > 0 && bitmap.getWidth() > 0) {
                return bitmap;
            }
        } catch (Exception e) {
            ErrorLog.log(e, ErrorLog.Levels.LOW);
        }
        return BitmapFactory.decodeResource(context.getResources(), placeholderImageResource);
    }

    public boolean isListRingtoneActive(ContentResolver contentResolver, List<EntityTone> entityToneList) {
        if (entityToneList == null) {
            return false;
        }
        return areRingtonesActive(contentResolver, Contacts.People.CONTENT_URI, entityToneList, "_id", "custom_ringtone", "display_name", "people._id");
    }

    public int update(ContentResolver contentResolver, List<EntityTone> entityToneList) {
        return updateToneRecord(contentResolver, Contacts.People.CONTENT_URI, entityToneList, "custom_ringtone");
    }

    /* access modifiers changed from: protected */
    public Uri buildContactRingtoneUri(Uri uri, String appendValue) {
        return Uri.withAppendedPath(uri, appendValue);
    }

    public List<EntityTone> loadAllEntities(ContentResolver contentResolver) {
        return loadAll(contentResolver, Contacts.People.CONTENT_URI, "_id", "display_name", "custom_ringtone");
    }
}
