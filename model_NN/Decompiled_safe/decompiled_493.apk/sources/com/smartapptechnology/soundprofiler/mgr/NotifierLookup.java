package com.smartapptechnology.soundprofiler.mgr;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import com.smartapptechnology.soundprofiler.CommonViewListeners;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.IActivity;
import com.smartapptechnology.soundprofiler.R;
import com.smartapptechnology.soundprofiler.mgr.EntityTone;
import java.util.List;

public class NotifierLookup extends AbstractLookup {
    private IActivity mActivity;

    public NotifierLookup(IActivity activity) {
        this.mActivity = activity;
        this.mPlaceholderImageResource = R.drawable.tone;
    }

    public Intent getPickerIntent() {
        return null;
    }

    public Uri getContentURI() {
        return null;
    }

    public int update(ContentResolver contentResolver, List<EntityTone> entityToneList) {
        if (entityToneList != null && entityToneList.size() > 0) {
            try {
                for (EntityTone e : entityToneList) {
                    if (CommonViewListeners.ISDEBUG) {
                        recordDebugMsg("\nNotifier Tone Update...", e);
                    }
                    RingToneHandler.setRingtone((Activity) this.mActivity, e.getToneType().getCode(), RingToneHandler.convert(e.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE), e.getToneType().getCode(), (Activity) this.mActivity));
                }
                return entityToneList.size();
            } catch (Exception e2) {
                ErrorLog.log(e2, ErrorLog.Levels.SEVERE);
            }
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0017  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isListRingtoneActive(android.content.ContentResolver r9, java.util.List<com.smartapptechnology.soundprofiler.mgr.EntityTone> r10) {
        /*
            r8 = this;
            r7 = 0
            if (r10 == 0) goto L_0x0009
            int r4 = r10.size()
            if (r4 != 0) goto L_0x000b
        L_0x0009:
            r4 = r7
        L_0x000a:
            return r4
        L_0x000b:
            java.util.Iterator r5 = r10.iterator()
        L_0x000f:
            boolean r4 = r5.hasNext()
            if (r4 != 0) goto L_0x0017
            r4 = 1
            goto L_0x000a
        L_0x0017:
            java.lang.Object r0 = r5.next()
            com.smartapptechnology.soundprofiler.mgr.EntityTone r0 = (com.smartapptechnology.soundprofiler.mgr.EntityTone) r0
            com.smartapptechnology.soundprofiler.IActivity r4 = r8.mActivity     // Catch:{ Exception -> 0x005b }
            android.app.Activity r4 = (android.app.Activity) r4     // Catch:{ Exception -> 0x005b }
            com.smartapptechnology.soundprofiler.ToneType r6 = r0.getToneType()     // Catch:{ Exception -> 0x005b }
            int r6 = r6.getCode()     // Catch:{ Exception -> 0x005b }
            android.net.Uri r4 = com.smartapptechnology.soundprofiler.mgr.RingToneHandler.getDefault(r4, r6)     // Catch:{ Exception -> 0x005b }
            java.lang.String r3 = r4.toString()     // Catch:{ Exception -> 0x005b }
            boolean r4 = com.smartapptechnology.soundprofiler.CommonViewListeners.ISDEBUG     // Catch:{ Exception -> 0x005b }
            if (r4 == 0) goto L_0x0047
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005b }
            java.lang.String r6 = "\nNotifier Tone isActive...?\n Def_Uri: "
            r4.<init>(r6)     // Catch:{ Exception -> 0x005b }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ Exception -> 0x005b }
            java.lang.String r2 = r4.toString()     // Catch:{ Exception -> 0x005b }
            r8.recordDebugMsg(r2, r0)     // Catch:{ Exception -> 0x005b }
        L_0x0047:
            if (r3 == 0) goto L_0x0059
            com.smartapptechnology.soundprofiler.mgr.EntityTone$ENTITY_TONE_TYPE r4 = com.smartapptechnology.soundprofiler.mgr.EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE     // Catch:{ Exception -> 0x005b }
            java.lang.String r4 = r0.getValue(r4)     // Catch:{ Exception -> 0x005b }
            java.lang.String r4 = com.smartapptechnology.soundprofiler.mgr.Utility.showParsingConflicts(r4)     // Catch:{ Exception -> 0x005b }
            boolean r4 = r3.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x005b }
            if (r4 != 0) goto L_0x000f
        L_0x0059:
            r4 = r7
            goto L_0x000a
        L_0x005b:
            r4 = move-exception
            r1 = r4
            com.smartapptechnology.soundprofiler.ErrorLog$Levels r4 = com.smartapptechnology.soundprofiler.ErrorLog.Levels.SEVERE
            com.smartapptechnology.soundprofiler.ErrorLog.log(r1, r4)
            r4 = r7
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.smartapptechnology.soundprofiler.mgr.NotifierLookup.isListRingtoneActive(android.content.ContentResolver, java.util.List):boolean");
    }

    public void destroy() {
        this.mActivity = null;
    }

    public List<EntityTone> loadAllEntities(ContentResolver contentResolver) {
        return null;
    }

    private void recordDebugMsg(String preMsg, EntityTone entityTone) {
        CommonViewListeners.appendAlertDialogMsg(String.valueOf(preMsg) + "\nRng_Uri= " + entityTone.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE) + "\nId= " + entityTone.getId() + "\nTitle= " + entityTone.getValue(EntityTone.ENTITY_TONE_TYPE.NAME_RINGTONE) + "\nName= " + entityTone.getValue(EntityTone.ENTITY_TONE_TYPE.NAME) + "\nToneType= " + entityTone.getToneType().toString());
    }

    public Bitmap loadEntityPhoto(Context context, Uri person, int placeholderImageResource, BitmapFactory.Options options) {
        return BitmapFactory.decodeResource(context.getResources(), placeholderImageResource);
    }

    public EntityTone getRefreshed(long id, String name, String toneUriAsStr, Activity mActivityInstance) {
        return getRecyclable(id, name, toneUriAsStr, mActivityInstance);
    }
}
