package com.smartapptechnology.soundprofiler.mgr;

import java.util.List;

public class Profile implements Comparable<Profile> {
    private String mName;
    private String mName4Sort;
    private List<EntityTone> mValueList;
    private String mValues;

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
        setName4Sort(name);
    }

    public void setName4Sort(String name) {
        if (name == null) {
            this.mName4Sort = String.valueOf(0);
        } else {
            this.mName4Sort = name.toLowerCase();
        }
    }

    public String getValues() {
        return this.mValues;
    }

    public void setValues(String values) {
        this.mValues = values;
        if (values == null) {
            setName4Sort(null);
        }
        this.mValueList = null;
    }

    public String toString() {
        return this.mName;
    }

    public boolean matchName(String name) {
        if (this.mName == null || "".equals(this.mName)) {
            return false;
        }
        return this.mName.equals(name);
    }

    public boolean matchValues(String values) {
        if (this.mValues == null || "".equals(this.mValues)) {
            return false;
        }
        return this.mValues.equals(values);
    }

    public List<EntityTone> getValueList() {
        return this.mValueList;
    }

    public void setValueList(List<EntityTone> valueList) {
        this.mValueList = valueList;
    }

    public int compareTo(Profile another) {
        return this.mName4Sort.compareTo(another.mName4Sort);
    }
}
