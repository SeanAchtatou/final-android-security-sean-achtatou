package com.smartapptechnology.soundprofiler;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.mobfox.sdk.MobFoxView;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.mgr.ProfileManager;
import com.smartapptechnology.soundprofiler.mgr.RingToneHandler;
import java.util.Calendar;

public class ToneTabWidget extends TabActivity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$smartapptechnology$soundprofiler$ToneType;
    CommonViewListeners mCommonViewListener = null;
    String mPre = "";
    String mSuf = "";

    static /* synthetic */ int[] $SWITCH_TABLE$com$smartapptechnology$soundprofiler$ToneType() {
        int[] iArr = $SWITCH_TABLE$com$smartapptechnology$soundprofiler$ToneType;
        if (iArr == null) {
            iArr = new int[ToneType.values().length];
            try {
                iArr[ToneType.ALARM.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ToneType.NOTIFICATION.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ToneType.OTHERS.ordinal()] = 5;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ToneType.RINGTONE.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ToneType.SOUND.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$smartapptechnology$soundprofiler$ToneType = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.layout_tab);
        this.mCommonViewListener = new CommonViewListeners(null, null);
        if (prompt4Rating()) {
            showDialog(6);
        }
        if (is4NewVersion()) {
            showDialog(3);
        }
        Resources resources = getResources();
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec("sound").setIndicator(String.valueOf(this.mPre) + ((Object) getText(R.string.tab_title_sound)) + this.mSuf, resources.getDrawable(R.drawable.tab_sound_drawable)).setContent(new Intent().setClass(this, SoundManagerActivity.class)));
        tabHost.addTab(tabHost.newTabSpec("contacts").setIndicator(String.valueOf(this.mPre) + ((Object) getText(R.string.tab_title_contact)) + this.mSuf, resources.getDrawable(R.drawable.tab_contact_drawable)).setContent(new Intent().setClass(this, ContactRingtoneActivity.class)));
        tabHost.addTab(tabHost.newTabSpec("alerts").setIndicator(String.valueOf(this.mPre) + ((Object) getText(R.string.tab_title_alerts)) + this.mSuf, resources.getDrawable(R.drawable.tab_notifier_drawable)).setContent(new Intent().setClass(this, NotifierRingtoneActivity.class)));
        tabSelector(tabHost, getIntent());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            MobFoxView mobFoxView = (MobFoxView) findViewById(R.id.mobFoxView);
            if (mobFoxView != null) {
                mobFoxView.resume();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        try {
            MobFoxView mobFoxView = (MobFoxView) findViewById(R.id.mobFoxView);
            if (mobFoxView != null) {
                mobFoxView.pause();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mCommonViewListener.destroy();
        this.mCommonViewListener = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = this.mCommonViewListener.processOnCreateDialog(id, this);
        if (dialog != null) {
            return dialog;
        }
        switch (id) {
            case CommonViewListeners.DIALOGUSERAGREEMENT:
                View layout = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.layout_release_dialog, (ViewGroup) findViewById(R.id.layout_dialog_release));
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setView(layout);
                builder.setTitle(R.string.user_agree);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        ToneTabWidget.this.store(CommonViewListeners.SETTING_VERSION, CommonViewListeners.mVersionCode);
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton(R.string.not_accept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.cancel();
                        ToneTabWidget.this.finish();
                    }
                });
                TextView newInReleaseTx = (TextView) layout.findViewById(R.id.lbl_dialog_release_new_in_release);
                newInReleaseTx.setText(String.valueOf(newInReleaseTx.getText().toString()) + " " + CommonViewListeners.mVersionName);
                String[] releaseNote = getResources().getStringArray(R.array.release_notes);
                TableLayout tableLayout = (TableLayout) layout.findViewById(R.id.dialog_release_note_table);
                int size = releaseNote.length;
                for (int i = 0; i < size; i++) {
                    TableRow tableRow = new TableRow(getApplicationContext());
                    tableRow.setPadding(0, 2, 0, 0);
                    TextView textView = new TextView(getApplicationContext());
                    textView.setPadding(2, 2, 2, 2);
                    textView.setGravity(3);
                    textView.setText(String.valueOf(i + 1) + ".");
                    tableRow.addView(textView);
                    TextView textView2 = new TextView(getApplicationContext());
                    textView2.setPadding(2, 2, 2, 2);
                    textView2.setGravity(3);
                    textView2.setText(releaseNote[i]);
                    tableRow.addView(textView2);
                    tableLayout.addView(tableRow);
                }
                return builder.create();
            case CommonViewListeners.DIALOGPROGRESSUNDETERMINE:
            case CommonViewListeners.DIALOGBYPASS:
            default:
                return super.onCreateDialog(id);
            case CommonViewListeners.DIALOGRATEAPP:
                View layout2 = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.layout_rate_dialog, (ViewGroup) findViewById(R.id.layout_dialog_rate));
                ((Button) layout2.findViewById(R.id.btn_dialog_rate_now)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        try {
                            ToneTabWidget.this.updateRatingDate(-5);
                            Intent intent = new Intent("android.intent.action.VIEW");
                            intent.setData(Uri.parse(String.valueOf((String) ToneTabWidget.this.getText(R.string.rating_website)) + CommonViewListeners.mPackageName));
                            ToneTabWidget.this.startActivity(intent);
                        } catch (Exception e) {
                            ErrorLog.log(e, ErrorLog.Levels.LOW);
                        }
                        ToneTabWidget.this.dismissDialog(6);
                    }
                });
                ((Button) layout2.findViewById(R.id.btn_dialog_rate_later)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ToneTabWidget.this.updateRatingDate(1);
                        ToneTabWidget.this.dismissDialog(6);
                    }
                });
                ((Button) layout2.findViewById(R.id.btn_dialog_rate_never)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ToneTabWidget.this.updateRatingDate(-5);
                        ToneTabWidget.this.dismissDialog(6);
                    }
                });
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setView(layout2);
                return builder2.create();
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        this.mCommonViewListener.processOnPrepareDialog(id, dialog);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        tabSelector(getTabHost(), intent);
    }

    private boolean is4NewVersion() {
        boolean z;
        boolean isNew = false;
        PackageInfo packageInfo = null;
        CommonViewListeners.mPackageName = getPackageName();
        try {
            packageInfo = getPackageManager().getPackageInfo(CommonViewListeners.mPackageName, 128);
        } catch (PackageManager.NameNotFoundException e) {
            CommonViewListeners.mPackageName = getClass().getPackage().getName();
            try {
                packageInfo = getPackageManager().getPackageInfo(CommonViewListeners.mPackageName, 128);
            } catch (Exception e2) {
            }
        }
        if (packageInfo != null) {
            int manifCode = packageInfo.versionCode;
            int instCode = retrieve(CommonViewListeners.SETTING_VERSION);
            if (manifCode > instCode) {
                isNew = true;
            } else {
                isNew = false;
            }
            CommonViewListeners.mVersionCode = isNew ? manifCode : instCode;
            CommonViewListeners.mVersionName = packageInfo.versionName;
            boolean endsWith = getString(packageInfo.applicationInfo.labelRes).toLowerCase().endsWith("lite");
            CommonViewListeners.mIsAppLite = endsWith;
            if (endsWith) {
                this.mSuf = " (" + ((Object) getText(R.string.lite)) + ")";
                if (isProBypass()) {
                    z = false;
                } else {
                    z = CommonViewListeners.mIsAppLite;
                }
                CommonViewListeners.mIsAppLite = z;
            }
        }
        return isNew;
    }

    private boolean prompt4Rating() {
        int storedDate = retrieve(CommonViewListeners.SETTING_RATE_APP);
        int currDate = storedDate;
        try {
            currDate = Integer.parseInt(CommonViewListeners.removeNonDigit(CommonViewListeners.addToDate(0)));
        } catch (NumberFormatException e) {
            ErrorLog.log(e, ErrorLog.Levels.LOW);
        }
        if (storedDate == -5 || currDate < storedDate) {
            return false;
        }
        return true;
    }

    private void tabSelector(TabHost tabHost, Intent intent) {
        int displayTab = 0;
        ToneType typeOfTone = RingToneHandler.determineIntentType(intent);
        if (typeOfTone != null) {
            switch ($SWITCH_TABLE$com$smartapptechnology$soundprofiler$ToneType()[typeOfTone.ordinal()]) {
                case 1:
                    displayTab = 1;
                    break;
                case 2:
                case CommonViewListeners.DIALOGUSERAGREEMENT:
                    displayTab = 2;
                    break;
            }
        }
        tabHost.setCurrentTab(displayTab);
    }

    public int retrieve(String storageName) {
        return getPreferences(0).getInt(storageName, -1);
    }

    public void store(String storageName, int value) {
        SharedPreferences.Editor editor = getPreferences(0).edit();
        editor.putInt(storageName, value);
        editor.commit();
    }

    public boolean isProBypass() {
        try {
            String expDateStr = ProfileManager.retrieve(this, CommonViewListeners.BYPASS_SHARED_PREF_NAME, CommonViewListeners.SETTING_BYPASS_LONG);
            if (expDateStr != null) {
                boolean bool = Calendar.getInstance().getTime().before(CommonViewListeners.parseDate(expDateStr));
                if (bool) {
                    this.mPre = "* ";
                }
                return bool;
            }
        } catch (Exception e) {
            ErrorLog.log(e, ErrorLog.Levels.LOW);
        }
        return false;
    }

    public boolean updateRatingDate(int daysToAdd) {
        int reminderDate = daysToAdd;
        if (daysToAdd != -5) {
            try {
                reminderDate = Integer.parseInt(CommonViewListeners.removeNonDigit(CommonViewListeners.addToDate(daysToAdd)));
            } catch (Exception e) {
                ErrorLog.log(e, ErrorLog.Levels.LOW);
                return false;
            }
        }
        store(CommonViewListeners.SETTING_RATE_APP, reminderDate);
        return true;
    }
}
