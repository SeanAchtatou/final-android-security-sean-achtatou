package com.smartapptechnology.soundprofiler;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.smartapptechnology.soundprofiler.ErrorLog;
import com.smartapptechnology.soundprofiler.mgr.AbstractLookup;
import com.smartapptechnology.soundprofiler.mgr.EntityTone;
import com.smartapptechnology.soundprofiler.mgr.LimitEnforcer;
import com.smartapptechnology.soundprofiler.mgr.Profile;
import com.smartapptechnology.soundprofiler.mgr.ProfileManager;
import com.smartapptechnology.soundprofiler.mgr.RingToneHandler;
import com.smartapptechnology.soundprofiler.mgr.Utility;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractRingtoneActivity extends Activity implements IActivity<String> {
    static final String CHECKRINGTONETASK = "CHECKRINGTONETASK";
    static final String LOADCONTACTSTASK = "LOADCONTACTSTASK";
    static final String LOADNOTIFTASK = "LOADNOTIFTASK";
    static final int PICKRINGTONE_INTENT = 3;
    static final String UPDATERINGTONETASK = "UPDATERINGTONETASK";
    ArrayList<String> mActiveTaskNameList;
    CommonViewListeners mCommonViewListener;
    boolean mIsCloseUsrInvoked = false;
    AbstractLookup mLookup;
    ProfileManager mProfileManager;
    long mSelectedListIndex4Ringtone = -1;
    Spinner mSpinner;
    Button mStoreBtn;
    Object mSyncObj = new Object();
    ArrayList<AsyncTask> mTaskList;
    TextView mTxtViewIsActive;
    ListView mlistView;

    /* access modifiers changed from: protected */
    public abstract Profile onCreateMyListFromProfile(List<Profile> list);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.layout_ringtone);
        this.mTaskList = new ArrayList<>();
        this.mActiveTaskNameList = new ArrayList<>();
        this.mProfileManager = new ProfileManager(this);
        this.mCommonViewListener = new CommonViewListeners(this, this.mProfileManager);
        List<Profile> profiles = this.mProfileManager.getProfiles();
        checkLimit(LimitEnforcer.Feature.SAVE_PROFILE, profiles, true);
        ArrayAdapter<Profile> arrayAdap = new ArrayAdapter<>(this, 17367048, profiles);
        arrayAdap.setDropDownViewResource(17367049);
        this.mSpinner = (Spinner) findViewById(R.id.spinner_Ringtone);
        this.mSpinner.setAdapter((SpinnerAdapter) arrayAdap);
        this.mSpinner.setOnItemSelectedListener(this.mCommonViewListener.getItemSelectedListener());
        this.mStoreBtn = (Button) findViewById(R.id.btn_save_Ringtone);
        this.mStoreBtn.setOnClickListener(this.mCommonViewListener.getOnClickListener());
        this.mTxtViewIsActive = (TextView) findViewById(R.id.lbl_checkRingtoneStatus);
        this.mTxtViewIsActive.setOnClickListener(this.mCommonViewListener.getOnClickListener());
        onCreateMyListFromProfile(profiles);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        registerForContextMenu(this.mlistView);
        checkProfile(false);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterForContextMenu(this.mlistView);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        int idx;
        if (!this.mIsCloseUsrInvoked && (idx = this.mProfileManager.findProfilePosInList(getText(R.string.profile_current).toString(), true)) > -1) {
            this.mProfileManager.getProfiles().get(idx).setValues(this.mProfileManager.deflateCurrentMainNSub(getMainComponents()));
        }
        this.mProfileManager.storeProfiles();
        this.mProfileManager.destroy();
        this.mProfileManager = null;
        this.mTaskList.clear();
        this.mTaskList = null;
        if (this.mCommonViewListener != null) {
            this.mCommonViewListener.destroy();
        }
        this.mCommonViewListener = null;
        if (this.mLookup != null) {
            this.mLookup.destroy();
        }
        this.mLookup = null;
        this.mlistView = null;
        this.mSpinner = null;
        this.mStoreBtn = null;
        this.mTxtViewIsActive = null;
        RingToneHandler.destroy();
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return this.mCommonViewListener.processOnCreateOptionMenu(menu, R.menu.ringtone_menu, this);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return this.mCommonViewListener.processOnPrepareOptionsMenu(menu, this.mStoreBtn, this.mSpinner);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.mCommonViewListener.processOnOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.menu_close /*2131230769*/:
                this.mIsCloseUsrInvoked = true;
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        this.mCommonViewListener.processOnCreateContextMenu(menu, v, menuInfo, R.menu.ringtone_context, this);
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo contextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menu_context_assignTone /*2131230763*/:
                this.mSelectedListIndex4Ringtone = contextMenuInfo.id;
                EntityToneListAdapter adapter = (EntityToneListAdapter) this.mlistView.getAdapter();
                if (adapter == null) {
                    return false;
                }
                EntityTone entityTone = adapter.getItem((int) this.mSelectedListIndex4Ringtone);
                startActivityForResult(RingToneHandler.getPickerIntent(entityTone.getToneType(), RingToneHandler.convert(entityTone.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE), entityTone.getToneType().getCode(), this), this), 3);
                return true;
            case R.id.menu_context_playtone /*2131230764*/:
                playTone(contextMenuInfo.id);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = this.mCommonViewListener.processOnCreateDialog(id, null);
        if (dialog == null) {
            return super.onCreateDialog(id);
        }
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        this.mCommonViewListener.processOnPrepareDialog(id, dialog);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3 && resultCode == -1) {
            loadRingToneInfo(data);
        }
    }

    /* access modifiers changed from: protected */
    public void continueOnCreate(Profile profile) {
        if (profile != null) {
            int idx = this.mProfileManager.findProfilePosInList(profile.getName(), true);
            if (idx < 0) {
                idx = 0;
            }
            this.mSpinner.setSelection(idx);
        }
    }

    /* access modifiers changed from: protected */
    public void createListAdapterInBackground(final EntityToneListAdapter adapter, ListView listView, final Activity activity) {
        AsyncTask<Integer, Void, Integer> mLoadAllContactsTask = new AsyncTask<Integer, Void, Integer>() {
            /* access modifiers changed from: protected */
            public Integer doInBackground(Integer... params) {
                AbstractRingtoneActivity.this.setListAdapter(adapter, AbstractRingtoneActivity.this.mlistView, activity);
                return new Integer(0);
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Integer result) {
                AbstractRingtoneActivity.this.mCommonViewListener.cancelProgressDialog();
                AbstractRingtoneActivity.this.removeTask(this, AbstractRingtoneActivity.LOADCONTACTSTASK);
            }
        };
        showDialog(4);
        addTask(mLoadAllContactsTask, LOADCONTACTSTASK);
        mLoadAllContactsTask.execute(0);
    }

    /* access modifiers changed from: package-private */
    public EntityToneListAdapter createListAdapter(Profile profile, AbstractLookup lookup) {
        List<EntityTone> entityToneLists = null;
        if (profile == null) {
            return null;
        }
        if (CommonViewListeners.ISDEBUG) {
            CommonViewListeners.appendAlertDialogMsg("\nProfile\nName= " + profile.getName() + "\nValue= " + profile.getValues());
        }
        if (profile.getValueList() != null) {
            entityToneLists = profile.getValueList();
        } else {
            String[][] subject_ringtones = this.mProfileManager.parseStream(profile.getValues());
            if (subject_ringtones != null) {
                for (int i = 0; i < subject_ringtones.length; i++) {
                    String name = Utility.showParsingConflicts(subject_ringtones[i][0]);
                    long id = -1;
                    String toneUriAsStr = null;
                    String[] temp = subject_ringtones[i][1].split(Utility.DEL4CONTID);
                    if (temp.length > 0 && temp[0] != null) {
                        toneUriAsStr = temp[0];
                    }
                    if (temp.length > 1 && temp[1] != null) {
                        try {
                            id = Long.parseLong(temp[1]);
                        } catch (NumberFormatException e) {
                            id = (long) name.hashCode();
                            ErrorLog.log(e, ErrorLog.Levels.MEDIUM);
                        }
                    }
                    if (lookup == null) {
                        return null;
                    }
                    EntityTone entityTone = lookup.getRefreshed(id, name, toneUriAsStr, this);
                    if (entityToneLists == null) {
                        entityToneLists = new ArrayList<>();
                    }
                    if (entityTone != null) {
                        entityToneLists.add(entityTone);
                    }
                }
                if (entityToneLists != null) {
                    profile.setValueList(entityToneLists);
                }
            }
        }
        if (profile != null) {
            if (getText(R.string.profile_current).toString().equalsIgnoreCase(profile.getName())) {
                profile.setValues(null);
            }
        }
        ArrayList<EntityTone> dataList = new ArrayList<>();
        if (entityToneLists != null) {
            Iterator<EntityTone> it = entityToneLists.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                EntityTone e2 = it.next();
                if (checkLimit(LimitEnforcer.Feature.ADD_ENTITY, dataList, false)) {
                    LimitEnforcer.checkNClip(LimitEnforcer.Feature.ADD_ENTITY, entityToneLists, this);
                    break;
                }
                try {
                    dataList.add(e2.deepClone());
                } catch (CloneNotSupportedException e3) {
                    ErrorLog.log(e3, ErrorLog.Levels.SEVERE);
                }
            }
        }
        return new EntityToneListAdapter(this, R.layout.layout_ringtone_row, new EntityTone.ENTITY_TONE_TYPE[]{EntityTone.ENTITY_TONE_TYPE.BITMAP, EntityTone.ENTITY_TONE_TYPE.NAME, EntityTone.ENTITY_TONE_TYPE.NAME_RINGTONE}, new int[]{R.id.imgview_contact, R.id.txtview_contact, R.id.txtview_ringtone}, dataList);
    }

    /* access modifiers changed from: package-private */
    public void setListAdapter(EntityToneListAdapter adapter, ListView listView, Activity activity) {
        try {
            ViewGroup viewGroup = (ViewGroup) activity.findViewById(R.id.layout_contact_ringtone);
            if (adapter == null || adapter.getCount() <= 0) {
                TextView empty = new TextView(activity);
                empty.setId(16908292);
                empty.setText(((Object) getText(R.string.no_list)) + " '" + ((Object) getText(R.string.menu_item_contacts)) + "'");
                empty.setGravity(1);
                viewGroup.addView(empty, -2);
                activity.unregisterForContextMenu(listView);
                return;
            }
            View view = viewGroup.findViewById(16908292);
            if (view != null) {
                viewGroup.removeView(view);
            }
            listView.setAdapter((ListAdapter) adapter);
            activity.registerForContextMenu(listView);
        } catch (Exception e) {
            ErrorLog.log(e, ErrorLog.Levels.MEDIUM);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x000e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.smartapptechnology.soundprofiler.mgr.Profile determineActiveProfile(java.util.List<com.smartapptechnology.soundprofiler.mgr.Profile> r4) {
        /*
            r3 = this;
            if (r4 == 0) goto L_0x000c
            java.util.Iterator r1 = r4.iterator()
        L_0x0006:
            boolean r2 = r1.hasNext()
            if (r2 != 0) goto L_0x000e
        L_0x000c:
            r1 = 0
        L_0x000d:
            return r1
        L_0x000e:
            java.lang.Object r0 = r1.next()
            com.smartapptechnology.soundprofiler.mgr.Profile r0 = (com.smartapptechnology.soundprofiler.mgr.Profile) r0
            java.lang.String r2 = r0.getValues()
            if (r2 != 0) goto L_0x0020
            java.util.List r2 = r0.getValueList()
            if (r2 == 0) goto L_0x0006
        L_0x0020:
            r1 = r0
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.smartapptechnology.soundprofiler.AbstractRingtoneActivity.determineActiveProfile(java.util.List):com.smartapptechnology.soundprofiler.mgr.Profile");
    }

    private void loadRingToneInfo(Intent dataIntent) {
        EntityToneListAdapter adapter = (EntityToneListAdapter) this.mlistView.getAdapter();
        if (adapter != null && adapter.getCount() >= 1 && this.mSelectedListIndex4Ringtone >= 0) {
            EntityTone entityTone = adapter.getItem((int) this.mSelectedListIndex4Ringtone);
            this.mSelectedListIndex4Ringtone = -1;
            if (entityTone != null) {
                Uri uri = (Uri) dataIntent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
                RingToneHandler.checkNSetRingTone(entityTone, uri == null ? null : uri.toString(), this);
                adapter.notifyDataSetChanged();
                if (CommonViewListeners.ISDEBUG) {
                    CommonViewListeners.captureIntentInfo("\nLoad Ringtone Intent...", dataIntent, true);
                    CommonViewListeners.appendAlertDialogMsg("\nLoad Ringtone info...\nTitle: " + entityTone.getValue(EntityTone.ENTITY_TONE_TYPE.NAME_RINGTONE) + "\nUri: " + entityTone.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE));
                }
            }
            checkProfile(false);
        }
    }

    private boolean playTone(long idx) {
        EntityToneListAdapter adapter = (EntityToneListAdapter) this.mlistView.getAdapter();
        if (adapter == null) {
            return false;
        }
        EntityTone contactTone = adapter.getItem((int) idx);
        if (contactTone.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE) == null) {
            return false;
        }
        RingToneHandler.playTone(contactTone.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE), ToneType.RINGTONE.getCode(), this);
        return true;
    }

    public List<Profile> createDefaultProfiles() {
        Profile profile = new Profile();
        profile.setName((String) getText(R.string.profile_current));
        profile.setValues(null);
        ArrayList<Profile> list = new ArrayList<>();
        list.add(profile);
        return list;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public String[] getMainComponents() {
        String[] names = null;
        if (this.mlistView == null || this.mlistView.getAdapter() == null) {
            return null;
        }
        ListAdapter adapter = this.mlistView.getAdapter();
        int size = adapter.getCount();
        if (size > 0) {
            names = new String[size];
        }
        for (int i = 0; i < size; i++) {
            names[i] = ((EntityTone) adapter.getItem(i)).getValue(EntityTone.ENTITY_TONE_TYPE.NAME);
        }
        return names;
    }

    public String getSubComponent(String name, boolean getMax) {
        ListAdapter adapter = this.mlistView.getAdapter();
        int size = adapter.getCount();
        for (int i = 0; i < size; i++) {
            EntityTone contactTone = (EntityTone) adapter.getItem(i);
            if (name.equals(contactTone.getValue(EntityTone.ENTITY_TONE_TYPE.NAME))) {
                String tone = Utility.hideParsingConficts(contactTone.getValue(EntityTone.ENTITY_TONE_TYPE.URI_RINGTONE));
                if (tone == null) {
                    tone = "";
                }
                return String.valueOf(tone) + Utility.DEL4CONTID + contactTone.getId();
            }
        }
        return null;
    }

    public void enableStoreBtn(boolean enabled) {
        if (this.mStoreBtn != null) {
            this.mStoreBtn.setEnabled(enabled);
        }
    }

    public boolean processOnClick(View view) {
        EntityToneListAdapter adapter;
        List<EntityTone> entityToneList;
        String actMsg = this.mTxtViewIsActive.getText().toString();
        if (view.getId() != this.mTxtViewIsActive.getId() || actMsg.indexOf((String) getText(R.string.no)) <= -1) {
            return false;
        }
        if (this.mlistView != null) {
            adapter = (EntityToneListAdapter) this.mlistView.getAdapter();
        } else {
            adapter = null;
        }
        if (adapter != null) {
            entityToneList = adapter.getDataList();
        } else {
            entityToneList = null;
        }
        if (entityToneList == null) {
            return false;
        }
        AsyncTask<List<EntityTone>, Void, Integer> mUpdateRingtoneTask = new AsyncTask<List<EntityTone>, Void, Integer>() {
            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
                return doInBackground((List<EntityTone>[]) ((List[]) objArr));
            }

            /* access modifiers changed from: protected */
            public Integer doInBackground(List<EntityTone>... paramsList) {
                CommonViewListeners.newAlertDialogMsg("\nProcessOnClick()...");
                return Integer.valueOf(AbstractRingtoneActivity.this.mLookup.update(this.getContentResolver(), paramsList[0]));
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Integer result) {
                Toast.makeText(this, result + (" " + AbstractRingtoneActivity.this.getText(R.string.updated).toString()), 0).show();
                AbstractRingtoneActivity.this.removeTask(this, AbstractRingtoneActivity.UPDATERINGTONETASK);
                if (!AbstractRingtoneActivity.this.hasDupTask(AbstractRingtoneActivity.UPDATERINGTONETASK)) {
                    AbstractRingtoneActivity.this.checkProfile(false);
                }
            }
        };
        updateIsActiveLabelStatus(R.string.setting);
        addTask(mUpdateRingtoneTask, UPDATERINGTONETASK);
        mUpdateRingtoneTask.execute(entityToneList);
        return true;
    }

    public boolean processSpinnerItemSelected(Profile profile) {
        EntityToneListAdapter adapters = createListAdapter(profile, this.mLookup);
        if (adapters == null || adapters.getCount() == 0) {
            return false;
        }
        setListAdapter(adapters, this.mlistView, this);
        checkProfile(true);
        return true;
    }

    public void checkProfile(boolean triggeredBySpinner) {
        EntityToneListAdapter adapter;
        List<EntityTone> entityToneList;
        if (this.mlistView.getAdapter() != null) {
            adapter = (EntityToneListAdapter) this.mlistView.getAdapter();
        } else {
            adapter = null;
        }
        if (adapter != null) {
            entityToneList = adapter.getDataList();
        } else {
            entityToneList = null;
        }
        if (entityToneList != null) {
            AsyncTask<List<EntityTone>, Void, Boolean> mCheckActiveRingtoneTask = new AsyncTask<List<EntityTone>, Void, Boolean>() {
                /* access modifiers changed from: protected */
                public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
                    return doInBackground((List<EntityTone>[]) ((List[]) objArr));
                }

                /* access modifiers changed from: protected */
                public Boolean doInBackground(List<EntityTone>... paramsList) {
                    return Boolean.valueOf(AbstractRingtoneActivity.this.mLookup.isListRingtoneActive(this.getContentResolver(), paramsList[0]));
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Boolean result) {
                    if (result.booleanValue()) {
                        AbstractRingtoneActivity.this.updateIsActiveLabelStatus(R.string.yes);
                    } else {
                        AbstractRingtoneActivity.this.updateIsActiveLabelStatus(R.string.no);
                    }
                    AbstractRingtoneActivity.this.removeTask(this, AbstractRingtoneActivity.CHECKRINGTONETASK);
                    if (!AbstractRingtoneActivity.this.hasDupTask(AbstractRingtoneActivity.CHECKRINGTONETASK)) {
                        AbstractRingtoneActivity.this.showDebugWindow();
                    }
                }
            };
            updateIsActiveLabelStatus(R.string.checking);
            addTask(mCheckActiveRingtoneTask, CHECKRINGTONETASK);
            mCheckActiveRingtoneTask.execute(entityToneList);
        }
        if (!triggeredBySpinner) {
            this.mCommonViewListener.check4Profile(getMainComponents(), this.mSpinner);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void preSaveOp(Profile profile) {
        EntityToneListAdapter adapters;
        if (this.mlistView.getAdapter() != null) {
            adapters = (EntityToneListAdapter) this.mlistView.getAdapter();
        } else {
            adapters = null;
        }
        if (adapters != null && adapters.getCount() != 0) {
            List<EntityTone> outBoundList = adapters.getDataList();
            List<EntityTone> inBoundlist = profile.getValueList();
            if (inBoundlist == null) {
                inBoundlist = new ArrayList<>();
            } else {
                inBoundlist.clear();
            }
            for (EntityTone e : outBoundList) {
                try {
                    inBoundlist.add(e.deepClone());
                } catch (CloneNotSupportedException e2) {
                    ErrorLog.log(e2, ErrorLog.Levels.SEVERE);
                }
            }
            profile.setValueList(inBoundlist);
        }
    }

    public boolean checkLimit(LimitEnforcer.Feature feature, List list, boolean shouldClip) {
        boolean check;
        if (shouldClip) {
            check = LimitEnforcer.checkNClip(feature, list, this);
        } else {
            check = LimitEnforcer.hasReachedLimit(feature, list, this);
        }
        if (check) {
            int resource = LimitEnforcer.Feature.ADD_ENTITY == feature ? R.string.limit_entity : R.string.limit_profile;
            if (CommonViewListeners.ISDEBUG) {
                CommonViewListeners.appendAlertDialogMsg(getText(resource).toString());
            } else {
                CommonViewListeners.newAlertDialogMsg(getText(resource).toString());
            }
            showDialog(2);
        }
        return check;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == 4) {
            this.mIsCloseUsrInvoked = true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: private */
    public void updateIsActiveLabelStatus(int res) {
        StringBuilder msg = new StringBuilder(String.valueOf((String) getText(R.string.is_active)) + " ");
        switch (res) {
            case R.string.yes /*2130968610*/:
                msg.append(getText(R.string.yes));
                this.mTxtViewIsActive.setTextColor(-16711936);
                break;
            case R.string.no /*2130968611*/:
                msg.append(((Object) getText(R.string.no)) + " " + ((Object) getText(R.string.to_make_active)));
                this.mTxtViewIsActive.setTextColor(-65536);
                break;
            default:
                msg.append((String) getText(res));
                this.mTxtViewIsActive.setTextColor(-7829368);
                break;
        }
        this.mTxtViewIsActive.setText(msg);
    }

    public void showDebugWindow() {
        if (CommonViewListeners.ISDEBUG) {
            showDialog(2);
        }
    }

    public void addTask(AsyncTask task, String taskname) {
        synchronized (this.mSyncObj) {
            this.mTaskList.add(task);
            this.mActiveTaskNameList.add(taskname);
            this.mSyncObj.notifyAll();
        }
    }

    public void removeTask(AsyncTask task, String taskname) {
        synchronized (this.mSyncObj) {
            this.mTaskList.remove(task);
            this.mActiveTaskNameList.remove(taskname);
            this.mSyncObj.notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hasDupTask(String taskname) {
        boolean found;
        synchronized (this.mSyncObj) {
            found = this.mActiveTaskNameList.contains(taskname);
            this.mSyncObj.notifyAll();
        }
        return found;
    }
}
