package com.smartapptechnology.soundprofiler;

import android.os.AsyncTask;
import android.view.View;
import android.widget.TableLayout;
import com.smartapptechnology.soundprofiler.mgr.EntityTone;
import com.smartapptechnology.soundprofiler.mgr.LimitEnforcer;
import com.smartapptechnology.soundprofiler.mgr.Profile;
import java.util.List;

public interface IActivity<T> {
    void addTask(AsyncTask asyncTask, String str);

    void buildHelpContent(TableLayout tableLayout);

    boolean checkLimit(LimitEnforcer.Feature feature, List list, boolean z);

    void checkProfile(boolean z);

    List<Profile> createDefaultProfiles();

    void enableStoreBtn(boolean z);

    T[] getMainComponents();

    T getSubComponent(T t, boolean z);

    ToneType getToneType(EntityTone entityTone);

    void preSaveOp(Profile profile);

    boolean processOnClick(View view);

    boolean processSpinnerItemSelected(Profile profile);

    void removeTask(AsyncTask asyncTask, String str);
}
