package com.smartapptechnology.soundprofiler;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.smartapptechnology.soundprofiler.mgr.EntityTone;
import com.smartapptechnology.soundprofiler.mgr.LimitEnforcer;
import com.smartapptechnology.soundprofiler.mgr.Profile;
import com.smartapptechnology.soundprofiler.mgr.ProfileManager;
import com.smartapptechnology.soundprofiler.mgr.Utility;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SoundManagerActivity extends Activity implements IActivity<Integer> {
    public static final String PROF_LOUD = "Loud";
    public static final String PROF_MUTE = "Mute";
    public static final String PROF_QUIET = "Quite";
    public static final int RINGER_MODE = 1001;
    public static final int RINGER_MODE_NORMAL = 2;
    public static final int RINGER_MODE_SILENT = 0;
    public static final int RINGER_MODE_VIBRATE = 1;
    public static final String SOUNDPROFILE = "SoundProfiler";
    public static final int VIBRATE_SETTING_OFF = 0;
    public static final int VIBRATE_SETTING_ON = 1;
    public static final int VIBRATE_SETTING_ONLY_SILENT = 2;
    private static final Integer[] mStreams = {2, 5, 3, 4, 1, 0};
    private static final Integer[] mViewLbls = {Integer.valueOf((int) R.id.ringtone_lbl), Integer.valueOf((int) R.id.notif_lbl), Integer.valueOf((int) R.id.media_lbl), Integer.valueOf((int) R.id.alarm_lbl), Integer.valueOf((int) R.id.system_lbl), Integer.valueOf((int) R.id.voicecall_lbl)};
    private static final Integer[] mViewSeekbars = {Integer.valueOf((int) R.id.ringtone_skb), Integer.valueOf((int) R.id.notif_skb), Integer.valueOf((int) R.id.media_skb), Integer.valueOf((int) R.id.alarm_skb), Integer.valueOf((int) R.id.system_skb), Integer.valueOf((int) R.id.voicecall_skb)};
    private AudioManager mAudioManager;
    private CommonViewListeners mCommonViewListener;
    private ProfileManager mProfileManager;
    private SoundManagerReceiver mReceiver;
    private SeekBar.OnSeekBarChangeListener mSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStopTrackingTouch(SeekBar seekBar) {
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                SoundManagerActivity.this.adjStreams(seekBar, progress);
                SoundManagerActivity.this.refreshViews(true);
            }
        }
    };
    private SoundViewsAdapter mSoundViewsAdapter;
    private Spinner mSpinner;
    private Button mStoreBtn;
    private HashMap<Integer, Integer> mVolHistMap = new HashMap<>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.layout_sound);
        this.mProfileManager = new ProfileManager(this);
        this.mCommonViewListener = new CommonViewListeners(this, this.mProfileManager);
        ArrayAdapter<Profile> arrayAdap = new ArrayAdapter<>(this, 17367048, this.mProfileManager.getProfiles());
        arrayAdap.setDropDownViewResource(17367049);
        this.mSpinner = (Spinner) findViewById(R.id.spinner);
        this.mSpinner.setAdapter((SpinnerAdapter) arrayAdap);
        this.mSpinner.setOnItemSelectedListener(this.mCommonViewListener.getItemSelectedListener());
        this.mAudioManager = getAudioManager();
        this.mSoundViewsAdapter = new SoundViewsAdapter(this, mViewLbls, mViewSeekbars, mStreams, this.mSeekBarChangeListener, this.mCommonViewListener.getOnClickListener());
        this.mStoreBtn = (Button) findViewById(R.id.save_btn);
        this.mStoreBtn.setOnClickListener(this.mCommonViewListener.getOnClickListener());
        setVolumeControlStream(2);
        this.mReceiver = new SoundManagerReceiver();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mProfileManager.storeProfiles();
        this.mProfileManager.destroy();
        this.mProfileManager = null;
        this.mAudioManager = null;
        this.mSeekBarChangeListener = null;
        this.mSoundViewsAdapter = null;
        this.mSpinner = null;
        this.mVolHistMap = null;
        if (this.mCommonViewListener != null) {
            this.mCommonViewListener.destroy();
        }
        this.mCommonViewListener = null;
        this.mReceiver = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mReceiver.registerActivity(this);
        refreshViews(true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mReceiver.unRegisterActivity();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = this.mCommonViewListener.processOnCreateDialog(id, null);
        if (dialog == null) {
            return super.onCreateDialog(id);
        }
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        this.mCommonViewListener.processOnPrepareDialog(id, dialog);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer
     arg types: [java.lang.Integer, int]
     candidates:
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Object, boolean):java.lang.Object
      com.smartapptechnology.soundprofiler.IActivity.getSubComponent(java.lang.Object, boolean):T
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == 24) {
            int stream = getVolumeControlStream();
            adjSingleStream(stream, getSubComponent(Integer.valueOf(stream), false).intValue() + 1);
            refreshViews(true);
            return true;
        } else if (event.getKeyCode() == 25) {
            int stream2 = getVolumeControlStream();
            adjSingleStream(stream2, getSubComponent(Integer.valueOf(stream2), false).intValue() - 1);
            refreshViews(true);
            return true;
        } else {
            if (keyCode == 67) {
                CommonViewListeners.mTriggerBypassMode = true;
                CommonViewListeners.mBypassKeyEntry.delete(0, CommonViewListeners.mBypassKeyEntry.length());
            }
            if (CommonViewListeners.mTriggerBypassMode) {
                CommonViewListeners.mBypassKeyEntry.append(keyCode);
            }
            if (keyCode == 62) {
                CommonViewListeners.mTriggerBypassMode = false;
                this.mCommonViewListener.byPassOps(this);
            }
            System.out.println("display = " + event.getDisplayLabel() + "; code = " + String.valueOf(keyCode));
            return super.onKeyDown(keyCode, event);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return this.mCommonViewListener.processOnCreateOptionMenu(menu, R.menu.app_menu, this);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return this.mCommonViewListener.processOnPrepareOptionsMenu(menu, this.mStoreBtn, this.mSpinner);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.mCommonViewListener.processOnOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.menu_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private AudioManager getAudioManager() {
        if (this.mAudioManager != null) {
            return this.mAudioManager;
        }
        AudioManager audioManager = (AudioManager) getSystemService("audio");
        this.mAudioManager = audioManager;
        return audioManager;
    }

    public Integer[] getMainComponents() {
        return mStreams;
    }

    public Integer getSubComponent(Integer type, boolean getMax) {
        if (this.mAudioManager == null) {
            this.mAudioManager = getAudioManager();
        }
        if (getMax) {
            return Integer.valueOf(this.mAudioManager.getStreamMaxVolume(type.intValue()));
        }
        return Integer.valueOf(this.mAudioManager.getStreamVolume(type.intValue()));
    }

    public int getVibrateSetting() {
        if (CommonViewListeners.ISDEBUG) {
            System.out.println("VIBRATE_TYPE_RINGER: " + this.mAudioManager.getVibrateSetting(0));
            System.out.println("VIBRATE_TYPE_NOTIFICATION: " + this.mAudioManager.getVibrateSetting(1));
        }
        return this.mAudioManager.getVibrateSetting(0);
    }

    public int getRingerMode() {
        return this.mAudioManager.getRingerMode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void broadcastModeChange(boolean isVibrateChangedAlso) {
        Intent intent = new Intent();
        intent.setAction("android.media.RINGER_MODE_CHANGED");
        if (isVibrateChangedAlso) {
            intent.setAction("android.media.VIBRATE_SETTING_CHANGED");
        }
        intent.putExtra(SOUNDPROFILE, true);
        sendBroadcast(intent);
    }

    /* access modifiers changed from: private */
    public void adjStreams(SeekBar seekBar, int volumeIdx) {
        int limit = mStreams.length;
        int i = 0;
        while (i < limit) {
            if (seekBar == null || seekBar.getId() != mViewSeekbars[i].intValue()) {
                if (seekBar == null) {
                    adjSingleStream(mStreams[i].intValue(), volumeIdx);
                }
                i++;
            } else {
                adjSingleStream(mStreams[i].intValue(), volumeIdx);
                this.mSoundViewsAdapter.modLblByStrmType(mStreams[i].intValue(), volumeIdx);
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer
     arg types: [java.lang.Integer, int]
     candidates:
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Object, boolean):java.lang.Object
      com.smartapptechnology.soundprofiler.IActivity.getSubComponent(java.lang.Object, boolean):T
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer */
    public void adjSingleStream(int streamType, int volumeIdx) {
        if (this.mAudioManager == null) {
            this.mAudioManager = getAudioManager();
        }
        if (volumeIdx >= 0) {
            int curVol = getSubComponent(Integer.valueOf(streamType), false).intValue();
            int histVol = volumeIdx;
            if (volumeIdx < 1) {
                if (curVol < 1) {
                    histVol = getSubComponent(Integer.valueOf(streamType), true).intValue() / 2;
                } else {
                    histVol = curVol;
                }
            }
            if (streamType == 2) {
                if ((getRingerMode() == 0 || getRingerMode() == 1) && volumeIdx > 0) {
                    this.mAudioManager.setRingerMode(2);
                    this.mVolHistMap.put(Integer.valueOf((int) RINGER_MODE), 2);
                } else if (getRingerMode() != 2 || volumeIdx >= 1) {
                    this.mVolHistMap.put(Integer.valueOf((int) RINGER_MODE), Integer.valueOf(getRingerMode()));
                } else {
                    this.mAudioManager.setRingerMode(0);
                    this.mVolHistMap.put(Integer.valueOf((int) RINGER_MODE), 0);
                }
                broadcastModeChange(false);
            }
            this.mVolHistMap.put(Integer.valueOf(streamType), Integer.valueOf(histVol));
            this.mAudioManager.setStreamVolume(streamType, volumeIdx, 4);
        }
    }

    public void refreshViews(boolean shouldChk4Profile) {
        this.mSoundViewsAdapter.refresh();
        if (shouldChk4Profile) {
            checkProfile(shouldChk4Profile);
        }
    }

    public void checkProfile(boolean triggeredByUI) {
        this.mCommonViewListener.check4Profile(mStreams, this.mSpinner);
    }

    public void enableStoreBtn(boolean enabled) {
        if (this.mStoreBtn != null) {
            this.mStoreBtn.setEnabled(enabled);
        }
    }

    public List<Profile> createDefaultProfiles() {
        ArrayList<Profile> profList = new ArrayList<>();
        Profile profile = new Profile();
        profile.setName((String) getText(R.string.profile_current));
        profile.setValues(null);
        profList.add(profile);
        Profile profile2 = new Profile();
        profile2.setName((String) getText(R.string.profile_loud));
        profile2.setValues(createDefStreamsLevels(PROF_LOUD));
        profList.add(profile2);
        Profile profile3 = new Profile();
        profile3.setName((String) getText(R.string.profile_quiet));
        profile3.setValues(createDefStreamsLevels(PROF_QUIET));
        profList.add(profile3);
        Profile profile4 = new Profile();
        profile4.setName((String) getText(R.string.profile_mute));
        profile4.setValues(createDefStreamsLevels(PROF_MUTE));
        profList.add(profile4);
        return profList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer
     arg types: [java.lang.Integer, int]
     candidates:
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Object, boolean):java.lang.Object
      com.smartapptechnology.soundprofiler.IActivity.getSubComponent(java.lang.Object, boolean):T
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer */
    private String createDefStreamsLevels(String profType) {
        StringBuilder sblder = new StringBuilder();
        int limit = mStreams.length;
        for (int i = 0; i < limit; i++) {
            sblder.append(mStreams[i] + Utility.DEL);
            if (PROF_MUTE.equals(profType)) {
                sblder.append("0");
            } else if (PROF_QUIET.equals(profType)) {
                sblder.append(getSubComponent(mStreams[i], true));
            } else if (PROF_LOUD.equals(profType)) {
                sblder.append(getSubComponent(mStreams[i], true));
            }
            sblder.append(Utility.SEP);
        }
        return sblder.toString();
    }

    public boolean processSpinnerItemSelected(Profile profile) {
        int[][] stream_Vol = this.mProfileManager.parseStreamVol(profile.getValues());
        if (stream_Vol == null) {
            return false;
        }
        for (int i = 0; i < stream_Vol.length; i++) {
            adjSingleStream(stream_Vol[i][0], stream_Vol[i][1]);
        }
        refreshViews(false);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer
     arg types: [java.lang.Integer, int]
     candidates:
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Object, boolean):java.lang.Object
      com.smartapptechnology.soundprofiler.IActivity.getSubComponent(java.lang.Object, boolean):T
      com.smartapptechnology.soundprofiler.SoundManagerActivity.getSubComponent(java.lang.Integer, boolean):java.lang.Integer */
    public boolean processOnClick(View view) {
        int mode;
        int vol;
        switch (view.getId()) {
            case R.id.vibrate_lbl:
                String title = (String) ((TextView) view).getText();
                if (getString(R.string.vibrate_off).equals(title)) {
                    this.mVolHistMap.put(Integer.valueOf((int) RINGER_MODE), Integer.valueOf(getRingerMode()));
                    this.mAudioManager.setVibrateSetting(0, 1);
                    this.mAudioManager.setVibrateSetting(1, 1);
                    this.mAudioManager.setRingerMode(1);
                } else if (getString(R.string.vibrate_on).equals(title)) {
                    this.mAudioManager.setVibrateSetting(0, 0);
                    this.mAudioManager.setVibrateSetting(1, 0);
                    if (this.mVolHistMap.get(Integer.valueOf((int) RINGER_MODE)) != null) {
                        mode = this.mVolHistMap.remove(Integer.valueOf((int) RINGER_MODE)).intValue();
                    } else {
                        mode = 2;
                    }
                    this.mAudioManager.setRingerMode(mode);
                }
                refreshViews(true);
                break;
            default:
                int i = 0;
                while (true) {
                    if (i >= mViewLbls.length) {
                        break;
                    } else if (view.getId() == mViewLbls[i].intValue()) {
                        if (getSubComponent(mStreams[i], false).intValue() > 0) {
                            adjSingleStream(mStreams[i].intValue(), 0);
                        } else {
                            if (this.mVolHistMap.get(mStreams[i]) == null) {
                                vol = getSubComponent(mStreams[i], true).intValue() / 2;
                            } else {
                                vol = this.mVolHistMap.get(mStreams[i]).intValue();
                            }
                            adjSingleStream(mStreams[i].intValue(), vol);
                        }
                        refreshViews(true);
                        break;
                    } else {
                        i++;
                    }
                }
        }
        return false;
    }

    public ToneType getToneType(EntityTone entityTone) {
        return ToneType.SOUND;
    }

    public void preSaveOp(Profile profile) {
    }

    public void showDebugWindow() {
        if (CommonViewListeners.ISDEBUG) {
            showDialog(2);
        }
    }

    public void buildHelpContent(TableLayout tableLayout) {
        String[] soundInstr = getResources().getStringArray(R.array.sound_help);
        String[] profInstr = getResources().getStringArray(R.array.profile_help);
        ArrayList<String> helpInstrList = new ArrayList<>();
        helpInstrList.addAll(Arrays.asList(soundInstr));
        helpInstrList.addAll(Arrays.asList(profInstr));
        String[] helpInstr = (String[]) helpInstrList.toArray(new String[1]);
        int size = helpInstr.length;
        for (int i = 0; i < size; i++) {
            TableRow tableRow = new TableRow(getApplicationContext());
            tableRow.setPadding(0, 10, 0, 0);
            TextView textView = new TextView(getApplicationContext());
            textView.setPadding(2, 2, 2, 2);
            textView.setGravity(3);
            textView.setText(String.valueOf(i + 1) + ".");
            tableRow.addView(textView);
            TextView textView2 = new TextView(getApplicationContext());
            textView2.setPadding(2, 2, 2, 2);
            textView2.setGravity(3);
            textView2.setText(helpInstr[i]);
            tableRow.addView(textView2);
            tableLayout.addView(tableRow);
        }
    }

    public boolean checkLimit(LimitEnforcer.Feature feature, List list, boolean shouldClip) {
        return false;
    }

    public void addTask(AsyncTask task, String taskname) {
    }

    public void removeTask(AsyncTask task, String taskname) {
    }
}
