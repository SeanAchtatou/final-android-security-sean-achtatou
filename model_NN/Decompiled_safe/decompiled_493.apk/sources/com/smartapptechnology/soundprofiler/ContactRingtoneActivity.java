package com.smartapptechnology.soundprofiler;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.smartapptechnology.soundprofiler.mgr.AbstractContactLookup;
import com.smartapptechnology.soundprofiler.mgr.EntityTone;
import com.smartapptechnology.soundprofiler.mgr.LimitEnforcer;
import com.smartapptechnology.soundprofiler.mgr.Profile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ContactRingtoneActivity extends AbstractRingtoneActivity {
    static final String LOADALLCONTACTSTASK = "LOADALLCONTACTSTASK";
    static final String LOADCONTACTTASK = "LOADCONTACTTASK";
    private static final int PICKCONTACT_INTENT = 2;

    /* access modifiers changed from: protected */
    public Profile onCreateMyListFromProfile(final List<Profile> profiles) {
        final Profile profile = determineActiveProfile(profiles);
        this.mLookup = AbstractContactLookup.getInstance(this);
        this.mlistView = (ListView) findViewById(R.id.list_ringtone);
        AsyncTask<Void, Void, Void> loadTask = new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... params) {
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Void result) {
                ContactRingtoneActivity.this.setListAdapter(ContactRingtoneActivity.this.createListAdapter(profile, ContactRingtoneActivity.this.mLookup), ContactRingtoneActivity.this.mlistView, ContactRingtoneActivity.this);
                if (profile != null && ContactRingtoneActivity.this.getText(R.string.default_str).toString().equalsIgnoreCase(profile.getName())) {
                    profile.setValues(null);
                    ContactRingtoneActivity.this.mProfileManager.sort(profiles);
                }
                ContactRingtoneActivity.this.mCommonViewListener.cancelProgressDialog();
                ContactRingtoneActivity.this.removeTask(this, "LOADCONTACTSTASK");
                ContactRingtoneActivity.this.continueOnCreate(profile);
            }
        };
        showDialog(4);
        addTask(loadTask, "LOADCONTACTSTASK");
        loadTask.execute(new Void[0]);
        return profile;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clearall /*2131230766*/:
                removeAll(this.mlistView.getAdapter());
                setListAdapter((EntityToneListAdapter) this.mlistView.getAdapter(), this.mlistView, this);
                checkProfile(false);
                return true;
            case R.id.menu_contacts /*2131230767*/:
                Intent i = this.mLookup.getPickerIntent();
                if (CommonViewListeners.ISDEBUG) {
                    CommonViewListeners.captureIntentInfo("\nPick Cntct Intent...", i, false);
                }
                startActivityForResult(i, 2);
                showDebugWindow();
                return true;
            case R.id.menu_contactsall /*2131230768*/:
                loadAllContacts();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo contextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.menu_context_delete /*2131230765*/:
                removeFromList(contextMenuInfo.id, this.mlistView.getAdapter());
                setListAdapter((EntityToneListAdapter) this.mlistView.getAdapter(), this.mlistView, this);
                checkProfile(false);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2 && resultCode == -1) {
            if (CommonViewListeners.ISDEBUG) {
                CommonViewListeners.captureIntentInfo("\nReturn Cntct Intent...", data, true);
            }
            loadContactInfo(data.getData());
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void loadAllContacts() {
        AsyncTask<Integer, Void, List<EntityTone>> mLoadAllContactsTask = new AsyncTask<Integer, Void, List<EntityTone>>() {
            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
                onPostExecute((List<EntityTone>) ((List) obj));
            }

            /* access modifiers changed from: protected */
            public List<EntityTone> doInBackground(Integer... params) {
                return ContactRingtoneActivity.this.mLookup.loadAllEntities(ContactRingtoneActivity.this.getContentResolver());
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(List<EntityTone> result) {
                if (result != null) {
                    ContactRingtoneActivity.this.mProfileManager.sort(result);
                    ContactRingtoneActivity.this.checkLimit(LimitEnforcer.Feature.ADD_ENTITY, result, true);
                    Profile profile = new Profile();
                    profile.setValueList(result);
                    ContactRingtoneActivity.this.setListAdapter(ContactRingtoneActivity.this.createListAdapter(profile, ContactRingtoneActivity.this.mLookup), ContactRingtoneActivity.this.mlistView, this);
                }
                ContactRingtoneActivity.this.mCommonViewListener.cancelProgressDialog();
                ContactRingtoneActivity.this.removeTask(this, ContactRingtoneActivity.LOADALLCONTACTSTASK);
                if (!ContactRingtoneActivity.this.hasDupTask(ContactRingtoneActivity.LOADALLCONTACTSTASK)) {
                    ContactRingtoneActivity.this.checkProfile(false);
                }
            }
        };
        removeAll(this.mlistView.getAdapter());
        showDialog(4);
        addTask(mLoadAllContactsTask, LOADALLCONTACTSTASK);
        mLoadAllContactsTask.execute(0);
    }

    private void loadContactInfo(Uri dataUri) {
        AsyncTask<Uri, Void, EntityTone> mLoadContactTask = new AsyncTask<Uri, Void, EntityTone>() {
            /* access modifiers changed from: protected */
            public EntityTone doInBackground(Uri... paramsUri) {
                if (CommonViewListeners.ISDEBUG) {
                    CommonViewListeners.appendAlertDialogMsg("\nLoad Contact...\nUri= " + paramsUri[0].toString());
                }
                return ((AbstractContactLookup) ContactRingtoneActivity.this.mLookup).load(ContactRingtoneActivity.this.getContentResolver(), paramsUri[0]);
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(EntityTone result) {
                ContactRingtoneActivity.this.mCommonViewListener.cancelProgressDialog();
                if (result != null) {
                    ContactRingtoneActivity.this.loadSingle2View(result);
                }
                ContactRingtoneActivity.this.removeTask(this, ContactRingtoneActivity.LOADCONTACTTASK);
            }
        };
        showDialog(4);
        addTask(mLoadContactTask, LOADCONTACTTASK);
        mLoadContactTask.execute(dataUri);
    }

    /* access modifiers changed from: private */
    public void loadSingle2View(EntityTone contactTone) {
        EntityToneListAdapter adapter;
        if (contactTone.isDisplayable() || CommonViewListeners.ISDEBUG) {
            if (this.mlistView.getAdapter() != null) {
                adapter = (EntityToneListAdapter) this.mlistView.getAdapter();
            } else {
                adapter = null;
            }
            if (adapter == null) {
                ArrayList<EntityTone> list = new ArrayList<>();
                Profile profile = new Profile();
                profile.setValueList(list);
                adapter = createListAdapter(profile, this.mLookup);
            }
            if (!checkLimit(LimitEnforcer.Feature.ADD_ENTITY, adapter.getDataList(), false) && !adapter.contains(contactTone)) {
                adapter.add(contactTone);
                setListAdapter(adapter, this.mlistView, this);
                checkProfile(false);
                return;
            }
            return;
        }
        Exception ex = contactTone.getException();
        if (ex != null) {
            if (CommonViewListeners.ISDEBUG) {
                CommonViewListeners.appendAlertDialogMsg(ex.getMessage());
            } else {
                CommonViewListeners.newAlertDialogMsg(ex.getMessage());
            }
            showDialog(2);
        }
    }

    private void removeAll(ListAdapter listAdapter) {
        if (listAdapter != null) {
            ((EntityToneListAdapter) listAdapter).removeAll();
        }
    }

    private EntityTone removeFromList(long idx, ListAdapter listAdapter) {
        if (listAdapter == null) {
            return null;
        }
        return ((EntityToneListAdapter) listAdapter).remove((int) idx);
    }

    public ToneType getToneType(EntityTone entityTone) {
        entityTone.setToneType(ToneType.RINGTONE);
        return ToneType.RINGTONE;
    }

    public void buildHelpContent(TableLayout tableLayout) {
        String[] contInstr = getResources().getStringArray(R.array.contact_help);
        String[] alarmInstr = getResources().getStringArray(R.array.alarm_help);
        String[] profInstr = getResources().getStringArray(R.array.profile_help);
        ArrayList<String> helpInstrList = new ArrayList<>();
        helpInstrList.addAll(Arrays.asList(contInstr));
        helpInstrList.addAll(Arrays.asList(alarmInstr));
        helpInstrList.addAll(Arrays.asList(profInstr));
        String[] helpInstr = (String[]) helpInstrList.toArray(new String[1]);
        int size = helpInstr.length;
        for (int i = 0; i < size; i++) {
            TableRow tableRow = new TableRow(getApplicationContext());
            tableRow.setPadding(0, 10, 0, 0);
            TextView textView = new TextView(getApplicationContext());
            textView.setPadding(2, 2, 2, 2);
            textView.setGravity(3);
            textView.setText(String.valueOf(i + 1) + ".");
            tableRow.addView(textView);
            TextView textView2 = new TextView(getApplicationContext());
            textView2.setPadding(2, 2, 2, 2);
            textView2.setGravity(3);
            textView2.setText(helpInstr[i]);
            tableRow.addView(textView2);
            tableLayout.addView(tableRow);
        }
    }
}
