package com.smartapptechnology.soundprofiler;

public enum ToneType {
    RINGTONE(1),
    ALARM(4),
    NOTIFICATION(2),
    SOUND(101),
    OTHERS(202);
    
    private final int mCode;

    public int getCode() {
        return this.mCode;
    }

    private ToneType(int code) {
        this.mCode = code;
    }

    public static ToneType valueOf(int code) {
        for (ToneType toneType : values()) {
            if (toneType.mCode == code) {
                return toneType;
            }
        }
        return OTHERS;
    }
}
