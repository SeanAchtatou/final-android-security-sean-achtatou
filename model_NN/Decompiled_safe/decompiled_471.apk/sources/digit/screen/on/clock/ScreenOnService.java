package digit.screen.on.clock;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import java.util.Calendar;

public class ScreenOnService extends Service {
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            ScreenOnService.this.mNotificationManager.cancelAll();
        }
    };
    private Calendar mCalendar = null;
    /* access modifiers changed from: private */
    public int mEdit_preference = 0;
    /* access modifiers changed from: private */
    public boolean mIsRepeat = true;
    /* access modifiers changed from: private */
    public Notification mNotification1 = null;
    /* access modifiers changed from: private */
    public Notification mNotification2 = null;
    /* access modifiers changed from: private */
    public Notification mNotification3 = null;
    /* access modifiers changed from: private */
    public NotificationManager mNotificationManager = null;
    /* access modifiers changed from: private */
    public int mOld_hour = 0;
    /* access modifiers changed from: private */
    public int mOld_minute = 0;
    /* access modifiers changed from: private */
    public PendingIntent mPendingIntent = null;
    private BroadcastReceiver mReceiver1 = null;
    private BroadcastReceiver mReceiver2 = null;
    private BroadcastReceiver mReceiver3 = null;
    /* access modifiers changed from: private */
    public SharedPreferences mSP = null;
    /* access modifiers changed from: private */
    public Thread mThread = null;

    public void onCreate() {
        super.onCreate();
        IntentFilter filter1 = new IntentFilter("android.intent.action.SCREEN_ON");
        IntentFilter filter2 = new IntentFilter("android.intent.action.SCREEN_OFF");
        IntentFilter filter3 = new IntentFilter("android.intent.action.USER_PRESENT");
        this.mSP = PreferenceManager.getDefaultSharedPreferences(this);
        this.mNotificationManager = (NotificationManager) getSystemService("notification");
        this.mNotification1 = new Notification();
        this.mNotification2 = new Notification();
        this.mNotification3 = new Notification();
        this.mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, ScreenOnClockActivity.class), 0);
        this.mReceiver1 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.d("TimeBroadcastReceiver", "onReceive");
                ScreenOnService.this.mEdit_preference = Integer.valueOf(ScreenOnService.this.mSP.getString("edittext_key", "5")).intValue();
                Runnable looper = new Runnable() {
                    public void run() {
                        int cnt = 0;
                        ScreenOnService.this.mOld_hour = 0;
                        ScreenOnService.this.mOld_minute = 0;
                        while (ScreenOnService.this.mIsRepeat && cnt < ScreenOnService.this.mEdit_preference + 1) {
                            ScreenOnService.this.doSomething();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                Log.e("looper", "InterruptedException");
                            }
                            cnt++;
                        }
                        ScreenOnService.this.handler.sendEmptyMessage(0);
                    }
                };
                ScreenOnService.this.mNotification1.setLatestEventInfo(ScreenOnService.this, null, null, ScreenOnService.this.mPendingIntent);
                ScreenOnService.this.mNotification2.setLatestEventInfo(ScreenOnService.this, null, null, ScreenOnService.this.mPendingIntent);
                ScreenOnService.this.mNotification3.setLatestEventInfo(ScreenOnService.this, null, null, ScreenOnService.this.mPendingIntent);
                if (Build.VERSION.SDK_INT < 9) {
                    ScreenOnService.this.mNotification1.when = System.currentTimeMillis() + 10000;
                    ScreenOnService.this.mNotification2.when = System.currentTimeMillis() + 20000;
                    ScreenOnService.this.mNotification3.when = System.currentTimeMillis() + 30000;
                } else {
                    ScreenOnService.this.mNotification1.when = System.currentTimeMillis() + 30000;
                    ScreenOnService.this.mNotification2.when = System.currentTimeMillis() + 20000;
                    ScreenOnService.this.mNotification3.when = System.currentTimeMillis() + 10000;
                }
                ScreenOnService.this.mNotification1.flags = 2;
                ScreenOnService.this.mNotification2.flags = 2;
                ScreenOnService.this.mNotification3.flags = 2;
                ScreenOnService.this.mIsRepeat = true;
                ScreenOnService.this.mThread = new Thread(looper);
                ScreenOnService.this.mThread.start();
            }
        };
        this.mReceiver2 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                ScreenOnService.this.mIsRepeat = false;
                if (ScreenOnService.this.mThread != null) {
                    ScreenOnService.this.mThread.interrupt();
                }
                ScreenOnService.this.mNotificationManager.cancelAll();
            }
        };
        this.mReceiver3 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                ScreenOnService.this.mIsRepeat = false;
                if (ScreenOnService.this.mThread != null) {
                    ScreenOnService.this.mThread.interrupt();
                }
                ScreenOnService.this.mNotificationManager.cancelAll();
            }
        };
        registerReceiver(this.mReceiver1, filter1);
        registerReceiver(this.mReceiver2, filter2);
        registerReceiver(this.mReceiver3, filter3);
    }

    /* access modifiers changed from: private */
    public void doSomething() {
        this.mCalendar = Calendar.getInstance();
        int int_hour = this.mCalendar.get(11);
        int int_minute = this.mCalendar.get(12);
        int int_second = this.mCalendar.get(13);
        String list_preference = this.mSP.getString("list_key", "unknown");
        if (list_preference.equals("kuro") || list_preference.equals("unknown")) {
            if (int_hour == 0) {
                this.mNotification1.icon = R.drawable.time_c_00;
            } else if (int_hour == 1) {
                this.mNotification1.icon = R.drawable.time_c_01;
            } else if (int_hour == 2) {
                this.mNotification1.icon = R.drawable.time_c_02;
            } else if (int_hour == 3) {
                this.mNotification1.icon = R.drawable.time_c_03;
            } else if (int_hour == 4) {
                this.mNotification1.icon = R.drawable.time_c_04;
            } else if (int_hour == 5) {
                this.mNotification1.icon = R.drawable.time_c_05;
            } else if (int_hour == 6) {
                this.mNotification1.icon = R.drawable.time_c_06;
            } else if (int_hour == 7) {
                this.mNotification1.icon = R.drawable.time_c_07;
            } else if (int_hour == 8) {
                this.mNotification1.icon = R.drawable.time_c_08;
            } else if (int_hour == 9) {
                this.mNotification1.icon = R.drawable.time_c_09;
            } else if (int_hour == 10) {
                this.mNotification1.icon = R.drawable.time_c_10;
            } else if (int_hour == 11) {
                this.mNotification1.icon = R.drawable.time_c_11;
            } else if (int_hour == 12) {
                this.mNotification1.icon = R.drawable.time_c_12;
            } else if (int_hour == 13) {
                this.mNotification1.icon = R.drawable.time_c_13;
            } else if (int_hour == 14) {
                this.mNotification1.icon = R.drawable.time_c_14;
            } else if (int_hour == 15) {
                this.mNotification1.icon = R.drawable.time_c_15;
            } else if (int_hour == 16) {
                this.mNotification1.icon = R.drawable.time_c_16;
            } else if (int_hour == 17) {
                this.mNotification1.icon = R.drawable.time_c_17;
            } else if (int_hour == 18) {
                this.mNotification1.icon = R.drawable.time_c_18;
            } else if (int_hour == 19) {
                this.mNotification1.icon = R.drawable.time_c_19;
            } else if (int_hour == 20) {
                this.mNotification1.icon = R.drawable.time_c_20;
            } else if (int_hour == 21) {
                this.mNotification1.icon = R.drawable.time_c_21;
            } else if (int_hour == 22) {
                this.mNotification1.icon = R.drawable.time_c_22;
            } else if (int_hour == 23) {
                this.mNotification1.icon = R.drawable.time_c_23;
            }
            if (int_minute == 0) {
                this.mNotification2.icon = R.drawable.time_c_00;
            } else if (int_minute == 1) {
                this.mNotification2.icon = R.drawable.time_c_01;
            } else if (int_minute == 2) {
                this.mNotification2.icon = R.drawable.time_c_02;
            } else if (int_minute == 3) {
                this.mNotification2.icon = R.drawable.time_c_03;
            } else if (int_minute == 4) {
                this.mNotification2.icon = R.drawable.time_c_04;
            } else if (int_minute == 5) {
                this.mNotification2.icon = R.drawable.time_c_05;
            } else if (int_minute == 6) {
                this.mNotification2.icon = R.drawable.time_c_06;
            } else if (int_minute == 7) {
                this.mNotification2.icon = R.drawable.time_c_07;
            } else if (int_minute == 8) {
                this.mNotification2.icon = R.drawable.time_c_08;
            } else if (int_minute == 9) {
                this.mNotification2.icon = R.drawable.time_c_09;
            } else if (int_minute == 10) {
                this.mNotification2.icon = R.drawable.time_c_10;
            } else if (int_minute == 11) {
                this.mNotification2.icon = R.drawable.time_c_11;
            } else if (int_minute == 12) {
                this.mNotification2.icon = R.drawable.time_c_12;
            } else if (int_minute == 13) {
                this.mNotification2.icon = R.drawable.time_c_13;
            } else if (int_minute == 14) {
                this.mNotification2.icon = R.drawable.time_c_14;
            } else if (int_minute == 15) {
                this.mNotification2.icon = R.drawable.time_c_15;
            } else if (int_minute == 16) {
                this.mNotification2.icon = R.drawable.time_c_16;
            } else if (int_minute == 17) {
                this.mNotification2.icon = R.drawable.time_c_17;
            } else if (int_minute == 18) {
                this.mNotification2.icon = R.drawable.time_c_18;
            } else if (int_minute == 19) {
                this.mNotification2.icon = R.drawable.time_c_19;
            } else if (int_minute == 20) {
                this.mNotification2.icon = R.drawable.time_c_20;
            } else if (int_minute == 21) {
                this.mNotification2.icon = R.drawable.time_c_21;
            } else if (int_minute == 22) {
                this.mNotification2.icon = R.drawable.time_c_22;
            } else if (int_minute == 23) {
                this.mNotification2.icon = R.drawable.time_c_23;
            } else if (int_minute == 24) {
                this.mNotification2.icon = R.drawable.time_c_24;
            } else if (int_minute == 25) {
                this.mNotification2.icon = R.drawable.time_c_25;
            } else if (int_minute == 26) {
                this.mNotification2.icon = R.drawable.time_c_26;
            } else if (int_minute == 27) {
                this.mNotification2.icon = R.drawable.time_c_27;
            } else if (int_minute == 28) {
                this.mNotification2.icon = R.drawable.time_c_28;
            } else if (int_minute == 29) {
                this.mNotification2.icon = R.drawable.time_c_29;
            } else if (int_minute == 30) {
                this.mNotification2.icon = R.drawable.time_c_30;
            } else if (int_minute == 31) {
                this.mNotification2.icon = R.drawable.time_c_31;
            } else if (int_minute == 32) {
                this.mNotification2.icon = R.drawable.time_c_32;
            } else if (int_minute == 33) {
                this.mNotification2.icon = R.drawable.time_c_33;
            } else if (int_minute == 34) {
                this.mNotification2.icon = R.drawable.time_c_34;
            } else if (int_minute == 35) {
                this.mNotification2.icon = R.drawable.time_c_35;
            } else if (int_minute == 36) {
                this.mNotification2.icon = R.drawable.time_c_36;
            } else if (int_minute == 37) {
                this.mNotification2.icon = R.drawable.time_c_37;
            } else if (int_minute == 38) {
                this.mNotification2.icon = R.drawable.time_c_38;
            } else if (int_minute == 39) {
                this.mNotification2.icon = R.drawable.time_c_39;
            } else if (int_minute == 40) {
                this.mNotification2.icon = R.drawable.time_c_40;
            } else if (int_minute == 41) {
                this.mNotification2.icon = R.drawable.time_c_41;
            } else if (int_minute == 42) {
                this.mNotification2.icon = R.drawable.time_c_42;
            } else if (int_minute == 43) {
                this.mNotification2.icon = R.drawable.time_c_43;
            } else if (int_minute == 44) {
                this.mNotification2.icon = R.drawable.time_c_44;
            } else if (int_minute == 45) {
                this.mNotification2.icon = R.drawable.time_c_45;
            } else if (int_minute == 46) {
                this.mNotification2.icon = R.drawable.time_c_46;
            } else if (int_minute == 47) {
                this.mNotification2.icon = R.drawable.time_c_47;
            } else if (int_minute == 48) {
                this.mNotification2.icon = R.drawable.time_c_48;
            } else if (int_minute == 49) {
                this.mNotification2.icon = R.drawable.time_c_49;
            } else if (int_minute == 50) {
                this.mNotification2.icon = R.drawable.time_c_50;
            } else if (int_minute == 51) {
                this.mNotification2.icon = R.drawable.time_c_51;
            } else if (int_minute == 52) {
                this.mNotification2.icon = R.drawable.time_c_52;
            } else if (int_minute == 53) {
                this.mNotification2.icon = R.drawable.time_c_53;
            } else if (int_minute == 54) {
                this.mNotification2.icon = R.drawable.time_c_54;
            } else if (int_minute == 55) {
                this.mNotification2.icon = R.drawable.time_c_55;
            } else if (int_minute == 56) {
                this.mNotification2.icon = R.drawable.time_c_56;
            } else if (int_minute == 57) {
                this.mNotification2.icon = R.drawable.time_c_57;
            } else if (int_minute == 58) {
                this.mNotification2.icon = R.drawable.time_c_58;
            } else if (int_minute == 59) {
                this.mNotification2.icon = R.drawable.time_c_59;
            }
            if (int_second == 0) {
                this.mNotification3.icon = R.drawable.time_00;
            } else if (int_second == 1) {
                this.mNotification3.icon = R.drawable.time_01;
            } else if (int_second == 2) {
                this.mNotification3.icon = R.drawable.time_02;
            } else if (int_second == 3) {
                this.mNotification3.icon = R.drawable.time_03;
            } else if (int_second == 4) {
                this.mNotification3.icon = R.drawable.time_04;
            } else if (int_second == 5) {
                this.mNotification3.icon = R.drawable.time_05;
            } else if (int_second == 6) {
                this.mNotification3.icon = R.drawable.time_06;
            } else if (int_second == 7) {
                this.mNotification3.icon = R.drawable.time_07;
            } else if (int_second == 8) {
                this.mNotification3.icon = R.drawable.time_08;
            } else if (int_second == 9) {
                this.mNotification3.icon = R.drawable.time_09;
            } else if (int_second == 10) {
                this.mNotification3.icon = R.drawable.time_10;
            } else if (int_second == 11) {
                this.mNotification3.icon = R.drawable.time_11;
            } else if (int_second == 12) {
                this.mNotification3.icon = R.drawable.time_12;
            } else if (int_second == 13) {
                this.mNotification3.icon = R.drawable.time_13;
            } else if (int_second == 14) {
                this.mNotification3.icon = R.drawable.time_14;
            } else if (int_second == 15) {
                this.mNotification3.icon = R.drawable.time_15;
            } else if (int_second == 16) {
                this.mNotification3.icon = R.drawable.time_16;
            } else if (int_second == 17) {
                this.mNotification3.icon = R.drawable.time_17;
            } else if (int_second == 18) {
                this.mNotification3.icon = R.drawable.time_18;
            } else if (int_second == 19) {
                this.mNotification3.icon = R.drawable.time_19;
            } else if (int_second == 20) {
                this.mNotification3.icon = R.drawable.time_20;
            } else if (int_second == 21) {
                this.mNotification3.icon = R.drawable.time_21;
            } else if (int_second == 22) {
                this.mNotification3.icon = R.drawable.time_22;
            } else if (int_second == 23) {
                this.mNotification3.icon = R.drawable.time_23;
            } else if (int_second == 24) {
                this.mNotification3.icon = R.drawable.time_24;
            } else if (int_second == 25) {
                this.mNotification3.icon = R.drawable.time_25;
            } else if (int_second == 26) {
                this.mNotification3.icon = R.drawable.time_26;
            } else if (int_second == 27) {
                this.mNotification3.icon = R.drawable.time_27;
            } else if (int_second == 28) {
                this.mNotification3.icon = R.drawable.time_28;
            } else if (int_second == 29) {
                this.mNotification3.icon = R.drawable.time_29;
            } else if (int_second == 30) {
                this.mNotification3.icon = R.drawable.time_30;
            } else if (int_second == 31) {
                this.mNotification3.icon = R.drawable.time_31;
            } else if (int_second == 32) {
                this.mNotification3.icon = R.drawable.time_32;
            } else if (int_second == 33) {
                this.mNotification3.icon = R.drawable.time_33;
            } else if (int_second == 34) {
                this.mNotification3.icon = R.drawable.time_34;
            } else if (int_second == 35) {
                this.mNotification3.icon = R.drawable.time_35;
            } else if (int_second == 36) {
                this.mNotification3.icon = R.drawable.time_36;
            } else if (int_second == 37) {
                this.mNotification3.icon = R.drawable.time_37;
            } else if (int_second == 38) {
                this.mNotification3.icon = R.drawable.time_38;
            } else if (int_second == 39) {
                this.mNotification3.icon = R.drawable.time_39;
            } else if (int_second == 40) {
                this.mNotification3.icon = R.drawable.time_40;
            } else if (int_second == 41) {
                this.mNotification3.icon = R.drawable.time_41;
            } else if (int_second == 42) {
                this.mNotification3.icon = R.drawable.time_42;
            } else if (int_second == 43) {
                this.mNotification3.icon = R.drawable.time_43;
            } else if (int_second == 44) {
                this.mNotification3.icon = R.drawable.time_44;
            } else if (int_second == 45) {
                this.mNotification3.icon = R.drawable.time_45;
            } else if (int_second == 46) {
                this.mNotification3.icon = R.drawable.time_46;
            } else if (int_second == 47) {
                this.mNotification3.icon = R.drawable.time_47;
            } else if (int_second == 48) {
                this.mNotification3.icon = R.drawable.time_48;
            } else if (int_second == 49) {
                this.mNotification3.icon = R.drawable.time_49;
            } else if (int_second == 50) {
                this.mNotification3.icon = R.drawable.time_50;
            } else if (int_second == 51) {
                this.mNotification3.icon = R.drawable.time_51;
            } else if (int_second == 52) {
                this.mNotification3.icon = R.drawable.time_52;
            } else if (int_second == 53) {
                this.mNotification3.icon = R.drawable.time_53;
            } else if (int_second == 54) {
                this.mNotification3.icon = R.drawable.time_54;
            } else if (int_second == 55) {
                this.mNotification3.icon = R.drawable.time_55;
            } else if (int_second == 56) {
                this.mNotification3.icon = R.drawable.time_56;
            } else if (int_second == 57) {
                this.mNotification3.icon = R.drawable.time_57;
            } else if (int_second == 58) {
                this.mNotification3.icon = R.drawable.time_58;
            } else if (int_second == 59) {
                this.mNotification3.icon = R.drawable.time_59;
            }
        } else if (list_preference.equals("simple")) {
            if (int_hour == 0) {
                this.mNotification1.icon = R.drawable.time_c_00_s;
            } else if (int_hour == 1) {
                this.mNotification1.icon = R.drawable.time_c_01_s;
            } else if (int_hour == 2) {
                this.mNotification1.icon = R.drawable.time_c_02_s;
            } else if (int_hour == 3) {
                this.mNotification1.icon = R.drawable.time_c_03_s;
            } else if (int_hour == 4) {
                this.mNotification1.icon = R.drawable.time_c_04_s;
            } else if (int_hour == 5) {
                this.mNotification1.icon = R.drawable.time_c_05_s;
            } else if (int_hour == 6) {
                this.mNotification1.icon = R.drawable.time_c_06_s;
            } else if (int_hour == 7) {
                this.mNotification1.icon = R.drawable.time_c_07_s;
            } else if (int_hour == 8) {
                this.mNotification1.icon = R.drawable.time_c_08_s;
            } else if (int_hour == 9) {
                this.mNotification1.icon = R.drawable.time_c_09_s;
            } else if (int_hour == 10) {
                this.mNotification1.icon = R.drawable.time_c_10_s;
            } else if (int_hour == 11) {
                this.mNotification1.icon = R.drawable.time_c_11_s;
            } else if (int_hour == 12) {
                this.mNotification1.icon = R.drawable.time_c_12_s;
            } else if (int_hour == 13) {
                this.mNotification1.icon = R.drawable.time_c_13_s;
            } else if (int_hour == 14) {
                this.mNotification1.icon = R.drawable.time_c_14_s;
            } else if (int_hour == 15) {
                this.mNotification1.icon = R.drawable.time_c_15_s;
            } else if (int_hour == 16) {
                this.mNotification1.icon = R.drawable.time_c_16_s;
            } else if (int_hour == 17) {
                this.mNotification1.icon = R.drawable.time_c_17_s;
            } else if (int_hour == 18) {
                this.mNotification1.icon = R.drawable.time_c_18_s;
            } else if (int_hour == 19) {
                this.mNotification1.icon = R.drawable.time_c_19_s;
            } else if (int_hour == 20) {
                this.mNotification1.icon = R.drawable.time_c_20_s;
            } else if (int_hour == 21) {
                this.mNotification1.icon = R.drawable.time_c_21_s;
            } else if (int_hour == 22) {
                this.mNotification1.icon = R.drawable.time_c_22_s;
            } else if (int_hour == 23) {
                this.mNotification1.icon = R.drawable.time_c_23_s;
            }
            if (int_minute == 0) {
                this.mNotification2.icon = R.drawable.time_c_00_s;
            } else if (int_minute == 1) {
                this.mNotification2.icon = R.drawable.time_c_01_s;
            } else if (int_minute == 2) {
                this.mNotification2.icon = R.drawable.time_c_02_s;
            } else if (int_minute == 3) {
                this.mNotification2.icon = R.drawable.time_c_03_s;
            } else if (int_minute == 4) {
                this.mNotification2.icon = R.drawable.time_c_04_s;
            } else if (int_minute == 5) {
                this.mNotification2.icon = R.drawable.time_c_05_s;
            } else if (int_minute == 6) {
                this.mNotification2.icon = R.drawable.time_c_06_s;
            } else if (int_minute == 7) {
                this.mNotification2.icon = R.drawable.time_c_07_s;
            } else if (int_minute == 8) {
                this.mNotification2.icon = R.drawable.time_c_08_s;
            } else if (int_minute == 9) {
                this.mNotification2.icon = R.drawable.time_c_09_s;
            } else if (int_minute == 10) {
                this.mNotification2.icon = R.drawable.time_c_10_s;
            } else if (int_minute == 11) {
                this.mNotification2.icon = R.drawable.time_c_11_s;
            } else if (int_minute == 12) {
                this.mNotification2.icon = R.drawable.time_c_12_s;
            } else if (int_minute == 13) {
                this.mNotification2.icon = R.drawable.time_c_13_s;
            } else if (int_minute == 14) {
                this.mNotification2.icon = R.drawable.time_c_14_s;
            } else if (int_minute == 15) {
                this.mNotification2.icon = R.drawable.time_c_15_s;
            } else if (int_minute == 16) {
                this.mNotification2.icon = R.drawable.time_c_16_s;
            } else if (int_minute == 17) {
                this.mNotification2.icon = R.drawable.time_c_17_s;
            } else if (int_minute == 18) {
                this.mNotification2.icon = R.drawable.time_c_18_s;
            } else if (int_minute == 19) {
                this.mNotification2.icon = R.drawable.time_c_19_s;
            } else if (int_minute == 20) {
                this.mNotification2.icon = R.drawable.time_c_20_s;
            } else if (int_minute == 21) {
                this.mNotification2.icon = R.drawable.time_c_21_s;
            } else if (int_minute == 22) {
                this.mNotification2.icon = R.drawable.time_c_22_s;
            } else if (int_minute == 23) {
                this.mNotification2.icon = R.drawable.time_c_23_s;
            } else if (int_minute == 24) {
                this.mNotification2.icon = R.drawable.time_c_24_s;
            } else if (int_minute == 25) {
                this.mNotification2.icon = R.drawable.time_c_25_s;
            } else if (int_minute == 26) {
                this.mNotification2.icon = R.drawable.time_c_26_s;
            } else if (int_minute == 27) {
                this.mNotification2.icon = R.drawable.time_c_27_s;
            } else if (int_minute == 28) {
                this.mNotification2.icon = R.drawable.time_c_28_s;
            } else if (int_minute == 29) {
                this.mNotification2.icon = R.drawable.time_c_29_s;
            } else if (int_minute == 30) {
                this.mNotification2.icon = R.drawable.time_c_30_s;
            } else if (int_minute == 31) {
                this.mNotification2.icon = R.drawable.time_c_31_s;
            } else if (int_minute == 32) {
                this.mNotification2.icon = R.drawable.time_c_32_s;
            } else if (int_minute == 33) {
                this.mNotification2.icon = R.drawable.time_c_33_s;
            } else if (int_minute == 34) {
                this.mNotification2.icon = R.drawable.time_c_34_s;
            } else if (int_minute == 35) {
                this.mNotification2.icon = R.drawable.time_c_35_s;
            } else if (int_minute == 36) {
                this.mNotification2.icon = R.drawable.time_c_36_s;
            } else if (int_minute == 37) {
                this.mNotification2.icon = R.drawable.time_c_37_s;
            } else if (int_minute == 38) {
                this.mNotification2.icon = R.drawable.time_c_38_s;
            } else if (int_minute == 39) {
                this.mNotification2.icon = R.drawable.time_c_39_s;
            } else if (int_minute == 40) {
                this.mNotification2.icon = R.drawable.time_c_40_s;
            } else if (int_minute == 41) {
                this.mNotification2.icon = R.drawable.time_c_41_s;
            } else if (int_minute == 42) {
                this.mNotification2.icon = R.drawable.time_c_42_s;
            } else if (int_minute == 43) {
                this.mNotification2.icon = R.drawable.time_c_43_s;
            } else if (int_minute == 44) {
                this.mNotification2.icon = R.drawable.time_c_44_s;
            } else if (int_minute == 45) {
                this.mNotification2.icon = R.drawable.time_c_45_s;
            } else if (int_minute == 46) {
                this.mNotification2.icon = R.drawable.time_c_46_s;
            } else if (int_minute == 47) {
                this.mNotification2.icon = R.drawable.time_c_47_s;
            } else if (int_minute == 48) {
                this.mNotification2.icon = R.drawable.time_c_48_s;
            } else if (int_minute == 49) {
                this.mNotification2.icon = R.drawable.time_c_49_s;
            } else if (int_minute == 50) {
                this.mNotification2.icon = R.drawable.time_c_50_s;
            } else if (int_minute == 51) {
                this.mNotification2.icon = R.drawable.time_c_51_s;
            } else if (int_minute == 52) {
                this.mNotification2.icon = R.drawable.time_c_52_s;
            } else if (int_minute == 53) {
                this.mNotification2.icon = R.drawable.time_c_53_s;
            } else if (int_minute == 54) {
                this.mNotification2.icon = R.drawable.time_c_54_s;
            } else if (int_minute == 55) {
                this.mNotification2.icon = R.drawable.time_c_55_s;
            } else if (int_minute == 56) {
                this.mNotification2.icon = R.drawable.time_c_56_s;
            } else if (int_minute == 57) {
                this.mNotification2.icon = R.drawable.time_c_57_s;
            } else if (int_minute == 58) {
                this.mNotification2.icon = R.drawable.time_c_58_s;
            } else if (int_minute == 59) {
                this.mNotification2.icon = R.drawable.time_c_59_s;
            }
            if (int_second == 0) {
                this.mNotification3.icon = R.drawable.time_00_s;
            } else if (int_second == 1) {
                this.mNotification3.icon = R.drawable.time_01_s;
            } else if (int_second == 2) {
                this.mNotification3.icon = R.drawable.time_02_s;
            } else if (int_second == 3) {
                this.mNotification3.icon = R.drawable.time_03_s;
            } else if (int_second == 4) {
                this.mNotification3.icon = R.drawable.time_04_s;
            } else if (int_second == 5) {
                this.mNotification3.icon = R.drawable.time_05_s;
            } else if (int_second == 6) {
                this.mNotification3.icon = R.drawable.time_06_s;
            } else if (int_second == 7) {
                this.mNotification3.icon = R.drawable.time_07_s;
            } else if (int_second == 8) {
                this.mNotification3.icon = R.drawable.time_08_s;
            } else if (int_second == 9) {
                this.mNotification3.icon = R.drawable.time_09_s;
            } else if (int_second == 10) {
                this.mNotification3.icon = R.drawable.time_10_s;
            } else if (int_second == 11) {
                this.mNotification3.icon = R.drawable.time_11_s;
            } else if (int_second == 12) {
                this.mNotification3.icon = R.drawable.time_12_s;
            } else if (int_second == 13) {
                this.mNotification3.icon = R.drawable.time_13_s;
            } else if (int_second == 14) {
                this.mNotification3.icon = R.drawable.time_14_s;
            } else if (int_second == 15) {
                this.mNotification3.icon = R.drawable.time_15_s;
            } else if (int_second == 16) {
                this.mNotification3.icon = R.drawable.time_16_s;
            } else if (int_second == 17) {
                this.mNotification3.icon = R.drawable.time_17_s;
            } else if (int_second == 18) {
                this.mNotification3.icon = R.drawable.time_18_s;
            } else if (int_second == 19) {
                this.mNotification3.icon = R.drawable.time_19_s;
            } else if (int_second == 20) {
                this.mNotification3.icon = R.drawable.time_20_s;
            } else if (int_second == 21) {
                this.mNotification3.icon = R.drawable.time_21_s;
            } else if (int_second == 22) {
                this.mNotification3.icon = R.drawable.time_22_s;
            } else if (int_second == 23) {
                this.mNotification3.icon = R.drawable.time_23_s;
            } else if (int_second == 24) {
                this.mNotification3.icon = R.drawable.time_24_s;
            } else if (int_second == 25) {
                this.mNotification3.icon = R.drawable.time_25_s;
            } else if (int_second == 26) {
                this.mNotification3.icon = R.drawable.time_26_s;
            } else if (int_second == 27) {
                this.mNotification3.icon = R.drawable.time_27_s;
            } else if (int_second == 28) {
                this.mNotification3.icon = R.drawable.time_28_s;
            } else if (int_second == 29) {
                this.mNotification3.icon = R.drawable.time_29_s;
            } else if (int_second == 30) {
                this.mNotification3.icon = R.drawable.time_30_s;
            } else if (int_second == 31) {
                this.mNotification3.icon = R.drawable.time_31_s;
            } else if (int_second == 32) {
                this.mNotification3.icon = R.drawable.time_32_s;
            } else if (int_second == 33) {
                this.mNotification3.icon = R.drawable.time_33_s;
            } else if (int_second == 34) {
                this.mNotification3.icon = R.drawable.time_34_s;
            } else if (int_second == 35) {
                this.mNotification3.icon = R.drawable.time_35_s;
            } else if (int_second == 36) {
                this.mNotification3.icon = R.drawable.time_36_s;
            } else if (int_second == 37) {
                this.mNotification3.icon = R.drawable.time_37_s;
            } else if (int_second == 38) {
                this.mNotification3.icon = R.drawable.time_38_s;
            } else if (int_second == 39) {
                this.mNotification3.icon = R.drawable.time_39_s;
            } else if (int_second == 40) {
                this.mNotification3.icon = R.drawable.time_40_s;
            } else if (int_second == 41) {
                this.mNotification3.icon = R.drawable.time_41_s;
            } else if (int_second == 42) {
                this.mNotification3.icon = R.drawable.time_42_s;
            } else if (int_second == 43) {
                this.mNotification3.icon = R.drawable.time_43_s;
            } else if (int_second == 44) {
                this.mNotification3.icon = R.drawable.time_44_s;
            } else if (int_second == 45) {
                this.mNotification3.icon = R.drawable.time_45_s;
            } else if (int_second == 46) {
                this.mNotification3.icon = R.drawable.time_46_s;
            } else if (int_second == 47) {
                this.mNotification3.icon = R.drawable.time_47_s;
            } else if (int_second == 48) {
                this.mNotification3.icon = R.drawable.time_48_s;
            } else if (int_second == 49) {
                this.mNotification3.icon = R.drawable.time_49_s;
            } else if (int_second == 50) {
                this.mNotification3.icon = R.drawable.time_50_s;
            } else if (int_second == 51) {
                this.mNotification3.icon = R.drawable.time_51_s;
            } else if (int_second == 52) {
                this.mNotification3.icon = R.drawable.time_52_s;
            } else if (int_second == 53) {
                this.mNotification3.icon = R.drawable.time_53_s;
            } else if (int_second == 54) {
                this.mNotification3.icon = R.drawable.time_54_s;
            } else if (int_second == 55) {
                this.mNotification3.icon = R.drawable.time_55_s;
            } else if (int_second == 56) {
                this.mNotification3.icon = R.drawable.time_56_s;
            } else if (int_second == 57) {
                this.mNotification3.icon = R.drawable.time_57_s;
            } else if (int_second == 58) {
                this.mNotification3.icon = R.drawable.time_58_s;
            } else if (int_second == 59) {
                this.mNotification3.icon = R.drawable.time_59_s;
            }
        } else if (list_preference.equals("pink")) {
            if (int_hour == 0) {
                this.mNotification1.icon = R.drawable.time_c_00_p;
            } else if (int_hour == 1) {
                this.mNotification1.icon = R.drawable.time_c_01_p;
            } else if (int_hour == 2) {
                this.mNotification1.icon = R.drawable.time_c_02_p;
            } else if (int_hour == 3) {
                this.mNotification1.icon = R.drawable.time_c_03_p;
            } else if (int_hour == 4) {
                this.mNotification1.icon = R.drawable.time_c_04_p;
            } else if (int_hour == 5) {
                this.mNotification1.icon = R.drawable.time_c_05_p;
            } else if (int_hour == 6) {
                this.mNotification1.icon = R.drawable.time_c_06_p;
            } else if (int_hour == 7) {
                this.mNotification1.icon = R.drawable.time_c_07_p;
            } else if (int_hour == 8) {
                this.mNotification1.icon = R.drawable.time_c_08_p;
            } else if (int_hour == 9) {
                this.mNotification1.icon = R.drawable.time_c_09_p;
            } else if (int_hour == 10) {
                this.mNotification1.icon = R.drawable.time_c_10_p;
            } else if (int_hour == 11) {
                this.mNotification1.icon = R.drawable.time_c_11_p;
            } else if (int_hour == 12) {
                this.mNotification1.icon = R.drawable.time_c_12_p;
            } else if (int_hour == 13) {
                this.mNotification1.icon = R.drawable.time_c_13_p;
            } else if (int_hour == 14) {
                this.mNotification1.icon = R.drawable.time_c_14_p;
            } else if (int_hour == 15) {
                this.mNotification1.icon = R.drawable.time_c_15_p;
            } else if (int_hour == 16) {
                this.mNotification1.icon = R.drawable.time_c_16_p;
            } else if (int_hour == 17) {
                this.mNotification1.icon = R.drawable.time_c_17_p;
            } else if (int_hour == 18) {
                this.mNotification1.icon = R.drawable.time_c_18_p;
            } else if (int_hour == 19) {
                this.mNotification1.icon = R.drawable.time_c_19_p;
            } else if (int_hour == 20) {
                this.mNotification1.icon = R.drawable.time_c_20_p;
            } else if (int_hour == 21) {
                this.mNotification1.icon = R.drawable.time_c_21_p;
            } else if (int_hour == 22) {
                this.mNotification1.icon = R.drawable.time_c_22_p;
            } else if (int_hour == 23) {
                this.mNotification1.icon = R.drawable.time_c_23_p;
            }
            if (int_minute == 0) {
                this.mNotification2.icon = R.drawable.time_c_00_p;
            } else if (int_minute == 1) {
                this.mNotification2.icon = R.drawable.time_c_01_p;
            } else if (int_minute == 2) {
                this.mNotification2.icon = R.drawable.time_c_02_p;
            } else if (int_minute == 3) {
                this.mNotification2.icon = R.drawable.time_c_03_p;
            } else if (int_minute == 4) {
                this.mNotification2.icon = R.drawable.time_c_04_p;
            } else if (int_minute == 5) {
                this.mNotification2.icon = R.drawable.time_c_05_p;
            } else if (int_minute == 6) {
                this.mNotification2.icon = R.drawable.time_c_06_p;
            } else if (int_minute == 7) {
                this.mNotification2.icon = R.drawable.time_c_07_p;
            } else if (int_minute == 8) {
                this.mNotification2.icon = R.drawable.time_c_08_p;
            } else if (int_minute == 9) {
                this.mNotification2.icon = R.drawable.time_c_09_p;
            } else if (int_minute == 10) {
                this.mNotification2.icon = R.drawable.time_c_10_p;
            } else if (int_minute == 11) {
                this.mNotification2.icon = R.drawable.time_c_11_p;
            } else if (int_minute == 12) {
                this.mNotification2.icon = R.drawable.time_c_12_p;
            } else if (int_minute == 13) {
                this.mNotification2.icon = R.drawable.time_c_13_p;
            } else if (int_minute == 14) {
                this.mNotification2.icon = R.drawable.time_c_14_p;
            } else if (int_minute == 15) {
                this.mNotification2.icon = R.drawable.time_c_15_p;
            } else if (int_minute == 16) {
                this.mNotification2.icon = R.drawable.time_c_16_p;
            } else if (int_minute == 17) {
                this.mNotification2.icon = R.drawable.time_c_17_p;
            } else if (int_minute == 18) {
                this.mNotification2.icon = R.drawable.time_c_18_p;
            } else if (int_minute == 19) {
                this.mNotification2.icon = R.drawable.time_c_19_p;
            } else if (int_minute == 20) {
                this.mNotification2.icon = R.drawable.time_c_20_p;
            } else if (int_minute == 21) {
                this.mNotification2.icon = R.drawable.time_c_21_p;
            } else if (int_minute == 22) {
                this.mNotification2.icon = R.drawable.time_c_22_p;
            } else if (int_minute == 23) {
                this.mNotification2.icon = R.drawable.time_c_23_p;
            } else if (int_minute == 24) {
                this.mNotification2.icon = R.drawable.time_c_24_p;
            } else if (int_minute == 25) {
                this.mNotification2.icon = R.drawable.time_c_25_p;
            } else if (int_minute == 26) {
                this.mNotification2.icon = R.drawable.time_c_26_p;
            } else if (int_minute == 27) {
                this.mNotification2.icon = R.drawable.time_c_27_p;
            } else if (int_minute == 28) {
                this.mNotification2.icon = R.drawable.time_c_28_p;
            } else if (int_minute == 29) {
                this.mNotification2.icon = R.drawable.time_c_29_p;
            } else if (int_minute == 30) {
                this.mNotification2.icon = R.drawable.time_c_30_p;
            } else if (int_minute == 31) {
                this.mNotification2.icon = R.drawable.time_c_31_p;
            } else if (int_minute == 32) {
                this.mNotification2.icon = R.drawable.time_c_32_p;
            } else if (int_minute == 33) {
                this.mNotification2.icon = R.drawable.time_c_33_p;
            } else if (int_minute == 34) {
                this.mNotification2.icon = R.drawable.time_c_34_p;
            } else if (int_minute == 35) {
                this.mNotification2.icon = R.drawable.time_c_35_p;
            } else if (int_minute == 36) {
                this.mNotification2.icon = R.drawable.time_c_36_p;
            } else if (int_minute == 37) {
                this.mNotification2.icon = R.drawable.time_c_37_p;
            } else if (int_minute == 38) {
                this.mNotification2.icon = R.drawable.time_c_38_p;
            } else if (int_minute == 39) {
                this.mNotification2.icon = R.drawable.time_c_39_p;
            } else if (int_minute == 40) {
                this.mNotification2.icon = R.drawable.time_c_40_p;
            } else if (int_minute == 41) {
                this.mNotification2.icon = R.drawable.time_c_41_p;
            } else if (int_minute == 42) {
                this.mNotification2.icon = R.drawable.time_c_42_p;
            } else if (int_minute == 43) {
                this.mNotification2.icon = R.drawable.time_c_43_p;
            } else if (int_minute == 44) {
                this.mNotification2.icon = R.drawable.time_c_44_p;
            } else if (int_minute == 45) {
                this.mNotification2.icon = R.drawable.time_c_45_p;
            } else if (int_minute == 46) {
                this.mNotification2.icon = R.drawable.time_c_46_p;
            } else if (int_minute == 47) {
                this.mNotification2.icon = R.drawable.time_c_47_p;
            } else if (int_minute == 48) {
                this.mNotification2.icon = R.drawable.time_c_48_p;
            } else if (int_minute == 49) {
                this.mNotification2.icon = R.drawable.time_c_49_p;
            } else if (int_minute == 50) {
                this.mNotification2.icon = R.drawable.time_c_50_p;
            } else if (int_minute == 51) {
                this.mNotification2.icon = R.drawable.time_c_51_p;
            } else if (int_minute == 52) {
                this.mNotification2.icon = R.drawable.time_c_52_p;
            } else if (int_minute == 53) {
                this.mNotification2.icon = R.drawable.time_c_53_p;
            } else if (int_minute == 54) {
                this.mNotification2.icon = R.drawable.time_c_54_p;
            } else if (int_minute == 55) {
                this.mNotification2.icon = R.drawable.time_c_55_p;
            } else if (int_minute == 56) {
                this.mNotification2.icon = R.drawable.time_c_56_p;
            } else if (int_minute == 57) {
                this.mNotification2.icon = R.drawable.time_c_57_p;
            } else if (int_minute == 58) {
                this.mNotification2.icon = R.drawable.time_c_58_p;
            } else if (int_minute == 59) {
                this.mNotification2.icon = R.drawable.time_c_59_p;
            }
            if (int_second == 0) {
                this.mNotification3.icon = R.drawable.time_00_p;
            } else if (int_second == 1) {
                this.mNotification3.icon = R.drawable.time_01_p;
            } else if (int_second == 2) {
                this.mNotification3.icon = R.drawable.time_02_p;
            } else if (int_second == 3) {
                this.mNotification3.icon = R.drawable.time_03_p;
            } else if (int_second == 4) {
                this.mNotification3.icon = R.drawable.time_04_p;
            } else if (int_second == 5) {
                this.mNotification3.icon = R.drawable.time_05_p;
            } else if (int_second == 6) {
                this.mNotification3.icon = R.drawable.time_06_p;
            } else if (int_second == 7) {
                this.mNotification3.icon = R.drawable.time_07_p;
            } else if (int_second == 8) {
                this.mNotification3.icon = R.drawable.time_08_p;
            } else if (int_second == 9) {
                this.mNotification3.icon = R.drawable.time_09_p;
            } else if (int_second == 10) {
                this.mNotification3.icon = R.drawable.time_10_p;
            } else if (int_second == 11) {
                this.mNotification3.icon = R.drawable.time_11_p;
            } else if (int_second == 12) {
                this.mNotification3.icon = R.drawable.time_12_p;
            } else if (int_second == 13) {
                this.mNotification3.icon = R.drawable.time_13_p;
            } else if (int_second == 14) {
                this.mNotification3.icon = R.drawable.time_14_p;
            } else if (int_second == 15) {
                this.mNotification3.icon = R.drawable.time_15_p;
            } else if (int_second == 16) {
                this.mNotification3.icon = R.drawable.time_16_p;
            } else if (int_second == 17) {
                this.mNotification3.icon = R.drawable.time_17_p;
            } else if (int_second == 18) {
                this.mNotification3.icon = R.drawable.time_18_p;
            } else if (int_second == 19) {
                this.mNotification3.icon = R.drawable.time_19_p;
            } else if (int_second == 20) {
                this.mNotification3.icon = R.drawable.time_20_p;
            } else if (int_second == 21) {
                this.mNotification3.icon = R.drawable.time_21_p;
            } else if (int_second == 22) {
                this.mNotification3.icon = R.drawable.time_22_p;
            } else if (int_second == 23) {
                this.mNotification3.icon = R.drawable.time_23_p;
            } else if (int_second == 24) {
                this.mNotification3.icon = R.drawable.time_24_p;
            } else if (int_second == 25) {
                this.mNotification3.icon = R.drawable.time_25_p;
            } else if (int_second == 26) {
                this.mNotification3.icon = R.drawable.time_26_p;
            } else if (int_second == 27) {
                this.mNotification3.icon = R.drawable.time_27_p;
            } else if (int_second == 28) {
                this.mNotification3.icon = R.drawable.time_28_p;
            } else if (int_second == 29) {
                this.mNotification3.icon = R.drawable.time_29_p;
            } else if (int_second == 30) {
                this.mNotification3.icon = R.drawable.time_30_p;
            } else if (int_second == 31) {
                this.mNotification3.icon = R.drawable.time_31_p;
            } else if (int_second == 32) {
                this.mNotification3.icon = R.drawable.time_32_p;
            } else if (int_second == 33) {
                this.mNotification3.icon = R.drawable.time_33_p;
            } else if (int_second == 34) {
                this.mNotification3.icon = R.drawable.time_34_p;
            } else if (int_second == 35) {
                this.mNotification3.icon = R.drawable.time_35_p;
            } else if (int_second == 36) {
                this.mNotification3.icon = R.drawable.time_36_p;
            } else if (int_second == 37) {
                this.mNotification3.icon = R.drawable.time_37_p;
            } else if (int_second == 38) {
                this.mNotification3.icon = R.drawable.time_38_p;
            } else if (int_second == 39) {
                this.mNotification3.icon = R.drawable.time_39_p;
            } else if (int_second == 40) {
                this.mNotification3.icon = R.drawable.time_40_p;
            } else if (int_second == 41) {
                this.mNotification3.icon = R.drawable.time_41_p;
            } else if (int_second == 42) {
                this.mNotification3.icon = R.drawable.time_42_p;
            } else if (int_second == 43) {
                this.mNotification3.icon = R.drawable.time_43_p;
            } else if (int_second == 44) {
                this.mNotification3.icon = R.drawable.time_44_p;
            } else if (int_second == 45) {
                this.mNotification3.icon = R.drawable.time_45_p;
            } else if (int_second == 46) {
                this.mNotification3.icon = R.drawable.time_46_p;
            } else if (int_second == 47) {
                this.mNotification3.icon = R.drawable.time_47_p;
            } else if (int_second == 48) {
                this.mNotification3.icon = R.drawable.time_48_p;
            } else if (int_second == 49) {
                this.mNotification3.icon = R.drawable.time_49_p;
            } else if (int_second == 50) {
                this.mNotification3.icon = R.drawable.time_50_p;
            } else if (int_second == 51) {
                this.mNotification3.icon = R.drawable.time_51_p;
            } else if (int_second == 52) {
                this.mNotification3.icon = R.drawable.time_52_p;
            } else if (int_second == 53) {
                this.mNotification3.icon = R.drawable.time_53_p;
            } else if (int_second == 54) {
                this.mNotification3.icon = R.drawable.time_54_p;
            } else if (int_second == 55) {
                this.mNotification3.icon = R.drawable.time_55_p;
            } else if (int_second == 56) {
                this.mNotification3.icon = R.drawable.time_56_p;
            } else if (int_second == 57) {
                this.mNotification3.icon = R.drawable.time_57_p;
            } else if (int_second == 58) {
                this.mNotification3.icon = R.drawable.time_58_p;
            } else if (int_second == 59) {
                this.mNotification3.icon = R.drawable.time_59_p;
            }
        }
        if (int_hour != this.mOld_hour || int_hour == 0) {
            this.mNotificationManager.notify(1, this.mNotification1);
        }
        if (int_minute != this.mOld_minute || int_minute == 0) {
            this.mNotificationManager.notify(2, this.mNotification2);
        }
        this.mNotificationManager.notify(3, this.mNotification3);
        this.mOld_hour = int_hour;
        this.mOld_minute = int_minute;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
