package digit.screen.on.clock;

import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class ScreenOnClockActivity extends PreferenceActivity implements Preference.OnPreferenceChangeListener {
    private EditTextPreference mEP = null;
    private ListPreference mLP = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.main);
        ((CheckBoxPreference) findPreference("on")).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Intent i = new Intent(ScreenOnClockActivity.this, ScreenOnService.class);
                if (((Boolean) newValue).booleanValue()) {
                    ScreenOnClockActivity.this.startService(i);
                    Toast.makeText(ScreenOnClockActivity.this, (int) R.string.toast01, 1).show();
                } else {
                    ScreenOnClockActivity.this.stopService(i);
                    Toast.makeText(ScreenOnClockActivity.this, (int) R.string.toast02, 1).show();
                }
                return true;
            }
        });
        this.mLP = (ListPreference) findPreference("list_key");
        this.mLP.setOnPreferenceChangeListener(this);
        String str1 = (String) this.mLP.getEntry();
        if (str1 != null) {
            this.mLP.setSummary(str1);
        }
        this.mEP = (EditTextPreference) findPreference("edittext_key");
        this.mEP.setOnPreferenceChangeListener(this);
        String str2 = this.mEP.getText();
        if (str2 == null) {
            this.mEP.setText("5");
            return;
        }
        this.mEP.setText(str2);
        this.mEP.setSummary(String.valueOf(str2) + " " + getString(R.string.sec).toString());
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == this.mLP && newValue != null) {
            String str = (String) newValue;
            if (str.equals("kuro")) {
                preference.setSummary((int) R.string.clock1);
            } else if (str.equals("simple")) {
                preference.setSummary((int) R.string.clock2);
            } else if (str.equals("pink")) {
                preference.setSummary((int) R.string.clock3);
            }
            return true;
        } else if (preference != this.mEP || newValue == null) {
            return false;
        } else {
            preference.setSummary(String.valueOf((String) newValue) + " " + getString(R.string.sec).toString());
            return true;
        }
    }
}
