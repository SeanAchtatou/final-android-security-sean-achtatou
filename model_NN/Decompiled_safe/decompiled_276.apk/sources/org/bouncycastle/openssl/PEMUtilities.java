package org.bouncycastle.openssl;

import java.io.IOException;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.generators.OpenSSLPBEParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;

final class PEMUtilities {
    PEMUtilities() {
    }

    static byte[] crypt(boolean z, String str, byte[] bArr, char[] cArr, String str2, byte[] bArr2) throws IOException {
        String str3;
        String str4;
        AlgorithmParameterSpec algorithmParameterSpec;
        String str5;
        String str6;
        String str7;
        String str8;
        byte[] bArr3;
        int i;
        AlgorithmParameterSpec algorithmParameterSpec2;
        SecretKey secretKey;
        AlgorithmParameterSpec ivParameterSpec = new IvParameterSpec(bArr2);
        if (str2.endsWith("-CFB")) {
            str3 = "CFB";
            str4 = "NoPadding";
        } else {
            str3 = "CBC";
            str4 = "PKCS5Padding";
        }
        if (str2.endsWith("-ECB") || "DES-EDE".equals(str2) || "DES-EDE3".equals(str2)) {
            str5 = "ECB";
            algorithmParameterSpec = null;
        } else {
            String str9 = str3;
            algorithmParameterSpec = ivParameterSpec;
            str5 = str9;
        }
        if (str2.endsWith("-OFB")) {
            str6 = "OFB";
            str7 = "NoPadding";
        } else {
            String str10 = str4;
            str6 = str5;
            str7 = str10;
        }
        if (str2.startsWith("DES-EDE")) {
            str8 = "DESede";
            algorithmParameterSpec2 = algorithmParameterSpec;
            secretKey = getKey(cArr, str8, 24, bArr2, !str2.startsWith("DES-EDE3"));
        } else if (str2.startsWith("DES-")) {
            str8 = "DES";
            algorithmParameterSpec2 = algorithmParameterSpec;
            secretKey = getKey(cArr, str8, 8, bArr2);
        } else if (str2.startsWith("BF-")) {
            str8 = "Blowfish";
            algorithmParameterSpec2 = algorithmParameterSpec;
            secretKey = getKey(cArr, str8, 16, bArr2);
        } else if (str2.startsWith("RC2-")) {
            str8 = "RC2";
            int i2 = str2.startsWith("RC2-40-") ? 40 : str2.startsWith("RC2-64-") ? 64 : 128;
            SecretKey key = getKey(cArr, str8, i2 / 8, bArr2);
            algorithmParameterSpec2 = algorithmParameterSpec == null ? new RC2ParameterSpec(i2) : new RC2ParameterSpec(i2, bArr2);
            secretKey = key;
        } else if (str2.startsWith("AES-")) {
            str8 = "AES";
            if (bArr2.length > 8) {
                bArr3 = new byte[8];
                System.arraycopy(bArr2, 0, bArr3, 0, 8);
            } else {
                bArr3 = bArr2;
            }
            if (str2.startsWith("AES-128-")) {
                i = 128;
            } else if (str2.startsWith("AES-192-")) {
                i = 192;
            } else if (str2.startsWith("AES-256-")) {
                i = 256;
            } else {
                throw new EncryptionException("unknown AES encryption with private key");
            }
            SecretKey key2 = getKey(cArr, "AES", i / 8, bArr3);
            algorithmParameterSpec2 = algorithmParameterSpec;
            secretKey = key2;
        } else {
            throw new EncryptionException("unknown encryption with private key");
        }
        try {
            Cipher instance = Cipher.getInstance(str8 + "/" + str6 + "/" + str7, str);
            int i3 = z ? 1 : 2;
            if (algorithmParameterSpec2 == null) {
                instance.init(i3, secretKey);
            } else {
                instance.init(i3, secretKey, algorithmParameterSpec2);
            }
            return instance.doFinal(bArr);
        } catch (Exception e) {
            throw new EncryptionException("exception using cipher - please check password and data.", e);
        }
    }

    private static SecretKey getKey(char[] cArr, String str, int i, byte[] bArr) {
        return getKey(cArr, str, i, bArr, false);
    }

    private static SecretKey getKey(char[] cArr, String str, int i, byte[] bArr, boolean z) {
        OpenSSLPBEParametersGenerator openSSLPBEParametersGenerator = new OpenSSLPBEParametersGenerator();
        openSSLPBEParametersGenerator.init(PBEParametersGenerator.PKCS5PasswordToBytes(cArr), bArr);
        byte[] key = ((KeyParameter) openSSLPBEParametersGenerator.generateDerivedParameters(i * 8)).getKey();
        if (z && key.length >= 24) {
            System.arraycopy(key, 0, key, 16, 8);
        }
        return new SecretKeySpec(key, str);
    }
}
