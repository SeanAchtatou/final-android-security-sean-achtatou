package org.bouncycastle.openssl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CRLException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAParams;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPrivateKey;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.pkcs.RSAPrivateKeyStructure;
import org.bouncycastle.asn1.x509.DSAParameter;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.util.Strings;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.x509.X509AttributeCertificate;
import org.bouncycastle.x509.X509V2AttributeCertificate;

public class PEMWriter extends BufferedWriter {
    private String provider;

    public PEMWriter(Writer writer) {
        this(writer, "BC");
    }

    public PEMWriter(Writer writer, String str) {
        super(writer);
        this.provider = str;
    }

    private void writeEncoded(byte[] bArr) throws IOException {
        char[] cArr = new char[64];
        byte[] encode = Base64.encode(bArr);
        int i = 0;
        while (i < encode.length) {
            int i2 = 0;
            while (i2 != cArr.length && i + i2 < encode.length) {
                cArr[i2] = (char) encode[i + i2];
                i2++;
            }
            write(cArr, 0, i2);
            newLine();
            i += cArr.length;
        }
    }

    private void writeFooter(String str) throws IOException {
        write("-----END " + str + "-----");
        newLine();
    }

    private void writeHeader(String str) throws IOException {
        write("-----BEGIN " + str + "-----");
        newLine();
    }

    private void writeHexEncoded(byte[] bArr) throws IOException {
        byte[] encode = Hex.encode(bArr);
        for (int i = 0; i != encode.length; i++) {
            write((char) encode[i]);
        }
    }

    public void writeObject(Object obj) throws IOException {
        String str;
        byte[] encoded;
        if (obj instanceof X509Certificate) {
            try {
                str = "CERTIFICATE";
                encoded = ((X509Certificate) obj).getEncoded();
            } catch (CertificateEncodingException e) {
                throw new IOException("Cannot encode object: " + e.toString());
            }
        } else if (obj instanceof X509CRL) {
            try {
                str = "X509 CRL";
                encoded = ((X509CRL) obj).getEncoded();
            } catch (CRLException e2) {
                throw new IOException("Cannot encode object: " + e2.toString());
            }
        } else if (obj instanceof KeyPair) {
            writeObject(((KeyPair) obj).getPrivate());
            return;
        } else if (obj instanceof PrivateKey) {
            PrivateKeyInfo privateKeyInfo = new PrivateKeyInfo((ASN1Sequence) ASN1Object.fromByteArray(((Key) obj).getEncoded()));
            if (obj instanceof RSAPrivateKey) {
                byte[] encoded2 = privateKeyInfo.getPrivateKey().getEncoded();
                str = "RSA PRIVATE KEY";
                encoded = encoded2;
            } else if (obj instanceof DSAPrivateKey) {
                DSAParameter instance = DSAParameter.getInstance(privateKeyInfo.getAlgorithmId().getParameters());
                ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
                aSN1EncodableVector.add(new DERInteger(0));
                aSN1EncodableVector.add(new DERInteger(instance.getP()));
                aSN1EncodableVector.add(new DERInteger(instance.getQ()));
                aSN1EncodableVector.add(new DERInteger(instance.getG()));
                BigInteger x = ((DSAPrivateKey) obj).getX();
                aSN1EncodableVector.add(new DERInteger(instance.getG().modPow(x, instance.getP())));
                aSN1EncodableVector.add(new DERInteger(x));
                str = "DSA PRIVATE KEY";
                encoded = new DERSequence(aSN1EncodableVector).getEncoded();
            } else {
                throw new IOException("Cannot identify private key");
            }
        } else if (obj instanceof PublicKey) {
            str = "PUBLIC KEY";
            encoded = ((PublicKey) obj).getEncoded();
        } else if (obj instanceof X509AttributeCertificate) {
            str = "ATTRIBUTE CERTIFICATE";
            encoded = ((X509V2AttributeCertificate) obj).getEncoded();
        } else if (obj instanceof PKCS10CertificationRequest) {
            str = "CERTIFICATE REQUEST";
            encoded = ((PKCS10CertificationRequest) obj).getEncoded();
        } else if (obj instanceof ContentInfo) {
            str = "PKCS7";
            encoded = ((ContentInfo) obj).getEncoded();
        } else {
            throw new IOException("unknown object passed - can't encode.");
        }
        writeHeader(str);
        writeEncoded(encoded);
        writeFooter(str);
    }

    public void writeObject(Object obj, String str, char[] cArr, SecureRandom secureRandom) throws IOException {
        byte[] bArr;
        String str2;
        if (obj instanceof KeyPair) {
            writeObject(((KeyPair) obj).getPrivate());
            return;
        }
        if (obj instanceof RSAPrivateCrtKey) {
            RSAPrivateCrtKey rSAPrivateCrtKey = (RSAPrivateCrtKey) obj;
            bArr = new RSAPrivateKeyStructure(rSAPrivateCrtKey.getModulus(), rSAPrivateCrtKey.getPublicExponent(), rSAPrivateCrtKey.getPrivateExponent(), rSAPrivateCrtKey.getPrimeP(), rSAPrivateCrtKey.getPrimeQ(), rSAPrivateCrtKey.getPrimeExponentP(), rSAPrivateCrtKey.getPrimeExponentQ(), rSAPrivateCrtKey.getCrtCoefficient()).getEncoded();
            str2 = "RSA PRIVATE KEY";
        } else if (obj instanceof DSAPrivateKey) {
            DSAPrivateKey dSAPrivateKey = (DSAPrivateKey) obj;
            DSAParams params = dSAPrivateKey.getParams();
            ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
            aSN1EncodableVector.add(new DERInteger(0));
            aSN1EncodableVector.add(new DERInteger(params.getP()));
            aSN1EncodableVector.add(new DERInteger(params.getQ()));
            aSN1EncodableVector.add(new DERInteger(params.getG()));
            BigInteger x = dSAPrivateKey.getX();
            aSN1EncodableVector.add(new DERInteger(params.getG().modPow(x, params.getP())));
            aSN1EncodableVector.add(new DERInteger(x));
            bArr = new DERSequence(aSN1EncodableVector).getEncoded();
            str2 = "DSA PRIVATE KEY";
        } else {
            bArr = null;
            str2 = null;
        }
        if (str2 == null || bArr == null) {
            throw new IllegalArgumentException("Object type not supported: " + obj.getClass().getName());
        }
        String upperCase = Strings.toUpperCase(str);
        String str3 = upperCase.equals("DESEDE") ? "DES-EDE3-CBC" : upperCase;
        byte[] bArr2 = new byte[(str3.startsWith("AES-") ? 16 : 8)];
        secureRandom.nextBytes(bArr2);
        byte[] crypt = PEMUtilities.crypt(true, this.provider, bArr, cArr, str3, bArr2);
        writeHeader(str2);
        write("Proc-Type: 4,ENCRYPTED");
        newLine();
        write("DEK-Info: " + str3 + ",");
        writeHexEncoded(bArr2);
        newLine();
        newLine();
        writeEncoded(crypt);
        writeFooter(str2);
    }
}
