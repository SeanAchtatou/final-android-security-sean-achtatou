package org.bouncycastle.openssl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.security.spec.DSAPrivateKeySpec;
import java.security.spec.DSAPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.StringTokenizer;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.sec.ECPrivateKeyStructure;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.RSAPublicKeyStructure;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.x509.X509AttributeCertificate;
import org.bouncycastle.x509.X509V2AttributeCertificate;

public class PEMReader extends BufferedReader {
    private final PasswordFinder pFinder;
    private final String provider;

    public PEMReader(Reader reader) {
        this(reader, null, "BC");
    }

    public PEMReader(Reader reader, PasswordFinder passwordFinder) {
        this(reader, passwordFinder, "BC");
    }

    public PEMReader(Reader reader, PasswordFinder passwordFinder, String str) {
        super(reader);
        this.pFinder = passwordFinder;
        this.provider = str;
    }

    private X509AttributeCertificate readAttributeCertificate(String str) throws IOException {
        return new X509V2AttributeCertificate(readBytes(str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] readBytes(java.lang.String r5) throws java.io.IOException {
        /*
            r4 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
        L_0x0005:
            java.lang.String r1 = r4.readLine()
            if (r1 == 0) goto L_0x0012
            int r2 = r1.indexOf(r5)
            r3 = -1
            if (r2 == r3) goto L_0x002d
        L_0x0012:
            if (r1 != 0) goto L_0x0035
            java.io.IOException r0 = new java.io.IOException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = " not found"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x002d:
            java.lang.String r1 = r1.trim()
            r0.append(r1)
            goto L_0x0005
        L_0x0035:
            java.lang.String r0 = r0.toString()
            byte[] r0 = org.bouncycastle.util.encoders.Base64.decode(r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.openssl.PEMReader.readBytes(java.lang.String):byte[]");
    }

    private X509CRL readCRL(String str) throws IOException {
        try {
            return (X509CRL) CertificateFactory.getInstance("X.509", this.provider).generateCRL(new ByteArrayInputStream(readBytes(str)));
        } catch (Exception e) {
            throw new IOException("problem parsing cert: " + e.toString());
        }
    }

    private X509Certificate readCertificate(String str) throws IOException {
        try {
            return (X509Certificate) CertificateFactory.getInstance("X.509", this.provider).generateCertificate(new ByteArrayInputStream(readBytes(str)));
        } catch (Exception e) {
            throw new IOException("problem parsing cert: " + e.toString());
        }
    }

    private PKCS10CertificationRequest readCertificateRequest(String str) throws IOException {
        try {
            return new PKCS10CertificationRequest(readBytes(str));
        } catch (Exception e) {
            throw new IOException("problem parsing cert: " + e.toString());
        }
    }

    private ECNamedCurveParameterSpec readECParameters(String str) throws IOException {
        return ECNamedCurveTable.getParameterSpec(((DERObjectIdentifier) ASN1Object.fromByteArray(readBytes(str))).getId());
    }

    private KeyPair readECPrivateKey(String str) throws IOException {
        try {
            ECPrivateKeyStructure eCPrivateKeyStructure = new ECPrivateKeyStructure((ASN1Sequence) ASN1Object.fromByteArray(readBytes(str)));
            AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, eCPrivateKeyStructure.getParameters());
            PrivateKeyInfo privateKeyInfo = new PrivateKeyInfo(algorithmIdentifier, eCPrivateKeyStructure.getDERObject());
            SubjectPublicKeyInfo subjectPublicKeyInfo = new SubjectPublicKeyInfo(algorithmIdentifier, eCPrivateKeyStructure.getPublicKey().getBytes());
            PKCS8EncodedKeySpec pKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKeyInfo.getEncoded());
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(subjectPublicKeyInfo.getEncoded());
            KeyFactory instance = KeyFactory.getInstance("ECDSA", this.provider);
            return new KeyPair(instance.generatePublic(x509EncodedKeySpec), instance.generatePrivate(pKCS8EncodedKeySpec));
        } catch (ClassCastException e) {
            throw new IOException("wrong ASN.1 object found in stream");
        } catch (Exception e2) {
            throw new IOException("problem parsing EC private key: " + e2);
        }
    }

    private KeyPair readKeyPair(String str, String str2) throws Exception {
        byte[] bArr;
        KeySpec dSAPublicKeySpec;
        KeySpec keySpec;
        String str3 = null;
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = false;
        while (true) {
            String readLine = readLine();
            if (readLine == null) {
                break;
            } else if (readLine.startsWith("Proc-Type: 4,ENCRYPTED")) {
                z = true;
            } else if (readLine.startsWith("DEK-Info:")) {
                str3 = readLine.substring(10);
            } else if (readLine.indexOf(str2) != -1) {
                break;
            } else {
                stringBuffer.append(readLine.trim());
            }
        }
        byte[] decode = Base64.decode(stringBuffer.toString());
        if (!z) {
            bArr = decode;
        } else if (this.pFinder == null) {
            throw new PasswordException("No password finder specified, but a password is required");
        } else {
            char[] password = this.pFinder.getPassword();
            if (password == null) {
                throw new PasswordException("Password is null, but a password is required");
            }
            StringTokenizer stringTokenizer = new StringTokenizer(str3, ",");
            bArr = PEMUtilities.crypt(false, this.provider, decode, password, stringTokenizer.nextToken(), Hex.decode(stringTokenizer.nextToken()));
        }
        ASN1Sequence aSN1Sequence = (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(bArr)).readObject();
        if (str.equals("RSA")) {
            DERInteger dERInteger = (DERInteger) aSN1Sequence.getObjectAt(1);
            DERInteger dERInteger2 = (DERInteger) aSN1Sequence.getObjectAt(2);
            KeySpec rSAPublicKeySpec = new RSAPublicKeySpec(dERInteger.getValue(), dERInteger2.getValue());
            keySpec = new RSAPrivateCrtKeySpec(dERInteger.getValue(), dERInteger2.getValue(), ((DERInteger) aSN1Sequence.getObjectAt(3)).getValue(), ((DERInteger) aSN1Sequence.getObjectAt(4)).getValue(), ((DERInteger) aSN1Sequence.getObjectAt(5)).getValue(), ((DERInteger) aSN1Sequence.getObjectAt(6)).getValue(), ((DERInteger) aSN1Sequence.getObjectAt(7)).getValue(), ((DERInteger) aSN1Sequence.getObjectAt(8)).getValue());
            dSAPublicKeySpec = rSAPublicKeySpec;
        } else {
            DERInteger dERInteger3 = (DERInteger) aSN1Sequence.getObjectAt(1);
            DERInteger dERInteger4 = (DERInteger) aSN1Sequence.getObjectAt(2);
            DERInteger dERInteger5 = (DERInteger) aSN1Sequence.getObjectAt(3);
            DSAPrivateKeySpec dSAPrivateKeySpec = new DSAPrivateKeySpec(((DERInteger) aSN1Sequence.getObjectAt(5)).getValue(), dERInteger3.getValue(), dERInteger4.getValue(), dERInteger5.getValue());
            dSAPublicKeySpec = new DSAPublicKeySpec(((DERInteger) aSN1Sequence.getObjectAt(4)).getValue(), dERInteger3.getValue(), dERInteger4.getValue(), dERInteger5.getValue());
            keySpec = dSAPrivateKeySpec;
        }
        KeyFactory instance = KeyFactory.getInstance(str, this.provider);
        return new KeyPair(instance.generatePublic(dSAPublicKeySpec), instance.generatePrivate(keySpec));
    }

    private ContentInfo readPKCS7(String str) throws IOException {
        String readLine;
        StringBuffer stringBuffer = new StringBuffer();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            readLine = readLine();
            if (readLine != null && readLine.indexOf(str) == -1) {
                stringBuffer.append(readLine.trim().trim());
                Base64.decode(stringBuffer.substring(0, (stringBuffer.length() / 4) * 4), byteArrayOutputStream);
                stringBuffer.delete(0, (stringBuffer.length() / 4) * 4);
            }
        }
        if (stringBuffer.length() != 0) {
            throw new IOException("base64 data appears to be truncated");
        } else if (readLine == null) {
            throw new IOException(str + " not found");
        } else {
            try {
                return ContentInfo.getInstance(new ASN1InputStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray())).readObject());
            } catch (Exception e) {
                throw new IOException("problem parsing PKCS7 object: " + e.toString());
            }
        }
    }

    private PublicKey readPublicKey(String str) throws IOException {
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(readBytes(str));
        String[] strArr = {"DSA", "RSA"};
        int i = 0;
        while (i < strArr.length) {
            try {
                return KeyFactory.getInstance(strArr[i], this.provider).generatePublic(x509EncodedKeySpec);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                i++;
            } catch (NoSuchProviderException e2) {
                throw new RuntimeException("can't find provider " + this.provider);
            }
        }
        return null;
    }

    private PublicKey readRSAPublicKey(String str) throws IOException {
        RSAPublicKeyStructure rSAPublicKeyStructure = new RSAPublicKeyStructure((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(readBytes(str))).readObject());
        try {
            return KeyFactory.getInstance("RSA", this.provider).generatePublic(new RSAPublicKeySpec(rSAPublicKeyStructure.getModulus(), rSAPublicKeyStructure.getPublicExponent()));
        } catch (NoSuchProviderException e) {
            throw new IOException("can't find provider " + this.provider);
        } catch (Exception e2) {
            throw new IOException("problem extracting key: " + e2.toString());
        }
    }

    public Object readObject() throws IOException {
        String readLine;
        do {
            readLine = readLine();
            if (readLine == null) {
                return null;
            }
            if (readLine.indexOf("-----BEGIN PUBLIC KEY") != -1) {
                return readPublicKey("-----END PUBLIC KEY");
            }
            if (readLine.indexOf("-----BEGIN RSA PUBLIC KEY") != -1) {
                return readRSAPublicKey("-----END RSA PUBLIC KEY");
            }
            if (readLine.indexOf("-----BEGIN CERTIFICATE REQUEST") != -1) {
                return readCertificateRequest("-----END CERTIFICATE REQUEST");
            }
            if (readLine.indexOf("-----BEGIN NEW CERTIFICATE REQUEST") != -1) {
                return readCertificateRequest("-----END NEW CERTIFICATE REQUEST");
            }
            if (readLine.indexOf("-----BEGIN CERTIFICATE") != -1) {
                return readCertificate("-----END CERTIFICATE");
            }
            if (readLine.indexOf("-----BEGIN PKCS7") != -1) {
                return readPKCS7("-----END PKCS7");
            }
            if (readLine.indexOf("-----BEGIN X509 CERTIFICATE") != -1) {
                return readCertificate("-----END X509 CERTIFICATE");
            }
            if (readLine.indexOf("-----BEGIN X509 CRL") != -1) {
                return readCRL("-----END X509 CRL");
            }
            if (readLine.indexOf("-----BEGIN ATTRIBUTE CERTIFICATE") != -1) {
                return readAttributeCertificate("-----END ATTRIBUTE CERTIFICATE");
            }
            if (readLine.indexOf("-----BEGIN RSA PRIVATE KEY") != -1) {
                try {
                    return readKeyPair("RSA", "-----END RSA PRIVATE KEY");
                } catch (Exception e) {
                    throw new IOException("problem creating RSA private key: " + e.toString());
                }
            } else if (readLine.indexOf("-----BEGIN DSA PRIVATE KEY") != -1) {
                try {
                    return readKeyPair("DSA", "-----END DSA PRIVATE KEY");
                } catch (Exception e2) {
                    throw new IOException("problem creating DSA private key: " + e2.toString());
                }
            } else if (readLine.indexOf("-----BEGIN EC PARAMETERS-----") != -1) {
                return readECParameters("-----END EC PARAMETERS-----");
            }
        } while (readLine.indexOf("-----BEGIN EC PRIVATE KEY-----") == -1);
        return readECPrivateKey("-----END EC PRIVATE KEY-----");
    }
}
