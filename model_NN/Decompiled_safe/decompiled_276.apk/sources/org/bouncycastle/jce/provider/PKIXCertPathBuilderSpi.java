package org.bouncycastle.jce.provider;

import java.security.InvalidAlgorithmParameterException;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertPathBuilderSpi;
import java.security.cert.CertPathParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.bouncycastle.jce.exception.ExtCertPathBuilderException;
import org.bouncycastle.util.Selector;
import org.bouncycastle.x509.ExtendedPKIXBuilderParameters;
import org.bouncycastle.x509.X509CertStoreSelector;

public class PKIXCertPathBuilderSpi extends CertPathBuilderSpi {
    private Exception certPathException;

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:74:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.security.cert.CertPathBuilderResult build(java.security.cert.X509Certificate r7, org.bouncycastle.x509.ExtendedPKIXBuilderParameters r8, java.util.List r9) {
        /*
            r6 = this;
            r5 = 0
            boolean r0 = r9.contains(r7)
            if (r0 == 0) goto L_0x0009
            r0 = r5
        L_0x0008:
            return r0
        L_0x0009:
            java.util.Set r0 = r8.getExcludedCerts()
            boolean r0 = r0.contains(r7)
            if (r0 == 0) goto L_0x0015
            r0 = r5
            goto L_0x0008
        L_0x0015:
            int r0 = r8.getMaxPathLength()
            r1 = -1
            if (r0 == r1) goto L_0x002a
            int r0 = r9.size()
            r1 = 1
            int r0 = r0 - r1
            int r1 = r8.getMaxPathLength()
            if (r0 <= r1) goto L_0x002a
            r0 = r5
            goto L_0x0008
        L_0x002a:
            r9.add(r7)
            java.lang.String r0 = "X.509"
            java.lang.String r1 = "BC"
            java.security.cert.CertificateFactory r0 = java.security.cert.CertificateFactory.getInstance(r0, r1)     // Catch:{ Exception -> 0x0068 }
            java.lang.String r1 = "PKIX"
            java.lang.String r2 = "BC"
            java.security.cert.CertPathValidator r1 = java.security.cert.CertPathValidator.getInstance(r1, r2)     // Catch:{ Exception -> 0x0068 }
            java.util.Set r2 = r8.getTrustAnchors()     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r3 = r8.getSigProvider()     // Catch:{ AnnotatedException -> 0x007a }
            java.security.cert.TrustAnchor r2 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.findTrustAnchor(r7, r2, r3)     // Catch:{ AnnotatedException -> 0x007a }
            if (r2 == 0) goto L_0x008e
            java.security.cert.CertPath r2 = r0.generateCertPath(r9)     // Catch:{ Exception -> 0x0071 }
            java.security.cert.CertPathValidatorResult r0 = r1.validate(r2, r8)     // Catch:{ Exception -> 0x0085 }
            java.security.cert.PKIXCertPathValidatorResult r0 = (java.security.cert.PKIXCertPathValidatorResult) r0     // Catch:{ Exception -> 0x0085 }
            java.security.cert.PKIXCertPathBuilderResult r1 = new java.security.cert.PKIXCertPathBuilderResult     // Catch:{ AnnotatedException -> 0x007a }
            java.security.cert.TrustAnchor r3 = r0.getTrustAnchor()     // Catch:{ AnnotatedException -> 0x007a }
            java.security.cert.PolicyNode r4 = r0.getPolicyTree()     // Catch:{ AnnotatedException -> 0x007a }
            java.security.PublicKey r0 = r0.getPublicKey()     // Catch:{ AnnotatedException -> 0x007a }
            r1.<init>(r2, r3, r4, r0)     // Catch:{ AnnotatedException -> 0x007a }
            r0 = r1
            goto L_0x0008
        L_0x0068:
            r0 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Exception creating support classes."
            r0.<init>(r1)
            throw r0
        L_0x0071:
            r0 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r1 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r2 = "Certification path could not be constructed from certificate list."
            r1.<init>(r2, r0)     // Catch:{ AnnotatedException -> 0x007a }
            throw r1     // Catch:{ AnnotatedException -> 0x007a }
        L_0x007a:
            r0 = move-exception
            r1 = r5
        L_0x007c:
            r6.certPathException = r0
            r0 = r1
        L_0x007f:
            if (r0 != 0) goto L_0x0008
            r9.remove(r7)
            goto L_0x0008
        L_0x0085:
            r0 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r1 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r2 = "Certification path could not be validated."
            r1.<init>(r2, r0)     // Catch:{ AnnotatedException -> 0x007a }
            throw r1     // Catch:{ AnnotatedException -> 0x007a }
        L_0x008e:
            org.bouncycastle.jce.provider.CertPathValidatorUtilities.addAdditionalStoresFromAltNames(r7, r8)     // Catch:{ CertificateParsingException -> 0x00ab }
            java.util.HashSet r0 = new java.util.HashSet     // Catch:{ AnnotatedException -> 0x007a }
            r0.<init>()     // Catch:{ AnnotatedException -> 0x007a }
            java.util.Collection r1 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.findIssuerCerts(r7, r8)     // Catch:{ AnnotatedException -> 0x00b4 }
            r0.addAll(r1)     // Catch:{ AnnotatedException -> 0x00b4 }
            boolean r1 = r0.isEmpty()     // Catch:{ AnnotatedException -> 0x007a }
            if (r1 == 0) goto L_0x00bd
            org.bouncycastle.jce.provider.AnnotatedException r0 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r1 = "No issuer certificate for certificate in certification path found."
            r0.<init>(r1)     // Catch:{ AnnotatedException -> 0x007a }
            throw r0     // Catch:{ AnnotatedException -> 0x007a }
        L_0x00ab:
            r0 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r1 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r2 = "No additiontal X.509 stores can be added from certificate locations."
            r1.<init>(r2, r0)     // Catch:{ AnnotatedException -> 0x007a }
            throw r1     // Catch:{ AnnotatedException -> 0x007a }
        L_0x00b4:
            r0 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r1 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r2 = "Cannot find issuer certificate for certificate in certification path."
            r1.<init>(r2, r0)     // Catch:{ AnnotatedException -> 0x007a }
            throw r1     // Catch:{ AnnotatedException -> 0x007a }
        L_0x00bd:
            java.util.Iterator r1 = r0.iterator()     // Catch:{ AnnotatedException -> 0x007a }
            r2 = r5
        L_0x00c2:
            boolean r0 = r1.hasNext()     // Catch:{ AnnotatedException -> 0x00d8 }
            if (r0 == 0) goto L_0x00d6
            if (r2 != 0) goto L_0x00d6
            java.lang.Object r0 = r1.next()     // Catch:{ AnnotatedException -> 0x00d8 }
            java.security.cert.X509Certificate r0 = (java.security.cert.X509Certificate) r0     // Catch:{ AnnotatedException -> 0x00d8 }
            java.security.cert.CertPathBuilderResult r0 = r6.build(r0, r8, r9)     // Catch:{ AnnotatedException -> 0x00d8 }
            r2 = r0
            goto L_0x00c2
        L_0x00d6:
            r0 = r2
            goto L_0x007f
        L_0x00d8:
            r0 = move-exception
            r1 = r2
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.jce.provider.PKIXCertPathBuilderSpi.build(java.security.cert.X509Certificate, org.bouncycastle.x509.ExtendedPKIXBuilderParameters, java.util.List):java.security.cert.CertPathBuilderResult");
    }

    public CertPathBuilderResult engineBuild(CertPathParameters certPathParameters) throws CertPathBuilderException, InvalidAlgorithmParameterException {
        if ((certPathParameters instanceof PKIXBuilderParameters) || (certPathParameters instanceof ExtendedPKIXBuilderParameters)) {
            ExtendedPKIXBuilderParameters extendedPKIXBuilderParameters = certPathParameters instanceof ExtendedPKIXBuilderParameters ? (ExtendedPKIXBuilderParameters) certPathParameters : (ExtendedPKIXBuilderParameters) ExtendedPKIXBuilderParameters.getInstance((PKIXBuilderParameters) certPathParameters);
            ArrayList arrayList = new ArrayList();
            Selector targetConstraints = extendedPKIXBuilderParameters.getTargetConstraints();
            if (!(targetConstraints instanceof X509CertStoreSelector)) {
                throw new CertPathBuilderException("TargetConstraints must be an instance of " + X509CertStoreSelector.class.getName() + " for " + getClass().getName() + " class.");
            }
            try {
                Collection findCertificates = CertPathValidatorUtilities.findCertificates((X509CertStoreSelector) targetConstraints, extendedPKIXBuilderParameters.getStores());
                findCertificates.addAll(CertPathValidatorUtilities.findCertificates((X509CertStoreSelector) targetConstraints, extendedPKIXBuilderParameters.getCertStores()));
                if (findCertificates.isEmpty()) {
                    throw new CertPathBuilderException("No certificate found matching targetContraints.");
                }
                Iterator it = findCertificates.iterator();
                CertPathBuilderResult certPathBuilderResult = null;
                while (it.hasNext() && certPathBuilderResult == null) {
                    certPathBuilderResult = build((X509Certificate) it.next(), extendedPKIXBuilderParameters, arrayList);
                }
                if (certPathBuilderResult != null || this.certPathException == null) {
                    if (certPathBuilderResult != null || this.certPathException != null) {
                        return certPathBuilderResult;
                    }
                    throw new CertPathBuilderException("Unable to find certificate chain.");
                } else if (this.certPathException instanceof AnnotatedException) {
                    throw new CertPathBuilderException(this.certPathException.getMessage(), this.certPathException.getCause());
                } else {
                    throw new CertPathBuilderException("Possible certificate chain could not be validated.", this.certPathException);
                }
            } catch (AnnotatedException e) {
                throw new ExtCertPathBuilderException("Error finding target certificate.", e);
            }
        } else {
            throw new InvalidAlgorithmParameterException("Parameters must be an instance of " + PKIXBuilderParameters.class.getName() + " or " + ExtendedPKIXBuilderParameters.class.getName() + ".");
        }
    }
}
