package org.bouncycastle.jce.provider;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.Principal;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertPathBuilderSpi;
import java.security.cert.CertPathParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.jce.exception.ExtCertPathBuilderException;
import org.bouncycastle.util.Selector;
import org.bouncycastle.x509.ExtendedPKIXBuilderParameters;
import org.bouncycastle.x509.X509AttributeCertStoreSelector;
import org.bouncycastle.x509.X509AttributeCertificate;
import org.bouncycastle.x509.X509CertStoreSelector;

public class PKIXAttrCertPathBuilderSpi extends CertPathBuilderSpi {
    private Exception certPathException;

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:79:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.security.cert.CertPathBuilderResult build(org.bouncycastle.x509.X509AttributeCertificate r7, java.security.cert.X509Certificate r8, org.bouncycastle.x509.ExtendedPKIXBuilderParameters r9, java.util.List r10) {
        /*
            r6 = this;
            r5 = 0
            boolean r0 = r10.contains(r8)
            if (r0 == 0) goto L_0x0009
            r0 = r5
        L_0x0008:
            return r0
        L_0x0009:
            java.util.Set r0 = r9.getExcludedCerts()
            boolean r0 = r0.contains(r8)
            if (r0 == 0) goto L_0x0015
            r0 = r5
            goto L_0x0008
        L_0x0015:
            int r0 = r9.getMaxPathLength()
            r1 = -1
            if (r0 == r1) goto L_0x002a
            int r0 = r10.size()
            r1 = 1
            int r0 = r0 - r1
            int r1 = r9.getMaxPathLength()
            if (r0 <= r1) goto L_0x002a
            r0 = r5
            goto L_0x0008
        L_0x002a:
            r10.add(r8)
            java.lang.String r0 = "X.509"
            java.lang.String r1 = "BC"
            java.security.cert.CertificateFactory r0 = java.security.cert.CertificateFactory.getInstance(r0, r1)     // Catch:{ Exception -> 0x0068 }
            java.lang.String r1 = "RFC3281"
            java.lang.String r2 = "BC"
            java.security.cert.CertPathValidator r1 = java.security.cert.CertPathValidator.getInstance(r1, r2)     // Catch:{ Exception -> 0x0068 }
            java.util.Set r2 = r9.getTrustAnchors()     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r3 = r9.getSigProvider()     // Catch:{ AnnotatedException -> 0x007a }
            java.security.cert.TrustAnchor r2 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.findTrustAnchor(r8, r2, r3)     // Catch:{ AnnotatedException -> 0x007a }
            if (r2 == 0) goto L_0x0096
            java.security.cert.CertPath r2 = r0.generateCertPath(r10)     // Catch:{ Exception -> 0x0071 }
            java.security.cert.CertPathValidatorResult r0 = r1.validate(r2, r9)     // Catch:{ Exception -> 0x008d }
            java.security.cert.PKIXCertPathValidatorResult r0 = (java.security.cert.PKIXCertPathValidatorResult) r0     // Catch:{ Exception -> 0x008d }
            java.security.cert.PKIXCertPathBuilderResult r1 = new java.security.cert.PKIXCertPathBuilderResult     // Catch:{ AnnotatedException -> 0x007a }
            java.security.cert.TrustAnchor r3 = r0.getTrustAnchor()     // Catch:{ AnnotatedException -> 0x007a }
            java.security.cert.PolicyNode r4 = r0.getPolicyTree()     // Catch:{ AnnotatedException -> 0x007a }
            java.security.PublicKey r0 = r0.getPublicKey()     // Catch:{ AnnotatedException -> 0x007a }
            r1.<init>(r2, r3, r4, r0)     // Catch:{ AnnotatedException -> 0x007a }
            r0 = r1
            goto L_0x0008
        L_0x0068:
            r0 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Exception creating support classes."
            r0.<init>(r1)
            throw r0
        L_0x0071:
            r0 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r1 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r2 = "Certification path could not be constructed from certificate list."
            r1.<init>(r2, r0)     // Catch:{ AnnotatedException -> 0x007a }
            throw r1     // Catch:{ AnnotatedException -> 0x007a }
        L_0x007a:
            r0 = move-exception
            r1 = r5
        L_0x007c:
            org.bouncycastle.jce.provider.AnnotatedException r2 = new org.bouncycastle.jce.provider.AnnotatedException
            java.lang.String r3 = "No valid certification path could be build."
            r2.<init>(r3, r0)
            r6.certPathException = r2
            r0 = r1
        L_0x0086:
            if (r0 != 0) goto L_0x0008
            r10.remove(r8)
            goto L_0x0008
        L_0x008d:
            r0 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r1 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r2 = "Certification path could not be validated."
            r1.<init>(r2, r0)     // Catch:{ AnnotatedException -> 0x007a }
            throw r1     // Catch:{ AnnotatedException -> 0x007a }
        L_0x0096:
            org.bouncycastle.jce.provider.CertPathValidatorUtilities.addAdditionalStoresFromAltNames(r8, r9)     // Catch:{ CertificateParsingException -> 0x00b3 }
            java.util.HashSet r0 = new java.util.HashSet     // Catch:{ AnnotatedException -> 0x007a }
            r0.<init>()     // Catch:{ AnnotatedException -> 0x007a }
            java.util.Collection r1 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.findIssuerCerts(r8, r9)     // Catch:{ AnnotatedException -> 0x00bc }
            r0.addAll(r1)     // Catch:{ AnnotatedException -> 0x00bc }
            boolean r1 = r0.isEmpty()     // Catch:{ AnnotatedException -> 0x007a }
            if (r1 == 0) goto L_0x00c5
            org.bouncycastle.jce.provider.AnnotatedException r0 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r1 = "No issuer certificate for certificate in certification path found."
            r0.<init>(r1)     // Catch:{ AnnotatedException -> 0x007a }
            throw r0     // Catch:{ AnnotatedException -> 0x007a }
        L_0x00b3:
            r0 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r1 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r2 = "No additional X.509 stores can be added from certificate locations."
            r1.<init>(r2, r0)     // Catch:{ AnnotatedException -> 0x007a }
            throw r1     // Catch:{ AnnotatedException -> 0x007a }
        L_0x00bc:
            r0 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r1 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x007a }
            java.lang.String r2 = "Cannot find issuer certificate for certificate in certification path."
            r1.<init>(r2, r0)     // Catch:{ AnnotatedException -> 0x007a }
            throw r1     // Catch:{ AnnotatedException -> 0x007a }
        L_0x00c5:
            java.util.Iterator r1 = r0.iterator()     // Catch:{ AnnotatedException -> 0x007a }
            r2 = r5
        L_0x00ca:
            boolean r0 = r1.hasNext()     // Catch:{ AnnotatedException -> 0x00ee }
            if (r0 == 0) goto L_0x00ec
            if (r2 != 0) goto L_0x00ec
            java.lang.Object r0 = r1.next()     // Catch:{ AnnotatedException -> 0x00ee }
            java.security.cert.X509Certificate r0 = (java.security.cert.X509Certificate) r0     // Catch:{ AnnotatedException -> 0x00ee }
            javax.security.auth.x500.X500Principal r3 = r0.getIssuerX500Principal()     // Catch:{ AnnotatedException -> 0x00ee }
            javax.security.auth.x500.X500Principal r4 = r0.getSubjectX500Principal()     // Catch:{ AnnotatedException -> 0x00ee }
            boolean r3 = r3.equals(r4)     // Catch:{ AnnotatedException -> 0x00ee }
            if (r3 != 0) goto L_0x00ca
            java.security.cert.CertPathBuilderResult r0 = r6.build(r7, r0, r9, r10)     // Catch:{ AnnotatedException -> 0x00ee }
            r2 = r0
            goto L_0x00ca
        L_0x00ec:
            r0 = r2
            goto L_0x0086
        L_0x00ee:
            r0 = move-exception
            r1 = r2
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.jce.provider.PKIXAttrCertPathBuilderSpi.build(org.bouncycastle.x509.X509AttributeCertificate, java.security.cert.X509Certificate, org.bouncycastle.x509.ExtendedPKIXBuilderParameters, java.util.List):java.security.cert.CertPathBuilderResult");
    }

    public CertPathBuilderResult engineBuild(CertPathParameters certPathParameters) throws CertPathBuilderException, InvalidAlgorithmParameterException {
        if ((certPathParameters instanceof PKIXBuilderParameters) || (certPathParameters instanceof ExtendedPKIXBuilderParameters)) {
            ExtendedPKIXBuilderParameters extendedPKIXBuilderParameters = certPathParameters instanceof ExtendedPKIXBuilderParameters ? (ExtendedPKIXBuilderParameters) certPathParameters : (ExtendedPKIXBuilderParameters) ExtendedPKIXBuilderParameters.getInstance((PKIXBuilderParameters) certPathParameters);
            ArrayList arrayList = new ArrayList();
            Selector targetConstraints = extendedPKIXBuilderParameters.getTargetConstraints();
            if (!(targetConstraints instanceof X509AttributeCertStoreSelector)) {
                throw new CertPathBuilderException("TargetConstraints must be an instance of " + X509AttributeCertStoreSelector.class.getName() + " for " + getClass().getName() + " class.");
            }
            try {
                Collection findCertificates = CertPathValidatorUtilities.findCertificates((X509AttributeCertStoreSelector) targetConstraints, extendedPKIXBuilderParameters.getStores());
                if (findCertificates.isEmpty()) {
                    throw new CertPathBuilderException("No attribute certificate found matching targetContraints.");
                }
                Iterator it = findCertificates.iterator();
                CertPathBuilderResult certPathBuilderResult = null;
                while (it.hasNext() && certPathBuilderResult == null) {
                    X509AttributeCertificate x509AttributeCertificate = (X509AttributeCertificate) it.next();
                    X509CertStoreSelector x509CertStoreSelector = new X509CertStoreSelector();
                    Principal[] principals = x509AttributeCertificate.getIssuer().getPrincipals();
                    HashSet hashSet = new HashSet();
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= principals.length) {
                            break;
                        }
                        try {
                            if (principals[i2] instanceof X500Principal) {
                                x509CertStoreSelector.setSubject(((X500Principal) principals[i2]).getEncoded());
                            }
                            hashSet.addAll(CertPathValidatorUtilities.findCertificates(x509CertStoreSelector, extendedPKIXBuilderParameters.getStores()));
                            hashSet.addAll(CertPathValidatorUtilities.findCertificates(x509CertStoreSelector, extendedPKIXBuilderParameters.getCertStores()));
                            i = i2 + 1;
                        } catch (AnnotatedException e) {
                            throw new ExtCertPathBuilderException("Public key certificate for attribute certificate cannot be searched.", e);
                        } catch (IOException e2) {
                            throw new ExtCertPathBuilderException("cannot encode X500Principal.", e2);
                        }
                    }
                    if (hashSet.isEmpty()) {
                        throw new CertPathBuilderException("Public key certificate for attribute certificate cannot be found.");
                    }
                    Iterator it2 = hashSet.iterator();
                    CertPathBuilderResult certPathBuilderResult2 = certPathBuilderResult;
                    while (it2.hasNext() && certPathBuilderResult2 == null) {
                        certPathBuilderResult2 = build(x509AttributeCertificate, (X509Certificate) it2.next(), extendedPKIXBuilderParameters, arrayList);
                    }
                    certPathBuilderResult = certPathBuilderResult2;
                }
                if (certPathBuilderResult == null && this.certPathException != null) {
                    throw new ExtCertPathBuilderException("Possible certificate chain could not be validated.", this.certPathException);
                } else if (certPathBuilderResult != null || this.certPathException != null) {
                    return certPathBuilderResult;
                } else {
                    throw new CertPathBuilderException("Unable to find certificate chain.");
                }
            } catch (AnnotatedException e3) {
                throw new ExtCertPathBuilderException("Error finding target attribute certificate.", e3);
            }
        } else {
            throw new InvalidAlgorithmParameterException("Parameters must be an instance of " + PKIXBuilderParameters.class.getName() + " or " + ExtendedPKIXBuilderParameters.class.getName() + ".");
        }
    }
}
