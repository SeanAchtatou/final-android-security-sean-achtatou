package org.bouncycastle.jce.provider.asymmetric.ec;

import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cryptopro.ECGOST3410NamedCurves;
import org.bouncycastle.asn1.nist.NISTNamedCurves;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.teletrust.TeleTrusTNamedCurves;
import org.bouncycastle.asn1.x9.X962NamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.provider.JCEECPublicKey;
import org.bouncycastle.jce.provider.ProviderUtil;
import org.bouncycastle.jce.spec.ECParameterSpec;

public class ECUtil {
    static int[] convertMidTerms(int[] iArr) {
        int[] iArr2 = new int[3];
        if (iArr.length == 1) {
            iArr2[0] = iArr[0];
        } else if (iArr.length != 3) {
            throw new IllegalArgumentException("Only Trinomials and pentanomials supported");
        } else if (iArr[0] < iArr[1] && iArr[0] < iArr[2]) {
            iArr2[0] = iArr[0];
            if (iArr[1] < iArr[2]) {
                iArr2[1] = iArr[1];
                iArr2[2] = iArr[2];
            } else {
                iArr2[1] = iArr[2];
                iArr2[2] = iArr[1];
            }
        } else if (iArr[1] < iArr[2]) {
            iArr2[0] = iArr[1];
            if (iArr[0] < iArr[2]) {
                iArr2[1] = iArr[0];
                iArr2[2] = iArr[2];
            } else {
                iArr2[1] = iArr[2];
                iArr2[2] = iArr[0];
            }
        } else {
            iArr2[0] = iArr[2];
            if (iArr[0] < iArr[1]) {
                iArr2[1] = iArr[0];
                iArr2[2] = iArr[1];
            } else {
                iArr2[1] = iArr[1];
                iArr2[2] = iArr[0];
            }
        }
        return iArr2;
    }

    public static AsymmetricKeyParameter generatePrivateKeyParameter(PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof ECPrivateKey) {
            ECPrivateKey eCPrivateKey = (ECPrivateKey) privateKey;
            ECParameterSpec parameters = eCPrivateKey.getParameters();
            ECParameterSpec ecImplicitlyCa = parameters == null ? ProviderUtil.getEcImplicitlyCa() : parameters;
            return new ECPrivateKeyParameters(eCPrivateKey.getD(), new ECDomainParameters(ecImplicitlyCa.getCurve(), ecImplicitlyCa.getG(), ecImplicitlyCa.getN(), ecImplicitlyCa.getH(), ecImplicitlyCa.getSeed()));
        }
        throw new InvalidKeyException("can't identify EC private key.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.bouncycastle.jce.provider.asymmetric.ec.EC5Util.convertSpec(java.security.spec.ECParameterSpec, boolean):org.bouncycastle.jce.spec.ECParameterSpec
     arg types: [java.security.spec.ECParameterSpec, int]
     candidates:
      org.bouncycastle.jce.provider.asymmetric.ec.EC5Util.convertSpec(java.security.spec.EllipticCurve, org.bouncycastle.jce.spec.ECParameterSpec):java.security.spec.ECParameterSpec
      org.bouncycastle.jce.provider.asymmetric.ec.EC5Util.convertSpec(java.security.spec.ECParameterSpec, boolean):org.bouncycastle.jce.spec.ECParameterSpec */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.bouncycastle.jce.provider.asymmetric.ec.EC5Util.convertPoint(java.security.spec.ECParameterSpec, java.security.spec.ECPoint, boolean):org.bouncycastle.math.ec.ECPoint
     arg types: [java.security.spec.ECParameterSpec, java.security.spec.ECPoint, int]
     candidates:
      org.bouncycastle.jce.provider.asymmetric.ec.EC5Util.convertPoint(org.bouncycastle.math.ec.ECCurve, java.security.spec.ECPoint, boolean):org.bouncycastle.math.ec.ECPoint
      org.bouncycastle.jce.provider.asymmetric.ec.EC5Util.convertPoint(java.security.spec.ECParameterSpec, java.security.spec.ECPoint, boolean):org.bouncycastle.math.ec.ECPoint */
    public static AsymmetricKeyParameter generatePublicKeyParameter(PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof ECPublicKey) {
            ECPublicKey eCPublicKey = (ECPublicKey) publicKey;
            ECParameterSpec parameters = eCPublicKey.getParameters();
            if (parameters != null) {
                return new ECPublicKeyParameters(eCPublicKey.getQ(), new ECDomainParameters(parameters.getCurve(), parameters.getG(), parameters.getN(), parameters.getH(), parameters.getSeed()));
            }
            ECParameterSpec ecImplicitlyCa = ProviderUtil.getEcImplicitlyCa();
            return new ECPublicKeyParameters(((JCEECPublicKey) eCPublicKey).engineGetQ(), new ECDomainParameters(ecImplicitlyCa.getCurve(), ecImplicitlyCa.getG(), ecImplicitlyCa.getN(), ecImplicitlyCa.getH(), ecImplicitlyCa.getSeed()));
        } else if (publicKey instanceof java.security.interfaces.ECPublicKey) {
            java.security.interfaces.ECPublicKey eCPublicKey2 = (java.security.interfaces.ECPublicKey) publicKey;
            ECParameterSpec convertSpec = EC5Util.convertSpec(eCPublicKey2.getParams(), false);
            return new ECPublicKeyParameters(EC5Util.convertPoint(eCPublicKey2.getParams(), eCPublicKey2.getW(), false), new ECDomainParameters(convertSpec.getCurve(), convertSpec.getG(), convertSpec.getN(), convertSpec.getH(), convertSpec.getSeed()));
        } else {
            throw new InvalidKeyException("cannot identify EC public key.");
        }
    }

    public static String getCurveName(DERObjectIdentifier dERObjectIdentifier) {
        String name = X962NamedCurves.getName(dERObjectIdentifier);
        if (name != null) {
            return name;
        }
        String name2 = SECNamedCurves.getName(dERObjectIdentifier);
        if (name2 == null) {
            name2 = NISTNamedCurves.getName(dERObjectIdentifier);
        }
        return name2 == null ? TeleTrusTNamedCurves.getName(dERObjectIdentifier) : name2;
    }

    public static X9ECParameters getNamedCurveByOid(DERObjectIdentifier dERObjectIdentifier) {
        X9ECParameters byOID = X962NamedCurves.getByOID(dERObjectIdentifier);
        if (byOID != null) {
            return byOID;
        }
        X9ECParameters byOID2 = SECNamedCurves.getByOID(dERObjectIdentifier);
        if (byOID2 == null) {
            byOID2 = NISTNamedCurves.getByOID(dERObjectIdentifier);
        }
        return byOID2 == null ? TeleTrusTNamedCurves.getByOID(dERObjectIdentifier) : byOID2;
    }

    public static DERObjectIdentifier getNamedCurveOid(String str) {
        DERObjectIdentifier oid = X962NamedCurves.getOID(str);
        if (oid != null) {
            return oid;
        }
        DERObjectIdentifier oid2 = SECNamedCurves.getOID(str);
        if (oid2 == null) {
            oid2 = NISTNamedCurves.getOID(str);
        }
        if (oid2 == null) {
            oid2 = TeleTrusTNamedCurves.getOID(str);
        }
        return oid2 == null ? ECGOST3410NamedCurves.getOID(str) : oid2;
    }
}
