package org.bouncycastle.jce.provider;

import java.security.InvalidAlgorithmParameterException;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathParameters;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertPathValidatorResult;
import java.security.cert.CertPathValidatorSpi;
import java.security.cert.Certificate;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jce.exception.ExtCertPathValidatorException;
import org.bouncycastle.x509.ExtendedPKIXParameters;

public class PKIXCertPathValidatorSpi extends CertPathValidatorSpi {
    public CertPathValidatorResult engineValidate(CertPath certPath, CertPathParameters certPathParameters) throws CertPathValidatorException, InvalidAlgorithmParameterException {
        X500Principal x500Principal;
        PublicKey cAPublicKey;
        HashSet hashSet;
        X509Certificate x509Certificate;
        PublicKey publicKey;
        int i;
        PKIXPolicyNode pKIXPolicyNode;
        int i2;
        X500Principal x500Principal2;
        int i3;
        int i4;
        HashSet hashSet2;
        if (!(certPathParameters instanceof PKIXParameters)) {
            throw new InvalidAlgorithmParameterException("Parameters must be a " + PKIXParameters.class.getName() + " instance.");
        }
        ExtendedPKIXParameters instance = certPathParameters instanceof ExtendedPKIXParameters ? (ExtendedPKIXParameters) certPathParameters : ExtendedPKIXParameters.getInstance((PKIXParameters) certPathParameters);
        if (instance.getTrustAnchors() == null) {
            throw new InvalidAlgorithmParameterException("trustAnchors is null, this is not allowed for certification path validation.");
        }
        List<? extends Certificate> certificates = certPath.getCertificates();
        int size = certificates.size();
        if (certificates.isEmpty()) {
            throw new CertPathValidatorException("Certification path is empty.", null, certPath, 0);
        }
        Set<String> initialPolicies = instance.getInitialPolicies();
        try {
            TrustAnchor findTrustAnchor = CertPathValidatorUtilities.findTrustAnchor((X509Certificate) certificates.get(certificates.size() - 1), instance.getTrustAnchors(), instance.getSigProvider());
            if (findTrustAnchor == null) {
                throw new CertPathValidatorException("Trust anchor for certification path not found.", null, certPath, -1);
            }
            ArrayList[] arrayListArr = new ArrayList[(size + 1)];
            for (int i5 = 0; i5 < arrayListArr.length; i5++) {
                arrayListArr[i5] = new ArrayList();
            }
            HashSet hashSet3 = new HashSet();
            hashSet3.add("2.5.29.32.0");
            PKIXPolicyNode pKIXPolicyNode2 = new PKIXPolicyNode(new ArrayList(), 0, hashSet3, null, new HashSet(), "2.5.29.32.0", false);
            arrayListArr[0].add(pKIXPolicyNode2);
            PKIXNameConstraintValidator pKIXNameConstraintValidator = new PKIXNameConstraintValidator();
            HashSet hashSet4 = new HashSet();
            int i6 = instance.isExplicitPolicyRequired() ? 0 : size + 1;
            int i7 = instance.isAnyPolicyInhibited() ? 0 : size + 1;
            int i8 = instance.isPolicyMappingInhibited() ? 0 : size + 1;
            X509Certificate trustedCert = findTrustAnchor.getTrustedCert();
            if (trustedCert != null) {
                try {
                    x500Principal = CertPathValidatorUtilities.getSubjectPrincipal(trustedCert);
                    cAPublicKey = trustedCert.getPublicKey();
                } catch (IllegalArgumentException e) {
                    throw new ExtCertPathValidatorException("Subject of trust anchor could not be (re)encoded.", e, certPath, -1);
                }
            } else {
                x500Principal = new X500Principal(findTrustAnchor.getCAName());
                cAPublicKey = findTrustAnchor.getCAPublicKey();
            }
            try {
                AlgorithmIdentifier algorithmIdentifier = CertPathValidatorUtilities.getAlgorithmIdentifier(cAPublicKey);
                algorithmIdentifier.getObjectId();
                algorithmIdentifier.getParameters();
                if (instance.getTargetConstraints() != null) {
                    if (!instance.getTargetConstraints().match((X509Certificate) certificates.get(0))) {
                        throw new ExtCertPathValidatorException("Target certificate in certification path does not match targetConstraints.", null, certPath, 0);
                    }
                }
                List<PKIXCertPathChecker> certPathCheckers = instance.getCertPathCheckers();
                for (PKIXCertPathChecker init : certPathCheckers) {
                    init.init(false);
                }
                int i9 = size;
                int i10 = i8;
                int i11 = i7;
                int i12 = i6;
                PKIXPolicyNode pKIXPolicyNode3 = pKIXPolicyNode2;
                PublicKey publicKey2 = cAPublicKey;
                X509Certificate x509Certificate2 = null;
                int size2 = certificates.size() - 1;
                X509Certificate x509Certificate3 = trustedCert;
                while (size2 >= 0) {
                    int i13 = size - size2;
                    X509Certificate x509Certificate4 = (X509Certificate) certificates.get(size2);
                    RFC3280CertPathUtilities.processCertA(certPath, instance, size2, publicKey2, size2 == certificates.size() - 1, x500Principal, x509Certificate3);
                    RFC3280CertPathUtilities.processCertBC(certPath, size2, pKIXNameConstraintValidator);
                    PKIXPolicyNode processCertE = RFC3280CertPathUtilities.processCertE(certPath, size2, RFC3280CertPathUtilities.processCertD(certPath, size2, hashSet4, pKIXPolicyNode3, arrayListArr, i11));
                    RFC3280CertPathUtilities.processCertF(certPath, size2, processCertE, i12);
                    if (i13 == size) {
                        x509Certificate = x509Certificate3;
                        publicKey = publicKey2;
                        i = i12;
                        pKIXPolicyNode = processCertE;
                        i2 = i11;
                        x500Principal2 = x500Principal;
                        i3 = i9;
                        i4 = i10;
                    } else if (x509Certificate4 == null || x509Certificate4.getVersion() != 1) {
                        RFC3280CertPathUtilities.prepareNextCertA(certPath, size2);
                        PKIXPolicyNode prepareCertB = RFC3280CertPathUtilities.prepareCertB(certPath, size2, arrayListArr, processCertE, i10);
                        RFC3280CertPathUtilities.prepareNextCertG(certPath, size2, pKIXNameConstraintValidator);
                        int prepareNextCertH1 = RFC3280CertPathUtilities.prepareNextCertH1(certPath, size2, i12);
                        int prepareNextCertH2 = RFC3280CertPathUtilities.prepareNextCertH2(certPath, size2, i10);
                        int prepareNextCertH3 = RFC3280CertPathUtilities.prepareNextCertH3(certPath, size2, i11);
                        int prepareNextCertI1 = RFC3280CertPathUtilities.prepareNextCertI1(certPath, size2, prepareNextCertH1);
                        int prepareNextCertI2 = RFC3280CertPathUtilities.prepareNextCertI2(certPath, size2, prepareNextCertH2);
                        int prepareNextCertJ = RFC3280CertPathUtilities.prepareNextCertJ(certPath, size2, prepareNextCertH3);
                        RFC3280CertPathUtilities.prepareNextCertK(certPath, size2);
                        int prepareNextCertM = RFC3280CertPathUtilities.prepareNextCertM(certPath, size2, RFC3280CertPathUtilities.prepareNextCertL(certPath, size2, i9));
                        RFC3280CertPathUtilities.prepareNextCertN(certPath, size2);
                        Set<String> criticalExtensionOIDs = x509Certificate4.getCriticalExtensionOIDs();
                        if (criticalExtensionOIDs != null) {
                            HashSet hashSet5 = new HashSet(criticalExtensionOIDs);
                            hashSet5.remove(RFC3280CertPathUtilities.KEY_USAGE);
                            hashSet5.remove(RFC3280CertPathUtilities.CERTIFICATE_POLICIES);
                            hashSet5.remove(RFC3280CertPathUtilities.POLICY_MAPPINGS);
                            hashSet5.remove(RFC3280CertPathUtilities.INHIBIT_ANY_POLICY);
                            hashSet5.remove(RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT);
                            hashSet5.remove(RFC3280CertPathUtilities.DELTA_CRL_INDICATOR);
                            hashSet5.remove(RFC3280CertPathUtilities.POLICY_CONSTRAINTS);
                            hashSet5.remove(RFC3280CertPathUtilities.BASIC_CONSTRAINTS);
                            hashSet5.remove(RFC3280CertPathUtilities.SUBJECT_ALTERNATIVE_NAME);
                            hashSet5.remove(RFC3280CertPathUtilities.NAME_CONSTRAINTS);
                            hashSet2 = hashSet5;
                        } else {
                            hashSet2 = new HashSet();
                        }
                        RFC3280CertPathUtilities.prepareNextCertO(certPath, size2, hashSet2, certPathCheckers);
                        X500Principal subjectPrincipal = CertPathValidatorUtilities.getSubjectPrincipal(x509Certificate4);
                        try {
                            PublicKey nextWorkingKey = CertPathValidatorUtilities.getNextWorkingKey(certPath.getCertificates(), size2);
                            AlgorithmIdentifier algorithmIdentifier2 = CertPathValidatorUtilities.getAlgorithmIdentifier(nextWorkingKey);
                            algorithmIdentifier2.getObjectId();
                            algorithmIdentifier2.getParameters();
                            pKIXPolicyNode = prepareCertB;
                            i3 = prepareNextCertM;
                            i4 = prepareNextCertI2;
                            x500Principal2 = subjectPrincipal;
                            i2 = prepareNextCertJ;
                            publicKey = nextWorkingKey;
                            i = prepareNextCertI1;
                            x509Certificate = x509Certificate4;
                        } catch (CertPathValidatorException e2) {
                            throw new CertPathValidatorException("Next working key could not be retrieved.", e2, certPath, size2);
                        }
                    } else {
                        throw new CertPathValidatorException("Version 1 certificates can't be used as CA ones.", null, certPath, size2);
                    }
                    size2--;
                    i9 = i3;
                    i10 = i4;
                    i11 = i2;
                    i12 = i;
                    pKIXPolicyNode3 = pKIXPolicyNode;
                    x509Certificate2 = x509Certificate4;
                    x509Certificate3 = x509Certificate;
                    x500Principal = x500Principal2;
                    publicKey2 = publicKey;
                }
                int wrapupCertB = RFC3280CertPathUtilities.wrapupCertB(certPath, size2 + 1, RFC3280CertPathUtilities.wrapupCertA(i12, x509Certificate2));
                Set<String> criticalExtensionOIDs2 = x509Certificate2.getCriticalExtensionOIDs();
                if (criticalExtensionOIDs2 != null) {
                    HashSet hashSet6 = new HashSet(criticalExtensionOIDs2);
                    hashSet6.remove(RFC3280CertPathUtilities.KEY_USAGE);
                    hashSet6.remove(RFC3280CertPathUtilities.CERTIFICATE_POLICIES);
                    hashSet6.remove(RFC3280CertPathUtilities.POLICY_MAPPINGS);
                    hashSet6.remove(RFC3280CertPathUtilities.INHIBIT_ANY_POLICY);
                    hashSet6.remove(RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT);
                    hashSet6.remove(RFC3280CertPathUtilities.DELTA_CRL_INDICATOR);
                    hashSet6.remove(RFC3280CertPathUtilities.POLICY_CONSTRAINTS);
                    hashSet6.remove(RFC3280CertPathUtilities.BASIC_CONSTRAINTS);
                    hashSet6.remove(RFC3280CertPathUtilities.SUBJECT_ALTERNATIVE_NAME);
                    hashSet6.remove(RFC3280CertPathUtilities.NAME_CONSTRAINTS);
                    hashSet6.remove(RFC3280CertPathUtilities.CRL_DISTRIBUTION_POINTS);
                    hashSet = hashSet6;
                } else {
                    hashSet = new HashSet();
                }
                RFC3280CertPathUtilities.wrapupCertF(certPath, size2 + 1, certPathCheckers, hashSet);
                PKIXPolicyNode wrapupCertG = RFC3280CertPathUtilities.wrapupCertG(certPath, instance, initialPolicies, size2 + 1, arrayListArr, pKIXPolicyNode3, hashSet4);
                if (wrapupCertB > 0 || wrapupCertG != null) {
                    return new PKIXCertPathValidatorResult(findTrustAnchor, wrapupCertG, x509Certificate2.getPublicKey());
                }
                throw new CertPathValidatorException("Path processing failed on policy.", null, certPath, size2);
            } catch (CertPathValidatorException e3) {
                throw new ExtCertPathValidatorException("Algorithm identifier of public key of trust anchor could not be read.", e3, certPath, -1);
            }
        } catch (AnnotatedException e4) {
            throw new CertPathValidatorException(e4.getMessage(), e4, certPath, certificates.size() - 1);
        }
    }
}
