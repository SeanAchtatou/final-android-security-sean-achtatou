package org.bouncycastle.jce.provider.asymmetric.ec;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Hashtable;
import javax.crypto.KeyAgreementSpi;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9IntegerConverter;
import org.bouncycastle.crypto.BasicAgreement;
import org.bouncycastle.crypto.DerivationFunction;
import org.bouncycastle.crypto.agreement.ECDHBasicAgreement;
import org.bouncycastle.crypto.agreement.ECDHCBasicAgreement;
import org.bouncycastle.crypto.agreement.kdf.DHKDFParameters;
import org.bouncycastle.crypto.agreement.kdf.ECDHKEKGenerator;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;

public class KeyAgreement extends KeyAgreementSpi {
    private static final Hashtable algorithms = new Hashtable();
    private static final X9IntegerConverter converter = new X9IntegerConverter();
    private BasicAgreement agreement;
    private DerivationFunction kdf;
    private ECPrivateKeyParameters privKey;
    private BigInteger result;

    public static class DH extends KeyAgreement {
        public DH() {
            super(new ECDHBasicAgreement());
        }
    }

    public static class DHC extends KeyAgreement {
        public DHC() {
            super(new ECDHCBasicAgreement());
        }
    }

    public static class DHwithSHA1KDF extends KeyAgreement {
        public DHwithSHA1KDF() {
            super(new ECDHBasicAgreement(), new ECDHKEKGenerator(new SHA1Digest()));
        }
    }

    static {
        Integer num = new Integer(128);
        Integer num2 = new Integer(192);
        Integer num3 = new Integer(256);
        algorithms.put(NISTObjectIdentifiers.id_aes128_CBC.getId(), num);
        algorithms.put(NISTObjectIdentifiers.id_aes192_CBC.getId(), num2);
        algorithms.put(NISTObjectIdentifiers.id_aes256_CBC.getId(), num3);
        algorithms.put(NISTObjectIdentifiers.id_aes128_wrap.getId(), num);
        algorithms.put(NISTObjectIdentifiers.id_aes192_wrap.getId(), num2);
        algorithms.put(NISTObjectIdentifiers.id_aes256_wrap.getId(), num3);
        algorithms.put(PKCSObjectIdentifiers.id_alg_CMS3DESwrap.getId(), num2);
    }

    protected KeyAgreement(BasicAgreement basicAgreement) {
        this.agreement = basicAgreement;
    }

    protected KeyAgreement(BasicAgreement basicAgreement, DerivationFunction derivationFunction) {
        this.agreement = basicAgreement;
        this.kdf = derivationFunction;
    }

    private byte[] bigIntToBytes(BigInteger bigInteger) {
        return converter.integerToBytes(bigInteger, converter.getByteLength(this.privKey.getParameters().getG().getX()));
    }

    /* access modifiers changed from: protected */
    public Key engineDoPhase(Key key, boolean z) throws InvalidKeyException, IllegalStateException {
        if (this.privKey == null) {
            throw new IllegalStateException("EC Diffie-Hellman not initialised.");
        } else if (!z) {
            throw new IllegalStateException("EC Diffie-Hellman can only be between two parties.");
        } else if (!(key instanceof ECPublicKey)) {
            throw new InvalidKeyException("EC Key Agreement doPhase requires ECPublicKey");
        } else {
            this.result = this.agreement.calculateAgreement(ECUtil.generatePublicKeyParameter((PublicKey) key));
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public int engineGenerateSecret(byte[] bArr, int i) throws IllegalStateException, ShortBufferException {
        byte[] bigIntToBytes = bigIntToBytes(this.result);
        if (bArr.length - i < bigIntToBytes.length) {
            throw new ShortBufferException("ECKeyAgreement - buffer too short");
        }
        System.arraycopy(bigIntToBytes, 0, bArr, i, bigIntToBytes.length);
        return bigIntToBytes.length;
    }

    /* access modifiers changed from: protected */
    public SecretKey engineGenerateSecret(String str) throws NoSuchAlgorithmException {
        if (this.kdf == null) {
            return new SecretKeySpec(bigIntToBytes(this.result), str);
        }
        if (!algorithms.containsKey(str)) {
            throw new NoSuchAlgorithmException("unknown algorithm encountered: " + str);
        }
        int intValue = ((Integer) algorithms.get(str)).intValue();
        DHKDFParameters dHKDFParameters = new DHKDFParameters(new DERObjectIdentifier(str), intValue, bigIntToBytes(this.result));
        byte[] bArr = new byte[(intValue / 8)];
        this.kdf.init(dHKDFParameters);
        this.kdf.generateBytes(bArr, 0, bArr.length);
        return new SecretKeySpec(bArr, str);
    }

    /* access modifiers changed from: protected */
    public byte[] engineGenerateSecret() throws IllegalStateException {
        return bigIntToBytes(this.result);
    }

    /* access modifiers changed from: protected */
    public void engineInit(Key key, SecureRandom secureRandom) throws InvalidKeyException {
        if (!(key instanceof ECPrivateKey)) {
            throw new InvalidKeyException("ECKeyAgreement requires ECPrivateKey");
        }
        this.privKey = (ECPrivateKeyParameters) ECUtil.generatePrivateKeyParameter((PrivateKey) key);
        this.agreement.init(this.privKey);
    }

    /* access modifiers changed from: protected */
    public void engineInit(Key key, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        if (!(key instanceof ECPrivateKey)) {
            throw new InvalidKeyException("ECKeyAgreement requires ECPrivateKey for initialisation");
        }
        this.privKey = (ECPrivateKeyParameters) ECUtil.generatePrivateKeyParameter((PrivateKey) key);
        this.agreement.init(this.privKey);
    }
}
