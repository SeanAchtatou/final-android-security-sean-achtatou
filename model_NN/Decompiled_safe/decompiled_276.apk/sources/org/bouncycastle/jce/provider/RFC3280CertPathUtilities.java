package org.bouncycastle.jce.provider;

import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.security.cert.X509Extension;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.GeneralSubtree;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.bouncycastle.asn1.x509.NameConstraints;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.jce.exception.ExtCertPathValidatorException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.x509.ExtendedPKIXBuilderParameters;
import org.bouncycastle.x509.ExtendedPKIXParameters;
import org.bouncycastle.x509.X509CRLStoreSelector;
import org.bouncycastle.x509.X509CertStoreSelector;

public class RFC3280CertPathUtilities {
    protected static final String ANY_POLICY = "2.5.29.32.0";
    protected static final String AUTHORITY_KEY_IDENTIFIER = X509Extensions.AuthorityKeyIdentifier.getId();
    protected static final String BASIC_CONSTRAINTS = X509Extensions.BasicConstraints.getId();
    protected static final String CERTIFICATE_POLICIES = X509Extensions.CertificatePolicies.getId();
    protected static final String CRL_DISTRIBUTION_POINTS = X509Extensions.CRLDistributionPoints.getId();
    protected static final String CRL_NUMBER = X509Extensions.CRLNumber.getId();
    protected static final int CRL_SIGN = 6;
    protected static final String DELTA_CRL_INDICATOR = X509Extensions.DeltaCRLIndicator.getId();
    protected static final String FRESHEST_CRL = X509Extensions.FreshestCRL.getId();
    protected static final String INHIBIT_ANY_POLICY = X509Extensions.InhibitAnyPolicy.getId();
    protected static final String ISSUING_DISTRIBUTION_POINT = X509Extensions.IssuingDistributionPoint.getId();
    protected static final int KEY_CERT_SIGN = 5;
    protected static final String KEY_USAGE = X509Extensions.KeyUsage.getId();
    protected static final String NAME_CONSTRAINTS = X509Extensions.NameConstraints.getId();
    protected static final String POLICY_CONSTRAINTS = X509Extensions.PolicyConstraints.getId();
    protected static final String POLICY_MAPPINGS = X509Extensions.PolicyMappings.getId();
    protected static final String SUBJECT_ALTERNATIVE_NAME = X509Extensions.SubjectAlternativeName.getId();
    protected static final String[] crlReasons = {"unspecified", "keyCompromise", "cACompromise", "affiliationChanged", "superseded", "cessationOfOperation", "certificateHold", "unknown", "removeFromCRL", "privilegeWithdrawn", "aACompromise"};

    private static void checkCRL(DistributionPoint distributionPoint, ExtendedPKIXParameters extendedPKIXParameters, X509Certificate x509Certificate, Date date, X509Certificate x509Certificate2, PublicKey publicKey, CertStatus certStatus, ReasonsMask reasonsMask, List list) throws AnnotatedException {
        Set<String> criticalExtensionOIDs;
        Date date2 = new Date(System.currentTimeMillis());
        if (date.getTime() > date2.getTime()) {
            throw new AnnotatedException("Validation time is in future.");
        }
        Iterator it = CertPathValidatorUtilities.getCompleteCRLs(distributionPoint, x509Certificate, date2, extendedPKIXParameters).iterator();
        AnnotatedException annotatedException = null;
        boolean z = false;
        while (it.hasNext() && certStatus.getCertStatus() == 11 && !reasonsMask.isAllReasons()) {
            try {
                X509CRL x509crl = (X509CRL) it.next();
                ReasonsMask processCRLD = processCRLD(x509crl, distributionPoint);
                if (processCRLD.hasNewReasons(reasonsMask)) {
                    X509CRL processCRLH = extendedPKIXParameters.isUseDeltasEnabled() ? processCRLH(CertPathValidatorUtilities.getDeltaCRLs(date2, extendedPKIXParameters, x509crl), processCRLG(x509crl, processCRLF(x509crl, x509Certificate, x509Certificate2, publicKey, extendedPKIXParameters, list))) : null;
                    if (extendedPKIXParameters.getValidityModel() == 1 || x509Certificate.getNotAfter().getTime() >= x509crl.getThisUpdate().getTime()) {
                        processCRLB1(distributionPoint, x509Certificate, x509crl);
                        processCRLB2(distributionPoint, x509Certificate, x509crl);
                        processCRLC(processCRLH, x509crl, extendedPKIXParameters);
                        processCRLI(date, processCRLH, x509Certificate, certStatus, extendedPKIXParameters);
                        processCRLJ(date, x509crl, x509Certificate, certStatus);
                        if (certStatus.getCertStatus() == 8) {
                            certStatus.setCertStatus(11);
                        }
                        reasonsMask.addReasons(processCRLD);
                        Set<String> criticalExtensionOIDs2 = x509crl.getCriticalExtensionOIDs();
                        if (criticalExtensionOIDs2 != null) {
                            HashSet hashSet = new HashSet(criticalExtensionOIDs2);
                            hashSet.remove(X509Extensions.IssuingDistributionPoint.getId());
                            hashSet.remove(X509Extensions.DeltaCRLIndicator.getId());
                            if (!hashSet.isEmpty()) {
                                throw new AnnotatedException("CRL contains unsupported critical extensions.");
                            }
                        }
                        if (!(processCRLH == null || (criticalExtensionOIDs = processCRLH.getCriticalExtensionOIDs()) == null)) {
                            HashSet hashSet2 = new HashSet(criticalExtensionOIDs);
                            hashSet2.remove(X509Extensions.IssuingDistributionPoint.getId());
                            hashSet2.remove(X509Extensions.DeltaCRLIndicator.getId());
                            if (!hashSet2.isEmpty()) {
                                throw new AnnotatedException("Delta CRL contains unsupported critical extension.");
                            }
                        }
                        z = true;
                    } else {
                        throw new AnnotatedException("No valid CRL for current time found.");
                    }
                } else {
                    continue;
                }
            } catch (AnnotatedException e) {
                annotatedException = e;
            }
        }
        if (!z) {
            throw annotatedException;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.bouncycastle.asn1.x509.DistributionPointName.<init>(int, org.bouncycastle.asn1.ASN1Encodable):void
     arg types: [int, org.bouncycastle.asn1.x509.GeneralNames]
     candidates:
      org.bouncycastle.asn1.x509.DistributionPointName.<init>(int, org.bouncycastle.asn1.DEREncodable):void
      org.bouncycastle.asn1.x509.DistributionPointName.<init>(int, org.bouncycastle.asn1.ASN1Encodable):void */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void checkCRLs(org.bouncycastle.x509.ExtendedPKIXParameters r13, java.security.cert.X509Certificate r14, java.util.Date r15, java.security.cert.X509Certificate r16, java.security.PublicKey r17, java.util.List r18) throws org.bouncycastle.jce.provider.AnnotatedException {
        /*
            r0 = 0
            java.lang.String r1 = org.bouncycastle.jce.provider.RFC3280CertPathUtilities.CRL_DISTRIBUTION_POINTS     // Catch:{ Exception -> 0x0051 }
            org.bouncycastle.asn1.DERObject r1 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.getExtensionValue(r14, r1)     // Catch:{ Exception -> 0x0051 }
            org.bouncycastle.asn1.x509.CRLDistPoint r1 = org.bouncycastle.asn1.x509.CRLDistPoint.getInstance(r1)     // Catch:{ Exception -> 0x0051 }
            org.bouncycastle.jce.provider.CertPathValidatorUtilities.addAdditionalStoresFromCRLDistributionPoint(r1, r13)     // Catch:{ AnnotatedException -> 0x005a }
            org.bouncycastle.jce.provider.CertStatus r6 = new org.bouncycastle.jce.provider.CertStatus
            r6.<init>()
            org.bouncycastle.jce.provider.ReasonsMask r7 = new org.bouncycastle.jce.provider.ReasonsMask
            r7.<init>()
            r2 = 0
            if (r1 == 0) goto L_0x0144
            org.bouncycastle.asn1.x509.DistributionPoint[] r9 = r1.getDistributionPoints()     // Catch:{ Exception -> 0x0063 }
            if (r9 == 0) goto L_0x0144
            r1 = 0
            r10 = r1
            r11 = r2
            r12 = r0
        L_0x0025:
            int r0 = r9.length
            if (r10 >= r0) goto L_0x0070
            int r0 = r6.getCertStatus()
            r1 = 11
            if (r0 != r1) goto L_0x0070
            boolean r0 = r7.isAllReasons()
            if (r0 != 0) goto L_0x0070
            java.lang.Object r1 = r13.clone()
            org.bouncycastle.x509.ExtendedPKIXParameters r1 = (org.bouncycastle.x509.ExtendedPKIXParameters) r1
            r0 = r9[r10]     // Catch:{ AnnotatedException -> 0x006c }
            r2 = r14
            r3 = r15
            r4 = r16
            r5 = r17
            r8 = r18
            checkCRL(r0, r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ AnnotatedException -> 0x006c }
            r0 = 1
            r1 = r12
        L_0x004b:
            int r2 = r10 + 1
            r10 = r2
            r11 = r0
            r12 = r1
            goto L_0x0025
        L_0x0051:
            r13 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r14 = new org.bouncycastle.jce.provider.AnnotatedException
            java.lang.String r15 = "CRL distribution point extension could not be read."
            r14.<init>(r15, r13)
            throw r14
        L_0x005a:
            r13 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r14 = new org.bouncycastle.jce.provider.AnnotatedException
            java.lang.String r15 = "No additional CRL locations could be decoded from CRL distribution point extension."
            r14.<init>(r15, r13)
            throw r14
        L_0x0063:
            r13 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r14 = new org.bouncycastle.jce.provider.AnnotatedException
            java.lang.String r15 = "Distribution points could not be read."
            r14.<init>(r15, r13)
            throw r14
        L_0x006c:
            r0 = move-exception
            r1 = r0
            r0 = r11
            goto L_0x004b
        L_0x0070:
            r9 = r11
            r10 = r12
        L_0x0072:
            int r0 = r6.getCertStatus()
            r1 = 11
            if (r0 != r1) goto L_0x0140
            boolean r0 = r7.isAllReasons()
            if (r0 != 0) goto L_0x0140
            org.bouncycastle.asn1.ASN1InputStream r0 = new org.bouncycastle.asn1.ASN1InputStream     // Catch:{ Exception -> 0x00c3 }
            javax.security.auth.x500.X500Principal r1 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.getEncodedIssuerPrincipal(r14)     // Catch:{ Exception -> 0x00c3 }
            byte[] r1 = r1.getEncoded()     // Catch:{ Exception -> 0x00c3 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00c3 }
            org.bouncycastle.asn1.DERObject r1 = r0.readObject()     // Catch:{ Exception -> 0x00c3 }
            org.bouncycastle.asn1.x509.DistributionPoint r0 = new org.bouncycastle.asn1.x509.DistributionPoint     // Catch:{ AnnotatedException -> 0x00cc }
            org.bouncycastle.asn1.x509.DistributionPointName r2 = new org.bouncycastle.asn1.x509.DistributionPointName     // Catch:{ AnnotatedException -> 0x00cc }
            r3 = 0
            org.bouncycastle.asn1.x509.GeneralNames r4 = new org.bouncycastle.asn1.x509.GeneralNames     // Catch:{ AnnotatedException -> 0x00cc }
            org.bouncycastle.asn1.x509.GeneralName r5 = new org.bouncycastle.asn1.x509.GeneralName     // Catch:{ AnnotatedException -> 0x00cc }
            r8 = 4
            r5.<init>(r8, r1)     // Catch:{ AnnotatedException -> 0x00cc }
            r4.<init>(r5)     // Catch:{ AnnotatedException -> 0x00cc }
            r2.<init>(r3, r4)     // Catch:{ AnnotatedException -> 0x00cc }
            r1 = 0
            r3 = 0
            r0.<init>(r2, r1, r3)     // Catch:{ AnnotatedException -> 0x00cc }
            java.lang.Object r1 = r13.clone()     // Catch:{ AnnotatedException -> 0x00cc }
            org.bouncycastle.x509.ExtendedPKIXParameters r1 = (org.bouncycastle.x509.ExtendedPKIXParameters) r1     // Catch:{ AnnotatedException -> 0x00cc }
            r2 = r14
            r3 = r15
            r4 = r16
            r5 = r17
            r8 = r18
            checkCRL(r0, r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ AnnotatedException -> 0x00cc }
            r13 = 1
            r14 = r10
        L_0x00bc:
            if (r13 != 0) goto L_0x00d8
            boolean r13 = r14 instanceof org.bouncycastle.jce.provider.AnnotatedException
            if (r13 == 0) goto L_0x00d0
            throw r14
        L_0x00c3:
            r13 = move-exception
            org.bouncycastle.jce.provider.AnnotatedException r14 = new org.bouncycastle.jce.provider.AnnotatedException     // Catch:{ AnnotatedException -> 0x00cc }
            java.lang.String r15 = "Issuer from certificate for CRL could not be reencoded."
            r14.<init>(r15, r13)     // Catch:{ AnnotatedException -> 0x00cc }
            throw r14     // Catch:{ AnnotatedException -> 0x00cc }
        L_0x00cc:
            r13 = move-exception
            r14 = r13
            r13 = r9
            goto L_0x00bc
        L_0x00d0:
            org.bouncycastle.jce.provider.AnnotatedException r13 = new org.bouncycastle.jce.provider.AnnotatedException
            java.lang.String r15 = "No valid CRL found."
            r13.<init>(r15, r14)
            throw r13
        L_0x00d8:
            int r13 = r6.getCertStatus()
            r14 = 11
            if (r13 == r14) goto L_0x011c
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "Certificate revocation after "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.util.Date r14 = r6.getRevocationDate()
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r13 = r13.toString()
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.StringBuilder r13 = r14.append(r13)
            java.lang.String r14 = ", reason: "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String[] r14 = org.bouncycastle.jce.provider.RFC3280CertPathUtilities.crlReasons
            int r15 = r6.getCertStatus()
            r14 = r14[r15]
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r13 = r13.toString()
            org.bouncycastle.jce.provider.AnnotatedException r14 = new org.bouncycastle.jce.provider.AnnotatedException
            r14.<init>(r13)
            throw r14
        L_0x011c:
            boolean r13 = r7.isAllReasons()
            if (r13 != 0) goto L_0x012f
            int r13 = r6.getCertStatus()
            r14 = 11
            if (r13 != r14) goto L_0x012f
            r13 = 12
            r6.setCertStatus(r13)
        L_0x012f:
            int r13 = r6.getCertStatus()
            r14 = 12
            if (r13 != r14) goto L_0x013f
            org.bouncycastle.jce.provider.AnnotatedException r13 = new org.bouncycastle.jce.provider.AnnotatedException
            java.lang.String r14 = "Certificate status could not be determined."
            r13.<init>(r14)
            throw r13
        L_0x013f:
            return
        L_0x0140:
            r13 = r9
            r14 = r10
            goto L_0x00bc
        L_0x0144:
            r9 = r2
            r10 = r0
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.jce.provider.RFC3280CertPathUtilities.checkCRLs(org.bouncycastle.x509.ExtendedPKIXParameters, java.security.cert.X509Certificate, java.util.Date, java.security.cert.X509Certificate, java.security.PublicKey, java.util.List):void");
    }

    protected static PKIXPolicyNode prepareCertB(CertPath certPath, int i, List[] listArr, PKIXPolicyNode pKIXPolicyNode, int i2) throws CertPathValidatorException {
        PKIXPolicyNode pKIXPolicyNode2;
        boolean z;
        Set set;
        List<? extends Certificate> certificates = certPath.getCertificates();
        X509Certificate x509Certificate = (X509Certificate) certificates.get(i);
        int size = certificates.size() - i;
        try {
            ASN1Sequence instance = DERSequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, POLICY_MAPPINGS));
            if (instance == null) {
                return pKIXPolicyNode;
            }
            HashMap hashMap = new HashMap();
            HashSet hashSet = new HashSet();
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 >= instance.size()) {
                    break;
                }
                ASN1Sequence aSN1Sequence = (ASN1Sequence) instance.getObjectAt(i4);
                String id = ((DERObjectIdentifier) aSN1Sequence.getObjectAt(0)).getId();
                String id2 = ((DERObjectIdentifier) aSN1Sequence.getObjectAt(1)).getId();
                if (!hashMap.containsKey(id)) {
                    HashSet hashSet2 = new HashSet();
                    hashSet2.add(id2);
                    hashMap.put(id, hashSet2);
                    hashSet.add(id);
                } else {
                    ((Set) hashMap.get(id)).add(id2);
                }
                i3 = i4 + 1;
            }
            Iterator it = hashSet.iterator();
            while (true) {
                PKIXPolicyNode pKIXPolicyNode3 = pKIXPolicyNode;
                if (!it.hasNext()) {
                    return pKIXPolicyNode3;
                }
                String str = (String) it.next();
                if (i2 > 0) {
                    Iterator it2 = listArr[size].iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            z = false;
                            break;
                        }
                        PKIXPolicyNode pKIXPolicyNode4 = (PKIXPolicyNode) it2.next();
                        if (pKIXPolicyNode4.getValidPolicy().equals(str)) {
                            pKIXPolicyNode4.expectedPolicies = (Set) hashMap.get(str);
                            z = true;
                            break;
                        }
                    }
                    if (!z) {
                        Iterator it3 = listArr[size].iterator();
                        while (true) {
                            if (!it3.hasNext()) {
                                break;
                            }
                            PKIXPolicyNode pKIXPolicyNode5 = (PKIXPolicyNode) it3.next();
                            if (ANY_POLICY.equals(pKIXPolicyNode5.getValidPolicy())) {
                                try {
                                    Enumeration objects = ((ASN1Sequence) CertPathValidatorUtilities.getExtensionValue(x509Certificate, CERTIFICATE_POLICIES)).getObjects();
                                    while (true) {
                                        if (!objects.hasMoreElements()) {
                                            set = null;
                                            break;
                                        }
                                        try {
                                            PolicyInformation instance2 = PolicyInformation.getInstance(objects.nextElement());
                                            if (ANY_POLICY.equals(instance2.getPolicyIdentifier().getId())) {
                                                try {
                                                    set = CertPathValidatorUtilities.getQualifierSet(instance2.getPolicyQualifiers());
                                                    break;
                                                } catch (CertPathValidatorException e) {
                                                    throw new ExtCertPathValidatorException("Policy qualifier info set could not be decoded.", e, certPath, i);
                                                }
                                            }
                                        } catch (Exception e2) {
                                            throw new CertPathValidatorException("Policy information could not be decoded.", e2, certPath, i);
                                        }
                                    }
                                    boolean contains = x509Certificate.getCriticalExtensionOIDs() != null ? x509Certificate.getCriticalExtensionOIDs().contains(CERTIFICATE_POLICIES) : false;
                                    PKIXPolicyNode pKIXPolicyNode6 = (PKIXPolicyNode) pKIXPolicyNode5.getParent();
                                    if (ANY_POLICY.equals(pKIXPolicyNode6.getValidPolicy())) {
                                        PKIXPolicyNode pKIXPolicyNode7 = new PKIXPolicyNode(new ArrayList(), size, (Set) hashMap.get(str), pKIXPolicyNode6, set, str, contains);
                                        pKIXPolicyNode6.addChild(pKIXPolicyNode7);
                                        listArr[size].add(pKIXPolicyNode7);
                                    }
                                } catch (AnnotatedException e3) {
                                    throw new ExtCertPathValidatorException("Certificate policies extension could not be decoded.", e3, certPath, i);
                                }
                            }
                        }
                    }
                    pKIXPolicyNode = pKIXPolicyNode3;
                } else if (i2 <= 0) {
                    Iterator it4 = listArr[size].iterator();
                    PKIXPolicyNode pKIXPolicyNode8 = pKIXPolicyNode3;
                    while (it4.hasNext()) {
                        PKIXPolicyNode pKIXPolicyNode9 = (PKIXPolicyNode) it4.next();
                        if (pKIXPolicyNode9.getValidPolicy().equals(str)) {
                            ((PKIXPolicyNode) pKIXPolicyNode9.getParent()).removeChild(pKIXPolicyNode9);
                            it4.remove();
                            pKIXPolicyNode2 = pKIXPolicyNode8;
                            for (int i5 = size - 1; i5 >= 0; i5--) {
                                List list = listArr[i5];
                                int i6 = 0;
                                while (true) {
                                    PKIXPolicyNode pKIXPolicyNode10 = pKIXPolicyNode2;
                                    if (i6 >= list.size()) {
                                        pKIXPolicyNode2 = pKIXPolicyNode10;
                                        break;
                                    }
                                    PKIXPolicyNode pKIXPolicyNode11 = (PKIXPolicyNode) list.get(i6);
                                    if (!pKIXPolicyNode11.hasChildren()) {
                                        pKIXPolicyNode2 = CertPathValidatorUtilities.removePolicyNode(pKIXPolicyNode10, listArr, pKIXPolicyNode11);
                                        if (pKIXPolicyNode2 == null) {
                                            break;
                                        }
                                    } else {
                                        pKIXPolicyNode2 = pKIXPolicyNode10;
                                    }
                                    i6++;
                                }
                            }
                        } else {
                            pKIXPolicyNode2 = pKIXPolicyNode8;
                        }
                        pKIXPolicyNode8 = pKIXPolicyNode2;
                    }
                    pKIXPolicyNode = pKIXPolicyNode8;
                } else {
                    pKIXPolicyNode = pKIXPolicyNode3;
                }
            }
        } catch (AnnotatedException e4) {
            throw new ExtCertPathValidatorException("Policy mappings extension could not be decoded.", e4, certPath, i);
        }
    }

    protected static void prepareNextCertA(CertPath certPath, int i) throws CertPathValidatorException {
        try {
            ASN1Sequence instance = DERSequence.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Certificate) certPath.getCertificates().get(i), POLICY_MAPPINGS));
            if (instance != null) {
                int i2 = 0;
                while (i2 < instance.size()) {
                    try {
                        ASN1Sequence instance2 = DERSequence.getInstance(instance.getObjectAt(i2));
                        DERObjectIdentifier instance3 = DERObjectIdentifier.getInstance(instance2.getObjectAt(0));
                        DERObjectIdentifier instance4 = DERObjectIdentifier.getInstance(instance2.getObjectAt(1));
                        if (ANY_POLICY.equals(instance3.getId())) {
                            throw new CertPathValidatorException("IssuerDomainPolicy is anyPolicy", null, certPath, i);
                        } else if (ANY_POLICY.equals(instance4.getId())) {
                            throw new CertPathValidatorException("SubjectDomainPolicy is anyPolicy,", null, certPath, i);
                        } else {
                            i2++;
                        }
                    } catch (Exception e) {
                        throw new ExtCertPathValidatorException("Policy mappings extension contents could not be decoded.", e, certPath, i);
                    }
                }
            }
        } catch (AnnotatedException e2) {
            throw new ExtCertPathValidatorException("Policy mappings extension could not be decoded.", e2, certPath, i);
        }
    }

    protected static void prepareNextCertG(CertPath certPath, int i, PKIXNameConstraintValidator pKIXNameConstraintValidator) throws CertPathValidatorException {
        try {
            ASN1Sequence instance = DERSequence.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Certificate) certPath.getCertificates().get(i), NAME_CONSTRAINTS));
            NameConstraints nameConstraints = instance != null ? new NameConstraints(instance) : null;
            if (nameConstraints != null) {
                ASN1Sequence permittedSubtrees = nameConstraints.getPermittedSubtrees();
                if (permittedSubtrees != null) {
                    try {
                        pKIXNameConstraintValidator.intersectPermittedSubtree(permittedSubtrees);
                    } catch (Exception e) {
                        throw new ExtCertPathValidatorException("Permitted subtrees cannot be build from name constraints extension.", e, certPath, i);
                    }
                }
                ASN1Sequence excludedSubtrees = nameConstraints.getExcludedSubtrees();
                if (excludedSubtrees != null) {
                    Enumeration objects = excludedSubtrees.getObjects();
                    while (objects.hasMoreElements()) {
                        try {
                            pKIXNameConstraintValidator.addExcludedSubtree(GeneralSubtree.getInstance(objects.nextElement()));
                        } catch (Exception e2) {
                            throw new ExtCertPathValidatorException("Excluded subtrees cannot be build from name constraints extension.", e2, certPath, i);
                        }
                    }
                }
            }
        } catch (Exception e3) {
            throw new ExtCertPathValidatorException("Name constraints extension could not be decoded.", e3, certPath, i);
        }
    }

    protected static int prepareNextCertH1(CertPath certPath, int i, int i2) {
        return (CertPathValidatorUtilities.isSelfIssued((X509Certificate) certPath.getCertificates().get(i)) || i2 == 0) ? i2 : i2 - 1;
    }

    protected static int prepareNextCertH2(CertPath certPath, int i, int i2) {
        return (CertPathValidatorUtilities.isSelfIssued((X509Certificate) certPath.getCertificates().get(i)) || i2 == 0) ? i2 : i2 - 1;
    }

    protected static int prepareNextCertH3(CertPath certPath, int i, int i2) {
        return (CertPathValidatorUtilities.isSelfIssued((X509Certificate) certPath.getCertificates().get(i)) || i2 == 0) ? i2 : i2 - 1;
    }

    protected static int prepareNextCertI1(CertPath certPath, int i, int i2) throws CertPathValidatorException {
        try {
            ASN1Sequence instance = DERSequence.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Certificate) certPath.getCertificates().get(i), POLICY_CONSTRAINTS));
            if (instance != null) {
                Enumeration objects = instance.getObjects();
                while (true) {
                    if (!objects.hasMoreElements()) {
                        break;
                    }
                    try {
                        ASN1TaggedObject instance2 = ASN1TaggedObject.getInstance(objects.nextElement());
                        if (instance2.getTagNo() == 0) {
                            int intValue = DERInteger.getInstance(instance2).getValue().intValue();
                            if (intValue < i2) {
                                return intValue;
                            }
                        }
                    } catch (IllegalArgumentException e) {
                        throw new ExtCertPathValidatorException("Policy constraints extension contents cannot be decoded.", e, certPath, i);
                    }
                }
            }
            return i2;
        } catch (Exception e2) {
            throw new ExtCertPathValidatorException("Policy constraints extension cannot be decoded.", e2, certPath, i);
        }
    }

    protected static int prepareNextCertI2(CertPath certPath, int i, int i2) throws CertPathValidatorException {
        try {
            ASN1Sequence instance = DERSequence.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Certificate) certPath.getCertificates().get(i), POLICY_CONSTRAINTS));
            if (instance != null) {
                Enumeration objects = instance.getObjects();
                while (true) {
                    if (!objects.hasMoreElements()) {
                        break;
                    }
                    try {
                        ASN1TaggedObject instance2 = ASN1TaggedObject.getInstance(objects.nextElement());
                        if (instance2.getTagNo() == 1) {
                            int intValue = DERInteger.getInstance(instance2).getValue().intValue();
                            if (intValue < i2) {
                                return intValue;
                            }
                        }
                    } catch (IllegalArgumentException e) {
                        throw new ExtCertPathValidatorException("Policy constraints extension contents cannot be decoded.", e, certPath, i);
                    }
                }
            }
            return i2;
        } catch (Exception e2) {
            throw new ExtCertPathValidatorException("Policy constraints extension cannot be decoded.", e2, certPath, i);
        }
    }

    protected static int prepareNextCertJ(CertPath certPath, int i, int i2) throws CertPathValidatorException {
        int intValue;
        try {
            DERInteger instance = DERInteger.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Certificate) certPath.getCertificates().get(i), INHIBIT_ANY_POLICY));
            return (instance == null || (intValue = instance.getValue().intValue()) >= i2) ? i2 : intValue;
        } catch (Exception e) {
            throw new ExtCertPathValidatorException("Inhibit any-policy extension cannot be decoded.", e, certPath, i);
        }
    }

    protected static void prepareNextCertK(CertPath certPath, int i) throws CertPathValidatorException {
        try {
            BasicConstraints instance = BasicConstraints.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Certificate) certPath.getCertificates().get(i), BASIC_CONSTRAINTS));
            if (instance == null) {
                throw new CertPathValidatorException("Intermediate certificate lacks BasicConstraints");
            } else if (!instance.isCA()) {
                throw new CertPathValidatorException("Not a CA certificate");
            }
        } catch (Exception e) {
            throw new ExtCertPathValidatorException("Basic constraints extension cannot be decoded.", e, certPath, i);
        }
    }

    protected static int prepareNextCertL(CertPath certPath, int i, int i2) throws CertPathValidatorException {
        if (CertPathValidatorUtilities.isSelfIssued((X509Certificate) certPath.getCertificates().get(i))) {
            return i2;
        }
        if (i2 > 0) {
            return i2 - 1;
        }
        throw new ExtCertPathValidatorException("Max path length not greater than zero", null, certPath, i);
    }

    protected static int prepareNextCertM(CertPath certPath, int i, int i2) throws CertPathValidatorException {
        BigInteger pathLenConstraint;
        int intValue;
        try {
            BasicConstraints instance = BasicConstraints.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Certificate) certPath.getCertificates().get(i), BASIC_CONSTRAINTS));
            return (instance == null || (pathLenConstraint = instance.getPathLenConstraint()) == null || (intValue = pathLenConstraint.intValue()) >= i2) ? i2 : intValue;
        } catch (Exception e) {
            throw new ExtCertPathValidatorException("Basic constraints extension cannot be decoded.", e, certPath, i);
        }
    }

    protected static void prepareNextCertN(CertPath certPath, int i) throws CertPathValidatorException {
        boolean[] keyUsage = ((X509Certificate) certPath.getCertificates().get(i)).getKeyUsage();
        if (keyUsage != null && !keyUsage[5]) {
            throw new ExtCertPathValidatorException("Issuer certificate keyusage extension is critical and does not permit key signing.", null, certPath, i);
        }
    }

    protected static void prepareNextCertO(CertPath certPath, int i, Set set, List list) throws CertPathValidatorException {
        X509Certificate x509Certificate = (X509Certificate) certPath.getCertificates().get(i);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            try {
                ((PKIXCertPathChecker) it.next()).check(x509Certificate, set);
            } catch (CertPathValidatorException e) {
                throw new CertPathValidatorException(e.getMessage(), e.getCause(), certPath, i);
            }
        }
        if (!set.isEmpty()) {
            throw new ExtCertPathValidatorException("Certificate has unsupported critical extension.", null, certPath, i);
        }
    }

    protected static Set processCRLA1i(Date date, ExtendedPKIXParameters extendedPKIXParameters, X509Certificate x509Certificate, X509CRL x509crl) throws AnnotatedException {
        HashSet hashSet = new HashSet();
        if (extendedPKIXParameters.isUseDeltasEnabled()) {
            try {
                CRLDistPoint instance = CRLDistPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, FRESHEST_CRL));
                if (instance == null) {
                    try {
                        instance = CRLDistPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509crl, FRESHEST_CRL));
                    } catch (AnnotatedException e) {
                        throw new AnnotatedException("Freshest CRL extension could not be decoded from CRL.", e);
                    }
                }
                if (instance != null) {
                    try {
                        CertPathValidatorUtilities.addAdditionalStoresFromCRLDistributionPoint(instance, extendedPKIXParameters);
                        try {
                            hashSet.addAll(CertPathValidatorUtilities.getDeltaCRLs(date, extendedPKIXParameters, x509crl));
                        } catch (AnnotatedException e2) {
                            throw new AnnotatedException("Exception obtaining delta CRLs.", e2);
                        }
                    } catch (AnnotatedException e3) {
                        throw new AnnotatedException("No new delta CRL locations could be added from Freshest CRL extension.", e3);
                    }
                }
            } catch (AnnotatedException e4) {
                throw new AnnotatedException("Freshest CRL extension could not be decoded from certificate.", e4);
            }
        }
        return hashSet;
    }

    protected static Set[] processCRLA1ii(Date date, ExtendedPKIXParameters extendedPKIXParameters, X509Certificate x509Certificate, X509CRL x509crl) throws AnnotatedException {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        X509CRLStoreSelector x509CRLStoreSelector = new X509CRLStoreSelector();
        x509CRLStoreSelector.setCertificateChecking(x509Certificate);
        if (extendedPKIXParameters.getDate() != null) {
            x509CRLStoreSelector.setDateAndTime(extendedPKIXParameters.getDate());
        } else {
            x509CRLStoreSelector.setDateAndTime(date);
        }
        try {
            x509CRLStoreSelector.addIssuerName(x509crl.getIssuerX500Principal().getEncoded());
            x509CRLStoreSelector.setCompleteCRLEnabled(true);
            try {
                hashSet.addAll(CertPathValidatorUtilities.findCRLs(x509CRLStoreSelector, extendedPKIXParameters.getAdditionalStores()));
                hashSet.addAll(CertPathValidatorUtilities.findCRLs(x509CRLStoreSelector, extendedPKIXParameters.getStores()));
                hashSet.addAll(CertPathValidatorUtilities.findCRLs(x509CRLStoreSelector, extendedPKIXParameters.getCertStores()));
                if (extendedPKIXParameters.isUseDeltasEnabled()) {
                    try {
                        hashSet2.addAll(CertPathValidatorUtilities.getDeltaCRLs(date, extendedPKIXParameters, x509crl));
                    } catch (AnnotatedException e) {
                        throw new AnnotatedException("Exception obtaining delta CRLs.", e);
                    }
                }
                return new Set[]{hashSet, hashSet2};
            } catch (AnnotatedException e2) {
                throw new AnnotatedException("Exception obtaining complete CRLs.", e2);
            }
        } catch (IOException e3) {
            throw new AnnotatedException("Cannot extract issuer from CRL." + e3, e3);
        }
    }

    protected static void processCRLB1(DistributionPoint distributionPoint, Object obj, X509CRL x509crl) throws AnnotatedException {
        boolean z;
        DERObject extensionValue = CertPathValidatorUtilities.getExtensionValue(x509crl, ISSUING_DISTRIBUTION_POINT);
        boolean z2 = extensionValue != null && IssuingDistributionPoint.getInstance(extensionValue).isIndirectCRL();
        byte[] encoded = CertPathValidatorUtilities.getIssuerPrincipal(x509crl).getEncoded();
        if (distributionPoint.getCRLIssuer() != null) {
            GeneralName[] names = distributionPoint.getCRLIssuer().getNames();
            boolean z3 = false;
            for (int i = 0; i < names.length; i++) {
                if (names[i].getTagNo() == 4) {
                    try {
                        if (Arrays.areEqual(names[i].getName().getDERObject().getEncoded(), encoded)) {
                            z3 = true;
                        }
                    } catch (IOException e) {
                        throw new AnnotatedException("CRL issuer information from distribution point cannot be decoded.", e);
                    }
                }
            }
            if (z3 && !z2) {
                throw new AnnotatedException("Distribution point contains cRLIssuer field but CRL is not indirect.");
            } else if (!z3) {
                throw new AnnotatedException("CRL issuer of CRL does not match CRL issuer of distribution point.");
            } else {
                z = z3;
            }
        } else {
            z = CertPathValidatorUtilities.getIssuerPrincipal(x509crl).equals(CertPathValidatorUtilities.getEncodedIssuerPrincipal(obj));
        }
        if (!z) {
            throw new AnnotatedException("Cannot find matching CRL issuer for certificate.");
        }
    }

    protected static void processCRLB2(DistributionPoint distributionPoint, Object obj, X509CRL x509crl) throws AnnotatedException {
        boolean z;
        GeneralName[] generalNameArr;
        boolean z2;
        try {
            IssuingDistributionPoint instance = IssuingDistributionPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509crl, ISSUING_DISTRIBUTION_POINT));
            if (instance != null) {
                if (instance.getDistributionPoint() != null) {
                    DistributionPointName distributionPoint2 = IssuingDistributionPoint.getInstance(instance).getDistributionPoint();
                    ArrayList arrayList = new ArrayList();
                    if (distributionPoint2.getType() == 0) {
                        GeneralName[] names = GeneralNames.getInstance(distributionPoint2.getName()).getNames();
                        for (GeneralName add : names) {
                            arrayList.add(add);
                        }
                    }
                    if (distributionPoint2.getType() == 1) {
                        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
                        try {
                            Enumeration objects = ASN1Sequence.getInstance(ASN1Sequence.fromByteArray(CertPathValidatorUtilities.getIssuerPrincipal(x509crl).getEncoded())).getObjects();
                            while (objects.hasMoreElements()) {
                                aSN1EncodableVector.add((DEREncodable) objects.nextElement());
                            }
                            aSN1EncodableVector.add(distributionPoint2.getName());
                            arrayList.add(new GeneralName(X509Name.getInstance(new DERSequence(aSN1EncodableVector))));
                        } catch (IOException e) {
                            throw new AnnotatedException("Could not read CRL issuer.", e);
                        }
                    }
                    if (distributionPoint.getDistributionPoint() != null) {
                        DistributionPointName distributionPoint3 = distributionPoint.getDistributionPoint();
                        GeneralName[] generalNameArr2 = null;
                        if (distributionPoint3.getType() == 0) {
                            generalNameArr2 = GeneralNames.getInstance(distributionPoint3.getName()).getNames();
                        }
                        if (distributionPoint3.getType() == 1) {
                            if (distributionPoint.getCRLIssuer() != null) {
                                generalNameArr = distributionPoint.getCRLIssuer().getNames();
                            } else {
                                generalNameArr = new GeneralName[1];
                                try {
                                    generalNameArr[0] = new GeneralName(new X509Name((ASN1Sequence) ASN1Sequence.fromByteArray(CertPathValidatorUtilities.getEncodedIssuerPrincipal(obj).getEncoded())));
                                } catch (IOException e2) {
                                    throw new AnnotatedException("Could not read certificate issuer.", e2);
                                }
                            }
                            for (int i = 0; i < generalNameArr.length; i++) {
                                Enumeration objects2 = ASN1Sequence.getInstance(generalNameArr[i].getName().getDERObject()).getObjects();
                                ASN1EncodableVector aSN1EncodableVector2 = new ASN1EncodableVector();
                                while (objects2.hasMoreElements()) {
                                    aSN1EncodableVector2.add((DEREncodable) objects2.nextElement());
                                }
                                aSN1EncodableVector2.add(distributionPoint3.getName());
                                generalNameArr[i] = new GeneralName(new X509Name(new DERSequence(aSN1EncodableVector2)));
                            }
                        }
                        GeneralName[] generalNameArr3 = generalNameArr;
                        if (generalNameArr3 != null) {
                            int i2 = 0;
                            while (true) {
                                if (i2 >= generalNameArr3.length) {
                                    break;
                                } else if (arrayList.contains(generalNameArr3[i2])) {
                                    z2 = true;
                                    break;
                                } else {
                                    i2++;
                                }
                            }
                        }
                        z2 = false;
                        if (!z2) {
                            throw new AnnotatedException("No match for certificate CRL issuing distribution point name to cRLIssuer CRL distribution point.");
                        }
                    } else if (distributionPoint.getCRLIssuer() == null) {
                        throw new AnnotatedException("Either the cRLIssuer or the distributionPoint field must be contained in DistributionPoint.");
                    } else {
                        GeneralName[] names2 = distributionPoint.getCRLIssuer().getNames();
                        int i3 = 0;
                        while (true) {
                            if (i3 >= names2.length) {
                                z = false;
                                break;
                            } else if (arrayList.contains(names2[i3])) {
                                z = true;
                                break;
                            } else {
                                i3++;
                            }
                        }
                        if (!z) {
                            throw new AnnotatedException("No match for certificate CRL issuing distribution point name to cRLIssuer CRL distribution point.");
                        }
                    }
                }
                try {
                    BasicConstraints instance2 = BasicConstraints.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Extension) obj, BASIC_CONSTRAINTS));
                    if (obj instanceof X509Certificate) {
                        if (instance.onlyContainsUserCerts() && instance2 != null && instance2.isCA()) {
                            throw new AnnotatedException("CA Cert CRL only contains user certificates.");
                        } else if (instance.onlyContainsCACerts() && (instance2 == null || !instance2.isCA())) {
                            throw new AnnotatedException("End CRL only contains CA certificates.");
                        }
                    }
                    if (instance.onlyContainsAttributeCerts()) {
                        throw new AnnotatedException("onlyContainsAttributeCerts boolean is asserted.");
                    }
                } catch (Exception e3) {
                    throw new AnnotatedException("Basic constraints extension could not be decoded.", e3);
                }
            }
        } catch (Exception e4) {
            throw new AnnotatedException("Issuing distribution point extension could not be decoded.", e4);
        }
    }

    protected static void processCRLC(X509CRL x509crl, X509CRL x509crl2, ExtendedPKIXParameters extendedPKIXParameters) throws AnnotatedException {
        boolean z;
        if (x509crl != null) {
            try {
                IssuingDistributionPoint instance = IssuingDistributionPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509crl2, ISSUING_DISTRIBUTION_POINT));
                if (!extendedPKIXParameters.isUseDeltasEnabled()) {
                    return;
                }
                if (!x509crl.getIssuerX500Principal().equals(x509crl2.getIssuerX500Principal())) {
                    throw new AnnotatedException("Complete CRL issuer does not match delta CRL issuer.");
                }
                try {
                    IssuingDistributionPoint instance2 = IssuingDistributionPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509crl, ISSUING_DISTRIBUTION_POINT));
                    if (instance == null) {
                        if (instance2 == null) {
                            z = true;
                        }
                        z = false;
                    } else {
                        if (instance.equals(instance2)) {
                            z = true;
                        }
                        z = false;
                    }
                    if (!z) {
                        throw new AnnotatedException("Issuing distribution point extension from delta CRL and complete CRL does not match.");
                    }
                    try {
                        DERObject extensionValue = CertPathValidatorUtilities.getExtensionValue(x509crl2, AUTHORITY_KEY_IDENTIFIER);
                        try {
                            DERObject extensionValue2 = CertPathValidatorUtilities.getExtensionValue(x509crl, AUTHORITY_KEY_IDENTIFIER);
                            if (extensionValue == null) {
                                throw new AnnotatedException("CRL authority key identifier is null.");
                            } else if (extensionValue2 == null) {
                                throw new AnnotatedException("Delta CRL authority key identifier is null.");
                            } else if (!extensionValue.equals(extensionValue2)) {
                                throw new AnnotatedException("Delta CRL authority key identifier does not match complete CRL authority key identifier.");
                            }
                        } catch (AnnotatedException e) {
                            throw new AnnotatedException("Authority key identifier extension could not be extracted from delta CRL.", e);
                        }
                    } catch (AnnotatedException e2) {
                        throw new AnnotatedException("Authority key identifier extension could not be extracted from complete CRL.", e2);
                    }
                } catch (Exception e3) {
                    throw new AnnotatedException("Issuing distribution point extension from delta CRL could not be decoded.", e3);
                }
            } catch (Exception e4) {
                throw new AnnotatedException("Issuing distribution point extension could not be decoded.", e4);
            }
        }
    }

    protected static ReasonsMask processCRLD(X509CRL x509crl, DistributionPoint distributionPoint) throws AnnotatedException {
        try {
            IssuingDistributionPoint instance = IssuingDistributionPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509crl, ISSUING_DISTRIBUTION_POINT));
            if (instance != null && instance.getOnlySomeReasons() != null && distributionPoint.getReasons() != null) {
                return new ReasonsMask(distributionPoint.getReasons().intValue()).intersect(new ReasonsMask(instance.getOnlySomeReasons().intValue()));
            }
            if ((instance == null || instance.getOnlySomeReasons() == null) && distributionPoint.getReasons() == null) {
                return ReasonsMask.allReasons;
            }
            return (distributionPoint.getReasons() == null ? ReasonsMask.allReasons : new ReasonsMask(distributionPoint.getReasons().intValue())).intersect(instance == null ? ReasonsMask.allReasons : new ReasonsMask(instance.getOnlySomeReasons().intValue()));
        } catch (Exception e) {
            throw new AnnotatedException("Issuing distribution point extension could not be decoded.", e);
        }
    }

    protected static Set processCRLF(X509CRL x509crl, Object obj, X509Certificate x509Certificate, PublicKey publicKey, ExtendedPKIXParameters extendedPKIXParameters, List list) throws AnnotatedException {
        X509CertStoreSelector x509CertStoreSelector = new X509CertStoreSelector();
        try {
            x509CertStoreSelector.setSubject(CertPathValidatorUtilities.getIssuerPrincipal(x509crl).getEncoded());
            try {
                Collection<X509Certificate> findCertificates = CertPathValidatorUtilities.findCertificates(x509CertStoreSelector, extendedPKIXParameters.getStores());
                findCertificates.addAll(CertPathValidatorUtilities.findCertificates(x509CertStoreSelector, extendedPKIXParameters.getAdditionalStores()));
                findCertificates.addAll(CertPathValidatorUtilities.findCertificates(x509CertStoreSelector, extendedPKIXParameters.getCertStores()));
                findCertificates.add(x509Certificate);
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                for (X509Certificate x509Certificate2 : findCertificates) {
                    if (x509Certificate2.equals(x509Certificate)) {
                        arrayList.add(x509Certificate2);
                        arrayList2.add(publicKey);
                    } else {
                        try {
                            CertPathBuilder instance = CertPathBuilder.getInstance("PKIX", "BC");
                            X509CertStoreSelector x509CertStoreSelector2 = new X509CertStoreSelector();
                            x509CertStoreSelector2.setCertificate(x509Certificate2);
                            ExtendedPKIXParameters extendedPKIXParameters2 = (ExtendedPKIXParameters) extendedPKIXParameters.clone();
                            extendedPKIXParameters2.setTargetCertConstraints(x509CertStoreSelector2);
                            ExtendedPKIXBuilderParameters extendedPKIXBuilderParameters = (ExtendedPKIXBuilderParameters) ExtendedPKIXBuilderParameters.getInstance(extendedPKIXParameters2);
                            if (list.contains(x509Certificate2)) {
                                extendedPKIXBuilderParameters.setRevocationEnabled(false);
                            } else {
                                extendedPKIXBuilderParameters.setRevocationEnabled(true);
                            }
                            List<? extends Certificate> certificates = instance.build(extendedPKIXBuilderParameters).getCertPath().getCertificates();
                            arrayList.add(x509Certificate2);
                            arrayList2.add(CertPathValidatorUtilities.getNextWorkingKey(certificates, 0));
                        } catch (CertPathBuilderException e) {
                            throw new AnnotatedException("Internal error.", e);
                        } catch (CertPathValidatorException e2) {
                            throw new AnnotatedException("Public key of issuer certificate of CRL could not be retrieved.", e2);
                        } catch (Exception e3) {
                            throw new RuntimeException(e3.getMessage());
                        }
                    }
                }
                HashSet hashSet = new HashSet();
                AnnotatedException annotatedException = null;
                for (int i = 0; i < arrayList.size(); i++) {
                    boolean[] keyUsage = ((X509Certificate) arrayList.get(i)).getKeyUsage();
                    if (keyUsage == null || (keyUsage.length >= 7 && keyUsage[6])) {
                        hashSet.add(arrayList2.get(i));
                    } else {
                        annotatedException = new AnnotatedException("Issuer certificate key usage extension does not permit CRL signing.");
                    }
                }
                if (hashSet.isEmpty() && annotatedException == null) {
                    throw new AnnotatedException("Cannot find a valid issuer certificate.");
                } else if (!hashSet.isEmpty() || annotatedException == null) {
                    return hashSet;
                } else {
                    throw annotatedException;
                }
            } catch (AnnotatedException e4) {
                throw new AnnotatedException("Issuer certificate for CRL cannot be searched.", e4);
            }
        } catch (IOException e5) {
            throw new AnnotatedException("Subject criteria for certificate selector to find issuer certificate for CRL could not be set.", e5);
        }
    }

    protected static PublicKey processCRLG(X509CRL x509crl, Set set) throws AnnotatedException {
        Exception e = null;
        Iterator it = set.iterator();
        while (it.hasNext()) {
            PublicKey publicKey = (PublicKey) it.next();
            try {
                x509crl.verify(publicKey);
                return publicKey;
            } catch (Exception e2) {
                e = e2;
            }
        }
        throw new AnnotatedException("Cannot verify CRL.", e);
    }

    protected static X509CRL processCRLH(Set set, PublicKey publicKey) throws AnnotatedException {
        Iterator it = set.iterator();
        Exception e = null;
        while (it.hasNext()) {
            X509CRL x509crl = (X509CRL) it.next();
            try {
                x509crl.verify(publicKey);
                return x509crl;
            } catch (Exception e2) {
                e = e2;
            }
        }
        if (e == null) {
            return null;
        }
        throw new AnnotatedException("Cannot verify delta CRL.", e);
    }

    protected static void processCRLI(Date date, X509CRL x509crl, Object obj, CertStatus certStatus, ExtendedPKIXParameters extendedPKIXParameters) throws AnnotatedException {
        if (extendedPKIXParameters.isUseDeltasEnabled() && x509crl != null) {
            CertPathValidatorUtilities.getCertStatus(date, x509crl, obj, certStatus);
        }
    }

    protected static void processCRLJ(Date date, X509CRL x509crl, Object obj, CertStatus certStatus) throws AnnotatedException {
        if (certStatus.getCertStatus() == 11) {
            CertPathValidatorUtilities.getCertStatus(date, x509crl, obj, certStatus);
        }
    }

    protected static void processCertA(CertPath certPath, ExtendedPKIXParameters extendedPKIXParameters, int i, PublicKey publicKey, boolean z, X500Principal x500Principal, X509Certificate x509Certificate) throws ExtCertPathValidatorException {
        List<? extends Certificate> certificates = certPath.getCertificates();
        X509Certificate x509Certificate2 = (X509Certificate) certificates.get(i);
        if (!z) {
            try {
                CertPathValidatorUtilities.verifyX509Certificate(x509Certificate2, publicKey, extendedPKIXParameters.getSigProvider());
            } catch (GeneralSecurityException e) {
                throw new ExtCertPathValidatorException("Could not validate certificate signature.", e, certPath, i);
            }
        }
        try {
            x509Certificate2.checkValidity(CertPathValidatorUtilities.getValidCertDateFromValidityModel(extendedPKIXParameters, certPath, i));
            if (extendedPKIXParameters.isRevocationEnabled()) {
                try {
                    checkCRLs(extendedPKIXParameters, x509Certificate2, CertPathValidatorUtilities.getValidCertDateFromValidityModel(extendedPKIXParameters, certPath, i), x509Certificate, publicKey, certificates);
                } catch (AnnotatedException e2) {
                    throw new ExtCertPathValidatorException(e2.getMessage(), e2.getCause(), certPath, i);
                }
            }
            if (!CertPathValidatorUtilities.getEncodedIssuerPrincipal(x509Certificate2).equals(x500Principal)) {
                throw new ExtCertPathValidatorException("IssuerName(" + CertPathValidatorUtilities.getEncodedIssuerPrincipal(x509Certificate2) + ") does not match SubjectName(" + x500Principal + ") of signing certificate.", null, certPath, i);
            }
        } catch (CertificateExpiredException e3) {
            throw new ExtCertPathValidatorException("Could not validate certificate: " + e3.getMessage(), e3, certPath, i);
        } catch (CertificateNotYetValidException e4) {
            throw new ExtCertPathValidatorException("Could not validate certificate: " + e4.getMessage(), e4, certPath, i);
        } catch (AnnotatedException e5) {
            throw new ExtCertPathValidatorException("Could not validate time of certificate.", e5, certPath, i);
        }
    }

    protected static void processCertBC(CertPath certPath, int i, PKIXNameConstraintValidator pKIXNameConstraintValidator) throws CertPathValidatorException {
        List<? extends Certificate> certificates = certPath.getCertificates();
        X509Certificate x509Certificate = (X509Certificate) certificates.get(i);
        int size = certificates.size();
        int i2 = size - i;
        if (!CertPathValidatorUtilities.isSelfIssued(x509Certificate) || i2 >= size) {
            try {
                ASN1Sequence instance = DERSequence.getInstance(new ASN1InputStream(CertPathValidatorUtilities.getSubjectPrincipal(x509Certificate).getEncoded()).readObject());
                try {
                    pKIXNameConstraintValidator.checkPermittedDN(instance);
                    pKIXNameConstraintValidator.checkExcludedDN(instance);
                    try {
                        GeneralNames instance2 = GeneralNames.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, SUBJECT_ALTERNATIVE_NAME));
                        Enumeration elements = new X509Name(instance).getValues(X509Name.EmailAddress).elements();
                        while (elements.hasMoreElements()) {
                            GeneralName generalName = new GeneralName(1, (String) elements.nextElement());
                            try {
                                pKIXNameConstraintValidator.checkPermitted(generalName);
                                pKIXNameConstraintValidator.checkExcluded(generalName);
                            } catch (PKIXNameConstraintValidatorException e) {
                                throw new CertPathValidatorException("Subtree check for certificate subject alternative email failed.", e, certPath, i);
                            }
                        }
                        if (instance2 != null) {
                            try {
                                GeneralName[] names = instance2.getNames();
                                int i3 = 0;
                                while (i3 < names.length) {
                                    try {
                                        pKIXNameConstraintValidator.checkPermitted(names[i3]);
                                        pKIXNameConstraintValidator.checkExcluded(names[i3]);
                                        i3++;
                                    } catch (PKIXNameConstraintValidatorException e2) {
                                        throw new CertPathValidatorException("Subtree check for certificate subject alternative name failed.", e2, certPath, i);
                                    }
                                }
                            } catch (Exception e3) {
                                throw new CertPathValidatorException("Subject alternative name contents could not be decoded.", e3, certPath, i);
                            }
                        }
                    } catch (Exception e4) {
                        throw new CertPathValidatorException("Subject alternative name extension could not be decoded.", e4, certPath, i);
                    }
                } catch (PKIXNameConstraintValidatorException e5) {
                    throw new CertPathValidatorException("Subtree check for certificate subject failed.", e5, certPath, i);
                }
            } catch (Exception e6) {
                throw new CertPathValidatorException("Exception extracting subject name when checking subtrees.", e6, certPath, i);
            }
        }
    }

    protected static PKIXPolicyNode processCertD(CertPath certPath, int i, Set set, PKIXPolicyNode pKIXPolicyNode, List[] listArr, int i2) throws CertPathValidatorException {
        PKIXPolicyNode pKIXPolicyNode2;
        String id;
        boolean z;
        List<? extends Certificate> certificates = certPath.getCertificates();
        X509Certificate x509Certificate = (X509Certificate) certificates.get(i);
        int size = certificates.size();
        int i3 = size - i;
        try {
            ASN1Sequence instance = DERSequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, CERTIFICATE_POLICIES));
            if (instance == null || pKIXPolicyNode == null) {
                return null;
            }
            Enumeration objects = instance.getObjects();
            HashSet hashSet = new HashSet();
            while (objects.hasMoreElements()) {
                PolicyInformation instance2 = PolicyInformation.getInstance(objects.nextElement());
                DERObjectIdentifier policyIdentifier = instance2.getPolicyIdentifier();
                hashSet.add(policyIdentifier.getId());
                if (!ANY_POLICY.equals(policyIdentifier.getId())) {
                    try {
                        Set qualifierSet = CertPathValidatorUtilities.getQualifierSet(instance2.getPolicyQualifiers());
                        if (!CertPathValidatorUtilities.processCertD1i(i3, listArr, policyIdentifier, qualifierSet)) {
                            CertPathValidatorUtilities.processCertD1ii(i3, listArr, policyIdentifier, qualifierSet);
                        }
                    } catch (CertPathValidatorException e) {
                        throw new ExtCertPathValidatorException("Policy qualifier info set could not be build.", e, certPath, i);
                    }
                }
            }
            if (set.isEmpty() || set.contains(ANY_POLICY)) {
                set.clear();
                set.addAll(hashSet);
            } else {
                HashSet hashSet2 = new HashSet();
                for (Object next : set) {
                    if (hashSet.contains(next)) {
                        hashSet2.add(next);
                    }
                }
                set.clear();
                set.addAll(hashSet2);
            }
            if (i2 > 0 || (i3 < size && CertPathValidatorUtilities.isSelfIssued(x509Certificate))) {
                Enumeration objects2 = instance.getObjects();
                while (true) {
                    if (!objects2.hasMoreElements()) {
                        break;
                    }
                    PolicyInformation instance3 = PolicyInformation.getInstance(objects2.nextElement());
                    if (ANY_POLICY.equals(instance3.getPolicyIdentifier().getId())) {
                        Set qualifierSet2 = CertPathValidatorUtilities.getQualifierSet(instance3.getPolicyQualifiers());
                        List list = listArr[i3 - 1];
                        int i4 = 0;
                        while (true) {
                            int i5 = i4;
                            if (i5 >= list.size()) {
                                break;
                            }
                            PKIXPolicyNode pKIXPolicyNode3 = (PKIXPolicyNode) list.get(i5);
                            for (Object next2 : pKIXPolicyNode3.getExpectedPolicies()) {
                                if (next2 instanceof String) {
                                    id = (String) next2;
                                } else if (next2 instanceof DERObjectIdentifier) {
                                    id = ((DERObjectIdentifier) next2).getId();
                                }
                                boolean z2 = false;
                                Iterator children = pKIXPolicyNode3.getChildren();
                                while (true) {
                                    z = z2;
                                    if (!children.hasNext()) {
                                        break;
                                    }
                                    z2 = id.equals(((PKIXPolicyNode) children.next()).getValidPolicy()) ? true : z;
                                }
                                if (!z) {
                                    HashSet hashSet3 = new HashSet();
                                    hashSet3.add(id);
                                    PKIXPolicyNode pKIXPolicyNode4 = new PKIXPolicyNode(new ArrayList(), i3, hashSet3, pKIXPolicyNode3, qualifierSet2, id, false);
                                    pKIXPolicyNode3.addChild(pKIXPolicyNode4);
                                    listArr[i3].add(pKIXPolicyNode4);
                                }
                            }
                            i4 = i5 + 1;
                        }
                    }
                }
            }
            int i6 = i3 - 1;
            PKIXPolicyNode pKIXPolicyNode5 = pKIXPolicyNode;
            while (i6 >= 0) {
                List list2 = listArr[i6];
                PKIXPolicyNode pKIXPolicyNode6 = pKIXPolicyNode5;
                int i7 = 0;
                while (true) {
                    if (i7 >= list2.size()) {
                        pKIXPolicyNode2 = pKIXPolicyNode6;
                        break;
                    }
                    PKIXPolicyNode pKIXPolicyNode7 = (PKIXPolicyNode) list2.get(i7);
                    if (!pKIXPolicyNode7.hasChildren()) {
                        pKIXPolicyNode2 = CertPathValidatorUtilities.removePolicyNode(pKIXPolicyNode6, listArr, pKIXPolicyNode7);
                        if (pKIXPolicyNode2 == null) {
                            break;
                        }
                    } else {
                        pKIXPolicyNode2 = pKIXPolicyNode6;
                    }
                    i7++;
                    pKIXPolicyNode6 = pKIXPolicyNode2;
                }
                i6--;
                pKIXPolicyNode5 = pKIXPolicyNode2;
            }
            Set<String> criticalExtensionOIDs = x509Certificate.getCriticalExtensionOIDs();
            if (criticalExtensionOIDs != null) {
                boolean contains = criticalExtensionOIDs.contains(CERTIFICATE_POLICIES);
                List list3 = listArr[i3];
                int i8 = 0;
                while (true) {
                    int i9 = i8;
                    if (i9 >= list3.size()) {
                        break;
                    }
                    ((PKIXPolicyNode) list3.get(i9)).setCritical(contains);
                    i8 = i9 + 1;
                }
            }
            return pKIXPolicyNode5;
        } catch (AnnotatedException e2) {
            throw new ExtCertPathValidatorException("Could not read certificate policies extension from certificate.", e2, certPath, i);
        }
    }

    protected static PKIXPolicyNode processCertE(CertPath certPath, int i, PKIXPolicyNode pKIXPolicyNode) throws CertPathValidatorException {
        try {
            if (DERSequence.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Certificate) certPath.getCertificates().get(i), CERTIFICATE_POLICIES)) == null) {
                return null;
            }
            return pKIXPolicyNode;
        } catch (AnnotatedException e) {
            throw new ExtCertPathValidatorException("Could not read certificate policies extension from certificate.", e, certPath, i);
        }
    }

    protected static void processCertF(CertPath certPath, int i, PKIXPolicyNode pKIXPolicyNode, int i2) throws CertPathValidatorException {
        if (i2 <= 0 && pKIXPolicyNode == null) {
            throw new ExtCertPathValidatorException("No valid policy tree found when one expected.", null, certPath, i);
        }
    }

    protected static int wrapupCertA(int i, X509Certificate x509Certificate) {
        return (CertPathValidatorUtilities.isSelfIssued(x509Certificate) || i == 0) ? i : i - 1;
    }

    protected static int wrapupCertB(CertPath certPath, int i, int i2) throws CertPathValidatorException {
        try {
            ASN1Sequence instance = DERSequence.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Certificate) certPath.getCertificates().get(i), POLICY_CONSTRAINTS));
            if (instance != null) {
                Enumeration objects = instance.getObjects();
                while (objects.hasMoreElements()) {
                    ASN1TaggedObject aSN1TaggedObject = (ASN1TaggedObject) objects.nextElement();
                    switch (aSN1TaggedObject.getTagNo()) {
                        case 0:
                            try {
                                if (DERInteger.getInstance(aSN1TaggedObject).getValue().intValue() != 0) {
                                    break;
                                } else {
                                    return 0;
                                }
                            } catch (Exception e) {
                                throw new ExtCertPathValidatorException("Policy constraints requireExplicitPolicy field could no be decoded.", e, certPath, i);
                            }
                    }
                }
            }
            return i2;
        } catch (AnnotatedException e2) {
            throw new ExtCertPathValidatorException("Policy constraints could no be decoded.", e2, certPath, i);
        }
    }

    protected static void wrapupCertF(CertPath certPath, int i, List list, Set set) throws CertPathValidatorException {
        X509Certificate x509Certificate = (X509Certificate) certPath.getCertificates().get(i);
        Iterator it = list.iterator();
        while (it.hasNext()) {
            try {
                ((PKIXCertPathChecker) it.next()).check(x509Certificate, set);
            } catch (CertPathValidatorException e) {
                throw new ExtCertPathValidatorException("Additional certificate path checker failed.", e, certPath, i);
            }
        }
        if (!set.isEmpty()) {
            throw new ExtCertPathValidatorException("Certificate has unsupported critical extension", null, certPath, i);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static org.bouncycastle.jce.provider.PKIXPolicyNode wrapupCertG(java.security.cert.CertPath r5, org.bouncycastle.x509.ExtendedPKIXParameters r6, java.util.Set r7, int r8, java.util.List[] r9, org.bouncycastle.jce.provider.PKIXPolicyNode r10, java.util.Set r11) throws java.security.cert.CertPathValidatorException {
        /*
            java.util.List r0 = r5.getCertificates()
            int r0 = r0.size()
            if (r10 != 0) goto L_0x001b
            boolean r6 = r6.isExplicitPolicyRequired()
            if (r6 == 0) goto L_0x0019
            org.bouncycastle.jce.exception.ExtCertPathValidatorException r6 = new org.bouncycastle.jce.exception.ExtCertPathValidatorException
            java.lang.String r7 = "Explicit policy requested but none available."
            r9 = 0
            r6.<init>(r7, r9, r5, r8)
            throw r6
        L_0x0019:
            r5 = 0
        L_0x001a:
            return r5
        L_0x001b:
            boolean r1 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.isAnyPolicy(r7)
            if (r1 == 0) goto L_0x00bd
            boolean r6 = r6.isExplicitPolicyRequired()
            if (r6 == 0) goto L_0x0162
            boolean r6 = r11.isEmpty()
            if (r6 == 0) goto L_0x0036
            org.bouncycastle.jce.exception.ExtCertPathValidatorException r6 = new org.bouncycastle.jce.exception.ExtCertPathValidatorException
            java.lang.String r7 = "Explicit policy requested but none available."
            r9 = 0
            r6.<init>(r7, r9, r5, r8)
            throw r6
        L_0x0036:
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            r5 = 0
            r7 = r5
        L_0x003d:
            int r5 = r9.length
            if (r7 >= r5) goto L_0x0076
            r8 = r9[r7]
            r5 = 0
            r1 = r5
        L_0x0044:
            int r5 = r8.size()
            if (r1 >= r5) goto L_0x0072
            java.lang.Object r5 = r8.get(r1)
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r5
            java.lang.String r2 = "2.5.29.32.0"
            java.lang.String r3 = r5.getValidPolicy()
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x006e
            java.util.Iterator r5 = r5.getChildren()
        L_0x0060:
            boolean r2 = r5.hasNext()
            if (r2 == 0) goto L_0x006e
            java.lang.Object r2 = r5.next()
            r6.add(r2)
            goto L_0x0060
        L_0x006e:
            int r5 = r1 + 1
            r1 = r5
            goto L_0x0044
        L_0x0072:
            int r5 = r7 + 1
            r7 = r5
            goto L_0x003d
        L_0x0076:
            java.util.Iterator r6 = r6.iterator()
        L_0x007a:
            boolean r5 = r6.hasNext()
            if (r5 == 0) goto L_0x0091
            java.lang.Object r5 = r6.next()
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r5
            java.lang.String r5 = r5.getValidPolicy()
            boolean r5 = r11.contains(r5)
            if (r5 != 0) goto L_0x007a
            goto L_0x007a
        L_0x0091:
            if (r10 == 0) goto L_0x0162
            r5 = 1
            int r5 = r0 - r5
            r6 = r5
            r5 = r10
        L_0x0098:
            if (r6 < 0) goto L_0x001a
            r7 = r9[r6]
            r8 = 0
            r10 = r5
        L_0x009e:
            int r5 = r7.size()
            if (r8 >= r5) goto L_0x00b8
            java.lang.Object r5 = r7.get(r8)
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r5
            boolean r11 = r5.hasChildren()
            if (r11 != 0) goto L_0x015f
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.removePolicyNode(r10, r9, r5)
        L_0x00b4:
            int r8 = r8 + 1
            r10 = r5
            goto L_0x009e
        L_0x00b8:
            int r5 = r6 + -1
            r6 = r5
            r5 = r10
            goto L_0x0098
        L_0x00bd:
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            r5 = 0
            r8 = r5
        L_0x00c4:
            int r5 = r9.length
            if (r8 >= r5) goto L_0x010b
            r11 = r9[r8]
            r5 = 0
            r1 = r5
        L_0x00cb:
            int r5 = r11.size()
            if (r1 >= r5) goto L_0x0107
            java.lang.Object r5 = r11.get(r1)
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r5
            java.lang.String r2 = "2.5.29.32.0"
            java.lang.String r3 = r5.getValidPolicy()
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0103
            java.util.Iterator r2 = r5.getChildren()
        L_0x00e7:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x0103
            java.lang.Object r5 = r2.next()
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r5
            java.lang.String r3 = "2.5.29.32.0"
            java.lang.String r4 = r5.getValidPolicy()
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x00e7
            r6.add(r5)
            goto L_0x00e7
        L_0x0103:
            int r5 = r1 + 1
            r1 = r5
            goto L_0x00cb
        L_0x0107:
            int r5 = r8 + 1
            r8 = r5
            goto L_0x00c4
        L_0x010b:
            java.util.Iterator r6 = r6.iterator()
            r8 = r10
        L_0x0110:
            boolean r5 = r6.hasNext()
            if (r5 == 0) goto L_0x012c
            java.lang.Object r5 = r6.next()
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r5
            java.lang.String r10 = r5.getValidPolicy()
            boolean r10 = r7.contains(r10)
            if (r10 != 0) goto L_0x015d
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.removePolicyNode(r8, r9, r5)
        L_0x012a:
            r8 = r5
            goto L_0x0110
        L_0x012c:
            if (r8 == 0) goto L_0x015a
            r5 = 1
            int r5 = r0 - r5
            r6 = r5
            r5 = r8
        L_0x0133:
            if (r6 < 0) goto L_0x001a
            r7 = r9[r6]
            r8 = 0
            r10 = r5
        L_0x0139:
            int r5 = r7.size()
            if (r8 >= r5) goto L_0x0153
            java.lang.Object r5 = r7.get(r8)
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r5
            boolean r11 = r5.hasChildren()
            if (r11 != 0) goto L_0x0158
            org.bouncycastle.jce.provider.PKIXPolicyNode r5 = org.bouncycastle.jce.provider.CertPathValidatorUtilities.removePolicyNode(r10, r9, r5)
        L_0x014f:
            int r8 = r8 + 1
            r10 = r5
            goto L_0x0139
        L_0x0153:
            int r5 = r6 + -1
            r6 = r5
            r5 = r10
            goto L_0x0133
        L_0x0158:
            r5 = r10
            goto L_0x014f
        L_0x015a:
            r5 = r8
            goto L_0x001a
        L_0x015d:
            r5 = r8
            goto L_0x012a
        L_0x015f:
            r5 = r10
            goto L_0x00b4
        L_0x0162:
            r5 = r10
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.jce.provider.RFC3280CertPathUtilities.wrapupCertG(java.security.cert.CertPath, org.bouncycastle.x509.ExtendedPKIXParameters, java.util.Set, int, java.util.List[], org.bouncycastle.jce.provider.PKIXPolicyNode, java.util.Set):org.bouncycastle.jce.provider.PKIXPolicyNode");
    }
}
