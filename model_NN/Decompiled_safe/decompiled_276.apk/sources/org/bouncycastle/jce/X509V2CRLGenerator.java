package org.bouncycastle.jce;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.SimpleTimeZone;
import java.util.Vector;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DEROutputStream;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERUTCTime;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.asn1.x509.TBSCertList;
import org.bouncycastle.asn1.x509.V2TBSCertListGenerator;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.jce.provider.X509CRLObject;
import org.bouncycastle.util.Strings;

public class X509V2CRLGenerator {
    private static Hashtable algorithms = new Hashtable();
    private SimpleDateFormat dateF = new SimpleDateFormat("yyMMddHHmmss");
    private Vector extOrdering = null;
    private Hashtable extensions = null;
    private AlgorithmIdentifier sigAlgId;
    private DERObjectIdentifier sigOID;
    private String signatureAlgorithm;
    private V2TBSCertListGenerator tbsGen;
    private SimpleTimeZone tz = new SimpleTimeZone(0, "Z");

    static {
        algorithms.put("MD2WITHRSAENCRYPTION", new DERObjectIdentifier("1.2.840.113549.1.1.2"));
        algorithms.put("MD2WITHRSA", new DERObjectIdentifier("1.2.840.113549.1.1.2"));
        algorithms.put("MD5WITHRSAENCRYPTION", new DERObjectIdentifier("1.2.840.113549.1.1.4"));
        algorithms.put("MD5WITHRSA", new DERObjectIdentifier("1.2.840.113549.1.1.4"));
        algorithms.put("SHA1WITHRSAENCRYPTION", new DERObjectIdentifier("1.2.840.113549.1.1.5"));
        algorithms.put("SHA1WITHRSA", new DERObjectIdentifier("1.2.840.113549.1.1.5"));
        algorithms.put("RIPEMD160WITHRSAENCRYPTION", new DERObjectIdentifier("1.3.36.3.3.1.2"));
        algorithms.put("RIPEMD160WITHRSA", new DERObjectIdentifier("1.3.36.3.3.1.2"));
        algorithms.put("SHA1WITHDSA", new DERObjectIdentifier("1.2.840.10040.4.3"));
        algorithms.put("DSAWITHSHA1", new DERObjectIdentifier("1.2.840.10040.4.3"));
        algorithms.put("SHA1WITHECDSA", new DERObjectIdentifier("1.2.840.10045.4.1"));
        algorithms.put("ECDSAWITHSHA1", new DERObjectIdentifier("1.2.840.10045.4.1"));
    }

    public X509V2CRLGenerator() {
        this.dateF.setTimeZone(this.tz);
        this.tbsGen = new V2TBSCertListGenerator();
    }

    public void addCRLEntry(BigInteger bigInteger, Date date, int i) {
        this.tbsGen.addCRLEntry(new DERInteger(bigInteger), new DERUTCTime(this.dateF.format(date) + "Z"), i);
    }

    public void addExtension(String str, boolean z, DEREncodable dEREncodable) {
        addExtension(new DERObjectIdentifier(str), z, dEREncodable);
    }

    public void addExtension(String str, boolean z, byte[] bArr) {
        addExtension(new DERObjectIdentifier(str), z, bArr);
    }

    public void addExtension(DERObjectIdentifier dERObjectIdentifier, boolean z, DEREncodable dEREncodable) {
        if (this.extensions == null) {
            this.extensions = new Hashtable();
            this.extOrdering = new Vector();
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            new DEROutputStream(byteArrayOutputStream).writeObject(dEREncodable);
            addExtension(dERObjectIdentifier, z, byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            throw new IllegalArgumentException("error encoding value: " + e);
        }
    }

    public void addExtension(DERObjectIdentifier dERObjectIdentifier, boolean z, byte[] bArr) {
        if (this.extensions == null) {
            this.extensions = new Hashtable();
            this.extOrdering = new Vector();
        }
        this.extensions.put(dERObjectIdentifier, new X509Extension(z, new DEROctetString(bArr)));
        this.extOrdering.addElement(dERObjectIdentifier);
    }

    public X509CRL generateX509CRL(PrivateKey privateKey) throws SecurityException, SignatureException, InvalidKeyException {
        try {
            return generateX509CRL(privateKey, "BC", null);
        } catch (NoSuchProviderException e) {
            throw new SecurityException("BC provider not installed!");
        }
    }

    public X509CRL generateX509CRL(PrivateKey privateKey, String str) throws NoSuchProviderException, SecurityException, SignatureException, InvalidKeyException {
        return generateX509CRL(privateKey, str, null);
    }

    public X509CRL generateX509CRL(PrivateKey privateKey, String str, SecureRandom secureRandom) throws NoSuchProviderException, SecurityException, SignatureException, InvalidKeyException {
        Signature instance;
        try {
            instance = Signature.getInstance(this.sigOID.getId(), str);
        } catch (NoSuchAlgorithmException e) {
            try {
                instance = Signature.getInstance(this.signatureAlgorithm, str);
            } catch (NoSuchAlgorithmException e2) {
                throw new SecurityException("exception creating signature: " + e2.toString());
            }
        }
        if (secureRandom != null) {
            instance.initSign(privateKey, secureRandom);
        } else {
            instance.initSign(privateKey);
        }
        if (this.extensions != null) {
            this.tbsGen.setExtensions(new X509Extensions(this.extOrdering, this.extensions));
        }
        TBSCertList generateTBSCertList = this.tbsGen.generateTBSCertList();
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            new DEROutputStream(byteArrayOutputStream).writeObject(generateTBSCertList);
            instance.update(byteArrayOutputStream.toByteArray());
            ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
            aSN1EncodableVector.add(generateTBSCertList);
            aSN1EncodableVector.add(this.sigAlgId);
            aSN1EncodableVector.add(new DERBitString(instance.sign()));
            try {
                return new X509CRLObject(new CertificateList(new DERSequence(aSN1EncodableVector)));
            } catch (CRLException e3) {
                throw new IllegalStateException("attempt to create malformed CRL: " + e3.getMessage());
            }
        } catch (Exception e4) {
            throw new SecurityException("exception encoding TBS cert - " + e4);
        }
    }

    public X509CRL generateX509CRL(PrivateKey privateKey, SecureRandom secureRandom) throws SecurityException, SignatureException, InvalidKeyException {
        try {
            return generateX509CRL(privateKey, "BC", secureRandom);
        } catch (NoSuchProviderException e) {
            throw new SecurityException("BC provider not installed!");
        }
    }

    public void reset() {
        this.tbsGen = new V2TBSCertListGenerator();
    }

    public void setIssuerDN(X509Name x509Name) {
        this.tbsGen.setIssuer(x509Name);
    }

    public void setNextUpdate(Date date) {
        this.tbsGen.setNextUpdate(new DERUTCTime(this.dateF.format(date) + "Z"));
    }

    public void setSignatureAlgorithm(String str) {
        this.signatureAlgorithm = str;
        this.sigOID = (DERObjectIdentifier) algorithms.get(Strings.toUpperCase(str));
        if (this.sigOID == null) {
            throw new IllegalArgumentException("Unknown signature type requested");
        }
        this.sigAlgId = new AlgorithmIdentifier(this.sigOID, null);
        this.tbsGen.setSignature(this.sigAlgId);
    }

    public void setThisUpdate(Date date) {
        this.tbsGen.setThisUpdate(new DERUTCTime(this.dateF.format(date) + "Z"));
    }
}
