package org.bouncycastle.jce;

import java.util.Enumeration;
import java.util.Vector;
import org.bouncycastle.asn1.nist.NISTNamedCurves;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.teletrust.TeleTrusTNamedCurves;
import org.bouncycastle.asn1.x9.X962NamedCurves;

public class ECNamedCurveTable {
    private static void addEnumeration(Vector vector, Enumeration enumeration) {
        while (enumeration.hasMoreElements()) {
            vector.addElement(enumeration.nextElement());
        }
    }

    public static Enumeration getNames() {
        Vector vector = new Vector();
        addEnumeration(vector, X962NamedCurves.getNames());
        addEnumeration(vector, SECNamedCurves.getNames());
        addEnumeration(vector, NISTNamedCurves.getNames());
        addEnumeration(vector, TeleTrusTNamedCurves.getNames());
        return vector.elements();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0022, code lost:
        r0 = org.bouncycastle.asn1.teletrust.TeleTrusTNamedCurves.getByName(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        r0 = org.bouncycastle.asn1.sec.SECNamedCurves.getByName(r7);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.bouncycastle.jce.spec.ECNamedCurveParameterSpec getParameterSpec(java.lang.String r7) {
        /*
            org.bouncycastle.asn1.x9.X9ECParameters r0 = org.bouncycastle.asn1.x9.X962NamedCurves.getByName(r7)
            if (r0 != 0) goto L_0x000f
            org.bouncycastle.asn1.DERObjectIdentifier r1 = new org.bouncycastle.asn1.DERObjectIdentifier     // Catch:{ IllegalArgumentException -> 0x005b }
            r1.<init>(r7)     // Catch:{ IllegalArgumentException -> 0x005b }
            org.bouncycastle.asn1.x9.X9ECParameters r0 = org.bouncycastle.asn1.x9.X962NamedCurves.getByOID(r1)     // Catch:{ IllegalArgumentException -> 0x005b }
        L_0x000f:
            if (r0 != 0) goto L_0x0020
            org.bouncycastle.asn1.x9.X9ECParameters r0 = org.bouncycastle.asn1.sec.SECNamedCurves.getByName(r7)
            if (r0 != 0) goto L_0x0020
            org.bouncycastle.asn1.DERObjectIdentifier r1 = new org.bouncycastle.asn1.DERObjectIdentifier     // Catch:{ IllegalArgumentException -> 0x0059 }
            r1.<init>(r7)     // Catch:{ IllegalArgumentException -> 0x0059 }
            org.bouncycastle.asn1.x9.X9ECParameters r0 = org.bouncycastle.asn1.sec.SECNamedCurves.getByOID(r1)     // Catch:{ IllegalArgumentException -> 0x0059 }
        L_0x0020:
            if (r0 != 0) goto L_0x0031
            org.bouncycastle.asn1.x9.X9ECParameters r0 = org.bouncycastle.asn1.teletrust.TeleTrusTNamedCurves.getByName(r7)
            if (r0 != 0) goto L_0x0031
            org.bouncycastle.asn1.DERObjectIdentifier r1 = new org.bouncycastle.asn1.DERObjectIdentifier     // Catch:{ IllegalArgumentException -> 0x0057 }
            r1.<init>(r7)     // Catch:{ IllegalArgumentException -> 0x0057 }
            org.bouncycastle.asn1.x9.X9ECParameters r0 = org.bouncycastle.asn1.teletrust.TeleTrusTNamedCurves.getByOID(r1)     // Catch:{ IllegalArgumentException -> 0x0057 }
        L_0x0031:
            if (r0 != 0) goto L_0x005d
            org.bouncycastle.asn1.x9.X9ECParameters r0 = org.bouncycastle.asn1.nist.NISTNamedCurves.getByName(r7)
            r1 = r0
        L_0x0038:
            if (r1 != 0) goto L_0x003c
            r0 = 0
        L_0x003b:
            return r0
        L_0x003c:
            org.bouncycastle.jce.spec.ECNamedCurveParameterSpec r0 = new org.bouncycastle.jce.spec.ECNamedCurveParameterSpec
            org.bouncycastle.math.ec.ECCurve r2 = r1.getCurve()
            org.bouncycastle.math.ec.ECPoint r3 = r1.getG()
            java.math.BigInteger r4 = r1.getN()
            java.math.BigInteger r5 = r1.getH()
            byte[] r6 = r1.getSeed()
            r1 = r7
            r0.<init>(r1, r2, r3, r4, r5, r6)
            goto L_0x003b
        L_0x0057:
            r1 = move-exception
            goto L_0x0031
        L_0x0059:
            r1 = move-exception
            goto L_0x0020
        L_0x005b:
            r1 = move-exception
            goto L_0x000f
        L_0x005d:
            r1 = r0
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.jce.ECNamedCurveTable.getParameterSpec(java.lang.String):org.bouncycastle.jce.spec.ECNamedCurveParameterSpec");
    }
}
