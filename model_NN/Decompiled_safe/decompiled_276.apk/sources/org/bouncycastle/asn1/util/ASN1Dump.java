package org.bouncycastle.asn1.util;

import java.io.IOException;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.BERApplicationSpecific;
import org.bouncycastle.asn1.BERConstructedOctetString;
import org.bouncycastle.asn1.BERConstructedSequence;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.DERApplicationSpecific;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERBoolean;
import org.bouncycastle.asn1.DERConstructedSequence;
import org.bouncycastle.asn1.DERConstructedSet;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERGeneralizedTime;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERT61String;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DERUTCTime;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.DERUnknownTag;
import org.bouncycastle.asn1.DERVisibleString;
import org.bouncycastle.util.encoders.Hex;

public class ASN1Dump {
    private static final int SAMPLE_SIZE = 32;
    private static final String TAB = "    ";

    static String _dumpAsString(String str, boolean z, DERObject dERObject) {
        String property = System.getProperty("line.separator");
        if (dERObject instanceof ASN1Sequence) {
            StringBuffer stringBuffer = new StringBuffer();
            Enumeration objects = ((ASN1Sequence) dERObject).getObjects();
            String str2 = str + TAB;
            stringBuffer.append(str);
            if (dERObject instanceof BERConstructedSequence) {
                stringBuffer.append("BER ConstructedSequence");
            } else if (dERObject instanceof DERConstructedSequence) {
                stringBuffer.append("DER ConstructedSequence");
            } else if (dERObject instanceof BERSequence) {
                stringBuffer.append("BER Sequence");
            } else if (dERObject instanceof DERSequence) {
                stringBuffer.append("DER Sequence");
            } else {
                stringBuffer.append("Sequence");
            }
            stringBuffer.append(property);
            while (objects.hasMoreElements()) {
                Object nextElement = objects.nextElement();
                if (nextElement == null || nextElement.equals(new DERNull())) {
                    stringBuffer.append(str2);
                    stringBuffer.append("NULL");
                    stringBuffer.append(property);
                } else if (nextElement instanceof DERObject) {
                    stringBuffer.append(_dumpAsString(str2, z, (DERObject) nextElement));
                } else {
                    stringBuffer.append(_dumpAsString(str2, z, ((DEREncodable) nextElement).getDERObject()));
                }
            }
            return stringBuffer.toString();
        } else if (dERObject instanceof DERTaggedObject) {
            StringBuffer stringBuffer2 = new StringBuffer();
            String str3 = str + TAB;
            stringBuffer2.append(str);
            if (dERObject instanceof BERTaggedObject) {
                stringBuffer2.append("BER Tagged [");
            } else {
                stringBuffer2.append("Tagged [");
            }
            DERTaggedObject dERTaggedObject = (DERTaggedObject) dERObject;
            stringBuffer2.append(Integer.toString(dERTaggedObject.getTagNo()));
            stringBuffer2.append(']');
            if (!dERTaggedObject.isExplicit()) {
                stringBuffer2.append(" IMPLICIT ");
            }
            stringBuffer2.append(property);
            if (dERTaggedObject.isEmpty()) {
                stringBuffer2.append(str3);
                stringBuffer2.append("EMPTY");
                stringBuffer2.append(property);
            } else {
                stringBuffer2.append(_dumpAsString(str3, z, dERTaggedObject.getObject()));
            }
            return stringBuffer2.toString();
        } else if (dERObject instanceof DERConstructedSet) {
            StringBuffer stringBuffer3 = new StringBuffer();
            Enumeration objects2 = ((ASN1Set) dERObject).getObjects();
            String str4 = str + TAB;
            stringBuffer3.append(str);
            stringBuffer3.append("ConstructedSet");
            stringBuffer3.append(property);
            while (objects2.hasMoreElements()) {
                Object nextElement2 = objects2.nextElement();
                if (nextElement2 == null) {
                    stringBuffer3.append(str4);
                    stringBuffer3.append("NULL");
                    stringBuffer3.append(property);
                } else if (nextElement2 instanceof DERObject) {
                    stringBuffer3.append(_dumpAsString(str4, z, (DERObject) nextElement2));
                } else {
                    stringBuffer3.append(_dumpAsString(str4, z, ((DEREncodable) nextElement2).getDERObject()));
                }
            }
            return stringBuffer3.toString();
        } else if (dERObject instanceof BERSet) {
            StringBuffer stringBuffer4 = new StringBuffer();
            Enumeration objects3 = ((ASN1Set) dERObject).getObjects();
            String str5 = str + TAB;
            stringBuffer4.append(str);
            stringBuffer4.append("BER Set");
            stringBuffer4.append(property);
            while (objects3.hasMoreElements()) {
                Object nextElement3 = objects3.nextElement();
                if (nextElement3 == null) {
                    stringBuffer4.append(str5);
                    stringBuffer4.append("NULL");
                    stringBuffer4.append(property);
                } else if (nextElement3 instanceof DERObject) {
                    stringBuffer4.append(_dumpAsString(str5, z, (DERObject) nextElement3));
                } else {
                    stringBuffer4.append(_dumpAsString(str5, z, ((DEREncodable) nextElement3).getDERObject()));
                }
            }
            return stringBuffer4.toString();
        } else if (dERObject instanceof DERSet) {
            StringBuffer stringBuffer5 = new StringBuffer();
            Enumeration objects4 = ((ASN1Set) dERObject).getObjects();
            String str6 = str + TAB;
            stringBuffer5.append(str);
            stringBuffer5.append("DER Set");
            stringBuffer5.append(property);
            while (objects4.hasMoreElements()) {
                Object nextElement4 = objects4.nextElement();
                if (nextElement4 == null) {
                    stringBuffer5.append(str6);
                    stringBuffer5.append("NULL");
                    stringBuffer5.append(property);
                } else if (nextElement4 instanceof DERObject) {
                    stringBuffer5.append(_dumpAsString(str6, z, (DERObject) nextElement4));
                } else {
                    stringBuffer5.append(_dumpAsString(str6, z, ((DEREncodable) nextElement4).getDERObject()));
                }
            }
            return stringBuffer5.toString();
        } else if (dERObject instanceof DERObjectIdentifier) {
            return str + "ObjectIdentifier(" + ((DERObjectIdentifier) dERObject).getId() + ")" + property;
        } else {
            if (dERObject instanceof DERBoolean) {
                return str + "Boolean(" + ((DERBoolean) dERObject).isTrue() + ")" + property;
            }
            if (dERObject instanceof DERInteger) {
                return str + "Integer(" + ((DERInteger) dERObject).getValue() + ")" + property;
            }
            if (dERObject instanceof BERConstructedOctetString) {
                ASN1OctetString aSN1OctetString = (ASN1OctetString) dERObject;
                return z ? str + "BER Constructed Octet String" + "[" + aSN1OctetString.getOctets().length + "] " + dumpBinaryDataAsString(str, aSN1OctetString.getOctets()) + property : str + "BER Constructed Octet String" + "[" + aSN1OctetString.getOctets().length + "] " + property;
            } else if (dERObject instanceof DEROctetString) {
                ASN1OctetString aSN1OctetString2 = (ASN1OctetString) dERObject;
                return z ? str + "DER Octet String" + "[" + aSN1OctetString2.getOctets().length + "] " + dumpBinaryDataAsString(str, aSN1OctetString2.getOctets()) + property : str + "DER Octet String" + "[" + aSN1OctetString2.getOctets().length + "] " + property;
            } else if (!(dERObject instanceof DERBitString)) {
                return dERObject instanceof DERIA5String ? str + "IA5String(" + ((DERIA5String) dERObject).getString() + ") " + property : dERObject instanceof DERUTF8String ? str + "UTF8String(" + ((DERUTF8String) dERObject).getString() + ") " + property : dERObject instanceof DERPrintableString ? str + "PrintableString(" + ((DERPrintableString) dERObject).getString() + ") " + property : dERObject instanceof DERVisibleString ? str + "VisibleString(" + ((DERVisibleString) dERObject).getString() + ") " + property : dERObject instanceof DERBMPString ? str + "BMPString(" + ((DERBMPString) dERObject).getString() + ") " + property : dERObject instanceof DERT61String ? str + "T61String(" + ((DERT61String) dERObject).getString() + ") " + property : dERObject instanceof DERUTCTime ? str + "UTCTime(" + ((DERUTCTime) dERObject).getTime() + ") " + property : dERObject instanceof DERGeneralizedTime ? str + "GeneralizedTime(" + ((DERGeneralizedTime) dERObject).getTime() + ") " + property : dERObject instanceof DERUnknownTag ? str + "Unknown " + Integer.toString(((DERUnknownTag) dERObject).getTag(), 16) + " " + new String(Hex.encode(((DERUnknownTag) dERObject).getData())) + property : dERObject instanceof BERApplicationSpecific ? outputApplicationSpecific(ASN1Encodable.BER, str, z, dERObject, property) : dERObject instanceof DERApplicationSpecific ? outputApplicationSpecific(ASN1Encodable.DER, str, z, dERObject, property) : str + dERObject.toString() + property;
            } else {
                DERBitString dERBitString = (DERBitString) dERObject;
                return z ? str + "DER Bit String" + "[" + dERBitString.getBytes().length + ", " + dERBitString.getPadBits() + "] " + dumpBinaryDataAsString(str, dERBitString.getBytes()) + property : str + "DER Bit String" + "[" + dERBitString.getBytes().length + ", " + dERBitString.getPadBits() + "] " + property;
            }
        }
    }

    private static String calculateAscString(byte[] bArr, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i3 = i; i3 != i + i2; i3++) {
            if (bArr[i3] >= 32 && bArr[i3] <= 126) {
                stringBuffer.append((char) bArr[i3]);
            }
        }
        return stringBuffer.toString();
    }

    public static String dumpAsString(Object obj) {
        return dumpAsString(obj, false);
    }

    public static String dumpAsString(Object obj, boolean z) {
        return obj instanceof DERObject ? _dumpAsString("", z, (DERObject) obj) : obj instanceof DEREncodable ? _dumpAsString("", z, ((DEREncodable) obj).getDERObject()) : "unknown object type " + obj.toString();
    }

    private static String dumpBinaryDataAsString(String str, byte[] bArr) {
        String property = System.getProperty("line.separator");
        StringBuffer stringBuffer = new StringBuffer();
        String str2 = str + TAB;
        stringBuffer.append(property);
        for (int i = 0; i < bArr.length; i += 32) {
            if (bArr.length - i > 32) {
                stringBuffer.append(str2);
                stringBuffer.append(new String(Hex.encode(bArr, i, 32)));
                stringBuffer.append(TAB);
                stringBuffer.append(calculateAscString(bArr, i, 32));
                stringBuffer.append(property);
            } else {
                stringBuffer.append(str2);
                stringBuffer.append(new String(Hex.encode(bArr, i, bArr.length - i)));
                for (int length = bArr.length - i; length != 32; length++) {
                    stringBuffer.append("  ");
                }
                stringBuffer.append(TAB);
                stringBuffer.append(calculateAscString(bArr, i, bArr.length - i));
                stringBuffer.append(property);
            }
        }
        return stringBuffer.toString();
    }

    private static String outputApplicationSpecific(String str, String str2, boolean z, DERObject dERObject, String str3) {
        DERApplicationSpecific dERApplicationSpecific = (DERApplicationSpecific) dERObject;
        StringBuffer stringBuffer = new StringBuffer();
        if (!dERApplicationSpecific.isConstructed()) {
            return str2 + str + " ApplicationSpecific[" + dERApplicationSpecific.getApplicationTag() + "] (" + new String(Hex.encode(dERApplicationSpecific.getContents())) + ")" + str3;
        }
        try {
            ASN1Sequence instance = ASN1Sequence.getInstance(dERApplicationSpecific.getObject(16));
            stringBuffer.append(str2 + str + " ApplicationSpecific[" + dERApplicationSpecific.getApplicationTag() + "]" + str3);
            Enumeration objects = instance.getObjects();
            while (objects.hasMoreElements()) {
                stringBuffer.append(_dumpAsString(str2 + TAB, z, (DERObject) objects.nextElement()));
            }
        } catch (IOException e) {
            stringBuffer.append(e);
        }
        return stringBuffer.toString();
    }
}
