package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class BERInputStream extends DERInputStream {
    private static final DERObject END_OF_STREAM = new DERObject() {
        /* access modifiers changed from: package-private */
        public void encode(DEROutputStream dEROutputStream) throws IOException {
            throw new IOException("Eeek!");
        }

        public boolean equals(Object obj) {
            return obj == this;
        }

        public int hashCode() {
            return 0;
        }
    };

    public BERInputStream(InputStream inputStream) {
        super(inputStream);
    }

    private BERConstructedOctetString buildConstructedOctetString() throws IOException {
        Vector vector = new Vector();
        while (true) {
            DERObject readObject = readObject();
            if (readObject == END_OF_STREAM) {
                return new BERConstructedOctetString(vector);
            }
            vector.addElement(readObject);
        }
    }

    private byte[] readIndefiniteLengthFully() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int read = read();
        while (true) {
            int read2 = read();
            if (read2 >= 0 && (read != 0 || read2 != 0)) {
                byteArrayOutputStream.write(read);
                read = read2;
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    public DERObject readObject() throws IOException {
        int read = read();
        if (read == -1) {
            throw new EOFException();
        }
        int readLength = readLength();
        if (readLength < 0) {
            switch (read) {
                case 5:
                    return null;
                case 36:
                    return buildConstructedOctetString();
                case 48:
                    BERConstructedSequence bERConstructedSequence = new BERConstructedSequence();
                    while (true) {
                        DERObject readObject = readObject();
                        if (readObject == END_OF_STREAM) {
                            return bERConstructedSequence;
                        }
                        bERConstructedSequence.addObject(readObject);
                    }
                case 49:
                    ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
                    while (true) {
                        DERObject readObject2 = readObject();
                        if (readObject2 == END_OF_STREAM) {
                            return new BERSet(aSN1EncodableVector);
                        }
                        aSN1EncodableVector.add(readObject2);
                    }
                default:
                    if ((read & 128) == 0) {
                        throw new IOException("unknown BER object encountered");
                    } else if ((read & 31) == 31) {
                        throw new IOException("unsupported high tag encountered");
                    } else if ((read & 32) == 0) {
                        return new BERTaggedObject(false, read & 31, new DEROctetString(readIndefiniteLengthFully()));
                    } else {
                        DERObject readObject3 = readObject();
                        if (readObject3 == END_OF_STREAM) {
                            return new DERTaggedObject(read & 31);
                        }
                        DERObject readObject4 = readObject();
                        if (readObject4 == END_OF_STREAM) {
                            return new BERTaggedObject(read & 31, readObject3);
                        }
                        BERConstructedSequence bERConstructedSequence2 = new BERConstructedSequence();
                        bERConstructedSequence2.addObject(readObject3);
                        DERObject dERObject = readObject4;
                        do {
                            bERConstructedSequence2.addObject(dERObject);
                            dERObject = readObject();
                        } while (dERObject != END_OF_STREAM);
                        return new BERTaggedObject(false, read & 31, bERConstructedSequence2);
                    }
            }
        } else if (read == 0 && readLength == 0) {
            return END_OF_STREAM;
        } else {
            byte[] bArr = new byte[readLength];
            readFully(bArr);
            return buildObject(read, bArr);
        }
    }
}
