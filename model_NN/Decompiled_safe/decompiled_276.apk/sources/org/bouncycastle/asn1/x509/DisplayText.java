package org.bouncycastle.asn1.x509;

import org.bouncycastle.asn1.ASN1Choice;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERString;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.DERVisibleString;

public class DisplayText extends ASN1Encodable implements ASN1Choice {
    public static final int CONTENT_TYPE_BMPSTRING = 1;
    public static final int CONTENT_TYPE_IA5STRING = 0;
    public static final int CONTENT_TYPE_UTF8STRING = 2;
    public static final int CONTENT_TYPE_VISIBLESTRING = 3;
    public static final int DISPLAY_TEXT_MAXIMUM_SIZE = 200;
    int contentType;
    DERString contents;

    public DisplayText(int i, String str) {
        String substring = str.length() > 200 ? str.substring(0, DISPLAY_TEXT_MAXIMUM_SIZE) : str;
        this.contentType = i;
        switch (i) {
            case 0:
                this.contents = new DERIA5String(substring);
                return;
            case 1:
                this.contents = new DERBMPString(substring);
                return;
            case 2:
                this.contents = new DERUTF8String(substring);
                return;
            case 3:
                this.contents = new DERVisibleString(substring);
                return;
            default:
                this.contents = new DERUTF8String(substring);
                return;
        }
    }

    public DisplayText(String str) {
        String substring = str.length() > 200 ? str.substring(0, DISPLAY_TEXT_MAXIMUM_SIZE) : str;
        this.contentType = 2;
        this.contents = new DERUTF8String(substring);
    }

    public DisplayText(DERString dERString) {
        this.contents = dERString;
    }

    public static DisplayText getInstance(Object obj) {
        if (obj instanceof DERString) {
            return new DisplayText((DERString) obj);
        }
        if (obj instanceof DisplayText) {
            return (DisplayText) obj;
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DisplayText getInstance(ASN1TaggedObject aSN1TaggedObject, boolean z) {
        return getInstance(aSN1TaggedObject.getObject());
    }

    public String getString() {
        return this.contents.getString();
    }

    public DERObject toASN1Object() {
        return (DERObject) this.contents;
    }
}
