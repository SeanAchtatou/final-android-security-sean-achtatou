package org.bouncycastle.asn1.x509.qualified;

import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.GeneralName;

public class SemanticsInformation extends ASN1Encodable {
    GeneralName[] nameRegistrationAuthorities;
    DERObjectIdentifier semanticsIdentifier;

    public SemanticsInformation(ASN1Sequence aSN1Sequence) {
        Object obj;
        Enumeration objects = aSN1Sequence.getObjects();
        if (aSN1Sequence.size() < 1) {
            throw new IllegalArgumentException("no objects in SemanticsInformation");
        }
        Object nextElement = objects.nextElement();
        if (nextElement instanceof DERObjectIdentifier) {
            this.semanticsIdentifier = DERObjectIdentifier.getInstance(nextElement);
            obj = objects.hasMoreElements() ? objects.nextElement() : null;
        } else {
            obj = nextElement;
        }
        if (obj != null) {
            ASN1Sequence instance = ASN1Sequence.getInstance(obj);
            this.nameRegistrationAuthorities = new GeneralName[instance.size()];
            for (int i = 0; i < instance.size(); i++) {
                this.nameRegistrationAuthorities[i] = GeneralName.getInstance(instance.getObjectAt(i));
            }
        }
    }

    public SemanticsInformation(DERObjectIdentifier dERObjectIdentifier) {
        this.semanticsIdentifier = dERObjectIdentifier;
        this.nameRegistrationAuthorities = null;
    }

    public SemanticsInformation(DERObjectIdentifier dERObjectIdentifier, GeneralName[] generalNameArr) {
        this.semanticsIdentifier = dERObjectIdentifier;
        this.nameRegistrationAuthorities = generalNameArr;
    }

    public SemanticsInformation(GeneralName[] generalNameArr) {
        this.semanticsIdentifier = null;
        this.nameRegistrationAuthorities = generalNameArr;
    }

    public static SemanticsInformation getInstance(Object obj) {
        if (obj == null || (obj instanceof SemanticsInformation)) {
            return (SemanticsInformation) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new SemanticsInformation(ASN1Sequence.getInstance(obj));
        }
        throw new IllegalArgumentException("unknown object in getInstance");
    }

    public GeneralName[] getNameRegistrationAuthorities() {
        return this.nameRegistrationAuthorities;
    }

    public DERObjectIdentifier getSemanticsIdentifier() {
        return this.semanticsIdentifier;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        if (this.semanticsIdentifier != null) {
            aSN1EncodableVector.add(this.semanticsIdentifier);
        }
        if (this.nameRegistrationAuthorities != null) {
            ASN1EncodableVector aSN1EncodableVector2 = new ASN1EncodableVector();
            for (GeneralName add : this.nameRegistrationAuthorities) {
                aSN1EncodableVector2.add(add);
            }
            aSN1EncodableVector.add(new DERSequence(aSN1EncodableVector2));
        }
        return new DERSequence(aSN1EncodableVector);
    }
}
