package org.bouncycastle.asn1.x509;

import java.io.IOException;
import org.bouncycastle.asn1.DERGeneralizedTime;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.DERUTF8String;

public class X509DefaultEntryConverter extends X509NameEntryConverter {
    public DERObject getConvertedValue(DERObjectIdentifier dERObjectIdentifier, String str) {
        if (str.length() == 0 || str.charAt(0) != '#') {
            String substring = (str.length() == 0 || str.charAt(0) != '\\') ? str : str.substring(1);
            return (dERObjectIdentifier.equals(X509Name.EmailAddress) || dERObjectIdentifier.equals(X509Name.DC)) ? new DERIA5String(substring) : dERObjectIdentifier.equals(X509Name.DATE_OF_BIRTH) ? new DERGeneralizedTime(substring) : (dERObjectIdentifier.equals(X509Name.C) || dERObjectIdentifier.equals(X509Name.SN) || dERObjectIdentifier.equals(X509Name.DN_QUALIFIER)) ? new DERPrintableString(substring) : new DERUTF8String(substring);
        }
        try {
            return convertHexEncoded(str, 1);
        } catch (IOException e) {
            throw new RuntimeException("can't recode value for oid " + dERObjectIdentifier.getId());
        }
    }
}
