package org.bouncycastle.asn1;

import java.io.IOException;

public class DERTaggedObject extends ASN1TaggedObject {
    private static final byte[] ZERO_BYTES = new byte[0];

    public DERTaggedObject(int i) {
        super(false, i, new DERSequence());
    }

    public DERTaggedObject(int i, DEREncodable dEREncodable) {
        super(i, dEREncodable);
    }

    public DERTaggedObject(boolean z, int i, DEREncodable dEREncodable) {
        super(z, i, dEREncodable);
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream dEROutputStream) throws IOException {
        if (!this.empty) {
            byte[] encoded = this.obj.getDERObject().getEncoded(ASN1Encodable.DER);
            if (this.explicit) {
                dEROutputStream.writeEncoded(160, this.tagNo, encoded);
                return;
            }
            dEROutputStream.writeTag((encoded[0] & 32) != 0 ? 160 : 128, this.tagNo);
            dEROutputStream.write(encoded, 1, encoded.length - 1);
            return;
        }
        dEROutputStream.writeEncoded(160, this.tagNo, ZERO_BYTES);
    }
}
