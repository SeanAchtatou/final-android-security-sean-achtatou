package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public class BERConstructedOctetString extends DEROctetString {
    private static final int MAX_LENGTH = 1000;
    private Vector octs;

    public BERConstructedOctetString(Vector vector) {
        super(toBytes(vector));
        this.octs = vector;
    }

    public BERConstructedOctetString(DEREncodable dEREncodable) {
        super(dEREncodable.getDERObject());
    }

    public BERConstructedOctetString(DERObject dERObject) {
        super(dERObject);
    }

    public BERConstructedOctetString(byte[] bArr) {
        super(bArr);
    }

    private Vector generateOcts() {
        Vector vector = new Vector();
        int i = 0;
        for (int i2 = 0; i2 + 1 < this.string.length; i2++) {
            if (this.string[i2] == 0 && this.string[i2 + 1] == 0) {
                byte[] bArr = new byte[((i2 - i) + 1)];
                System.arraycopy(this.string, i, bArr, 0, bArr.length);
                vector.addElement(new DEROctetString(bArr));
                i = i2 + 1;
            }
        }
        byte[] bArr2 = new byte[(this.string.length - i)];
        System.arraycopy(this.string, i, bArr2, 0, bArr2.length);
        vector.addElement(new DEROctetString(bArr2));
        return vector;
    }

    private static byte[] toBytes(Vector vector) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 == vector.size()) {
                return byteArrayOutputStream.toByteArray();
            }
            try {
                byteArrayOutputStream.write(((DEROctetString) vector.elementAt(i2)).getOctets());
                i = i2 + 1;
            } catch (ClassCastException e) {
                throw new IllegalArgumentException(vector.elementAt(i2).getClass().getName() + " found in input should only contain DEROctetString");
            } catch (IOException e2) {
                throw new IllegalArgumentException("exception converting octets " + e2.toString());
            }
        }
    }

    public void encode(DEROutputStream dEROutputStream) throws IOException {
        if ((dEROutputStream instanceof ASN1OutputStream) || (dEROutputStream instanceof BEROutputStream)) {
            dEROutputStream.write(36);
            dEROutputStream.write(128);
            if (this.octs != null) {
                for (int i = 0; i != this.octs.size(); i++) {
                    dEROutputStream.writeObject(this.octs.elementAt(i));
                }
            } else {
                for (int i2 = 0; i2 < this.string.length; i2 += MAX_LENGTH) {
                    byte[] bArr = new byte[((i2 + MAX_LENGTH > this.string.length ? this.string.length : i2 + MAX_LENGTH) - i2)];
                    System.arraycopy(this.string, i2, bArr, 0, bArr.length);
                    dEROutputStream.writeObject(new DEROctetString(bArr));
                }
            }
            dEROutputStream.write(0);
            dEROutputStream.write(0);
            return;
        }
        super.encode(dEROutputStream);
    }

    public Enumeration getObjects() {
        return this.octs == null ? generateOcts().elements() : this.octs.elements();
    }

    public byte[] getOctets() {
        return this.string;
    }
}
