package org.bouncycastle.asn1.x9;

import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.math.ec.ECCurve;

public class X9Curve extends ASN1Encodable implements X9ObjectIdentifiers {
    private ECCurve curve;
    private DERObjectIdentifier fieldIdentifier = null;
    private byte[] seed;

    public X9Curve(X9FieldID x9FieldID, ASN1Sequence aSN1Sequence) {
        int intValue;
        int i;
        int i2;
        this.fieldIdentifier = x9FieldID.getIdentifier();
        if (this.fieldIdentifier.equals(prime_field)) {
            BigInteger value = ((DERInteger) x9FieldID.getParameters()).getValue();
            this.curve = new ECCurve.Fp(value, new X9FieldElement(value, (ASN1OctetString) aSN1Sequence.getObjectAt(0)).getValue().toBigInteger(), new X9FieldElement(value, (ASN1OctetString) aSN1Sequence.getObjectAt(1)).getValue().toBigInteger());
        } else if (this.fieldIdentifier.equals(characteristic_two_field)) {
            DERSequence dERSequence = (DERSequence) x9FieldID.getParameters();
            int intValue2 = ((DERInteger) dERSequence.getObjectAt(0)).getValue().intValue();
            if (((DERObjectIdentifier) dERSequence.getObjectAt(1)).equals(tpBasis)) {
                intValue = 0;
                i2 = 0;
                i = ((DERInteger) dERSequence.getObjectAt(2)).getValue().intValue();
            } else {
                DERSequence dERSequence2 = (DERSequence) dERSequence.getObjectAt(2);
                int intValue3 = ((DERInteger) dERSequence2.getObjectAt(0)).getValue().intValue();
                int intValue4 = ((DERInteger) dERSequence2.getObjectAt(1)).getValue().intValue();
                intValue = ((DERInteger) dERSequence2.getObjectAt(2)).getValue().intValue();
                int i3 = intValue4;
                i = intValue3;
                i2 = i3;
            }
            this.curve = new ECCurve.F2m(intValue2, i, i2, intValue, new X9FieldElement(intValue2, i, i2, intValue, (ASN1OctetString) aSN1Sequence.getObjectAt(0)).getValue().toBigInteger(), new X9FieldElement(intValue2, i, i2, intValue, (ASN1OctetString) aSN1Sequence.getObjectAt(1)).getValue().toBigInteger());
        }
        if (aSN1Sequence.size() == 3) {
            this.seed = ((DERBitString) aSN1Sequence.getObjectAt(2)).getBytes();
        }
    }

    public X9Curve(ECCurve eCCurve) {
        this.curve = eCCurve;
        this.seed = null;
        setFieldIdentifier();
    }

    public X9Curve(ECCurve eCCurve, byte[] bArr) {
        this.curve = eCCurve;
        this.seed = bArr;
        setFieldIdentifier();
    }

    private void setFieldIdentifier() {
        if (this.curve instanceof ECCurve.Fp) {
            this.fieldIdentifier = prime_field;
        } else if (this.curve instanceof ECCurve.F2m) {
            this.fieldIdentifier = characteristic_two_field;
        } else {
            throw new IllegalArgumentException("This type of ECCurve is not implemented");
        }
    }

    public ECCurve getCurve() {
        return this.curve;
    }

    public byte[] getSeed() {
        return this.seed;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        if (this.fieldIdentifier.equals(prime_field)) {
            aSN1EncodableVector.add(new X9FieldElement(this.curve.getA()).getDERObject());
            aSN1EncodableVector.add(new X9FieldElement(this.curve.getB()).getDERObject());
        } else if (this.fieldIdentifier.equals(characteristic_two_field)) {
            aSN1EncodableVector.add(new X9FieldElement(this.curve.getA()).getDERObject());
            aSN1EncodableVector.add(new X9FieldElement(this.curve.getB()).getDERObject());
        }
        if (this.seed != null) {
            aSN1EncodableVector.add(new DERBitString(this.seed));
        }
        return new DERSequence(aSN1EncodableVector);
    }
}
