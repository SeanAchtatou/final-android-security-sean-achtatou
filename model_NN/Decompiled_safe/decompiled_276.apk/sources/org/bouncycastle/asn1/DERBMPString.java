package org.bouncycastle.asn1;

import java.io.IOException;

public class DERBMPString extends ASN1Object implements DERString {
    String string;

    public DERBMPString(String str) {
        this.string = str;
    }

    public DERBMPString(byte[] bArr) {
        char[] cArr = new char[(bArr.length / 2)];
        for (int i = 0; i != cArr.length; i++) {
            cArr[i] = (char) ((bArr[i * 2] << 8) | (bArr[(i * 2) + 1] & 255));
        }
        this.string = new String(cArr);
    }

    public static DERBMPString getInstance(Object obj) {
        if (obj == null || (obj instanceof DERBMPString)) {
            return (DERBMPString) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERBMPString(((ASN1OctetString) obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERBMPString getInstance(ASN1TaggedObject aSN1TaggedObject, boolean z) {
        return getInstance(aSN1TaggedObject.getObject());
    }

    /* access modifiers changed from: protected */
    public boolean asn1Equals(DERObject dERObject) {
        if (!(dERObject instanceof DERBMPString)) {
            return false;
        }
        return getString().equals(((DERBMPString) dERObject).getString());
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream dEROutputStream) throws IOException {
        char[] charArray = this.string.toCharArray();
        byte[] bArr = new byte[(charArray.length * 2)];
        for (int i = 0; i != charArray.length; i++) {
            bArr[i * 2] = (byte) (charArray[i] >> 8);
            bArr[(i * 2) + 1] = (byte) charArray[i];
        }
        dEROutputStream.writeEncoded(30, bArr);
    }

    public String getString() {
        return this.string;
    }

    public int hashCode() {
        return getString().hashCode();
    }

    public String toString() {
        return this.string;
    }
}
