package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.bouncycastle.util.Arrays;

public class DERApplicationSpecific extends ASN1Object {
    private final boolean isConstructed;
    private final byte[] octets;
    private final int tag;

    public DERApplicationSpecific(int i, ASN1EncodableVector aSN1EncodableVector) {
        this.tag = i;
        this.isConstructed = true;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 != aSN1EncodableVector.size()) {
                try {
                    byteArrayOutputStream.write(((ASN1Encodable) aSN1EncodableVector.get(i3)).getEncoded());
                    i2 = i3 + 1;
                } catch (IOException e) {
                    throw new IllegalStateException("malformed object: " + e);
                }
            } else {
                this.octets = byteArrayOutputStream.toByteArray();
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.bouncycastle.asn1.DERApplicationSpecific.<init>(boolean, int, org.bouncycastle.asn1.DEREncodable):void
     arg types: [int, int, org.bouncycastle.asn1.DEREncodable]
     candidates:
      org.bouncycastle.asn1.DERApplicationSpecific.<init>(boolean, int, byte[]):void
      org.bouncycastle.asn1.DERApplicationSpecific.<init>(boolean, int, org.bouncycastle.asn1.DEREncodable):void */
    public DERApplicationSpecific(int i, DEREncodable dEREncodable) throws IOException {
        this(true, i, dEREncodable);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.bouncycastle.asn1.DERApplicationSpecific.<init>(boolean, int, byte[]):void
     arg types: [int, int, byte[]]
     candidates:
      org.bouncycastle.asn1.DERApplicationSpecific.<init>(boolean, int, org.bouncycastle.asn1.DEREncodable):void
      org.bouncycastle.asn1.DERApplicationSpecific.<init>(boolean, int, byte[]):void */
    public DERApplicationSpecific(int i, byte[] bArr) {
        this(false, i, bArr);
    }

    public DERApplicationSpecific(boolean z, int i, DEREncodable dEREncodable) throws IOException {
        byte[] dEREncoded = dEREncodable.getDERObject().getDEREncoded();
        this.isConstructed = z;
        this.tag = i;
        if (z) {
            this.octets = dEREncoded;
            return;
        }
        int lengthOfLength = getLengthOfLength(dEREncoded);
        byte[] bArr = new byte[(dEREncoded.length - lengthOfLength)];
        System.arraycopy(dEREncoded, lengthOfLength, bArr, 0, bArr.length);
        this.octets = bArr;
    }

    DERApplicationSpecific(boolean z, int i, byte[] bArr) {
        this.isConstructed = z;
        this.tag = i;
        this.octets = bArr;
    }

    private int getLengthOfLength(byte[] bArr) {
        int i = 2;
        while ((bArr[i - 1] & 128) != 0) {
            i++;
        }
        return i;
    }

    /* JADX WARN: Type inference failed for: r0v13, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] replaceTagNumber(int r8, byte[] r9) throws java.io.IOException {
        /*
            r7 = this;
            r5 = 1
            r4 = 0
            byte r0 = r9[r4]
            r0 = r0 & 31
            r1 = 31
            if (r0 != r1) goto L_0x004a
            int r0 = r5 + 1
            byte r1 = r9[r5]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r2 = r1 & 127(0x7f, float:1.78E-43)
            if (r2 != 0) goto L_0x0045
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "corrupted stream - invalid high tag number found"
            r0.<init>(r1)
            throw r0
        L_0x001c:
            if (r0 < 0) goto L_0x0032
            r3 = r0 & 128(0x80, float:1.794E-43)
            if (r3 == 0) goto L_0x0032
            r0 = r0 & 127(0x7f, float:1.78E-43)
            r0 = r0 | r2
            int r0 = r0 << 7
            int r2 = r1 + 1
            byte r1 = r9[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r6 = r1
            r1 = r2
            r2 = r0
            r0 = r6
            goto L_0x001c
        L_0x0032:
            r0 = r0 & 127(0x7f, float:1.78E-43)
            r0 = r0 | r2
            r0 = r1
        L_0x0036:
            int r1 = r9.length
            int r1 = r1 - r0
            int r1 = r1 + 1
            byte[] r1 = new byte[r1]
            int r2 = r1.length
            int r2 = r2 - r5
            java.lang.System.arraycopy(r9, r0, r1, r5, r2)
            byte r0 = (byte) r8
            r1[r4] = r0
            return r1
        L_0x0045:
            r2 = r4
            r6 = r0
            r0 = r1
            r1 = r6
            goto L_0x001c
        L_0x004a:
            r0 = r5
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.asn1.DERApplicationSpecific.replaceTagNumber(int, byte[]):byte[]");
    }

    /* access modifiers changed from: package-private */
    public boolean asn1Equals(DERObject dERObject) {
        if (!(dERObject instanceof DERApplicationSpecific)) {
            return false;
        }
        DERApplicationSpecific dERApplicationSpecific = (DERApplicationSpecific) dERObject;
        return this.isConstructed == dERApplicationSpecific.isConstructed && this.tag == dERApplicationSpecific.tag && Arrays.areEqual(this.octets, dERApplicationSpecific.octets);
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream dEROutputStream) throws IOException {
        int i = 64;
        if (this.isConstructed) {
            i = 64 | 32;
        }
        dEROutputStream.writeEncoded(i, this.tag, this.octets);
    }

    public int getApplicationTag() {
        return this.tag;
    }

    public byte[] getContents() {
        return this.octets;
    }

    public DERObject getObject() throws IOException {
        return new ASN1InputStream(getContents()).readObject();
    }

    public DERObject getObject(int i) throws IOException {
        if (i >= 31) {
            throw new IOException("unsupported tag number");
        }
        byte[] encoded = getEncoded();
        byte[] replaceTagNumber = replaceTagNumber(i, encoded);
        if ((encoded[0] & 32) != 0) {
            replaceTagNumber[0] = (byte) (replaceTagNumber[0] | 32);
        }
        return new ASN1InputStream(replaceTagNumber).readObject();
    }

    public int hashCode() {
        return ((this.isConstructed ? 1 : 0) ^ this.tag) ^ Arrays.hashCode(this.octets);
    }

    public boolean isConstructed() {
        return this.isConstructed;
    }
}
