package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public abstract class ASN1Set extends ASN1Object {
    protected Vector set = new Vector();

    private byte[] getEncoded(DEREncodable dEREncodable) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            new ASN1OutputStream(byteArrayOutputStream).writeObject(dEREncodable);
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException("cannot encode object added to SET");
        }
    }

    public static ASN1Set getInstance(Object obj) {
        if (obj == null || (obj instanceof ASN1Set)) {
            return (ASN1Set) obj;
        }
        throw new IllegalArgumentException("unknown object in getInstance: " + obj.getClass().getName());
    }

    public static ASN1Set getInstance(ASN1TaggedObject aSN1TaggedObject, boolean z) {
        if (z) {
            if (aSN1TaggedObject.isExplicit()) {
                return (ASN1Set) aSN1TaggedObject.getObject();
            }
            throw new IllegalArgumentException("object implicit - explicit expected.");
        } else if (aSN1TaggedObject.isExplicit()) {
            return new DERSet(aSN1TaggedObject.getObject());
        } else {
            if (aSN1TaggedObject.getObject() instanceof ASN1Set) {
                return (ASN1Set) aSN1TaggedObject.getObject();
            }
            ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
            if (aSN1TaggedObject.getObject() instanceof ASN1Sequence) {
                Enumeration objects = ((ASN1Sequence) aSN1TaggedObject.getObject()).getObjects();
                while (objects.hasMoreElements()) {
                    aSN1EncodableVector.add((DEREncodable) objects.nextElement());
                }
                return new DERSet(aSN1EncodableVector, false);
            }
            throw new IllegalArgumentException("unknown object in getInstance: " + aSN1TaggedObject.getClass().getName());
        }
    }

    private boolean lessThanOrEqual(byte[] bArr, byte[] bArr2) {
        if (bArr.length <= bArr2.length) {
            for (int i = 0; i != bArr.length; i++) {
                byte b = bArr[i] & 255;
                byte b2 = bArr2[i] & 255;
                if (b2 > b) {
                    return true;
                }
                if (b > b2) {
                    return false;
                }
            }
            return true;
        }
        for (int i2 = 0; i2 != bArr2.length; i2++) {
            byte b3 = bArr[i2] & 255;
            byte b4 = bArr2[i2] & 255;
            if (b4 > b3) {
                return true;
            }
            if (b3 > b4) {
                return false;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void addObject(DEREncodable dEREncodable) {
        this.set.addElement(dEREncodable);
    }

    /* access modifiers changed from: package-private */
    public boolean asn1Equals(DERObject dERObject) {
        if (!(dERObject instanceof ASN1Set)) {
            return false;
        }
        ASN1Set aSN1Set = (ASN1Set) dERObject;
        if (size() != aSN1Set.size()) {
            return false;
        }
        Enumeration objects = getObjects();
        Enumeration objects2 = aSN1Set.getObjects();
        while (objects.hasMoreElements()) {
            DERObject dERObject2 = ((DEREncodable) objects.nextElement()).getDERObject();
            DERObject dERObject3 = ((DEREncodable) objects2.nextElement()).getDERObject();
            if (dERObject2 != dERObject3 && (dERObject2 == null || !dERObject2.equals(dERObject3))) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public abstract void encode(DEROutputStream dEROutputStream) throws IOException;

    public DEREncodable getObjectAt(int i) {
        return (DEREncodable) this.set.elementAt(i);
    }

    public Enumeration getObjects() {
        return this.set.elements();
    }

    public int hashCode() {
        Enumeration objects = getObjects();
        int size = size();
        while (objects.hasMoreElements()) {
            Object nextElement = objects.nextElement();
            size *= 17;
            if (nextElement != null) {
                size ^= nextElement.hashCode();
            }
        }
        return size;
    }

    public ASN1SetParser parser() {
        return new ASN1SetParser() {
            private int index;
            private final int max = ASN1Set.this.size();

            public DERObject getDERObject() {
                return this;
            }

            public DEREncodable readObject() throws IOException {
                if (this.index == this.max) {
                    return null;
                }
                ASN1Set aSN1Set = ASN1Set.this;
                int i = this.index;
                this.index = i + 1;
                DEREncodable objectAt = aSN1Set.getObjectAt(i);
                return objectAt instanceof ASN1Sequence ? ((ASN1Sequence) objectAt).parser() : objectAt instanceof ASN1Set ? ((ASN1Set) objectAt).parser() : objectAt;
            }
        };
    }

    public int size() {
        return this.set.size();
    }

    /* access modifiers changed from: protected */
    public void sort() {
        boolean z;
        int i;
        if (this.set.size() > 1) {
            int size = this.set.size() - 1;
            boolean z2 = true;
            while (z2) {
                byte[] encoded = getEncoded((DEREncodable) this.set.elementAt(0));
                int i2 = 0;
                int i3 = 0;
                boolean z3 = false;
                while (i3 != size) {
                    byte[] encoded2 = getEncoded((DEREncodable) this.set.elementAt(i3 + 1));
                    if (lessThanOrEqual(encoded, encoded2)) {
                        i = i2;
                        z = z3;
                    } else {
                        Object elementAt = this.set.elementAt(i3);
                        this.set.setElementAt(this.set.elementAt(i3 + 1), i3);
                        this.set.setElementAt(elementAt, i3 + 1);
                        encoded2 = encoded;
                        z = true;
                        i = i3;
                    }
                    i3++;
                    z3 = z;
                    i2 = i;
                    encoded = encoded2;
                }
                size = i2;
                z2 = z3;
            }
        }
    }

    public String toString() {
        return this.set.toString();
    }
}
