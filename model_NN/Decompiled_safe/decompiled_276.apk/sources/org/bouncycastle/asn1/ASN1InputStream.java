package org.bouncycastle.asn1;

import com.yhiker.config.GuideConfig;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.bouncycastle.util.io.Streams;

public class ASN1InputStream extends FilterInputStream implements DERTags {
    private final boolean lazyEvaluate;
    private final int limit;

    public ASN1InputStream(InputStream inputStream) {
        this(inputStream, Integer.MAX_VALUE);
    }

    public ASN1InputStream(InputStream inputStream, int i) {
        this(inputStream, i, false);
    }

    public ASN1InputStream(InputStream inputStream, int i, boolean z) {
        super(inputStream);
        this.limit = i;
        this.lazyEvaluate = z;
    }

    public ASN1InputStream(byte[] bArr) {
        this(new ByteArrayInputStream(bArr), bArr.length);
    }

    public ASN1InputStream(byte[] bArr, boolean z) {
        this(new ByteArrayInputStream(bArr), bArr.length, z);
    }

    static DERObject createPrimitiveDERObject(int i, byte[] bArr) {
        switch (i) {
            case 1:
                return new DERBoolean(bArr);
            case 2:
                return new DERInteger(bArr);
            case 3:
                byte b = bArr[0];
                byte[] bArr2 = new byte[(bArr.length - 1)];
                System.arraycopy(bArr, 1, bArr2, 0, bArr.length - 1);
                return new DERBitString(bArr2, b);
            case 4:
                return new DEROctetString(bArr);
            case 5:
                return DERNull.INSTANCE;
            case 6:
                return new DERObjectIdentifier(bArr);
            case 7:
            case 8:
            case 9:
            case CertStatus.UNREVOKED /*11*/:
            case 13:
            case 14:
            case GuideConfig.satelliteSnr_minlimit:
            case 16:
            case 17:
            case DERTags.VIDEOTEX_STRING /*21*/:
            case DERTags.GRAPHIC_STRING /*25*/:
            case 29:
            default:
                return new DERUnknownTag(false, i, bArr);
            case 10:
                return new DEREnumerated(bArr);
            case 12:
                return new DERUTF8String(bArr);
            case DERTags.NUMERIC_STRING /*18*/:
                return new DERNumericString(bArr);
            case DERTags.PRINTABLE_STRING /*19*/:
                return new DERPrintableString(bArr);
            case 20:
                return new DERT61String(bArr);
            case DERTags.IA5_STRING /*22*/:
                return new DERIA5String(bArr);
            case DERTags.UTC_TIME /*23*/:
                return new DERUTCTime(bArr);
            case 24:
                return new DERGeneralizedTime(bArr);
            case DERTags.VISIBLE_STRING /*26*/:
                return new DERVisibleString(bArr);
            case DERTags.GENERAL_STRING /*27*/:
                return new DERGeneralString(bArr);
            case DERTags.UNIVERSAL_STRING /*28*/:
                return new DERUniversalString(bArr);
            case DERTags.BMP_STRING /*30*/:
                return new DERBMPString(bArr);
        }
    }

    static int readLength(InputStream inputStream, int i) throws IOException {
        int i2 = 0;
        int read = inputStream.read();
        if (read < 0) {
            throw new EOFException("EOF found when length expected");
        } else if (read == 128) {
            return -1;
        } else {
            if (read <= 127) {
                return read;
            }
            int i3 = read & 127;
            if (i3 > 4) {
                throw new IOException("DER length more than 4 bytes");
            }
            for (int i4 = 0; i4 < i3; i4++) {
                int read2 = inputStream.read();
                if (read2 < 0) {
                    throw new EOFException("EOF found reading length");
                }
                i2 = (i2 << 8) + read2;
            }
            if (i2 < 0) {
                throw new IOException("corrupted stream - negative length found");
            } else if (i2 < i) {
                return i2;
            } else {
                throw new IOException("corrupted stream - out of bounds length found");
            }
        }
    }

    static int readTagNumber(InputStream inputStream, int i) throws IOException {
        int i2 = i & 31;
        if (i2 != 31) {
            return i2;
        }
        int read = inputStream.read();
        if ((read & 127) == 0) {
            throw new IOException("corrupted stream - invalid high tag number found");
        }
        int i3 = read;
        int i4 = 0;
        int i5 = i3;
        while (i5 >= 0 && (i5 & 128) != 0) {
            i4 = ((i5 & 127) | i4) << 7;
            i5 = inputStream.read();
        }
        if (i5 >= 0) {
            return (i5 & 127) | i4;
        }
        throw new EOFException("EOF found inside tag value.");
    }

    /* access modifiers changed from: package-private */
    public ASN1EncodableVector buildDEREncodableVector(DefiniteLengthInputStream definiteLengthInputStream) throws IOException {
        return new ASN1InputStream(definiteLengthInputStream).buildEncodableVector();
    }

    /* access modifiers changed from: package-private */
    public ASN1EncodableVector buildEncodableVector() throws IOException {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        while (true) {
            DERObject readObject = readObject();
            if (readObject == null) {
                return aSN1EncodableVector;
            }
            aSN1EncodableVector.add(readObject);
        }
    }

    /* access modifiers changed from: protected */
    public DERObject buildObject(int i, int i2, int i3) throws IOException {
        boolean z = (i & 32) != 0;
        DefiniteLengthInputStream definiteLengthInputStream = new DefiniteLengthInputStream(this, i3);
        if ((i & 64) != 0) {
            return new DERApplicationSpecific(z, i2, definiteLengthInputStream.toByteArray());
        }
        if ((i & 128) != 0) {
            return new BERTaggedObjectParser(i, i2, definiteLengthInputStream).getDERObject();
        }
        if (!z) {
            return createPrimitiveDERObject(i2, definiteLengthInputStream.toByteArray());
        }
        switch (i2) {
            case 4:
                return new BERConstructedOctetString(buildDEREncodableVector(definiteLengthInputStream).v);
            case 16:
                return this.lazyEvaluate ? new LazyDERSequence(definiteLengthInputStream.toByteArray()) : DERFactory.createSequence(buildDEREncodableVector(definiteLengthInputStream));
            case 17:
                return DERFactory.createSet(buildDEREncodableVector(definiteLengthInputStream), false);
            default:
                return new DERUnknownTag(true, i2, definiteLengthInputStream.toByteArray());
        }
    }

    /* access modifiers changed from: protected */
    public void readFully(byte[] bArr) throws IOException {
        if (Streams.readFully(this, bArr) != bArr.length) {
            throw new EOFException("EOF encountered in middle of object");
        }
    }

    /* access modifiers changed from: protected */
    public int readLength() throws IOException {
        return readLength(this, this.limit);
    }

    public DERObject readObject() throws IOException {
        int read = read();
        if (read > 0) {
            int readTagNumber = readTagNumber(this, read);
            boolean z = (read & 32) != 0;
            int readLength = readLength();
            if (readLength >= 0) {
                return buildObject(read, readTagNumber, readLength);
            }
            if (!z) {
                throw new IOException("indefinite length primitive encoding encountered");
            }
            IndefiniteLengthInputStream indefiniteLengthInputStream = new IndefiniteLengthInputStream(this);
            if ((read & 64) != 0) {
                return new BERApplicationSpecificParser(readTagNumber, new ASN1StreamParser(indefiniteLengthInputStream)).getDERObject();
            }
            if ((read & 128) != 0) {
                return new BERTaggedObjectParser(read, readTagNumber, indefiniteLengthInputStream).getDERObject();
            }
            ASN1StreamParser aSN1StreamParser = new ASN1StreamParser(indefiniteLengthInputStream);
            switch (readTagNumber) {
                case 4:
                    return new BEROctetStringParser(aSN1StreamParser).getDERObject();
                case 16:
                    return new BERSequenceParser(aSN1StreamParser).getDERObject();
                case 17:
                    return new BERSetParser(aSN1StreamParser).getDERObject();
                default:
                    throw new IOException("unknown BER object encountered");
            }
        } else if (read != 0) {
            return null;
        } else {
            throw new IOException("unexpected end-of-contents marker");
        }
    }
}
