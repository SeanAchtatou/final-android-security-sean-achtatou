package org.bouncycastle.asn1.pkcs;

import java.math.BigInteger;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERSequence;

public class DHParameter extends ASN1Encodable {
    DERInteger g;
    DERInteger l;
    DERInteger p;

    public DHParameter(BigInteger bigInteger, BigInteger bigInteger2, int i) {
        this.p = new DERInteger(bigInteger);
        this.g = new DERInteger(bigInteger2);
        if (i != 0) {
            this.l = new DERInteger(i);
        } else {
            this.l = null;
        }
    }

    public DHParameter(ASN1Sequence aSN1Sequence) {
        Enumeration objects = aSN1Sequence.getObjects();
        this.p = (DERInteger) objects.nextElement();
        this.g = (DERInteger) objects.nextElement();
        if (objects.hasMoreElements()) {
            this.l = (DERInteger) objects.nextElement();
        } else {
            this.l = null;
        }
    }

    public BigInteger getG() {
        return this.g.getPositiveValue();
    }

    public BigInteger getL() {
        if (this.l == null) {
            return null;
        }
        return this.l.getPositiveValue();
    }

    public BigInteger getP() {
        return this.p.getPositiveValue();
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        aSN1EncodableVector.add(this.p);
        aSN1EncodableVector.add(this.g);
        if (getL() != null) {
            aSN1EncodableVector.add(this.l);
        }
        return new DERSequence(aSN1EncodableVector);
    }
}
