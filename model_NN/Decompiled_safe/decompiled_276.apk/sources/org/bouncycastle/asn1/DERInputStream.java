package org.bouncycastle.asn1;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DERInputStream extends FilterInputStream implements DERTags {
    public DERInputStream(InputStream inputStream) {
        super(inputStream);
    }

    /* access modifiers changed from: protected */
    public DERObject buildObject(int i, byte[] bArr) throws IOException {
        switch (i) {
            case 1:
                return new DERBoolean(bArr);
            case 2:
                return new DERInteger(bArr);
            case 3:
                byte b = bArr[0];
                byte[] bArr2 = new byte[(bArr.length - 1)];
                System.arraycopy(bArr, 1, bArr2, 0, bArr.length - 1);
                return new DERBitString(bArr2, b);
            case 4:
                return new DEROctetString(bArr);
            case 5:
                return null;
            case 6:
                return new DERObjectIdentifier(bArr);
            case 10:
                return new DEREnumerated(bArr);
            case 12:
                return new DERUTF8String(bArr);
            case DERTags.PRINTABLE_STRING:
                return new DERPrintableString(bArr);
            case 20:
                return new DERT61String(bArr);
            case DERTags.IA5_STRING:
                return new DERIA5String(bArr);
            case DERTags.UTC_TIME:
                return new DERUTCTime(bArr);
            case 24:
                return new DERGeneralizedTime(bArr);
            case DERTags.VISIBLE_STRING:
                return new DERVisibleString(bArr);
            case DERTags.GENERAL_STRING:
                return new DERGeneralString(bArr);
            case DERTags.UNIVERSAL_STRING:
                return new DERUniversalString(bArr);
            case DERTags.BMP_STRING:
                return new DERBMPString(bArr);
            case 48:
                BERInputStream bERInputStream = new BERInputStream(new ByteArrayInputStream(bArr));
                DERConstructedSequence dERConstructedSequence = new DERConstructedSequence();
                while (true) {
                    try {
                        dERConstructedSequence.addObject(bERInputStream.readObject());
                    } catch (EOFException e) {
                        return dERConstructedSequence;
                    }
                }
            case 49:
                BERInputStream bERInputStream2 = new BERInputStream(new ByteArrayInputStream(bArr));
                ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
                while (true) {
                    try {
                        aSN1EncodableVector.add(bERInputStream2.readObject());
                    } catch (EOFException e2) {
                        return new DERConstructedSet(aSN1EncodableVector);
                    }
                }
            default:
                if ((i & 128) == 0) {
                    return new DERUnknownTag(i, bArr);
                }
                if ((i & 31) == 31) {
                    throw new IOException("unsupported high tag encountered");
                } else if (bArr.length == 0) {
                    return (i & 32) == 0 ? new DERTaggedObject(false, i & 31, new DERNull()) : new DERTaggedObject(false, i & 31, new DERConstructedSequence());
                } else {
                    if ((i & 32) == 0) {
                        return new DERTaggedObject(false, i & 31, new DEROctetString(bArr));
                    }
                    BERInputStream bERInputStream3 = new BERInputStream(new ByteArrayInputStream(bArr));
                    DERObject readObject = bERInputStream3.readObject();
                    if (bERInputStream3.available() == 0) {
                        return new DERTaggedObject(i & 31, readObject);
                    }
                    DERConstructedSequence dERConstructedSequence2 = new DERConstructedSequence();
                    dERConstructedSequence2.addObject(readObject);
                    while (true) {
                        try {
                            dERConstructedSequence2.addObject(bERInputStream3.readObject());
                        } catch (EOFException e3) {
                            return new DERTaggedObject(false, i & 31, dERConstructedSequence2);
                        }
                    }
                }
        }
    }

    /* access modifiers changed from: protected */
    public void readFully(byte[] bArr) throws IOException {
        int length = bArr.length;
        if (length != 0) {
            while (length > 0) {
                int read = read(bArr, bArr.length - length, length);
                if (read < 0) {
                    throw new EOFException("unexpected end of stream");
                }
                length -= read;
            }
        }
    }

    /* access modifiers changed from: protected */
    public int readLength() throws IOException {
        int i = 0;
        int read = read();
        if (read < 0) {
            throw new IOException("EOF found when length expected");
        } else if (read == 128) {
            return -1;
        } else {
            if (read <= 127) {
                return read;
            }
            int i2 = read & 127;
            if (i2 > 4) {
                throw new IOException("DER length more than 4 bytes");
            }
            for (int i3 = 0; i3 < i2; i3++) {
                int read2 = read();
                if (read2 < 0) {
                    throw new IOException("EOF found reading length");
                }
                i = (i << 8) + read2;
            }
            if (i >= 0) {
                return i;
            }
            throw new IOException("corrupted stream - negative length found");
        }
    }

    public DERObject readObject() throws IOException {
        int read = read();
        if (read == -1) {
            throw new EOFException();
        }
        byte[] bArr = new byte[readLength()];
        readFully(bArr);
        return buildObject(read, bArr);
    }
}
