package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.OutputStream;

public class BEROctetStringGenerator extends BERGenerator {

    private class BufferedBEROctetStream extends OutputStream {
        private byte[] _buf;
        private int _off = 0;

        BufferedBEROctetStream(byte[] bArr) {
            this._buf = bArr;
        }

        public void close() throws IOException {
            if (this._off != 0) {
                byte[] bArr = new byte[this._off];
                System.arraycopy(this._buf, 0, bArr, 0, this._off);
                BEROctetStringGenerator.this._out.write(new DEROctetString(bArr).getEncoded());
            }
            BEROctetStringGenerator.this.writeBEREnd();
        }

        public void write(int i) throws IOException {
            byte[] bArr = this._buf;
            int i2 = this._off;
            this._off = i2 + 1;
            bArr[i2] = (byte) i;
            if (this._off == this._buf.length) {
                BEROctetStringGenerator.this._out.write(new DEROctetString(this._buf).getEncoded());
                this._off = 0;
            }
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            int i3 = i2;
            int i4 = i;
            while (i3 > 0) {
                int min = Math.min(i3, this._buf.length - this._off);
                System.arraycopy(bArr, i4, this._buf, this._off, min);
                this._off += min;
                if (this._off >= this._buf.length) {
                    BEROctetStringGenerator.this._out.write(new DEROctetString(this._buf).getEncoded());
                    this._off = 0;
                    i4 += min;
                    i3 -= min;
                } else {
                    return;
                }
            }
        }
    }

    public BEROctetStringGenerator(OutputStream outputStream) throws IOException {
        super(outputStream);
        writeBERHeader(36);
    }

    public BEROctetStringGenerator(OutputStream outputStream, int i, boolean z) throws IOException {
        super(outputStream, i, z);
        writeBERHeader(36);
    }

    public OutputStream getOctetOutputStream() {
        return getOctetOutputStream(new byte[1000]);
    }

    public OutputStream getOctetOutputStream(byte[] bArr) {
        return new BufferedBEROctetStream(bArr);
    }
}
