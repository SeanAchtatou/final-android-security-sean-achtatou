package org.bouncycastle.asn1.cmp;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObject;

public class PollReqContent extends ASN1Encodable {
    private ASN1Sequence content;

    private PollReqContent(ASN1Sequence aSN1Sequence) {
        this.content = aSN1Sequence;
    }

    public static PollReqContent getInstance(Object obj) {
        if (obj instanceof PollReqContent) {
            return (PollReqContent) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new PollReqContent((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("Invalid object: " + obj.getClass().getName());
    }

    private DERInteger[] seqenceToDERIntegerArray(ASN1Sequence aSN1Sequence) {
        DERInteger[] dERIntegerArr = new DERInteger[aSN1Sequence.size()];
        for (int i = 0; i != dERIntegerArr.length; i++) {
            dERIntegerArr[i] = DERInteger.getInstance(aSN1Sequence.getObjectAt(i));
        }
        return dERIntegerArr;
    }

    public DERInteger[][] getCertReqIds() {
        DERInteger[][] dERIntegerArr = new DERInteger[this.content.size()][];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 == dERIntegerArr.length) {
                return dERIntegerArr;
            }
            dERIntegerArr[i2] = seqenceToDERIntegerArray((ASN1Sequence) this.content.getObjectAt(i2));
            i = i2 + 1;
        }
    }

    public DERObject toASN1Object() {
        return this.content;
    }
}
