package org.bouncycastle.asn1.cmp;

import com.yhiker.config.GuideConfig;
import org.bouncycastle.asn1.ASN1Choice;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DERTags;
import org.bouncycastle.asn1.crmf.CertReqMessages;
import org.bouncycastle.asn1.pkcs.CertificationRequest;

public class PKIBody extends ASN1Encodable implements ASN1Choice {
    private ASN1Encodable body;
    private int tagNo;

    private PKIBody(ASN1TaggedObject aSN1TaggedObject) {
        this.tagNo = aSN1TaggedObject.getTagNo();
        switch (aSN1TaggedObject.getTagNo()) {
            case 0:
                this.body = CertReqMessages.getInstance(aSN1TaggedObject.getObject());
                return;
            case 1:
                this.body = CertRepMessage.getInstance(aSN1TaggedObject.getObject());
                return;
            case 2:
                this.body = CertReqMessages.getInstance(aSN1TaggedObject.getObject());
                return;
            case 3:
                this.body = CertRepMessage.getInstance(aSN1TaggedObject.getObject());
                return;
            case 4:
                this.body = CertificationRequest.getInstance(aSN1TaggedObject.getObject());
                return;
            case 5:
                this.body = POPODecKeyChallContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case 6:
                this.body = POPODecKeyRespContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case 7:
                this.body = CertReqMessages.getInstance(aSN1TaggedObject.getObject());
                return;
            case 8:
                this.body = CertRepMessage.getInstance(aSN1TaggedObject.getObject());
                return;
            case 9:
                this.body = CertReqMessages.getInstance(aSN1TaggedObject.getObject());
                return;
            case 10:
                this.body = KeyRecRepContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case CertStatus.UNREVOKED /*11*/:
                this.body = RevReqContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case 12:
                this.body = RevRepContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case 13:
                this.body = CertReqMessages.getInstance(aSN1TaggedObject.getObject());
                return;
            case 14:
                this.body = CertRepMessage.getInstance(aSN1TaggedObject.getObject());
                return;
            case GuideConfig.satelliteSnr_minlimit:
                this.body = CAKeyUpdAnnContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case 16:
                this.body = CMPCertificate.getInstance(aSN1TaggedObject.getObject());
                return;
            case 17:
                this.body = RevAnnContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case DERTags.NUMERIC_STRING:
                this.body = CRLAnnContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case DERTags.PRINTABLE_STRING:
                this.body = PKIConfirmContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case 20:
                this.body = PKIMessages.getInstance(aSN1TaggedObject.getObject());
                return;
            case DERTags.VIDEOTEX_STRING:
                this.body = GenMsgContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case DERTags.IA5_STRING:
                this.body = GenRepContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case DERTags.UTC_TIME:
                this.body = ErrorMsgContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case 24:
                this.body = CertConfirmContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case DERTags.GRAPHIC_STRING:
                this.body = PollReqContent.getInstance(aSN1TaggedObject.getObject());
                return;
            case DERTags.VISIBLE_STRING:
                this.body = PollRepContent.getInstance(aSN1TaggedObject.getObject());
                return;
            default:
                throw new IllegalArgumentException("unknown tag number: " + aSN1TaggedObject.getTagNo());
        }
    }

    public static PKIBody getInstance(Object obj) {
        if (obj instanceof PKIBody) {
            return (PKIBody) obj;
        }
        if (obj instanceof ASN1TaggedObject) {
            return new PKIBody((ASN1TaggedObject) obj);
        }
        throw new IllegalArgumentException("Invalid object: " + obj.getClass().getName());
    }

    public DERObject toASN1Object() {
        return new DERTaggedObject(true, this.tagNo, this.body);
    }
}
