package org.bouncycastle.asn1;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ASN1StreamParser {
    private final InputStream _in;
    private final int _limit;

    public ASN1StreamParser(InputStream inputStream) {
        this(inputStream, Integer.MAX_VALUE);
    }

    public ASN1StreamParser(InputStream inputStream, int i) {
        this._in = inputStream;
        this._limit = i;
    }

    public ASN1StreamParser(byte[] bArr) {
        this(new ByteArrayInputStream(bArr), bArr.length);
    }

    private void set00Check(boolean z) {
        if (this._in instanceof IndefiniteLengthInputStream) {
            ((IndefiniteLengthInputStream) this._in).setEofOn00(z);
        }
    }

    public DEREncodable readObject() throws IOException {
        int read = this._in.read();
        if (read == -1) {
            return null;
        }
        set00Check(false);
        int readTagNumber = ASN1InputStream.readTagNumber(this._in, read);
        boolean z = (read & 32) != 0;
        int readLength = ASN1InputStream.readLength(this._in, this._limit);
        if (readLength >= 0) {
            DefiniteLengthInputStream definiteLengthInputStream = new DefiniteLengthInputStream(this._in, readLength);
            if ((read & 64) != 0) {
                return new DERApplicationSpecific(z, readTagNumber, definiteLengthInputStream.toByteArray());
            }
            if ((read & 128) != 0) {
                return new BERTaggedObjectParser(read, readTagNumber, definiteLengthInputStream);
            }
            if (z) {
                switch (readTagNumber) {
                    case 4:
                        return new BEROctetStringParser(new ASN1StreamParser(definiteLengthInputStream));
                    case 16:
                        return new DERSequenceParser(new ASN1StreamParser(definiteLengthInputStream));
                    case 17:
                        return new DERSetParser(new ASN1StreamParser(definiteLengthInputStream));
                    default:
                        return new DERUnknownTag(true, readTagNumber, definiteLengthInputStream.toByteArray());
                }
            } else {
                switch (readTagNumber) {
                    case 4:
                        return new DEROctetStringParser(definiteLengthInputStream);
                    default:
                        return ASN1InputStream.createPrimitiveDERObject(readTagNumber, definiteLengthInputStream.toByteArray());
                }
            }
        } else if (!z) {
            throw new IOException("indefinite length primitive encoding encountered");
        } else {
            IndefiniteLengthInputStream indefiniteLengthInputStream = new IndefiniteLengthInputStream(this._in);
            if ((read & 64) != 0) {
                return new BERApplicationSpecificParser(readTagNumber, new ASN1StreamParser(indefiniteLengthInputStream));
            }
            if ((read & 128) != 0) {
                return new BERTaggedObjectParser(read, readTagNumber, indefiniteLengthInputStream);
            }
            ASN1StreamParser aSN1StreamParser = new ASN1StreamParser(indefiniteLengthInputStream);
            switch (readTagNumber) {
                case 4:
                    return new BEROctetStringParser(aSN1StreamParser);
                case 16:
                    return new BERSequenceParser(aSN1StreamParser);
                case 17:
                    return new BERSetParser(aSN1StreamParser);
                default:
                    throw new IOException("unknown BER object encountered");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ASN1EncodableVector readVector() throws IOException {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        while (true) {
            DEREncodable readObject = readObject();
            if (readObject == null) {
                return aSN1EncodableVector;
            }
            aSN1EncodableVector.add(readObject.getDERObject());
        }
    }
}
