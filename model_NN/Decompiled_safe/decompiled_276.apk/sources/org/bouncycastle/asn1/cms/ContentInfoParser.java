package org.bouncycastle.asn1.cms;

import java.io.IOException;
import org.bouncycastle.asn1.ASN1SequenceParser;
import org.bouncycastle.asn1.ASN1TaggedObjectParser;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERObjectIdentifier;

public class ContentInfoParser {
    private ASN1TaggedObjectParser content;
    private DERObjectIdentifier contentType;

    public ContentInfoParser(ASN1SequenceParser aSN1SequenceParser) throws IOException {
        this.contentType = (DERObjectIdentifier) aSN1SequenceParser.readObject();
        this.content = (ASN1TaggedObjectParser) aSN1SequenceParser.readObject();
    }

    public DEREncodable getContent(int i) throws IOException {
        if (this.content != null) {
            return this.content.getObjectParser(i, true);
        }
        return null;
    }

    public DERObjectIdentifier getContentType() {
        return this.contentType;
    }
}
