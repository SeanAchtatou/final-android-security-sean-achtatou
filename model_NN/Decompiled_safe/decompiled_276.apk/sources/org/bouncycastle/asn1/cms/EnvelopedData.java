package org.bouncycastle.asn1.cms;

import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERTaggedObject;

public class EnvelopedData extends ASN1Encodable {
    private EncryptedContentInfo encryptedContentInfo;
    private OriginatorInfo originatorInfo;
    private ASN1Set recipientInfos;
    private ASN1Set unprotectedAttrs;
    private DERInteger version;

    public EnvelopedData(ASN1Sequence aSN1Sequence) {
        int i;
        int i2 = 0 + 1;
        this.version = (DERInteger) aSN1Sequence.getObjectAt(0);
        int i3 = i2 + 1;
        DEREncodable objectAt = aSN1Sequence.getObjectAt(i2);
        if (objectAt instanceof ASN1TaggedObject) {
            this.originatorInfo = OriginatorInfo.getInstance((ASN1TaggedObject) objectAt, false);
            i = i3 + 1;
            objectAt = aSN1Sequence.getObjectAt(i3);
        } else {
            i = i3;
        }
        this.recipientInfos = ASN1Set.getInstance(objectAt);
        int i4 = i + 1;
        this.encryptedContentInfo = EncryptedContentInfo.getInstance(aSN1Sequence.getObjectAt(i));
        if (aSN1Sequence.size() > i4) {
            this.unprotectedAttrs = ASN1Set.getInstance((ASN1TaggedObject) aSN1Sequence.getObjectAt(i4), false);
        }
    }

    public EnvelopedData(OriginatorInfo originatorInfo2, ASN1Set aSN1Set, EncryptedContentInfo encryptedContentInfo2, ASN1Set aSN1Set2) {
        if (originatorInfo2 == null && aSN1Set2 == null) {
            this.version = new DERInteger(0);
            Enumeration objects = aSN1Set.getObjects();
            while (true) {
                if (objects.hasMoreElements()) {
                    if (!RecipientInfo.getInstance(objects.nextElement()).getVersion().equals(this.version)) {
                        this.version = new DERInteger(2);
                        break;
                    }
                } else {
                    break;
                }
            }
        } else {
            this.version = new DERInteger(2);
        }
        this.originatorInfo = originatorInfo2;
        this.recipientInfos = aSN1Set;
        this.encryptedContentInfo = encryptedContentInfo2;
        this.unprotectedAttrs = aSN1Set2;
    }

    public static EnvelopedData getInstance(Object obj) {
        if (obj == null || (obj instanceof EnvelopedData)) {
            return (EnvelopedData) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new EnvelopedData((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("Invalid EnvelopedData: " + obj.getClass().getName());
    }

    public static EnvelopedData getInstance(ASN1TaggedObject aSN1TaggedObject, boolean z) {
        return getInstance(ASN1Sequence.getInstance(aSN1TaggedObject, z));
    }

    public EncryptedContentInfo getEncryptedContentInfo() {
        return this.encryptedContentInfo;
    }

    public OriginatorInfo getOriginatorInfo() {
        return this.originatorInfo;
    }

    public ASN1Set getRecipientInfos() {
        return this.recipientInfos;
    }

    public ASN1Set getUnprotectedAttrs() {
        return this.unprotectedAttrs;
    }

    public DERInteger getVersion() {
        return this.version;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        aSN1EncodableVector.add(this.version);
        if (this.originatorInfo != null) {
            aSN1EncodableVector.add(new DERTaggedObject(false, 0, this.originatorInfo));
        }
        aSN1EncodableVector.add(this.recipientInfos);
        aSN1EncodableVector.add(this.encryptedContentInfo);
        if (this.unprotectedAttrs != null) {
            aSN1EncodableVector.add(new DERTaggedObject(false, 1, this.unprotectedAttrs));
        }
        return new BERSequence(aSN1EncodableVector);
    }
}
