package org.bouncycastle.asn1.cms;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.DERObjectIdentifier;

public class AttributeTable {
    private Hashtable attributes = new Hashtable();

    public AttributeTable(Hashtable hashtable) {
        this.attributes = copyTable(hashtable);
    }

    public AttributeTable(ASN1Set aSN1Set) {
        for (int i = 0; i != aSN1Set.size(); i++) {
            Attribute instance = Attribute.getInstance(aSN1Set.getObjectAt(i));
            addAttribute(instance.getAttrType(), instance);
        }
    }

    public AttributeTable(DEREncodableVector dEREncodableVector) {
        for (int i = 0; i != dEREncodableVector.size(); i++) {
            Attribute instance = Attribute.getInstance(dEREncodableVector.get(i));
            addAttribute(instance.getAttrType(), instance);
        }
    }

    private void addAttribute(DERObjectIdentifier dERObjectIdentifier, Attribute attribute) {
        Vector vector;
        Object obj = this.attributes.get(dERObjectIdentifier);
        if (obj == null) {
            this.attributes.put(dERObjectIdentifier, attribute);
            return;
        }
        if (obj instanceof Attribute) {
            Vector vector2 = new Vector();
            vector2.addElement(obj);
            vector2.addElement(attribute);
            vector = vector2;
        } else {
            vector = (Vector) obj;
            vector.addElement(attribute);
        }
        this.attributes.put(dERObjectIdentifier, vector);
    }

    private Hashtable copyTable(Hashtable hashtable) {
        Hashtable hashtable2 = new Hashtable();
        Enumeration keys = hashtable.keys();
        while (keys.hasMoreElements()) {
            Object nextElement = keys.nextElement();
            hashtable2.put(nextElement, hashtable.get(nextElement));
        }
        return hashtable2;
    }

    public Attribute get(DERObjectIdentifier dERObjectIdentifier) {
        Object obj = this.attributes.get(dERObjectIdentifier);
        return obj instanceof Vector ? (Attribute) ((Vector) obj).elementAt(0) : (Attribute) obj;
    }

    public ASN1EncodableVector getAll(DERObjectIdentifier dERObjectIdentifier) {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        Object obj = this.attributes.get(dERObjectIdentifier);
        if (obj instanceof Vector) {
            Enumeration elements = ((Vector) obj).elements();
            while (elements.hasMoreElements()) {
                aSN1EncodableVector.add((Attribute) elements.nextElement());
            }
        } else if (obj != null) {
            aSN1EncodableVector.add((Attribute) obj);
        }
        return aSN1EncodableVector;
    }

    public ASN1EncodableVector toASN1EncodableVector() {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        Enumeration elements = this.attributes.elements();
        while (elements.hasMoreElements()) {
            Object nextElement = elements.nextElement();
            if (nextElement instanceof Vector) {
                Enumeration elements2 = ((Vector) nextElement).elements();
                while (elements2.hasMoreElements()) {
                    aSN1EncodableVector.add(Attribute.getInstance(elements2.nextElement()));
                }
            } else {
                aSN1EncodableVector.add(Attribute.getInstance(nextElement));
            }
        }
        return aSN1EncodableVector;
    }

    public Hashtable toHashtable() {
        return copyTable(this.attributes);
    }
}
