package org.bouncycastle.asn1;

import java.io.IOException;

public abstract class ASN1Object extends DERObject {
    public static ASN1Object fromByteArray(byte[] bArr) throws IOException {
        return (ASN1Object) new ASN1InputStream(bArr).readObject();
    }

    /* access modifiers changed from: package-private */
    public abstract boolean asn1Equals(DERObject dERObject);

    /* access modifiers changed from: package-private */
    public abstract void encode(DEROutputStream dEROutputStream) throws IOException;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof DEREncodable) && asn1Equals(((DEREncodable) obj).getDERObject());
    }

    public abstract int hashCode();
}
