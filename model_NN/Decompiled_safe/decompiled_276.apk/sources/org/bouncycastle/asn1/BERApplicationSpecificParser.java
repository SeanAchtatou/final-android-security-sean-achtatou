package org.bouncycastle.asn1;

import java.io.IOException;

public class BERApplicationSpecificParser implements ASN1ApplicationSpecificParser {
    private final ASN1StreamParser parser;
    private final int tag;

    BERApplicationSpecificParser(int i, ASN1StreamParser aSN1StreamParser) {
        this.tag = i;
        this.parser = aSN1StreamParser;
    }

    public DERObject getDERObject() {
        try {
            return new BERApplicationSpecific(this.tag, this.parser.readVector());
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    public DEREncodable readObject() throws IOException {
        return this.parser.readObject();
    }
}
