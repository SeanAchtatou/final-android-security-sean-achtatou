package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.InputStream;
import org.bouncycastle.util.io.Streams;

public class BEROctetStringParser implements ASN1OctetStringParser {
    private ASN1StreamParser _parser;

    protected BEROctetStringParser(ASN1ObjectParser aSN1ObjectParser) {
        this._parser = aSN1ObjectParser._aIn;
    }

    BEROctetStringParser(ASN1StreamParser aSN1StreamParser) {
        this._parser = aSN1StreamParser;
    }

    public DERObject getDERObject() {
        try {
            return new BERConstructedOctetString(Streams.readAll(getOctetStream()));
        } catch (IOException e) {
            throw new IllegalStateException("IOException converting stream to byte array: " + e.getMessage());
        }
    }

    public InputStream getOctetStream() {
        return new ConstructedOctetStream(this._parser);
    }
}
