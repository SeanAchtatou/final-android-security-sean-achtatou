package org.bouncycastle.crypto.tls;

import java.io.IOException;

public abstract class TlsCipherSuite {
    protected static final short KE_DHE_DSS = 3;
    protected static final short KE_DHE_DSS_EXPORT = 4;
    protected static final short KE_DHE_RSA = 5;
    protected static final short KE_DHE_RSA_EXPORT = 6;
    protected static final short KE_DH_DSS = 7;
    protected static final short KE_DH_RSA = 8;
    protected static final short KE_DH_anon = 9;
    protected static final short KE_RSA = 1;
    protected static final short KE_RSA_EXPORT = 2;

    /* access modifiers changed from: protected */
    public abstract byte[] decodeCiphertext(short s, byte[] bArr, int i, int i2, TlsProtocolHandler tlsProtocolHandler) throws IOException;

    /* access modifiers changed from: protected */
    public abstract byte[] encodePlaintext(short s, byte[] bArr, int i, int i2);

    /* access modifiers changed from: protected */
    public abstract short getKeyExchangeAlgorithm();

    /* access modifiers changed from: protected */
    public abstract void init(byte[] bArr, byte[] bArr2, byte[] bArr3);
}
