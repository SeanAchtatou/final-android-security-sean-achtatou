package org.bouncycastle.crypto.tls;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.asn1.DERTags;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509CertificateStructure;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.agreement.DHBasicAgreement;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSABlindedEngine;
import org.bouncycastle.crypto.generators.DHBasicKeyPairGenerator;
import org.bouncycastle.crypto.io.SignerInputStream;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.DHKeyGenerationParameters;
import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import org.bouncycastle.crypto.params.DSAPublicKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.prng.ThreadedSeedGenerator;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.util.BigIntegers;

public class TlsProtocolHandler {
    protected static final short AL_fatal = 2;
    protected static final short AL_warning = 1;
    protected static final short AP_access_denied = 49;
    protected static final short AP_bad_certificate = 42;
    protected static final short AP_bad_record_mac = 20;
    protected static final short AP_certificate_expired = 45;
    protected static final short AP_certificate_revoked = 44;
    protected static final short AP_certificate_unknown = 46;
    protected static final short AP_close_notify = 0;
    protected static final short AP_decode_error = 50;
    protected static final short AP_decompression_failure = 30;
    protected static final short AP_decrypt_error = 51;
    protected static final short AP_decryption_failed = 21;
    protected static final short AP_export_restriction = 60;
    protected static final short AP_handshake_failure = 40;
    protected static final short AP_illegal_parameter = 47;
    protected static final short AP_insufficient_security = 71;
    protected static final short AP_internal_error = 80;
    protected static final short AP_no_renegotiation = 100;
    protected static final short AP_protocol_version = 70;
    protected static final short AP_record_overflow = 22;
    protected static final short AP_unexpected_message = 10;
    protected static final short AP_unknown_ca = 48;
    protected static final short AP_unsupported_certificate = 43;
    protected static final short AP_user_canceled = 90;
    private static final short CS_CERTIFICATE_REQUEST_RECEIVED = 5;
    private static final short CS_CLIENT_CHANGE_CIPHER_SPEC_SEND = 8;
    private static final short CS_CLIENT_FINISHED_SEND = 9;
    private static final short CS_CLIENT_HELLO_SEND = 1;
    private static final short CS_CLIENT_KEY_EXCHANGE_SEND = 7;
    private static final short CS_DONE = 11;
    private static final short CS_SERVER_CERTIFICATE_RECEIVED = 3;
    private static final short CS_SERVER_CHANGE_CIPHER_SPEC_RECEIVED = 10;
    private static final short CS_SERVER_HELLO_DONE_RECEIVED = 6;
    private static final short CS_SERVER_HELLO_RECEIVED = 2;
    private static final short CS_SERVER_KEY_EXCHANGE_RECEIVED = 4;
    private static final short HP_CERTIFICATE = 11;
    private static final short HP_CERTIFICATE_REQUEST = 13;
    private static final short HP_CERTIFICATE_VERIFY = 15;
    private static final short HP_CLIENT_HELLO = 1;
    private static final short HP_CLIENT_KEY_EXCHANGE = 16;
    private static final short HP_FINISHED = 20;
    private static final short HP_HELLO_REQUEST = 0;
    private static final short HP_SERVER_HELLO = 2;
    private static final short HP_SERVER_HELLO_DONE = 14;
    private static final short HP_SERVER_KEY_EXCHANGE = 12;
    private static final BigInteger ONE = BigInteger.valueOf(1);
    private static final short RL_ALERT = 21;
    private static final short RL_APPLICATION_DATA = 23;
    private static final short RL_CHANGE_CIPHER_SPEC = 20;
    private static final short RL_HANDSHAKE = 22;
    private static final String TLS_ERROR_MESSAGE = "Internal TLS error, this could be an attack";
    private static final BigInteger TWO = BigInteger.valueOf(2);
    private static final byte[] emptybuf = new byte[0];
    private BigInteger Yc;
    private ByteQueue alertQueue = new ByteQueue();
    private boolean appDataReady = false;
    private ByteQueue applicationDataQueue = new ByteQueue();
    private ByteQueue changeCipherSpecQueue = new ByteQueue();
    private TlsCipherSuite chosenCipherSuite = null;
    private byte[] clientRandom;
    private boolean closed = false;
    private short connection_state;
    private boolean failedWithError = false;
    private ByteQueue handshakeQueue = new ByteQueue();
    private byte[] ms;
    private byte[] pms;
    private SecureRandom random;
    private RecordStream rs;
    private AsymmetricKeyParameter serverPublicKey = null;
    private byte[] serverRandom;
    private TlsInputStream tlsInputStream = null;
    private TlsOuputStream tlsOutputStream = null;
    private CertificateVerifyer verifyer = null;

    public TlsProtocolHandler(InputStream inputStream, OutputStream outputStream) {
        ThreadedSeedGenerator threadedSeedGenerator = new ThreadedSeedGenerator();
        this.random = new SecureRandom();
        this.random.setSeed(threadedSeedGenerator.generateSeed(20, true));
        this.rs = new RecordStream(this, inputStream, outputStream);
    }

    public TlsProtocolHandler(InputStream inputStream, OutputStream outputStream, SecureRandom secureRandom) {
        this.random = secureRandom;
        this.rs = new RecordStream(this, inputStream, outputStream);
    }

    private void processAlert() throws IOException {
        while (this.alertQueue.size() >= 2) {
            byte[] bArr = new byte[2];
            this.alertQueue.read(bArr, 0, 2, 0);
            this.alertQueue.removeData(2);
            short s = (short) bArr[0];
            short s2 = (short) bArr[1];
            if (s == 2) {
                this.failedWithError = true;
                this.closed = true;
                try {
                    this.rs.close();
                } catch (Exception e) {
                }
                throw new IOException(TLS_ERROR_MESSAGE);
            } else if (s2 == 0) {
                failWithError(1, 0);
            }
        }
    }

    private void processApplicationData() {
    }

    private void processChangeCipherSpec() throws IOException {
        while (this.changeCipherSpecQueue.size() > 0) {
            byte[] bArr = new byte[1];
            this.changeCipherSpecQueue.read(bArr, 0, 1, 0);
            this.changeCipherSpecQueue.removeData(1);
            if (bArr[0] != 1) {
                failWithError(2, 10);
            } else if (this.connection_state == 9) {
                this.rs.readSuite = this.rs.writeSuite;
                this.connection_state = 10;
            } else {
                failWithError(2, AP_handshake_failure);
            }
        }
    }

    private void processDHEKeyExchange(ByteArrayInputStream byteArrayInputStream, Signer signer) throws IOException {
        InputStream inputStream;
        if (signer != null) {
            signer.init(false, this.serverPublicKey);
            signer.update(this.clientRandom, 0, this.clientRandom.length);
            signer.update(this.serverRandom, 0, this.serverRandom.length);
            inputStream = new SignerInputStream(byteArrayInputStream, signer);
        } else {
            inputStream = byteArrayInputStream;
        }
        byte[] bArr = new byte[TlsUtils.readUint16(inputStream)];
        TlsUtils.readFully(bArr, inputStream);
        byte[] bArr2 = new byte[TlsUtils.readUint16(inputStream)];
        TlsUtils.readFully(bArr2, inputStream);
        byte[] bArr3 = new byte[TlsUtils.readUint16(inputStream)];
        TlsUtils.readFully(bArr3, inputStream);
        if (signer != null) {
            byte[] bArr4 = new byte[TlsUtils.readUint16(byteArrayInputStream)];
            TlsUtils.readFully(bArr4, byteArrayInputStream);
            if (!signer.verifySignature(bArr4)) {
                failWithError(2, AP_bad_certificate);
            }
        }
        assertEmpty(byteArrayInputStream);
        BigInteger bigInteger = new BigInteger(1, bArr);
        BigInteger bigInteger2 = new BigInteger(1, bArr2);
        BigInteger bigInteger3 = new BigInteger(1, bArr3);
        if (!bigInteger.isProbablePrime(10)) {
            failWithError(2, AP_illegal_parameter);
        }
        if (bigInteger2.compareTo(TWO) < 0 || bigInteger2.compareTo(bigInteger.subtract(TWO)) > 0) {
            failWithError(2, AP_illegal_parameter);
        }
        if (bigInteger3.compareTo(TWO) < 0 || bigInteger3.compareTo(bigInteger.subtract(ONE)) > 0) {
            failWithError(2, AP_illegal_parameter);
        }
        DHParameters dHParameters = new DHParameters(bigInteger, bigInteger2);
        DHBasicKeyPairGenerator dHBasicKeyPairGenerator = new DHBasicKeyPairGenerator();
        dHBasicKeyPairGenerator.init(new DHKeyGenerationParameters(this.random, dHParameters));
        AsymmetricCipherKeyPair generateKeyPair = dHBasicKeyPairGenerator.generateKeyPair();
        this.Yc = ((DHPublicKeyParameters) generateKeyPair.getPublic()).getY();
        DHBasicAgreement dHBasicAgreement = new DHBasicAgreement();
        dHBasicAgreement.init(generateKeyPair.getPrivate());
        this.pms = BigIntegers.asUnsignedByteArray(dHBasicAgreement.calculateAgreement(new DHPublicKeyParameters(bigInteger3, dHParameters)));
    }

    private void processHandshake() throws IOException {
        boolean z;
        do {
            if (this.handshakeQueue.size() >= 4) {
                byte[] bArr = new byte[4];
                this.handshakeQueue.read(bArr, 0, 4, 0);
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
                short readUint8 = TlsUtils.readUint8(byteArrayInputStream);
                int readUint24 = TlsUtils.readUint24(byteArrayInputStream);
                if (this.handshakeQueue.size() >= readUint24 + 4) {
                    byte[] bArr2 = new byte[readUint24];
                    this.handshakeQueue.read(bArr2, 0, readUint24, 4);
                    this.handshakeQueue.removeData(readUint24 + 4);
                    if (readUint8 != 20) {
                        this.rs.hash1.update(bArr, 0, 4);
                        this.rs.hash2.update(bArr, 0, 4);
                        this.rs.hash1.update(bArr2, 0, readUint24);
                        this.rs.hash2.update(bArr2, 0, readUint24);
                    }
                    ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(bArr2);
                    switch (readUint8) {
                        case 2:
                            switch (this.connection_state) {
                                case 1:
                                    TlsUtils.checkVersion(byteArrayInputStream2, this);
                                    this.serverRandom = new byte[32];
                                    TlsUtils.readFully(this.serverRandom, byteArrayInputStream2);
                                    TlsUtils.readFully(new byte[TlsUtils.readUint8(byteArrayInputStream2)], byteArrayInputStream2);
                                    this.chosenCipherSuite = TlsCipherSuiteManager.getCipherSuite(TlsUtils.readUint16(byteArrayInputStream2), this);
                                    if (TlsUtils.readUint8(byteArrayInputStream2) != 0) {
                                        failWithError(2, AP_illegal_parameter);
                                    }
                                    assertEmpty(byteArrayInputStream2);
                                    this.connection_state = 2;
                                    z = true;
                                    continue;
                                default:
                                    failWithError(2, 10);
                                    break;
                            }
                        case CertStatus.UNREVOKED /*11*/:
                            switch (this.connection_state) {
                                case 2:
                                    Certificate parse = Certificate.parse(byteArrayInputStream2);
                                    assertEmpty(byteArrayInputStream2);
                                    X509CertificateStructure x509CertificateStructure = parse.certs[0];
                                    try {
                                        this.serverPublicKey = PublicKeyFactory.createKey(x509CertificateStructure.getSubjectPublicKeyInfo());
                                    } catch (RuntimeException e) {
                                        failWithError(2, AP_unsupported_certificate);
                                    }
                                    if (this.serverPublicKey.isPrivate()) {
                                        failWithError(2, AP_internal_error);
                                    }
                                    switch (this.chosenCipherSuite.getKeyExchangeAlgorithm()) {
                                        case 1:
                                            if (!(this.serverPublicKey instanceof RSAKeyParameters)) {
                                                failWithError(2, AP_certificate_unknown);
                                            }
                                            validateKeyUsage(x509CertificateStructure, 32);
                                            break;
                                        case 2:
                                        case 4:
                                        default:
                                            failWithError(2, AP_unsupported_certificate);
                                            break;
                                        case 3:
                                            if (!(this.serverPublicKey instanceof DSAPublicKeyParameters)) {
                                                failWithError(2, AP_certificate_unknown);
                                                break;
                                            }
                                            break;
                                        case 5:
                                            if (!(this.serverPublicKey instanceof RSAKeyParameters)) {
                                                failWithError(2, AP_certificate_unknown);
                                            }
                                            validateKeyUsage(x509CertificateStructure, 128);
                                            break;
                                    }
                                    if (!this.verifyer.isValid(parse.getCerts())) {
                                        failWithError(2, AP_user_canceled);
                                        break;
                                    }
                                    break;
                                default:
                                    failWithError(2, 10);
                                    break;
                            }
                            this.connection_state = CS_SERVER_CERTIFICATE_RECEIVED;
                            z = true;
                            continue;
                        case 12:
                            switch (this.connection_state) {
                                case 3:
                                    switch (this.chosenCipherSuite.getKeyExchangeAlgorithm()) {
                                        case 3:
                                            processDHEKeyExchange(byteArrayInputStream2, new TlsDSSSigner());
                                            break;
                                        case 4:
                                        default:
                                            failWithError(2, 10);
                                            break;
                                        case 5:
                                            processDHEKeyExchange(byteArrayInputStream2, new TlsRSASigner());
                                            break;
                                    }
                                default:
                                    failWithError(2, 10);
                                    break;
                            }
                            this.connection_state = CS_SERVER_KEY_EXCHANGE_RECEIVED;
                            z = true;
                            continue;
                        case 13:
                            switch (this.connection_state) {
                                case 3:
                                    if (this.chosenCipherSuite.getKeyExchangeAlgorithm() != 1) {
                                        failWithError(2, 10);
                                    }
                                case 4:
                                    TlsUtils.readFully(new byte[TlsUtils.readUint8(byteArrayInputStream2)], byteArrayInputStream2);
                                    TlsUtils.readFully(new byte[TlsUtils.readUint16(byteArrayInputStream2)], byteArrayInputStream2);
                                    assertEmpty(byteArrayInputStream2);
                                    break;
                                default:
                                    failWithError(2, 10);
                                    break;
                            }
                            this.connection_state = CS_CERTIFICATE_REQUEST_RECEIVED;
                            z = true;
                            continue;
                        case 14:
                            switch (this.connection_state) {
                                case 3:
                                    if (this.chosenCipherSuite.getKeyExchangeAlgorithm() != 1) {
                                        failWithError(2, 10);
                                    }
                                case 4:
                                case 5:
                                    assertEmpty(byteArrayInputStream2);
                                    boolean z2 = this.connection_state == 5;
                                    this.connection_state = CS_SERVER_HELLO_DONE_RECEIVED;
                                    if (z2) {
                                        sendClientCertificate();
                                    }
                                    switch (this.chosenCipherSuite.getKeyExchangeAlgorithm()) {
                                        case 1:
                                            this.pms = new byte[48];
                                            this.random.nextBytes(this.pms);
                                            this.pms[0] = 3;
                                            this.pms[1] = 1;
                                            PKCS1Encoding pKCS1Encoding = new PKCS1Encoding(new RSABlindedEngine());
                                            pKCS1Encoding.init(true, new ParametersWithRandom(this.serverPublicKey, this.random));
                                            byte[] bArr3 = null;
                                            try {
                                                bArr3 = pKCS1Encoding.processBlock(this.pms, 0, this.pms.length);
                                            } catch (InvalidCipherTextException e2) {
                                                failWithError(2, AP_internal_error);
                                            }
                                            sendClientKeyExchange(bArr3);
                                            break;
                                        case 2:
                                        case 4:
                                        default:
                                            failWithError(2, 10);
                                            break;
                                        case 3:
                                        case 5:
                                            sendClientKeyExchange(BigIntegers.asUnsignedByteArray(this.Yc));
                                            break;
                                    }
                                    this.connection_state = CS_CLIENT_KEY_EXCHANGE_SEND;
                                    byte[] bArr4 = {1};
                                    this.rs.writeMessage(20, bArr4, 0, bArr4.length);
                                    this.connection_state = CS_CLIENT_CHANGE_CIPHER_SPEC_SEND;
                                    this.ms = new byte[48];
                                    byte[] bArr5 = new byte[(this.clientRandom.length + this.serverRandom.length)];
                                    System.arraycopy(this.clientRandom, 0, bArr5, 0, this.clientRandom.length);
                                    System.arraycopy(this.serverRandom, 0, bArr5, this.clientRandom.length, this.serverRandom.length);
                                    TlsUtils.PRF(this.pms, TlsUtils.toByteArray("master secret"), bArr5, this.ms);
                                    this.rs.writeSuite = this.chosenCipherSuite;
                                    this.rs.writeSuite.init(this.ms, this.clientRandom, this.serverRandom);
                                    byte[] bArr6 = new byte[12];
                                    byte[] bArr7 = new byte[36];
                                    this.rs.hash1.doFinal(bArr7, 0);
                                    TlsUtils.PRF(this.ms, TlsUtils.toByteArray("client finished"), bArr7, bArr6);
                                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                    TlsUtils.writeUint8(20, byteArrayOutputStream);
                                    TlsUtils.writeUint24(12, byteArrayOutputStream);
                                    byteArrayOutputStream.write(bArr6);
                                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                                    this.rs.writeMessage(22, byteArray, 0, byteArray.length);
                                    this.connection_state = CS_CLIENT_FINISHED_SEND;
                                    z = true;
                                    continue;
                                default:
                                    failWithError(2, AP_handshake_failure);
                                    break;
                            }
                        case 20:
                            switch (this.connection_state) {
                                case 10:
                                    byte[] bArr8 = new byte[12];
                                    TlsUtils.readFully(bArr8, byteArrayInputStream2);
                                    assertEmpty(byteArrayInputStream2);
                                    byte[] bArr9 = new byte[12];
                                    byte[] bArr10 = new byte[36];
                                    this.rs.hash2.doFinal(bArr10, 0);
                                    TlsUtils.PRF(this.ms, TlsUtils.toByteArray("server finished"), bArr10, bArr9);
                                    for (int i = 0; i < bArr8.length; i++) {
                                        if (bArr8[i] != bArr9[i]) {
                                            failWithError(2, AP_handshake_failure);
                                        }
                                    }
                                    this.connection_state = 11;
                                    this.appDataReady = true;
                                    z = true;
                                    continue;
                                default:
                                    failWithError(2, 10);
                                    break;
                            }
                        default:
                            failWithError(2, 10);
                            break;
                    }
                }
            }
            z = false;
            continue;
        } while (z);
    }

    private void sendClientCertificate() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8(11, byteArrayOutputStream);
        TlsUtils.writeUint24(3, byteArrayOutputStream);
        TlsUtils.writeUint24(0, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.rs.writeMessage(22, byteArray, 0, byteArray.length);
    }

    private void sendClientKeyExchange(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8(HP_CLIENT_KEY_EXCHANGE, byteArrayOutputStream);
        TlsUtils.writeUint24(bArr.length + 2, byteArrayOutputStream);
        TlsUtils.writeUint16(bArr.length, byteArrayOutputStream);
        byteArrayOutputStream.write(bArr);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.rs.writeMessage(22, byteArray, 0, byteArray.length);
    }

    private void validateKeyUsage(X509CertificateStructure x509CertificateStructure, int i) throws IOException {
        X509Extension extension;
        X509Extensions extensions = x509CertificateStructure.getTBSCertificate().getExtensions();
        if (extensions != null && (extension = extensions.getExtension(X509Extensions.KeyUsage)) != null && (KeyUsage.getInstance(extension).getBytes()[0] & 255 & i) != i) {
            failWithError(2, AP_certificate_unknown);
        }
    }

    /* access modifiers changed from: protected */
    public void assertEmpty(ByteArrayInputStream byteArrayInputStream) throws IOException {
        if (byteArrayInputStream.available() > 0) {
            failWithError(2, AP_decode_error);
        }
    }

    public void close() throws IOException {
        if (!this.closed) {
            failWithError(1, 0);
        }
    }

    public void connect(CertificateVerifyer certificateVerifyer) throws IOException {
        this.verifyer = certificateVerifyer;
        this.clientRandom = new byte[32];
        this.random.nextBytes(this.clientRandom);
        int currentTimeMillis = (int) (System.currentTimeMillis() / 1000);
        this.clientRandom[0] = (byte) (currentTimeMillis >> 24);
        this.clientRandom[1] = (byte) (currentTimeMillis >> 16);
        this.clientRandom[2] = (byte) (currentTimeMillis >> 8);
        this.clientRandom[3] = (byte) currentTimeMillis;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeVersion(byteArrayOutputStream);
        byteArrayOutputStream.write(this.clientRandom);
        TlsUtils.writeUint8(0, byteArrayOutputStream);
        TlsCipherSuiteManager.writeCipherSuites(byteArrayOutputStream);
        byte[] bArr = {0};
        TlsUtils.writeUint8((short) bArr.length, byteArrayOutputStream);
        byteArrayOutputStream.write(bArr);
        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
        TlsUtils.writeUint8(1, byteArrayOutputStream2);
        TlsUtils.writeUint24(byteArrayOutputStream.size(), byteArrayOutputStream2);
        byteArrayOutputStream2.write(byteArrayOutputStream.toByteArray());
        byte[] byteArray = byteArrayOutputStream2.toByteArray();
        this.rs.writeMessage(22, byteArray, 0, byteArray.length);
        this.connection_state = 1;
        while (this.connection_state != 11) {
            this.rs.readData();
        }
        this.tlsInputStream = new TlsInputStream(this);
        this.tlsOutputStream = new TlsOuputStream(this);
    }

    /* access modifiers changed from: protected */
    public void failWithError(short s, short s2) throws IOException {
        if (!this.closed) {
            byte[] bArr = {(byte) s, (byte) s2};
            this.closed = true;
            if (s == 2) {
                this.failedWithError = true;
            }
            this.rs.writeMessage(21, bArr, 0, 2);
            this.rs.close();
            if (s == 2) {
                throw new IOException(TLS_ERROR_MESSAGE);
            }
            return;
        }
        throw new IOException(TLS_ERROR_MESSAGE);
    }

    /* access modifiers changed from: protected */
    public void flush() throws IOException {
        this.rs.flush();
    }

    public InputStream getInputStream() {
        return this.tlsInputStream;
    }

    public OutputStream getOutputStream() {
        return this.tlsOutputStream;
    }

    public TlsInputStream getTlsInputStream() {
        return this.tlsInputStream;
    }

    public TlsOuputStream getTlsOuputStream() {
        return this.tlsOutputStream;
    }

    /* access modifiers changed from: protected */
    public void processData(short s, byte[] bArr, int i, int i2) throws IOException {
        switch (s) {
            case 20:
                this.changeCipherSpecQueue.addData(bArr, i, i2);
                processChangeCipherSpec();
                return;
            case DERTags.VIDEOTEX_STRING:
                this.alertQueue.addData(bArr, i, i2);
                processAlert();
                return;
            case DERTags.IA5_STRING:
                this.handshakeQueue.addData(bArr, i, i2);
                processHandshake();
                return;
            case DERTags.UTC_TIME:
                if (!this.appDataReady) {
                    failWithError(2, 10);
                }
                this.applicationDataQueue.addData(bArr, i, i2);
                processApplicationData();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public int readApplicationData(byte[] bArr, int i, int i2) throws IOException {
        while (this.applicationDataQueue.size() == 0) {
            if (this.failedWithError) {
                throw new IOException(TLS_ERROR_MESSAGE);
            } else if (this.closed) {
                return -1;
            } else {
                try {
                    this.rs.readData();
                } catch (IOException e) {
                    if (!this.closed) {
                        failWithError(2, AP_internal_error);
                    }
                    throw e;
                } catch (RuntimeException e2) {
                    if (!this.closed) {
                        failWithError(2, AP_internal_error);
                    }
                    throw e2;
                }
            }
        }
        int min = Math.min(i2, this.applicationDataQueue.size());
        this.applicationDataQueue.read(bArr, i, min, 0);
        this.applicationDataQueue.removeData(min);
        return min;
    }

    /* access modifiers changed from: protected */
    public void writeData(byte[] bArr, int i, int i2) throws IOException {
        if (this.failedWithError) {
            throw new IOException(TLS_ERROR_MESSAGE);
        } else if (this.closed) {
            throw new IOException("Sorry, connection has been closed, you cannot write more data");
        } else {
            this.rs.writeMessage(RL_APPLICATION_DATA, emptybuf, 0, 0);
            int i3 = i2;
            int i4 = i;
            do {
                int min = Math.min(i3, 16384);
                try {
                    this.rs.writeMessage(RL_APPLICATION_DATA, bArr, i4, min);
                    i4 += min;
                    i3 -= min;
                } catch (IOException e) {
                    if (!this.closed) {
                        failWithError(2, AP_internal_error);
                    }
                    throw e;
                } catch (RuntimeException e2) {
                    if (!this.closed) {
                        failWithError(2, AP_internal_error);
                    }
                    throw e2;
                }
            } while (i3 > 0);
        }
    }
}
