package org.bouncycastle.crypto.tls;

public class TlsNullCipherSuite extends TlsCipherSuite {
    /* access modifiers changed from: protected */
    public byte[] decodeCiphertext(short s, byte[] bArr, int i, int i2, TlsProtocolHandler tlsProtocolHandler) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public byte[] encodePlaintext(short s, byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public short getKeyExchangeAlgorithm() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void init(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        throw new TlsRuntimeException("Sorry, init of TLS_NULL_WITH_NULL_NULL is forbidden");
    }
}
