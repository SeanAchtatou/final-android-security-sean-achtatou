package org.bouncycastle.crypto.tls;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.x509.X509CertificateStructure;

public class Certificate {
    protected X509CertificateStructure[] certs;

    private Certificate(X509CertificateStructure[] x509CertificateStructureArr) {
        this.certs = x509CertificateStructureArr;
    }

    protected static Certificate parse(InputStream inputStream) throws IOException {
        int readUint24 = TlsUtils.readUint24(inputStream);
        Vector vector = new Vector();
        while (readUint24 > 0) {
            int readUint242 = TlsUtils.readUint24(inputStream);
            readUint24 -= readUint242 + 3;
            byte[] bArr = new byte[readUint242];
            TlsUtils.readFully(bArr, inputStream);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            vector.addElement(X509CertificateStructure.getInstance(new ASN1InputStream(byteArrayInputStream).readObject()));
            if (byteArrayInputStream.available() > 0) {
                throw new IllegalArgumentException("Sorry, there is garbage data left after the certificate");
            }
        }
        X509CertificateStructure[] x509CertificateStructureArr = new X509CertificateStructure[vector.size()];
        for (int i = 0; i < vector.size(); i++) {
            x509CertificateStructureArr[i] = (X509CertificateStructure) vector.elementAt(i);
        }
        return new Certificate(x509CertificateStructureArr);
    }

    public X509CertificateStructure[] getCerts() {
        X509CertificateStructure[] x509CertificateStructureArr = new X509CertificateStructure[this.certs.length];
        System.arraycopy(this.certs, 0, x509CertificateStructureArr, 0, this.certs.length);
        return x509CertificateStructureArr;
    }
}
