package org.bouncycastle.crypto.tls;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class RecordStream {
    private TlsProtocolHandler handler;
    protected CombinedHash hash1;
    protected CombinedHash hash2;
    private InputStream is;
    private OutputStream os;
    protected TlsCipherSuite readSuite = null;
    protected TlsCipherSuite writeSuite = null;

    protected RecordStream(TlsProtocolHandler tlsProtocolHandler, InputStream inputStream, OutputStream outputStream) {
        this.handler = tlsProtocolHandler;
        this.is = inputStream;
        this.os = outputStream;
        this.hash1 = new CombinedHash();
        this.hash2 = new CombinedHash();
        this.readSuite = new TlsNullCipherSuite();
        this.writeSuite = this.readSuite;
    }

    /* access modifiers changed from: protected */
    public void close() throws IOException {
        IOException e = null;
        try {
            this.is.close();
        } catch (IOException e2) {
            e = e2;
        }
        try {
            this.os.close();
        } catch (IOException e3) {
            e = e3;
        }
        if (e != null) {
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    public byte[] decodeAndVerify(short s, InputStream inputStream, int i) throws IOException {
        byte[] bArr = new byte[i];
        TlsUtils.readFully(bArr, inputStream);
        return this.readSuite.decodeCiphertext(s, bArr, 0, bArr.length, this.handler);
    }

    /* access modifiers changed from: protected */
    public void flush() throws IOException {
        this.os.flush();
    }

    public void readData() throws IOException {
        short readUint8 = TlsUtils.readUint8(this.is);
        TlsUtils.checkVersion(this.is, this.handler);
        byte[] decodeAndVerify = decodeAndVerify(readUint8, this.is, TlsUtils.readUint16(this.is));
        this.handler.processData(readUint8, decodeAndVerify, 0, decodeAndVerify.length);
    }

    /* access modifiers changed from: protected */
    public void writeMessage(short s, byte[] bArr, int i, int i2) throws IOException {
        if (s == 22) {
            this.hash1.update(bArr, i, i2);
            this.hash2.update(bArr, i, i2);
        }
        byte[] encodePlaintext = this.writeSuite.encodePlaintext(s, bArr, i, i2);
        byte[] bArr2 = new byte[(encodePlaintext.length + 5)];
        TlsUtils.writeUint8(s, bArr2, 0);
        TlsUtils.writeUint8(3, bArr2, 1);
        TlsUtils.writeUint8(1, bArr2, 2);
        TlsUtils.writeUint16(encodePlaintext.length, bArr2, 3);
        System.arraycopy(encodePlaintext, 0, bArr2, 5, encodePlaintext.length);
        this.os.write(bArr2);
        this.os.flush();
    }
}
