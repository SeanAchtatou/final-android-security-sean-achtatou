package org.bouncycastle.crypto.tls;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;

public class TlsMac {
    private HMac mac;
    private long seqNo = 0;

    protected TlsMac(Digest digest, byte[] bArr, int i, int i2) {
        this.mac = new HMac(digest);
        this.mac.init(new KeyParameter(bArr, i, i2));
    }

    /* access modifiers changed from: protected */
    public byte[] calculateMac(short s, byte[] bArr, int i, int i2) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            long j = this.seqNo;
            this.seqNo = 1 + j;
            TlsUtils.writeUint64(j, byteArrayOutputStream);
            TlsUtils.writeUint8(s, byteArrayOutputStream);
            TlsUtils.writeVersion(byteArrayOutputStream);
            TlsUtils.writeUint16(i2, byteArrayOutputStream);
            byteArrayOutputStream.write(bArr, i, i2);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            this.mac.update(byteArray, 0, byteArray.length);
            byte[] bArr2 = new byte[this.mac.getMacSize()];
            this.mac.doFinal(bArr2, 0);
            this.mac.reset();
            return bArr2;
        } catch (IOException e) {
            throw new IllegalStateException("Internal error during mac calculation");
        }
    }

    /* access modifiers changed from: protected */
    public int getSize() {
        return this.mac.getMacSize();
    }
}
