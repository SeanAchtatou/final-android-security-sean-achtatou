package org.bouncycastle.crypto.tls;

import java.io.IOException;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;

public class TlsBlockCipherCipherSuite extends TlsCipherSuite {
    private int cipherKeySize;
    private BlockCipher decryptCipher;
    private BlockCipher encryptCipher;
    private short keyExchange;
    private Digest readDigest;
    private TlsMac readMac;
    private Digest writeDigest;
    private TlsMac writeMac;

    protected TlsBlockCipherCipherSuite(BlockCipher blockCipher, BlockCipher blockCipher2, Digest digest, Digest digest2, int i, short s) {
        this.encryptCipher = blockCipher;
        this.decryptCipher = blockCipher2;
        this.writeDigest = digest;
        this.readDigest = digest2;
        this.cipherKeySize = i;
        this.keyExchange = s;
    }

    private void initCipher(boolean z, BlockCipher blockCipher, byte[] bArr, int i, int i2, int i3) {
        blockCipher.init(z, new ParametersWithIV(new KeyParameter(bArr, i2, i), bArr, i3, blockCipher.getBlockSize()));
    }

    /* access modifiers changed from: protected */
    public byte[] decodeCiphertext(short s, byte[] bArr, int i, int i2, TlsProtocolHandler tlsProtocolHandler) throws IOException {
        boolean z;
        int blockSize = this.decryptCipher.getBlockSize();
        for (int i3 = 0; i3 < i2; i3 += blockSize) {
            this.decryptCipher.processBlock(bArr, i3 + i, bArr, i3 + i);
        }
        byte b = bArr[(i + i2) - 1];
        if (((i + i2) - 1) - b < 0) {
            b = 0;
            z = true;
        } else {
            boolean z2 = false;
            for (int i4 = 0; i4 <= b; i4++) {
                if (bArr[((i + i2) - 1) - i4] != b) {
                    z2 = true;
                }
            }
            z = z2;
        }
        int size = ((i2 - this.readMac.getSize()) - b) - 1;
        byte[] calculateMac = this.readMac.calculateMac(s, bArr, i, size);
        boolean z3 = z;
        for (int i5 = 0; i5 < calculateMac.length; i5++) {
            if (bArr[i + size + i5] != calculateMac[i5]) {
                z3 = true;
            }
        }
        if (z3) {
            tlsProtocolHandler.failWithError(2, 20);
        }
        byte[] bArr2 = new byte[size];
        System.arraycopy(bArr, i, bArr2, 0, size);
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public byte[] encodePlaintext(short s, byte[] bArr, int i, int i2) {
        int blockSize = this.encryptCipher.getBlockSize();
        int size = blockSize - (((this.writeMac.getSize() + i2) + 1) % blockSize);
        int size2 = this.writeMac.getSize() + i2 + size + 1;
        byte[] bArr2 = new byte[size2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        byte[] calculateMac = this.writeMac.calculateMac(s, bArr, i, i2);
        System.arraycopy(calculateMac, 0, bArr2, i2, calculateMac.length);
        int length = calculateMac.length + i2;
        for (int i3 = 0; i3 <= size; i3++) {
            bArr2[i3 + length] = (byte) size;
        }
        for (int i4 = 0; i4 < size2; i4 += blockSize) {
            this.encryptCipher.processBlock(bArr2, i4, bArr2, i4);
        }
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public short getKeyExchangeAlgorithm() {
        return this.keyExchange;
    }

    /* access modifiers changed from: protected */
    public void init(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        byte[] bArr4 = new byte[((this.cipherKeySize * 2) + (this.writeDigest.getDigestSize() * 2) + (this.encryptCipher.getBlockSize() * 2))];
        byte[] bArr5 = new byte[(bArr2.length + bArr3.length)];
        System.arraycopy(bArr2, 0, bArr5, bArr3.length, bArr2.length);
        System.arraycopy(bArr3, 0, bArr5, 0, bArr3.length);
        TlsUtils.PRF(bArr, TlsUtils.toByteArray("key expansion"), bArr5, bArr4);
        this.writeMac = new TlsMac(this.writeDigest, bArr4, 0, this.writeDigest.getDigestSize());
        int digestSize = this.writeDigest.getDigestSize() + 0;
        this.readMac = new TlsMac(this.readDigest, bArr4, digestSize, this.readDigest.getDigestSize());
        int digestSize2 = digestSize + this.readDigest.getDigestSize();
        initCipher(true, this.encryptCipher, bArr4, this.cipherKeySize, digestSize2, digestSize2 + (this.cipherKeySize * 2));
        int i = digestSize2 + this.cipherKeySize;
        initCipher(false, this.decryptCipher, bArr4, this.cipherKeySize, i, this.cipherKeySize + i + this.decryptCipher.getBlockSize());
    }
}
