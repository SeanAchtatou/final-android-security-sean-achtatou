package org.bouncycastle.crypto.tls;

import java.io.IOException;
import java.io.OutputStream;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.engines.DESedeEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;

public class TlsCipherSuiteManager {
    private static final int TLS_DHE_DSS_WITH_3DES_EDE_CBC_SHA = 19;
    private static final int TLS_DHE_DSS_WITH_AES_128_CBC_SHA = 50;
    private static final int TLS_DHE_DSS_WITH_AES_256_CBC_SHA = 56;
    private static final int TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA = 22;
    private static final int TLS_DHE_RSA_WITH_AES_128_CBC_SHA = 51;
    private static final int TLS_DHE_RSA_WITH_AES_256_CBC_SHA = 57;
    private static final int TLS_RSA_WITH_3DES_EDE_CBC_SHA = 10;
    private static final int TLS_RSA_WITH_AES_128_CBC_SHA = 47;
    private static final int TLS_RSA_WITH_AES_256_CBC_SHA = 53;

    protected static TlsCipherSuite getCipherSuite(int i, TlsProtocolHandler tlsProtocolHandler) throws IOException {
        switch (i) {
            case 10:
                return new TlsBlockCipherCipherSuite(new CBCBlockCipher(new DESedeEngine()), new CBCBlockCipher(new DESedeEngine()), new SHA1Digest(), new SHA1Digest(), 24, 1);
            case 19:
                return new TlsBlockCipherCipherSuite(new CBCBlockCipher(new DESedeEngine()), new CBCBlockCipher(new DESedeEngine()), new SHA1Digest(), new SHA1Digest(), 24, 3);
            case 22:
                return new TlsBlockCipherCipherSuite(new CBCBlockCipher(new DESedeEngine()), new CBCBlockCipher(new DESedeEngine()), new SHA1Digest(), new SHA1Digest(), 24, 5);
            case TLS_RSA_WITH_AES_128_CBC_SHA /*47*/:
                return new TlsBlockCipherCipherSuite(new CBCBlockCipher(new AESFastEngine()), new CBCBlockCipher(new AESFastEngine()), new SHA1Digest(), new SHA1Digest(), 16, 1);
            case TLS_DHE_DSS_WITH_AES_128_CBC_SHA /*50*/:
                return new TlsBlockCipherCipherSuite(new CBCBlockCipher(new AESFastEngine()), new CBCBlockCipher(new AESFastEngine()), new SHA1Digest(), new SHA1Digest(), 16, 3);
            case TLS_DHE_RSA_WITH_AES_128_CBC_SHA /*51*/:
                return new TlsBlockCipherCipherSuite(new CBCBlockCipher(new AESFastEngine()), new CBCBlockCipher(new AESFastEngine()), new SHA1Digest(), new SHA1Digest(), 16, 5);
            case TLS_RSA_WITH_AES_256_CBC_SHA /*53*/:
                return new TlsBlockCipherCipherSuite(new CBCBlockCipher(new AESFastEngine()), new CBCBlockCipher(new AESFastEngine()), new SHA1Digest(), new SHA1Digest(), 32, 1);
            case TLS_DHE_DSS_WITH_AES_256_CBC_SHA /*56*/:
                return new TlsBlockCipherCipherSuite(new CBCBlockCipher(new AESFastEngine()), new CBCBlockCipher(new AESFastEngine()), new SHA1Digest(), new SHA1Digest(), 32, 3);
            case TLS_DHE_RSA_WITH_AES_256_CBC_SHA /*57*/:
                return new TlsBlockCipherCipherSuite(new CBCBlockCipher(new AESFastEngine()), new CBCBlockCipher(new AESFastEngine()), new SHA1Digest(), new SHA1Digest(), 32, 5);
            default:
                tlsProtocolHandler.failWithError(2, 40);
                return null;
        }
    }

    protected static void writeCipherSuites(OutputStream outputStream) throws IOException {
        int[] iArr = {TLS_DHE_RSA_WITH_AES_256_CBC_SHA, TLS_DHE_DSS_WITH_AES_256_CBC_SHA, TLS_DHE_RSA_WITH_AES_128_CBC_SHA, TLS_DHE_DSS_WITH_AES_128_CBC_SHA, 22, 19, TLS_RSA_WITH_AES_256_CBC_SHA, TLS_RSA_WITH_AES_128_CBC_SHA, 10};
        TlsUtils.writeUint16(iArr.length * 2, outputStream);
        for (int writeUint16 : iArr) {
            TlsUtils.writeUint16(writeUint16, outputStream);
        }
    }
}
