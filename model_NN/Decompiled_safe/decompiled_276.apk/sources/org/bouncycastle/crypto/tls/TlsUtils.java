package org.bouncycastle.crypto.tls;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;

public class TlsUtils {
    protected static void PRF(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4) {
        int length = (bArr.length + 1) / 2;
        byte[] bArr5 = new byte[length];
        byte[] bArr6 = new byte[length];
        System.arraycopy(bArr, 0, bArr5, 0, length);
        System.arraycopy(bArr, bArr.length - length, bArr6, 0, length);
        byte[] bArr7 = new byte[(bArr2.length + bArr3.length)];
        System.arraycopy(bArr2, 0, bArr7, 0, bArr2.length);
        System.arraycopy(bArr3, 0, bArr7, bArr2.length, bArr3.length);
        byte[] bArr8 = new byte[bArr4.length];
        hmac_hash(new MD5Digest(), bArr5, bArr7, bArr8);
        hmac_hash(new SHA1Digest(), bArr6, bArr7, bArr4);
        for (int i = 0; i < bArr4.length; i++) {
            bArr4[i] = (byte) (bArr4[i] ^ bArr8[i]);
        }
    }

    protected static void checkVersion(InputStream inputStream, TlsProtocolHandler tlsProtocolHandler) throws IOException {
        int read = inputStream.read();
        int read2 = inputStream.read();
        if (read != 3 || read2 != 1) {
            tlsProtocolHandler.failWithError(2, 70);
        }
    }

    protected static void checkVersion(byte[] bArr, TlsProtocolHandler tlsProtocolHandler) throws IOException {
        if (bArr[0] != 3 || bArr[1] != 1) {
            tlsProtocolHandler.failWithError(2, 70);
        }
    }

    private static void hmac_hash(Digest digest, byte[] bArr, byte[] bArr2, byte[] bArr3) {
        HMac hMac = new HMac(digest);
        KeyParameter keyParameter = new KeyParameter(bArr);
        int digestSize = digest.getDigestSize();
        int length = ((bArr3.length + digestSize) - 1) / digestSize;
        byte[] bArr4 = new byte[hMac.getMacSize()];
        byte[] bArr5 = new byte[hMac.getMacSize()];
        int i = 0;
        byte[] bArr6 = bArr2;
        while (i < length) {
            hMac.init(keyParameter);
            hMac.update(bArr6, 0, bArr6.length);
            hMac.doFinal(bArr4, 0);
            hMac.init(keyParameter);
            hMac.update(bArr4, 0, bArr4.length);
            hMac.update(bArr2, 0, bArr2.length);
            hMac.doFinal(bArr5, 0);
            System.arraycopy(bArr5, 0, bArr3, digestSize * i, Math.min(digestSize, bArr3.length - (digestSize * i)));
            i++;
            bArr6 = bArr4;
        }
    }

    protected static void readFully(byte[] bArr, InputStream inputStream) throws IOException {
        int i = 0;
        while (i != bArr.length) {
            int read = inputStream.read(bArr, i, bArr.length - i);
            if (read == -1) {
                throw new EOFException();
            }
            i += read;
        }
    }

    protected static int readUint16(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        int read2 = inputStream.read();
        if ((read | read2) >= 0) {
            return (read << 8) | read2;
        }
        throw new EOFException();
    }

    protected static int readUint24(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        int read2 = inputStream.read();
        int read3 = inputStream.read();
        if ((read | read2 | read3) >= 0) {
            return (read << 16) | (read2 << 8) | read3;
        }
        throw new EOFException();
    }

    protected static long readUint32(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        int read2 = inputStream.read();
        int read3 = inputStream.read();
        int read4 = inputStream.read();
        if ((read | read2 | read3 | read4) < 0) {
            throw new EOFException();
        }
        return (((long) read2) << 16) | (((long) read) << 24) | (((long) read3) << 8) | ((long) read4);
    }

    protected static short readUint8(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        if (read != -1) {
            return (short) read;
        }
        throw new EOFException();
    }

    static byte[] toByteArray(String str) {
        char[] charArray = str.toCharArray();
        byte[] bArr = new byte[charArray.length];
        for (int i = 0; i != bArr.length; i++) {
            bArr[i] = (byte) charArray[i];
        }
        return bArr;
    }

    protected static void writeUint16(int i, OutputStream outputStream) throws IOException {
        outputStream.write(i >> 8);
        outputStream.write(i);
    }

    protected static void writeUint16(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) (i >> 8);
        bArr[i2 + 1] = (byte) i;
    }

    protected static void writeUint24(int i, OutputStream outputStream) throws IOException {
        outputStream.write(i >> 16);
        outputStream.write(i >> 8);
        outputStream.write(i);
    }

    protected static void writeUint24(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) (i >> 16);
        bArr[i2 + 1] = (byte) (i >> 8);
        bArr[i2 + 2] = (byte) i;
    }

    protected static void writeUint32(long j, OutputStream outputStream) throws IOException {
        outputStream.write((int) (j >> 24));
        outputStream.write((int) (j >> 16));
        outputStream.write((int) (j >> 8));
        outputStream.write((int) j);
    }

    protected static void writeUint32(long j, byte[] bArr, int i) {
        bArr[i] = (byte) ((int) (j >> 24));
        bArr[i + 1] = (byte) ((int) (j >> 16));
        bArr[i + 2] = (byte) ((int) (j >> 8));
        bArr[i + 3] = (byte) ((int) j);
    }

    protected static void writeUint64(long j, OutputStream outputStream) throws IOException {
        outputStream.write((int) (j >> 56));
        outputStream.write((int) (j >> 48));
        outputStream.write((int) (j >> 40));
        outputStream.write((int) (j >> 32));
        outputStream.write((int) (j >> 24));
        outputStream.write((int) (j >> 16));
        outputStream.write((int) (j >> 8));
        outputStream.write((int) j);
    }

    protected static void writeUint64(long j, byte[] bArr, int i) {
        bArr[i] = (byte) ((int) (j >> 56));
        bArr[i + 1] = (byte) ((int) (j >> 48));
        bArr[i + 2] = (byte) ((int) (j >> 40));
        bArr[i + 3] = (byte) ((int) (j >> 32));
        bArr[i + 4] = (byte) ((int) (j >> 24));
        bArr[i + 5] = (byte) ((int) (j >> 16));
        bArr[i + 6] = (byte) ((int) (j >> 8));
        bArr[i + 7] = (byte) ((int) j);
    }

    protected static void writeUint8(short s, OutputStream outputStream) throws IOException {
        outputStream.write(s);
    }

    protected static void writeUint8(short s, byte[] bArr, int i) {
        bArr[i] = (byte) s;
    }

    protected static void writeVersion(OutputStream outputStream) throws IOException {
        outputStream.write(3);
        outputStream.write(1);
    }
}
