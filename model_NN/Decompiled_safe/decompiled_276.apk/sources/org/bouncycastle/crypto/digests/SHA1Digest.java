package org.bouncycastle.crypto.digests;

public class SHA1Digest extends GeneralDigest {
    private static final int DIGEST_LENGTH = 20;
    private static final int Y1 = 1518500249;
    private static final int Y2 = 1859775393;
    private static final int Y3 = -1894007588;
    private static final int Y4 = -899497514;
    private int H1;
    private int H2;
    private int H3;
    private int H4;
    private int H5;
    private int[] X = new int[80];
    private int xOff;

    public SHA1Digest() {
        reset();
    }

    public SHA1Digest(SHA1Digest sHA1Digest) {
        super(sHA1Digest);
        this.H1 = sHA1Digest.H1;
        this.H2 = sHA1Digest.H2;
        this.H3 = sHA1Digest.H3;
        this.H4 = sHA1Digest.H4;
        this.H5 = sHA1Digest.H5;
        System.arraycopy(sHA1Digest.X, 0, this.X, 0, sHA1Digest.X.length);
        this.xOff = sHA1Digest.xOff;
    }

    private int f(int i, int i2, int i3) {
        return (i & i2) | ((i ^ -1) & i3);
    }

    private int g(int i, int i2, int i3) {
        return (i & i2) | (i & i3) | (i2 & i3);
    }

    private int h(int i, int i2, int i3) {
        return (i ^ i2) ^ i3;
    }

    private void unpackWord(int i, byte[] bArr, int i2) {
        int i3 = i2 + 1;
        bArr[i2] = (byte) (i >>> 24);
        int i4 = i3 + 1;
        bArr[i3] = (byte) (i >>> 16);
        int i5 = i4 + 1;
        bArr[i4] = (byte) (i >>> 8);
        int i6 = i5 + 1;
        bArr[i5] = (byte) i;
    }

    public int doFinal(byte[] bArr, int i) {
        finish();
        unpackWord(this.H1, bArr, i);
        unpackWord(this.H2, bArr, i + 4);
        unpackWord(this.H3, bArr, i + 8);
        unpackWord(this.H4, bArr, i + 12);
        unpackWord(this.H5, bArr, i + 16);
        reset();
        return 20;
    }

    public String getAlgorithmName() {
        return "SHA-1";
    }

    public int getDigestSize() {
        return 20;
    }

    /* access modifiers changed from: protected */
    public void processBlock() {
        for (int i = 16; i < 80; i++) {
            int i2 = ((this.X[i - 3] ^ this.X[i - 8]) ^ this.X[i - 14]) ^ this.X[i - 16];
            this.X[i] = (i2 >>> 31) | (i2 << 1);
        }
        int i3 = this.H1;
        int i4 = this.H2;
        int i5 = this.H3;
        int i6 = this.H4;
        int i7 = i3;
        int i8 = 0;
        int i9 = i5;
        int i10 = this.H5;
        int i11 = i4;
        int i12 = 0;
        int i13 = i11;
        while (i8 < 4) {
            int i14 = i12 + 1;
            int f = this.X[i12] + ((i7 << 5) | (i7 >>> 27)) + f(i13, i9, i6) + Y1 + i10;
            int i15 = (i13 << 30) | (i13 >>> 2);
            int i16 = i14 + 1;
            int f2 = i6 + ((f << 5) | (f >>> 27)) + f(i7, i15, i9) + this.X[i14] + Y1;
            int i17 = (i7 << 30) | (i7 >>> 2);
            int i18 = i16 + 1;
            int f3 = i9 + ((f2 << 5) | (f2 >>> 27)) + f(f, i17, i15) + this.X[i16] + Y1;
            int i19 = (f >>> 2) | (f << 30);
            int i20 = i18 + 1;
            int f4 = i15 + ((f3 << 5) | (f3 >>> 27)) + f(f2, i19, i17) + this.X[i18] + Y1;
            i6 = (f2 >>> 2) | (f2 << 30);
            int f5 = i17 + ((f4 << 5) | (f4 >>> 27)) + f(f3, i6, i19) + this.X[i20] + Y1;
            i9 = (f3 >>> 2) | (f3 << 30);
            i8++;
            i7 = f5;
            i13 = f4;
            i10 = i19;
            i12 = i20 + 1;
        }
        int i21 = 0;
        while (i21 < 4) {
            int i22 = i12 + 1;
            int h = this.X[i12] + ((i7 << 5) | (i7 >>> 27)) + h(i13, i9, i6) + Y2 + i10;
            int i23 = (i13 << 30) | (i13 >>> 2);
            int i24 = i22 + 1;
            int h2 = i6 + ((h << 5) | (h >>> 27)) + h(i7, i23, i9) + this.X[i22] + Y2;
            int i25 = (i7 << 30) | (i7 >>> 2);
            int i26 = i24 + 1;
            int h3 = i9 + ((h2 << 5) | (h2 >>> 27)) + h(h, i25, i23) + this.X[i24] + Y2;
            int i27 = (h >>> 2) | (h << 30);
            int i28 = i26 + 1;
            int h4 = i23 + ((h3 << 5) | (h3 >>> 27)) + h(h2, i27, i25) + this.X[i26] + Y2;
            i6 = (h2 >>> 2) | (h2 << 30);
            int h5 = i25 + ((h4 << 5) | (h4 >>> 27)) + h(h3, i6, i27) + this.X[i28] + Y2;
            i9 = (h3 >>> 2) | (h3 << 30);
            i21++;
            i7 = h5;
            i13 = h4;
            i10 = i27;
            i12 = i28 + 1;
        }
        int i29 = 0;
        while (i29 < 4) {
            int i30 = i12 + 1;
            int g = this.X[i12] + ((i7 << 5) | (i7 >>> 27)) + g(i13, i9, i6) + Y3 + i10;
            int i31 = (i13 << 30) | (i13 >>> 2);
            int i32 = i30 + 1;
            int g2 = i6 + ((g << 5) | (g >>> 27)) + g(i7, i31, i9) + this.X[i30] + Y3;
            int i33 = (i7 << 30) | (i7 >>> 2);
            int i34 = i32 + 1;
            int g3 = i9 + ((g2 << 5) | (g2 >>> 27)) + g(g, i33, i31) + this.X[i32] + Y3;
            int i35 = (g >>> 2) | (g << 30);
            int i36 = i34 + 1;
            int g4 = i31 + ((g3 << 5) | (g3 >>> 27)) + g(g2, i35, i33) + this.X[i34] + Y3;
            i6 = (g2 >>> 2) | (g2 << 30);
            int g5 = i33 + ((g4 << 5) | (g4 >>> 27)) + g(g3, i6, i35) + this.X[i36] + Y3;
            i9 = (g3 >>> 2) | (g3 << 30);
            i29++;
            i7 = g5;
            i13 = g4;
            i10 = i35;
            i12 = i36 + 1;
        }
        int i37 = 0;
        while (i37 <= 3) {
            int i38 = i12 + 1;
            int h6 = this.X[i12] + ((i7 << 5) | (i7 >>> 27)) + h(i13, i9, i6) + Y4 + i10;
            int i39 = (i13 << 30) | (i13 >>> 2);
            int i40 = i38 + 1;
            int h7 = i6 + ((h6 << 5) | (h6 >>> 27)) + h(i7, i39, i9) + this.X[i38] + Y4;
            int i41 = (i7 << 30) | (i7 >>> 2);
            int i42 = i40 + 1;
            int h8 = i9 + ((h7 << 5) | (h7 >>> 27)) + h(h6, i41, i39) + this.X[i40] + Y4;
            int i43 = (h6 >>> 2) | (h6 << 30);
            int i44 = i42 + 1;
            int h9 = i39 + ((h8 << 5) | (h8 >>> 27)) + h(h7, i43, i41) + this.X[i42] + Y4;
            i6 = (h7 >>> 2) | (h7 << 30);
            int h10 = i41 + ((h9 << 5) | (h9 >>> 27)) + h(h8, i6, i43) + this.X[i44] + Y4;
            i9 = (h8 >>> 2) | (h8 << 30);
            i37++;
            i7 = h10;
            i13 = h9;
            i10 = i43;
            i12 = i44 + 1;
        }
        this.H1 += i7;
        this.H2 += i13;
        this.H3 += i9;
        this.H4 += i6;
        this.H5 += i10;
        this.xOff = 0;
        for (int i45 = 0; i45 < 16; i45++) {
            this.X[i45] = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void processLength(long j) {
        if (this.xOff > 14) {
            processBlock();
        }
        this.X[14] = (int) (j >>> 32);
        this.X[15] = (int) (-1 & j);
    }

    /* access modifiers changed from: protected */
    public void processWord(byte[] bArr, int i) {
        int[] iArr = this.X;
        int i2 = this.xOff;
        this.xOff = i2 + 1;
        iArr[i2] = ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << Tnaf.POW_2_WIDTH) | ((bArr[i + 2] & 255) << 8) | (bArr[i + 3] & 255);
        if (this.xOff == 16) {
            processBlock();
        }
    }

    public void reset() {
        super.reset();
        this.H1 = 1732584193;
        this.H2 = -271733879;
        this.H3 = -1732584194;
        this.H4 = 271733878;
        this.H5 = -1009589776;
        this.xOff = 0;
        for (int i = 0; i != this.X.length; i++) {
            this.X[i] = 0;
        }
    }
}
