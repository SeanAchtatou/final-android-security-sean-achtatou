package org.bouncycastle.crypto.digests;

public class SHA224Digest extends GeneralDigest {
    private static final int DIGEST_LENGTH = 28;
    static final int[] K = {1116352408, 1899447441, -1245643825, -373957723, 961987163, 1508970993, -1841331548, -1424204075, -670586216, 310598401, 607225278, 1426881987, 1925078388, -2132889090, -1680079193, -1046744716, -459576895, -272742522, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, -1740746414, -1473132947, -1341970488, -1084653625, -958395405, -710438585, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, -2117940946, -1838011259, -1564481375, -1474664885, -1035236496, -949202525, -778901479, -694614492, -200395387, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, -2067236844, -1933114872, -1866530822, -1538233109, -1090935817, -965641998};
    private int H1;
    private int H2;
    private int H3;
    private int H4;
    private int H5;
    private int H6;
    private int H7;
    private int H8;
    private int[] X = new int[64];
    private int xOff;

    public SHA224Digest() {
        reset();
    }

    public SHA224Digest(SHA224Digest sHA224Digest) {
        super(sHA224Digest);
        this.H1 = sHA224Digest.H1;
        this.H2 = sHA224Digest.H2;
        this.H3 = sHA224Digest.H3;
        this.H4 = sHA224Digest.H4;
        this.H5 = sHA224Digest.H5;
        this.H6 = sHA224Digest.H6;
        this.H7 = sHA224Digest.H7;
        this.H8 = sHA224Digest.H8;
        System.arraycopy(sHA224Digest.X, 0, this.X, 0, sHA224Digest.X.length);
        this.xOff = sHA224Digest.xOff;
    }

    private int Ch(int i, int i2, int i3) {
        return (i & i2) ^ ((i ^ -1) & i3);
    }

    private int Maj(int i, int i2, int i3) {
        return ((i & i2) ^ (i & i3)) ^ (i2 & i3);
    }

    private int Sum0(int i) {
        return (((i >>> 2) | (i << 30)) ^ ((i >>> 13) | (i << 19))) ^ ((i >>> 22) | (i << 10));
    }

    private int Sum1(int i) {
        return (((i >>> 6) | (i << 26)) ^ ((i >>> 11) | (i << 21))) ^ ((i >>> 25) | (i << 7));
    }

    private int Theta0(int i) {
        return (((i >>> 7) | (i << 25)) ^ ((i >>> 18) | (i << 14))) ^ (i >>> 3);
    }

    private int Theta1(int i) {
        return (((i >>> 17) | (i << 15)) ^ ((i >>> 19) | (i << 13))) ^ (i >>> 10);
    }

    private void unpackWord(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) (i >>> 24);
        bArr[i2 + 1] = (byte) (i >>> 16);
        bArr[i2 + 2] = (byte) (i >>> 8);
        bArr[i2 + 3] = (byte) i;
    }

    public int doFinal(byte[] bArr, int i) {
        finish();
        unpackWord(this.H1, bArr, i);
        unpackWord(this.H2, bArr, i + 4);
        unpackWord(this.H3, bArr, i + 8);
        unpackWord(this.H4, bArr, i + 12);
        unpackWord(this.H5, bArr, i + 16);
        unpackWord(this.H6, bArr, i + 20);
        unpackWord(this.H7, bArr, i + 24);
        reset();
        return 28;
    }

    public String getAlgorithmName() {
        return "SHA-224";
    }

    public int getDigestSize() {
        return 28;
    }

    /* access modifiers changed from: protected */
    public void processBlock() {
        for (int i = 16; i <= 63; i++) {
            this.X[i] = Theta1(this.X[i - 2]) + this.X[i - 7] + Theta0(this.X[i - 15]) + this.X[i - 16];
        }
        int i2 = this.H1;
        int i3 = this.H2;
        int i4 = this.H3;
        int i5 = this.H4;
        int i6 = this.H5;
        int i7 = this.H6;
        int i8 = this.H7;
        int i9 = i2;
        int i10 = 0;
        int i11 = i4;
        int i12 = this.H8;
        int i13 = i7;
        int i14 = i6;
        int i15 = i13;
        int i16 = i5;
        int i17 = i8;
        int i18 = i16;
        int i19 = i3;
        int i20 = 0;
        int i21 = i19;
        while (i10 < 8) {
            int i22 = i20 + 1;
            int Sum1 = this.X[i20] + Sum1(i14) + Ch(i14, i15, i17) + K[i20] + i12;
            int i23 = i18 + Sum1;
            int Sum0 = Sum1 + Sum0(i9) + Maj(i9, i21, i11);
            int i24 = i22 + 1;
            int Sum12 = i17 + Sum1(i23) + Ch(i23, i14, i15) + K[i22] + this.X[i22];
            int i25 = i11 + Sum12;
            int Sum02 = Sum12 + Sum0(Sum0) + Maj(Sum0, i9, i21);
            int i26 = i24 + 1;
            int Sum13 = i15 + Sum1(i25) + Ch(i25, i23, i14) + K[i24] + this.X[i24];
            int i27 = i21 + Sum13;
            int Sum03 = Sum13 + Sum0(Sum02) + Maj(Sum02, Sum0, i9);
            int i28 = i26 + 1;
            int Sum14 = i14 + Sum1(i27) + Ch(i27, i25, i23) + K[i26] + this.X[i26];
            int i29 = i9 + Sum14;
            int Sum04 = Sum14 + Sum0(Sum03) + Maj(Sum03, Sum02, Sum0);
            int i30 = i28 + 1;
            int Sum15 = i23 + Sum1(i29) + Ch(i29, i27, i25) + K[i28] + this.X[i28];
            int i31 = Sum0 + Sum15;
            int Sum05 = Sum15 + Sum0(Sum04) + Maj(Sum04, Sum03, Sum02);
            int i32 = i30 + 1;
            int Sum16 = i25 + Sum1(i31) + Ch(i31, i29, i27) + K[i30] + this.X[i30];
            i17 = Sum02 + Sum16;
            int Sum06 = Sum16 + Sum0(Sum05) + Maj(Sum05, Sum04, Sum03);
            int i33 = i32 + 1;
            int Sum17 = i27 + Sum1(i17) + Ch(i17, i31, i29) + K[i32] + this.X[i32];
            i15 = Sum03 + Sum17;
            int Sum07 = Sum17 + Sum0(Sum06) + Maj(Sum06, Sum05, Sum04);
            int Sum18 = i29 + Sum1(i15) + Ch(i15, i17, i31) + K[i33] + this.X[i33];
            i14 = Sum04 + Sum18;
            i10++;
            i9 = Sum18 + Sum0(Sum07) + Maj(Sum07, Sum06, Sum05);
            i21 = Sum07;
            i11 = Sum06;
            i18 = Sum05;
            i12 = i31;
            i20 = i33 + 1;
        }
        this.H1 = this.H1 + i9;
        this.H2 = this.H2 + i21;
        this.H3 = this.H3 + i11;
        this.H4 = this.H4 + i18;
        this.H5 = this.H5 + i14;
        this.H6 = this.H6 + i15;
        this.H7 = this.H7 + i17;
        this.H8 = this.H8 + i12;
        this.xOff = 0;
        for (int i34 = 0; i34 < 16; i34++) {
            this.X[i34] = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void processLength(long j) {
        if (this.xOff > 14) {
            processBlock();
        }
        this.X[14] = (int) (j >>> 32);
        this.X[15] = (int) (-1 & j);
    }

    /* access modifiers changed from: protected */
    public void processWord(byte[] bArr, int i) {
        int[] iArr = this.X;
        int i2 = this.xOff;
        this.xOff = i2 + 1;
        iArr[i2] = ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << Tnaf.POW_2_WIDTH) | ((bArr[i + 2] & 255) << 8) | (bArr[i + 3] & 255);
        if (this.xOff == 16) {
            processBlock();
        }
    }

    public void reset() {
        super.reset();
        this.H1 = -1056596264;
        this.H2 = 914150663;
        this.H3 = 812702999;
        this.H4 = -150054599;
        this.H5 = -4191439;
        this.H6 = 1750603025;
        this.H7 = 1694076839;
        this.H8 = -1090891868;
        this.xOff = 0;
        for (int i = 0; i != this.X.length; i++) {
            this.X[i] = 0;
        }
    }
}
