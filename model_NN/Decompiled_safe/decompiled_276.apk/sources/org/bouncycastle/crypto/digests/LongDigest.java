package org.bouncycastle.crypto.digests;

import org.bouncycastle.crypto.ExtendedDigest;

public abstract class LongDigest implements ExtendedDigest {
    private static final int BYTE_LENGTH = 128;
    static final long[] K = {4794697086780616226L, 8158064640168781261L, -5349999486874862801L, -1606136188198331460L, 4131703408338449720L, 6480981068601479193L, -7908458776815382629L, -6116909921290321640L, -2880145864133508542L, 1334009975649890238L, 2608012711638119052L, 6128411473006802146L, 8268148722764581231L, -9160688886553864527L, -7215885187991268811L, -4495734319001033068L, -1973867731355612462L, -1171420211273849373L, 1135362057144423861L, 2597628984639134821L, 3308224258029322869L, 5365058923640841347L, 6679025012923562964L, 8573033837759648693L, -7476448914759557205L, -6327057829258317296L, -5763719355590565569L, -4658551843659510044L, -4116276920077217854L, -3051310485924567259L, 489312712824947311L, 1452737877330783856L, 2861767655752347644L, 3322285676063803686L, 5560940570517711597L, 5996557281743188959L, 7280758554555802590L, 8532644243296465576L, -9096487096722542874L, -7894198246740708037L, -6719396339535248540L, -6333637450476146687L, -4446306890439682159L, -4076793802049405392L, -3345356375505022440L, -2983346525034927856L, -860691631967231958L, 1182934255886127544L, 1847814050463011016L, 2177327727835720531L, 2830643537854262169L, 3796741975233480872L, 4115178125766777443L, 5681478168544905931L, 6601373596472566643L, 7507060721942968483L, 8399075790359081724L, 8693463985226723168L, -8878714635349349518L, -8302665154208450068L, -8016688836872298968L, -6606660893046293015L, -4685533653050689259L, -4147400797238176981L, -3880063495543823972L, -3348786107499101689L, -1523767162380948706L, -757361751448694408L, 500013540394364858L, 748580250866718886L, 1242879168328830382L, 1977374033974150939L, 2944078676154940804L, 3659926193048069267L, 4368137639120453308L, 4836135668995329356L, 5532061633213252278L, 6448918945643986474L, 6902733635092675308L, 7801388544844847127L};
    protected long H1;
    protected long H2;
    protected long H3;
    protected long H4;
    protected long H5;
    protected long H6;
    protected long H7;
    protected long H8;
    private long[] W;
    private long byteCount1;
    private long byteCount2;
    private int wOff;
    private byte[] xBuf;
    private int xBufOff;

    protected LongDigest() {
        this.W = new long[80];
        this.xBuf = new byte[8];
        this.xBufOff = 0;
        reset();
    }

    protected LongDigest(LongDigest longDigest) {
        this.W = new long[80];
        this.xBuf = new byte[longDigest.xBuf.length];
        System.arraycopy(longDigest.xBuf, 0, this.xBuf, 0, longDigest.xBuf.length);
        this.xBufOff = longDigest.xBufOff;
        this.byteCount1 = longDigest.byteCount1;
        this.byteCount2 = longDigest.byteCount2;
        this.H1 = longDigest.H1;
        this.H2 = longDigest.H2;
        this.H3 = longDigest.H3;
        this.H4 = longDigest.H4;
        this.H5 = longDigest.H5;
        this.H6 = longDigest.H6;
        this.H7 = longDigest.H7;
        this.H8 = longDigest.H8;
        System.arraycopy(longDigest.W, 0, this.W, 0, longDigest.W.length);
        this.wOff = longDigest.wOff;
    }

    private long Ch(long j, long j2, long j3) {
        return (j & j2) ^ ((-1 ^ j) & j3);
    }

    private long Maj(long j, long j2, long j3) {
        return ((j & j2) ^ (j & j3)) ^ (j2 & j3);
    }

    private long Sigma0(long j) {
        return (((j << 63) | (j >>> 1)) ^ ((j << 56) | (j >>> 8))) ^ (j >>> 7);
    }

    private long Sigma1(long j) {
        return (((j << 45) | (j >>> 19)) ^ ((j << 3) | (j >>> 61))) ^ (j >>> 6);
    }

    private long Sum0(long j) {
        return (((j << 36) | (j >>> 28)) ^ ((j << 30) | (j >>> 34))) ^ ((j << 25) | (j >>> 39));
    }

    private long Sum1(long j) {
        return (((j << 50) | (j >>> 14)) ^ ((j << 46) | (j >>> 18))) ^ ((j << 23) | (j >>> 41));
    }

    private void adjustByteCounts() {
        if (this.byteCount1 > 2305843009213693951L) {
            this.byteCount2 += this.byteCount1 >>> 61;
            this.byteCount1 &= 2305843009213693951L;
        }
    }

    public void finish() {
        adjustByteCounts();
        long j = this.byteCount1 << 3;
        long j2 = this.byteCount2;
        update(Byte.MIN_VALUE);
        while (this.xBufOff != 0) {
            update((byte) 0);
        }
        processLength(j, j2);
        processBlock();
    }

    public int getByteLength() {
        return 128;
    }

    /* access modifiers changed from: protected */
    public void processBlock() {
        adjustByteCounts();
        for (int i = 16; i <= 79; i++) {
            this.W[i] = Sigma1(this.W[i - 2]) + this.W[i - 7] + Sigma0(this.W[i - 15]) + this.W[i - 16];
        }
        long j = this.H1;
        long j2 = this.H2;
        long j3 = this.H3;
        long j4 = this.H4;
        long j5 = this.H5;
        long j6 = this.H6;
        long j7 = this.H7;
        long j8 = this.H8;
        int i2 = 0;
        long j9 = j4;
        int i3 = 0;
        long j10 = j6;
        long j11 = j2;
        long j12 = j5;
        long j13 = j;
        long j14 = j12;
        long j15 = j10;
        long j16 = j7;
        long j17 = j3;
        while (i2 < 10) {
            int i4 = i3 + 1;
            long Sum1 = Sum1(j14) + Ch(j14, j15, j16) + K[i3] + this.W[i3] + j8;
            long j18 = j9 + Sum1;
            long Sum0 = Sum0(j13) + Maj(j13, j11, j17) + Sum1;
            int i5 = i4 + 1;
            long Ch = Ch(j18, j14, j15) + Sum1(j18) + K[i4] + this.W[i4] + j16;
            long j19 = j17 + Ch;
            long Maj = Ch + Maj(Sum0, j13, j11) + Sum0(Sum0);
            int i6 = i5 + 1;
            long Sum12 = j15 + Sum1(j19) + Ch(j19, j18, j14) + K[i5] + this.W[i5];
            long j20 = j11 + Sum12;
            long Maj2 = Sum12 + Maj(Maj, Sum0, j13) + Sum0(Maj);
            int i7 = i6 + 1;
            long Sum13 = j14 + Sum1(j20) + Ch(j20, j19, j18) + K[i6] + this.W[i6];
            long j21 = j13 + Sum13;
            long Maj3 = Sum13 + Maj(Maj2, Maj, Sum0) + Sum0(Maj2);
            int i8 = i7 + 1;
            long Sum14 = j18 + Sum1(j21) + Ch(j21, j20, j19) + K[i7] + this.W[i7];
            long j22 = Sum0 + Sum14;
            long Maj4 = Sum14 + Maj(Maj3, Maj2, Maj) + Sum0(Maj3);
            int i9 = i8 + 1;
            long Sum15 = Sum1(j22) + Ch(j22, j21, j20) + K[i8] + this.W[i8] + j19;
            j16 = Maj + Sum15;
            long Maj5 = Sum15 + Maj(Maj4, Maj3, Maj2) + Sum0(Maj4);
            int i10 = i9 + 1;
            long Ch2 = Ch(j16, j22, j21) + Sum1(j16) + K[i9] + this.W[i9] + j20;
            long j23 = Maj2 + Ch2;
            long Maj6 = Ch2 + Maj(Maj5, Maj4, Maj3) + Sum0(Maj5);
            int i11 = i10 + 1;
            long Ch3 = j21 + Ch(j23, j16, j22) + Sum1(j23) + K[i10] + this.W[i10];
            long Sum02 = Sum0(Maj6);
            i2++;
            j8 = j22;
            j17 = Maj5;
            i3 = i11;
            j9 = Maj4;
            j14 = Maj3 + Ch3;
            long j24 = Maj6;
            j15 = j23;
            j13 = Maj(Maj6, Maj5, Maj4) + Sum02 + Ch3;
            j11 = j24;
        }
        this.H1 = this.H1 + j13;
        this.H2 = this.H2 + j11;
        this.H3 = this.H3 + j17;
        this.H4 = this.H4 + j9;
        this.H5 = this.H5 + j14;
        this.H6 = this.H6 + j15;
        this.H7 = this.H7 + j16;
        this.H8 = this.H8 + j8;
        this.wOff = 0;
        for (int i12 = 0; i12 < 16; i12++) {
            this.W[i12] = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void processLength(long j, long j2) {
        if (this.wOff > 14) {
            processBlock();
        }
        this.W[14] = j2;
        this.W[15] = j;
    }

    /* access modifiers changed from: protected */
    public void processWord(byte[] bArr, int i) {
        long[] jArr = this.W;
        int i2 = this.wOff;
        this.wOff = i2 + 1;
        jArr[i2] = (((long) (bArr[i] & 255)) << 56) | (((long) (bArr[i + 1] & 255)) << 48) | (((long) (bArr[i + 2] & 255)) << 40) | (((long) (bArr[i + 3] & 255)) << 32) | (((long) (bArr[i + 4] & 255)) << 24) | (((long) (bArr[i + 5] & 255)) << 16) | (((long) (bArr[i + 6] & 255)) << 8) | ((long) (bArr[i + 7] & 255));
        if (this.wOff == 16) {
            processBlock();
        }
    }

    public void reset() {
        this.byteCount1 = 0;
        this.byteCount2 = 0;
        this.xBufOff = 0;
        for (int i = 0; i < this.xBuf.length; i++) {
            this.xBuf[i] = 0;
        }
        this.wOff = 0;
        for (int i2 = 0; i2 != this.W.length; i2++) {
            this.W[i2] = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void unpackWord(long j, byte[] bArr, int i) {
        bArr[i] = (byte) ((int) (j >>> 56));
        bArr[i + 1] = (byte) ((int) (j >>> 48));
        bArr[i + 2] = (byte) ((int) (j >>> 40));
        bArr[i + 3] = (byte) ((int) (j >>> 32));
        bArr[i + 4] = (byte) ((int) (j >>> 24));
        bArr[i + 5] = (byte) ((int) (j >>> 16));
        bArr[i + 6] = (byte) ((int) (j >>> 8));
        bArr[i + 7] = (byte) ((int) j);
    }

    public void update(byte b) {
        byte[] bArr = this.xBuf;
        int i = this.xBufOff;
        this.xBufOff = i + 1;
        bArr[i] = b;
        if (this.xBufOff == this.xBuf.length) {
            processWord(this.xBuf, 0);
            this.xBufOff = 0;
        }
        this.byteCount1++;
    }

    public void update(byte[] bArr, int i, int i2) {
        int i3 = i2;
        int i4 = i;
        while (this.xBufOff != 0 && i3 > 0) {
            update(bArr[i4]);
            i4++;
            i3--;
        }
        while (i3 > this.xBuf.length) {
            processWord(bArr, i4);
            i4 += this.xBuf.length;
            i3 -= this.xBuf.length;
            this.byteCount1 += (long) this.xBuf.length;
        }
        while (i3 > 0) {
            update(bArr[i4]);
            i4++;
            i3--;
        }
    }
}
