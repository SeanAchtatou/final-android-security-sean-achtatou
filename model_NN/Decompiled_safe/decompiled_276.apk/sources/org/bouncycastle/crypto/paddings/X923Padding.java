package org.bouncycastle.crypto.paddings;

import java.security.SecureRandom;
import org.bouncycastle.crypto.InvalidCipherTextException;

public class X923Padding implements BlockCipherPadding {
    SecureRandom random = null;

    public int addPadding(byte[] bArr, int i) {
        byte length = (byte) (bArr.length - i);
        int i2 = i;
        while (i2 < bArr.length - 1) {
            if (this.random == null) {
                bArr[i2] = 0;
            } else {
                bArr[i2] = (byte) this.random.nextInt();
            }
            i2++;
        }
        bArr[i2] = length;
        return length;
    }

    public String getPaddingName() {
        return "X9.23";
    }

    public void init(SecureRandom secureRandom) throws IllegalArgumentException {
        this.random = secureRandom;
    }

    public int padCount(byte[] bArr) throws InvalidCipherTextException {
        byte b = bArr[bArr.length - 1] & 255;
        if (b <= bArr.length) {
            return b;
        }
        throw new InvalidCipherTextException("pad block corrupted");
    }
}
