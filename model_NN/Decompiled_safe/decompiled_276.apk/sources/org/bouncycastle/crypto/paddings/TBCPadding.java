package org.bouncycastle.crypto.paddings;

import java.security.SecureRandom;
import org.bouncycastle.crypto.InvalidCipherTextException;

public class TBCPadding implements BlockCipherPadding {
    public int addPadding(byte[] bArr, int i) {
        int i2;
        byte b;
        int length = bArr.length - i;
        if (i > 0) {
            b = (byte) ((bArr[i - 1] & 1) == 0 ? 255 : 0);
            i2 = i;
        } else {
            b = (byte) ((bArr[bArr.length - 1] & 1) == 0 ? 255 : 0);
            i2 = i;
        }
        while (i2 < bArr.length) {
            bArr[i2] = b;
            i2++;
        }
        return length;
    }

    public String getPaddingName() {
        return "TBC";
    }

    public void init(SecureRandom secureRandom) throws IllegalArgumentException {
    }

    public int padCount(byte[] bArr) throws InvalidCipherTextException {
        byte b = bArr[bArr.length - 1];
        int length = bArr.length - 1;
        while (length > 0 && bArr[length - 1] == b) {
            length--;
        }
        return bArr.length - length;
    }
}
