package org.bouncycastle.crypto.modes;

import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.macs.CBCBlockCipherMac;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.Arrays;

public class CCMBlockCipher implements AEADBlockCipher {
    private byte[] associatedText;
    private int blockSize;
    private BlockCipher cipher;
    private ByteArrayOutputStream data = new ByteArrayOutputStream();
    private boolean forEncryption;
    private CipherParameters keyParam;
    private byte[] macBlock;
    private int macSize;
    private byte[] nonce;

    public CCMBlockCipher(BlockCipher blockCipher) {
        this.cipher = blockCipher;
        this.blockSize = blockCipher.getBlockSize();
        this.macBlock = new byte[this.blockSize];
        if (this.blockSize != 16) {
            throw new IllegalArgumentException("cipher required with a block size of 16.");
        }
    }

    private int calculateMac(byte[] bArr, int i, int i2, byte[] bArr2) {
        int i3;
        CBCBlockCipherMac cBCBlockCipherMac = new CBCBlockCipherMac(this.cipher, this.macSize * 8);
        cBCBlockCipherMac.init(this.keyParam);
        byte[] bArr3 = new byte[16];
        if (hasAssociatedText()) {
            bArr3[0] = (byte) (bArr3[0] | 64);
        }
        bArr3[0] = (byte) (bArr3[0] | ((((cBCBlockCipherMac.getMacSize() - 2) / 2) & 7) << 3));
        bArr3[0] = (byte) (bArr3[0] | (((15 - this.nonce.length) - 1) & 7));
        System.arraycopy(this.nonce, 0, bArr3, 1, this.nonce.length);
        int i4 = 1;
        int i5 = i2;
        while (i5 > 0) {
            bArr3[bArr3.length - i4] = (byte) (i5 & 255);
            i5 >>>= 8;
            i4++;
        }
        cBCBlockCipherMac.update(bArr3, 0, bArr3.length);
        if (hasAssociatedText()) {
            if (this.associatedText.length < 65280) {
                cBCBlockCipherMac.update((byte) (this.associatedText.length >> 8));
                cBCBlockCipherMac.update((byte) this.associatedText.length);
                i3 = 2;
            } else {
                cBCBlockCipherMac.update((byte) -1);
                cBCBlockCipherMac.update((byte) -2);
                cBCBlockCipherMac.update((byte) (this.associatedText.length >> 24));
                cBCBlockCipherMac.update((byte) (this.associatedText.length >> 16));
                cBCBlockCipherMac.update((byte) (this.associatedText.length >> 8));
                cBCBlockCipherMac.update((byte) this.associatedText.length);
                i3 = 6;
            }
            cBCBlockCipherMac.update(this.associatedText, 0, this.associatedText.length);
            int length = (i3 + this.associatedText.length) % 16;
            if (length != 0) {
                for (int i6 = 0; i6 != 16 - length; i6++) {
                    cBCBlockCipherMac.update((byte) 0);
                }
            }
        }
        cBCBlockCipherMac.update(bArr, i, i2);
        return cBCBlockCipherMac.doFinal(bArr2, 0);
    }

    private boolean hasAssociatedText() {
        return (this.associatedText == null || this.associatedText.length == 0) ? false : true;
    }

    public int doFinal(byte[] bArr, int i) throws IllegalStateException, InvalidCipherTextException {
        byte[] byteArray = this.data.toByteArray();
        byte[] processPacket = processPacket(byteArray, 0, byteArray.length);
        System.arraycopy(processPacket, 0, bArr, i, processPacket.length);
        reset();
        return processPacket.length;
    }

    public String getAlgorithmName() {
        return this.cipher.getAlgorithmName() + "/CCM";
    }

    public byte[] getMac() {
        byte[] bArr = new byte[this.macSize];
        System.arraycopy(this.macBlock, 0, bArr, 0, bArr.length);
        return bArr;
    }

    public int getOutputSize(int i) {
        return this.forEncryption ? this.data.size() + i + this.macSize : (this.data.size() + i) - this.macSize;
    }

    public BlockCipher getUnderlyingCipher() {
        return this.cipher;
    }

    public int getUpdateOutputSize(int i) {
        return 0;
    }

    public void init(boolean z, CipherParameters cipherParameters) throws IllegalArgumentException {
        this.forEncryption = z;
        if (cipherParameters instanceof AEADParameters) {
            AEADParameters aEADParameters = (AEADParameters) cipherParameters;
            this.nonce = aEADParameters.getNonce();
            this.associatedText = aEADParameters.getAssociatedText();
            this.macSize = aEADParameters.getMacSize() / 8;
            this.keyParam = aEADParameters.getKey();
        } else if (cipherParameters instanceof ParametersWithIV) {
            ParametersWithIV parametersWithIV = (ParametersWithIV) cipherParameters;
            this.nonce = parametersWithIV.getIV();
            this.associatedText = null;
            this.macSize = this.macBlock.length / 2;
            this.keyParam = parametersWithIV.getParameters();
        } else {
            throw new IllegalArgumentException("invalid parameters passed to CCM");
        }
    }

    public int processByte(byte b, byte[] bArr, int i) throws DataLengthException, IllegalStateException {
        this.data.write(b);
        return 0;
    }

    public int processBytes(byte[] bArr, int i, int i2, byte[] bArr2, int i3) throws DataLengthException, IllegalStateException {
        this.data.write(bArr, i, i2);
        return 0;
    }

    public byte[] processPacket(byte[] bArr, int i, int i2) throws IllegalStateException, InvalidCipherTextException {
        if (this.keyParam == null) {
            throw new IllegalStateException("CCM cipher unitialized.");
        }
        SICBlockCipher sICBlockCipher = new SICBlockCipher(this.cipher);
        byte[] bArr2 = new byte[this.blockSize];
        bArr2[0] = (byte) (((15 - this.nonce.length) - 1) & 7);
        System.arraycopy(this.nonce, 0, bArr2, 1, this.nonce.length);
        sICBlockCipher.init(this.forEncryption, new ParametersWithIV(this.keyParam, bArr2));
        if (this.forEncryption) {
            byte[] bArr3 = new byte[(this.macSize + i2)];
            calculateMac(bArr, i, i2, this.macBlock);
            sICBlockCipher.processBlock(this.macBlock, 0, this.macBlock, 0);
            int i3 = 0;
            int i4 = i;
            while (i4 < i2 - this.blockSize) {
                sICBlockCipher.processBlock(bArr, i4, bArr3, i3);
                i3 += this.blockSize;
                i4 += this.blockSize;
            }
            byte[] bArr4 = new byte[this.blockSize];
            System.arraycopy(bArr, i4, bArr4, 0, i2 - i4);
            sICBlockCipher.processBlock(bArr4, 0, bArr4, 0);
            System.arraycopy(bArr4, 0, bArr3, i3, i2 - i4);
            int i5 = (i2 - i4) + i3;
            System.arraycopy(this.macBlock, 0, bArr3, i5, bArr3.length - i5);
            return bArr3;
        }
        byte[] bArr5 = new byte[(i2 - this.macSize)];
        System.arraycopy(bArr, (i + i2) - this.macSize, this.macBlock, 0, this.macSize);
        sICBlockCipher.processBlock(this.macBlock, 0, this.macBlock, 0);
        for (int i6 = this.macSize; i6 != this.macBlock.length; i6++) {
            this.macBlock[i6] = 0;
        }
        int i7 = 0;
        int i8 = i;
        while (i7 < bArr5.length - this.blockSize) {
            sICBlockCipher.processBlock(bArr, i8, bArr5, i7);
            i7 += this.blockSize;
            i8 += this.blockSize;
        }
        byte[] bArr6 = new byte[this.blockSize];
        System.arraycopy(bArr, i8, bArr6, 0, bArr5.length - i7);
        sICBlockCipher.processBlock(bArr6, 0, bArr6, 0);
        System.arraycopy(bArr6, 0, bArr5, i7, bArr5.length - i7);
        byte[] bArr7 = new byte[this.blockSize];
        calculateMac(bArr5, 0, bArr5.length, bArr7);
        if (Arrays.areEqual(this.macBlock, bArr7)) {
            return bArr5;
        }
        throw new InvalidCipherTextException("mac check in CCM failed");
    }

    public void reset() {
        this.cipher.reset();
        this.data.reset();
    }
}
