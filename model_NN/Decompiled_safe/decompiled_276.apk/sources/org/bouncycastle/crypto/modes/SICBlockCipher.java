package org.bouncycastle.crypto.modes;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.ParametersWithIV;

public class SICBlockCipher implements BlockCipher {
    private byte[] IV = new byte[this.blockSize];
    private final int blockSize = this.cipher.getBlockSize();
    private final BlockCipher cipher;
    private byte[] counter = new byte[this.blockSize];
    private byte[] counterOut = new byte[this.blockSize];

    public SICBlockCipher(BlockCipher blockCipher) {
        this.cipher = blockCipher;
    }

    public String getAlgorithmName() {
        return this.cipher.getAlgorithmName() + "/SIC";
    }

    public int getBlockSize() {
        return this.cipher.getBlockSize();
    }

    public BlockCipher getUnderlyingCipher() {
        return this.cipher;
    }

    public void init(boolean z, CipherParameters cipherParameters) throws IllegalArgumentException {
        if (cipherParameters instanceof ParametersWithIV) {
            ParametersWithIV parametersWithIV = (ParametersWithIV) cipherParameters;
            System.arraycopy(parametersWithIV.getIV(), 0, this.IV, 0, this.IV.length);
            reset();
            this.cipher.init(true, parametersWithIV.getParameters());
        }
    }

    public int processBlock(byte[] bArr, int i, byte[] bArr2, int i2) throws DataLengthException, IllegalStateException {
        this.cipher.processBlock(this.counter, 0, this.counterOut, 0);
        for (int i3 = 0; i3 < this.counterOut.length; i3++) {
            bArr2[i2 + i3] = (byte) (this.counterOut[i3] ^ bArr[i + i3]);
        }
        int length = this.counter.length - 1;
        byte b = 1;
        while (length >= 0) {
            int i4 = b + (this.counter[length] & 255);
            byte b2 = i4 > 255 ? (byte) 1 : 0;
            this.counter[length] = (byte) i4;
            length--;
            b = b2;
        }
        return this.counter.length;
    }

    public void reset() {
        System.arraycopy(this.IV, 0, this.counter, 0, this.counter.length);
        this.cipher.reset();
    }
}
