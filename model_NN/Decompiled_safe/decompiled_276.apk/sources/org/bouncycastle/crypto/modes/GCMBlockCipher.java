package org.bouncycastle.crypto.modes;

import java.math.BigInteger;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.BigIntegers;

public class GCMBlockCipher implements AEADBlockCipher {
    private static final int BLOCK_SIZE = 16;
    private static final BigInteger R = new BigInteger("11100001", 2).shiftLeft(120);
    private static final BigInteger ZERO = BigInteger.valueOf(0);
    private static final byte[] ZEROES = new byte[16];
    private byte[] A;
    private BigInteger H;
    private byte[] J0;
    private BigInteger S;
    private byte[] bufBlock;
    private int bufOff;
    private final BlockCipher cipher;
    private byte[] counter;
    private boolean forEncryption;
    private BigInteger initS;
    private KeyParameter keyParam;
    private byte[] macBlock;
    private int macSize;
    private byte[] nonce;
    private long totalLength;

    public GCMBlockCipher(BlockCipher blockCipher) {
        if (blockCipher.getBlockSize() != 16) {
            throw new IllegalArgumentException("cipher required with a block size of 16.");
        }
        this.cipher = blockCipher;
    }

    private byte[] asBlock(BigInteger bigInteger) {
        byte[] asUnsignedByteArray = BigIntegers.asUnsignedByteArray(bigInteger);
        if (asUnsignedByteArray.length >= 16) {
            return asUnsignedByteArray;
        }
        byte[] bArr = new byte[16];
        System.arraycopy(asUnsignedByteArray, 0, bArr, bArr.length - asUnsignedByteArray.length, asUnsignedByteArray.length);
        return bArr;
    }

    private void gCTRBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        inc(this.counter);
        byte[] bArr3 = new byte[16];
        this.cipher.processBlock(this.counter, 0, bArr3, 0);
        if (this.forEncryption) {
            System.arraycopy(ZEROES, i, bArr3, i, 16 - i);
            for (int i3 = i - 1; i3 >= 0; i3--) {
                bArr3[i3] = (byte) (bArr3[i3] ^ bArr[i3]);
                bArr2[i2 + i3] = bArr3[i3];
            }
            gHASHBlock(bArr3);
        } else {
            for (int i4 = i - 1; i4 >= 0; i4--) {
                bArr3[i4] = (byte) (bArr3[i4] ^ bArr[i4]);
                bArr2[i2 + i4] = bArr3[i4];
            }
            gHASHBlock(bArr);
        }
        this.totalLength += (long) i;
    }

    private BigInteger gHASH(byte[] bArr, boolean z) {
        BigInteger bigInteger = ZERO;
        for (int i = 0; i < bArr.length; i += 16) {
            byte[] bArr2 = new byte[16];
            System.arraycopy(bArr, i, bArr2, 0, Math.min(bArr.length - i, 16));
            bigInteger = multiply(bigInteger.xor(new BigInteger(1, bArr2)), this.H);
        }
        return bigInteger;
    }

    private void gHASHBlock(byte[] bArr) {
        byte[] bArr2;
        if (bArr.length > 16) {
            bArr2 = new byte[16];
            System.arraycopy(bArr, 0, bArr2, 0, 16);
        } else {
            bArr2 = bArr;
        }
        this.S = multiply(this.S.xor(new BigInteger(1, bArr2)), this.H);
    }

    private static void inc(byte[] bArr) {
        int i = 15;
        while (i >= 12) {
            byte b = (byte) ((bArr[i] + 1) & 255);
            bArr[i] = b;
            if (b == 0) {
                i--;
            } else {
                return;
            }
        }
    }

    private BigInteger multiply(BigInteger bigInteger, BigInteger bigInteger2) {
        BigInteger bigInteger3 = bigInteger;
        BigInteger bigInteger4 = ZERO;
        for (int i = 0; i < 128; i++) {
            if (bigInteger2.testBit(127 - i)) {
                bigInteger4 = bigInteger4.xor(bigInteger3);
            }
            boolean testBit = bigInteger3.testBit(0);
            bigInteger3 = bigInteger3.shiftRight(1);
            if (testBit) {
                bigInteger3 = bigInteger3.xor(R);
            }
        }
        return bigInteger4;
    }

    private int process(byte b, byte[] bArr, int i) throws DataLengthException {
        byte[] bArr2 = this.bufBlock;
        int i2 = this.bufOff;
        this.bufOff = i2 + 1;
        bArr2[i2] = b;
        if (this.bufOff != this.bufBlock.length) {
            return 0;
        }
        gCTRBlock(this.bufBlock, 16, bArr, i);
        if (!this.forEncryption) {
            System.arraycopy(this.bufBlock, 16, this.bufBlock, 0, 16);
        }
        this.bufOff = this.bufBlock.length - 16;
        return 16;
    }

    private void reset(boolean z) {
        this.S = this.initS;
        this.counter = Arrays.clone(this.J0);
        this.bufOff = 0;
        this.totalLength = 0;
        if (this.bufBlock != null) {
            Arrays.fill(this.bufBlock, (byte) 0);
        }
        if (z) {
            this.macBlock = null;
        }
        this.cipher.reset();
    }

    public int doFinal(byte[] bArr, int i) throws IllegalStateException, InvalidCipherTextException {
        int i2 = this.bufOff;
        if (!this.forEncryption) {
            if (i2 < this.macSize) {
                throw new InvalidCipherTextException("data too short");
            }
            i2 -= this.macSize;
        }
        if (i2 > 0) {
            byte[] bArr2 = new byte[16];
            System.arraycopy(this.bufBlock, 0, bArr2, 0, i2);
            gCTRBlock(bArr2, i2, bArr, i);
        }
        this.S = multiply(this.S.xor(BigInteger.valueOf((long) (this.A.length * 8)).shiftLeft(64).add(BigInteger.valueOf(this.totalLength * 8))), this.H);
        byte[] bArr3 = new byte[16];
        this.cipher.processBlock(this.J0, 0, bArr3, 0);
        byte[] asBlock = asBlock(this.S.xor(new BigInteger(1, bArr3)));
        if (this.forEncryption) {
            this.macBlock = asBlock;
            System.arraycopy(asBlock, 0, bArr, this.bufOff + i, asBlock.length);
            i2 += asBlock.length;
        } else {
            this.macBlock = new byte[this.macSize];
            System.arraycopy(this.bufBlock, i2, this.macBlock, 0, this.macSize);
            if (!Arrays.areEqual(asBlock, this.macBlock)) {
                throw new InvalidCipherTextException("mac check in GCM failed");
            }
        }
        reset(false);
        return i2;
    }

    public String getAlgorithmName() {
        return this.cipher.getAlgorithmName() + "/GCM";
    }

    public byte[] getMac() {
        return Arrays.clone(this.macBlock);
    }

    public int getOutputSize(int i) {
        return this.forEncryption ? this.bufOff + i + this.macSize : (this.bufOff + i) - this.macSize;
    }

    public BlockCipher getUnderlyingCipher() {
        return this.cipher;
    }

    public int getUpdateOutputSize(int i) {
        return ((this.bufOff + i) / 16) * 16;
    }

    public void init(boolean z, CipherParameters cipherParameters) throws IllegalArgumentException {
        this.forEncryption = z;
        this.macSize = 16;
        this.macBlock = null;
        this.bufBlock = new byte[(z ? 16 : this.macSize + 16)];
        if (cipherParameters instanceof AEADParameters) {
            AEADParameters aEADParameters = (AEADParameters) cipherParameters;
            this.nonce = aEADParameters.getNonce();
            this.A = aEADParameters.getAssociatedText();
            if (aEADParameters.getMacSize() != 128) {
                throw new IllegalArgumentException("only 128-bit MAC supported currently");
            }
            this.keyParam = aEADParameters.getKey();
        } else if (cipherParameters instanceof ParametersWithIV) {
            ParametersWithIV parametersWithIV = (ParametersWithIV) cipherParameters;
            this.nonce = parametersWithIV.getIV();
            this.A = null;
            this.keyParam = (KeyParameter) parametersWithIV.getParameters();
        } else {
            throw new IllegalArgumentException("invalid parameters passed to GCM");
        }
        if (this.nonce == null || this.nonce.length < 1) {
            throw new IllegalArgumentException("IV must be at least 1 byte");
        }
        if (this.A == null) {
            this.A = new byte[0];
        }
        this.cipher.init(true, this.keyParam);
        byte[] bArr = new byte[16];
        this.cipher.processBlock(ZEROES, 0, bArr, 0);
        this.H = new BigInteger(1, bArr);
        this.initS = gHASH(this.A, false);
        if (this.nonce.length == 12) {
            this.J0 = new byte[16];
            System.arraycopy(this.nonce, 0, this.J0, 0, this.nonce.length);
            this.J0[15] = 1;
        } else {
            this.J0 = asBlock(multiply(gHASH(this.nonce, true).xor(BigInteger.valueOf((long) (this.nonce.length * 8))), this.H));
        }
        this.S = this.initS;
        this.counter = Arrays.clone(this.J0);
        this.bufOff = 0;
        this.totalLength = 0;
    }

    public int processByte(byte b, byte[] bArr, int i) throws DataLengthException {
        return process(b, bArr, i);
    }

    public int processBytes(byte[] bArr, int i, int i2, byte[] bArr2, int i3) throws DataLengthException {
        int i4 = 0;
        for (int i5 = 0; i5 != i2; i5++) {
            i4 += process(bArr[i + i5], bArr2, i3 + i4);
        }
        return i4;
    }

    public void reset() {
        reset(true);
    }
}
