package org.bouncycastle.crypto.generators;

import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.params.DSAParameters;
import org.bouncycastle.crypto.params.DSAValidationParameters;

public class DSAParametersGenerator {
    private static final BigInteger ONE = BigInteger.valueOf(1);
    private static final BigInteger TWO = BigInteger.valueOf(2);
    private int certainty;
    private SecureRandom random;
    private int size;

    private void add(byte[] bArr, byte[] bArr2, int i) {
        int i2 = (bArr2[bArr2.length - 1] & 255) + i;
        bArr[bArr2.length - 1] = (byte) i2;
        int i3 = i2 >>> 8;
        for (int length = bArr2.length - 2; length >= 0; length--) {
            int i4 = i3 + (bArr2[length] & 255);
            bArr[length] = (byte) i4;
            i3 = i4 >>> 8;
        }
    }

    public DSAParameters generateParameters() {
        BigInteger bigInteger;
        byte[] bArr = new byte[20];
        byte[] bArr2 = new byte[20];
        byte[] bArr3 = new byte[20];
        byte[] bArr4 = new byte[20];
        SHA1Digest sHA1Digest = new SHA1Digest();
        int i = (this.size - 1) / 160;
        byte[] bArr5 = new byte[(this.size / 8)];
        BigInteger bigInteger2 = null;
        boolean z = false;
        int i2 = 0;
        BigInteger bigInteger3 = null;
        while (!z) {
            do {
                this.random.nextBytes(bArr);
                sHA1Digest.update(bArr, 0, bArr.length);
                sHA1Digest.doFinal(bArr2, 0);
                System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
                add(bArr3, bArr, 1);
                sHA1Digest.update(bArr3, 0, bArr3.length);
                sHA1Digest.doFinal(bArr3, 0);
                for (int i3 = 0; i3 != bArr4.length; i3++) {
                    bArr4[i3] = (byte) (bArr2[i3] ^ bArr3[i3]);
                }
                bArr4[0] = (byte) (bArr4[0] | Byte.MIN_VALUE);
                bArr4[19] = (byte) (bArr4[19] | 1);
                bigInteger = new BigInteger(1, bArr4);
            } while (!bigInteger.isProbablePrime(this.certainty));
            int i4 = 0;
            BigInteger bigInteger4 = bigInteger3;
            int i5 = 2;
            while (true) {
                if (i4 >= 4096) {
                    break;
                }
                for (int i6 = 0; i6 < i; i6++) {
                    add(bArr2, bArr, i5 + i6);
                    sHA1Digest.update(bArr2, 0, bArr2.length);
                    sHA1Digest.doFinal(bArr2, 0);
                    System.arraycopy(bArr2, 0, bArr5, bArr5.length - ((i6 + 1) * bArr2.length), bArr2.length);
                }
                add(bArr2, bArr, i5 + i);
                sHA1Digest.update(bArr2, 0, bArr2.length);
                sHA1Digest.doFinal(bArr2, 0);
                System.arraycopy(bArr2, bArr2.length - (bArr5.length - (bArr2.length * i)), bArr5, 0, bArr5.length - (bArr2.length * i));
                bArr5[0] = (byte) (bArr5[0] | Byte.MIN_VALUE);
                BigInteger bigInteger5 = new BigInteger(1, bArr5);
                bigInteger4 = bigInteger5.subtract(bigInteger5.mod(bigInteger.multiply(TWO)).subtract(ONE));
                if (bigInteger4.testBit(this.size - 1) && bigInteger4.isProbablePrime(this.certainty)) {
                    z = true;
                    break;
                }
                i4++;
                i5 += i + 1;
            }
            bigInteger3 = bigInteger4;
            int i7 = i4;
            bigInteger2 = bigInteger;
            i2 = i7;
        }
        BigInteger divide = bigInteger3.subtract(ONE).divide(bigInteger2);
        while (true) {
            BigInteger bigInteger6 = new BigInteger(this.size, this.random);
            if (bigInteger6.compareTo(ONE) > 0 && bigInteger6.compareTo(bigInteger3.subtract(ONE)) < 0) {
                BigInteger modPow = bigInteger6.modPow(divide, bigInteger3);
                if (modPow.compareTo(ONE) > 0) {
                    return new DSAParameters(bigInteger3, bigInteger2, modPow, new DSAValidationParameters(bArr, i2));
                }
            }
        }
    }

    public void init(int i, int i2, SecureRandom secureRandom) {
        this.size = i;
        this.certainty = i2;
        this.random = secureRandom;
    }
}
