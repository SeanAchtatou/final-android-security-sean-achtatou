package org.bouncycastle.crypto.signers;

import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.SignerWithRecovery;
import org.bouncycastle.crypto.digests.RIPEMD128Digest;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.params.RSAKeyParameters;

public class ISO9796d2Signer implements SignerWithRecovery {
    public static final int TRAILER_IMPLICIT = 188;
    public static final int TRAILER_RIPEMD128 = 13004;
    public static final int TRAILER_RIPEMD160 = 12748;
    public static final int TRAILER_SHA1 = 13260;
    private byte[] block;
    private AsymmetricBlockCipher cipher;
    private Digest digest;
    private boolean fullMessage;
    private int keyBits;
    private byte[] mBuf;
    private int messageLength;
    private byte[] recoveredMessage;
    private int trailer;

    public ISO9796d2Signer(AsymmetricBlockCipher asymmetricBlockCipher, Digest digest2) {
        this(asymmetricBlockCipher, digest2, false);
    }

    public ISO9796d2Signer(AsymmetricBlockCipher asymmetricBlockCipher, Digest digest2, boolean z) {
        this.cipher = asymmetricBlockCipher;
        this.digest = digest2;
        if (z) {
            this.trailer = 188;
        } else if (digest2 instanceof SHA1Digest) {
            this.trailer = 13260;
        } else if (digest2 instanceof RIPEMD160Digest) {
            this.trailer = 12748;
        } else if (digest2 instanceof RIPEMD128Digest) {
            this.trailer = 13004;
        } else {
            throw new IllegalArgumentException("no valid trailer for digest");
        }
    }

    private void clearBlock(byte[] bArr) {
        for (int i = 0; i != bArr.length; i++) {
            bArr[i] = 0;
        }
    }

    private boolean isSameAs(byte[] bArr, byte[] bArr2) {
        if (this.messageLength > this.mBuf.length) {
            if (this.mBuf.length > bArr2.length) {
                return false;
            }
            for (int i = 0; i != this.mBuf.length; i++) {
                if (bArr[i] != bArr2[i]) {
                    return false;
                }
            }
        } else if (this.messageLength != bArr2.length) {
            return false;
        } else {
            for (int i2 = 0; i2 != bArr2.length; i2++) {
                if (bArr[i2] != bArr2[i2]) {
                    return false;
                }
            }
        }
        return true;
    }

    public byte[] generateSignature() throws CryptoException {
        int i;
        int i2;
        byte b;
        int i3;
        int digestSize = this.digest.getDigestSize();
        if (this.trailer == 188) {
            int length = (this.block.length - digestSize) - 1;
            this.digest.doFinal(this.block, length);
            this.block[this.block.length - 1] = PSSSigner.TRAILER_IMPLICIT;
            int i4 = length;
            i = 8;
            i2 = i4;
        } else {
            int length2 = (this.block.length - digestSize) - 2;
            this.digest.doFinal(this.block, length2);
            this.block[this.block.length - 2] = (byte) (this.trailer >>> 8);
            this.block[this.block.length - 1] = (byte) this.trailer;
            int i5 = length2;
            i = 16;
            i2 = i5;
        }
        int i6 = ((((digestSize + this.messageLength) * 8) + i) + 4) - this.keyBits;
        if (i6 > 0) {
            int i7 = this.messageLength - ((i6 + 7) / 8);
            i3 = i2 - i7;
            System.arraycopy(this.mBuf, 0, this.block, i3, i7);
            b = 96;
        } else {
            b = 64;
            i3 = i2 - this.messageLength;
            System.arraycopy(this.mBuf, 0, this.block, i3, this.messageLength);
        }
        if (i3 - 1 > 0) {
            for (int i8 = i3 - 1; i8 != 0; i8--) {
                this.block[i8] = -69;
            }
            byte[] bArr = this.block;
            int i9 = i3 - 1;
            bArr[i9] = (byte) (bArr[i9] ^ 1);
            this.block[0] = 11;
            byte[] bArr2 = this.block;
            bArr2[0] = (byte) (b | bArr2[0]);
        } else {
            this.block[0] = 10;
            byte[] bArr3 = this.block;
            bArr3[0] = (byte) (b | bArr3[0]);
        }
        byte[] processBlock = this.cipher.processBlock(this.block, 0, this.block.length);
        clearBlock(this.mBuf);
        clearBlock(this.block);
        return processBlock;
    }

    public byte[] getRecoveredMessage() {
        return this.recoveredMessage;
    }

    public boolean hasFullMessage() {
        return this.fullMessage;
    }

    public void init(boolean z, CipherParameters cipherParameters) {
        RSAKeyParameters rSAKeyParameters = (RSAKeyParameters) cipherParameters;
        this.cipher.init(z, rSAKeyParameters);
        this.keyBits = rSAKeyParameters.getModulus().bitLength();
        this.block = new byte[((this.keyBits + 7) / 8)];
        if (this.trailer == 188) {
            this.mBuf = new byte[((this.block.length - this.digest.getDigestSize()) - 2)];
        } else {
            this.mBuf = new byte[((this.block.length - this.digest.getDigestSize()) - 3)];
        }
        reset();
    }

    public void reset() {
        this.digest.reset();
        this.messageLength = 0;
        clearBlock(this.mBuf);
        if (this.recoveredMessage != null) {
            clearBlock(this.recoveredMessage);
        }
        this.recoveredMessage = null;
        this.fullMessage = false;
    }

    public void update(byte b) {
        this.digest.update(b);
        if (this.messageLength < this.mBuf.length) {
            this.mBuf[this.messageLength] = b;
        }
        this.messageLength++;
    }

    public void update(byte[] bArr, int i, int i2) {
        this.digest.update(bArr, i, i2);
        if (this.messageLength < this.mBuf.length) {
            int i3 = 0;
            while (i3 < i2 && this.messageLength + i3 < this.mBuf.length) {
                this.mBuf[this.messageLength + i3] = bArr[i + i3];
                i3++;
            }
        }
        this.messageLength += i2;
    }

    public boolean verifySignature(byte[] bArr) {
        int i;
        try {
            byte[] processBlock = this.cipher.processBlock(bArr, 0, bArr.length);
            if (((processBlock[0] & 192) ^ 64) != 0) {
                clearBlock(this.mBuf);
                clearBlock(processBlock);
                return false;
            } else if (((processBlock[processBlock.length - 1] & 15) ^ 12) != 0) {
                clearBlock(this.mBuf);
                clearBlock(processBlock);
                return false;
            } else {
                if (((processBlock[processBlock.length - 1] & 255) ^ PSSSigner.TRAILER_IMPLICIT) == 0) {
                    i = 1;
                } else {
                    switch (((processBlock[processBlock.length - 2] & 255) << 8) | (processBlock[processBlock.length - 1] & 255)) {
                        case 12748:
                            if (!(this.digest instanceof RIPEMD160Digest)) {
                                throw new IllegalStateException("signer should be initialised with RIPEMD160");
                            }
                            break;
                        case 13004:
                            if (!(this.digest instanceof RIPEMD128Digest)) {
                                throw new IllegalStateException("signer should be initialised with RIPEMD128");
                            }
                            break;
                        case 13260:
                            if (!(this.digest instanceof SHA1Digest)) {
                                throw new IllegalStateException("signer should be initialised with SHA1");
                            }
                            break;
                        default:
                            throw new IllegalArgumentException("unrecognised hash in signature");
                    }
                    i = 2;
                }
                int i2 = 0;
                while (i2 != processBlock.length && ((processBlock[i2] & 15) ^ 10) != 0) {
                    i2++;
                }
                int i3 = i2 + 1;
                byte[] bArr2 = new byte[this.digest.getDigestSize()];
                int length = (processBlock.length - i) - bArr2.length;
                if (length - i3 <= 0) {
                    clearBlock(this.mBuf);
                    clearBlock(processBlock);
                    return false;
                }
                if ((processBlock[0] & 32) == 0) {
                    this.fullMessage = true;
                    this.digest.reset();
                    this.digest.update(processBlock, i3, length - i3);
                    this.digest.doFinal(bArr2, 0);
                    for (int i4 = 0; i4 != bArr2.length; i4++) {
                        int i5 = length + i4;
                        processBlock[i5] = (byte) (processBlock[i5] ^ bArr2[i4]);
                        if (processBlock[length + i4] != 0) {
                            clearBlock(this.mBuf);
                            clearBlock(processBlock);
                            return false;
                        }
                    }
                    this.recoveredMessage = new byte[(length - i3)];
                    System.arraycopy(processBlock, i3, this.recoveredMessage, 0, this.recoveredMessage.length);
                } else {
                    this.fullMessage = false;
                    this.digest.doFinal(bArr2, 0);
                    for (int i6 = 0; i6 != bArr2.length; i6++) {
                        int i7 = length + i6;
                        processBlock[i7] = (byte) (processBlock[i7] ^ bArr2[i6]);
                        if (processBlock[length + i6] != 0) {
                            clearBlock(this.mBuf);
                            clearBlock(processBlock);
                            return false;
                        }
                    }
                    this.recoveredMessage = new byte[(length - i3)];
                    System.arraycopy(processBlock, i3, this.recoveredMessage, 0, this.recoveredMessage.length);
                }
                if (this.messageLength == 0 || isSameAs(this.mBuf, this.recoveredMessage)) {
                    clearBlock(this.mBuf);
                    clearBlock(processBlock);
                    return true;
                }
                clearBlock(this.mBuf);
                clearBlock(processBlock);
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
