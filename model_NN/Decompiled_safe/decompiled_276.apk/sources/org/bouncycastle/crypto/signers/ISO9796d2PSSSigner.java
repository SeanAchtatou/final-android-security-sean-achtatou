package org.bouncycastle.crypto.signers;

import java.security.SecureRandom;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.SignerWithRecovery;
import org.bouncycastle.crypto.digests.RIPEMD128Digest;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.params.ParametersWithSalt;
import org.bouncycastle.crypto.params.RSAKeyParameters;

public class ISO9796d2PSSSigner implements SignerWithRecovery {
    public static final int TRAILER_IMPLICIT = 188;
    public static final int TRAILER_RIPEMD128 = 13004;
    public static final int TRAILER_RIPEMD160 = 12748;
    public static final int TRAILER_SHA1 = 13260;
    private byte[] block;
    private AsymmetricBlockCipher cipher;
    private Digest digest;
    private boolean fullMessage;
    private int hLen;
    private int keyBits;
    private byte[] mBuf;
    private int messageLength;
    private SecureRandom random;
    private byte[] recoveredMessage;
    private int saltLength;
    private byte[] standardSalt;
    private int trailer;

    public ISO9796d2PSSSigner(AsymmetricBlockCipher asymmetricBlockCipher, Digest digest2, int i) {
        this(asymmetricBlockCipher, digest2, i, false);
    }

    public ISO9796d2PSSSigner(AsymmetricBlockCipher asymmetricBlockCipher, Digest digest2, int i, boolean z) {
        this.cipher = asymmetricBlockCipher;
        this.digest = digest2;
        this.hLen = digest2.getDigestSize();
        this.saltLength = i;
        if (z) {
            this.trailer = 188;
        } else if (digest2 instanceof SHA1Digest) {
            this.trailer = 13260;
        } else if (digest2 instanceof RIPEMD160Digest) {
            this.trailer = 12748;
        } else if (digest2 instanceof RIPEMD128Digest) {
            this.trailer = 13004;
        } else {
            throw new IllegalArgumentException("no valid trailer for digest");
        }
    }

    private void ItoOSP(int i, byte[] bArr) {
        bArr[0] = (byte) (i >>> 24);
        bArr[1] = (byte) (i >>> 16);
        bArr[2] = (byte) (i >>> 8);
        bArr[3] = (byte) (i >>> 0);
    }

    private void LtoOSP(long j, byte[] bArr) {
        bArr[0] = (byte) ((int) (j >>> 56));
        bArr[1] = (byte) ((int) (j >>> 48));
        bArr[2] = (byte) ((int) (j >>> 40));
        bArr[3] = (byte) ((int) (j >>> 32));
        bArr[4] = (byte) ((int) (j >>> 24));
        bArr[5] = (byte) ((int) (j >>> 16));
        bArr[6] = (byte) ((int) (j >>> 8));
        bArr[7] = (byte) ((int) (j >>> 0));
    }

    private void clearBlock(byte[] bArr) {
        for (int i = 0; i != bArr.length; i++) {
            bArr[i] = 0;
        }
    }

    private boolean isSameAs(byte[] bArr, byte[] bArr2) {
        if (this.messageLength != bArr2.length) {
            return false;
        }
        for (int i = 0; i != bArr2.length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }

    private byte[] maskGeneratorFunction1(byte[] bArr, int i, int i2, int i3) {
        byte[] bArr2 = new byte[i3];
        byte[] bArr3 = new byte[this.hLen];
        byte[] bArr4 = new byte[4];
        this.digest.reset();
        int i4 = 0;
        while (i4 < i3 / this.hLen) {
            ItoOSP(i4, bArr4);
            this.digest.update(bArr, i, i2);
            this.digest.update(bArr4, 0, bArr4.length);
            this.digest.doFinal(bArr3, 0);
            System.arraycopy(bArr3, 0, bArr2, this.hLen * i4, this.hLen);
            i4++;
        }
        if (this.hLen * i4 < i3) {
            ItoOSP(i4, bArr4);
            this.digest.update(bArr, i, i2);
            this.digest.update(bArr4, 0, bArr4.length);
            this.digest.doFinal(bArr3, 0);
            System.arraycopy(bArr3, 0, bArr2, this.hLen * i4, bArr2.length - (i4 * this.hLen));
        }
        return bArr2;
    }

    public byte[] generateSignature() throws CryptoException {
        byte[] bArr;
        byte[] bArr2 = new byte[this.digest.getDigestSize()];
        this.digest.doFinal(bArr2, 0);
        byte[] bArr3 = new byte[8];
        LtoOSP((long) (this.messageLength * 8), bArr3);
        this.digest.update(bArr3, 0, bArr3.length);
        this.digest.update(this.mBuf, 0, this.messageLength);
        this.digest.update(bArr2, 0, bArr2.length);
        if (this.standardSalt != null) {
            bArr = this.standardSalt;
        } else {
            bArr = new byte[this.saltLength];
            this.random.nextBytes(bArr);
        }
        this.digest.update(bArr, 0, bArr.length);
        byte[] bArr4 = new byte[this.digest.getDigestSize()];
        this.digest.doFinal(bArr4, 0);
        int i = this.trailer == 188 ? 1 : 2;
        int length = ((((this.block.length - this.messageLength) - bArr.length) - this.hLen) - i) - 1;
        this.block[length] = 1;
        System.arraycopy(this.mBuf, 0, this.block, length + 1, this.messageLength);
        System.arraycopy(bArr, 0, this.block, length + 1 + this.messageLength, bArr.length);
        byte[] maskGeneratorFunction1 = maskGeneratorFunction1(bArr4, 0, bArr4.length, (this.block.length - this.hLen) - i);
        for (int i2 = 0; i2 != maskGeneratorFunction1.length; i2++) {
            byte[] bArr5 = this.block;
            bArr5[i2] = (byte) (bArr5[i2] ^ maskGeneratorFunction1[i2]);
        }
        System.arraycopy(bArr4, 0, this.block, (this.block.length - this.hLen) - i, this.hLen);
        if (this.trailer == 188) {
            this.block[this.block.length - 1] = PSSSigner.TRAILER_IMPLICIT;
        } else {
            this.block[this.block.length - 2] = (byte) (this.trailer >>> 8);
            this.block[this.block.length - 1] = (byte) this.trailer;
        }
        byte[] bArr6 = this.block;
        bArr6[0] = (byte) (bArr6[0] & Byte.MAX_VALUE);
        byte[] processBlock = this.cipher.processBlock(this.block, 0, this.block.length);
        clearBlock(this.mBuf);
        clearBlock(this.block);
        this.messageLength = 0;
        return processBlock;
    }

    public byte[] getRecoveredMessage() {
        return this.recoveredMessage;
    }

    public boolean hasFullMessage() {
        return this.fullMessage;
    }

    public void init(boolean z, CipherParameters cipherParameters) {
        int i;
        RSAKeyParameters rSAKeyParameters;
        int i2 = this.saltLength;
        if (cipherParameters instanceof ParametersWithRandom) {
            ParametersWithRandom parametersWithRandom = (ParametersWithRandom) cipherParameters;
            RSAKeyParameters rSAKeyParameters2 = (RSAKeyParameters) parametersWithRandom.getParameters();
            if (z) {
                this.random = parametersWithRandom.getRandom();
            }
            int i3 = i2;
            rSAKeyParameters = rSAKeyParameters2;
            i = i3;
        } else if (cipherParameters instanceof ParametersWithSalt) {
            ParametersWithSalt parametersWithSalt = (ParametersWithSalt) cipherParameters;
            RSAKeyParameters rSAKeyParameters3 = (RSAKeyParameters) parametersWithSalt.getParameters();
            this.standardSalt = parametersWithSalt.getSalt();
            int length = this.standardSalt.length;
            if (this.standardSalt.length != this.saltLength) {
                throw new IllegalArgumentException("Fixed salt is of wrong length");
            }
            int i4 = length;
            rSAKeyParameters = rSAKeyParameters3;
            i = i4;
        } else {
            RSAKeyParameters rSAKeyParameters4 = (RSAKeyParameters) cipherParameters;
            if (z) {
                this.random = new SecureRandom();
            }
            i = i2;
            rSAKeyParameters = rSAKeyParameters4;
        }
        this.cipher.init(z, rSAKeyParameters);
        this.keyBits = rSAKeyParameters.getModulus().bitLength();
        this.block = new byte[((this.keyBits + 7) / 8)];
        if (this.trailer == 188) {
            this.mBuf = new byte[((((this.block.length - this.digest.getDigestSize()) - i) - 1) - 1)];
        } else {
            this.mBuf = new byte[((((this.block.length - this.digest.getDigestSize()) - i) - 1) - 2)];
        }
        reset();
    }

    public void reset() {
        this.digest.reset();
        this.messageLength = 0;
        if (this.mBuf != null) {
            clearBlock(this.mBuf);
        }
        if (this.recoveredMessage != null) {
            clearBlock(this.recoveredMessage);
            this.recoveredMessage = null;
        }
        this.fullMessage = false;
    }

    public void update(byte b) {
        if (this.messageLength < this.mBuf.length) {
            byte[] bArr = this.mBuf;
            int i = this.messageLength;
            this.messageLength = i + 1;
            bArr[i] = b;
            return;
        }
        this.digest.update(b);
    }

    public void update(byte[] bArr, int i, int i2) {
        int i3 = i2;
        int i4 = i;
        while (i3 > 0 && this.messageLength < this.mBuf.length) {
            update(bArr[i4]);
            i4++;
            i3--;
        }
        if (i3 > 0) {
            this.digest.update(bArr, i4, i3);
        }
    }

    public boolean verifySignature(byte[] bArr) {
        int i;
        try {
            byte[] processBlock = this.cipher.processBlock(bArr, 0, bArr.length);
            if (processBlock.length < (this.keyBits + 7) / 8) {
                byte[] bArr2 = new byte[((this.keyBits + 7) / 8)];
                System.arraycopy(processBlock, 0, bArr2, bArr2.length - processBlock.length, processBlock.length);
                clearBlock(processBlock);
                processBlock = bArr2;
            }
            if (((processBlock[processBlock.length - 1] & 255) ^ PSSSigner.TRAILER_IMPLICIT) == 0) {
                i = 1;
            } else {
                switch (((processBlock[processBlock.length - 2] & 255) << 8) | (processBlock[processBlock.length - 1] & 255)) {
                    case 12748:
                        if (!(this.digest instanceof RIPEMD160Digest)) {
                            throw new IllegalStateException("signer should be initialised with RIPEMD160");
                        }
                        break;
                    case 13004:
                        if (!(this.digest instanceof RIPEMD128Digest)) {
                            throw new IllegalStateException("signer should be initialised with RIPEMD128");
                        }
                        break;
                    case 13260:
                        if (!(this.digest instanceof SHA1Digest)) {
                            throw new IllegalStateException("signer should be initialised with SHA1");
                        }
                        break;
                    default:
                        throw new IllegalArgumentException("unrecognised hash in signature");
                }
                i = 2;
            }
            byte[] bArr3 = new byte[this.hLen];
            this.digest.doFinal(bArr3, 0);
            byte[] maskGeneratorFunction1 = maskGeneratorFunction1(processBlock, (processBlock.length - this.hLen) - i, this.hLen, (processBlock.length - this.hLen) - i);
            for (int i2 = 0; i2 != maskGeneratorFunction1.length; i2++) {
                processBlock[i2] = (byte) (processBlock[i2] ^ maskGeneratorFunction1[i2]);
            }
            processBlock[0] = (byte) (processBlock[0] & Byte.MAX_VALUE);
            int i3 = 0;
            while (i3 != processBlock.length && processBlock[i3] != 1) {
                i3++;
            }
            int i4 = i3 + 1;
            if (i4 >= processBlock.length) {
                clearBlock(processBlock);
                return false;
            }
            this.fullMessage = i4 > 1;
            this.recoveredMessage = new byte[((maskGeneratorFunction1.length - i4) - this.saltLength)];
            System.arraycopy(processBlock, i4, this.recoveredMessage, 0, this.recoveredMessage.length);
            byte[] bArr4 = new byte[8];
            LtoOSP((long) (this.recoveredMessage.length * 8), bArr4);
            this.digest.update(bArr4, 0, bArr4.length);
            if (this.recoveredMessage.length != 0) {
                this.digest.update(this.recoveredMessage, 0, this.recoveredMessage.length);
            }
            this.digest.update(bArr3, 0, bArr3.length);
            this.digest.update(processBlock, this.recoveredMessage.length + i4, this.saltLength);
            byte[] bArr5 = new byte[this.digest.getDigestSize()];
            this.digest.doFinal(bArr5, 0);
            int length = (processBlock.length - i) - bArr5.length;
            for (int i5 = 0; i5 != bArr5.length; i5++) {
                if (bArr5[i5] != processBlock[length + i5]) {
                    clearBlock(processBlock);
                    clearBlock(bArr5);
                    clearBlock(this.recoveredMessage);
                    this.fullMessage = false;
                    return false;
                }
            }
            clearBlock(processBlock);
            clearBlock(bArr5);
            if (this.messageLength != 0) {
                if (!isSameAs(this.mBuf, this.recoveredMessage)) {
                    clearBlock(this.mBuf);
                    return false;
                }
                this.messageLength = 0;
            }
            clearBlock(this.mBuf);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
