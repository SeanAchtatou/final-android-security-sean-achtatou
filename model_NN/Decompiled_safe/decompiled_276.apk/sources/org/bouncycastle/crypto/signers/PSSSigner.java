package org.bouncycastle.crypto.signers;

import java.security.SecureRandom;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.params.RSABlindingParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;

public class PSSSigner implements Signer {
    public static final byte TRAILER_IMPLICIT = -68;
    private byte[] block;
    private AsymmetricBlockCipher cipher;
    private Digest digest;
    private int emBits;
    private int hLen;
    private byte[] mDash;
    private SecureRandom random;
    private int sLen;
    private byte[] salt;
    private byte trailer;

    public PSSSigner(AsymmetricBlockCipher asymmetricBlockCipher, Digest digest2, int i) {
        this(asymmetricBlockCipher, digest2, i, TRAILER_IMPLICIT);
    }

    public PSSSigner(AsymmetricBlockCipher asymmetricBlockCipher, Digest digest2, int i, byte b) {
        this.cipher = asymmetricBlockCipher;
        this.digest = digest2;
        this.hLen = digest2.getDigestSize();
        this.sLen = i;
        this.salt = new byte[i];
        this.mDash = new byte[(i + 8 + this.hLen)];
        this.trailer = b;
    }

    private void ItoOSP(int i, byte[] bArr) {
        bArr[0] = (byte) (i >>> 24);
        bArr[1] = (byte) (i >>> 16);
        bArr[2] = (byte) (i >>> 8);
        bArr[3] = (byte) (i >>> 0);
    }

    private void clearBlock(byte[] bArr) {
        for (int i = 0; i != bArr.length; i++) {
            bArr[i] = 0;
        }
    }

    private byte[] maskGeneratorFunction1(byte[] bArr, int i, int i2, int i3) {
        byte[] bArr2 = new byte[i3];
        byte[] bArr3 = new byte[this.hLen];
        byte[] bArr4 = new byte[4];
        this.digest.reset();
        int i4 = 0;
        while (i4 < i3 / this.hLen) {
            ItoOSP(i4, bArr4);
            this.digest.update(bArr, i, i2);
            this.digest.update(bArr4, 0, bArr4.length);
            this.digest.doFinal(bArr3, 0);
            System.arraycopy(bArr3, 0, bArr2, this.hLen * i4, this.hLen);
            i4++;
        }
        if (this.hLen * i4 < i3) {
            ItoOSP(i4, bArr4);
            this.digest.update(bArr, i, i2);
            this.digest.update(bArr4, 0, bArr4.length);
            this.digest.doFinal(bArr3, 0);
            System.arraycopy(bArr3, 0, bArr2, this.hLen * i4, bArr2.length - (i4 * this.hLen));
        }
        return bArr2;
    }

    public byte[] generateSignature() throws CryptoException, DataLengthException {
        if (this.emBits < (this.hLen * 8) + (this.sLen * 8) + 9) {
            throw new DataLengthException("encoding error");
        }
        this.digest.doFinal(this.mDash, (this.mDash.length - this.hLen) - this.sLen);
        if (this.sLen != 0) {
            this.random.nextBytes(this.salt);
            System.arraycopy(this.salt, 0, this.mDash, this.mDash.length - this.sLen, this.sLen);
        }
        byte[] bArr = new byte[this.hLen];
        this.digest.update(this.mDash, 0, this.mDash.length);
        this.digest.doFinal(bArr, 0);
        this.block[(((this.block.length - this.sLen) - 1) - this.hLen) - 1] = 1;
        System.arraycopy(this.salt, 0, this.block, ((this.block.length - this.sLen) - this.hLen) - 1, this.sLen);
        byte[] maskGeneratorFunction1 = maskGeneratorFunction1(bArr, 0, bArr.length, (this.block.length - this.hLen) - 1);
        for (int i = 0; i != maskGeneratorFunction1.length; i++) {
            byte[] bArr2 = this.block;
            bArr2[i] = (byte) (bArr2[i] ^ maskGeneratorFunction1[i]);
        }
        byte[] bArr3 = this.block;
        bArr3[0] = (byte) (bArr3[0] & (255 >> ((this.block.length * 8) - this.emBits)));
        System.arraycopy(bArr, 0, this.block, (this.block.length - this.hLen) - 1, this.hLen);
        this.block[this.block.length - 1] = this.trailer;
        byte[] processBlock = this.cipher.processBlock(this.block, 0, this.block.length);
        clearBlock(this.block);
        return processBlock;
    }

    public void init(boolean z, CipherParameters cipherParameters) {
        CipherParameters cipherParameters2;
        if (cipherParameters instanceof ParametersWithRandom) {
            ParametersWithRandom parametersWithRandom = (ParametersWithRandom) cipherParameters;
            cipherParameters2 = parametersWithRandom.getParameters();
            this.random = parametersWithRandom.getRandom();
        } else {
            if (z) {
                this.random = new SecureRandom();
            }
            cipherParameters2 = cipherParameters;
        }
        this.cipher.init(z, cipherParameters2);
        this.emBits = (cipherParameters2 instanceof RSABlindingParameters ? ((RSABlindingParameters) cipherParameters2).getPublicKey() : (RSAKeyParameters) cipherParameters2).getModulus().bitLength() - 1;
        this.block = new byte[((this.emBits + 7) / 8)];
        reset();
    }

    public void reset() {
        this.digest.reset();
    }

    public void update(byte b) {
        this.digest.update(b);
    }

    public void update(byte[] bArr, int i, int i2) {
        this.digest.update(bArr, i, i2);
    }

    public boolean verifySignature(byte[] bArr) {
        if (this.emBits < (this.hLen * 8) + (this.sLen * 8) + 9) {
            return false;
        }
        this.digest.doFinal(this.mDash, (this.mDash.length - this.hLen) - this.sLen);
        try {
            byte[] processBlock = this.cipher.processBlock(bArr, 0, bArr.length);
            System.arraycopy(processBlock, 0, this.block, this.block.length - processBlock.length, processBlock.length);
            if (this.block[this.block.length - 1] != this.trailer) {
                clearBlock(this.block);
                return false;
            }
            byte[] maskGeneratorFunction1 = maskGeneratorFunction1(this.block, (this.block.length - this.hLen) - 1, this.hLen, (this.block.length - this.hLen) - 1);
            for (int i = 0; i != maskGeneratorFunction1.length; i++) {
                byte[] bArr2 = this.block;
                bArr2[i] = (byte) (bArr2[i] ^ maskGeneratorFunction1[i]);
            }
            byte[] bArr3 = this.block;
            bArr3[0] = (byte) (bArr3[0] & (255 >> ((this.block.length * 8) - this.emBits)));
            for (int i2 = 0; i2 != ((this.block.length - this.hLen) - this.sLen) - 2; i2++) {
                if (this.block[i2] != 0) {
                    clearBlock(this.block);
                    return false;
                }
            }
            if (this.block[((this.block.length - this.hLen) - this.sLen) - 2] != 1) {
                clearBlock(this.block);
                return false;
            }
            System.arraycopy(this.block, ((this.block.length - this.sLen) - this.hLen) - 1, this.mDash, this.mDash.length - this.sLen, this.sLen);
            this.digest.update(this.mDash, 0, this.mDash.length);
            this.digest.doFinal(this.mDash, this.mDash.length - this.hLen);
            int length = this.mDash.length - this.hLen;
            int length2 = (this.block.length - this.hLen) - 1;
            for (int i3 = length; i3 != this.mDash.length; i3++) {
                if ((this.block[length2] ^ this.mDash[i3]) != 0) {
                    clearBlock(this.mDash);
                    clearBlock(this.block);
                    return false;
                }
                length2++;
            }
            clearBlock(this.mDash);
            clearBlock(this.block);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
