package org.bouncycastle.crypto;

public class BufferedBlockCipher {
    protected byte[] buf;
    protected int bufOff;
    protected BlockCipher cipher;
    protected boolean forEncryption;
    protected boolean partialBlockOkay;
    protected boolean pgpCFB;

    protected BufferedBlockCipher() {
    }

    public BufferedBlockCipher(BlockCipher blockCipher) {
        this.cipher = blockCipher;
        this.buf = new byte[blockCipher.getBlockSize()];
        this.bufOff = 0;
        String algorithmName = blockCipher.getAlgorithmName();
        int indexOf = algorithmName.indexOf(47) + 1;
        this.pgpCFB = indexOf > 0 && algorithmName.startsWith("PGP", indexOf);
        if (this.pgpCFB) {
            this.partialBlockOkay = true;
        } else {
            this.partialBlockOkay = indexOf > 0 && (algorithmName.startsWith("CFB", indexOf) || algorithmName.startsWith("OFB", indexOf) || algorithmName.startsWith("OpenPGP", indexOf) || algorithmName.startsWith("SIC", indexOf) || algorithmName.startsWith("GCTR", indexOf));
        }
    }

    public int doFinal(byte[] bArr, int i) throws DataLengthException, IllegalStateException, InvalidCipherTextException {
        int i2;
        if (this.bufOff + i > bArr.length) {
            throw new DataLengthException("output buffer too short for doFinal()");
        }
        if (this.bufOff != 0 && this.partialBlockOkay) {
            this.cipher.processBlock(this.buf, 0, this.buf, 0);
            i2 = this.bufOff;
            this.bufOff = 0;
            System.arraycopy(this.buf, 0, bArr, i, i2);
        } else if (this.bufOff != 0) {
            throw new DataLengthException("data not block size aligned");
        } else {
            i2 = 0;
        }
        reset();
        return i2;
    }

    public int getBlockSize() {
        return this.cipher.getBlockSize();
    }

    public int getOutputSize(int i) {
        int length;
        int i2 = this.bufOff + i;
        if (this.pgpCFB) {
            length = (i2 % this.buf.length) - (this.cipher.getBlockSize() + 2);
        } else {
            length = i2 % this.buf.length;
            if (length == 0) {
                return i2;
            }
        }
        return (i2 - length) + this.buf.length;
    }

    public BlockCipher getUnderlyingCipher() {
        return this.cipher;
    }

    public int getUpdateOutputSize(int i) {
        int i2 = this.bufOff + i;
        return i2 - (this.pgpCFB ? (i2 % this.buf.length) - (this.cipher.getBlockSize() + 2) : i2 % this.buf.length);
    }

    public void init(boolean z, CipherParameters cipherParameters) throws IllegalArgumentException {
        this.forEncryption = z;
        reset();
        this.cipher.init(z, cipherParameters);
    }

    public int processByte(byte b, byte[] bArr, int i) throws DataLengthException, IllegalStateException {
        byte[] bArr2 = this.buf;
        int i2 = this.bufOff;
        this.bufOff = i2 + 1;
        bArr2[i2] = b;
        if (this.bufOff != this.buf.length) {
            return 0;
        }
        int processBlock = this.cipher.processBlock(this.buf, 0, bArr, i);
        this.bufOff = 0;
        return processBlock;
    }

    public int processBytes(byte[] bArr, int i, int i2, byte[] bArr2, int i3) throws DataLengthException, IllegalStateException {
        int i4;
        int i5;
        int i6;
        if (i2 < 0) {
            throw new IllegalArgumentException("Can't have a negative input length!");
        }
        int blockSize = getBlockSize();
        int updateOutputSize = getUpdateOutputSize(i2);
        if (updateOutputSize <= 0 || updateOutputSize + i3 <= bArr2.length) {
            int length = this.buf.length - this.bufOff;
            if (i2 > length) {
                System.arraycopy(bArr, i, this.buf, this.bufOff, length);
                this.bufOff = 0;
                int i7 = i2 - length;
                int i8 = length + i;
                int processBlock = this.cipher.processBlock(this.buf, 0, bArr2, i3) + 0;
                while (i7 > this.buf.length) {
                    processBlock += this.cipher.processBlock(bArr, i8, bArr2, i3 + processBlock);
                    i7 -= blockSize;
                    i8 += blockSize;
                }
                i4 = processBlock;
                i5 = i7;
                i6 = i8;
            } else {
                i4 = 0;
                i5 = i2;
                i6 = i;
            }
            System.arraycopy(bArr, i6, this.buf, this.bufOff, i5);
            this.bufOff = i5 + this.bufOff;
            if (this.bufOff != this.buf.length) {
                return i4;
            }
            int processBlock2 = i4 + this.cipher.processBlock(this.buf, 0, bArr2, i3 + i4);
            this.bufOff = 0;
            return processBlock2;
        }
        throw new DataLengthException("output buffer too short");
    }

    public void reset() {
        for (int i = 0; i < this.buf.length; i++) {
            this.buf[i] = 0;
        }
        this.bufOff = 0;
        this.cipher.reset();
    }
}
