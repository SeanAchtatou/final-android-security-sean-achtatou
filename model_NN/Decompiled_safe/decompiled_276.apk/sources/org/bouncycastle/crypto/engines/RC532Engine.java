package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.RC5Parameters;

public class RC532Engine implements BlockCipher {
    private static final int P32 = -1209970333;
    private static final int Q32 = -1640531527;
    private int[] _S = null;
    private int _noRounds = 12;
    private boolean forEncryption;

    private int bytesToWord(byte[] bArr, int i) {
        return (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << Tnaf.POW_2_WIDTH) | ((bArr[i + 3] & 255) << 24);
    }

    private int decryptBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        int bytesToWord = bytesToWord(bArr, i);
        int bytesToWord2 = bytesToWord(bArr, i + 4);
        int i3 = bytesToWord;
        for (int i4 = this._noRounds; i4 >= 1; i4--) {
            bytesToWord2 = rotateRight(bytesToWord2 - this._S[(i4 * 2) + 1], i3) ^ i3;
            i3 = rotateRight(i3 - this._S[i4 * 2], bytesToWord2) ^ bytesToWord2;
        }
        wordToBytes(i3 - this._S[0], bArr2, i2);
        wordToBytes(bytesToWord2 - this._S[1], bArr2, i2 + 4);
        return 8;
    }

    private int encryptBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        int bytesToWord = bytesToWord(bArr, i) + this._S[0];
        int bytesToWord2 = bytesToWord(bArr, i + 4) + this._S[1];
        int i3 = bytesToWord;
        for (int i4 = 1; i4 <= this._noRounds; i4++) {
            i3 = rotateLeft(i3 ^ bytesToWord2, bytesToWord2) + this._S[i4 * 2];
            bytesToWord2 = rotateLeft(bytesToWord2 ^ i3, i3) + this._S[(i4 * 2) + 1];
        }
        wordToBytes(i3, bArr2, i2);
        wordToBytes(bytesToWord2, bArr2, i2 + 4);
        return 8;
    }

    private int rotateLeft(int i, int i2) {
        return (i << (i2 & 31)) | (i >>> (32 - (i2 & 31)));
    }

    private int rotateRight(int i, int i2) {
        return (i >>> (i2 & 31)) | (i << (32 - (i2 & 31)));
    }

    private void setKey(byte[] bArr) {
        int i = 0;
        int[] iArr = new int[((bArr.length + 3) / 4)];
        for (int i2 = 0; i2 != bArr.length; i2++) {
            int i3 = i2 / 4;
            iArr[i3] = iArr[i3] + ((bArr[i2] & 255) << ((i2 % 4) * 8));
        }
        this._S = new int[((this._noRounds + 1) * 2)];
        this._S[0] = P32;
        for (int i4 = 1; i4 < this._S.length; i4++) {
            this._S[i4] = this._S[i4 - 1] + Q32;
        }
        int length = iArr.length > this._S.length ? iArr.length * 3 : this._S.length * 3;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        for (int i8 = 0; i8 < length; i8++) {
            int[] iArr2 = this._S;
            i = rotateLeft(i + this._S[i6] + i7, 3);
            iArr2[i6] = i;
            i7 = rotateLeft(iArr[i5] + i + i7, i7 + i);
            iArr[i5] = i7;
            i6 = (i6 + 1) % this._S.length;
            i5 = (i5 + 1) % iArr.length;
        }
    }

    private void wordToBytes(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) i;
        bArr[i2 + 1] = (byte) (i >> 8);
        bArr[i2 + 2] = (byte) (i >> 16);
        bArr[i2 + 3] = (byte) (i >> 24);
    }

    public String getAlgorithmName() {
        return "RC5-32";
    }

    public int getBlockSize() {
        return 8;
    }

    public void init(boolean z, CipherParameters cipherParameters) {
        if (cipherParameters instanceof RC5Parameters) {
            RC5Parameters rC5Parameters = (RC5Parameters) cipherParameters;
            this._noRounds = rC5Parameters.getRounds();
            setKey(rC5Parameters.getKey());
        } else if (cipherParameters instanceof KeyParameter) {
            setKey(((KeyParameter) cipherParameters).getKey());
        } else {
            throw new IllegalArgumentException("invalid parameter passed to RC532 init - " + cipherParameters.getClass().getName());
        }
        this.forEncryption = z;
    }

    public int processBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        return this.forEncryption ? encryptBlock(bArr, i, bArr2, i2) : decryptBlock(bArr, i, bArr2, i2);
    }

    public void reset() {
    }
}
