package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.signers.PSSSigner;

public final class TwofishEngine implements BlockCipher {
    private static final int BLOCK_SIZE = 16;
    private static final int GF256_FDBK = 361;
    private static final int GF256_FDBK_2 = 180;
    private static final int GF256_FDBK_4 = 90;
    private static final int INPUT_WHITEN = 0;
    private static final int MAX_KEY_BITS = 256;
    private static final int MAX_ROUNDS = 16;
    private static final int OUTPUT_WHITEN = 4;
    private static final byte[][] P = {new byte[]{-87, 103, -77, -24, 4, -3, -93, 118, -102, -110, Byte.MIN_VALUE, 120, -28, -35, -47, 56, 13, -58, 53, -104, 24, -9, -20, 108, 67, 117, 55, 38, -6, 19, -108, 72, -14, -48, -117, 48, -124, 84, -33, 35, 25, 91, 61, 89, -13, -82, -94, -126, 99, 1, -125, 46, -39, 81, -101, 124, -90, -21, -91, -66, 22, 12, -29, 97, -64, -116, 58, -11, 115, 44, 37, 11, -69, 78, -119, 107, 83, 106, -76, -15, -31, -26, -67, 69, -30, -12, -74, 102, -52, -107, 3, 86, -44, 28, 30, -41, -5, -61, -114, -75, -23, -49, -65, -70, -22, 119, 57, -81, 51, -55, 98, 113, -127, 121, 9, -83, 36, -51, -7, -40, -27, -59, -71, 77, 68, 8, -122, -25, -95, 29, -86, -19, 6, 112, -78, -46, 65, 123, -96, 17, 49, -62, 39, -112, 32, -10, 96, -1, -106, 92, -79, -85, -98, -100, 82, 27, 95, -109, 10, -17, -111, -123, 73, -18, 45, 79, -113, 59, 71, -121, 109, 70, -42, 62, 105, 100, 42, -50, -53, 47, -4, -105, 5, 122, -84, Byte.MAX_VALUE, -43, 26, 75, 14, -89, 90, 40, 20, 63, 41, -120, 60, 76, 2, -72, -38, -80, 23, 85, 31, -118, 125, 87, -57, -115, 116, -73, -60, -97, 114, 126, 21, 34, 18, 88, 7, -103, 52, 110, 80, -34, 104, 101, PSSSigner.TRAILER_IMPLICIT, -37, -8, -56, -88, 43, 64, -36, -2, 50, -92, -54, Tnaf.POW_2_WIDTH, 33, -16, -45, 93, 15, 0, 111, -99, 54, 66, 74, 94, -63, -32}, new byte[]{117, -13, -58, -12, -37, 123, -5, -56, 74, -45, -26, 107, 69, 125, -24, 75, -42, 50, -40, -3, 55, 113, -15, -31, 48, 15, -8, 27, -121, -6, 6, 63, 94, -70, -82, 91, -118, 0, PSSSigner.TRAILER_IMPLICIT, -99, 109, -63, -79, 14, Byte.MIN_VALUE, 93, -46, -43, -96, -124, 7, 20, -75, -112, 44, -93, -78, 115, 76, 84, -110, 116, 54, 81, 56, -80, -67, 90, -4, 96, 98, -106, 108, 66, -9, Tnaf.POW_2_WIDTH, 124, 40, 39, -116, 19, -107, -100, -57, 36, 70, 59, 112, -54, -29, -123, -53, 17, -48, -109, -72, -90, -125, 32, -1, -97, 119, -61, -52, 3, 111, 8, -65, 64, -25, 43, -30, 121, 12, -86, -126, 65, 58, -22, -71, -28, -102, -92, -105, 126, -38, 122, 23, 102, -108, -95, 29, 61, -16, -34, -77, 11, 114, -89, 28, -17, -47, 83, 62, -113, 51, 38, 95, -20, 118, 42, 73, -127, -120, -18, 33, -60, 26, -21, -39, -59, 57, -103, -51, -83, 49, -117, 1, 24, 35, -35, 31, 78, 45, -7, 72, 79, -14, 101, -114, 120, 92, 88, 25, -115, -27, -104, 87, 103, Byte.MAX_VALUE, 5, 100, -81, 99, -74, -2, -11, -73, 60, -91, -50, -23, 104, 68, -32, 77, 67, 105, 41, 46, -84, 21, 89, -88, 10, -98, 110, 71, -33, 52, 53, 106, -49, -36, 34, -55, -64, -101, -119, -44, -19, -85, 18, -94, 13, 82, -69, 2, 47, -87, -41, 97, 30, -76, 80, 4, -10, -62, 22, 37, -122, 86, 85, 9, -66, -111}};
    private static final int P_00 = 1;
    private static final int P_01 = 0;
    private static final int P_02 = 0;
    private static final int P_03 = 1;
    private static final int P_04 = 1;
    private static final int P_10 = 0;
    private static final int P_11 = 0;
    private static final int P_12 = 1;
    private static final int P_13 = 1;
    private static final int P_14 = 0;
    private static final int P_20 = 1;
    private static final int P_21 = 1;
    private static final int P_22 = 0;
    private static final int P_23 = 0;
    private static final int P_24 = 0;
    private static final int P_30 = 0;
    private static final int P_31 = 1;
    private static final int P_32 = 1;
    private static final int P_33 = 0;
    private static final int P_34 = 1;
    private static final int ROUNDS = 16;
    private static final int ROUND_SUBKEYS = 8;
    private static final int RS_GF_FDBK = 333;
    private static final int SK_BUMP = 16843009;
    private static final int SK_ROTL = 9;
    private static final int SK_STEP = 33686018;
    private static final int TOTAL_SUBKEYS = 40;
    private boolean encrypting = false;
    private int[] gMDS0 = new int[256];
    private int[] gMDS1 = new int[256];
    private int[] gMDS2 = new int[256];
    private int[] gMDS3 = new int[256];
    private int[] gSBox;
    private int[] gSubKeys;
    private int k64Cnt = 0;
    private byte[] workingKey = null;

    public TwofishEngine() {
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        int[] iArr3 = new int[2];
        for (int i = 0; i < 256; i++) {
            int i2 = P[0][i] & 255;
            iArr[0] = i2;
            iArr2[0] = Mx_X(i2) & 255;
            iArr3[0] = Mx_Y(i2) & 255;
            int i3 = P[1][i] & 255;
            iArr[1] = i3;
            iArr2[1] = Mx_X(i3) & 255;
            iArr3[1] = Mx_Y(i3) & 255;
            this.gMDS0[i] = iArr[1] | (iArr2[1] << 8) | (iArr3[1] << 16) | (iArr3[1] << 24);
            this.gMDS1[i] = iArr3[0] | (iArr3[0] << 8) | (iArr2[0] << 16) | (iArr[0] << 24);
            this.gMDS2[i] = iArr2[1] | (iArr3[1] << 8) | (iArr[1] << 16) | (iArr3[1] << 24);
            this.gMDS3[i] = iArr2[0] | (iArr[0] << 8) | (iArr3[0] << 16) | (iArr2[0] << 24);
        }
    }

    private final void Bits32ToBytes(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) i;
        bArr[i2 + 1] = (byte) (i >> 8);
        bArr[i2 + 2] = (byte) (i >> 16);
        bArr[i2 + 3] = (byte) (i >> 24);
    }

    private final int BytesTo32Bits(byte[] bArr, int i) {
        return (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << Tnaf.POW_2_WIDTH) | ((bArr[i + 3] & 255) << 24);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v15, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v13, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v18, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v15, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v22, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v19, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v20, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int F32(int r14, int[] r15) {
        /*
            r13 = this;
            r10 = 1
            r9 = 0
            int r0 = r13.b0(r14)
            int r1 = r13.b1(r14)
            int r2 = r13.b2(r14)
            int r3 = r13.b3(r14)
            r4 = r15[r9]
            r5 = r15[r10]
            r6 = 2
            r6 = r15[r6]
            r7 = 3
            r7 = r15[r7]
            int r8 = r13.k64Cnt
            r8 = r8 & 3
            switch(r8) {
                case 0: goto L_0x006d;
                case 1: goto L_0x0025;
                case 2: goto L_0x0160;
                case 3: goto L_0x0158;
                default: goto L_0x0023;
            }
        L_0x0023:
            r0 = r9
        L_0x0024:
            return r0
        L_0x0025:
            int[] r5 = r13.gMDS0
            byte[][] r6 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r6 = r6[r9]
            byte r0 = r6[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r6 = r13.b0(r4)
            r0 = r0 ^ r6
            r0 = r5[r0]
            int[] r5 = r13.gMDS1
            byte[][] r6 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r6 = r6[r9]
            byte r1 = r6[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r6 = r13.b1(r4)
            r1 = r1 ^ r6
            r1 = r5[r1]
            r0 = r0 ^ r1
            int[] r1 = r13.gMDS2
            byte[][] r5 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r5 = r5[r10]
            byte r2 = r5[r2]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r5 = r13.b2(r4)
            r2 = r2 ^ r5
            r1 = r1[r2]
            r0 = r0 ^ r1
            int[] r1 = r13.gMDS3
            byte[][] r2 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r2 = r2[r10]
            byte r2 = r2[r3]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r3 = r13.b3(r4)
            r2 = r2 ^ r3
            r1 = r1[r2]
            r0 = r0 ^ r1
            goto L_0x0024
        L_0x006d:
            byte[][] r8 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r8 = r8[r10]
            byte r0 = r8[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r8 = r13.b0(r7)
            r0 = r0 ^ r8
            byte[][] r8 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r8 = r8[r9]
            byte r1 = r8[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r8 = r13.b1(r7)
            r1 = r1 ^ r8
            byte[][] r8 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r8 = r8[r9]
            byte r2 = r8[r2]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r8 = r13.b2(r7)
            r2 = r2 ^ r8
            byte[][] r8 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r8 = r8[r10]
            byte r3 = r8[r3]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r7 = r13.b3(r7)
            r3 = r3 ^ r7
            r11 = r3
            r3 = r0
            r0 = r11
            r12 = r1
            r1 = r2
            r2 = r12
        L_0x00a7:
            byte[][] r7 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r7 = r7[r10]
            byte r3 = r7[r3]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r7 = r13.b0(r6)
            r3 = r3 ^ r7
            byte[][] r7 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r7 = r7[r10]
            byte r2 = r7[r2]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r7 = r13.b1(r6)
            r2 = r2 ^ r7
            byte[][] r7 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r7 = r7[r9]
            byte r1 = r7[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r7 = r13.b2(r6)
            r1 = r1 ^ r7
            byte[][] r7 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r7 = r7[r9]
            byte r0 = r7[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r6 = r13.b3(r6)
            r0 = r0 ^ r6
        L_0x00db:
            int[] r6 = r13.gMDS0
            byte[][] r7 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r7 = r7[r9]
            byte[][] r8 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r8 = r8[r9]
            byte r3 = r8[r3]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r8 = r13.b0(r5)
            r3 = r3 ^ r8
            byte r3 = r7[r3]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r7 = r13.b0(r4)
            r3 = r3 ^ r7
            r3 = r6[r3]
            int[] r6 = r13.gMDS1
            byte[][] r7 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r7 = r7[r9]
            byte[][] r8 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r8 = r8[r10]
            byte r2 = r8[r2]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r8 = r13.b1(r5)
            r2 = r2 ^ r8
            byte r2 = r7[r2]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r7 = r13.b1(r4)
            r2 = r2 ^ r7
            r2 = r6[r2]
            r2 = r2 ^ r3
            int[] r3 = r13.gMDS2
            byte[][] r6 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r6 = r6[r10]
            byte[][] r7 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r7 = r7[r9]
            byte r1 = r7[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r7 = r13.b2(r5)
            r1 = r1 ^ r7
            byte r1 = r6[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r6 = r13.b2(r4)
            r1 = r1 ^ r6
            r1 = r3[r1]
            r1 = r1 ^ r2
            int[] r2 = r13.gMDS3
            byte[][] r3 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r3 = r3[r10]
            byte[][] r6 = org.bouncycastle.crypto.engines.TwofishEngine.P
            r6 = r6[r10]
            byte r0 = r6[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r5 = r13.b3(r5)
            r0 = r0 ^ r5
            byte r0 = r3[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r3 = r13.b3(r4)
            r0 = r0 ^ r3
            r0 = r2[r0]
            r0 = r0 ^ r1
            goto L_0x0024
        L_0x0158:
            r11 = r3
            r3 = r0
            r0 = r11
            r12 = r1
            r1 = r2
            r2 = r12
            goto L_0x00a7
        L_0x0160:
            r11 = r3
            r3 = r0
            r0 = r11
            r12 = r1
            r1 = r2
            r2 = r12
            goto L_0x00db
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.crypto.engines.TwofishEngine.F32(int, int[]):int");
    }

    private final int Fe32_0(int i) {
        return ((this.gSBox[((i & 255) * 2) + 0] ^ this.gSBox[(((i >>> 8) & 255) * 2) + 1]) ^ this.gSBox[(((i >>> 16) & 255) * 2) + 512]) ^ this.gSBox[(((i >>> 24) & 255) * 2) + 513];
    }

    private final int Fe32_3(int i) {
        return ((this.gSBox[(((i >>> 24) & 255) * 2) + 0] ^ this.gSBox[((i & 255) * 2) + 1]) ^ this.gSBox[(((i >>> 8) & 255) * 2) + 512]) ^ this.gSBox[(((i >>> 16) & 255) * 2) + 513];
    }

    private final int LFSR1(int i) {
        return (i >> 1) ^ ((i & 1) != 0 ? GF256_FDBK_2 : 0);
    }

    private final int LFSR2(int i) {
        return ((i >> 2) ^ ((i & 2) != 0 ? GF256_FDBK_2 : 0)) ^ ((i & 1) != 0 ? GF256_FDBK_4 : 0);
    }

    private final int Mx_X(int i) {
        return LFSR2(i) ^ i;
    }

    private final int Mx_Y(int i) {
        return (LFSR1(i) ^ i) ^ LFSR2(i);
    }

    private final int RS_MDS_Encode(int i, int i2) {
        int i3 = i2;
        for (int i4 = 0; i4 < 4; i4++) {
            i3 = RS_rem(i3);
        }
        int i5 = i3 ^ i;
        for (int i6 = 0; i6 < 4; i6++) {
            i5 = RS_rem(i5);
        }
        return i5;
    }

    private final int RS_rem(int i) {
        int i2 = (i >>> 24) & 255;
        int i3 = ((i2 << 1) ^ ((i2 & 128) != 0 ? RS_GF_FDBK : 0)) & 255;
        int i4 = ((i2 >>> 1) ^ ((i2 & 1) != 0 ? 166 : 0)) ^ i3;
        return i2 ^ (((i3 << 16) ^ ((i << 8) ^ (i4 << 24))) ^ (i4 << 8));
    }

    private final int b0(int i) {
        return i & 255;
    }

    private final int b1(int i) {
        return (i >>> 8) & 255;
    }

    private final int b2(int i) {
        return (i >>> 16) & 255;
    }

    private final int b3(int i) {
        return (i >>> 24) & 255;
    }

    private void decryptBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        int BytesTo32Bits = BytesTo32Bits(bArr, i) ^ this.gSubKeys[4];
        int BytesTo32Bits2 = BytesTo32Bits(bArr, i + 4) ^ this.gSubKeys[5];
        int i3 = BytesTo32Bits;
        int i4 = 0;
        int BytesTo32Bits3 = BytesTo32Bits(bArr, i + 8) ^ this.gSubKeys[6];
        int BytesTo32Bits4 = BytesTo32Bits(bArr, i + 12) ^ this.gSubKeys[7];
        int i5 = BytesTo32Bits2;
        int i6 = 39;
        int i7 = i5;
        while (i4 < 16) {
            int Fe32_0 = Fe32_0(i3);
            int Fe32_3 = Fe32_3(i7);
            int i8 = i6 - 1;
            int i9 = (this.gSubKeys[i6] + ((Fe32_3 * 2) + Fe32_0)) ^ BytesTo32Bits4;
            int i10 = (BytesTo32Bits3 << 1) | (BytesTo32Bits3 >>> 31);
            int i11 = Fe32_0 + Fe32_3;
            int i12 = i8 - 1;
            int i13 = i10 ^ (i11 + this.gSubKeys[i8]);
            int i14 = (i9 << 31) | (i9 >>> 1);
            int Fe32_02 = Fe32_0(i13);
            int Fe32_32 = Fe32_3(i14);
            int i15 = i12 - 1;
            int i16 = i7 ^ (this.gSubKeys[i12] + ((Fe32_32 * 2) + Fe32_02));
            i7 = (i16 << 31) | (i16 >>> 1);
            i4 += 2;
            i3 = ((Fe32_02 + Fe32_32) + this.gSubKeys[i15]) ^ ((i3 >>> 31) | (i3 << 1));
            BytesTo32Bits3 = i13;
            BytesTo32Bits4 = i14;
            i6 = i15 - 1;
        }
        Bits32ToBytes(this.gSubKeys[0] ^ BytesTo32Bits3, bArr2, i2);
        Bits32ToBytes(this.gSubKeys[1] ^ BytesTo32Bits4, bArr2, i2 + 4);
        Bits32ToBytes(this.gSubKeys[2] ^ i3, bArr2, i2 + 8);
        Bits32ToBytes(this.gSubKeys[3] ^ i7, bArr2, i2 + 12);
    }

    private void encryptBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        int BytesTo32Bits = BytesTo32Bits(bArr, i) ^ this.gSubKeys[0];
        int BytesTo32Bits2 = BytesTo32Bits(bArr, i + 4) ^ this.gSubKeys[1];
        int i3 = BytesTo32Bits;
        int i4 = 0;
        int BytesTo32Bits3 = BytesTo32Bits(bArr, i + 8) ^ this.gSubKeys[2];
        int BytesTo32Bits4 = BytesTo32Bits(bArr, i + 12) ^ this.gSubKeys[3];
        int i5 = BytesTo32Bits2;
        int i6 = 8;
        int i7 = i5;
        while (i4 < 16) {
            int Fe32_0 = Fe32_0(i3);
            int Fe32_3 = Fe32_3(i7);
            int i8 = i6 + 1;
            int i9 = (this.gSubKeys[i6] + (Fe32_0 + Fe32_3)) ^ BytesTo32Bits3;
            int i10 = (i9 << 31) | (i9 >>> 1);
            int i11 = i8 + 1;
            BytesTo32Bits4 = ((BytesTo32Bits4 >>> 31) | (BytesTo32Bits4 << 1)) ^ (((Fe32_3 * 2) + Fe32_0) + this.gSubKeys[i8]);
            int Fe32_02 = Fe32_0(i10);
            int Fe32_32 = Fe32_3(BytesTo32Bits4);
            int i12 = i11 + 1;
            int i13 = i3 ^ (this.gSubKeys[i11] + (Fe32_02 + Fe32_32));
            i3 = (i13 << 31) | (i13 >>> 1);
            i4 += 2;
            i7 = ((Fe32_02 + (Fe32_32 * 2)) + this.gSubKeys[i12]) ^ ((i7 >>> 31) | (i7 << 1));
            BytesTo32Bits3 = i10;
            i6 = i12 + 1;
        }
        Bits32ToBytes(this.gSubKeys[4] ^ BytesTo32Bits3, bArr2, i2);
        Bits32ToBytes(this.gSubKeys[5] ^ BytesTo32Bits4, bArr2, i2 + 4);
        Bits32ToBytes(this.gSubKeys[6] ^ i3, bArr2, i2 + 8);
        Bits32ToBytes(this.gSubKeys[7] ^ i7, bArr2, i2 + 12);
    }

    private void setKey(byte[] bArr) {
        byte b;
        byte b2;
        byte b3;
        byte b4;
        byte b5;
        byte b6;
        byte b7;
        byte b8;
        int[] iArr = new int[4];
        int[] iArr2 = new int[4];
        int[] iArr3 = new int[4];
        this.gSubKeys = new int[TOTAL_SUBKEYS];
        if (this.k64Cnt < 1) {
            throw new IllegalArgumentException("Key size less than 64 bits");
        } else if (this.k64Cnt > 4) {
            throw new IllegalArgumentException("Key size larger than 256 bits");
        } else {
            for (int i = 0; i < this.k64Cnt; i++) {
                int i2 = i * 8;
                iArr[i] = BytesTo32Bits(bArr, i2);
                iArr2[i] = BytesTo32Bits(bArr, i2 + 4);
                iArr3[(this.k64Cnt - 1) - i] = RS_MDS_Encode(iArr[i], iArr2[i]);
            }
            for (int i3 = 0; i3 < 20; i3++) {
                int i4 = SK_STEP * i3;
                int F32 = F32(i4, iArr);
                int F322 = F32(i4 + SK_BUMP, iArr2);
                int i5 = (F322 >>> 24) | (F322 << 8);
                int i6 = F32 + i5;
                this.gSubKeys[i3 * 2] = i6;
                int i7 = i5 + i6;
                this.gSubKeys[(i3 * 2) + 1] = (i7 >>> 23) | (i7 << 9);
            }
            int i8 = iArr3[0];
            int i9 = iArr3[1];
            int i10 = iArr3[2];
            int i11 = iArr3[3];
            this.gSBox = new int[1024];
            for (int i12 = 0; i12 < 256; i12++) {
                switch (this.k64Cnt & 3) {
                    case 0:
                        byte b0 = (P[1][i12] & 255) ^ b0(i11);
                        byte b1 = (P[0][i12] & 255) ^ b1(i11);
                        byte b22 = (P[0][i12] & 255) ^ b2(i11);
                        b8 = b0;
                        b5 = (P[1][i12] & 255) ^ b3(i11);
                        byte b9 = b1;
                        b6 = b22;
                        b7 = b9;
                        b4 = (P[1][b8] & 255) ^ b0(i10);
                        b3 = (P[1][b7] & 255) ^ b1(i10);
                        b2 = (P[0][b6] & 255) ^ b2(i10);
                        b = (P[0][b5] & 255) ^ b3(i10);
                        break;
                    case 1:
                        this.gSBox[i12 * 2] = this.gMDS0[(P[0][i12] & 255) ^ b0(i8)];
                        this.gSBox[(i12 * 2) + 1] = this.gMDS1[(P[0][i12] & 255) ^ b1(i8)];
                        this.gSBox[(i12 * 2) + 512] = this.gMDS2[(P[1][i12] & 255) ^ b2(i8)];
                        this.gSBox[(i12 * 2) + 513] = this.gMDS3[(P[1][i12] & 255) ^ b3(i8)];
                        continue;
                    case 2:
                        b = i12;
                        b2 = i12;
                        b3 = i12;
                        b4 = i12;
                        break;
                    case 3:
                        b5 = i12;
                        b6 = i12;
                        b7 = i12;
                        b8 = i12;
                        b4 = (P[1][b8] & 255) ^ b0(i10);
                        b3 = (P[1][b7] & 255) ^ b1(i10);
                        b2 = (P[0][b6] & 255) ^ b2(i10);
                        b = (P[0][b5] & 255) ^ b3(i10);
                        break;
                    default:
                }
                this.gSBox[i12 * 2] = this.gMDS0[(P[0][(P[0][b4] & 255) ^ b0(i9)] & 255) ^ b0(i8)];
                this.gSBox[(i12 * 2) + 1] = this.gMDS1[(P[0][(P[1][b3] & 255) ^ b1(i9)] & 255) ^ b1(i8)];
                this.gSBox[(i12 * 2) + 512] = this.gMDS2[(P[1][(P[0][b2] & 255) ^ b2(i9)] & 255) ^ b2(i8)];
                this.gSBox[(i12 * 2) + 513] = this.gMDS3[(P[1][(P[1][b] & 255) ^ b3(i9)] & 255) ^ b3(i8)];
            }
        }
    }

    public String getAlgorithmName() {
        return "Twofish";
    }

    public int getBlockSize() {
        return 16;
    }

    public void init(boolean z, CipherParameters cipherParameters) {
        if (cipherParameters instanceof KeyParameter) {
            this.encrypting = z;
            this.workingKey = ((KeyParameter) cipherParameters).getKey();
            this.k64Cnt = this.workingKey.length / 8;
            setKey(this.workingKey);
            return;
        }
        throw new IllegalArgumentException("invalid parameter passed to Twofish init - " + cipherParameters.getClass().getName());
    }

    public final int processBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        if (this.workingKey == null) {
            throw new IllegalStateException("Twofish not initialised");
        } else if (i + 16 > bArr.length) {
            throw new DataLengthException("input buffer too short");
        } else if (i2 + 16 > bArr2.length) {
            throw new DataLengthException("output buffer too short");
        } else if (this.encrypting) {
            encryptBlock(bArr, i, bArr2, i2);
            return 16;
        } else {
            decryptBlock(bArr, i, bArr2, i2);
            return 16;
        }
    }

    public void reset() {
        if (this.workingKey != null) {
            setKey(this.workingKey);
        }
    }
}
