package org.bouncycastle.crypto.engines;

import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;

public class RSABlindedEngine implements AsymmetricBlockCipher {
    private static BigInteger ZERO = BigInteger.valueOf(0);
    private RSACoreEngine core = new RSACoreEngine();
    private RSAKeyParameters key;
    private SecureRandom random;

    private BigInteger calculateR(BigInteger bigInteger) {
        int bitLength = bigInteger.bitLength() - 1;
        int i = bitLength / 2;
        int nextInt = (((bitLength - i) / 255) * (this.random.nextInt() & 255)) + i;
        BigInteger bigInteger2 = new BigInteger(nextInt, this.random);
        while (bigInteger2.equals(ZERO)) {
            bigInteger2 = new BigInteger(nextInt, this.random);
        }
        return bigInteger2;
    }

    public int getInputBlockSize() {
        return this.core.getInputBlockSize();
    }

    public int getOutputBlockSize() {
        return this.core.getOutputBlockSize();
    }

    public void init(boolean z, CipherParameters cipherParameters) {
        this.core.init(z, cipherParameters);
        if (cipherParameters instanceof ParametersWithRandom) {
            ParametersWithRandom parametersWithRandom = (ParametersWithRandom) cipherParameters;
            this.key = (RSAKeyParameters) parametersWithRandom.getParameters();
            this.random = parametersWithRandom.getRandom();
            return;
        }
        this.key = (RSAKeyParameters) cipherParameters;
        this.random = new SecureRandom();
    }

    public byte[] processBlock(byte[] bArr, int i, int i2) {
        if (this.key == null) {
            throw new IllegalStateException("RSA engine not initialised");
        } else if (!(this.key instanceof RSAPrivateCrtKeyParameters)) {
            return this.core.convertOutput(this.core.processBlock(this.core.convertInput(bArr, i, i2)));
        } else {
            RSAPrivateCrtKeyParameters rSAPrivateCrtKeyParameters = (RSAPrivateCrtKeyParameters) this.key;
            if (rSAPrivateCrtKeyParameters.getPublicExponent() == null) {
                return this.core.convertOutput(this.core.processBlock(this.core.convertInput(bArr, i, i2)));
            }
            BigInteger convertInput = this.core.convertInput(bArr, i, i2);
            BigInteger modulus = rSAPrivateCrtKeyParameters.getModulus();
            BigInteger calculateR = calculateR(modulus);
            return this.core.convertOutput(this.core.processBlock(calculateR.modPow(rSAPrivateCrtKeyParameters.getPublicExponent(), modulus).multiply(convertInput).mod(modulus)).multiply(calculateR.modInverse(modulus)).mod(modulus));
        }
    }
}
