package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;

public class SerpentEngine implements BlockCipher {
    private static final int BLOCK_SIZE = 16;
    static final int PHI = -1640531527;
    static final int ROUNDS = 32;
    private int X0;
    private int X1;
    private int X2;
    private int X3;
    private boolean encrypting;
    private int[] wKey;

    private void LT() {
        int rotateLeft = rotateLeft(this.X0, 13);
        int rotateLeft2 = rotateLeft(this.X2, 3);
        this.X1 = rotateLeft((this.X1 ^ rotateLeft) ^ rotateLeft2, 1);
        this.X3 = rotateLeft((this.X3 ^ rotateLeft2) ^ (rotateLeft << 3), 7);
        this.X0 = rotateLeft((rotateLeft ^ this.X1) ^ this.X3, 5);
        this.X2 = rotateLeft((this.X3 ^ rotateLeft2) ^ (this.X1 << 7), 22);
    }

    private int bytesToWord(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << Tnaf.POW_2_WIDTH) | ((bArr[i + 2] & 255) << 8) | (bArr[i + 3] & 255);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [org.bouncycastle.crypto.engines.SerpentEngine] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void decryptBlock(byte[] r5, int r6, byte[] r7, int r8) {
        /*
            r4 = this;
            int[] r0 = r4.wKey
            r1 = 131(0x83, float:1.84E-43)
            r0 = r0[r1]
            int r1 = r4.bytesToWord(r5, r6)
            r0 = r0 ^ r1
            r4.X3 = r0
            int[] r0 = r4.wKey
            r1 = 130(0x82, float:1.82E-43)
            r0 = r0[r1]
            int r1 = r6 + 4
            int r1 = r4.bytesToWord(r5, r1)
            r0 = r0 ^ r1
            r4.X2 = r0
            int[] r0 = r4.wKey
            r1 = 129(0x81, float:1.81E-43)
            r0 = r0[r1]
            int r1 = r6 + 8
            int r1 = r4.bytesToWord(r5, r1)
            r0 = r0 ^ r1
            r4.X1 = r0
            int[] r0 = r4.wKey
            r1 = 128(0x80, float:1.794E-43)
            r0 = r0[r1]
            int r1 = r6 + 12
            int r1 = r4.bytesToWord(r5, r1)
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib7(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 124(0x7c, float:1.74E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 125(0x7d, float:1.75E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 126(0x7e, float:1.77E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 127(0x7f, float:1.78E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib6(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 120(0x78, float:1.68E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 121(0x79, float:1.7E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 122(0x7a, float:1.71E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 123(0x7b, float:1.72E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib5(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 116(0x74, float:1.63E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 117(0x75, float:1.64E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 118(0x76, float:1.65E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 119(0x77, float:1.67E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib4(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 112(0x70, float:1.57E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 113(0x71, float:1.58E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 114(0x72, float:1.6E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 115(0x73, float:1.61E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib3(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 108(0x6c, float:1.51E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 109(0x6d, float:1.53E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 110(0x6e, float:1.54E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 111(0x6f, float:1.56E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib2(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 104(0x68, float:1.46E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 105(0x69, float:1.47E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 106(0x6a, float:1.49E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 107(0x6b, float:1.5E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib1(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 100
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 101(0x65, float:1.42E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 102(0x66, float:1.43E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 103(0x67, float:1.44E-43)
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib0(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 96
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 97
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 98
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 99
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib7(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 92
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 93
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 94
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 95
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib6(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 88
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 89
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 90
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 91
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib5(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 84
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 85
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 86
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 87
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib4(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 80
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 81
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 82
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 83
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib3(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 76
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 77
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 78
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 79
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib2(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 72
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 73
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 74
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 75
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib1(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 68
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 69
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 70
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 71
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib0(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 64
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 65
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 66
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 67
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib7(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 60
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 61
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 62
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 63
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib6(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 56
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 57
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 58
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 59
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib5(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 52
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 53
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 54
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 55
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib4(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 48
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 49
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 50
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 51
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib3(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 44
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 45
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 46
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 47
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib2(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 40
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 41
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 42
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 43
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib1(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 36
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 37
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 38
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 39
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib0(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 32
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 33
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 34
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 35
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib7(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 28
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 29
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 30
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 31
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib6(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 24
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 25
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 26
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 27
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib5(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 20
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 21
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 22
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 23
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib4(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 16
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 17
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 18
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 19
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib3(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 12
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 13
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 14
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 15
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib2(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 8
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 9
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 10
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 11
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib1(r0, r1, r2, r3)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 4
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X0 = r0
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 5
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X1 = r0
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 6
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X2 = r0
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 7
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.X3 = r0
            r4.inverseLT()
            int r0 = r4.X0
            int r1 = r4.X1
            int r2 = r4.X2
            int r3 = r4.X3
            r4.ib0(r0, r1, r2, r3)
            int r0 = r4.X3
            int[] r1 = r4.wKey
            r2 = 3
            r1 = r1[r2]
            r0 = r0 ^ r1
            r4.wordToBytes(r0, r7, r8)
            int r0 = r4.X2
            int[] r1 = r4.wKey
            r2 = 2
            r1 = r1[r2]
            r0 = r0 ^ r1
            int r1 = r8 + 4
            r4.wordToBytes(r0, r7, r1)
            int r0 = r4.X1
            int[] r1 = r4.wKey
            r2 = 1
            r1 = r1[r2]
            r0 = r0 ^ r1
            int r1 = r8 + 8
            r4.wordToBytes(r0, r7, r1)
            int r0 = r4.X0
            int[] r1 = r4.wKey
            r2 = 0
            r1 = r1[r2]
            r0 = r0 ^ r1
            int r1 = r8 + 12
            r4.wordToBytes(r0, r7, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.crypto.engines.SerpentEngine.decryptBlock(byte[], int, byte[], int):void");
    }

    private void encryptBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        this.X3 = bytesToWord(bArr, i);
        this.X2 = bytesToWord(bArr, i + 4);
        this.X1 = bytesToWord(bArr, i + 8);
        this.X0 = bytesToWord(bArr, i + 12);
        sb0(this.wKey[0] ^ this.X0, this.wKey[1] ^ this.X1, this.wKey[2] ^ this.X2, this.wKey[3] ^ this.X3);
        LT();
        sb1(this.wKey[4] ^ this.X0, this.wKey[5] ^ this.X1, this.wKey[6] ^ this.X2, this.wKey[7] ^ this.X3);
        LT();
        sb2(this.wKey[8] ^ this.X0, this.wKey[9] ^ this.X1, this.wKey[10] ^ this.X2, this.wKey[11] ^ this.X3);
        LT();
        sb3(this.wKey[12] ^ this.X0, this.wKey[13] ^ this.X1, this.wKey[14] ^ this.X2, this.wKey[15] ^ this.X3);
        LT();
        sb4(this.wKey[16] ^ this.X0, this.wKey[17] ^ this.X1, this.wKey[18] ^ this.X2, this.wKey[19] ^ this.X3);
        LT();
        sb5(this.wKey[20] ^ this.X0, this.wKey[21] ^ this.X1, this.wKey[22] ^ this.X2, this.wKey[23] ^ this.X3);
        LT();
        sb6(this.wKey[24] ^ this.X0, this.wKey[25] ^ this.X1, this.wKey[26] ^ this.X2, this.wKey[27] ^ this.X3);
        LT();
        sb7(this.wKey[28] ^ this.X0, this.wKey[29] ^ this.X1, this.wKey[30] ^ this.X2, this.wKey[31] ^ this.X3);
        LT();
        sb0(this.wKey[32] ^ this.X0, this.wKey[33] ^ this.X1, this.wKey[34] ^ this.X2, this.wKey[35] ^ this.X3);
        LT();
        sb1(this.wKey[36] ^ this.X0, this.wKey[37] ^ this.X1, this.wKey[38] ^ this.X2, this.wKey[39] ^ this.X3);
        LT();
        sb2(this.wKey[40] ^ this.X0, this.wKey[41] ^ this.X1, this.wKey[42] ^ this.X2, this.wKey[43] ^ this.X3);
        LT();
        sb3(this.wKey[44] ^ this.X0, this.wKey[45] ^ this.X1, this.wKey[46] ^ this.X2, this.wKey[47] ^ this.X3);
        LT();
        sb4(this.wKey[48] ^ this.X0, this.wKey[49] ^ this.X1, this.wKey[50] ^ this.X2, this.wKey[51] ^ this.X3);
        LT();
        sb5(this.wKey[52] ^ this.X0, this.wKey[53] ^ this.X1, this.wKey[54] ^ this.X2, this.wKey[55] ^ this.X3);
        LT();
        sb6(this.wKey[56] ^ this.X0, this.wKey[57] ^ this.X1, this.wKey[58] ^ this.X2, this.wKey[59] ^ this.X3);
        LT();
        sb7(this.wKey[60] ^ this.X0, this.wKey[61] ^ this.X1, this.wKey[62] ^ this.X2, this.wKey[63] ^ this.X3);
        LT();
        sb0(this.wKey[64] ^ this.X0, this.wKey[65] ^ this.X1, this.wKey[66] ^ this.X2, this.wKey[67] ^ this.X3);
        LT();
        sb1(this.wKey[68] ^ this.X0, this.wKey[69] ^ this.X1, this.wKey[70] ^ this.X2, this.wKey[71] ^ this.X3);
        LT();
        sb2(this.wKey[72] ^ this.X0, this.wKey[73] ^ this.X1, this.wKey[74] ^ this.X2, this.wKey[75] ^ this.X3);
        LT();
        sb3(this.wKey[76] ^ this.X0, this.wKey[77] ^ this.X1, this.wKey[78] ^ this.X2, this.wKey[79] ^ this.X3);
        LT();
        sb4(this.wKey[80] ^ this.X0, this.wKey[81] ^ this.X1, this.wKey[82] ^ this.X2, this.wKey[83] ^ this.X3);
        LT();
        sb5(this.wKey[84] ^ this.X0, this.wKey[85] ^ this.X1, this.wKey[86] ^ this.X2, this.wKey[87] ^ this.X3);
        LT();
        sb6(this.wKey[88] ^ this.X0, this.wKey[89] ^ this.X1, this.wKey[90] ^ this.X2, this.wKey[91] ^ this.X3);
        LT();
        sb7(this.wKey[92] ^ this.X0, this.wKey[93] ^ this.X1, this.wKey[94] ^ this.X2, this.wKey[95] ^ this.X3);
        LT();
        sb0(this.wKey[96] ^ this.X0, this.wKey[97] ^ this.X1, this.wKey[98] ^ this.X2, this.wKey[99] ^ this.X3);
        LT();
        sb1(this.wKey[100] ^ this.X0, this.wKey[101] ^ this.X1, this.wKey[102] ^ this.X2, this.wKey[103] ^ this.X3);
        LT();
        sb2(this.wKey[104] ^ this.X0, this.wKey[105] ^ this.X1, this.wKey[106] ^ this.X2, this.wKey[107] ^ this.X3);
        LT();
        sb3(this.wKey[108] ^ this.X0, this.wKey[109] ^ this.X1, this.wKey[110] ^ this.X2, this.wKey[111] ^ this.X3);
        LT();
        sb4(this.wKey[112] ^ this.X0, this.wKey[113] ^ this.X1, this.wKey[114] ^ this.X2, this.wKey[115] ^ this.X3);
        LT();
        sb5(this.wKey[116] ^ this.X0, this.wKey[117] ^ this.X1, this.wKey[118] ^ this.X2, this.wKey[119] ^ this.X3);
        LT();
        sb6(this.wKey[120] ^ this.X0, this.wKey[121] ^ this.X1, this.wKey[122] ^ this.X2, this.wKey[123] ^ this.X3);
        LT();
        sb7(this.wKey[124] ^ this.X0, this.wKey[125] ^ this.X1, this.wKey[126] ^ this.X2, this.wKey[127] ^ this.X3);
        wordToBytes(this.wKey[131] ^ this.X3, bArr2, i2);
        wordToBytes(this.wKey[130] ^ this.X2, bArr2, i2 + 4);
        wordToBytes(this.wKey[129] ^ this.X1, bArr2, i2 + 8);
        wordToBytes(this.wKey[128] ^ this.X0, bArr2, i2 + 12);
    }

    private void ib0(int i, int i2, int i3, int i4) {
        int i5 = i ^ -1;
        int i6 = i ^ i2;
        int i7 = (i5 | i6) ^ i4;
        int i8 = i3 ^ i7;
        this.X2 = i6 ^ i8;
        int i9 = i5 ^ (i6 & i4);
        this.X1 = (this.X2 & i9) ^ i7;
        this.X3 = (i & i7) ^ (this.X1 | i8);
        this.X0 = (i9 ^ i8) ^ this.X3;
    }

    private void ib1(int i, int i2, int i3, int i4) {
        int i5 = i2 ^ i4;
        int i6 = (i2 & i5) ^ i;
        int i7 = i5 ^ i6;
        this.X3 = i3 ^ i7;
        int i8 = (i5 & i6) ^ i2;
        this.X1 = i6 ^ (this.X3 | i8);
        int i9 = this.X1 ^ -1;
        int i10 = i8 ^ this.X3;
        this.X0 = i9 ^ i10;
        this.X2 = (i10 | i9) ^ i7;
    }

    private void ib2(int i, int i2, int i3, int i4) {
        int i5 = i2 ^ i4;
        int i6 = i ^ i3;
        int i7 = i3 ^ i5;
        this.X0 = (i2 & i7) ^ i6;
        this.X3 = i5 ^ ((((i5 ^ -1) | i) ^ i4) | i6);
        int i8 = i7 ^ -1;
        int i9 = this.X0 | this.X3;
        this.X1 = i8 ^ i9;
        this.X2 = (i8 & i4) ^ (i9 ^ i6);
    }

    private void ib3(int i, int i2, int i3, int i4) {
        int i5 = i2 ^ i3;
        int i6 = (i2 & i5) ^ i;
        int i7 = i4 | i6;
        this.X0 = i5 ^ i7;
        int i8 = (i5 | i7) ^ i4;
        this.X2 = (i3 ^ i6) ^ i8;
        int i9 = (i | i2) ^ i8;
        this.X3 = (this.X0 & i9) ^ i6;
        this.X1 = (i9 ^ this.X0) ^ this.X3;
    }

    private void ib4(int i, int i2, int i3, int i4) {
        int i5 = ((i3 | i4) & i) ^ i2;
        int i6 = (i & i5) ^ i3;
        this.X1 = i4 ^ i6;
        int i7 = i ^ -1;
        this.X3 = (i6 & this.X1) ^ i5;
        int i8 = (this.X1 | i7) ^ i4;
        this.X0 = this.X3 ^ i8;
        this.X2 = (i5 & i8) ^ (this.X1 ^ i7);
    }

    private void ib5(int i, int i2, int i3, int i4) {
        int i5 = i3 ^ -1;
        int i6 = (i2 & i5) ^ i4;
        int i7 = i & i6;
        this.X3 = (i2 ^ i5) ^ i7;
        int i8 = this.X3 | i2;
        this.X1 = i6 ^ (i & i8);
        int i9 = i | i4;
        this.X0 = (i5 ^ i8) ^ i9;
        this.X2 = (i2 & i9) ^ ((i ^ i3) | i7);
    }

    private void ib6(int i, int i2, int i3, int i4) {
        int i5 = i ^ -1;
        int i6 = i ^ i2;
        int i7 = i3 ^ i6;
        int i8 = (i3 | i5) ^ i4;
        this.X1 = i7 ^ i8;
        int i9 = i6 ^ (i7 & i8);
        this.X3 = i8 ^ (i2 | i9);
        int i10 = this.X3 | i2;
        this.X0 = i9 ^ i10;
        this.X2 = (i5 & i4) ^ (i7 ^ i10);
    }

    private void ib7(int i, int i2, int i3, int i4) {
        int i5 = (i & i2) | i3;
        int i6 = (i | i2) & i4;
        this.X3 = i5 ^ i6;
        int i7 = i6 ^ i2;
        this.X1 = (((i4 ^ -1) ^ this.X3) | i7) ^ i;
        this.X0 = (i7 ^ i3) ^ (this.X1 | i4);
        this.X2 = (i5 ^ this.X1) ^ (this.X0 ^ (this.X3 & i));
    }

    private void inverseLT() {
        int rotateRight = (rotateRight(this.X2, 22) ^ this.X3) ^ (this.X1 << 7);
        int rotateRight2 = (rotateRight(this.X0, 5) ^ this.X1) ^ this.X3;
        int rotateRight3 = rotateRight(this.X3, 7);
        int rotateRight4 = rotateRight(this.X1, 1);
        this.X3 = (rotateRight3 ^ rotateRight) ^ (rotateRight2 << 3);
        this.X1 = (rotateRight4 ^ rotateRight2) ^ rotateRight;
        this.X2 = rotateRight(rotateRight, 3);
        this.X0 = rotateRight(rotateRight2, 13);
    }

    private int[] makeWorkingKey(byte[] bArr) throws IllegalArgumentException {
        int[] iArr = new int[16];
        int length = bArr.length - 4;
        int i = 0;
        while (length > 0) {
            iArr[i] = bytesToWord(bArr, length);
            length -= 4;
            i++;
        }
        if (length == 0) {
            int i2 = i + 1;
            iArr[i] = bytesToWord(bArr, 0);
            if (i2 < 8) {
                iArr[i2] = 1;
            }
            int[] iArr2 = new int[132];
            for (int i3 = 8; i3 < 16; i3++) {
                iArr[i3] = rotateLeft(((((iArr[i3 - 8] ^ iArr[i3 - 5]) ^ iArr[i3 - 3]) ^ iArr[i3 - 1]) ^ PHI) ^ (i3 - 8), 11);
            }
            System.arraycopy(iArr, 8, iArr2, 0, 8);
            for (int i4 = 8; i4 < 132; i4++) {
                iArr2[i4] = rotateLeft(((((iArr2[i4 - 8] ^ iArr2[i4 - 5]) ^ iArr2[i4 - 3]) ^ iArr2[i4 - 1]) ^ PHI) ^ i4, 11);
            }
            sb3(iArr2[0], iArr2[1], iArr2[2], iArr2[3]);
            iArr2[0] = this.X0;
            iArr2[1] = this.X1;
            iArr2[2] = this.X2;
            iArr2[3] = this.X3;
            sb2(iArr2[4], iArr2[5], iArr2[6], iArr2[7]);
            iArr2[4] = this.X0;
            iArr2[5] = this.X1;
            iArr2[6] = this.X2;
            iArr2[7] = this.X3;
            sb1(iArr2[8], iArr2[9], iArr2[10], iArr2[11]);
            iArr2[8] = this.X0;
            iArr2[9] = this.X1;
            iArr2[10] = this.X2;
            iArr2[11] = this.X3;
            sb0(iArr2[12], iArr2[13], iArr2[14], iArr2[15]);
            iArr2[12] = this.X0;
            iArr2[13] = this.X1;
            iArr2[14] = this.X2;
            iArr2[15] = this.X3;
            sb7(iArr2[16], iArr2[17], iArr2[18], iArr2[19]);
            iArr2[16] = this.X0;
            iArr2[17] = this.X1;
            iArr2[18] = this.X2;
            iArr2[19] = this.X3;
            sb6(iArr2[20], iArr2[21], iArr2[22], iArr2[23]);
            iArr2[20] = this.X0;
            iArr2[21] = this.X1;
            iArr2[22] = this.X2;
            iArr2[23] = this.X3;
            sb5(iArr2[24], iArr2[25], iArr2[26], iArr2[27]);
            iArr2[24] = this.X0;
            iArr2[25] = this.X1;
            iArr2[26] = this.X2;
            iArr2[27] = this.X3;
            sb4(iArr2[28], iArr2[29], iArr2[30], iArr2[31]);
            iArr2[28] = this.X0;
            iArr2[29] = this.X1;
            iArr2[30] = this.X2;
            iArr2[31] = this.X3;
            sb3(iArr2[32], iArr2[33], iArr2[34], iArr2[35]);
            iArr2[32] = this.X0;
            iArr2[33] = this.X1;
            iArr2[34] = this.X2;
            iArr2[35] = this.X3;
            sb2(iArr2[36], iArr2[37], iArr2[38], iArr2[39]);
            iArr2[36] = this.X0;
            iArr2[37] = this.X1;
            iArr2[38] = this.X2;
            iArr2[39] = this.X3;
            sb1(iArr2[40], iArr2[41], iArr2[42], iArr2[43]);
            iArr2[40] = this.X0;
            iArr2[41] = this.X1;
            iArr2[42] = this.X2;
            iArr2[43] = this.X3;
            sb0(iArr2[44], iArr2[45], iArr2[46], iArr2[47]);
            iArr2[44] = this.X0;
            iArr2[45] = this.X1;
            iArr2[46] = this.X2;
            iArr2[47] = this.X3;
            sb7(iArr2[48], iArr2[49], iArr2[50], iArr2[51]);
            iArr2[48] = this.X0;
            iArr2[49] = this.X1;
            iArr2[50] = this.X2;
            iArr2[51] = this.X3;
            sb6(iArr2[52], iArr2[53], iArr2[54], iArr2[55]);
            iArr2[52] = this.X0;
            iArr2[53] = this.X1;
            iArr2[54] = this.X2;
            iArr2[55] = this.X3;
            sb5(iArr2[56], iArr2[57], iArr2[58], iArr2[59]);
            iArr2[56] = this.X0;
            iArr2[57] = this.X1;
            iArr2[58] = this.X2;
            iArr2[59] = this.X3;
            sb4(iArr2[60], iArr2[61], iArr2[62], iArr2[63]);
            iArr2[60] = this.X0;
            iArr2[61] = this.X1;
            iArr2[62] = this.X2;
            iArr2[63] = this.X3;
            sb3(iArr2[64], iArr2[65], iArr2[66], iArr2[67]);
            iArr2[64] = this.X0;
            iArr2[65] = this.X1;
            iArr2[66] = this.X2;
            iArr2[67] = this.X3;
            sb2(iArr2[68], iArr2[69], iArr2[70], iArr2[71]);
            iArr2[68] = this.X0;
            iArr2[69] = this.X1;
            iArr2[70] = this.X2;
            iArr2[71] = this.X3;
            sb1(iArr2[72], iArr2[73], iArr2[74], iArr2[75]);
            iArr2[72] = this.X0;
            iArr2[73] = this.X1;
            iArr2[74] = this.X2;
            iArr2[75] = this.X3;
            sb0(iArr2[76], iArr2[77], iArr2[78], iArr2[79]);
            iArr2[76] = this.X0;
            iArr2[77] = this.X1;
            iArr2[78] = this.X2;
            iArr2[79] = this.X3;
            sb7(iArr2[80], iArr2[81], iArr2[82], iArr2[83]);
            iArr2[80] = this.X0;
            iArr2[81] = this.X1;
            iArr2[82] = this.X2;
            iArr2[83] = this.X3;
            sb6(iArr2[84], iArr2[85], iArr2[86], iArr2[87]);
            iArr2[84] = this.X0;
            iArr2[85] = this.X1;
            iArr2[86] = this.X2;
            iArr2[87] = this.X3;
            sb5(iArr2[88], iArr2[89], iArr2[90], iArr2[91]);
            iArr2[88] = this.X0;
            iArr2[89] = this.X1;
            iArr2[90] = this.X2;
            iArr2[91] = this.X3;
            sb4(iArr2[92], iArr2[93], iArr2[94], iArr2[95]);
            iArr2[92] = this.X0;
            iArr2[93] = this.X1;
            iArr2[94] = this.X2;
            iArr2[95] = this.X3;
            sb3(iArr2[96], iArr2[97], iArr2[98], iArr2[99]);
            iArr2[96] = this.X0;
            iArr2[97] = this.X1;
            iArr2[98] = this.X2;
            iArr2[99] = this.X3;
            sb2(iArr2[100], iArr2[101], iArr2[102], iArr2[103]);
            iArr2[100] = this.X0;
            iArr2[101] = this.X1;
            iArr2[102] = this.X2;
            iArr2[103] = this.X3;
            sb1(iArr2[104], iArr2[105], iArr2[106], iArr2[107]);
            iArr2[104] = this.X0;
            iArr2[105] = this.X1;
            iArr2[106] = this.X2;
            iArr2[107] = this.X3;
            sb0(iArr2[108], iArr2[109], iArr2[110], iArr2[111]);
            iArr2[108] = this.X0;
            iArr2[109] = this.X1;
            iArr2[110] = this.X2;
            iArr2[111] = this.X3;
            sb7(iArr2[112], iArr2[113], iArr2[114], iArr2[115]);
            iArr2[112] = this.X0;
            iArr2[113] = this.X1;
            iArr2[114] = this.X2;
            iArr2[115] = this.X3;
            sb6(iArr2[116], iArr2[117], iArr2[118], iArr2[119]);
            iArr2[116] = this.X0;
            iArr2[117] = this.X1;
            iArr2[118] = this.X2;
            iArr2[119] = this.X3;
            sb5(iArr2[120], iArr2[121], iArr2[122], iArr2[123]);
            iArr2[120] = this.X0;
            iArr2[121] = this.X1;
            iArr2[122] = this.X2;
            iArr2[123] = this.X3;
            sb4(iArr2[124], iArr2[125], iArr2[126], iArr2[127]);
            iArr2[124] = this.X0;
            iArr2[125] = this.X1;
            iArr2[126] = this.X2;
            iArr2[127] = this.X3;
            sb3(iArr2[128], iArr2[129], iArr2[130], iArr2[131]);
            iArr2[128] = this.X0;
            iArr2[129] = this.X1;
            iArr2[130] = this.X2;
            iArr2[131] = this.X3;
            return iArr2;
        }
        throw new IllegalArgumentException("key must be a multiple of 4 bytes");
    }

    private int rotateLeft(int i, int i2) {
        return (i << i2) | (i >>> (-i2));
    }

    private int rotateRight(int i, int i2) {
        return (i >>> i2) | (i << (-i2));
    }

    private void sb0(int i, int i2, int i3, int i4) {
        int i5 = i ^ i4;
        int i6 = i3 ^ i5;
        int i7 = i2 ^ i6;
        this.X3 = (i & i4) ^ i7;
        int i8 = (i5 & i2) ^ i;
        this.X2 = i7 ^ (i3 | i8);
        int i9 = this.X3 & (i6 ^ i8);
        this.X1 = (i6 ^ -1) ^ i9;
        this.X0 = (i8 ^ -1) ^ i9;
    }

    private void sb1(int i, int i2, int i3, int i4) {
        int i5 = (i ^ -1) ^ i2;
        int i6 = (i | i5) ^ i3;
        this.X2 = i4 ^ i6;
        int i7 = (i4 | i5) ^ i2;
        int i8 = i5 ^ this.X2;
        this.X3 = (i6 & i7) ^ i8;
        int i9 = i7 ^ i6;
        this.X1 = this.X3 ^ i9;
        this.X0 = (i8 & i9) ^ i6;
    }

    private void sb2(int i, int i2, int i3, int i4) {
        int i5 = i ^ -1;
        int i6 = i2 ^ i4;
        this.X0 = (i3 & i5) ^ i6;
        int i7 = i3 ^ i5;
        int i8 = (this.X0 ^ i3) & i2;
        this.X3 = i7 ^ i8;
        this.X2 = ((i7 | this.X0) & (i8 | i4)) ^ i;
        this.X1 = ((i5 | i4) ^ this.X2) ^ (i6 ^ this.X3);
    }

    private void sb3(int i, int i2, int i3, int i4) {
        int i5 = i ^ i2;
        int i6 = i | i4;
        int i7 = i3 ^ i4;
        int i8 = (i & i3) | (i5 & i6);
        this.X2 = i7 ^ i8;
        int i9 = i8 ^ (i6 ^ i2);
        this.X0 = i5 ^ (i7 & i9);
        int i10 = this.X2 & this.X0;
        this.X1 = i9 ^ i10;
        this.X3 = (i10 ^ i7) ^ (i2 | i4);
    }

    private void sb4(int i, int i2, int i3, int i4) {
        int i5 = i ^ i4;
        int i6 = (i4 & i5) ^ i3;
        int i7 = i2 | i6;
        this.X3 = i5 ^ i7;
        int i8 = i2 ^ -1;
        this.X0 = (i5 | i8) ^ i6;
        int i9 = i5 ^ i8;
        this.X2 = (i7 & i9) ^ (this.X0 & i);
        this.X1 = (i9 & this.X2) ^ (i6 ^ i);
    }

    private void sb5(int i, int i2, int i3, int i4) {
        int i5 = i ^ -1;
        int i6 = i ^ i2;
        int i7 = i ^ i4;
        this.X0 = (i3 ^ i5) ^ (i6 | i7);
        int i8 = this.X0 & i4;
        this.X1 = (this.X0 ^ i6) ^ i8;
        int i9 = (i5 | this.X0) ^ i7;
        this.X2 = (i6 | i8) ^ i9;
        this.X3 = (i9 & this.X1) ^ (i2 ^ i8);
    }

    private void sb6(int i, int i2, int i3, int i4) {
        int i5 = i ^ i4;
        int i6 = i2 ^ i5;
        int i7 = ((i ^ -1) | i5) ^ i3;
        this.X1 = i2 ^ i7;
        int i8 = (i5 | this.X1) ^ i4;
        this.X2 = (i7 & i8) ^ i6;
        int i9 = i8 ^ i7;
        this.X0 = this.X2 ^ i9;
        this.X3 = (i7 ^ -1) ^ (i9 & i6);
    }

    private void sb7(int i, int i2, int i3, int i4) {
        int i5 = i2 ^ i3;
        int i6 = (i3 & i5) ^ i4;
        int i7 = i ^ i6;
        this.X1 = ((i4 | i5) & i7) ^ i2;
        this.X3 = i5 ^ (i & i7);
        int i8 = i7 ^ (this.X1 | i6);
        this.X2 = i6 ^ (this.X3 & i8);
        this.X0 = (i8 ^ -1) ^ (this.X3 & this.X2);
    }

    private void wordToBytes(int i, byte[] bArr, int i2) {
        bArr[i2 + 3] = (byte) i;
        bArr[i2 + 2] = (byte) (i >>> 8);
        bArr[i2 + 1] = (byte) (i >>> 16);
        bArr[i2] = (byte) (i >>> 24);
    }

    public String getAlgorithmName() {
        return "Serpent";
    }

    public int getBlockSize() {
        return 16;
    }

    public void init(boolean z, CipherParameters cipherParameters) {
        if (cipherParameters instanceof KeyParameter) {
            this.encrypting = z;
            this.wKey = makeWorkingKey(((KeyParameter) cipherParameters).getKey());
            return;
        }
        throw new IllegalArgumentException("invalid parameter passed to Serpent init - " + cipherParameters.getClass().getName());
    }

    public final int processBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        if (this.wKey == null) {
            throw new IllegalStateException("Serpent not initialised");
        } else if (i + 16 > bArr.length) {
            throw new DataLengthException("input buffer too short");
        } else if (i2 + 16 > bArr2.length) {
            throw new DataLengthException("output buffer too short");
        } else if (this.encrypting) {
            encryptBlock(bArr, i, bArr2, i2);
            return 16;
        } else {
            decryptBlock(bArr, i, bArr2, i2);
            return 16;
        }
    }

    public void reset() {
    }
}
