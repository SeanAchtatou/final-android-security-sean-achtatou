package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;

public class DESedeEngine extends DESEngine {
    protected static final int BLOCK_SIZE = 8;
    private boolean forEncryption;
    private int[] workingKey1 = null;
    private int[] workingKey2 = null;
    private int[] workingKey3 = null;

    public String getAlgorithmName() {
        return "DESede";
    }

    public int getBlockSize() {
        return 8;
    }

    public void init(boolean z, CipherParameters cipherParameters) {
        if (!(cipherParameters instanceof KeyParameter)) {
            throw new IllegalArgumentException("invalid parameter passed to DESede init - " + cipherParameters.getClass().getName());
        }
        byte[] key = ((KeyParameter) cipherParameters).getKey();
        byte[] bArr = new byte[8];
        byte[] bArr2 = new byte[8];
        byte[] bArr3 = new byte[8];
        if (key.length > 24) {
            throw new IllegalArgumentException("key size greater than 24 bytes");
        }
        this.forEncryption = z;
        if (key.length == 24) {
            System.arraycopy(key, 0, bArr, 0, bArr.length);
            System.arraycopy(key, 8, bArr2, 0, bArr2.length);
            System.arraycopy(key, 16, bArr3, 0, bArr3.length);
            this.workingKey1 = generateWorkingKey(z, bArr);
            this.workingKey2 = generateWorkingKey(!z, bArr2);
            this.workingKey3 = generateWorkingKey(z, bArr3);
            return;
        }
        System.arraycopy(key, 0, bArr, 0, bArr.length);
        System.arraycopy(key, 8, bArr2, 0, bArr2.length);
        this.workingKey1 = generateWorkingKey(z, bArr);
        this.workingKey2 = generateWorkingKey(!z, bArr2);
        this.workingKey3 = this.workingKey1;
    }

    public int processBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        if (this.workingKey1 == null) {
            throw new IllegalStateException("DESede engine not initialised");
        } else if (i + 8 > bArr.length) {
            throw new DataLengthException("input buffer too short");
        } else if (i2 + 8 > bArr2.length) {
            throw new DataLengthException("output buffer too short");
        } else if (this.forEncryption) {
            desFunc(this.workingKey1, bArr, i, bArr2, i2);
            desFunc(this.workingKey2, bArr2, i2, bArr2, i2);
            desFunc(this.workingKey3, bArr2, i2, bArr2, i2);
            return 8;
        } else {
            desFunc(this.workingKey3, bArr, i, bArr2, i2);
            desFunc(this.workingKey2, bArr2, i2, bArr2, i2);
            desFunc(this.workingKey1, bArr2, i2, bArr2, i2);
            return 8;
        }
    }

    public void reset() {
    }
}
