package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.RC2Parameters;
import org.bouncycastle.crypto.signers.PSSSigner;

public class RC2Engine implements BlockCipher {
    private static final int BLOCK_SIZE = 8;
    private static byte[] piTable = {-39, 120, -7, -60, 25, -35, -75, -19, 40, -23, -3, 121, 74, -96, -40, -99, -58, 126, 55, -125, 43, 118, 83, -114, 98, 76, 100, -120, 68, -117, -5, -94, 23, -102, 89, -11, -121, -77, 79, 19, 97, 69, 109, -115, 9, -127, 125, 50, -67, -113, 64, -21, -122, -73, 123, 11, -16, -107, 33, 34, 92, 107, 78, -126, 84, -42, 101, -109, -50, 96, -78, 28, 115, 86, -64, 20, -89, -116, -15, -36, 18, 117, -54, 31, 59, -66, -28, -47, 66, 61, -44, 48, -93, 60, -74, 38, 111, -65, 14, -38, 70, 105, 7, 87, 39, -14, 29, -101, PSSSigner.TRAILER_IMPLICIT, -108, 67, 3, -8, 17, -57, -10, -112, -17, 62, -25, 6, -61, -43, 47, -56, 102, 30, -41, 8, -24, -22, -34, Byte.MIN_VALUE, 82, -18, -9, -124, -86, 114, -84, 53, 77, 106, 42, -106, 26, -46, 113, 90, 21, 73, 116, 75, -97, -48, 94, 4, 24, -92, -20, -62, -32, 65, 110, 15, 81, -53, -52, 36, -111, -81, 80, -95, -12, 112, 57, -103, 124, 58, -123, 35, -72, -76, 122, -4, 2, 54, 91, 37, 85, -105, 49, 45, 93, -6, -104, -29, -118, -110, -82, 5, -33, 41, Tnaf.POW_2_WIDTH, 103, 108, -70, -55, -45, 0, -26, -49, -31, -98, -88, 44, 99, 22, 1, 63, 88, -30, -119, -87, 13, 56, 52, 27, -85, 51, -1, -80, -69, 72, 12, 95, -71, -79, -51, 46, -59, -13, -37, 71, -27, -91, -100, 119, 10, -90, 32, 104, -2, Byte.MAX_VALUE, -63, -83};
    private boolean encrypting;
    private int[] workingKey;

    private void decryptBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        int i3 = ((bArr[i + 7] & 255) << 8) + (bArr[i + 6] & 255);
        int i4 = ((bArr[i + 5] & 255) << 8) + (bArr[i + 4] & 255);
        int i5 = ((bArr[i + 3] & 255) << 8) + (bArr[i + 2] & 255);
        int i6 = i3;
        int i7 = i4;
        int i8 = ((bArr[i + 1] & 255) << 8) + (bArr[i + 0] & 255);
        int i9 = i7;
        for (int i10 = 60; i10 >= 44; i10 -= 4) {
            i6 = rotateWordLeft(i6, 11) - ((((i9 ^ -1) & i8) + (i5 & i9)) + this.workingKey[i10 + 3]);
            i9 = rotateWordLeft(i9, 13) - ((((i5 ^ -1) & i6) + (i8 & i5)) + this.workingKey[i10 + 2]);
            i5 = rotateWordLeft(i5, 14) - ((((i8 ^ -1) & i9) + (i6 & i8)) + this.workingKey[i10 + 1]);
            i8 = rotateWordLeft(i8, 15) - ((((i6 ^ -1) & i5) + (i9 & i6)) + this.workingKey[i10]);
        }
        int i11 = i6 - this.workingKey[i9 & 63];
        int i12 = i9 - this.workingKey[i5 & 63];
        int i13 = i5 - this.workingKey[i8 & 63];
        int i14 = i8 - this.workingKey[i11 & 63];
        int i15 = i11;
        for (int i16 = 40; i16 >= 20; i16 -= 4) {
            i15 = rotateWordLeft(i15, 11) - ((((i12 ^ -1) & i14) + (i13 & i12)) + this.workingKey[i16 + 3]);
            i12 = rotateWordLeft(i12, 13) - ((((i13 ^ -1) & i15) + (i14 & i13)) + this.workingKey[i16 + 2]);
            i13 = rotateWordLeft(i13, 14) - ((((i14 ^ -1) & i12) + (i15 & i14)) + this.workingKey[i16 + 1]);
            i14 = rotateWordLeft(i14, 15) - ((((i15 ^ -1) & i13) + (i12 & i15)) + this.workingKey[i16]);
        }
        int i17 = i15 - this.workingKey[i12 & 63];
        int i18 = i12 - this.workingKey[i13 & 63];
        int i19 = i13 - this.workingKey[i14 & 63];
        int i20 = i14 - this.workingKey[i17 & 63];
        int i21 = i17;
        for (int i22 = 16; i22 >= 0; i22 -= 4) {
            i21 = rotateWordLeft(i21, 11) - ((((i18 ^ -1) & i20) + (i19 & i18)) + this.workingKey[i22 + 3]);
            i18 = rotateWordLeft(i18, 13) - ((((i19 ^ -1) & i21) + (i20 & i19)) + this.workingKey[i22 + 2]);
            i19 = rotateWordLeft(i19, 14) - ((((i20 ^ -1) & i18) + (i21 & i20)) + this.workingKey[i22 + 1]);
            i20 = rotateWordLeft(i20, 15) - ((((i21 ^ -1) & i19) + (i18 & i21)) + this.workingKey[i22]);
        }
        bArr2[i2 + 0] = (byte) i20;
        bArr2[i2 + 1] = (byte) (i20 >> 8);
        bArr2[i2 + 2] = (byte) i19;
        bArr2[i2 + 3] = (byte) (i19 >> 8);
        bArr2[i2 + 4] = (byte) i18;
        bArr2[i2 + 5] = (byte) (i18 >> 8);
        bArr2[i2 + 6] = (byte) i21;
        bArr2[i2 + 7] = (byte) (i21 >> 8);
    }

    private void encryptBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        int i3 = ((bArr[i + 7] & 255) << 8) + (bArr[i + 6] & 255);
        int i4 = ((bArr[i + 5] & 255) << 8) + (bArr[i + 4] & 255);
        int i5 = ((bArr[i + 3] & 255) << 8) + (bArr[i + 2] & 255);
        int i6 = i3;
        int i7 = i4;
        int i8 = ((bArr[i + 1] & 255) << 8) + (bArr[i + 0] & 255);
        int i9 = i7;
        for (int i10 = 0; i10 <= 16; i10 += 4) {
            i8 = rotateWordLeft(i8 + ((i6 ^ -1) & i5) + (i9 & i6) + this.workingKey[i10], 1);
            i5 = rotateWordLeft(i5 + ((i8 ^ -1) & i9) + (i6 & i8) + this.workingKey[i10 + 1], 2);
            i9 = rotateWordLeft(i9 + ((i5 ^ -1) & i6) + (i8 & i5) + this.workingKey[i10 + 2], 3);
            i6 = rotateWordLeft(i6 + ((i9 ^ -1) & i8) + (i5 & i9) + this.workingKey[i10 + 3], 5);
        }
        int i11 = this.workingKey[i6 & 63] + i8;
        int i12 = this.workingKey[i11 & 63] + i5;
        int i13 = this.workingKey[i12 & 63] + i9;
        int i14 = this.workingKey[i13 & 63] + i6;
        int i15 = i13;
        int i16 = i12;
        int i17 = i11;
        for (int i18 = 20; i18 <= 40; i18 += 4) {
            i17 = rotateWordLeft(i17 + ((i14 ^ -1) & i16) + (i15 & i14) + this.workingKey[i18], 1);
            i16 = rotateWordLeft(i16 + ((i17 ^ -1) & i15) + (i14 & i17) + this.workingKey[i18 + 1], 2);
            i15 = rotateWordLeft(i15 + ((i16 ^ -1) & i14) + (i17 & i16) + this.workingKey[i18 + 2], 3);
            i14 = rotateWordLeft(i14 + ((i15 ^ -1) & i17) + (i16 & i15) + this.workingKey[i18 + 3], 5);
        }
        int i19 = this.workingKey[i14 & 63] + i17;
        int i20 = this.workingKey[i19 & 63] + i16;
        int i21 = this.workingKey[i20 & 63] + i15;
        int i22 = this.workingKey[i21 & 63] + i14;
        int i23 = i21;
        int i24 = i20;
        int i25 = i19;
        for (int i26 = 44; i26 < 64; i26 += 4) {
            i25 = rotateWordLeft(i25 + ((i22 ^ -1) & i24) + (i23 & i22) + this.workingKey[i26], 1);
            i24 = rotateWordLeft(i24 + ((i25 ^ -1) & i23) + (i22 & i25) + this.workingKey[i26 + 1], 2);
            i23 = rotateWordLeft(i23 + ((i24 ^ -1) & i22) + (i25 & i24) + this.workingKey[i26 + 2], 3);
            i22 = rotateWordLeft(i22 + ((i23 ^ -1) & i25) + (i24 & i23) + this.workingKey[i26 + 3], 5);
        }
        bArr2[i2 + 0] = (byte) i25;
        bArr2[i2 + 1] = (byte) (i25 >> 8);
        bArr2[i2 + 2] = (byte) i24;
        bArr2[i2 + 3] = (byte) (i24 >> 8);
        bArr2[i2 + 4] = (byte) i23;
        bArr2[i2 + 5] = (byte) (i23 >> 8);
        bArr2[i2 + 6] = (byte) i22;
        bArr2[i2 + 7] = (byte) (i22 >> 8);
    }

    private int[] generateWorkingKey(byte[] bArr, int i) {
        int[] iArr = new int[128];
        for (int i2 = 0; i2 != bArr.length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        int length = bArr.length;
        if (length < 128) {
            int i3 = iArr[length - 1];
            int i4 = length;
            int i5 = 0;
            while (true) {
                int i6 = i5 + 1;
                int i7 = piTable[(iArr[i5] + i3) & 255] & 255;
                int i8 = i4 + 1;
                iArr[i4] = i7;
                if (i8 >= 128) {
                    break;
                }
                i4 = i8;
                i3 = i7;
                i5 = i6;
            }
        }
        int i9 = (i + 7) >> 3;
        int i10 = piTable[iArr[128 - i9] & (255 >> ((-i) & 7))] & 255;
        iArr[128 - i9] = i10;
        int i11 = i10;
        for (int i12 = (128 - i9) - 1; i12 >= 0; i12--) {
            i11 = piTable[i11 ^ iArr[i12 + i9]] & 255;
            iArr[i12] = i11;
        }
        int[] iArr2 = new int[64];
        for (int i13 = 0; i13 != iArr2.length; i13++) {
            iArr2[i13] = iArr[i13 * 2] + (iArr[(i13 * 2) + 1] << 8);
        }
        return iArr2;
    }

    private int rotateWordLeft(int i, int i2) {
        int i3 = 65535 & i;
        return (i3 >> (16 - i2)) | (i3 << i2);
    }

    public String getAlgorithmName() {
        return "RC2";
    }

    public int getBlockSize() {
        return 8;
    }

    public void init(boolean z, CipherParameters cipherParameters) {
        this.encrypting = z;
        if (cipherParameters instanceof RC2Parameters) {
            RC2Parameters rC2Parameters = (RC2Parameters) cipherParameters;
            this.workingKey = generateWorkingKey(rC2Parameters.getKey(), rC2Parameters.getEffectiveKeyBits());
        } else if (cipherParameters instanceof KeyParameter) {
            byte[] key = ((KeyParameter) cipherParameters).getKey();
            this.workingKey = generateWorkingKey(key, key.length * 8);
        } else {
            throw new IllegalArgumentException("invalid parameter passed to RC2 init - " + cipherParameters.getClass().getName());
        }
    }

    public final int processBlock(byte[] bArr, int i, byte[] bArr2, int i2) {
        if (this.workingKey == null) {
            throw new IllegalStateException("RC2 engine not initialised");
        } else if (i + 8 > bArr.length) {
            throw new DataLengthException("input buffer too short");
        } else if (i2 + 8 > bArr2.length) {
            throw new DataLengthException("output buffer too short");
        } else if (this.encrypting) {
            encryptBlock(bArr, i, bArr2, i2);
            return 8;
        } else {
            decryptBlock(bArr, i, bArr2, i2);
            return 8;
        }
    }

    public void reset() {
    }
}
