package org.bouncycastle.x509;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.PKIXParameters;
import java.security.cert.PolicyNode;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DEREnumerated;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.GeneralSubtree;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.bouncycastle.asn1.x509.NameConstraints;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.qualified.MonetaryValue;
import org.bouncycastle.asn1.x509.qualified.QCStatement;
import org.bouncycastle.i18n.ErrorBundle;
import org.bouncycastle.i18n.LocaleString;
import org.bouncycastle.i18n.filter.TrustedInput;
import org.bouncycastle.i18n.filter.UntrustedInput;
import org.bouncycastle.i18n.filter.UntrustedUrlInput;
import org.bouncycastle.jce.provider.AnnotatedException;
import org.bouncycastle.jce.provider.CertPathValidatorUtilities;
import org.bouncycastle.jce.provider.PKIXNameConstraintValidator;
import org.bouncycastle.jce.provider.PKIXNameConstraintValidatorException;

public class PKIXCertPathReviewer extends CertPathValidatorUtilities {
    private static final String AUTH_INFO_ACCESS = X509Extensions.AuthorityInfoAccess.getId();
    private static final String CRL_DIST_POINTS = X509Extensions.CRLDistributionPoints.getId();
    private static final String QC_STATEMENT = X509Extensions.QCStatements.getId();
    private static final String RESOURCE_NAME = "org.bouncycastle.x509.CertPathReviewerMessages";
    protected CertPath certPath;
    protected List certs;
    protected List[] errors;
    private boolean initialized;
    protected int n;
    protected List[] notifications;
    protected PKIXParameters pkixParams;
    protected PolicyNode policyTree;
    protected PublicKey subjectPublicKey;
    protected TrustAnchor trustAnchor;
    protected Date validDate;

    public PKIXCertPathReviewer() {
    }

    public PKIXCertPathReviewer(CertPath certPath2, PKIXParameters pKIXParameters) throws CertPathReviewerException {
        init(certPath2, pKIXParameters);
    }

    private String IPtoString(byte[] bArr) {
        try {
            return InetAddress.getByAddress(bArr).getHostAddress();
        } catch (Exception e) {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i != bArr.length; i++) {
                stringBuffer.append(Integer.toHexString(bArr[i] & 255));
                stringBuffer.append(' ');
            }
            return stringBuffer.toString();
        }
    }

    private void checkCriticalExtensions() {
        int size;
        List<PKIXCertPathChecker> certPathCheckers = this.pkixParams.getCertPathCheckers();
        for (PKIXCertPathChecker init : certPathCheckers) {
            try {
                init.init(false);
            } catch (CertPathValidatorException e) {
                throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.criticalExtensionError", new Object[]{e.getMessage(), e, e.getClass().getName()}), e.getCause(), this.certPath, size);
            } catch (CertPathValidatorException e2) {
                throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.certPathCheckerError", new Object[]{e2.getMessage(), e2, e2.getClass().getName()}), e2);
            } catch (CertPathReviewerException e3) {
                addError(e3.getErrorMessage(), e3.getIndex());
                return;
            }
        }
        size = this.certs.size() - 1;
        while (size >= 0) {
            X509Certificate x509Certificate = (X509Certificate) this.certs.get(size);
            Set<String> criticalExtensionOIDs = x509Certificate.getCriticalExtensionOIDs();
            if (criticalExtensionOIDs != null && !criticalExtensionOIDs.isEmpty()) {
                criticalExtensionOIDs.remove(KEY_USAGE);
                criticalExtensionOIDs.remove(CERTIFICATE_POLICIES);
                criticalExtensionOIDs.remove(POLICY_MAPPINGS);
                criticalExtensionOIDs.remove(INHIBIT_ANY_POLICY);
                criticalExtensionOIDs.remove(ISSUING_DISTRIBUTION_POINT);
                criticalExtensionOIDs.remove(DELTA_CRL_INDICATOR);
                criticalExtensionOIDs.remove(POLICY_CONSTRAINTS);
                criticalExtensionOIDs.remove(BASIC_CONSTRAINTS);
                criticalExtensionOIDs.remove(SUBJECT_ALTERNATIVE_NAME);
                criticalExtensionOIDs.remove(NAME_CONSTRAINTS);
                if (criticalExtensionOIDs.contains(QC_STATEMENT) && processQcStatements(x509Certificate, size)) {
                    criticalExtensionOIDs.remove(QC_STATEMENT);
                }
                for (PKIXCertPathChecker check : certPathCheckers) {
                    check.check(x509Certificate, criticalExtensionOIDs);
                }
                if (!criticalExtensionOIDs.isEmpty()) {
                    for (String dERObjectIdentifier : criticalExtensionOIDs) {
                        addError(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.unknownCriticalExt", new Object[]{new DERObjectIdentifier(dERObjectIdentifier)}), size);
                    }
                }
            }
            size--;
        }
    }

    private void checkNameConstraints() {
        GeneralName instance;
        PKIXNameConstraintValidator pKIXNameConstraintValidator = new PKIXNameConstraintValidator();
        for (int size = this.certs.size() - 1; size > 0; size--) {
            int i = this.n - size;
            X509Certificate x509Certificate = (X509Certificate) this.certs.get(size);
            if (!isSelfIssued(x509Certificate)) {
                X500Principal subjectPrincipal = getSubjectPrincipal(x509Certificate);
                try {
                    ASN1Sequence aSN1Sequence = (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(subjectPrincipal.getEncoded())).readObject();
                    pKIXNameConstraintValidator.checkPermittedDN(aSN1Sequence);
                    pKIXNameConstraintValidator.checkExcludedDN(aSN1Sequence);
                    ASN1Sequence aSN1Sequence2 = (ASN1Sequence) getExtensionValue(x509Certificate, SUBJECT_ALTERNATIVE_NAME);
                    if (aSN1Sequence2 != null) {
                        for (int i2 = 0; i2 < aSN1Sequence2.size(); i2++) {
                            instance = GeneralName.getInstance(aSN1Sequence2.getObjectAt(i2));
                            pKIXNameConstraintValidator.checkPermitted(instance);
                            pKIXNameConstraintValidator.checkExcluded(instance);
                        }
                    }
                } catch (AnnotatedException e) {
                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.ncExtError"), e, this.certPath, size);
                } catch (PKIXNameConstraintValidatorException e2) {
                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.notPermittedEmail", new Object[]{new UntrustedInput(instance)}), e2, this.certPath, size);
                } catch (AnnotatedException e3) {
                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.subjAltNameExtError"), e3, this.certPath, size);
                } catch (PKIXNameConstraintValidatorException e4) {
                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.excludedDN", new Object[]{new UntrustedInput(subjectPrincipal.getName())}), e4, this.certPath, size);
                } catch (PKIXNameConstraintValidatorException e5) {
                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.notPermittedDN", new Object[]{new UntrustedInput(subjectPrincipal.getName())}), e5, this.certPath, size);
                } catch (IOException e6) {
                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.ncSubjectNameError", new Object[]{new UntrustedInput(subjectPrincipal)}), e6, this.certPath, size);
                } catch (CertPathReviewerException e7) {
                    addError(e7.getErrorMessage(), e7.getIndex());
                    return;
                }
            }
            ASN1Sequence aSN1Sequence3 = (ASN1Sequence) getExtensionValue(x509Certificate, NAME_CONSTRAINTS);
            if (aSN1Sequence3 != null) {
                NameConstraints nameConstraints = new NameConstraints(aSN1Sequence3);
                ASN1Sequence permittedSubtrees = nameConstraints.getPermittedSubtrees();
                if (permittedSubtrees != null) {
                    pKIXNameConstraintValidator.intersectPermittedSubtree(permittedSubtrees);
                }
                ASN1Sequence excludedSubtrees = nameConstraints.getExcludedSubtrees();
                if (excludedSubtrees != null) {
                    Enumeration objects = excludedSubtrees.getObjects();
                    while (objects.hasMoreElements()) {
                        pKIXNameConstraintValidator.addExcludedSubtree(GeneralSubtree.getInstance(objects.nextElement()));
                    }
                }
            }
        }
    }

    private void checkPathLength() {
        BasicConstraints basicConstraints;
        int i;
        BigInteger pathLenConstraint;
        int i2 = this.n;
        int size = this.certs.size() - 1;
        int i3 = 0;
        int i4 = i2;
        while (size > 0) {
            int i5 = this.n - size;
            X509Certificate x509Certificate = (X509Certificate) this.certs.get(size);
            if (!isSelfIssued(x509Certificate)) {
                if (i4 <= 0) {
                    addError(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.pathLenghtExtended"));
                }
                i4--;
                i3++;
            }
            try {
                basicConstraints = BasicConstraints.getInstance(getExtensionValue(x509Certificate, BASIC_CONSTRAINTS));
            } catch (AnnotatedException e) {
                addError(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.processLengthConstError"), size);
                basicConstraints = null;
            }
            if (basicConstraints == null || (pathLenConstraint = basicConstraints.getPathLenConstraint()) == null || (i = pathLenConstraint.intValue()) >= i4) {
                i = i4;
            }
            size--;
            i4 = i;
        }
        addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.totalPathLength", new Object[]{new Integer(i3)}));
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v95, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v37, resolved type: java.security.cert.X509Certificate} */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void checkPolicy() {
        /*
            r25 = this;
            r0 = r25
            java.security.cert.PKIXParameters r0 = r0.pkixParams
            r3 = r0
            java.util.Set r13 = r3.getInitialPolicies()
            r0 = r25
            int r0 = r0.n
            r3 = r0
            int r3 = r3 + 1
            java.util.ArrayList[] r14 = new java.util.ArrayList[r3]
            r3 = 0
        L_0x0013:
            int r4 = r14.length
            if (r3 >= r4) goto L_0x0020
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r14[r3] = r4
            int r3 = r3 + 1
            goto L_0x0013
        L_0x0020:
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            java.lang.String r3 = "2.5.29.32.0"
            r6.add(r3)
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = new org.bouncycastle.jce.provider.PKIXPolicyNode
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r5 = 0
            r7 = 0
            java.util.HashSet r8 = new java.util.HashSet
            r8.<init>()
            java.lang.String r9 = "2.5.29.32.0"
            r10 = 0
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)
            r4 = 0
            r4 = r14[r4]
            r4.add(r3)
            r0 = r25
            java.security.cert.PKIXParameters r0 = r0.pkixParams
            r4 = r0
            boolean r4 = r4.isExplicitPolicyRequired()
            if (r4 == 0) goto L_0x00f5
            r4 = 0
        L_0x0050:
            r0 = r25
            java.security.cert.PKIXParameters r0 = r0.pkixParams
            r5 = r0
            boolean r5 = r5.isAnyPolicyInhibited()
            if (r5 == 0) goto L_0x00fe
            r5 = 0
        L_0x005c:
            r0 = r25
            java.security.cert.PKIXParameters r0 = r0.pkixParams
            r6 = r0
            boolean r6 = r6.isPolicyMappingInhibited()
            if (r6 == 0) goto L_0x0107
            r6 = 0
        L_0x0068:
            r7 = 0
            r8 = 0
            r0 = r25
            java.util.List r0 = r0.certs     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r9 = r0
            int r9 = r9.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r10 = 1
            int r9 = r9 - r10
            r15 = r9
            r16 = r6
            r17 = r5
            r18 = r4
            r19 = r3
            r3 = r8
            r4 = r7
        L_0x0080:
            if (r15 < 0) goto L_0x047f
            r0 = r25
            int r0 = r0.n     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = r0
            int r5 = r3 - r15
            r0 = r25
            java.util.List r0 = r0.certs     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = r0
            java.lang.Object r3 = r3.get(r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r3
            java.security.cert.X509Certificate r0 = (java.security.cert.X509Certificate) r0     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r11 = r0
            java.lang.String r3 = org.bouncycastle.x509.PKIXCertPathReviewer.CERTIFICATE_POLICIES     // Catch:{ AnnotatedException -> 0x0110 }
            org.bouncycastle.asn1.DERObject r3 = getExtensionValue(r11, r3)     // Catch:{ AnnotatedException -> 0x0110 }
            r0 = r3
            org.bouncycastle.asn1.ASN1Sequence r0 = (org.bouncycastle.asn1.ASN1Sequence) r0     // Catch:{ AnnotatedException -> 0x0110 }
            r12 = r0
            if (r12 == 0) goto L_0x0286
            if (r19 == 0) goto L_0x0286
            java.util.Enumeration r3 = r12.getObjects()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.util.HashSet r6 = new java.util.HashSet     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6.<init>()     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x00ad:
            boolean r7 = r3.hasMoreElements()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r7 == 0) goto L_0x013a
            java.lang.Object r7 = r3.nextElement()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.asn1.x509.PolicyInformation r7 = org.bouncycastle.asn1.x509.PolicyInformation.getInstance(r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.asn1.DERObjectIdentifier r8 = r7.getPolicyIdentifier()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r9 = r8.getId()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6.add(r9)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r9 = "2.5.29.32.0"
            java.lang.String r10 = r8.getId()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r9 = r9.equals(r10)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r9 != 0) goto L_0x00ad
            org.bouncycastle.asn1.ASN1Sequence r7 = r7.getPolicyQualifiers()     // Catch:{ CertPathValidatorException -> 0x0125 }
            java.util.Set r7 = getQualifierSet(r7)     // Catch:{ CertPathValidatorException -> 0x0125 }
            boolean r9 = processCertD1i(r5, r14, r8, r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r9 != 0) goto L_0x00ad
            processCertD1ii(r5, r14, r8, r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            goto L_0x00ad
        L_0x00e4:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = r3.getErrorMessage()
            int r3 = r3.getIndex()
            r0 = r25
            r1 = r4
            r2 = r3
            r0.addError(r1, r2)
        L_0x00f4:
            return
        L_0x00f5:
            r0 = r25
            int r0 = r0.n
            r4 = r0
            int r4 = r4 + 1
            goto L_0x0050
        L_0x00fe:
            r0 = r25
            int r0 = r0.n
            r5 = r0
            int r5 = r5 + 1
            goto L_0x005c
        L_0x0107:
            r0 = r25
            int r0 = r0.n
            r6 = r0
            int r6 = r6 + 1
            goto L_0x0068
        L_0x0110:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r6 = "CertPathReviewer.policyExtError"
            r4.<init>(r5, r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r5 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = r0
            r5.<init>(r4, r3, r6, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r5     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0125:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r6 = "CertPathReviewer.policyQualifierError"
            r4.<init>(r5, r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r5 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = r0
            r5.<init>(r4, r3, r6, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r5     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x013a:
            if (r4 == 0) goto L_0x0144
            java.lang.String r3 = "2.5.29.32.0"
            boolean r3 = r4.contains(r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x01ce
        L_0x0144:
            r20 = r6
        L_0x0146:
            if (r17 > 0) goto L_0x0155
            r0 = r25
            int r0 = r0.n     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = r0
            if (r5 >= r3) goto L_0x0235
            boolean r3 = isSelfIssued(r11)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x0235
        L_0x0155:
            java.util.Enumeration r3 = r12.getObjects()     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0159:
            boolean r4 = r3.hasMoreElements()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r4 == 0) goto L_0x0235
            java.lang.Object r4 = r3.nextElement()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.asn1.x509.PolicyInformation r4 = org.bouncycastle.asn1.x509.PolicyInformation.getInstance(r4)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r6 = "2.5.29.32.0"
            org.bouncycastle.asn1.DERObjectIdentifier r7 = r4.getPolicyIdentifier()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r7 = r7.getId()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r6 = r6.equals(r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r6 == 0) goto L_0x0159
            org.bouncycastle.asn1.ASN1Sequence r3 = r4.getPolicyQualifiers()     // Catch:{ CertPathValidatorException -> 0x01ef }
            java.util.Set r8 = getQualifierSet(r3)     // Catch:{ CertPathValidatorException -> 0x01ef }
            r3 = 1
            int r3 = r5 - r3
            r21 = r14[r3]     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = 0
            r22 = r3
        L_0x0187:
            int r3 = r21.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r22
            r1 = r3
            if (r0 >= r1) goto L_0x0235
            java.lang.Object r7 = r21.get(r22)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r7 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r7     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.util.Set r3 = r7.getExpectedPolicies()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.util.Iterator r23 = r3.iterator()     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x019e:
            boolean r3 = r23.hasNext()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x022f
            java.lang.Object r3 = r23.next()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r4 = r3 instanceof java.lang.String     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r4 == 0) goto L_0x0204
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r9 = r3
        L_0x01af:
            r3 = 0
            java.util.Iterator r4 = r7.getChildren()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = r3
        L_0x01b5:
            boolean r3 = r4.hasNext()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x0210
            java.lang.Object r3 = r4.next()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r3 = r3.getValidPolicy()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r3 = r9.equals(r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x06a2
            r3 = 1
        L_0x01cc:
            r6 = r3
            goto L_0x01b5
        L_0x01ce:
            java.util.Iterator r3 = r4.iterator()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.util.HashSet r4 = new java.util.HashSet     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r4.<init>()     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x01d7:
            boolean r7 = r3.hasNext()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r7 == 0) goto L_0x01eb
            java.lang.Object r7 = r3.next()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r8 = r6.contains(r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r8 == 0) goto L_0x01d7
            r4.add(r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            goto L_0x01d7
        L_0x01eb:
            r20 = r4
            goto L_0x0146
        L_0x01ef:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r6 = "CertPathReviewer.policyQualifierError"
            r4.<init>(r5, r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r5 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = r0
            r5.<init>(r4, r3, r6, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r5     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0204:
            boolean r4 = r3 instanceof org.bouncycastle.asn1.DERObjectIdentifier     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r4 == 0) goto L_0x019e
            org.bouncycastle.asn1.DERObjectIdentifier r3 = (org.bouncycastle.asn1.DERObjectIdentifier) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r3 = r3.getId()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r9 = r3
            goto L_0x01af
        L_0x0210:
            if (r6 != 0) goto L_0x019e
            java.util.HashSet r6 = new java.util.HashSet     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6.<init>()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6.add(r9)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = new org.bouncycastle.jce.provider.PKIXPolicyNode     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r4.<init>()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r10 = 0
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r7.addChild(r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r4 = r14[r5]     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r4.add(r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            goto L_0x019e
        L_0x022f:
            int r3 = r22 + 1
            r22 = r3
            goto L_0x0187
        L_0x0235:
            r3 = 1
            int r3 = r5 - r3
            r4 = r3
            r6 = r19
        L_0x023b:
            if (r4 < 0) goto L_0x0263
            r7 = r14[r4]     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = 0
            r8 = r6
            r6 = r3
        L_0x0242:
            int r3 = r7.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r6 >= r3) goto L_0x069f
            java.lang.Object r3 = r7.get(r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r9 = r3.hasChildren()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r9 != 0) goto L_0x025e
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = removePolicyNode(r8, r14, r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 != 0) goto L_0x025f
        L_0x025a:
            int r4 = r4 + -1
            r6 = r3
            goto L_0x023b
        L_0x025e:
            r3 = r8
        L_0x025f:
            int r6 = r6 + 1
            r8 = r3
            goto L_0x0242
        L_0x0263:
            java.util.Set r3 = r11.getCriticalExtensionOIDs()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x069a
            java.lang.String r4 = org.bouncycastle.x509.PKIXCertPathReviewer.CERTIFICATE_POLICIES     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r4 = r3.contains(r4)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r7 = r14[r5]     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = 0
            r8 = r3
        L_0x0273:
            int r3 = r7.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r8 >= r3) goto L_0x069a
            java.lang.Object r3 = r7.get(r8)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3.setCritical(r4)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            int r3 = r8 + 1
            r8 = r3
            goto L_0x0273
        L_0x0286:
            r8 = r4
            r3 = r19
        L_0x0289:
            if (r12 != 0) goto L_0x0697
            r3 = 0
            r9 = r3
        L_0x028d:
            if (r18 > 0) goto L_0x02a0
            if (r9 != 0) goto L_0x02a0
            org.bouncycastle.i18n.ErrorBundle r3 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r4 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r5 = "CertPathReviewer.noValidPolicyTree"
            r3.<init>(r4, r5)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r4 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r4.<init>(r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x02a0:
            r0 = r25
            int r0 = r0.n     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = r0
            if (r5 == r3) goto L_0x068e
            java.lang.String r3 = org.bouncycastle.x509.PKIXCertPathReviewer.POLICY_MAPPINGS     // Catch:{ AnnotatedException -> 0x02ef }
            org.bouncycastle.asn1.DERObject r4 = getExtensionValue(r11, r3)     // Catch:{ AnnotatedException -> 0x02ef }
            if (r4 == 0) goto L_0x0328
            r0 = r4
            org.bouncycastle.asn1.ASN1Sequence r0 = (org.bouncycastle.asn1.ASN1Sequence) r0     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = r0
            r6 = 0
            r10 = r6
        L_0x02b5:
            int r6 = r3.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r10 >= r6) goto L_0x0328
            org.bouncycastle.asn1.DEREncodable r6 = r3.getObjectAt(r10)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.asn1.ASN1Sequence r6 = (org.bouncycastle.asn1.ASN1Sequence) r6     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r7 = 0
            org.bouncycastle.asn1.DEREncodable r7 = r6.getObjectAt(r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.asn1.DERObjectIdentifier r7 = (org.bouncycastle.asn1.DERObjectIdentifier) r7     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r12 = 1
            org.bouncycastle.asn1.DEREncodable r6 = r6.getObjectAt(r12)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.asn1.DERObjectIdentifier r6 = (org.bouncycastle.asn1.DERObjectIdentifier) r6     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r12 = "2.5.29.32.0"
            java.lang.String r7 = r7.getId()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r7 = r12.equals(r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r7 == 0) goto L_0x0304
            org.bouncycastle.i18n.ErrorBundle r3 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r4 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r5 = "CertPathReviewer.invalidPolicyMapping"
            r3.<init>(r4, r5)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r4 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r5 = r0
            r4.<init>(r3, r5, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x02ef:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r6 = "CertPathReviewer.policyMapExtError"
            r4.<init>(r5, r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r5 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = r0
            r5.<init>(r4, r3, r6, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r5     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0304:
            java.lang.String r7 = "2.5.29.32.0"
            java.lang.String r6 = r6.getId()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r6 = r7.equals(r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r6 == 0) goto L_0x0324
            org.bouncycastle.i18n.ErrorBundle r3 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r4 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r5 = "CertPathReviewer.invalidPolicyMapping"
            r3.<init>(r4, r5)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r4 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r5 = r0
            r4.<init>(r3, r5, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0324:
            int r6 = r10 + 1
            r10 = r6
            goto L_0x02b5
        L_0x0328:
            if (r4 == 0) goto L_0x068b
            org.bouncycastle.asn1.ASN1Sequence r4 = (org.bouncycastle.asn1.ASN1Sequence) r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.util.HashMap r7 = new java.util.HashMap     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r7.<init>()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.util.HashSet r10 = new java.util.HashSet     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r10.<init>()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = 0
            r12 = r3
        L_0x0338:
            int r3 = r4.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r12 >= r3) goto L_0x0386
            org.bouncycastle.asn1.DEREncodable r3 = r4.getObjectAt(r12)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.asn1.ASN1Sequence r3 = (org.bouncycastle.asn1.ASN1Sequence) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = 0
            org.bouncycastle.asn1.DEREncodable r6 = r3.getObjectAt(r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.asn1.DERObjectIdentifier r6 = (org.bouncycastle.asn1.DERObjectIdentifier) r6     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r6 = r6.getId()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r19 = 1
            r0 = r3
            r1 = r19
            org.bouncycastle.asn1.DEREncodable r3 = r0.getObjectAt(r1)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.asn1.DERObjectIdentifier r3 = (org.bouncycastle.asn1.DERObjectIdentifier) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r19 = r3.getId()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r3 = r7.containsKey(r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 != 0) goto L_0x0379
            java.util.HashSet r3 = new java.util.HashSet     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3.<init>()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r3
            r1 = r19
            r0.add(r1)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r7.put(r6, r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r10.add(r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0375:
            int r3 = r12 + 1
            r12 = r3
            goto L_0x0338
        L_0x0379:
            java.lang.Object r3 = r7.get(r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.util.Set r3 = (java.util.Set) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r3
            r1 = r19
            r0.add(r1)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            goto L_0x0375
        L_0x0386:
            java.util.Iterator r4 = r10.iterator()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = r9
        L_0x038b:
            boolean r3 = r4.hasNext()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x03d0
            java.lang.Object r3 = r4.next()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r16 <= 0) goto L_0x03c9
            prepareNextCertB1(r5, r14, r3, r7, r11)     // Catch:{ AnnotatedException -> 0x039f, CertPathValidatorException -> 0x03b4 }
            r3 = r6
        L_0x039d:
            r6 = r3
            goto L_0x038b
        L_0x039f:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r6 = "CertPathReviewer.policyExtError"
            r4.<init>(r5, r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r5 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = r0
            r5.<init>(r4, r3, r6, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r5     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x03b4:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r6 = "CertPathReviewer.policyQualifierError"
            r4.<init>(r5, r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r5 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = r0
            r5.<init>(r4, r3, r6, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r5     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x03c9:
            if (r16 > 0) goto L_0x0688
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = prepareNextCertB2(r5, r14, r3, r6)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            goto L_0x039d
        L_0x03d0:
            r4 = r6
        L_0x03d1:
            boolean r3 = isSelfIssued(r11)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 != 0) goto L_0x0680
            if (r18 == 0) goto L_0x067c
            int r3 = r18 + -1
        L_0x03db:
            if (r16 == 0) goto L_0x0678
            int r5 = r16 + -1
        L_0x03df:
            if (r17 == 0) goto L_0x0673
            int r6 = r17 + -1
            r7 = r3
        L_0x03e4:
            java.lang.String r3 = org.bouncycastle.x509.PKIXCertPathReviewer.POLICY_CONSTRAINTS     // Catch:{ AnnotatedException -> 0x042e }
            org.bouncycastle.asn1.DERObject r3 = getExtensionValue(r11, r3)     // Catch:{ AnnotatedException -> 0x042e }
            org.bouncycastle.asn1.ASN1Sequence r3 = (org.bouncycastle.asn1.ASN1Sequence) r3     // Catch:{ AnnotatedException -> 0x042e }
            if (r3 == 0) goto L_0x0443
            java.util.Enumeration r9 = r3.getObjects()     // Catch:{ AnnotatedException -> 0x042e }
        L_0x03f2:
            boolean r3 = r9.hasMoreElements()     // Catch:{ AnnotatedException -> 0x042e }
            if (r3 == 0) goto L_0x0443
            java.lang.Object r3 = r9.nextElement()     // Catch:{ AnnotatedException -> 0x042e }
            org.bouncycastle.asn1.ASN1TaggedObject r3 = (org.bouncycastle.asn1.ASN1TaggedObject) r3     // Catch:{ AnnotatedException -> 0x042e }
            int r10 = r3.getTagNo()     // Catch:{ AnnotatedException -> 0x042e }
            switch(r10) {
                case 0: goto L_0x040a;
                case 1: goto L_0x041e;
                default: goto L_0x0405;
            }     // Catch:{ AnnotatedException -> 0x042e }
        L_0x0405:
            r3 = r5
            r5 = r7
        L_0x0407:
            r7 = r5
            r5 = r3
            goto L_0x03f2
        L_0x040a:
            org.bouncycastle.asn1.DERInteger r3 = org.bouncycastle.asn1.DERInteger.getInstance(r3)     // Catch:{ AnnotatedException -> 0x042e }
            java.math.BigInteger r3 = r3.getValue()     // Catch:{ AnnotatedException -> 0x042e }
            int r3 = r3.intValue()     // Catch:{ AnnotatedException -> 0x042e }
            if (r3 >= r7) goto L_0x0405
            r24 = r5
            r5 = r3
            r3 = r24
            goto L_0x0407
        L_0x041e:
            org.bouncycastle.asn1.DERInteger r3 = org.bouncycastle.asn1.DERInteger.getInstance(r3)     // Catch:{ AnnotatedException -> 0x042e }
            java.math.BigInteger r3 = r3.getValue()     // Catch:{ AnnotatedException -> 0x042e }
            int r3 = r3.intValue()     // Catch:{ AnnotatedException -> 0x042e }
            if (r3 >= r5) goto L_0x0405
            r5 = r7
            goto L_0x0407
        L_0x042e:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r3 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r4 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r5 = "CertPathReviewer.policyConstExtError"
            r3.<init>(r4, r5)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r4 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r5 = r0
            r4.<init>(r3, r5, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0443:
            java.lang.String r3 = org.bouncycastle.x509.PKIXCertPathReviewer.INHIBIT_ANY_POLICY     // Catch:{ AnnotatedException -> 0x046a }
            org.bouncycastle.asn1.DERObject r3 = getExtensionValue(r11, r3)     // Catch:{ AnnotatedException -> 0x046a }
            org.bouncycastle.asn1.DERInteger r3 = (org.bouncycastle.asn1.DERInteger) r3     // Catch:{ AnnotatedException -> 0x046a }
            if (r3 == 0) goto L_0x0670
            java.math.BigInteger r3 = r3.getValue()     // Catch:{ AnnotatedException -> 0x046a }
            int r3 = r3.intValue()     // Catch:{ AnnotatedException -> 0x046a }
            if (r3 >= r6) goto L_0x0670
        L_0x0457:
            r6 = r4
            r4 = r3
            r3 = r5
            r5 = r7
        L_0x045b:
            int r7 = r15 + -1
            r15 = r7
            r16 = r3
            r17 = r4
            r18 = r5
            r19 = r6
            r3 = r11
            r4 = r8
            goto L_0x0080
        L_0x046a:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r3 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r4 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r5 = "CertPathReviewer.policyInhibitExtError"
            r3.<init>(r4, r5)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r4 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r5 = r0
            r4.<init>(r3, r5, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x047f:
            boolean r5 = isSelfIssued(r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r5 != 0) goto L_0x066c
            if (r18 <= 0) goto L_0x066c
            int r5 = r18 + -1
        L_0x0489:
            java.lang.String r6 = org.bouncycastle.x509.PKIXCertPathReviewer.POLICY_CONSTRAINTS     // Catch:{ AnnotatedException -> 0x04bd }
            org.bouncycastle.asn1.DERObject r3 = getExtensionValue(r3, r6)     // Catch:{ AnnotatedException -> 0x04bd }
            org.bouncycastle.asn1.ASN1Sequence r3 = (org.bouncycastle.asn1.ASN1Sequence) r3     // Catch:{ AnnotatedException -> 0x04bd }
            if (r3 == 0) goto L_0x04d2
            java.util.Enumeration r6 = r3.getObjects()     // Catch:{ AnnotatedException -> 0x04bd }
        L_0x0497:
            boolean r3 = r6.hasMoreElements()     // Catch:{ AnnotatedException -> 0x04bd }
            if (r3 == 0) goto L_0x04d2
            java.lang.Object r3 = r6.nextElement()     // Catch:{ AnnotatedException -> 0x04bd }
            org.bouncycastle.asn1.ASN1TaggedObject r3 = (org.bouncycastle.asn1.ASN1TaggedObject) r3     // Catch:{ AnnotatedException -> 0x04bd }
            int r7 = r3.getTagNo()     // Catch:{ AnnotatedException -> 0x04bd }
            switch(r7) {
                case 0: goto L_0x04ad;
                default: goto L_0x04aa;
            }     // Catch:{ AnnotatedException -> 0x04bd }
        L_0x04aa:
            r3 = r5
        L_0x04ab:
            r5 = r3
            goto L_0x0497
        L_0x04ad:
            org.bouncycastle.asn1.DERInteger r3 = org.bouncycastle.asn1.DERInteger.getInstance(r3)     // Catch:{ AnnotatedException -> 0x04bd }
            java.math.BigInteger r3 = r3.getValue()     // Catch:{ AnnotatedException -> 0x04bd }
            int r3 = r3.intValue()     // Catch:{ AnnotatedException -> 0x04bd }
            if (r3 != 0) goto L_0x04aa
            r3 = 0
            goto L_0x04ab
        L_0x04bd:
            r3 = move-exception
            org.bouncycastle.i18n.ErrorBundle r3 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r4 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r5 = "CertPathReviewer.policyConstExtError"
            r3.<init>(r4, r5)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r4 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r5 = r0
            r4.<init>(r3, r5, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x04d2:
            if (r19 != 0) goto L_0x0507
            r0 = r25
            java.security.cert.PKIXParameters r0 = r0.pkixParams     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = r0
            boolean r3 = r3.isExplicitPolicyRequired()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x04f3
            org.bouncycastle.i18n.ErrorBundle r3 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r4 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r5 = "CertPathReviewer.explicitPolicy"
            r3.<init>(r4, r5)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r4 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r5 = r0
            r4.<init>(r3, r5, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x04f3:
            r3 = 0
        L_0x04f4:
            if (r5 > 0) goto L_0x00f4
            if (r3 != 0) goto L_0x00f4
            org.bouncycastle.i18n.ErrorBundle r3 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r4 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r5 = "CertPathReviewer.invalidPolicy"
            r3.<init>(r4, r5)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r4 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r4.<init>(r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0507:
            boolean r3 = isAnyPolicy(r13)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x05be
            r0 = r25
            java.security.cert.PKIXParameters r0 = r0.pkixParams     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = r0
            boolean r3 = r3.isExplicitPolicyRequired()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x0668
            boolean r3 = r4.isEmpty()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x0532
            org.bouncycastle.i18n.ErrorBundle r3 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r4 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r5 = "CertPathReviewer.explicitPolicy"
            r3.<init>(r4, r5)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.x509.CertPathReviewerException r4 = new org.bouncycastle.x509.CertPathReviewerException     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r0 = r25
            java.security.cert.CertPath r0 = r0.certPath     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r5 = r0
            r4.<init>(r3, r5, r15)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            throw r4     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0532:
            java.util.HashSet r6 = new java.util.HashSet     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6.<init>()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = 0
            r7 = r3
        L_0x0539:
            int r3 = r14.length     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r7 >= r3) goto L_0x0572
            r8 = r14[r7]     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = 0
            r9 = r3
        L_0x0540:
            int r3 = r8.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r9 >= r3) goto L_0x056e
            java.lang.Object r3 = r8.get(r9)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r10 = "2.5.29.32.0"
            java.lang.String r11 = r3.getValidPolicy()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r10 = r10.equals(r11)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r10 == 0) goto L_0x056a
            java.util.Iterator r3 = r3.getChildren()     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x055c:
            boolean r10 = r3.hasNext()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r10 == 0) goto L_0x056a
            java.lang.Object r10 = r3.next()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6.add(r10)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            goto L_0x055c
        L_0x056a:
            int r3 = r9 + 1
            r9 = r3
            goto L_0x0540
        L_0x056e:
            int r3 = r7 + 1
            r7 = r3
            goto L_0x0539
        L_0x0572:
            java.util.Iterator r6 = r6.iterator()     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0576:
            boolean r3 = r6.hasNext()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x058d
            java.lang.Object r3 = r6.next()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r3 = r3.getValidPolicy()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r3 = r4.contains(r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 != 0) goto L_0x0576
            goto L_0x0576
        L_0x058d:
            if (r19 == 0) goto L_0x0668
            r0 = r25
            int r0 = r0.n     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = r0
            r4 = 1
            int r3 = r3 - r4
            r4 = r3
            r3 = r19
        L_0x0599:
            if (r4 < 0) goto L_0x04f4
            r6 = r14[r4]     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r7 = 0
            r8 = r3
        L_0x059f:
            int r3 = r6.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r7 >= r3) goto L_0x05b9
            java.lang.Object r3 = r6.get(r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r9 = r3.hasChildren()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r9 != 0) goto L_0x0665
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = removePolicyNode(r8, r14, r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x05b5:
            int r7 = r7 + 1
            r8 = r3
            goto L_0x059f
        L_0x05b9:
            int r3 = r4 + -1
            r4 = r3
            r3 = r8
            goto L_0x0599
        L_0x05be:
            java.util.HashSet r4 = new java.util.HashSet     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r4.<init>()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = 0
            r6 = r3
        L_0x05c5:
            int r3 = r14.length     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r6 >= r3) goto L_0x060c
            r7 = r14[r6]     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = 0
            r8 = r3
        L_0x05cc:
            int r3 = r7.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r8 >= r3) goto L_0x0608
            java.lang.Object r3 = r7.get(r8)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r9 = "2.5.29.32.0"
            java.lang.String r10 = r3.getValidPolicy()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r9 = r9.equals(r10)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r9 == 0) goto L_0x0604
            java.util.Iterator r9 = r3.getChildren()     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x05e8:
            boolean r3 = r9.hasNext()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x0604
            java.lang.Object r3 = r9.next()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r10 = "2.5.29.32.0"
            java.lang.String r11 = r3.getValidPolicy()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r10 = r10.equals(r11)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r10 != 0) goto L_0x05e8
            r4.add(r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            goto L_0x05e8
        L_0x0604:
            int r3 = r8 + 1
            r8 = r3
            goto L_0x05cc
        L_0x0608:
            int r3 = r6 + 1
            r6 = r3
            goto L_0x05c5
        L_0x060c:
            java.util.Iterator r4 = r4.iterator()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r6 = r19
        L_0x0612:
            boolean r3 = r4.hasNext()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r3 == 0) goto L_0x062e
            java.lang.Object r3 = r4.next()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            java.lang.String r7 = r3.getValidPolicy()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r7 = r13.contains(r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r7 != 0) goto L_0x0663
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = removePolicyNode(r6, r14, r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x062c:
            r6 = r3
            goto L_0x0612
        L_0x062e:
            if (r6 == 0) goto L_0x0660
            r0 = r25
            int r0 = r0.n     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r3 = r0
            r4 = 1
            int r3 = r3 - r4
            r4 = r3
            r3 = r6
        L_0x0639:
            if (r4 < 0) goto L_0x04f4
            r6 = r14[r4]     // Catch:{ CertPathReviewerException -> 0x00e4 }
            r7 = 0
            r8 = r3
        L_0x063f:
            int r3 = r6.size()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r7 >= r3) goto L_0x0659
            java.lang.Object r3 = r6.get(r7)     // Catch:{ CertPathReviewerException -> 0x00e4 }
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = (org.bouncycastle.jce.provider.PKIXPolicyNode) r3     // Catch:{ CertPathReviewerException -> 0x00e4 }
            boolean r9 = r3.hasChildren()     // Catch:{ CertPathReviewerException -> 0x00e4 }
            if (r9 != 0) goto L_0x065e
            org.bouncycastle.jce.provider.PKIXPolicyNode r3 = removePolicyNode(r8, r14, r3)     // Catch:{ CertPathReviewerException -> 0x00e4 }
        L_0x0655:
            int r7 = r7 + 1
            r8 = r3
            goto L_0x063f
        L_0x0659:
            int r3 = r4 + -1
            r4 = r3
            r3 = r8
            goto L_0x0639
        L_0x065e:
            r3 = r8
            goto L_0x0655
        L_0x0660:
            r3 = r6
            goto L_0x04f4
        L_0x0663:
            r3 = r6
            goto L_0x062c
        L_0x0665:
            r3 = r8
            goto L_0x05b5
        L_0x0668:
            r3 = r19
            goto L_0x04f4
        L_0x066c:
            r5 = r18
            goto L_0x0489
        L_0x0670:
            r3 = r6
            goto L_0x0457
        L_0x0673:
            r6 = r17
            r7 = r3
            goto L_0x03e4
        L_0x0678:
            r5 = r16
            goto L_0x03df
        L_0x067c:
            r3 = r18
            goto L_0x03db
        L_0x0680:
            r5 = r16
            r6 = r17
            r7 = r18
            goto L_0x03e4
        L_0x0688:
            r3 = r6
            goto L_0x039d
        L_0x068b:
            r4 = r9
            goto L_0x03d1
        L_0x068e:
            r3 = r16
            r4 = r17
            r5 = r18
            r6 = r9
            goto L_0x045b
        L_0x0697:
            r9 = r3
            goto L_0x028d
        L_0x069a:
            r8 = r20
            r3 = r6
            goto L_0x0289
        L_0x069f:
            r3 = r8
            goto L_0x025a
        L_0x06a2:
            r3 = r6
            goto L_0x01cc
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.x509.PKIXCertPathReviewer.checkPolicy():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:155:0x04e6  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x04ed  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x008a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void checkSignatures() {
        /*
            r22 = this;
            r6 = 0
            r7 = 0
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r8 = "CertPathReviewer.certPathValidDate"
            r9 = 2
            java.lang.Object[] r9 = new java.lang.Object[r9]
            r10 = 0
            org.bouncycastle.i18n.filter.TrustedInput r11 = new org.bouncycastle.i18n.filter.TrustedInput
            r0 = r22
            java.util.Date r0 = r0.validDate
            r12 = r0
            r11.<init>(r12)
            r9[r10] = r11
            r10 = 1
            org.bouncycastle.i18n.filter.TrustedInput r11 = new org.bouncycastle.i18n.filter.TrustedInput
            java.util.Date r12 = new java.util.Date
            r12.<init>()
            r11.<init>(r12)
            r9[r10] = r11
            r4.<init>(r5, r8, r9)
            r0 = r22
            r1 = r4
            r0.addNotification(r1)
            r0 = r22
            java.util.List r0 = r0.certs     // Catch:{ CertPathReviewerException -> 0x01ea }
            r4 = r0
            r0 = r22
            java.util.List r0 = r0.certs     // Catch:{ CertPathReviewerException -> 0x01ea }
            r5 = r0
            int r5 = r5.size()     // Catch:{ CertPathReviewerException -> 0x01ea }
            r8 = 1
            int r5 = r5 - r8
            java.lang.Object r4 = r4.get(r5)     // Catch:{ CertPathReviewerException -> 0x01ea }
            java.security.cert.X509Certificate r4 = (java.security.cert.X509Certificate) r4     // Catch:{ CertPathReviewerException -> 0x01ea }
            r0 = r22
            java.security.cert.PKIXParameters r0 = r0.pkixParams     // Catch:{ CertPathReviewerException -> 0x01ea }
            r5 = r0
            java.util.Set r5 = r5.getTrustAnchors()     // Catch:{ CertPathReviewerException -> 0x01ea }
            r0 = r22
            r1 = r4
            r2 = r5
            java.util.Collection r5 = r0.getTrustAnchors(r1, r2)     // Catch:{ CertPathReviewerException -> 0x01ea }
            int r8 = r5.size()     // Catch:{ CertPathReviewerException -> 0x01ea }
            r9 = 1
            if (r8 <= r9) goto L_0x016b
            org.bouncycastle.i18n.ErrorBundle r8 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x01ea }
            java.lang.String r9 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r10 = "CertPathReviewer.conflictingTrustAnchors"
            r11 = 2
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ CertPathReviewerException -> 0x01ea }
            r12 = 0
            java.lang.Integer r13 = new java.lang.Integer     // Catch:{ CertPathReviewerException -> 0x01ea }
            int r5 = r5.size()     // Catch:{ CertPathReviewerException -> 0x01ea }
            r13.<init>(r5)     // Catch:{ CertPathReviewerException -> 0x01ea }
            r11[r12] = r13     // Catch:{ CertPathReviewerException -> 0x01ea }
            r5 = 1
            org.bouncycastle.i18n.filter.UntrustedInput r12 = new org.bouncycastle.i18n.filter.UntrustedInput     // Catch:{ CertPathReviewerException -> 0x01ea }
            javax.security.auth.x500.X500Principal r4 = r4.getIssuerX500Principal()     // Catch:{ CertPathReviewerException -> 0x01ea }
            r12.<init>(r4)     // Catch:{ CertPathReviewerException -> 0x01ea }
            r11[r5] = r12     // Catch:{ CertPathReviewerException -> 0x01ea }
            r8.<init>(r9, r10, r11)     // Catch:{ CertPathReviewerException -> 0x01ea }
            r0 = r22
            r1 = r8
            r0.addError(r1)     // Catch:{ CertPathReviewerException -> 0x01ea }
            r4 = r6
        L_0x0087:
            r13 = r4
        L_0x0088:
            if (r13 == 0) goto L_0x04ed
            java.security.cert.X509Certificate r4 = r13.getTrustedCert()
            if (r4 == 0) goto L_0x01f9
            javax.security.auth.x500.X500Principal r5 = getSubjectPrincipal(r4)     // Catch:{ IllegalArgumentException -> 0x0204 }
        L_0x0094:
            if (r4 == 0) goto L_0x00b0
            boolean[] r4 = r4.getKeyUsage()
            if (r4 == 0) goto L_0x00b0
            r6 = 5
            boolean r4 = r4[r6]
            if (r4 != 0) goto L_0x00b0
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r6 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.trustKeyUsage"
            r4.<init>(r6, r7)
            r0 = r22
            r1 = r4
            r0.addNotification(r1)
        L_0x00b0:
            r4 = r5
        L_0x00b1:
            r5 = 0
            r6 = 0
            if (r13 == 0) goto L_0x04e6
            java.security.cert.X509Certificate r5 = r13.getTrustedCert()
            if (r5 == 0) goto L_0x0226
            java.security.PublicKey r6 = r5.getPublicKey()
        L_0x00bf:
            org.bouncycastle.asn1.x509.AlgorithmIdentifier r7 = getAlgorithmIdentifier(r6)     // Catch:{ CertPathValidatorException -> 0x022c }
            r7.getObjectId()     // Catch:{ CertPathValidatorException -> 0x022c }
            r7.getParameters()     // Catch:{ CertPathValidatorException -> 0x022c }
        L_0x00c9:
            r0 = r22
            java.util.List r0 = r0.certs
            r7 = r0
            int r7 = r7.size()
            r8 = 1
            int r7 = r7 - r8
            r12 = r7
            r8 = r5
            r14 = r4
            r9 = r6
        L_0x00d8:
            if (r12 < 0) goto L_0x04d3
            r0 = r22
            int r0 = r0.n
            r4 = r0
            int r15 = r4 - r12
            r0 = r22
            java.util.List r0 = r0.certs
            r4 = r0
            java.lang.Object r6 = r4.get(r12)
            java.security.cert.X509Certificate r6 = (java.security.cert.X509Certificate) r6
            if (r9 == 0) goto L_0x026c
            r0 = r22
            java.security.cert.PKIXParameters r0 = r0.pkixParams     // Catch:{ GeneralSecurityException -> 0x023e }
            r4 = r0
            java.lang.String r4 = r4.getSigProvider()     // Catch:{ GeneralSecurityException -> 0x023e }
            org.bouncycastle.jce.provider.CertPathValidatorUtilities.verifyX509Certificate(r6, r9, r4)     // Catch:{ GeneralSecurityException -> 0x023e }
        L_0x00fa:
            r0 = r22
            java.util.Date r0 = r0.validDate     // Catch:{ CertificateNotYetValidException -> 0x0333, CertificateExpiredException -> 0x0355 }
            r4 = r0
            r6.checkValidity(r4)     // Catch:{ CertificateNotYetValidException -> 0x0333, CertificateExpiredException -> 0x0355 }
        L_0x0102:
            r0 = r22
            java.security.cert.PKIXParameters r0 = r0.pkixParams
            r4 = r0
            boolean r4 = r4.isRevocationEnabled()
            if (r4 == 0) goto L_0x03e2
            r4 = 0
            java.lang.String r5 = org.bouncycastle.x509.PKIXCertPathReviewer.CRL_DIST_POINTS     // Catch:{ AnnotatedException -> 0x0377 }
            org.bouncycastle.asn1.DERObject r5 = getExtensionValue(r6, r5)     // Catch:{ AnnotatedException -> 0x0377 }
            if (r5 == 0) goto L_0x011a
            org.bouncycastle.asn1.x509.CRLDistPoint r4 = org.bouncycastle.asn1.x509.CRLDistPoint.getInstance(r5)     // Catch:{ AnnotatedException -> 0x0377 }
        L_0x011a:
            r5 = 0
            java.lang.String r7 = org.bouncycastle.x509.PKIXCertPathReviewer.AUTH_INFO_ACCESS     // Catch:{ AnnotatedException -> 0x038a }
            org.bouncycastle.asn1.DERObject r7 = getExtensionValue(r6, r7)     // Catch:{ AnnotatedException -> 0x038a }
            if (r7 == 0) goto L_0x0127
            org.bouncycastle.asn1.x509.AuthorityInformationAccess r5 = org.bouncycastle.asn1.x509.AuthorityInformationAccess.getInstance(r7)     // Catch:{ AnnotatedException -> 0x038a }
        L_0x0127:
            r0 = r22
            r1 = r4
            java.util.Vector r10 = r0.getCRLDistUrls(r1)
            r0 = r22
            r1 = r5
            java.util.Vector r11 = r0.getOCSPUrls(r1)
            java.util.Iterator r4 = r10.iterator()
        L_0x0139:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x039d
            org.bouncycastle.i18n.ErrorBundle r5 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r7 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r16 = "CertPathReviewer.crlDistPoint"
            r17 = 1
            r0 = r17
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r17 = r0
            r18 = 0
            org.bouncycastle.i18n.filter.UntrustedUrlInput r19 = new org.bouncycastle.i18n.filter.UntrustedUrlInput
            java.lang.Object r20 = r4.next()
            r19.<init>(r20)
            r17[r18] = r19
            r0 = r5
            r1 = r7
            r2 = r16
            r3 = r17
            r0.<init>(r1, r2, r3)
            r0 = r22
            r1 = r5
            r2 = r12
            r0.addNotification(r1, r2)
            goto L_0x0139
        L_0x016b:
            boolean r8 = r5.isEmpty()     // Catch:{ CertPathReviewerException -> 0x01ea }
            if (r8 == 0) goto L_0x01a7
            org.bouncycastle.i18n.ErrorBundle r5 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x01ea }
            java.lang.String r8 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r9 = "CertPathReviewer.noTrustAnchorFound"
            r10 = 2
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ CertPathReviewerException -> 0x01ea }
            r11 = 0
            org.bouncycastle.i18n.filter.UntrustedInput r12 = new org.bouncycastle.i18n.filter.UntrustedInput     // Catch:{ CertPathReviewerException -> 0x01ea }
            javax.security.auth.x500.X500Principal r4 = r4.getIssuerX500Principal()     // Catch:{ CertPathReviewerException -> 0x01ea }
            r12.<init>(r4)     // Catch:{ CertPathReviewerException -> 0x01ea }
            r10[r11] = r12     // Catch:{ CertPathReviewerException -> 0x01ea }
            r4 = 1
            java.lang.Integer r11 = new java.lang.Integer     // Catch:{ CertPathReviewerException -> 0x01ea }
            r0 = r22
            java.security.cert.PKIXParameters r0 = r0.pkixParams     // Catch:{ CertPathReviewerException -> 0x01ea }
            r12 = r0
            java.util.Set r12 = r12.getTrustAnchors()     // Catch:{ CertPathReviewerException -> 0x01ea }
            int r12 = r12.size()     // Catch:{ CertPathReviewerException -> 0x01ea }
            r11.<init>(r12)     // Catch:{ CertPathReviewerException -> 0x01ea }
            r10[r4] = r11     // Catch:{ CertPathReviewerException -> 0x01ea }
            r5.<init>(r8, r9, r10)     // Catch:{ CertPathReviewerException -> 0x01ea }
            r0 = r22
            r1 = r5
            r0.addError(r1)     // Catch:{ CertPathReviewerException -> 0x01ea }
            r4 = r6
            goto L_0x0087
        L_0x01a7:
            java.util.Iterator r5 = r5.iterator()     // Catch:{ CertPathReviewerException -> 0x01ea }
            java.lang.Object r5 = r5.next()     // Catch:{ CertPathReviewerException -> 0x01ea }
            java.security.cert.TrustAnchor r5 = (java.security.cert.TrustAnchor) r5     // Catch:{ CertPathReviewerException -> 0x01ea }
            java.security.cert.X509Certificate r6 = r5.getTrustedCert()     // Catch:{ CertPathReviewerException -> 0x04e3 }
            if (r6 == 0) goto L_0x01ce
            java.security.cert.X509Certificate r6 = r5.getTrustedCert()     // Catch:{ CertPathReviewerException -> 0x04e3 }
            java.security.PublicKey r6 = r6.getPublicKey()     // Catch:{ CertPathReviewerException -> 0x04e3 }
        L_0x01bf:
            r0 = r22
            java.security.cert.PKIXParameters r0 = r0.pkixParams     // Catch:{ SignatureException -> 0x01d3, Exception -> 0x01e6 }
            r8 = r0
            java.lang.String r8 = r8.getSigProvider()     // Catch:{ SignatureException -> 0x01d3, Exception -> 0x01e6 }
            org.bouncycastle.jce.provider.CertPathValidatorUtilities.verifyX509Certificate(r4, r6, r8)     // Catch:{ SignatureException -> 0x01d3, Exception -> 0x01e6 }
            r4 = r5
            goto L_0x0087
        L_0x01ce:
            java.security.PublicKey r6 = r5.getCAPublicKey()     // Catch:{ CertPathReviewerException -> 0x04e3 }
            goto L_0x01bf
        L_0x01d3:
            r4 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ CertPathReviewerException -> 0x04e3 }
            java.lang.String r6 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r8 = "CertPathReviewer.trustButInvalidCert"
            r4.<init>(r6, r8)     // Catch:{ CertPathReviewerException -> 0x04e3 }
            r0 = r22
            r1 = r4
            r0.addError(r1)     // Catch:{ CertPathReviewerException -> 0x04e3 }
            r4 = r5
            goto L_0x0087
        L_0x01e6:
            r4 = move-exception
            r4 = r5
            goto L_0x0087
        L_0x01ea:
            r4 = move-exception
            r5 = r6
        L_0x01ec:
            org.bouncycastle.i18n.ErrorBundle r4 = r4.getErrorMessage()
            r0 = r22
            r1 = r4
            r0.addError(r1)
            r13 = r5
            goto L_0x0088
        L_0x01f9:
            javax.security.auth.x500.X500Principal r5 = new javax.security.auth.x500.X500Principal     // Catch:{ IllegalArgumentException -> 0x0204 }
            java.lang.String r6 = r13.getCAName()     // Catch:{ IllegalArgumentException -> 0x0204 }
            r5.<init>(r6)     // Catch:{ IllegalArgumentException -> 0x0204 }
            goto L_0x0094
        L_0x0204:
            r5 = move-exception
            org.bouncycastle.i18n.ErrorBundle r5 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r6 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r8 = "CertPathReviewer.trustDNInvalid"
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]
            r10 = 0
            org.bouncycastle.i18n.filter.UntrustedInput r11 = new org.bouncycastle.i18n.filter.UntrustedInput
            java.lang.String r12 = r13.getCAName()
            r11.<init>(r12)
            r9[r10] = r11
            r5.<init>(r6, r8, r9)
            r0 = r22
            r1 = r5
            r0.addError(r1)
            r5 = r7
            goto L_0x0094
        L_0x0226:
            java.security.PublicKey r6 = r13.getCAPublicKey()
            goto L_0x00bf
        L_0x022c:
            r7 = move-exception
            org.bouncycastle.i18n.ErrorBundle r7 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r8 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r9 = "CertPathReviewer.trustPubKeyError"
            r7.<init>(r8, r9)
            r0 = r22
            r1 = r7
            r0.addError(r1)
            goto L_0x00c9
        L_0x023e:
            r4 = move-exception
            org.bouncycastle.i18n.ErrorBundle r5 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r7 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r10 = "CertPathReviewer.signatureNotVerified"
            r11 = 3
            java.lang.Object[] r11 = new java.lang.Object[r11]
            r16 = 0
            java.lang.String r17 = r4.getMessage()
            r11[r16] = r17
            r16 = 1
            r11[r16] = r4
            r16 = 2
            java.lang.Class r4 = r4.getClass()
            java.lang.String r4 = r4.getName()
            r11[r16] = r4
            r5.<init>(r7, r10, r11)
            r0 = r22
            r1 = r5
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x00fa
        L_0x026c:
            boolean r4 = isSelfIssued(r6)
            if (r4 == 0) goto L_0x02c2
            java.security.PublicKey r4 = r6.getPublicKey()     // Catch:{ GeneralSecurityException -> 0x0294 }
            r0 = r22
            java.security.cert.PKIXParameters r0 = r0.pkixParams     // Catch:{ GeneralSecurityException -> 0x0294 }
            r5 = r0
            java.lang.String r5 = r5.getSigProvider()     // Catch:{ GeneralSecurityException -> 0x0294 }
            org.bouncycastle.jce.provider.CertPathValidatorUtilities.verifyX509Certificate(r6, r4, r5)     // Catch:{ GeneralSecurityException -> 0x0294 }
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ GeneralSecurityException -> 0x0294 }
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.rootKeyIsValidButNotATrustAnchor"
            r4.<init>(r5, r7)     // Catch:{ GeneralSecurityException -> 0x0294 }
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)     // Catch:{ GeneralSecurityException -> 0x0294 }
            goto L_0x00fa
        L_0x0294:
            r4 = move-exception
            org.bouncycastle.i18n.ErrorBundle r5 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r7 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r10 = "CertPathReviewer.signatureNotVerified"
            r11 = 3
            java.lang.Object[] r11 = new java.lang.Object[r11]
            r16 = 0
            java.lang.String r17 = r4.getMessage()
            r11[r16] = r17
            r16 = 1
            r11[r16] = r4
            r16 = 2
            java.lang.Class r4 = r4.getClass()
            java.lang.String r4 = r4.getName()
            r11[r16] = r4
            r5.<init>(r7, r10, r11)
            r0 = r22
            r1 = r5
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x00fa
        L_0x02c2:
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.NoIssuerPublicKey"
            r4.<init>(r5, r7)
            org.bouncycastle.asn1.DERObjectIdentifier r5 = org.bouncycastle.asn1.x509.X509Extensions.AuthorityKeyIdentifier
            java.lang.String r5 = r5.getId()
            byte[] r5 = r6.getExtensionValue(r5)
            if (r5 == 0) goto L_0x032a
            org.bouncycastle.asn1.ASN1Object r5 = org.bouncycastle.x509.extension.X509ExtensionUtil.fromExtensionValue(r5)     // Catch:{ IOException -> 0x04e0 }
            org.bouncycastle.asn1.x509.AuthorityKeyIdentifier r5 = org.bouncycastle.asn1.x509.AuthorityKeyIdentifier.getInstance(r5)     // Catch:{ IOException -> 0x04e0 }
            org.bouncycastle.asn1.x509.GeneralNames r7 = r5.getAuthorityCertIssuer()     // Catch:{ IOException -> 0x04e0 }
            if (r7 == 0) goto L_0x032a
            org.bouncycastle.asn1.x509.GeneralName[] r7 = r7.getNames()     // Catch:{ IOException -> 0x04e0 }
            r10 = 0
            r7 = r7[r10]     // Catch:{ IOException -> 0x04e0 }
            java.math.BigInteger r5 = r5.getAuthorityCertSerialNumber()     // Catch:{ IOException -> 0x04e0 }
            if (r5 == 0) goto L_0x032a
            r10 = 7
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ IOException -> 0x04e0 }
            r11 = 0
            org.bouncycastle.i18n.LocaleString r16 = new org.bouncycastle.i18n.LocaleString     // Catch:{ IOException -> 0x04e0 }
            java.lang.String r17 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r18 = "missingIssuer"
            r16.<init>(r17, r18)     // Catch:{ IOException -> 0x04e0 }
            r10[r11] = r16     // Catch:{ IOException -> 0x04e0 }
            r11 = 1
            java.lang.String r16 = " \""
            r10[r11] = r16     // Catch:{ IOException -> 0x04e0 }
            r11 = 2
            r10[r11] = r7     // Catch:{ IOException -> 0x04e0 }
            r7 = 3
            java.lang.String r11 = "\" "
            r10[r7] = r11     // Catch:{ IOException -> 0x04e0 }
            r7 = 4
            org.bouncycastle.i18n.LocaleString r11 = new org.bouncycastle.i18n.LocaleString     // Catch:{ IOException -> 0x04e0 }
            java.lang.String r16 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r17 = "missingSerial"
            r0 = r11
            r1 = r16
            r2 = r17
            r0.<init>(r1, r2)     // Catch:{ IOException -> 0x04e0 }
            r10[r7] = r11     // Catch:{ IOException -> 0x04e0 }
            r7 = 5
            java.lang.String r11 = " "
            r10[r7] = r11     // Catch:{ IOException -> 0x04e0 }
            r7 = 6
            r10[r7] = r5     // Catch:{ IOException -> 0x04e0 }
            r4.setExtraArguments(r10)     // Catch:{ IOException -> 0x04e0 }
        L_0x032a:
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x00fa
        L_0x0333:
            r4 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.certificateNotYetValid"
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]
            r11 = 0
            org.bouncycastle.i18n.filter.TrustedInput r16 = new org.bouncycastle.i18n.filter.TrustedInput
            java.util.Date r17 = r6.getNotBefore()
            r16.<init>(r17)
            r10[r11] = r16
            r4.<init>(r5, r7, r10)
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x0102
        L_0x0355:
            r4 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.certificateExpired"
            r10 = 1
            java.lang.Object[] r10 = new java.lang.Object[r10]
            r11 = 0
            org.bouncycastle.i18n.filter.TrustedInput r16 = new org.bouncycastle.i18n.filter.TrustedInput
            java.util.Date r17 = r6.getNotAfter()
            r16.<init>(r17)
            r10[r11] = r16
            r4.<init>(r5, r7, r10)
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x0102
        L_0x0377:
            r5 = move-exception
            org.bouncycastle.i18n.ErrorBundle r5 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r7 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r10 = "CertPathReviewer.crlDistPtExtError"
            r5.<init>(r7, r10)
            r0 = r22
            r1 = r5
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x011a
        L_0x038a:
            r7 = move-exception
            org.bouncycastle.i18n.ErrorBundle r7 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r10 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r11 = "CertPathReviewer.crlAuthInfoAccError"
            r7.<init>(r10, r11)
            r0 = r22
            r1 = r7
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x0127
        L_0x039d:
            java.util.Iterator r4 = r11.iterator()
        L_0x03a1:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x03d3
            org.bouncycastle.i18n.ErrorBundle r5 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r7 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r16 = "CertPathReviewer.ocspLocation"
            r17 = 1
            r0 = r17
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r17 = r0
            r18 = 0
            org.bouncycastle.i18n.filter.UntrustedUrlInput r19 = new org.bouncycastle.i18n.filter.UntrustedUrlInput
            java.lang.Object r20 = r4.next()
            r19.<init>(r20)
            r17[r18] = r19
            r0 = r5
            r1 = r7
            r2 = r16
            r3 = r17
            r0.<init>(r1, r2, r3)
            r0 = r22
            r1 = r5
            r2 = r12
            r0.addNotification(r1, r2)
            goto L_0x03a1
        L_0x03d3:
            r0 = r22
            java.security.cert.PKIXParameters r0 = r0.pkixParams     // Catch:{ CertPathReviewerException -> 0x048f }
            r5 = r0
            r0 = r22
            java.util.Date r0 = r0.validDate     // Catch:{ CertPathReviewerException -> 0x048f }
            r7 = r0
            r4 = r22
            r4.checkRevocation(r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ CertPathReviewerException -> 0x048f }
        L_0x03e2:
            if (r14 == 0) goto L_0x0413
            javax.security.auth.x500.X500Principal r4 = r6.getIssuerX500Principal()
            boolean r4 = r4.equals(r14)
            if (r4 != 0) goto L_0x0413
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.certWrongIssuer"
            r8 = 2
            java.lang.Object[] r8 = new java.lang.Object[r8]
            r10 = 0
            java.lang.String r11 = r14.getName()
            r8[r10] = r11
            r10 = 1
            javax.security.auth.x500.X500Principal r11 = r6.getIssuerX500Principal()
            java.lang.String r11 = r11.getName()
            r8[r10] = r11
            r4.<init>(r5, r7, r8)
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)
        L_0x0413:
            r0 = r22
            int r0 = r0.n
            r4 = r0
            if (r15 == r4) goto L_0x0470
            if (r6 == 0) goto L_0x0433
            int r4 = r6.getVersion()
            r5 = 1
            if (r4 != r5) goto L_0x0433
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.noCACert"
            r4.<init>(r5, r7)
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)
        L_0x0433:
            java.lang.String r4 = org.bouncycastle.x509.PKIXCertPathReviewer.BASIC_CONSTRAINTS     // Catch:{ AnnotatedException -> 0x04ae }
            org.bouncycastle.asn1.DERObject r4 = getExtensionValue(r6, r4)     // Catch:{ AnnotatedException -> 0x04ae }
            org.bouncycastle.asn1.x509.BasicConstraints r4 = org.bouncycastle.asn1.x509.BasicConstraints.getInstance(r4)     // Catch:{ AnnotatedException -> 0x04ae }
            if (r4 == 0) goto L_0x049d
            boolean r4 = r4.isCA()     // Catch:{ AnnotatedException -> 0x04ae }
            if (r4 != 0) goto L_0x0455
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ AnnotatedException -> 0x04ae }
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.noCACert"
            r4.<init>(r5, r7)     // Catch:{ AnnotatedException -> 0x04ae }
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)     // Catch:{ AnnotatedException -> 0x04ae }
        L_0x0455:
            boolean[] r4 = r6.getKeyUsage()
            if (r4 == 0) goto L_0x0470
            r5 = 5
            boolean r4 = r4[r5]
            if (r4 != 0) goto L_0x0470
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.noCertSign"
            r4.<init>(r5, r7)
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)
        L_0x0470:
            javax.security.auth.x500.X500Principal r4 = r6.getSubjectX500Principal()
            r0 = r22
            java.util.List r0 = r0.certs     // Catch:{ CertPathValidatorException -> 0x04c0 }
            r5 = r0
            java.security.PublicKey r5 = getNextWorkingKey(r5, r12)     // Catch:{ CertPathValidatorException -> 0x04c0 }
            org.bouncycastle.asn1.x509.AlgorithmIdentifier r7 = getAlgorithmIdentifier(r5)     // Catch:{ CertPathValidatorException -> 0x04de }
            r7.getObjectId()     // Catch:{ CertPathValidatorException -> 0x04de }
            r7.getParameters()     // Catch:{ CertPathValidatorException -> 0x04de }
        L_0x0487:
            int r7 = r12 + -1
            r12 = r7
            r8 = r6
            r14 = r4
            r9 = r5
            goto L_0x00d8
        L_0x048f:
            r4 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = r4.getErrorMessage()
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x03e2
        L_0x049d:
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle     // Catch:{ AnnotatedException -> 0x04ae }
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.noBasicConstraints"
            r4.<init>(r5, r7)     // Catch:{ AnnotatedException -> 0x04ae }
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)     // Catch:{ AnnotatedException -> 0x04ae }
            goto L_0x0455
        L_0x04ae:
            r4 = move-exception
            org.bouncycastle.i18n.ErrorBundle r4 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r5 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r7 = "CertPathReviewer.errorProcesingBC"
            r4.<init>(r5, r7)
            r0 = r22
            r1 = r4
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x0455
        L_0x04c0:
            r5 = move-exception
            r5 = r9
        L_0x04c2:
            org.bouncycastle.i18n.ErrorBundle r7 = new org.bouncycastle.i18n.ErrorBundle
            java.lang.String r8 = "org.bouncycastle.x509.CertPathReviewerMessages"
            java.lang.String r9 = "CertPathReviewer.pubKeyError"
            r7.<init>(r8, r9)
            r0 = r22
            r1 = r7
            r2 = r12
            r0.addError(r1, r2)
            goto L_0x0487
        L_0x04d3:
            r0 = r13
            r1 = r22
            r1.trustAnchor = r0
            r0 = r9
            r1 = r22
            r1.subjectPublicKey = r0
            return
        L_0x04de:
            r7 = move-exception
            goto L_0x04c2
        L_0x04e0:
            r5 = move-exception
            goto L_0x032a
        L_0x04e3:
            r4 = move-exception
            goto L_0x01ec
        L_0x04e6:
            r21 = r6
            r6 = r5
            r5 = r21
            goto L_0x00c9
        L_0x04ed:
            r4 = r7
            goto L_0x00b1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.x509.PKIXCertPathReviewer.checkSignatures():void");
    }

    private X509CRL getCRL(String str) throws CertPathReviewerException {
        try {
            URL url = new URL(str);
            if (!url.getProtocol().equals("http") && !url.getProtocol().equals("https")) {
                return null;
            }
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode() == 200) {
                return (X509CRL) CertificateFactory.getInstance("X.509", "BC").generateCRL(httpURLConnection.getInputStream());
            }
            throw new Exception(httpURLConnection.getResponseMessage());
        } catch (Exception e) {
            throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.loadCrlDistPointError", new Object[]{new UntrustedInput(str), e.getMessage(), e, e.getClass().getName()}));
        }
    }

    private boolean processQcStatements(X509Certificate x509Certificate, int i) {
        try {
            ASN1Sequence aSN1Sequence = (ASN1Sequence) getExtensionValue(x509Certificate, QC_STATEMENT);
            boolean z = false;
            for (int i2 = 0; i2 < aSN1Sequence.size(); i2++) {
                QCStatement instance = QCStatement.getInstance(aSN1Sequence.getObjectAt(i2));
                if (QCStatement.id_etsi_qcs_QcCompliance.equals(instance.getStatementId())) {
                    addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.QcEuCompliance"), i);
                } else if (!QCStatement.id_qcs_pkixQCSyntax_v1.equals(instance.getStatementId())) {
                    if (QCStatement.id_etsi_qcs_QcSSCD.equals(instance.getStatementId())) {
                        addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.QcSSCD"), i);
                    } else if (QCStatement.id_etsi_qcs_LimiteValue.equals(instance.getStatementId())) {
                        MonetaryValue instance2 = MonetaryValue.getInstance(instance.getStatementInfo());
                        instance2.getCurrency();
                        double doubleValue = instance2.getAmount().doubleValue() * Math.pow(10.0d, instance2.getExponent().doubleValue());
                        addNotification(instance2.getCurrency().isAlphabetic() ? new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.QcLimitValueAlpha", new Object[]{instance2.getCurrency().getAlphabetic(), new TrustedInput(new Double(doubleValue)), instance2}) : new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.QcLimitValueNum", new Object[]{new Integer(instance2.getCurrency().getNumeric()), new TrustedInput(new Double(doubleValue)), instance2}), i);
                    } else {
                        addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.QcUnknownStatement", new Object[]{instance.getStatementId(), new UntrustedInput(instance)}), i);
                        z = true;
                    }
                }
            }
            return !z;
        } catch (AnnotatedException e) {
            addError(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.QcStatementExtError"), i);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void addError(ErrorBundle errorBundle) {
        this.errors[0].add(errorBundle);
    }

    /* access modifiers changed from: protected */
    public void addError(ErrorBundle errorBundle, int i) {
        if (i < -1 || i >= this.n) {
            throw new IndexOutOfBoundsException();
        }
        this.errors[i + 1].add(errorBundle);
    }

    /* access modifiers changed from: protected */
    public void addNotification(ErrorBundle errorBundle) {
        this.notifications[0].add(errorBundle);
    }

    /* access modifiers changed from: protected */
    public void addNotification(ErrorBundle errorBundle, int i) {
        if (i < -1 || i >= this.n) {
            throw new IndexOutOfBoundsException();
        }
        this.notifications[i + 1].add(errorBundle);
    }

    /* access modifiers changed from: protected */
    public void checkCRLs(PKIXParameters pKIXParameters, X509Certificate x509Certificate, Date date, X509Certificate x509Certificate2, PublicKey publicKey, Vector vector, int i) throws CertPathReviewerException {
        Iterator it;
        boolean z;
        boolean[] keyUsage;
        String str;
        X509CRL crl;
        X509CRLStoreSelector x509CRLStoreSelector = new X509CRLStoreSelector();
        try {
            x509CRLStoreSelector.addIssuerName(getEncodedIssuerPrincipal(x509Certificate).getEncoded());
            x509CRLStoreSelector.setCertificateChecking(x509Certificate);
            try {
                Collection findCRLs = findCRLs(x509CRLStoreSelector, pKIXParameters.getCertStores());
                Iterator it2 = findCRLs.iterator();
                if (findCRLs.isEmpty()) {
                    ArrayList arrayList = new ArrayList();
                    for (X509CRL issuerX500Principal : findCRLs(new X509CRLStoreSelector(), pKIXParameters.getCertStores())) {
                        arrayList.add(issuerX500Principal.getIssuerX500Principal());
                    }
                    addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.noCrlInCertstore", new Object[]{new UntrustedInput(x509CRLStoreSelector.getIssuerNames()), new UntrustedInput(arrayList), new Integer(arrayList.size())}), i);
                }
                it = it2;
            } catch (AnnotatedException e) {
                addError(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlExtractionError", new Object[]{e.getCause().getMessage(), e.getCause(), e.getCause().getClass().getName()}), i);
                it = new ArrayList().iterator();
            }
            X509CRL x509crl = null;
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                x509crl = (X509CRL) it.next();
                if (x509crl.getNextUpdate() == null || new Date().before(x509crl.getNextUpdate())) {
                    z = true;
                    addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.localValidCRL", new Object[]{new TrustedInput(x509crl.getThisUpdate()), new TrustedInput(x509crl.getNextUpdate())}), i);
                } else {
                    addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.localInvalidCRL", new Object[]{new TrustedInput(x509crl.getThisUpdate()), new TrustedInput(x509crl.getNextUpdate())}), i);
                }
            }
            if (!z) {
                Iterator it3 = vector.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    try {
                        str = (String) it3.next();
                        crl = getCRL(str);
                        if (crl == null) {
                            continue;
                        } else if (!x509Certificate.getIssuerX500Principal().equals(crl.getIssuerX500Principal())) {
                            addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.onlineCRLWrongCA", new Object[]{new UntrustedInput(crl.getIssuerX500Principal().getName()), new UntrustedInput(x509Certificate.getIssuerX500Principal().getName()), new UntrustedUrlInput(str)}), i);
                        } else if (crl.getNextUpdate() == null || new Date().before(crl.getNextUpdate())) {
                            z = true;
                            addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.onlineValidCRL", new Object[]{new TrustedInput(crl.getThisUpdate()), new TrustedInput(crl.getNextUpdate()), new UntrustedUrlInput(str)}), i);
                        } else {
                            addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.onlineInvalidCRL", new Object[]{new TrustedInput(crl.getThisUpdate()), new TrustedInput(crl.getNextUpdate()), new UntrustedUrlInput(str)}), i);
                        }
                    } catch (CertPathReviewerException e2) {
                        addNotification(e2.getErrorMessage(), i);
                        z = z;
                    }
                }
                z = true;
                addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.onlineValidCRL", new Object[]{new TrustedInput(crl.getThisUpdate()), new TrustedInput(crl.getNextUpdate()), new UntrustedUrlInput(str)}), i);
                x509crl = crl;
            }
            if (x509crl != null) {
                if (x509Certificate2 != null && (keyUsage = x509Certificate2.getKeyUsage()) != null && (keyUsage.length < 7 || !keyUsage[6])) {
                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.noCrlSigningPermited"));
                } else if (publicKey != null) {
                    try {
                        x509crl.verify(publicKey, "BC");
                        X509CRLEntry revokedCertificate = x509crl.getRevokedCertificate(x509Certificate.getSerialNumber());
                        if (revokedCertificate != null) {
                            String str2 = null;
                            if (revokedCertificate.hasExtensions()) {
                                try {
                                    DEREnumerated instance = DEREnumerated.getInstance(getExtensionValue(revokedCertificate, X509Extensions.ReasonCode.getId()));
                                    str2 = instance != null ? crlReasons[instance.getValue().intValue()] : crlReasons[7];
                                } catch (AnnotatedException e3) {
                                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlReasonExtError"), e3);
                                }
                            }
                            LocaleString localeString = new LocaleString(RESOURCE_NAME, str2);
                            if (!date.before(revokedCertificate.getRevocationDate())) {
                                throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.certRevoked", new Object[]{new TrustedInput(revokedCertificate.getRevocationDate()), localeString}));
                            }
                            addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.revokedAfterValidation", new Object[]{new TrustedInput(revokedCertificate.getRevocationDate()), localeString}), i);
                        } else {
                            addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.notRevoked"), i);
                        }
                        if (x509crl.getNextUpdate() != null && x509crl.getNextUpdate().before(new Date())) {
                            addNotification(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlUpdateAvailable", new Object[]{new TrustedInput(x509crl.getNextUpdate())}), i);
                        }
                        try {
                            DERObject extensionValue = getExtensionValue(x509crl, ISSUING_DISTRIBUTION_POINT);
                            try {
                                DERObject extensionValue2 = getExtensionValue(x509crl, DELTA_CRL_INDICATOR);
                                if (extensionValue2 != null) {
                                    X509CRLStoreSelector x509CRLStoreSelector2 = new X509CRLStoreSelector();
                                    try {
                                        x509CRLStoreSelector2.addIssuerName(getIssuerPrincipal(x509crl).getEncoded());
                                        x509CRLStoreSelector2.setMinCRLNumber(((DERInteger) extensionValue2).getPositiveValue());
                                        try {
                                            x509CRLStoreSelector2.setMaxCRLNumber(((DERInteger) getExtensionValue(x509crl, CRL_NUMBER)).getPositiveValue().subtract(BigInteger.valueOf(1)));
                                            boolean z2 = false;
                                            try {
                                                Iterator it4 = findCRLs(x509CRLStoreSelector2, pKIXParameters.getCertStores()).iterator();
                                                while (true) {
                                                    if (!it4.hasNext()) {
                                                        break;
                                                    }
                                                    try {
                                                        DERObject extensionValue3 = getExtensionValue((X509CRL) it4.next(), ISSUING_DISTRIBUTION_POINT);
                                                        if (extensionValue == null) {
                                                            if (extensionValue3 == null) {
                                                                z2 = true;
                                                                break;
                                                            }
                                                        } else if (extensionValue.equals(extensionValue3)) {
                                                            z2 = true;
                                                            break;
                                                        }
                                                    } catch (AnnotatedException e4) {
                                                        throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.distrPtExtError"), e4);
                                                    }
                                                }
                                                if (!z2) {
                                                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.noBaseCRL"));
                                                }
                                            } catch (AnnotatedException e5) {
                                                throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlExtractionError"), e5);
                                            }
                                        } catch (AnnotatedException e6) {
                                            throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlNbrExtError"), e6);
                                        }
                                    } catch (IOException e7) {
                                        throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlIssuerException"), e7);
                                    }
                                }
                                if (extensionValue != null) {
                                    IssuingDistributionPoint instance2 = IssuingDistributionPoint.getInstance(extensionValue);
                                    try {
                                        BasicConstraints instance3 = BasicConstraints.getInstance(getExtensionValue(x509Certificate, BASIC_CONSTRAINTS));
                                        if (instance2.onlyContainsUserCerts() && instance3 != null && instance3.isCA()) {
                                            throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlOnlyUserCert"));
                                        } else if (instance2.onlyContainsCACerts() && (instance3 == null || !instance3.isCA())) {
                                            throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlOnlyCaCert"));
                                        } else if (instance2.onlyContainsAttributeCerts()) {
                                            throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlOnlyAttrCert"));
                                        }
                                    } catch (AnnotatedException e8) {
                                        throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlBCExtError"), e8);
                                    }
                                }
                            } catch (AnnotatedException e9) {
                                throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.deltaCrlExtError"));
                            }
                        } catch (AnnotatedException e10) {
                            throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.distrPtExtError"));
                        }
                    } catch (Exception e11) {
                        throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlVerifyFailed"), e11);
                    }
                } else {
                    throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlNoIssuerPublicKey"));
                }
            }
            if (!z) {
                throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.noValidCrlFound"));
            }
        } catch (IOException e12) {
            throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.crlIssuerException"), e12);
        }
    }

    /* access modifiers changed from: protected */
    public void checkRevocation(PKIXParameters pKIXParameters, X509Certificate x509Certificate, Date date, X509Certificate x509Certificate2, PublicKey publicKey, Vector vector, Vector vector2, int i) throws CertPathReviewerException {
        checkCRLs(pKIXParameters, x509Certificate, date, x509Certificate2, publicKey, vector, i);
    }

    /* access modifiers changed from: protected */
    public void doChecks() {
        if (!this.initialized) {
            throw new IllegalStateException("Object not initialized. Call init() first.");
        } else if (this.notifications == null) {
            this.notifications = new List[(this.n + 1)];
            this.errors = new List[(this.n + 1)];
            for (int i = 0; i < this.notifications.length; i++) {
                this.notifications[i] = new ArrayList();
                this.errors[i] = new ArrayList();
            }
            checkSignatures();
            checkNameConstraints();
            checkPathLength();
            checkPolicy();
            checkCriticalExtensions();
        }
    }

    /* access modifiers changed from: protected */
    public Vector getCRLDistUrls(CRLDistPoint cRLDistPoint) {
        Vector vector = new Vector();
        if (cRLDistPoint != null) {
            DistributionPoint[] distributionPoints = cRLDistPoint.getDistributionPoints();
            for (DistributionPoint distributionPoint : distributionPoints) {
                DistributionPointName distributionPoint2 = distributionPoint.getDistributionPoint();
                if (distributionPoint2.getType() == 0) {
                    GeneralName[] names = GeneralNames.getInstance(distributionPoint2.getName()).getNames();
                    for (int i = 0; i < names.length; i++) {
                        if (names[i].getTagNo() == 6) {
                            vector.add(((DERIA5String) names[i].getName()).getString());
                        }
                    }
                }
            }
        }
        return vector;
    }

    public CertPath getCertPath() {
        return this.certPath;
    }

    public int getCertPathSize() {
        return this.n;
    }

    public List getErrors(int i) {
        doChecks();
        return this.errors[i + 1];
    }

    public List[] getErrors() {
        doChecks();
        return this.errors;
    }

    public List getNotifications(int i) {
        doChecks();
        return this.notifications[i + 1];
    }

    public List[] getNotifications() {
        doChecks();
        return this.notifications;
    }

    /* access modifiers changed from: protected */
    public Vector getOCSPUrls(AuthorityInformationAccess authorityInformationAccess) {
        Vector vector = new Vector();
        if (authorityInformationAccess != null) {
            AccessDescription[] accessDescriptions = authorityInformationAccess.getAccessDescriptions();
            for (int i = 0; i < accessDescriptions.length; i++) {
                if (accessDescriptions[i].getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                    GeneralName accessLocation = accessDescriptions[i].getAccessLocation();
                    if (accessLocation.getTagNo() == 6) {
                        vector.add(((DERIA5String) accessLocation.getName()).getString());
                    }
                }
            }
        }
        return vector;
    }

    public PolicyNode getPolicyTree() {
        doChecks();
        return this.policyTree;
    }

    public PublicKey getSubjectPublicKey() {
        doChecks();
        return this.subjectPublicKey;
    }

    public TrustAnchor getTrustAnchor() {
        doChecks();
        return this.trustAnchor;
    }

    /* access modifiers changed from: protected */
    public Collection getTrustAnchors(X509Certificate x509Certificate, Set set) throws CertPathReviewerException {
        ArrayList arrayList = new ArrayList();
        Iterator it = set.iterator();
        X509CertSelector x509CertSelector = new X509CertSelector();
        try {
            x509CertSelector.setSubject(getEncodedIssuerPrincipal(x509Certificate).getEncoded());
            byte[] extensionValue = x509Certificate.getExtensionValue(X509Extensions.AuthorityKeyIdentifier.getId());
            if (extensionValue != null) {
                AuthorityKeyIdentifier instance = AuthorityKeyIdentifier.getInstance(ASN1Object.fromByteArray(((ASN1OctetString) ASN1Object.fromByteArray(extensionValue)).getOctets()));
                x509CertSelector.setSerialNumber(instance.getAuthorityCertSerialNumber());
                x509CertSelector.setSubjectKeyIdentifier(new DEROctetString(instance.getKeyIdentifier()).getEncoded());
            }
            while (it.hasNext()) {
                TrustAnchor trustAnchor2 = (TrustAnchor) it.next();
                if (trustAnchor2.getTrustedCert() != null) {
                    if (x509CertSelector.match(trustAnchor2.getTrustedCert())) {
                        arrayList.add(trustAnchor2);
                    }
                } else if (!(trustAnchor2.getCAName() == null || trustAnchor2.getCAPublicKey() == null || !getEncodedIssuerPrincipal(x509Certificate).equals(new X500Principal(trustAnchor2.getCAName())))) {
                    arrayList.add(trustAnchor2);
                }
            }
            return arrayList;
        } catch (IOException e) {
            throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.trustAnchorIssuerError"));
        }
    }

    public void init(CertPath certPath2, PKIXParameters pKIXParameters) throws CertPathReviewerException {
        if (this.initialized) {
            throw new IllegalStateException("object is already initialized!");
        }
        this.initialized = true;
        if (certPath2 == null) {
            throw new NullPointerException("certPath was null");
        }
        this.certPath = certPath2;
        this.certs = certPath2.getCertificates();
        this.n = this.certs.size();
        if (this.certs.isEmpty()) {
            throw new CertPathReviewerException(new ErrorBundle(RESOURCE_NAME, "CertPathReviewer.emptyCertPath"));
        }
        this.pkixParams = (PKIXParameters) pKIXParameters.clone();
        this.validDate = getValidDate(this.pkixParams);
        this.notifications = null;
        this.errors = null;
        this.trustAnchor = null;
        this.subjectPublicKey = null;
        this.policyTree = null;
    }

    public boolean isValidCertPath() {
        doChecks();
        for (List isEmpty : this.errors) {
            if (!isEmpty.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
