package org.bouncycastle.x509.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.Principal;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.x509.CertificatePair;
import org.bouncycastle.asn1.x509.X509CertificateStructure;
import org.bouncycastle.jce.X509LDAPCertStoreParameters;
import org.bouncycastle.jce.provider.X509AttrCertParser;
import org.bouncycastle.jce.provider.X509CRLParser;
import org.bouncycastle.jce.provider.X509CertPairParser;
import org.bouncycastle.jce.provider.X509CertParser;
import org.bouncycastle.util.StoreException;
import org.bouncycastle.x509.X509AttributeCertStoreSelector;
import org.bouncycastle.x509.X509AttributeCertificate;
import org.bouncycastle.x509.X509CRLStoreSelector;
import org.bouncycastle.x509.X509CertPairStoreSelector;
import org.bouncycastle.x509.X509CertStoreSelector;
import org.bouncycastle.x509.X509CertificatePair;

public class LDAPStoreHelper {
    private static String LDAP_PROVIDER = "com.sun.jndi.ldap.LdapCtxFactory";
    private static String REFERRALS_IGNORE = "ignore";
    private static final String SEARCH_SECURITY_LEVEL = "none";
    private static final String URL_CONTEXT_PREFIX = "com.sun.jndi.url";
    private static int cacheSize = 32;
    private static long lifeTime = 60000;
    private Map cacheMap = new HashMap(cacheSize);
    private X509LDAPCertStoreParameters params;

    public LDAPStoreHelper(X509LDAPCertStoreParameters x509LDAPCertStoreParameters) {
        this.params = x509LDAPCertStoreParameters;
    }

    private synchronized void addToCache(String str, List list) {
        Object obj;
        long j;
        Date date = new Date(System.currentTimeMillis());
        ArrayList arrayList = new ArrayList();
        arrayList.add(date);
        arrayList.add(list);
        if (this.cacheMap.containsKey(str)) {
            this.cacheMap.put(str, arrayList);
        } else {
            if (this.cacheMap.size() >= cacheSize) {
                Object obj2 = null;
                long time = date.getTime();
                for (Map.Entry entry : this.cacheMap.entrySet()) {
                    long time2 = ((Date) ((List) entry.getValue()).get(0)).getTime();
                    if (time2 < time) {
                        obj = entry.getKey();
                        j = time2;
                    } else {
                        obj = obj2;
                        j = time;
                    }
                    time = j;
                    obj2 = obj;
                }
                this.cacheMap.remove(obj2);
            }
            this.cacheMap.put(str, arrayList);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00df A[LOOP:1: B:32:0x00d9->B:34:0x00df, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0109  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List attrCertSubjectSerialSearch(org.bouncycastle.x509.X509AttributeCertStoreSelector r8, java.lang.String[] r9, java.lang.String[] r10, java.lang.String[] r11) throws org.bouncycastle.util.StoreException {
        /*
            r7 = this;
            r5 = 0
            r4 = 0
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.HashSet r2 = new java.util.HashSet
            r2.<init>()
            org.bouncycastle.x509.AttributeCertificateHolder r0 = r8.getHolder()
            if (r0 == 0) goto L_0x010c
            org.bouncycastle.x509.AttributeCertificateHolder r0 = r8.getHolder()
            java.math.BigInteger r0 = r0.getSerialNumber()
            if (r0 == 0) goto L_0x002b
            org.bouncycastle.x509.AttributeCertificateHolder r0 = r8.getHolder()
            java.math.BigInteger r0 = r0.getSerialNumber()
            java.lang.String r0 = r0.toString()
            r2.add(r0)
        L_0x002b:
            org.bouncycastle.x509.AttributeCertificateHolder r0 = r8.getHolder()
            java.security.Principal[] r0 = r0.getEntityNames()
            if (r0 == 0) goto L_0x010c
            org.bouncycastle.x509.AttributeCertificateHolder r0 = r8.getHolder()
            java.security.Principal[] r0 = r0.getEntityNames()
        L_0x003d:
            org.bouncycastle.x509.X509AttributeCertificate r3 = r8.getAttributeCert()
            if (r3 == 0) goto L_0x006c
            org.bouncycastle.x509.X509AttributeCertificate r3 = r8.getAttributeCert()
            org.bouncycastle.x509.AttributeCertificateHolder r3 = r3.getHolder()
            java.security.Principal[] r3 = r3.getEntityNames()
            if (r3 == 0) goto L_0x005d
            org.bouncycastle.x509.X509AttributeCertificate r0 = r8.getAttributeCert()
            org.bouncycastle.x509.AttributeCertificateHolder r0 = r0.getHolder()
            java.security.Principal[] r0 = r0.getEntityNames()
        L_0x005d:
            org.bouncycastle.x509.X509AttributeCertificate r3 = r8.getAttributeCert()
            java.math.BigInteger r3 = r3.getSerialNumber()
            java.lang.String r3 = r3.toString()
            r2.add(r3)
        L_0x006c:
            if (r0 == 0) goto L_0x0109
            r3 = r0[r4]
            boolean r3 = r3 instanceof javax.security.auth.x500.X500Principal
            if (r3 == 0) goto L_0x00bf
            r0 = r0[r4]
            javax.security.auth.x500.X500Principal r0 = (javax.security.auth.x500.X500Principal) r0
            java.lang.String r3 = "RFC1779"
            java.lang.String r0 = r0.getName(r3)
            r3 = r0
        L_0x007f:
            java.math.BigInteger r0 = r8.getSerialNumber()
            if (r0 == 0) goto L_0x0090
            java.math.BigInteger r0 = r8.getSerialNumber()
            java.lang.String r0 = r0.toString()
            r2.add(r0)
        L_0x0090:
            if (r3 == 0) goto L_0x00c7
            r0 = r4
        L_0x0093:
            int r4 = r11.length
            if (r0 >= r4) goto L_0x00c7
            r4 = r11[r0]
            java.lang.String r4 = r7.parseDN(r3, r4)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "*"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r4 = r5.append(r4)
            java.lang.String r5 = "*"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            java.util.List r4 = r7.search(r10, r4, r9)
            r1.addAll(r4)
            int r0 = r0 + 1
            goto L_0x0093
        L_0x00bf:
            r0 = r0[r4]
            java.lang.String r0 = r0.getName()
            r3 = r0
            goto L_0x007f
        L_0x00c7:
            int r0 = r2.size()
            if (r0 <= 0) goto L_0x00f7
            org.bouncycastle.jce.X509LDAPCertStoreParameters r0 = r7.params
            java.lang.String r0 = r0.getSearchForSerialNumberIn()
            if (r0 == 0) goto L_0x00f7
            java.util.Iterator r4 = r2.iterator()
        L_0x00d9:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00f7
            java.lang.Object r0 = r4.next()
            java.lang.String r0 = (java.lang.String) r0
            org.bouncycastle.jce.X509LDAPCertStoreParameters r5 = r7.params
            java.lang.String r5 = r5.getSearchForSerialNumberIn()
            java.lang.String[] r5 = r7.splitString(r5)
            java.util.List r0 = r7.search(r5, r0, r9)
            r1.addAll(r0)
            goto L_0x00d9
        L_0x00f7:
            int r0 = r2.size()
            if (r0 != 0) goto L_0x0108
            if (r3 != 0) goto L_0x0108
            java.lang.String r0 = "*"
            java.util.List r0 = r7.search(r10, r0, r9)
            r1.addAll(r0)
        L_0x0108:
            return r1
        L_0x0109:
            r3 = r5
            goto L_0x007f
        L_0x010c:
            r0 = r5
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.x509.util.LDAPStoreHelper.attrCertSubjectSerialSearch(org.bouncycastle.x509.X509AttributeCertStoreSelector, java.lang.String[], java.lang.String[], java.lang.String[]):java.util.List");
    }

    private List cRLIssuerSearch(X509CRLStoreSelector x509CRLStoreSelector, String[] strArr, String[] strArr2, String[] strArr3) throws StoreException {
        ArrayList arrayList = new ArrayList();
        String str = null;
        HashSet<X500Principal> hashSet = new HashSet<>();
        if (x509CRLStoreSelector.getIssuers() != null) {
            hashSet.addAll(x509CRLStoreSelector.getIssuers());
        }
        if (x509CRLStoreSelector.getCertificateChecking() != null) {
            hashSet.add(getCertificateIssuer(x509CRLStoreSelector.getCertificateChecking()));
        }
        if (x509CRLStoreSelector.getAttrCertificateChecking() != null) {
            Principal[] principals = x509CRLStoreSelector.getAttrCertificateChecking().getIssuer().getPrincipals();
            for (int i = 0; i < principals.length; i++) {
                if (principals[i] instanceof X500Principal) {
                    hashSet.add(principals[i]);
                }
            }
        }
        for (X500Principal name : hashSet) {
            str = name.getName("RFC1779");
            for (int i2 = 0; i2 < strArr3.length; i2++) {
                arrayList.addAll(search(strArr2, "*" + parseDN(str, strArr3[i2]) + "*", strArr));
            }
        }
        if (str == null) {
            arrayList.addAll(search(strArr2, "*", strArr));
        }
        return arrayList;
    }

    private List certSubjectSerialSearch(X509CertStoreSelector x509CertStoreSelector, String[] strArr, String[] strArr2, String[] strArr3) throws StoreException {
        ArrayList arrayList = new ArrayList();
        String str = null;
        String subjectAsString = getSubjectAsString(x509CertStoreSelector);
        if (x509CertStoreSelector.getSerialNumber() != null) {
            str = x509CertStoreSelector.getSerialNumber().toString();
        }
        if (x509CertStoreSelector.getCertificate() != null) {
            String name = x509CertStoreSelector.getCertificate().getSubjectX500Principal().getName("RFC1779");
            subjectAsString = name;
            str = x509CertStoreSelector.getCertificate().getSerialNumber().toString();
        }
        if (subjectAsString != null) {
            for (int i = 0; i < strArr3.length; i++) {
                arrayList.addAll(search(strArr2, "*" + parseDN(subjectAsString, strArr3[i]) + "*", strArr));
            }
        }
        if (!(str == null || this.params.getSearchForSerialNumberIn() == null)) {
            arrayList.addAll(search(splitString(this.params.getSearchForSerialNumberIn()), str, strArr));
        }
        if (str == null && subjectAsString == null) {
            arrayList.addAll(search(strArr2, "*", strArr));
        }
        return arrayList;
    }

    private DirContext connectLDAP() throws NamingException {
        Properties properties = new Properties();
        properties.setProperty("java.naming.factory.initial", LDAP_PROVIDER);
        properties.setProperty("java.naming.batchsize", "0");
        properties.setProperty("java.naming.provider.url", this.params.getLdapURL());
        properties.setProperty("java.naming.factory.url.pkgs", URL_CONTEXT_PREFIX);
        properties.setProperty("java.naming.referral", REFERRALS_IGNORE);
        properties.setProperty("java.naming.security.authentication", SEARCH_SECURITY_LEVEL);
        return new InitialDirContext(properties);
    }

    private Set createAttributeCertificates(List list, X509AttributeCertStoreSelector x509AttributeCertStoreSelector) throws StoreException {
        HashSet hashSet = new HashSet();
        Iterator it = list.iterator();
        X509AttrCertParser x509AttrCertParser = new X509AttrCertParser();
        while (it.hasNext()) {
            try {
                x509AttrCertParser.engineInit(new ByteArrayInputStream((byte[]) it.next()));
                X509AttributeCertificate x509AttributeCertificate = (X509AttributeCertificate) x509AttrCertParser.engineRead();
                if (x509AttributeCertStoreSelector.match(x509AttributeCertificate)) {
                    hashSet.add(x509AttributeCertificate);
                }
            } catch (StreamParsingException e) {
            }
        }
        return hashSet;
    }

    private Set createCRLs(List list, X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        HashSet hashSet = new HashSet();
        X509CRLParser x509CRLParser = new X509CRLParser();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            try {
                x509CRLParser.engineInit(new ByteArrayInputStream((byte[]) it.next()));
                X509CRL x509crl = (X509CRL) x509CRLParser.engineRead();
                if (x509CRLStoreSelector.match((Object) x509crl)) {
                    hashSet.add(x509crl);
                }
            } catch (StreamParsingException e) {
            }
        }
        return hashSet;
    }

    private Set createCerts(List list, X509CertStoreSelector x509CertStoreSelector) throws StoreException {
        HashSet hashSet = new HashSet();
        Iterator it = list.iterator();
        X509CertParser x509CertParser = new X509CertParser();
        while (it.hasNext()) {
            try {
                x509CertParser.engineInit(new ByteArrayInputStream((byte[]) it.next()));
                X509Certificate x509Certificate = (X509Certificate) x509CertParser.engineRead();
                if (x509CertStoreSelector.match((Object) x509Certificate)) {
                    hashSet.add(x509Certificate);
                }
            } catch (Exception e) {
            }
        }
        return hashSet;
    }

    private Set createCrossCertificatePairs(List list, X509CertPairStoreSelector x509CertPairStoreSelector) throws StoreException {
        X509CertificatePair x509CertificatePair;
        HashSet hashSet = new HashSet();
        int i = 0;
        while (i < list.size()) {
            try {
                X509CertPairParser x509CertPairParser = new X509CertPairParser();
                x509CertPairParser.engineInit(new ByteArrayInputStream((byte[]) list.get(i)));
                x509CertificatePair = (X509CertificatePair) x509CertPairParser.engineRead();
                try {
                    if (x509CertPairStoreSelector.match(x509CertificatePair)) {
                        hashSet.add(x509CertificatePair);
                    }
                } catch (IOException | CertificateParsingException e) {
                }
            } catch (StreamParsingException e2) {
                X509CertificatePair x509CertificatePair2 = new X509CertificatePair(new CertificatePair(X509CertificateStructure.getInstance(new ASN1InputStream((byte[]) list.get(i)).readObject()), X509CertificateStructure.getInstance(new ASN1InputStream((byte[]) list.get(i + 1)).readObject())));
                i++;
                x509CertificatePair = x509CertificatePair2;
            }
            i++;
        }
        return hashSet;
    }

    private List crossCertificatePairSubjectSearch(X509CertPairStoreSelector x509CertPairStoreSelector, String[] strArr, String[] strArr2, String[] strArr3) throws StoreException {
        ArrayList arrayList = new ArrayList();
        String str = null;
        if (x509CertPairStoreSelector.getForwardSelector() != null) {
            str = getSubjectAsString(x509CertPairStoreSelector.getForwardSelector());
        }
        if (!(x509CertPairStoreSelector.getCertPair() == null || x509CertPairStoreSelector.getCertPair().getForward() == null)) {
            str = x509CertPairStoreSelector.getCertPair().getForward().getSubjectX500Principal().getName("RFC1779");
        }
        if (str != null) {
            for (int i = 0; i < strArr3.length; i++) {
                arrayList.addAll(search(strArr2, "*" + parseDN(str, strArr3[i]) + "*", strArr));
            }
        }
        if (str == null) {
            arrayList.addAll(search(strArr2, "*", strArr));
        }
        return arrayList;
    }

    private X500Principal getCertificateIssuer(X509Certificate x509Certificate) {
        return x509Certificate.getIssuerX500Principal();
    }

    private List getFromCache(String str) {
        List list = (List) this.cacheMap.get(str);
        long currentTimeMillis = System.currentTimeMillis();
        if (list == null) {
            return null;
        }
        if (((Date) list.get(0)).getTime() < currentTimeMillis - lifeTime) {
            return null;
        }
        return (List) list.get(1);
    }

    private String getSubjectAsString(X509CertStoreSelector x509CertStoreSelector) {
        try {
            byte[] subjectAsBytes = x509CertStoreSelector.getSubjectAsBytes();
            if (subjectAsBytes != null) {
                return new X500Principal(subjectAsBytes).getName("RFC1779");
            }
            return null;
        } catch (IOException e) {
            throw new StoreException("exception processing name: " + e.getMessage(), e);
        }
    }

    private String parseDN(String str, String str2) {
        int i;
        int indexOf = str.toLowerCase().indexOf(str2.toLowerCase() + "=");
        if (indexOf == -1) {
            return "";
        }
        String substring = str.substring(indexOf + str2.length());
        int indexOf2 = substring.indexOf(44);
        if (indexOf2 == -1) {
            indexOf2 = substring.length();
        }
        while (substring.charAt(i - 1) == '\\') {
            i = substring.indexOf(44, i + 1);
            if (i == -1) {
                i = substring.length();
            }
        }
        String substring2 = substring.substring(0, i);
        String substring3 = substring2.substring(substring2.indexOf(61) + 1);
        if (substring3.charAt(0) == ' ') {
            substring3 = substring3.substring(1);
        }
        if (substring3.startsWith("\"")) {
            substring3 = substring3.substring(1);
        }
        return substring3.endsWith("\"") ? substring3.substring(0, substring3.length() - 1) : substring3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x0132 A[SYNTHETIC, Splitter:B:45:0x0132] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List search(java.lang.String[] r7, java.lang.String r8, java.lang.String[] r9) throws org.bouncycastle.util.StoreException {
        /*
            r6 = this;
            r5 = 0
            r4 = 0
            if (r7 != 0) goto L_0x002e
            r0 = r5
        L_0x0005:
            java.lang.String r1 = ""
            r2 = r1
            r1 = r4
        L_0x0009:
            int r3 = r9.length
            if (r1 >= r3) goto L_0x0085
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "("
            java.lang.StringBuilder r2 = r2.append(r3)
            r3 = r9[r1]
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "=*)"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            int r1 = r1 + 1
            goto L_0x0009
        L_0x002e:
            java.lang.String r0 = ""
            java.lang.String r1 = "**"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x0142
            java.lang.String r1 = "*"
        L_0x003a:
            r2 = r0
            r0 = r4
        L_0x003c:
            int r3 = r7.length
            if (r0 >= r3) goto L_0x006b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = "("
            java.lang.StringBuilder r2 = r2.append(r3)
            r3 = r7[r0]
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "="
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r3 = ")"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            int r0 = r0 + 1
            goto L_0x003c
        L_0x006b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "(|"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = ")"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            goto L_0x0005
        L_0x0085:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "(|"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ")"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "(&"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r3 = ")"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            if (r0 != 0) goto L_0x0140
        L_0x00c3:
            java.util.List r0 = r6.getFromCache(r1)
            if (r0 == 0) goto L_0x00ca
        L_0x00c9:
            return r0
        L_0x00ca:
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            javax.naming.directory.DirContext r3 = r6.connectLDAP()     // Catch:{ NamingException -> 0x013d, all -> 0x012e }
            javax.naming.directory.SearchControls r0 = new javax.naming.directory.SearchControls     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            r0.<init>()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            r4 = 2
            r0.setSearchScope(r4)     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            r4 = 0
            r0.setCountLimit(r4)     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            r0.setReturningAttributes(r9)     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            org.bouncycastle.jce.X509LDAPCertStoreParameters r4 = r6.params     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            java.lang.String r4 = r4.getBaseDN()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            javax.naming.NamingEnumeration r4 = r3.search(r4, r1, r0)     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
        L_0x00ee:
            boolean r0 = r4.hasMoreElements()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            if (r0 == 0) goto L_0x0123
            java.lang.Object r0 = r4.next()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            javax.naming.directory.SearchResult r0 = (javax.naming.directory.SearchResult) r0     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            javax.naming.directory.Attributes r0 = r0.getAttributes()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            javax.naming.NamingEnumeration r0 = r0.getAll()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            java.lang.Object r0 = r0.next()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            javax.naming.directory.Attribute r0 = (javax.naming.directory.Attribute) r0     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            javax.naming.NamingEnumeration r0 = r0.getAll()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
        L_0x010c:
            boolean r5 = r0.hasMore()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            if (r5 == 0) goto L_0x00ee
            java.lang.Object r5 = r0.next()     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            r2.add(r5)     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            goto L_0x010c
        L_0x011a:
            r0 = move-exception
            r0 = r3
        L_0x011c:
            if (r0 == 0) goto L_0x0121
            r0.close()     // Catch:{ Exception -> 0x0136 }
        L_0x0121:
            r0 = r2
            goto L_0x00c9
        L_0x0123:
            r6.addToCache(r1, r2)     // Catch:{ NamingException -> 0x011a, all -> 0x013a }
            if (r3 == 0) goto L_0x0121
            r3.close()     // Catch:{ Exception -> 0x012c }
            goto L_0x0121
        L_0x012c:
            r0 = move-exception
            goto L_0x0121
        L_0x012e:
            r0 = move-exception
            r1 = r5
        L_0x0130:
            if (r1 == 0) goto L_0x0135
            r1.close()     // Catch:{ Exception -> 0x0138 }
        L_0x0135:
            throw r0
        L_0x0136:
            r0 = move-exception
            goto L_0x0121
        L_0x0138:
            r1 = move-exception
            goto L_0x0135
        L_0x013a:
            r0 = move-exception
            r1 = r3
            goto L_0x0130
        L_0x013d:
            r0 = move-exception
            r0 = r5
            goto L_0x011c
        L_0x0140:
            r1 = r2
            goto L_0x00c3
        L_0x0142:
            r1 = r8
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.x509.util.LDAPStoreHelper.search(java.lang.String[], java.lang.String, java.lang.String[]):java.util.List");
    }

    private String[] splitString(String str) {
        return str.split("\\s+");
    }

    public Collection getAACertificates(X509AttributeCertStoreSelector x509AttributeCertStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getAACertificateAttribute());
        String[] splitString2 = splitString(this.params.getLdapAACertificateAttributeName());
        String[] splitString3 = splitString(this.params.getAACertificateSubjectAttributeName());
        Set createAttributeCertificates = createAttributeCertificates(attrCertSubjectSerialSearch(x509AttributeCertStoreSelector, splitString, splitString2, splitString3), x509AttributeCertStoreSelector);
        if (createAttributeCertificates.size() == 0) {
            createAttributeCertificates.addAll(createAttributeCertificates(attrCertSubjectSerialSearch(new X509AttributeCertStoreSelector(), splitString, splitString2, splitString3), x509AttributeCertStoreSelector));
        }
        return createAttributeCertificates;
    }

    public Collection getAttributeAuthorityRevocationLists(X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getAttributeAuthorityRevocationListAttribute());
        String[] splitString2 = splitString(this.params.getLdapAttributeAuthorityRevocationListAttributeName());
        String[] splitString3 = splitString(this.params.getAttributeAuthorityRevocationListIssuerAttributeName());
        Set createCRLs = createCRLs(cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (createCRLs.size() == 0) {
            createCRLs.addAll(createCRLs(cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return createCRLs;
    }

    public Collection getAttributeCertificateAttributes(X509AttributeCertStoreSelector x509AttributeCertStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getAttributeCertificateAttributeAttribute());
        String[] splitString2 = splitString(this.params.getLdapAttributeCertificateAttributeAttributeName());
        String[] splitString3 = splitString(this.params.getAttributeCertificateAttributeSubjectAttributeName());
        Set createAttributeCertificates = createAttributeCertificates(attrCertSubjectSerialSearch(x509AttributeCertStoreSelector, splitString, splitString2, splitString3), x509AttributeCertStoreSelector);
        if (createAttributeCertificates.size() == 0) {
            createAttributeCertificates.addAll(createAttributeCertificates(attrCertSubjectSerialSearch(new X509AttributeCertStoreSelector(), splitString, splitString2, splitString3), x509AttributeCertStoreSelector));
        }
        return createAttributeCertificates;
    }

    public Collection getAttributeCertificateRevocationLists(X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getAttributeCertificateRevocationListAttribute());
        String[] splitString2 = splitString(this.params.getLdapAttributeCertificateRevocationListAttributeName());
        String[] splitString3 = splitString(this.params.getAttributeCertificateRevocationListIssuerAttributeName());
        Set createCRLs = createCRLs(cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (createCRLs.size() == 0) {
            createCRLs.addAll(createCRLs(cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return createCRLs;
    }

    public Collection getAttributeDescriptorCertificates(X509AttributeCertStoreSelector x509AttributeCertStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getAttributeDescriptorCertificateAttribute());
        String[] splitString2 = splitString(this.params.getLdapAttributeDescriptorCertificateAttributeName());
        String[] splitString3 = splitString(this.params.getAttributeDescriptorCertificateSubjectAttributeName());
        Set createAttributeCertificates = createAttributeCertificates(attrCertSubjectSerialSearch(x509AttributeCertStoreSelector, splitString, splitString2, splitString3), x509AttributeCertStoreSelector);
        if (createAttributeCertificates.size() == 0) {
            createAttributeCertificates.addAll(createAttributeCertificates(attrCertSubjectSerialSearch(new X509AttributeCertStoreSelector(), splitString, splitString2, splitString3), x509AttributeCertStoreSelector));
        }
        return createAttributeCertificates;
    }

    public Collection getAuthorityRevocationLists(X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getAuthorityRevocationListAttribute());
        String[] splitString2 = splitString(this.params.getLdapAuthorityRevocationListAttributeName());
        String[] splitString3 = splitString(this.params.getAuthorityRevocationListIssuerAttributeName());
        Set createCRLs = createCRLs(cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (createCRLs.size() == 0) {
            createCRLs.addAll(createCRLs(cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return createCRLs;
    }

    public Collection getCACertificates(X509CertStoreSelector x509CertStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getCACertificateAttribute());
        String[] splitString2 = splitString(this.params.getLdapCACertificateAttributeName());
        String[] splitString3 = splitString(this.params.getCACertificateSubjectAttributeName());
        Set createCerts = createCerts(certSubjectSerialSearch(x509CertStoreSelector, splitString, splitString2, splitString3), x509CertStoreSelector);
        if (createCerts.size() == 0) {
            createCerts.addAll(createCerts(certSubjectSerialSearch(new X509CertStoreSelector(), splitString, splitString2, splitString3), x509CertStoreSelector));
        }
        return createCerts;
    }

    public Collection getCertificateRevocationLists(X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getCertificateRevocationListAttribute());
        String[] splitString2 = splitString(this.params.getLdapCertificateRevocationListAttributeName());
        String[] splitString3 = splitString(this.params.getCertificateRevocationListIssuerAttributeName());
        Set createCRLs = createCRLs(cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (createCRLs.size() == 0) {
            createCRLs.addAll(createCRLs(cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return createCRLs;
    }

    public Collection getCrossCertificatePairs(X509CertPairStoreSelector x509CertPairStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getCrossCertificateAttribute());
        String[] splitString2 = splitString(this.params.getLdapCrossCertificateAttributeName());
        String[] splitString3 = splitString(this.params.getCrossCertificateSubjectAttributeName());
        Set createCrossCertificatePairs = createCrossCertificatePairs(crossCertificatePairSubjectSearch(x509CertPairStoreSelector, splitString, splitString2, splitString3), x509CertPairStoreSelector);
        if (createCrossCertificatePairs.size() == 0) {
            X509CertStoreSelector x509CertStoreSelector = new X509CertStoreSelector();
            X509CertPairStoreSelector x509CertPairStoreSelector2 = new X509CertPairStoreSelector();
            x509CertPairStoreSelector2.setForwardSelector(x509CertStoreSelector);
            x509CertPairStoreSelector2.setReverseSelector(x509CertStoreSelector);
            createCrossCertificatePairs.addAll(createCrossCertificatePairs(crossCertificatePairSubjectSearch(x509CertPairStoreSelector2, splitString, splitString2, splitString3), x509CertPairStoreSelector));
        }
        return createCrossCertificatePairs;
    }

    public Collection getDeltaCertificateRevocationLists(X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getDeltaRevocationListAttribute());
        String[] splitString2 = splitString(this.params.getLdapDeltaRevocationListAttributeName());
        String[] splitString3 = splitString(this.params.getDeltaRevocationListIssuerAttributeName());
        Set createCRLs = createCRLs(cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (createCRLs.size() == 0) {
            createCRLs.addAll(createCRLs(cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return createCRLs;
    }

    public Collection getUserCertificates(X509CertStoreSelector x509CertStoreSelector) throws StoreException {
        String[] splitString = splitString(this.params.getUserCertificateAttribute());
        String[] splitString2 = splitString(this.params.getLdapUserCertificateAttributeName());
        String[] splitString3 = splitString(this.params.getUserCertificateSubjectAttributeName());
        Set createCerts = createCerts(certSubjectSerialSearch(x509CertStoreSelector, splitString, splitString2, splitString3), x509CertStoreSelector);
        if (createCerts.size() == 0) {
            createCerts.addAll(createCerts(certSubjectSerialSearch(new X509CertStoreSelector(), splitString, splitString2, splitString3), x509CertStoreSelector));
        }
        return createCerts;
    }
}
