package org.bouncycastle.x509.extension;

import java.io.IOException;
import java.security.PublicKey;
import java.security.cert.CertificateParsingException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;

public class SubjectKeyIdentifierStructure extends SubjectKeyIdentifier {
    private AuthorityKeyIdentifier authKeyID;

    public SubjectKeyIdentifierStructure(PublicKey publicKey) throws CertificateParsingException {
        super(fromPublicKey(publicKey));
    }

    public SubjectKeyIdentifierStructure(byte[] bArr) throws IOException {
        super((ASN1OctetString) X509ExtensionUtil.fromExtensionValue(bArr));
    }

    private static ASN1OctetString fromPublicKey(PublicKey publicKey) throws CertificateParsingException {
        try {
            return (ASN1OctetString) new SubjectKeyIdentifier(new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(publicKey.getEncoded()).readObject())).toASN1Object();
        } catch (Exception e) {
            throw new CertificateParsingException("Exception extracting certificate details: " + e.toString());
        }
    }
}
