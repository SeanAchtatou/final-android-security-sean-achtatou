package org.bouncycastle.util;

import java.io.ByteArrayOutputStream;
import java.util.Vector;

public final class Strings {
    public static String fromUTF8ByteArray(byte[] bArr) {
        char c;
        int i;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i4 < bArr.length) {
            i3++;
            if ((bArr[i4] & 240) == 240) {
                i3++;
                i4 += 4;
            } else {
                i4 = (bArr[i4] & 224) == 224 ? i4 + 3 : (bArr[i4] & 192) == 192 ? i4 + 2 : i4 + 1;
            }
        }
        char[] cArr = new char[i3];
        int i5 = 0;
        while (true) {
            int i6 = i2;
            if (i6 >= bArr.length) {
                return new String(cArr);
            }
            if ((bArr[i6] & 240) == 240) {
                int i7 = (((((bArr[i6] & 3) << 18) | ((bArr[i6 + 1] & 63) << 12)) | ((bArr[i6 + 2] & 63) << 6)) | (bArr[i6 + 3] & 63)) - 65536;
                int i8 = i5 + 1;
                cArr[i5] = (char) (55296 | (i7 >> 10));
                int i9 = i6 + 4;
                i = i8;
                c = (char) ((i7 & 1023) | 56320);
                i2 = i9;
            } else if ((bArr[i6] & 224) == 224) {
                c = (char) (((bArr[i6] & 15) << 12) | ((bArr[i6 + 1] & 63) << 6) | (bArr[i6 + 2] & 63));
                i2 = i6 + 3;
                i = i5;
            } else if ((bArr[i6] & 208) == 208) {
                c = (char) (((bArr[i6] & 31) << 6) | (bArr[i6 + 1] & 63));
                i2 = i6 + 2;
                i = i5;
            } else if ((bArr[i6] & 192) == 192) {
                c = (char) (((bArr[i6] & 31) << 6) | (bArr[i6 + 1] & 63));
                i2 = i6 + 2;
                i = i5;
            } else {
                c = (char) (bArr[i6] & 255);
                i2 = i6 + 1;
                i = i5;
            }
            cArr[i] = c;
            i5 = i + 1;
        }
    }

    public static String[] split(String str, char c) {
        Vector vector = new Vector();
        boolean z = true;
        String str2 = str;
        while (z) {
            int indexOf = str2.indexOf(c);
            if (indexOf > 0) {
                vector.addElement(str2.substring(0, indexOf));
                str2 = str2.substring(indexOf + 1);
            } else {
                vector.addElement(str2);
                z = false;
            }
        }
        String[] strArr = new String[vector.size()];
        for (int i = 0; i != strArr.length; i++) {
            strArr[i] = (String) vector.elementAt(i);
        }
        return strArr;
    }

    public static byte[] toByteArray(String str) {
        byte[] bArr = new byte[str.length()];
        for (int i = 0; i != bArr.length; i++) {
            bArr[i] = (byte) str.charAt(i);
        }
        return bArr;
    }

    public static String toLowerCase(String str) {
        char[] charArray = str.toCharArray();
        boolean z = false;
        for (int i = 0; i != charArray.length; i++) {
            char c = charArray[i];
            if ('A' <= c && 'Z' >= c) {
                z = true;
                charArray[i] = (char) ((c - 'A') + 97);
            }
        }
        return z ? new String(charArray) : str;
    }

    public static byte[] toUTF8ByteArray(String str) {
        return toUTF8ByteArray(str.toCharArray());
    }

    public static byte[] toUTF8ByteArray(char[] cArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        while (i < cArr.length) {
            char c = cArr[i];
            if (c < 128) {
                byteArrayOutputStream.write(c);
            } else if (c < 2048) {
                byteArrayOutputStream.write((c >> 6) | 192);
                byteArrayOutputStream.write((c & '?') | 128);
            } else if (c < 55296 || c > 57343) {
                byteArrayOutputStream.write((c >> 12) | 224);
                byteArrayOutputStream.write(((c >> 6) & 63) | 128);
                byteArrayOutputStream.write((c & '?') | 128);
            } else if (i + 1 >= cArr.length) {
                throw new IllegalStateException("invalid UTF-16 codepoint");
            } else {
                i++;
                char c2 = cArr[i];
                if (c > 56319) {
                    throw new IllegalStateException("invalid UTF-16 codepoint");
                }
                int i2 = (((c & 1023) << 10) | (c2 & 1023)) + 0;
                byteArrayOutputStream.write((i2 >> 18) | 240);
                byteArrayOutputStream.write(((i2 >> 12) & 63) | 128);
                byteArrayOutputStream.write(((i2 >> 6) & 63) | 128);
                byteArrayOutputStream.write((i2 & 63) | 128);
            }
            i++;
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static String toUpperCase(String str) {
        char[] charArray = str.toCharArray();
        boolean z = false;
        for (int i = 0; i != charArray.length; i++) {
            char c = charArray[i];
            if ('a' <= c && 'z' >= c) {
                z = true;
                charArray[i] = (char) ((c - 'a') + 65);
            }
        }
        return z ? new String(charArray) : str;
    }
}
