package org.bouncycastle.util.encoders;

public class HexTranslator implements Translator {
    private static final byte[] hexTable = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    public int decode(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        int i4 = i2 / 2;
        int i5 = i3;
        for (int i6 = 0; i6 < i4; i6++) {
            byte b = bArr[(i6 * 2) + i];
            byte b2 = bArr[(i6 * 2) + i + 1];
            if (b < 97) {
                bArr2[i5] = (byte) ((b - 48) << 4);
            } else {
                bArr2[i5] = (byte) (((b - 97) + 10) << 4);
            }
            if (b2 < 97) {
                bArr2[i5] = (byte) (bArr2[i5] + ((byte) (b2 - 48)));
            } else {
                bArr2[i5] = (byte) (bArr2[i5] + ((byte) ((b2 - 97) + 10)));
            }
            i5++;
        }
        return i4;
    }

    public int encode(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        int i4 = 0;
        int i5 = 0;
        int i6 = i;
        while (i5 < i2) {
            bArr2[i3 + i4] = hexTable[(bArr[i6] >> 4) & 15];
            bArr2[i3 + i4 + 1] = hexTable[bArr[i6] & 15];
            i6++;
            i5++;
            i4 += 2;
        }
        return i2 * 2;
    }

    public int getDecodedBlockSize() {
        return 1;
    }

    public int getEncodedBlockSize() {
        return 2;
    }
}
