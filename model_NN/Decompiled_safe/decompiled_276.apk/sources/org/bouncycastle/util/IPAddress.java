package org.bouncycastle.util;

public class IPAddress {
    private static boolean isMaskValue(String str, int i) {
        try {
            int parseInt = Integer.parseInt(str);
            return parseInt >= 0 && parseInt <= i;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isValid(String str) {
        return isValidIPv4(str) || isValidIPv6(str);
    }

    public static boolean isValidIPv4(String str) {
        int indexOf;
        if (str.length() == 0) {
            return false;
        }
        String str2 = str + ".";
        int i = 0;
        int i2 = 0;
        while (i < str2.length() && (indexOf = str2.indexOf(46, i)) > i) {
            if (i2 == 4) {
                return false;
            }
            try {
                int parseInt = Integer.parseInt(str2.substring(i, indexOf));
                if (parseInt < 0 || parseInt > 255) {
                    return false;
                }
                i = indexOf + 1;
                i2++;
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return i2 == 4;
    }

    public static boolean isValidIPv4WithNetmask(String str) {
        int indexOf = str.indexOf("/");
        String substring = str.substring(indexOf + 1);
        return indexOf > 0 && isValidIPv4(str.substring(0, indexOf)) && (isValidIPv4(substring) || isMaskValue(substring, 32));
    }

    public static boolean isValidIPv6(String str) {
        int indexOf;
        boolean z;
        int i;
        int i2;
        if (str.length() == 0) {
            return false;
        }
        String str2 = str + ":";
        int i3 = 0;
        boolean z2 = false;
        int i4 = 0;
        while (i3 < str2.length() && (indexOf = str2.indexOf(58, i3)) >= i3) {
            if (i4 == 8) {
                return false;
            }
            if (i3 != indexOf) {
                String substring = str2.substring(i3, indexOf);
                if (indexOf != str2.length() - 1 || substring.indexOf(46) <= 0) {
                    try {
                        int parseInt = Integer.parseInt(str2.substring(i3, indexOf), 16);
                        if (parseInt < 0 || parseInt > 65535) {
                            return false;
                        }
                        i2 = i4;
                    } catch (NumberFormatException e) {
                        return false;
                    }
                } else if (!isValidIPv4(substring)) {
                    return false;
                } else {
                    i2 = i4 + 1;
                }
                boolean z3 = z2;
                i = i2;
                z = z3;
            } else if (indexOf != 1 && indexOf != str2.length() - 1 && z2) {
                return false;
            } else {
                z = true;
                i = i4;
            }
            i4 = i + 1;
            z2 = z;
            i3 = indexOf + 1;
        }
        return i4 == 8 || z2;
    }

    public static boolean isValidIPv6WithNetmask(String str) {
        int indexOf = str.indexOf("/");
        String substring = str.substring(indexOf + 1);
        return indexOf > 0 && isValidIPv6(str.substring(0, indexOf)) && (isValidIPv6(substring) || isMaskValue(substring, 128));
    }

    public static boolean isValidWithNetMask(String str) {
        return isValidIPv4WithNetmask(str) || isValidIPv6WithNetmask(str);
    }
}
