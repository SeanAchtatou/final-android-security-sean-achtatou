package org.bouncycastle.math.ec;

import java.math.BigInteger;

class WNafMultiplier implements ECMultiplier {
    WNafMultiplier() {
    }

    public ECPoint multiply(ECPoint eCPoint, BigInteger bigInteger, PreCompInfo preCompInfo) {
        int i;
        byte b;
        int length;
        ECPoint[] eCPointArr;
        WNafPreCompInfo wNafPreCompInfo = (preCompInfo == null || !(preCompInfo instanceof WNafPreCompInfo)) ? new WNafPreCompInfo() : (WNafPreCompInfo) preCompInfo;
        int bitLength = bigInteger.bitLength();
        if (bitLength < 13) {
            i = 1;
            b = 2;
        } else if (bitLength < 41) {
            b = 3;
            i = 2;
        } else if (bitLength < 121) {
            i = 4;
            b = 4;
        } else if (bitLength < 337) {
            b = 5;
            i = 8;
        } else if (bitLength < 897) {
            b = 6;
            i = 16;
        } else if (bitLength < 2305) {
            b = 7;
            i = 32;
        } else {
            i = 127;
            b = 8;
        }
        ECPoint[] preComp = wNafPreCompInfo.getPreComp();
        ECPoint twiceP = wNafPreCompInfo.getTwiceP();
        if (preComp == null) {
            preComp = new ECPoint[]{eCPoint};
            length = 1;
        } else {
            length = preComp.length;
        }
        if (twiceP == null) {
            twiceP = eCPoint.twice();
        }
        if (length < i) {
            ECPoint[] eCPointArr2 = new ECPoint[i];
            System.arraycopy(preComp, 0, eCPointArr2, 0, length);
            for (int i2 = length; i2 < i; i2++) {
                eCPointArr2[i2] = twiceP.add(eCPointArr2[i2 - 1]);
            }
            eCPointArr = eCPointArr2;
        } else {
            eCPointArr = preComp;
        }
        byte[] windowNaf = windowNaf(b, bigInteger);
        int length2 = windowNaf.length;
        ECPoint infinity = eCPoint.getCurve().getInfinity();
        for (int i3 = length2 - 1; i3 >= 0; i3--) {
            infinity = infinity.twice();
            if (windowNaf[i3] != 0) {
                infinity = windowNaf[i3] > 0 ? infinity.add(eCPointArr[(windowNaf[i3] - 1) / 2]) : infinity.subtract(eCPointArr[((-windowNaf[i3]) - 1) / 2]);
            }
        }
        wNafPreCompInfo.setPreComp(eCPointArr);
        wNafPreCompInfo.setTwiceP(twiceP);
        eCPoint.setPreCompInfo(wNafPreCompInfo);
        return infinity;
    }

    public byte[] windowNaf(byte b, BigInteger bigInteger) {
        byte[] bArr = new byte[(bigInteger.bitLength() + 1)];
        short s = (short) (1 << b);
        BigInteger valueOf = BigInteger.valueOf((long) s);
        int i = 0;
        int i2 = 0;
        BigInteger bigInteger2 = bigInteger;
        while (bigInteger2.signum() > 0) {
            if (bigInteger2.testBit(0)) {
                BigInteger mod = bigInteger2.mod(valueOf);
                if (mod.testBit(b - 1)) {
                    bArr[i2] = (byte) (mod.intValue() - s);
                } else {
                    bArr[i2] = (byte) mod.intValue();
                }
                bigInteger2 = bigInteger2.subtract(BigInteger.valueOf((long) bArr[i2]));
                i = i2;
            } else {
                bArr[i2] = 0;
            }
            bigInteger2 = bigInteger2.shiftRight(1);
            i2++;
        }
        int i3 = i + 1;
        byte[] bArr2 = new byte[i3];
        System.arraycopy(bArr, 0, bArr2, 0, i3);
        return bArr2;
    }
}
