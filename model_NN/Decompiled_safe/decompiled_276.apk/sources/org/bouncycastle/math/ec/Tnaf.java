package org.bouncycastle.math.ec;

import java.math.BigInteger;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;

class Tnaf {
    private static final BigInteger MINUS_ONE = ECConstants.ONE.negate();
    private static final BigInteger MINUS_THREE = ECConstants.THREE.negate();
    private static final BigInteger MINUS_TWO = ECConstants.TWO.negate();
    public static final byte POW_2_WIDTH = 16;
    public static final byte WIDTH = 4;
    public static final ZTauElement[] alpha0 = {null, new ZTauElement(ECConstants.ONE, ECConstants.ZERO), null, new ZTauElement(MINUS_THREE, MINUS_ONE), null, new ZTauElement(MINUS_ONE, MINUS_ONE), null, new ZTauElement(ECConstants.ONE, MINUS_ONE), null};
    public static final byte[][] alpha0Tnaf = {null, new byte[]{1}, null, new byte[]{-1, 0, 1}, null, new byte[]{1, 0, 1}, null, new byte[]{-1, 0, 0, 1}};
    public static final ZTauElement[] alpha1 = {null, new ZTauElement(ECConstants.ONE, ECConstants.ZERO), null, new ZTauElement(MINUS_THREE, ECConstants.ONE), null, new ZTauElement(MINUS_ONE, ECConstants.ONE), null, new ZTauElement(ECConstants.ONE, ECConstants.ONE), null};
    public static final byte[][] alpha1Tnaf = {null, new byte[]{1}, null, new byte[]{-1, 0, 1}, null, new byte[]{1, 0, 1}, null, new byte[]{-1, 0, 0, -1}};

    Tnaf() {
    }

    public static SimpleBigDecimal approximateDivisionByN(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, byte b, int i, int i2) {
        int i3 = ((i + 5) / 2) + i2;
        BigInteger multiply = bigInteger2.multiply(bigInteger.shiftRight(((i - i3) - 2) + b));
        BigInteger add = multiply.add(bigInteger3.multiply(multiply.shiftRight(i)));
        BigInteger shiftRight = add.shiftRight(i3 - i2);
        return new SimpleBigDecimal(add.testBit((i3 - i2) - 1) ? shiftRight.add(ECConstants.ONE) : shiftRight, i2);
    }

    public static BigInteger[] getLucas(byte b, int i, boolean z) {
        BigInteger bigInteger;
        BigInteger bigInteger2;
        if (b == 1 || b == -1) {
            if (z) {
                bigInteger = ECConstants.TWO;
                bigInteger2 = BigInteger.valueOf((long) b);
            } else {
                bigInteger = ECConstants.ZERO;
                bigInteger2 = ECConstants.ONE;
            }
            BigInteger bigInteger3 = bigInteger;
            BigInteger bigInteger4 = bigInteger2;
            int i2 = 1;
            while (i2 < i) {
                i2++;
                BigInteger subtract = (b == 1 ? bigInteger4 : bigInteger4.negate()).subtract(bigInteger3.shiftLeft(1));
                bigInteger3 = bigInteger4;
                bigInteger4 = subtract;
            }
            return new BigInteger[]{bigInteger3, bigInteger4};
        }
        throw new IllegalArgumentException("mu must be 1 or -1");
    }

    public static byte getMu(ECCurve.F2m f2m) {
        BigInteger bigInteger = f2m.getA().toBigInteger();
        if (bigInteger.equals(ECConstants.ZERO)) {
            return -1;
        }
        if (bigInteger.equals(ECConstants.ONE)) {
            return 1;
        }
        throw new IllegalArgumentException("No Koblitz curve (ABC), TNAF multiplication not possible");
    }

    public static ECPoint.F2m[] getPreComp(ECPoint.F2m f2m, byte b) {
        ECPoint.F2m[] f2mArr = new ECPoint.F2m[16];
        f2mArr[1] = f2m;
        byte[][] bArr = b == 0 ? alpha0Tnaf : alpha1Tnaf;
        int length = bArr.length;
        for (int i = 3; i < length; i += 2) {
            f2mArr[i] = multiplyFromTnaf(f2m, bArr[i]);
        }
        return f2mArr;
    }

    public static BigInteger[] getSi(ECCurve.F2m f2m) {
        BigInteger add;
        BigInteger add2;
        if (!f2m.isKoblitz()) {
            throw new IllegalArgumentException("si is defined for Koblitz curves only");
        }
        int m = f2m.getM();
        int intValue = f2m.getA().toBigInteger().intValue();
        byte mu = f2m.getMu();
        int intValue2 = f2m.getH().intValue();
        BigInteger[] lucas = getLucas(mu, (m + 3) - intValue, false);
        if (mu == 1) {
            add = ECConstants.ONE.subtract(lucas[1]);
            add2 = ECConstants.ONE.subtract(lucas[0]);
        } else if (mu == -1) {
            add = ECConstants.ONE.add(lucas[1]);
            add2 = ECConstants.ONE.add(lucas[0]);
        } else {
            throw new IllegalArgumentException("mu must be 1 or -1");
        }
        BigInteger[] bigIntegerArr = new BigInteger[2];
        if (intValue2 == 2) {
            bigIntegerArr[0] = add.shiftRight(1);
            bigIntegerArr[1] = add2.shiftRight(1).negate();
        } else if (intValue2 == 4) {
            bigIntegerArr[0] = add.shiftRight(2);
            bigIntegerArr[1] = add2.shiftRight(2).negate();
        } else {
            throw new IllegalArgumentException("h (Cofactor) must be 2 or 4");
        }
        return bigIntegerArr;
    }

    public static BigInteger getTw(byte b, int i) {
        if (i == 4) {
            return b == 1 ? BigInteger.valueOf(6) : BigInteger.valueOf(10);
        }
        BigInteger[] lucas = getLucas(b, i, false);
        BigInteger bit = ECConstants.ZERO.setBit(i);
        return ECConstants.TWO.multiply(lucas[0]).multiply(lucas[1].modInverse(bit)).mod(bit);
    }

    public static ECPoint.F2m multiplyFromTnaf(ECPoint.F2m f2m, byte[] bArr) {
        int length = bArr.length - 1;
        ECPoint.F2m f2m2 = (ECPoint.F2m) ((ECCurve.F2m) f2m.getCurve()).getInfinity();
        for (int i = length; i >= 0; i--) {
            f2m2 = tau(f2m2);
            if (bArr[i] == 1) {
                f2m2 = f2m2.addSimple(f2m);
            } else if (bArr[i] == -1) {
                f2m2 = f2m2.subtractSimple(f2m);
            }
        }
        return f2m2;
    }

    public static ECPoint.F2m multiplyRTnaf(ECPoint.F2m f2m, BigInteger bigInteger) {
        ECCurve.F2m f2m2 = (ECCurve.F2m) f2m.getCurve();
        byte mu = f2m2.getMu();
        BigInteger[] si = f2m2.getSi();
        return multiplyTnaf(f2m, partModReduction(bigInteger, f2m2.getM(), (byte) f2m2.getA().toBigInteger().intValue(), si, mu, (byte) 10));
    }

    public static ECPoint.F2m multiplyTnaf(ECPoint.F2m f2m, ZTauElement zTauElement) {
        return multiplyFromTnaf(f2m, tauAdicNaf(((ECCurve.F2m) f2m.getCurve()).getMu(), zTauElement));
    }

    public static BigInteger norm(byte b, ZTauElement zTauElement) {
        BigInteger multiply = zTauElement.u.multiply(zTauElement.u);
        BigInteger multiply2 = zTauElement.u.multiply(zTauElement.v);
        BigInteger shiftLeft = zTauElement.v.multiply(zTauElement.v).shiftLeft(1);
        if (b == 1) {
            return multiply.add(multiply2).add(shiftLeft);
        }
        if (b == -1) {
            return multiply.subtract(multiply2).add(shiftLeft);
        }
        throw new IllegalArgumentException("mu must be 1 or -1");
    }

    public static SimpleBigDecimal norm(byte b, SimpleBigDecimal simpleBigDecimal, SimpleBigDecimal simpleBigDecimal2) {
        SimpleBigDecimal multiply = simpleBigDecimal.multiply(simpleBigDecimal);
        SimpleBigDecimal multiply2 = simpleBigDecimal.multiply(simpleBigDecimal2);
        SimpleBigDecimal shiftLeft = simpleBigDecimal2.multiply(simpleBigDecimal2).shiftLeft(1);
        if (b == 1) {
            return multiply.add(multiply2).add(shiftLeft);
        }
        if (b == -1) {
            return multiply.subtract(multiply2).add(shiftLeft);
        }
        throw new IllegalArgumentException("mu must be 1 or -1");
    }

    public static ZTauElement partModReduction(BigInteger bigInteger, int i, byte b, BigInteger[] bigIntegerArr, byte b2, byte b3) {
        BigInteger add = b2 == 1 ? bigIntegerArr[0].add(bigIntegerArr[1]) : bigIntegerArr[0].subtract(bigIntegerArr[1]);
        BigInteger bigInteger2 = getLucas(b2, i, true)[1];
        ZTauElement round = round(approximateDivisionByN(bigInteger, bigIntegerArr[0], bigInteger2, b, i, b3), approximateDivisionByN(bigInteger, bigIntegerArr[1], bigInteger2, b, i, b3), b2);
        return new ZTauElement(bigInteger.subtract(add.multiply(round.u)).subtract(BigInteger.valueOf(2).multiply(bigIntegerArr[1]).multiply(round.v)), bigIntegerArr[1].multiply(round.u).subtract(bigIntegerArr[0].multiply(round.v)));
    }

    public static ZTauElement round(SimpleBigDecimal simpleBigDecimal, SimpleBigDecimal simpleBigDecimal2, byte b) {
        SimpleBigDecimal subtract;
        SimpleBigDecimal simpleBigDecimal3;
        byte b2;
        byte b3;
        int i;
        int i2 = 1;
        if (simpleBigDecimal2.getScale() != simpleBigDecimal.getScale()) {
            throw new IllegalArgumentException("lambda0 and lambda1 do not have same scale");
        } else if (b == 1 || b == -1) {
            BigInteger round = simpleBigDecimal.round();
            BigInteger round2 = simpleBigDecimal2.round();
            SimpleBigDecimal subtract2 = simpleBigDecimal.subtract(round);
            SimpleBigDecimal subtract3 = simpleBigDecimal2.subtract(round2);
            SimpleBigDecimal add = subtract2.add(subtract2);
            SimpleBigDecimal add2 = b == 1 ? add.add(subtract3) : add.subtract(subtract3);
            SimpleBigDecimal add3 = subtract3.add(subtract3).add(subtract3);
            SimpleBigDecimal add4 = add3.add(subtract3);
            if (b == 1) {
                SimpleBigDecimal subtract4 = subtract2.subtract(add3);
                subtract = subtract2.add(add4);
                simpleBigDecimal3 = subtract4;
            } else {
                SimpleBigDecimal add5 = subtract2.add(add3);
                subtract = subtract2.subtract(add4);
                simpleBigDecimal3 = add5;
            }
            if (add2.compareTo(ECConstants.ONE) >= 0) {
                if (simpleBigDecimal3.compareTo(MINUS_ONE) < 0) {
                    b2 = b;
                    i2 = 0;
                } else {
                    b2 = 0;
                }
            } else if (subtract.compareTo(ECConstants.TWO) >= 0) {
                b2 = b;
                i2 = 0;
            } else {
                b2 = 0;
                i2 = 0;
            }
            if (add2.compareTo(MINUS_ONE) < 0) {
                if (simpleBigDecimal3.compareTo(ECConstants.ONE) >= 0) {
                    b3 = (byte) (-b);
                    i = i2;
                } else {
                    b3 = b2;
                    i = -1;
                }
            } else if (subtract.compareTo(MINUS_TWO) < 0) {
                b3 = (byte) (-b);
                i = i2;
            } else {
                b3 = b2;
                i = i2;
            }
            return new ZTauElement(round.add(BigInteger.valueOf((long) i)), round2.add(BigInteger.valueOf((long) b3)));
        } else {
            throw new IllegalArgumentException("mu must be 1 or -1");
        }
    }

    public static ECPoint.F2m tau(ECPoint.F2m f2m) {
        if (f2m.isInfinity()) {
            return f2m;
        }
        return new ECPoint.F2m(f2m.getCurve(), f2m.getX().square(), f2m.getY().square(), f2m.isCompressed());
    }

    public static byte[] tauAdicNaf(byte b, ZTauElement zTauElement) {
        if (b == 1 || b == -1) {
            int bitLength = norm(b, zTauElement).bitLength();
            byte[] bArr = new byte[(bitLength > 30 ? bitLength + 4 : 34)];
            BigInteger bigInteger = zTauElement.u;
            BigInteger bigInteger2 = zTauElement.v;
            int i = 0;
            int i2 = 0;
            while (true) {
                BigInteger bigInteger3 = bigInteger2;
                BigInteger bigInteger4 = bigInteger;
                BigInteger bigInteger5 = bigInteger3;
                if (!bigInteger4.equals(ECConstants.ZERO) || !bigInteger5.equals(ECConstants.ZERO)) {
                    if (bigInteger4.testBit(0)) {
                        bArr[i2] = (byte) ECConstants.TWO.subtract(bigInteger4.subtract(bigInteger5.shiftLeft(1)).mod(ECConstants.FOUR)).intValue();
                        bigInteger4 = bArr[i2] == 1 ? bigInteger4.clearBit(0) : bigInteger4.add(ECConstants.ONE);
                        i = i2;
                    } else {
                        bArr[i2] = 0;
                    }
                    BigInteger shiftRight = bigInteger4.shiftRight(1);
                    bigInteger = b == 1 ? bigInteger5.add(shiftRight) : bigInteger5.subtract(shiftRight);
                    bigInteger2 = bigInteger4.shiftRight(1).negate();
                    i2++;
                } else {
                    int i3 = i + 1;
                    byte[] bArr2 = new byte[i3];
                    System.arraycopy(bArr, 0, bArr2, 0, i3);
                    return bArr2;
                }
            }
        } else {
            throw new IllegalArgumentException("mu must be 1 or -1");
        }
    }

    public static byte[] tauAdicWNaf(byte b, ZTauElement zTauElement, byte b2, BigInteger bigInteger, BigInteger bigInteger2, ZTauElement[] zTauElementArr) {
        byte b3;
        boolean z;
        if (b == 1 || b == -1) {
            int bitLength = norm(b, zTauElement).bitLength();
            byte[] bArr = new byte[(bitLength > 30 ? bitLength + 4 + b2 : b2 + 34)];
            BigInteger shiftRight = bigInteger.shiftRight(1);
            BigInteger bigInteger3 = zTauElement.u;
            BigInteger bigInteger4 = zTauElement.v;
            BigInteger bigInteger5 = bigInteger3;
            int i = 0;
            while (true) {
                if (bigInteger5.equals(ECConstants.ZERO) && bigInteger4.equals(ECConstants.ZERO)) {
                    return bArr;
                }
                if (bigInteger5.testBit(0)) {
                    BigInteger mod = bigInteger5.add(bigInteger4.multiply(bigInteger2)).mod(bigInteger);
                    byte intValue = mod.compareTo(shiftRight) >= 0 ? (byte) mod.subtract(bigInteger).intValue() : (byte) mod.intValue();
                    bArr[i] = intValue;
                    if (intValue < 0) {
                        b3 = (byte) (-intValue);
                        z = false;
                    } else {
                        b3 = intValue;
                        z = true;
                    }
                    if (z) {
                        bigInteger5 = bigInteger5.subtract(zTauElementArr[b3].u);
                        bigInteger4 = bigInteger4.subtract(zTauElementArr[b3].v);
                    } else {
                        bigInteger5 = bigInteger5.add(zTauElementArr[b3].u);
                        bigInteger4 = bigInteger4.add(zTauElementArr[b3].v);
                    }
                } else {
                    bArr[i] = 0;
                }
                BigInteger add = b == 1 ? bigInteger4.add(bigInteger5.shiftRight(1)) : bigInteger4.subtract(bigInteger5.shiftRight(1));
                i++;
                BigInteger negate = bigInteger5.shiftRight(1).negate();
                bigInteger5 = add;
                bigInteger4 = negate;
            }
        } else {
            throw new IllegalArgumentException("mu must be 1 or -1");
        }
    }
}
