package org.bouncycastle.math.ec;

import java.math.BigInteger;
import org.bouncycastle.util.Arrays;

class IntArray {
    private int[] m_ints;

    public IntArray(int i) {
        this.m_ints = new int[i];
    }

    public IntArray(BigInteger bigInteger) {
        this(bigInteger, 0);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:41:0x006d */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:40:0x006d */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:36:0x004b */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:35:0x004b */
    /* JADX WARN: Type inference failed for: r0v3, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r2v0, types: [byte] */
    /* JADX WARN: Type inference failed for: r3v5, types: [byte, int] */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v9, types: [int] */
    /* JADX WARN: Type inference failed for: r5v3, types: [byte, int] */
    /* JADX WARN: Type inference failed for: r5v4 */
    /* JADX WARN: Type inference failed for: r5v5, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public IntArray(java.math.BigInteger r9, int r10) {
        /*
            r8 = this;
            r7 = 1
            r6 = 0
            r8.<init>()
            int r0 = r9.signum()
            r1 = -1
            if (r0 != r1) goto L_0x0014
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Only positive Integers allowed"
            r0.<init>(r1)
            throw r0
        L_0x0014:
            java.math.BigInteger r0 = org.bouncycastle.math.ec.ECConstants.ZERO
            boolean r0 = r9.equals(r0)
            if (r0 == 0) goto L_0x0023
            int[] r0 = new int[r7]
            r0[r6] = r6
            r8.m_ints = r0
        L_0x0022:
            return
        L_0x0023:
            byte[] r0 = r9.toByteArray()
            int r1 = r0.length
            byte r2 = r0[r6]
            if (r2 != 0) goto L_0x007e
            int r1 = r1 + -1
            r2 = r1
            r1 = r7
        L_0x0030:
            int r3 = r2 + 3
            int r3 = r3 / 4
            if (r3 >= r10) goto L_0x004f
            int[] r4 = new int[r10]
            r8.m_ints = r4
        L_0x003a:
            int r3 = r3 - r7
            int r2 = r2 % 4
            int r2 = r2 + r1
            if (r1 >= r2) goto L_0x007c
            r4 = r6
        L_0x0041:
            if (r1 >= r2) goto L_0x0054
            int r4 = r4 << 8
            byte r5 = r0[r1]
            if (r5 >= 0) goto L_0x004b
            int r5 = r5 + 256
        L_0x004b:
            r4 = r4 | r5
            int r1 = r1 + 1
            goto L_0x0041
        L_0x004f:
            int[] r4 = new int[r3]
            r8.m_ints = r4
            goto L_0x003a
        L_0x0054:
            int[] r2 = r8.m_ints
            int r5 = r3 - r7
            r2[r3] = r4
            r2 = r5
        L_0x005b:
            if (r2 < 0) goto L_0x0022
            r3 = r1
            r4 = r6
            r1 = r6
        L_0x0060:
            r5 = 4
            if (r1 >= r5) goto L_0x0073
            int r4 = r4 << 8
            int r5 = r3 + 1
            byte r3 = r0[r3]
            if (r3 >= 0) goto L_0x006d
            int r3 = r3 + 256
        L_0x006d:
            r3 = r3 | r4
            int r1 = r1 + 1
            r4 = r3
            r3 = r5
            goto L_0x0060
        L_0x0073:
            int[] r1 = r8.m_ints
            r1[r2] = r4
            int r1 = r2 + -1
            r2 = r1
            r1 = r3
            goto L_0x005b
        L_0x007c:
            r2 = r3
            goto L_0x005b
        L_0x007e:
            r2 = r1
            r1 = r6
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.math.ec.IntArray.<init>(java.math.BigInteger, int):void");
    }

    public IntArray(int[] iArr) {
        this.m_ints = iArr;
    }

    private int[] resizedInts(int i) {
        int[] iArr = new int[i];
        int length = this.m_ints.length;
        if (length >= i) {
            length = i;
        }
        System.arraycopy(this.m_ints, 0, iArr, 0, length);
        return iArr;
    }

    public void addShifted(IntArray intArray, int i) {
        int usedLength = intArray.getUsedLength();
        int i2 = usedLength + i;
        if (i2 > this.m_ints.length) {
            this.m_ints = resizedInts(i2);
        }
        for (int i3 = 0; i3 < usedLength; i3++) {
            int[] iArr = this.m_ints;
            int i4 = i3 + i;
            iArr[i4] = iArr[i4] ^ intArray.m_ints[i3];
        }
    }

    public int bitLength() {
        int usedLength = getUsedLength();
        if (usedLength == 0) {
            return 0;
        }
        int i = usedLength - 1;
        int i2 = this.m_ints[i];
        int i3 = (i << 5) + 1;
        if ((-65536 & i2) != 0) {
            if ((-16777216 & i2) != 0) {
                i3 += 24;
                i2 >>>= 24;
            } else {
                i3 += 16;
                i2 >>>= 16;
            }
        } else if (i2 > 255) {
            i3 += 8;
            i2 >>>= 8;
        }
        while (i2 != 1) {
            i3++;
            i2 >>>= 1;
        }
        return i3;
    }

    public Object clone() {
        return new IntArray(Arrays.clone(this.m_ints));
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof IntArray)) {
            return false;
        }
        IntArray intArray = (IntArray) obj;
        int usedLength = getUsedLength();
        if (intArray.getUsedLength() != usedLength) {
            return false;
        }
        for (int i = 0; i < usedLength; i++) {
            if (this.m_ints[i] != intArray.m_ints[i]) {
                return false;
            }
        }
        return true;
    }

    public void flipBit(int i) {
        int i2 = i >> 5;
        int[] iArr = this.m_ints;
        iArr[i2] = (1 << (i & 31)) ^ iArr[i2];
    }

    public int getLength() {
        return this.m_ints.length;
    }

    public int getUsedLength() {
        int length = this.m_ints.length;
        if (length < 1) {
            return 0;
        }
        if (this.m_ints[0] != 0) {
            do {
                length--;
            } while (this.m_ints[length] == 0);
            return length + 1;
        }
        do {
            length--;
            if (this.m_ints[length] != 0) {
                return length + 1;
            }
        } while (length > 0);
        return 0;
    }

    public int hashCode() {
        int usedLength = getUsedLength();
        int i = 1;
        for (int i2 = 0; i2 < usedLength; i2++) {
            i = (i * 31) + this.m_ints[i2];
        }
        return i;
    }

    public boolean isZero() {
        return this.m_ints.length == 0 || (this.m_ints[0] == 0 && getUsedLength() == 0);
    }

    public IntArray multiply(IntArray intArray, int i) {
        int i2 = (i + 31) >> 5;
        if (this.m_ints.length < i2) {
            this.m_ints = resizedInts(i2);
        }
        IntArray intArray2 = new IntArray(intArray.resizedInts(intArray.getLength() + 1));
        IntArray intArray3 = new IntArray(((i + i) + 31) >> 5);
        int i3 = 1;
        for (int i4 = 0; i4 < 32; i4++) {
            for (int i5 = 0; i5 < i2; i5++) {
                if ((this.m_ints[i5] & i3) != 0) {
                    intArray3.addShifted(intArray2, i5);
                }
            }
            i3 <<= 1;
            intArray2.shiftLeft();
        }
        return intArray3;
    }

    public void reduce(int i, int[] iArr) {
        for (int i2 = (i + i) - 2; i2 >= i; i2--) {
            if (testBit(i2)) {
                int i3 = i2 - i;
                flipBit(i3);
                flipBit(i2);
                int length = iArr.length;
                while (true) {
                    length--;
                    if (length < 0) {
                        break;
                    }
                    flipBit(iArr[length] + i3);
                }
            }
        }
        this.m_ints = resizedInts((i + 31) >> 5);
    }

    public void setBit(int i) {
        int i2 = i >> 5;
        int[] iArr = this.m_ints;
        iArr[i2] = (1 << (i & 31)) | iArr[i2];
    }

    public IntArray shiftLeft(int i) {
        int usedLength = getUsedLength();
        if (usedLength == 0) {
            return this;
        }
        if (i == 0) {
            return this;
        }
        if (i > 31) {
            throw new IllegalArgumentException("shiftLeft() for max 31 bits , " + i + "bit shift is not possible");
        }
        int[] iArr = new int[(usedLength + 1)];
        int i2 = 32 - i;
        iArr[0] = this.m_ints[0] << i;
        for (int i3 = 1; i3 < usedLength; i3++) {
            iArr[i3] = (this.m_ints[i3] << i) | (this.m_ints[i3 - 1] >>> i2);
        }
        iArr[usedLength] = this.m_ints[usedLength - 1] >>> i2;
        return new IntArray(iArr);
    }

    public void shiftLeft() {
        int usedLength = getUsedLength();
        if (usedLength != 0) {
            if (this.m_ints[usedLength - 1] < 0 && (usedLength = usedLength + 1) > this.m_ints.length) {
                this.m_ints = resizedInts(this.m_ints.length + 1);
            }
            int i = 0;
            boolean z = false;
            while (i < usedLength) {
                boolean z2 = this.m_ints[i] < 0;
                int[] iArr = this.m_ints;
                iArr[i] = iArr[i] << 1;
                if (z) {
                    int[] iArr2 = this.m_ints;
                    iArr2[i] = iArr2[i] | 1;
                }
                i++;
                z = z2;
            }
        }
    }

    public IntArray square(int i) {
        int[] iArr = {0, 1, 4, 5, 16, 17, 20, 21, 64, 65, 68, 69, 80, 81, 84, 85};
        int i2 = (i + 31) >> 5;
        if (this.m_ints.length < i2) {
            this.m_ints = resizedInts(i2);
        }
        IntArray intArray = new IntArray(i2 + i2);
        for (int i3 = 0; i3 < i2; i3++) {
            int i4 = 0;
            for (int i5 = 0; i5 < 4; i5++) {
                i4 = (i4 >>> 8) | (iArr[(this.m_ints[i3] >>> (i5 * 4)) & 15] << 24);
            }
            intArray.m_ints[i3 + i3] = i4;
            int i6 = this.m_ints[i3] >>> 16;
            int i7 = 0;
            for (int i8 = 0; i8 < 4; i8++) {
                i7 = (i7 >>> 8) | (iArr[(i6 >>> (i8 * 4)) & 15] << 24);
            }
            intArray.m_ints[i3 + i3 + 1] = i7;
        }
        return intArray;
    }

    public boolean testBit(int i) {
        return (this.m_ints[i >> 5] & (1 << (i & 31))) != 0;
    }

    public BigInteger toBigInteger() {
        int usedLength = getUsedLength();
        if (usedLength == 0) {
            return ECConstants.ZERO;
        }
        int i = this.m_ints[usedLength - 1];
        byte[] bArr = new byte[4];
        boolean z = false;
        int i2 = 0;
        for (int i3 = 3; i3 >= 0; i3--) {
            byte b = (byte) (i >>> (i3 * 8));
            if (z || b != 0) {
                bArr[i2] = b;
                i2++;
                z = true;
            }
        }
        byte[] bArr2 = new byte[(((usedLength - 1) * 4) + i2)];
        for (int i4 = 0; i4 < i2; i4++) {
            bArr2[i4] = bArr[i4];
        }
        int i5 = usedLength - 2;
        int i6 = i2;
        while (i5 >= 0) {
            int i7 = i6;
            int i8 = 3;
            while (i8 >= 0) {
                bArr2[i7] = (byte) (this.m_ints[i5] >>> (i8 * 8));
                i8--;
                i7++;
            }
            i5--;
            i6 = i7;
        }
        return new BigInteger(1, bArr2);
    }

    public String toString() {
        int usedLength = getUsedLength();
        if (usedLength == 0) {
            return "0";
        }
        StringBuffer stringBuffer = new StringBuffer(Integer.toBinaryString(this.m_ints[usedLength - 1]));
        for (int i = usedLength - 2; i >= 0; i--) {
            String binaryString = Integer.toBinaryString(this.m_ints[i]);
            String str = binaryString;
            for (int length = binaryString.length(); length < 8; length++) {
                str = "0" + str;
            }
            stringBuffer.append(str);
        }
        return stringBuffer.toString();
    }
}
