package org.bouncycastle.math.ec;

import java.math.BigInteger;
import java.util.Random;

public abstract class ECFieldElement implements ECConstants {

    public static class F2m extends ECFieldElement {
        public static final int GNB = 1;
        public static final int PPB = 3;
        public static final int TPB = 2;
        private int k1;
        private int k2;
        private int k3;
        private int m;
        private int representation;
        private int t;
        private IntArray x;

        public F2m(int i, int i2, int i3, int i4, BigInteger bigInteger) {
            this.t = (i + 31) >> 5;
            this.x = new IntArray(bigInteger, this.t);
            if (i3 == 0 && i4 == 0) {
                this.representation = 2;
            } else if (i3 >= i4) {
                throw new IllegalArgumentException("k2 must be smaller than k3");
            } else if (i3 <= 0) {
                throw new IllegalArgumentException("k2 must be larger than 0");
            } else {
                this.representation = 3;
            }
            if (bigInteger.signum() < 0) {
                throw new IllegalArgumentException("x value cannot be negative");
            }
            this.m = i;
            this.k1 = i2;
            this.k2 = i3;
            this.k3 = i4;
        }

        private F2m(int i, int i2, int i3, int i4, IntArray intArray) {
            this.t = (i + 31) >> 5;
            this.x = intArray;
            this.m = i;
            this.k1 = i2;
            this.k2 = i3;
            this.k3 = i4;
            if (i3 == 0 && i4 == 0) {
                this.representation = 2;
            } else {
                this.representation = 3;
            }
        }

        public F2m(int i, int i2, BigInteger bigInteger) {
            this(i, i2, 0, 0, bigInteger);
        }

        public static void checkFieldElements(ECFieldElement eCFieldElement, ECFieldElement eCFieldElement2) {
            if (!(eCFieldElement instanceof F2m) || !(eCFieldElement2 instanceof F2m)) {
                throw new IllegalArgumentException("Field elements are not both instances of ECFieldElement.F2m");
            }
            F2m f2m = (F2m) eCFieldElement;
            F2m f2m2 = (F2m) eCFieldElement2;
            if (f2m.m != f2m2.m || f2m.k1 != f2m2.k1 || f2m.k2 != f2m2.k2 || f2m.k3 != f2m2.k3) {
                throw new IllegalArgumentException("Field elements are not elements of the same field F2m");
            } else if (f2m.representation != f2m2.representation) {
                throw new IllegalArgumentException("One of the field elements are not elements has incorrect representation");
            }
        }

        public ECFieldElement add(ECFieldElement eCFieldElement) {
            IntArray intArray = (IntArray) this.x.clone();
            intArray.addShifted(((F2m) eCFieldElement).x, 0);
            return new F2m(this.m, this.k1, this.k2, this.k3, intArray);
        }

        public ECFieldElement divide(ECFieldElement eCFieldElement) {
            return multiply(eCFieldElement.invert());
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof F2m)) {
                return false;
            }
            F2m f2m = (F2m) obj;
            return this.m == f2m.m && this.k1 == f2m.k1 && this.k2 == f2m.k2 && this.k3 == f2m.k3 && this.representation == f2m.representation && this.x.equals(f2m.x);
        }

        public String getFieldName() {
            return "F2m";
        }

        public int getFieldSize() {
            return this.m;
        }

        public int getK1() {
            return this.k1;
        }

        public int getK2() {
            return this.k2;
        }

        public int getK3() {
            return this.k3;
        }

        public int getM() {
            return this.m;
        }

        public int getRepresentation() {
            return this.representation;
        }

        public int hashCode() {
            return (((this.x.hashCode() ^ this.m) ^ this.k1) ^ this.k2) ^ this.k3;
        }

        public ECFieldElement invert() {
            IntArray intArray;
            IntArray intArray2;
            int i;
            IntArray intArray3;
            IntArray intArray4;
            IntArray intArray5 = (IntArray) this.x.clone();
            IntArray intArray6 = new IntArray(this.t);
            intArray6.setBit(this.m);
            intArray6.setBit(0);
            intArray6.setBit(this.k1);
            if (this.representation == 3) {
                intArray6.setBit(this.k2);
                intArray6.setBit(this.k3);
            }
            IntArray intArray7 = new IntArray(this.t);
            intArray7.setBit(0);
            IntArray intArray8 = new IntArray(this.t);
            IntArray intArray9 = intArray7;
            IntArray intArray10 = intArray5;
            IntArray intArray11 = intArray9;
            while (!intArray10.isZero()) {
                int bitLength = intArray10.bitLength() - intArray6.bitLength();
                if (bitLength < 0) {
                    intArray = intArray6;
                    intArray4 = intArray11;
                    i = -bitLength;
                    intArray3 = intArray10;
                    intArray2 = intArray8;
                } else {
                    intArray = intArray10;
                    intArray2 = intArray11;
                    i = bitLength;
                    intArray3 = intArray6;
                    intArray4 = intArray8;
                }
                int i2 = i >> 5;
                int i3 = i & 31;
                intArray.addShifted(intArray3.shiftLeft(i3), i2);
                intArray2.addShifted(intArray4.shiftLeft(i3), i2);
                intArray8 = intArray4;
                intArray11 = intArray2;
                intArray6 = intArray3;
                intArray10 = intArray;
            }
            return new F2m(this.m, this.k1, this.k2, this.k3, intArray8);
        }

        public ECFieldElement multiply(ECFieldElement eCFieldElement) {
            IntArray multiply = this.x.multiply(((F2m) eCFieldElement).x, this.m);
            multiply.reduce(this.m, new int[]{this.k1, this.k2, this.k3});
            return new F2m(this.m, this.k1, this.k2, this.k3, multiply);
        }

        public ECFieldElement negate() {
            return this;
        }

        public ECFieldElement sqrt() {
            throw new RuntimeException("Not implemented");
        }

        public ECFieldElement square() {
            IntArray square = this.x.square(this.m);
            square.reduce(this.m, new int[]{this.k1, this.k2, this.k3});
            return new F2m(this.m, this.k1, this.k2, this.k3, square);
        }

        public ECFieldElement subtract(ECFieldElement eCFieldElement) {
            return add(eCFieldElement);
        }

        public BigInteger toBigInteger() {
            return this.x.toBigInteger();
        }
    }

    public static class Fp extends ECFieldElement {
        BigInteger q;
        BigInteger x;

        public Fp(BigInteger bigInteger, BigInteger bigInteger2) {
            this.x = bigInteger2;
            if (bigInteger2.compareTo(bigInteger) >= 0) {
                throw new IllegalArgumentException("x value too large in field element");
            }
            this.q = bigInteger;
        }

        private static BigInteger[] lucasSequence(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4) {
            BigInteger bigInteger5;
            int bitLength = bigInteger4.bitLength();
            int lowestSetBit = bigInteger4.getLowestSetBit();
            BigInteger bigInteger6 = ECConstants.ONE;
            BigInteger bigInteger7 = ECConstants.TWO;
            BigInteger bigInteger8 = ECConstants.ONE;
            int i = bitLength - 1;
            BigInteger bigInteger9 = bigInteger6;
            BigInteger bigInteger10 = ECConstants.ONE;
            BigInteger bigInteger11 = bigInteger7;
            BigInteger bigInteger12 = bigInteger8;
            BigInteger bigInteger13 = bigInteger2;
            while (i >= lowestSetBit + 1) {
                BigInteger mod = bigInteger12.multiply(bigInteger10).mod(bigInteger);
                if (bigInteger4.testBit(i)) {
                    bigInteger5 = mod.multiply(bigInteger3).mod(bigInteger);
                    bigInteger9 = bigInteger9.multiply(bigInteger13).mod(bigInteger);
                    bigInteger11 = bigInteger13.multiply(bigInteger11).subtract(bigInteger2.multiply(mod)).mod(bigInteger);
                    bigInteger13 = bigInteger13.multiply(bigInteger13).subtract(bigInteger5.shiftLeft(1)).mod(bigInteger);
                } else {
                    BigInteger mod2 = bigInteger9.multiply(bigInteger11).subtract(mod).mod(bigInteger);
                    bigInteger13 = bigInteger13.multiply(bigInteger11).subtract(bigInteger2.multiply(mod)).mod(bigInteger);
                    bigInteger11 = bigInteger11.multiply(bigInteger11).subtract(mod.shiftLeft(1)).mod(bigInteger);
                    bigInteger9 = mod2;
                    bigInteger5 = mod;
                }
                i--;
                BigInteger bigInteger14 = bigInteger5;
                bigInteger12 = mod;
                bigInteger10 = bigInteger14;
            }
            BigInteger mod3 = bigInteger12.multiply(bigInteger10).mod(bigInteger);
            BigInteger mod4 = mod3.multiply(bigInteger3).mod(bigInteger);
            BigInteger mod5 = bigInteger9.multiply(bigInteger11).subtract(mod3).mod(bigInteger);
            BigInteger mod6 = bigInteger13.multiply(bigInteger11).subtract(bigInteger2.multiply(mod3)).mod(bigInteger);
            BigInteger mod7 = mod3.multiply(mod4).mod(bigInteger);
            BigInteger bigInteger15 = mod6;
            BigInteger bigInteger16 = mod5;
            BigInteger bigInteger17 = bigInteger15;
            for (int i2 = 1; i2 <= lowestSetBit; i2++) {
                bigInteger16 = bigInteger16.multiply(bigInteger17).mod(bigInteger);
                bigInteger17 = bigInteger17.multiply(bigInteger17).subtract(mod7.shiftLeft(1)).mod(bigInteger);
                mod7 = mod7.multiply(mod7).mod(bigInteger);
            }
            return new BigInteger[]{bigInteger16, bigInteger17};
        }

        public ECFieldElement add(ECFieldElement eCFieldElement) {
            return new Fp(this.q, this.x.add(eCFieldElement.toBigInteger()).mod(this.q));
        }

        public ECFieldElement divide(ECFieldElement eCFieldElement) {
            return new Fp(this.q, this.x.multiply(eCFieldElement.toBigInteger().modInverse(this.q)).mod(this.q));
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Fp)) {
                return false;
            }
            Fp fp = (Fp) obj;
            return this.q.equals(fp.q) && this.x.equals(fp.x);
        }

        public String getFieldName() {
            return "Fp";
        }

        public int getFieldSize() {
            return this.q.bitLength();
        }

        public BigInteger getQ() {
            return this.q;
        }

        public int hashCode() {
            return this.q.hashCode() ^ this.x.hashCode();
        }

        public ECFieldElement invert() {
            return new Fp(this.q, this.x.modInverse(this.q));
        }

        public ECFieldElement multiply(ECFieldElement eCFieldElement) {
            return new Fp(this.q, this.x.multiply(eCFieldElement.toBigInteger()).mod(this.q));
        }

        public ECFieldElement negate() {
            return new Fp(this.q, this.x.negate().mod(this.q));
        }

        public ECFieldElement sqrt() {
            if (!this.q.testBit(0)) {
                throw new RuntimeException("not done yet");
            } else if (this.q.testBit(1)) {
                Fp fp = new Fp(this.q, this.x.modPow(this.q.shiftRight(2).add(ONE), this.q));
                if (fp.square().equals(this)) {
                    return fp;
                }
                return null;
            } else {
                BigInteger subtract = this.q.subtract(ECConstants.ONE);
                BigInteger shiftRight = subtract.shiftRight(1);
                if (!this.x.modPow(shiftRight, this.q).equals(ECConstants.ONE)) {
                    return null;
                }
                BigInteger add = subtract.shiftRight(2).shiftLeft(1).add(ECConstants.ONE);
                BigInteger bigInteger = this.x;
                BigInteger mod = bigInteger.shiftLeft(2).mod(this.q);
                Random random = new Random();
                while (true) {
                    BigInteger bigInteger2 = new BigInteger(this.q.bitLength(), random);
                    if (bigInteger2.compareTo(this.q) < 0 && bigInteger2.multiply(bigInteger2).subtract(mod).modPow(shiftRight, this.q).equals(subtract)) {
                        BigInteger[] lucasSequence = lucasSequence(this.q, bigInteger2, bigInteger, add);
                        BigInteger bigInteger3 = lucasSequence[0];
                        BigInteger bigInteger4 = lucasSequence[1];
                        if (bigInteger4.multiply(bigInteger4).mod(this.q).equals(mod)) {
                            return new Fp(this.q, (bigInteger4.testBit(0) ? bigInteger4.add(this.q) : bigInteger4).shiftRight(1));
                        } else if (!bigInteger3.equals(ECConstants.ONE) && !bigInteger3.equals(subtract)) {
                            return null;
                        }
                    }
                }
            }
        }

        public ECFieldElement square() {
            return new Fp(this.q, this.x.multiply(this.x).mod(this.q));
        }

        public ECFieldElement subtract(ECFieldElement eCFieldElement) {
            return new Fp(this.q, this.x.subtract(eCFieldElement.toBigInteger()).mod(this.q));
        }

        public BigInteger toBigInteger() {
            return this.x;
        }
    }

    public abstract ECFieldElement add(ECFieldElement eCFieldElement);

    public abstract ECFieldElement divide(ECFieldElement eCFieldElement);

    public abstract String getFieldName();

    public abstract int getFieldSize();

    public abstract ECFieldElement invert();

    public abstract ECFieldElement multiply(ECFieldElement eCFieldElement);

    public abstract ECFieldElement negate();

    public abstract ECFieldElement sqrt();

    public abstract ECFieldElement square();

    public abstract ECFieldElement subtract(ECFieldElement eCFieldElement);

    public abstract BigInteger toBigInteger();

    public String toString() {
        return toBigInteger().toString(2);
    }
}
