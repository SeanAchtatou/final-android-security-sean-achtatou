package org.bouncycastle.math.ec;

import java.math.BigInteger;

class ReferenceMultiplier implements ECMultiplier {
    ReferenceMultiplier() {
    }

    public ECPoint multiply(ECPoint eCPoint, BigInteger bigInteger, PreCompInfo preCompInfo) {
        ECPoint infinity = eCPoint.getCurve().getInfinity();
        int bitLength = bigInteger.bitLength();
        ECPoint eCPoint2 = eCPoint;
        ECPoint eCPoint3 = infinity;
        ECPoint eCPoint4 = eCPoint3;
        for (int i = 0; i < bitLength; i++) {
            if (bigInteger.testBit(i)) {
                eCPoint4 = eCPoint4.add(eCPoint2);
            }
            eCPoint2 = eCPoint2.twice();
        }
        return eCPoint4;
    }
}
