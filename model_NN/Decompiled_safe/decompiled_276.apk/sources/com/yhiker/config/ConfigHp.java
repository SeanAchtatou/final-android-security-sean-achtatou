package com.yhiker.config;

public class ConfigHp {
    public static final String att_broad = "com.yhiker.playmate.attchange";
    public static final int bufferSize = 524288;
    public static final String cityact_broad = "com.yhiker.playmate.citychange";
    public static final String conf_dir = "/yhiker/conf";
    public static final String data_dir = "/yhiker/data";
    public static final int data_zip_conn_timeout = 15000;
    public static final boolean debuggable = false;
    public static final String down_broad = "com.yhiker.playmate.down";
    public static final String down_notitle = "玩伴儿";
    public static final String oneByone_play = "com.yhiker.oneByone.play";
    public static final String per_conf_path = "/yhiker/conf/per_conf.db";
    public static final int picwall_conn_timeout = 15000;
    public static final int res_sys_conf_ver = 1;
    public static final String scenic_city_path = "/yhiker/conf/scenic_";
    public static final String scenic_data_dir = "/yhiker/data";
    public static final String scenic_list_data_update_path = "/yhiker/update/scenic_list_data.zip";
    public static final String scenic_list_data_ver_dir = "/yhiker/data/0086";
    public static final String scenic_list_path = "/yhiker/conf/scenic_list.db";
    public static final String scenic_list_update_path = "/yhiker/update/scenic_list.zip";
    public static final String scenicact_broad = "com.yhiker.playmate.scenicchange";
    public static final String server_url = "http://58.211.138.180:88/0420/";
    public static final long space_avi_size = 524288000;
    public static final String sys_conf_path = "/yhiker/conf/sys_conf.db";
    public static final String sys_conf_update_path = "/yhiker/update/sys_conf.zip";
    public static final String update_dir = "/yhiker/update";
    public static final int update_service_conn_timeout = 15000;
    public static final String zip_passwd = "_y_hiker22011_";
}
