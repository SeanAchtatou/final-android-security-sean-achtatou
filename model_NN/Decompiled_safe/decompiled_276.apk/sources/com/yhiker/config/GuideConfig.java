package com.yhiker.config;

import android.content.Context;
import android.os.Environment;
import java.util.HashMap;
import java.util.Map;

public class GuideConfig {
    public static String APK_NAME = "onebyone.apk";
    public static String APK_PACKAGE_NAME = "com.yhiker.playmate";
    public static String APK_SAVE_PATH = "/yhiker/update/onebyone.apk";
    public static String APK_VERIFY_UPDATE_URL = "http://58.211.138.180:8088/dms-new/update/apk";
    public static final String URL_ACCOUNT_LOGIN = "http://58.211.138.180:8088/dms-new/service/account/login.do";
    public static final String URL_ACCOUNT_REG = "http://58.211.138.180:8088/dms-new/service/account/regist.do";
    public static final String URL_ACCOUNT_UPDATE = "http://58.211.138.180:8088/dms-new/service/account/update.do";
    public static final String URL_ALIPAY_SYN = "http://58.211.138.180:8088/dms-new/service/pay/result.do";
    public static final String URL_CITY_PAY = "http://58.211.138.180:8088/dms-new/service/pay/pay.do";
    public static final String URL_LOG_PHONE = "http://58.211.138.180:8088/dms-new/service/phone/add.do";
    public static final String URL_PARK_CITY_INTO = "http://58.211.138.180:8088/dms-new/service/pay/list.do";
    public static final String URL_PARK_DATE = "http://58.211.138.180:8088/dms-new/getParkXmlData.do";
    public static final String URL_PARK_FREE_ADD = "http://58.211.138.180:8088/dms-new/service/pay/addFree.do";
    public static final String URL_PARK_LIST_VERSION_VERIFY = "http://58.211.138.180:8088/dms-new/service/park/listVerify.do";
    public static final String URL_PARK_VERIFY = "http://58.211.138.180:8088/dms-new/service/park/verify.do";
    public static String URL_PERSISTDETAILS = "http://58.211.138.180:8088/dms-new/service/user_handle_records/persistDetails";
    public static String URL_PERSISTPRIMARY = "http://58.211.138.180:8088/dms-new/service/user_handle_records/persistPrimary";
    public static Map<String, String> apkSysConfMap = new HashMap();
    public static final int availableSatellitesCount_minlimit = 4;
    public static final int broadCastButtonHeight = 20;
    public static final int broadCastButtonWidth = 20;
    public static final String channelCode = "100014";
    public static String curCityCode = null;
    public static String curScenicKey = null;
    public static String deviceId = null;
    public static final String hikerserverpath = "http://58.211.138.180:8088/dms-new";
    public static final String httpurlrootpath = "http://58.211.138.180:88";
    private static String initDataDir = null;
    public static final boolean isKeepCenterForMovePoint = false;
    public static final boolean isRecordGPS = false;
    public static final boolean isShowBroadCastArea = false;
    public static final boolean isShowBroadCastButton = true;
    public static final boolean isShowBroadCastName = true;
    public static String keyForkey = "yhiker001";
    public static String phoneNumber = null;
    private static String rootDir = null;
    public static final int satelliteSnr_minlimit = 15;
    public static String scenic_db_version_tag = "scenic_db_version";
    public static final int segmentHeight = 240;
    public static final int segmentWidth = 240;
    public static final String server_url = "http://58.211.138.180:88/0420/";
    public static String simId;
    public static Map<String, String> sysConfMap = new HashMap();

    public static String getInitDataDir() {
        if (initDataDir == null || "".equals(initDataDir)) {
            initDataDir = "/data/data/com.yhiker.playmate/files";
        }
        return initDataDir;
    }

    public static void setInitDataDir(Context context) {
        if (context != null) {
            initDataDir = context.getFilesDir().getAbsolutePath();
        }
    }

    public static String getRootDir() {
        if ((rootDir == null || "".equals(rootDir)) && Environment.getExternalStorageState().equals("mounted")) {
            setRootDir(Environment.getExternalStorageDirectory().getAbsolutePath());
        }
        return rootDir;
    }

    public static void setRootDir(String rootDir2) {
        rootDir = rootDir2;
    }
}
