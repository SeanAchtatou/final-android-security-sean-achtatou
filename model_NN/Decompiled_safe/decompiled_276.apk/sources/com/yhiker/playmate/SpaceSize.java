package com.yhiker.playmate;

import android.os.Environment;
import android.os.StatFs;
import java.text.DecimalFormat;

public class SpaceSize {
    public static long getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
    }

    public static long getAvailableInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    public static long getAvailableExternalMemorySize() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
        } else if (Environment.getExternalStorageState().equals("removed")) {
            return -1;
        } else {
            return 0;
        }
    }

    public static long getTotalExternalMemorySize() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
        } else if (Environment.getExternalStorageState().equals("removed")) {
            return -1;
        } else {
            return 0;
        }
    }

    private String[] fileSize(long size) {
        String str = "";
        if (size >= 1024) {
            str = "KB";
            size /= 1024;
            if (size >= 1024) {
                str = "MB";
                size /= 1024;
            }
        }
        DecimalFormat formatter = new DecimalFormat();
        formatter.setGroupingSize(3);
        return new String[]{formatter.format(size), str};
    }
}
