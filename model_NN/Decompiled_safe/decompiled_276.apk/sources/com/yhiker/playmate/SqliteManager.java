package com.yhiker.playmate;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqliteManager extends SQLiteOpenHelper {
    SqliteManager(Context ct, String dbName) {
        super(ct, dbName, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
    }

    public void onUpgrade(SQLiteDatabase db, int oVersion, int nVersion) {
    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
}
