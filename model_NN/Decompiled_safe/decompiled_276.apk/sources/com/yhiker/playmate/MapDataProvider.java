package com.yhiker.playmate;

import android.util.Log;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.util.UnZip;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

public class MapDataProvider {
    public int downMapZipFile(String uri, String scenicId) {
        ConnectTimeoutException e;
        int processRes;
        ClientConnectionManager connectionManager;
        Log.i(getClass().getName(), "map datazipfile:" + uri);
        int processRes2 = 0;
        DefaultHttpClient httpclient = null;
        try {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
            HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            try {
                HttpGet httpGet = new HttpGet("http://58.211.138.180:88/0420/" + uri + "/" + scenicId + ".lat");
                HttpResponse respHttpServerMapZipFileVer = defaultHttpClient.execute(httpGet);
                if (respHttpServerMapZipFileVer.getStatusLine().getStatusCode() == 200) {
                    String[] localMapDataVers = new File(GuideConfig.getRootDir() + "/yhiker/data" + uri).list(new FileNameSearch("ver"));
                    String localMapDataVer = null;
                    if (localMapDataVers != null && localMapDataVers.length > 0) {
                        localMapDataVer = localMapDataVers[0].substring(localMapDataVers[0].lastIndexOf("_"), localMapDataVers[0].lastIndexOf("."));
                    }
                    if (Integer.valueOf(EntityUtils.toString(respHttpServerMapZipFileVer.getEntity())).intValue() > Integer.valueOf(localMapDataVer).intValue()) {
                        HttpGet httpGet2 = new HttpGet("http://58.211.138.180:88/0420/" + uri + "/" + scenicId + ".zip");
                        HttpResponse respHttpMapZip = defaultHttpClient.execute(httpGet2);
                        if (respHttpMapZip.getStatusLine().getStatusCode() == 200) {
                            InputStream isMapZip = respHttpMapZip.getEntity().getContent();
                            if (isMapZip == null) {
                                throw new RuntimeException("stream is null");
                            }
                            File file = new File(GuideConfig.getRootDir() + ConfigHp.update_dir + "/" + scenicId + "_MapData.zip" + ".dow");
                            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file), ConfigHp.bufferSize);
                            BufferedInputStream bufferedInputStream = new BufferedInputStream(isMapZip, ConfigHp.bufferSize);
                            byte[] data = new byte[ConfigHp.bufferSize];
                            while (true) {
                                int count = bufferedInputStream.read(data, 0, ConfigHp.bufferSize);
                                if (count == -1) {
                                    break;
                                }
                                bufferedOutputStream.write(data, 0, count);
                            }
                            bufferedOutputStream.flush();
                            bufferedOutputStream.close();
                            isMapZip.close();
                            file.renameTo(new File(GuideConfig.getRootDir() + ConfigHp.update_dir + "/" + scenicId + "_MapData.zip"));
                            UnZip unz = new UnZip();
                            Log.i("unzip", GuideConfig.getRootDir() + ConfigHp.update_dir + "/" + scenicId + "_MapData.zip" + " 2 " + GuideConfig.getRootDir() + "/yhiker/data" + uri);
                            unz.deCompFile(GuideConfig.getRootDir() + ConfigHp.update_dir + "/" + scenicId + "_MapData.zip", GuideConfig.getRootDir() + "/yhiker/data" + uri);
                            processRes2 = 2;
                        } else {
                            httpGet2.abort();
                        }
                    } else {
                        processRes2 = 1;
                    }
                } else {
                    httpGet.abort();
                }
                defaultHttpClient.getConnectionManager().shutdown();
            } catch (ConnectTimeoutException e2) {
                e = e2;
                httpclient = defaultHttpClient;
            } catch (Exception e3) {
                httpclient = defaultHttpClient;
                processRes = -2;
                connectionManager = httpclient.getConnectionManager();
                connectionManager.shutdown();
                return processRes;
            } catch (Throwable th) {
                th = th;
                httpclient = defaultHttpClient;
                httpclient.getConnectionManager().shutdown();
                throw th;
            }
        } catch (ConnectTimeoutException e4) {
            e = e4;
            try {
                e.printStackTrace();
                processRes = -1;
                connectionManager = httpclient.getConnectionManager();
                connectionManager.shutdown();
                return processRes;
            } catch (Throwable th2) {
                th = th2;
                httpclient.getConnectionManager().shutdown();
                throw th;
            }
        } catch (Exception e5) {
            processRes = -2;
            connectionManager = httpclient.getConnectionManager();
            connectionManager.shutdown();
            return processRes;
        }
        return processRes;
    }

    public String getParam(String paramName) {
        return "";
    }

    public void getPic() {
    }
}
