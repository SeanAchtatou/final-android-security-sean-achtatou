package com.yhiker.playmate;

import android.location.Location;
import java.math.BigDecimal;

public class GpsGisUtil {
    /* JADX INFO: Multiple debug info for r9v2 float[]: [D('results' float[]), D('distance' double)] */
    /* JADX INFO: Multiple debug info for r11v5 double: [D('lat1' double), D('distance' double)] */
    public static double calcGeoDistance(double lat1, double lon1, double lat2, double lon2) {
        try {
            float[] results = new float[3];
            Location.distanceBetween(lat1, lon1, lat2, lon2, results);
            return (double) results[0];
        } catch (Exception e) {
            return 0.0d;
        }
    }

    public static double RoundDecimal(double value, int decimalPlace) {
        return new BigDecimal(value).setScale(decimalPlace, 6).doubleValue();
    }
}
