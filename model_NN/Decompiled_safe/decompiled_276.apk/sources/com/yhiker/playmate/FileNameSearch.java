package com.yhiker.playmate;

import java.io.File;
import java.io.FilenameFilter;

public class FileNameSearch implements FilenameFilter {
    public String dat;

    public String getDat() {
        return this.dat;
    }

    public void setDat(String dat2) {
        this.dat = dat2;
    }

    public FileNameSearch(String dat2) {
        setDat(dat2);
    }

    public boolean accept(File dir, String fileName) {
        return fileName.endsWith(getDat());
    }
}
