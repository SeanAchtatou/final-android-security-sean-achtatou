package com.yhiker.util;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FileUtils {
    private static final int BUFFER = 4096;
    private static final String TAG = (Env.logTagPrefix + FileUtils.class.getSimpleName());

    /* JADX INFO: Multiple debug info for r0v2 int: [D('fileName' java.lang.String), D('i' int)] */
    /* JADX INFO: Multiple debug info for r0v5 java.io.InputStream: [D('fileName' java.lang.String), D('in' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r4v1 int: [D('outFile' java.io.File), D('len' int)] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0022 A[SYNTHETIC, Splitter:B:9:0x0022] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void copyAssets(android.content.Context r8, java.lang.String r9, java.lang.String r10) {
        /*
            android.content.res.Resources r0 = r8.getResources()     // Catch:{ IOException -> 0x0050 }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ IOException -> 0x0050 }
            java.lang.String[] r1 = r0.list(r9)     // Catch:{ IOException -> 0x0050 }
            java.io.File r5 = new java.io.File
            r5.<init>(r10)
            boolean r0 = r5.exists()
            if (r0 != 0) goto L_0x001d
            boolean r0 = r5.mkdirs()
            if (r0 != 0) goto L_0x001d
        L_0x001d:
            r0 = 0
            r2 = r0
        L_0x001f:
            int r0 = r1.length
            if (r2 >= r0) goto L_0x0051
            r0 = r1[r2]     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.String r3 = "."
            boolean r3 = r0.contains(r3)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            if (r3 != 0) goto L_0x0089
            int r3 = r9.length()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            if (r3 != 0) goto L_0x0052
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.String r3 = r3.toString()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            copyAssets(r8, r0, r3)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
        L_0x004c:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x001f
        L_0x0050:
            r8 = move-exception
        L_0x0051:
            return
        L_0x0052:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.String r3 = r3.toString()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r4 = r4.append(r10)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            copyAssets(r8, r3, r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            goto L_0x004c
        L_0x0084:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004c
        L_0x0089:
            java.io.File r4 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r4.<init>(r5, r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            boolean r3 = r4.exists()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            if (r3 == 0) goto L_0x0097
            r4.delete()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
        L_0x0097:
            r3 = 0
            int r3 = r9.length()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            if (r3 == 0) goto L_0x00d8
            android.content.res.AssetManager r3 = r8.getAssets()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r6.<init>()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.String r7 = "/"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.io.InputStream r0 = r3.open(r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r3 = r0
        L_0x00be:
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r6.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r0 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
        L_0x00c7:
            int r4 = r3.read(r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            if (r4 <= 0) goto L_0x00e2
            r7 = 0
            r6.write(r0, r7, r4)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            goto L_0x00c7
        L_0x00d2:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004c
        L_0x00d8:
            android.content.res.AssetManager r3 = r8.getAssets()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            java.io.InputStream r0 = r3.open(r0)     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r3 = r0
            goto L_0x00be
        L_0x00e2:
            r3.close()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            r6.close()     // Catch:{ FileNotFoundException -> 0x0084, IOException -> 0x00d2 }
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.util.FileUtils.copyAssets(android.content.Context, java.lang.String, java.lang.String):void");
    }

    public static void copy(InputStream sourceFileIs, String targetFile) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(targetFile));
        byte[] buffer = new byte[BUFFER];
        while (true) {
            int count = sourceFileIs.read(buffer);
            if (count > 0) {
                fos.write(buffer, 0, count);
            } else {
                fos.close();
                sourceFileIs.close();
                return;
            }
        }
    }

    public static void copy(InputStream sourceFileIs, File targetFile) throws IOException {
        FileOutputStream fos = new FileOutputStream(targetFile);
        byte[] buffer = new byte[BUFFER];
        while (true) {
            int count = sourceFileIs.read(buffer);
            if (count > 0) {
                fos.write(buffer, 0, count);
            } else {
                fos.close();
                sourceFileIs.close();
                return;
            }
        }
    }

    public static void copy(File sourceFile, File targetFile) throws IOException {
        if (!sourceFile.exists()) {
            Log.i(TAG, "the source file is not exists: " + sourceFile.getAbsolutePath());
        } else if (sourceFile.isFile()) {
            copyFile(sourceFile, targetFile);
        } else {
            copyDirectory(sourceFile, targetFile);
        }
    }

    public static void copyFile(File sourceFile, File targetFile) throws IOException {
        FileInputStream input = new FileInputStream(sourceFile);
        BufferedInputStream inBuff = new BufferedInputStream(input);
        FileOutputStream output = new FileOutputStream(targetFile);
        BufferedOutputStream outBuff = new BufferedOutputStream(output);
        byte[] b = new byte[BUFFER];
        while (true) {
            int len = inBuff.read(b);
            if (len != -1) {
                outBuff.write(b, 0, len);
            } else {
                outBuff.flush();
                inBuff.close();
                outBuff.close();
                output.close();
                input.close();
                return;
            }
        }
    }

    public static void copyDirectory(File sourceDir, File targetDir) throws IOException {
        targetDir.mkdirs();
        File[] file = sourceDir.listFiles();
        for (int i = 0; i < file.length; i++) {
            if (file[i].isFile()) {
                copyFile(file[i], new File(targetDir.getAbsolutePath() + File.separator + file[i].getName()));
            } else if (file[i].isDirectory()) {
                copyDirectory(new File(sourceDir, file[i].getName()), new File(targetDir, file[i].getName()));
            }
        }
    }

    public static boolean delete(File file) {
        if (!file.exists()) {
            Log.i(TAG, "the file is not exists: " + file.getAbsolutePath());
            return false;
        } else if (file.isFile()) {
            return deleteFile(file);
        } else {
            return deleteDirectory(file, true);
        }
    }

    public static boolean deleteFile(File file) {
        if (!file.isFile() || !file.exists()) {
            Log.i(TAG, "the file is not exists: " + file.getAbsolutePath());
            return false;
        }
        file.delete();
        return true;
    }

    public static boolean deleteDirectory(File dirFile, boolean includeSelf) {
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            Log.i(TAG, "the directory is not exists: " + dirFile.getAbsolutePath());
            return false;
        }
        boolean flag = true;
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (!files[i].isFile()) {
                flag = deleteDirectory(files[i], true);
                if (!flag) {
                    break;
                }
            } else {
                flag = deleteFile(files[i]);
                if (!flag) {
                    break;
                }
            }
        }
        if (!flag) {
            Log.i(TAG, "delete directory fail: " + dirFile.getAbsolutePath());
            return false;
        } else if (!includeSelf) {
            return true;
        } else {
            if (dirFile.delete()) {
                return true;
            }
            Log.i(TAG, "delete directory fail: " + dirFile.getAbsolutePath());
            return false;
        }
    }

    public static String readTextFile(File file) throws IOException {
        String strbuffer = new String();
        byte[] data = new byte[BUFFER];
        try {
            DataInputStream inputdata = new DataInputStream(new FileInputStream(file));
            while (true) {
                int nbyteread = inputdata.read(data);
                if (nbyteread != -1) {
                    strbuffer = strbuffer + new String(data, 0, nbyteread);
                } else {
                    inputdata.close();
                    return strbuffer;
                }
            }
        } catch (IOException e) {
            throw e;
        }
    }

    public static void writeTextFile(File file, String str) throws IOException {
        try {
            DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
            out.write(str.getBytes());
            out.close();
        } catch (IOException e) {
            throw e;
        }
    }

    public static void writeTextFile(File file, String[] strArray) throws IOException {
        String str = "";
        for (int i = 0; i < strArray.length; i++) {
            str = str + strArray[i];
            if (i != strArray.length - 1) {
                str = str + "\r\n";
            }
        }
        try {
            DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
            out.write(str.getBytes());
            out.close();
        } catch (IOException e) {
            throw e;
        }
    }

    public static void combineTextFile(File[] sFiles, File dFile) {
        IOException e2;
        FileNotFoundException e1;
        BufferedReader in = null;
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dFile)));
            int i = 0;
            while (i < sFiles.length) {
                try {
                    in = new BufferedReader(new InputStreamReader(new FileInputStream(sFiles[i])));
                    try {
                        String oldLine = in.readLine();
                        while (true) {
                            String newLine = in.readLine();
                            if (newLine == null) {
                                break;
                            }
                            out.write(oldLine);
                            out.newLine();
                            oldLine = newLine;
                        }
                        out.write(oldLine);
                        if (i != sFiles.length - 1) {
                            out.newLine();
                        }
                        out.flush();
                        i++;
                    } catch (FileNotFoundException e) {
                        e1 = e;
                        e1.printStackTrace();
                    } catch (IOException e3) {
                        e2 = e3;
                        e2.printStackTrace();
                    }
                } catch (FileNotFoundException e4) {
                    e1 = e4;
                    e1.printStackTrace();
                } catch (IOException e5) {
                    e2 = e5;
                    e2.printStackTrace();
                }
            }
            out.close();
        } catch (FileNotFoundException e6) {
            e1 = e6;
            e1.printStackTrace();
        } catch (IOException e7) {
            e2 = e7;
            e2.printStackTrace();
        }
    }

    public static void writeFile(File file, byte[] data) throws Exception {
        DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
        out.write(data);
        out.close();
    }

    public static int writeFile(File file, InputStream inStream) throws IOException {
        byte[] data = new byte[BUFFER];
        long dataSize = 0;
        DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
        DataInputStream in = new DataInputStream(inStream);
        while (true) {
            int nbyteread = in.read(data);
            if (nbyteread != -1) {
                out.write(data, 0, nbyteread);
                dataSize += (long) nbyteread;
            } else {
                in.close();
                out.close();
                return (int) (dataSize / 1024);
            }
        }
    }

    public static long getDirSize(File dir) {
        long dirSize;
        long size = 0;
        if (dir.exists() && dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.isFile()) {
                    dirSize = file.length();
                } else {
                    dirSize = getDirSize(file);
                }
                size += dirSize;
            }
        }
        return size;
    }
}
