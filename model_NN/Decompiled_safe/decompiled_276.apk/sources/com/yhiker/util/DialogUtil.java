package com.yhiker.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.yhiker.playmate.R;

public class DialogUtil {
    public static void showExitDialog(final Activity context) {
        new AlertDialog.Builder(context).setIcon((int) R.drawable.ic_menu_info_details).setTitle((int) R.string.quit_exit).setMessage((int) R.string.issure_exit).setPositiveButton((int) R.string.confirm_exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                context.finish();
            }
        }).setNegativeButton((int) R.string.cancel_exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setCancelable(true).create().show();
    }

    public static void showDialog(Activity context, String strTitle, String strText, int icon) {
        AlertDialog.Builder tDialog = new AlertDialog.Builder(context);
        tDialog.setIcon(icon);
        tDialog.setTitle(strTitle);
        tDialog.setMessage(strText);
        tDialog.setPositiveButton((int) R.string.Ensure, (DialogInterface.OnClickListener) null);
        tDialog.show();
    }

    public static ProgressDialog showProgress(Context context, CharSequence title, CharSequence message, boolean indeterminate, boolean cancelable) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.requestWindowFeature(1);
        dialog.setProgressStyle(R.style.progressRound);
        dialog.setMessage(message);
        dialog.setCancelable(false);
        dialog.show();
        return dialog;
    }
}
