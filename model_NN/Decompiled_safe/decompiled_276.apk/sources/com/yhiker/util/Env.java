package com.yhiker.util;

import android.view.Display;
import java.io.File;

public class Env {
    public static final int CACHE_EXTERNAL = 2;
    public static final int CACHE_INTERNAL = 1;
    public static final int INSTALL_STATUS_NEW = 1;
    public static final int INSTALL_STATUS_NO_CHANGE = 0;
    public static final int INSTALL_STATUS_UPDATE = 2;
    public static boolean appRunning = true;
    public static File cacheDirExternal;
    public static File cacheDirInternal;
    public static File cacheDirTemp;
    public static String client;
    public static Display display;
    public static File downloadDir;
    public static int installStatus = 0;
    public static String latestVersionApk = "";
    public static int latestVersionCode = 0;
    public static String latestVersionName = "1.0.0";
    public static String latestVersionRemind = "";
    public static String logTagPrefix;
    private static long maxCacheID = 0;
    public static File tempCacheDirExternal;
    public static File tempCacheDirInternal;
    public static File tempCacheDirTemp;
    public static int versionCode;
    public static String versionName;

    public static synchronized long getNextCacheId() {
        long j;
        synchronized (Env.class) {
            maxCacheID++;
            j = maxCacheID;
        }
        return j;
    }

    public static void setMaxCacheID(long id) {
        maxCacheID = id;
    }
}
