package com.yhiker.util;

import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnZip {
    private static final String TAG = "UnZip";

    public boolean deCompFile(String fileName, String destDir) {
        try {
            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(fileName));
            int counter = 0;
            long s = System.currentTimeMillis();
            while (true) {
                ZipEntry z = zipInputStream.getNextEntry();
                if (z != null) {
                    String name = z.getName();
                    if (z.isDirectory()) {
                        File file = new File(destDir + File.separator + name.substring(0, name.length() - 1));
                        if (file.exists()) {
                            file.delete();
                        }
                        file.mkdirs();
                        if (counter == 0) {
                            String extractedFile = file.toString();
                        }
                        counter++;
                    } else {
                        File file2 = new File(destDir + File.separator + name);
                        file2.createNewFile();
                        FileOutputStream out = new FileOutputStream(file2);
                        byte[] buffer = new byte[1024];
                        while (true) {
                            int ch = zipInputStream.read(buffer);
                            if (ch == -1) {
                                break;
                            }
                            out.write(buffer, 0, ch);
                            out.flush();
                        }
                        out.close();
                    }
                } else {
                    zipInputStream.close();
                    Log.d("", "unzipping file 总用时间: " + (System.currentTimeMillis() - s));
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }
}
