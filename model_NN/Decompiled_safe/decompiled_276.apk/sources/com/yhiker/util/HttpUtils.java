package com.yhiker.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtils {
    private static final String Accept_Encoding = "gzip, deflate";
    private static final int BUFFER = 8192;
    private static final String TAG = (Env.logTagPrefix + HttpUtils.class.getSimpleName());
    private static final String USER_AGENT = (Env.client + " " + Env.versionName);
    private static final int connectTimeout = 10;
    private static final int readTimeout = 20;

    public static InputStream getInputStream(String urlStr) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(urlStr).openConnection();
        conn.setRequestProperty("User-Agent", USER_AGENT);
        if (conn.getResponseCode() == 200) {
            return conn.getInputStream();
        }
        return null;
    }

    public static String invokeText(String urlStr) throws IOException {
        StringBuffer response = null;
        new URL(urlStr);
        InputStream inputStream = getInputStream(urlStr);
        if (inputStream != null) {
            response = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                response.append(line).append("\r\n");
            }
        }
        if (response != null) {
            return response.toString();
        }
        return null;
    }

    public static byte[] invokeData(String urlStr) throws IOException {
        byte[] data = null;
        HttpURLConnection conn = (HttpURLConnection) new URL(urlStr).openConnection();
        conn.setRequestProperty("User-Agent", USER_AGENT);
        if (conn.getResponseCode() == 200) {
            InputStream is = conn.getInputStream();
            int length = conn.getContentLength();
            if (length > 0) {
                data = new byte[length];
                byte[] buffer = new byte[BUFFER];
                int destPos = 0;
                while (true) {
                    int readLen = is.read(buffer);
                    if (readLen <= 0) {
                        break;
                    }
                    System.arraycopy(buffer, 0, data, destPos, readLen);
                    destPos += readLen;
                }
            }
        }
        return data;
    }

    public long getRemoteFileSize(String urlStr) throws IOException {
        int fileLength = -1;
        HttpURLConnection conn = (HttpURLConnection) new URL(urlStr).openConnection();
        conn.addRequestProperty("User-Agent", USER_AGENT);
        conn.addRequestProperty("Accept-Encoding", Accept_Encoding);
        if (conn.getResponseCode() == 200) {
            InputStream inputStream = conn.getInputStream();
            fileLength = conn.getContentLength();
        }
        return (long) fileLength;
    }

    /* JADX INFO: Multiple debug info for r0v7 org.apache.http.HttpResponse: [D('httpClient' org.apache.http.client.HttpClient), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r3v2 int: [D('request' org.apache.http.client.methods.HttpGet), D('statusCode' int)] */
    /* JADX INFO: Multiple debug info for r0v10 org.apache.http.Header: [D('response' org.apache.http.HttpResponse), D('contentEncoding' org.apache.http.Header)] */
    /* JADX INFO: Multiple debug info for r8v10 byte[]: [D('dest' java.io.File), D('buffer' byte[])] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0113  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long download(java.lang.String r7, java.io.File r8, boolean r9) throws java.lang.Exception {
        /*
            java.lang.String r0 = com.yhiker.util.HttpUtils.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Download file: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r0, r1)
            r0 = 0
            r4 = -1
            if (r9 != 0) goto L_0x002c
            boolean r1 = r8.exists()
            if (r1 == 0) goto L_0x002c
            boolean r1 = r8.isFile()
            if (r1 == 0) goto L_0x002c
            r8.delete()
        L_0x002c:
            if (r9 == 0) goto L_0x0049
            boolean r1 = r8.exists()
            if (r1 == 0) goto L_0x0049
            boolean r1 = r8.exists()
            if (r1 == 0) goto L_0x0049
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00df, all -> 0x0156 }
            r1.<init>(r8)     // Catch:{ IOException -> 0x00df, all -> 0x0156 }
            int r0 = r1.available()     // Catch:{ IOException -> 0x015e, all -> 0x015a }
            if (r1 == 0) goto L_0x0049
            r1.close()
        L_0x0049:
            org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet
            r3.<init>(r7)
            java.lang.String r1 = "User-Agent"
            java.lang.String r2 = com.yhiker.util.HttpUtils.USER_AGENT
            r3.addHeader(r1, r2)
            java.lang.String r1 = "Accept-Encoding"
            java.lang.String r2 = "gzip, deflate"
            r3.addHeader(r1, r2)
            if (r0 <= 0) goto L_0x007c
            java.lang.String r1 = "RANGE"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r6 = "bytes="
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "-"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r3.addHeader(r1, r0)
        L_0x007c:
            org.apache.http.params.BasicHttpParams r1 = new org.apache.http.params.BasicHttpParams
            r1.<init>()
            r0 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r1, r0)
            r0 = 20000(0x4e20, float:2.8026E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r0)
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            r0.<init>(r1)
            r1 = 0
            r2 = 0
            org.apache.http.HttpResponse r0 = r0.execute(r3)     // Catch:{ all -> 0x0107 }
            org.apache.http.StatusLine r3 = r0.getStatusLine()     // Catch:{ all -> 0x0107 }
            int r3 = r3.getStatusCode()     // Catch:{ all -> 0x0107 }
            r6 = 200(0xc8, float:2.8E-43)
            if (r3 == r6) goto L_0x00a6
            r6 = 206(0xce, float:2.89E-43)
            if (r3 != r6) goto L_0x0164
        L_0x00a6:
            org.apache.http.HttpEntity r3 = r0.getEntity()     // Catch:{ all -> 0x0107 }
            java.io.InputStream r1 = r3.getContent()     // Catch:{ all -> 0x0107 }
            java.lang.String r3 = "Content-Encoding"
            org.apache.http.Header r0 = r0.getFirstHeader(r3)     // Catch:{ all -> 0x0107 }
            if (r0 == 0) goto L_0x0161
            java.lang.String r0 = r0.getValue()     // Catch:{ all -> 0x0107 }
            java.lang.String r3 = "gzip"
            boolean r0 = r0.equalsIgnoreCase(r3)     // Catch:{ all -> 0x0107 }
            if (r0 == 0) goto L_0x0161
            java.util.zip.GZIPInputStream r0 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0107 }
            r0.<init>(r1)     // Catch:{ all -> 0x0107 }
        L_0x00c7:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x0144 }
            r1.<init>(r8, r9)     // Catch:{ all -> 0x0144 }
            r8 = 8192(0x2000, float:1.14794E-41)
            byte[] r8 = new byte[r8]     // Catch:{ all -> 0x014a }
            r9 = 0
            r2 = r4
        L_0x00d2:
            int r9 = r0.read(r8)     // Catch:{ all -> 0x0150 }
            if (r9 <= 0) goto L_0x0117
            r4 = 0
            r1.write(r8, r4, r9)     // Catch:{ all -> 0x0150 }
            long r4 = (long) r9
            long r2 = r2 + r4
            goto L_0x00d2
        L_0x00df:
            r7 = move-exception
            r9 = r0
        L_0x00e1:
            java.lang.String r0 = com.yhiker.util.HttpUtils.TAG     // Catch:{ all -> 0x00fe }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00fe }
            r1.<init>()     // Catch:{ all -> 0x00fe }
            java.lang.String r2 = "Get local file size fail: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00fe }
            java.lang.String r8 = r8.getAbsolutePath()     // Catch:{ all -> 0x00fe }
            java.lang.StringBuilder r8 = r1.append(r8)     // Catch:{ all -> 0x00fe }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x00fe }
            android.util.Log.e(r0, r8)     // Catch:{ all -> 0x00fe }
            throw r7     // Catch:{ all -> 0x00fe }
        L_0x00fe:
            r7 = move-exception
            r8 = r7
            r7 = r9
        L_0x0101:
            if (r7 == 0) goto L_0x0106
            r7.close()
        L_0x0106:
            throw r8
        L_0x0107:
            r7 = move-exception
            r9 = r7
            r8 = r2
            r7 = r1
            r0 = r4
        L_0x010c:
            if (r8 == 0) goto L_0x0111
            r8.close()
        L_0x0111:
            if (r7 == 0) goto L_0x0116
            r7.close()
        L_0x0116:
            throw r9
        L_0x0117:
            r9 = r1
            r8 = r0
            r0 = r2
        L_0x011a:
            if (r9 == 0) goto L_0x011f
            r9.close()
        L_0x011f:
            if (r8 == 0) goto L_0x0124
            r8.close()
        L_0x0124:
            r8 = 0
            int r8 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r8 >= 0) goto L_0x0143
            java.lang.Exception r8 = new java.lang.Exception
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r0 = "Download file fail: "
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.StringBuilder r7 = r9.append(r7)
            java.lang.String r7 = r7.toString()
            r8.<init>(r7)
            throw r8
        L_0x0143:
            return r0
        L_0x0144:
            r7 = move-exception
            r9 = r7
            r8 = r2
            r7 = r0
            r0 = r4
            goto L_0x010c
        L_0x014a:
            r7 = move-exception
            r9 = r7
            r8 = r1
            r7 = r0
            r0 = r4
            goto L_0x010c
        L_0x0150:
            r7 = move-exception
            r9 = r7
            r8 = r1
            r7 = r0
            r0 = r2
            goto L_0x010c
        L_0x0156:
            r7 = move-exception
            r8 = r7
            r7 = r0
            goto L_0x0101
        L_0x015a:
            r7 = move-exception
            r8 = r7
            r7 = r1
            goto L_0x0101
        L_0x015e:
            r7 = move-exception
            r9 = r1
            goto L_0x00e1
        L_0x0161:
            r0 = r1
            goto L_0x00c7
        L_0x0164:
            r9 = r2
            r8 = r1
            r0 = r4
            goto L_0x011a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.util.HttpUtils.download(java.lang.String, java.io.File, boolean):long");
    }
}
