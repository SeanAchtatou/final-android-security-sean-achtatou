package com.yhiker.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

public class EncryptToolkit {
    private static final String CipherMode = "AES/ECB/PKCS5Padding";
    private static final String HEX = "0123456789ABCDEF";

    public static String encrypt(String seed, String cleartext) throws Exception {
        return toHex(encrypt(getRawKey(seed.getBytes()), cleartext.getBytes()));
    }

    public static String decrypt(String seed, String encrypted) throws Exception {
        return new String(decrypt(getRawKey(seed.getBytes()), toByte(encrypted)));
    }

    private static byte[] getRawKey(byte[] seed) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(seed);
        kgen.init(128, sr);
        return kgen.generateKey().getEncoded();
    }

    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance(CipherMode);
        cipher.init(1, skeySpec);
        return cipher.doFinal(clear);
    }

    private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance(CipherMode);
        cipher.init(2, skeySpec);
        return cipher.doFinal(encrypted);
    }

    public static void aesEnCrypt(String infilename, String outfilename, byte[] pwd) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128, new SecureRandom(pwd));
        SecretKeySpec key = new SecretKeySpec(kgen.generateKey().getEncoded(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(1, key);
        InputStream in = new FileInputStream(infilename);
        DataOutputStream out = new DataOutputStream(new FileOutputStream(outfilename));
        crypt(in, out, cipher);
        out.close();
        in.close();
    }

    public static void aesDeCrypt(String infilename, String outfilename, byte[] pwd) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128, new SecureRandom(pwd));
        SecretKeySpec key = new SecretKeySpec(kgen.generateKey().getEncoded(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(2, key);
        OutputStream out = new FileOutputStream(outfilename);
        DataInputStream in = new DataInputStream(new FileInputStream(infilename));
        crypt(in, out, cipher);
        in.close();
        out.close();
    }

    public static void crypt(InputStream in, OutputStream out, Cipher cipher) throws IOException, GeneralSecurityException {
        byte[] outBytes;
        int blockSize = cipher.getBlockSize();
        byte[] inBytes = new byte[blockSize];
        byte[] outBytes2 = new byte[cipher.getOutputSize(blockSize)];
        int inLength = 0;
        boolean more = true;
        while (more) {
            inLength = in.read(inBytes);
            if (inLength == blockSize) {
                out.write(outBytes2, 0, cipher.update(inBytes, 0, blockSize, outBytes2));
            } else {
                more = false;
            }
        }
        if (inLength > 0) {
            outBytes = cipher.doFinal(inBytes, 0, inLength);
        } else {
            outBytes = cipher.doFinal();
        }
        out.write(outBytes);
    }

    public static String crypt(String in, Cipher cipher) throws IOException, GeneralSecurityException {
        byte[] outBytes;
        StringBuffer out = new StringBuffer();
        int blockSize = cipher.getBlockSize();
        byte[] inBytes = new byte[blockSize];
        byte[] outBytes2 = new byte[cipher.getOutputSize(blockSize)];
        int inLength = 0;
        boolean more = true;
        while (more) {
            for (int i = 0; i < in.getBytes().length; i++) {
                System.out.println(in.getBytes().length * 8);
                if (in.getBytes().length >= i * blockSize) {
                    System.arraycopy(in, blockSize * 0, inBytes, 0, blockSize);
                    cipher.update(inBytes, 0, blockSize, outBytes2);
                    out.append(outBytes2);
                } else {
                    inLength = in.getBytes().length - (i * blockSize);
                    more = false;
                }
            }
        }
        if (inLength > 0) {
            outBytes = cipher.doFinal(inBytes, 0, inLength);
        } else {
            outBytes = cipher.doFinal();
        }
        out.append(outBytes);
        return out.toString();
    }

    public static String toHex(String txt) {
        return toHex(txt.getBytes());
    }

    public static String fromHex(String hex) {
        return new String(toByte(hex));
    }

    public static byte[] toByte(String hexString) {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++) {
            result[i] = Integer.valueOf(hexString.substring(i * 2, (i * 2) + 2), 16).byteValue();
        }
        return result;
    }

    public static String toHex(byte[] buf) {
        if (buf == null) {
            return "";
        }
        StringBuffer result = new StringBuffer(buf.length * 2);
        for (byte appendHex : buf) {
            appendHex(result, appendHex);
        }
        return result.toString();
    }

    private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b >> 4) & 15)).append(HEX.charAt(b & 15));
    }
}
