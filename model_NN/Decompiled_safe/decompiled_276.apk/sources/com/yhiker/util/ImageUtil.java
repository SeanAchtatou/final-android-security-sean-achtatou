package com.yhiker.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import com.yhiker.config.GuideConfig;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageUtil {
    public ExecutorService executorService = Executors.newCachedThreadPool();
    /* access modifiers changed from: private */
    public HashMap<String, Drawable> imgCache = new HashMap<>();

    public static Drawable loadBitmapByPlay(String cityCode, String ScenicPintCode) {
        Drawable imageDrawable = null;
        String rlPath = "0086/" + cityCode + "/img/" + ScenicPintCode + "_218-152.jpg";
        String iconPath = GuideConfig.getInitDataDir() + "/yhiker/data" + "/" + rlPath;
        String iconSdPath = GuideConfig.getRootDir() + "/yhiker/data" + "/" + rlPath;
        String iconUrl = "http://58.211.138.180:88/0420/" + rlPath;
        Log.i("loadimg wallact", iconPath);
        if (new File(iconSdPath).exists()) {
            return Drawable.createFromPath(iconSdPath);
        }
        if (new File(iconPath).exists()) {
            return Drawable.createFromPath(iconPath);
        }
        try {
            byte[] imgData = HttpUtils.invokeData(iconUrl);
            if (imgData != null && imgData.length > 0) {
                imageDrawable = new BitmapDrawable(BitmapFactory.decodeByteArray(imgData, 0, imgData.length));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageDrawable;
    }

    public static Drawable loadBitmapByWall(String cityCode, String ScenicPintCode) {
        Drawable imageDrawable = null;
        String rlPath = "0086/" + cityCode + "/img/" + ScenicPintCode + "_152-152.jpg";
        String iconPath = GuideConfig.getInitDataDir() + "/yhiker/data" + "/" + rlPath;
        String iconSdPath = GuideConfig.getRootDir() + "/yhiker/data" + "/" + rlPath;
        String iconUrl = "http://58.211.138.180:88/0420/" + rlPath;
        Log.i("loadimg wallact", iconPath);
        if (new File(iconSdPath).exists()) {
            return Drawable.createFromPath(iconSdPath);
        }
        if (new File(iconPath).exists()) {
            return Drawable.createFromPath(iconPath);
        }
        try {
            byte[] imgData = HttpUtils.invokeData(iconUrl);
            if (imgData != null && imgData.length > 0) {
                imageDrawable = new BitmapDrawable(BitmapFactory.decodeByteArray(imgData, 0, imgData.length));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageDrawable;
    }

    public static Bitmap loadBitmapByScenicCode(Context context, String cityCode, String ScenicCode) {
        Bitmap image = null;
        try {
            final String rlPath = "0086/" + cityCode + "/img/" + ScenicCode + ".jpg";
            final String iconPath = GuideConfig.getRootDir() + "/yhiker/data" + "/" + rlPath;
            boolean downFlag = true;
            if (new File(iconPath).exists()) {
                return BitmapFactory.decodeFile(iconPath);
            }
            final AssetManager am = context.getAssets();
            InputStream is = am.open(rlPath, 0);
            if (is != null) {
                downFlag = false;
                image = BitmapFactory.decodeStream(is);
                is.close();
            }
            final boolean downFlagf = downFlag;
            new Thread() {
                public void run() {
                    try {
                        if (downFlagf) {
                            HttpUtils.download("http://58.211.138.180:88/0420/" + rlPath, new File(iconPath), true);
                        } else {
                            FileUtils.copy(am.open(rlPath, 0), iconPath);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap loadBitmapByCityCode(Context context, String cityCode) {
        Bitmap image = null;
        try {
            final String rlPath = "0086/" + cityCode + "/img/" + cityCode + ".jpg";
            boolean downFlag = true;
            final String iconPath = GuideConfig.getRootDir() + "/yhiker/data" + "/" + rlPath;
            if (new File(iconPath).exists()) {
                return BitmapFactory.decodeFile(iconPath);
            }
            final AssetManager am = context.getAssets();
            InputStream is = am.open(rlPath, 0);
            if (is != null) {
                downFlag = false;
                image = BitmapFactory.decodeStream(is);
                is.close();
            }
            final boolean downFlagf = downFlag;
            new Thread() {
                public void run() {
                    try {
                        if (downFlagf) {
                            HttpUtils.download("http://58.211.138.180:88/0420/" + rlPath, new File(iconPath), true);
                        } else {
                            FileUtils.copy(am.open(rlPath, 0), iconPath);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap loadBitmapFromFile(Context context, String fileName) {
        Bitmap image = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            image = BitmapFactory.decodeStream(is, null, options);
            is.close();
            return image;
        } catch (Exception e) {
            e.printStackTrace();
            return image;
        }
    }

    public void loadImage(final String uri, final ImageView imgV, boolean iconed, Handler mHandler) {
        this.executorService.submit(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:10:0x00d5 A[Catch:{ Exception -> 0x00e8 }] */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x0148 A[SYNTHETIC, Splitter:B:26:0x0148] */
            /* JADX WARNING: Removed duplicated region for block: B:35:? A[Catch:{ Exception -> 0x00e8 }, RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:5:0x0055 A[Catch:{ Exception -> 0x00e8 }] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r17 = this;
                    android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x00e8 }
                    r4.<init>()     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r14 = "loadimg wallact"
                    java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
                    r15.<init>()     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r16 = com.yhiker.config.GuideConfig.getRootDir()     // Catch:{ Exception -> 0x00e8 }
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r16 = "/yhiker/data"
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00e8 }
                    r0 = r17
                    java.lang.String r0 = r3     // Catch:{ Exception -> 0x00e8 }
                    r16 = r0
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x00e8 }
                    android.util.Log.i(r14, r15)     // Catch:{ Exception -> 0x00e8 }
                    r0 = r17
                    com.yhiker.util.ImageUtil r0 = com.yhiker.util.ImageUtil.this     // Catch:{ Exception -> 0x00e8 }
                    r14 = r0
                    java.util.HashMap r14 = r14.imgCache     // Catch:{ Exception -> 0x00e8 }
                    r0 = r17
                    java.lang.String r0 = r3     // Catch:{ Exception -> 0x00e8 }
                    r15 = r0
                    boolean r14 = r14.containsKey(r15)     // Catch:{ Exception -> 0x00e8 }
                    if (r14 == 0) goto L_0x005e
                    r0 = r17
                    com.yhiker.util.ImageUtil r0 = com.yhiker.util.ImageUtil.this     // Catch:{ Exception -> 0x00e8 }
                    r14 = r0
                    java.util.HashMap r14 = r14.imgCache     // Catch:{ Exception -> 0x00e8 }
                    r0 = r17
                    java.lang.String r0 = r3     // Catch:{ Exception -> 0x00e8 }
                    r15 = r0
                    java.lang.Object r4 = r14.get(r15)     // Catch:{ Exception -> 0x00e8 }
                    android.graphics.drawable.Drawable r4 = (android.graphics.drawable.Drawable) r4     // Catch:{ Exception -> 0x00e8 }
                L_0x0053:
                    if (r4 == 0) goto L_0x005d
                    r0 = r17
                    android.widget.ImageView r0 = r4     // Catch:{ Exception -> 0x00e8 }
                    r14 = r0
                    r14.setBackgroundDrawable(r4)     // Catch:{ Exception -> 0x00e8 }
                L_0x005d:
                    return
                L_0x005e:
                    java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x00e8 }
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
                    r14.<init>()     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r15 = com.yhiker.config.GuideConfig.getRootDir()     // Catch:{ Exception -> 0x00e8 }
                    java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r15 = "/yhiker/data"
                    java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x00e8 }
                    r0 = r17
                    java.lang.String r0 = r3     // Catch:{ Exception -> 0x00e8 }
                    r15 = r0
                    java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x00e8 }
                    r9.<init>(r14)     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r14 = "loadimg wallact"
                    java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
                    r15.<init>()     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r16 = com.yhiker.config.GuideConfig.getRootDir()     // Catch:{ Exception -> 0x00e8 }
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r16 = "/yhiker/data"
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00e8 }
                    r0 = r17
                    java.lang.String r0 = r3     // Catch:{ Exception -> 0x00e8 }
                    r16 = r0
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x00e8 }
                    android.util.Log.i(r14, r15)     // Catch:{ Exception -> 0x00e8 }
                    boolean r14 = r9.exists()     // Catch:{ Exception -> 0x00e8 }
                    if (r14 == 0) goto L_0x00ef
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
                    r14.<init>()     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r15 = com.yhiker.config.GuideConfig.getRootDir()     // Catch:{ Exception -> 0x00e8 }
                    java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r15 = "/yhiker/data"
                    java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x00e8 }
                    r0 = r17
                    java.lang.String r0 = r3     // Catch:{ Exception -> 0x00e8 }
                    r15 = r0
                    java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x00e8 }
                    java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x00e8 }
                    android.graphics.drawable.Drawable r4 = android.graphics.drawable.Drawable.createFromPath(r14)     // Catch:{ Exception -> 0x00e8 }
                L_0x00d3:
                    if (r4 == 0) goto L_0x0053
                    r0 = r17
                    com.yhiker.util.ImageUtil r0 = com.yhiker.util.ImageUtil.this     // Catch:{ Exception -> 0x00e8 }
                    r14 = r0
                    java.util.HashMap r14 = r14.imgCache     // Catch:{ Exception -> 0x00e8 }
                    r0 = r17
                    java.lang.String r0 = r3     // Catch:{ Exception -> 0x00e8 }
                    r15 = r0
                    r14.put(r15, r4)     // Catch:{ Exception -> 0x00e8 }
                    goto L_0x0053
                L_0x00e8:
                    r14 = move-exception
                    r5 = r14
                    r5.printStackTrace()
                    goto L_0x005d
                L_0x00ef:
                    r10 = 0
                    r1 = 0
                    java.net.URL r11 = new java.net.URL     // Catch:{ Exception -> 0x014e }
                    java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x014e }
                    r14.<init>()     // Catch:{ Exception -> 0x014e }
                    java.lang.String r15 = "http://58.211.138.180:88/0420/0086"
                    java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x014e }
                    r0 = r17
                    java.lang.String r0 = r3     // Catch:{ Exception -> 0x014e }
                    r15 = r0
                    java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ Exception -> 0x014e }
                    java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x014e }
                    r11.<init>(r14)     // Catch:{ Exception -> 0x014e }
                    java.net.URLConnection r2 = r11.openConnection()     // Catch:{ Exception -> 0x0154 }
                    java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Exception -> 0x0154 }
                    r14 = 1
                    r2.setDoInput(r14)     // Catch:{ Exception -> 0x0154 }
                    r14 = 15000(0x3a98, float:2.102E-41)
                    r2.setConnectTimeout(r14)     // Catch:{ Exception -> 0x0154 }
                    r2.connect()     // Catch:{ Exception -> 0x0154 }
                    java.io.InputStream r7 = r2.getInputStream()     // Catch:{ Exception -> 0x0154 }
                    int r8 = r2.getContentLength()     // Catch:{ Exception -> 0x0154 }
                    r14 = -1
                    if (r8 == r14) goto L_0x0145
                    byte[] r6 = new byte[r8]     // Catch:{ Exception -> 0x0154 }
                    r14 = 512(0x200, float:7.175E-43)
                    byte[] r13 = new byte[r14]     // Catch:{ Exception -> 0x0154 }
                    r12 = 0
                    r3 = 0
                L_0x0133:
                    int r12 = r7.read(r13)     // Catch:{ Exception -> 0x0154 }
                    if (r12 <= 0) goto L_0x013f
                    r14 = 0
                    java.lang.System.arraycopy(r13, r14, r6, r3, r12)     // Catch:{ Exception -> 0x0154 }
                    int r3 = r3 + r12
                    goto L_0x0133
                L_0x013f:
                    r14 = 0
                    int r15 = r6.length     // Catch:{ Exception -> 0x0154 }
                    android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeByteArray(r6, r14, r15)     // Catch:{ Exception -> 0x0154 }
                L_0x0145:
                    r10 = r11
                L_0x0146:
                    if (r1 == 0) goto L_0x00d3
                    android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable     // Catch:{ Exception -> 0x00e8 }
                    r4.<init>(r1)     // Catch:{ Exception -> 0x00e8 }
                    goto L_0x00d3
                L_0x014e:
                    r14 = move-exception
                    r5 = r14
                L_0x0150:
                    r5.printStackTrace()     // Catch:{ Exception -> 0x00e8 }
                    goto L_0x0146
                L_0x0154:
                    r14 = move-exception
                    r5 = r14
                    r10 = r11
                    goto L_0x0150
                */
                throw new UnsupportedOperationException("Method not decompiled: com.yhiker.util.ImageUtil.AnonymousClass3.run():void");
            }
        });
    }
}
