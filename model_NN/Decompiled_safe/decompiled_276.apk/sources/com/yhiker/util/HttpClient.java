package com.yhiker.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class HttpClient {
    public static String doGet(String urlString) throws Exception {
        try {
            URLConnection uc = new URL(urlString).openConnection();
            uc.setRequestProperty("User-Agent", "Mozilla/5.0");
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            String line = in.readLine();
            in.close();
            return line.trim();
        } catch (IOException e) {
            throw new Exception(e);
        }
    }

    public static String doPostRequest(String urlString) throws Exception {
        try {
            URLConnection uc = new URL(urlString).openConnection();
            uc.setRequestProperty("User-Agent", "Mozilla/5.0");
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            String line = in.readLine();
            in.close();
            return line.trim();
        } catch (IOException e) {
            throw new Exception(e);
        }
    }

    /* JADX INFO: Multiple debug info for r2v1 org.apache.http.client.methods.HttpPost: [D('params' org.apache.http.params.HttpParams), D('httppost' org.apache.http.client.methods.HttpPost)] */
    /* JADX INFO: Multiple debug info for r9v9 java.lang.Object[]: [D('urlString' java.lang.String), D('keys' java.lang.Object[])] */
    /* JADX INFO: Multiple debug info for r9v12 org.apache.http.HttpEntity: [D('entity' org.apache.http.HttpEntity), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r9v13 java.io.InputStream: [D('entity' org.apache.http.HttpEntity), D('content' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r10v4 javax.xml.parsers.DocumentBuilder: [D('dbf' javax.xml.parsers.DocumentBuilderFactory), D('db' javax.xml.parsers.DocumentBuilder)] */
    /* JADX INFO: Multiple debug info for r9v14 org.w3c.dom.Document: [D('content' java.io.InputStream), D('doc' org.w3c.dom.Document)] */
    /* JADX INFO: Multiple debug info for r9v15 java.lang.Object[]: [D('keys' java.lang.Object[]), D('arr$' java.lang.Object[])] */
    public static Document doPostToXml(String urlString, Map valueMap) throws Exception {
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, 15000);
        HttpConnectionParams.setSocketBufferSize(params, 8192);
        DefaultHttpClient httpclient = new DefaultHttpClient(params);
        HttpPost httppost = new HttpPost(urlString);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            Object[] keys = valueMap.keySet().toArray();
            if (keys != null) {
                for (Object key : keys) {
                    if (valueMap.get(key) != null && !"".equals(valueMap.get(key).toString())) {
                        nameValuePairs.add(new BasicNameValuePair(key.toString(), valueMap.get(key).toString()));
                    }
                }
            }
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(httpclient.execute(httppost).getEntity().getContent());
            httpclient.getConnectionManager().shutdown();
            return doc;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            httpclient.getConnectionManager().shutdown();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            httpclient.getConnectionManager().shutdown();
            return null;
        } catch (Throwable th) {
            httpclient.getConnectionManager().shutdown();
            throw th;
        }
    }

    public static Document doGetToXml(String urlString) {
        IOException e;
        SAXException e2;
        ParserConfigurationException e3;
        Document doc = null;
        try {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
                conn.setDoInput(true);
                conn.connect();
                doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(conn.getInputStream());
            } catch (ParserConfigurationException e4) {
                e3 = e4;
                e3.printStackTrace();
                return doc;
            } catch (SAXException e5) {
                e2 = e5;
                e2.printStackTrace();
                return doc;
            } catch (IOException e6) {
                e = e6;
                e.printStackTrace();
                return doc;
            }
        } catch (ParserConfigurationException e7) {
            e3 = e7;
            e3.printStackTrace();
            return doc;
        } catch (SAXException e8) {
            e2 = e8;
            e2.printStackTrace();
            return doc;
        } catch (IOException e9) {
            e = e9;
            e.printStackTrace();
            return doc;
        }
        return doc;
    }

    /* JADX INFO: Multiple debug info for r8v5 java.lang.Object[]: [D('keys' java.lang.Object[]), D('params' org.apache.http.params.HttpParams)] */
    /* JADX INFO: Multiple debug info for r9v2 java.lang.String: [D('retSrc' java.lang.String), D('se' org.apache.http.entity.StringEntity)] */
    /* JADX INFO: Multiple debug info for r8v11 java.lang.Object[]: [D('keys' java.lang.Object[]), D('arr$' java.lang.Object[])] */
    public static JSONObject doPostToJson(String urlString, String urlImple, Map valueMap) throws Exception {
        HttpPost request = new HttpPost(urlString);
        request.addHeader("content-type", "application/json");
        request.addHeader("Accept", "application/json");
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, 15000);
        HttpConnectionParams.setSocketBufferSize(params, 8192);
        DefaultHttpClient httpclient = new DefaultHttpClient(params);
        JSONObject injson = new JSONObject();
        Object[] keys = valueMap.keySet().toArray();
        if (keys != null) {
            for (Object key : keys) {
                if (valueMap.get(key) != null && !"".equals(valueMap.get(key).toString())) {
                    injson.put(key.toString(), valueMap.get(key).toString());
                }
            }
        }
        JSONObject rootJson = new JSONObject();
        rootJson.put(urlImple, injson);
        request.setEntity(new StringEntity(rootJson.toString(), "UTF-8"));
        return new JSONObject((String) EntityUtils.toString(httpclient.execute(request).getEntity()));
    }
}
