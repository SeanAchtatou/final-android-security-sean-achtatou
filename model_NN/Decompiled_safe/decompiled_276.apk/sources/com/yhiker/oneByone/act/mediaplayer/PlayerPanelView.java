package com.yhiker.oneByone.act.mediaplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import com.yhiker.config.ConfigHp;
import com.yhiker.oneByone.act.GlobalApp;
import com.yhiker.playmate.R;
import java.util.Formatter;
import java.util.Locale;

public class PlayerPanelView extends RelativeLayout implements View.OnClickListener {
    public static final int PANEL_TRANSANIM_DURATION = 500;
    private static String audioNames = null;
    public static Drawable boardScienceImage = null;
    public static String curAttCode = "";
    private static int durationInMs;
    private static String durationString;
    public static boolean finished = false;
    public static boolean imgBarHidden = false;
    /* access modifiers changed from: private */
    public static TextView mNowPosTxt = null;
    public static ImageButton mPlayButton = null;
    private static SeekBar mProgress = null;
    public static ImageButton mSpotButton = null;
    private static String mp3Path = "";
    private static String mp3Url = "";
    public static boolean musicBarHidden = false;
    public static boolean pause;
    protected static MyReceiver receiver;
    private static StringBuilder sFormatBuilder = new StringBuilder();
    private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
    private static final Object[] sTimeArgs = new Object[5];
    private ScrollView explanationScroll;
    private ImageButton mAnchorButton = null;
    private TranslateAnimation mPanelTransAnim = null;
    private Resources mResource = null;
    public ImageButton mSetsailButton = null;
    private ImageView mSpotimageBoard = null;
    private ImageView mSpotimageView = null;
    private Window mWindow = null;

    public PlayerPanelView(Context context) {
        super(context);
    }

    public PlayerPanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void initPlayerPanel(Window window) {
        Log.i(getClass().getSimpleName(), "initPlayerPanel is called");
        this.mWindow = window;
        this.mResource = this.mWindow.getContext().getResources();
        if (receiver == null) {
            receiver = new MyReceiver();
            getContext().registerReceiver(receiver, new IntentFilter(ConfigHp.oneByone_play));
        }
        findPanelViews(window);
        loadButtonListener();
    }

    public void refreshPlayerPanel() {
        Log.i(getClass().getSimpleName(), "refreshPlayerPanel is called");
        initPanelViews();
        initMediaPlayer();
    }

    public void initPlayerPanelForMap(Window window) {
        Log.i(getClass().getSimpleName(), "initPlayerPanelForMap initPlayerPanel is called");
        this.mWindow = window;
        this.mResource = this.mWindow.getContext().getResources();
        this.mPanelTransAnim = null;
        findPanelViews(window);
        initPanelViewsForMap();
        loadButtonListener();
        initMediaPlayer();
    }

    private void findPanelViews(Window window) {
        mPlayButton = (ImageButton) window.findViewById(R.id.player_playbtn);
        mProgress = (SeekBar) window.findViewById(R.id.player_progresslayout);
        mNowPosTxt = (TextView) window.findViewById(R.id.player_nowpostxt);
        mSpotButton = (ImageButton) window.findViewById(R.id.player_spotbtn);
        this.mAnchorButton = (ImageButton) window.findViewById(R.id.player_anchorbtn);
        this.mSetsailButton = (ImageButton) window.findViewById(R.id.player_setsailbtn);
        this.mSpotimageView = (ImageView) window.findViewById(R.id.player_spotview);
        this.mSpotimageBoard = (ImageView) window.findViewById(R.id.player_override);
    }

    private void initPanelViews() {
        if (curAttCode.equals(GlobalApp.curAttCode)) {
            if (PlayerService.pause) {
                mPlayButton.setBackgroundResource(R.drawable.ppanelplayselector);
            } else {
                mPlayButton.setBackgroundResource(R.drawable.ppanelpauseselector);
            }
            if (musicBarHidden) {
                mAnchorButtonClickListener(this.mWindow);
            } else {
                mSetsailButtonClickListener(this.mWindow);
            }
            if (imgBarHidden) {
                mSpotButtonClickListenerHidden();
            } else {
                mSpotButtonClickListenerNoHidden();
            }
        } else {
            mPlayButton.setBackgroundResource(R.drawable.ppanelpauseselector);
            mSpotButtonClickListenerNoHidden();
        }
        this.mAnchorButton.setBackgroundResource(R.drawable.ppanelanchorselector);
        this.mSetsailButton.setBackgroundResource(R.drawable.ppanelsetsailselector);
        this.mSetsailButton.setVisibility(8);
    }

    private void initPanelViewsForMap() {
        if (curAttCode.equals(GlobalApp.curAttCode)) {
            if (pause) {
                mPlayButton.setBackgroundResource(R.drawable.ppanelplay);
            } else {
                mPlayButton.setBackgroundResource(R.drawable.ppanelpause);
            }
            if (musicBarHidden) {
                mAnchorButtonClickListener(this.mWindow);
            }
            if (imgBarHidden) {
                mSpotButtonClickListenerHidden();
            } else {
                mSpotButtonClickListenerNoHidden();
            }
        } else {
            pause = false;
            mSpotButtonClickListenerNoHidden();
        }
        mPlayButton.setBackgroundResource(R.drawable.ppanelplayselector);
        this.mAnchorButton.setBackgroundResource(R.drawable.ppanelanchorselector);
        this.mSetsailButton.setBackgroundResource(R.drawable.ppanelsetsailselector);
        this.mSetsailButton.setVisibility(8);
    }

    private void loadButtonListener() {
        mPlayButton.setOnClickListener(this);
        mSpotButton.setOnClickListener(this);
        this.mAnchorButton.setOnClickListener(this);
        this.mSetsailButton.setOnClickListener(this);
    }

    private void initMediaPlayer() {
        if (boardScienceImage != null) {
            this.mSpotimageView.setImageDrawable(boardScienceImage);
            mSpotButton.setImageDrawable(boardScienceImage);
        }
    }

    public void initExplanationScroll(Window window) {
        this.explanationScroll = (ScrollView) window.findViewById(R.id.explanationScroll);
    }

    public void btnPlaPauClk() {
        Log.i(getClass().getSimpleName(), "btnPlaPauClk is called");
        pauseMP3();
        if (PlayerService.pause) {
            mPlayButton.setBackgroundResource(R.drawable.ppanelpauseselector);
        } else {
            mPlayButton.setBackgroundResource(R.drawable.ppanelplayselector);
        }
    }

    public void btnPlaPauClk(String mp3Path2, String audioNames2) {
        Log.i(getClass().getSimpleName(), "btnPlaPauClk is called");
        mp3Path = mp3Path2;
        audioNames = audioNames2;
        String mp3Url2 = mp3Path + audioNames;
        if (!curAttCode.equals(GlobalApp.curAttCode)) {
            Log.i(getClass().getSimpleName(), "priSeekUpdate is called");
            durationString = makeTimeString(0);
            mProgress.setProgress(0);
            Log.i("mediaPlayer.getCurrentPosition():", "0");
            String curStr = makeTimeString(0);
            Log.i("now playing at:", curStr);
            mNowPosTxt.setText(curStr + "/" + durationString);
            playAutio(mp3Url2);
            mp3Url = mp3Url2;
        }
    }

    private void playAutio(String nowPlayingUrl) {
        Log.i(getClass().getSimpleName(), "playAutio is called");
        Log.i(getClass().getSimpleName(), "nowPlayingUrl=" + nowPlayingUrl);
        curAttCode = GlobalApp.curAttCode;
        try {
            mPlayButton.setBackgroundResource(R.drawable.ppanelpauseselector);
            PlayerService.curAttCode = curAttCode;
            Intent intent = new Intent();
            intent.putExtra("MSG", 1);
            intent.setClass(getContext(), PlayerService.class);
            getContext().startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pauseMP3() {
        Log.i(getClass().getSimpleName(), "pauseMP3 is called");
        Intent intent = new Intent();
        intent.putExtra("MSG", 2);
        intent.setClass(getContext(), PlayerService.class);
        getContext().startService(intent);
    }

    private void mSpotButtonClickListenerHidden() {
        this.mSpotimageView.setVisibility(8);
        this.mSpotimageBoard.setVisibility(8);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.mSpotimageView.getLayoutParams();
        RelativeLayout.LayoutParams layoutBoardParams = (RelativeLayout.LayoutParams) this.mSpotimageBoard.getLayoutParams();
        layoutParams.height = 0;
        layoutBoardParams.height = 0;
        this.mSpotimageView.setLayoutParams(layoutParams);
        this.mSpotimageBoard.setLayoutParams(layoutBoardParams);
        if (this.explanationScroll != null) {
            RelativeLayout.LayoutParams explanationParams = (RelativeLayout.LayoutParams) this.explanationScroll.getLayoutParams();
            explanationParams.topMargin = 100;
            this.explanationScroll.setLayoutParams(explanationParams);
        }
    }

    private void mSpotButtonClickListenerNoHidden() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.mSpotimageView.getLayoutParams();
        RelativeLayout.LayoutParams layoutBoardParams = (RelativeLayout.LayoutParams) this.mSpotimageBoard.getLayoutParams();
        layoutParams.height = 125;
        layoutBoardParams.height = 125;
        this.mSpotimageView.setLayoutParams(layoutParams);
        this.mSpotimageBoard.setLayoutParams(layoutBoardParams);
        this.mSpotimageView.setVisibility(0);
        this.mSpotimageBoard.setVisibility(0);
        if (this.explanationScroll != null) {
            RelativeLayout.LayoutParams explanationParams = (RelativeLayout.LayoutParams) this.explanationScroll.getLayoutParams();
            explanationParams.topMargin = 220;
            this.explanationScroll.setLayoutParams(explanationParams);
        }
    }

    private void mSpotButtonClickListener() {
        Log.i(getClass().getSimpleName(), "mSpotButtonClickListener is called");
        if (this.mSpotimageView.getVisibility() == 0) {
            mSpotButtonClickListenerHidden();
            imgBarHidden = true;
            return;
        }
        mSpotButtonClickListenerNoHidden();
        imgBarHidden = false;
    }

    public void mAnchorButtonClickListener(Window window) {
        Log.i(getClass().getSimpleName(), "mAnchorButtonClickListener is called");
        int w = window.getWindowManager().getDefaultDisplay().getWidth();
        musicBarHidden = true;
        this.mPanelTransAnim = new TranslateAnimation(0.0f, (float) w, 0.0f, 0.0f);
        this.mPanelTransAnim.setDuration(500);
        this.mPanelTransAnim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                PlayerPanelView.this.PanelPopup(false);
                PlayerPanelView.this.mSetsailButton.setVisibility(0);
            }
        });
        startAnimation(this.mPanelTransAnim);
    }

    public void mSetsailButtonClickListener(Window window) {
        int w = window.getWindowManager().getDefaultDisplay().getWidth();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) getLayoutParams();
        layoutParams.rightMargin = 0;
        musicBarHidden = false;
        setLayoutParams(layoutParams);
        this.mPanelTransAnim = new TranslateAnimation((float) w, 0.0f, 0.0f, 0.0f);
        this.mPanelTransAnim.setDuration(500);
        this.mPanelTransAnim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                PlayerPanelView.this.PanelPopup(true);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }
        });
        startAnimation(this.mPanelTransAnim);
        this.mSetsailButton.setVisibility(8);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.player_setsailbtn /*2131361814*/:
                mSetsailButtonClickListener(this.mWindow);
                return;
            case R.id.player_playbtn /*2131361839*/:
                Log.i("PlayerPanelView", "setOnClickListener");
                btnPlaPauClk();
                return;
            case R.id.player_spotbtn /*2131361843*/:
                mSpotButtonClickListener();
                return;
            case R.id.player_anchorbtn /*2131361844*/:
                mAnchorButtonClickListener(this.mWindow);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void PanelPopup(boolean b) {
        if (this.explanationScroll != null) {
            RelativeLayout.LayoutParams explanationParams = (RelativeLayout.LayoutParams) this.explanationScroll.getLayoutParams();
            if (b) {
                setVisibility(0);
                explanationParams.topMargin = 220;
                this.explanationScroll.setLayoutParams(explanationParams);
                return;
            }
            setVisibility(8);
            explanationParams.topMargin = 50;
            this.explanationScroll.setLayoutParams(explanationParams);
        } else if (b) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void priSeekUpdate() {
        Log.i(getClass().getSimpleName(), "priSeekUpdate is called");
        finished = PlayerService.finished;
        durationInMs = PlayerService.duration;
        durationString = makeTimeString((long) (durationInMs / 1000));
        mProgress.setProgress((int) ((((float) PlayerService.currentPosition) / ((float) durationInMs)) * 100.0f));
        secSeekUpdate(PlayerService.secCurrentPosition);
        Log.i("mediaPlayer.getCurrentPosition():", PlayerService.currentPosition + "");
        String curStr = makeTimeString((long) (PlayerService.currentPosition / 1000));
        Log.i("now playing at:", curStr);
        mNowPosTxt.setText(curStr + "/" + durationString);
        Log.i(getClass().getSimpleName(), "PlayerService.finished=" + PlayerService.finished);
        if (PlayerService.finished) {
            Log.i(getClass().getSimpleName(), "R.drawable.ppanelplayselector=" + PlayerService.finished);
            mPlayButton.setBackgroundResource(R.drawable.ppanelplayselector);
        }
    }

    private void secSeekUpdate(int percent) {
        mProgress.setSecondaryProgress(percent);
    }

    private void sonPlayFinish() {
        Log.i(getClass().getSimpleName(), "sonPlayFinish is called");
        mPlayButton.setBackgroundResource(R.drawable.ppanelplayselector);
    }

    private String makeTimeString(long secs) {
        String durationformat = this.mResource.getString(secs < 3600 ? R.string.durationformatshort : R.string.durationformatlong);
        sFormatBuilder.setLength(0);
        Object[] timeArgs = sTimeArgs;
        timeArgs[0] = Long.valueOf(secs / 3600);
        timeArgs[1] = Long.valueOf(secs / 60);
        timeArgs[2] = Long.valueOf((secs / 60) % 60);
        timeArgs[3] = Long.valueOf(secs);
        timeArgs[4] = Long.valueOf(secs % 60);
        return sFormatter.format(durationformat, timeArgs).toString();
    }

    public class MyReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Log.i("MyReceiver", "duration" + PlayerService.duration + " currentPosition=" + PlayerService.currentPosition);
            System.out.println("OnReceiver");
            if (PlayerService.currentPosition > 0) {
                PlayerPanelView.this.priSeekUpdate();
            } else if (!"".equals(PlayerService.bufferMsg)) {
                PlayerPanelView.mNowPosTxt.setText(PlayerService.bufferMsg);
                PlayerService.bufferMsg = "";
            } else if (!"".equals(PlayerService.error)) {
                PlayerService.error = "";
            }
        }

        public MyReceiver() {
        }
    }
}
