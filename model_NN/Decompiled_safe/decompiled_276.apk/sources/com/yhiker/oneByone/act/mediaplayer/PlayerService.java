package com.yhiker.oneByone.act.mediaplayer;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.bouncycastle.asn1.x509.DisplayText;

public class PlayerService extends Service implements Runnable, MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnBufferingUpdateListener {
    public static final int PAUSE = 2;
    public static final int PAUSEFORTEl = 3;
    public static final int PLAY_MAG = 1;
    public static final int STARTORTEl = 4;
    public static String bufferMsg = "";
    public static String curAttCode = "";
    public static int currentPosition;
    public static int duration;
    public static String error = "";
    public static boolean finished = false;
    public static MediaPlayer mMediaPlayer = null;
    public static String oldcurAttCode = "";
    public static boolean pause = false;
    public static int secCurrentPosition;
    private int MSG;
    private OnPlayFinishedListener mOnPlayFinishedListener = null;
    private OnPlayStateChangedListener mOnPlayStateChangedListener = null;

    public interface OnPlayFinishedListener {
        boolean OnPlayFinished();
    }

    public interface OnPlayStateChangedListener {
        boolean OnPlayStateChanged(boolean z);
    }

    private final class PhoneListener extends PhoneStateListener {
        private PhoneListener() {
        }

        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case 0:
                    PlayerService.this.playForTel();
                    return;
                case 1:
                    PlayerService.this.pauseForTel();
                    return;
                case 2:
                    PlayerService.this.pauseForTel();
                    return;
                default:
                    return;
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(3);
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnBufferingUpdateListener(this);
        mMediaPlayer.setOnCompletionListener(this);
        ((TelephonyManager) getSystemService("phone")).listen(new PhoneListener(), 32);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        System.out.println("service onDestroy");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return 0;
        }
        this.MSG = intent.getIntExtra("MSG", 1);
        if (this.MSG == 1 && !curAttCode.equals(oldcurAttCode)) {
            oldcurAttCode = curAttCode;
            String audioa = "/yhiker/data/0086/" + GuideConfig.curCityCode + "/audio/" + oldcurAttCode + "_32Kbps.m4a";
            String mp3 = GuideConfig.getRootDir() + ("/yhiker/data/0086/" + GuideConfig.curCityCode + "/audio/" + oldcurAttCode + "_24Kbps.mp3");
            String mp3sd = GuideConfig.getRootDir() + audioa;
            String m4a = GuideConfig.getInitDataDir() + audioa;
            if (new File(mp3sd).exists()) {
                playMusic(mp3sd);
            } else if (new File(mp3).exists()) {
                playMusic(mp3);
            } else if (new File(m4a).exists()) {
                playMusicTy(m4a);
            } else {
                playMusic("http://58.211.138.180:88/0420/0086/" + GuideConfig.curCityCode + "/audio/" + oldcurAttCode + "_24Kbps.mp3");
            }
        }
        if (this.MSG == 2) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            } else {
                mMediaPlayer.start();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public void playMusicTy(String mp3Path) {
        try {
            Log.i("PlayerService", "mp path=" + mp3Path);
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(new FileInputStream(mp3Path).getFD());
            mMediaPlayer.prepare();
            mMediaPlayer.start();
            new Thread(this).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playMusic(String mp3Path) {
        try {
            Log.i("PlayerService", "mp path=" + mp3Path);
            mMediaPlayer.reset();
            if (mp3Path.startsWith("/")) {
                mMediaPlayer.setDataSource(new FileInputStream(mp3Path).getFD());
                mMediaPlayer.prepareAsync();
            } else {
                mMediaPlayer.setDataSource(mp3Path);
                mMediaPlayer.prepareAsync();
                bufferMsg = "缓冲中..";
            }
            new Thread(this).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        boolean z;
        finished = false;
        currentPosition = 0;
        secCurrentPosition = 0;
        duration = mMediaPlayer.getDuration();
        Intent cityActIntent = new Intent(ConfigHp.oneByone_play);
        sendBroadcast(cityActIntent);
        while (mMediaPlayer != null && currentPosition < duration) {
            try {
                Thread.sleep(1000);
                if (mMediaPlayer != null) {
                    currentPosition = mMediaPlayer.getCurrentPosition();
                    if (!mMediaPlayer.isPlaying()) {
                        z = true;
                    } else {
                        z = false;
                    }
                    pause = z;
                    finished = false;
                    duration = mMediaPlayer.getDuration();
                    sendBroadcast(cityActIntent);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        finished = true;
        sendBroadcast(cityActIntent);
    }

    public void onCompletion(MediaPlayer mp) {
        finished = true;
    }

    public void onBufferingUpdate(MediaPlayer arg0, int arg1) {
        secCurrentPosition = arg1;
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append("Media Player Error: ");
        switch (what) {
            case 1:
                sb.append("Unknown");
                break;
            case 100:
                sb.append("Server Died");
                break;
            case DisplayText.DISPLAY_TEXT_MAXIMUM_SIZE /*200*/:
                sb.append("Not Valid for Progressive Playback");
                break;
            default:
                sb.append(" Non standard (");
                sb.append(what);
                sb.append(")");
                break;
        }
        sb.append(" (" + what + ") ");
        sb.append(extra);
        error = "";
        Intent cityActIntent = new Intent(ConfigHp.oneByone_play);
        currentPosition = mMediaPlayer.getCurrentPosition();
        if (!mMediaPlayer.isPlaying()) {
            z = true;
        } else {
            z = false;
        }
        pause = z;
        if (mMediaPlayer.isPlaying()) {
            finished = false;
        }
        duration = mMediaPlayer.getDuration();
        sendBroadcast(cityActIntent);
        return true;
    }

    public void onPrepared(MediaPlayer mp) {
        mp.start();
    }

    public void pauseForTel() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
            pause = true;
        }
    }

    public void playForTel() {
        if (mMediaPlayer == null) {
            pause = false;
            return;
        }
        if (pause) {
            mMediaPlayer.start();
        }
        pause = false;
    }

    public void SetOnPlayFinishedListener(OnPlayFinishedListener l) {
        this.mOnPlayFinishedListener = l;
    }

    public void SetOnPlayStateChangedListener(OnPlayStateChangedListener l) {
        this.mOnPlayStateChangedListener = l;
    }
}
