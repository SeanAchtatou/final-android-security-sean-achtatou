package com.yhiker.oneByone.act;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.bo.FreeServer;
import com.yhiker.oneByone.bo.UserService;
import com.yhiker.pay.NetXMLDataFactory;
import com.yhiker.playmate.R;
import com.yhiker.util.DialogUtil;
import com.yhiker.util.HttpClient;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Document;

public class RegAct extends BaseAct {
    public GlobalApp gApp;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    private ProgressDialog mProgress = null;
    /* access modifiers changed from: private */
    public NetXMLDataFactory netXMLDataFactory = null;
    private String regUrl = "";
    /* access modifiers changed from: private */
    public Document reg_doc = null;
    public String reginfo;
    private String[] suffixs = {".com", ".cn", ".org", ".mob"};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.reg);
        this.gApp = (GlobalApp) getApplication();
        ((Button) findViewById(R.id.btnReg)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String userEmail = ((EditText) RegAct.this.findViewById(R.id.etUid)).getEditableText().toString().trim();
                String nickname = ((EditText) RegAct.this.findViewById(R.id.etNickname)).getEditableText().toString();
                String pwd = ((EditText) RegAct.this.findViewById(R.id.etPwd)).getEditableText().toString().trim();
                String repwd = ((EditText) RegAct.this.findViewById(R.id.etrePwd)).getEditableText().toString().trim();
                String mobilephone = ((EditText) RegAct.this.findViewById(R.id.etMobilePhone)).getEditableText().toString().trim();
                if (RegAct.this.verify(userEmail, nickname, pwd, repwd)) {
                    RegAct.this.register(userEmail, nickname, pwd, mobilephone);
                }
            }
        });
        ((EditText) findViewById(R.id.etUid)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    DialogUtil.showDialog(RegAct.this, "提示", "邮箱可用于找回密码，请再次确认", R.drawable.infoicon);
                }
            }
        });
        ((TextView) findViewById(R.id.showAgreement)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegAct.this.showAgreement();
            }
        });
    }

    public boolean verify(String userEmail, String nickname, String pwd, String repwd) {
        if (!((CheckBox) findViewById(R.id.isReadAgreement)).isChecked()) {
            Toast.makeText(this, "未阅读或同意《海客注册协议》", 1).show();
            return false;
        } else if (nickname.trim().equals("")) {
            Toast.makeText(this, "昵称不能为空", 1).show();
            return false;
        } else if (userEmail.equals("")) {
            Toast.makeText(this, "邮箱地址不能为空", 1).show();
            return false;
        } else if (pwd.equals("") || nickname.trim().equals("")) {
            Toast.makeText(this, "密码不能为空", 1).show();
            return false;
        } else if (!pwd.equals(repwd)) {
            Toast.makeText(this, "密码输入不一致", 1).show();
            return false;
        } else {
            String[] emails = userEmail.split("@");
            if (emails.length == 1) {
                Toast.makeText(this, "邮件格式不对！", 1).show();
                return false;
            } else if (emails[0].trim().equals("")) {
                Toast.makeText(this, "邮件格式不对！", 1).show();
                return false;
            } else if (emails[1].trim().equals("")) {
                Toast.makeText(this, "邮件格式不对！", 1).show();
                return false;
            } else {
                for (String suffix : this.suffixs) {
                    if (emails[1].trim().toLowerCase().endsWith(suffix)) {
                        return true;
                    }
                }
                Toast.makeText(this, "邮件格式不对！", 1).show();
                return false;
            }
        }
    }

    public void showAgreement() {
        View dialogview = LayoutInflater.from(this).inflate((int) R.layout.disclaimer, (ViewGroup) null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("免责声明");
        builder.setView(dialogview);
        builder.setNegativeButton((int) R.string.menu_reback, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });
        builder.show();
    }

    public void register(String userEmail, String nickname, String pwd, String mobilephone) {
        this.netXMLDataFactory = NetXMLDataFactory.getInstance();
        this.reg_doc = null;
        this.mProgress = DialogUtil.showProgress(this, "注册", "注册中,请稍侯...", false, true);
        final String str = userEmail;
        final String str2 = pwd;
        final String str3 = mobilephone;
        final String str4 = nickname;
        new Thread() {
            public void run() {
                try {
                    sleep(5);
                    Map valuesMap = new HashMap();
                    valuesMap.put("email", str);
                    valuesMap.put("password", str2);
                    valuesMap.put("telNumber", str3 == null ? "" : str3);
                    valuesMap.put("nickName", str4);
                    valuesMap.put("deviceId", GuideConfig.deviceId);
                    valuesMap.put("channelCode", GuideConfig.channelCode);
                    Document unused = RegAct.this.reg_doc = HttpClient.doPostToXml(GuideConfig.URL_ACCOUNT_REG, valuesMap);
                    if (RegAct.this.reg_doc == null) {
                        RegAct.this.handler.post(new Runnable() {
                            public void run() {
                                RegAct.this.closeProgress();
                                Toast.makeText(RegAct.this, "注册失败", 1).show();
                            }
                        });
                    } else if (Boolean.parseBoolean(RegAct.this.netXMLDataFactory.getTextNodeByTagName("Suc", RegAct.this.reg_doc))) {
                        FreeServer.clear(GuideConfig.deviceId);
                        UserService.insert(RegAct.this.netXMLDataFactory.getTextNodeByTagName("id", RegAct.this.reg_doc), str, str4, str2, str3);
                        RegAct.this.handler.post(new Runnable() {
                            public void run() {
                                RegAct.this.gotoLoginActivity(str, str2);
                                RegAct.this.closeProgress();
                                RegAct.this.finish();
                            }
                        });
                    } else {
                        final String reason = RegAct.this.netXMLDataFactory.getTextNodeByTagName("Reason", RegAct.this.reg_doc);
                        RegAct.this.handler.post(new Runnable() {
                            public void run() {
                                RegAct.this.closeProgress();
                                Toast.makeText(RegAct.this, "注册失败," + reason, 1).show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    RegAct.this.handler.post(new Runnable() {
                        public void run() {
                            RegAct.this.closeProgress();
                            Toast.makeText(RegAct.this, "注册失败,", 1).show();
                        }
                    });
                }
            }
        }.start();
    }

    public void gotoLoginActivity(String userEmail, String userPwd) {
        this.gApp.curScenicName = "";
        GuideConfig.curCityCode = this.gApp.curVCityCode;
        GlobalApp globalApp = this.gApp;
        GlobalApp.curScenicCode = "";
        Toast.makeText(this, "注册成功", 1).show();
        Intent intent = new Intent(this, AccountAct.class);
        Bundle bundle = new Bundle();
        bundle.putString("userEmail", userEmail);
        bundle.putString("userPwd", userPwd);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void closeProgress() {
        try {
            if (this.mProgress != null) {
                this.mProgress.dismiss();
                this.mProgress = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
