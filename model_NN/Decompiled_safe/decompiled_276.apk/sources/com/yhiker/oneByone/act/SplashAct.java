package com.yhiker.oneByone.act;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.act.scenic.ScenicAct;
import com.yhiker.oneByone.act.scenicpoint.WallPlayer;
import com.yhiker.oneByone.bo.LogService;
import com.yhiker.oneByone.bo.UserService;
import com.yhiker.playmate.R;
import com.yhiker.util.FileUtils;
import com.yhiker.util.HttpClient;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class SplashAct extends Activity {
    private final int APK_NEED_UPDATE = 1;
    private final int FINISH = 9;
    /* access modifiers changed from: private */
    public boolean ISSDCARD_INEXISTENCE = true;
    private final int SDCARD_INEXISTENCE = 10;
    private final int SPLASHFINISH = 11;
    private final int SPLASHFINISH_IMG = 12;
    private final int UPDATING_CITY_DATA = 8;
    private final int UPDATING_DB = 2;
    private final int UPDATING_SCENIC_LIST = 3;
    private final int UPDATING_SCENIC_LIST_DATA = 7;
    /* access modifiers changed from: private */
    public boolean apkNeedInstall;
    GlobalApp gApp;
    /* access modifiers changed from: private */
    public Handler mHandler;
    String userEmail = "Guest";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.gApp = (GlobalApp) getApplication();
        Log.i(getClass().getName(), "android_id=" + Settings.System.getString(getContentResolver(), "ANDROID_ID"));
        Log.i(getClass().getName(), "android_id=" + Settings.Secure.getString(getContentResolver(), "android_id"));
        if (GuideConfig.curCityCode == null || "".equals(GuideConfig.curCityCode)) {
            TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService("phone");
            String deviceId = mTelephonyMgr.getDeviceId();
            String simId = mTelephonyMgr.getSimSerialNumber();
            String phoneNumber = mTelephonyMgr.getLine1Number();
            String operator = mTelephonyMgr.getSimOperator();
            GuideConfig.deviceId = deviceId;
            GuideConfig.simId = simId;
            GuideConfig.phoneNumber = phoneNumber;
            GuideConfig.setInitDataDir(getBaseContext());
            this.gApp.gettedDeviceId = mTelephonyMgr.getDeviceId();
            Log.i(getClass().getName(), System.getProperty("os.name"));
            Log.i(getClass().getName(), System.getProperty("os.version"));
            Log.i(getClass().getName(), Build.VERSION.SDK_INT + "");
            Log.i(getClass().getName(), Build.MODEL);
            Log.i(getClass().getName(), "deviceId" + deviceId);
            Log.i(getClass().getName(), "smiId" + simId);
            Log.i(getClass().getName(), "phoneNumber" + phoneNumber);
            Log.i(getClass().getName(), "operator" + operator);
            Log.i("Integer MaxVal:", "2147483647");
            Log.i("a size:", String.valueOf(new TextView(this).getTextSize()));
            setContentView((int) R.layout.splashact);
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            Log.i("dispaly", (((float) dm.widthPixels) * dm.density) + " * " + (((float) dm.heightPixels) * dm.density));
            this.mHandler = new Handler() {
                /* Debug info: failed to restart local var, previous not found, register: 21 */
                /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
                public void handleMessage(Message msg) {
                    Intent i;
                    switch (msg.what) {
                        case 1:
                            Intent intent = new Intent("android.intent.action.VIEW");
                            intent.addFlags(268435456);
                            intent.setDataAndType(Uri.parse("file://" + GuideConfig.getRootDir() + GuideConfig.APK_SAVE_PATH), "application/vnd.android.package-archive");
                            SplashAct.this.startActivity(intent);
                            SplashAct.this.finish();
                            break;
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        default:
                            return;
                        case 7:
                            break;
                        case 8:
                            ((TextView) SplashAct.this.findViewById(R.id.loading_action)).setText("正在解压下载数据，请耐心等待");
                            return;
                        case 9:
                            HashMap userMap = UserService.finde();
                            if (!userMap.containsKey("smi_id")) {
                                UserService.insert("", "", "", "", "");
                                new Thread() {
                                    public void run() {
                                        try {
                                            sleep(5);
                                            Map valuesMap = new HashMap();
                                            valuesMap.put("email", "");
                                            valuesMap.put("password", "");
                                            valuesMap.put("telNumber", "");
                                            valuesMap.put("nickName", "");
                                            valuesMap.put("deviceId", GuideConfig.deviceId);
                                            valuesMap.put("smiId", GuideConfig.simId);
                                            valuesMap.put("channelCode", GuideConfig.channelCode);
                                            HttpClient.doPostToXml(GuideConfig.URL_ACCOUNT_REG, valuesMap);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }.start();
                            }
                            String firest = SplashAct.this.getSharedPreferences("firest-ty", 0).getString("firest-ty", "");
                            if (firest == null || "".equals(firest)) {
                                AnonymousClass3 r0 = new View.OnClickListener() {
                                    public void onClick(View v) {
                                        SplashAct.this.gApp.curScenicName = "拙政园";
                                        SplashAct.this.gApp.curCityName = "结束体验";
                                        GlobalApp globalApp = SplashAct.this.gApp;
                                        GlobalApp.curCitySeq = 99;
                                        GuideConfig.curCityCode = "00863299";
                                        SplashAct.this.gApp.curScenicHasMap = true;
                                        GlobalApp globalApp2 = SplashAct.this.gApp;
                                        GlobalApp.curScenicCode = "00863299030001";
                                        Log.i(getClass().getSimpleName(), " curAttCode = " + "00863299030001008");
                                        GlobalApp.curTalkPointId = "";
                                        new Thread() {
                                            public void run() {
                                                SplashAct.this.gApp.curCityCodeForPlay = GuideConfig.curCityCode;
                                                GlobalApp globalApp = SplashAct.this.gApp;
                                                GlobalApp globalApp2 = SplashAct.this.gApp;
                                                globalApp.curScenicCodeForPlay = GlobalApp.curScenicCode;
                                                GlobalApp globalApp3 = SplashAct.this.gApp;
                                                LogService.bcScenicSpot(GlobalApp.curScenicCode, "8", "-1", "-1", "听雨轩", false, "1");
                                            }
                                        }.start();
                                        GlobalApp globalApp3 = SplashAct.this.gApp;
                                        GlobalApp.curAttCode = "00863299030001008";
                                        GlobalApp globalApp4 = SplashAct.this.gApp;
                                        GlobalApp.curAttName = "听雨轩";
                                        Intent intent = new Intent(SplashAct.this, WallPlayer.class);
                                        intent.addFlags(131072);
                                        SplashAct.this.startActivity(intent);
                                        SplashAct.this.finish();
                                    }
                                };
                                AnonymousClass4 r02 = new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Intent i;
                                        if (GuideConfig.curCityCode == null || "".equals(GuideConfig.curCityCode)) {
                                            i = new Intent(SplashAct.this, CityAct.class);
                                        } else {
                                            i = new Intent(SplashAct.this, ScenicAct.class);
                                        }
                                        SplashAct.this.startActivity(i);
                                        SplashAct.this.finish();
                                    }
                                };
                                CommonDialog commonDialog = new CommonDialog(SplashAct.this, R.style.dialog);
                                commonDialog.setButtonYesListener(r0);
                                commonDialog.setButtonNoListener(r02);
                                commonDialog.setMessage("体验功能只出现一次,不需要网络.开始体验吧!");
                                commonDialog.requestWindowFeature(1);
                                commonDialog.show();
                                new Thread() {
                                    public void run() {
                                        LogService.loginLog("afterInstall", GuideConfig.deviceId, "1");
                                    }
                                }.start();
                                SharedPreferences.Editor passwdfile = SplashAct.this.getSharedPreferences("firest-ty", 0).edit();
                                passwdfile.putString("firest-ty", "firest-ty");
                                passwdfile.commit();
                                return;
                            }
                            if (userMap.get("e_mail") != null && !"".equals(userMap.get("e_mail").toString())) {
                                SplashAct.this.userEmail = userMap.get("e_mail").toString();
                            }
                            new Thread() {
                                public void run() {
                                    LogService.loginLog(SplashAct.this.userEmail, GuideConfig.deviceId, "2");
                                }
                            }.start();
                            if (GuideConfig.curCityCode == null || "".equals(GuideConfig.curCityCode)) {
                                i = new Intent(SplashAct.this, CityAct.class);
                            } else {
                                i = new Intent(SplashAct.this, ScenicAct.class);
                            }
                            SplashAct.this.startActivity(i);
                            SplashAct.this.finish();
                            return;
                        case 10:
                            new AlertDialog.Builder(SplashAct.this).setTitle("提示").setMessage("SD卡不可用").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    SplashAct.this.finish();
                                }
                            }).create().show();
                            return;
                        case CertStatus.UNREVOKED /*11*/:
                            ((RelativeLayout) SplashAct.this.findViewById(R.id.load_rl)).setVisibility(0);
                            new IntThreed().start();
                            return;
                    }
                    ((TextView) SplashAct.this.findViewById(R.id.loading_action)).setText("正在加载数据，请耐心等待");
                }
            };
            new IntThreed().start();
            return;
        }
        startActivity(new Intent(this, ScenicAct.class));
        finish();
    }

    public void onBackPressed() {
    }

    class IntThreed extends Thread {
        String locProvider = null;
        LocationManager locationManager = null;

        IntThreed() {
        }

        public void initSysPreDate() {
            SQLiteDatabase per_conf_db = null;
            Cursor per_conf_res = null;
            SQLiteDatabase per_conf_tmp_db = null;
            Cursor per_conf_tmp_res = null;
            try {
                File data_dire = new File(GuideConfig.getInitDataDir() + "/yhiker/data");
                if (!data_dire.exists()) {
                    data_dire.mkdirs();
                }
                File conf_dir = new File(GuideConfig.getInitDataDir() + ConfigHp.conf_dir);
                if (!conf_dir.exists()) {
                    conf_dir.mkdirs();
                }
                File per_conf_file_db = new File(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path);
                if (!per_conf_file_db.exists()) {
                    FileUtils.copy(SplashAct.this.getResources().openRawResource(R.raw.per_conf), per_conf_file_db);
                }
                File per_conf_file_db_tmp = new File(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path + "tmp.db");
                if (per_conf_file_db_tmp.exists()) {
                    per_conf_file_db_tmp.delete();
                }
                FileUtils.copy(SplashAct.this.getResources().openRawResource(R.raw.per_conf), per_conf_file_db_tmp);
                per_conf_db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
                per_conf_res = per_conf_db.rawQuery("SELECT pc_key,pc_value FROM per_conf", null);
                per_conf_res.moveToFirst();
                while (!per_conf_res.isAfterLast()) {
                    GuideConfig.sysConfMap.put(per_conf_res.getString(0), per_conf_res.getString(1));
                    per_conf_res.moveToNext();
                }
                per_conf_tmp_db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path + "tmp.db", null, 16);
                Cursor per_conf_tmp_res2 = per_conf_tmp_db.rawQuery("SELECT pc_key,pc_value FROM per_conf", null);
                per_conf_tmp_res2.moveToFirst();
                while (!per_conf_tmp_res2.isAfterLast()) {
                    GuideConfig.apkSysConfMap.put(per_conf_tmp_res2.getString(0), per_conf_tmp_res2.getString(1));
                    per_conf_tmp_res2.moveToNext();
                }
                if (per_conf_res != null) {
                    per_conf_res.close();
                }
                if (per_conf_db != null) {
                    per_conf_db.close();
                }
                if (per_conf_tmp_res2 != null) {
                    per_conf_tmp_res2.close();
                }
                if (per_conf_tmp_db == null) {
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (per_conf_res != null) {
                    per_conf_res.close();
                }
                if (per_conf_db != null) {
                    per_conf_db.close();
                }
                if (per_conf_tmp_res != null) {
                    per_conf_tmp_res.close();
                }
                if (per_conf_tmp_db == null) {
                    return;
                }
            } catch (Throwable th) {
                if (per_conf_res != null) {
                    per_conf_res.close();
                }
                if (per_conf_db != null) {
                    per_conf_db.close();
                }
                if (per_conf_tmp_res != null) {
                    per_conf_tmp_res.close();
                }
                if (per_conf_tmp_db != null) {
                    per_conf_tmp_db.close();
                }
                throw th;
            }
            per_conf_tmp_db.close();
        }

        public void updateScenicListDate() {
            SQLiteDatabase scenic_list_db = null;
            Cursor scenic_list_conf_res = null;
            String scenic_apk_db_version = "0";
            String scenic_sys_db_version = "0";
            boolean scenic_db_update = true;
            try {
                if (GuideConfig.sysConfMap.containsKey(GuideConfig.scenic_db_version_tag)) {
                    scenic_sys_db_version = GuideConfig.sysConfMap.get(GuideConfig.scenic_db_version_tag);
                }
                if (GuideConfig.apkSysConfMap.containsKey(GuideConfig.scenic_db_version_tag)) {
                    scenic_apk_db_version = GuideConfig.apkSysConfMap.get(GuideConfig.scenic_db_version_tag);
                }
                if (Integer.parseInt(scenic_apk_db_version) > Integer.parseInt(scenic_sys_db_version)) {
                    scenic_db_update = true;
                }
                File scenic_list_path = new File(GuideConfig.getInitDataDir() + ConfigHp.scenic_list_path);
                if (!scenic_list_path.exists() || scenic_db_update) {
                    FileUtils.copy(SplashAct.this.getResources().openRawResource(R.raw.scenic_list), scenic_list_path);
                }
                scenic_list_db = SQLiteDatabase.openDatabase(scenic_list_path.getAbsolutePath(), null, 16);
                scenic_list_conf_res = scenic_list_db.rawQuery("SELECT slc_city_code FROM scenic_list_city", null);
                scenic_list_conf_res.moveToFirst();
                while (!scenic_list_conf_res.isAfterLast()) {
                    String slc_city_code = scenic_list_conf_res.getString(0).toLowerCase();
                    File scenic_city_path = new File(GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + slc_city_code + ".db");
                    if (!scenic_city_path.exists() || scenic_db_update) {
                        FileUtils.copy(SplashAct.this.getResources().openRawResource(SplashAct.this.getResources().getIdentifier("scenic_" + slc_city_code, "raw", SplashAct.this.getPackageName())), scenic_city_path);
                    }
                    scenic_list_conf_res.moveToNext();
                }
                File scenic_city_ty_db_path = new File(GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + "00863299.db");
                if (!scenic_city_ty_db_path.exists() || scenic_db_update) {
                    FileUtils.copy(SplashAct.this.getResources().openRawResource(SplashAct.this.getResources().getIdentifier("scenic_00863299", "raw", SplashAct.this.getPackageName())), scenic_city_ty_db_path);
                }
                if (!new File(GuideConfig.getInitDataDir() + ConfigHp.scenic_list_data_ver_dir + "/00863299").exists()) {
                    FileUtils.copyAssets(SplashAct.this, "0086/00863299", GuideConfig.getInitDataDir() + ConfigHp.scenic_list_data_ver_dir + "/00863299/");
                }
                if (scenic_db_update) {
                    UserService.updateAttr(GuideConfig.scenic_db_version_tag, scenic_apk_db_version);
                }
                if (scenic_list_conf_res != null) {
                    scenic_list_conf_res.close();
                }
                if (scenic_list_db == null) {
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (scenic_list_conf_res != null) {
                    scenic_list_conf_res.close();
                }
                if (scenic_list_db == null) {
                    return;
                }
            } catch (Throwable th) {
                if (scenic_list_conf_res != null) {
                    scenic_list_conf_res.close();
                }
                if (scenic_list_db != null) {
                    scenic_list_db.close();
                }
                throw th;
            }
            scenic_list_db.close();
        }

        public void run() {
            Handler access$100;
            AnonymousClass1 r0 = new LocationListener() {
                public void onLocationChanged(Location location) {
                    SplashAct.this.gApp.netLocation = location;
                    Log.i(getClass().getName(), location.getLongitude() + "");
                }

                public void onProviderDisabled(String provider) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }
            };
            try {
                SplashAct.this.gApp.currApkVerCode = SplashAct.this.gApp.getPackageManager().getPackageInfo(GuideConfig.APK_PACKAGE_NAME, 0).versionCode;
                Log.i("versioncode", (SplashAct.this.gApp.currApkVerCode + 1) + "");
                File apkPath = new File(GuideConfig.getRootDir() + GuideConfig.APK_SAVE_PATH);
                if (apkPath.exists()) {
                    if (SplashAct.this.gApp.getPackageManager().getPackageArchiveInfo(GuideConfig.getRootDir() + GuideConfig.APK_SAVE_PATH, 1).versionCode > SplashAct.this.gApp.currApkVerCode) {
                        boolean unused = SplashAct.this.apkNeedInstall = true;
                        SplashAct.this.mHandler.sendEmptyMessage(1);
                        if (SplashAct.this.ISSDCARD_INEXISTENCE) {
                            if (this.locationManager != null) {
                                this.locationManager.removeUpdates(r0);
                            }
                            if (!SplashAct.this.apkNeedInstall) {
                                access$100 = SplashAct.this.mHandler;
                                access$100.sendEmptyMessage(9);
                            }
                            return;
                        }
                        return;
                    }
                    apkPath.delete();
                }
                this.locationManager = (LocationManager) SplashAct.this.getSystemService("location");
                Criteria criteria = new Criteria();
                criteria.setAccuracy(2);
                criteria.setAltitudeRequired(false);
                criteria.setBearingRequired(false);
                criteria.setCostAllowed(true);
                criteria.setPowerRequirement(1);
                this.locProvider = this.locationManager.getBestProvider(criteria, true);
                if (this.locProvider == null || this.locProvider.equals("")) {
                    if (this.locationManager.isProviderEnabled("network")) {
                        this.locProvider = "network";
                    } else if (this.locationManager.isProviderEnabled("gps")) {
                        this.locProvider = "gps";
                    }
                }
                try {
                    this.locationManager.requestLocationUpdates(this.locProvider, 1000, 0.0f, r0, Looper.getMainLooper());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                initSysPreDate();
                updateScenicListDate();
                SQLiteDatabase scenic_list_db = null;
                Cursor sl_conf_res = null;
                Cursor scenic_list_res = null;
                try {
                    SplashAct.this.gApp.netLocation = this.locationManager.getLastKnownLocation(this.locProvider);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                try {
                    if (SplashAct.this.gApp.netLocation == null) {
                        SystemClock.sleep(5000);
                    }
                    double lastLon = 0.0d;
                    double lastLat = 0.0d;
                    if (SplashAct.this.gApp.netLocation != null) {
                        lastLon = SplashAct.this.gApp.netLocation.getLongitude();
                        lastLat = SplashAct.this.gApp.netLocation.getLatitude();
                        scenic_list_db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.scenic_list_path, null, 16);
                        scenic_list_res = scenic_list_db.rawQuery("select scenic_list.sl_name sl_name,city_code.cc_name cc_name,city_code.cc_code cc_code,abs(scenic_list.sl_lon-" + lastLon + ")*abs(scenic_list.sl_lon-" + lastLon + ")+abs(scenic_list.sl_lat-" + lastLat + ")*abs(scenic_list.sl_lat-" + lastLat + ") dist from scenic_list  left outer join city_code on scenic_list.sl2slc_city_code=city_code.cc_code where scenic_list.sl_lon is not null order by dist limit 1", null);
                        scenic_list_res.moveToFirst();
                        if (!scenic_list_res.isAfterLast()) {
                            SplashAct.this.gApp.curCityName = scenic_list_res.getString(1);
                            GuideConfig.curCityCode = scenic_list_res.getString(2);
                            SplashAct.this.gApp.curCityCodeForLocation = scenic_list_res.getString(2);
                        }
                    }
                    Log.i("LL:", "lon=" + lastLon + ",lat=" + lastLat);
                    Log.i("curCity", "curCityName=" + SplashAct.this.gApp.curCityName + ",curCityCode=" + GuideConfig.curCityCode);
                    if (scenic_list_res != null) {
                        scenic_list_res.close();
                    }
                    if (sl_conf_res != null) {
                        sl_conf_res.close();
                    }
                    if (scenic_list_db != null) {
                        scenic_list_db.close();
                    }
                } catch (Exception e3) {
                    Exception ex = e3;
                    Log.e(getClass().getName(), ex.toString());
                    ex.printStackTrace();
                    if (scenic_list_res != null) {
                        scenic_list_res.close();
                    }
                    if (sl_conf_res != null) {
                        sl_conf_res.close();
                    }
                    if (scenic_list_db != null) {
                        scenic_list_db.close();
                    }
                } catch (Throwable th) {
                    if (scenic_list_res != null) {
                        scenic_list_res.close();
                    }
                    if (sl_conf_res != null) {
                        sl_conf_res.close();
                    }
                    if (scenic_list_db != null) {
                        scenic_list_db.close();
                    }
                    throw th;
                }
                Log.i("updaterun", "gApp.scenic_list_update_url=" + SplashAct.this.gApp.scenic_list_update_url);
                SplashAct.this.startService(new Intent(SplashAct.this.gApp, UpdateService.class));
                if (SplashAct.this.ISSDCARD_INEXISTENCE) {
                    if (this.locationManager != null) {
                        this.locationManager.removeUpdates(r0);
                    }
                    if (!SplashAct.this.apkNeedInstall) {
                        access$100 = SplashAct.this.mHandler;
                        access$100.sendEmptyMessage(9);
                    }
                }
            } catch (Exception e4) {
                Exception ex2 = e4;
                Log.e(getClass().getName(), ex2.toString());
                ex2.printStackTrace();
                if (SplashAct.this.ISSDCARD_INEXISTENCE) {
                    if (this.locationManager != null) {
                        this.locationManager.removeUpdates(r0);
                    }
                    if (!SplashAct.this.apkNeedInstall) {
                        access$100 = SplashAct.this.mHandler;
                    }
                }
            } catch (Throwable th2) {
                if (SplashAct.this.ISSDCARD_INEXISTENCE) {
                    if (this.locationManager != null) {
                        this.locationManager.removeUpdates(r0);
                    }
                    if (!SplashAct.this.apkNeedInstall) {
                        SplashAct.this.mHandler.sendEmptyMessage(9);
                    }
                }
                throw th2;
            }
        }
    }
}
