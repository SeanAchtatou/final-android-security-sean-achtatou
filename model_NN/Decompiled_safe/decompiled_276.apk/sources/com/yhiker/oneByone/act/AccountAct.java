package com.yhiker.oneByone.act;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.yhiker.oneByone.bo.UserService;
import com.yhiker.playmate.R;
import com.yhiker.util.DialogUtil;
import java.util.HashMap;

public class AccountAct extends BaseAct {
    public static final int UPDATEFINISH = 9;
    GlobalApp gApp;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 9:
                    AccountAct.this.load();
                    return;
                default:
                    return;
            }
        }
    };
    HashMap m;
    /* access modifiers changed from: private */
    public ProgressDialog mProgress = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.account_info);
        this.gApp = (GlobalApp) getApplication();
        ((Button) findViewById(R.id.btnLo)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserService.updateAttr("auto_login", "0");
                ProgressDialog unused = AccountAct.this.mProgress = DialogUtil.showProgress(AccountAct.this, "注销", "注销,请稍侯...", false, true);
                new Thread() {
                    public void run() {
                        GlobalApp globalApp = AccountAct.this.gApp;
                        GlobalApp.curScenicCode = null;
                        AccountAct.this.gApp.curScenicName = null;
                        UserService.logOut();
                        AccountAct.this.startActivity(new Intent(AccountAct.this, LoginAct.class));
                        AccountAct.this.finish();
                        AccountAct.this.closeProgress();
                    }
                }.start();
            }
        });
        ((Button) findViewById(R.id.btnUpdate)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AccountAct.this.showUpdateAccount();
            }
        });
        load();
    }

    public void load() {
        this.m = UserService.finde();
        String userEmail = this.m.get("e_mail").toString();
        String nickName = this.m.get("nick").toString();
        String telNumber = this.m.get("mobile") == null ? "" : this.m.get("mobile").toString();
        ((TextView) findViewById(R.id.email_account)).setText(userEmail);
        ((TextView) findViewById(R.id.nickname_account)).setText(nickName);
        ((TextView) findViewById(R.id.telNumber_account)).setText(telNumber);
        Button btnUpdate = (Button) findViewById(R.id.btnUpdate);
        Button btnLo = (Button) findViewById(R.id.btnLo);
        if ("Guest".equals(userEmail)) {
            btnUpdate.setVisibility(4);
            btnLo.setVisibility(4);
        }
    }

    public void showUpdateAccount() {
        AccountUpDia cd = new AccountUpDia(this, R.style.dialog);
        cd.requestWindowFeature(1);
        cd.mhandler = this.handler;
        cd.show();
    }

    /* access modifiers changed from: package-private */
    public void closeProgress() {
        try {
            if (this.mProgress != null) {
                this.mProgress.dismiss();
                this.mProgress = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
