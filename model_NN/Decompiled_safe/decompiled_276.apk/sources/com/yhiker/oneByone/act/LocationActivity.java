package com.yhiker.oneByone.act;

import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageButton;
import android.widget.Toast;
import com.yhiker.guid.service.ParkMapAction;
import com.yhiker.guid.service.Signal;
import com.yhiker.oneByone.view.SignalStrengthView;
import com.yhiker.playmate.R;
import java.util.Iterator;

public class LocationActivity extends DrawActivity implements LocationListener {
    private static int availableSatellitesCount_minlimit;
    private static String providerString = "gps";
    private static int satelliteSnr_minlimit;
    /* access modifiers changed from: private */
    public LocationManager gpsManager;
    private GpsSatellite gpsSatellite;
    private Handler handler = new Handler();
    private Iterator<GpsSatellite> it;
    private Iterable<GpsSatellite> itGpsStatellites;
    private GpsStatus mGPSStatus;
    private Thread thread;

    public void displayBuilder(ImageButton imageButton) {
        this.handler.post(new Runnable() {
            public void run() {
                switch (ParkMapAction.GPSlevel) {
                    case 0:
                        if (ParkMapAction.gpsButton != null) {
                            ParkMapAction.gpsButton.setBackgroundResource(R.drawable.nosignal);
                            return;
                        }
                        return;
                    case 1:
                        if (ParkMapAction.gpsButton != null) {
                            ParkMapAction.gpsButton.setBackgroundResource(R.drawable.signal1);
                            return;
                        }
                        return;
                    case 2:
                        if (ParkMapAction.gpsButton != null) {
                            ParkMapAction.gpsButton.setBackgroundResource(R.drawable.signal2);
                            return;
                        }
                        return;
                    case 3:
                        if (ParkMapAction.gpsButton != null) {
                            ParkMapAction.gpsButton.setBackgroundResource(R.drawable.signal3);
                            return;
                        }
                        return;
                    case 4:
                        if (ParkMapAction.gpsButton != null) {
                            ParkMapAction.gpsButton.setBackgroundResource(R.drawable.signal4);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        });
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        satelliteSnr_minlimit = 15;
        availableSatellitesCount_minlimit = 4;
        this.gpsManager = (LocationManager) getSystemService("location");
        this.gpsManager.addGpsStatusListener(new GpsStatus.Listener() {
            public void onGpsStatusChanged(int event) {
            }
        });
        this.thread = new Thread() {
            public void run() {
                while (true) {
                    try {
                        sleep(1000);
                        ParkMapAction.GPSlevel = Signal.getLevel(LocationActivity.this.gpsManager.getGpsStatus(null).getSatellites());
                        SignalStrengthView.signalLevel = ParkMapAction.GPSlevel;
                        Thread.sleep(10);
                        LocationActivity.this.displayBuilder((ImageButton) LocationActivity.this.findViewById(R.id.gpsbutton));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            }
        };
        this.thread.start();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (providerString != null) {
            this.gpsManager.requestLocationUpdates(providerString, 1000, 0.0f, this);
        }
    }

    public void postNewLocation(Location location) {
        boolean broadcastFlag;
        try {
            this.mGPSStatus = this.gpsManager.getGpsStatus(null);
            this.itGpsStatellites = this.mGPSStatus.getSatellites();
            this.it = this.itGpsStatellites.iterator();
            int availableSatellitesCount = 0;
            while (this.it.hasNext()) {
                this.gpsSatellite = this.it.next();
                if (((int) this.gpsSatellite.getSnr()) >= satelliteSnr_minlimit) {
                    availableSatellitesCount++;
                }
            }
            if (availableSatellitesCount >= availableSatellitesCount_minlimit) {
                broadcastFlag = true;
            } else {
                broadcastFlag = false;
            }
            if (ParkMapAction.GPSlevel > 1) {
                super.moveGps(location, broadcastFlag);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postNewLocation(Location location, Location gprslocation) {
        boolean broadcastFlag;
        try {
            this.mGPSStatus = this.gpsManager.getGpsStatus(null);
            this.itGpsStatellites = this.mGPSStatus.getSatellites();
            this.it = this.itGpsStatellites.iterator();
            int availableSatellitesCount = 0;
            while (this.it.hasNext()) {
                this.gpsSatellite = this.it.next();
                if (((int) this.gpsSatellite.getSnr()) >= satelliteSnr_minlimit) {
                    availableSatellitesCount++;
                }
            }
            if (availableSatellitesCount >= availableSatellitesCount_minlimit) {
                broadcastFlag = true;
            } else {
                broadcastFlag = false;
            }
            if (ParkMapAction.GPSlevel > 1) {
                super.moveGps(location, broadcastFlag);
                Toast.makeText(this, "GPS OK", 1).show();
                return;
            }
            super.moveGps(gprslocation, broadcastFlag);
            Toast.makeText(this, "NO GPS", 1).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.gpsManager.requestLocationUpdates(providerString, 1000, 0.0f, this);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void stopLisLocation() {
        this.thread.interrupt();
        while (this.thread.isAlive()) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.gpsManager.removeUpdates(this);
        super.onStop();
    }

    public void onLocationChanged(Location location) {
        postNewLocation(location);
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
