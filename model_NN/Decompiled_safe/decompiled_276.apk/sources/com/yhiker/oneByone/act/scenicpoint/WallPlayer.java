package com.yhiker.oneByone.act.scenicpoint;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.act.BaseAct;
import com.yhiker.oneByone.act.GlobalApp;
import com.yhiker.oneByone.act.MapAct;
import com.yhiker.oneByone.act.mediaplayer.PlayerPanelView;
import com.yhiker.oneByone.bo.ScenicPointBo;
import com.yhiker.oneByone.bo.WallPlayerService;
import com.yhiker.oneByone.module.ScenicPoint;
import com.yhiker.playmate.R;
import com.yhiker.util.ImageUtil;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WallPlayer extends BaseAct {
    private static final int REFRESHDATA = 2545;
    public static PlayerPanelView playerPanelView;
    private ExecutorService executorService;
    /* access modifiers changed from: private */
    public GlobalApp gApp;
    protected Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 2545:
                    Log.i("handler", "get attChangeIntent:" + msg.arg1);
                    WallPlayer.this.refView(ScenicPointBo.getScenicPointByScenicPointCode(GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + GuideConfig.curCityCode.toLowerCase() + ".db", msg.obj.toString()));
                    return;
                default:
                    return;
            }
        }
    };
    private WallPlayerService wallPlayerService;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(getClass().getSimpleName(), "onCreate is called");
        setContentView((int) R.layout.audio);
        this.gApp = (GlobalApp) getApplication();
        this.executorService = Executors.newFixedThreadPool(5);
        this.wallPlayerService = new WallPlayerService();
        playerPanelView = (PlayerPanelView) findViewById(R.id.player_panellayout);
        playerPanelView.initPlayerPanel(getWindow());
        playerPanelView.initExplanationScroll(getWindow());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.i(getClass().getSimpleName(), "onResume is called");
        ((TextView) findViewById(R.id.preText)).setText(this.gApp.curScenicName);
        GlobalApp globalApp = this.gApp;
        ((TextView) findViewById(R.id.currText)).setText(GlobalApp.curAttName);
        TextView map_btn = (TextView) findViewById(R.id.nextText);
        map_btn.setText("");
        map_btn.setVisibility(4);
        map_btn.setEnabled(false);
        if (this.gApp.curScenicHasMap) {
            map_btn.setEnabled(true);
            map_btn.setText("地图");
            map_btn.setVisibility(0);
            map_btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (GuideConfig.curCityCode.equals("00863299")) {
                        GlobalApp unused = WallPlayer.this.gApp;
                        GlobalApp.tyHasViewMap = true;
                    }
                    Intent intent = new Intent(WallPlayer.this, MapAct.class);
                    intent.addFlags(131072);
                    WallPlayer.this.startActivity(intent);
                }
            });
        }
        ((TextView) findViewById(R.id.preText)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                WallPlayer.this.startActivity(new Intent(WallPlayer.this, WallAct.class));
            }
        });
        playerPanelView.refreshPlayerPanel();
        Handler handler = this.mHandler;
        Handler handler2 = this.mHandler;
        GlobalApp globalApp2 = this.gApp;
        handler.sendMessage(handler2.obtainMessage(2545, 0, 0, GlobalApp.curAttCode));
    }

    /* access modifiers changed from: private */
    public void refView(ScenicPoint scenicPoint) {
        Log.i(getClass().getSimpleName(), "refView is called");
        if (scenicPoint != null) {
            String al_intro = scenicPoint.getDintro();
            String al_code = scenicPoint.getCode();
            String mp3Path = "http://58.211.138.180:88/0420/0086/" + GuideConfig.curCityCode + File.separator + "audio/";
            File kbs32 = new File((GuideConfig.getRootDir() + "/yhiker/data" + File.separator + "0086/" + GuideConfig.curCityCode + File.separator + "audio/") + al_code + "_32Kbps.m4a");
            File kbsty32 = new File((GuideConfig.getInitDataDir() + "/yhiker/data" + File.separator + "0086/" + GuideConfig.curCityCode + File.separator + "audio/") + al_code + "_32Kbps.m4a");
            if (kbs32.exists()) {
                playerPanelView.btnPlaPauClk(mp3Path, al_code + "_32Kbps.m4a");
            } else if (kbsty32.exists()) {
                playerPanelView.btnPlaPauClk(mp3Path, al_code + "_32Kbps.m4a");
            } else {
                playerPanelView.btnPlaPauClk(mp3Path, al_code + "_24Kbps.mp3");
            }
            ((TextView) findViewById(R.id.picWallPlayText)).setText(al_intro);
            loadImage(GuideConfig.curCityCode, al_code, (ImageView) findViewById(R.id.player_spotview), false);
        }
    }

    private void loadImage(String cityCode, String ScenicPintCode, ImageView imgV, boolean iconed) {
        if (!this.executorService.isShutdown()) {
            final ImageView imageView = imgV;
            final boolean z = iconed;
            final String str = cityCode;
            final String str2 = ScenicPintCode;
            this.executorService.submit(new Runnable() {
                public void run() {
                    final Drawable drawable;
                    try {
                        final WeakReference<ImageView> imageViewReference = new WeakReference<>(imageView);
                        if (z) {
                            drawable = Drawable.createFromResourceStream(WallPlayer.this.getResources(), null, WallPlayer.this.getResources().openRawResource(R.drawable.icon), WallPlayer.this.getResources().getResourceName(R.drawable.icon));
                            Log.i("", "iconDraw loaded");
                        } else {
                            Drawable drawable1 = ImageUtil.loadBitmapByPlay(str, str2);
                            if (drawable1 == null) {
                                drawable = WallPlayer.this.getResources().getDrawable(R.drawable.picwall_big);
                            } else {
                                drawable = drawable1;
                            }
                        }
                        WallPlayer.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (imageViewReference.get() != null) {
                                    ((ImageView) imageViewReference.get()).setImageDrawable(drawable);
                                    PlayerPanelView.mSpotButton.setImageDrawable(drawable);
                                    PlayerPanelView.boardScienceImage = drawable;
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.i(getClass().getSimpleName(), "onPause is called");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i(getClass().getSimpleName(), "onDestroy is called");
        this.executorService.shutdown();
        super.onDestroy();
    }
}
