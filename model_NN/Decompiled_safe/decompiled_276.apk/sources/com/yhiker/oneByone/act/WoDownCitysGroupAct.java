package com.yhiker.oneByone.act;

import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.yhiker.playmate.R;

public class WoDownCitysGroupAct extends ActivityGroup {
    private static final String TAG = "WoCitysGroup-";
    private LinearLayout container;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.citysgroup);
        this.container = (LinearLayout) findViewById(R.id.citysgroup_view);
        this.container.removeAllViews();
        Intent intent = new Intent(this, WoCitysAct.class);
        intent.addFlags(67108864);
        intent.putExtra("woTabId", "down");
        View view = getLocalActivityManager().startActivity("WoCitysAct", intent).getDecorView();
        this.container.addView(view);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.width = -1;
        params.height = -1;
        view.setLayoutParams(params);
    }
}
