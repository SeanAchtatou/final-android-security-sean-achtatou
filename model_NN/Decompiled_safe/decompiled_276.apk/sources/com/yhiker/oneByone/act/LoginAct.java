package com.yhiker.oneByone.act;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.bo.LogService;
import com.yhiker.oneByone.bo.UserService;
import com.yhiker.pay.NetXMLDataFactory;
import com.yhiker.playmate.R;
import com.yhiker.util.DialogUtil;
import com.yhiker.util.HttpClient;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Document;

public class LoginAct extends BaseAct {
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public Document login_doc = null;
    private ProgressDialog mProgress = null;
    private View menuaccount;
    private View menudown;
    private View menuexit;
    private View menuhistory;
    /* access modifiers changed from: private */
    public NetXMLDataFactory netXMLDataFactory = null;
    private PopupWindow pop;
    private View view;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.login);
        ((Button) findViewById(R.id.btnLogin)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoginAct.this.login();
            }
        });
        ((Button) findViewById(R.id.btnReg)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoginAct.this.startActivity(new Intent(LoginAct.this, RegAct.class));
            }
        });
        ((Button) findViewById(R.id.btnTy)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserService.insert("", "Guest", "Guest", null, "");
            }
        });
        HashMap hm = UserService.finde();
        if (hm.containsKey("recode_accountname") && "1".equals(hm.get("recode_accountname"))) {
            ((EditText) findViewById(R.id.etUid)).setText(hm.get("e_mail").toString());
            ((CheckBox) findViewById(R.id.raccountname)).setSelected(true);
        }
    }

    public void login() {
        this.netXMLDataFactory = NetXMLDataFactory.getInstance();
        this.login_doc = null;
        final String userEmail = ((EditText) findViewById(R.id.etUid)).getEditableText().toString().trim();
        final String userPwd = ((EditText) findViewById(R.id.etPwd)).getEditableText().toString().trim();
        final CheckBox autologinckb = (CheckBox) findViewById(R.id.autologin);
        final CheckBox raccountnameckb = (CheckBox) findViewById(R.id.raccountname);
        if (userEmail.equals("") || userPwd.equals("")) {
            Toast.makeText(this, "请输入帐号或密码!", 0).show();
            return;
        }
        this.mProgress = DialogUtil.showProgress(this, "登陆", "登陆中,请稍侯...", false, true);
        new Thread() {
            public void run() {
                try {
                    sleep(5);
                    Map valuesMap = new HashMap();
                    valuesMap.put("email", userEmail);
                    valuesMap.put("password", userPwd);
                    Document unused = LoginAct.this.login_doc = HttpClient.doPostToXml(GuideConfig.URL_ACCOUNT_LOGIN, valuesMap);
                    if (LoginAct.this.login_doc == null) {
                        return;
                    }
                    if (Boolean.parseBoolean(LoginAct.this.netXMLDataFactory.getTextNodeByTagName("Suc", LoginAct.this.login_doc))) {
                        UserService.insert(LoginAct.this.netXMLDataFactory.getTextNodeByTagName("id", LoginAct.this.login_doc), userEmail, LoginAct.this.netXMLDataFactory.getTextNodeByTagName("nickName", LoginAct.this.login_doc), userPwd, LoginAct.this.netXMLDataFactory.getTextNodeByTagName("telNumber", LoginAct.this.login_doc));
                        if (autologinckb.isChecked()) {
                            UserService.updateAttr("auto_login", "1");
                        } else {
                            UserService.updateAttr("auto_login", "0");
                        }
                        if (raccountnameckb.isChecked()) {
                            UserService.updateAttr("recode_accountname", "1");
                        } else {
                            UserService.updateAttr("recode_accountname", "0");
                        }
                        LoginAct.this.handler.post(new Runnable() {
                            public void run() {
                                LoginAct.this.gotoMainActivity(userEmail, userPwd);
                                LoginAct.this.closeProgress();
                                LoginAct.this.finish();
                            }
                        });
                        return;
                    }
                    LoginAct.this.handler.post(new Runnable() {
                        public void run() {
                            LoginAct.this.closeProgress();
                            Toast.makeText(LoginAct.this, "登录失败，用户名或密码不正确!", 0).show();
                        }
                    });
                } catch (Exception e) {
                    LoginAct.this.handler.post(new Runnable() {
                        public void run() {
                            LoginAct.this.closeProgress();
                            DialogUtil.showDialog(LoginAct.this, "异常", "登陆失败", R.drawable.infoicon);
                        }
                    });
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void gotoMainActivity(final String userEmail, String userPwd) {
        new Thread() {
            public void run() {
                LogService.loginLog(userEmail, GuideConfig.deviceId, "2");
            }
        }.start();
        Intent intent = new Intent(this, AccountAct.class);
        Bundle bundle = new Bundle();
        bundle.putString("userEmail", userEmail);
        bundle.putString("userPwd", userPwd);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void closeProgress() {
        try {
            if (this.mProgress != null) {
                this.mProgress.dismiss();
                this.mProgress = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
