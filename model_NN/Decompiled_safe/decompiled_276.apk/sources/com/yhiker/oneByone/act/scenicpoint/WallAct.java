package com.yhiker.oneByone.act.scenicpoint;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.act.BaseAct;
import com.yhiker.oneByone.act.CityAct;
import com.yhiker.oneByone.act.CommonDialog;
import com.yhiker.oneByone.act.GlobalApp;
import com.yhiker.oneByone.act.MapAct;
import com.yhiker.oneByone.act.scenic.ScenicAct;
import com.yhiker.oneByone.bo.CityBo;
import com.yhiker.oneByone.bo.LogService;
import com.yhiker.oneByone.bo.ScenicPointBo;
import com.yhiker.oneByone.bo.WallPlayerService;
import com.yhiker.playmate.R;
import com.yhiker.util.ImageUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WallAct extends BaseAct {
    public static final int REFRESHDATA = 2545;
    private MyAdapter attractionListAdapter;
    private String curScenicCode;
    /* access modifiers changed from: private */
    public ExecutorService executorService = Executors.newCachedThreadPool();
    /* access modifiers changed from: private */
    public GlobalApp gApp;
    private GridView gridview;
    /* access modifiers changed from: private */
    public HashMap<String, Drawable> imgCache;
    protected Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WallAct.REFRESHDATA /*2545*/:
                    StringBuilder append = new StringBuilder().append("get scenicChangeIntent:");
                    GlobalApp unused = WallAct.this.gApp;
                    Log.i("handler", append.append(GlobalApp.curScenicCode).toString());
                    WallAct.this.queryListData();
                    return;
                default:
                    return;
            }
        }
    };
    private Button map_btn;
    private WallPlayerService wallPlayerService = new WallPlayerService();

    /* access modifiers changed from: private */
    public void queryListData() {
        String dbPath = GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + GuideConfig.curCityCode.toLowerCase() + ".db";
        int curCitySeq = CityBo.getCityId(GuideConfig.getInitDataDir() + ConfigHp.scenic_list_path, GuideConfig.curCityCode);
        if (curCitySeq > 0) {
            GlobalApp globalApp = this.gApp;
            GlobalApp.curCitySeq = curCitySeq;
        }
        GlobalApp globalApp2 = this.gApp;
        this.attractionListAdapter = new MyAdapter(this, ScenicPointBo.getScenicMapByScenicCode(dbPath, GlobalApp.curScenicCode), R.layout.picwallact_item, new String[]{"al_name"}, new int[]{R.id.picwall_text});
        this.attractionListAdapter.notifyDataSetChanged();
        this.gridview.setAdapter((ListAdapter) this.attractionListAdapter);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.picwallact);
        Log.d(getClass().getSimpleName(), "onCreate is called");
        this.searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
        this.gApp = (GlobalApp) getApplication();
        this.imgCache = new HashMap<>();
        this.gridview = (GridView) findViewById(R.id.picwall_gridview);
        this.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                final String bcName = ((TextView) v.findViewById(R.id.picwall_text)).getText().toString();
                Toast.makeText(WallAct.this, bcName, 0).show();
                Map<String, String> tempMap = (Map) ((MyAdapter) parent.getAdapter()).getItem(position);
                String curAttCode = (String) tempMap.get("al_code");
                Log.i(getClass().getSimpleName(), " curAttCode = " + curAttCode);
                GlobalApp unused = WallAct.this.gApp;
                GlobalApp unused2 = WallAct.this.gApp;
                GlobalApp.preCurScenicCode = GlobalApp.curScenicCode;
                if (!GuideConfig.curCityCode.equals("00863299") || curAttCode.equals("00863299030001008")) {
                    Log.d(getClass().getSimpleName(), " GlobalApp.curBcPointSeq = " + GlobalApp.curBcPointSeq);
                    final String curBcPointSeqPosition = "" + position;
                    new Thread() {
                        public void run() {
                            try {
                                sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            WallAct.this.gApp.curCityCodeForPlay = GuideConfig.curCityCode;
                            GlobalApp access$000 = WallAct.this.gApp;
                            GlobalApp unused = WallAct.this.gApp;
                            access$000.curScenicCodeForPlay = GlobalApp.curScenicCode;
                            GlobalApp unused2 = WallAct.this.gApp;
                            LogService.bcScenicSpot(GlobalApp.curScenicCode, curBcPointSeqPosition, "-1", "-1", bcName, false, "1");
                        }
                    }.start();
                    GlobalApp unused3 = WallAct.this.gApp;
                    GlobalApp.curAttCode = (String) tempMap.get("al_code");
                    GlobalApp unused4 = WallAct.this.gApp;
                    GlobalApp.curAttName = (String) tempMap.get("al_name");
                    GlobalApp unused5 = WallAct.this.gApp;
                    if (GlobalApp.curAttCode != null) {
                        GlobalApp unused6 = WallAct.this.gApp;
                        if (GlobalApp.curAttCode.length() >= 17) {
                            GlobalApp unused7 = WallAct.this.gApp;
                            GlobalApp unused8 = WallAct.this.gApp;
                            GlobalApp.curAttId = Integer.parseInt(GlobalApp.curAttCode.substring(14, 17)) + 1;
                        }
                    }
                    Intent intent = new Intent(WallAct.this, WallPlayer.class);
                    intent.addFlags(131072);
                    WallAct.this.startActivity(intent);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.preText)).setText(this.gApp.curCityName);
        ((TextView) findViewById(R.id.currText)).setText(this.gApp.curScenicName);
        this.map_btn = (Button) findViewById(R.id.nextText);
        this.map_btn.setText("");
        this.map_btn.setVisibility(4);
        this.map_btn.setEnabled(false);
        if (this.gApp.curScenicHasMap) {
            this.map_btn.setEnabled(true);
            this.map_btn.setText("地图");
            this.map_btn.setVisibility(0);
            this.map_btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Intent intent = new Intent(WallAct.this, MapAct.class);
                    intent.addFlags(131072);
                    WallAct.this.startActivity(intent);
                }
            });
        }
        ((TextView) findViewById(R.id.preText)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (GuideConfig.curCityCode.equals("00863299")) {
                    GlobalApp unused = WallAct.this.gApp;
                    if (GlobalApp.tyHasViewMap) {
                        WallAct.this.startActivity(new Intent(WallAct.this, CityAct.class));
                        return;
                    }
                    final CommonDialog ed = new CommonDialog(WallAct.this, R.style.dialog);
                    View.OnClickListener buttonYesListener = new View.OnClickListener() {
                        public void onClick(View v) {
                            GlobalApp unused = WallAct.this.gApp;
                            GlobalApp.tyHasViewMap = true;
                            ed.dismiss();
                            Intent intent = new Intent(WallAct.this, MapAct.class);
                            intent.addFlags(131072);
                            WallAct.this.startActivity(intent);
                        }
                    };
                    View.OnClickListener buttonNoListener = new View.OnClickListener() {
                        public void onClick(View v) {
                            ed.dismiss();
                            Intent i = new Intent(WallAct.this, CityAct.class);
                            i.addFlags(131072);
                            WallAct.this.startActivity(i);
                        }
                    };
                    ed.setButtonYesListener(buttonYesListener);
                    ed.setButtonNoListener(buttonNoListener);
                    ed.setMessage("您还没使用地图功能呢!请使用");
                    ed.requestWindowFeature(1);
                    ed.show();
                    return;
                }
                WallAct.this.startActivity(new Intent(WallAct.this, ScenicAct.class));
            }
        });
        Log.d(getClass().getSimpleName(), "onResume is called");
        Log.d(getClass().getSimpleName(), "curScenicCode = " + this.curScenicCode);
        String simpleName = getClass().getSimpleName();
        StringBuilder append = new StringBuilder().append("gApp.curScenicCode = ");
        GlobalApp globalApp = this.gApp;
        Log.d(simpleName, append.append(GlobalApp.curScenicCode).toString());
        if (this.curScenicCode != null) {
            GlobalApp globalApp2 = this.gApp;
            if (GlobalApp.curScenicCode.equals(this.curScenicCode)) {
                this.attractionListAdapter.notifyDataSetChanged();
                return;
            }
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(REFRESHDATA, 0));
        GlobalApp globalApp3 = this.gApp;
        this.curScenicCode = GlobalApp.curScenicCode;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.d(getClass().getSimpleName(), "onPause is called");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.d(getClass().getSimpleName(), "onDestroy is called");
        this.executorService.shutdown();
        super.onDestroy();
    }

    private class MyAdapter extends SimpleAdapter {
        private Context context;
        private List<Map<String, String>> data;
        private Animation mAnimation;

        public MyAdapter(Context context2, List<Map<String, String>> data2, int resource, String[] from, int[] to) {
            super(context2, data2, resource, from, to);
            this.data = data2;
            this.context = context2;
            this.mAnimation = AnimationUtils.loadAnimation(context2, R.anim.alpha);
        }

        public int getCount() {
            return this.data.size();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:8:0x00bc, code lost:
            if (r0.equals(com.yhiker.oneByone.act.GlobalApp.curAttCode) != false) goto L_0x00be;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.view.View getView(int r11, android.view.View r12, android.view.ViewGroup r13) {
            /*
                r10 = this;
                r6 = r12
                android.content.Context r7 = r10.context     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r8 = "layout_inflater"
                java.lang.Object r3 = r7.getSystemService(r8)     // Catch:{ Exception -> 0x00c4 }
                android.view.LayoutInflater r3 = (android.view.LayoutInflater) r3     // Catch:{ Exception -> 0x00c4 }
                r7 = 2130903067(0x7f03001b, float:1.7412942E38)
                r8 = 0
                android.view.View r6 = r3.inflate(r7, r8)     // Catch:{ Exception -> 0x00c4 }
                java.lang.Class r7 = r10.getClass()     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r7 = r7.getSimpleName()     // Catch:{ Exception -> 0x00c4 }
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c4 }
                r8.<init>()     // Catch:{ Exception -> 0x00c4 }
                java.lang.StringBuilder r8 = r8.append(r11)     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r9 = ""
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00c4 }
                android.util.Log.i(r7, r8)     // Catch:{ Exception -> 0x00c4 }
                java.util.List<java.util.Map<java.lang.String, java.lang.String>> r7 = r10.data     // Catch:{ Exception -> 0x00c4 }
                java.lang.Object r5 = r7.get(r11)     // Catch:{ Exception -> 0x00c4 }
                java.util.Map r5 = (java.util.Map) r5     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r7 = "al_code"
                java.lang.Object r0 = r5.get(r7)     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00c4 }
                java.lang.Class r7 = r10.getClass()     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r7 = r7.getSimpleName()     // Catch:{ Exception -> 0x00c4 }
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c4 }
                r8.<init>()     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r9 = "al_code123:"
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x00c4 }
                java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00c4 }
                android.util.Log.d(r7, r8)     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r7 = "al_name"
                java.lang.Object r1 = r5.get(r7)     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x00c4 }
                if (r1 == 0) goto L_0x0075
                r7 = 2131361892(0x7f0a0064, float:1.834355E38)
                android.view.View r7 = r6.findViewById(r7)     // Catch:{ Exception -> 0x00c4 }
                android.widget.TextView r7 = (android.widget.TextView) r7     // Catch:{ Exception -> 0x00c4 }
                r7.setText(r1)     // Catch:{ Exception -> 0x00c4 }
            L_0x0075:
                java.lang.Class r7 = r10.getClass()     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r7 = r7.getName()     // Catch:{ Exception -> 0x00c4 }
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c4 }
                r8.<init>()     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r9 = " gApp.curAttSeq = "
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x00c4 }
                com.yhiker.oneByone.act.scenicpoint.WallAct r9 = com.yhiker.oneByone.act.scenicpoint.WallAct.this     // Catch:{ Exception -> 0x00c4 }
                com.yhiker.oneByone.act.GlobalApp unused = r9.gApp     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r9 = com.yhiker.oneByone.act.GlobalApp.curAttCode     // Catch:{ Exception -> 0x00c4 }
                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x00c4 }
                android.util.Log.d(r7, r8)     // Catch:{ Exception -> 0x00c4 }
                r7 = 2131361890(0x7f0a0062, float:1.8343545E38)
                android.view.View r4 = r6.findViewById(r7)     // Catch:{ Exception -> 0x00c4 }
                android.widget.ImageView r4 = (android.widget.ImageView) r4     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r7 = com.yhiker.config.GuideConfig.curCityCode     // Catch:{ Exception -> 0x00c4 }
                r8 = 0
                r10.loadImage(r7, r0, r4, r8)     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r7 = "00863299030001008"
                boolean r7 = r0.equals(r7)     // Catch:{ Exception -> 0x00c4 }
                if (r7 != 0) goto L_0x00be
                com.yhiker.oneByone.act.scenicpoint.WallAct r7 = com.yhiker.oneByone.act.scenicpoint.WallAct.this     // Catch:{ Exception -> 0x00c4 }
                com.yhiker.oneByone.act.GlobalApp unused = r7.gApp     // Catch:{ Exception -> 0x00c4 }
                java.lang.String r7 = com.yhiker.oneByone.act.GlobalApp.curAttCode     // Catch:{ Exception -> 0x00c4 }
                boolean r7 = r0.equals(r7)     // Catch:{ Exception -> 0x00c4 }
                if (r7 == 0) goto L_0x00c3
            L_0x00be:
                android.view.animation.Animation r7 = r10.mAnimation     // Catch:{ Exception -> 0x00c4 }
                r4.setAnimation(r7)     // Catch:{ Exception -> 0x00c4 }
            L_0x00c3:
                return r6
            L_0x00c4:
                r7 = move-exception
                r2 = r7
                r2.printStackTrace()
                goto L_0x00c3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.act.scenicpoint.WallAct.MyAdapter.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
        }

        private void loadImage(final String cityCode, final String curAttCode, final ImageView imgV, boolean iconed) {
            WallAct.this.executorService.submit(new Runnable() {
                public void run() {
                    final Drawable drawable;
                    Log.i("loadimg wallact", curAttCode);
                    if (WallAct.this.imgCache.containsKey(curAttCode)) {
                        drawable = (Drawable) WallAct.this.imgCache.get(curAttCode);
                    } else {
                        drawable = ImageUtil.loadBitmapByWall(cityCode, curAttCode);
                    }
                    if (drawable != null) {
                        WallAct.this.imgCache.put(curAttCode, drawable);
                        WallAct.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (drawable != null) {
                                    imgV.setBackgroundDrawable(drawable);
                                }
                            }
                        });
                    }
                }
            });
        }
    }
}
