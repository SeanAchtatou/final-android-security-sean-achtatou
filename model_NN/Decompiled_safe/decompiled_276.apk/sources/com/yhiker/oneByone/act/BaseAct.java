package com.yhiker.oneByone.act;

import android.app.Activity;
import android.content.Intent;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;
import com.yhiker.config.GuideConfig;
import com.yhiker.location.GPSReceiver;
import com.yhiker.location.LocationData;
import com.yhiker.oneByone.act.scenic.ScenicHistoryAct;
import com.yhiker.oneByone.bo.UserService;
import com.yhiker.playmate.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BaseAct extends Activity {
    public static List<Activity> activityList = new ArrayList();
    GPSReceiver m_GPSReceiver = null;
    private LocationManager m_LocationManager = null;
    private GPSHandler m_hGPSHandler = null;
    private View.OnClickListener menuaccountClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            HashMap hm = UserService.finde();
            BaseAct.this.pop.dismiss();
            if (!hm.containsKey("auto_login") || "0".equals(hm.get("auto_login"))) {
                BaseAct.this.startActivity(new Intent(BaseAct.this, LoginAct.class));
                return;
            }
            BaseAct.this.startActivity(new Intent(BaseAct.this, AccountAct.class));
        }
    };
    private View.OnClickListener menudownClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            BaseAct.this.pop.dismiss();
            BaseAct.this.startActivity(new Intent(BaseAct.this, DownCityAct.class));
        }
    };
    private View.OnClickListener menuexitClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            View.OnClickListener buttonYesListener = new View.OnClickListener() {
                public void onClick(View v) {
                    int siz = BaseAct.activityList.size();
                    for (int i = 0; i < siz; i++) {
                        if (BaseAct.activityList.get(i) != null) {
                            BaseAct.activityList.get(i).finish();
                        }
                    }
                    Process.killProcess(Process.myPid());
                }
            };
            CommonDialog ed = new CommonDialog(BaseAct.this, R.style.dialog);
            ed.setButtonYesListener(buttonYesListener);
            ed.setMessage("提示:确认退出玩伴儿吗？");
            ed.requestWindowFeature(1);
            ed.show();
            BaseAct.this.pop.dismiss();
        }
    };
    private View.OnClickListener menuhistoryClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            BaseAct.this.pop.dismiss();
            BaseAct.this.startActivity(new Intent(BaseAct.this, ScenicHistoryAct.class));
        }
    };
    protected PopupWindow pop;
    private Button searchButton;
    protected View.OnClickListener searchButtonListener;
    protected EditText searchEditText;
    protected LinearLayout searchLayout;
    private View view;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getComponentName().getClassName().equals("com.yhiker.oneByone.act.CityAct") || (GuideConfig.curCityCode != null && !"".equals(GuideConfig.curCityCode))) {
            requestWindowFeature(1);
            activityList.add(this);
            initPopupWindow();
            setupViews();
            this.m_LocationManager = (LocationManager) getSystemService("location");
            Criteria criteria = new Criteria();
            criteria.setAccuracy(2);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(1);
            this.m_LocationManager.getBestProvider(criteria, true);
            this.m_GPSReceiver = GPSReceiver.GetInstance();
            if (this.m_GPSReceiver != null) {
                this.m_GPSReceiver.Init(this.m_LocationManager, 1000, 0, this.m_hGPSHandler);
            }
            Looper looper = Looper.myLooper();
            if (looper != null) {
                this.m_hGPSHandler = new GPSHandler(looper);
                return;
            }
            return;
        }
        startActivity(new Intent(this, CityAct.class));
        finish();
    }

    /* access modifiers changed from: protected */
    public void initSearchLayout(LinearLayout searchLayout2) {
        if (searchLayout2 != null) {
            this.searchEditText = (EditText) searchLayout2.findViewById(R.id.searchedit);
            this.searchButton = (Button) searchLayout2.findViewById(R.id.searchbtn);
            this.searchButton.setOnClickListener(this.searchButtonListener);
        }
    }

    private void initPopupWindow() {
        this.view = getLayoutInflater().inflate((int) R.layout.popup_menu, (ViewGroup) null);
        this.pop = new PopupWindow(this.view, -1, -2);
        this.pop.setOutsideTouchable(true);
    }

    private void setupViews() {
        this.view.findViewById(R.id.menuhistory).setOnClickListener(this.menuhistoryClickListener);
        this.view.findViewById(R.id.menudown).setOnClickListener(this.menudownClickListener);
        this.view.findViewById(R.id.menuaccount).setOnClickListener(this.menuaccountClickListener);
        this.view.findViewById(R.id.menuexit).setOnClickListener(this.menuexitClickListener);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.i(getClass().getSimpleName(), "onKeyDown is called");
        if (!this.pop.isShowing()) {
            this.pop.showAtLocation(findViewById(R.id.linearLayout), 80, 0, 0);
            return true;
        }
        this.pop.dismiss();
        return true;
    }

    public void onBackPressed() {
        Log.i(getClass().getName(), "back pressed");
        if (this.pop.isShowing()) {
            this.pop.dismiss();
        } else {
            super.onBackPressed();
        }
    }

    private class GPSHandler extends Handler {
        public GPSHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            if (msg != null) {
                switch (msg.what) {
                    case 9000:
                        switch (msg.arg1) {
                            case 2:
                                BaseAct.this.LocateChage();
                                break;
                        }
                }
                super.handleMessage(msg);
            }
        }
    }

    public void LocateChage() {
        LocationData location;
        if (this.m_GPSReceiver != null && (location = this.m_GPSReceiver.getLocation()) != null) {
            Toast.makeText(this, "" + location.dLongitude + "," + location.dLatitude, 0).show();
        }
    }
}
