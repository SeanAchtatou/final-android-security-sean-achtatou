package com.yhiker.oneByone.act;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.bo.PayService;
import com.yhiker.oneByone.bo.UserService;
import com.yhiker.pay.ResultChecker;
import com.yhiker.playmate.R;
import com.yhiker.util.DialogUtil;

public class PayAct extends Activity {
    /* access modifiers changed from: private */
    public String body;
    /* access modifiers changed from: private */
    public String cityId;
    /* access modifiers changed from: private */
    public Activity contextParent;
    private boolean curCityIsDirect;
    GlobalApp gApp;
    private Handler handler = new Handler();
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                String strRet = (String) msg.obj;
                switch (msg.what) {
                    case 1:
                        try {
                            String memo = strRet.substring(strRet.indexOf("memo=") + "memo=".length() + 1, strRet.indexOf(";result=") - 1);
                            String resultStatus = strRet.substring(strRet.indexOf("resultStatus==") + "resultStatus==".length() + 1, strRet.indexOf(";memo=") - 1);
                            int retVal = new ResultChecker(strRet).checkSign();
                            final String user_Email = UserService.finde().get("e_mail").toString();
                            if (retVal != 1) {
                                if ("9000".equals(resultStatus)) {
                                    new Thread() {
                                        public void run() {
                                            PayService.addPay(user_Email, GuideConfig.deviceId, PayAct.this.cityId, null, PayAct.this.outTradeNo, PayAct.this.price);
                                        }
                                    }.start();
                                    DialogUtil.showDialog(PayAct.this.contextParent, "提示", "支付成功", R.drawable.infoicon);
                                    break;
                                } else {
                                    DialogUtil.showDialog(PayAct.this.contextParent, "提示", memo + ",支付没有成功,请重新进行支付.", R.drawable.infoicon);
                                    break;
                                }
                            } else {
                                DialogUtil.showDialog(PayAct.this.contextParent, "提示1", PayAct.this.getResources().getString(R.string.check_sign_failed), 17301543);
                                break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            DialogUtil.showDialog(PayAct.this.contextParent, "提示", strRet, R.drawable.infoicon);
                            break;
                        }
                }
                super.handleMessage(msg);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    };
    /* access modifiers changed from: private */
    public String outTradeNo;
    /* access modifiers changed from: private */
    public String price = "9.9";
    private String provinceId;
    /* access modifiers changed from: private */
    public String subject;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.pay);
        this.gApp = (GlobalApp) getApplication();
        this.contextParent = getParent().getParent();
        change();
    }

    public void onResume() {
        super.onResume();
        change();
    }

    public void change() {
        String curCityName = this.gApp.curCityName;
        this.cityId = GuideConfig.curCityCode;
        ((TextView) findViewById(R.id.edtPayCf)).setText("您共需支付9.9元");
        ((TextView) findViewById(R.id.edtPayCf1)).setText(curCityName + "共有" + this.gApp.curCitySceniceTotal + "个景区，推广期间只需半价，9.9元即可购买" + curCityName + "的智能语音导游.");
        this.subject = "购买" + curCityName + "导游";
        this.body = "您9.9元,购买" + curCityName + ",请现在进行支付.";
        ((Button) findViewById(R.id.btnPay)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String unused = PayAct.this.outTradeNo = PayService.getOutTradeNo(GuideConfig.deviceId + "C" + PayAct.this.cityId + "U" + UserService.finde().get("user_id").toString());
                PayService.aliPay(PayAct.this.contextParent, PayAct.this.mHandler, PayAct.this.outTradeNo, PayAct.this.subject, PayAct.this.body, PayAct.this.price);
            }
        });
    }

    public void putBuyInfo() {
        change();
    }

    public void onBackPressed() {
        Log.i(getClass().getName(), "back pressed");
        super.openOptionsMenu();
    }
}
