package com.yhiker.oneByone.act;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.act.IDownloadService;
import com.yhiker.oneByone.bo.CityService;
import com.yhiker.playmate.R;
import com.yhiker.playmate.SpaceSize;
import java.io.File;
import java.util.Map;

public class DownDlg extends CommonDialog {
    IDownloadService downService;
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            DownDlg.this.downService = IDownloadService.Stub.asInterface(service);
            Log.i("ScenicTabAct", "downService connected");
        }

        public void onServiceDisconnected(ComponentName className) {
            DownDlg.this.downService = null;
        }
    };

    public DownDlg(Context context, int style) {
        super(context, style);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Intent mIntent = new Intent(getContext(), DownloadService.class);
        Context context = getContext();
        getContext().startService(mIntent);
        getContext().getApplicationContext().bindService(mIntent, this.mConnection, 1);
        final String curCityCode = GlobalApp.curCityCodeDowing;
        Map<String, Object> p = CityService.getCityCodes(curCityCode);
        final String curCityName = p.get("cc_name").toString();
        String curCitySceniceTotal = p.get("slc_scenic_total").toString();
        final String zipSizestr = p.get("slc_scenics_zip").toString();
        int zipSize = 0;
        try {
            zipSize = Integer.parseInt(zipSizestr) / 1048576;
        } catch (Exception e) {
            e.printStackTrace();
        }
        setButtonYesListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    DownDlg.this.dismiss();
                    if (((long) Integer.parseInt(zipSizestr)) > SpaceSize.getAvailableExternalMemorySize()) {
                        Log.i("avilibale size", ((SpaceSize.getAvailableExternalMemorySize() / 1024) / 1024) + "M");
                        final AlertDialog commonDialog = new AlertDialog(DownDlg.this.getContext(), R.style.dialog);
                        commonDialog.setButtonYesListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                commonDialog.dismiss();
                            }
                        });
                        commonDialog.setMessage("提示:可用空间不足，您也可以在线使用");
                        commonDialog.show();
                    } else if (!DownDlg.this.downService.downloadFile(ConfigHp.down_notitle, curCityName, "http://58.211.138.180:88/0420/0086" + File.separator + curCityCode + File.separator + curCityCode + ".zip", curCityCode + ".zip", GuideConfig.getRootDir() + ConfigHp.update_dir, ConfigHp.down_broad, 10000, "20112011")) {
                        final AlertDialog commonDialog2 = new AlertDialog(DownDlg.this.getContext(), R.style.dialog);
                        commonDialog2.setButtonYesListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                commonDialog2.dismiss();
                            }
                        });
                        commonDialog2.setMessage("提示:目前支持单个下载，请稍后重试");
                        commonDialog2.show();
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
        setMessage(curCityName + "市现有" + curCitySceniceTotal + "个景区，导游资料共" + zipSize + "兆，" + "建议您通过WIFI下载。如果意外中断，重新下载会从中断的地方开始。确认下载吗");
        super.onCreate(savedInstanceState);
    }
}
