package com.yhiker.oneByone.act;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.act.IDownloadService;
import com.yhiker.oneByone.act.scenic.ScenicAct;
import com.yhiker.playmate.R;
import com.yhiker.util.ImageUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DownCityAct extends BaseAct {
    LazyAdapter cityAdapter;
    ListAdapter cityListAdapter;
    List<HashMap<String, String>> clist;
    List<HashMap<String, String>> dlist;
    LazyAdapter downAdapter;
    IDownloadService downService;
    GlobalApp gApp;
    ListView listCity;
    ListView listdownedCity;
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            DownCityAct.this.downService = IDownloadService.Stub.asInterface(service);
            Log.i("ScenicTabAct", "downService connected");
        }

        public void onServiceDisconnected(ComponentName className) {
            DownCityAct.this.downService = null;
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.dlist = queryListData(1);
        this.clist = queryListData(0);
        if (this.dlist.isEmpty() || this.dlist.size() <= 0) {
            setContentView((int) R.layout.downolcityact);
            initOlCity();
        } else {
            setContentView((int) R.layout.downcityact);
            initCity();
        }
        this.gApp = (GlobalApp) getApplication();
        Intent mIntent = new Intent(this, DownloadService.class);
        startService(mIntent);
        if (this.mConnection != null) {
            getApplicationContext().bindService(mIntent, this.mConnection, 1);
        }
    }

    public void initCity() {
        this.listCity = (ListView) findViewById(R.id.listcity);
        this.listdownedCity = (ListView) findViewById(R.id.listdown);
        this.cityAdapter = new LazyAdapter(this, this.clist);
        this.downAdapter = new LazyAdapter(this, this.dlist);
        this.listCity.setAdapter((ListAdapter) this.cityAdapter);
        this.listdownedCity.setAdapter((ListAdapter) this.downAdapter);
        this.listdownedCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                HashMap<String, String> hm = DownCityAct.this.dlist.get(arg2);
                GuideConfig.curCityCode = (String) hm.get("slc_city_code");
                DownCityAct.this.gApp.curCityName = (String) hm.get("cc_name");
                GlobalApp globalApp = DownCityAct.this.gApp;
                GlobalApp.curCitySeq = Integer.parseInt((String) hm.get("_id"));
                DownCityAct.this.startActivity(new Intent(DownCityAct.this, ScenicAct.class));
            }
        });
        this.listCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                HashMap<String, String> hm = DownCityAct.this.clist.get(arg2);
                String curCityCode = (String) hm.get("slc_city_code");
                String str = (String) hm.get("slc_scenics_zip");
                GuideConfig.curCityCode = curCityCode;
                GlobalApp.curCityCodeDowing = curCityCode;
                DownCityAct.this.gApp.curCityName = (String) hm.get("cc_name");
                DownDlg cd = new DownDlg(DownCityAct.this, R.style.dialog);
                cd.requestWindowFeature(1);
                cd.show();
            }
        });
    }

    public void initOlCity() {
        this.listCity = (ListView) findViewById(R.id.listcity);
        this.cityAdapter = new LazyAdapter(this, this.clist);
        this.listCity.setAdapter((ListAdapter) this.cityAdapter);
        this.listCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                GlobalApp.curCityCodeDowing = (String) DownCityAct.this.clist.get(arg2).get("slc_city_code");
                DownDlg cd = new DownDlg(DownCityAct.this, R.style.dialog);
                cd.requestWindowFeature(1);
                cd.show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private List<HashMap<String, String>> queryListData(int downed) {
        Cursor scenic_list_city_res;
        SQLiteDatabase scenic_list_db = null;
        Cursor scenic_list_city_res2 = null;
        List<HashMap<String, String>> ls = new ArrayList<>();
        try {
            SQLiteDatabase scenic_list_db2 = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.scenic_list_path, null, 16);
            if (downed == 1) {
                scenic_list_city_res = scenic_list_db2.rawQuery("SELECT scenic_list_city.scenic_list_city_seq _id,scenic_list_city.slc_city_code slc_city_code,scenic_list_city.slc_intro slc_intr,city_code.cc_name cc_name,scenic_list_city.slc_downed slc_downed,scenic_list_city.slc_scenic_total slc_scenic_total,scenic_list_city.slc_scenics_zip slc_scenics_zip FROM scenic_list_city left outer join city_code on scenic_list_city.slc_city_code=city_code.cc_code  where slc_downed='1'order by  scenic_list_city.slc_order", null);
            } else {
                scenic_list_city_res = scenic_list_db2.rawQuery("SELECT scenic_list_city.scenic_list_city_seq _id,scenic_list_city.slc_city_code slc_city_code,scenic_list_city.slc_intro slc_intr,city_code.cc_name cc_name,scenic_list_city.slc_downed slc_downed,scenic_list_city.slc_scenic_total slc_scenic_total ,scenic_list_city.slc_scenics_zip slc_scenics_zip FROM scenic_list_city left outer join city_code on scenic_list_city.slc_city_code=city_code.cc_code  where slc_downed = '0'order by scenic_list_city.slc_order", null);
            }
            scenic_list_city_res.moveToFirst();
            while (!scenic_list_city_res.isAfterLast()) {
                String[] columnNames = scenic_list_city_res.getColumnNames();
                HashMap<String, String> hm = new HashMap<>();
                for (String columnName : columnNames) {
                    hm.put(columnName, scenic_list_city_res.getString(scenic_list_city_res.getColumnIndex(columnName)));
                }
                scenic_list_city_res.moveToNext();
                ls.add(hm);
            }
            if (scenic_list_city_res != null) {
                scenic_list_city_res.close();
            }
            if (scenic_list_db2 != null) {
                scenic_list_db2.close();
            }
        } catch (Exception e) {
            Exception ex = e;
            Log.e(getClass().getName(), ex.toString());
            ex.printStackTrace();
            if (scenic_list_city_res2 != null) {
                scenic_list_city_res2.close();
            }
            if (scenic_list_db != null) {
                scenic_list_db.close();
            }
        } catch (Throwable th) {
            if (scenic_list_city_res2 != null) {
                scenic_list_city_res2.close();
            }
            if (scenic_list_db != null) {
                scenic_list_db.close();
            }
            throw th;
        }
        return ls;
    }

    class LazyAdapter extends ArrayAdapter<HashMap<String, String>> {
        private static final int mResource = 2130903046;
        private Animation mAnimation;
        protected LayoutInflater mInflater;

        public LazyAdapter(Context context, List<HashMap<String, String>> vlaues) {
            super(context, (int) R.layout.city_item, vlaues);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            this.mAnimation = AnimationUtils.loadAnimation(context, R.anim.alpha);
        }

        /* Debug info: failed to restart local var, previous not found, register: 10 */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            HashMap<String, String> mp = (HashMap) getItem(position);
            if (convertView == null) {
                view = this.mInflater.inflate((int) R.layout.city_item, parent, false);
            } else {
                view = convertView;
            }
            String slc_city_code = (String) mp.get("slc_city_code");
            ImageView iv = (ImageView) view.findViewById(R.id.cityImg);
            iv.setImageBitmap(BitmapFactory.decodeFile(GuideConfig.getRootDir() + ConfigHp.scenic_list_data_ver_dir + File.separator + slc_city_code + File.separator + "img" + File.separator + slc_city_code + ".jpg"));
            iv.setImageBitmap(ImageUtil.loadBitmapByCityCode(getContext(), slc_city_code));
            String cc_name = (String) mp.get("cc_name");
            if (DownCityAct.this.gApp.curCityCodeForPlay != null) {
                if (slc_city_code.equals(DownCityAct.this.gApp.curCityCodeForPlay)) {
                    iv.setAnimation(this.mAnimation);
                } else {
                    iv.clearAnimation();
                }
            }
            if (DownCityAct.this.gApp.curCityCodeForLocation != null) {
                if (slc_city_code.equals(DownCityAct.this.gApp.curCityCodeForLocation)) {
                    ((TextView) view.findViewById(R.id.nearest_city)).setVisibility(0);
                } else {
                    ((TextView) view.findViewById(R.id.nearest_city)).setVisibility(4);
                }
            }
            if (cc_name != null) {
                ((TextView) view.findViewById(R.id.cityName)).setText(cc_name);
            }
            String slc_intr = (String) mp.get("slc_intr");
            if (slc_intr != null) {
                ((TextView) view.findViewById(R.id.cityIntr)).setText(slc_intr);
            }
            return view;
        }
    }
}
