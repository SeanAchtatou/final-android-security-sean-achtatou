package com.yhiker.oneByone.act;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.act.scenic.ScenicAct;
import com.yhiker.oneByone.bo.CityBo;
import com.yhiker.oneByone.module.City;
import com.yhiker.playmate.R;
import com.yhiker.util.ImageUtil;
import java.util.List;

public class CityAct extends BaseAct {
    CityAdapter cityAdapter;
    GlobalApp gApp;
    ListView listCity;
    List<City> listCityDate;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.city);
        this.searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
        this.gApp = (GlobalApp) getApplication();
        load();
    }

    public void load() {
        this.listCityDate = CityBo.getCity(GuideConfig.getInitDataDir() + ConfigHp.scenic_list_path);
        this.listCity = (ListView) findViewById(R.id.listcity);
        this.cityAdapter = new CityAdapter(this, this.listCityDate);
        this.listCity.setAdapter((ListAdapter) this.cityAdapter);
        this.listCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                City city = CityAct.this.listCityDate.get(arg2);
                String curCityCode = city.getCode();
                String curCityName = city.getName();
                GuideConfig.curCityCode = curCityCode;
                GlobalApp globalApp = CityAct.this.gApp;
                GlobalApp.curCitySeq = Integer.parseInt(city.getId());
                CityAct.this.gApp.curCityName = curCityName;
                CityAct.this.startActivity(new Intent(CityAct.this, ScenicAct.class));
            }
        });
        ((TextView) findViewById(R.id.currText)).setText("中国");
        ((TextView) findViewById(R.id.preText)).setVisibility(4);
        ((TextView) findViewById(R.id.nextText)).setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    class CityAdapter extends ArrayAdapter<List> {
        private static final int mResource = 2130903046;
        private Animation mAnimation;
        protected LayoutInflater mInflater;

        public CityAdapter(Context context, List vlaues) {
            super(context, (int) R.layout.city_item, vlaues);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            this.mAnimation = AnimationUtils.loadAnimation(context, R.anim.alpha);
        }

        /* Debug info: failed to restart local var, previous not found, register: 10 */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            City city = (City) getItem(position);
            if (convertView == null) {
                view = this.mInflater.inflate((int) R.layout.city_item, parent, false);
            } else {
                view = convertView;
            }
            String slc_city_code = city.getCode();
            String cc_name = city.getName();
            String slc_intr = city.getIntroduce();
            String slc_downed = city.getDowned();
            if (CityAct.this.gApp.curCityCodeForLocation != null) {
                if (slc_city_code.equals(CityAct.this.gApp.curCityCodeForLocation)) {
                    ((TextView) view.findViewById(R.id.nearest_city)).setVisibility(0);
                } else {
                    ((TextView) view.findViewById(R.id.nearest_city)).setVisibility(4);
                }
            }
            ((TextView) view.findViewById(R.id.havedowned_city)).setVisibility(4);
            if ("1".equals(slc_downed)) {
                ((TextView) view.findViewById(R.id.havedowned_city)).setVisibility(0);
            }
            ImageView iv = (ImageView) view.findViewById(R.id.cityImg);
            iv.setImageBitmap(ImageUtil.loadBitmapByCityCode(getContext(), slc_city_code));
            if (CityAct.this.gApp.curCityCodeForPlay != null) {
                if (slc_city_code.equals(CityAct.this.gApp.curCityCodeForPlay)) {
                    iv.setAnimation(this.mAnimation);
                } else {
                    iv.clearAnimation();
                }
            }
            if (cc_name != null) {
                ((TextView) view.findViewById(R.id.cityName)).setText(cc_name);
            }
            if (slc_intr != null) {
                ((TextView) view.findViewById(R.id.cityIntr)).setText(slc_intr);
            }
            return view;
        }
    }
}
