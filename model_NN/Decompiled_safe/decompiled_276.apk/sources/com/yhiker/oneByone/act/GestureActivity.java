package com.yhiker.oneByone.act;

import android.os.Build;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.yhiker.guid.module.MapArea;
import com.yhiker.guid.service.ParkMapAction;
import com.yhiker.playmate.R;

public class GestureActivity extends BaseAct implements GestureDetector.OnGestureListener, View.OnTouchListener {
    private double distance1 = -1.0d;
    private double distance2;
    private boolean downFlag = false;
    private ImageButton largeButton;
    private GestureDetector mGestureDetector;
    private int mapImageX;
    private int mapImageY;
    private boolean multiFlag = false;
    private boolean scaleFlag = false;
    private ImageButton smallButton;
    public RelativeLayout v;
    private float x1;
    private float x2;
    private float y1;
    private float y2;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.maplayout);
        initGesture();
        initScaleButton();
    }

    public void initGesture() {
        this.v = (RelativeLayout) findViewById(R.id.linearLayout);
        this.mGestureDetector = new GestureDetector(this);
        this.v.setOnTouchListener(this);
        this.v.setLongClickable(true);
        this.mGestureDetector.setIsLongpressEnabled(true);
    }

    public void initScaleButton() {
        this.largeButton = (ImageButton) findViewById(R.id.largebutton);
        this.largeButton.setBackgroundResource(R.drawable.largeselector);
        this.largeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ParkMapAction.large();
            }
        });
        this.smallButton = (ImageButton) findViewById(R.id.smallbutton);
        this.smallButton.setBackgroundResource(R.drawable.smallselector);
        this.smallButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ParkMapAction.small();
            }
        });
    }

    public boolean onTouch(View v2, MotionEvent event) {
        int piontCounts = 0;
        if (Build.VERSION.SDK_INT > 4) {
            try {
                piontCounts = event.getPointerCount();
            } catch (Exception e) {
                piontCounts = 1;
            }
        }
        if (piontCounts < 2) {
            return this.mGestureDetector.onTouchEvent(event);
        }
        if (this.downFlag) {
            this.x1 = event.getX(0);
            this.y1 = event.getY(0);
            this.x2 = event.getX(1);
            this.y2 = event.getY(1);
            this.distance1 = Math.hypot((double) (this.x1 - this.x2), (double) (this.y1 - this.y2));
            if (this.distance1 < 70.0d) {
                this.distance1 = -1.0d;
            }
            this.downFlag = false;
            this.multiFlag = true;
            this.scaleFlag = true;
        } else {
            this.x1 = event.getX(0);
            this.y1 = event.getY(0);
            this.x2 = event.getX(1);
            this.y2 = event.getY(1);
            this.distance2 = Math.hypot((double) (this.x1 - this.x2), (double) (this.y1 - this.y2));
            if (this.distance1 > 0.0d && this.scaleFlag) {
                if (this.distance2 - this.distance1 > 100.0d) {
                    ParkMapAction.large();
                    this.scaleFlag = false;
                } else if (this.distance1 - this.distance2 > 100.0d) {
                    ParkMapAction.small();
                    this.scaleFlag = false;
                }
            }
        }
        return true;
    }

    public boolean onDown(MotionEvent e) {
        this.downFlag = true;
        this.multiFlag = false;
        return false;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        try {
            MapArea mapArea = MapArea.getInstance();
            if (this.multiFlag) {
                return false;
            }
            if (this.downFlag) {
                this.mapImageX = mapArea.getLayoutParamsX();
                this.mapImageY = mapArea.getLayoutParamsY();
                this.downFlag = false;
            }
            int x12 = ((int) e1.getX()) - this.mapImageX;
            int y12 = ((int) e1.getY()) - this.mapImageY;
            ParkMapAction.moveMapImage(((int) e2.getX()) - x12, ((int) e2.getY()) - y12);
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    public void onLongPress(MotionEvent e) {
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }
}
