package com.yhiker.oneByone.act;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.bo.UserService;
import com.yhiker.pay.NetXMLDataFactory;
import com.yhiker.playmate.R;
import com.yhiker.util.DialogUtil;
import com.yhiker.util.HttpClient;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Document;

public class AccountUpDia extends Dialog {
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public ProgressDialog mProgress = null;
    public Handler mhandler = null;

    public AccountUpDia(Context context) {
        super(context);
    }

    public AccountUpDia(Context context, int style) {
        super(context, style);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.account_update);
        HashMap m = UserService.finde();
        final String userEmail = m.get("e_mail").toString();
        String nickName = m.get("nick").toString();
        if (m.containsKey("password")) {
            m.get("password").toString();
        }
        String mobile = m.get("mobile").toString();
        final TextView nickName_view = (TextView) findViewById(R.id.accout_update_nick);
        nickName_view.setText(nickName);
        final TextView pwd_view = (TextView) findViewById(R.id.accout_update_old_pwd);
        pwd_view.setText("");
        final TextView newpwd_view = (TextView) findViewById(R.id.accout_update_new_pwd);
        final TextView renewpwd_view = (TextView) findViewById(R.id.accout_update_new_repwd);
        final TextView mobile_view = (TextView) findViewById(R.id.accout_update_mobile);
        mobile_view.setText(mobile);
        ((Button) findViewById(R.id.button_yes)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String dpwd = pwd_view.getEditableText().toString().trim();
                final String npwd = newpwd_view.getEditableText().toString().trim();
                String renpwd = renewpwd_view.getEditableText().toString().trim();
                final String mobile = mobile_view.getEditableText().toString().trim();
                final String nick = nickName_view.getEditableText().toString().trim();
                if (nick.trim().equals("")) {
                    Toast.makeText(AccountUpDia.this.getContext(), "昵称不能为空", 1).show();
                } else if (dpwd.equals("")) {
                    Toast.makeText(AccountUpDia.this.getContext(), "原始密码不能为空", 1).show();
                } else if (!dpwd.equals("")) {
                    Toast.makeText(AccountUpDia.this.getContext(), "原始密码输入不正确", 1).show();
                } else if (npwd.equals("")) {
                    Toast.makeText(AccountUpDia.this.getContext(), "新密码不能为空", 1).show();
                } else if (!renpwd.equals(npwd)) {
                    Toast.makeText(AccountUpDia.this.getContext(), "新密码输入不一致", 1).show();
                } else {
                    ProgressDialog unused = AccountUpDia.this.mProgress = DialogUtil.showProgress(AccountUpDia.this.getContext(), "修改", "修改中", false, true);
                    new Thread() {
                        public void run() {
                            try {
                                sleep(5);
                                Map valuesMap = new HashMap();
                                valuesMap.put("email", userEmail);
                                valuesMap.put("password", npwd);
                                valuesMap.put("telNumber", mobile);
                                valuesMap.put("nickName", nick);
                                valuesMap.put("deviceId", GuideConfig.deviceId);
                                Document reg_doc = HttpClient.doPostToXml(GuideConfig.URL_ACCOUNT_UPDATE, valuesMap);
                                if (reg_doc == null) {
                                    AccountUpDia.this.handler.post(new Runnable() {
                                        public void run() {
                                            AccountUpDia.this.closeProgress();
                                            Toast.makeText(AccountUpDia.this.getContext(), "修改失败", 1).show();
                                        }
                                    });
                                    return;
                                }
                                if (Boolean.parseBoolean(NetXMLDataFactory.getInstance().getTextNodeByTagName("Suc", reg_doc))) {
                                    UserService.update(null, nick, npwd, mobile);
                                    AccountUpDia.this.handler.post(new Runnable() {
                                        public void run() {
                                            AccountUpDia.this.mhandler.sendEmptyMessage(9);
                                            AccountUpDia.this.closeProgress();
                                            Toast.makeText(AccountUpDia.this.getContext(), "修改成功", 1).show();
                                            AccountUpDia.this.dismiss();
                                        }
                                    });
                                } else {
                                    AccountUpDia.this.handler.post(new Runnable() {
                                        public void run() {
                                            AccountUpDia.this.closeProgress();
                                            Toast.makeText(AccountUpDia.this.getContext(), "修改失败", 1).show();
                                        }
                                    });
                                }
                                sleep(10);
                                AccountUpDia.this.closeProgress();
                            } catch (Exception e) {
                                e.printStackTrace();
                                AccountUpDia.this.handler.post(new Runnable() {
                                    public void run() {
                                        AccountUpDia.this.closeProgress();
                                        Toast.makeText(AccountUpDia.this.getContext(), "注册失败", 1).show();
                                    }
                                });
                            }
                        }
                    }.start();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.d("TAG", "+++++++++++++++++++++++++++");
    }

    /* access modifiers changed from: package-private */
    public void closeProgress() {
        try {
            if (this.mProgress != null) {
                this.mProgress.dismiss();
                this.mProgress = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
