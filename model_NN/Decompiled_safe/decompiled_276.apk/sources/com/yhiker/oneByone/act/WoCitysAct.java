package com.yhiker.oneByone.act;

import android.app.ActivityGroup;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.bo.CityService;
import com.yhiker.oneByone.bo.UserService;
import com.yhiker.playmate.R;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bouncycastle.i18n.MessageBundle;

public class WoCitysAct extends ListActivity {
    private static final String TAG = "ParksActivity-";
    GlobalApp gApp;
    LocationManager gpsManager;
    HashMap m;
    private List<Map<String, Object>> myData;
    private SimpleAdapter mySimperAdapter;
    String woTabId;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.m = UserService.finde();
    }

    public void onResume() {
        super.onResume();
        if (!"Guest".equals(this.m.get("e_mail").toString())) {
            load();
        }
    }

    private void load() {
        this.woTabId = getIntent().getStringExtra("woTabId");
        this.gApp = (GlobalApp) getApplication();
        if ("down".equals(this.woTabId)) {
            this.myData = CityService.getPaidCityListDB(this, "down", this.gApp);
        } else {
            this.myData = CityService.getPaidCityListDB(this, "paid", this.gApp);
        }
        this.mySimperAdapter = new myAdapter(this, this.myData, R.layout.citys, new String[]{MessageBundle.TITLE_ENTRY, "introduce", "img"}, new int[]{R.id.citys_title, R.id.citys_introduce, R.id.citys_img});
        setListAdapter(this.mySimperAdapter);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        String cityId = ((Map) l.getItemAtPosition(position)).get("id").toString();
        LinearLayout container = (LinearLayout) ((ActivityGroup) getParent()).getWindow().findViewById(R.id.citysgroup_view);
        container.removeAllViews();
        Intent intent = new Intent();
        intent.putExtra("cityId", cityId);
        intent.putExtra("woTabId", this.woTabId);
        intent.addFlags(67108864);
        intent.setClass(getApplicationContext(), WoParksAct.class);
        View view = ((ActivityGroup) getParent()).getLocalActivityManager().startActivity("ParksAct", intent).getDecorView();
        container.addView(view);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.width = -1;
        params.height = -1;
        view.setLayoutParams(params);
    }

    class myAdapter extends SimpleAdapter {
        private List<? extends Map<String, ?>> mData;
        private int mDropDownResource;
        private String[] mFrom;
        private LayoutInflater mInflater;
        private int mResource;
        private int[] mTo;
        private SimpleAdapter.ViewBinder mViewBinder;

        public myAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            this.mData = data;
            this.mDropDownResource = resource;
            this.mResource = resource;
            this.mFrom = from;
            this.mTo = to;
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            return createViewFromResource(position, convertView, parent, this.mResource);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        private View createViewFromResource(int position, View convertView, ViewGroup parent, int resource) {
            View v;
            if (convertView == null) {
                v = this.mInflater.inflate(resource, parent, false);
                int[] to = this.mTo;
                int count = to.length;
                View[] holder = new View[count];
                for (int i = 0; i < count; i++) {
                    holder[i] = v.findViewById(to[i]);
                }
                v.setTag(holder);
            } else {
                v = convertView;
            }
            bindView(position, v);
            return v;
        }

        private void bindView(int position, View view) {
            Map dataSet = (Map) this.mData.get(position);
            if (dataSet != null) {
                SimpleAdapter.ViewBinder viewBinder = this.mViewBinder;
                View[] holder = (View[]) view.getTag();
                String[] from = this.mFrom;
                int count = this.mTo.length;
                for (int i = 0; i < count; i++) {
                    View v = holder[i];
                    if (v != null) {
                        if (v instanceof ImageView) {
                            String slc_city_code = dataSet.get("city_code").toString();
                            Bitmap data = BitmapFactory.decodeFile(GuideConfig.getRootDir() + ConfigHp.scenic_list_data_ver_dir + File.separator + slc_city_code + File.separator + slc_city_code + ".jpg");
                            if (data instanceof Integer) {
                                setViewImage((ImageView) v, ((Integer) data).intValue());
                            } else if (data instanceof Bitmap) {
                                setViewImage((ImageView) v, data);
                            }
                        } else if (v instanceof TextView) {
                            setViewText((TextView) v, dataSet.get(from[i]).toString());
                        }
                    }
                }
            }
        }

        public void setViewImage(ImageView v, int value) {
            v.setImageResource(value);
        }

        public void setViewImage(ImageView v, Bitmap bm) {
            v.setImageBitmap(bm);
        }
    }
}
