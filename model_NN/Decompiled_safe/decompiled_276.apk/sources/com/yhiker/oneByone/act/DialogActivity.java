package com.yhiker.oneByone.act;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.yhiker.playmate.R;

public class DialogActivity extends Activity {
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            DialogActivity.this.finish();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.dialogactivity);
        ((TextView) findViewById(R.id.textview)).setText("很抱歉，没有找到与搜索条件相匹配的景区或景点，请更换搜索条件再次尝试！");
        ((Button) findViewById(R.id.button_yes)).setOnClickListener(this.clickListener);
        ((Button) findViewById(R.id.button_no)).setOnClickListener(this.clickListener);
    }
}
