package com.yhiker.oneByone.act.scenic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.act.BaseAct;
import com.yhiker.oneByone.act.CityAct;
import com.yhiker.oneByone.act.DownDlg;
import com.yhiker.oneByone.act.GlobalApp;
import com.yhiker.oneByone.act.scenicpoint.WallAct;
import com.yhiker.oneByone.bo.ParkService;
import com.yhiker.oneByone.bo.ScenicBo;
import com.yhiker.oneByone.module.Scenic;
import com.yhiker.playmate.R;
import com.yhiker.util.ImageUtil;
import java.util.List;

public class ScenicAct extends BaseAct {
    /* access modifiers changed from: private */
    public String curCityCode;
    GlobalApp gApp;
    double lastLat = 0.0d;
    double lastLon = 0.0d;
    ListView listScenic;
    List<Scenic> listScenicDate;
    ScenicAdapter scenicAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.scenic);
        this.searchLayout = (LinearLayout) findViewById(R.id.searchLayout);
        this.gApp = (GlobalApp) getApplication();
        load();
    }

    public void load() {
        this.lastLon = 0.0d;
        this.lastLat = 0.0d;
        if (this.gApp.netLocation != null) {
            this.lastLon = this.gApp.netLocation.getLongitude();
            this.lastLat = this.gApp.netLocation.getLatitude();
        }
        this.curCityCode = GuideConfig.curCityCode;
        this.listScenicDate = ScenicBo.getScenicByCityCode(GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + GuideConfig.curCityCode.toLowerCase() + ".db", GuideConfig.curCityCode, this.lastLon, this.lastLat);
        this.listScenic = (ListView) findViewById(R.id.listscenic);
        this.scenicAdapter = new ScenicAdapter(this, this.listScenicDate);
        this.listScenic.setAdapter((ListAdapter) this.scenicAdapter);
        this.listScenic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Log.i(ScenicAct.this.getClass().getSimpleName(), "onItemClick is called");
                Scenic scenic = ScenicAct.this.listScenicDate.get(arg2);
                String curScenicCode = scenic.getCode();
                ScenicAct.this.gApp.curScenicName = scenic.getName();
                GlobalApp globalApp = ScenicAct.this.gApp;
                GlobalApp globalApp2 = ScenicAct.this.gApp;
                GlobalApp.preCurScenicCode = GlobalApp.curScenicCode;
                GlobalApp globalApp3 = ScenicAct.this.gApp;
                GlobalApp.curScenicCode = curScenicCode;
                ScenicAct.this.gApp.curScenicHasMap = scenic.getHasMap() >= 1;
                new Thread() {
                    public void run() {
                        GlobalApp globalApp = ScenicAct.this.gApp;
                        ParkService.addHistory(GlobalApp.curScenicCode, GuideConfig.curCityCode);
                    }
                }.start();
                ScenicBo.getScenicKey(GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + GuideConfig.curCityCode.toLowerCase() + ".db", GlobalApp.curScenicCode);
                Intent intent = new Intent(ScenicAct.this, WallAct.class);
                intent.addFlags(131072);
                ScenicAct.this.startActivity(intent);
            }
        });
        ((TextView) findViewById(R.id.preText)).setText("中国");
        ((TextView) findViewById(R.id.currText)).setText(this.gApp.curCityName);
        ((TextView) findViewById(R.id.nextText)).setText("下载");
        ((TextView) findViewById(R.id.preText)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ScenicAct.this.startActivity(new Intent(ScenicAct.this, CityAct.class));
            }
        });
        ((TextView) findViewById(R.id.nextText)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                GlobalApp.curCityCodeDowing = ScenicAct.this.curCityCode;
                DownDlg cd = new DownDlg(ScenicAct.this, R.style.dialog);
                cd.requestWindowFeature(1);
                cd.show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    class ScenicAdapter extends ArrayAdapter<List> {
        private static final int mResource = 2130903073;
        private Animation mAnimation;
        protected LayoutInflater mInflater;

        public ScenicAdapter(Context context, List vlaues) {
            super(context, (int) R.layout.scenic_item, vlaues);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            this.mAnimation = AnimationUtils.loadAnimation(context, R.anim.alpha);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            Log.i(ScenicAct.class.getSimpleName(), "getView is called");
            Scenic scenic = (Scenic) getItem(position);
            if (convertView == null) {
                view = this.mInflater.inflate(R.layout.scenic_item, parent, false);
            } else {
                view = convertView;
            }
            String slc_city_code = scenic.getCityCode();
            String sl_code = scenic.getCode();
            String sl_intr = scenic.getIntro();
            String level = "" + scenic.getLevel();
            String sl_name = scenic.getName();
            String sl_addr = scenic.getAddress();
            if (sl_name != null) {
                ((TextView) view.findViewById(R.id.scenicName)).setText(sl_name);
            }
            if (sl_intr != null) {
                ((TextView) view.findViewById(R.id.scenicIntr)).setText(sl_intr);
            }
            if (sl_addr != null) {
                ((TextView) view.findViewById(R.id.scenicAddr)).setText(sl_addr);
            }
            ImageView iv = (ImageView) view.findViewById(R.id.scenicImg);
            ((ImageView) view.findViewById(R.id.city_parks_level)).setImageDrawable(ScenicAct.this.getResources().getDrawable(ScenicAct.this.getResources().getIdentifier("level" + level, "drawable", ScenicAct.this.getPackageName())));
            iv.setImageBitmap(ImageUtil.loadBitmapByScenicCode(getContext(), slc_city_code, sl_code));
            ((TextView) view.findViewById(R.id.nearest_scenic)).setVisibility(4);
            if (ScenicAct.this.gApp.curCityCodeForLocation != null && ScenicAct.this.gApp.curCityCodeForLocation.equals(slc_city_code) && position == 0) {
                ((TextView) view.findViewById(R.id.nearest_scenic)).setVisibility(0);
            }
            if (ScenicAct.this.gApp.curScenicCodeForPlay != null) {
                if (sl_code.equals(ScenicAct.this.gApp.curScenicCodeForPlay)) {
                    iv.setAnimation(this.mAnimation);
                } else {
                    iv.clearAnimation();
                }
            }
            String intro = scenic.getIntro();
            return view;
        }
    }
}
