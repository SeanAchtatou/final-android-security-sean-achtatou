package com.yhiker.oneByone.act;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.yhiker.playmate.R;

public class AlertDialog extends Dialog {
    private View.OnClickListener buttonNoListener = new View.OnClickListener() {
        public void onClick(View v) {
            AlertDialog.this.dismiss();
        }
    };
    private View.OnClickListener buttonYesListener = new View.OnClickListener() {
        public void onClick(View v) {
            AlertDialog.this.dismiss();
        }
    };
    private String message;

    public AlertDialog(Context context, int theme) {
        super(context, theme);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.alert_dialog);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        ((TextView) findViewById(R.id.message)).setText(this.message);
        ((Button) findViewById(R.id.button_yes)).setOnClickListener(this.buttonYesListener);
    }

    public void setButtonYesListener(View.OnClickListener buttonYesListener2) {
        this.buttonYesListener = buttonYesListener2;
    }

    public void setButtonNoListener(View.OnClickListener buttonNoListener2) {
        this.buttonNoListener = buttonNoListener2;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }
}
