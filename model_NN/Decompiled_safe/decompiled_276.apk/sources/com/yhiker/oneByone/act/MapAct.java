package com.yhiker.oneByone.act;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.yhiker.config.GuideConfig;
import com.yhiker.guid.menu.CommendWayDialog;
import com.yhiker.guid.menu.GetCommendWay;
import com.yhiker.guid.module.MovePoint;
import com.yhiker.guid.module.ParkDataInfo;
import com.yhiker.guid.service.BCButtonAction;
import com.yhiker.guid.service.ParkMapAction;
import com.yhiker.oneByone.act.mediaplayer.PlayerPanelView;
import com.yhiker.oneByone.act.scenic.ScenicAct;
import com.yhiker.oneByone.act.scenicpoint.WallAct;
import com.yhiker.oneByone.act.scenicpoint.WallPlayer;
import com.yhiker.oneByone.bo.LogService;
import com.yhiker.oneByone.view.BroadcastView;
import com.yhiker.playmate.DebugUtil;
import com.yhiker.playmate.R;
import java.lang.Thread;

public class MapAct extends LocationActivity {
    private static final int MEDIA_PLAY = 5;
    private static final int NOMAP_OR_NETERR = 101;
    private static final int PARKTICKET_ID = 2;
    private static final int QUIT_ID = 4;
    private static final int SETTING_ID = 3;
    /* access modifiers changed from: private */
    public BroadcastView circleView;
    /* access modifiers changed from: private */
    public boolean isLoadNewMap = true;
    Handler mapActHandler = new Handler() {
        /* Debug info: failed to restart local var, previous not found, register: 9 */
        public void handleMessage(Message msg) {
            Log.i("main looper", "msg=" + msg.what);
            switch (msg.what) {
                case 7:
                    MapAct.this.v.setEnabled(true);
                    MapAct.this.v.findViewById(R.id.mapload_layout).setVisibility(4);
                    GlobalApp globalApp = (GlobalApp) MapAct.this.getApplication();
                    String scenicCode = "";
                    if (GlobalApp.curAttCode != null && !"".equals(GlobalApp.curAttCode) && GlobalApp.curAttCode.length() > 14) {
                        scenicCode = GlobalApp.curAttCode.substring(0, 14);
                    }
                    Log.i("GlobalApp.curScenicCode:", GlobalApp.curScenicCode);
                    if (scenicCode == null || "".equalsIgnoreCase(scenicCode) || !scenicCode.equalsIgnoreCase(GlobalApp.curScenicCode)) {
                        Log.d(MapAct.this.getClass().getSimpleName(), "broadcast point is not in loading scenic");
                        MapAct.this.playerPanelView.setVisibility(4);
                        MapAct.this.playerPanelView.mSetsailButton.setVisibility(4);
                    } else {
                        Log.d(MapAct.this.getClass().getSimpleName(), "broadcast point is in loading scenic");
                        if (!PlayerPanelView.finished) {
                            Log.d(MapAct.this.getClass().getSimpleName(), "audio is playing");
                            MapAct.this.playerPanelView.setVisibility(0);
                            if (GlobalApp.m_bAnchorState) {
                                MapAct.this.playerPanelView.mAnchorButtonClickListener(MapAct.this.getWindow());
                            } else {
                                MapAct.this.playerPanelView.mSetsailButtonClickListener(MapAct.this.getWindow());
                            }
                            BCButtonAction.animationBcButton(MapAct.this.circleView);
                        }
                    }
                    ParkMapAction.displayCommendWay(GetCommendWay.getSelectedRouteFromMap(0));
                    MapAct.this.playerPanelView.refreshPlayerPanel();
                    return;
                case MapAct.NOMAP_OR_NETERR /*101*/:
                    MapAct.this.v.findViewById(R.id.mapload_prog).setVisibility(4);
                    ((TextView) MapAct.this.v.findViewById(R.id.mapload_action)).setText("此景区地图数据正在制作或网络故障");
                    DrawActivity.currentParkId = "-1";
                    return;
                default:
                    return;
            }
        }
    };
    private boolean mbInitInterrupt = false;
    ParkDataInfo pParkInfo = null;
    /* access modifiers changed from: private */
    public PlayerPanelView playerPanelView = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(getClass().getSimpleName(), "onCreate is called");
        this.mbInitInterrupt = false;
        BCButtonAction.window = getWindow();
        BCButtonAction.gApp = (GlobalApp) getApplication();
        ParkMapAction.initSceneryActivity(getWindow());
        ParkMapAction.moveMenu();
        this.playerPanelView = (PlayerPanelView) getWindow().findViewById(R.id.player_panellayout);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.i(getClass().getSimpleName(), "onResume is called");
        super.onResume();
        GlobalApp gApp = (GlobalApp) getApplication();
        ((TextView) findViewById(R.id.preText)).setText(gApp.curCityName);
        ((TextView) findViewById(R.id.currText)).setText(gApp.curScenicName);
        TextView intrTextView = (TextView) findViewById(R.id.nextText);
        intrTextView.setText("文字介绍");
        RelativeLayout.LayoutParams intrTextViewParams = (RelativeLayout.LayoutParams) intrTextView.getLayoutParams();
        intrTextViewParams.width = 80;
        intrTextView.setLayoutParams(intrTextViewParams);
        ((TextView) findViewById(R.id.nextText)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (GlobalApp.curAttCode == null || !GlobalApp.curAttCode.startsWith(GlobalApp.curScenicCode)) {
                    Intent intent = new Intent(MapAct.this, WallAct.class);
                    intent.addFlags(131072);
                    MapAct.this.startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent(MapAct.this, WallPlayer.class);
                intent2.addFlags(131072);
                MapAct.this.startActivity(intent2);
            }
        });
        ((TextView) findViewById(R.id.preText)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (GuideConfig.curCityCode.equals("00863299")) {
                    MapAct.this.startActivity(new Intent(MapAct.this, CityAct.class));
                    return;
                }
                MapAct.this.startActivity(new Intent(MapAct.this, ScenicAct.class));
            }
        });
        this.circleView = (BroadcastView) getWindow().findViewById(R.id.circle);
        this.playerPanelView.initPlayerPanel(getWindow());
        initParkMap();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        DebugUtil.printTime(getClass().getName() + ".onStart() started");
        ImageButton bestRoute = (ImageButton) findViewById(R.id.bestRoute);
        bestRoute.setBackgroundResource(R.drawable.bestroutselector);
        bestRoute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CommendWayDialog cDialog = new CommendWayDialog(MapAct.this);
                cDialog.setOwnerActivity(MapAct.this);
                cDialog.show();
            }
        });
        ImageButton myLocationButton = (ImageButton) findViewById(R.id.mylocationbutton);
        myLocationButton.setBackgroundResource(R.drawable.wheremeselector);
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (DrawActivity.isInCurrentPark) {
                    ParkMapAction.keepPointCenter();
                } else {
                    Toast.makeText(this, (int) R.string.isnotinpark, 0).show();
                }
            }
        });
        super.onStart();
        DebugUtil.printTime(getClass().getName() + ".onStart() stopped");
    }

    public class MapThread extends Thread {
        public MapThread(String threadName) {
            super(threadName);
        }

        public void run() {
            try {
                ParkMapAction.initParkInfo(GlobalApp.curScenicCode);
                if (ParkMapAction.mParkDataInfo == null) {
                    MapAct.this.mapActHandler.sendEmptyMessage(MapAct.NOMAP_OR_NETERR);
                    boolean unused = MapAct.this.isLoadNewMap = false;
                    return;
                }
                ParkMapAction.initMapData(MapAct.this.mapActHandler);
                boolean unused2 = MapAct.this.isLoadNewMap = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void initParkMap() {
        GlobalApp gApp = (GlobalApp) getApplication();
        Thread mapThread = new MapThread(GlobalApp.curScenicCode);
        Log.d(getClass().getSimpleName(), "gApp.curScenicCode=" + GlobalApp.curScenicCode);
        Log.d(getClass().getSimpleName(), "gApp.curScenicCode=" + GlobalApp.curScenicCode + " preScenicCode=" + currentParkId);
        if (!GlobalApp.curScenicCode.equals(currentParkId)) {
            Log.d(getClass().getSimpleName(), "loading new map");
            this.isLoadNewMap = true;
            if (!"-1".equals(currentParkId)) {
                stopLisLocation();
                BCButtonAction.clearAnimationBcButton();
                ParkMapAction.clearMapData();
                ParkMapAction.stopLoadingMap();
                stopPreMapThread(currentParkId);
            }
            currentParkId = GlobalApp.curScenicCode;
        }
        if (this.isLoadNewMap) {
            this.playerPanelView.setVisibility(4);
            this.v.setEnabled(false);
            if (MovePoint.getInstance().getMovepoint() != null) {
                MovePoint.getInstance().getMovepoint().setVisibility(4);
            }
            this.v.findViewById(R.id.mapload_prog).setVisibility(0);
            this.v.findViewById(R.id.mapload_layout).setVisibility(0);
            ((TextView) this.v.findViewById(R.id.mapload_action)).setText("载入...");
            final String scenicName = gApp.curScenicName;
            setTitle(gApp.curScenicName);
            mapThread.start();
            new Thread() {
                public void run() {
                    LogService.enterParkMap(DrawActivity.currentParkId, DrawActivity.longitude, DrawActivity.latitude, scenicName, DrawActivity.isInCurrentPark);
                }
            }.start();
            BCButtonAction.currentParkId = currentParkId;
            return;
        }
        BCButtonAction.animationBcButton(this.circleView);
        Log.i("***SceneryActivity***", "BCButtonAction.animationBcButton(circleView)");
    }

    public void stopPreMapThread(String preParkId) {
        Thread preThread = new MapThread(preParkId);
        preThread.stop();
        if (preThread != null && Thread.State.RUNNABLE == preThread.getState()) {
            preThread.interrupt();
            do {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (Thread.State.TERMINATED == preThread.getState()) {
                    return;
                }
            } while (preThread != null);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.i(getClass().getSimpleName(), "onPause is called");
        Log.i(getClass().getSimpleName(), "GlobalApp.wallActIntent=" + GlobalApp.wallActIntent);
        if (GlobalApp.wallActIntent != null) {
            sendBroadcast(GlobalApp.wallActIntent);
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("curid", currentParkId);
        outState.putBoolean("isLoadNewMap", this.isLoadNewMap);
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        stopLisLocation();
        ParkMapAction.clearMapData();
    }
}
