package com.yhiker.oneByone.act;

import android.location.Location;
import android.os.Bundle;
import com.yhiker.config.GuideConfig;
import com.yhiker.guid.module.GetScenicpoints;
import com.yhiker.guid.module.GetTalkpoints;
import com.yhiker.guid.module.MovePoint;
import com.yhiker.guid.parse.common.CoordinateTranslate;
import com.yhiker.guid.service.BCButtonAction;
import com.yhiker.guid.service.ParkMapAction;
import com.yhiker.oneByone.bo.LogService;
import com.yhiker.oneByone.module.ScenicPoint;
import com.yhiker.oneByone.module.TalkPoint;
import com.yhiker.playmate.DebugUtil;

public class DrawActivity extends GestureActivity {
    public static String currentParkId = "-1";
    public static boolean isInCurrentPark = false;
    public static String latitude = "-1";
    public static String longitude = "-1";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ParkMapAction.isClearFinish = false;
        DebugUtil.printTime("DrawActivity.Oncreate() end");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        ParkMapAction.stopMediaPlay();
    }

    public void moveGps(Location location, boolean broadcastFlag) {
        if (location != null) {
            Double moveLongitude = Double.valueOf(location.getLongitude());
            Double moveLatitude = Double.valueOf(location.getLatitude());
            longitude = "" + moveLongitude;
            latitude = "" + moveLatitude;
            Double[] dArr = new Double[2];
            Double[] coordinate = CoordinateTranslate.getCoordinateFrGps(moveLongitude, moveLatitude);
            int mapX = coordinate[0].intValue();
            int mapY = coordinate[1].intValue();
            isInCurrentPark = ParkMapAction.isInCurrentPark(mapX, mapY);
            MovePoint movepoint = MovePoint.getInstance();
            movepoint.setGuidMode(isInCurrentPark);
            if (isInCurrentPark) {
                movepoint.setMapX(mapX);
                movepoint.setMapY(mapY);
                movepoint.getMovepoint().setVisibility(0);
                ParkMapAction.pointWalk();
                TalkPoint circle = GetTalkpoints.isInbcCircle(mapX, mapY);
                if (circle != null && broadcastFlag && !isPlayedInScenic(circle)) {
                    broadcastScenerySpot(circle);
                    return;
                }
                return;
            }
            movepoint.getMovepoint().setVisibility(4);
        }
    }

    public void broadcastScenerySpot(TalkPoint circle) {
        final String scenic_code = circle.getScenicid();
        ScenicPoint scenicPoint = GetScenicpoints.getBCbyId(scenic_code);
        final String scenicName = circle.getScenicName();
        new Thread() {
            public void run() {
                GlobalApp gApp = (GlobalApp) DrawActivity.this.getApplication();
                gApp.curCityCodeForPlay = GuideConfig.curCityCode;
                gApp.curScenicCodeForPlay = GlobalApp.curScenicCode;
                LogService.bcScenicSpot(DrawActivity.currentParkId, scenic_code, DrawActivity.longitude, DrawActivity.latitude, scenicName, DrawActivity.isInCurrentPark, "2");
            }
        }.start();
        BCButtonAction.isAutoPlayed = true;
        scenicPoint.getBcButton().performClick();
        scenicPoint.setPlayed(true);
    }

    public boolean isPlayedInScenic(TalkPoint circle) {
        return GetScenicpoints.getBCbyId(circle.getScenicid() + "").isPlayed();
    }
}
