package com.yhiker.oneByone.act.scenic;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.location.GPSReceiver;
import com.yhiker.oneByone.act.BaseAct;
import com.yhiker.oneByone.act.DownloadService;
import com.yhiker.oneByone.act.GlobalApp;
import com.yhiker.oneByone.act.scenicpoint.WallAct;
import com.yhiker.oneByone.bo.CityService;
import com.yhiker.oneByone.bo.ParkService;
import com.yhiker.oneByone.bo.ScenicBo;
import com.yhiker.oneByone.module.Scenic;
import com.yhiker.playmate.R;
import com.yhiker.util.ImageUtil;
import java.util.List;

public class ScenicHistoryAct extends BaseAct {
    GlobalApp gApp;
    ListView listScenic;
    List<Scenic> listScenicDate;
    GPSReceiver m_GPSReceiver = null;
    private LocationManager m_LocationManager = null;
    private GPSHandler m_hGPSHandler = null;
    ScenicAdapter scenicAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        startService(new Intent(this, DownloadService.class));
        setContentView((int) R.layout.scenichistory);
        this.gApp = (GlobalApp) getApplication();
        load();
    }

    public void load() {
        if (this.gApp.netLocation != null) {
            double lastLon = this.gApp.netLocation.getLongitude();
            double lastLat = this.gApp.netLocation.getLatitude();
        }
        this.listScenicDate = ParkService.getParkListHistory();
        this.listScenic = (ListView) findViewById(R.id.listscenic);
        this.scenicAdapter = new ScenicAdapter(this, this.listScenicDate);
        this.listScenic.setAdapter((ListAdapter) this.scenicAdapter);
        this.listScenic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Scenic scenic = ScenicHistoryAct.this.listScenicDate.get(arg2);
                String scenicCode = scenic.getCode();
                Scenic scenic2 = ScenicBo.getScenicByScenicCode(GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + scenic.getCityCode().toLowerCase() + ".db", scenicCode);
                ScenicHistoryAct.this.gApp.curScenicName = scenic2.getName();
                GlobalApp globalApp = ScenicHistoryAct.this.gApp;
                GlobalApp globalApp2 = ScenicHistoryAct.this.gApp;
                GlobalApp.preCurScenicCode = GlobalApp.curScenicCode;
                GlobalApp globalApp3 = ScenicHistoryAct.this.gApp;
                GlobalApp.curScenicCode = scenicCode;
                ScenicHistoryAct.this.gApp.curScenicHasMap = scenic2.getHasMap() >= 1;
                GuideConfig.curCityCode = scenic2.getCityCode();
                ScenicHistoryAct.this.gApp.curCityName = CityService.getCityCodes(GuideConfig.curCityCode).get("cc_name").toString();
                new Thread() {
                    public void run() {
                        GlobalApp globalApp = ScenicHistoryAct.this.gApp;
                        ParkService.addHistory(GlobalApp.curScenicCode, GuideConfig.curCityCode);
                    }
                }.start();
                Intent intent = new Intent(ScenicHistoryAct.this, WallAct.class);
                intent.addFlags(131072);
                ScenicHistoryAct.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    class ScenicAdapter extends ArrayAdapter<List> {
        private static final int mResource = 2130903073;
        private Animation mAnimation;
        protected LayoutInflater mInflater;

        public ScenicAdapter(Context context, List vlaues) {
            super(context, (int) R.layout.scenic_item, vlaues);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            this.mAnimation = AnimationUtils.loadAnimation(context, R.anim.alpha);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            Scenic scenic = (Scenic) getItem(position);
            if (convertView == null) {
                view = this.mInflater.inflate(R.layout.scenic_item, parent, false);
            } else {
                view = convertView;
            }
            try {
                String slc_city_code = scenic.getCityCode();
                String sl_code = scenic.getCode();
                Scenic scenic2 = ScenicBo.getScenicByScenicCode(GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + slc_city_code.toLowerCase() + ".db", sl_code);
                String sl_intr = scenic2.getIntro();
                String level = "" + scenic2.getLevel();
                String sl_name = scenic2.getName();
                String sl_addr = scenic2.getAddress();
                if (sl_name != null) {
                    ((TextView) view.findViewById(R.id.scenicName)).setText(sl_name);
                }
                if (sl_intr != null) {
                    ((TextView) view.findViewById(R.id.scenicIntr)).setText(sl_intr);
                }
                if (sl_addr != null) {
                    ((TextView) view.findViewById(R.id.scenicAddr)).setText(sl_addr);
                }
                ImageView iv = (ImageView) view.findViewById(R.id.scenicImg);
                ((ImageView) view.findViewById(R.id.city_parks_level)).setImageDrawable(ScenicHistoryAct.this.getResources().getDrawable(ScenicHistoryAct.this.getResources().getIdentifier("level" + level, "drawable", ScenicHistoryAct.this.getPackageName())));
                iv.setImageBitmap(ImageUtil.loadBitmapByScenicCode(getContext(), slc_city_code, sl_code));
                ((TextView) view.findViewById(R.id.nearest_scenic)).setVisibility(4);
                if (ScenicHistoryAct.this.gApp.curCityCodeForLocation != null && ScenicHistoryAct.this.gApp.curCityCodeForLocation.equals(slc_city_code) && position == 0) {
                    ((TextView) view.findViewById(R.id.nearest_scenic)).setVisibility(0);
                }
                if (ScenicHistoryAct.this.gApp.curScenicCodeForPlay != null) {
                    if (sl_code.equals(ScenicHistoryAct.this.gApp.curScenicCodeForPlay)) {
                        iv.setAnimation(this.mAnimation);
                    } else {
                        iv.clearAnimation();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return view;
        }
    }

    private class GPSHandler extends Handler {
        public GPSHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            if (msg != null) {
                switch (msg.what) {
                    case 9000:
                        int i = msg.arg1;
                        break;
                }
                super.handleMessage(msg);
            }
        }
    }
}
