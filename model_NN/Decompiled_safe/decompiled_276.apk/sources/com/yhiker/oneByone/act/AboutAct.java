package com.yhiker.oneByone.act;

import android.app.ProgressDialog;
import android.os.Bundle;
import com.yhiker.playmate.R;

public class AboutAct extends BaseAct {
    private ProgressDialog mProgress = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void closeProgress() {
        try {
            if (this.mProgress != null) {
                this.mProgress.dismiss();
                this.mProgress = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
