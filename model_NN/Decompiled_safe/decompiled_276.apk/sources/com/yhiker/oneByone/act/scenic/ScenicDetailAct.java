package com.yhiker.oneByone.act.scenic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.yhiker.oneByone.act.CityAct;
import com.yhiker.oneByone.act.DownDlg;
import com.yhiker.oneByone.act.GlobalApp;
import com.yhiker.oneByone.act.MapAct;
import com.yhiker.playmate.R;
import com.yhiker.util.ImageUtil;

public class ScenicDetailAct extends Activity {
    /* access modifiers changed from: private */
    public String cityCode;
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.closeButton /*2131361919*/:
                    ScenicDetailAct.this.finish();
                    return;
                case R.id.trybutton /*2131361920*/:
                    GlobalApp globalApp = (GlobalApp) ScenicDetailAct.this.getApplication();
                    GlobalApp.curScenicCode = ScenicDetailAct.this.scenicCode;
                    Intent intent = new Intent(ScenicDetailAct.this, MapAct.class);
                    intent.addFlags(131072);
                    ScenicDetailAct.this.startActivity(intent);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public String scenicCode;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.scenicdetail);
        Bundle bundle = getIntent().getExtras();
        this.cityCode = bundle.getString("cityCode");
        this.scenicCode = bundle.getString("scenicCode");
        String scenicName = bundle.getString("scenicName");
        String scenicInfo = bundle.getString("detailInfo");
        ((TextView) findViewById(R.id.title)).setText(scenicName);
        ((TextView) findViewById(R.id.scenicName)).setText(scenicName + "简介");
        ((TextView) findViewById(R.id.scenicDetailInfo)).setText(scenicInfo);
        ((ImageView) findViewById(R.id.scenicImg)).setImageBitmap(ImageUtil.loadBitmapByScenicCode(this, this.cityCode, this.scenicCode));
        ((TextView) findViewById(R.id.trybutton)).setOnClickListener(this.clickListener);
        ((Button) findViewById(R.id.closeButton)).setOnClickListener(this.clickListener);
        ((TextView) findViewById(R.id.preText)).setText("中国");
        ((TextView) findViewById(R.id.currText)).setText(((GlobalApp) getApplication()).curCityName);
        ((TextView) findViewById(R.id.nextText)).setText("下载");
        ((TextView) findViewById(R.id.preText)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ScenicDetailAct.this.startActivity(new Intent(ScenicDetailAct.this, CityAct.class));
            }
        });
        ((TextView) findViewById(R.id.nextText)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                GlobalApp.curCityCodeDowing = ScenicDetailAct.this.cityCode;
                DownDlg cd = new DownDlg(ScenicDetailAct.this, R.style.dialog);
                cd.requestWindowFeature(1);
                cd.show();
            }
        });
    }
}
