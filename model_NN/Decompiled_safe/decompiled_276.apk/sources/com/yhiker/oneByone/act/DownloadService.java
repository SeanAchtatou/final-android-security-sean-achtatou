package com.yhiker.oneByone.act;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.widget.RemoteViews;
import com.yhiker.oneByone.act.IDownloadService;
import com.yhiker.playmate.R;
import java.lang.ref.WeakReference;

public class DownloadService extends Service {
    private final int NOTIFICATION_ID = 1;
    int connTimeout;
    private RemoteViews contentView;
    String destDir;
    String downBroadCast;
    private Thread downThread = null;
    String downTitle;
    String fileName;
    /* access modifiers changed from: private */
    public long fileSize = 0;
    /* access modifiers changed from: private */
    public boolean isDownloading = false;
    private final IBinder mBinder = new ServiceStub(this);
    private Notification n;
    private NotificationManager nManager;
    String notiTitle;
    String passwd;
    /* access modifiers changed from: private */
    public long readLen = 0;
    long timeStarted;
    String url;

    static /* synthetic */ long access$114(DownloadService x0, long x1) {
        long j = x0.readLen + x1;
        x0.readLen = j;
        return j;
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        this.nManager = (NotificationManager) getSystemService("notification");
        this.n = new Notification(R.drawable.icon, this.notiTitle, System.currentTimeMillis());
    }

    public void onDestroy() {
        this.nManager.cancel(1);
    }

    /* access modifiers changed from: private */
    public void showNotification(String notiTitle2, String downTitle2, int percent) {
        this.contentView = new RemoteViews(getPackageName(), (int) R.layout.noti);
        this.contentView.setImageViewResource(R.id.image, R.drawable.icon);
        this.contentView.setTextViewText(R.id.tv, downTitle2);
        this.n.contentView = this.contentView;
        this.n.contentIntent = PendingIntent.getActivity(this, 0, new Intent(), 0);
        this.n.contentView.setProgressBar(R.id.pb, 100, percent, false);
        this.nManager.notify(1, this.n);
    }

    public boolean downloadFile(String notiTitle2, String downTitle2, String url2, String fileName2, String destDir2, String downBroadCast2, int connTimeout2, String passwd2) {
        if (this.downThread != null && this.downThread.isAlive()) {
            return false;
        }
        this.notiTitle = notiTitle2;
        this.downTitle = downTitle2;
        this.url = url2;
        Log.i("downurl", url2);
        this.fileName = fileName2;
        this.destDir = destDir2;
        this.downBroadCast = downBroadCast2;
        this.connTimeout = connTimeout2;
        this.passwd = passwd2;
        this.downThread = new Thread(new downRun());
        this.downThread.start();
        this.timeStarted = System.currentTimeMillis();
        showNotification(notiTitle2, downTitle2, 0);
        return true;
    }

    class downRun implements Runnable {
        downRun() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x039b A[SYNTHETIC, Splitter:B:40:0x039b] */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x03a0 A[SYNTHETIC, Splitter:B:43:0x03a0] */
        /* JADX WARNING: Removed duplicated region for block: B:70:0x06d6 A[SYNTHETIC, Splitter:B:70:0x06d6] */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x06db A[SYNTHETIC, Splitter:B:73:0x06db] */
        /* JADX WARNING: Removed duplicated region for block: B:97:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r40 = this;
                java.io.File r33 = new java.io.File
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.destDir
                r34 = r0
                r33.<init>(r34)
                boolean r33 = r33.exists()
                if (r33 != 0) goto L_0x002b
                java.io.File r33 = new java.io.File
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.destDir
                r34 = r0
                r33.<init>(r34)
                r33.mkdirs()
            L_0x002b:
                java.io.File r11 = new java.io.File
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r0 = r33
                java.lang.String r0 = r0.destDir
                r33 = r0
                java.lang.StringBuilder r34 = new java.lang.StringBuilder
                r34.<init>()
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r35 = r0
                r0 = r35
                java.lang.String r0 = r0.fileName
                r35 = r0
                java.lang.StringBuilder r34 = r34.append(r35)
                java.lang.String r35 = ".dow"
                java.lang.StringBuilder r34 = r34.append(r35)
                java.lang.String r34 = r34.toString()
                r0 = r11
                r1 = r33
                r2 = r34
                r0.<init>(r1, r2)
                r4 = 8192(0x2000, float:1.14794E-41)
                r5 = 1
                r19 = 0
                r21 = 0
                r15 = 0
                r10 = 0
                r24 = 0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r34 = 0
                long unused = r33.fileSize = r34
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r34 = 0
                long unused = r33.readLen = r34
                android.content.Intent r12 = new android.content.Intent
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r0 = r33
                java.lang.String r0 = r0.downBroadCast
                r33 = r0
                r0 = r12
                r1 = r33
                r0.<init>(r1)
                java.lang.String r33 = "url"
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.url
                r34 = r0
                r0 = r12
                r1 = r33
                r2 = r34
                r0.putExtra(r1, r2)
                java.lang.String r33 = "notiTitle"
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.notiTitle
                r34 = r0
                r0 = r12
                r1 = r33
                r2 = r34
                r0.putExtra(r1, r2)
                java.lang.String r33 = "downTitle"
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.downTitle
                r34 = r0
                r0 = r12
                r1 = r33
                r2 = r34
                r0.putExtra(r1, r2)
                org.apache.http.params.BasicHttpParams r23 = new org.apache.http.params.BasicHttpParams
                r23.<init>()
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r0 = r33
                int r0 = r0.connTimeout
                r33 = r0
                r0 = r23
                r1 = r33
                org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r1)
                r33 = 600000(0x927c0, float:8.40779E-40)
                r0 = r23
                r1 = r33
                org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r1)
                org.apache.http.impl.client.DefaultHttpClient r18 = new org.apache.http.impl.client.DefaultHttpClient
                r0 = r18
                r1 = r23
                r0.<init>(r1)
                java.lang.Class r33 = r40.getClass()
                java.lang.String r33 = r33.getName()
                java.lang.StringBuilder r34 = new java.lang.StringBuilder
                r34.<init>()
                java.lang.String r35 = "down fileName:"
                java.lang.StringBuilder r34 = r34.append(r35)
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r35 = r0
                r0 = r35
                java.lang.String r0 = r0.fileName
                r35 = r0
                java.lang.StringBuilder r34 = r34.append(r35)
                java.lang.String r35 = ".dow"
                java.lang.StringBuilder r34 = r34.append(r35)
                java.lang.String r34 = r34.toString()
                android.util.Log.i(r33, r34)
                if (r5 == 0) goto L_0x0148
                boolean r33 = r11.exists()     // Catch:{ Exception -> 0x06fe }
                if (r33 == 0) goto L_0x0148
                java.io.FileInputStream r16 = new java.io.FileInputStream     // Catch:{ Exception -> 0x06fe }
                r0 = r16
                r1 = r11
                r0.<init>(r1)     // Catch:{ Exception -> 0x06fe }
                int r10 = r16.available()     // Catch:{ Exception -> 0x0703, all -> 0x06f6 }
                r15 = r16
            L_0x0148:
                org.apache.http.client.methods.HttpGet r26 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                r0 = r33
                java.lang.String r0 = r0.url     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                r0 = r26
                r1 = r33
                r0.<init>(r1)     // Catch:{ Exception -> 0x06fe }
                if (r10 <= 0) goto L_0x0186
                java.lang.String r33 = "RANGE"
                java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r34.<init>()     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = "bytes="
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                r0 = r34
                r1 = r10
                java.lang.StringBuilder r34 = r0.append(r1)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = "-"
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x06fe }
                r0 = r26
                r1 = r33
                r2 = r34
                r0.addHeader(r1, r2)     // Catch:{ Exception -> 0x06fe }
            L_0x0186:
                r0 = r18
                r1 = r26
                org.apache.http.HttpResponse r27 = r0.execute(r1)     // Catch:{ Exception -> 0x06fe }
                org.apache.http.StatusLine r33 = r27.getStatusLine()     // Catch:{ Exception -> 0x06fe }
                int r30 = r33.getStatusCode()     // Catch:{ Exception -> 0x06fe }
                r33 = 200(0xc8, float:2.8E-43)
                r0 = r30
                r1 = r33
                if (r0 == r1) goto L_0x01a6
                r33 = 206(0xce, float:2.89E-43)
                r0 = r30
                r1 = r33
                if (r0 != r1) goto L_0x03e0
            L_0x01a6:
                org.apache.http.HttpEntity r33 = r27.getEntity()     // Catch:{ Exception -> 0x06fe }
                java.io.InputStream r19 = r33.getContent()     // Catch:{ Exception -> 0x06fe }
                org.apache.http.HttpEntity r14 = r27.getEntity()     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                long r34 = r14.getContentLength()     // Catch:{ Exception -> 0x06fe }
                r0 = r10
                long r0 = (long) r0     // Catch:{ Exception -> 0x06fe }
                r36 = r0
                long r34 = r34 + r36
                long unused = r33.fileSize = r34     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                r0 = r10
                long r0 = (long) r0     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                long unused = r33.readLen = r34     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                long r33 = r33.readLen     // Catch:{ Exception -> 0x06fe }
                r35 = 100
                long r33 = r33 * r35
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r35 = r0
                long r35 = r35.fileSize     // Catch:{ Exception -> 0x06fe }
                long r33 = r33 / r35
                r0 = r33
                int r0 = (int) r0     // Catch:{ Exception -> 0x06fe }
                r24 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.notiTitle     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                java.lang.StringBuilder r35 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r35.<init>()     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r36 = r0
                r0 = r36
                java.lang.String r0 = r0.downTitle     // Catch:{ Exception -> 0x06fe }
                r36 = r0
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r36 = ""
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                r0 = r35
                r1 = r24
                java.lang.StringBuilder r35 = r0.append(r1)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r36 = "%"
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = r35.toString()     // Catch:{ Exception -> 0x06fe }
                r0 = r33
                r1 = r34
                r2 = r35
                r3 = r24
                r0.showNotification(r1, r2, r3)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r33 = "Content-Encoding"
                r0 = r27
                r1 = r33
                org.apache.http.Header r8 = r0.getFirstHeader(r1)     // Catch:{ Exception -> 0x06fe }
                if (r8 == 0) goto L_0x025e
                java.lang.String r33 = r8.getValue()     // Catch:{ Exception -> 0x06fe }
                java.lang.String r34 = "gzip"
                boolean r33 = r33.equalsIgnoreCase(r34)     // Catch:{ Exception -> 0x06fe }
                if (r33 == 0) goto L_0x025e
                java.util.zip.GZIPInputStream r20 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x06fe }
                r0 = r20
                r1 = r19
                r0.<init>(r1)     // Catch:{ Exception -> 0x06fe }
                r19 = r20
            L_0x025e:
                java.io.FileOutputStream r22 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x06fe }
                r0 = r22
                r1 = r11
                r2 = r5
                r0.<init>(r1, r2)     // Catch:{ Exception -> 0x06fe }
                byte[] r6 = new byte[r4]     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r25 = 0
            L_0x026b:
                r0 = r19
                r1 = r6
                int r25 = r0.read(r1)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                if (r25 <= 0) goto L_0x03a4
                r33 = 0
                r0 = r22
                r1 = r6
                r2 = r33
                r3 = r25
                r0.write(r1, r2, r3)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r33 = r0
                r0 = r25
                long r0 = (long) r0     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r34 = r0
                com.yhiker.oneByone.act.DownloadService.access$114(r33, r34)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r33 = r0
                long r33 = r33.readLen     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r35 = 100
                long r33 = r33 * r35
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r35 = r0
                long r35 = r35.fileSize     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                long r33 = r33 / r35
                r0 = r33
                int r0 = (int) r0     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r9 = r0
                r0 = r9
                r1 = r24
                if (r0 <= r1) goto L_0x026b
                r24 = r9
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r33 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.notiTitle     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r34 = r0
                java.lang.StringBuilder r35 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r35.<init>()     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r36 = r0
                r0 = r36
                java.lang.String r0 = r0.downTitle     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r36 = r0
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                java.lang.String r36 = ""
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r0 = r35
                r1 = r24
                java.lang.StringBuilder r35 = r0.append(r1)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                java.lang.String r36 = "%"
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                java.lang.String r35 = r35.toString()     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r0 = r33
                r1 = r34
                r2 = r35
                r3 = r24
                r0.showNotification(r1, r2, r3)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                goto L_0x026b
            L_0x02ff:
                r33 = move-exception
                r13 = r33
                r21 = r22
            L_0x0304:
                java.lang.String r33 = "complete"
                r34 = 0
                r0 = r12
                r1 = r33
                r2 = r34
                r0.putExtra(r1, r2)     // Catch:{ all -> 0x068f }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ all -> 0x068f }
                r33 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ all -> 0x068f }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.notiTitle     // Catch:{ all -> 0x068f }
                r34 = r0
                java.lang.StringBuilder r35 = new java.lang.StringBuilder     // Catch:{ all -> 0x068f }
                r35.<init>()     // Catch:{ all -> 0x068f }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ all -> 0x068f }
                r36 = r0
                r0 = r36
                java.lang.String r0 = r0.downTitle     // Catch:{ all -> 0x068f }
                r36 = r0
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ all -> 0x068f }
                java.lang.String r36 = "下载失败,请重新下载."
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ all -> 0x068f }
                java.lang.String r35 = r35.toString()     // Catch:{ all -> 0x068f }
                r0 = r33
                r1 = r34
                r2 = r35
                r3 = r24
                r0.showNotification(r1, r2, r3)     // Catch:{ all -> 0x068f }
                java.lang.String r33 = "downloadfile"
                java.lang.String r34 = r13.toString()     // Catch:{ all -> 0x068f }
                android.util.Log.e(r33, r34)     // Catch:{ all -> 0x068f }
                org.apache.http.conn.ClientConnectionManager r33 = r18.getConnectionManager()
                r33.shutdown()
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r34 = 0
                r0 = r34
                r2 = r33
                r2.timeStarted = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r0 = r33
                r1 = r12
                r0.sendBroadcast(r1)
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r0 = r33
                java.lang.String r0 = r0.fileName
                r33 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                long r34 = r34.fileSize
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r36 = r0
                long r36 = r36.readLen
                com.yhiker.oneByone.bo.CityService.updateRecodeByDown(r33, r34, r36)
                if (r21 == 0) goto L_0x039e
                r21.close()     // Catch:{ IOException -> 0x06e9 }
            L_0x039e:
                if (r19 == 0) goto L_0x03a3
                r19.close()     // Catch:{ IOException -> 0x06ef }
            L_0x03a3:
                return
            L_0x03a4:
                java.io.File r33 = new java.io.File     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r34.<init>()     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r35 = r0
                r0 = r35
                java.lang.String r0 = r0.destDir     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r35 = r0
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                java.lang.String r35 = "/"
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r35 = r0
                r0 = r35
                java.lang.String r0 = r0.fileName     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r35 = r0
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r33.<init>(r34)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r0 = r11
                r1 = r33
                r0.renameTo(r1)     // Catch:{ Exception -> 0x02ff, all -> 0x06fa }
                r21 = r22
            L_0x03e0:
                r33 = 416(0x1a0, float:5.83E-43)
                r0 = r30
                r1 = r33
                if (r0 != r1) goto L_0x0422
                java.io.File r33 = new java.io.File     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r34.<init>()     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r35 = r0
                r0 = r35
                java.lang.String r0 = r0.destDir     // Catch:{ Exception -> 0x06fe }
                r35 = r0
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = "/"
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r35 = r0
                r0 = r35
                java.lang.String r0 = r0.fileName     // Catch:{ Exception -> 0x06fe }
                r35 = r0
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x06fe }
                r33.<init>(r34)     // Catch:{ Exception -> 0x06fe }
                r0 = r11
                r1 = r33
                r0.renameTo(r1)     // Catch:{ Exception -> 0x06fe }
            L_0x0422:
                long r28 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x06fe }
                java.io.File r32 = new java.io.File     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r33 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r33.<init>()     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.destDir     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r34 = "/"
                java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.fileName     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r33 = r33.toString()     // Catch:{ Exception -> 0x06fe }
                r32.<init>(r33)     // Catch:{ Exception -> 0x06fe }
                boolean r33 = r32.exists()     // Catch:{ Exception -> 0x06fe }
                if (r33 == 0) goto L_0x0639
                java.io.File r33 = new java.io.File     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r34.<init>()     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = com.yhiker.config.GuideConfig.getRootDir()     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = "/yhiker/data/0086"
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x06fe }
                r33.<init>(r34)     // Catch:{ Exception -> 0x06fe }
                boolean r33 = r33.exists()     // Catch:{ Exception -> 0x06fe }
                if (r33 != 0) goto L_0x04a1
                java.io.File r33 = new java.io.File     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r34.<init>()     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = com.yhiker.config.GuideConfig.getRootDir()     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = "/yhiker/data/0086"
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x06fe }
                r33.<init>(r34)     // Catch:{ Exception -> 0x06fe }
                r33.mkdirs()     // Catch:{ Exception -> 0x06fe }
            L_0x04a1:
                r24 = 100
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.notiTitle     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                java.lang.StringBuilder r35 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r35.<init>()     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r36 = r0
                r0 = r36
                java.lang.String r0 = r0.downTitle     // Catch:{ Exception -> 0x06fe }
                r36 = r0
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r36 = ""
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                r36 = 100
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r36 = "%"
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = r35.toString()     // Catch:{ Exception -> 0x06fe }
                r36 = 100
                r33.showNotification(r34, r35, r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r17 = r32.getName()     // Catch:{ Exception -> 0x06fe }
                r33 = 0
                java.lang.String r34 = "."
                r0 = r17
                r1 = r34
                int r34 = r0.indexOf(r1)     // Catch:{ Exception -> 0x06fe }
                r0 = r17
                r1 = r33
                r2 = r34
                java.lang.String r17 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.notiTitle     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                java.lang.StringBuilder r35 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r35.<init>()     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r36 = r0
                r0 = r36
                java.lang.String r0 = r0.downTitle     // Catch:{ Exception -> 0x06fe }
                r36 = r0
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r36 = "数据解压中请稍后..."
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = r35.toString()     // Catch:{ Exception -> 0x06fe }
                r0 = r33
                r1 = r34
                r2 = r35
                r3 = r24
                r0.showNotification(r1, r2, r3)     // Catch:{ Exception -> 0x06fe }
                com.yhiker.util.UnZip r31 = new com.yhiker.util.UnZip     // Catch:{ Exception -> 0x06fe }
                r31.<init>()     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r33 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r33.<init>()     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.destDir     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r34 = "/"
                java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.fileName     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                java.lang.StringBuilder r33 = r33.append(r34)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r33 = r33.toString()     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r34.<init>()     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = com.yhiker.config.GuideConfig.getRootDir()     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = "/yhiker/data/0086"
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = "/"
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                r0 = r34
                r1 = r17
                java.lang.StringBuilder r34 = r0.append(r1)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x06fe }
                r0 = r31
                r1 = r33
                r2 = r34
                r0.deCompFile(r1, r2)     // Catch:{ Exception -> 0x06fe }
                r32.delete()     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.notiTitle     // Catch:{ Exception -> 0x06fe }
                r34 = r0
                java.lang.StringBuilder r35 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r35.<init>()     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r36 = r0
                r0 = r36
                java.lang.String r0 = r0.downTitle     // Catch:{ Exception -> 0x06fe }
                r36 = r0
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r36 = "数据解压完毕用时"
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                long r36 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x06fe }
                long r36 = r36 - r28
                r38 = 60000(0xea60, double:2.9644E-319)
                long r36 = r36 / r38
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r36 = "分钟"
                java.lang.StringBuilder r35 = r35.append(r36)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = r35.toString()     // Catch:{ Exception -> 0x06fe }
                r0 = r33
                r1 = r34
                r2 = r35
                r3 = r24
                r0.showNotification(r1, r2, r3)     // Catch:{ Exception -> 0x06fe }
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                r0 = r33
                java.lang.String r0 = r0.fileName     // Catch:{ Exception -> 0x06fe }
                r33 = r0
                java.lang.String r34 = ".zip"
                java.lang.String r35 = ""
                java.lang.String r7 = r33.replace(r34, r35)     // Catch:{ Exception -> 0x06fe }
                java.io.File r33 = new java.io.File     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fe }
                r34.<init>()     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = com.yhiker.config.GuideConfig.getRootDir()     // Catch:{ Exception -> 0x06fe }
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = "/yhiker/data/0086"
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r35 = "/"
                java.lang.StringBuilder r34 = r34.append(r35)     // Catch:{ Exception -> 0x06fe }
                r0 = r34
                r1 = r17
                java.lang.StringBuilder r34 = r0.append(r1)     // Catch:{ Exception -> 0x06fe }
                java.lang.String r34 = r34.toString()     // Catch:{ Exception -> 0x06fe }
                r33.<init>(r34)     // Catch:{ Exception -> 0x06fe }
                long r33 = r33.length()     // Catch:{ Exception -> 0x06fe }
                r0 = r7
                r1 = r33
                com.yhiker.oneByone.bo.CityService.updateDowned(r0, r1)     // Catch:{ Exception -> 0x06fe }
            L_0x0639:
                org.apache.http.conn.ClientConnectionManager r33 = r18.getConnectionManager()
                r33.shutdown()
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r34 = 0
                r0 = r34
                r2 = r33
                r2.timeStarted = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r0 = r33
                r1 = r12
                r0.sendBroadcast(r1)
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r33 = r0
                r0 = r33
                java.lang.String r0 = r0.fileName
                r33 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                long r34 = r34.fileSize
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r36 = r0
                long r36 = r36.readLen
                com.yhiker.oneByone.bo.CityService.updateRecodeByDown(r33, r34, r36)
                if (r21 == 0) goto L_0x0682
                r21.close()     // Catch:{ IOException -> 0x06f1 }
            L_0x0682:
                if (r19 == 0) goto L_0x03a3
                r19.close()     // Catch:{ IOException -> 0x0689 }
                goto L_0x03a3
            L_0x0689:
                r13 = move-exception
            L_0x068a:
                r13.printStackTrace()
                goto L_0x03a3
            L_0x068f:
                r33 = move-exception
            L_0x0690:
                org.apache.http.conn.ClientConnectionManager r34 = r18.getConnectionManager()
                r34.shutdown()
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                r35 = 0
                r0 = r35
                r2 = r34
                r2.timeStarted = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                r0 = r34
                r1 = r12
                r0.sendBroadcast(r1)
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r34 = r0
                r0 = r34
                java.lang.String r0 = r0.fileName
                r34 = r0
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r35 = r0
                long r35 = r35.fileSize
                r0 = r40
                com.yhiker.oneByone.act.DownloadService r0 = com.yhiker.oneByone.act.DownloadService.this
                r37 = r0
                long r37 = r37.readLen
                com.yhiker.oneByone.bo.CityService.updateRecodeByDown(r34, r35, r37)
                if (r21 == 0) goto L_0x06d9
                r21.close()     // Catch:{ IOException -> 0x06df }
            L_0x06d9:
                if (r19 == 0) goto L_0x06de
                r19.close()     // Catch:{ IOException -> 0x06e4 }
            L_0x06de:
                throw r33
            L_0x06df:
                r13 = move-exception
                r13.printStackTrace()
                goto L_0x06d9
            L_0x06e4:
                r13 = move-exception
                r13.printStackTrace()
                goto L_0x06de
            L_0x06e9:
                r13 = move-exception
                r13.printStackTrace()
                goto L_0x039e
            L_0x06ef:
                r13 = move-exception
                goto L_0x068a
            L_0x06f1:
                r13 = move-exception
                r13.printStackTrace()
                goto L_0x0682
            L_0x06f6:
                r33 = move-exception
                r15 = r16
                goto L_0x0690
            L_0x06fa:
                r33 = move-exception
                r21 = r22
                goto L_0x0690
            L_0x06fe:
                r33 = move-exception
                r13 = r33
                goto L_0x0304
            L_0x0703:
                r33 = move-exception
                r13 = r33
                r15 = r16
                goto L_0x0304
            */
            throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.act.DownloadService.downRun.run():void");
        }
    }

    public int downloadedPercent(String url2) {
        return ((int) this.readLen) / ((int) this.fileSize);
    }

    public void removeDownloading(String url2) {
        this.downThread.stop();
        this.timeStarted = 0;
    }

    static class ServiceStub extends IDownloadService.Stub {
        WeakReference<DownloadService> mService;

        ServiceStub(DownloadService service) {
            this.mService = new WeakReference<>(service);
        }

        public boolean downloadFile(String notiTitle, String downTitle, String url, String fileName, String destDir, String downBroadCast, int connTimeout, String passwd) throws RemoteException {
            return this.mService.get().downloadFile(notiTitle, downTitle, url, fileName, destDir, downBroadCast, connTimeout, passwd);
        }

        public int downloadedPercent(String url) throws RemoteException {
            return this.mService.get().downloadedPercent(url);
        }

        public void removeDownloading(String url) throws RemoteException {
            this.mService.get().removeDownloading(url);
        }

        public boolean isDownloading() {
            return this.mService.get().isDownloading;
        }

        public long downStartedTime() {
            return this.mService.get().timeStarted;
        }
    }
}
