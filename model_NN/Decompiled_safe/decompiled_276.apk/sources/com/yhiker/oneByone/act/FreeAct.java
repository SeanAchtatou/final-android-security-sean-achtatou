package com.yhiker.oneByone.act;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.bo.PayService;
import com.yhiker.oneByone.bo.UserService;
import com.yhiker.playmate.R;
import java.util.HashMap;

public class FreeAct extends Activity {
    public TabHost mTabhost;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.freealert);
        SharedPreferences.Editor passwdfile = getSharedPreferences("firest", 0).edit();
        passwdfile.putString("firest", "firest");
        passwdfile.commit();
        ((Button) findViewById(R.id.btnfree)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HashMap finde = UserService.finde();
                String deviceId = ((TelephonyManager) FreeAct.this.getBaseContext().getSystemService("phone")).getDeviceId();
                new Thread() {
                    public void run() {
                        PayService.addPhoneLog(GuideConfig.deviceId, GuideConfig.simId);
                    }
                }.start();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
