package com.yhiker.oneByone.act;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.act.IUpdateService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

public class UpdateService extends Service {
    GlobalApp gApp = null;
    private final IBinder mBinder = new ServiceStub(this);
    Thread updateThread = null;

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        this.gApp = (GlobalApp) getApplication();
        if (this.updateThread == null || !this.updateThread.isAlive()) {
            this.updateThread = new Thread(new updateRun());
            this.updateThread.start();
        }
    }

    public void onDestroy() {
        if (this.updateThread != null && this.updateThread.isAlive()) {
            this.updateThread.stop();
        }
    }

    public void update() {
        if (this.updateThread == null || !this.updateThread.isAlive()) {
            this.updateThread = new Thread(new updateRun());
            this.updateThread.start();
        }
    }

    class updateRun implements Runnable {
        updateRun() {
        }

        public void run() {
            Exception e;
            DefaultHttpClient httpclient = null;
            try {
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
                HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
                DefaultHttpClient httpclient2 = new DefaultHttpClient(basicHttpParams);
                try {
                    HttpGet httpGet = new HttpGet(GuideConfig.APK_VERIFY_UPDATE_URL);
                    HttpResponse responseApkVer = httpclient2.execute(httpGet);
                    Log.i("updaterun", "apkver request=" + UpdateService.this.gApp.apk_update_url);
                    if (responseApkVer.getStatusLine().getStatusCode() == 200) {
                        if (Integer.parseInt(EntityUtils.toString(responseApkVer.getEntity())) > UpdateService.this.gApp.currApkVerCode) {
                            HttpGet httpGet2 = new HttpGet(GuideConfig.APK_VERIFY_UPDATE_URL + "/" + GuideConfig.APK_NAME);
                            HttpResponse responseApk = httpclient2.execute(httpGet2);
                            if (responseApk.getStatusLine().getStatusCode() == 200) {
                                InputStream isApk = responseApk.getEntity().getContent();
                                if (isApk == null) {
                                    throw new RuntimeException("stream is null");
                                }
                                File file = new File(GuideConfig.getRootDir() + GuideConfig.APK_SAVE_PATH + ".dow");
                                FileOutputStream fos = new FileOutputStream(file);
                                byte[] data = new byte[8192];
                                while (true) {
                                    int count = isApk.read(data, 0, 8192);
                                    if (count == -1) {
                                        break;
                                    }
                                    fos.write(data, 0, count);
                                }
                                isApk.close();
                                file.renameTo(new File(GuideConfig.getRootDir() + GuideConfig.APK_SAVE_PATH));
                            } else {
                                httpGet2.abort();
                            }
                        }
                    } else {
                        httpGet.abort();
                    }
                    httpclient2.getConnectionManager().shutdown();
                } catch (Exception e2) {
                    e = e2;
                    httpclient = httpclient2;
                    try {
                        Log.e("downloadfile", e.toString());
                        e.printStackTrace();
                        httpclient.getConnectionManager().shutdown();
                    } catch (Throwable th) {
                        th = th;
                        httpclient.getConnectionManager().shutdown();
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    httpclient = httpclient2;
                    httpclient.getConnectionManager().shutdown();
                    throw th;
                }
            } catch (Exception e3) {
                e = e3;
                Log.e("downloadfile", e.toString());
                e.printStackTrace();
                httpclient.getConnectionManager().shutdown();
            }
        }
    }

    static class ServiceStub extends IUpdateService.Stub {
        WeakReference<UpdateService> mService;

        ServiceStub(UpdateService service) {
            this.mService = new WeakReference<>(service);
        }

        public void update() {
            this.mService.get().update();
        }
    }
}
