package com.yhiker.oneByone.act.scenic;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.oneByone.act.BaseAct;
import com.yhiker.oneByone.act.GlobalApp;
import com.yhiker.oneByone.act.scenicpoint.WallAct;
import com.yhiker.oneByone.bo.BaseService;
import com.yhiker.oneByone.bo.ParkService;
import com.yhiker.oneByone.module.Scenic;
import com.yhiker.playmate.R;
import java.io.File;
import java.util.List;

public class ScenicListAct extends BaseAct {
    private View.OnClickListener buttonYesListener = new View.OnClickListener() {
        public void onClick(View v) {
            ScenicListAct.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public GlobalApp gApp;
    private AdapterView.OnItemClickListener itemListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            Log.i(ScenicListAct.this.getClass().getSimpleName(), "onItemClick is called");
            Scenic scenic = (Scenic) ScenicListAct.this.listScenicDate.get(arg2);
            String curScenicCode = scenic.getCode();
            ScenicListAct.this.gApp.curScenicName = scenic.getName();
            GlobalApp unused = ScenicListAct.this.gApp;
            GlobalApp unused2 = ScenicListAct.this.gApp;
            GlobalApp.preCurScenicCode = GlobalApp.curScenicCode;
            GlobalApp unused3 = ScenicListAct.this.gApp;
            GlobalApp.curScenicCode = curScenicCode;
            ScenicListAct.this.gApp.curScenicHasMap = scenic.getHasMap() >= 1;
            new Thread() {
                public void run() {
                    GlobalApp unused = ScenicListAct.this.gApp;
                    ParkService.addHistory(GlobalApp.curScenicCode, GuideConfig.curCityCode);
                }
            }.start();
            Intent intent = new Intent(ScenicListAct.this, WallAct.class);
            intent.addFlags(131072);
            ScenicListAct.this.startActivity(intent);
        }
    };
    private ListView listScenic;
    /* access modifiers changed from: private */
    public List<Scenic> listScenicDate;
    private ScenicAdapter scenicAdapter;
    private String searchContent;

    public void onCreate(Bundle savedInstanceState) {
        this.searchContent = getIntent().getExtras().getString("searchContent");
        this.gApp = (GlobalApp) getApplication();
        Log.i(ScenicListAct.class.getSimpleName(), "dbPath=" + (GuideConfig.getInitDataDir() + ConfigHp.scenic_list_path));
        this.listScenicDate = BaseService.getAllScenicList(this.searchContent);
        if (this.listScenicDate == null || this.listScenicDate.size() == 0) {
            setTheme(16973835);
            super.onCreate(savedInstanceState);
            setContentView((int) R.layout.sceniclist);
            TextView textview = (TextView) findViewById(R.id.textview);
            textview.setVisibility(0);
            textview.setText("很抱歉，没有找到与搜索条件相匹配的景区或景点，请更换搜索条件再次尝试！");
            Button button = (Button) findViewById(R.id.button);
            button.setVisibility(0);
            button.setOnClickListener(this.buttonYesListener);
            return;
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.sceniclist);
        this.listScenic = (ListView) findViewById(R.id.listscenic);
        this.listScenic.setVisibility(0);
        loadScenicList();
    }

    public void loadScenicList() {
        this.scenicAdapter = new ScenicAdapter(this, R.layout.scenic_item, this.listScenicDate);
        this.listScenic.setAdapter((ListAdapter) this.scenicAdapter);
        this.listScenic.setOnItemClickListener(this.itemListener);
    }

    class ScenicAdapter extends ArrayAdapter<List> {
        private Animation mAnimation;
        protected LayoutInflater mInflater;

        public ScenicAdapter(Context context, int textViewResourceId, List objects) {
            super(context, textViewResourceId, objects);
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            Scenic scenic = (Scenic) getItem(position);
            String cityCode = scenic.getCityCode();
            String scenicCode = scenic.getCode();
            String scenicName = scenic.getName();
            String scenicIntr = scenic.getIntro();
            String scenicLevel = "" + scenic.getLevel();
            String scenicAddr = scenic.getAddress();
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.scenic_item, parent, false);
            }
            if (scenicName != null) {
                ((TextView) convertView.findViewById(R.id.scenicName)).setText(scenicName);
            }
            if (scenicIntr != null) {
                ((TextView) convertView.findViewById(R.id.scenicIntr)).setText(scenicIntr);
            }
            if (scenicAddr != null) {
                ((TextView) convertView.findViewById(R.id.scenicAddr)).setText(scenicAddr);
            }
            ImageView iv = (ImageView) convertView.findViewById(R.id.scenicImg);
            ((ImageView) convertView.findViewById(R.id.city_parks_level)).setImageDrawable(ScenicListAct.this.getResources().getDrawable(ScenicListAct.this.getResources().getIdentifier("level" + scenicLevel, "drawable", ScenicListAct.this.getPackageName())));
            iv.setImageBitmap(BitmapFactory.decodeFile(GuideConfig.getRootDir() + ConfigHp.scenic_list_data_ver_dir + File.separator + cityCode + File.separator + "img" + File.separator + scenicCode + ".jpg"));
            Log.i("scenicact pic", GuideConfig.getRootDir() + ConfigHp.scenic_list_data_ver_dir + File.separator + cityCode + File.separator + "img" + File.separator + scenicCode + ".jpg");
            ((TextView) convertView.findViewById(R.id.nearest_scenic)).setVisibility(4);
            if (ScenicListAct.this.gApp.curCityCodeForLocation != null && ScenicListAct.this.gApp.curCityCodeForLocation.equals(cityCode) && position == 0) {
                ((TextView) convertView.findViewById(R.id.nearest_scenic)).setVisibility(0);
            }
            if (ScenicListAct.this.gApp.curScenicCodeForPlay != null) {
                if (scenicCode.equals(ScenicListAct.this.gApp.curScenicCodeForPlay)) {
                    iv.setAnimation(this.mAnimation);
                } else {
                    iv.clearAnimation();
                }
            }
            return convertView;
        }
    }

    public void onBackPressed() {
        Log.i(getClass().getSimpleName(), "onBackPressed is called!");
        finish();
        super.onBackPressed();
    }
}
