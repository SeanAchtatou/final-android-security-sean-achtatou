package com.yhiker.oneByone.bo;

public class CityBo {
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0077, code lost:
        if (r3 != null) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0079, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x007c, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008e, code lost:
        if (r3 != null) goto L_0x0079;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.yhiker.oneByone.module.City> getCity(java.lang.String r8) {
        /*
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r3 = 0
            r2 = 0
            java.lang.String r5 = "SELECT scenic_list_city.scenic_list_city_seq _id,scenic_list_city.slc_city_code slc_city_code,scenic_list_city.slc_intro slc_intr,city_code.cc_name cc_name,scenic_list_city.slc_downed slc_downed,scenic_list_city.slc_scenic_total slc_scenic_total ,scenic_list_city.slc_scenics_zip slc_scenics_zip FROM scenic_list_city left outer join city_code on scenic_list_city.slc_city_code=city_code.cc_code  order by scenic_list_city.slc_order"
            r6 = 0
            r7 = 16
            android.database.sqlite.SQLiteDatabase r3 = android.database.sqlite.SQLiteDatabase.openDatabase(r8, r6, r7)     // Catch:{ Exception -> 0x006d }
            r6 = 0
            android.database.Cursor r2 = r3.rawQuery(r5, r6)     // Catch:{ Exception -> 0x006d }
            if (r2 == 0) goto L_0x0089
            r2.moveToFirst()     // Catch:{ Exception -> 0x006d }
        L_0x001a:
            boolean r6 = r2.isAfterLast()     // Catch:{ Exception -> 0x006d }
            if (r6 != 0) goto L_0x0089
            com.yhiker.oneByone.module.City r0 = new com.yhiker.oneByone.module.City     // Catch:{ Exception -> 0x006d }
            r0.<init>()     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = "_id"
            int r6 = r2.getColumnIndex(r6)     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = r2.getString(r6)     // Catch:{ Exception -> 0x006d }
            r0.setId(r6)     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = "slc_city_code"
            int r6 = r2.getColumnIndex(r6)     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = r2.getString(r6)     // Catch:{ Exception -> 0x006d }
            r0.setCode(r6)     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = "cc_name"
            int r6 = r2.getColumnIndex(r6)     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = r2.getString(r6)     // Catch:{ Exception -> 0x006d }
            r0.setName(r6)     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = "slc_intr"
            int r6 = r2.getColumnIndex(r6)     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = r2.getString(r6)     // Catch:{ Exception -> 0x006d }
            r0.setIntroduce(r6)     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = "slc_downed"
            int r6 = r2.getColumnIndex(r6)     // Catch:{ Exception -> 0x006d }
            java.lang.String r6 = r2.getString(r6)     // Catch:{ Exception -> 0x006d }
            r0.setDowned(r6)     // Catch:{ Exception -> 0x006d }
            r1.add(r0)     // Catch:{ Exception -> 0x006d }
            r2.moveToNext()     // Catch:{ Exception -> 0x006d }
            goto L_0x001a
        L_0x006d:
            r6 = move-exception
            r4 = r6
            r4.printStackTrace()     // Catch:{ all -> 0x007d }
            if (r2 == 0) goto L_0x0077
            r2.close()
        L_0x0077:
            if (r3 == 0) goto L_0x007c
        L_0x0079:
            r3.close()
        L_0x007c:
            return r1
        L_0x007d:
            r6 = move-exception
            if (r2 == 0) goto L_0x0083
            r2.close()
        L_0x0083:
            if (r3 == 0) goto L_0x0088
            r3.close()
        L_0x0088:
            throw r6
        L_0x0089:
            if (r2 == 0) goto L_0x008e
            r2.close()
        L_0x008e:
            if (r3 == 0) goto L_0x007c
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.CityBo.getCity(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004b, code lost:
        if (r2 != null) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004d, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0050, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0062, code lost:
        if (r2 != null) goto L_0x004d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int getCityId(java.lang.String r7, java.lang.String r8) {
        /*
            r0 = 0
            r2 = 0
            r1 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0041 }
            r5.<init>()     // Catch:{ Exception -> 0x0041 }
            java.lang.String r6 = "SELECT city_code_seq FROM city_code where cc_code='"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0041 }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x0041 }
            java.lang.String r6 = "'"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0041 }
            java.lang.String r4 = r5.toString()     // Catch:{ Exception -> 0x0041 }
            r5 = 0
            r6 = 16
            android.database.sqlite.SQLiteDatabase r2 = android.database.sqlite.SQLiteDatabase.openDatabase(r7, r5, r6)     // Catch:{ Exception -> 0x0041 }
            r5 = 0
            android.database.Cursor r1 = r2.rawQuery(r4, r5)     // Catch:{ Exception -> 0x0041 }
            if (r1 == 0) goto L_0x005d
            r1.moveToFirst()     // Catch:{ Exception -> 0x0041 }
        L_0x002d:
            boolean r5 = r1.isAfterLast()     // Catch:{ Exception -> 0x0041 }
            if (r5 != 0) goto L_0x005d
            java.lang.String r5 = "city_code_seq"
            int r5 = r1.getColumnIndex(r5)     // Catch:{ Exception -> 0x0041 }
            int r0 = r1.getInt(r5)     // Catch:{ Exception -> 0x0041 }
            r1.moveToNext()     // Catch:{ Exception -> 0x0041 }
            goto L_0x002d
        L_0x0041:
            r5 = move-exception
            r3 = r5
            r3.printStackTrace()     // Catch:{ all -> 0x0051 }
            if (r1 == 0) goto L_0x004b
            r1.close()
        L_0x004b:
            if (r2 == 0) goto L_0x0050
        L_0x004d:
            r2.close()
        L_0x0050:
            return r0
        L_0x0051:
            r5 = move-exception
            if (r1 == 0) goto L_0x0057
            r1.close()
        L_0x0057:
            if (r2 == 0) goto L_0x005c
            r2.close()
        L_0x005c:
            throw r5
        L_0x005d:
            if (r1 == 0) goto L_0x0062
            r1.close()
        L_0x0062:
            if (r2 == 0) goto L_0x0050
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.CityBo.getCityId(java.lang.String, java.lang.String):int");
    }
}
