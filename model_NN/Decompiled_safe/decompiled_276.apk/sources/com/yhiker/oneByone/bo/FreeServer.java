package com.yhiker.oneByone.bo;

import android.database.sqlite.SQLiteDatabase;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;

public class FreeServer {
    static String freeTable = "free_list";
    static int freeTotal = 2;
    static String payTable = "paid_list";

    public static void clear(String deviceId) {
        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
            db.execSQL("CREATE TABLE IF NOT EXISTS " + payTable + " (paid_list_seq INTEGER PRIMARY KEY,pl2sc_code VARCHAR(64),pl_key VARCHAR(64),pl_zip_ver INTEGER)");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + freeTable + " (park_id VARCHAR(64),device_id VARCHAR(64))");
            db.execSQL("delete FROM " + freeTable + " where  device_id <> '" + deviceId + "'");
            db.execSQL("delete FROM " + payTable + " where  pl_key='" + deviceId + "'");
            if (db == null) {
                return;
            }
        } catch (Exception e) {
            if (db == null) {
                return;
            }
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
        db.close();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x007d, code lost:
        if (r2 != null) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0063, code lost:
        if (r2 != null) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0065, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0068, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean verifyPay(java.lang.String r7, java.lang.String r8) {
        /*
            r2 = 0
            java.lang.String r7 = com.yhiker.config.GuideConfig.deviceId
            r1 = 0
            r0 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            r4.<init>()     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.String r5 = com.yhiker.config.GuideConfig.getInitDataDir()     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.String r5 = "/yhiker/conf/per_conf.db"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            r5 = 0
            r6 = 16
            android.database.sqlite.SQLiteDatabase r2 = android.database.sqlite.SQLiteDatabase.openDatabase(r4, r5, r6)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            r4.<init>()     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.String r5 = "SELECT pl2sc_code,pl_key FROM "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.String r5 = com.yhiker.oneByone.bo.FreeServer.payTable     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.String r5 = " where pl2sc_code='"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.String r5 = "' and pl_key='"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            r5 = 0
            android.database.Cursor r0 = r2.rawQuery(r4, r5)     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            int r3 = r0.getCount()     // Catch:{ Exception -> 0x0077, all -> 0x006b }
            if (r3 <= 0) goto L_0x0069
            r1 = 1
        L_0x005e:
            if (r0 == 0) goto L_0x0063
            r0.close()
        L_0x0063:
            if (r2 == 0) goto L_0x0068
        L_0x0065:
            r2.close()
        L_0x0068:
            return r1
        L_0x0069:
            r1 = 0
            goto L_0x005e
        L_0x006b:
            r4 = move-exception
            if (r0 == 0) goto L_0x0071
            r0.close()
        L_0x0071:
            if (r2 == 0) goto L_0x0076
            r2.close()
        L_0x0076:
            throw r4
        L_0x0077:
            r4 = move-exception
            if (r0 == 0) goto L_0x007d
            r0.close()
        L_0x007d:
            if (r2 == 0) goto L_0x0068
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.FreeServer.verifyPay(java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x00a1, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x00a4, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00f5, code lost:
        if (r4 != null) goto L_0x00a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x009f, code lost:
        if (r4 != null) goto L_0x00a1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean verify(java.lang.String r9, java.lang.String r10, java.lang.String r11) {
        /*
            r4 = 0
            r3 = 0
            java.lang.String r9 = com.yhiker.config.GuideConfig.deviceId
            r1 = r9
            r2 = r11
            r0 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r6.<init>()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = com.yhiker.config.GuideConfig.getInitDataDir()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = "/yhiker/conf/per_conf.db"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r7 = 0
            r8 = 16
            android.database.sqlite.SQLiteDatabase r4 = android.database.sqlite.SQLiteDatabase.openDatabase(r6, r7, r8)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r6.<init>()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = "SELECT park_id,device_id FROM "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = com.yhiker.oneByone.bo.FreeServer.freeTable     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = " where park_id='"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = r6.append(r11)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = "' and device_id='"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = "'"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r7 = 0
            android.database.Cursor r0 = r4.rawQuery(r6, r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            int r5 = r0.getCount()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            if (r5 > 0) goto L_0x00e1
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r6.<init>()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = "SELECT pl2sc_code,pl_key FROM "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = com.yhiker.oneByone.bo.FreeServer.payTable     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = " where pl2sc_code='"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = "' and pl_key='"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = "'"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r7 = 0
            android.database.Cursor r0 = r4.rawQuery(r6, r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            int r5 = r0.getCount()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            if (r5 <= 0) goto L_0x00a5
            r3 = 1
        L_0x009a:
            if (r0 == 0) goto L_0x009f
            r0.close()
        L_0x009f:
            if (r4 == 0) goto L_0x00a4
        L_0x00a1:
            r4.close()
        L_0x00a4:
            return r3
        L_0x00a5:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r6.<init>()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = "SELECT park_id,device_id FROM "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = com.yhiker.oneByone.bo.FreeServer.freeTable     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = " where device_id='"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r7 = "'"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r7 = 0
            android.database.Cursor r0 = r4.rawQuery(r6, r7)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            int r5 = r0.getCount()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            int r6 = com.yhiker.oneByone.bo.FreeServer.freeTotal     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            if (r5 >= r6) goto L_0x009a
            com.yhiker.oneByone.bo.FreeServer$1 r6 = new com.yhiker.oneByone.bo.FreeServer$1     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r6.<init>(r1, r2)     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r6.start()     // Catch:{ Exception -> 0x00ef, all -> 0x00e3 }
            r3 = 1
            goto L_0x009a
        L_0x00e1:
            r3 = 1
            goto L_0x009a
        L_0x00e3:
            r6 = move-exception
            if (r0 == 0) goto L_0x00e9
            r0.close()
        L_0x00e9:
            if (r4 == 0) goto L_0x00ee
            r4.close()
        L_0x00ee:
            throw r6
        L_0x00ef:
            r6 = move-exception
            if (r0 == 0) goto L_0x00f5
            r0.close()
        L_0x00f5:
            if (r4 == 0) goto L_0x00a4
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.FreeServer.verify(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    public static void addFree(String deviceId, String parkId) {
        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
            db.execSQL("insert into " + freeTable + "(park_id,device_id) values('" + parkId + "','" + deviceId + "')");
            if (db == null) {
                return;
            }
        } catch (Exception e) {
            if (db == null) {
                return;
            }
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
        db.close();
    }

    public static void addPay(String deviceId, String cityId) {
        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
            db.execSQL("insert into " + payTable + "(pl2sc_code,pl_key) values('" + cityId + "','" + deviceId + "')");
            if (db == null) {
                return;
            }
        } catch (Exception e) {
            if (db == null) {
                return;
            }
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
        db.close();
    }
}
