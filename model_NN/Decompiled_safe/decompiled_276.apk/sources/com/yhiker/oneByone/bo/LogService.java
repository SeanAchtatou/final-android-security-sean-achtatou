package com.yhiker.oneByone.bo;

import android.util.Log;
import com.yhiker.config.GuideConfig;
import com.yhiker.util.HttpClient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class LogService {
    public static final String LOGINLOG = "LO";
    private static String jllsh;
    private static int orderJllsh = 1000;
    private static String orderNo;

    public static void addPrimary(String userName, String status) {
        if (orderNo == null) {
            orderNo = PayService.getOutTradeNo("LOG");
        }
        Log.i("LogService--addPrimary orderNo:", orderNo);
    }

    public static void addDetails(String scenicSpotId, String scenicRegionId, String longitude, String Latitude, String materialType, String playType) {
        if (orderNo == null) {
            orderNo = PayService.getOutTradeNo("LOG");
        }
        Log.i("LogService--addDetails orderNo:", orderNo);
    }

    public static void loginLog(String userId, String deviceId, String status) {
        Map valueMap = new HashMap();
        valueMap.put("userId", userId);
        valueMap.put("deviceId", deviceId);
        valueMap.put("channelId", GuideConfig.channelCode);
        valueMap.put("simId", GuideConfig.simId);
        createJllsh(deviceId, LOGINLOG);
        valueMap.put("jllsh", jllsh);
        valueMap.put("status", status);
        try {
            if ("T".equals(((JSONObject) HttpClient.doPostToJson(GuideConfig.URL_PERSISTPRIMARY, "usePrimaryRecord", valueMap).get("response")).getString("is_success"))) {
                Log.i("LogService--loginLog:", "Success");
            } else {
                Log.i("LogService--loginLog:", "Faile");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void enterParkMap(String scenicRegionId, String scenicLongitude, String scenicLatitude, String materialCode, boolean inScenicRegion) {
        Map valueMap = new HashMap();
        valueMap.put("jllsh", jllsh);
        valueMap.put("scenicRegionId", scenicRegionId);
        valueMap.put("scenicSpotId", "-1");
        valueMap.put("scenicLongitude", scenicLongitude);
        valueMap.put("scenicLatitude", scenicLatitude);
        if (inScenicRegion) {
            valueMap.put("inScenicRegion", "Y");
        } else {
            valueMap.put("inScenicRegion", "N");
        }
        valueMap.put("materialType", "2");
        valueMap.put("materialCode", materialCode);
        valueMap.put("status", "3");
        try {
            if ("T".equals(((JSONObject) HttpClient.doPostToJson(GuideConfig.URL_PERSISTDETAILS, "useDetailsRecord", valueMap).get("response")).getString("is_success"))) {
                Log.i("LogService--enterParkMap:", "Success");
            } else {
                Log.i("LogService--enterParkMap:", "Faile");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void bcScenicSpot(String scenicRegionId, String scenicSpotId, String scenicLongitude, String scenicLatitude, String materialCode, boolean inScenicRegion, String playType) {
        Map valueMap = new HashMap();
        valueMap.put("jllsh", jllsh);
        valueMap.put("scenicRegionId", scenicRegionId);
        valueMap.put("scenicSpotId", scenicSpotId);
        valueMap.put("scenicLongitude", scenicLongitude);
        valueMap.put("scenicLatitude", scenicLatitude);
        if (inScenicRegion) {
            valueMap.put("inScenicRegion", "Y");
        } else {
            valueMap.put("inScenicRegion", "N");
        }
        valueMap.put("materialType", "1");
        valueMap.put("materialCode", materialCode);
        valueMap.put("status", "1");
        valueMap.put("playType", playType);
        try {
            if ("T".equals(((JSONObject) HttpClient.doPostToJson(GuideConfig.URL_PERSISTDETAILS, "useDetailsRecord", valueMap).get("response")).getString("is_success"))) {
                Log.i("LogService--bcScenicSpot:", "Success:");
            } else {
                Log.i("LogService--bcScenicSpot:", "Faile");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFormatTime() {
        return new SimpleDateFormat("MMddHHmmsss").format(new Date());
    }

    public static void createJllsh(String deviceId, String flag) {
        jllsh = flag + deviceId + getFormatTime() + orderJllsh;
        Log.i("LogService--createJllsh:", jllsh);
    }
}
