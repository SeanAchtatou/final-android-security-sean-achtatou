package com.yhiker.oneByone.bo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.yhiker.boot.util.ClientConstants;
import com.yhiker.oneByone.module.Scenic;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseService {
    private static Cursor cursor = null;
    private static SQLiteDatabase db = null;

    public static List<Map<String, String>> getCityScenicPointList(String searchEditValue, String scenicCode) {
        Log.d(BaseService.class.getSimpleName(), "scenicCode=" + scenicCode);
        List<Map<String, String>> scenicPointMapList = new ArrayList<>();
        for (Scenic scenicPoint : getDataList(ClientConstants.SCENICLISTDBPATH, "SELECT scenic_list.scenic_list_seq _id, scenic_list.sl2slc_city_code cityCode, scenic_list.sl_code scenicCode,scenic_list.sl_name scenicName,scenic_list.sl_intro scenicIntr,scenic_list.sl_level scenicLevel,scenic_list.sl_addr scenicAddr FROM scenic_list WHERE (scenic_list.sl_name like '%" + searchEditValue + "%' or scenic_list.sl_intro like '%" + searchEditValue + "%') and (scenic_list.sl_code like '" + scenicCode + "%' and scenic_list.sl_code != '" + scenicCode + "')")) {
            Map<String, String> scenicPointMap = new HashMap<>();
            scenicPointMap.put("al_code", scenicPoint.getCode());
            scenicPointMap.put("al_name", scenicPoint.getName());
            scenicPointMapList.add(scenicPointMap);
        }
        return scenicPointMapList;
    }

    public static List<Scenic> getCityScenicList(String searchEditValue, String cityCode) {
        Log.d(BaseService.class.getSimpleName(), "cityCode=" + cityCode);
        return changeScenicSeq(cityCode, filterScenicPoint(getDataList(ClientConstants.SCENICLISTDBPATH, "SELECT scenic_list.scenic_list_seq _id, scenic_list.sl2slc_city_code cityCode, scenic_list.sl_code scenicCode,scenic_list.sl_name scenicName,scenic_list.sl_intro scenicIntr,scenic_list.sl_level scenicLevel,scenic_list.sl_addr scenicAddr FROM scenic_list WHERE (scenic_list.sl_name like '%" + searchEditValue + "%' or scenic_list.sl_intro like '%" + searchEditValue + "%') and scenic_list.sl2slc_city_code = '" + cityCode + "'")));
    }

    private static List<Scenic> filterScenicPoint(List<Scenic> scenicAndScenicPointList) {
        List<Scenic> scenicList = new ArrayList<>();
        for (Scenic scenic : scenicAndScenicPointList) {
            String scenicCode = scenic.getCode();
            if (scenicCode.length() > 14) {
                List<Scenic> tempScenicList = getDataList(ClientConstants.SCENICLISTDBPATH, "SELECT scenic_list.scenic_list_seq _id, scenic_list.sl2slc_city_code cityCode, scenic_list.sl_code scenicCode,scenic_list.sl_name scenicName,scenic_list.sl_intro scenicIntr,scenic_list.sl_level scenicLevel,scenic_list.sl_addr scenicAddr FROM scenic_list WHERE scenic_list.sl_code = '" + scenicCode.substring(0, scenicCode.length() - 3) + "'");
                if (tempScenicList != null && tempScenicList.size() > 0) {
                    scenicList.add(tempScenicList.get(0));
                }
            } else {
                scenicList.add(scenic);
            }
        }
        return scenicList;
    }

    private static List<Scenic> changeScenicSeq(String cityCode, List<Scenic> scenicList) {
        String dbPath = ClientConstants.CITYSCENICLISTDBPATH + cityCode + ".db";
        List<Scenic> scenicResultList = new ArrayList<>();
        for (Scenic scenic : scenicList) {
            List<Scenic> tempScenicResultList = getDataList(dbPath, "SELECT scenic_list.scenic_list_seq _id, scenic_list.sl2slc_city_code cityCode, scenic_list.sl_code scenicCode,scenic_list.sl_name scenicName,scenic_list.sl_intro scenicIntr,scenic_list.sl_level scenicLevel,scenic_list.sl_addr scenicAddr FROM scenic_list WHERE scenic_list.sl_code = '" + scenic.getCode() + "'");
            if (tempScenicResultList != null && tempScenicResultList.size() > 0) {
                scenicResultList.add(tempScenicResultList.get(0));
            }
        }
        return scenicResultList;
    }

    private static List<Scenic> changeScenicSeq(List<Scenic> scenicList) {
        List<Scenic> scenicResultList = new ArrayList<>();
        for (Scenic scenic : scenicList) {
            List<Scenic> tempScenicResultList = getDataList(ClientConstants.CITYSCENICLISTDBPATH + scenic.getCityCode() + ".db", "SELECT scenic_list.scenic_list_seq _id, scenic_list.sl2slc_city_code cityCode, scenic_list.sl_code scenicCode,scenic_list.sl_name scenicName,scenic_list.sl_intro scenicIntr,scenic_list.sl_level scenicLevel,scenic_list.sl_addr scenicAddr FROM scenic_list WHERE scenic_list.sl_code = '" + scenic.getCode() + "'");
            if (tempScenicResultList != null && tempScenicResultList.size() > 0) {
                scenicResultList.add(tempScenicResultList.get(0));
            }
        }
        return scenicResultList;
    }

    public static List<Scenic> getAllScenicList(String searchEditValue) {
        return changeScenicSeq(filterScenicPoint(getDataList(ClientConstants.SCENICLISTDBPATH, "SELECT scenic_list.scenic_list_seq _id, scenic_list.sl2slc_city_code cityCode, scenic_list.sl_code scenicCode,scenic_list.sl_name scenicName,scenic_list.sl_intro scenicIntr,scenic_list.sl_level scenicLevel,scenic_list.sl_addr scenicAddr FROM scenic_list WHERE scenic_list.sl_name like '%" + searchEditValue + "%' or scenic_list.sl_intro like '%" + searchEditValue + "%' ")));
    }

    private static List<Scenic> getDataList(String dbPath, String querySql) {
        SQLiteDatabase sQLiteDatabase;
        List<Scenic> scenicList = new ArrayList<>();
        Log.i(BaseService.class.getSimpleName(), "dbPath=" + dbPath);
        Log.d(BaseService.class.getSimpleName(), "querySql=" + querySql);
        try {
            db = SQLiteDatabase.openDatabase(dbPath, null, 16);
            cursor = db.rawQuery(querySql, null);
            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Scenic scenic = new Scenic();
                    scenic.setId(cursor.getString(cursor.getColumnIndex("_id")));
                    scenic.setCityCode(cursor.getString(cursor.getColumnIndex("cityCode")));
                    scenic.setCode(cursor.getString(cursor.getColumnIndex("scenicCode")));
                    scenic.setName(cursor.getString(cursor.getColumnIndex("scenicName")));
                    scenic.setIntro(cursor.getString(cursor.getColumnIndex("scenicIntr")));
                    scenic.setLevel(cursor.getInt(cursor.getColumnIndex("scenicLevel")));
                    scenic.setAddress(cursor.getString(cursor.getColumnIndex("scenicAddr")));
                    scenic.setHasMap(1);
                    scenicList.add(scenic);
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                sQLiteDatabase = db;
                sQLiteDatabase.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                sQLiteDatabase = db;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        Log.d(BaseService.class.getSimpleName(), "scenicList.size()=" + scenicList.size());
        return scenicList;
    }
}
