package com.yhiker.oneByone.bo;

public class MapVectorParaBo {
    /* JADX INFO: Multiple debug info for r18v29 java.lang.Double: [D('a1Str' java.lang.String), D('lgtx' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r18v30 java.lang.Double: [D('lgty' java.lang.Double), D('lgtx' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r18v31 java.lang.Double: [D('lgty' java.lang.Double), D('lgtz' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r18v32 java.lang.Double: [D('lgtz' java.lang.Double), D('latx' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r18v33 java.lang.Double: [D('latx' java.lang.Double), D('laty' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r18v34 java.lang.Double: [D('laty' java.lang.Double), D('latz' java.lang.Double)] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x019f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.yhiker.guid.module.ParkDataInfo getParkDataInfoByScenicCode(java.lang.String r18, java.lang.String r19) {
        /*
            com.yhiker.guid.module.ParkDataInfo r9 = new com.yhiker.guid.module.ParkDataInfo
            r9.<init>()
            r5 = 0
            r3 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            r4.<init>()     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            java.lang.String r6 = "SELECT sm_pic_w,sm_pic_h,sm_sgm_wn,sm_sgm_hn,sc_vp_a1,sc_vp_a2,sc_vp_a3,sc_vp_b1,sc_vp_b2,sc_vp_b3 FROM scenic_map where sc_code='"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            r0 = r4
            r1 = r19
            java.lang.StringBuilder r4 = r0.append(r1)     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            java.lang.String r6 = "' "
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            java.lang.String r6 = "MapVectorParaBo"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            r7.<init>()     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            java.lang.String r8 = "querySql:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            android.util.Log.i(r6, r7)     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            r6 = 0
            r7 = 16
            r0 = r18
            r1 = r6
            r2 = r7
            android.database.sqlite.SQLiteDatabase r10 = android.database.sqlite.SQLiteDatabase.openDatabase(r0, r1, r2)     // Catch:{ Exception -> 0x01bd, all -> 0x018f }
            r18 = 0
            r0 = r10
            r1 = r4
            r2 = r18
            android.database.Cursor r5 = r0.rawQuery(r1, r2)     // Catch:{ Exception -> 0x01c4, all -> 0x01a3 }
            if (r5 == 0) goto L_0x0180
            r5.moveToFirst()     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
        L_0x0055:
            boolean r18 = r5.isAfterLast()     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            if (r18 != 0) goto L_0x0180
            java.lang.String r18 = "sm_pic_w"
            r0 = r5
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r5
            r1 = r18
            java.lang.String r12 = r0.getString(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r18 = "sm_pic_h"
            r0 = r5
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r5
            r1 = r18
            java.lang.String r11 = r0.getString(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r18 = "sm_sgm_wn"
            r0 = r5
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r5
            r1 = r18
            java.lang.String r14 = r0.getString(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r18 = "sm_sgm_hn"
            r0 = r5
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r5
            r1 = r18
            java.lang.String r13 = r0.getString(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r18 = "sc_vp_a1"
            r0 = r5
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r5
            r1 = r18
            java.lang.String r18 = r0.getString(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r3 = "sc_vp_a2"
            int r3 = r5.getColumnIndex(r3)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r3 = r5.getString(r3)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r4 = "sc_vp_a3"
            int r4 = r5.getColumnIndex(r4)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r4 = r5.getString(r4)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r6 = 0
            r7 = 10
            java.lang.String r4 = r4.substring(r6, r7)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r6 = "sc_vp_b1"
            int r6 = r5.getColumnIndex(r6)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r6 = r5.getString(r6)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r7 = "sc_vp_b2"
            int r7 = r5.getColumnIndex(r7)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r7 = r5.getString(r7)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r8 = "sc_vp_b3"
            int r8 = r5.getColumnIndex(r8)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.String r8 = r5.getString(r8)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r15 = 0
            r16 = 10
            r0 = r8
            r1 = r15
            r2 = r16
            java.lang.String r8 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r9
            r1 = r19
            r0.setId(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            int r12 = java.lang.Integer.parseInt(r12)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r9.setDrawableWidth(r12)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            int r11 = java.lang.Integer.parseInt(r11)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r9.setDrawableHeight(r11)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            int r11 = java.lang.Integer.parseInt(r14)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r9.setSegmentWidthNum(r11)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            int r11 = java.lang.Integer.parseInt(r13)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r9.setSegmentHeightNum(r11)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            double r11 = java.lang.Double.parseDouble(r18)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.Double r18 = java.lang.Double.valueOf(r11)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r9
            r1 = r18
            r0.setLgtx(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            double r11 = java.lang.Double.parseDouble(r3)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.Double r18 = java.lang.Double.valueOf(r11)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r9
            r1 = r18
            r0.setLgty(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            double r3 = java.lang.Double.parseDouble(r4)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.Double r18 = java.lang.Double.valueOf(r3)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r9
            r1 = r18
            r0.setLgtz(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            double r3 = java.lang.Double.parseDouble(r6)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.Double r18 = java.lang.Double.valueOf(r3)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r9
            r1 = r18
            r0.setLatx(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            double r3 = java.lang.Double.parseDouble(r7)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.Double r18 = java.lang.Double.valueOf(r3)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r9
            r1 = r18
            r0.setLaty(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            double r3 = java.lang.Double.parseDouble(r8)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            java.lang.Double r18 = java.lang.Double.valueOf(r3)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r0 = r9
            r1 = r18
            r0.setLatz(r1)     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            r5.moveToNext()     // Catch:{ Exception -> 0x016a, all -> 0x01ad }
            goto L_0x0055
        L_0x016a:
            r18 = move-exception
            r19 = r18
            r3 = r10
            r18 = r5
        L_0x0170:
            r19.printStackTrace()     // Catch:{ all -> 0x01b5 }
            if (r18 == 0) goto L_0x0178
            r18.close()
        L_0x0178:
            if (r3 == 0) goto L_0x017d
            r3.close()
        L_0x017d:
            r19 = r3
        L_0x017f:
            return r9
        L_0x0180:
            if (r5 == 0) goto L_0x0185
            r5.close()
        L_0x0185:
            if (r10 == 0) goto L_0x018a
            r10.close()
        L_0x018a:
            r18 = r5
            r19 = r10
            goto L_0x017f
        L_0x018f:
            r18 = move-exception
            r19 = r5
            r17 = r3
            r3 = r18
            r18 = r17
        L_0x0198:
            if (r18 == 0) goto L_0x019d
            r18.close()
        L_0x019d:
            if (r19 == 0) goto L_0x01a2
            r19.close()
        L_0x01a2:
            throw r3
        L_0x01a3:
            r18 = move-exception
            r19 = r10
            r17 = r3
            r3 = r18
            r18 = r17
            goto L_0x0198
        L_0x01ad:
            r18 = move-exception
            r3 = r18
            r19 = r10
            r18 = r5
            goto L_0x0198
        L_0x01b5:
            r19 = move-exception
            r17 = r19
            r19 = r3
            r3 = r17
            goto L_0x0198
        L_0x01bd:
            r18 = move-exception
            r19 = r18
            r18 = r3
            r3 = r5
            goto L_0x0170
        L_0x01c4:
            r18 = move-exception
            r19 = r18
            r18 = r3
            r3 = r10
            goto L_0x0170
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.MapVectorParaBo.getParkDataInfoByScenicCode(java.lang.String, java.lang.String):com.yhiker.guid.module.ParkDataInfo");
    }
}
