package com.yhiker.oneByone.bo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.yhiker.guid.service.BCButtonAction;

public class WallPlayerService {
    private Cursor attraction_list;
    private SQLiteDatabase scenic_list_db;

    /* JADX INFO: finally extract failed */
    public String getScenicCodeByAttSeqId(int bcScenicPointCode, String dbPath) {
        Log.i(BCButtonAction.class.getSimpleName(), " bcScenicPointCode = " + bcScenicPointCode);
        Log.i(BCButtonAction.class.getSimpleName(), " dbPath = " + dbPath);
        String scienceCode = "";
        String querySql = "SELECT al2sl_code FROM attraction_list where attraction_list_seq=" + bcScenicPointCode;
        Log.i(BCButtonAction.class.getSimpleName(), " querySql = " + querySql);
        if (dbPath == null || "".equalsIgnoreCase(dbPath)) {
            return "";
        }
        try {
            this.scenic_list_db = SQLiteDatabase.openDatabase(dbPath, null, 16);
            this.attraction_list = this.scenic_list_db.rawQuery(querySql, null);
            if (this.attraction_list != null) {
                this.attraction_list.moveToFirst();
                if (!this.attraction_list.isAfterLast()) {
                    scienceCode = this.attraction_list.getString(this.attraction_list.getColumnIndex("al2sl_code"));
                }
            }
            Log.i(BCButtonAction.class.getSimpleName(), "scienceCode=" + scienceCode);
            if (this.attraction_list != null) {
                this.attraction_list.close();
            }
            if (this.scenic_list_db != null) {
                this.scenic_list_db.close();
            }
        } catch (Exception e) {
            Exception ex = e;
            Log.e(BCButtonAction.class.getSimpleName(), ex.toString());
            ex.printStackTrace();
            if (this.attraction_list != null) {
                this.attraction_list.close();
            }
            if (this.scenic_list_db != null) {
                this.scenic_list_db.close();
            }
        } catch (Throwable th) {
            if (this.attraction_list != null) {
                this.attraction_list.close();
            }
            if (this.scenic_list_db != null) {
                this.scenic_list_db.close();
            }
            throw th;
        }
        return scienceCode;
    }

    /* JADX INFO: finally extract failed */
    public int getBcButtonIdByAttSeqId(int bcScenicPointCode, String dbPath) {
        int bcButtonId = -1;
        if (bcScenicPointCode < 10) {
            String attSeqs = "00" + bcScenicPointCode;
        } else if (bcScenicPointCode > 100 || bcScenicPointCode < 10) {
            String attSeqs2 = "" + bcScenicPointCode;
        } else {
            String attSeqs3 = "0" + bcScenicPointCode;
        }
        String querySql = "SELECT al_code FROM attraction_list where attraction_list_seq=" + bcScenicPointCode;
        Log.i(BCButtonAction.class.getSimpleName(), " querySql = " + querySql);
        if (dbPath == null || "".equalsIgnoreCase(dbPath)) {
            return -1;
        }
        try {
            this.scenic_list_db = SQLiteDatabase.openDatabase(dbPath, null, 16);
            this.attraction_list = this.scenic_list_db.rawQuery(querySql, null);
            if (this.attraction_list != null) {
                this.attraction_list.moveToFirst();
                if (!this.attraction_list.isAfterLast()) {
                    String sciencePointCode = this.attraction_list.getString(this.attraction_list.getColumnIndex("al_code"));
                    String seq = sciencePointCode.substring(sciencePointCode.length() - 3);
                    Log.d(BCButtonAction.class.getSimpleName(), "seq= " + seq);
                    bcButtonId = seq.length() == seq.lastIndexOf("0") + 1 ? 0 : Integer.valueOf(seq.substring(seq.lastIndexOf("0") + 1)).intValue();
                }
            }
            Log.i(BCButtonAction.class.getSimpleName(), "bcButtonId=" + bcButtonId);
            if (this.attraction_list != null) {
                this.attraction_list.close();
            }
            if (this.scenic_list_db != null) {
                this.scenic_list_db.close();
            }
        } catch (Exception e) {
            Exception ex = e;
            Log.e(BCButtonAction.class.getSimpleName(), ex.toString());
            ex.printStackTrace();
            if (this.attraction_list != null) {
                this.attraction_list.close();
            }
            if (this.scenic_list_db != null) {
                this.scenic_list_db.close();
            }
        } catch (Throwable th) {
            if (this.attraction_list != null) {
                this.attraction_list.close();
            }
            if (this.scenic_list_db != null) {
                this.scenic_list_db.close();
            }
            throw th;
        }
        return bcButtonId;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0135  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.util.Map<java.lang.String, java.lang.String>> getScenicPoitListByScenicSeq(int r11, java.lang.String r12, java.lang.Boolean r13) {
        /*
            r10 = this;
            r0 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e }
            r5.<init>()     // Catch:{ Exception -> 0x013e }
            java.lang.String r6 = "SELECT attraction_list_seq _id,al_code,al2sl_code,al_name,al_intro,al_clon,al_clat,al_pic_ext FROM attraction_list where al2scenic_list_seq="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x013e }
            java.lang.StringBuilder r5 = r5.append(r11)     // Catch:{ Exception -> 0x013e }
            java.lang.String r6 = " order by al_order"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x013e }
            java.lang.String r3 = r5.toString()     // Catch:{ Exception -> 0x013e }
            java.lang.Class r5 = r10.getClass()     // Catch:{ Exception -> 0x013e }
            java.lang.String r5 = r5.getSimpleName()     // Catch:{ Exception -> 0x013e }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e }
            r6.<init>()     // Catch:{ Exception -> 0x013e }
            java.lang.String r7 = "querySql:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x013e }
            java.lang.StringBuilder r6 = r6.append(r3)     // Catch:{ Exception -> 0x013e }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x013e }
            android.util.Log.i(r5, r6)     // Catch:{ Exception -> 0x013e }
            r5 = 0
            r6 = 16
            android.database.sqlite.SQLiteDatabase r5 = android.database.sqlite.SQLiteDatabase.openDatabase(r12, r5, r6)     // Catch:{ Exception -> 0x013e }
            r10.scenic_list_db = r5     // Catch:{ Exception -> 0x013e }
            android.database.sqlite.SQLiteDatabase r5 = r10.scenic_list_db     // Catch:{ Exception -> 0x013e }
            r6 = 0
            android.database.Cursor r5 = r5.rawQuery(r3, r6)     // Catch:{ Exception -> 0x013e }
            r10.attraction_list = r5     // Catch:{ Exception -> 0x013e }
            android.database.Cursor r5 = r10.attraction_list     // Catch:{ Exception -> 0x013e }
            if (r5 == 0) goto L_0x0114
            android.database.Cursor r5 = r10.attraction_list     // Catch:{ Exception -> 0x013e }
            r5.moveToFirst()     // Catch:{ Exception -> 0x013e }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Exception -> 0x013e }
            r1.<init>()     // Catch:{ Exception -> 0x013e }
        L_0x0058:
            android.database.Cursor r5 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            boolean r5 = r5.isAfterLast()     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            if (r5 != 0) goto L_0x0113
            java.lang.Class r5 = r10.getClass()     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r5 = r5.getSimpleName()     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            r6.<init>()     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r7 = "al_code:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            android.database.Cursor r7 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            android.database.Cursor r8 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r9 = "al_code"
            int r8 = r8.getColumnIndex(r9)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r7 = r7.getString(r8)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            android.util.Log.d(r5, r6)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            r4.<init>()     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r5 = "al_code"
            android.database.Cursor r6 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            android.database.Cursor r7 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r8 = "al_code"
            int r7 = r7.getColumnIndex(r8)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r6 = r6.getString(r7)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r5 = "al_pic_ext"
            android.database.Cursor r6 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            android.database.Cursor r7 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r8 = "al_pic_ext"
            int r7 = r7.getColumnIndex(r8)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r6 = r6.getString(r7)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r5 = "_id"
            android.database.Cursor r6 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            android.database.Cursor r7 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r8 = "_id"
            int r7 = r7.getColumnIndex(r8)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            int r6 = r6.getInt(r7)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r5 = "al_name"
            android.database.Cursor r6 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            android.database.Cursor r7 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r8 = "al_name"
            int r7 = r7.getColumnIndex(r8)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            java.lang.String r6 = r6.getString(r7)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            r4.put(r5, r6)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            r1.add(r4)     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            android.database.Cursor r5 = r10.attraction_list     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            r5.moveToNext()     // Catch:{ Exception -> 0x00eb, all -> 0x013b }
            goto L_0x0058
        L_0x00eb:
            r5 = move-exception
            r2 = r5
            r0 = r1
        L_0x00ee:
            java.lang.Class r5 = r10.getClass()     // Catch:{ all -> 0x0127 }
            java.lang.String r5 = r5.getName()     // Catch:{ all -> 0x0127 }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x0127 }
            android.util.Log.e(r5, r6)     // Catch:{ all -> 0x0127 }
            r2.printStackTrace()     // Catch:{ all -> 0x0127 }
            android.database.Cursor r5 = r10.attraction_list
            if (r5 == 0) goto L_0x0109
            android.database.Cursor r5 = r10.attraction_list
            r5.close()
        L_0x0109:
            android.database.sqlite.SQLiteDatabase r5 = r10.scenic_list_db
            if (r5 == 0) goto L_0x0112
            android.database.sqlite.SQLiteDatabase r5 = r10.scenic_list_db
            r5.close()
        L_0x0112:
            return r0
        L_0x0113:
            r0 = r1
        L_0x0114:
            android.database.Cursor r5 = r10.attraction_list
            if (r5 == 0) goto L_0x011d
            android.database.Cursor r5 = r10.attraction_list
            r5.close()
        L_0x011d:
            android.database.sqlite.SQLiteDatabase r5 = r10.scenic_list_db
            if (r5 == 0) goto L_0x0112
            android.database.sqlite.SQLiteDatabase r5 = r10.scenic_list_db
            r5.close()
            goto L_0x0112
        L_0x0127:
            r5 = move-exception
        L_0x0128:
            android.database.Cursor r6 = r10.attraction_list
            if (r6 == 0) goto L_0x0131
            android.database.Cursor r6 = r10.attraction_list
            r6.close()
        L_0x0131:
            android.database.sqlite.SQLiteDatabase r6 = r10.scenic_list_db
            if (r6 == 0) goto L_0x013a
            android.database.sqlite.SQLiteDatabase r6 = r10.scenic_list_db
            r6.close()
        L_0x013a:
            throw r5
        L_0x013b:
            r5 = move-exception
            r0 = r1
            goto L_0x0128
        L_0x013e:
            r5 = move-exception
            r2 = r5
            goto L_0x00ee
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.WallPlayerService.getScenicPoitListByScenicSeq(int, java.lang.String, java.lang.Boolean):java.util.List");
    }
}
