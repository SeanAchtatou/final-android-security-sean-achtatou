package com.yhiker.oneByone.bo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import java.util.HashMap;
import java.util.Map;

public class ParkService {
    static String histroyTable = "history_list";

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x00aa, code lost:
        if (r3 != null) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x00ac, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00af, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00e1, code lost:
        if (r3 != null) goto L_0x00ac;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getParkCodes() {
        /*
            r3 = 0
            java.lang.String r2 = ""
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            r4.<init>()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = com.yhiker.config.GuideConfig.getInitDataDir()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = "/yhiker/conf/per_conf.db"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            r5 = 0
            r6 = 16
            android.database.sqlite.SQLiteDatabase r3 = android.database.sqlite.SQLiteDatabase.openDatabase(r4, r5, r6)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            r4.<init>()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = "select park_id from "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = com.yhiker.oneByone.bo.FreeServer.freeTable     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = " where device_id='"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = com.yhiker.config.GuideConfig.deviceId     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            r5 = 0
            android.database.Cursor r0 = r3.rawQuery(r4, r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r4 = "cityact"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            r5.<init>()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r6 = "records:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            int r6 = r0.getCount()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            android.util.Log.i(r4, r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            boolean r4 = r0.moveToFirst()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            if (r4 == 0) goto L_0x00a5
        L_0x0075:
            java.lang.String r4 = ""
            boolean r4 = r4.equals(r2)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            if (r4 != 0) goto L_0x00b0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            r4.<init>()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = ",'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            r5 = 0
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r2 = r4.toString()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
        L_0x009f:
            boolean r4 = r0.moveToNext()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            if (r4 != 0) goto L_0x0075
        L_0x00a5:
            if (r0 == 0) goto L_0x00aa
            r0.close()
        L_0x00aa:
            if (r3 == 0) goto L_0x00af
        L_0x00ac:
            r3.close()
        L_0x00af:
            return r2
        L_0x00b0:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            r4.<init>()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            r5 = 0
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            java.lang.String r2 = r4.toString()     // Catch:{ Exception -> 0x00db, all -> 0x00cf }
            goto L_0x009f
        L_0x00cf:
            r4 = move-exception
            if (r0 == 0) goto L_0x00d5
            r0.close()
        L_0x00d5:
            if (r3 == 0) goto L_0x00da
            r3.close()
        L_0x00da:
            throw r4
        L_0x00db:
            r4 = move-exception
            if (r0 == 0) goto L_0x00e1
            r0.close()
        L_0x00e1:
            if (r3 == 0) goto L_0x00af
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ParkService.getParkCodes():java.lang.String");
    }

    public static void addHistory(String parkId, String cityCode) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
            db.execSQL("CREATE TABLE IF NOT EXISTS history_list (history_seq INTEGER PRIMARY KEY,sl_code VARCHAR(64),sl_city_code VARCHAR(64))");
            db.execSQL("delete from  history_list where sl_code='" + parkId + "'");
            Cursor c2 = db.rawQuery("select max(history_seq) from history_list", null);
            int key = 0;
            if (c2.moveToFirst()) {
                do {
                    key = c2.getInt(0);
                } while (c2.moveToNext());
            }
            db.execSQL("insert into history_list(history_seq,sl_code,sl_city_code) values(" + (key + 1) + ",'" + parkId + "','" + cityCode + "')");
            if (c2 != null) {
                c2.close();
            }
            if (db == null) {
                return;
            }
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db == null) {
                return;
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
        db.close();
    }

    public static Map<String, Object> getCityRow(String cityCode) {
        SQLiteDatabase db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.scenic_list_path, null, 16);
        Cursor c = db.rawQuery("SELECT cc_name FROM  city_code where cc_code='" + cityCode + "'", null);
        HashMap<String, Object> map = new HashMap<>();
        if (c.moveToFirst()) {
            do {
                map.put("cc_name", c.getString(c.getColumnIndex("cc_name")));
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return map;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0089, code lost:
        return r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0094, code lost:
        if (r4 != null) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0084, code lost:
        if (r4 != null) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0086, code lost:
        r4.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.yhiker.oneByone.module.Scenic> getParkListHistory() {
        /*
            r4 = 0
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r0 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008a }
            r6.<init>()     // Catch:{ Exception -> 0x008a }
            java.lang.String r7 = com.yhiker.config.GuideConfig.getInitDataDir()     // Catch:{ Exception -> 0x008a }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x008a }
            java.lang.String r7 = "/yhiker/conf/per_conf.db"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x008a }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x008a }
            r7 = 0
            r8 = 16
            android.database.sqlite.SQLiteDatabase r4 = android.database.sqlite.SQLiteDatabase.openDatabase(r6, r7, r8)     // Catch:{ Exception -> 0x008a }
            java.lang.String r6 = "CREATE TABLE IF NOT EXISTS history_list (history_seq INTEGER PRIMARY KEY,sl_code VARCHAR(64),sl_city_code VARCHAR(64))"
            r4.execSQL(r6)     // Catch:{ Exception -> 0x008a }
            java.lang.String r2 = getParkCodes()     // Catch:{ Exception -> 0x008a }
            java.lang.String r6 = "SELECT sl_code,sl_city_code,hl.history_seq FROM  history_list hl  order by hl.history_seq desc"
            r7 = 0
            android.database.Cursor r0 = r4.rawQuery(r6, r7)     // Catch:{ Exception -> 0x008a }
            java.lang.String r6 = "sl_code"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008a }
            r7.<init>()     // Catch:{ Exception -> 0x008a }
            java.lang.String r8 = "records:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x008a }
            int r8 = r0.getCount()     // Catch:{ Exception -> 0x008a }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x008a }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x008a }
            android.util.Log.i(r6, r7)     // Catch:{ Exception -> 0x008a }
            boolean r6 = r0.moveToFirst()     // Catch:{ Exception -> 0x008a }
            if (r6 == 0) goto L_0x007f
        L_0x0057:
            com.yhiker.oneByone.module.Scenic r3 = new com.yhiker.oneByone.module.Scenic     // Catch:{ Exception -> 0x008a }
            r3.<init>()     // Catch:{ Exception -> 0x008a }
            java.lang.String r6 = "sl_city_code"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x008a }
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x008a }
            r3.setCityCode(r6)     // Catch:{ Exception -> 0x008a }
            java.lang.String r6 = "sl_code"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x008a }
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x008a }
            r3.setCode(r6)     // Catch:{ Exception -> 0x008a }
            r5.add(r3)     // Catch:{ Exception -> 0x008a }
            boolean r6 = r0.moveToNext()     // Catch:{ Exception -> 0x008a }
            if (r6 != 0) goto L_0x0057
        L_0x007f:
            if (r0 == 0) goto L_0x0084
            r0.close()
        L_0x0084:
            if (r4 == 0) goto L_0x0089
        L_0x0086:
            r4.close()
        L_0x0089:
            return r5
        L_0x008a:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()     // Catch:{ all -> 0x0097 }
            if (r0 == 0) goto L_0x0094
            r0.close()
        L_0x0094:
            if (r4 == 0) goto L_0x0089
            goto L_0x0086
        L_0x0097:
            r6 = move-exception
            if (r0 == 0) goto L_0x009d
            r0.close()
        L_0x009d:
            if (r4 == 0) goto L_0x00a2
            r4.close()
        L_0x00a2:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ParkService.getParkListHistory():java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00e1, code lost:
        if (r7 != null) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00e3, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00e6, code lost:
        return r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x014c, code lost:
        if (r7 != null) goto L_0x00e3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<java.util.Map<java.lang.String, java.lang.Object>> getParkListDB(android.content.Context r13, java.lang.String r14) {
        /*
            r12 = 30
            r7 = 0
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r0 = 0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0142 }
            r8.<init>()     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = com.yhiker.config.GuideConfig.getInitDataDir()     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = "/yhiker/conf/scenic_list.db"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0142 }
            r9 = 0
            r10 = 16
            android.database.sqlite.SQLiteDatabase r7 = android.database.sqlite.SQLiteDatabase.openDatabase(r8, r9, r10)     // Catch:{ Exception -> 0x0142 }
            if (r14 == 0) goto L_0x00e7
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0142 }
            r8.<init>()     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = "SELECT sl_code,sl_name,sl_intro,sl_level,sl_addr,sl_pic ,sl2slc_city_code ,scenic_list_seq _id FROM scenic_list  where sl2slc_city_code = '"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r8 = r8.append(r14)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = "'"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = " order by scenic_list_seq"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0142 }
            r9 = 0
            android.database.Cursor r0 = r7.rawQuery(r8, r9)     // Catch:{ Exception -> 0x0142 }
        L_0x004d:
            java.lang.String r8 = "cityact"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0142 }
            r9.<init>()     // Catch:{ Exception -> 0x0142 }
            java.lang.String r10 = "records:"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0142 }
            int r10 = r0.getCount()     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0142 }
            android.util.Log.i(r8, r9)     // Catch:{ Exception -> 0x0142 }
            boolean r8 = r0.moveToFirst()     // Catch:{ Exception -> 0x0142 }
            if (r8 == 0) goto L_0x00dc
        L_0x006f:
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ Exception -> 0x0142 }
            r4.<init>()     // Catch:{ Exception -> 0x0142 }
            java.lang.String r8 = "sl_code"
            r9 = 0
            java.lang.String r9 = r0.getString(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r8 = "title"
            r9 = 1
            java.lang.String r9 = r0.getString(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r8 = "city_code"
            r9 = 6
            java.lang.String r9 = r0.getString(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            r8 = 2
            java.lang.String r2 = r0.getString(r8)     // Catch:{ Exception -> 0x0142 }
            r2.trim()     // Catch:{ Exception -> 0x0142 }
            java.lang.String r8 = "introduce"
            int r9 = r2.length()     // Catch:{ Exception -> 0x0142 }
            if (r9 >= r12) goto L_0x0111
            r9 = r2
        L_0x00a3:
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r8 = "id"
            r9 = 7
            java.lang.String r9 = r0.getString(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            r8 = 3
            java.lang.String r3 = r0.getString(r8)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r8 = "5"
            boolean r8 = r8.equals(r3)     // Catch:{ Exception -> 0x0142 }
            if (r8 == 0) goto L_0x012d
            java.lang.String r8 = "level"
            r9 = 2130837569(0x7f020041, float:1.7280096E38)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
        L_0x00c9:
            java.lang.String r8 = "attr"
            r9 = 4
            java.lang.String r9 = r0.getString(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            r5.add(r4)     // Catch:{ Exception -> 0x0142 }
            boolean r8 = r0.moveToNext()     // Catch:{ Exception -> 0x0142 }
            if (r8 != 0) goto L_0x006f
        L_0x00dc:
            if (r0 == 0) goto L_0x00e1
            r0.close()
        L_0x00e1:
            if (r7 == 0) goto L_0x00e6
        L_0x00e3:
            r7.close()
        L_0x00e6:
            return r5
        L_0x00e7:
            java.lang.String r6 = getParkCodes()     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0142 }
            r8.<init>()     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = "SELECT sl_code,sl_name,sl_intro,sl_level,sl_addr,sl_pic,sl2slc_city_code,scenic_list_seq _id FROM scenic_list  where sl_code in ("
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r8 = r8.append(r6)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = ")"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = " order by scenic_list_seq"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0142 }
            r9 = 0
            android.database.Cursor r0 = r7.rawQuery(r8, r9)     // Catch:{ Exception -> 0x0142 }
            goto L_0x004d
        L_0x0111:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0142 }
            r9.<init>()     // Catch:{ Exception -> 0x0142 }
            r10 = 0
            r11 = 30
            java.lang.String r10 = r2.substring(r10, r11)     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r10 = "... "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0142 }
            goto L_0x00a3
        L_0x012d:
            java.lang.String r8 = "4"
            boolean r8 = r8.equals(r3)     // Catch:{ Exception -> 0x0142 }
            if (r8 == 0) goto L_0x014f
            java.lang.String r8 = "level"
            r9 = 2130837568(0x7f020040, float:1.7280094E38)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            goto L_0x00c9
        L_0x0142:
            r8 = move-exception
            r1 = r8
            r1.printStackTrace()     // Catch:{ all -> 0x0165 }
            if (r0 == 0) goto L_0x014c
            r0.close()
        L_0x014c:
            if (r7 == 0) goto L_0x00e6
            goto L_0x00e3
        L_0x014f:
            java.lang.String r8 = "3"
            boolean r8 = r8.equals(r3)     // Catch:{ Exception -> 0x0142 }
            if (r8 == 0) goto L_0x0171
            java.lang.String r8 = "level"
            r9 = 2130837567(0x7f02003f, float:1.7280092E38)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            goto L_0x00c9
        L_0x0165:
            r8 = move-exception
            if (r0 == 0) goto L_0x016b
            r0.close()
        L_0x016b:
            if (r7 == 0) goto L_0x0170
            r7.close()
        L_0x0170:
            throw r8
        L_0x0171:
            java.lang.String r8 = "2"
            boolean r8 = r8.equals(r3)     // Catch:{ Exception -> 0x0142 }
            if (r8 == 0) goto L_0x0187
            java.lang.String r8 = "level"
            r9 = 2130837566(0x7f02003e, float:1.728009E38)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            goto L_0x00c9
        L_0x0187:
            java.lang.String r8 = "1"
            boolean r8 = r8.equals(r3)     // Catch:{ Exception -> 0x0142 }
            if (r8 == 0) goto L_0x019d
            java.lang.String r8 = "level"
            r9 = 2130837565(0x7f02003d, float:1.7280088E38)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            goto L_0x00c9
        L_0x019d:
            java.lang.String r8 = "level"
            r9 = 2130837564(0x7f02003c, float:1.7280086E38)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0142 }
            r4.put(r8, r9)     // Catch:{ Exception -> 0x0142 }
            goto L_0x00c9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ParkService.getParkListDB(android.content.Context, java.lang.String):java.util.List");
    }
}
