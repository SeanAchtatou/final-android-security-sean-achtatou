package com.yhiker.oneByone.bo;

public class CommendWayBo {
    /* JADX INFO: Multiple debug info for r11v13 com.yhiker.guid.menu.CommendWay: [D('last_code' int), D('clearItem' com.yhiker.guid.menu.CommendWay)] */
    /* JADX INFO: Multiple debug info for r11v22 int: [D('X' int), D('name' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r12v22 int: [D('Y' int), D('pic_x' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r3v7 android.graphics.Point: [D('point' android.graphics.Point), D('last_code' int)] */
    /* JADX INFO: Multiple debug info for r11v23 int: [D('X' int), D('name' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r12v23 int: [D('Y' int), D('pic_x' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r2v13 android.graphics.Point: [D('cur_code' int), D('point' android.graphics.Point)] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0135  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.yhiker.guid.menu.CommendWay> getCommendWaysByScenicCode(java.lang.String r11, java.lang.String r12) {
        /*
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r2 = 0
            r0 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            r3.<init>()     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            java.lang.String r4 = "Select B.sr_name name,A.sr_code sr_code,A.at_in_pic_x pic_x,A.at_in_pic_y pic_y from scenic_route_point A Left join scenic_route B on A.sr_code=B.sr_code where B.sc_code='"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            java.lang.StringBuilder r12 = r3.append(r12)     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            java.lang.String r3 = "'"
            java.lang.StringBuilder r12 = r12.append(r3)     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            java.lang.String r3 = "CommendWayBo"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            r4.<init>()     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            java.lang.String r5 = "querySql:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            java.lang.StringBuilder r4 = r4.append(r12)     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            android.util.Log.i(r3, r4)     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            r3 = 0
            r4 = 16
            android.database.sqlite.SQLiteDatabase r6 = android.database.sqlite.SQLiteDatabase.openDatabase(r11, r3, r4)     // Catch:{ Exception -> 0x0116, all -> 0x0129 }
            r11 = 0
            android.database.Cursor r0 = r6.rawQuery(r12, r11)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2 = 0
            r12 = 0
            r11 = -1
            if (r0 == 0) goto L_0x00ee
            r0.moveToFirst()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r3 = r11
            r5 = r12
            r8 = r2
        L_0x004f:
            boolean r11 = r0.isAfterLast()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            if (r11 != 0) goto L_0x00eb
            java.lang.String r11 = "sr_code"
            int r11 = r0.getColumnIndex(r11)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r7 = r0.getString(r11)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r11 = "name"
            int r11 = r0.getColumnIndex(r11)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r11 = r0.getString(r11)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r12 = "pic_x"
            int r12 = r0.getColumnIndex(r12)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r12 = r0.getString(r12)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r2 = "pic_y"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r4 = r0.getString(r2)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2.<init>()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r9 = 14
            char r9 = r7.charAt(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r9 = ""
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            if (r3 != r2) goto L_0x00ba
            int r11 = java.lang.Integer.parseInt(r12)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            int r12 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            android.graphics.Point r2 = new android.graphics.Point     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2.<init>(r11, r12)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            if (r5 == 0) goto L_0x00ae
            r5.add(r2)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
        L_0x00ae:
            r4 = r5
            r5 = r8
            r10 = r2
            r2 = r3
            r3 = r10
        L_0x00b3:
            r0.moveToNext()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r3 = r2
            r8 = r5
            r5 = r4
            goto L_0x004f
        L_0x00ba:
            if (r8 == 0) goto L_0x00c4
            if (r5 == 0) goto L_0x00c1
            r8.setPointlist(r5)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
        L_0x00c1:
            r1.add(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
        L_0x00c4:
            com.yhiker.guid.menu.CommendWay r8 = new com.yhiker.guid.menu.CommendWay     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r8.<init>()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r5.<init>()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r8.setId(r2)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r8.setSr_code(r7)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r8.setName(r11)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            int r11 = java.lang.Integer.parseInt(r12)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            int r12 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            android.graphics.Point r3 = new android.graphics.Point     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r3.<init>(r11, r12)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r5.add(r3)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r2 = r2
            r4 = r5
            r5 = r8
            goto L_0x00b3
        L_0x00eb:
            r11 = r3
            r12 = r5
            r2 = r8
        L_0x00ee:
            if (r2 == 0) goto L_0x00f8
            if (r12 == 0) goto L_0x00f5
            r2.setPointlist(r12)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
        L_0x00f5:
            r1.add(r2)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
        L_0x00f8:
            com.yhiker.guid.menu.CommendWay r11 = new com.yhiker.guid.menu.CommendWay     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r11.<init>()     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r12 = -1
            r11.setId(r12)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            java.lang.String r12 = "清除路线"
            r11.setName(r12)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            r1.add(r11)     // Catch:{ Exception -> 0x0144, all -> 0x0139 }
            if (r0 == 0) goto L_0x010e
            r0.close()
        L_0x010e:
            if (r6 == 0) goto L_0x0113
            r6.close()
        L_0x0113:
            r11 = r0
            r12 = r6
        L_0x0115:
            return r1
        L_0x0116:
            r11 = move-exception
            r12 = r11
            r11 = r0
            r0 = r2
        L_0x011a:
            r12.printStackTrace()     // Catch:{ all -> 0x013f }
            if (r11 == 0) goto L_0x0122
            r11.close()
        L_0x0122:
            if (r0 == 0) goto L_0x0127
            r0.close()
        L_0x0127:
            r12 = r0
            goto L_0x0115
        L_0x0129:
            r11 = move-exception
            r12 = r2
            r10 = r0
            r0 = r11
            r11 = r10
        L_0x012e:
            if (r11 == 0) goto L_0x0133
            r11.close()
        L_0x0133:
            if (r12 == 0) goto L_0x0138
            r12.close()
        L_0x0138:
            throw r0
        L_0x0139:
            r11 = move-exception
            r12 = r6
            r10 = r0
            r0 = r11
            r11 = r10
            goto L_0x012e
        L_0x013f:
            r12 = move-exception
            r10 = r12
            r12 = r0
            r0 = r10
            goto L_0x012e
        L_0x0144:
            r11 = move-exception
            r12 = r11
            r11 = r0
            r0 = r6
            goto L_0x011a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.CommendWayBo.getCommendWaysByScenicCode(java.lang.String, java.lang.String):java.util.List");
    }
}
