package com.yhiker.oneByone.bo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import java.util.HashMap;

public class UserService {
    public static final String TB_NAME = "per_conf";
    private static HashMap map = new HashMap();

    public static HashMap finde() {
        if (map.size() > 0) {
            return map;
        }
        SQLiteDatabase db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
        Cursor c = db.query(TB_NAME, null, null, null, null, null, null);
        map.put("size", Integer.valueOf(c.getCount()));
        if (c.moveToFirst()) {
            do {
                map.put(c.getString(1), c.getString(2));
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return map;
    }

    public static void logOut() {
        SQLiteDatabase db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
        db.execSQL("delete from  per_conf where pc_key='password';");
        db.close();
        map.clear();
        finde();
    }

    public static void login(String password) {
        SQLiteDatabase db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
        db.execSQL("delete from  per_conf where pc_value='Guest';");
        ContentValues values = new ContentValues();
        values.put("pc_key", "password");
        values.put("pc_value", password);
        db.insert(TB_NAME, "per_conf_seq", values);
        db.close();
        map.clear();
        finde();
    }

    public static void update(String email, String nick, String password, String mobile) {
        SQLiteDatabase db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
        ContentValues values = new ContentValues();
        values.put("pc_value", nick);
        db.update(TB_NAME, values, "pc_key='nick'", null);
        ContentValues values2 = new ContentValues();
        values2.put("pc_value", password);
        db.update(TB_NAME, values2, "pc_key='password'", null);
        ContentValues values3 = new ContentValues();
        values3.put("pc_value", mobile);
        db.update(TB_NAME, values3, "pc_key='mobile'", null);
        db.close();
        map.clear();
        finde();
    }

    public static void updateAttr(String pc_key, String pc_value) {
        SQLiteDatabase db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
        ContentValues values = new ContentValues();
        values.put("pc_value", pc_value);
        if (db.update(TB_NAME, values, "pc_key='" + pc_key + "'", null) <= 0) {
            new ContentValues();
            values.put("pc_key", pc_key);
            values.put("pc_value", pc_value);
            db.insert(TB_NAME, "per_conf_seq", values);
        }
        db.close();
        map.clear();
    }

    public static void insert(String userId, String email, String nick, String password, String mobile) {
        SQLiteDatabase db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
        db.execSQL("delete from  per_conf;");
        ContentValues values = new ContentValues();
        values.put("pc_key", "device_id");
        values.put("pc_value", GuideConfig.deviceId);
        db.insert(TB_NAME, "per_conf_seq", values);
        values.put("pc_key", "smi_id");
        values.put("pc_value", GuideConfig.simId);
        db.insert(TB_NAME, "per_conf_seq", values);
        values.put("pc_key", "user_id");
        values.put("pc_value", userId);
        db.insert(TB_NAME, "per_conf_seq", values);
        values.put("pc_key", "e_mail");
        values.put("pc_value", email);
        db.insert(TB_NAME, "per_conf_seq", values);
        ContentValues values2 = new ContentValues();
        values2.put("pc_key", "nick");
        values2.put("pc_value", nick);
        db.insert(TB_NAME, "per_conf_seq", values2);
        if (password != null && !"".equals(password)) {
            ContentValues values3 = new ContentValues();
            values3.put("pc_key", "password");
            values3.put("pc_value", password);
            db.insert(TB_NAME, "per_conf_seq", values3);
        }
        if (mobile == null || "null".equals(mobile)) {
            mobile = "";
        }
        ContentValues values4 = new ContentValues();
        values4.put("pc_key", "mobile");
        values4.put("pc_value", mobile);
        db.insert(TB_NAME, "per_conf_seq", values4);
        db.close();
        map.clear();
        finde();
    }
}
