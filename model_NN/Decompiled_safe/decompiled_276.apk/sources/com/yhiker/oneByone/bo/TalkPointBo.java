package com.yhiker.oneByone.bo;

public class TalkPointBo {
    /* JADX INFO: Multiple debug info for r9v12 'attraction_list'  android.database.Cursor: [D('dbPath' java.lang.String), D('attraction_list' android.database.Cursor)] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00df  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.yhiker.oneByone.module.TalkPoint> getTalkPointsByScenicCode(java.lang.String r9, java.lang.String r10) {
        /*
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            r1 = 0
            r0 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            r2.<init>()     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            java.lang.String r3 = "SELECT sp_code,sp_name,at_in_pic_x,at_in_pic_y,at_redius FROM scenic_auto_talk_point where sc_code='"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            java.lang.StringBuilder r10 = r2.append(r10)     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            java.lang.String r2 = "' "
            java.lang.StringBuilder r10 = r10.append(r2)     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            java.lang.String r2 = " order by sp_code"
            java.lang.StringBuilder r10 = r10.append(r2)     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            java.lang.String r2 = "TalkPointBo"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            r3.<init>()     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            java.lang.String r4 = "querySql:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            android.util.Log.i(r2, r3)     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            r2 = 0
            r3 = 16
            android.database.sqlite.SQLiteDatabase r4 = android.database.sqlite.SQLiteDatabase.openDatabase(r9, r2, r3)     // Catch:{ Exception -> 0x00c0, all -> 0x00d3 }
            r9 = 0
            android.database.Cursor r9 = r4.rawQuery(r10, r9)     // Catch:{ Exception -> 0x00f2, all -> 0x00e3 }
            r10 = 0
            if (r9 == 0) goto L_0x00b4
            r9.moveToFirst()     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
        L_0x0050:
            boolean r0 = r9.isAfterLast()     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            if (r0 != 0) goto L_0x00b4
            com.yhiker.oneByone.module.TalkPoint r6 = new com.yhiker.oneByone.module.TalkPoint     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            r6.<init>()     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r0 = "sp_code"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r5 = r9.getString(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r0 = "sp_name"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r3 = r9.getString(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r0 = "at_in_pic_x"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r0 = r9.getString(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r1 = "at_in_pic_y"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r1 = r9.getString(r1)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r2 = "at_redius"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            java.lang.String r2 = r9.getString(r2)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            r6.setId(r10)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            r6.setScenicid(r5)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            r6.setScenicName(r3)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            r6.setCenterX(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            int r0 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            r6.setCenterY(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            int r0 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            r6.setRedius(r0)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            r7.add(r6)     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            r9.moveToNext()     // Catch:{ Exception -> 0x00f7, all -> 0x00e9 }
            int r10 = r10 + 1
            goto L_0x0050
        L_0x00b4:
            if (r9 == 0) goto L_0x00b9
            r9.close()
        L_0x00b9:
            if (r4 == 0) goto L_0x00be
            r4.close()
        L_0x00be:
            r10 = r4
        L_0x00bf:
            return r7
        L_0x00c0:
            r9 = move-exception
            r10 = r9
            r9 = r0
            r0 = r1
        L_0x00c4:
            r10.printStackTrace()     // Catch:{ all -> 0x00ed }
            if (r9 == 0) goto L_0x00cc
            r9.close()
        L_0x00cc:
            if (r0 == 0) goto L_0x00d1
            r0.close()
        L_0x00d1:
            r10 = r0
            goto L_0x00bf
        L_0x00d3:
            r9 = move-exception
            r10 = r1
            r8 = r0
            r0 = r9
            r9 = r8
        L_0x00d8:
            if (r9 == 0) goto L_0x00dd
            r9.close()
        L_0x00dd:
            if (r10 == 0) goto L_0x00e2
            r10.close()
        L_0x00e2:
            throw r0
        L_0x00e3:
            r9 = move-exception
            r10 = r4
            r8 = r0
            r0 = r9
            r9 = r8
            goto L_0x00d8
        L_0x00e9:
            r10 = move-exception
            r0 = r10
            r10 = r4
            goto L_0x00d8
        L_0x00ed:
            r10 = move-exception
            r8 = r10
            r10 = r0
            r0 = r8
            goto L_0x00d8
        L_0x00f2:
            r9 = move-exception
            r10 = r9
            r9 = r0
            r0 = r4
            goto L_0x00c4
        L_0x00f7:
            r10 = move-exception
            r0 = r4
            goto L_0x00c4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.TalkPointBo.getTalkPointsByScenicCode(java.lang.String, java.lang.String):java.util.List");
    }
}
