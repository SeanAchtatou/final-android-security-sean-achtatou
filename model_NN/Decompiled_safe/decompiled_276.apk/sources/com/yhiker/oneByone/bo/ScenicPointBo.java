package com.yhiker.oneByone.bo;

public class ScenicPointBo {
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00a1, code lost:
        if (r4 != null) goto L_0x00a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00a3, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00a6, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b8, code lost:
        if (r4 != null) goto L_0x00a3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.yhiker.oneByone.module.ScenicPoint getScenicPointByScenicPointCode(java.lang.String r8, java.lang.String r9) {
        /*
            com.yhiker.oneByone.module.ScenicPoint r3 = new com.yhiker.oneByone.module.ScenicPoint
            r3.<init>()
            r4 = 0
            r0 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097 }
            r5.<init>()     // Catch:{ Exception -> 0x0097 }
            java.lang.String r6 = "SELECT sp_order _id,sc_code,sp_name ,sp_code,sp_about,sp_long_about FROM scenic_point where sp_code='"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0097 }
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r6 = "' "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r2 = r5.toString()     // Catch:{ Exception -> 0x0097 }
            java.lang.String r5 = "ScenicPointBo"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097 }
            r6.<init>()     // Catch:{ Exception -> 0x0097 }
            java.lang.String r7 = "querySql:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0097 }
            java.lang.StringBuilder r6 = r6.append(r2)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0097 }
            android.util.Log.i(r5, r6)     // Catch:{ Exception -> 0x0097 }
            r5 = 0
            r6 = 16
            android.database.sqlite.SQLiteDatabase r4 = android.database.sqlite.SQLiteDatabase.openDatabase(r8, r5, r6)     // Catch:{ Exception -> 0x0097 }
            r5 = 0
            android.database.Cursor r0 = r4.rawQuery(r2, r5)     // Catch:{ Exception -> 0x0097 }
            if (r0 == 0) goto L_0x00b3
            r0.moveToFirst()     // Catch:{ Exception -> 0x0097 }
        L_0x0049:
            boolean r5 = r0.isAfterLast()     // Catch:{ Exception -> 0x0097 }
            if (r5 != 0) goto L_0x00b3
            java.lang.String r5 = "_id"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0097 }
            int r5 = r0.getInt(r5)     // Catch:{ Exception -> 0x0097 }
            r3.setId(r5)     // Catch:{ Exception -> 0x0097 }
            r3.setCode(r9)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r5 = "sp_name"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0097 }
            r3.setName(r5)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r5 = "sc_code"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0097 }
            r3.setScenicCode(r5)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r5 = "sp_about"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0097 }
            r3.setIntro(r5)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r5 = "sp_long_about"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0097 }
            r3.setDintro(r5)     // Catch:{ Exception -> 0x0097 }
            r0.moveToNext()     // Catch:{ Exception -> 0x0097 }
            goto L_0x0049
        L_0x0097:
            r5 = move-exception
            r1 = r5
            r1.printStackTrace()     // Catch:{ all -> 0x00a7 }
            if (r0 == 0) goto L_0x00a1
            r0.close()
        L_0x00a1:
            if (r4 == 0) goto L_0x00a6
        L_0x00a3:
            r4.close()
        L_0x00a6:
            return r3
        L_0x00a7:
            r5 = move-exception
            if (r0 == 0) goto L_0x00ad
            r0.close()
        L_0x00ad:
            if (r4 == 0) goto L_0x00b2
            r4.close()
        L_0x00b2:
            throw r5
        L_0x00b3:
            if (r0 == 0) goto L_0x00b8
            r0.close()
        L_0x00b8:
            if (r4 == 0) goto L_0x00a6
            goto L_0x00a3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ScenicPointBo.getScenicPointByScenicPointCode(java.lang.String, java.lang.String):com.yhiker.oneByone.module.ScenicPoint");
    }

    /* JADX INFO: Multiple debug info for r10v12 'attraction_list'  android.database.Cursor: [D('dbPath' java.lang.String), D('attraction_list' android.database.Cursor)] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.yhiker.oneByone.module.ScenicPoint> getScenicPointsByScenicCode(java.lang.String r10, java.lang.String r11) {
        /*
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            r2 = 0
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            r1.<init>()     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            java.lang.String r3 = "SELECT sp_order,sp_code,sp_name,sc_in_pic_x,sc_in_pic_y,sp_level FROM scenic_point where sc_code='"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            java.lang.StringBuilder r1 = r1.append(r11)     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            java.lang.String r3 = "' "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            java.lang.String r3 = " and sp_type='0' order by sp_order"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            java.lang.String r3 = "ScenicPointBo"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            r4.<init>()     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            java.lang.String r5 = "querySql:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            android.util.Log.i(r3, r4)     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            r3 = 0
            r4 = 16
            android.database.sqlite.SQLiteDatabase r8 = android.database.sqlite.SQLiteDatabase.openDatabase(r10, r3, r4)     // Catch:{ Exception -> 0x0105, all -> 0x00e6 }
            r10 = 0
            android.database.Cursor r10 = r8.rawQuery(r1, r10)     // Catch:{ Exception -> 0x010a, all -> 0x00f6 }
            if (r10 == 0) goto L_0x00da
            r10.moveToFirst()     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
        L_0x004f:
            boolean r0 = r10.isAfterLast()     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            if (r0 != 0) goto L_0x00da
            com.yhiker.oneByone.module.ScenicPoint r6 = new com.yhiker.oneByone.module.ScenicPoint     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            r6.<init>()     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r0 = "sp_order"
            int r0 = r10.getColumnIndex(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r1 = r10.getString(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r0 = "sp_name"
            int r0 = r10.getColumnIndex(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r3 = r10.getString(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r0 = "sp_code"
            int r0 = r10.getColumnIndex(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r0 = r10.getString(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r2 = "sc_in_pic_x"
            int r2 = r10.getColumnIndex(r2)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r4 = r10.getString(r2)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r2 = "sc_in_pic_y"
            int r2 = r10.getColumnIndex(r2)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r5 = r10.getString(r2)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r2 = "sp_level"
            int r2 = r10.getColumnIndex(r2)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r2 = r10.getString(r2)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            r6.setId(r1)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            r6.setName(r3)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            r6.setCode(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            r6.setScenicCode(r11)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            int r0 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            r6.setPositionX(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            int r0 = java.lang.Integer.parseInt(r5)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            int r0 = r0 + 20
            r6.setPositionY(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            int r0 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            r6.setLevel(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            java.lang.String r0 = "0"
            r6.setType(r0)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            r7.add(r6)     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            r10.moveToNext()     // Catch:{ Exception -> 0x00c9, all -> 0x00fc }
            goto L_0x004f
        L_0x00c9:
            r11 = move-exception
            r0 = r8
        L_0x00cb:
            r11.printStackTrace()     // Catch:{ all -> 0x0100 }
            if (r10 == 0) goto L_0x00d3
            r10.close()
        L_0x00d3:
            if (r0 == 0) goto L_0x00d8
            r0.close()
        L_0x00d8:
            r11 = r0
        L_0x00d9:
            return r7
        L_0x00da:
            if (r10 == 0) goto L_0x00df
            r10.close()
        L_0x00df:
            if (r8 == 0) goto L_0x00e4
            r8.close()
        L_0x00e4:
            r11 = r8
            goto L_0x00d9
        L_0x00e6:
            r10 = move-exception
            r11 = r2
            r9 = r0
            r0 = r10
            r10 = r9
        L_0x00eb:
            if (r10 == 0) goto L_0x00f0
            r10.close()
        L_0x00f0:
            if (r11 == 0) goto L_0x00f5
            r11.close()
        L_0x00f5:
            throw r0
        L_0x00f6:
            r10 = move-exception
            r11 = r8
            r9 = r0
            r0 = r10
            r10 = r9
            goto L_0x00eb
        L_0x00fc:
            r11 = move-exception
            r0 = r11
            r11 = r8
            goto L_0x00eb
        L_0x0100:
            r11 = move-exception
            r9 = r11
            r11 = r0
            r0 = r9
            goto L_0x00eb
        L_0x0105:
            r10 = move-exception
            r11 = r10
            r10 = r0
            r0 = r2
            goto L_0x00cb
        L_0x010a:
            r10 = move-exception
            r11 = r10
            r10 = r0
            r0 = r8
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ScenicPointBo.getScenicPointsByScenicCode(java.lang.String, java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00c5, code lost:
        if (r5 != null) goto L_0x00c7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00c7, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00ca, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00dc, code lost:
        if (r5 != null) goto L_0x00c7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<java.util.Map<java.lang.String, java.lang.String>> getScenicMapByScenicCode(java.lang.String r9, java.lang.String r10) {
        /*
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r5 = 0
            r0 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
            r6.<init>()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = "SELECT sp_order,sp_code,sp_name FROM scenic_point where sc_code='"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = "' "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = " and sp_type='0' order by sp_order"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r2 = r6.toString()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r6 = "ScenicPointBo"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
            r7.<init>()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r8 = "querySql:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x00bb }
            android.util.Log.i(r6, r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r6 = com.yhiker.config.GuideConfig.curCityCode     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = "00863299"
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x00bb }
            if (r6 == 0) goto L_0x0067
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00bb }
            r6.<init>()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = "SELECT sp_order,sp_code,sp_name FROM scenic_point where sc_code='"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = "' "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = "  order by sp_order"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r2 = r6.toString()     // Catch:{ Exception -> 0x00bb }
        L_0x0067:
            r6 = 0
            r7 = 16
            android.database.sqlite.SQLiteDatabase r5 = android.database.sqlite.SQLiteDatabase.openDatabase(r9, r6, r7)     // Catch:{ Exception -> 0x00bb }
            r6 = 0
            android.database.Cursor r0 = r5.rawQuery(r2, r6)     // Catch:{ Exception -> 0x00bb }
            if (r0 == 0) goto L_0x00d7
            r0.moveToFirst()     // Catch:{ Exception -> 0x00bb }
        L_0x0078:
            boolean r6 = r0.isAfterLast()     // Catch:{ Exception -> 0x00bb }
            if (r6 != 0) goto L_0x00d7
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ Exception -> 0x00bb }
            r3.<init>()     // Catch:{ Exception -> 0x00bb }
            java.lang.String r6 = "al_code"
            java.lang.String r7 = "sp_code"
            int r7 = r0.getColumnIndex(r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = r0.getString(r7)     // Catch:{ Exception -> 0x00bb }
            r3.put(r6, r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r6 = "_id"
            java.lang.String r7 = "sp_order"
            int r7 = r0.getColumnIndex(r7)     // Catch:{ Exception -> 0x00bb }
            int r7 = r0.getInt(r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x00bb }
            r3.put(r6, r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r6 = "al_name"
            java.lang.String r7 = "sp_name"
            int r7 = r0.getColumnIndex(r7)     // Catch:{ Exception -> 0x00bb }
            java.lang.String r7 = r0.getString(r7)     // Catch:{ Exception -> 0x00bb }
            r3.put(r6, r7)     // Catch:{ Exception -> 0x00bb }
            r4.add(r3)     // Catch:{ Exception -> 0x00bb }
            r0.moveToNext()     // Catch:{ Exception -> 0x00bb }
            goto L_0x0078
        L_0x00bb:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()     // Catch:{ all -> 0x00cb }
            if (r0 == 0) goto L_0x00c5
            r0.close()
        L_0x00c5:
            if (r5 == 0) goto L_0x00ca
        L_0x00c7:
            r5.close()
        L_0x00ca:
            return r4
        L_0x00cb:
            r6 = move-exception
            if (r0 == 0) goto L_0x00d1
            r0.close()
        L_0x00d1:
            if (r5 == 0) goto L_0x00d6
            r5.close()
        L_0x00d6:
            throw r6
        L_0x00d7:
            if (r0 == 0) goto L_0x00dc
            r0.close()
        L_0x00dc:
            if (r5 == 0) goto L_0x00ca
            goto L_0x00c7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ScenicPointBo.getScenicMapByScenicCode(java.lang.String, java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00c2, code lost:
        if (r8 != null) goto L_0x00c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00c4, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00c7, code lost:
        return r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00d9, code lost:
        if (r8 != null) goto L_0x00c4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.yhiker.oneByone.module.SpecialPoint> getSpecialPointsByScenicCode(java.lang.String r14, java.lang.String r15) {
        /*
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            r8 = 0
            r0 = 0
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b8 }
            r11.<init>()     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r12 = "SELECT sp_order,sp_code,sp_name,sc_in_pic_x,sc_in_pic_y FROM scenic_point where sc_code='"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x00b8 }
            java.lang.StringBuilder r11 = r11.append(r15)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r12 = "' "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r12 = " and sp_type='1' order by sp_order"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r7 = r11.toString()     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r11 = "ScenicPointBo"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b8 }
            r12.<init>()     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r13 = "querySql:"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00b8 }
            java.lang.StringBuilder r12 = r12.append(r7)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x00b8 }
            android.util.Log.i(r11, r12)     // Catch:{ Exception -> 0x00b8 }
            r11 = 0
            r12 = 16
            android.database.sqlite.SQLiteDatabase r8 = android.database.sqlite.SQLiteDatabase.openDatabase(r14, r11, r12)     // Catch:{ Exception -> 0x00b8 }
            r11 = 0
            android.database.Cursor r0 = r8.rawQuery(r7, r11)     // Catch:{ Exception -> 0x00b8 }
            if (r0 == 0) goto L_0x00d4
            r0.moveToFirst()     // Catch:{ Exception -> 0x00b8 }
        L_0x004f:
            boolean r11 = r0.isAfterLast()     // Catch:{ Exception -> 0x00b8 }
            if (r11 != 0) goto L_0x00d4
            com.yhiker.oneByone.module.SpecialPoint r9 = new com.yhiker.oneByone.module.SpecialPoint     // Catch:{ Exception -> 0x00b8 }
            r9.<init>()     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r11 = "sp_order"
            int r11 = r0.getColumnIndex(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r3 = r0.getString(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r11 = "sp_name"
            int r11 = r0.getColumnIndex(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r4 = r0.getString(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r11 = "sp_code"
            int r11 = r0.getColumnIndex(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r1 = r0.getString(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r11 = "sc_in_pic_x"
            int r11 = r0.getColumnIndex(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r5 = r0.getString(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r11 = "sc_in_pic_y"
            int r11 = r0.getColumnIndex(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r6 = r0.getString(r11)     // Catch:{ Exception -> 0x00b8 }
            int r11 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x00b8 }
            r9.setId(r11)     // Catch:{ Exception -> 0x00b8 }
            r9.setName(r4)     // Catch:{ Exception -> 0x00b8 }
            r9.setCode(r1)     // Catch:{ Exception -> 0x00b8 }
            r9.setScenicCode(r15)     // Catch:{ Exception -> 0x00b8 }
            int r11 = java.lang.Integer.parseInt(r5)     // Catch:{ Exception -> 0x00b8 }
            r9.setPointX(r11)     // Catch:{ Exception -> 0x00b8 }
            int r11 = java.lang.Integer.parseInt(r6)     // Catch:{ Exception -> 0x00b8 }
            int r11 = r11 + 20
            r9.setPointY(r11)     // Catch:{ Exception -> 0x00b8 }
            java.lang.String r11 = "1"
            r9.setType(r11)     // Catch:{ Exception -> 0x00b8 }
            r10.add(r9)     // Catch:{ Exception -> 0x00b8 }
            r0.moveToNext()     // Catch:{ Exception -> 0x00b8 }
            goto L_0x004f
        L_0x00b8:
            r11 = move-exception
            r2 = r11
            r2.printStackTrace()     // Catch:{ all -> 0x00c8 }
            if (r0 == 0) goto L_0x00c2
            r0.close()
        L_0x00c2:
            if (r8 == 0) goto L_0x00c7
        L_0x00c4:
            r8.close()
        L_0x00c7:
            return r10
        L_0x00c8:
            r11 = move-exception
            if (r0 == 0) goto L_0x00ce
            r0.close()
        L_0x00ce:
            if (r8 == 0) goto L_0x00d3
            r8.close()
        L_0x00d3:
            throw r11
        L_0x00d4:
            if (r0 == 0) goto L_0x00d9
            r0.close()
        L_0x00d9:
            if (r8 == 0) goto L_0x00c7
            goto L_0x00c4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ScenicPointBo.getSpecialPointsByScenicCode(java.lang.String, java.lang.String):java.util.List");
    }
}
