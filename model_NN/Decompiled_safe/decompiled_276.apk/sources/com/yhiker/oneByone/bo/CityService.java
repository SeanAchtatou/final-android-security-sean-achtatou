package com.yhiker.oneByone.bo;

import android.database.sqlite.SQLiteDatabase;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;

public class CityService {
    public static void updateDowned(String cityCode, long dateSize) {
        SQLiteDatabase scenic_list_db = null;
        try {
            scenic_list_db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.scenic_list_path, null, 16);
            scenic_list_db.execSQL(" update scenic_list_city set slc_downed='1',slc_date_size=" + dateSize + " where slc_city_code='" + cityCode + "'");
            if (scenic_list_db == null) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (scenic_list_db == null) {
                return;
            }
        } catch (Throwable th) {
            if (scenic_list_db != null) {
                scenic_list_db.close();
            }
            throw th;
        }
        scenic_list_db.close();
    }

    public static void updateRecodeByDown(String cityCode, long dateZipSize, long dateZipDowningSize) {
        SQLiteDatabase scenic_list_db = null;
        try {
            scenic_list_db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.scenic_list_path, null, 16);
            scenic_list_db.execSQL(" update scenic_list_city set slc_downing_size=" + dateZipDowningSize + ",slc_scenics_zip=" + dateZipSize + " where slc_city_code='" + cityCode + "'");
            if (scenic_list_db == null) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (scenic_list_db == null) {
                return;
            }
        } catch (Throwable th) {
            if (scenic_list_db != null) {
                scenic_list_db.close();
            }
            throw th;
        }
        scenic_list_db.close();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0086, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0099, code lost:
        if (r2 != null) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0081, code lost:
        if (r2 != null) goto L_0x0083;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0083, code lost:
        r2.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.Object> getCityCodes(java.lang.String r6) {
        /*
            r2 = 0
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            r0 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            r3.<init>()     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r4 = com.yhiker.config.GuideConfig.getInitDataDir()     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r4 = "/yhiker/conf/scenic_list.db"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            r4 = 0
            r5 = 16
            android.database.sqlite.SQLiteDatabase r2 = android.database.sqlite.SQLiteDatabase.openDatabase(r3, r4, r5)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            r3.<init>()     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r4 = "SELECT scenic_list_city.scenic_list_city_seq _id,scenic_list_city.slc_city_code,scenic_list_city.slc_scenics_zip slc_scenics_zip,scenic_list_city.slc_scenic_total slc_scenic_total,scenic_list_city.slc_intro slc_intr,city_code.cc_name cc_name FROM scenic_list_city left outer join city_code on scenic_list_city.slc_city_code=city_code.cc_code   where scenic_list_city.slc_city_code ='"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            r4 = 0
            android.database.Cursor r0 = r2.rawQuery(r3, r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            boolean r3 = r0.moveToFirst()     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            if (r3 == 0) goto L_0x007c
        L_0x0049:
            java.lang.String r3 = "cc_name"
            java.lang.String r4 = "cc_name"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            r1.put(r3, r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r3 = "slc_scenic_total"
            java.lang.String r4 = "slc_scenic_total"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            r1.put(r3, r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r3 = "slc_scenics_zip"
            java.lang.String r4 = "slc_scenics_zip"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            r1.put(r3, r4)     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            boolean r3 = r0.moveToNext()     // Catch:{ Exception -> 0x0093, all -> 0x0087 }
            if (r3 != 0) goto L_0x0049
        L_0x007c:
            if (r0 == 0) goto L_0x0081
            r0.close()
        L_0x0081:
            if (r2 == 0) goto L_0x0086
        L_0x0083:
            r2.close()
        L_0x0086:
            return r1
        L_0x0087:
            r3 = move-exception
            if (r0 == 0) goto L_0x008d
            r0.close()
        L_0x008d:
            if (r2 == 0) goto L_0x0092
            r2.close()
        L_0x0092:
            throw r3
        L_0x0093:
            r3 = move-exception
            if (r0 == 0) goto L_0x0099
            r0.close()
        L_0x0099:
            if (r2 == 0) goto L_0x0086
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.CityService.getCityCodes(java.lang.String):java.util.Map");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00b6, code lost:
        if (r3 != null) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00b8, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00bb, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x011b, code lost:
        if (r3 != null) goto L_0x00b8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getPaidCityCodes(java.lang.String r7) {
        /*
            r3 = 0
            java.lang.String r1 = ""
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r0 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r4.<init>()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.io.File r5 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = "/yhiker/conf/per_conf.db"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r5 = 0
            r6 = 16
            android.database.sqlite.SQLiteDatabase r3 = android.database.sqlite.SQLiteDatabase.openDatabase(r4, r5, r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r4 = "down"
            boolean r4 = r4.equals(r7)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r4 == 0) goto L_0x00bc
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r4.<init>()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = "select pl2sc_code from "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = com.yhiker.oneByone.bo.FreeServer.payTable     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = " where pl_key='"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = com.yhiker.config.GuideConfig.deviceId     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = "' and pl_zip_ver>0"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r5 = 0
            android.database.Cursor r0 = r3.rawQuery(r4, r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
        L_0x005f:
            java.lang.String r4 = "cityact"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r5.<init>()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r6 = "records:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            int r6 = r0.getCount()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            android.util.Log.i(r4, r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            boolean r4 = r0.moveToFirst()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r4 == 0) goto L_0x00b1
        L_0x0081:
            java.lang.String r4 = ""
            boolean r4 = r4.equals(r1)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r4 != 0) goto L_0x00ea
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r4.<init>()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = ",'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r5 = 0
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r1 = r4.toString()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
        L_0x00ab:
            boolean r4 = r0.moveToNext()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            if (r4 != 0) goto L_0x0081
        L_0x00b1:
            if (r0 == 0) goto L_0x00b6
            r0.close()
        L_0x00b6:
            if (r3 == 0) goto L_0x00bb
        L_0x00b8:
            r3.close()
        L_0x00bb:
            return r1
        L_0x00bc:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r4.<init>()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = "select pl2sc_code from "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = com.yhiker.oneByone.bo.FreeServer.payTable     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = " where pl_key='"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = com.yhiker.config.GuideConfig.deviceId     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r5 = 0
            android.database.Cursor r0 = r3.rawQuery(r4, r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            goto L_0x005f
        L_0x00ea:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r4.<init>()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            r5 = 0
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r5 = "'"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            java.lang.String r1 = r4.toString()     // Catch:{ Exception -> 0x0115, all -> 0x0109 }
            goto L_0x00ab
        L_0x0109:
            r4 = move-exception
            if (r0 == 0) goto L_0x010f
            r0.close()
        L_0x010f:
            if (r3 == 0) goto L_0x0114
            r3.close()
        L_0x0114:
            throw r4
        L_0x0115:
            r4 = move-exception
            if (r0 == 0) goto L_0x011b
            r0.close()
        L_0x011b:
            if (r3 == 0) goto L_0x00bb
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.CityService.getPaidCityCodes(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00c1, code lost:
        if (r4 != null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d7, code lost:
        if (r4 != null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        if (r4 != null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001a, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<java.util.Map<java.lang.String, java.lang.Object>> getPaidCityListDB(android.content.Context r8, java.lang.String r9, com.yhiker.oneByone.act.GlobalApp r10) {
        /*
            r4 = 0
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r0 = 0
            java.lang.String r1 = getPaidCityCodes(r9)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r5 = ""
            boolean r5 = r5.equals(r1)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            if (r5 == 0) goto L_0x001e
            if (r0 == 0) goto L_0x0018
            r0.close()
        L_0x0018:
            if (r4 == 0) goto L_0x001d
        L_0x001a:
            r4.close()
        L_0x001d:
            return r3
        L_0x001e:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r5.<init>()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.io.File r6 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r6 = r6.getAbsolutePath()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r6 = "/yhiker/conf/scenic_list.db"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r6 = 0
            r7 = 16
            android.database.sqlite.SQLiteDatabase r4 = android.database.sqlite.SQLiteDatabase.openDatabase(r5, r6, r7)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r5.<init>()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r6 = "SELECT scenic_list_city.scenic_list_city_seq _id,scenic_list_city.slc_city_code,scenic_list_city.slc_intro slc_intr,city_code.cc_name cc_name FROM scenic_list_city left outer join city_code on scenic_list_city.slc_city_code=city_code.cc_code   where scenic_list_city.slc_city_code in ("
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.StringBuilder r5 = r5.append(r1)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r6 = ")"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r6 = "order by scenic_list_city.slc_order"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r6 = 0
            android.database.Cursor r0 = r4.rawQuery(r5, r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r5 = "cityact"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r6.<init>()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r7 = "records:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            int r7 = r0.getCount()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            android.util.Log.i(r5, r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            boolean r5 = r0.moveToFirst()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            if (r5 == 0) goto L_0x00bc
        L_0x0086:
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r2.<init>()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r5 = "id"
            r6 = 1
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r2.put(r5, r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r5 = "title"
            r6 = 3
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r2.put(r5, r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r5 = "city_code"
            r6 = 1
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r2.put(r5, r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            java.lang.String r5 = "introduce"
            r6 = 2
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r2.put(r5, r6)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            r3.add(r2)     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            boolean r5 = r0.moveToNext()     // Catch:{ Exception -> 0x00d1, all -> 0x00c5 }
            if (r5 != 0) goto L_0x0086
        L_0x00bc:
            if (r0 == 0) goto L_0x00c1
            r0.close()
        L_0x00c1:
            if (r4 == 0) goto L_0x001d
            goto L_0x001a
        L_0x00c5:
            r5 = move-exception
            if (r0 == 0) goto L_0x00cb
            r0.close()
        L_0x00cb:
            if (r4 == 0) goto L_0x00d0
            r4.close()
        L_0x00d0:
            throw r5
        L_0x00d1:
            r5 = move-exception
            if (r0 == 0) goto L_0x00d7
            r0.close()
        L_0x00d7:
            if (r4 == 0) goto L_0x001d
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.CityService.getPaidCityListDB(android.content.Context, java.lang.String, com.yhiker.oneByone.act.GlobalApp):java.util.List");
    }
}
