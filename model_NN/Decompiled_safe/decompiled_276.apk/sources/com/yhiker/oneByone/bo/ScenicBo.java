package com.yhiker.oneByone.bo;

public class ScenicBo {
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00d9, code lost:
        if (r1 != null) goto L_0x00db;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00db, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00de, code lost:
        return r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00f0, code lost:
        if (r1 != null) goto L_0x00db;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.yhiker.oneByone.module.Scenic> getScenicByCityCode(java.lang.String r8, java.lang.String r9, double r10, double r12) {
        /*
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r1 = 0
            r0 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cf }
            r6.<init>()     // Catch:{ Exception -> 0x00cf }
            java.lang.String r7 = "SELECT sc_order _id,sc_code,sc_name,sc_level,sc_intro,sc_address,sc_has_map,abs(sc_longitude-"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00cf }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r7 = ")*abs(sc_longitude-"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00cf }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r7 = ")+abs(sc_latitude-"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00cf }
            java.lang.StringBuilder r6 = r6.append(r12)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r7 = ")*abs(sc_latitude-"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00cf }
            java.lang.StringBuilder r6 = r6.append(r12)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r7 = ") dist "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r7 = "FROM scenic where city_code='"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00cf }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r7 = "' order by dist"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r3 = r6.toString()     // Catch:{ Exception -> 0x00cf }
            r6 = 0
            r7 = 16
            android.database.sqlite.SQLiteDatabase r1 = android.database.sqlite.SQLiteDatabase.openDatabase(r8, r6, r7)     // Catch:{ Exception -> 0x00cf }
            r6 = 0
            android.database.Cursor r0 = r1.rawQuery(r3, r6)     // Catch:{ Exception -> 0x00cf }
            if (r0 == 0) goto L_0x00eb
            r0.moveToFirst()     // Catch:{ Exception -> 0x00cf }
        L_0x005f:
            boolean r6 = r0.isAfterLast()     // Catch:{ Exception -> 0x00cf }
            if (r6 != 0) goto L_0x00eb
            com.yhiker.oneByone.module.Scenic r4 = new com.yhiker.oneByone.module.Scenic     // Catch:{ Exception -> 0x00cf }
            r4.<init>()     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = "_id"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00cf }
            r4.setId(r6)     // Catch:{ Exception -> 0x00cf }
            r4.setCityCode(r9)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = "sc_code"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00cf }
            r4.setCode(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = "sc_name"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00cf }
            r4.setName(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = "sc_level"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x00cf }
            int r6 = r0.getInt(r6)     // Catch:{ Exception -> 0x00cf }
            r4.setLevel(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = "sc_intro"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00cf }
            r4.setIntro(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = "sc_address"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = r0.getString(r6)     // Catch:{ Exception -> 0x00cf }
            r4.setAddress(r6)     // Catch:{ Exception -> 0x00cf }
            java.lang.String r6 = "sc_has_map"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x00cf }
            int r6 = r0.getInt(r6)     // Catch:{ Exception -> 0x00cf }
            r4.setHasMap(r6)     // Catch:{ Exception -> 0x00cf }
            r5.add(r4)     // Catch:{ Exception -> 0x00cf }
            r0.moveToNext()     // Catch:{ Exception -> 0x00cf }
            goto L_0x005f
        L_0x00cf:
            r6 = move-exception
            r2 = r6
            r2.printStackTrace()     // Catch:{ all -> 0x00df }
            if (r0 == 0) goto L_0x00d9
            r0.close()
        L_0x00d9:
            if (r1 == 0) goto L_0x00de
        L_0x00db:
            r1.close()
        L_0x00de:
            return r5
        L_0x00df:
            r6 = move-exception
            if (r0 == 0) goto L_0x00e5
            r0.close()
        L_0x00e5:
            if (r1 == 0) goto L_0x00ea
            r1.close()
        L_0x00ea:
            throw r6
        L_0x00eb:
            if (r0 == 0) goto L_0x00f0
            r0.close()
        L_0x00f0:
            if (r1 == 0) goto L_0x00de
            goto L_0x00db
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ScenicBo.getScenicByCityCode(java.lang.String, java.lang.String, double, double):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00a3, code lost:
        if (r1 != null) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00a5, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00a8, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ba, code lost:
        if (r1 != null) goto L_0x00a5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.yhiker.oneByone.module.Scenic getScenicByScenicCode(java.lang.String r7, java.lang.String r8) {
        /*
            com.yhiker.oneByone.module.Scenic r4 = new com.yhiker.oneByone.module.Scenic
            r4.<init>()
            r1 = 0
            r0 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0099 }
            r5.<init>()     // Catch:{ Exception -> 0x0099 }
            java.lang.String r6 = "SELECT sc_name,sc_intro,sc_level,sc_address,city_code,sc_order,sc_has_map FROM scenic sl where sc_code='"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0099 }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r6 = "'"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r3 = r5.toString()     // Catch:{ Exception -> 0x0099 }
            r5 = 0
            r6 = 16
            android.database.sqlite.SQLiteDatabase r1 = android.database.sqlite.SQLiteDatabase.openDatabase(r7, r5, r6)     // Catch:{ Exception -> 0x0099 }
            r5 = 0
            android.database.Cursor r0 = r1.rawQuery(r3, r5)     // Catch:{ Exception -> 0x0099 }
            if (r0 == 0) goto L_0x00b5
            r0.moveToFirst()     // Catch:{ Exception -> 0x0099 }
        L_0x0031:
            boolean r5 = r0.isAfterLast()     // Catch:{ Exception -> 0x0099 }
            if (r5 != 0) goto L_0x00b5
            java.lang.String r5 = "sc_order"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0099 }
            r4.setId(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = "sc_name"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0099 }
            r4.setName(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = "sc_level"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0099 }
            int r5 = r0.getInt(r5)     // Catch:{ Exception -> 0x0099 }
            r4.setLevel(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = "sc_intro"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0099 }
            r4.setIntro(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = "sc_address"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0099 }
            r4.setAddress(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = "sc_has_map"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0099 }
            int r5 = r0.getInt(r5)     // Catch:{ Exception -> 0x0099 }
            r4.setHasMap(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = "city_code"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ Exception -> 0x0099 }
            java.lang.String r5 = r0.getString(r5)     // Catch:{ Exception -> 0x0099 }
            r4.setCityCode(r5)     // Catch:{ Exception -> 0x0099 }
            r4.setCode(r8)     // Catch:{ Exception -> 0x0099 }
            r0.moveToNext()     // Catch:{ Exception -> 0x0099 }
            goto L_0x0031
        L_0x0099:
            r5 = move-exception
            r2 = r5
            r2.printStackTrace()     // Catch:{ all -> 0x00a9 }
            if (r0 == 0) goto L_0x00a3
            r0.close()
        L_0x00a3:
            if (r1 == 0) goto L_0x00a8
        L_0x00a5:
            r1.close()
        L_0x00a8:
            return r4
        L_0x00a9:
            r5 = move-exception
            if (r0 == 0) goto L_0x00af
            r0.close()
        L_0x00af:
            if (r1 == 0) goto L_0x00b4
            r1.close()
        L_0x00b4:
            throw r5
        L_0x00b5:
            if (r0 == 0) goto L_0x00ba
            r0.close()
        L_0x00ba:
            if (r1 == 0) goto L_0x00a8
            goto L_0x00a5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ScenicBo.getScenicByScenicCode(java.lang.String, java.lang.String):com.yhiker.oneByone.module.Scenic");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004c, code lost:
        if (r1 != null) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004e, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0051, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0076, code lost:
        if (r1 != null) goto L_0x004e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getScenicKey(java.lang.String r8, java.lang.String r9) {
        /*
            java.lang.String r3 = ""
            r1 = 0
            r0 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0042 }
            r6.<init>()     // Catch:{ Exception -> 0x0042 }
            java.lang.String r7 = "SELECT sc_key FROM scenic sl where sc_code='"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0042 }
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ Exception -> 0x0042 }
            java.lang.String r7 = "'"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0042 }
            java.lang.String r5 = r6.toString()     // Catch:{ Exception -> 0x0042 }
            r6 = 0
            r7 = 16
            android.database.sqlite.SQLiteDatabase r1 = android.database.sqlite.SQLiteDatabase.openDatabase(r8, r6, r7)     // Catch:{ Exception -> 0x0042 }
            r6 = 0
            android.database.Cursor r0 = r1.rawQuery(r5, r6)     // Catch:{ Exception -> 0x0042 }
            if (r0 == 0) goto L_0x0052
            r0.moveToFirst()     // Catch:{ Exception -> 0x0042 }
        L_0x002e:
            boolean r6 = r0.isAfterLast()     // Catch:{ Exception -> 0x0042 }
            if (r6 != 0) goto L_0x0052
            java.lang.String r6 = "sc_key"
            int r6 = r0.getColumnIndex(r6)     // Catch:{ Exception -> 0x0042 }
            java.lang.String r3 = r0.getString(r6)     // Catch:{ Exception -> 0x0042 }
            r0.moveToNext()     // Catch:{ Exception -> 0x0042 }
            goto L_0x002e
        L_0x0042:
            r6 = move-exception
            r2 = r6
            r2.printStackTrace()     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x004c
            r0.close()
        L_0x004c:
            if (r1 == 0) goto L_0x0051
        L_0x004e:
            r1.close()
        L_0x0051:
            return r3
        L_0x0052:
            java.lang.String r6 = "danny"
            android.util.Log.i(r6, r3)     // Catch:{ Exception -> 0x0042 }
            java.lang.String r6 = com.yhiker.config.GuideConfig.keyForkey     // Catch:{ Exception -> 0x0042 }
            java.lang.String r7 = "1"
            java.lang.String r4 = com.yhiker.util.EncryptToolkit.encrypt(r6, r7)     // Catch:{ Exception -> 0x0042 }
            java.lang.String r6 = "danny"
            android.util.Log.i(r6, r4)     // Catch:{ Exception -> 0x0042 }
            java.lang.String r6 = "danny"
            java.lang.String r7 = com.yhiker.config.GuideConfig.keyForkey     // Catch:{ Exception -> 0x0042 }
            java.lang.String r7 = com.yhiker.util.EncryptToolkit.decrypt(r7, r4)     // Catch:{ Exception -> 0x0042 }
            android.util.Log.i(r6, r7)     // Catch:{ Exception -> 0x0042 }
            com.yhiker.config.GuideConfig.curScenicKey = r3     // Catch:{ Exception -> 0x0042 }
            if (r0 == 0) goto L_0x0076
            r0.close()
        L_0x0076:
            if (r1 == 0) goto L_0x0051
            goto L_0x004e
        L_0x0079:
            r6 = move-exception
            if (r0 == 0) goto L_0x007f
            r0.close()
        L_0x007f:
            if (r1 == 0) goto L_0x0084
            r1.close()
        L_0x0084:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.oneByone.bo.ScenicBo.getScenicKey(java.lang.String, java.lang.String):java.lang.String");
    }
}
