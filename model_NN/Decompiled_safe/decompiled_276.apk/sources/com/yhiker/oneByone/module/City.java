package com.yhiker.oneByone.module;

public class City {
    private int amount;
    private String code;
    private String downed;
    private String id;
    private String introduce;
    private boolean isDirect;
    private double lat1;
    private double lat2;
    private double long1;
    private double long2;
    private String name;
    private String picpath;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount2) {
        this.amount = amount2;
    }

    public String getPicpath() {
        return this.picpath;
    }

    public void setPicpath(String picpath2) {
        this.picpath = picpath2;
    }

    public double getLong1() {
        return this.long1;
    }

    public void setLong1(double long12) {
        this.long1 = long12;
    }

    public double getLat1() {
        return this.lat1;
    }

    public void setLat1(double lat12) {
        this.lat1 = lat12;
    }

    public double getLong2() {
        return this.long2;
    }

    public void setLong2(double long22) {
        this.long2 = long22;
    }

    public double getLat2() {
        return this.lat2;
    }

    public void setLat2(double lat22) {
        this.lat2 = lat22;
    }

    public boolean isDirect() {
        return this.isDirect;
    }

    public void setDirect(boolean isDirect2) {
        this.isDirect = isDirect2;
    }

    public String getIntroduce() {
        return this.introduce;
    }

    public void setIntroduce(String introduce2) {
        this.introduce = introduce2;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code2) {
        this.code = code2;
    }

    public String getDowned() {
        return this.downed;
    }

    public void setDowned(String downed2) {
        this.downed = downed2;
    }
}
