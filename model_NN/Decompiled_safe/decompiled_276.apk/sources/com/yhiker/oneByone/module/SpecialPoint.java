package com.yhiker.oneByone.module;

public class SpecialPoint {
    private String code;
    private int id;
    private int level;
    private String name;
    private int pointX;
    private int pointY;
    private String scenicCode;
    private String type;

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public int getPointX() {
        return this.pointX;
    }

    public void setPointX(int pointX2) {
        this.pointX = pointX2;
    }

    public int getPointY() {
        return this.pointY;
    }

    public void setPointY(int pointY2) {
        this.pointY = pointY2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getScenicCode() {
        return this.scenicCode;
    }

    public void setScenicCode(String scenicCode2) {
        this.scenicCode = scenicCode2;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code2) {
        this.code = code2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }
}
