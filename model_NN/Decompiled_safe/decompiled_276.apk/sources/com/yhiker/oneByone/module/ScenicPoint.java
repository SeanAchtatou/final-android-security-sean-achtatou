package com.yhiker.oneByone.module;

import android.widget.Button;

public class ScenicPoint {
    private Button bcButton;
    private String code;
    private String dintro;
    private int id;
    private String intro;
    private float latitude;
    private int level;
    private float longitude;
    private String name;
    private int order;
    private boolean played;
    private int positionX;
    private int positionY;
    private String scenicCode;
    private String type;

    public String getScenicCode() {
        return this.scenicCode;
    }

    public void setScenicCode(String scenicCode2) {
        this.scenicCode = scenicCode2;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code2) {
        this.code = code2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getIntro() {
        return this.intro;
    }

    public void setIntro(String intro2) {
        this.intro = intro2;
    }

    public String getDintro() {
        return this.dintro;
    }

    public void setDintro(String dintro2) {
        this.dintro = dintro2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public int getPositionX() {
        return this.positionX;
    }

    public void setPositionX(int positionX2) {
        this.positionX = positionX2;
    }

    public int getPositionY() {
        return this.positionY;
    }

    public void setPositionY(int positionY2) {
        this.positionY = positionY2;
    }

    public Button getBcButton() {
        return this.bcButton;
    }

    public void setBcButton(Button bcButton2) {
        this.bcButton = bcButton2;
    }

    public boolean isPlayed() {
        return this.played;
    }

    public void setPlayed(boolean played2) {
        this.played = played2;
    }

    public float getLongitude() {
        return this.longitude;
    }

    public void setLongitude(float longitude2) {
        this.longitude = longitude2;
    }

    public float getLatitude() {
        return this.latitude;
    }

    public void setLatitude(float latitude2) {
        this.latitude = latitude2;
    }

    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order2) {
        this.order = order2;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }
}
