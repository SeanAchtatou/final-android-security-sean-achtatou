package com.yhiker.oneByone.module;

public class TalkPoint {
    private int centerX;
    private int centerY;
    private int id;
    private int redius;
    private String scenicName;
    private String scenicid;

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getScenicid() {
        return this.scenicid;
    }

    public void setScenicid(String scenicid2) {
        this.scenicid = scenicid2;
    }

    public String getScenicName() {
        return this.scenicName;
    }

    public void setScenicName(String scenicName2) {
        this.scenicName = scenicName2;
    }

    public int getCenterX() {
        return this.centerX;
    }

    public void setCenterX(int centerX2) {
        this.centerX = centerX2;
    }

    public int getCenterY() {
        return this.centerY;
    }

    public void setCenterY(int centerY2) {
        this.centerY = centerY2;
    }

    public int getRedius() {
        return this.redius;
    }

    public void setRedius(int redius2) {
        this.redius = redius2;
    }
}
