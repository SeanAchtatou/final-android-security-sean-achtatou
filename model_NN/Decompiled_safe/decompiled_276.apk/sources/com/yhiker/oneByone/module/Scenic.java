package com.yhiker.oneByone.module;

public class Scenic {
    private String address;
    private String cityCode;
    private String code;
    private String dintro;
    private int hasMap;
    private int heightNum;
    private String id;
    private String intro;
    private float latitude;
    private int level;
    private float longitude;
    private long mapSize;
    private String name;
    private int order;
    private int widthNum;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getCityCode() {
        return this.cityCode;
    }

    public void setCityCode(String cityCode2) {
        this.cityCode = cityCode2;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code2) {
        this.code = code2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getIntro() {
        return this.intro;
    }

    public void setIntro(String intro2) {
        this.intro = intro2;
    }

    public String getDintro() {
        return this.dintro;
    }

    public void setDintro(String dintro2) {
        this.dintro = dintro2;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address2) {
        this.address = address2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order2) {
        this.order = order2;
    }

    public int getHasMap() {
        return this.hasMap;
    }

    public void setHasMap(int hasMap2) {
        this.hasMap = hasMap2;
    }

    public int getWidthNum() {
        return this.widthNum;
    }

    public void setWidthNum(int widthNum2) {
        this.widthNum = widthNum2;
    }

    public int getHeightNum() {
        return this.heightNum;
    }

    public void setHeightNum(int heightNum2) {
        this.heightNum = heightNum2;
    }

    public long getMapSize() {
        return this.mapSize;
    }

    public void setMapSize(long mapSize2) {
        this.mapSize = mapSize2;
    }

    public float getLongitude() {
        return this.longitude;
    }

    public void setLongitude(float longitude2) {
        this.longitude = longitude2;
    }

    public float getLatitude() {
        return this.latitude;
    }

    public void setLatitude(float latitude2) {
        this.latitude = latitude2;
    }
}
