package com.yhiker.oneByone.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsoluteLayout;
import com.yhiker.guid.menu.CommendWay;
import com.yhiker.guid.module.MapArea;
import com.yhiker.playmate.R;
import java.util.List;

public class CommendWayView extends AbsoluteLayout {
    /* access modifiers changed from: private */
    public CommendWay commendWay;
    private PaintWayView wayView;

    public CommendWayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        removeAllViews();
        this.wayView = new PaintWayView(context);
        addView(this.wayView);
        this.wayView.invalidate();
    }

    public void creatWayView(Context context, CommendWay commendWay2) {
        removeAllViews();
        this.commendWay = commendWay2;
        this.wayView = new PaintWayView(context);
        addView(this.wayView);
        this.wayView.invalidate();
    }

    public void reDrawWayView(Context context) {
        if (this.wayView != null) {
            this.wayView.invalidate();
        }
    }

    public void removeWayView() {
        if (!(this.wayView == null || this.wayView.arrowBitmap == null)) {
            this.wayView.arrowBitmap.recycle();
            this.wayView.arrowBitmap = null;
        }
        removeAllViews();
        this.wayView = null;
    }

    public class PaintWayView extends View {
        Bitmap arrowBitmap;
        int arrowNum;
        private Paint paint;
        Paint paintArrowPaint;
        private int paintWidth;
        float scale;

        public PaintWayView(Context context) {
            super(context);
            this.paintWidth = 0;
            this.arrowNum = 5;
            this.paintArrowPaint = new Paint();
            this.arrowBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.routearrow);
            this.paint = new Paint();
            this.paintWidth = 8;
            this.paint.setColor(Color.rgb(255, 144, 0));
            this.paint.setStrokeWidth((float) this.paintWidth);
            this.paint.setAntiAlias(true);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            this.scale = MapArea.getInstance().getmScale();
            if (CommendWayView.this.commendWay != null) {
                List<Point> allPoints = CommendWayView.this.commendWay.getPointlist();
                for (int i = 0; i < allPoints.size() - 1; i++) {
                    Canvas canvas2 = canvas;
                    canvas2.drawLine(this.scale * ((float) allPoints.get(i).x), this.scale * ((float) allPoints.get(i).y), this.scale * ((float) allPoints.get(i + 1).x), this.scale * ((float) allPoints.get(i + 1).y), this.paint);
                    Canvas canvas3 = canvas;
                    canvas3.drawArc(new RectF((((float) allPoints.get(i).x) * this.scale) - ((float) (this.paintWidth / 2)), (((float) allPoints.get(i).y) * this.scale) - ((float) (this.paintWidth / 2)), ((float) (this.paintWidth / 2)) + (((float) allPoints.get(i).x) * this.scale), (((float) allPoints.get(i).y) * this.scale) + ((float) (this.paintWidth / 2))), 0.0f, 360.0f, true, this.paint);
                    try {
                        if (i % this.arrowNum == 0) {
                            double x1 = (double) (((float) allPoints.get(i).x) * this.scale);
                            double y1 = (double) (((float) allPoints.get(i).y) * this.scale);
                            double x2 = (double) (((float) allPoints.get(i + 1).x) * this.scale);
                            double y2 = (double) (((float) allPoints.get(i + 1).y) * this.scale);
                            double preangle1 = Math.abs(Math.atan((y2 - y1) / (x2 - x1)));
                            double angle1 = 0.0d;
                            if (x2 > x1 && y2 > y1) {
                                angle1 = preangle1;
                            }
                            if (x2 < x1 && y2 > y1) {
                                angle1 = 3.141592653589793d - preangle1;
                            }
                            if (x2 < x1 && y2 < y1) {
                                angle1 = 3.141592653589793d + preangle1;
                            }
                            if (x2 > x1 && y2 < y1) {
                                angle1 = 6.283185307179586d - preangle1;
                            }
                            if (x2 > x1 && y2 == y1) {
                                angle1 = 0.0d;
                            }
                            if (x2 < x1 && y2 == y1) {
                                angle1 = 3.141592653589793d;
                            }
                            if (y2 > y1 && x2 == x1) {
                                angle1 = 1.5707963267948966d;
                            }
                            if (y2 < y1 && x2 == x1) {
                                angle1 = 4.71238898038469d;
                            }
                            canvas.save();
                            canvas.translate((float) ((int) x1), (float) ((int) y1));
                            canvas.rotate((float) ((angle1 / 3.141592653589793d) * 180.0d));
                            canvas.drawBitmap(this.arrowBitmap, new Rect(0, 0, this.arrowBitmap.getWidth(), this.arrowBitmap.getHeight()), new Rect(-6, -6, 6, 6), this.paintArrowPaint);
                            canvas.restore();
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
    }
}
