package com.yhiker.oneByone.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.Window;
import android.widget.AbsoluteLayout;
import com.yhiker.guid.module.GetSpecialPoints;
import com.yhiker.guid.module.MapArea;
import com.yhiker.guid.service.BCButtonAction;
import com.yhiker.oneByone.module.SpecialPoint;

public class BroadcastView extends AbsoluteLayout {
    private PaintView paintView;

    public BroadcastView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void creatBCView(Window window) {
        Context context = window.getContext();
        removeAllViews();
        BCButtonAction.creatAudioButtons(window, this);
        this.paintView = new PaintView(context);
        addView(this.paintView);
        this.paintView.invalidate();
    }

    public void changeBCView(Context context) {
        BCButtonAction.changeAudioButtons();
        this.paintView.invalidate();
    }

    public class PaintView extends View {
        private float FONT_HEIGHTSIZE = 13.5f;
        private float FONT_WIDTHSIZE = 12.0f;
        private Paint circlepaint;
        private Paint textbgpaint;
        private Paint textpaint;

        public PaintView(Context context) {
            super(context);
            initViewPaint();
        }

        public void initViewPaint() {
            this.circlepaint = new Paint();
            this.circlepaint.setColor(Color.argb(150, 155, 155, 155));
            this.circlepaint.setAntiAlias(true);
            this.textpaint = new Paint();
            this.textpaint.setColor(-1);
            this.textpaint.setAntiAlias(true);
            this.textbgpaint = new Paint();
            this.textbgpaint.setColor(-2012147439);
            this.textbgpaint.setAntiAlias(true);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            float scale = MapArea.getInstance().getmScale();
            for (SpecialPoint specialPoint : GetSpecialPoints.getSpecialPointList()) {
                int len = specialPoint.getName().length();
                float left = ((float) specialPoint.getPointX()) * scale;
                float top = ((float) specialPoint.getPointY()) * scale;
                canvas.drawRect(left, (top - this.FONT_HEIGHTSIZE) + 2.0f, left + (this.FONT_WIDTHSIZE * ((float) len)), top + 2.0f, this.textbgpaint);
                canvas.drawText(specialPoint.getName(), left, top, this.textpaint);
            }
        }
    }
}
