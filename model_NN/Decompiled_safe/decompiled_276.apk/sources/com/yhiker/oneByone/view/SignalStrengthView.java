package com.yhiker.oneByone.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsoluteLayout;
import com.yhiker.playmate.R;

public class SignalStrengthView extends AbsoluteLayout {
    public static int signalLevel = 0;
    private SignalStrength signalStrength;

    public SignalStrengthView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.signalStrength = new SignalStrength(context);
        addView(this.signalStrength);
        this.signalStrength.invalidate();
    }

    public void clear() {
        if (this.signalStrength.nosignalBitmap != null) {
            if (!this.signalStrength.nosignalBitmap.isRecycled()) {
                this.signalStrength.nosignalBitmap.recycle();
            }
            Bitmap unused = this.signalStrength.nosignalBitmap = null;
        }
        if (this.signalStrength.signal1Bitmap != null) {
            if (!this.signalStrength.signal1Bitmap.isRecycled()) {
                this.signalStrength.signal1Bitmap.recycle();
            }
            Bitmap unused2 = this.signalStrength.signal1Bitmap = null;
        }
        if (this.signalStrength.signal2Bitmap != null) {
            if (!this.signalStrength.signal2Bitmap.isRecycled()) {
                this.signalStrength.signal2Bitmap.recycle();
            }
            Bitmap unused3 = this.signalStrength.signal2Bitmap = null;
        }
        if (this.signalStrength.signal3Bitmap != null) {
            if (!this.signalStrength.signal3Bitmap.isRecycled()) {
                this.signalStrength.signal3Bitmap.recycle();
            }
            Bitmap unused4 = this.signalStrength.signal3Bitmap = null;
        }
        if (this.signalStrength.signal4Bitmap != null) {
            if (!this.signalStrength.signal4Bitmap.isRecycled()) {
                this.signalStrength.signal4Bitmap.recycle();
            }
            Bitmap unused5 = this.signalStrength.signal4Bitmap = null;
        }
        removeAllViews();
        this.signalStrength = null;
    }

    public class SignalStrength extends View {
        private static final int width = 40;
        /* access modifiers changed from: private */
        public Bitmap nosignalBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.nosignal);
        private Paint paint = new Paint();
        /* access modifiers changed from: private */
        public Bitmap signal1Bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.signal1);
        /* access modifiers changed from: private */
        public Bitmap signal2Bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.signal2);
        /* access modifiers changed from: private */
        public Bitmap signal3Bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.signal3);
        /* access modifiers changed from: private */
        public Bitmap signal4Bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.signal4);

        public SignalStrength(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
        }
    }
}
