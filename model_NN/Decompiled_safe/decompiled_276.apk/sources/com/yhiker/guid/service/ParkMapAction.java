package com.yhiker.guid.service;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;
import com.yhiker.guid.NetDataFactory;
import com.yhiker.guid.menu.CommendWay;
import com.yhiker.guid.menu.GetCommendWay;
import com.yhiker.guid.module.GetScenicpoints;
import com.yhiker.guid.module.GetSpecialPoints;
import com.yhiker.guid.module.GetTalkpoints;
import com.yhiker.guid.module.MapArea;
import com.yhiker.guid.module.MovePoint;
import com.yhiker.guid.module.ParkDataInfo;
import com.yhiker.guid.parse.common.CoordinateTranslate;
import com.yhiker.guid.parse.common.ZipParseMatrixCommon;
import com.yhiker.oneByone.act.GlobalApp;
import com.yhiker.oneByone.act.mediaplayer.PlayerPanelView;
import com.yhiker.oneByone.view.BroadcastView;
import com.yhiker.oneByone.view.CommendWayView;
import com.yhiker.oneByone.view.SignalStrengthView;
import com.yhiker.playmate.R;
import dalvik.system.VMRuntime;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.HashMap;

public class ParkMapAction {
    public static int GPSlevel = 0;
    static boolean IsNeedResume = false;
    private static final int NOMAP_OR_NETERR = 101;
    public static final int WORKING_BEGIN_DOWNLOADING = 4;
    public static final int WORKING_BEGIN_UNZIPING = 5;
    public static final int WORKING_INIT_FINISHING = 7;
    public static final int WORKING__INTERRUPT = 3;
    public static final int WORKING__MAPDATA = 2;
    public static final int WORKING__ZIPDATA = 1;
    /* access modifiers changed from: private */
    public static RelativeLayout allbuttonRelativeLayout;
    private static HashMap<Integer, SoftReference<Bitmap>> bmTileMap = null;
    /* access modifiers changed from: private */
    public static Canvas canvas;
    /* access modifiers changed from: private */
    public static BroadcastView circleView;
    private static int clearAreaWidth = 66;
    private static CommendWayView commendWayView;
    public static String curScenicCode;
    private static int displayHeight;
    private static int displayWidth;
    static int dpHeight = 0;
    static int dpWidth = 0;
    static float g_minScale = 0.0f;
    public static ImageButton gpsButton;
    private static View.OnClickListener gpsButtonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (ParkMapAction.GPSlevel) {
                case 0:
                    Toast.makeText(ParkMapAction.mWindow.getContext().getApplicationContext(), "无GPS信号，无法自动语音导游", 0).show();
                    return;
                case 1:
                    Toast.makeText(ParkMapAction.mWindow.getContext().getApplicationContext(), "GPS信号强度很差，无法自动语音导游", 0).show();
                    return;
                case 2:
                    Toast.makeText(ParkMapAction.mWindow.getContext().getApplicationContext(), "GPS信号强度一般，可以使用", 0).show();
                    return;
                case 3:
                    Toast.makeText(ParkMapAction.mWindow.getContext().getApplicationContext(), "GPS信号强度很好，定位精度高", 0).show();
                    return;
                case 4:
                    Toast.makeText(ParkMapAction.mWindow.getContext().getApplicationContext(), "GPS信号强度非常好，定位精准", 0).show();
                    return;
                default:
                    return;
            }
        }
    };
    public static boolean isClearFinish = true;
    /* access modifiers changed from: private */
    public static boolean largeBtnDisabled;
    /* access modifiers changed from: private */
    public static ImageButton largeButton;
    /* access modifiers changed from: private */
    public static int mLoadMapState = 0;
    public static ParkDataInfo mParkDataInfo = null;
    /* access modifiers changed from: private */
    public static Window mWindow;
    /* access modifiers changed from: private */
    public static InputStream mZipIS = null;
    /* access modifiers changed from: private */
    public static byte[] mZipdata = null;
    public static Handler mapActHandler;
    /* access modifiers changed from: private */
    public static MapArea mapArea;
    /* access modifiers changed from: private */
    public static Bitmap mapBitmap;
    private static String mapDatazipPath = "";
    private static String maptilePicPath = "";
    /* access modifiers changed from: private */
    public static int maxBmHeight;
    /* access modifiers changed from: private */
    public static int maxBmWidth;
    /* access modifiers changed from: private */
    public static boolean mbContinueLoadMap = true;
    static boolean menuInScreen = false;
    private static MovePoint movepoint;
    private static TranslateAnimation myAnimation_Translate;
    private static ImageView mypoint;
    static int oglHeight = 0;
    static int oglWidth = 0;
    private static BitmapFactory.Options options;
    private static Paint paint = new Paint();
    public static PlayerPanelView playerPanelView;
    public static ImageButton rightFold;
    private static View.OnClickListener rightfoldClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            ParkMapAction.moveMenu();
        }
    };
    /* access modifiers changed from: private */
    public static int segmentHeightNum;
    /* access modifiers changed from: private */
    public static int segmentWidthNum;
    private static SignalStrengthView signalStrengthView;
    /* access modifiers changed from: private */
    public static boolean smallBtnDisabled;
    /* access modifiers changed from: private */
    public static ImageButton smallButton;
    /* access modifiers changed from: private */
    public static HashMap<Integer, byte[]> tileMap;

    public static void SetContinueLoadMap(boolean isContinue) {
        if (!isContinue) {
            NetDataFactory.getInstance().stopNDTask();
        }
        mbContinueLoadMap = isContinue;
    }

    public static void stopLoadingMap() {
        NetDataFactory.getInstance().stopNDTask();
        ZipParseMatrixCommon.getInstance().stopMtrTask();
    }

    public static int getLoadMapState() {
        return mLoadMapState;
    }

    public static int getDisplayWidth() {
        return displayWidth;
    }

    public static int getDisplayHeight() {
        return displayHeight;
    }

    public static void stopMediaPlay() {
    }

    private static void findViews(Window window) {
        mypoint = (ImageView) window.findViewById(R.id.mypoint);
        circleView = (BroadcastView) window.findViewById(R.id.circle);
        commendWayView = (CommendWayView) window.findViewById(R.id.commendway);
        signalStrengthView = (SignalStrengthView) window.findViewById(R.id.signalStrength);
        playerPanelView = (PlayerPanelView) window.findViewById(R.id.player_panellayout);
        largeButton = (ImageButton) window.findViewById(R.id.largebutton);
        smallButton = (ImageButton) window.findViewById(R.id.smallbutton);
        gpsButton = (ImageButton) window.findViewById(R.id.gpsbutton);
        rightFold = (ImageButton) window.findViewById(R.id.rightfold);
        allbuttonRelativeLayout = (RelativeLayout) window.findViewById(R.id.AllButtons);
    }

    private static void initViews() {
        mypoint.setBackgroundResource(R.drawable.greenstar);
        mypoint.setVisibility(4);
        gpsButton.setBackgroundResource(R.drawable.signal4);
        rightFold.setBackgroundResource(R.drawable.leftfold);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) allbuttonRelativeLayout.getLayoutParams();
        layoutParams.bottomMargin = -clearAreaWidth;
        allbuttonRelativeLayout.setLayoutParams(layoutParams);
    }

    private static void loadListeners() {
        gpsButton.setOnClickListener(gpsButtonClickListener);
        rightFold.setOnClickListener(rightfoldClickListener);
    }

    private static void initMovePoint(Resources r) {
        movepoint = MovePoint.getInstance();
        movepoint.setMovepoint(mypoint);
        Drawable movedrawable = r.getDrawable(R.drawable.greenstar);
        int moveWidth = movedrawable.getIntrinsicWidth() / 2;
        int moveHeight = movedrawable.getIntrinsicHeight() / 2;
        movepoint.setMoveWidth(moveWidth);
        movepoint.setMoveHeight(moveHeight);
        movepoint.setGuidMode(false);
    }

    private static void initMap(Window window) {
        mapArea = MapArea.getInstance();
        DisplayMetrics dm = new DisplayMetrics();
        window.getWindowManager().getDefaultDisplay().getMetrics(dm);
        displayWidth = dm.widthPixels;
        displayHeight = dm.heightPixels - 68;
        mapArea.setDisplayWidth(displayWidth);
        mapArea.setDisplayHeight(displayHeight);
    }

    public static void initSceneryActivity(Window window) {
        Log.i(ParkMapAction.class.getSimpleName(), "initSceneryActivity is called");
        mLoadMapState = 0;
        mbContinueLoadMap = true;
        menuInScreen = false;
        Resources r = window.getContext().getApplicationContext().getResources();
        mWindow = window;
        findViews(window);
        initViews();
        initMovePoint(r);
        initMap(window);
        loadListeners();
        mapArea.getBitmapForShow().setSegmentWidth(240);
        mapArea.getBitmapForShow().setSegmentHeight(240);
        rightFold.setVisibility(8);
        allbuttonRelativeLayout.setVisibility(8);
    }

    public static void initParkInfo(String curScenic_Code) {
        mParkDataInfo = CoordinateTranslate.findOriginTude(GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + GuideConfig.curCityCode.toLowerCase() + ".db", curScenic_Code);
        curScenicCode = curScenic_Code;
        StringBuilder newId_char = new StringBuilder();
        newId_char.append("").append(curScenic_Code.substring(0, 4)).append("/").append(curScenic_Code.substring(0, 8)).append("/map").append("/");
        String netpath = "http://58.211.138.180:88/0420/" + newId_char.toString();
        maptilePicPath = netpath + "img/";
        mapDatazipPath = netpath + curScenic_Code + ".zip";
    }

    public static void moveMenu() {
        int padding;
        int startx;
        menuInScreen = !menuInScreen;
        if (!menuInScreen) {
            padding = -clearAreaWidth;
            startx = -clearAreaWidth;
        } else {
            padding = 0;
            startx = clearAreaWidth;
        }
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) allbuttonRelativeLayout.getLayoutParams();
        layoutParams.bottomMargin = padding;
        allbuttonRelativeLayout.setLayoutParams(layoutParams);
        allbuttonRelativeLayout.setVisibility(8);
        if (menuInScreen) {
            rightFold.setBackgroundResource(R.drawable.rightfold);
        } else {
            rightFold.setBackgroundResource(R.drawable.leftfold);
        }
        myAnimation_Translate = new TranslateAnimation(0.0f, 0.0f, (float) startx, 0.0f);
        myAnimation_Translate.setDuration(200);
        allbuttonRelativeLayout.startAnimation(myAnimation_Translate);
    }

    /* JADX INFO: Multiple debug info for r0v8 float: [D('maxSampleSize' float), D('x' float)] */
    /* JADX INFO: Multiple debug info for r0v10 int: [D('x' float), D('answer' int)] */
    /* JADX INFO: Multiple debug info for r0v12 float: [D('answer' int), D('minScale' float)] */
    public static void initMapData(final Handler h) {
        float maxSampleSize;
        mapActHandler = h;
        bmTileMap = new HashMap<>();
        mbContinueLoadMap = true;
        int originalWidth = mParkDataInfo.getDrawableWidth();
        int originalHeight = mParkDataInfo.getDrawableHeight();
        oglWidth = originalWidth;
        oglHeight = originalHeight;
        mapArea.setOriginalWidth(originalWidth);
        mapArea.setOriginalHeight(originalHeight);
        mapArea.setLayoutParamsX(0);
        mapArea.setLayoutParamsY(0);
        int displayWidth2 = mapArea.getDisplayWidth();
        int displayHeight2 = mapArea.getDisplayHeight();
        dpWidth = displayWidth2;
        dpHeight = displayHeight2;
        if (((float) originalWidth) / ((float) displayWidth2) > ((float) originalHeight) / ((float) displayHeight2)) {
            maxSampleSize = ((float) originalHeight) / ((float) displayHeight2);
        } else {
            maxSampleSize = ((float) originalWidth) / ((float) displayWidth2);
        }
        float minScale = 1.0f / ((float) Double.valueOf(Math.pow(2.0d, Math.floor((double) ((float) (Math.log10((double) maxSampleSize) / Math.log10(2.0d)))))).intValue());
        g_minScale = minScale;
        mapArea.setMinScale(minScale);
        mapArea.setmScale(minScale);
        mapArea.getBitmapForShow().setBeginXindex(-1);
        mapArea.getBitmapForShow().setBeginYindex(-1);
        mapArea.getBitmapForShow().setEndXindex(-1);
        mapArea.getBitmapForShow().setEndYindex(-1);
        if (mapBitmap != null) {
            if (!mapBitmap.isRecycled()) {
                mapBitmap.recycle();
            }
            mapBitmap = null;
            System.gc();
        }
        int segmentWidth = mapArea.getBitmapForShow().getSegmentWidth();
        int segmentHeight = mapArea.getBitmapForShow().getSegmentHeight();
        maxBmWidth = (((displayWidth2 - 1) / segmentWidth) + 2) * segmentWidth;
        maxBmHeight = (((displayHeight2 - 1) / segmentHeight) + 2) * segmentHeight;
        options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inSampleSize = Math.round(1.0f / minScale);
        segmentWidthNum = mParkDataInfo.getSegmentWidthNum();
        segmentHeightNum = mParkDataInfo.getSegmentHeightNum();
        NetDataFactory netDataFactory = NetDataFactory.getInstance();
        netDataFactory.SetOnUpdatebufListener(new NetDataFactory.OnUpdatebufListener() {
            public boolean OnUpdatebuf(int percent, Handler ph) {
                ph.sendMessage(ph.obtainMessage(1, percent, 0));
                return ParkMapAction.mbContinueLoadMap;
            }
        }, h);
        netDataFactory.SetOnGetBytesFrHttpFinishListener(new NetDataFactory.OnGetBytesFrHttpFinishListener() {
            public void OnGetBytesFrHttpFinish(boolean finish, byte[] data, Handler ph) {
                if (finish) {
                    byte[] unused = ParkMapAction.mZipdata = data;
                    InputStream unused2 = ParkMapAction.mZipIS = new ByteArrayInputStream(ParkMapAction.mZipdata);
                    ph.sendEmptyMessage(0);
                    return;
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                h.sendEmptyMessage(3);
            }
        }, new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                int unused = ParkMapAction.mLoadMapState = 5;
                try {
                    ParkMapAction.createTileStreamMap(h);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        mLoadMapState = 4;
        File localZipFile = new File(GuideConfig.getRootDir() + "/yhiker/data" + mapDatazipPath.substring(mapDatazipPath.indexOf("/0086")));
        if (GuideConfig.curCityCode.equals("00863299")) {
            localZipFile = new File(GuideConfig.getInitDataDir() + "/yhiker/data" + mapDatazipPath.substring(mapDatazipPath.indexOf("/0086")));
        }
        Log.i("datazipfile", "localdatazipfile=" + localZipFile.getAbsolutePath());
        if (localZipFile.exists()) {
            netDataFactory.getBytesFrHttpAsync(localZipFile.getAbsolutePath());
            Log.i("parkmapaction", "read datazippath=" + localZipFile.getAbsolutePath());
            return;
        }
        netDataFactory.getBytesFrHttpAsync(mapDatazipPath);
        Log.i("parkmapaction", "read datazippath=" + mapDatazipPath);
    }

    public static void moveMapImage(int layoutParamsX, int layoutParamsY) {
        int displayWidth2 = mapArea.getDisplayWidth();
        int displayHeight2 = mapArea.getDisplayHeight();
        int mapImageWidth = mapArea.getMapImageWidth();
        int mapImageHeight = mapArea.getMapImageHeight();
        float mScale = mapArea.getmScale();
        if (layoutParamsX > 0) {
            layoutParamsX = 0;
        }
        if (layoutParamsX < displayWidth2 - mapImageWidth) {
            layoutParamsX = displayWidth2 - mapImageWidth;
        }
        if (layoutParamsY > 0) {
            layoutParamsY = 0;
        }
        if (layoutParamsY < displayHeight2 - mapImageHeight) {
            layoutParamsY = displayHeight2 - mapImageHeight;
        }
        mapArea.setLayoutParamsX(layoutParamsX);
        mapArea.setLayoutParamsY(layoutParamsY);
        tilesJoint(maptilePicPath, layoutParamsX, layoutParamsY);
        ImageView mapImage = (ImageView) mWindow.findViewById(R.id.mapImage);
        mapImage.setImageBitmap(mapBitmap);
        mapImage.setLayoutParams(new AbsoluteLayout.LayoutParams(maxBmWidth, maxBmHeight, mapArea.getBitmapForShow().getBmLayoutParamsX(), mapArea.getBitmapForShow().getBmlayoutParamsY()));
        circleView.setLayoutParams(new AbsoluteLayout.LayoutParams(mapArea.getMapImageWidth(), mapArea.getMapImageHeight(), layoutParamsX, layoutParamsY));
        commendWayView.setLayoutParams(new AbsoluteLayout.LayoutParams(mapArea.getMapImageWidth(), mapArea.getMapImageHeight(), layoutParamsX, layoutParamsY));
        signalStrengthView.setLayoutParams(new AbsoluteLayout.LayoutParams(mapArea.getMapImageWidth(), mapArea.getMapImageHeight(), 0, 0));
        if (movepoint.isGuidMode()) {
            int pointleft = Math.round(((float) layoutParamsX) + (((float) movepoint.getMapX()) * mScale));
            int pointtop = Math.round((((float) movepoint.getMapY()) * mScale) + ((float) layoutParamsY));
            movepoint.getMovepoint().setLayoutParams(new AbsoluteLayout.LayoutParams(movepoint.getMoveWidth(), movepoint.getMoveHeight(), pointleft, pointtop));
        }
    }

    public static void pointWalk() {
        int layoutParamsX = mapArea.getLayoutParamsX();
        int layoutParamsY = mapArea.getLayoutParamsY();
        if (movepoint.isGuidMode()) {
            float mScale = mapArea.getmScale();
            int pointleft = Math.round((((float) movepoint.getMapX()) * mScale) + ((float) layoutParamsX));
            int pointtop = Math.round((((float) movepoint.getMapY()) * mScale) + ((float) layoutParamsY));
            movepoint.getMovepoint().setLayoutParams(new AbsoluteLayout.LayoutParams(movepoint.getMoveWidth(), movepoint.getMoveHeight(), pointleft, pointtop));
        }
    }

    public static void large() {
        if (!largeBtnDisabled) {
            scaleChange(2.0f);
            if (!mapArea.isScaleAvailable(2.0f)) {
                largeButton.setBackgroundResource(R.drawable.largedisabled);
                largeBtnDisabled = true;
            }
            if (smallBtnDisabled) {
                smallButton.setBackgroundResource(R.drawable.smallselector);
                smallBtnDisabled = false;
                return;
            }
            return;
        }
        Toast.makeText(mWindow.getContext().getApplicationContext(), (int) R.string.maxscale_map, 0).show();
    }

    public static void small() {
        if (!smallBtnDisabled) {
            scaleChange(0.5f);
            if (!mapArea.isScaleAvailable(0.5f)) {
                smallButton.setBackgroundResource(R.drawable.smalldisabled);
                smallBtnDisabled = true;
            }
            if (largeBtnDisabled) {
                largeButton.setBackgroundResource(R.drawable.largeselector);
                largeBtnDisabled = false;
                return;
            }
            return;
        }
        Toast.makeText(mWindow.getContext().getApplicationContext(), (int) R.string.minscale_map, 0).show();
    }

    public static void scaleChange(float scale) {
        float perscale = mapArea.getmScale();
        mapArea.setmScale(scale * perscale);
        options.inSampleSize = Math.round(1.0f / (scale * perscale));
        clearbmTileMap();
        bmTileMap = new HashMap<>();
        moveMapImage(Math.round((((float) (mapArea.getDisplayWidth() / 2)) * (1.0f - scale)) + (((float) mapArea.getLayoutParamsX()) * scale)), Math.round((((float) (mapArea.getDisplayHeight() / 2)) * (1.0f - scale)) + (((float) mapArea.getLayoutParamsY()) * scale)));
        circleView.changeBCView(mWindow.getContext().getApplicationContext());
        commendWayView.reDrawWayView(mWindow.getContext().getApplicationContext());
    }

    public static void keepPointCenter() {
        float mScale = mapArea.getmScale();
        int centerX = Math.round(((float) movepoint.getMapX()) * mScale);
        int centerY = Math.round(((float) movepoint.getMapY()) * mScale);
        moveMapImage((mapArea.getDisplayWidth() / 2) - centerX, (mapArea.getDisplayHeight() / 2) - centerY);
    }

    public static boolean isInCurrentPark(int x, int y) {
        int drawableWidth = mapArea.getOriginalWidth();
        int drawableHeight = mapArea.getOriginalHeight();
        if (x < 0 || x > drawableWidth || y < 0 || y > drawableHeight) {
            return false;
        }
        return true;
    }

    /* JADX INFO: Multiple debug info for r2v1 int: [D('displayWidth' int), D('endXindex' int)] */
    /* JADX INFO: Multiple debug info for r14v1 int: [D('layoutParamsY' int), D('endYindex' int)] */
    /* JADX INFO: Multiple debug info for r13v8 int: [D('isBmChanged' boolean), D('i' int)] */
    /* JADX INFO: Multiple debug info for r3v6 int: [D('j' int), D('tileBm' android.graphics.Bitmap)] */
    /* JADX INFO: Multiple debug info for r4v6 int: [D('key' int), D('tileBm' android.graphics.Bitmap)] */
    /* JADX INFO: Multiple debug info for r13v23 android.graphics.Bitmap: [D('data' byte[]), D('tileBm' android.graphics.Bitmap)] */
    public static void tilesJoint(String titlesPath, int layoutParamsX, int layoutParamsY) {
        int displayWidth2 = mapArea.getDisplayWidth();
        int displayHeight2 = mapArea.getDisplayHeight();
        int segmentWidth = mapArea.getBitmapForShow().getSegmentWidth();
        int segmentHeight = mapArea.getBitmapForShow().getSegmentHeight();
        float mScale = mapArea.getmScale();
        int tileWidth = Math.round(((float) segmentWidth) * mScale);
        int tileHeight = Math.round(mScale * ((float) segmentHeight));
        int beginXindex = (layoutParamsX * -1) / tileWidth;
        int beginYindex = (layoutParamsY * -1) / tileHeight;
        int endXindex = ((layoutParamsX * -1) + displayWidth2) / tileWidth;
        int endYindex = ((layoutParamsY * -1) + displayHeight2) / tileHeight;
        if (mapArea.getBitmapForShow().isBmChanged(beginXindex, beginYindex, endXindex, endYindex)) {
            int tileLeft = 0;
            int tileLeft2 = 0;
            Bitmap tileBm = null;
            int i = beginXindex;
            while (i < endXindex + 1) {
                int j = beginYindex;
                int tileTop = tileLeft;
                int tileLeft3 = tileLeft2;
                Bitmap tileBm2 = tileBm;
                while (true) {
                    int j2 = j;
                    if (j2 >= endYindex + 1) {
                        break;
                    }
                    int key = (segmentWidthNum * j2) + i;
                    tileLeft3 = tileWidth * (i - beginXindex);
                    tileTop = tileHeight * (j2 - beginYindex);
                    if (bmTileMap.get(Integer.valueOf(key)) == null || bmTileMap.get(Integer.valueOf(key)).get() == null) {
                        byte[] data = tileMap.get(Integer.valueOf(key));
                        Bitmap tileBm3 = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                        bmTileMap.put(Integer.valueOf(key), new SoftReference(tileBm3));
                        tileBm2 = tileBm3;
                    } else {
                        tileBm2 = (Bitmap) bmTileMap.get(Integer.valueOf(key)).get();
                    }
                    if (tileBm2 != null) {
                        canvas.drawBitmap(tileBm2, (float) tileLeft3, (float) tileTop, paint);
                    }
                    j = j2 + 1;
                }
                i++;
                tileBm = tileBm2;
                tileLeft2 = tileLeft3;
                tileLeft = tileTop;
            }
            System.gc();
        }
    }

    public static void clearbmTileMap() {
        if (bmTileMap != null) {
            for (SoftReference<Bitmap> softReference : bmTileMap.values()) {
                Bitmap tileBitmap = (Bitmap) softReference.get();
                if (tileBitmap != null) {
                    tileBitmap.recycle();
                }
            }
            bmTileMap.clear();
            bmTileMap = null;
            System.gc();
            VMRuntime.getRuntime().gcSoftReferences();
        }
    }

    public static void createTileStreamMap(Handler pCreateTileStreamMapHandler) throws IOException {
        tileMap = new HashMap<>(segmentWidthNum * segmentHeightNum);
        ZipParseMatrixCommon zipParseMatrixCommon = ZipParseMatrixCommon.getInstance();
        zipParseMatrixCommon.SetOnGetMatrixDataFrZipbufChildFinishListener(new ZipParseMatrixCommon.OnGetMatrixDataFrZipbufChildFinishListener() {
            public boolean OnGetMatrixDataFrZipbufChildFinish(int indexX, int indexY, byte[] data, Handler ph) {
                if (ParkMapAction.mbContinueLoadMap) {
                    int key = (ParkMapAction.segmentWidthNum * indexY) + indexX;
                    ParkMapAction.tileMap.put(Integer.valueOf(key), data);
                    Log.e("OnGetMatrixDataFrZipbufChildFinish pd", "" + data.length);
                    if (ParkMapAction.segmentWidthNum == 0 || ParkMapAction.segmentHeightNum == 0) {
                        Log.e("pd", "error");
                    } else {
                        int nowpos = (key * 100) / ((ParkMapAction.segmentWidthNum * ParkMapAction.segmentHeightNum) - 1);
                    }
                    ph.sendMessage(ph.obtainMessage(2, key + 1, 0));
                } else {
                    ph.sendEmptyMessage(3);
                }
                return ParkMapAction.mbContinueLoadMap;
            }
        }, pCreateTileStreamMapHandler);
        zipParseMatrixCommon.SetOnGetMatrixDataFrZipbufFinishListener(new ZipParseMatrixCommon.OnGetMatrixDataFrZipbufFinishListener() {
            public void OnGetMatrixDataFrZipbufFinish(boolean finished, Handler ph) {
                if (finished) {
                    ph.sendEmptyMessage(0);
                }
            }
        }, new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                boolean z;
                boolean z2;
                super.handleMessage(msg);
                String dbPath = GuideConfig.getInitDataDir() + ConfigHp.scenic_city_path + GuideConfig.curCityCode.toLowerCase() + ".db";
                GetScenicpoints.parseScenicpointsInfoFromDB(dbPath, ParkMapAction.curScenicCode);
                GetTalkpoints.parseTalkPointsInfoFromDB(dbPath, ParkMapAction.curScenicCode);
                GetSpecialPoints.parseSpecialpointsInfoFromDB(dbPath, ParkMapAction.curScenicCode);
                GetCommendWay.parseRoutesInfoFromDB(dbPath, ParkMapAction.curScenicCode);
                if (ParkMapAction.mZipIS != null) {
                    try {
                        ParkMapAction.mZipIS.close();
                        InputStream unused = ParkMapAction.mZipIS = null;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                byte[] unused2 = ParkMapAction.mZipdata = null;
                if (ParkMapAction.mapBitmap != null) {
                    if (!ParkMapAction.mapBitmap.isRecycled()) {
                        ParkMapAction.mapBitmap.recycle();
                        Bitmap unused3 = ParkMapAction.mapBitmap = null;
                    } else {
                        Bitmap unused4 = ParkMapAction.mapBitmap = null;
                    }
                }
                System.gc();
                Bitmap unused5 = ParkMapAction.mapBitmap = Bitmap.createBitmap(ParkMapAction.maxBmWidth, ParkMapAction.maxBmHeight, Bitmap.Config.RGB_565);
                Canvas unused6 = ParkMapAction.canvas = new Canvas(ParkMapAction.mapBitmap);
                ParkMapAction.circleView.creatBCView(ParkMapAction.mWindow);
                if (GlobalApp.curScenicCode == null || !"00863299030001".equals(GlobalApp.curScenicCode)) {
                    ParkMapAction.moveMapImage(Math.round((((float) (ParkMapAction.dpWidth - ParkMapAction.oglWidth)) * ParkMapAction.g_minScale) / 2.0f), Math.round((((float) (ParkMapAction.dpHeight - ParkMapAction.oglHeight)) * ParkMapAction.g_minScale) / 2.0f));
                } else {
                    ParkMapAction.moveMapImage(Math.round((((float) (ParkMapAction.dpWidth - ParkMapAction.oglWidth)) * ParkMapAction.g_minScale) / 2.0f), Math.round((((float) (ParkMapAction.dpHeight - ParkMapAction.oglHeight)) * ParkMapAction.g_minScale) / 2.0f) + 90);
                }
                ParkMapAction.largeButton.setBackgroundResource(R.drawable.largeselector);
                ParkMapAction.smallButton.setBackgroundResource(R.drawable.smallselector);
                if (!ParkMapAction.mapArea.isScaleAvailable(2.0f)) {
                    z = true;
                } else {
                    z = false;
                }
                boolean unused7 = ParkMapAction.largeBtnDisabled = z;
                if (ParkMapAction.largeBtnDisabled) {
                    ParkMapAction.largeButton.setBackgroundResource(R.drawable.largedisabled);
                }
                if (!ParkMapAction.mapArea.isScaleAvailable(0.5f)) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                boolean unused8 = ParkMapAction.smallBtnDisabled = z2;
                if (ParkMapAction.smallBtnDisabled) {
                    ParkMapAction.smallButton.setBackgroundResource(R.drawable.smalldisabled);
                }
                ParkMapAction.rightFold.setVisibility(8);
                ParkMapAction.allbuttonRelativeLayout.setVisibility(0);
                int unused9 = ParkMapAction.mLoadMapState = 7;
                ParkMapAction.mapActHandler.sendEmptyMessage(7);
            }
        });
        if (mZipdata == null || mZipdata.length <= 0) {
            mapActHandler.sendEmptyMessage(NOMAP_OR_NETERR);
        } else {
            zipParseMatrixCommon.getMatrixDataFrZipbufAsync(mZipdata, segmentWidthNum, segmentHeightNum, "-", ".jpg");
        }
    }

    public static void clearMapData() {
        ((ImageView) mWindow.findViewById(R.id.mapImage)).setImageBitmap(null);
        if (tileMap != null) {
            if (!tileMap.isEmpty()) {
                tileMap.clear();
            }
            tileMap = null;
        }
        movepoint.claerme();
        clearRefs();
        if (mapBitmap != null) {
            if (!mapBitmap.isRecycled()) {
                mapBitmap.recycle();
            }
            mapBitmap = null;
        }
        clearbmTileMap();
        clearCommendWay();
        System.gc();
        isClearFinish = true;
    }

    public static void clearRefs() {
        allbuttonRelativeLayout.clearAnimation();
        circleView.removeAllViews();
        allbuttonRelativeLayout.setVisibility(4);
    }

    public static void displayCommendWay(CommendWay commendWay) {
        commendWayView.creatWayView(mWindow.getContext().getApplicationContext(), commendWay);
    }

    public static void clearCommendWay() {
        commendWayView.removeWayView();
    }
}
