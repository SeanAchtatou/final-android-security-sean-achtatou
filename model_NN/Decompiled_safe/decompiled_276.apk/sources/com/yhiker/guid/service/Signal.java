package com.yhiker.guid.service;

import android.location.GpsSatellite;
import java.util.Iterator;

public class Signal {
    public static int getLevel(Iterable<GpsSatellite> itGpsStatellites1) {
        int ActiveSatellitesNum1 = 0;
        int ActiveSatellitesNum2 = 0;
        int ActiveSatellitesNum3 = 0;
        int ActiveSatellitesNum4 = 0;
        Iterator<GpsSatellite> it = itGpsStatellites1.iterator();
        int satellitesCount = 0;
        while (it.hasNext()) {
            it.next();
            satellitesCount++;
        }
        if (satellitesCount <= 3) {
            return 0;
        }
        if (satellitesCount >= 4) {
            for (GpsSatellite gpsSatellite : itGpsStatellites1) {
                ActiveSatellitesNum1++;
                if (gpsSatellite.getSnr() > 0.0f && gpsSatellite.getSnr() <= 15.0f) {
                    ActiveSatellitesNum2++;
                } else if (gpsSatellite.getSnr() > 15.0f && gpsSatellite.getSnr() <= 25.0f) {
                    ActiveSatellitesNum3++;
                } else if (gpsSatellite.getSnr() > 25.0f) {
                    ActiveSatellitesNum4++;
                }
            }
            if (ActiveSatellitesNum4 >= 5) {
                return 4;
            }
            if (ActiveSatellitesNum4 + ActiveSatellitesNum3 >= 5) {
                return 3;
            }
            if (ActiveSatellitesNum3 + ActiveSatellitesNum2 >= 5) {
                return 2;
            }
            if (ActiveSatellitesNum2 + ActiveSatellitesNum1 >= 5) {
                return 1;
            }
            if (ActiveSatellitesNum1 >= 1) {
                return 0;
            }
        }
        return 0;
    }
}
