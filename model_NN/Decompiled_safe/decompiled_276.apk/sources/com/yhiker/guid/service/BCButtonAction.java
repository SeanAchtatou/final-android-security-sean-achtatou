package com.yhiker.guid.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageView;
import com.yhiker.config.GuideConfig;
import com.yhiker.guid.NetBitmaptFactory;
import com.yhiker.guid.module.GetScenicpoints;
import com.yhiker.guid.module.MapArea;
import com.yhiker.oneByone.act.GlobalApp;
import com.yhiker.oneByone.act.mediaplayer.PlayerPanelView;
import com.yhiker.oneByone.bo.LogService;
import com.yhiker.oneByone.bo.WallPlayerService;
import com.yhiker.oneByone.module.ScenicPoint;
import com.yhiker.oneByone.view.BroadcastView;
import com.yhiker.playmate.R;
import java.io.File;
import java.util.List;

public class BCButtonAction {
    private static int FONT_HEIGHTSIZE = 34;
    private static int FONT_WIDTHSIZE = 15;
    static final int MSG_SpotImageFinish = 1001;
    public static String currentParkId = "-1";
    public static Button currentPlayButton = null;
    /* access modifiers changed from: private */
    public static boolean currentPointPlayFinished = false;
    public static GlobalApp gApp;
    static Drawable g_SpotDrawable = null;
    static ImageView g_SpotiView = null;
    public static boolean isAutoPlayed = false;
    static NetBitmaptFactory mNetBitmapFty = null;
    /* access modifiers changed from: private */
    public static Context mcontext;
    static Handler pHandler = null;
    static PlayerPanelView playerPanelView;
    private static WallPlayerService wallPlayerService = new WallPlayerService();
    public static Window window;

    public static void animationBcButton(BroadcastView view) {
        Log.d(BCButtonAction.class.getSimpleName(), "GlobalApp.curBcPointSeq= " + GlobalApp.curBcPointSeq);
        GlobalApp globalApp = gApp;
        GlobalApp globalApp2 = gApp;
        GlobalApp.preCurScenicCode = GlobalApp.curScenicCode;
        Button bcButton = null;
        GlobalApp globalApp3 = gApp;
        if (GlobalApp.curAttCode != null) {
            GlobalApp globalApp4 = gApp;
            if (!"".equals(GlobalApp.curAttCode)) {
                GlobalApp globalApp5 = gApp;
                String str = GlobalApp.curAttCode;
                GlobalApp globalApp6 = gApp;
                if (str.startsWith(GlobalApp.curScenicCode)) {
                    GlobalApp globalApp7 = gApp;
                    if (GlobalApp.curAttId > 0) {
                        GlobalApp globalApp8 = gApp;
                        bcButton = (Button) view.findViewById(GlobalApp.curAttId);
                    }
                }
            }
        }
        if (bcButton != null) {
            Log.d(BCButtonAction.class.getSimpleName(), bcButton.getText().toString());
            Log.i(BCButtonAction.class.getSimpleName(), "currentPlayButton= " + currentPlayButton);
            if (!(currentPlayButton == null || currentPlayButton.getId() == bcButton.getId())) {
                Log.d(BCButtonAction.class.getSimpleName(), currentPlayButton.getText().toString());
                currentPlayButton.clearAnimation();
                currentPlayButton.setBackgroundResource(R.drawable.audioplayselector);
            }
            bcButton.setAnimation(AnimationUtils.loadAnimation(mcontext, R.anim.alpha));
            bcButton.setBackgroundResource(R.drawable.audiopauseselector);
            currentPlayButton = bcButton;
            return;
        }
        Log.d(BCButtonAction.class.getSimpleName(), "bcButton is null");
        if (currentPlayButton != null) {
            Log.d(BCButtonAction.class.getSimpleName(), currentPlayButton.getText().toString());
            currentPlayButton.clearAnimation();
            currentPlayButton.setBackgroundResource(R.drawable.audioplayselector);
        }
    }

    public static void clearAnimationBcButton() {
        if (currentPlayButton != null) {
            Log.d(BCButtonAction.class.getSimpleName(), currentPlayButton.getText().toString());
            currentPlayButton.clearAnimation();
            currentPlayButton = null;
        }
    }

    /* JADX INFO: Multiple debug info for r8v3 com.yhiker.guid.module.MapArea: [D('scenicpoints' java.util.List<com.yhiker.oneByone.module.ScenicPoint>), D('mapArea' com.yhiker.guid.module.MapArea)] */
    public static void creatAudioButtons(Window window2, AbsoluteLayout audioButtonlayout) {
        Log.d(BCButtonAction.class.getSimpleName(), "creatAudioButtons is called");
        mcontext = window2.getContext();
        List<ScenicPoint> scenicpoints = GetScenicpoints.getScenicPointList();
        Log.d(BCButtonAction.class.getSimpleName(), "scenicpoints=" + scenicpoints);
        float scale = MapArea.getInstance().getmScale();
        for (ScenicPoint circle : scenicpoints) {
            Button playButton = new Button(mcontext);
            int bcId = circle.getId();
            playButton.setBackgroundColor(Color.argb(155, 0, 0, 0));
            playButton.setBackgroundResource(R.drawable.audioplayselector);
            playButton.setText(circle.getName());
            playButton.setTextColor(Color.argb(255, 255, 255, 255));
            playButton.setTextSize(12.0f);
            playButton.setId(bcId);
            Log.d(BCButtonAction.class.getSimpleName(), "playButton's id = " + bcId);
            playButton.setLayoutParams(new AbsoluteLayout.LayoutParams((circle.getName().length() * FONT_WIDTHSIZE) + 20, FONT_HEIGHTSIZE, Math.round(((float) circle.getPositionX()) * scale), Math.round(((float) circle.getPositionY()) * scale)));
            playButton.setContentDescription(circle.getCode() + "_" + circle.getName());
            audioButtonlayout.addView(playButton);
            playButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String description = (String) v.getContentDescription();
                    if (!"".equalsIgnoreCase(description) && description != null && !"".equalsIgnoreCase(BCButtonAction.currentParkId) && BCButtonAction.currentParkId != null) {
                        GlobalApp.curAttCode = description.split("_")[0];
                        GlobalApp.curAttName = description.split("_")[1];
                        GlobalApp.curAttId = v.getId();
                    }
                    View rootView = v.getRootView();
                    if (BCButtonAction.currentPlayButton == null) {
                        BCButtonAction.currentPlayButton = (Button) v;
                        BCButtonAction.currentPlayButton.setAnimation(AnimationUtils.loadAnimation(BCButtonAction.mcontext, R.anim.alpha));
                        boolean unused = BCButtonAction.currentPointPlayFinished = false;
                        BCButtonAction.broadcastScenic(GlobalApp.curAttCode, rootView);
                        return;
                    }
                    BCButtonAction.currentPlayButton.clearAnimation();
                    if (!((String) BCButtonAction.currentPlayButton.getContentDescription()).equals(description) || BCButtonAction.currentPointPlayFinished) {
                        BCButtonAction.currentPlayButton.setBackgroundResource(R.drawable.audioplayselector);
                        BCButtonAction.currentPlayButton = (Button) v;
                        BCButtonAction.currentPlayButton.setAnimation(AnimationUtils.loadAnimation(BCButtonAction.mcontext, R.anim.alpha));
                        boolean unused2 = BCButtonAction.currentPointPlayFinished = false;
                        BCButtonAction.broadcastScenic(GlobalApp.curAttCode, rootView);
                        return;
                    }
                    BCButtonAction.currentPlayButton.setAnimation(AnimationUtils.loadAnimation(BCButtonAction.mcontext, R.anim.alpha));
                    if (!PlayerPanelView.finished) {
                        BCButtonAction.currentPlayButton.setBackgroundResource(R.drawable.audioplayselector);
                    } else {
                        BCButtonAction.currentPlayButton.setBackgroundResource(R.drawable.audiopauseselector);
                    }
                }
            });
            circle.setBcButton(playButton);
        }
    }

    public static void broadcastScenic(String bcIdStr, View rootView) {
        GlobalApp.curBcPointSeq = bcIdStr;
        Log.e(BCButtonAction.class.getSimpleName(), "broadcastScenic:" + GlobalApp.curBcPointSeq);
        Log.i(BCButtonAction.class.getSimpleName(), "broadcastScenic is called");
        playerPanelView = (PlayerPanelView) window.findViewById(R.id.player_panellayout);
        playerPanelView.setVisibility(0);
        final ScenicPoint clkCircle = GetScenicpoints.getBCbyId(bcIdStr);
        if (isAutoPlayed) {
            isAutoPlayed = false;
        } else {
            new Thread() {
                public void run() {
                    BCButtonAction.gApp.curCityCodeForPlay = GuideConfig.curCityCode;
                    GlobalApp globalApp = BCButtonAction.gApp;
                    GlobalApp globalApp2 = BCButtonAction.gApp;
                    globalApp.curScenicCodeForPlay = GlobalApp.curScenicCode;
                    LogService.bcScenicSpot(BCButtonAction.currentParkId, "" + clkCircle.getId(), "-1", "-1", clkCircle.getName(), false, "1");
                }
            }.start();
        }
        currentPlayButton.setBackgroundResource(R.drawable.audiopauseselector);
        String al_code = clkCircle.getCode();
        String mp3Path = (GuideConfig.getRootDir() + "/yhiker/data" + File.separator + "0086/" + GuideConfig.curCityCode + File.separator) + "audio/";
        String data_url = "http://58.211.138.180:88/0420/0086/" + GuideConfig.curCityCode + File.separator;
        String mp3url = data_url + "audio/";
        File kbs32 = new File(mp3Path + al_code + "_32Kbps.m4a");
        if (!PlayerPanelView.finished) {
            if (kbs32.exists()) {
                playerPanelView.btnPlaPauClk(mp3url, al_code + "_32Kbps.m4a");
            } else {
                playerPanelView.btnPlaPauClk(mp3url, al_code + "_24Kbps.mp3");
            }
        }
        Log.i("map mp3 folder", mp3Path);
        String imgPath = data_url + "img/" + al_code + "_218-152.jpg";
        g_SpotiView = (ImageView) window.findViewById(R.id.player_spotview);
        mNetBitmapFty = NetBitmaptFactory.getInstance();
        pHandler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case BCButtonAction.MSG_SpotImageFinish /*1001*/:
                        BCButtonAction.g_SpotiView.setImageDrawable(BCButtonAction.g_SpotDrawable);
                        ((ImageView) BCButtonAction.window.findViewById(R.id.player_spotbtn)).setImageDrawable(BCButtonAction.g_SpotDrawable);
                        PlayerPanelView playerPanelView = BCButtonAction.playerPanelView;
                        PlayerPanelView.boardScienceImage = BCButtonAction.g_SpotDrawable;
                        return;
                    default:
                        return;
                }
            }
        };
        File spotFile = new File(imgPath.replace("http://58.211.138.180:88/0420/", GuideConfig.getRootDir() + "/yhiker/data" + "/"));
        if (spotFile.exists()) {
            g_SpotDrawable = BitmapDrawable.createFromPath(spotFile.getAbsolutePath());
            pHandler.sendEmptyMessage(MSG_SpotImageFinish);
            return;
        }
        mNetBitmapFty.SetOnGetBitmapFrHttpListener(new NetBitmaptFactory.OnGetBitmapFrHttpListener() {
            public void OnGetBitmapFrHttp(Bitmap bitmap, Handler h) {
                BCButtonAction.g_SpotDrawable = new BitmapDrawable(bitmap);
                h.sendEmptyMessage(BCButtonAction.MSG_SpotImageFinish);
            }
        }, pHandler);
        mNetBitmapFty.getBitmapFrHttpAsync(imgPath);
    }

    public static void changeAudioButtons() {
        List<ScenicPoint> scenicpoints = GetScenicpoints.getScenicPointList();
        int bcButtonCount = scenicpoints.size();
        float scale = MapArea.getInstance().getmScale();
        for (int i = 0; i < bcButtonCount; i++) {
            ScenicPoint circle = scenicpoints.get(i);
            scenicpoints.get(i).getBcButton().setLayoutParams(new AbsoluteLayout.LayoutParams((FONT_WIDTHSIZE * circle.getName().length()) + 20, FONT_HEIGHTSIZE, Math.round(((float) circle.getPositionX()) * scale), Math.round(((float) circle.getPositionY()) * scale)));
        }
    }
}
