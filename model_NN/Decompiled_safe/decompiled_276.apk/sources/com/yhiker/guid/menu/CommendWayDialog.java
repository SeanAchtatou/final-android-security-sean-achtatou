package com.yhiker.guid.menu;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.yhiker.guid.service.ParkMapAction;
import com.yhiker.playmate.R;
import java.util.List;

public class CommendWayDialog extends Dialog {
    private String commendWay_Path;
    private Context context;
    /* access modifiers changed from: private */
    public CommendWay selectedItem;

    public CommendWayDialog(Context context2) {
        super(context2);
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(true);
        setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                CommendWayDialog.this.onStop();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        List<CommendWay> items = GetCommendWay.getRouteListFromMap();
        setTitle((int) R.string.commendway_title);
        setContentView((int) R.layout.selectedlist);
        if (items != null) {
            ListView listView = (ListView) findViewById(R.id.list);
            listView.setAdapter((ListAdapter) new ArrayAdapter<>(this.context, (int) R.layout.list_item, items));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                    CommendWay unused = CommendWayDialog.this.selectedItem = (CommendWay) adapter.getItemAtPosition(position);
                    if (CommendWayDialog.this.selectedItem.getId() >= 0) {
                        ParkMapAction.displayCommendWay(CommendWayDialog.this.selectedItem);
                    } else {
                        ParkMapAction.clearCommendWay();
                    }
                    CommendWayDialog.this.onStop();
                    CommendWayDialog.this.cancel();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public void setCommendWay_Path(String commendWayPath) {
        this.commendWay_Path = commendWayPath;
    }
}
