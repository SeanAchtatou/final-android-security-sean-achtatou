package com.yhiker.guid.menu;

import android.graphics.Point;
import com.yhiker.guid.module.ItemListFace;
import java.util.List;

public class CommendWay implements ItemListFace {
    private String description;
    private int id;
    private String name;
    private List<Point> pointlist;
    private String sr_code;

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getSr_code() {
        return this.sr_code;
    }

    public void setSr_code(String srCode) {
        this.sr_code = srCode;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public List<Point> getPointlist() {
        return this.pointlist;
    }

    public void setPointlist(List<Point> pointlist2) {
        this.pointlist = pointlist2;
    }

    public String toString() {
        return this.name;
    }

    public List<ItemListFace> getChildrenItem() {
        return null;
    }
}
