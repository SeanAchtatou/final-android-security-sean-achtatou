package com.yhiker.guid.menu;

import com.yhiker.oneByone.bo.CommendWayBo;
import java.util.ArrayList;
import java.util.List;

public class GetCommendWay {
    private static List<CommendWay> commendWays_instance = new ArrayList();

    private GetCommendWay() {
    }

    public static void parseRoutesInfoFromDB(String dbPath, String scenicCode) {
        commendWays_instance = CommendWayBo.getCommendWaysByScenicCode(dbPath, scenicCode);
    }

    public static CommendWay getSelectedRouteFromMap(int itemId) {
        if (!commendWays_instance.isEmpty()) {
            return commendWays_instance.get(itemId);
        }
        return null;
    }

    public static List<CommendWay> getRouteListFromMap() {
        return commendWays_instance;
    }

    public static void clearRoutes() {
        if (commendWays_instance != null) {
            commendWays_instance.clear();
            commendWays_instance = null;
        }
    }
}
