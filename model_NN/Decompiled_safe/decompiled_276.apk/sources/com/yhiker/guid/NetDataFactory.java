package com.yhiker.guid;

import android.os.Handler;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class NetDataFactory {
    public static final String TAG = "netdatafac-";
    private static NetDataFactory netDataFactory_instance = new NetDataFactory();
    private boolean mCanTaskRunFinish;
    Thread mCurTask = null;
    String mCurUrl = null;
    Runnable mNDTaskRunnable = new Runnable() {
        public void run() {
            NetDataFactory.this.doGetBytesFrHttpAsync();
            NetDataFactory.this.mTaskCount--;
        }
    };
    OnGetBytesFrHttpFinishListener mOnGetBytesFrHttpFinishListener = null;
    OnUpdatebufListener mOnUpdatebufListener = null;
    int mTaskCount = 0;
    private boolean mbTaskContinue;
    int mnWorkMode = 0;
    Handler pOnGetBytesFrHttpFinishListenerHandler = null;
    Handler pOnUpdatebufListenerHandler = null;

    public interface OnGetBytesFrHttpFinishListener {
        void OnGetBytesFrHttpFinish(boolean z, byte[] bArr, Handler handler);
    }

    public interface OnUpdatebufListener {
        boolean OnUpdatebuf(int i, Handler handler);
    }

    private NetDataFactory() {
    }

    public static NetDataFactory getInstance() {
        return netDataFactory_instance;
    }

    public void stopNDTask() {
        if (!(this.mCurTask == null && this.mTaskCount == 0) && Thread.State.RUNNABLE == this.mCurTask.getState()) {
            this.mCurTask.interrupt();
            do {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (Thread.State.TERMINATED == this.mCurTask.getState() || this.mTaskCount == 0) {
                    return;
                }
            } while (this.mCurTask != null);
        }
    }

    public void getBytesFrHttpAsync(String url) {
        this.mCurUrl = url;
        this.mbTaskContinue = true;
        this.mCanTaskRunFinish = true;
        if (this.mTaskCount > 1 && this.mCurTask != null) {
            stopNDTask();
        }
        this.mTaskCount++;
        this.mCurTask = new Thread(this.mNDTaskRunnable);
        this.mCurTask.start();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:53:0x0125=Splitter:B:53:0x0125, B:37:0x00c8=Splitter:B:37:0x00c8} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void doGetBytesFrHttpAsync() {
        /*
            r20 = this;
            monitor-enter(r20)
            r5 = 0
            r12 = 0
            r9 = 0
            r11 = 0
            r6 = 0
            r0 = r20
            java.lang.String r0 = r0.mCurUrl     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r17 = r0
            java.lang.String r18 = "http://"
            boolean r17 = r17.contains(r18)     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            if (r17 == 0) goto L_0x009b
            java.net.URL r13 = new java.net.URL     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r0 = r20
            java.lang.String r0 = r0.mCurUrl     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r17 = r0
            r0 = r13
            r1 = r17
            r0.<init>(r1)     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            java.net.URLConnection r17 = r13.openConnection()     // Catch:{ IOException -> 0x0138, InterruptedException -> 0x012e, all -> 0x0142 }
            r0 = r17
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0138, InterruptedException -> 0x012e, all -> 0x0142 }
            r6 = r0
            r17 = 1
            r0 = r6
            r1 = r17
            r0.setDoInput(r1)     // Catch:{ IOException -> 0x0138, InterruptedException -> 0x012e, all -> 0x0142 }
            r6.connect()     // Catch:{ IOException -> 0x0138, InterruptedException -> 0x012e, all -> 0x0142 }
            r17 = 5
            java.lang.Thread.sleep(r17)     // Catch:{ IOException -> 0x0138, InterruptedException -> 0x012e, all -> 0x0142 }
            java.io.InputStream r9 = r6.getInputStream()     // Catch:{ IOException -> 0x0138, InterruptedException -> 0x012e, all -> 0x0142 }
            int r11 = r6.getContentLength()     // Catch:{ IOException -> 0x0138, InterruptedException -> 0x012e, all -> 0x0142 }
            r12 = r13
        L_0x0044:
            r17 = -1
            r0 = r17
            r1 = r11
            if (r0 == r1) goto L_0x0099
            byte[] r5 = new byte[r11]     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r17 = 512(0x200, float:7.175E-43)
            r0 = r17
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r15 = r0
            r14 = 0
            r7 = 0
            r16 = 0
        L_0x0058:
            int r14 = r9.read(r15)     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            if (r14 <= 0) goto L_0x0070
            java.lang.Thread r17 = java.lang.Thread.currentThread()     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            boolean r17 = r17.isInterrupted()     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            if (r17 == 0) goto L_0x00b4
            r17 = 0
            r0 = r17
            r1 = r20
            r1.mCanTaskRunFinish = r0     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
        L_0x0070:
            if (r9 == 0) goto L_0x0075
            r9.close()     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
        L_0x0075:
            r0 = r20
            com.yhiker.guid.NetDataFactory$OnGetBytesFrHttpFinishListener r0 = r0.mOnGetBytesFrHttpFinishListener     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r17 = r0
            if (r17 == 0) goto L_0x0099
            r0 = r20
            com.yhiker.guid.NetDataFactory$OnGetBytesFrHttpFinishListener r0 = r0.mOnGetBytesFrHttpFinishListener     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r17 = r0
            r0 = r20
            boolean r0 = r0.mCanTaskRunFinish     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r18 = r0
            r0 = r20
            android.os.Handler r0 = r0.pOnGetBytesFrHttpFinishListenerHandler     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r19 = r0
            r0 = r17
            r1 = r18
            r2 = r5
            r3 = r19
            r0.OnGetBytesFrHttpFinish(r1, r2, r3)     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
        L_0x0099:
            monitor-exit(r20)
            return
        L_0x009b:
            java.io.FileInputStream r10 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            java.io.File r17 = new java.io.File     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r0 = r20
            java.lang.String r0 = r0.mCurUrl     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r18 = r0
            r17.<init>(r18)     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r0 = r10
            r1 = r17
            r0.<init>(r1)     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            int r11 = r10.available()     // Catch:{ IOException -> 0x013d, InterruptedException -> 0x0133 }
            r9 = r10
            goto L_0x0044
        L_0x00b4:
            r0 = r20
            boolean r0 = r0.mbTaskContinue     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r17 = r0
            if (r17 != 0) goto L_0x00d8
            r17 = 0
            r0 = r17
            r1 = r20
            r1.mCanTaskRunFinish = r0     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            goto L_0x0070
        L_0x00c5:
            r17 = move-exception
            r8 = r17
        L_0x00c8:
            r8.printStackTrace()     // Catch:{ all -> 0x00d5 }
            java.lang.String r17 = "netdatafac-ConnError"
            java.lang.String r18 = r8.toString()     // Catch:{ all -> 0x00d5 }
            android.util.Log.e(r17, r18)     // Catch:{ all -> 0x00d5 }
            goto L_0x0099
        L_0x00d5:
            r17 = move-exception
        L_0x00d6:
            monitor-exit(r20)
            throw r17
        L_0x00d8:
            r17 = 0
            r0 = r15
            r1 = r17
            r2 = r5
            r3 = r7
            r4 = r14
            java.lang.System.arraycopy(r0, r1, r2, r3, r4)     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            int r7 = r7 + r14
            int r16 = r16 + 1
            r17 = 10
            r0 = r16
            r1 = r17
            if (r0 < r1) goto L_0x0058
            r0 = r20
            com.yhiker.guid.NetDataFactory$OnUpdatebufListener r0 = r0.mOnUpdatebufListener     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r17 = r0
            if (r17 == 0) goto L_0x0058
            r0 = r20
            com.yhiker.guid.NetDataFactory$OnUpdatebufListener r0 = r0.mOnUpdatebufListener     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r17 = r0
            int r18 = r7 * 100
            int r18 = r18 / r11
            r0 = r20
            android.os.Handler r0 = r0.pOnUpdatebufListenerHandler     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r19 = r0
            boolean r17 = r17.OnUpdatebuf(r18, r19)     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r0 = r17
            r1 = r20
            r1.mbTaskContinue = r0     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r0 = r20
            boolean r0 = r0.mbTaskContinue     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            r17 = r0
            if (r17 != 0) goto L_0x012a
            r17 = 0
            r0 = r17
            r1 = r20
            r1.mCanTaskRunFinish = r0     // Catch:{ IOException -> 0x00c5, InterruptedException -> 0x0122 }
            goto L_0x0070
        L_0x0122:
            r17 = move-exception
            r8 = r17
        L_0x0125:
            r8.printStackTrace()     // Catch:{ all -> 0x00d5 }
            goto L_0x0099
        L_0x012a:
            r16 = 0
            goto L_0x0058
        L_0x012e:
            r17 = move-exception
            r8 = r17
            r12 = r13
            goto L_0x0125
        L_0x0133:
            r17 = move-exception
            r8 = r17
            r9 = r10
            goto L_0x0125
        L_0x0138:
            r17 = move-exception
            r8 = r17
            r12 = r13
            goto L_0x00c8
        L_0x013d:
            r17 = move-exception
            r8 = r17
            r9 = r10
            goto L_0x00c8
        L_0x0142:
            r17 = move-exception
            r12 = r13
            goto L_0x00d6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.guid.NetDataFactory.doGetBytesFrHttpAsync():void");
    }

    public byte[] getBytesFrHttp(String url) {
        int readLen;
        byte[] imgData = null;
        try {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                conn.setDoInput(true);
                conn.connect();
                Thread.sleep(5);
                InputStream is = conn.getInputStream();
                int length = conn.getContentLength();
                if (-1 != length) {
                    imgData = new byte[length];
                    byte[] temp = new byte[512];
                    int destPos = 0;
                    while (!Thread.currentThread().isInterrupted() && (readLen = is.read(temp)) > 0) {
                        System.arraycopy(temp, 0, imgData, destPos, readLen);
                        destPos += readLen;
                    }
                }
            } catch (IOException e) {
                IOException e2 = e;
                e2.printStackTrace();
                Log.e("netdatafac-ConnError", e2.toString());
                return null;
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
            return imgData;
        } catch (MalformedURLException e4) {
            e4.printStackTrace();
            Log.e("netdatafac-URL", "error");
            return null;
        }
    }

    public void SetOnGetBytesFrHttpFinishListener(OnGetBytesFrHttpFinishListener l, Handler h) {
        this.mOnGetBytesFrHttpFinishListener = l;
        this.pOnGetBytesFrHttpFinishListenerHandler = h;
    }

    public void SetOnUpdatebufListener(OnUpdatebufListener l, Handler h) {
        this.mOnUpdatebufListener = l;
        this.pOnUpdatebufListenerHandler = h;
    }
}
