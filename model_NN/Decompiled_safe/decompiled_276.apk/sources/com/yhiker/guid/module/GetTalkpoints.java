package com.yhiker.guid.module;

import com.yhiker.oneByone.bo.TalkPointBo;
import com.yhiker.oneByone.module.TalkPoint;
import java.util.ArrayList;
import java.util.List;

public class GetTalkpoints {
    private static List<TalkPoint> talkPoints_instance = new ArrayList();

    private GetTalkpoints() {
    }

    public static TalkPoint isInbcCircle(int mapX, int mapY) {
        int bcSize = talkPoints_instance.size();
        for (int i = 0; i < bcSize; i++) {
            TalkPoint circle = talkPoints_instance.get(i);
            if (isInCircle(mapX, mapY, circle.getCenterX(), circle.getCenterY(), circle.getRedius())) {
                return circle;
            }
        }
        return null;
    }

    public static boolean isInCircle(int starX, int starY, int centerX, int centerY, int redius) {
        if (Math.hypot((double) (starX - centerX), (double) (starY - centerY)) <= ((double) redius)) {
            return true;
        }
        return false;
    }

    public static void parseTalkPointsInfoFromDB(String dbPath, String scenicCode) {
        talkPoints_instance = TalkPointBo.getTalkPointsByScenicCode(dbPath, scenicCode);
    }

    public static List<TalkPoint> getTalkPointList() {
        return talkPoints_instance;
    }
}
