package com.yhiker.guid.module;

import com.yhiker.guid.NetXMLDataFactory;
import com.yhiker.guid.parse.common.XmlParseCommon;
import com.yhiker.oneByone.bo.MapVectorParaBo;
import java.io.ByteArrayInputStream;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

public class GetParkDataInfo {
    private static ParkDataInfo parkDataInfo = null;

    public static ParkDataInfo getCurParkInfoObj() {
        return parkDataInfo;
    }

    /* JADX INFO: Multiple debug info for r4v1 java.lang.String: [D('netXMLDataFactory' com.yhiker.guid.NetXMLDataFactory), D('b3Str' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r12v8 java.lang.Double: [D('a1Str' java.lang.String), D('lgtx' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r12v9 java.lang.Double: [D('lgty' java.lang.Double), D('lgtx' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r12v10 java.lang.Double: [D('lgty' java.lang.Double), D('lgtz' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r12v11 java.lang.Double: [D('lgtz' java.lang.Double), D('latx' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r12v12 java.lang.Double: [D('latx' java.lang.Double), D('laty' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r12v13 java.lang.Double: [D('laty' java.lang.Double), D('latz' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r12v14 com.yhiker.guid.module.ParkDataInfo: [D('xmlurl' java.lang.String), D('latz' java.lang.Double)] */
    public static ParkDataInfo parseParkDataInfo(String xmlurl) {
        parkDataInfo = new ParkDataInfo();
        NetXMLDataFactory netXMLDataFactory = NetXMLDataFactory.getInstance();
        Document vectorPara_doc = netXMLDataFactory.getDocumentData(xmlurl);
        if (vectorPara_doc == null) {
            return null;
        }
        String scenicID = netXMLDataFactory.getTextNodeByTagName("ScenicID", vectorPara_doc);
        String widthStr = netXMLDataFactory.getTextNodeByTagName("Width", vectorPara_doc);
        String heightStr = netXMLDataFactory.getTextNodeByTagName("Height", vectorPara_doc);
        String widthNum = netXMLDataFactory.getTextNodeByTagName("SegmentWidthNum", vectorPara_doc);
        String heightNum = netXMLDataFactory.getTextNodeByTagName("SegmentHeightNum", vectorPara_doc);
        String a1Str = netXMLDataFactory.getTextNodeByTagName("A1", vectorPara_doc);
        String a2Str = netXMLDataFactory.getTextNodeByTagName("A2", vectorPara_doc);
        String a3Str = netXMLDataFactory.getTextNodeByTagName("A3", vectorPara_doc);
        String b1Str = netXMLDataFactory.getTextNodeByTagName("B1", vectorPara_doc);
        String b2Str = netXMLDataFactory.getTextNodeByTagName("B2", vectorPara_doc);
        String b3Str = netXMLDataFactory.getTextNodeByTagName("B3", vectorPara_doc);
        parkDataInfo.setId(scenicID);
        parkDataInfo.setDrawableWidth(Integer.parseInt(widthStr));
        parkDataInfo.setDrawableHeight(Integer.parseInt(heightStr));
        parkDataInfo.setSegmentWidthNum(Integer.parseInt(widthNum));
        parkDataInfo.setSegmentHeightNum(Integer.parseInt(heightNum));
        parkDataInfo.setLgtx(Double.valueOf(Double.parseDouble(a1Str)));
        parkDataInfo.setLgty(Double.valueOf(Double.parseDouble(a2Str)));
        parkDataInfo.setLgtz(Double.valueOf(Double.parseDouble(a3Str)));
        parkDataInfo.setLatx(Double.valueOf(Double.parseDouble(b1Str)));
        parkDataInfo.setLaty(Double.valueOf(Double.parseDouble(b2Str)));
        parkDataInfo.setLatz(Double.valueOf(Double.parseDouble(b3Str)));
        return parkDataInfo;
    }

    /* JADX INFO: Multiple debug info for r11v2 byte[]: [D('dataScenicpoints' byte[]), D('map' java.util.Map<java.lang.String, byte[]>)] */
    /* JADX INFO: Multiple debug info for r11v3 javax.xml.parsers.DocumentBuilderFactory: [D('dataScenicpoints' byte[]), D('dbf' javax.xml.parsers.DocumentBuilderFactory)] */
    /* JADX INFO: Multiple debug info for r1v2 java.lang.String: [D('xmlIs' java.io.InputStream), D('a3Str' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v2 java.lang.String: [D('vectorPara_doc' org.w3c.dom.Document), D('b3Str' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r11v14 java.lang.Double: [D('a1Str' java.lang.String), D('lgtx' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r11v15 java.lang.Double: [D('lgty' java.lang.Double), D('lgtx' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r11v16 java.lang.Double: [D('lgty' java.lang.Double), D('lgtz' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r11v17 java.lang.Double: [D('lgtz' java.lang.Double), D('latx' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r11v18 java.lang.Double: [D('latx' java.lang.Double), D('laty' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r11v19 java.lang.Double: [D('laty' java.lang.Double), D('latz' java.lang.Double)] */
    /* JADX INFO: Multiple debug info for r11v22 javax.xml.parsers.DocumentBuilder: [D('dbf' javax.xml.parsers.DocumentBuilderFactory), D('db' javax.xml.parsers.DocumentBuilder)] */
    /* JADX INFO: Multiple debug info for r11v23 org.w3c.dom.Document: [D('vectorPara_doc' org.w3c.dom.Document), D('db' javax.xml.parsers.DocumentBuilder)] */
    public static ParkDataInfo parseParkDataInfoFromDataZip1(Map<String, byte[]> map) {
        Document vectorPara_doc;
        parkDataInfo = new ParkDataInfo();
        try {
            vectorPara_doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(map.get(ParkInfoConf.WholePictureVectorParaXMLFileName)));
        } catch (Exception e) {
            e.printStackTrace();
            vectorPara_doc = null;
        }
        if (vectorPara_doc == null) {
            return null;
        }
        String textNodeByTagName = XmlParseCommon.getTextNodeByTagName("ScenicName", vectorPara_doc);
        String scenicID = XmlParseCommon.getTextNodeByTagName("ScenicID", vectorPara_doc);
        String widthStr = XmlParseCommon.getTextNodeByTagName("Width", vectorPara_doc);
        String heightStr = XmlParseCommon.getTextNodeByTagName("Height", vectorPara_doc);
        String widthNum = XmlParseCommon.getTextNodeByTagName("SegmentWidthNum", vectorPara_doc);
        String heightNum = XmlParseCommon.getTextNodeByTagName("SegmentHeightNum", vectorPara_doc);
        String a1Str = XmlParseCommon.getTextNodeByTagName("A1", vectorPara_doc);
        String a2Str = XmlParseCommon.getTextNodeByTagName("A2", vectorPara_doc);
        String a3Str = XmlParseCommon.getTextNodeByTagName("A3", vectorPara_doc);
        String b1Str = XmlParseCommon.getTextNodeByTagName("B1", vectorPara_doc);
        String b2Str = XmlParseCommon.getTextNodeByTagName("B2", vectorPara_doc);
        String b3Str = XmlParseCommon.getTextNodeByTagName("B3", vectorPara_doc);
        parkDataInfo.setId(scenicID);
        parkDataInfo.setDrawableWidth(Integer.parseInt(widthStr));
        parkDataInfo.setDrawableHeight(Integer.parseInt(heightStr));
        parkDataInfo.setSegmentWidthNum(Integer.parseInt(widthNum));
        parkDataInfo.setSegmentHeightNum(Integer.parseInt(heightNum));
        parkDataInfo.setLgtx(Double.valueOf(Double.parseDouble(a1Str)));
        parkDataInfo.setLgty(Double.valueOf(Double.parseDouble(a2Str)));
        parkDataInfo.setLgtz(Double.valueOf(Double.parseDouble(a3Str)));
        parkDataInfo.setLatx(Double.valueOf(Double.parseDouble(b1Str)));
        parkDataInfo.setLaty(Double.valueOf(Double.parseDouble(b2Str)));
        parkDataInfo.setLatz(Double.valueOf(Double.parseDouble(b3Str)));
        return parkDataInfo;
    }

    public static ParkDataInfo parseParkDataFromDB(String dbPath, String scenicCode) {
        parkDataInfo = MapVectorParaBo.getParkDataInfoByScenicCode(dbPath, scenicCode);
        return parkDataInfo;
    }
}
