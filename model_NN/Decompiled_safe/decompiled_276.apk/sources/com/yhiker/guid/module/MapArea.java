package com.yhiker.guid.module;

public class MapArea {
    /* access modifiers changed from: private */
    public static MapArea mapArea_instance = new MapArea();
    private BitmapForShow bitmapForShow = new BitmapForShow();
    private int displayHeight;
    private int displayWidth;
    private int layoutParamsX;
    private int layoutParamsY;
    private float mScale;
    private String maptilePicPath = "";
    private float maxScale = 1.0f;
    private float minScale;
    private int originalHeight;
    private int originalWidth;
    private int segmentHeightNum;
    private int segmentWidthNum;

    private MapArea() {
    }

    public static MapArea getInstance() {
        return mapArea_instance;
    }

    public float getmScale() {
        return this.mScale;
    }

    public String getMaptilePicPath() {
        return this.maptilePicPath;
    }

    public void setMaptilePicPath(String maptilePicPath2) {
        this.maptilePicPath = maptilePicPath2;
    }

    public int getSegmentWidthNum() {
        return this.segmentWidthNum;
    }

    public void setSegmentWidthNum(int segmentWidthNum2) {
        this.segmentWidthNum = segmentWidthNum2;
    }

    public int getSegmentHeightNum() {
        return this.segmentHeightNum;
    }

    public void setSegmentHeightNum(int segmentHeightNum2) {
        this.segmentHeightNum = segmentHeightNum2;
    }

    public int getDisplayWidth() {
        return this.displayWidth;
    }

    public void setDisplayWidth(int displayWidth2) {
        this.displayWidth = displayWidth2;
    }

    public int getDisplayHeight() {
        return this.displayHeight;
    }

    public void setDisplayHeight(int displayHeight2) {
        this.displayHeight = displayHeight2;
    }

    public void setmScale(float mScale2) {
        this.mScale = mScale2;
    }

    public int getOriginalWidth() {
        return this.originalWidth;
    }

    public void setOriginalWidth(int originalWidth2) {
        this.originalWidth = originalWidth2;
    }

    public int getOriginalHeight() {
        return this.originalHeight;
    }

    public void setOriginalHeight(int originalHeight2) {
        this.originalHeight = originalHeight2;
    }

    public int getMapImageWidth() {
        return Math.round(((float) this.originalWidth) * this.mScale);
    }

    public int getMapImageHeight() {
        return Math.round(((float) this.originalHeight) * this.mScale);
    }

    public boolean isScaleAvailable(float scale) {
        boolean canBeScaled = true;
        int scaledImageWidth = Math.round(((float) this.originalWidth) * this.mScale * scale);
        int scaledImageHeight = Math.round(((float) this.originalHeight) * this.mScale * scale);
        if (scaledImageWidth <= this.displayWidth) {
            canBeScaled = false;
        }
        if (scaledImageHeight <= this.displayHeight) {
            canBeScaled = false;
        }
        float wScale = this.mScale * scale;
        if (wScale > this.maxScale) {
            canBeScaled = false;
        }
        if (wScale >= this.minScale) {
            return canBeScaled;
        }
        return false;
    }

    public int getLayoutParamsX() {
        return this.layoutParamsX;
    }

    public void setLayoutParamsX(int layoutParamsX2) {
        this.layoutParamsX = layoutParamsX2;
    }

    public int getLayoutParamsY() {
        return this.layoutParamsY;
    }

    public void setLayoutParamsY(int layoutParamsY2) {
        this.layoutParamsY = layoutParamsY2;
    }

    public float getMaxScale() {
        return this.maxScale;
    }

    public void setMaxScale(float maxScale2) {
        this.maxScale = maxScale2;
    }

    public float getMinScale() {
        return this.minScale;
    }

    public void setMinScale(float minScale2) {
        this.minScale = minScale2;
    }

    public BitmapForShow getBitmapForShow() {
        return this.bitmapForShow;
    }

    public void setBitmapForShow(BitmapForShow bitmapForShow2) {
        this.bitmapForShow = bitmapForShow2;
    }

    public class BitmapForShow {
        private int beginXindex;
        private int beginYindex;
        private int endXindex;
        private int endYindex;
        private int segmentHeight;
        private int segmentWidth;

        public BitmapForShow() {
        }

        public int getBeginXindex() {
            return this.beginXindex;
        }

        public void setBeginXindex(int beginXindex2) {
            this.beginXindex = beginXindex2;
        }

        public int getBeginYindex() {
            return this.beginYindex;
        }

        public void setBeginYindex(int beginYindex2) {
            this.beginYindex = beginYindex2;
        }

        public int getEndXindex() {
            return this.endXindex;
        }

        public void setEndXindex(int endXindex2) {
            this.endXindex = endXindex2;
        }

        public int getEndYindex() {
            return this.endYindex;
        }

        public void setEndYindex(int endYindex2) {
            this.endYindex = endYindex2;
        }

        public int getSegmentWidth() {
            return this.segmentWidth;
        }

        public void setSegmentWidth(int segmentWidth2) {
            this.segmentWidth = segmentWidth2;
        }

        public int getSegmentHeight() {
            return this.segmentHeight;
        }

        public void setSegmentHeight(int segmentHeight2) {
            this.segmentHeight = segmentHeight2;
        }

        public int getBmLayoutParamsX() {
            return MapArea.mapArea_instance.getLayoutParamsX() + Math.round(((float) (this.beginXindex * this.segmentWidth)) * MapArea.mapArea_instance.getmScale());
        }

        public int getBmlayoutParamsY() {
            return MapArea.mapArea_instance.getLayoutParamsY() + Math.round(((float) (this.beginYindex * this.segmentHeight)) * MapArea.mapArea_instance.getmScale());
        }

        public boolean isBmChanged(int nbx, int nby, int nex, int ney) {
            boolean isChanged = false;
            if (this.beginXindex != nbx) {
                isChanged = true;
                this.beginXindex = nbx;
            }
            if (this.beginYindex != nby) {
                isChanged = true;
                this.beginYindex = nby;
            }
            if (this.endXindex != nex) {
                isChanged = true;
                this.endXindex = nex;
            }
            if (this.endYindex == ney) {
                return isChanged;
            }
            this.endYindex = ney;
            return true;
        }
    }
}
