package com.yhiker.guid.module;

public class parkInfoWithDistanse extends ParkInfo {
    double distance = -1.0d;

    public void setDistance(double in_distance) {
        this.distance = in_distance;
    }

    public double getDistance() {
        return this.distance;
    }
}
