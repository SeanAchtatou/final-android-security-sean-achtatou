package com.yhiker.guid.module;

public class ParkInfoConf {
    public static String BestRouteXMLFileName = "BestRoute.xml";
    public static String ScenicpointsXMLFileName = "ScenicPoint.xml";
    public static String SpecialPointsXMLFileName = "SpecialPoint.xml";
    public static String TalkpointsInfoXMLFileName = "TalkPoint.xml";
    public static String WholePictureVectorParaXMLFileName = "WholePictureVectorPara.xml";
}
