package com.yhiker.guid.module;

import java.util.List;

public interface ItemListFace {
    List<ItemListFace> getChildrenItem();

    int getId();

    String getName();

    String toString();
}
