package com.yhiker.guid.module;

public class ProvinceInfo {
    private int amount;
    private String id;
    private String introduce;
    private boolean isDirect;
    private String name;
    private String picpath;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public boolean isDirect() {
        return this.isDirect;
    }

    public void setDirect(boolean isDirect2) {
        this.isDirect = isDirect2;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount2) {
        this.amount = amount2;
    }

    public String getPicpath() {
        return this.picpath;
    }

    public void setPicpath(String picpath2) {
        this.picpath = picpath2;
    }

    public String getIntroduce() {
        return this.introduce;
    }

    public void setIntroduce(String introduce2) {
        this.introduce = introduce2;
    }
}
