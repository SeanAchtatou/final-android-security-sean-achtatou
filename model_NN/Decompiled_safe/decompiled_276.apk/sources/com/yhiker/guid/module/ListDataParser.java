package com.yhiker.guid.module;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.io.InputStream;

public class ListDataParser {
    public static Bitmap getBitmapFromFile(Context context, String fileName) {
        Bitmap image = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            image = BitmapFactory.decodeStream(is, null, options);
            is.close();
            return image;
        } catch (Exception e) {
            e.printStackTrace();
            return image;
        }
    }

    public static Drawable getDrawableFromFile(Context context, String fileName) {
        Bitmap image = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            image = BitmapFactory.decodeStream(is, null, options);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new BitmapDrawable(context.getResources(), image);
    }
}
