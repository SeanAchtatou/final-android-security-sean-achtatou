package com.yhiker.guid.module;

import com.yhiker.oneByone.bo.ScenicPointBo;
import com.yhiker.oneByone.module.ScenicPoint;
import java.util.ArrayList;
import java.util.List;

public class GetScenicpoints {
    private static List<ScenicPoint> scenicPoints_instance = new ArrayList();

    private GetScenicpoints() {
    }

    public static ScenicPoint getBCbyId(String bcIdStr) {
        int bcSize = scenicPoints_instance.size();
        for (int i = 0; i < bcSize; i++) {
            ScenicPoint circle = scenicPoints_instance.get(i);
            if (bcIdStr.equals(circle.getCode())) {
                return circle;
            }
        }
        return null;
    }

    public static void parseScenicpointsInfoFromDB(String dbPath, String scenicCode) {
        scenicPoints_instance = ScenicPointBo.getScenicPointsByScenicCode(dbPath, scenicCode);
    }

    public static List<ScenicPoint> getScenicPointList() {
        return scenicPoints_instance;
    }
}
