package com.yhiker.guid.module;

import com.yhiker.oneByone.bo.ScenicPointBo;
import com.yhiker.oneByone.module.SpecialPoint;
import java.util.ArrayList;
import java.util.List;

public class GetSpecialPoints {
    private static List<SpecialPoint> specialPoints_instance = new ArrayList();

    private GetSpecialPoints() {
    }

    public static List<SpecialPoint> getSpecialPointList() {
        return specialPoints_instance;
    }

    public static void parseSpecialpointsInfoFromDB(String dbPath, String scenicCode) {
        specialPoints_instance = ScenicPointBo.getSpecialPointsByScenicCode(dbPath, scenicCode);
    }
}
