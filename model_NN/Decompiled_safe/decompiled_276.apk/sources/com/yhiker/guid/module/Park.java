package com.yhiker.guid.module;

public class Park {
    private String commendWay_Path;
    private String dataRoot_Path;
    private String datazip_Path;
    private int heightNum;
    private String id;
    private String imgintro_Path;
    private float latitude;
    private float longitude;
    private String mapRes_Path;
    private String name;
    private String scenicPointXML_Path;
    private String specialPointXML_Path;
    private String talkPointXML_Path;
    private String textintro;
    private String vectorParaXML_Path;
    private int widthNum;

    public int getWidthNum() {
        return this.widthNum;
    }

    public void setWidthNum(int widthNum2) {
        this.widthNum = widthNum2;
    }

    public int getHeightNum() {
        return this.heightNum;
    }

    public void setHeightNum(int heightNum2) {
        this.heightNum = heightNum2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getDataRoot_Path() {
        return this.dataRoot_Path;
    }

    public void setDataRoot_Path(String dataRootPath) {
        this.dataRoot_Path = dataRootPath;
    }

    public String getDatazip_Path() {
        return this.datazip_Path;
    }

    public void setDatazip_Path(String datazipPath) {
        this.datazip_Path = datazipPath;
    }

    public String getVectorParaXML_Path() {
        return this.vectorParaXML_Path;
    }

    public void setVectorParaXML_Path(String vectorParaXMLPath) {
        this.vectorParaXML_Path = vectorParaXMLPath;
    }

    public String getScenicPointXML_Path() {
        return this.scenicPointXML_Path;
    }

    public void setScenicPointXML_Path(String scenicPointXMLPath) {
        this.scenicPointXML_Path = scenicPointXMLPath;
    }

    public String getTalkPointXML_Path() {
        return this.talkPointXML_Path;
    }

    public void setTalkPointXML_Path(String talkPointXMLPath) {
        this.talkPointXML_Path = talkPointXMLPath;
    }

    public String getSpecialPointXML_Path() {
        return this.specialPointXML_Path;
    }

    public void setSpecialPointXML_Path(String specialPointXMLPath) {
        this.specialPointXML_Path = specialPointXMLPath;
    }

    public String getMapRes_Path() {
        return this.mapRes_Path;
    }

    public void setMapRes_Path(String mapResPath) {
        this.mapRes_Path = mapResPath;
    }

    public String getImgintro_Path() {
        return this.imgintro_Path;
    }

    public void setImgintro_Path(String imgintroPath) {
        this.imgintro_Path = imgintroPath;
    }

    public String getCommendWay_Path() {
        return this.commendWay_Path;
    }

    public void setCommendWay_Path(String commendWayPath) {
        this.commendWay_Path = commendWayPath;
    }

    public String getTextintro() {
        return this.textintro;
    }

    public void setTextintro(String textintro2) {
        this.textintro = textintro2;
    }

    public float getLongitude() {
        return this.longitude;
    }

    public void setLongitude(float longitude2) {
        this.longitude = longitude2;
    }

    public float getLatitude() {
        return this.latitude;
    }

    public void setLatitude(float latitude2) {
        this.latitude = latitude2;
    }
}
