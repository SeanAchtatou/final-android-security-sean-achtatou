package com.yhiker.guid.module;

public class ListsourceInfo {

    /* renamed from: com  reason: collision with root package name */
    private String f0com;
    private String version;

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getCom() {
        return this.f0com;
    }

    public void setCom(String com2) {
        this.f0com = com2;
    }
}
