package com.yhiker.guid.module;

import java.util.List;

public class ParkDataInfo {
    private int SegmentHeightNum;
    private int SegmentWidthNum;
    private int drawableHeight;
    private int drawableWidth;
    private String id;
    private Double latx;
    private Double laty;
    private Double latz;
    private Double lgtx;
    private Double lgty;
    private Double lgtz;
    private List<String> segmentList;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public Double getLgtx() {
        return this.lgtx;
    }

    public void setLgtx(Double lgtx2) {
        this.lgtx = lgtx2;
    }

    public Double getLgty() {
        return this.lgty;
    }

    public void setLgty(Double lgty2) {
        this.lgty = lgty2;
    }

    public Double getLgtz() {
        return this.lgtz;
    }

    public void setLgtz(Double lgtz2) {
        this.lgtz = lgtz2;
    }

    public Double getLatx() {
        return this.latx;
    }

    public void setLatx(Double latx2) {
        this.latx = latx2;
    }

    public Double getLaty() {
        return this.laty;
    }

    public void setLaty(Double laty2) {
        this.laty = laty2;
    }

    public Double getLatz() {
        return this.latz;
    }

    public void setLatz(Double latz2) {
        this.latz = latz2;
    }

    public int getDrawableWidth() {
        return this.drawableWidth;
    }

    public void setDrawableWidth(int drawableWidth2) {
        this.drawableWidth = drawableWidth2;
    }

    public int getDrawableHeight() {
        return this.drawableHeight;
    }

    public void setDrawableHeight(int drawableHeight2) {
        this.drawableHeight = drawableHeight2;
    }

    public int getSegmentWidthNum() {
        return this.SegmentWidthNum;
    }

    public void setSegmentWidthNum(int segmentWidthNum) {
        this.SegmentWidthNum = segmentWidthNum;
    }

    public int getSegmentHeightNum() {
        return this.SegmentHeightNum;
    }

    public void setSegmentHeightNum(int segmentHeightNum) {
        this.SegmentHeightNum = segmentHeightNum;
    }

    public int getSegmentNum() {
        return this.SegmentWidthNum * this.SegmentHeightNum;
    }

    public List<String> getSegmentList() {
        return this.segmentList;
    }

    public void setSegmentList(List<String> segmentList2) {
        this.segmentList = segmentList2;
    }
}
