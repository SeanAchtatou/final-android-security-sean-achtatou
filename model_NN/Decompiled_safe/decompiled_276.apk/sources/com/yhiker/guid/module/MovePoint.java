package com.yhiker.guid.module;

import android.widget.ImageView;

public class MovePoint {
    private static final MovePoint movePoint_instance = new MovePoint();
    private boolean guidMode;
    private int mapX = -100;
    private int mapY = -100;
    private int moveHeight;
    private int moveWidth;
    private ImageView movepoint;

    private MovePoint() {
    }

    public static MovePoint getInstance() {
        return movePoint_instance;
    }

    public ImageView getMovepoint() {
        return this.movepoint;
    }

    public void setMovepoint(ImageView movepoint2) {
        this.movepoint = movepoint2;
    }

    public int getMoveWidth() {
        return this.moveWidth;
    }

    public void setMoveWidth(int moveWidth2) {
        this.moveWidth = moveWidth2;
    }

    public int getMoveHeight() {
        return this.moveHeight;
    }

    public void setMoveHeight(int moveHeight2) {
        this.moveHeight = moveHeight2;
    }

    public int getMapX() {
        return this.mapX;
    }

    public void setMapX(int mapX2) {
        this.mapX = mapX2;
    }

    public int getMapY() {
        return this.mapY;
    }

    public void setMapY(int mapY2) {
        this.mapY = mapY2;
    }

    public boolean isGuidMode() {
        return this.guidMode;
    }

    public void setGuidMode(boolean guidMode2) {
        this.guidMode = guidMode2;
    }

    public void claerme() {
        this.movepoint = null;
    }
}
