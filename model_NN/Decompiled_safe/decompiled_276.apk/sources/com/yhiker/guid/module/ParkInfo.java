package com.yhiker.guid.module;

public class ParkInfo {
    private String id;
    private String intro;
    private double lattd;
    private float level;
    private double longtd;
    private String name;
    private String picpath;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public float getLevel() {
        return this.level;
    }

    public void setLevel(float level2) {
        this.level = level2;
    }

    public String getPicpath() {
        return this.picpath;
    }

    public void setPicpath(String picpath2) {
        this.picpath = picpath2;
    }

    public double getLongtd() {
        return this.longtd;
    }

    public void setLongtd(double longtd2) {
        this.longtd = longtd2;
    }

    public double getLattd() {
        return this.lattd;
    }

    public void setLattd(double lattd2) {
        this.lattd = lattd2;
    }

    public String getIntro() {
        return this.intro;
    }

    public void setIntro(String intro2) {
        this.intro = intro2;
    }
}
