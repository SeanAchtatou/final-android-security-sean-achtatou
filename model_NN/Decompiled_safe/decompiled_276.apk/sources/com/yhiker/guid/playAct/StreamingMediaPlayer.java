package com.yhiker.guid.playAct;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class StreamingMediaPlayer {
    private static final int INTIAL_KB_BUFFER = 25;
    private Context context;
    private int counter = 0;
    /* access modifiers changed from: private */
    public File downloadingMediaFile;
    private final Handler handler = new Handler();
    private boolean isInterrupted;
    /* access modifiers changed from: private */
    public long mediaLengthInKb;
    private long mediaLengthInSeconds;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer;
    private ImageButton playButton;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    /* access modifiers changed from: private */
    public TextView textStreamed;
    /* access modifiers changed from: private */
    public int totalKbRead = 0;

    public StreamingMediaPlayer(Context context2, TextView textStreamed2, ImageButton playButton2, Button streamButton, ProgressBar progressBar2) {
        this.context = context2;
        this.textStreamed = textStreamed2;
        this.playButton = playButton2;
        this.progressBar = progressBar2;
    }

    public void startStreaming(final String mediaUrl, long mediaLengthInKb2, long mediaLengthInSeconds2) throws IOException {
        this.mediaLengthInKb = mediaLengthInKb2;
        this.mediaLengthInSeconds = mediaLengthInSeconds2;
        new Thread(new Runnable() {
            public void run() {
                try {
                    StreamingMediaPlayer.this.downloadAudioIncrement(mediaUrl);
                } catch (IOException e) {
                    Log.e(getClass().getName(), "Unable to initialize the MediaPlayer for fileUrl=" + mediaUrl, e);
                }
            }
        }).start();
    }

    public void downloadAudioIncrement(String mediaUrl) throws IOException {
        URLConnection cn = new URL(mediaUrl).openConnection();
        cn.connect();
        InputStream stream = cn.getInputStream();
        if (stream == null) {
            Log.e(getClass().getName(), "Unable to create InputStream for mediaUrl:" + mediaUrl);
        }
        this.downloadingMediaFile = new File(this.context.getCacheDir(), "downloadingMedia.dat");
        if (this.downloadingMediaFile.exists()) {
            this.downloadingMediaFile.delete();
        }
        FileOutputStream out = new FileOutputStream(this.downloadingMediaFile);
        byte[] buf = new byte[16384];
        int totalBytesRead = 0;
        int incrementalBytesRead = 0;
        do {
            int numread = stream.read(buf);
            if (numread <= 0) {
                break;
            }
            out.write(buf, 0, numread);
            totalBytesRead += numread;
            incrementalBytesRead += numread;
            this.totalKbRead = totalBytesRead / 1000;
            testMediaBuffer();
            fireDataLoadUpdate();
        } while (validateNotInterrupted());
        stream.close();
        if (validateNotInterrupted()) {
            fireDataFullyLoaded();
        }
    }

    private boolean validateNotInterrupted() {
        if (!this.isInterrupted) {
            return true;
        }
        if (this.mediaPlayer != null) {
            this.mediaPlayer.pause();
        }
        return false;
    }

    private void testMediaBuffer() {
        this.handler.post(new Runnable() {
            public void run() {
                if (StreamingMediaPlayer.this.mediaPlayer == null) {
                    if (StreamingMediaPlayer.this.totalKbRead >= 25) {
                        try {
                            StreamingMediaPlayer.this.startMediaPlayer();
                        } catch (Exception e) {
                            Log.e(getClass().getName(), "Error copying buffered conent.", e);
                        }
                    }
                } else if (StreamingMediaPlayer.this.mediaPlayer.getDuration() - StreamingMediaPlayer.this.mediaPlayer.getCurrentPosition() <= 1000) {
                    StreamingMediaPlayer.this.transferBufferToMediaPlayer();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void startMediaPlayer() {
        try {
            File cacheDir = this.context.getCacheDir();
            StringBuilder append = new StringBuilder().append("playingMedia");
            int i = this.counter;
            this.counter = i + 1;
            File bufferedFile = new File(cacheDir, append.append(i).append(".dat").toString());
            moveFile(this.downloadingMediaFile, bufferedFile);
            Log.e(getClass().getName(), "Buffered File path: " + bufferedFile.getAbsolutePath());
            Log.e(getClass().getName(), "Buffered File length: " + bufferedFile.length() + "");
            this.mediaPlayer = createMediaPlayer(bufferedFile);
            this.mediaPlayer.start();
            startPlayProgressUpdater();
            this.playButton.setEnabled(true);
        } catch (IOException e) {
            Log.e(getClass().getName(), "Error initializing the MediaPlayer.", e);
        }
    }

    private MediaPlayer createMediaPlayer(File mediaFile) throws IOException {
        MediaPlayer mPlayer = new MediaPlayer();
        mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e(getClass().getName(), "Error in MediaPlayer: (" + what + ") with extra (" + extra + ")");
                return false;
            }
        });
        mPlayer.setDataSource(new FileInputStream(mediaFile).getFD());
        mPlayer.prepare();
        return mPlayer;
    }

    /* access modifiers changed from: private */
    public void transferBufferToMediaPlayer() {
        try {
            boolean wasPlaying = this.mediaPlayer.isPlaying();
            int curPosition = this.mediaPlayer.getCurrentPosition();
            File oldBufferedFile = new File(this.context.getCacheDir(), "playingMedia" + this.counter + ".dat");
            File cacheDir = this.context.getCacheDir();
            StringBuilder append = new StringBuilder().append("playingMedia");
            int i = this.counter;
            this.counter = i + 1;
            File bufferedFile = new File(cacheDir, append.append(i).append(".dat").toString());
            bufferedFile.deleteOnExit();
            moveFile(this.downloadingMediaFile, bufferedFile);
            this.mediaPlayer.pause();
            this.mediaPlayer = createMediaPlayer(bufferedFile);
            this.mediaPlayer.seekTo(curPosition);
            boolean atEndOfFile = this.mediaPlayer.getDuration() - this.mediaPlayer.getCurrentPosition() <= 1000;
            if (wasPlaying || atEndOfFile) {
                this.mediaPlayer.start();
            }
            oldBufferedFile.delete();
        } catch (Exception e) {
            Log.e(getClass().getName(), "Error updating to newly loaded content.", e);
        }
    }

    private void fireDataLoadUpdate() {
        this.handler.post(new Runnable() {
            public void run() {
                StreamingMediaPlayer.this.textStreamed.setText(StreamingMediaPlayer.this.totalKbRead + " Kb read" + ",");
                StreamingMediaPlayer.this.progressBar.setSecondaryProgress((int) (100.0f * (((float) StreamingMediaPlayer.this.totalKbRead) / ((float) StreamingMediaPlayer.this.mediaLengthInKb))));
            }
        });
    }

    private void fireDataFullyLoaded() {
        this.handler.post(new Runnable() {
            public void run() {
                StreamingMediaPlayer.this.transferBufferToMediaPlayer();
                StreamingMediaPlayer.this.downloadingMediaFile.delete();
                StreamingMediaPlayer.this.textStreamed.setText("Audio full loaded: " + StreamingMediaPlayer.this.totalKbRead + " Kb read");
            }
        });
    }

    public MediaPlayer getMediaPlayer() {
        return this.mediaPlayer;
    }

    public void startPlayProgressUpdater() {
        this.progressBar.setProgress((int) (100.0f * ((((float) this.mediaPlayer.getCurrentPosition()) / 1000.0f) / ((float) this.mediaLengthInSeconds))));
        if (this.mediaPlayer.isPlaying()) {
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    StreamingMediaPlayer.this.startPlayProgressUpdater();
                }
            }, 1000);
        }
    }

    public void interrupt() {
        this.playButton.setEnabled(false);
        this.isInterrupted = true;
        validateNotInterrupted();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public void moveFile(File oldLocation, File newLocation) throws IOException {
        if (oldLocation.exists()) {
            BufferedInputStream reader = new BufferedInputStream(new FileInputStream(oldLocation));
            BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(newLocation, false));
            try {
                byte[] buff = new byte[8192];
                while (true) {
                    int numChars = reader.read(buff, 0, buff.length);
                    if (numChars == -1) {
                        break;
                    }
                    writer.write(buff, 0, numChars);
                }
                if (reader != null) {
                    try {
                        writer.close();
                        reader.close();
                    } catch (IOException e) {
                        Log.e(getClass().getName(), "Error closing files when transferring " + oldLocation.getPath() + " to " + newLocation.getPath());
                    }
                }
            } catch (IOException e2) {
                throw new IOException("IOException when transferring " + oldLocation.getPath() + " to " + newLocation.getPath());
            } catch (Throwable th) {
                if (reader != null) {
                    try {
                        writer.close();
                        reader.close();
                    } catch (IOException e3) {
                        Log.e(getClass().getName(), "Error closing files when transferring " + oldLocation.getPath() + " to " + newLocation.getPath());
                    }
                }
                throw th;
            }
        } else {
            throw new IOException("Old location does not exist when transferring " + oldLocation.getPath() + " to " + newLocation.getPath());
        }
    }
}
