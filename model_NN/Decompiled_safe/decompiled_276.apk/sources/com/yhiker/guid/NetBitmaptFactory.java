package com.yhiker.guid;

import android.graphics.Bitmap;
import android.os.Handler;
import java.lang.Thread;
import java.util.ArrayList;

public class NetBitmaptFactory {
    static final int MATRIXDATAMODE = 2;
    static final int SINGLEBITMAPMODE = 1;
    public static final String TAG = "netbfty-";
    private static NetBitmaptFactory netBitmapFactory_instance = new NetBitmaptFactory();
    Thread mCurTask = null;
    OnGetBitmapFrHttpListener mOnGetBitmapFrHttpListener = null;
    String mSglCurStrUrl = null;
    int mSglTaskCount = 0;
    Runnable mSglTaskRunnable = new Runnable() {
        public void run() {
            NetBitmaptFactory.this.doGetBitmapFrHttpAsync(NetBitmaptFactory.this.mSglCurStrUrl);
            NetBitmaptFactory.this.mSglTaskCount--;
        }
    };
    ArrayList<String> mStrUrlList = new ArrayList<>();
    ArrayList<Thread> mTaskList = new ArrayList<>();
    int mnWorkMode = 0;
    Handler pOnGetBitmapFrHttpListenerHandle = null;

    public interface OnGetBitmapFrHttpListener {
        void OnGetBitmapFrHttp(Bitmap bitmap, Handler handler);
    }

    private NetBitmaptFactory() {
    }

    public static NetBitmaptFactory getInstance() {
        return netBitmapFactory_instance;
    }

    public void SetOnGetBitmapFrHttpListener(OnGetBitmapFrHttpListener l, Handler h) {
        this.mOnGetBitmapFrHttpListener = l;
        this.pOnGetBitmapFrHttpListenerHandle = h;
    }

    /* access modifiers changed from: package-private */
    public void restet() {
    }

    public void getBitmapFrHttp(ArrayList<String> strUrlList, int i, int j) {
        this.mStrUrlList = strUrlList;
    }

    public void getMatrixDataFrHttp(String url) {
        this.mSglCurStrUrl = url;
    }

    public void getBitmapFrHttpAsync(String url) {
        this.mSglCurStrUrl = url;
        if (this.mSglTaskCount > 1 && this.mCurTask != null) {
            stopSglTask();
        }
        this.mSglTaskCount++;
        this.mCurTask = new Thread(this.mSglTaskRunnable);
        this.mCurTask.start();
    }

    /* access modifiers changed from: package-private */
    public void stopSglTask() {
        this.mCurTask.interrupt();
        do {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (Thread.State.TERMINATED == this.mCurTask.getState()) {
                return;
            }
        } while (this.mCurTask != null);
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [java.net.URLConnection] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doGetBitmapFrHttpAsync(java.lang.String r19) {
        /*
            r18 = this;
            r11 = 0
            r3 = 0
            r4 = 0
            r9 = 0
            java.net.URL r12 = new java.net.URL     // Catch:{ MalformedURLException -> 0x004f }
            r0 = r12
            r1 = r19
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x004f }
            r11 = r12
        L_0x000d:
            r15 = 5
            java.lang.Thread.sleep(r15)     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            java.net.URLConnection r5 = r11.openConnection()     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            r0 = r5
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            r4 = r0
            r15 = 1
            r4.setDoInput(r15)     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            r4.connect()     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            r15 = 5
            java.lang.Thread.sleep(r15)     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            java.io.InputStream r9 = r4.getInputStream()     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            int r10 = r4.getContentLength()     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            r15 = -1
            if (r15 == r10) goto L_0x0069
            byte[] r8 = new byte[r10]     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            r15 = 512(0x200, float:7.175E-43)
            byte[] r14 = new byte[r15]     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            r13 = 0
            r5 = 0
        L_0x0039:
            java.lang.Thread r15 = java.lang.Thread.currentThread()     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            boolean r15 = r15.isInterrupted()     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            if (r15 != 0) goto L_0x005c
            int r13 = r9.read(r14)     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            if (r13 <= 0) goto L_0x005c
            r15 = 0
            java.lang.System.arraycopy(r14, r15, r8, r5, r13)     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            int r5 = r5 + r13
            goto L_0x0039
        L_0x004f:
            r15 = move-exception
            r7 = r15
            r7.printStackTrace()
            java.lang.String r15 = "URL"
            java.lang.String r16 = "error"
            android.util.Log.e(r15, r16)
            goto L_0x000d
        L_0x005c:
            r15 = 0
            r0 = r8
            int r0 = r0.length     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
            r16 = r0
            r0 = r8
            r1 = r15
            r2 = r16
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeByteArray(r0, r1, r2)     // Catch:{ IOException -> 0x0092, InterruptedException -> 0x00c9 }
        L_0x0069:
            if (r9 == 0) goto L_0x006e
            r9.close()     // Catch:{ IOException -> 0x008d }
        L_0x006e:
            if (r4 == 0) goto L_0x0073
            r4.disconnect()
        L_0x0073:
            r0 = r18
            com.yhiker.guid.NetBitmaptFactory$OnGetBitmapFrHttpListener r0 = r0.mOnGetBitmapFrHttpListener
            r15 = r0
            if (r15 == 0) goto L_0x008c
            r0 = r18
            com.yhiker.guid.NetBitmaptFactory$OnGetBitmapFrHttpListener r0 = r0.mOnGetBitmapFrHttpListener
            r15 = r0
            r0 = r18
            android.os.Handler r0 = r0.pOnGetBitmapFrHttpListenerHandle
            r16 = r0
            r0 = r15
            r1 = r3
            r2 = r16
            r0.OnGetBitmapFrHttp(r1, r2)
        L_0x008c:
            return
        L_0x008d:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x006e
        L_0x0092:
            r15 = move-exception
            r6 = r15
            r6.printStackTrace()     // Catch:{ all -> 0x00f7 }
            java.lang.String r15 = "conn error"
            java.lang.String r16 = r6.toString()     // Catch:{ all -> 0x00f7 }
            android.util.Log.e(r15, r16)     // Catch:{ all -> 0x00f7 }
            if (r9 == 0) goto L_0x00a5
            r9.close()     // Catch:{ IOException -> 0x00c4 }
        L_0x00a5:
            if (r4 == 0) goto L_0x00aa
            r4.disconnect()
        L_0x00aa:
            r0 = r18
            com.yhiker.guid.NetBitmaptFactory$OnGetBitmapFrHttpListener r0 = r0.mOnGetBitmapFrHttpListener
            r15 = r0
            if (r15 == 0) goto L_0x008c
            r0 = r18
            com.yhiker.guid.NetBitmaptFactory$OnGetBitmapFrHttpListener r0 = r0.mOnGetBitmapFrHttpListener
            r15 = r0
            r0 = r18
            android.os.Handler r0 = r0.pOnGetBitmapFrHttpListenerHandle
            r16 = r0
            r0 = r15
            r1 = r3
            r2 = r16
            r0.OnGetBitmapFrHttp(r1, r2)
            goto L_0x008c
        L_0x00c4:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x00a5
        L_0x00c9:
            r15 = move-exception
            r6 = r15
            r6.printStackTrace()     // Catch:{ all -> 0x00f7 }
            if (r9 == 0) goto L_0x00d3
            r9.close()     // Catch:{ IOException -> 0x00f2 }
        L_0x00d3:
            if (r4 == 0) goto L_0x00d8
            r4.disconnect()
        L_0x00d8:
            r0 = r18
            com.yhiker.guid.NetBitmaptFactory$OnGetBitmapFrHttpListener r0 = r0.mOnGetBitmapFrHttpListener
            r15 = r0
            if (r15 == 0) goto L_0x008c
            r0 = r18
            com.yhiker.guid.NetBitmaptFactory$OnGetBitmapFrHttpListener r0 = r0.mOnGetBitmapFrHttpListener
            r15 = r0
            r0 = r18
            android.os.Handler r0 = r0.pOnGetBitmapFrHttpListenerHandle
            r16 = r0
            r0 = r15
            r1 = r3
            r2 = r16
            r0.OnGetBitmapFrHttp(r1, r2)
            goto L_0x008c
        L_0x00f2:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x00d3
        L_0x00f7:
            r15 = move-exception
            if (r9 == 0) goto L_0x00fd
            r9.close()     // Catch:{ IOException -> 0x011f }
        L_0x00fd:
            if (r4 == 0) goto L_0x0102
            r4.disconnect()
        L_0x0102:
            r0 = r18
            com.yhiker.guid.NetBitmaptFactory$OnGetBitmapFrHttpListener r0 = r0.mOnGetBitmapFrHttpListener
            r16 = r0
            if (r16 == 0) goto L_0x011e
            r0 = r18
            com.yhiker.guid.NetBitmaptFactory$OnGetBitmapFrHttpListener r0 = r0.mOnGetBitmapFrHttpListener
            r16 = r0
            r0 = r18
            android.os.Handler r0 = r0.pOnGetBitmapFrHttpListenerHandle
            r17 = r0
            r0 = r16
            r1 = r3
            r2 = r17
            r0.OnGetBitmapFrHttp(r1, r2)
        L_0x011e:
            throw r15
        L_0x011f:
            r6 = move-exception
            r6.printStackTrace()
            goto L_0x00fd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.guid.NetBitmaptFactory.doGetBitmapFrHttpAsync(java.lang.String):void");
    }

    /* JADX WARN: Type inference failed for: r14v7, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap getBitmapFrHttp(java.lang.String r17) {
        /*
            r16 = this;
            r10 = 0
            r2 = 0
            r3 = 0
            r8 = 0
            java.net.URL r11 = new java.net.URL     // Catch:{ MalformedURLException -> 0x003a }
            r0 = r11
            r1 = r17
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x003a }
            java.net.URLConnection r14 = r11.openConnection()     // Catch:{ IOException -> 0x0060 }
            r0 = r14
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0060 }
            r3 = r0
            r14 = 1
            r3.setDoInput(r14)     // Catch:{ IOException -> 0x0060 }
            r3.connect()     // Catch:{ IOException -> 0x0060 }
            java.io.InputStream r8 = r3.getInputStream()     // Catch:{ IOException -> 0x0060 }
            int r9 = r3.getContentLength()     // Catch:{ IOException -> 0x0060 }
            r14 = -1
            if (r14 == r9) goto L_0x004e
            byte[] r7 = new byte[r9]     // Catch:{ IOException -> 0x0060 }
            r14 = 512(0x200, float:7.175E-43)
            byte[] r13 = new byte[r14]     // Catch:{ IOException -> 0x0060 }
            r12 = 0
            r4 = 0
        L_0x002e:
            int r12 = r8.read(r13)     // Catch:{ IOException -> 0x0060 }
            if (r12 <= 0) goto L_0x0048
            r14 = 0
            java.lang.System.arraycopy(r13, r14, r7, r4, r12)     // Catch:{ IOException -> 0x0060 }
            int r4 = r4 + r12
            goto L_0x002e
        L_0x003a:
            r14 = move-exception
            r6 = r14
            r6.printStackTrace()
            java.lang.String r14 = "URL"
            java.lang.String r15 = "error"
            android.util.Log.e(r14, r15)
            r14 = 0
        L_0x0047:
            return r14
        L_0x0048:
            r14 = 0
            int r15 = r7.length     // Catch:{ IOException -> 0x0060 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeByteArray(r7, r14, r15)     // Catch:{ IOException -> 0x0060 }
        L_0x004e:
            if (r8 == 0) goto L_0x0053
            r8.close()     // Catch:{ IOException -> 0x005b }
        L_0x0053:
            if (r3 == 0) goto L_0x0058
            r3.disconnect()
        L_0x0058:
            r10 = r11
            r14 = r2
            goto L_0x0047
        L_0x005b:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0053
        L_0x0060:
            r14 = move-exception
            r5 = r14
            r5.printStackTrace()     // Catch:{ all -> 0x007f }
            java.lang.String r14 = "conn error"
            java.lang.String r15 = r5.toString()     // Catch:{ all -> 0x007f }
            android.util.Log.e(r14, r15)     // Catch:{ all -> 0x007f }
            r2 = 0
            if (r8 == 0) goto L_0x0074
            r8.close()     // Catch:{ IOException -> 0x007a }
        L_0x0074:
            if (r3 == 0) goto L_0x0058
            r3.disconnect()
            goto L_0x0058
        L_0x007a:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0074
        L_0x007f:
            r14 = move-exception
            if (r8 == 0) goto L_0x0085
            r8.close()     // Catch:{ IOException -> 0x008b }
        L_0x0085:
            if (r3 == 0) goto L_0x008a
            r3.disconnect()
        L_0x008a:
            throw r14
        L_0x008b:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.guid.NetBitmaptFactory.getBitmapFrHttp(java.lang.String):android.graphics.Bitmap");
    }
}
