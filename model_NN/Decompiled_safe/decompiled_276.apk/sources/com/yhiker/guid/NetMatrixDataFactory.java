package com.yhiker.guid;

import android.os.Handler;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class NetMatrixDataFactory {
    static final int MATRIXMODE = 2;
    static final int SINGLEMODE = 1;
    public static final String TAG = "netDataFty-";
    private static NetMatrixDataFactory netMatrixDataFactory_instance = new NetMatrixDataFactory();
    Thread mCurTask = null;
    String mMatrixMainUrl = null;
    String mMatrixSeparator = null;
    String mMatrixSuffix = null;
    int mMatrixXamount = 0;
    int mMatrixYamount = 0;
    int mMtrTaskCount = 0;
    Runnable mMtrTaskRunnable = new Runnable() {
        public void run() {
            NetMatrixDataFactory.this.doGetMatrixFrHttpAsync();
            NetMatrixDataFactory.this.mMtrTaskCount--;
        }
    };
    OnGetMatrixDataFrHttpChildFinishListener mOnGetMatrixDataFrHttpChildFinishListener = null;
    OnGetMatrixDataFrHttpFinishListener mOnGetMatrixDataFrHttpFinishListener = null;
    int mnWorkMode = 0;
    Handler pOnGetMatrixDataFrHttpChildFinishListenerHandler = null;
    Handler pOnGetMatrixDataFrHttpFinishListenerHandler = null;

    public interface OnGetMatrixDataFrHttpChildFinishListener {
        void OnGetMatrixDataFrHttpChildFinish(int i, int i2, byte[] bArr, Handler handler);
    }

    public interface OnGetMatrixDataFrHttpFinishListener {
        void OnGetMatrixDataFrHttpFinish(Handler handler);
    }

    private NetMatrixDataFactory() {
    }

    public static NetMatrixDataFactory getInstance() {
        return netMatrixDataFactory_instance;
    }

    public void SetOnGetMatrixDataFrHttpChildFinishListener(OnGetMatrixDataFrHttpChildFinishListener l, Handler h) {
        this.mOnGetMatrixDataFrHttpChildFinishListener = l;
        this.pOnGetMatrixDataFrHttpChildFinishListenerHandler = h;
    }

    public void SetOnGetMatrixDataFrHttpFinishListener(OnGetMatrixDataFrHttpFinishListener l, Handler h) {
        this.mOnGetMatrixDataFrHttpFinishListener = l;
        this.pOnGetMatrixDataFrHttpFinishListenerHandler = h;
    }

    /* access modifiers changed from: package-private */
    public void restet() {
    }

    /* access modifiers changed from: package-private */
    public void stopMtrTask() {
        this.mCurTask.interrupt();
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (Thread.State.TERMINATED == this.mCurTask.getState() && this.mCurTask == null) {
                return;
            }
        }
    }

    public void getMatrixFrHttpAsync(String mainurl, int x, int y, String separator, String suffix) {
        this.mMatrixMainUrl = mainurl;
        this.mMatrixXamount = x;
        this.mMatrixYamount = y;
        this.mMatrixSeparator = separator;
        this.mMatrixSuffix = suffix;
        if (this.mMtrTaskCount > 1 && this.mCurTask != null) {
            stopMtrTask();
        }
        this.mMtrTaskCount++;
        this.mCurTask = new Thread(this.mMtrTaskRunnable);
        this.mCurTask.start();
    }

    /* access modifiers changed from: private */
    public void doGetMatrixFrHttpAsync() {
        int i = 0;
        loop0:
        while (i < this.mMatrixYamount) {
            try {
                for (int j = 0; j < this.mMatrixXamount; j++) {
                    String dataPath = this.mMatrixMainUrl + j + this.mMatrixSeparator + i + this.mMatrixSuffix;
                    if (Thread.currentThread().isInterrupted()) {
                        break loop0;
                    }
                    byte[] data = getBytesFrHttp(dataPath);
                    if (!(data.length == 0 || this.mOnGetMatrixDataFrHttpChildFinishListener == null)) {
                        Thread.sleep(5);
                        this.mOnGetMatrixDataFrHttpChildFinishListener.OnGetMatrixDataFrHttpChildFinish(j, i, data, this.pOnGetMatrixDataFrHttpChildFinishListenerHandler);
                    }
                }
                i++;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
        if (this.mOnGetMatrixDataFrHttpFinishListener != null) {
            Thread.sleep(5);
            this.mOnGetMatrixDataFrHttpFinishListener.OnGetMatrixDataFrHttpFinish(this.pOnGetMatrixDataFrHttpFinishListenerHandler);
        }
    }

    private byte[] getBytesFrHttp(String url) {
        int readLen;
        byte[] imgData = null;
        try {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                conn.setDoInput(true);
                conn.connect();
                Thread.sleep(5);
                InputStream is = conn.getInputStream();
                int length = conn.getContentLength();
                if (-1 != length) {
                    imgData = new byte[length];
                    byte[] temp = new byte[512];
                    int destPos = 0;
                    while (!Thread.currentThread().isInterrupted() && (readLen = is.read(temp)) > 0) {
                        System.arraycopy(temp, 0, imgData, destPos, readLen);
                        destPos += readLen;
                    }
                }
            } catch (IOException e) {
                IOException e2 = e;
                e2.printStackTrace();
                Log.e("netDataFty-ConnError", e2.toString());
                return null;
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
            return imgData;
        } catch (MalformedURLException e4) {
            e4.printStackTrace();
            Log.e("netDataFty-URL", "error");
            return null;
        }
    }
}
