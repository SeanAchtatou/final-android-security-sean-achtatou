package com.yhiker.guid.parse.common;

import com.yhiker.guid.module.GetParkDataInfo;
import com.yhiker.guid.module.ParkDataInfo;

public class CoordinateTranslate {
    private static double latx = 0.0d;
    private static double laty = 0.0d;
    private static double latz = 0.0d;
    private static double lgtx = 0.0d;
    private static double lgty = 0.0d;
    private static double lgtz = 0.0d;

    public static ParkDataInfo findOriginTude(String dbPath, String scenicCode) {
        ParkDataInfo parkDataInfo = GetParkDataInfo.parseParkDataFromDB(dbPath, scenicCode);
        if (parkDataInfo == null) {
            return null;
        }
        lgtx = parkDataInfo.getLgtx().doubleValue();
        lgty = parkDataInfo.getLgty().doubleValue();
        lgtz = parkDataInfo.getLgtz().doubleValue();
        latx = parkDataInfo.getLatx().doubleValue();
        laty = parkDataInfo.getLaty().doubleValue();
        latz = parkDataInfo.getLatz().doubleValue();
        return parkDataInfo;
    }

    public static Double[] getCoordinateFrGps(Double moveLongitude, Double moveLatitude) {
        Double[] coordinate = new Double[2];
        Double D1 = moveLongitude;
        coordinate[0] = Double.valueOf(((((laty * D1.doubleValue()) - (lgty * moveLatitude.doubleValue())) - (laty * lgtz)) + (lgty * latz)) / ((lgtx * laty) - (latx * lgty)));
        coordinate[1] = Double.valueOf(((D1.doubleValue() - lgtz) - (lgtx * coordinate[0].doubleValue())) / lgty);
        return coordinate;
    }
}
