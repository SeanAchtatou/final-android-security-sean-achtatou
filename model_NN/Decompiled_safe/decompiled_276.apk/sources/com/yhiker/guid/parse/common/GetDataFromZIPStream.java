package com.yhiker.guid.parse.common;

import java.io.InputStream;
import java.util.Map;

public class GetDataFromZIPStream {
    Map<String, byte[]> dataMap;

    GetDataFromZIPStream(InputStream inputStream) throws Exception {
        this.dataMap = zip.unzipFileFromStream(inputStream);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public byte[] getDataByName(String name) {
        for (Map.Entry entry : this.dataMap.entrySet()) {
            if (entry.getKey().equals(name)) {
                return (byte[]) entry.getValue();
            }
        }
        return null;
    }
}
