package com.yhiker.guid.parse.common;

import android.util.Log;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class XmlParseCommon {
    public static XmlPullParser CreateXppForXml(String xmlPath) {
        XmlPullParser xpp = null;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            xpp = factory.newPullParser();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        try {
            xpp.setInput(new FileInputStream(xmlPath), "GB2312");
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (XmlPullParserException e3) {
            e3.printStackTrace();
        }
        return xpp;
    }

    public static String getAttrValueByName(String name, XmlPullParser xpp) {
        int attrCount = xpp.getAttributeCount();
        for (int i = 0; i < attrCount; i++) {
            if (name.equals(xpp.getAttributeName(i))) {
                return xpp.getAttributeValue(i);
            }
        }
        return "";
    }

    public static String getTextNodeByTagName(String tagName, Document doc) {
        NodeList nodelist = doc.getElementsByTagName(tagName);
        if (nodelist.getLength() > 0) {
            Element elt = (Element) nodelist.item(0);
            if (elt.hasChildNodes()) {
                return elt.getFirstChild().getNodeValue();
            }
            Log.e("XML", tagName + " tag hasn't children nodes!");
            return null;
        }
        Log.e("XML", tagName + " tag can't be found!");
        return null;
    }

    public static String getTextNodeByTagName(String tagName, Element elt_root) {
        NodeList nodelist = elt_root.getElementsByTagName(tagName);
        if (nodelist.getLength() > 0) {
            Element elt = (Element) nodelist.item(0);
            if (elt.hasChildNodes()) {
                return elt.getFirstChild().getNodeValue();
            }
            Log.e("XML", tagName + " tag hasn't children nodes!");
            return null;
        }
        Log.e("XML", tagName + " tag can't be found!");
        return null;
    }
}
