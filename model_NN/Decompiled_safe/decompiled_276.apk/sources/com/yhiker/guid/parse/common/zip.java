package com.yhiker.guid.parse.common;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class zip {
    public static Map<String, byte[]> unzipFileFromStream(InputStream is) throws Exception {
        Map<String, byte[]> result = new HashMap<>();
        ZipInputStream ziStream = new ZipInputStream(is);
        while (true) {
            ZipEntry zipEntry = ziStream.getNextEntry();
            if (zipEntry == null) {
                return result;
            }
            int zipsize = (int) zipEntry.getSize();
            byte[] temp = new byte[zipsize];
            byte[] beRead = new byte[zipsize];
            int nowpos = 0;
            while (true) {
                int beReadThisTime = ziStream.read(temp);
                if (-1 == beReadThisTime) {
                    break;
                }
                System.arraycopy(temp, 0, beRead, nowpos, beReadThisTime);
                nowpos += beReadThisTime;
                temp = new byte[zipsize];
            }
            result.put(zipEntry.getName(), beRead);
        }
    }

    /* JADX INFO: Multiple debug info for r1v4 java.lang.String[]: [D('nowpos' int), D('fileNames' java.lang.String[])] */
    public static Map<String, byte[]> unzipFileFromStream(InputStream is, String type) throws Exception {
        Map<String, byte[]> result = new HashMap<>();
        ZipInputStream ziStream = new ZipInputStream(is);
        while (true) {
            ZipEntry zipEntry = ziStream.getNextEntry();
            if (zipEntry == null) {
                return result;
            }
            if (zipEntry.getName().endsWith("." + type)) {
                int zipsize = (int) zipEntry.getSize();
                byte[] temp = new byte[zipsize];
                byte[] beRead = new byte[zipsize];
                int nowpos = 0;
                while (true) {
                    int beReadThisTime = ziStream.read(temp);
                    if (-1 == beReadThisTime) {
                        break;
                    }
                    System.arraycopy(temp, 0, beRead, nowpos, beReadThisTime);
                    nowpos += beReadThisTime;
                    temp = new byte[zipsize];
                }
                String[] fileNames = zipEntry.getName().split("_");
                String fileName = fileNames[0];
                if (fileNames.length > 1) {
                    fileName = fileNames[1];
                }
                result.put(fileName, beRead);
            }
        }
    }
}
