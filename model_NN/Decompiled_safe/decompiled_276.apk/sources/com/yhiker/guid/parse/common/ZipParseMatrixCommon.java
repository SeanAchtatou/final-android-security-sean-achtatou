package com.yhiker.guid.parse.common;

import android.os.Handler;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.Thread;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipParseMatrixCommon {
    public static final String TAG = "zipParser-";
    private static ZipParseMatrixCommon zipParseMatrixCommon_instance = new ZipParseMatrixCommon();
    private boolean mCanTaskRunFinish = true;
    Thread mCurTask = null;
    String mMatrixSeparator = null;
    String mMatrixSuffix = null;
    int mMatrixXamount = 0;
    int mMatrixYamount = 0;
    int mMtrTaskCount = 0;
    Runnable mMtrTaskRunnable = new Runnable() {
        public void run() {
            ZipParseMatrixCommon.this.doGetMatrixFrZipbufAsync();
            ZipParseMatrixCommon.this.mMtrTaskCount--;
        }
    };
    OnGetMatrixDataFrZipbufChildFinishListener mOnGetMatrixDataFrZipbufChildFinishListener = null;
    OnGetMatrixDataFrZipbufFinishListener mOnGetMatrixDataFrZipbufFinishListener = null;
    byte[] mZipbuf = null;
    int mZipbufLen = 0;
    private boolean mbContinue = true;
    int mnWorkMode = 0;
    Handler pOnGetMatrixDataFrZipbufChildFinishListenerHandler = null;
    Handler pOnGetMatrixDataFrZipbufFinishListenerHandler = null;

    public interface OnGetMatrixDataFrZipbufChildFinishListener {
        boolean OnGetMatrixDataFrZipbufChildFinish(int i, int i2, byte[] bArr, Handler handler);
    }

    public interface OnGetMatrixDataFrZipbufFinishListener {
        void OnGetMatrixDataFrZipbufFinish(boolean z, Handler handler);
    }

    private ZipParseMatrixCommon() {
    }

    public static ZipParseMatrixCommon getInstance() {
        return zipParseMatrixCommon_instance;
    }

    public void getMatrixDataFrZipbufAsync(byte[] zipbuf, int x, int y, String separator, String suffix) {
        this.mMatrixXamount = x;
        this.mMatrixYamount = y;
        this.mMatrixSeparator = separator;
        this.mMatrixSuffix = suffix;
        this.mZipbufLen = zipbuf.length;
        this.mZipbuf = zipbuf;
        if (this.mMtrTaskCount > 1 && this.mCurTask != null) {
            stopMtrTask();
        }
        this.mMtrTaskCount++;
        this.mCurTask = new Thread(this.mMtrTaskRunnable);
        this.mbContinue = true;
        this.mCanTaskRunFinish = true;
        this.mCurTask.start();
    }

    public void stopMtrTask() {
        if (this.mCurTask != null) {
            this.mCurTask.interrupt();
            do {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (Thread.State.TERMINATED == this.mCurTask.getState()) {
                    return;
                }
            } while (this.mCurTask != null);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void doGetMatrixFrZipbufAsync() {
        ZipEntry entry;
        try {
            ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(this.mZipbuf));
            while (true) {
                if (Thread.currentThread().isInterrupted() || (entry = zipInputStream.getNextEntry()) == null) {
                    break;
                } else if (!this.mbContinue) {
                    this.mCanTaskRunFinish = false;
                    break;
                } else {
                    String fileName = entry.getName();
                    if (fileName.endsWith(this.mMatrixSuffix)) {
                        long size = entry.getSize();
                        byte[] buf = new byte[((int) size)];
                        byte[] minibuf = new byte[((int) size)];
                        int rcount = 0;
                        while (true) {
                            int len = zipInputStream.read(minibuf);
                            if (-1 == len) {
                                break;
                            }
                            System.arraycopy(minibuf, 0, buf, rcount, len);
                            rcount += len;
                            minibuf = new byte[((int) size)];
                        }
                        int sepindex = fileName.indexOf("-");
                        int dotindex = fileName.indexOf(".");
                        int sep_index = fileName.indexOf("_");
                        int slashindex = sep_index;
                        if (sep_index > 0) {
                            String x = String.valueOf(fileName.substring(slashindex + 1, sepindex));
                            String y = String.valueOf(fileName.substring(sepindex + 1, dotindex));
                            if (!(buf.length == 0 || this.mOnGetMatrixDataFrZipbufChildFinishListener == null)) {
                                Thread.sleep(5);
                                this.mbContinue = this.mOnGetMatrixDataFrZipbufChildFinishListener.OnGetMatrixDataFrZipbufChildFinish(Integer.parseInt(x), Integer.parseInt(y), buf, this.pOnGetMatrixDataFrZipbufChildFinishListenerHandler);
                            }
                        }
                    }
                    zipInputStream.closeEntry();
                }
            }
            if (this.mOnGetMatrixDataFrZipbufFinishListener != null) {
                Thread.sleep(5);
                this.mOnGetMatrixDataFrZipbufFinishListener.OnGetMatrixDataFrZipbufFinish(this.mCanTaskRunFinish, this.pOnGetMatrixDataFrZipbufFinishListenerHandler);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return;
    }

    public void SetOnGetMatrixDataFrZipbufChildFinishListener(OnGetMatrixDataFrZipbufChildFinishListener l, Handler h) {
        this.mOnGetMatrixDataFrZipbufChildFinishListener = l;
        this.pOnGetMatrixDataFrZipbufChildFinishListenerHandler = h;
    }

    public void SetOnGetMatrixDataFrZipbufFinishListener(OnGetMatrixDataFrZipbufFinishListener l, Handler h) {
        this.mOnGetMatrixDataFrZipbufFinishListener = l;
        this.pOnGetMatrixDataFrZipbufFinishListenerHandler = h;
    }
}
