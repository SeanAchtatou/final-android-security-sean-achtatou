package com.yhiker.guid;

import android.util.Log;
import com.yhiker.config.GuideConfig;
import com.yhiker.guid.parse.common.zip;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class NetXMLDataFactory {
    private static NetXMLDataFactory netXMLDataFactory_instance = new NetXMLDataFactory();

    private NetXMLDataFactory() {
    }

    public static NetXMLDataFactory getInstance() {
        return netXMLDataFactory_instance;
    }

    public Map<String, byte[]> getDocumentData(InputStream input) {
        try {
            return zip.unzipFileFromStream(input);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Document getDocumentData(String url) {
        File localXmlFile = new File(GuideConfig.getInitDataDir() + "/yhiker/data" + url.substring(url.indexOf("/0086")));
        Log.i(getClass().getName(), "local xml path:" + GuideConfig.getInitDataDir() + "/yhiker/data" + url.substring(url.indexOf("/0086")));
        if (localXmlFile.exists()) {
            try {
                Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(localXmlFile);
                Log.i(getClass().getName(), "read local xml path:" + localXmlFile.getAbsolutePath());
                return doc;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                conn.setConnectTimeout(15000);
                conn.setDoInput(true);
                conn.connect();
                Document doc2 = null;
                try {
                    doc2 = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(conn.getInputStream());
                } catch (ParserConfigurationException e2) {
                    e2.printStackTrace();
                } catch (SAXException e3) {
                    e3.printStackTrace();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                return doc2;
            } catch (Exception e5) {
                Exception e6 = e5;
                e6.printStackTrace();
                Log.e("conn error", e6.toString());
                return null;
            }
        } catch (MalformedURLException e7) {
            e7.printStackTrace();
            Log.e(AlixDefine.URL, "error");
            return null;
        }
    }

    public String getTextNodeByTagName(String tagName, Document doc) {
        NodeList nodelist = doc.getElementsByTagName(tagName);
        if (nodelist.getLength() > 0) {
            Element elt = (Element) nodelist.item(0);
            if (elt.hasChildNodes()) {
                return elt.getFirstChild().getNodeValue();
            }
            Log.e("XML", tagName + " tag hasn't children nodes!");
            return null;
        }
        Log.e("XML", tagName + " tag can't be found!");
        return null;
    }

    public String getTextNodeByTagName(String tagName, Element elt_root) {
        NodeList nodelist = elt_root.getElementsByTagName(tagName);
        if (nodelist.getLength() > 0) {
            Element elt = (Element) nodelist.item(0);
            if (elt.hasChildNodes()) {
                return elt.getFirstChild().getNodeValue();
            }
            Log.e("XML", tagName + " tag hasn't children nodes!");
            return null;
        }
        Log.e("XML", tagName + " tag can't be found!");
        return null;
    }
}
