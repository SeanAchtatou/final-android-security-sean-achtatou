package com.yhiker.location;

public class LocationData {
    public double dAltitude;
    public double dBearing;
    public double dLatitude;
    public double dLongitude;
    public double dSpeed;
    public int nDay;
    public int nEasting;
    public int nFixMode;
    public int nHour;
    public int nMinute;
    public int nMonth;
    public int nNorthing;
    public int nQualityIndicator;
    public int nSatellites;
    public int nSecond;
    public long nTime;
    public int nYear;
}
