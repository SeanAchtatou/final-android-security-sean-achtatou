package com.yhiker.location;

public class SatelliteData {
    public int nAzimuth;
    public int nElevation;
    public short nMessageStatus;
    public int nSatelliteID;
    public int nSignal;
    public short nSvStatus;
}
