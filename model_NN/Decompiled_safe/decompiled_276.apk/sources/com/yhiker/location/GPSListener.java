package com.yhiker.location;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class GPSListener implements LocationListener {
    public static final int GPS_LOCATION_CHANGED = 9000;
    public static final int GPS_STATUS_AVAILABLE = 2;
    public static final int GPS_STATUS_CHANGED = 9001;
    public static final int GPS_STATUS_OUT_OF_SERVICE = 0;
    public static final int GPS_STATUS_TEMPORARILY_UNAVAILABLE = 1;
    private static GPSListener m_hInstance = new GPSListener();
    String locProvider;
    private Calendar m_Calendar = Calendar.getInstance();
    private LocationData m_Location = new LocationData();
    private LocationManager m_LocationManager = null;
    private List<SatelliteData> m_Statellites = new ArrayList();
    private List<SatelliteData> m_Statellites4Parsing = new ArrayList();
    private Handler m_hHandler = null;
    private int m_nStatus = -1;

    public static GPSListener GetInstance() {
        return m_hInstance;
    }

    public void Init(LocationManager locationManager, long nMinTime, long nMinDistance, Handler gpsHandler) {
        this.m_LocationManager = locationManager;
        this.m_hHandler = gpsHandler;
        try {
            if (this.m_LocationManager.isProviderEnabled("network")) {
                this.locProvider = "network";
            } else if (this.m_LocationManager.isProviderEnabled("gps")) {
                this.locProvider = "gps";
            }
            this.m_LocationManager.requestLocationUpdates(this.locProvider, nMinTime, (float) nMinDistance, this);
            this.m_nStatus = 0;
        } catch (IllegalArgumentException e) {
        }
    }

    public int GetStatus() {
        return this.m_nStatus;
    }

    public LocationData getLocation() {
        return this.m_Location;
    }

    public List<SatelliteData> getSatellites() {
        return this.m_Statellites;
    }

    public void RefreshLastKnownLocation() {
        ParseLocation(this.m_LocationManager.getLastKnownLocation(this.locProvider));
    }

    public void onLocationChanged(Location location) {
        if (ParseLocation(location)) {
            this.m_nStatus = 2;
        } else {
            this.m_nStatus = 0;
        }
        if (this.m_hHandler != null) {
            Message msg = this.m_hHandler.obtainMessage(9000);
            msg.arg1 = this.m_nStatus;
            this.m_hHandler.sendMessage(msg);
        }
    }

    private boolean ParseLocation(Location location) {
        if (location != null) {
            this.m_Statellites4Parsing.removeAll(this.m_Statellites4Parsing);
            Bundle bundle = location.getExtras();
            int nSatelliteCount = 0;
            if (bundle != null) {
                nSatelliteCount = bundle.getInt("NumSatellite", 0);
                for (int i = 0; i < nSatelliteCount; i++) {
                    SatelliteData satellite = new SatelliteData();
                    satellite.nSatelliteID = bundle.getInt("SatelliteID" + i, 0);
                    satellite.nSignal = (short) bundle.getInt("SignalStrength" + i, 0);
                    satellite.nAzimuth = bundle.getInt("Azimuth" + i, 0);
                    satellite.nElevation = bundle.getInt("ElevationAngle" + i, 0);
                    this.m_Statellites4Parsing.add(satellite);
                }
            }
            this.m_Statellites = this.m_Statellites4Parsing;
            if (nSatelliteCount > 3) {
                this.m_Location.nFixMode = 1;
            } else {
                this.m_Location.nFixMode = 0;
            }
            this.m_Location.dAltitude = location.getAltitude();
            this.m_Location.dLongitude = location.getLongitude();
            this.m_Location.dLatitude = location.getLatitude();
            this.m_Location.dBearing = (double) location.getBearing();
            this.m_Location.dSpeed = (double) location.getSpeed();
            this.m_Location.nTime = location.getTime();
            if (this.m_Location.nTime > 0) {
                this.m_Calendar.setTimeInMillis(this.m_Location.nTime);
                this.m_Location.nYear = this.m_Calendar.get(1);
                this.m_Location.nMonth = this.m_Calendar.get(2) + 1;
                this.m_Location.nDay = this.m_Calendar.get(5);
                this.m_Location.nHour = this.m_Calendar.get(11);
                this.m_Location.nMinute = this.m_Calendar.get(12);
                this.m_Location.nSecond = this.m_Calendar.get(13);
            }
            return true;
        }
        this.m_Location.nFixMode = 0;
        this.m_Location.dAltitude = 0.0d;
        this.m_Location.dLongitude = 0.0d;
        this.m_Location.dLatitude = 0.0d;
        this.m_Location.dBearing = 0.0d;
        this.m_Location.dSpeed = 0.0d;
        this.m_Location.nTime = 0;
        this.m_Location.nYear = 0;
        this.m_Location.nMonth = 0;
        this.m_Location.nDay = 0;
        this.m_Location.nHour = 0;
        this.m_Location.nMinute = 0;
        this.m_Location.nSecond = 0;
        this.m_Statellites.removeAll(this.m_Statellites);
        return false;
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case 0:
                this.m_nStatus = 0;
                if (this.m_hHandler != null) {
                    Message msg = this.m_hHandler.obtainMessage(9001);
                    msg.arg1 = this.m_nStatus;
                    this.m_hHandler.sendMessage(msg);
                    return;
                }
                return;
            case 1:
                this.m_nStatus = 1;
                if (this.m_hHandler != null) {
                    Message msg2 = this.m_hHandler.obtainMessage(9001);
                    msg2.arg1 = this.m_nStatus;
                    this.m_hHandler.sendMessage(msg2);
                    return;
                }
                return;
            case 2:
                this.m_nStatus = 2;
                if (this.m_hHandler != null) {
                    Message msg3 = this.m_hHandler.obtainMessage(9001);
                    msg3.arg1 = this.m_nStatus;
                    this.m_hHandler.sendMessage(msg3);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
