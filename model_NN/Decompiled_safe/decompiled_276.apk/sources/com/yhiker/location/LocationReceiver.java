package com.yhiker.location;

import java.util.List;

public abstract class LocationReceiver {
    public abstract int GetStatus();

    public abstract void RefreshLastKnownLocation();

    public abstract LocationData getLocation();

    public abstract List<SatelliteData> getSatellites();
}
