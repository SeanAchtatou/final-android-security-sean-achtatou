package com.yhiker.location;

import android.location.LocationManager;
import android.os.Handler;
import java.util.List;

public class GPSReceiver extends LocationReceiver {
    public static final int GPS_LOCATION_CHANGED = 9000;
    public static final int GPS_STATUS_AVAILABLE = 2;
    public static final int GPS_STATUS_CHANGED = 9001;
    public static final int GPS_STATUS_OUT_OF_SERVICE = 0;
    public static final int GPS_STATUS_TEMPORARILY_UNAVAILABLE = 1;
    private static GPSReceiver m_hInstance = new GPSReceiver();
    private GPSListener m_LocationListener;

    public void Init(LocationManager locationManager, long nMinTime, long nMinDistance, Handler gpsHandler) {
        if (this.m_LocationListener == null) {
            this.m_LocationListener = GPSListener.GetInstance();
        }
        if (this.m_LocationListener != null) {
            this.m_LocationListener.Init(locationManager, nMinTime, nMinDistance, gpsHandler);
        }
    }

    public int GetStatus() {
        if (this.m_LocationListener != null) {
            return this.m_LocationListener.GetStatus();
        }
        return 0;
    }

    public LocationData getLocation() {
        if (this.m_LocationListener != null) {
            return this.m_LocationListener.getLocation();
        }
        return null;
    }

    public List<SatelliteData> getSatellites() {
        if (this.m_LocationListener != null) {
            return this.m_LocationListener.getSatellites();
        }
        return null;
    }

    public void RefreshLastKnownLocation() {
        if (this.m_LocationListener != null) {
            this.m_LocationListener.RefreshLastKnownLocation();
        }
    }

    private GPSReceiver() {
        this.m_LocationListener = null;
        this.m_LocationListener = GPSListener.GetInstance();
    }

    public static GPSReceiver GetInstance() {
        return m_hInstance;
    }
}
