package com.yhiker.pay;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

public class NetworkManager {
    static final String TAG = "NetworkManager";
    private int connectTimeout = 30000;
    Context mContext;
    Proxy mProxy = null;
    private int readTimeout = 30000;

    public NetworkManager(Context context) {
        this.mContext = context;
        setDefaultHostnameVerifier();
    }

    public void detectProxy() {
        NetworkInfo ni = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (ni != null && ni.isAvailable() && ni.getType() == 0) {
            String proxyHost = android.net.Proxy.getDefaultHost();
            int port = android.net.Proxy.getDefaultPort();
            if (proxyHost != null) {
                this.mProxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, port));
            }
        }
    }

    private void setDefaultHostnameVerifier() {
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
    }

    /* JADX WARN: Type inference failed for: r9v5, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r9v12, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String SendAndWaitResponse(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            r12.detectProxy()
            r7 = 0
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            org.apache.http.message.BasicNameValuePair r9 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r10 = "requestData"
            r9.<init>(r10, r13)
            r6.add(r9)
            r3 = 0
            org.apache.http.client.entity.UrlEncodedFormEntity r5 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ IOException -> 0x007d }
            java.lang.String r9 = "utf-8"
            r5.<init>(r6, r9)     // Catch:{ IOException -> 0x007d }
            java.net.URL r8 = new java.net.URL     // Catch:{ IOException -> 0x007d }
            r8.<init>(r14)     // Catch:{ IOException -> 0x007d }
            java.net.Proxy r9 = r12.mProxy     // Catch:{ IOException -> 0x007d }
            if (r9 == 0) goto L_0x0074
            java.net.Proxy r9 = r12.mProxy     // Catch:{ IOException -> 0x007d }
            java.net.URLConnection r9 = r8.openConnection(r9)     // Catch:{ IOException -> 0x007d }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x007d }
            r3 = r0
        L_0x002e:
            int r9 = r12.connectTimeout     // Catch:{ IOException -> 0x007d }
            r3.setConnectTimeout(r9)     // Catch:{ IOException -> 0x007d }
            int r9 = r12.readTimeout     // Catch:{ IOException -> 0x007d }
            r3.setReadTimeout(r9)     // Catch:{ IOException -> 0x007d }
            r9 = 1
            r3.setDoOutput(r9)     // Catch:{ IOException -> 0x007d }
            java.lang.String r9 = "Content-type"
            java.lang.String r10 = "application/x-www-form-urlencoded;charset=utf-8"
            r3.addRequestProperty(r9, r10)     // Catch:{ IOException -> 0x007d }
            r3.connect()     // Catch:{ IOException -> 0x007d }
            java.io.OutputStream r4 = r3.getOutputStream()     // Catch:{ IOException -> 0x007d }
            r5.writeTo(r4)     // Catch:{ IOException -> 0x007d }
            r4.flush()     // Catch:{ IOException -> 0x007d }
            java.io.InputStream r1 = r3.getInputStream()     // Catch:{ IOException -> 0x007d }
            java.lang.String r7 = com.yhiker.pay.BaseHelper.convertStreamToString(r1)     // Catch:{ IOException -> 0x007d }
            java.lang.String r9 = "NetworkManager"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x007d }
            r10.<init>()     // Catch:{ IOException -> 0x007d }
            java.lang.String r11 = "response "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ IOException -> 0x007d }
            java.lang.StringBuilder r10 = r10.append(r7)     // Catch:{ IOException -> 0x007d }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x007d }
            com.yhiker.pay.BaseHelper.log(r9, r10)     // Catch:{ IOException -> 0x007d }
        L_0x0070:
            r3.disconnect()
            return r7
        L_0x0074:
            java.net.URLConnection r9 = r8.openConnection()     // Catch:{ IOException -> 0x007d }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x007d }
            r3 = r0
            goto L_0x002e
        L_0x007d:
            r9 = move-exception
            r2 = r9
            r2.printStackTrace()     // Catch:{ all -> 0x0083 }
            goto L_0x0070
        L_0x0083:
            r9 = move-exception
            r3.disconnect()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.yhiker.pay.NetworkManager.SendAndWaitResponse(java.lang.String, java.lang.String):java.lang.String");
    }

    public boolean urlDownloadToFile(Context context, String strurl, String path) {
        HttpURLConnection conn;
        detectProxy();
        try {
            URL url = new URL(strurl);
            if (this.mProxy != null) {
                conn = (HttpURLConnection) url.openConnection(this.mProxy);
            } else {
                conn = (HttpURLConnection) url.openConnection();
            }
            conn.setConnectTimeout(this.connectTimeout);
            conn.setReadTimeout(this.readTimeout);
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            File file = new File(path);
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            byte[] temp = new byte[1024];
            while (true) {
                int i = is.read(temp);
                if (i > 0) {
                    fos.write(temp, 0, i);
                } else {
                    fos.close();
                    is.close();
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
