package com.yhiker.pay;

import org.json.JSONObject;

public class ResultChecker {
    public static final int RESULT_CHECK_SIGN_FAILED = 1;
    public static final int RESULT_CHECK_SIGN_SUCCEED = 2;
    public static final int RESULT_INVALID_PARAM = 0;
    String mContent;

    public ResultChecker(String content) {
        this.mContent = content;
    }

    /* access modifiers changed from: package-private */
    public String getSuccess() {
        try {
            String result = BaseHelper.string2JSON(this.mContent, ";").getString("result");
            return BaseHelper.string2JSON(result.substring(1, result.length() - 1), AlixDefine.split).getString("success").replace("\"", "");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int checkSign() {
        try {
            String result = BaseHelper.string2JSON(this.mContent, ";").getString("result");
            String result2 = result.substring(1, result.length() - 1);
            String signContent = result2.substring(0, result2.indexOf("&sign_type="));
            JSONObject objResult = BaseHelper.string2JSON(result2, AlixDefine.split);
            String signType = objResult.getString(AlixDefine.sign_type).replace("\"", "");
            String sign = objResult.getString(AlixDefine.sign).replace("\"", "");
            if (!signType.equalsIgnoreCase("RSA") || Rsa.doCheck(signContent, sign, PartnerConfig.RSA_ALIPAY_PUBLIC)) {
                return 2;
            }
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isPayOk() {
        if (!getSuccess().equalsIgnoreCase("true") || checkSign() != 2) {
            return false;
        }
        return true;
    }
}
