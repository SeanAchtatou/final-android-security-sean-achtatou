package com.yhiker.pay;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import com.alipay.android.app.IAlixPay;
import com.alipay.android.app.IRemoteServiceCallback;

public class MobileSecurePayer {
    static String TAG = "MobileSecurePayer";
    Integer lock = 0;
    Activity mActivity = null;
    IAlixPay mAlixPay = null;
    /* access modifiers changed from: private */
    public ServiceConnection mAlixPayConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            synchronized (MobileSecurePayer.this.lock) {
                MobileSecurePayer.this.mAlixPay = IAlixPay.Stub.asInterface(service);
                MobileSecurePayer.this.lock.notify();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            MobileSecurePayer.this.mAlixPay = null;
        }
    };
    /* access modifiers changed from: private */
    public IRemoteServiceCallback mCallback = new IRemoteServiceCallback.Stub() {
        public void startActivity(String packageName, String className, int iCallingPid, Bundle bundle) throws RemoteException {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            if (bundle == null) {
                bundle = new Bundle();
            }
            try {
                bundle.putInt("CallingPid", iCallingPid);
                intent.putExtras(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            intent.setClassName(packageName, className);
            MobileSecurePayer.this.mActivity.startActivity(intent);
        }
    };
    boolean mbPaying = false;

    public boolean pay(final String strOrderInfo, final Handler callback, final int myWhat, Activity activity) {
        if (this.mbPaying) {
            return false;
        }
        this.mbPaying = true;
        this.mActivity = activity;
        if (this.mAlixPay == null) {
            this.mActivity.bindService(new Intent(IAlixPay.class.getName()), this.mAlixPayConnection, 1);
        }
        new Thread(new Runnable() {
            public void run() {
                try {
                    synchronized (MobileSecurePayer.this.lock) {
                        if (MobileSecurePayer.this.mAlixPay == null) {
                            MobileSecurePayer.this.lock.wait();
                        }
                    }
                    MobileSecurePayer.this.mAlixPay.registerCallback(MobileSecurePayer.this.mCallback);
                    String strRet = MobileSecurePayer.this.mAlixPay.Pay(strOrderInfo);
                    BaseHelper.log(MobileSecurePayer.TAG, "After Pay: " + strRet);
                    MobileSecurePayer.this.mbPaying = false;
                    MobileSecurePayer.this.mAlixPay.unregisterCallback(MobileSecurePayer.this.mCallback);
                    MobileSecurePayer.this.mActivity.unbindService(MobileSecurePayer.this.mAlixPayConnection);
                    Message msg = new Message();
                    msg.what = myWhat;
                    msg.obj = strRet;
                    callback.sendMessage(msg);
                } catch (Exception e) {
                    Exception e2 = e;
                    e2.printStackTrace();
                    Message msg2 = new Message();
                    msg2.what = myWhat;
                    msg2.obj = e2.toString();
                    callback.sendMessage(msg2);
                }
            }
        }).start();
        return true;
    }
}
