package com.yhiker.pay;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Handler;
import android.widget.Toast;
import com.yhiker.config.GuideConfig;
import com.yhiker.playmate.R;
import com.yhiker.util.DialogUtil;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class PayService {
    private static Map<String, String> citys = new HashMap();
    private static String deviceId;
    private static Map<String, String> directCitys = new HashMap();
    private static Map<String, String> freePark = new HashMap();
    public static int freeTotal = 2;

    public static void setDeviceId(String sdeviceId) {
        deviceId = sdeviceId;
    }

    public static String getDeviceId() {
        return deviceId;
    }

    /* JADX INFO: Multiple debug info for r0v1 boolean: [D('isMobile_spExist' boolean), D('mspHelper' com.yhiker.pay.MobileSecurePayHelper)] */
    /* JADX INFO: Multiple debug info for r3v2 java.lang.String: [D('orderInfo' java.lang.String), D('outTradeNo' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v1 java.lang.String: [D('signType' java.lang.String), D('subject' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v2 java.lang.String: [D('signType' java.lang.String), D('strsign' java.lang.String)] */
    public static void aliPay(Activity activity, Handler mHandler, String outTradeNo, String subject, String body, String price) {
        if (new MobileSecurePayHelper(activity).detectMobile_sp()) {
            if (!checkInfo()) {
                DialogUtil.showDialog(activity, "提示", "缺少partner或者seller，请在src/com/alipay/android/appDemo4/PartnerConfig.java中增加。", R.drawable.infoicon);
                return;
            }
            try {
                String outTradeNo2 = getOrderInfo(outTradeNo, subject, body, price);
                new MobileSecurePayer().pay(outTradeNo2 + "&sign=" + "\"" + URLEncoder.encode(sign(getSignType(), outTradeNo2)) + "\"" + AlixDefine.split + getSignType(), mHandler, 1, activity);
            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(activity, (int) R.string.remote_call_failed, 0).show();
            }
        }
    }

    static class AlixOnCancelListener implements DialogInterface.OnCancelListener {
        Activity mcontext;

        AlixOnCancelListener(Activity context) {
            this.mcontext = context;
        }

        public void onCancel(DialogInterface dialog) {
            this.mcontext.onKeyDown(4, null);
        }
    }

    public static boolean checkInfo() {
        if ("2088201529003297" == 0 || "2088201529003297".length() <= 0 || "2088201529003297" == 0 || "2088201529003297".length() <= 0) {
            return false;
        }
        return true;
    }

    public static String sign(String signType, String content) {
        return Rsa.sign(content, PartnerConfig.RSA_PRIVATE);
    }

    public static String getSignType() {
        return "sign_type=\"RSA\"";
    }

    static String getCharset() {
        return "charset=\"utf-8\"";
    }

    public static String getOutTradeNo(String pre) {
        return pre + "-" + (new SimpleDateFormat("MMddHHmmss").format(new Date()) + new Random().nextInt()).substring(0, 15);
    }

    public static String getOrderInfo(String outTradeNo, String subject, String body, String price) {
        return ((((((((((("partner=\"2088201529003297\"" + AlixDefine.split) + "seller=\"2088201529003297\"") + AlixDefine.split) + "out_trade_no=\"" + outTradeNo + "\"") + AlixDefine.split) + "subject=\"" + subject + "\"") + AlixDefine.split) + "body=\"" + body + "\"") + AlixDefine.split) + "total_fee=\"" + price + "\"") + AlixDefine.split) + "notify_url=\"http://notify.java.jpxx.org/index.jsp\"";
    }

    /* JADX INFO: Multiple debug info for r6v2 java.lang.String: [D('deviceId' java.lang.String), D('param' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v5 com.yhiker.pay.NetXMLDataFactory: [D('url' java.lang.String), D('netXMLDataFactory' com.yhiker.pay.NetXMLDataFactory)] */
    /* JADX INFO: Multiple debug info for r6v7 boolean: [D('isSuccessed' boolean), D('netXMLDataFactory' com.yhiker.pay.NetXMLDataFactory)] */
    /* JADX INFO: Multiple debug info for r0v5 org.w3c.dom.NodeList: [D('park_doc' org.w3c.dom.Document), D('directCityslist' org.w3c.dom.NodeList)] */
    /* JADX INFO: Multiple debug info for r6v20 int: [D('cityId' java.lang.String), D('i' int)] */
    /* JADX INFO: Multiple debug info for r6v22 org.w3c.dom.Element: [D('elt' org.w3c.dom.Element), D('cityslist' org.w3c.dom.NodeList)] */
    /* JADX INFO: Multiple debug info for r6v27 int: [D('i' int), D('directCityId' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v12 org.w3c.dom.Element: [D('freeParkslist' org.w3c.dom.NodeList), D('elt' org.w3c.dom.Element)] */
    public static boolean authorList(String deviceId2) {
        NodeList ids;
        NodeList ids2;
        NodeList ids3;
        String param = GuideConfig.URL_PARK_CITY_INTO + ("?deviceId=" + deviceId2);
        NetXMLDataFactory netXMLDataFactory = NetXMLDataFactory.getInstance();
        Document park_doc = null;
        if (park_doc == null || !Boolean.parseBoolean(netXMLDataFactory.getTextNodeByTagName("Suc", park_doc))) {
            return false;
        }
        NodeList freeParkslist = park_doc.getElementsByTagName("freeParks");
        NodeList cityslist = park_doc.getElementsByTagName("citys");
        NodeList directCityslist = park_doc.getElementsByTagName("directCitys");
        if (!(freeParkslist == null || freeParkslist.getLength() <= 0 || (ids3 = ((Element) freeParkslist.item(0)).getElementsByTagName("id")) == null)) {
            int l = ids3.getLength();
            for (int i = 0; i < l; i++) {
                String parkId = ids3.item(i).getFirstChild().getNodeValue();
                freePark.put(parkId, parkId);
            }
        }
        if (!(cityslist == null || cityslist.getLength() <= 0 || (ids2 = ((Element) cityslist.item(0)).getElementsByTagName("id")) == null)) {
            int l2 = ids2.getLength();
            for (int i2 = 0; i2 < l2; i2++) {
                String directCityId = ids2.item(i2).getFirstChild().getNodeValue();
                directCitys.put(directCityId, directCityId);
            }
        }
        if (!(directCityslist == null || directCityslist.getLength() <= 0 || (ids = ((Element) directCityslist.item(0)).getElementsByTagName("id")) == null)) {
            int l3 = ids.getLength();
            for (int i3 = 0; i3 < l3; i3++) {
                String cityId = ids.item(i3).getFirstChild().getNodeValue();
                citys.put(cityId, cityId);
            }
        }
        return true;
    }

    public static boolean addPay(String userId, String deviceId2, String cityId, String provinceId, String outTradeNo, String price) {
        String param = ((("?userId=" + userId) + "&deviceId=" + deviceId2) + "&orderNo=" + outTradeNo) + "&amount=" + price;
        if (cityId != null && !"".equals(cityId)) {
            param = param + "&cityId=" + cityId;
        } else if (provinceId != null && !"".equals(provinceId)) {
            param = param + "&provinceId=" + provinceId;
        }
        NetXMLDataFactory netXMLDataFactory = NetXMLDataFactory.getInstance();
        Document park_doc = netXMLDataFactory.getDocumentData(GuideConfig.URL_CITY_PAY + param);
        if (park_doc == null) {
            return false;
        }
        if (!Boolean.parseBoolean(netXMLDataFactory.getTextNodeByTagName("Suc", park_doc))) {
            return false;
        }
        return true;
    }

    public static boolean verify(String deviceId2, String parkId) {
        NetXMLDataFactory netXMLDataFactory = NetXMLDataFactory.getInstance();
        Document park_doc = netXMLDataFactory.getDocumentData(GuideConfig.URL_PARK_VERIFY + (("?deviceId=" + deviceId2) + "&parkId=" + parkId));
        if (park_doc == null) {
            return false;
        }
        if (!Boolean.parseBoolean(netXMLDataFactory.getTextNodeByTagName("Suc", park_doc))) {
            return false;
        }
        return true;
    }

    public static boolean addFree(String deviceId2, String parkId) {
        NetXMLDataFactory netXMLDataFactory = NetXMLDataFactory.getInstance();
        Document park_doc = netXMLDataFactory.getDocumentData(GuideConfig.URL_PARK_FREE_ADD + (("deviceId=" + deviceId2) + "&parkId=" + parkId));
        if (park_doc == null) {
            return false;
        }
        if (!Boolean.parseBoolean(netXMLDataFactory.getTextNodeByTagName("Suc", park_doc))) {
            return false;
        }
        return true;
    }

    public static Map<String, String> getFreePark() {
        return freePark;
    }

    public static void setFreePark(Map<String, String> freePark2) {
        freePark = freePark2;
    }

    public static Map<String, String> getDirectCitys() {
        return directCitys;
    }

    public static void setDirectCitys(Map<String, String> directCitys2) {
        directCitys = directCitys2;
    }

    public static Map<String, String> getCitys() {
        return citys;
    }

    public static void setCitys(Map<String, String> citys2) {
        citys = citys2;
    }
}
