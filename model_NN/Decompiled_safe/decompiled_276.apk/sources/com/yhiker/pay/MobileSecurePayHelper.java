package com.yhiker.pay;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.yhiker.playmate.R;
import com.yhiker.util.DialogUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class MobileSecurePayHelper {
    static final String TAG = "MobileSecurePayHelper";
    Context mContext = null;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                switch (msg.what) {
                    case 2:
                        MobileSecurePayHelper.this.closeProgress();
                        MobileSecurePayHelper.this.showInstallConfirmDialog(MobileSecurePayHelper.this.mContext, (String) msg.obj);
                        break;
                }
                super.handleMessage(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private ProgressDialog mProgress = null;

    public MobileSecurePayHelper(Context context) {
        this.mContext = context;
    }

    public boolean detectMobile_sp() {
        boolean isMobile_spExist = isMobile_spExist();
        if (!isMobile_spExist) {
            final String cachePath = this.mContext.getCacheDir().getAbsolutePath() + "/temp.apk";
            retrieveApkFromAssets(this.mContext, PartnerConfig.ALIPAY_PLUGIN_NAME, cachePath);
            this.mProgress = DialogUtil.showProgress(this.mContext, null, "正在检测安全支付服务版本", false, true);
            new Thread(new Runnable() {
                public void run() {
                    String newApkdlUrl = MobileSecurePayHelper.this.checkNewUpdate(MobileSecurePayHelper.getApkInfo(MobileSecurePayHelper.this.mContext, cachePath));
                    if (newApkdlUrl != null) {
                        MobileSecurePayHelper.this.retrieveApkFromNet(MobileSecurePayHelper.this.mContext, newApkdlUrl, cachePath);
                    }
                    Message msg = new Message();
                    msg.what = 2;
                    msg.obj = cachePath;
                    MobileSecurePayHelper.this.mHandler.sendMessage(msg);
                }
            }).start();
        }
        return isMobile_spExist;
    }

    public void showInstallConfirmDialog(final Context context, final String cachePath) {
        AlertDialog.Builder tDialog = new AlertDialog.Builder(context);
        tDialog.setIcon((int) R.drawable.info);
        tDialog.setTitle(context.getResources().getString(R.string.confirm_install_hint));
        tDialog.setMessage(context.getResources().getString(R.string.confirm_install));
        tDialog.setPositiveButton((int) R.string.Ensure, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BaseHelper.chmod("777", cachePath);
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.addFlags(268435456);
                intent.setDataAndType(Uri.parse("file://" + cachePath), "application/vnd.android.package-archive");
                context.startActivity(intent);
            }
        });
        tDialog.setNegativeButton(context.getResources().getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        tDialog.show();
    }

    public boolean isMobile_spExist() {
        List<PackageInfo> pkgList = this.mContext.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < pkgList.size(); i++) {
            if (pkgList.get(i).packageName.equalsIgnoreCase("com.alipay.android.app")) {
                return true;
            }
        }
        return false;
    }

    public boolean retrieveApkFromAssets(Context context, String fileName, String path) {
        try {
            InputStream is = context.getAssets().open(fileName);
            File file = new File(path);
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            byte[] temp = new byte[1024];
            while (true) {
                int i = is.read(temp);
                if (i > 0) {
                    fos.write(temp, 0, i);
                } else {
                    fos.close();
                    is.close();
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static PackageInfo getApkInfo(Context context, String archiveFilePath) {
        return context.getPackageManager().getPackageArchiveInfo(archiveFilePath, 128);
    }

    public String checkNewUpdate(PackageInfo packageInfo) {
        try {
            JSONObject resp = sendCheckNewUpdate(packageInfo.versionName);
            if (resp.getString("needUpdate").equalsIgnoreCase("true")) {
                return resp.getString("updateUrl");
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject sendCheckNewUpdate(String versionName) {
        try {
            JSONObject req = new JSONObject();
            req.put(AlixDefine.action, AlixDefine.actionUpdate);
            JSONObject data = new JSONObject();
            data.put(AlixDefine.platform, "android");
            data.put(AlixDefine.VERSION, versionName);
            data.put(AlixDefine.partner, "");
            req.put(AlixDefine.data, data);
            return sendRequest(req.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject sendRequest(String content) {
        String response;
        NetworkManager nM = new NetworkManager(this.mContext);
        JSONObject jsonResponse = null;
        try {
            synchronized (nM) {
                response = nM.SendAndWaitResponse(content, Constant.server_url);
            }
            jsonResponse = new JSONObject(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (jsonResponse != null) {
            BaseHelper.log(TAG, jsonResponse.toString());
        }
        return jsonResponse;
    }

    public boolean retrieveApkFromNet(Context context, String strurl, String filename) {
        try {
            return new NetworkManager(this.mContext).urlDownloadToFile(context, strurl, filename);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void closeProgress() {
        try {
            if (this.mProgress != null) {
                this.mProgress.dismiss();
                this.mProgress = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
