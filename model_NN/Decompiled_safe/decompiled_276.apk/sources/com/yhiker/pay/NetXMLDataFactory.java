package com.yhiker.pay;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class NetXMLDataFactory {
    private static NetXMLDataFactory netXMLDataFactory_instance = new NetXMLDataFactory();

    private NetXMLDataFactory() {
    }

    public static NetXMLDataFactory getInstance() {
        return netXMLDataFactory_instance;
    }

    public Map<String, byte[]> getDocumentData(InputStream input) {
        return null;
    }

    public Document getDocumentData(String url) {
        try {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                conn.setDoInput(true);
                conn.connect();
                Document doc = null;
                try {
                    doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(conn.getInputStream());
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } catch (SAXException e2) {
                    e2.printStackTrace();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                return doc;
            } catch (IOException e4) {
                IOException e5 = e4;
                e5.printStackTrace();
                Log.e("conn error", e5.toString());
                return null;
            }
        } catch (MalformedURLException e6) {
            e6.printStackTrace();
            Log.e(AlixDefine.URL, "error");
            return null;
        }
    }

    public String getTextNodeByTagName(String tagName, Document doc) {
        NodeList nodelist = doc.getElementsByTagName(tagName);
        if (nodelist.getLength() > 0) {
            Element elt = (Element) nodelist.item(0);
            if (elt.hasChildNodes()) {
                return elt.getFirstChild().getNodeValue();
            }
            Log.e("XML", tagName + " tag hasn't children nodes!");
            return null;
        }
        Log.e("XML", tagName + " tag can't be found!");
        return null;
    }

    public String getTextNodeByTagName(String tagName, Element elt_root) {
        NodeList nodelist = elt_root.getElementsByTagName(tagName);
        if (nodelist.getLength() > 0) {
            Element elt = (Element) nodelist.item(0);
            if (elt.hasChildNodes()) {
                return elt.getFirstChild().getNodeValue();
            }
            Log.e("XML", tagName + " tag hasn't children nodes!");
            return null;
        }
        Log.e("XML", tagName + " tag can't be found!");
        return null;
    }
}
