package com.yhiker.boot.sqlite;

public class SQLServiceFactory {
    public static SQLService sqlService = null;

    public static SQLService getSQLService() {
        if (sqlService == null) {
            sqlService = new SQLService();
        }
        return sqlService;
    }
}
