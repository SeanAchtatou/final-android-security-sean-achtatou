package com.yhiker.boot.sqlite;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.yhiker.config.ConfigHp;
import com.yhiker.config.GuideConfig;

public class SQLService {
    public void updateFrequency(long frequency) {
        Log.i(getClass().getSimpleName(), "updateFrequency is called");
        SQLiteDatabase scenic_list_db = null;
        try {
            scenic_list_db = SQLiteDatabase.openDatabase(GuideConfig.getInitDataDir() + ConfigHp.per_conf_path, null, 16);
            scenic_list_db.execSQL(" update per_conf set pc_value=" + frequency + " where pc_key='msgFreq'");
            if (scenic_list_db != null) {
                scenic_list_db.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (scenic_list_db != null) {
                scenic_list_db.close();
            }
        } catch (Throwable th) {
            if (scenic_list_db != null) {
                scenic_list_db.close();
            }
            throw th;
        }
    }
}
