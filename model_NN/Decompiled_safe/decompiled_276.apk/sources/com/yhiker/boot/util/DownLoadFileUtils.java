package com.yhiker.boot.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

public class DownLoadFileUtils {
    public static void downLoadFile(final Activity activity, final String downloadUrl, final String fileName, Handler handler) {
        try {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        File tempFile = DownLoadFileUtils.getDataSource(downloadUrl, fileName);
                        if (activity != null) {
                            DownLoadFileUtils.openFile(activity, tempFile);
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX INFO: Multiple debug info for r7v1 org.apache.http.impl.client.DefaultHttpClient: [D('downloadUrl' java.lang.String), D('httpclient' org.apache.http.client.HttpClient)] */
    /* JADX INFO: Multiple debug info for r7v2 org.apache.http.HttpResponse: [D('httpclient' org.apache.http.client.HttpClient), D('responseApk' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r0v5 byte[]: [D('buf' byte[]), D('count' int)] */
    /* access modifiers changed from: private */
    public static File getDataSource(String downloadUrl, String fileName) throws Exception {
        HttpGet mHttpGetApk = new HttpGet(downloadUrl);
        HttpResponse responseApk = new DefaultHttpClient(new BasicHttpParams()).execute(mHttpGetApk);
        if (responseApk.getStatusLine().getStatusCode() == 200) {
            InputStream isApk = responseApk.getEntity().getContent();
            if (isApk == null) {
                throw new RuntimeException("stream is null");
            }
            File myTempFile = new File(fileName + "dom");
            FileOutputStream fos = new FileOutputStream(myTempFile);
            byte[] data = new byte[8192];
            while (true) {
                int count = isApk.read(data, 0, 8192);
                if (count != -1) {
                    fos.write(data, 0, count);
                    byte[] buf = new byte[128];
                    while (true) {
                        int numread = isApk.read(buf);
                        if (numread > 0) {
                            fos.write(buf, 0, numread);
                        }
                    }
                } else {
                    isApk.close();
                    myTempFile.renameTo(new File(fileName));
                    return myTempFile;
                }
            }
        } else {
            mHttpGetApk.abort();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void openFile(Activity activity, File f) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(f), getMIMEType(f));
        activity.startActivity(intent);
    }

    private static String getMIMEType(File f) {
        String fName = f.getName();
        String end = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        if (end.equals("doc") || end.equals("dot")) {
            return "application/msword";
        }
        if (end.equals("xls")) {
            return "application/vnd.ms-excel";
        }
        if (end.equals("ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (end.equals("pdf")) {
            return "application/pdf";
        }
        if (end.equals("rar")) {
            return "application/x-rar-compressed";
        }
        if (end.equals("zip")) {
            return "application/zip";
        }
        if (end.equals("bmp")) {
            return "image/bmp";
        }
        if (end.equals("gif")) {
            return "image/gif";
        }
        if (end.equals("jpg")) {
            return "image/jpeg";
        }
        if (end.equals("png")) {
            return "image/png";
        }
        if (end.equals("txt")) {
            return "text/plain";
        }
        if (end.equals("dotx")) {
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
        }
        if (end.equals("docx")) {
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        }
        if (end.equals("xlsx")) {
            return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
        if (end.equals("pptx")) {
            return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
        }
        if (end.equals("apk")) {
            return "application/vnd.android.package-archive";
        }
        return "*/*";
    }

    public static void delFile(File myFile) {
        if (myFile.exists()) {
            myFile.delete();
        }
    }
}
