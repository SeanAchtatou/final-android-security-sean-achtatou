package com.yhiker.boot.util;

import java.util.Map;

public class ClientUtils {
    public static String getJsonStr(Map<String, String> jsonMap) {
        StringBuffer jsonStr = new StringBuffer();
        jsonStr.append("{");
        for (String mapKey : jsonMap.keySet()) {
            jsonStr.append("'").append(mapKey).append("'").append(":").append("'").append(jsonMap.get(mapKey)).append("'").append(",");
        }
        if (jsonStr.toString().length() > 1) {
            jsonStr = new StringBuffer(jsonStr.substring(0, jsonStr.length() - 1));
        }
        return jsonStr.append("}").toString();
    }
}
