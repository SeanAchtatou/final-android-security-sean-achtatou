package com.yhiker.boot.util;

import android.os.Environment;
import com.yhiker.config.ConfigHp;

public class ClientConstants {
    public static final String APKFILENAME = (FILEPATH + "/playmate.apk");
    public static final String CITYSCENICLISTDBPATH = (SDCARDPATH + "/yhiker/conf/scenic_list_");
    public static final String CONTENT = "content";
    public static final String DATAFILENAME = (FILEPATH + "/scenic.zip");
    public static final String DOWNLOADURL = "downloadUrl";
    public static final String FILENAME = "filename";
    public static final String FILEPATH = (SDCARDPATH + ConfigHp.update_dir);
    public static final String NOTIFITYINDEX = "notifityindex";
    public static final String NOTIFYTITLE = "notifyTitle";
    public static final String SCENICLISTDBPATH = (SDCARDPATH + ConfigHp.scenic_list_path);
    public static final String SDCARDPATH = Environment.getExternalStorageDirectory().toString();
    public static final long SENDINTERVAL = 2000;
    public static final String SERVICEURL = "http://58.211.138.180:8088/dms/service/user_notify_message/getNotifyMessage";
    public static final String SHOWSTATICPAGE = "showStaticPage";
    public static final String TYPE = "type";
    public static final String UPDATEAPK = "updateApk";
    public static final String UPDATEDATA = "updateData";
}
