package com.yhiker.boot.webservice;

import android.util.Log;
import com.yhiker.boot.sqlite.SQLServiceFactory;
import com.yhiker.boot.util.ClientConstants;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyWebservice {
    final String serviceUrl = ClientConstants.SERVICEURL;

    public JSONArray getRespondMessages(JSONObject jsonObj) {
        Log.d(MyWebservice.class.getSimpleName(), "serviceUrl = http://58.211.138.180:8088/dms/service/user_notify_message/getNotifyMessage");
        HttpPost request = new HttpPost(ClientConstants.SERVICEURL);
        request.setHeader("Accept-Encoding", "application/json");
        request.setHeader("Content-type", "application/json");
        HttpClient httpclient = new DefaultHttpClient();
        Log.d(MyWebservice.class.getSimpleName(), "jsonObj = " + jsonObj.toString());
        try {
            request.setEntity(new StringEntity(jsonObj.toString(), "UTF-8"));
            HttpEntity entity = httpclient.execute(request).getEntity();
            Log.d(MyWebservice.class.getSimpleName(), "responseStr = " + "{'notifyResponseMsg':{'frequency':'2','result':[{'type':'updateApk','notifyTitle':'玩伴儿APK有更新','content':'玩伴儿APK1.1.5版本发布','downloadUrl':'http://122.193.88.130:8080/123.mp3'},{'type':'updateData','notifyTitle':'玩伴儿景区有更新','content':'玩伴儿景区1.1.5版本发布','downloadUrl':'http://10.0.2.2:8080/data.zip'},{'type':'showStaticPage','notifyTitle':'玩伴儿软件有软件更新','content':'玩伴儿软件1.1.5版本发布','downloadUrl':'http://www.163.com'}]}}");
            JSONObject messageJsonObj = new JSONObject("{'notifyResponseMsg':{'frequency':'2','result':[{'type':'updateApk','notifyTitle':'玩伴儿APK有更新','content':'玩伴儿APK1.1.5版本发布','downloadUrl':'http://122.193.88.130:8080/123.mp3'},{'type':'updateData','notifyTitle':'玩伴儿景区有更新','content':'玩伴儿景区1.1.5版本发布','downloadUrl':'http://10.0.2.2:8080/data.zip'},{'type':'showStaticPage','notifyTitle':'玩伴儿软件有软件更新','content':'玩伴儿软件1.1.5版本发布','downloadUrl':'http://www.163.com'}]}}").getJSONObject("notifyResponseMsg");
            SQLServiceFactory.getSQLService().updateFrequency((long) messageJsonObj.getInt("frequency"));
            return messageJsonObj.getJSONArray("result");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        } catch (JSONException e4) {
            e4.printStackTrace();
            return null;
        }
    }
}
