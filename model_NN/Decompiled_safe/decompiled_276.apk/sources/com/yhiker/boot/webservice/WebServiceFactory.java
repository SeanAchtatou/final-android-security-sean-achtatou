package com.yhiker.boot.webservice;

public class WebServiceFactory {
    public static MyWebservice myWebservice = null;

    public static MyWebservice getMyWebservice() {
        if (myWebservice == null) {
            myWebservice = new MyWebservice();
        }
        return myWebservice;
    }
}
