package com.yhiker.boot.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import com.yhiker.playmate.R;

public class NotificationView extends MyActivity {
    String[] aryShop = {"第一天", "第二条"};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showDialog(1);
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new AlertDialog.Builder(this).setTitle("这是一个简单的弹出对话框的Demo").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i(getClass().getSimpleName(), "单击了简单对话框上的\"确认\"按钮\n");
                    }
                }).setNegativeButton("back", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
            case 1:
                return new AlertDialog.Builder(this).setTitle("标题").setIcon((int) R.drawable.icon).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface a0, int a1) {
                        Log.i(getClass().getSimpleName(), "单击了复杂对话框上的\"确认\"按钮\n");
                    }
                }).setItems(this.aryShop, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i(getClass().getSimpleName(), "你选择了：" + NotificationView.this.aryShop[which] + "\n");
                    }
                }).setNegativeButton("back", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
            default:
                return null;
        }
    }
}
