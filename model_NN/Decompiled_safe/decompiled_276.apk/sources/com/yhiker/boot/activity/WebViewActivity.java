package com.yhiker.boot.activity;

import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.yhiker.playmate.R;

public class WebViewActivity extends MyActivity {
    private WebView wv;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.webview);
        init();
        loadurl(this.wv, this.fileDownloadUrl);
    }

    public void onBackPressed() {
        Log.d(getClass().getSimpleName(), "onBackPressed is called");
        this.wv.goBack();
        super.onBackPressed();
    }

    public void init() {
        this.wv = (WebView) findViewById(R.id.webview);
        this.wv.getSettings().setJavaScriptEnabled(true);
        this.wv.clearCache(true);
        this.wv.setScrollBarStyle(0);
        this.wv.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                WebViewActivity.this.loadurl(view, url);
                return true;
            }
        });
        this.wv.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    WebViewActivity.this.handler.sendEmptyMessage(1);
                }
                super.onProgressChanged(view, progress);
            }
        });
        this.pd.setMessage("载入中，请稍候...");
    }

    public void loadurl(final WebView view, final String url) {
        new Thread() {
            public void run() {
                WebViewActivity.this.handler.sendEmptyMessage(0);
                view.loadUrl(url);
            }
        }.start();
    }
}
