package com.yhiker.boot.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.yhiker.boot.util.ClientConstants;
import com.yhiker.boot.util.DownLoadFileUtils;
import com.yhiker.playmate.R;

public class MyActivity extends Activity {
    protected String fileDownloadUrl;
    protected String fileName;
    protected Handler handler;
    protected int notificationIndex;
    protected ProgressDialog pd;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(getClass().getSimpleName(), "onCreate is called");
        this.pd = new ProgressDialog(this);
        this.pd.setProgressStyle(0);
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                if (!Thread.currentThread().isInterrupted()) {
                    switch (msg.what) {
                        case 0:
                            MyActivity.this.pd.show();
                            break;
                        case 1:
                            MyActivity.this.pd.hide();
                            break;
                    }
                }
                super.handleMessage(msg);
            }
        };
        Bundle activityBundle = getIntent().getExtras();
        this.fileName = activityBundle.getString(ClientConstants.FILENAME);
        this.fileDownloadUrl = activityBundle.getString(ClientConstants.DOWNLOADURL);
        this.notificationIndex = activityBundle.getInt(ClientConstants.NOTIFITYINDEX);
        Log.d(getClass().getSimpleName(), "fileName=" + this.fileName);
        Log.d(getClass().getSimpleName(), "fileDownloadUrl=" + this.fileDownloadUrl);
        Log.d(getClass().getSimpleName(), "notificationIndex=" + this.notificationIndex);
        if (!"".equalsIgnoreCase(this.fileName) && this.fileName != null) {
            showDialog(0);
        }
        ((NotificationManager) getSystemService("notification")).cancel(this.notificationIndex);
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new AlertDialog.Builder(this).setTitle("请确认是否开始下载").setPositiveButton((int) R.string.Ensure, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MyActivity.this.handler.sendEmptyMessage(0);
                        DownLoadFileUtils.downLoadFile(MyActivity.this, MyActivity.this.fileDownloadUrl, MyActivity.this.fileName, MyActivity.this.handler);
                    }
                }).setNegativeButton((int) R.string.Cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        MyActivity.this.finish();
                    }
                }).create();
            default:
                return null;
        }
    }
}
