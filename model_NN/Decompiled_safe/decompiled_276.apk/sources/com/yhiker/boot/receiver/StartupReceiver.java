package com.yhiker.boot.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.yhiker.boot.service.RemoteService;

public class StartupReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, RemoteService.class));
    }
}
