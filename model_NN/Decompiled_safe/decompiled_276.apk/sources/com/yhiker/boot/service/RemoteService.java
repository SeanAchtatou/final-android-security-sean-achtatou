package com.yhiker.boot.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.yhiker.boot.activity.UpdateApkView;
import com.yhiker.boot.activity.UpdateDateView;
import com.yhiker.boot.activity.WebViewActivity;
import com.yhiker.boot.util.ClientConstants;
import com.yhiker.boot.webservice.WebServiceFactory;
import com.yhiker.playmate.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RemoteService extends Service {
    public IBinder onBind(Intent intent) {
        Log.d(getClass().getSimpleName(), "onBind is called");
        return null;
    }

    public void onCreate() {
        Log.d(getClass().getSimpleName(), "onCreate is called");
        super.onCreate();
    }

    public void onDestroy() {
        Log.d(getClass().getSimpleName(), "onDestroy is called");
        super.onDestroy();
    }

    public void onRebind(Intent intent) {
        Log.d(getClass().getSimpleName(), "onRebind is called");
        super.onRebind(intent);
    }

    public boolean onUnbind(Intent intent) {
        Log.d(getClass().getSimpleName(), "onUnbind is called");
        return super.onUnbind(intent);
    }

    public void onStart(Intent intent, int startId) {
        Log.d(getClass().getSimpleName(), "onStart is called");
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Log.d(getClass().getSimpleName(), "do what just i want do");
                JSONArray messageJsonArray = WebServiceFactory.getMyWebservice().getRespondMessages(RemoteService.this.getDeviceInfo());
                Log.d(RemoteService.this.getClass().getSimpleName(), "messageJsonArray=" + messageJsonArray.toString());
                if (messageJsonArray != null && messageJsonArray.length() > 0) {
                    RemoteService.this.sendNotify(messageJsonArray);
                }
            }
        }, ClientConstants.SENDINTERVAL);
        super.onStart(intent, startId);
    }

    /* access modifiers changed from: private */
    public JSONObject getDeviceInfo() {
        Log.d(getClass().getSimpleName(), "getDeviceInfo is called");
        JSONObject rootJson = new JSONObject();
        JSONObject innerJson = new JSONObject();
        String deviceId = ((TelephonyManager) getSystemService("phone")).getDeviceId();
        String os = Build.VERSION.SDK + "," + Build.VERSION.RELEASE;
        String mtype = Build.MODEL;
        try {
            innerJson.put("versioncode", "");
            innerJson.put("deviceId", deviceId);
            innerJson.put("os", os);
            innerJson.put("mtype", mtype);
            innerJson.put("sceniclistver", "");
            rootJson.put("notifyRequestMsg", innerJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rootJson;
    }

    /* access modifiers changed from: private */
    public void sendNotify(JSONArray messageJsonArray) {
        Log.d(getClass().getSimpleName(), "sendNotify is called");
        Log.i(getClass().getSimpleName(), "messageJsonArray.length()=" + messageJsonArray.length());
        NotificationManager nm = (NotificationManager) getSystemService("notification");
        for (int i = 0; i < messageJsonArray.length(); i++) {
            try {
                JSONObject messageJsonObj = messageJsonArray.getJSONObject(i);
                Notification notification = new Notification(R.drawable.icon, messageJsonObj.getString(ClientConstants.NOTIFYTITLE), System.currentTimeMillis());
                String string = messageJsonObj.getString(ClientConstants.CONTENT);
                String type = messageJsonObj.getString(ClientConstants.TYPE);
                Intent activityIntent = null;
                String fileName = null;
                if (ClientConstants.UPDATEAPK.equalsIgnoreCase(type)) {
                    fileName = ClientConstants.APKFILENAME;
                    activityIntent = new Intent(this, UpdateApkView.class);
                } else if (ClientConstants.UPDATEDATA.equalsIgnoreCase(type)) {
                    fileName = ClientConstants.DATAFILENAME;
                    activityIntent = new Intent(this, UpdateDateView.class);
                } else if (ClientConstants.SHOWSTATICPAGE.equalsIgnoreCase(type)) {
                    activityIntent = new Intent(this, WebViewActivity.class);
                }
                Bundle activityBundle = new Bundle();
                activityBundle.putInt(ClientConstants.NOTIFITYINDEX, i);
                activityBundle.putString(ClientConstants.FILENAME, fileName);
                activityBundle.putString(ClientConstants.DOWNLOADURL, messageJsonObj.getString(ClientConstants.DOWNLOADURL));
                activityIntent.putExtras(activityBundle);
                notification.setLatestEventInfo(this, "", string, PendingIntent.getActivity(this, 0, activityIntent, 0));
                notification.vibrate = new long[]{100, 250, 100, 500};
                nm.notify(i, notification);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
