package com.christmasbunnypuzzle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import java.util.HashMap;
import java.util.Map;

public class ImageAdapter extends BaseAdapter {
    private static String TAG = "ImageAdapter";
    private static ImageAdapter imageAdapter;
    private static Integer[] mImageIds = {Integer.valueOf((int) R.drawable.image_1), Integer.valueOf((int) R.drawable.image_2), Integer.valueOf((int) R.drawable.image_3), Integer.valueOf((int) R.drawable.image_4), Integer.valueOf((int) R.drawable.image_5), Integer.valueOf((int) R.drawable.image_6), Integer.valueOf((int) R.drawable.image_7), Integer.valueOf((int) R.drawable.image_8), Integer.valueOf((int) R.drawable.image_9), Integer.valueOf((int) R.drawable.image_10), Integer.valueOf((int) R.drawable.image_11), Integer.valueOf((int) R.drawable.image_12), Integer.valueOf((int) R.drawable.image_13), Integer.valueOf((int) R.drawable.image_14), Integer.valueOf((int) R.drawable.image_15), Integer.valueOf((int) R.drawable.image_16), Integer.valueOf((int) R.drawable.image_17), Integer.valueOf((int) R.drawable.image_18), Integer.valueOf((int) R.drawable.image_19), Integer.valueOf((int) R.drawable.image_20)};
    private static Map<Integer, Image> mImageViews = new HashMap();
    private static Integer[] mThumbnailIds = {Integer.valueOf((int) R.drawable.thumbnail_1), Integer.valueOf((int) R.drawable.thumbnail_2), Integer.valueOf((int) R.drawable.thumbnail_3), Integer.valueOf((int) R.drawable.thumbnail_4), Integer.valueOf((int) R.drawable.thumbnail_5), Integer.valueOf((int) R.drawable.thumbnail_6), Integer.valueOf((int) R.drawable.thumbnail_7), Integer.valueOf((int) R.drawable.thumbnail_8), Integer.valueOf((int) R.drawable.thumbnail_9), Integer.valueOf((int) R.drawable.thumbnail_10), Integer.valueOf((int) R.drawable.thumbnail_11), Integer.valueOf((int) R.drawable.thumbnail_12), Integer.valueOf((int) R.drawable.thumbnail_13), Integer.valueOf((int) R.drawable.thumbnail_14), Integer.valueOf((int) R.drawable.thumbnail_15), Integer.valueOf((int) R.drawable.thumbnail_16), Integer.valueOf((int) R.drawable.thumbnail_17), Integer.valueOf((int) R.drawable.thumbnail_18), Integer.valueOf((int) R.drawable.thumbnail_19), Integer.valueOf((int) R.drawable.thumbnail_20)};
    private static Integer[] mWallpaperIds = {Integer.valueOf((int) R.drawable.wallpaper_1), Integer.valueOf((int) R.drawable.wallpaper_2), Integer.valueOf((int) R.drawable.wallpaper_3), Integer.valueOf((int) R.drawable.wallpaper_4), Integer.valueOf((int) R.drawable.wallpaper_5), Integer.valueOf((int) R.drawable.wallpaper_6), Integer.valueOf((int) R.drawable.wallpaper_7), Integer.valueOf((int) R.drawable.wallpaper_8), Integer.valueOf((int) R.drawable.wallpaper_9), Integer.valueOf((int) R.drawable.wallpaper_10), Integer.valueOf((int) R.drawable.wallpaper_11), Integer.valueOf((int) R.drawable.wallpaper_12), Integer.valueOf((int) R.drawable.wallpaper_13), Integer.valueOf((int) R.drawable.wallpaper_14), Integer.valueOf((int) R.drawable.wallpaper_15), Integer.valueOf((int) R.drawable.wallpaper_16), Integer.valueOf((int) R.drawable.wallpaper_17), Integer.valueOf((int) R.drawable.wallpaper_18), Integer.valueOf((int) R.drawable.wallpaper_19), Integer.valueOf((int) R.drawable.wallpaper_20)};

    private ImageAdapter(Context pContext, TypedArray pA) {
        int galleryItemBackground = pA.getResourceId(0, 0);
        pA.recycle();
        for (int i = 0; i < mImageIds.length; i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(pContext.getResources(), mImageIds[i].intValue());
            Integer imageId = mThumbnailIds[i];
            ImageView imageView = new ImageView(pContext);
            imageView.setImageResource(imageId.intValue());
            imageView.setLayoutParams(new Gallery.LayoutParams(150, 100));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setBackgroundResource(galleryItemBackground);
            mImageViews.put(Integer.valueOf(i), new Image(bitmap, imageView));
        }
    }

    public static ImageAdapter initImageAdapter(Context pContext, TypedArray pA) {
        if (imageAdapter == null) {
            imageAdapter = new ImageAdapter(pContext, pA);
        }
        return imageAdapter;
    }

    public static ImageAdapter getImageAdapter() {
        return imageAdapter;
    }

    public static Bitmap getBitmapByPosition(int position) {
        return mImageViews.get(Integer.valueOf(position)).getBitmap();
    }

    public static Bitmap getWallpaperByPosition(Context context, int position) {
        return BitmapFactory.decodeResource(context.getResources(), mWallpaperIds[position].intValue());
    }

    public int getCount() {
        return mImageIds.length;
    }

    public Object getItem(int position) {
        return mImageIds[position];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return mImageViews.get(Integer.valueOf(position)).getImageView();
    }

    private static class Image {
        private Bitmap mBitmap;
        private ImageView mImageView;

        public Image(Bitmap bitmap, ImageView imageView) {
            this.mBitmap = bitmap;
            this.mImageView = imageView;
        }

        public Bitmap getBitmap() {
            return this.mBitmap;
        }

        public void setBitmap(Bitmap bitmap) {
            this.mBitmap = bitmap;
        }

        public ImageView getImageView() {
            return this.mImageView;
        }

        public void setImageView(ImageView imageView) {
            this.mImageView = imageView;
        }
    }
}
