package com.christmasbunnypuzzle.game;

import com.christmasbunnypuzzle.R;

public enum WallpaperGame {
    wallpaper(R.string.f19wallpaperchooseset),
    play(R.string.f20wallpaperchooseplay);
    
    private int action;

    private WallpaperGame(int pAction) {
        this.action = pAction;
    }

    public int getAction() {
        return this.action;
    }

    public void setAction(int action2) {
        this.action = action2;
    }
}
