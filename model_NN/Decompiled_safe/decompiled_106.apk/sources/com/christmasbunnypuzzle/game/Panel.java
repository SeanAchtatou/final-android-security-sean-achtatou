package com.christmasbunnypuzzle.game;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;
import com.christmasbunnypuzzle.ImageAdapter;
import com.christmasbunnypuzzle.R;
import com.christmasbunnypuzzle.WallpaperMenu;
import com.christmasbunnypuzzle.db.WallpaperDbManager;
import com.christmasbunnypuzzle.game.MoveBitmap;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Panel extends SurfaceView implements SurfaceHolder.Callback, Serializable {
    private static final String TAG = "Panel";
    private Integer endPuzzleX;
    private Integer endPuzzleY;
    private boolean isSizeChanged;
    private Integer lastX;
    private Integer lastY;
    private int lineCount;
    private Bitmap mBackground;
    private List<Bitmap> mBitmaps;
    private Activity mContext;
    private Graphic mCurrentGraphic;
    private GameStatus mGameStatus = GameStatus.beforeStart;
    private GameType mGameType;
    private int mItemPosition;
    private MoveBitmap mMoveBitmap;
    private Bitmap mScratch;
    private Bitmap mScratchResized;
    private List<Integer> mShuffleBitmap;
    private int orientation;
    private int resultHeight;
    private int resultWidth;
    int screenHeight;
    int screenWidth;
    private int startPuzzleX;
    private int startPuzzleY;
    private int startSquareX;
    private int startSquareY;
    private Integer touchedX;
    private Integer touchedY;

    public Panel(Activity context, GameType gameType, int itemPosition) {
        super(context);
        this.mContext = context;
        this.mGameType = gameType;
        this.isSizeChanged = true;
        this.mItemPosition = itemPosition;
        this.lineCount = this.mGameType.getQuantity() - 1;
        this.mScratch = ResourceLauncher.getScratch();
        if (this.mScratch == null) {
            this.mScratch = ImageAdapter.getBitmapByPosition(this.mItemPosition);
            ResourceLauncher.setScratch(this.mScratch);
        }
        if (ResourceLauncher.getBackground() == null) {
            ResourceLauncher.setBackground(BitmapFactory.decodeResource(this.mContext.getResources(), R.drawable.background_top));
        }
        initScreenOrientation();
        cutBitmap();
        getHolder().addCallback(this);
        setFocusable(true);
    }

    public void onDraw(Canvas canvas) {
        canvas.drawBitmap(this.mBackground, 0.0f, 0.0f, (Paint) null);
        if (GameStatus.started.equals(this.mGameStatus)) {
            int bitmapStartX = this.startPuzzleX;
            int bitmapStartY = this.startPuzzleY;
            int countCicles = 0;
            int bitmapWidth = this.mMoveBitmap.getParameters().getBitmapWidth();
            int bitmapHeight = this.mMoveBitmap.getParameters().getBitmapHeight();
            this.mShuffleBitmap = this.mMoveBitmap.getShuffleBitmap();
            for (Integer bitmapId : this.mShuffleBitmap) {
                Bitmap bitmap = this.mMoveBitmap.getBitmapView().get(bitmapId);
                int bitmapEndX = bitmapStartX + bitmapWidth;
                int bitmapEndY = bitmapStartY + bitmapHeight;
                if (this.touchedX == null || this.touchedY == null || bitmapStartX >= this.touchedX.intValue() || bitmapEndX <= this.touchedX.intValue() || bitmapStartY >= this.touchedY.intValue() || bitmapEndY <= this.touchedY.intValue()) {
                    canvas.drawBitmap(bitmap, (float) bitmapStartX, (float) bitmapStartY, (Paint) null);
                } else if (this.mCurrentGraphic != null) {
                    this.mCurrentGraphic.setBitmap(bitmap);
                    this.mCurrentGraphic.setBitmapId(bitmapId.intValue());
                }
                if (this.lastX == null || this.lastY == null || this.lastX.intValue() <= this.startPuzzleX || this.lastX.intValue() >= this.endPuzzleX.intValue() || this.lastY.intValue() <= this.startPuzzleY || this.lastY.intValue() >= this.endPuzzleY.intValue() || bitmapStartX >= this.lastX.intValue() || bitmapEndX <= this.lastX.intValue() || bitmapStartY >= this.lastY.intValue() || bitmapEndY <= this.lastY.intValue() || this.mCurrentGraphic == null) {
                    countCicles++;
                    if (countCicles >= this.mGameType.getQuantity()) {
                        bitmapStartX = this.startPuzzleX;
                        bitmapStartY += bitmapHeight;
                        countCicles = 0;
                    } else {
                        bitmapStartX += bitmapWidth;
                    }
                } else {
                    this.mMoveBitmap.recalculateShuffle(this.mCurrentGraphic.getBitmapId(), bitmapId.intValue());
                    checkCompleted();
                    resetLastXY();
                    onDraw(canvas);
                    return;
                }
            }
            drawGrid(canvas, bitmapWidth, bitmapHeight);
            if (this.mCurrentGraphic != null && this.touchedX != null && this.touchedY != null && this.mCurrentGraphic.getBitmap() != null) {
                canvas.drawBitmap(this.mCurrentGraphic.getBitmap(), (float) this.mCurrentGraphic.getTouchedX(), (float) this.mCurrentGraphic.getTouchedY(), (Paint) null);
                return;
            }
            return;
        }
        canvas.drawBitmap(this.mScratchResized, (float) this.startSquareX, (float) this.startSquareY, (Paint) null);
    }

    private void drawGrid(Canvas canvas, int bitmapWidth, int bitmapHeight) {
        Paint paintTransparent = new Paint();
        paintTransparent.setColor(0);
        paintTransparent.setAlpha(120);
        paintTransparent.setStyle(Paint.Style.STROKE);
        int startVerticalLineX = this.startPuzzleX + bitmapWidth;
        int startVerticalLineY = this.startPuzzleY;
        int stopVerticalLineY = this.endPuzzleY.intValue();
        int startHorisontalLineX = this.startPuzzleX;
        int startHorisontalLineY = this.startPuzzleY + bitmapHeight;
        int stopHorisontalLineX = this.endPuzzleX.intValue();
        for (int verticalLine = 0; verticalLine < this.lineCount; verticalLine++) {
            canvas.drawLine((float) (startVerticalLineX + 1), (float) startVerticalLineY, (float) (startVerticalLineX + 1), (float) stopVerticalLineY, paintTransparent);
            canvas.drawLine((float) startVerticalLineX, (float) startVerticalLineY, (float) startVerticalLineX, (float) stopVerticalLineY, paintTransparent);
            Canvas canvas2 = canvas;
            canvas2.drawLine((float) startHorisontalLineX, (float) (startHorisontalLineY + 1), (float) stopHorisontalLineX, (float) (startHorisontalLineY + 1), paintTransparent);
            canvas.drawLine((float) startHorisontalLineX, (float) startHorisontalLineY, (float) stopHorisontalLineX, (float) startHorisontalLineY, paintTransparent);
            startVerticalLineX += bitmapWidth;
            startHorisontalLineY += bitmapHeight;
        }
    }

    public void initScreenOrientation() {
        Display getOrient = this.mContext.getWindowManager().getDefaultDisplay();
        if (getOrient.getWidth() < getOrient.getHeight()) {
            this.orientation = 1;
        } else {
            this.orientation = 2;
        }
    }

    private Bitmap getScratchResized() {
        return Bitmap.createScaledBitmap(this.mScratch, this.resultWidth, this.resultHeight, false);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (this.isSizeChanged) {
            calculateVisualParameters();
            if (ResourceLauncher.getScratchPortrait() == null || ResourceLauncher.getScratchLandscape() == null) {
                this.mScratchResized = getScratchResized();
                if (1 == this.orientation) {
                    ResourceLauncher.setScratchPortrait(this.mScratchResized);
                } else {
                    ResourceLauncher.setScratchLandscape(this.mScratchResized);
                }
            } else if (1 == this.orientation) {
                this.mScratchResized = ResourceLauncher.getScratchPortrait();
            } else {
                this.mScratchResized = ResourceLauncher.getScratchLandscape();
            }
            if (ResourceLauncher.getBackgroundPortrait() == null || ResourceLauncher.getBackgroundLandscape() == null) {
                this.mBackground = Bitmap.createScaledBitmap(ResourceLauncher.getBackground(), w, h, false);
                if (1 == this.orientation) {
                    ResourceLauncher.setBackgroundPortrait(this.mBackground);
                } else {
                    ResourceLauncher.setBackgroundLandscape(this.mBackground);
                }
            } else if (1 == this.orientation) {
                this.mBackground = ResourceLauncher.getBackgroundPortrait();
            } else {
                this.mBackground = ResourceLauncher.getBackgroundLandscape();
            }
            this.isSizeChanged = false;
        }
    }

    private void calculateVisualParameters() {
        this.screenWidth = getWidth();
        this.screenHeight = getHeight();
        this.resultWidth = this.mScratch.getWidth();
        this.resultHeight = this.mScratch.getHeight();
        double rateWidth = ((double) this.resultWidth) / ((double) this.screenWidth);
        if (rateWidth > 1.0d) {
            this.resultWidth = this.screenWidth;
            this.resultHeight = (int) (((double) this.resultHeight) / rateWidth);
        }
        double rateHeight = ((double) this.resultHeight) / ((double) this.screenHeight);
        if (rateHeight > 1.0d) {
            this.resultHeight = this.screenHeight;
            this.resultWidth = (int) (((double) this.resultWidth) / rateHeight);
        }
        float xScale = ((float) this.resultWidth) / ((float) this.mScratch.getWidth());
        float yScale = ((float) this.resultHeight) / ((float) this.mScratch.getHeight());
        this.startSquareX = (this.screenWidth - this.resultWidth) / 2;
        this.startSquareY = (this.screenHeight - this.resultHeight) / 2;
        if (GameStatus.started.equals(this.mGameStatus)) {
            Bitmap firstBitmap = this.mBitmaps.get(0);
            int bitmapWidth = (int) (((float) firstBitmap.getWidth()) * xScale);
            int bitmapHeight = (int) (((float) firstBitmap.getHeight()) * yScale);
            int moveWidth = this.mGameType.getQuantity() * bitmapWidth;
            int moveHeight = this.mGameType.getQuantity() * bitmapHeight;
            this.startPuzzleX = (this.screenWidth - moveWidth) / 2;
            this.startPuzzleY = (this.screenHeight - moveHeight) / 2;
            this.endPuzzleX = Integer.valueOf(this.startPuzzleX + moveWidth);
            this.endPuzzleY = Integer.valueOf(this.startPuzzleY + moveHeight);
            MoveBitmap.Parameters parameters = new MoveBitmap.Parameters(moveWidth, moveHeight, xScale, yScale, bitmapWidth, bitmapHeight);
            if (this.mMoveBitmap == null) {
                this.mMoveBitmap = new MoveBitmap(this.mBitmaps, parameters);
                if (this.mShuffleBitmap != null) {
                    this.mMoveBitmap.setShuffleBitmap(this.mShuffleBitmap);
                    this.mMoveBitmap.setShuffled(true);
                } else {
                    this.mMoveBitmap.shuffle();
                }
            } else {
                this.mMoveBitmap.setParameters(parameters);
            }
            this.mMoveBitmap.prepareViewBitmap(this.orientation);
        } else if (GameStatus.beforeStart.equals(this.mGameStatus)) {
            Toast.makeText(this.mContext, this.mContext.getString(R.string.f9gamestart), 1).show();
        }
    }

    private void cutBitmap() {
        this.mBitmaps = ResourceLauncher.getBitmapsCut();
        if (this.mBitmaps == null) {
            this.mBitmaps = new ArrayList();
            fillBitmaps(0, 0, this.mScratch.getWidth() / this.mGameType.getQuantity(), this.mScratch.getHeight() / this.mGameType.getQuantity());
            ResourceLauncher.setBitmapsCut(this.mBitmaps);
        }
    }

    private void fillBitmaps(int startX, int startY, int countX, int countY) {
        int nextX;
        int nextY;
        this.mBitmaps.add(Bitmap.createBitmap(this.mScratch, startX, startY, countX, countY));
        if (startX + (countX * 2) <= this.mScratch.getWidth()) {
            nextX = startX + countX;
            nextY = startY;
        } else if (startY + (countY * 2) <= this.mScratch.getHeight()) {
            nextX = 0;
            nextY = startY + countY;
        } else {
            return;
        }
        fillBitmaps(nextX, nextY, countX, countY);
    }

    private void resetLastXY() {
        this.lastX = null;
        this.lastY = null;
    }

    private void resetTouchedXY() {
        this.touchedX = null;
        this.touchedY = null;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (GameStatus.beforeStart.equals(this.mGameStatus)) {
            if (event.getAction() == 0) {
                setGameStatus(GameStatus.started);
                calculateVisualParameters();
            }
        } else if (GameStatus.started.equals(this.mGameStatus)) {
            if (event.getAction() == 0) {
                resetLastXY();
                this.mCurrentGraphic = null;
                this.touchedX = Integer.valueOf((int) event.getX());
                this.touchedY = Integer.valueOf((int) event.getY());
                if (this.touchedX.intValue() > this.startPuzzleX && this.touchedX.intValue() < this.endPuzzleX.intValue() && this.touchedY.intValue() > this.startPuzzleY && this.touchedY.intValue() < this.endPuzzleY.intValue()) {
                    this.mCurrentGraphic = new Graphic(this.touchedX.intValue(), this.touchedY.intValue());
                }
            } else if (event.getAction() == 2) {
                if (this.mCurrentGraphic != null) {
                    this.mCurrentGraphic.setX((int) event.getX());
                    this.mCurrentGraphic.setY((int) event.getY());
                }
            } else if (event.getAction() == 1) {
                resetTouchedXY();
                this.lastX = Integer.valueOf((int) event.getX());
                this.lastY = Integer.valueOf((int) event.getY());
            }
        } else if (GameStatus.wallpaper.equals(getGameStatus()) && event.getAction() == 0) {
            WallpaperMenu.initWallpaperDialog(this.mContext, this.mItemPosition, true).show();
        }
        if (!GameStatus.wallpaper.equals(this.mGameStatus)) {
            displayAction();
        }
        return true;
    }

    private void checkCompleted() {
        int i = 0;
        while (i < this.mShuffleBitmap.size()) {
            if (i == this.mShuffleBitmap.get(i).intValue()) {
                i++;
            } else {
                return;
            }
        }
        setGameStatus(GameStatus.completed);
    }

    private void displayAction() {
        Canvas c = getHolder().lockCanvas(null);
        onDraw(c);
        getHolder().unlockCanvasAndPost(c);
        if (GameStatus.completed.equals(this.mGameStatus)) {
            WallpaperDbManager.saveItem(this.mItemPosition);
            setGameStatus(GameStatus.wallpaper);
            Toast.makeText(this.mContext, this.mContext.getString(R.string.f10gamecompleted), 1).show();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        displayAction();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public List<Integer> getShuffleBitmap() {
        return this.mShuffleBitmap;
    }

    public void setShufleBitmap(List<Integer> shufleBitmap) {
        this.mShuffleBitmap = shufleBitmap;
    }

    public GameStatus getGameStatus() {
        return this.mGameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.mGameStatus = gameStatus;
    }
}
