package com.christmasbunnypuzzle.game;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ResourceLauncher {
    private static Bitmap mBackground;
    private static Bitmap mBackgroundLandscape;
    private static Drawable mBackgroundMenu;
    private static Bitmap mBackgroundPortrait;
    private static Drawable mBackgroundPuzzleBottom;
    private static List<Bitmap> mBitmapsCut;
    private static Map<Integer, Bitmap> mBitmapsShuffleLandscape;
    private static Map<Integer, Bitmap> mBitmapsShufflePortrait;
    private static Bitmap mScratch;
    private static Bitmap mScratchLandscape;
    private static Bitmap mScratchPortrait;

    public static void invalidateResources() {
        if (mScratch != null) {
            mScratch = null;
        }
        if (mScratchPortrait != null) {
            mScratchPortrait.recycle();
            mScratchPortrait = null;
        }
        if (mScratchLandscape != null) {
            mScratchLandscape.recycle();
            mScratchLandscape = null;
        }
        if (mBitmapsCut != null) {
            clearBitmaps(mBitmapsCut);
            mBitmapsCut.clear();
            mBitmapsCut = null;
        }
        if (mBitmapsShufflePortrait != null) {
            clearBitmaps(mBitmapsShufflePortrait.values());
            mBitmapsShufflePortrait.clear();
            mBitmapsShufflePortrait = null;
        }
        if (mBitmapsShuffleLandscape != null) {
            clearBitmaps(mBitmapsShuffleLandscape.values());
            mBitmapsShuffleLandscape.clear();
            mBitmapsShuffleLandscape = null;
        }
    }

    private static void clearBitmaps(Collection<Bitmap> bitmaps) {
        for (Bitmap bitmap : bitmaps) {
            bitmap.recycle();
        }
    }

    public static Bitmap getScratch() {
        return mScratch;
    }

    public static void setScratch(Bitmap mScratch2) {
        mScratch = mScratch2;
    }

    public static Bitmap getScratchPortrait() {
        return mScratchPortrait;
    }

    public static void setScratchPortrait(Bitmap mScratchPortrait2) {
        mScratchPortrait = mScratchPortrait2;
    }

    public static Bitmap getScratchLandscape() {
        return mScratchLandscape;
    }

    public static void setScratchLandscape(Bitmap mScratchLandscape2) {
        mScratchLandscape = mScratchLandscape2;
    }

    public static List<Bitmap> getBitmapsCut() {
        return mBitmapsCut;
    }

    public static void setBitmapsCut(List<Bitmap> bitmapsPortrait) {
        mBitmapsCut = bitmapsPortrait;
    }

    public static Map<Integer, Bitmap> getBitmapsShufflePortrait() {
        return mBitmapsShufflePortrait;
    }

    public static void setBitmapsShufflePortrait(Map<Integer, Bitmap> bitmapsShufflePortrait) {
        mBitmapsShufflePortrait = bitmapsShufflePortrait;
    }

    public static Map<Integer, Bitmap> getBitmapsShuffleLandscape() {
        return mBitmapsShuffleLandscape;
    }

    public static void setBitmapsShuffleLandscape(Map<Integer, Bitmap> bitmapsShuffleLandscape) {
        mBitmapsShuffleLandscape = bitmapsShuffleLandscape;
    }

    public static Bitmap getBackgroundPortrait() {
        return mBackgroundPortrait;
    }

    public static void setBackgroundPortrait(Bitmap mBackgroundPortrait2) {
        mBackgroundPortrait = mBackgroundPortrait2;
    }

    public static Bitmap getBackgroundLandscape() {
        return mBackgroundLandscape;
    }

    public static void setBackgroundLandscape(Bitmap mBackgroundLandscape2) {
        mBackgroundLandscape = mBackgroundLandscape2;
    }

    public static Bitmap getBackground() {
        return mBackground;
    }

    public static void setBackground(Bitmap mBackground2) {
        mBackground = mBackground2;
    }

    public static Drawable getBackgroundPuzzleBottom() {
        return mBackgroundPuzzleBottom;
    }

    public static void setBackgroundPuzzleBottom(Drawable backgroundPuzzleBottom) {
        mBackgroundPuzzleBottom = backgroundPuzzleBottom;
    }

    public static Drawable getBackgroundMenu() {
        return mBackgroundMenu;
    }

    public static void setBackgroundMenu(Drawable backgroundMenu) {
        mBackgroundMenu = backgroundMenu;
    }
}
