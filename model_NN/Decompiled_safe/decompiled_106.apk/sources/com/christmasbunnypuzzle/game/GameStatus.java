package com.christmasbunnypuzzle.game;

public enum GameStatus {
    beforeStart,
    started,
    completed,
    wallpaper
}
