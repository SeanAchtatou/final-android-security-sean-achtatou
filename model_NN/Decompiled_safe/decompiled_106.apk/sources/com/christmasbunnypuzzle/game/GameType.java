package com.christmasbunnypuzzle.game;

import com.christmasbunnypuzzle.R;

public enum GameType {
    x3(R.string.f4gamex3, 3),
    x4(R.string.f5gamex4, 4),
    x5(R.string.f6gamex5, 5),
    x6(R.string.f7gamex6, 6);
    
    private int gameName;
    private int quantity;

    private GameType(int pGameName, int quantity2) {
        this.gameName = pGameName;
        this.quantity = quantity2;
    }

    public int getGameName() {
        return this.gameName;
    }

    public void setGameName(int gameName2) {
        this.gameName = gameName2;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity2) {
        this.quantity = quantity2;
    }
}
