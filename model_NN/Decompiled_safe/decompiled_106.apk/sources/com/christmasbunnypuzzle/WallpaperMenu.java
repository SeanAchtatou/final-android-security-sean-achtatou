package com.christmasbunnypuzzle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import com.admob.android.ads.AdView;
import com.christmasbunnypuzzle.db.WallpaperDbManager;
import com.christmasbunnypuzzle.game.GameType;
import com.christmasbunnypuzzle.game.ResourceLauncher;
import com.christmasbunnypuzzle.game.WallpaperGame;

public class WallpaperMenu extends Activity {
    public static final int AD_REQUEST_INTERVAL = 60;
    public static final String ITEM_GAME_TYPE = "itemGameType";
    public static final String ITEM_POSITION = "itemPosition";
    public static final String LOADED = "loaded";
    private static final String TAG = "WallpaperMenu";
    public static ProgressDialog mProgressDialog;
    public static AlertDialog wallpaperGameDialog;
    private AlertDialog closeDialog;
    /* access modifiers changed from: private */
    public AlertDialog gameTypeDialog;
    /* access modifiers changed from: private */
    public ImageView imageView;
    /* access modifiers changed from: private */
    public int itemPosition = 0;
    private Boolean loaded = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.menu);
        if (ResourceLauncher.getBackgroundMenu() == null) {
            ResourceLauncher.setBackgroundMenu(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.background)));
        }
        ((LinearLayout) findViewById(R.id.root)).setBackgroundDrawable(ResourceLauncher.getBackgroundMenu());
        if (savedInstanceState != null) {
            this.loaded = Boolean.valueOf(savedInstanceState.getBoolean(LOADED));
        }
        Bundle extras = getIntent().getExtras();
        this.itemPosition = extras != null ? extras.getInt(ITEM_POSITION) : 0;
        WallpaperDbManager.initWallpaperDbManager(this);
        if (!this.loaded.booleanValue() || ImageAdapter.getImageAdapter() == null) {
            mProgressDialog = ProgressDialog.show(this, "", getString(R.string.f1apploading), true);
            new Thread() {
                public void run() {
                    try {
                        ImageAdapter.initImageAdapter(WallpaperMenu.this, WallpaperMenu.this.obtainStyledAttributes(R.styleable.MenuTheme));
                    } catch (Exception e) {
                        Log.e(WallpaperMenu.TAG, e.toString());
                    }
                    WallpaperMenu.mProgressDialog.dismiss();
                }
            }.start();
            mProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    WallpaperMenu.this.initCreation();
                }
            });
            return;
        }
        initCreation();
    }

    /* access modifiers changed from: private */
    public void initCreation() {
        this.loaded = true;
        ((AdView) findViewById(R.id.ad)).setRequestInterval(60);
        this.imageView = (ImageView) findViewById(R.id.image);
        ImageAdapter imageAdapter = ImageAdapter.getImageAdapter();
        if (imageAdapter != null) {
            initGameTypeDialog();
            initWallpaperGameDialog();
            initCloseDialog();
            if (imageAdapter.getCount() != 0) {
                this.imageView.setImageBitmap(getImageBitmap());
                this.imageView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        WallpaperMenu.this.showGalleryMenu();
                    }
                });
            }
            Gallery g = (Gallery) findViewById(R.id.gallery);
            g.setAdapter((SpinnerAdapter) imageAdapter);
            g.setSelection(this.itemPosition);
            g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    if (position == WallpaperMenu.this.itemPosition) {
                        WallpaperMenu.this.showGalleryMenu();
                    }
                }
            });
            g.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    ResourceLauncher.invalidateResources();
                    if (position != WallpaperMenu.this.itemPosition) {
                        int unused = WallpaperMenu.this.itemPosition = position;
                        WallpaperMenu.this.imageView.setImageBitmap(WallpaperMenu.this.getImageBitmap());
                    }
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void showGalleryMenu() {
        if (WallpaperDbManager.getItemById(this.itemPosition) != null) {
            wallpaperGameDialog.show();
        } else {
            this.gameTypeDialog.show();
        }
    }

    /* access modifiers changed from: private */
    public Bitmap getImageBitmap() {
        return ImageAdapter.getBitmapByPosition(this.itemPosition);
    }

    private void initGameTypeDialog() {
        CharSequence[] items = {getString(GameType.x3.getGameName()), getString(GameType.x4.getGameName()), getString(GameType.x5.getGameName()), getString(GameType.x6.getGameName())};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.f8gametype));
        builder.setIcon((int) R.drawable.icon);
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                ResourceLauncher.invalidateResources();
                Intent intent = new Intent(WallpaperMenu.this, WallpaperPuzzle.class);
                intent.setFlags(67108864);
                intent.putExtra(WallpaperMenu.ITEM_GAME_TYPE, GameType.values()[item]);
                intent.putExtra(WallpaperMenu.ITEM_POSITION, WallpaperMenu.this.itemPosition);
                WallpaperMenu.this.startActivity(intent);
                dialog.cancel();
            }
        });
        this.gameTypeDialog = builder.create();
    }

    private void initWallpaperGameDialog() {
        CharSequence[] items = {getString(WallpaperGame.wallpaper.getAction()), getString(WallpaperGame.play.getAction())};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.f18wallpaperchooseaction);
        builder.setIcon((int) R.drawable.icon);
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (WallpaperGame.wallpaper.equals(WallpaperGame.values()[item])) {
                    WallpaperMenu.initWallpaperDialog(WallpaperMenu.this, WallpaperMenu.this.itemPosition, false).show();
                } else {
                    WallpaperMenu.this.gameTypeDialog.show();
                }
                dialog.cancel();
            }
        });
        wallpaperGameDialog = builder.create();
    }

    public void initCloseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.f2appquit);
        builder.setIcon((int) R.drawable.icon);
        builder.setMessage((int) R.string.f3appquittext).setCancelable(true).setPositiveButton((int) R.string.f16dialogyes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                WallpaperMenu.this.finish();
            }
        }).setNegativeButton((int) R.string.f17dialogno, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        this.closeDialog = builder.create();
    }

    public static AlertDialog initWallpaperDialog(final Activity activity, final int itemPosition2, final boolean showMainMenu) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle((int) R.string.f19wallpaperchooseset);
        builder.setIcon((int) R.drawable.icon);
        builder.setMessage((int) R.string.f13wallpaperdialog).setCancelable(true).setPositiveButton((int) R.string.f16dialogyes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                final Bitmap bitmap = ImageAdapter.getWallpaperByPosition(activity, itemPosition2);
                WallpaperMenu.mProgressDialog = ProgressDialog.show(activity, "", activity.getString(R.string.f14wallpaperloading), true);
                dialog.dismiss();
                final AsyncTask asyncTask = new AsyncTask() {
                    /* access modifiers changed from: protected */
                    public Object doInBackground(Object... objects) {
                        return null;
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(Object result) {
                        if (WallpaperMenu.mProgressDialog.isShowing()) {
                            WallpaperMenu.mProgressDialog.dismiss();
                            WallpaperMenu.showMainMenu(activity, itemPosition2, showMainMenu);
                            Toast.makeText(activity, activity.getString(R.string.f15wallpaperset), 0).show();
                        }
                    }
                };
                new Thread() {
                    public void run() {
                        try {
                            activity.getApplicationContext().setWallpaper(bitmap);
                            bitmap.recycle();
                            asyncTask.execute(new Object[0]);
                        } catch (Exception e) {
                            Log.e(WallpaperMenu.TAG, e.getMessage());
                        }
                    }
                }.start();
            }
        }).setNegativeButton((int) R.string.f17dialogno, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                WallpaperMenu.showMainMenu(activity, itemPosition2, showMainMenu);
                dialog.cancel();
            }
        });
        return builder.create();
    }

    public static void showMainMenu(Activity activity, int itemPosition2, boolean showMainMenu) {
        if (showMainMenu) {
            Intent intent = new Intent(activity, WallpaperMenu.class);
            intent.setFlags(67108864);
            intent.putExtra(ITEM_POSITION, itemPosition2);
            activity.startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ITEM_POSITION, this.itemPosition);
        outState.putBoolean(LOADED, this.loaded.booleanValue());
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        this.itemPosition = ((Integer) inState.get(ITEM_POSITION)).intValue();
        if (this.imageView != null) {
            this.imageView.setImageBitmap(getImageBitmap());
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    public void onBackPressed() {
        this.closeDialog.show();
    }
}
