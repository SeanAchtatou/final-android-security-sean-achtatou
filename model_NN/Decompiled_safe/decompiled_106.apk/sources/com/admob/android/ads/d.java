package com.admob.android.ads;

import android.util.Log;
import com.admob.android.ads.InterstitialAd;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/* compiled from: AnalyticsData */
public final class d implements h {
    private final String a;
    private final String b;
    private final String c;
    private String d;
    private a e = null;
    private HashSet<e> f = new HashSet<>();
    private int g = 0;

    /* compiled from: AnalyticsData */
    public interface a {
    }

    public d(String str, String str2, String str3, String str4) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
    }

    public final void a(String str, Map<String, String> map) {
        if (this.c != null && str != null) {
            this.g++;
            String a2 = a(str, map, this.g == 1);
            if (a2 != null) {
                if (this.f != null) {
                    e a3 = g.a("http://r.admob.com/ad_source.php", "AnalyticsData", this.d, this, 5000, null, a2);
                    this.f.add(a3);
                    a3.f();
                }
                if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                    Log.d(AdManager.LOG, "Analytics event " + this.c + "/" + str + " data:" + a2 + " has been recorded.");
                }
            } else if (InterstitialAd.c.a(AdManager.LOG, 6)) {
                Log.e(AdManager.LOG, "Could not create analytics URL.  Analytics data not tracked.");
            }
        }
    }

    private String a(String str, Map<String, String> map, boolean z) {
        Set<String> keySet;
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("rt=1&ex=1");
            sb.append("&a=").append(this.a);
            sb.append("&p=").append(URLEncoder.encode(str, "UTF-8"));
            sb.append("&o=").append(this.d);
            sb.append("&v=").append(AdManager.SDK_VERSION);
            long currentTimeMillis = System.currentTimeMillis();
            sb.append("&z").append("=").append(currentTimeMillis / 1000).append(".").append(currentTimeMillis % 1000);
            sb.append("&h%5BHTTP_HOST%5D=").append(URLEncoder.encode(this.c, "UTF-8"));
            sb.append("&h%5BHTTP_REFERER%5D=http%3A%2F%2F").append(this.b);
            if (z) {
                sb.append("&startvisit=1");
            }
            if (!(map == null || (keySet = map.keySet()) == null)) {
                for (String next : keySet) {
                    sb.append("&").append(next).append("=").append(URLEncoder.encode(map.get(next)));
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e2) {
            return null;
        }
    }

    public final void a(e eVar, Exception exc) {
        if (InterstitialAd.c.a(AdManager.LOG, 5)) {
            Log.w(AdManager.LOG, "analytics request failed for " + eVar.b(), exc);
        }
        b(eVar);
    }

    public final void a(e eVar) {
        if (InterstitialAd.c.a(AdManager.LOG, 2)) {
            Log.v(AdManager.LOG, "Analytics event " + eVar.b() + " has been recorded.");
            int size = this.f.size();
            if (size > 0) {
                Log.v(AdManager.LOG, "Pending Analytics requests: " + size);
            }
        }
        b(eVar);
    }

    private void b(e eVar) {
        this.f.remove(eVar);
    }
}
