package com.admob.android.ads;

import android.os.Bundle;

/* compiled from: ClickURL */
public final class w implements n {
    public String a;
    public boolean b;

    public w() {
        this.a = null;
        this.b = false;
    }

    public w(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("u", this.a);
        bundle.putBoolean("p", this.b);
        return bundle;
    }

    public final int hashCode() {
        if (this.a != null) {
            return this.a.hashCode();
        }
        return super.hashCode();
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (!(obj instanceof w)) {
            return false;
        }
        w wVar = (w) obj;
        boolean z3 = this.a == null && wVar.a != null;
        if (this.a == null || this.a.equals(wVar.a)) {
            z = false;
        } else {
            z = true;
        }
        if (this.b != wVar.b) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z3 || z || z2) {
            return false;
        }
        return true;
    }
}
