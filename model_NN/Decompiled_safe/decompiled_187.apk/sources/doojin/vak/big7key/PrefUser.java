package doojin.vak.big7key;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;

public class PrefUser extends PreferenceActivity {
    PreferenceCategory category1;
    SharedPreferences mainPreference;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.pref_user);
        this.mainPreference = PreferenceManager.getDefaultSharedPreferences(this);
        this.category1 = (PreferenceCategory) findPreference("catHaptic");
        this.category1.setEnabled(this.mainPreference.getBoolean("hapticOn", true));
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference.equals((CheckBoxPreference) findPreference("hapticOn"))) {
            this.category1.setEnabled(this.mainPreference.getBoolean("hapticOn", true));
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
}
