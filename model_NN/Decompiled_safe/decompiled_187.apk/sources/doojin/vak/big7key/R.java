package doojin.vak.big7key;

public final class R {

    public static final class array {
        public static final int text_length = 2131165184;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int candidate_background = 2131034115;
        public static final int candidate_normal = 2131034112;
        public static final int candidate_other = 2131034114;
        public static final int candidate_recommended = 2131034113;
    }

    public static final class dimen {
        public static final int candidate_font_height = 2131099649;
        public static final int candidate_vertical_padding = 2131099650;
        public static final int key_height = 2131099648;
    }

    public static final class drawable {
        public static final int big7key = 2130837504;
        public static final int bigroid = 2130837505;
        public static final int key_a = 2130837506;
        public static final int key_e = 2130837507;
        public static final int key_ent = 2130837508;
        public static final int key_i = 2130837509;
        public static final int key_o = 2130837510;
        public static final int key_sp = 2130837511;
        public static final int key_t = 2130837512;
        public static final int nkey_0 = 2130837513;
        public static final int nkey_1 = 2130837514;
        public static final int nkey_2 = 2130837515;
        public static final int nkey_3 = 2130837516;
        public static final int nkey_4 = 2130837517;
        public static final int nkey_5 = 2130837518;
        public static final int nkey_6 = 2130837519;
        public static final int nkey_7 = 2130837520;
        public static final int nkey_8 = 2130837521;
        public static final int nkey_9 = 2130837522;
        public static final int nkey_hash = 2130837523;
        public static final int nkey_star = 2130837524;
        public static final int pv_a = 2130837525;
        public static final int pv_e = 2130837526;
        public static final int pv_ent = 2130837527;
        public static final int pv_i = 2130837528;
        public static final int pv_o = 2130837529;
        public static final int pv_sp = 2130837530;
        public static final int pv_t = 2130837531;
        public static final int sym_keyboard_delete = 2130837532;
        public static final int sym_keyboard_done = 2130837533;
        public static final int sym_keyboard_return = 2130837534;
        public static final int sym_keyboard_search = 2130837535;
        public static final int sym_keyboard_shift = 2130837536;
        public static final int sym_keyboard_space = 2130837537;
    }

    public static final class id {
        public static final int keyboard = 2131296256;
    }

    public static final class layout {
        public static final int input = 2130903040;
        public static final int pref_user = 2130903041;
    }

    public static final class string {
        public static final int ime_name = 2131230720;
        public static final int label_go_key = 2131230722;
        public static final int label_next_key = 2131230723;
        public static final int label_send_key = 2131230724;
        public static final int word_separators = 2131230721;
    }

    public static final class xml {
        public static final int bigkey = 2130968576;
        public static final int bigkeyland = 2130968577;
        public static final int bigkeynum = 2130968578;
        public static final int method = 2130968579;
        public static final int qwerty = 2130968580;
        public static final int symbols = 2130968581;
        public static final int symbols_shift = 2130968582;
    }
}
