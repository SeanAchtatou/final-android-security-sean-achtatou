package doojin.vak.big7key;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.preference.PreferenceManager;
import android.text.method.MetaKeyKeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import java.util.ArrayList;
import java.util.List;

public class SoftKeyboard extends InputMethodService implements KeyboardView.OnKeyboardActionListener {
    static final boolean DEBUG = false;
    static final int KEY_0 = 13;
    static final int KEY_1 = 0;
    static final int KEY_123 = 10;
    static final int KEY_2 = 1;
    static final int KEY_3 = 2;
    static final int KEY_4 = 5;
    static final int KEY_5 = 6;
    static final int KEY_6 = 7;
    static final int KEY_7 = 10;
    static final int KEY_8 = 11;
    static final int KEY_9 = 12;
    static final int KEY_A = 2;
    static final int KEY_E = 1;
    static final int KEY_ENT = 6;
    static final int KEY_HASH = 3;
    static final int KEY_I = 4;
    static final int KEY_O = 8;
    static final int KEY_SH = 0;
    static final int KEY_SP = 5;
    static final int KEY_STAR = 8;
    static final int KEY_T = 9;
    static final int KEY_USER = 7;
    static final boolean PROCESS_HARD_KEYS = true;
    static final int TEXT_LENGTH = 8;
    static final int VAL_123 = -2;
    static final int VAL_ENT = 10;
    static final int VAL_SH = -1;
    static final int VAL_SP = 32;
    static final int VAL_USER = -34;
    private int keyIn;
    private int keyOut;
    private int[] keycode_dummy;
    private List<Keyboard.Key> keys;
    private boolean mBigCLock;
    private LatinKeyboard mBigKeyboard;
    private LatinKeyboard mBigLandKeyboard;
    private LatinKeyboard mBigNumKeyboard;
    private CandidateView mCandidateView;
    private boolean mCapsLock;
    private boolean mCompletionOn;
    private CompletionInfo[] mCompletions;
    private StringBuilder mComposing = new StringBuilder();
    private LatinKeyboard mCurKeyboard;
    private KeyboardView mInputView;
    private int mLastDisplayWidth;
    private long mLastShiftTime;
    private long mMetaState;
    private boolean mPredictionOn;
    private LatinKeyboard mQwertyKeyboard;
    private boolean mShiftToggle = DEBUG;
    private LatinKeyboard mSymbolsKeyboard;
    private LatinKeyboard mSymbolsShiftedKeyboard;
    private String mWordSeparators;
    private List<Keyboard.Key> nkeys;
    private SharedPreferences sharedPref;

    public void onCreate() {
        super.onCreate();
        this.mWordSeparators = getResources().getString(R.string.word_separators);
        this.sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public void onInitializeInterface() {
        if (this.mBigKeyboard != null) {
            int displayWidth = getMaxWidth();
            if (displayWidth != this.mLastDisplayWidth) {
                this.mLastDisplayWidth = displayWidth;
            } else {
                return;
            }
        }
        this.mQwertyKeyboard = new LatinKeyboard(this, R.xml.qwerty);
        this.mSymbolsKeyboard = new LatinKeyboard(this, R.xml.symbols);
        this.mSymbolsShiftedKeyboard = new LatinKeyboard(this, R.xml.symbols_shift);
        this.mBigKeyboard = new LatinKeyboard(this, R.xml.bigkey);
        this.mBigNumKeyboard = new LatinKeyboard(this, R.xml.bigkeynum);
        this.mBigLandKeyboard = new LatinKeyboard(this, R.xml.bigkeyland);
        this.keys = this.mBigKeyboard.getKeys();
        this.nkeys = this.mBigNumKeyboard.getKeys();
    }

    public View onCreateInputView() {
        this.mInputView = (KeyboardView) getLayoutInflater().inflate((int) R.layout.input, (ViewGroup) null);
        this.mInputView.setOnKeyboardActionListener(this);
        this.mInputView.setKeyboard(this.mBigKeyboard);
        return this.mInputView;
    }

    public void onStartInput(EditorInfo attribute, boolean restarting) {
        super.onStartInput(attribute, restarting);
        this.mComposing.setLength(0);
        updateCandidates();
        this.mPredictionOn = DEBUG;
        this.mCompletionOn = DEBUG;
        this.mCompletions = null;
        switch (attribute.inputType & 15) {
            case 1:
                if (getResources().getConfiguration().orientation == 1) {
                    this.mCurKeyboard = this.mBigKeyboard;
                    return;
                } else {
                    this.mCurKeyboard = this.mQwertyKeyboard;
                    return;
                }
            case 2:
            case KEY_HASH /*3*/:
            case KEY_I /*4*/:
                if (getResources().getConfiguration().orientation == 1) {
                    this.mCurKeyboard = this.mBigNumKeyboard;
                    return;
                } else {
                    this.mCurKeyboard = this.mSymbolsKeyboard;
                    return;
                }
            default:
                this.mCurKeyboard = this.mBigKeyboard;
                return;
        }
    }

    public void onFinishInput() {
        super.onFinishInput();
        this.mComposing.setLength(0);
        this.mCurKeyboard = this.mBigKeyboard;
        if (this.mInputView != null) {
            this.mInputView.closing();
        }
    }

    public void onStartInputView(EditorInfo attribute, boolean restarting) {
        super.onStartInputView(attribute, restarting);
        this.mInputView.setKeyboard(this.mCurKeyboard);
        this.mInputView.closing();
    }

    public void onUpdateSelection(int oldSelStart, int oldSelEnd, int newSelStart, int newSelEnd, int candidatesStart, int candidatesEnd) {
        super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd, candidatesStart, candidatesEnd);
        if (this.mComposing.length() <= 0) {
            return;
        }
        if (newSelStart != candidatesEnd || newSelEnd != candidatesEnd) {
            this.mComposing.setLength(0);
            updateCandidates();
            InputConnection ic = getCurrentInputConnection();
            if (ic != null) {
                ic.finishComposingText();
            }
        }
    }

    public void onDisplayCompletions(CompletionInfo[] completions) {
        if (this.mCompletionOn) {
            this.mCompletions = completions;
            if (completions == null) {
                setSuggestions(null, DEBUG, DEBUG);
                return;
            }
            List<String> stringList = new ArrayList<>();
            int i = 0;
            while (true) {
                if (i >= (completions != null ? completions.length : 0)) {
                    setSuggestions(stringList, PROCESS_HARD_KEYS, PROCESS_HARD_KEYS);
                    return;
                }
                CompletionInfo ci = completions[i];
                if (ci != null) {
                    stringList.add(ci.getText().toString());
                }
                i++;
            }
        }
    }

    private boolean translateKeyDown(int keyCode, KeyEvent event) {
        int composed;
        this.mMetaState = MetaKeyKeyListener.handleKeyDown(this.mMetaState, keyCode, event);
        int c = event.getUnicodeChar(MetaKeyKeyListener.getMetaState(this.mMetaState));
        this.mMetaState = MetaKeyKeyListener.adjustMetaAfterKeypress(this.mMetaState);
        InputConnection ic = getCurrentInputConnection();
        keyDownUp(43);
        if (c == 0 || ic == null) {
            return DEBUG;
        }
        if ((Integer.MIN_VALUE & c) != 0) {
            c &= Integer.MAX_VALUE;
        }
        if (this.mComposing.length() > 0 && (composed = KeyEvent.getDeadChar(this.mComposing.charAt(this.mComposing.length() - 1), c)) != 0) {
            c = composed;
            this.mComposing.setLength(this.mComposing.length() - 1);
        }
        onKey(c, null);
        return PROCESS_HARD_KEYS;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KEY_I /*4*/:
                if (event.getRepeatCount() == 0 && this.mInputView != null && this.mInputView.handleBack()) {
                    return PROCESS_HARD_KEYS;
                }
            case 66:
                return DEBUG;
            case 67:
                if (this.mComposing.length() > 0) {
                    onKey(-5, null);
                    return PROCESS_HARD_KEYS;
                }
                break;
            default:
                if (this.mPredictionOn && translateKeyDown(keyCode, event)) {
                    return PROCESS_HARD_KEYS;
                }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    private void commitTyped(InputConnection inputConnection) {
        if (this.mComposing.length() > 0) {
            inputConnection.commitText(this.mComposing, this.mComposing.length());
            this.mComposing.setLength(0);
            updateCandidates();
        }
    }

    private void updateShiftKeyState(EditorInfo attr) {
        EditorInfo ei;
        if (attr != null && this.mInputView != null && this.mQwertyKeyboard == this.mInputView.getKeyboard() && (ei = getCurrentInputEditorInfo()) != null && ei.inputType != 0) {
            getCurrentInputConnection().getCursorCapsMode(attr.inputType);
        }
    }

    private boolean isAlphabet(int code) {
        if (Character.isLetter(code)) {
            return PROCESS_HARD_KEYS;
        }
        return DEBUG;
    }

    private void keyDownUp(int keyEventCode) {
        getCurrentInputConnection().sendKeyEvent(new KeyEvent(0, keyEventCode));
        getCurrentInputConnection().sendKeyEvent(new KeyEvent(1, keyEventCode));
    }

    private void sendKey(int keyCode) {
        switch (keyCode) {
            case 10:
                keyDownUp(66);
                return;
            default:
                if (keyCode < 48 || keyCode > 57) {
                    getCurrentInputConnection().commitText(String.valueOf((char) keyCode), 1);
                    return;
                } else {
                    keyDownUp((keyCode - 48) + 7);
                    return;
                }
        }
    }

    public void onKey(int primaryCode, int[] keyCodes) {
        this.keycode_dummy = keyCodes;
        Keyboard current = this.mInputView.getKeyboard();
        if (isWordSeparator(primaryCode)) {
            if (this.mComposing.length() > 0) {
                commitTyped(getCurrentInputConnection());
            }
            if (primaryCode == 10 && current != this.mBigKeyboard) {
                sendKey(10);
            }
        } else if (primaryCode == -5) {
            handleBackspace();
        } else if (primaryCode == VAL_SH) {
            handleShift();
        } else if (primaryCode == -3) {
            handleClose();
        } else if (primaryCode == VAL_123 && this.mInputView != null) {
            if (current == this.mSymbolsKeyboard || current == this.mSymbolsShiftedKeyboard) {
                current = this.mQwertyKeyboard;
            } else if (current == this.mQwertyKeyboard) {
                current = this.mSymbolsKeyboard;
            } else if (current != this.mBigKeyboard) {
                current = this.mBigKeyboard;
            }
            this.mInputView.setKeyboard(current);
            if (current == this.mSymbolsKeyboard) {
                current.setShifted(DEBUG);
            }
        } else if (current == this.mSymbolsKeyboard || current == this.mSymbolsShiftedKeyboard || current == this.mQwertyKeyboard) {
            handleCharacter(primaryCode, keyCodes);
        }
    }

    public void onText(CharSequence text) {
        InputConnection ic = getCurrentInputConnection();
        if (ic != null) {
            ic.beginBatchEdit();
            if (this.mComposing.length() > 0) {
                commitTyped(ic);
            }
            ic.commitText(text, 0);
            ic.endBatchEdit();
        }
    }

    private void updateCandidates() {
        if (this.mCompletionOn) {
            return;
        }
        if (this.mComposing.length() > 0) {
            ArrayList<String> list = new ArrayList<>();
            list.add(this.mComposing.toString());
            setSuggestions(list, PROCESS_HARD_KEYS, PROCESS_HARD_KEYS);
            return;
        }
        setSuggestions(null, DEBUG, DEBUG);
    }

    public void setSuggestions(List<String> suggestions, boolean completions, boolean typedWordValid) {
        if (suggestions != null && suggestions.size() > 0) {
            setCandidatesViewShown(PROCESS_HARD_KEYS);
        } else if (isExtractViewShown()) {
            setCandidatesViewShown(PROCESS_HARD_KEYS);
        }
        if (this.mCandidateView != null) {
            this.mCandidateView.setSuggestions(suggestions, completions, typedWordValid);
        }
    }

    private void handleBackspace() {
        int length = this.mComposing.length();
        if (length > 1) {
            this.mComposing.delete(length - 1, length);
            getCurrentInputConnection().setComposingText(this.mComposing, 1);
            updateCandidates();
        } else if (length > 0) {
            this.mComposing.setLength(0);
            getCurrentInputConnection().commitText("", 0);
            updateCandidates();
        } else {
            keyDownUp(67);
        }
        updateShiftKeyState(getCurrentInputEditorInfo());
    }

    private void handleShift() {
        if (this.mInputView != null && this.mQwertyKeyboard == this.mInputView.getKeyboard()) {
            checkToggleCapsLock();
            this.mInputView.setShifted((this.mCapsLock || !this.mInputView.isShifted()) ? PROCESS_HARD_KEYS : DEBUG);
        }
    }

    private void handleCharacter(int primaryCode, int[] keyCodes) {
        if (isInputViewShown() && this.mInputView.isShifted()) {
            primaryCode = Character.toUpperCase(primaryCode);
        }
        if (!isAlphabet(primaryCode) || !this.mPredictionOn) {
            getCurrentInputConnection().commitText(String.valueOf((char) primaryCode), 1);
            return;
        }
        this.mComposing.append((char) primaryCode);
        getCurrentInputConnection().setComposingText(this.mComposing, 1);
        updateShiftKeyState(getCurrentInputEditorInfo());
        updateCandidates();
    }

    private void handleClose() {
        commitTyped(getCurrentInputConnection());
        requestHideSelf(0);
        this.mInputView.closing();
    }

    private void checkToggleCapsLock() {
        long now = System.currentTimeMillis();
        if (this.mLastShiftTime + 100 > now) {
            this.mCapsLock = this.mCapsLock ? DEBUG : PROCESS_HARD_KEYS;
            this.mLastShiftTime = 0;
            return;
        }
        this.mLastShiftTime = now;
    }

    private String getWordSeparators() {
        return this.mWordSeparators;
    }

    public boolean isWordSeparator(int code) {
        return getWordSeparators().contains(String.valueOf((char) code));
    }

    public void pickDefaultCandidate() {
        pickSuggestionManually(0);
    }

    public void pickSuggestionManually(int index) {
        if (this.mCompletionOn && this.mCompletions != null && index >= 0 && index < this.mCompletions.length) {
            getCurrentInputConnection().commitCompletion(this.mCompletions[index]);
            if (this.mCandidateView != null) {
                this.mCandidateView.clear();
            }
        } else if (this.mComposing.length() > 0) {
            commitTyped(getCurrentInputConnection());
        }
    }

    public void swipeRight() {
        if (this.mCompletionOn) {
            pickDefaultCandidate();
        }
    }

    public void swipeLeft() {
    }

    public void swipeDown() {
        handleClose();
    }

    public void swipeUp() {
    }

    public void onPress(int primaryCode) {
        this.keyIn = primaryCode;
        Keyboard current = this.mInputView.getKeyboard();
        if ((current == this.mBigKeyboard || current == this.mBigNumKeyboard) && this.sharedPref.getBoolean("labelMapping", PROCESS_HARD_KEYS)) {
            setKeyLabels(primaryCode);
            this.mInputView.invalidateAllKeys();
        }
        if (!this.sharedPref.getBoolean("hapticGuide", PROCESS_HARD_KEYS) || current != this.mBigKeyboard) {
            vib();
            return;
        }
        if (!(primaryCode == 97 || primaryCode == 105 || primaryCode == 105)) {
            vib();
        }
        if (this.keyIn == VAL_SP) {
            vib();
            vib();
        }
    }

    public void vib() {
        if (this.sharedPref.getBoolean("hapticOn", PROCESS_HARD_KEYS)) {
            this.mInputView.performHapticFeedback(1, 1);
        }
    }

    public void onRelease(int primaryCode) {
        boolean z;
        this.keyOut = primaryCode;
        Keyboard current = this.mInputView.getKeyboard();
        if ((current == this.mBigKeyboard || current == this.mBigNumKeyboard) && this.sharedPref.getBoolean("labelMapping", PROCESS_HARD_KEYS)) {
            resetKeyLabels();
            this.mInputView.invalidateAllKeys();
        }
        if (!this.sharedPref.getBoolean("hapticGuide", PROCESS_HARD_KEYS) || current != this.mBigKeyboard) {
            vib();
        } else {
            if (!(this.keyOut == 101 || this.keyOut == 111 || this.keyOut == 10)) {
                vib();
            }
            if (this.keyOut == VAL_SP) {
                vib();
                vib();
            }
        }
        if (current == this.mBigKeyboard) {
            switch (this.keyIn) {
                case VAL_USER /*-34*/:
                    switch (this.keyOut) {
                        case VAL_123 /*-2*/:
                            Context context = getApplicationContext();
                            Intent it = new Intent(context, PrefUser.class);
                            it.addFlags(268435456);
                            context.startActivity(it);
                            break;
                        case 10:
                            stringOut(this.sharedPref.getString("userEnt", ""));
                            break;
                        case VAL_SP /*32*/:
                            stringOut(this.sharedPref.getString("userSP", ""));
                            break;
                        case 97:
                            stringOut(this.sharedPref.getString("userA", ""));
                            break;
                        case 101:
                            stringOut(this.sharedPref.getString("userE", ""));
                            break;
                        case 105:
                            stringOut(this.sharedPref.getString("userI", ""));
                            break;
                        case 111:
                            stringOut(this.sharedPref.getString("userO", ""));
                            break;
                        case 116:
                            stringOut(this.sharedPref.getString("userT", ""));
                            break;
                    }
                case VAL_123 /*-2*/:
                    switch (this.keyOut) {
                        case VAL_USER /*-34*/:
                            if (this.mInputView != null) {
                                if (current == this.mSymbolsKeyboard || current == this.mSymbolsShiftedKeyboard) {
                                    current = this.mQwertyKeyboard;
                                } else if (current == this.mQwertyKeyboard) {
                                    current = this.mSymbolsKeyboard;
                                } else if (current == this.mBigKeyboard) {
                                    current = this.mBigNumKeyboard;
                                } else {
                                    current = this.mBigKeyboard;
                                }
                                this.mInputView.setKeyboard(current);
                                if (current == this.mSymbolsKeyboard) {
                                    current.setShifted(DEBUG);
                                    break;
                                }
                            }
                            break;
                        case 10:
                            handleCharacter(61, this.keycode_dummy);
                            break;
                        case VAL_SP /*32*/:
                            handleCharacter(42, this.keycode_dummy);
                            break;
                        case 97:
                            handleCharacter(45, this.keycode_dummy);
                            break;
                        case 101:
                            handleCharacter(43, this.keycode_dummy);
                            break;
                        case 105:
                            handleCharacter(47, this.keycode_dummy);
                            break;
                        case 111:
                            handleCharacter(40, this.keycode_dummy);
                            break;
                        case 116:
                            handleCharacter(41, this.keycode_dummy);
                            break;
                    }
                case VAL_SH /*-1*/:
                    switch (this.keyOut) {
                        case VAL_USER /*-34*/:
                            this.mBigCLock = PROCESS_HARD_KEYS;
                            current.setShifted(PROCESS_HARD_KEYS);
                            break;
                        default:
                            if (!this.mBigCLock) {
                                if (this.mShiftToggle) {
                                    z = false;
                                } else {
                                    z = true;
                                }
                                this.mShiftToggle = z;
                                current.setShifted(this.mShiftToggle);
                                break;
                            } else {
                                this.mBigCLock = DEBUG;
                                current.setShifted(DEBUG);
                                this.mShiftToggle = DEBUG;
                                break;
                            }
                    }
                case 10:
                    switch (this.keyOut) {
                        case 10:
                            sendKey(10);
                            break;
                        case VAL_SP /*32*/:
                            handleBackspace();
                            break;
                        case 97:
                            handleCharacter(46, this.keycode_dummy);
                            break;
                        case 101:
                            handleCharacter(44, this.keycode_dummy);
                            break;
                        case 105:
                            handleCharacter(58, this.keycode_dummy);
                            break;
                        case 111:
                            handleCharacter(64, this.keycode_dummy);
                            break;
                        case 116:
                            handleCharacter(122, this.keycode_dummy);
                            break;
                    }
                case VAL_SP /*32*/:
                    switch (this.keyOut) {
                        case 10:
                            handleCharacter(121, this.keycode_dummy);
                            break;
                        case VAL_SP /*32*/:
                            handleCharacter(VAL_SP, this.keycode_dummy);
                            break;
                        case 97:
                            handleCharacter(39, this.keycode_dummy);
                            break;
                        case 101:
                            handleCharacter(109, this.keycode_dummy);
                            break;
                        case 105:
                            handleCharacter(110, this.keycode_dummy);
                            break;
                        case 111:
                            handleCharacter(112, this.keycode_dummy);
                            break;
                        case 116:
                            handleCharacter(119, this.keycode_dummy);
                            break;
                    }
                case 97:
                    switch (this.keyOut) {
                        case VAL_SH /*-1*/:
                            handleCharacter(65, this.keycode_dummy);
                            this.mInputView.setShifted(DEBUG);
                            break;
                        case 10:
                            handleCharacter(98, this.keycode_dummy);
                            break;
                        case VAL_SP /*32*/:
                            handleCharacter(99, this.keycode_dummy);
                            break;
                        case 97:
                            handleCharacter(97, this.keycode_dummy);
                            break;
                        case 101:
                            handleCharacter(100, this.keycode_dummy);
                            break;
                        case 105:
                            handleCharacter(51, this.keycode_dummy);
                            break;
                        case 111:
                            handleCharacter(50, this.keycode_dummy);
                            break;
                        case 116:
                            handleCharacter(49, this.keycode_dummy);
                            break;
                    }
                case 101:
                    switch (this.keyOut) {
                        case VAL_SH /*-1*/:
                            handleCharacter(69, this.keycode_dummy);
                            this.mInputView.setShifted(DEBUG);
                            break;
                        case 10:
                            handleCharacter(52, this.keycode_dummy);
                            break;
                        case VAL_SP /*32*/:
                            handleCharacter(103, this.keycode_dummy);
                            break;
                        case 97:
                            handleCharacter(102, this.keycode_dummy);
                            break;
                        case 101:
                            handleCharacter(101, this.keycode_dummy);
                            break;
                        case 105:
                            handleCharacter(104, this.keycode_dummy);
                            break;
                        case 111:
                            handleCharacter(54, this.keycode_dummy);
                            break;
                        case 116:
                            handleCharacter(53, this.keycode_dummy);
                            break;
                    }
                case 105:
                    switch (this.keyOut) {
                        case VAL_SH /*-1*/:
                            handleCharacter(73, this.keycode_dummy);
                            this.mInputView.setShifted(DEBUG);
                            break;
                        case 10:
                            handleCharacter(56, this.keycode_dummy);
                            break;
                        case VAL_SP /*32*/:
                            handleCharacter(107, this.keycode_dummy);
                            break;
                        case 97:
                            handleCharacter(55, this.keycode_dummy);
                            break;
                        case 101:
                            handleCharacter(106, this.keycode_dummy);
                            break;
                        case 105:
                            handleCharacter(105, this.keycode_dummy);
                            break;
                        case 111:
                            handleCharacter(108, this.keycode_dummy);
                            break;
                        case 116:
                            handleCharacter(57, this.keycode_dummy);
                            break;
                    }
                case 111:
                    switch (this.keyOut) {
                        case VAL_SH /*-1*/:
                            handleCharacter(79, this.keycode_dummy);
                            this.mInputView.setShifted(DEBUG);
                            break;
                        case 10:
                            handleCharacter(35, this.keycode_dummy);
                            break;
                        case VAL_SP /*32*/:
                            handleCharacter(114, this.keycode_dummy);
                            break;
                        case 97:
                            handleCharacter(33, this.keycode_dummy);
                            break;
                        case 101:
                            handleCharacter(48, this.keycode_dummy);
                            break;
                        case 105:
                            handleCharacter(113, this.keycode_dummy);
                            break;
                        case 111:
                            handleCharacter(111, this.keycode_dummy);
                            break;
                        case 116:
                            handleCharacter(115, this.keycode_dummy);
                            break;
                    }
                case 116:
                    switch (this.keyOut) {
                        case VAL_SH /*-1*/:
                            handleCharacter(84, this.keycode_dummy);
                            this.mInputView.setShifted(DEBUG);
                            break;
                        case 10:
                            handleCharacter(120, this.keycode_dummy);
                            break;
                        case VAL_SP /*32*/:
                            handleCharacter(118, this.keycode_dummy);
                            break;
                        case 97:
                            handleCharacter(94, this.keycode_dummy);
                            break;
                        case 101:
                            handleCharacter(38, this.keycode_dummy);
                            break;
                        case 105:
                            handleCharacter(63, this.keycode_dummy);
                            break;
                        case 111:
                            handleCharacter(117, this.keycode_dummy);
                            break;
                        case 116:
                            handleCharacter(116, this.keycode_dummy);
                            break;
                    }
            }
            if (!this.mBigCLock && this.keyIn != VAL_SH) {
                current.setShifted(DEBUG);
                this.mShiftToggle = DEBUG;
            }
        }
        if (current == this.mBigNumKeyboard) {
            switch (this.keyIn) {
                case 35:
                    switch (this.keyOut) {
                        case 35:
                            handleCharacter(35, this.keycode_dummy);
                            return;
                        case 42:
                            handleCharacter(167, this.keycode_dummy);
                            return;
                        case 51:
                            handleCharacter(38, this.keycode_dummy);
                            return;
                        case 54:
                            handleCharacter(8594, this.keycode_dummy);
                            return;
                        default:
                            return;
                    }
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                default:
                    return;
                case 42:
                    switch (this.keyOut) {
                        case 35:
                            handleCharacter(183, this.keycode_dummy);
                            return;
                        case 42:
                            handleCharacter(42, this.keycode_dummy);
                            return;
                        case 48:
                            handleCharacter(63, this.keycode_dummy);
                            return;
                        case 51:
                            handleCharacter(8593, this.keycode_dummy);
                            return;
                        case 54:
                            handleCharacter(34, this.keycode_dummy);
                            return;
                        case 57:
                            handleCharacter(176, this.keycode_dummy);
                            return;
                        default:
                            return;
                    }
                case 48:
                    switch (this.keyOut) {
                        case 42:
                            handleCharacter(124, this.keycode_dummy);
                            return;
                        case 48:
                            handleCharacter(48, this.keycode_dummy);
                            return;
                        case 54:
                            handleCharacter(181, this.keycode_dummy);
                            return;
                        case 57:
                            handleCharacter(93, this.keycode_dummy);
                            return;
                        default:
                            return;
                    }
                case 49:
                    switch (this.keyOut) {
                        case 49:
                            handleCharacter(49, this.keycode_dummy);
                            return;
                        case 50:
                            handleCharacter(33, this.keycode_dummy);
                            return;
                        case 51:
                        default:
                            return;
                        case 52:
                            handleCharacter(126, this.keycode_dummy);
                            return;
                        case 53:
                            handleCharacter(169, this.keycode_dummy);
                            return;
                    }
                case 50:
                    switch (this.keyOut) {
                        case 49:
                            handleCharacter(64, this.keycode_dummy);
                            return;
                        case 50:
                            handleCharacter(50, this.keycode_dummy);
                            return;
                        case 51:
                            handleCharacter(36, this.keycode_dummy);
                            return;
                        case 52:
                            handleCharacter(174, this.keycode_dummy);
                            return;
                        case 53:
                            handleCharacter(8364, this.keycode_dummy);
                            return;
                        case 54:
                            handleCharacter(945, this.keycode_dummy);
                            return;
                        default:
                            return;
                    }
                case 51:
                    switch (this.keyOut) {
                        case 35:
                            handleCharacter(36, this.keycode_dummy);
                            return;
                        case 42:
                            handleCharacter(8592, this.keycode_dummy);
                            return;
                        case 50:
                            handleCharacter(37, this.keycode_dummy);
                            return;
                        case 51:
                            handleCharacter(51, this.keycode_dummy);
                            return;
                        case 53:
                            handleCharacter(8596, this.keycode_dummy);
                            return;
                        case 54:
                            handleCharacter(164, this.keycode_dummy);
                            return;
                        default:
                            return;
                    }
                case 52:
                    switch (this.keyOut) {
                        case 49:
                            handleCharacter(96, this.keycode_dummy);
                            return;
                        case 50:
                            handleCharacter(8482, this.keycode_dummy);
                            return;
                        case 51:
                        case 54:
                        default:
                            return;
                        case 52:
                            handleCharacter(52, this.keycode_dummy);
                            return;
                        case 53:
                            handleCharacter(44, this.keycode_dummy);
                            return;
                        case 55:
                            handleCharacter(45, this.keycode_dummy);
                            return;
                        case 56:
                            handleCharacter(177, this.keycode_dummy);
                            return;
                    }
                case 53:
                    switch (this.keyOut) {
                        case 49:
                            handleCharacter(8226, this.keycode_dummy);
                            return;
                        case 50:
                            handleCharacter(163, this.keycode_dummy);
                            return;
                        case 51:
                            handleCharacter(946, this.keycode_dummy);
                            return;
                        case 52:
                            handleCharacter(46, this.keycode_dummy);
                            return;
                        case 53:
                            handleCharacter(53, this.keycode_dummy);
                            return;
                        case 54:
                            handleCharacter(59, this.keycode_dummy);
                            return;
                        case 55:
                            handleCharacter(178, this.keycode_dummy);
                            return;
                        case 56:
                            handleCharacter(43, this.keycode_dummy);
                            return;
                        case 57:
                            handleCharacter(188, this.keycode_dummy);
                            return;
                        default:
                            return;
                    }
                case 54:
                    switch (this.keyOut) {
                        case 35:
                            handleCharacter(8595, this.keycode_dummy);
                            return;
                        case 42:
                            handleCharacter(39, this.keycode_dummy);
                            return;
                        case 48:
                            handleCharacter(95, this.keycode_dummy);
                            return;
                        case 50:
                            handleCharacter(8597, this.keycode_dummy);
                            return;
                        case 51:
                            handleCharacter(165, this.keycode_dummy);
                            return;
                        case 53:
                            handleCharacter(58, this.keycode_dummy);
                            return;
                        case 54:
                            handleCharacter(54, this.keycode_dummy);
                            return;
                        case 56:
                            handleCharacter(952, this.keycode_dummy);
                            return;
                        case 57:
                            handleCharacter(47, this.keycode_dummy);
                            return;
                        default:
                            return;
                    }
                case 55:
                    switch (this.keyOut) {
                        case 52:
                            handleCharacter(247, this.keycode_dummy);
                            return;
                        case 53:
                            handleCharacter(215, this.keycode_dummy);
                            return;
                        case 54:
                        default:
                            return;
                        case 55:
                            handleCharacter(55, this.keycode_dummy);
                            return;
                        case 56:
                            handleCharacter(40, this.keycode_dummy);
                            return;
                    }
                case 56:
                    switch (this.keyOut) {
                        case 52:
                            handleCharacter(179, this.keycode_dummy);
                            return;
                        case 53:
                            handleCharacter(61, this.keycode_dummy);
                            return;
                        case 54:
                            handleCharacter(189, this.keycode_dummy);
                            return;
                        case 55:
                            handleCharacter(41, this.keycode_dummy);
                            return;
                        case 56:
                            handleCharacter(56, this.keycode_dummy);
                            return;
                        case 57:
                            handleCharacter(60, this.keycode_dummy);
                            return;
                        default:
                            return;
                    }
                case 57:
                    switch (this.keyOut) {
                        case 42:
                            handleCharacter(197, this.keycode_dummy);
                            return;
                        case 48:
                            handleCharacter(91, this.keycode_dummy);
                            return;
                        case 53:
                            handleCharacter(960, this.keycode_dummy);
                            return;
                        case 54:
                            handleCharacter(92, this.keycode_dummy);
                            return;
                        case 56:
                            handleCharacter(62, this.keycode_dummy);
                            return;
                        case 57:
                            handleCharacter(57, this.keycode_dummy);
                            return;
                        default:
                            return;
                    }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void stringOut(String outText) {
        getCurrentInputConnection().commitText(outText, outText.length());
    }

    /* access modifiers changed from: package-private */
    public void resetKeyLabels() {
        Keyboard current = this.mInputView.getKeyboard();
        if (current == this.mBigKeyboard) {
            this.keys.get(0).label = null;
            this.keys.get(1).label = null;
            this.keys.get(2).label = null;
            this.keys.get(KEY_I).label = null;
            this.keys.get(5).label = null;
            this.keys.get(6).label = null;
            this.keys.get(8).label = null;
            this.keys.get(KEY_T).label = null;
            this.keys.get(7).label = "user";
            this.keys.get(10).label = "123";
            this.keys.get(0).iconPreview = null;
            this.keys.get(1).iconPreview = null;
            this.keys.get(2).iconPreview = null;
            this.keys.get(KEY_I).iconPreview = null;
            this.keys.get(5).iconPreview = null;
            this.keys.get(6).iconPreview = null;
            this.keys.get(8).iconPreview = null;
            this.keys.get(KEY_T).iconPreview = null;
            this.keys.get(7).iconPreview = null;
            this.keys.get(10).iconPreview = null;
        }
        if (current == this.mBigNumKeyboard) {
            this.nkeys.get(0).label = null;
            this.nkeys.get(1).label = null;
            this.nkeys.get(2).label = null;
            this.nkeys.get(KEY_HASH).label = null;
            this.nkeys.get(5).label = null;
            this.nkeys.get(6).label = null;
            this.nkeys.get(7).label = null;
            this.nkeys.get(8).label = null;
            this.nkeys.get(10).label = null;
            this.nkeys.get(KEY_8).label = null;
            this.nkeys.get(KEY_9).label = null;
            this.nkeys.get(KEY_0).label = null;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    /* access modifiers changed from: package-private */
    public void setKeyLabels(int keyId) {
        Keyboard current = this.mInputView.getKeyboard();
        if (current == this.mBigKeyboard) {
            switch (keyId) {
                case VAL_USER /*-34*/:
                    String kE = this.sharedPref.getString("userE", "");
                    String kA = this.sharedPref.getString("userA", "");
                    String kI = this.sharedPref.getString("userI", "");
                    String kSP = this.sharedPref.getString("userSP", "");
                    String kEnt = this.sharedPref.getString("userEnt", "");
                    String kO = this.sharedPref.getString("userO", "");
                    String kT = this.sharedPref.getString("userT", "");
                    this.keys.get(1).label = kE.length() <= 8 ? kE : kE.substring(0, 8);
                    this.keys.get(2).label = kA.length() <= 8 ? kA : kA.substring(0, 8);
                    this.keys.get(KEY_I).label = kI.length() <= 8 ? kI : kI.substring(0, 8);
                    this.keys.get(5).label = kSP.length() <= 8 ? kSP : kSP.substring(0, 8);
                    this.keys.get(6).label = kEnt.length() <= 8 ? kEnt : kEnt.substring(0, 8);
                    this.keys.get(8).label = kO.length() <= 8 ? kO : kO.substring(0, 8);
                    this.keys.get(KEY_T).label = kT.length() <= 8 ? kT : kT.substring(0, 8);
                    this.keys.get(7).label = " ";
                    this.keys.get(0).label = " ";
                    this.keys.get(10).label = "user";
                    break;
                case VAL_123 /*-2*/:
                    this.keys.get(1).label = "+";
                    this.keys.get(2).label = "-";
                    this.keys.get(KEY_I).label = "/";
                    this.keys.get(5).label = "*";
                    this.keys.get(6).label = "=";
                    this.keys.get(8).label = "(";
                    this.keys.get(KEY_T).label = ")";
                    this.keys.get(0).label = " ";
                    this.keys.get(7).label = "123";
                    this.keys.get(10).label = " ";
                    break;
                case VAL_SH /*-1*/:
                    this.keys.get(7).label = "Caps";
                    break;
                case 10:
                    this.keys.get(1).label = ",";
                    this.keys.get(2).label = ".";
                    this.keys.get(KEY_I).label = ":";
                    this.keys.get(5).label = "DEL";
                    this.keys.get(6).label = "Enter";
                    this.keys.get(8).label = "@";
                    this.keys.get(KEY_T).label = "z";
                    this.keys.get(7).label = " ";
                    this.keys.get(0).label = " ";
                    this.keys.get(10).label = " ";
                    break;
                case VAL_SP /*32*/:
                    this.keys.get(1).label = "m";
                    this.keys.get(2).label = "'";
                    this.keys.get(KEY_I).label = "n";
                    this.keys.get(5).label = "Space";
                    this.keys.get(6).label = "y";
                    this.keys.get(8).label = "p";
                    this.keys.get(KEY_T).label = "w";
                    this.keys.get(7).label = " ";
                    this.keys.get(0).label = " ";
                    this.keys.get(10).label = " ";
                    break;
                case 97:
                    this.keys.get(1).label = "d";
                    this.keys.get(2).label = "a";
                    this.keys.get(KEY_I).label = "3";
                    this.keys.get(5).label = "c";
                    this.keys.get(6).label = "b";
                    this.keys.get(8).label = "2";
                    this.keys.get(KEY_T).label = "1";
                    this.keys.get(7).label = " ";
                    this.keys.get(0).label = "A";
                    this.keys.get(10).label = " ";
                    break;
                case 101:
                    this.keys.get(1).label = "e";
                    this.keys.get(2).label = "f";
                    this.keys.get(KEY_I).label = "h";
                    this.keys.get(5).label = "g";
                    this.keys.get(6).label = "4";
                    this.keys.get(8).label = "6";
                    this.keys.get(KEY_T).label = "5";
                    this.keys.get(7).label = " ";
                    this.keys.get(0).label = "E";
                    this.keys.get(10).label = " ";
                    break;
                case 105:
                    this.keys.get(1).label = "j";
                    this.keys.get(2).label = "7";
                    this.keys.get(KEY_I).label = "i";
                    this.keys.get(5).label = "k";
                    this.keys.get(6).label = "8";
                    this.keys.get(8).label = "l";
                    this.keys.get(KEY_T).label = "9";
                    this.keys.get(7).label = " ";
                    this.keys.get(0).label = "I";
                    this.keys.get(10).label = " ";
                    break;
                case 111:
                    this.keys.get(1).label = "0";
                    this.keys.get(2).label = "!";
                    this.keys.get(KEY_I).label = "q";
                    this.keys.get(5).label = "r";
                    this.keys.get(6).label = "#";
                    this.keys.get(8).label = "o";
                    this.keys.get(KEY_T).label = "s";
                    this.keys.get(7).label = " ";
                    this.keys.get(0).label = "O";
                    this.keys.get(10).label = " ";
                    break;
                case 116:
                    this.keys.get(1).label = "&";
                    this.keys.get(2).label = "^";
                    this.keys.get(KEY_I).label = "?";
                    this.keys.get(5).label = "v";
                    this.keys.get(6).label = "x";
                    this.keys.get(8).label = "u";
                    this.keys.get(KEY_T).label = "t";
                    this.keys.get(7).label = " ";
                    this.keys.get(0).label = "T";
                    this.keys.get(10).label = " ";
                    break;
            }
        }
        if (current == this.mBigNumKeyboard) {
            switch (keyId) {
                case 35:
                    this.nkeys.get(0).label = " ";
                    this.nkeys.get(1).label = " ";
                    this.nkeys.get(2).label = "&";
                    this.nkeys.get(KEY_HASH).label = "#";
                    this.nkeys.get(5).label = " ";
                    this.nkeys.get(6).label = " ";
                    this.nkeys.get(7).label = "→";
                    this.nkeys.get(8).label = "§";
                    this.nkeys.get(10).label = " ";
                    this.nkeys.get(KEY_8).label = " ";
                    this.nkeys.get(KEY_9).label = " ";
                    this.nkeys.get(KEY_0).label = " ";
                    return;
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                default:
                    return;
                case 42:
                    this.nkeys.get(0).label = " ";
                    this.nkeys.get(1).label = " ";
                    this.nkeys.get(2).label = "↑";
                    this.nkeys.get(KEY_HASH).label = "·";
                    this.nkeys.get(5).label = " ";
                    this.nkeys.get(6).label = " ";
                    this.nkeys.get(7).label = "\"";
                    this.nkeys.get(8).label = "*";
                    this.nkeys.get(10).label = " ";
                    this.nkeys.get(KEY_8).label = " ";
                    this.nkeys.get(KEY_9).label = "°";
                    this.nkeys.get(KEY_0).label = "?";
                    return;
                case 48:
                    this.nkeys.get(0).label = " ";
                    this.nkeys.get(1).label = " ";
                    this.nkeys.get(2).label = " ";
                    this.nkeys.get(KEY_HASH).label = " ";
                    this.nkeys.get(5).label = " ";
                    this.nkeys.get(6).label = " ";
                    this.nkeys.get(7).label = "µ";
                    this.nkeys.get(8).label = "|";
                    this.nkeys.get(10).label = " ";
                    this.nkeys.get(KEY_8).label = " ";
                    this.nkeys.get(KEY_9).label = "]";
                    this.nkeys.get(KEY_0).label = "0";
                    return;
                case 49:
                    this.nkeys.get(0).label = "1";
                    this.nkeys.get(1).label = "!";
                    this.nkeys.get(2).label = " ";
                    this.nkeys.get(KEY_HASH).label = " ";
                    this.nkeys.get(5).label = "~";
                    this.nkeys.get(6).label = "©";
                    this.nkeys.get(7).label = " ";
                    this.nkeys.get(8).label = " ";
                    this.nkeys.get(10).label = " ";
                    this.nkeys.get(KEY_8).label = " ";
                    this.nkeys.get(KEY_9).label = " ";
                    this.nkeys.get(KEY_0).label = " ";
                    return;
                case 50:
                    this.nkeys.get(0).label = "@";
                    this.nkeys.get(1).label = "2";
                    this.nkeys.get(2).label = "$";
                    this.nkeys.get(KEY_HASH).label = " ";
                    this.nkeys.get(5).label = "®";
                    this.nkeys.get(6).label = "€";
                    this.nkeys.get(7).label = "α";
                    this.nkeys.get(8).label = " ";
                    this.nkeys.get(10).label = " ";
                    this.nkeys.get(KEY_8).label = " ";
                    this.nkeys.get(KEY_9).label = " ";
                    this.nkeys.get(KEY_0).label = " ";
                    return;
                case 51:
                    this.nkeys.get(0).label = " ";
                    this.nkeys.get(1).label = "%";
                    this.nkeys.get(2).label = "3";
                    this.nkeys.get(KEY_HASH).label = "$";
                    this.nkeys.get(5).label = " ";
                    this.nkeys.get(6).label = "↔";
                    this.nkeys.get(7).label = "¤";
                    this.nkeys.get(8).label = "←";
                    this.nkeys.get(10).label = " ";
                    this.nkeys.get(KEY_8).label = " ";
                    this.nkeys.get(KEY_9).label = " ";
                    this.nkeys.get(KEY_0).label = " ";
                    return;
                case 52:
                    this.nkeys.get(0).label = "`";
                    this.nkeys.get(1).label = "™";
                    this.nkeys.get(2).label = " ";
                    this.nkeys.get(KEY_HASH).label = " ";
                    this.nkeys.get(5).label = "4";
                    this.nkeys.get(6).label = ",";
                    this.nkeys.get(7).label = " ";
                    this.nkeys.get(8).label = " ";
                    this.nkeys.get(10).label = "-";
                    this.nkeys.get(KEY_8).label = "±";
                    this.nkeys.get(KEY_9).label = " ";
                    this.nkeys.get(KEY_0).label = " ";
                    return;
                case 53:
                    this.nkeys.get(0).label = "•";
                    this.nkeys.get(1).label = "£";
                    this.nkeys.get(2).label = "β";
                    this.nkeys.get(KEY_HASH).label = " ";
                    this.nkeys.get(5).label = ".";
                    this.nkeys.get(6).label = "5";
                    this.nkeys.get(7).label = ";";
                    this.nkeys.get(8).label = " ";
                    this.nkeys.get(10).label = "²";
                    this.nkeys.get(KEY_8).label = "+";
                    this.nkeys.get(KEY_9).label = "¼";
                    this.nkeys.get(KEY_0).label = " ";
                    return;
                case 54:
                    this.nkeys.get(0).label = " ";
                    this.nkeys.get(1).label = "↕";
                    this.nkeys.get(2).label = "¥";
                    this.nkeys.get(KEY_HASH).label = "↓";
                    this.nkeys.get(5).label = " ";
                    this.nkeys.get(6).label = ":";
                    this.nkeys.get(7).label = "6";
                    this.nkeys.get(8).label = "'";
                    this.nkeys.get(10).label = " ";
                    this.nkeys.get(KEY_8).label = "θ";
                    this.nkeys.get(KEY_9).label = "/";
                    this.nkeys.get(KEY_0).label = "_";
                    return;
                case 55:
                    this.nkeys.get(0).label = " ";
                    this.nkeys.get(1).label = " ";
                    this.nkeys.get(2).label = " ";
                    this.nkeys.get(KEY_HASH).label = " ";
                    this.nkeys.get(5).label = "÷";
                    this.nkeys.get(6).label = "×";
                    this.nkeys.get(7).label = " ";
                    this.nkeys.get(8).label = " ";
                    this.nkeys.get(10).label = "7";
                    this.nkeys.get(KEY_8).label = "(";
                    this.nkeys.get(KEY_9).label = " ";
                    this.nkeys.get(KEY_0).label = " ";
                    return;
                case 56:
                    this.nkeys.get(0).label = " ";
                    this.nkeys.get(1).label = " ";
                    this.nkeys.get(2).label = " ";
                    this.nkeys.get(KEY_HASH).label = " ";
                    this.nkeys.get(5).label = "³";
                    this.nkeys.get(6).label = "=";
                    this.nkeys.get(7).label = "½";
                    this.nkeys.get(8).label = " ";
                    this.nkeys.get(10).label = ")";
                    this.nkeys.get(KEY_8).label = "8";
                    this.nkeys.get(KEY_9).label = "<";
                    this.nkeys.get(KEY_0).label = " ";
                    return;
                case 57:
                    this.nkeys.get(0).label = " ";
                    this.nkeys.get(1).label = " ";
                    this.nkeys.get(2).label = " ";
                    this.nkeys.get(KEY_HASH).label = " ";
                    this.nkeys.get(5).label = " ";
                    this.nkeys.get(6).label = "π";
                    this.nkeys.get(7).label = "\\";
                    this.nkeys.get(8).label = "Å";
                    this.nkeys.get(10).label = " ";
                    this.nkeys.get(KEY_8).label = ">";
                    this.nkeys.get(KEY_9).label = "9";
                    this.nkeys.get(KEY_0).label = "[";
                    return;
            }
        }
    }
}
