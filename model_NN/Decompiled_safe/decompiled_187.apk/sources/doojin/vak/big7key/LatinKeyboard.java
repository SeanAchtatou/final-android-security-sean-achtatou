package doojin.vak.big7key;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.inputmethodservice.Keyboard;
import java.util.List;

public class LatinKeyboard extends Keyboard {
    private Keyboard.Key keyA;
    private Keyboard.Key keyDel;
    private Keyboard.Key keyE;
    private Keyboard.Key keyEnt;
    private Keyboard.Key keyI;
    private List keyList;
    private Keyboard.Key keyNum;
    private Keyboard.Key keyO;
    private Keyboard.Key keyShift;
    private Keyboard.Key keySp;
    private Keyboard.Key keyT;
    private Keyboard.Key keyUser;

    public LatinKeyboard(Context context, int xmlLayoutResId) {
        super(context, xmlLayoutResId);
    }

    public LatinKeyboard(Context context, int layoutTemplateResId, CharSequence characters, int columns, int horizontalPadding) {
        super(context, layoutTemplateResId, characters, columns, horizontalPadding);
    }

    /* access modifiers changed from: protected */
    public Keyboard.Key createKeyFromXml(Resources res, Keyboard.Row parent, int x, int y, XmlResourceParser parser) {
        Keyboard.Key key = new LatinKey(res, parent, x, y, parser);
        if (key.codes[0] == -1) {
            this.keyShift = key;
        }
        if (key.codes[0] == 101) {
            this.keyE = key;
        }
        if (key.codes[0] == 97) {
            this.keyA = key;
        }
        if (key.codes[0] == -5) {
            this.keyDel = key;
        }
        if (key.codes[0] == 105) {
            this.keyI = key;
        }
        if (key.codes[0] == 32) {
            this.keySp = key;
        }
        if (key.codes[0] == 10) {
            this.keyEnt = key;
        }
        if (key.codes[0] == -100) {
            this.keyUser = key;
        }
        if (key.codes[0] == 111) {
            this.keyO = key;
        }
        if (key.codes[0] == 116) {
            this.keyT = key;
        }
        if (key.codes[0] == -2) {
            this.keyNum = key;
        }
        return key;
    }

    /* access modifiers changed from: package-private */
    public void setKeyLabels(int keyID) {
    }

    /* access modifiers changed from: package-private */
    public void resetKeyLabels() {
    }

    /* access modifiers changed from: package-private */
    public void setImeOptions(Resources res, int options) {
    }

    static class LatinKey extends Keyboard.Key {
        public LatinKey(Resources res, Keyboard.Row parent, int x, int y, XmlResourceParser parser) {
            super(res, parent, x, y, parser);
        }

        public boolean isInside(int x, int y) {
            return super.isInside(x, this.codes[0] == -3 ? y - 10 : y);
        }
    }
}
