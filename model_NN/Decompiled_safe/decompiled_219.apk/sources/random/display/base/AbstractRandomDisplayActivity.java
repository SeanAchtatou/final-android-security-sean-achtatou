package random.display.base;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import random.display.arashi.R;
import random.display.utils.ToastUtils;

public abstract class AbstractRandomDisplayActivity extends RandomDisplayBaseActivity {
    public void onCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.main);
        super.onCreate(savedInstanceState);
        setTitle(getText(R.string.title));
    }

    public ImageView getImageView() {
        return (ImageView) findViewById(R.id.web_image);
    }

    public FrameLayout getFrameLayout() {
        return (FrameLayout) findViewById(R.id.frameLayout1);
    }

    public ImageButton getNextBtn() {
        return (ImageButton) findViewById(R.id.nextButton);
    }

    public ImageButton getZoonInBtn() {
        return (ImageButton) findViewById(R.id.zoomInButton);
    }

    public ImageButton getZoonOutBtn() {
        return (ImageButton) findViewById(R.id.zoomOutButton);
    }

    public ViewGroup getAdmobContainer() {
        return (LinearLayout) findViewById(R.id.linearLayout1);
    }

    public String getLoadingText() {
        return getText(R.string.pic_loading_msg).toString();
    }

    public String getSavingMenuText() {
        return getText(R.string.save_image_menuitem_label).toString();
    }

    public String getSetAsWallpaperMenuText() {
        return getText(R.string.set_wallpaper_menuitem_label).toString();
    }

    public String getImageDownloadErrorText() {
        return getText(R.string.search_failed_error_msg).toString();
    }

    public void onNoSDCardError() {
        ToastUtils.showToast(getText(R.string.sdcard_mount_error_msg).toString(), getImageView(), this);
    }

    public void onBeforeSaving() {
        ToastUtils.showToast(getText(R.string.save_image_msg).toString(), getImageView(), this);
    }

    public void onBeforeWallpaperCropping() {
        ToastUtils.showToast(getText(R.string.set_wallpaper_msg1).toString(), getImageView(), this);
    }

    public void onSaved(String path) {
        ToastUtils.showToast(String.valueOf(getText(R.string.save_image_path_msg).toString()) + path, getImageView(), this);
    }

    public void onWallpaperSet() {
        ToastUtils.showToast(getText(R.string.set_wallpaper_msg2).toString(), getImageView(), this);
    }

    public void onWallpaperError() {
        ToastUtils.showToast(getText(R.string.set_wallpaper_error_msg1).toString(), getImageView(), this);
    }

    public void onUnableToSetWallpaper() {
        ToastUtils.showToast(getText(R.string.set_wallpaper_error_msg2).toString(), getImageView(), this);
    }

    public String getNetworkErrorTitle() {
        return getText(R.string.error_dialog_title).toString();
    }

    public String getNetworkErrorContent() {
        return getText(R.string.error_dialog_content).toString();
    }

    public String getNetworkButtonLabel() {
        return getText(R.string.error_dialog_btn_label).toString();
    }

    public String getSavePuzzleSrcLabel() {
        return getText(R.string.save_as_puzzle_src).toString();
    }

    public String getPlayPuzzleLabel() {
        return getText(R.string.play_puzzle).toString();
    }

    /* access modifiers changed from: protected */
    public int getAdmobFilter() {
        return 0;
    }
}
