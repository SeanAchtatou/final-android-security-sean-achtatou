package random.display.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import random.display.base.ImageSaver;
import random.display.base.ZoomImageView;
import random.display.log.UsageLog;
import random.display.log.UsageLogger;
import random.display.log.UsageType;
import random.display.provider.ImageProvider;
import random.display.provider.ImageProviderListener;
import random.display.provider.flickr.FlickrImageProvider;
import random.display.puzzle.PuzzleActivity;
import random.display.utils.NetworkUtils;
import random.display.utils.ToastUtils;

public abstract class RandomDisplayBaseActivity extends Activity implements SensorEventListener, ImageProviderListener, RandomDisplayResourceProvider, ImageSaver.ImageSaverEventListener {
    public static final int GENDER_FEMALE = 2;
    public static final int GENDER_MALE = 1;
    private static Runnable displayPic;
    /* access modifiers changed from: private */
    public static boolean running = false;
    /* access modifiers changed from: private */
    public int admodCount = 0;
    /* access modifiers changed from: private */
    public int clickCount = 0;
    /* access modifiers changed from: private */
    public Thread clickTimer;
    private ImageProvider currentImageProvider;
    /* access modifiers changed from: private */
    public boolean isCancel = false;
    private boolean isInitialized = false;
    private Sensor mAccelerometer;
    private PowerManager mPowerManager;
    private SensorManager mSensorManager;
    private PowerManager.WakeLock mWakeLock;
    private ImageSaver m_ImageSaver;
    private ProgressDialog pd;
    /* access modifiers changed from: private */
    public Bitmap pic;
    private Thread thread;
    /* access modifiers changed from: private */
    public ZoomImageView zoomImageView;

    /* access modifiers changed from: protected */
    public abstract int getAdmobFilter();

    /* access modifiers changed from: protected */
    public abstract String getAdmobId();

    /* access modifiers changed from: protected */
    public abstract ImageProvider getImageProvider();

    /* access modifiers changed from: protected */
    public abstract String[] getKeywords();

    /* access modifiers changed from: protected */
    public abstract String getStorageFolderName();

    /* access modifiers changed from: protected */
    public boolean isSaveEnabled() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isSetAsWallpaperEnabled() {
        return true;
    }

    private void displayNetworkError() {
        getImageView().post(new Runnable() {
            public void run() {
                AlertDialog.Builder MyAlertDialog = new AlertDialog.Builder(RandomDisplayBaseActivity.this);
                MyAlertDialog.setTitle(RandomDisplayBaseActivity.this.getNetworkErrorTitle());
                MyAlertDialog.setMessage(RandomDisplayBaseActivity.this.getNetworkErrorContent());
                MyAlertDialog.setNeutralButton(RandomDisplayBaseActivity.this.getNetworkButtonLabel(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        RandomDisplayBaseActivity.this.finish();
                    }
                });
                MyAlertDialog.show();
            }
        });
    }

    private void initAndroidManagers() {
        this.mPowerManager = (PowerManager) getSystemService("power");
        this.mWakeLock = this.mPowerManager.newWakeLock(10, "BackLights");
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mAccelerometer = this.mSensorManager.getDefaultSensor(1);
    }

    /* access modifiers changed from: private */
    public void initAdMob() {
        AdView adView = new AdView(this, AdSize.BANNER, getAdmobId());
        getAdmobContainer().removeAllViews();
        getAdmobContainer().addView(adView);
        AdRequest adRequest = new AdRequest();
        if ((getAdmobFilter() & 1) == 1) {
            adRequest.setGender(AdRequest.Gender.MALE);
        } else if ((getAdmobFilter() & 2) == 2) {
            adRequest.setGender(AdRequest.Gender.FEMALE);
        }
        adView.loadAd(adRequest);
    }

    /* access modifiers changed from: private */
    public void createTimer() {
        this.clickTimer = new Thread(new Runnable() {
            public void run() {
                long pass = System.currentTimeMillis();
                while (System.currentTimeMillis() - pass < 500) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                RandomDisplayBaseActivity.this.clickTimer = null;
                if (RandomDisplayBaseActivity.this.clickCount > 1) {
                    RandomDisplayBaseActivity.this.zoomIn();
                } else {
                    RandomDisplayBaseActivity.this.getImageView().post(new Runnable() {
                        public void run() {
                            RandomDisplayBaseActivity.this.changedPic();
                        }
                    });
                }
                RandomDisplayBaseActivity.this.clickCount = 0;
            }
        });
        this.clickTimer.start();
    }

    private void initialize() {
        synchronized (RandomDisplayBaseActivity.class) {
            if (!this.isInitialized) {
                initAndroidManagers();
                initAdMob();
                displayPic = new Runnable() {
                    public void run() {
                        RandomDisplayBaseActivity.this.changePicture();
                        RandomDisplayBaseActivity.running = false;
                    }
                };
                getImageView().setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        synchronized (RandomDisplayBaseActivity.class) {
                            RandomDisplayBaseActivity randomDisplayBaseActivity = RandomDisplayBaseActivity.this;
                            randomDisplayBaseActivity.clickCount = randomDisplayBaseActivity.clickCount + 1;
                            if (RandomDisplayBaseActivity.this.clickTimer == null) {
                                RandomDisplayBaseActivity.this.createTimer();
                            }
                        }
                    }
                });
                FrameLayout frameLayout = getFrameLayout();
                this.zoomImageView = new ZoomImageView(this);
                this.zoomImageView.setOnImagePaneListener(new ZoomImageView.ImagePanelListener() {
                    public void onClick(ZoomImageView p) {
                    }

                    public void onDoubleClick(ZoomImageView p) {
                        RandomDisplayBaseActivity.this.zoomOut();
                    }
                });
                frameLayout.addView(this.zoomImageView);
                this.zoomImageView.setVisibility(8);
                this.isInitialized = true;
                getNextBtn().setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        RandomDisplayBaseActivity.this.changedPic();
                    }
                });
                getZoonInBtn().setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        RandomDisplayBaseActivity.this.zoomIn();
                    }
                });
                getZoonOutBtn().setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        RandomDisplayBaseActivity.this.zoomOut();
                    }
                });
                frameLayout.bringChildToFront(getNextBtn());
                frameLayout.bringChildToFront(getZoonInBtn());
                frameLayout.bringChildToFront(getZoonOutBtn());
                getNextBtn().setAlpha(180);
                getZoonInBtn().setAlpha(180);
                getZoonOutBtn().setAlpha(180);
                getImageView().setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void changePicture() {
        ImageProvider ip = getImageProvider();
        ip.setImageProviderListener(this);
        this.currentImageProvider = ip;
        ip.initialize();
        if (this.pic != null) {
            this.pic.recycle();
        }
        if (ip.hasKeywords()) {
            this.pic = ip.downloadImage();
        } else {
            this.pic = ip.downloadImage(getKeywords());
        }
        getImageView().post(new Runnable() {
            public void run() {
                if (!RandomDisplayBaseActivity.this.isCancel) {
                    if (RandomDisplayBaseActivity.this.pic == null) {
                        RandomDisplayBaseActivity.this.changedPic();
                        return;
                    }
                    RandomDisplayBaseActivity randomDisplayBaseActivity = RandomDisplayBaseActivity.this;
                    randomDisplayBaseActivity.admodCount = randomDisplayBaseActivity.admodCount + 1;
                    if (RandomDisplayBaseActivity.this.admodCount > 5) {
                        RandomDisplayBaseActivity.this.initAdMob();
                        RandomDisplayBaseActivity.this.admodCount = 0;
                    }
                    RandomDisplayBaseActivity.this.getImageView().setDrawingCacheEnabled(true);
                    RandomDisplayBaseActivity.this.getImageView().setImageBitmap(RandomDisplayBaseActivity.this.pic);
                    RandomDisplayBaseActivity.this.zoomImageView.setBitmap(RandomDisplayBaseActivity.this.pic);
                }
            }
        });
        if (!this.isCancel) {
            this.pd.dismiss();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.m_ImageSaver = new ImageSaver();
        this.m_ImageSaver.setMainActivity(this);
        this.m_ImageSaver.setSavingEventListener(this);
        this.m_ImageSaver.setStorageFolderName(getStorageFolderName());
        if (!NetworkUtils.isOnline(this)) {
            displayNetworkError();
        } else {
            initialize();
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.equals((Object) 2)) {
            closeEverything();
        } else if (newConfig.equals((Object) 1)) {
            closeEverything();
        } else if (newConfig.equals((Object) 3)) {
            closeEverything();
        } else if (newConfig.equals((Object) 0)) {
            closeEverything();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.isInitialized) {
            this.isCancel = false;
            changedPic();
            this.mWakeLock.acquire();
            this.mSensorManager.registerListener(this, this.mAccelerometer, 3);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.isInitialized) {
            this.isCancel = true;
            closeEverything();
            this.mSensorManager.unregisterListener(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.isInitialized) {
            this.mWakeLock.release();
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        for (float abs : event.values) {
            if (Math.abs(abs) > 12.0f) {
                changedPic();
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void changedPic() {
        synchronized (RandomDisplayBaseActivity.class) {
            if (!running) {
                this.pd = ProgressDialog.show(this, "", getLoadingText(), true, true);
                this.thread = new Thread(displayPic);
                running = true;
                zoomOut();
                this.thread.start();
                getImageView().setImageBitmap(null);
                this.zoomImageView.setBitmap(null);
            }
        }
    }

    private void closeEverything() {
        synchronized (RandomDisplayBaseActivity.class) {
            if (this.pd.isShowing()) {
                this.pd.dismiss();
            }
            running = false;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (isSaveEnabled()) {
            menu.add(0, 0, 0, getSavingMenuText());
        }
        if (isSetAsWallpaperEnabled()) {
            menu.add(0, 1, 1, getSetAsWallpaperMenuText());
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    private void saveUsageLog(UsageType ut) {
        UsageLog ulRet = new UsageLog();
        ulRet.setAppName(getClass().getSimpleName());
        ulRet.setUrl(this.currentImageProvider.getCurrentUrl());
        ulRet.setSource(this.currentImageProvider.getCurrentSource());
        ulRet.setKeyword(this.currentImageProvider.getCurrentKeyword());
        ulRet.setUsage(ut);
        Log.i("RandomDisplayBase", "url: " + this.currentImageProvider.getCurrentUrl());
        UsageLogger.getInstance().addUsageLog(ulRet);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case FlickrImageProvider.LARGE_FIRST /*0*/:
                saveUsageLog(UsageType.Save);
                this.m_ImageSaver.savePic(this.pic, this, false, false);
                break;
            case 1:
                saveUsageLog(UsageType.Wallpaper);
                this.m_ImageSaver.savePic(this.pic, this, true, false);
                break;
            case GENDER_FEMALE /*2*/:
                this.m_ImageSaver.savePic(this.pic, this, false, true);
                break;
            case 3:
                startPuzzleActivity();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.m_ImageSaver.processCropingIntentResult(requestCode, resultCode, data);
    }

    public void onDownloadFailed() {
        ToastUtils.showToast(getImageDownloadErrorText(), getImageView(), this);
        changedPic();
    }

    public void onNetworkError() {
        displayNetworkError();
    }

    /* access modifiers changed from: private */
    public void zoomOut() {
        getFrameLayout().post(new Runnable() {
            public void run() {
                RandomDisplayBaseActivity.this.getImageView().setVisibility(0);
                RandomDisplayBaseActivity.this.zoomImageView.setVisibility(8);
            }
        });
    }

    /* access modifiers changed from: private */
    public void zoomIn() {
        getFrameLayout().post(new Runnable() {
            public void run() {
                RandomDisplayBaseActivity.this.getImageView().setVisibility(8);
                RandomDisplayBaseActivity.this.zoomImageView.setVisibility(0);
            }
        });
    }

    private void startPuzzleActivity() {
        Intent intent = new Intent(this, PuzzleActivity.class);
        intent.putExtra("PuzzleSrc", "/mnt/sdcard" + getStorageFolderName() + "puzzle");
        intent.putExtra("AdmobId", getAdmobId());
        startActivity(intent);
    }
}
