package random.display.base;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int frame = 2130837504;
        public static final int icon = 2130837505;
        public static final int next = 2130837506;
        public static final int no_pic = 2130837507;
        public static final int zoom_in = 2130837508;
        public static final int zoom_out = 2130837509;
    }

    public static final class id {
        public static final int Widget = 2131099656;
        public static final int admodLayout = 2131099660;
        public static final int answerView = 2131099648;
        public static final int frameLayout1 = 2131099650;
        public static final int framelayout = 2131099657;
        public static final int linearLayout = 2131099649;
        public static final int linearLayout1 = 2131099655;
        public static final int nextButton = 2131099652;
        public static final int photo = 2131099658;
        public static final int puzzleLayout = 2131099659;
        public static final int web_image = 2131099651;
        public static final int zoomInButton = 2131099653;
        public static final int zoomOutButton = 2131099654;
    }

    public static final class layout {
        public static final int answer = 2130903040;
        public static final int main = 2130903041;
        public static final int photoframewidget = 2130903042;
        public static final int puzzle = 2130903043;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int error_dialog_btn_label = 2131034113;
        public static final int error_dialog_content = 2131034114;
        public static final int error_dialog_title = 2131034115;
        public static final int photoframe_name = 2131034132;
        public static final int pic_loading_msg = 2131034116;
        public static final int play_next_puzzle = 2131034131;
        public static final int play_puzzle = 2131034129;
        public static final int save_as_puzzle_src = 2131034128;
        public static final int save_image_menuitem_label = 2131034117;
        public static final int save_image_msg = 2131034118;
        public static final int save_image_path_msg = 2131034119;
        public static final int sdcard_mount_error_msg = 2131034120;
        public static final int search_failed_error_msg = 2131034127;
        public static final int set_wallpaper_error_msg1 = 2131034121;
        public static final int set_wallpaper_error_msg2 = 2131034122;
        public static final int set_wallpaper_menuitem_label = 2131034123;
        public static final int set_wallpaper_msg1 = 2131034124;
        public static final int set_wallpaper_msg2 = 2131034125;
        public static final int show_anwser = 2131034130;
        public static final int title = 2131034126;
    }

    public static final class xml {
        public static final int photoframewidget = 2130968576;
    }
}
