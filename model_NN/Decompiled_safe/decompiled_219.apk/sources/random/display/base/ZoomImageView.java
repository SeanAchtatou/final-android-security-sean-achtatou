package random.display.base;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import random.display.provider.flickr.FlickrImageProvider;

public class ZoomImageView extends View implements View.OnClickListener, View.OnTouchListener {
    private int baseBottom = 0;
    private int baseLeft = 0;
    private int baseRight = 0;
    private int baseTop = 0;
    private Bitmap bitmap;
    /* access modifiers changed from: private */
    public int clickCount = 0;
    /* access modifiers changed from: private */
    public Thread clickTimer;
    private int height;
    private Drawable image;
    private boolean isMove = false;
    private float lastX;
    private float lastY;
    /* access modifiers changed from: private */
    public boolean moving = false;
    private float offsetX;
    private float offsetY;
    private ImagePanelListener onImagePaneListener;
    private int width;

    public interface ImagePanelListener {
        void onClick(ZoomImageView zoomImageView);

        void onDoubleClick(ZoomImageView zoomImageView);
    }

    public ImagePanelListener getOnImagePaneListener() {
        return this.onImagePaneListener;
    }

    public void setOnImagePaneListener(ImagePanelListener onImagePaneListener2) {
        this.onImagePaneListener = onImagePaneListener2;
    }

    public ZoomImageView(Context context) {
        super(context);
        setFocusable(true);
        setOnTouchListener(this);
        setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.width = w;
        this.height = h;
        if (this.bitmap != null) {
            this.baseLeft = (w / 2) - (this.bitmap.getWidth() / 2);
            this.baseTop = (h / 2) - (this.bitmap.getHeight() / 2);
            this.baseRight = (w / 2) + (this.bitmap.getWidth() / 2);
            this.baseBottom = (h / 2) + (this.bitmap.getHeight() / 2);
        }
    }

    public void setBitmap(Bitmap pic) {
        this.bitmap = pic;
        if (pic == null) {
            this.image = null;
            return;
        }
        this.image = new BitmapDrawable(pic);
        this.baseLeft = (this.width / 2) - (this.bitmap.getWidth() / 2);
        this.baseTop = (this.height / 2) - (this.bitmap.getHeight() / 2);
        this.baseRight = (this.width / 2) + (this.bitmap.getWidth() / 2);
        this.baseBottom = (this.height / 2) + (this.bitmap.getHeight() / 2);
        this.lastX = 0.0f;
        this.lastY = 0.0f;
        this.offsetX = 0.0f;
        this.offsetY = 0.0f;
        this.clickCount = 0;
        this.isMove = false;
        this.moving = false;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.image != null) {
            this.image.setBounds(this.baseLeft + Math.round(this.offsetX), this.baseTop + Math.round(this.offsetY), this.baseRight + Math.round(this.offsetX), this.baseBottom + Math.round(this.offsetY));
            this.image.draw(canvas);
        }
    }

    private void picMov(float x, float y) {
        synchronized (ZoomImageView.class) {
            this.isMove = Math.abs(x - this.lastX) > 5.0f || Math.abs(y - this.lastY) > 5.0f;
            this.moving |= this.isMove;
            if (this.isMove) {
                float centerX = this.offsetX + (x - this.lastX);
                float centerY = this.offsetY + (y - this.lastY);
                int left = this.baseLeft + Math.round(centerX);
                int top = this.baseTop + Math.round(centerY);
                int right = this.baseRight + Math.round(centerX);
                int bottom = this.baseBottom + Math.round(centerY);
                if (left <= 0 && right >= getWidth()) {
                    this.offsetX += x - this.lastX;
                }
                if (top <= 0 && bottom >= getHeight()) {
                    this.offsetY += y - this.lastY;
                }
                this.lastX = x;
                this.lastY = y;
                invalidate();
            }
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case FlickrImageProvider.LARGE_FIRST /*0*/:
                this.moving = false;
                this.lastX = event.getX();
                this.lastY = event.getY();
                break;
            case RandomDisplayBaseActivity.GENDER_FEMALE /*2*/:
                picMov(event.getX(), event.getY());
                break;
        }
        return false;
    }

    public void onClick(View view) {
        synchronized (ZoomImageView.class) {
            this.clickCount++;
            if (this.clickTimer == null) {
                createTimer();
            }
        }
    }

    /* access modifiers changed from: private */
    public void dispatchEvent() {
        if (getOnImagePaneListener() != null) {
            if (this.clickCount > 1) {
                Log.i("PanelClick", "multiple click");
                getOnImagePaneListener().onDoubleClick(this);
            } else {
                Log.i("PanelClick", "single click");
                getOnImagePaneListener().onClick(this);
            }
            this.offsetX = 0.0f;
            this.offsetY = 0.0f;
        }
    }

    private void createTimer() {
        this.clickTimer = new Thread(new Runnable() {
            public void run() {
                long pass = System.currentTimeMillis();
                while (System.currentTimeMillis() - pass < 500) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                ZoomImageView.this.clickTimer = null;
                if (!ZoomImageView.this.moving) {
                    ZoomImageView.this.dispatchEvent();
                }
                ZoomImageView.this.clickCount = 0;
            }
        });
        this.clickTimer.start();
    }
}
