package random.display.arashi;

import android.util.Log;
import java.util.Random;
import random.display.base.AbstractRandomDisplayActivity;
import random.display.provider.ImageProvider;
import random.display.provider.flickr.FlickrImageProvider;
import random.display.provider.picasa.PicasaImageProvider;

public class Arashi extends AbstractRandomDisplayActivity {
    private FlickrImageProvider fip = new FlickrImageProvider();
    private PicasaImageProvider pip = new PicasaImageProvider();

    /* renamed from: random  reason: collision with root package name */
    private Random f0random = new Random(System.currentTimeMillis());

    public Arashi() {
        this.fip.setKeywords(new String[]{"大野智 -笑脸 -大野智史 -taipei -東京 -FREE -grace -Television -FREESTYLE -祖師爺 -TokiOh -長崎", "松本潤 -fashion -taipei -マジモトテイオー -Allen -演唱會 -Mosaic", "櫻井翔 -taipei -cat -Mitsu -ハチミツとクローバー -JoJo", "二宮和也 -taipei -watercolor -小肉翅 -神樂坂 -戸田恵梨香 -photoshop -TV -painting -arashiyama", "相葉雅紀 -Taipei -MOS -MISS -Keikarou -Television"});
        this.pip.setKeywords(new String[]{"\"大野智\" -\"Dive\" -\"自由行\" -\"老朋友\"", "\"松本潤\" -\"日本模特兒\" -\"Sp360\" -\"F40\" -\"大佛\" -\"井上真央\" -\"松本\" -\"不是潤\"", "\"櫻井翔\" -\"吾妻\" -\"台場摩天輪\"", "\"二宮和也\" -\"流星\" -\"神楽坂\" -\"觀光客\" -\"唯愛\" -\"生日\"", "\"相葉雅紀\"  -\"生日蛋糕\" -\"ハローグッバイ\" -\"キュージョン\" -\"キューピー\" -\"いぬのふく\""});
    }

    /* access modifiers changed from: protected */
    public String getStorageFolderName() {
        return "/arashi/";
    }

    /* access modifiers changed from: protected */
    public String[] getKeywords() {
        return null;
    }

    /* access modifiers changed from: protected */
    public ImageProvider getImageProvider() {
        if (this.f0random.nextBoolean()) {
            Log.i("Arashi", "use flickr");
            return this.fip;
        }
        Log.i("Arashi", "use picasa");
        return this.pip;
    }

    /* access modifiers changed from: protected */
    public String getAdmobId() {
        return "a14e3a6b428950a";
    }

    /* access modifiers changed from: protected */
    public int getAdmobFilter() {
        return 2;
    }
}
