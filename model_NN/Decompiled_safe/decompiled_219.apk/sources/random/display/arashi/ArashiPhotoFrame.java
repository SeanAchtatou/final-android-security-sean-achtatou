package random.display.arashi;

import random.display.provider.ImageProvider;
import random.display.provider.SDStorageImageProvider;
import random.display.widget.AbstractPhotoFrame;

public class ArashiPhotoFrame extends AbstractPhotoFrame {
    private static SDStorageImageProvider m_ImageProvider = new SDStorageImageProvider("/arashi/");

    /* access modifiers changed from: protected */
    public String getActivityName() {
        return "random.display.arashi/random.display.arashi.Arashi";
    }

    /* access modifiers changed from: protected */
    public int getWaitTimeout() {
        return 20000;
    }

    /* access modifiers changed from: protected */
    public ImageProvider getImageProvider() {
        return m_ImageProvider;
    }
}
