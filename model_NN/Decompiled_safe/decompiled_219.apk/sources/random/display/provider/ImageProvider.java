package random.display.provider;

import android.graphics.Bitmap;

public interface ImageProvider {
    Bitmap downloadImage();

    Bitmap downloadImage(String[] strArr);

    String getCurrentKeyword();

    String getCurrentSource();

    String getCurrentUrl();

    ImageProviderListener getImageProviderListener();

    boolean hasKeywords();

    void initialize();

    void setImageProviderListener(ImageProviderListener imageProviderListener);
}
