package random.display.provider;

public interface ImageProviderListener {
    void onDownloadFailed();

    void onNetworkError();
}
