package random.display.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.widget.RemoteViews;
import random.display.arashi.R;
import random.display.provider.ImageProvider;

public abstract class AbstractPhotoFrame extends AppWidgetProvider {
    public static final String UPDATE_ACTION = "random.display.widgets.PhotoFrame.UPDATE_ACTION";

    /* access modifiers changed from: protected */
    public abstract String getActivityName();

    /* access modifiers changed from: protected */
    public abstract ImageProvider getImageProvider();

    /* access modifiers changed from: protected */
    public abstract int getWaitTimeout();

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setComponent(ComponentName.unflattenFromString(getActivityName()));
        intent.addCategory("android.intent.category.LAUNCHER");
        Bitmap bmFrame = loadImage(context, R.drawable.frame);
        Bitmap bmImage = drawImageOnIt(context, bmFrame);
        bmFrame.recycle();
        RemoteViews views = new RemoteViews(context.getPackageName(), (int) R.layout.photoframewidget);
        views.setBitmap(R.id.photo, "setImageBitmap", bmImage);
        views.setOnClickPendingIntent(R.id.photo, PendingIntent.getActivity(context, 0, intent, 0));
        appWidgetManager.updateAppWidget(appWidgetIds, views);
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    public void onReceive(Context context, Intent intent) {
        if (UPDATE_ACTION.equals(intent.getAction())) {
            ComponentName thisWidget = new ComponentName(context, getClass());
            AppWidgetManager appWidgetMgr = AppWidgetManager.getInstance(context);
            onUpdate(context, appWidgetMgr, appWidgetMgr.getAppWidgetIds(thisWidget));
        }
        super.onReceive(context, intent);
    }

    public void onEnabled(Context context) {
        super.onEnabled(context);
        try {
            ((AlarmManager) context.getSystemService("alarm")).setRepeating(1, System.currentTimeMillis(), (long) getWaitTimeout(), PendingIntent.getBroadcast(context, 0, new Intent(UPDATE_ACTION, null, context, getClass()), 0));
        } catch (Exception e) {
            Log.e("PhotoFrame", "enable alarm failed", e);
        }
    }

    public void onDisabled(Context context) {
        super.onDisabled(context);
        try {
            ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, 0, new Intent(UPDATE_ACTION, null, context, getClass()), 0));
        } catch (Exception e) {
            Log.e("PhotoFrame", "disable alarm failed", e);
        }
    }

    private Bitmap loadImage(Context context, int id) {
        return BitmapFactory.decodeResource(context.getResources(), id);
    }

    private Bitmap selectImage(Context ctx) {
        getImageProvider().initialize();
        Bitmap bmImage = getImageProvider().downloadImage();
        if (bmImage == null) {
            return loadImage(ctx, R.drawable.no_pic);
        }
        return bmImage;
    }

    private Bitmap drawImageOnIt(Context ctx, Bitmap frame) {
        Bitmap bmImage = frame.copy(frame.getConfig(), true);
        Canvas canvas = new Canvas(bmImage);
        Rect rctBackground = new Rect(49, 49, 248, 317);
        Paint p = new Paint();
        p.setAlpha(140);
        p.setColor(-16777216);
        p.setAntiAlias(true);
        p.setStyle(Paint.Style.FILL);
        canvas.drawRect(rctBackground, p);
        Bitmap bmDisplaying = selectImage(ctx);
        float fRatio = Math.min(199.0f / ((float) bmDisplaying.getWidth()), 267.0f / ((float) bmDisplaying.getHeight()));
        Matrix mxDisplaying = new Matrix();
        mxDisplaying.setScale(fRatio, fRatio);
        mxDisplaying.postTranslate(((float) (bmImage.getWidth() / 2)) - ((((float) bmDisplaying.getWidth()) * fRatio) / 2.0f), ((float) (bmImage.getHeight() / 2)) - ((((float) bmDisplaying.getHeight()) * fRatio) / 2.0f));
        canvas.drawBitmap(bmDisplaying, mxDisplaying, null);
        canvas.save();
        bmDisplaying.recycle();
        canvas.restore();
        canvas.drawBitmap(frame, 0.0f, 0.0f, (Paint) null);
        canvas.save();
        return bmImage;
    }
}
