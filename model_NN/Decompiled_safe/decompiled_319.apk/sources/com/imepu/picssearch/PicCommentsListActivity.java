package com.imepu.picssearch;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import com.imepu.picssearch.adapter.CommentsListAdapter;
import com.imepu.picssearch.base.PrcmListActivity;
import com.imepu.picssearch.json.PrcmComment;
import com.imepu.picssearch.task.CommentRequest;
import com.imepu.picssearch.two_ne_one.R;
import java.util.ArrayList;

public class PicCommentsListActivity extends PrcmListActivity {
    private View.OnClickListener commentBtn = new View.OnClickListener() {
        public void onClick(View v) {
            PicCommentsListActivity.this.openUrl(PicCommentsListActivity.this.webUrl);
        }
    };
    private String pictureId;
    private String title;
    /* access modifiers changed from: private */
    public String webUrl;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.pic_comments_list);
        this.pictureId = getIntent().getStringExtra("picture_id");
        this.title = getIntent().getStringExtra("title");
        this.webUrl = getIntent().getStringExtra("web_url");
        setTitle(this.title);
        _findButtonClickListener(R.id.open_web_btn, this.commentBtn);
        postOnCreate();
    }

    public void setTaskParameter(CommentRequest request) {
        request.setPictureId(this.pictureId);
    }

    public ArrayAdapter<PrcmComment> getCommentsListAdapter(PrcmListActivity context, ArrayList<PrcmComment> arrayList) {
        return new CommentsListAdapter(context, arrayList);
    }

    /* access modifiers changed from: protected */
    public void onItemClick(PrcmComment prcmComment) {
        finish();
    }
}
