package com.imepu.picssearch.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.imepu.picssearch.base.PrcmDisplay;
import com.imepu.picssearch.base.PrcmGridActivity;
import com.imepu.picssearch.json.PrcmPic;
import com.imepu.picssearch.two_ne_one.R;
import com.imepu.picssearch.view.PrcmImageView;
import java.util.ArrayList;

public class ImageGridAdapter extends BaseAdapter {
    private PrcmGridActivity mContext;
    private ArrayList<PrcmPic> mList = null;

    public ImageGridAdapter(PrcmGridActivity c, ArrayList<PrcmPic> adapterList) {
        this.mContext = c;
        this.mList = adapterList;
    }

    public int getCount() {
        return this.mList.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        PrcmPic pic = this.mList.get(position);
        PrcmImageView imageView = (PrcmImageView) convertView;
        Float dip = Float.valueOf(PrcmDisplay.getDip(this.mContext));
        if (imageView == null) {
            imageView = new PrcmImageView(this.mContext);
            imageView.setLayoutParams(new AbsListView.LayoutParams(Float.valueOf(100.0f * dip.floatValue()).intValue(), Float.valueOf(120.0f * dip.floatValue()).intValue()));
            imageView.setAdjustViewBounds(false);
            imageView.setBackgroundResource(R.xml.shape_grid_background);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
        int padding = pic.isDecome() ? (int) (37.0f * dip.floatValue()) : (int) (3.0f * dip.floatValue());
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setImageBitmap(null);
        imageView.setImageUrl(pic.getSmallThumbnail().getUrl());
        if (position == this.mList.size() - 1) {
            this.mContext.loadNextPage();
        }
        return imageView;
    }
}
