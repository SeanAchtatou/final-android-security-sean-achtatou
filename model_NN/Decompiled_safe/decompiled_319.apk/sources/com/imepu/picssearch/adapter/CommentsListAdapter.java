package com.imepu.picssearch.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.imepu.picssearch.base.PrcmListActivity;
import com.imepu.picssearch.json.PrcmComment;
import com.imepu.picssearch.json.PrcmThumbnail;
import com.imepu.picssearch.two_ne_one.R;
import com.imepu.picssearch.utils.EmojiUtils;
import com.imepu.picssearch.view.PrcmImageView;
import java.util.List;

public class CommentsListAdapter extends ArrayAdapter<PrcmComment> {
    private PrcmListActivity context;
    private PrcmImageView imageView;
    private LayoutInflater inflater;

    public CommentsListAdapter(Context context2, List<PrcmComment> objects) {
        super(context2, 0, objects);
        this.context = (PrcmListActivity) context2;
        this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = this.inflater.inflate((int) R.layout.comment_row, (ViewGroup) null);
        }
        PrcmComment prcmComment = (PrcmComment) getItem(position);
        if (prcmComment != null) {
            CharSequence commentHtml = Html.fromHtml(EmojiUtils.convertTag(String.valueOf(prcmComment.isLike() ? "%%ix:28%% " : prcmComment.isWarn() ? "%%i:88%% " : "") + prcmComment.getText().trim()), this.context.emojiGetter, null);
            CharSequence screenNameHtml = Html.fromHtml(prcmComment.getScreenName().trim(), this.context.emojiGetter, null);
            PrcmThumbnail thumbnail = prcmComment.getPic().getSmallThumbnail();
            ((TextView) view.findViewById(R.id.comment)).setText(commentHtml);
            ((TextView) view.findViewById(R.id.date)).setText(prcmComment.getCreatedAtScreen().trim());
            ((TextView) view.findViewById(R.id.res_id)).setText(String.valueOf(prcmComment.getResNo()));
            ((TextView) view.findViewById(R.id.screen_name)).setText(screenNameHtml);
            this.imageView = (PrcmImageView) view.findViewById(R.id.thumbnail);
            this.imageView.setImageBitmap(null);
            this.imageView.setImageUrl(thumbnail.getUrl());
            if (position == this.context.getItemCount() - 1) {
                this.context.nextCommentsLoad();
            }
        }
        return view;
    }
}
