package com.imepu.picssearch.http;

import android.util.Log;
import com.imepu.picssearch.json.PrcmComment;
import com.imepu.picssearch.json.PrcmCommentsList;
import com.imepu.picssearch.json.PrcmDetail;
import com.imepu.picssearch.json.PrcmPic;
import com.imepu.picssearch.json.PrcmPicsList;
import com.imepu.picssearch.task.CommentRequest;
import com.imepu.picssearch.utils.ObjectCache;
import com.imepu.picssearch.utils.StringUtils;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PrcmApi extends PrcmApiClient {
    public static final int DEFAULT_SINCE_ID = -1;
    public static final int GRID_COUNT = 50;
    public static final int LIST_COUNT = 30;

    public static PrcmPicsList searchList(String word, int sinceId, int count) throws Exception {
        return getList(addSinceId("/pics/list-picture/search?q=" + URLEncoder.encode(word, "UTF-8") + "&count=" + count, sinceId));
    }

    private static PrcmPicsList getList(String url) throws Exception {
        PrcmPicsList prcmList = (PrcmPicsList) ObjectCache.get(url);
        if (prcmList != null) {
            return prcmList;
        }
        JSONObject jsonObject = doRequest(url);
        PrcmPicsList prcmList2 = new PrcmPicsList(getPicList(jsonObject.getJSONArray("pictures"), true), jsonObject.getInt("since_id"));
        if (jsonObject.has("total")) {
            prcmList2.setTotal(jsonObject.getInt("total"));
            prcmList2.setText(jsonObject.getString("text"));
        }
        ObjectCache.set(url, prcmList2);
        return prcmList2;
    }

    public static ArrayList<PrcmPic> getPicList(JSONArray pics, boolean exceptWarn) {
        ArrayList<PrcmPic> list = new ArrayList<>();
        int i = 0;
        while (i < pics.length()) {
            try {
                JSONObject picObject = pics.getJSONObject(i);
                if (!exceptWarn || !picObject.getBoolean("is_warn")) {
                    list.add(new PrcmPic(picObject));
                    i++;
                } else {
                    i++;
                }
            } catch (JSONException e) {
                Log.d("_list", e.getMessage());
            }
        }
        return list;
    }

    public static PrcmDetail detail(String pictureId) throws Exception {
        if (!StringUtils.isEmpty(pictureId)) {
            return new PrcmDetail(doRequest("/pics/picture/detail?picture_id=" + pictureId + "&emoji=prcm_code"));
        }
        throw new Exception("Error! picture_id is empty.");
    }

    public static PrcmCommentsList comments(CommentRequest param) throws Exception {
        ArrayList<PrcmComment> list = new ArrayList<>();
        String url = addParam(addParam(addParam(addSinceId("/pics/comments/list?emoji=prcm_code&count=" + param.getCount(), param.getSinceId().intValue()), "picture_id", param.getPictureId()), "album_id", param.getAlbumId()), "only", param.getOnly());
        PrcmCommentsList commentsList = (PrcmCommentsList) ObjectCache.get(url);
        if (commentsList != null) {
            return commentsList;
        }
        JSONObject jsonObject = doRequest(url);
        JSONArray comments = jsonObject.getJSONArray(CommentRequest.ITEM_COMMENTS);
        int since_id = jsonObject.getInt("since_id");
        int count = jsonObject.has("count") ? jsonObject.getInt("count") : 0;
        int length = comments.length();
        for (int i = 0; i < length; i++) {
            try {
                list.add(new PrcmComment(comments.getJSONObject(i)));
            } catch (JSONException e) {
                Log.d(CommentRequest.ITEM_COMMENTS, e.getMessage());
            }
        }
        PrcmCommentsList commentsList2 = new PrcmCommentsList(list, since_id, count);
        ObjectCache.set(url, commentsList2);
        return commentsList2;
    }
}
