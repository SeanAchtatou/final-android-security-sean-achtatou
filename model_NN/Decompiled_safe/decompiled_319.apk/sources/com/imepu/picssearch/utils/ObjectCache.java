package com.imepu.picssearch.utils;

import android.util.Log;
import java.util.HashMap;

public class ObjectCache {
    private static HashMap<String, Object> cache = new HashMap<>();

    public static Object get(String key) {
        if (!cache.containsKey(key)) {
            return null;
        }
        if (!cache.containsKey("time_" + key) || System.currentTimeMillis() - ((Long) cache.get("time_" + key)).longValue() <= 600000) {
            Log.d("cache", "cache hit!");
            return cache.get(key);
        }
        deleteKey("time_" + key);
        deleteKey(key);
        return null;
    }

    public static void set(String key, Object json) {
        cache.put("time_" + key, Long.valueOf(System.currentTimeMillis()));
        cache.put(key, json);
    }

    public static void clear() {
        cache.clear();
    }

    public static void deleteKey(String key) {
        cache.remove(key);
    }
}
