package com.imepu.picssearch.utils;

import android.content.Context;
import java.io.FileInputStream;
import java.io.InputStream;

public class StreamUtil {
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0011  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void data2file(byte[] r4, java.lang.String r5) throws java.lang.Exception {
        /*
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x000d }
            r2.<init>(r5)     // Catch:{ Exception -> 0x000d }
            r2.write(r4)     // Catch:{ Exception -> 0x0015 }
            r2.close()     // Catch:{ Exception -> 0x0015 }
            return
        L_0x000d:
            r3 = move-exception
            r0 = r3
        L_0x000f:
            if (r1 == 0) goto L_0x0014
            r1.close()
        L_0x0014:
            throw r0
        L_0x0015:
            r3 = move-exception
            r0 = r3
            r1 = r2
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imepu.picssearch.utils.StreamUtil.data2file(byte[], java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0025 A[SYNTHETIC, Splitter:B:12:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002a A[Catch:{ Exception -> 0x002e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] stream2data(java.io.InputStream r6) throws java.lang.Exception {
        /*
            r5 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r5]
            r1 = 0
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0030 }
            r2.<init>()     // Catch:{ Exception -> 0x0030 }
        L_0x000a:
            int r3 = r6.read(r4)     // Catch:{ Exception -> 0x0020 }
            if (r3 > 0) goto L_0x001b
            r6.close()     // Catch:{ Exception -> 0x0020 }
            r2.close()     // Catch:{ Exception -> 0x0020 }
            byte[] r5 = r2.toByteArray()     // Catch:{ Exception -> 0x0020 }
            return r5
        L_0x001b:
            r5 = 0
            r2.write(r4, r5, r3)     // Catch:{ Exception -> 0x0020 }
            goto L_0x000a
        L_0x0020:
            r5 = move-exception
            r0 = r5
            r1 = r2
        L_0x0023:
            if (r6 == 0) goto L_0x0028
            r6.close()     // Catch:{ Exception -> 0x002e }
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()     // Catch:{ Exception -> 0x002e }
        L_0x002d:
            throw r0
        L_0x002e:
            r5 = move-exception
            goto L_0x002d
        L_0x0030:
            r5 = move-exception
            r0 = r5
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imepu.picssearch.utils.StreamUtil.stream2data(java.io.InputStream):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002c A[SYNTHETIC, Splitter:B:14:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0031 A[Catch:{ Exception -> 0x0035 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] file2data(java.lang.String r8) throws java.lang.Exception {
        /*
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r6 = new byte[r7]
            r1 = 0
            r3 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0037 }
            r2.<init>(r8)     // Catch:{ Exception -> 0x0037 }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x003a }
            r4.<init>()     // Catch:{ Exception -> 0x003a }
        L_0x0010:
            int r5 = r2.read(r6)     // Catch:{ Exception -> 0x0026 }
            if (r5 > 0) goto L_0x0021
            r2.close()     // Catch:{ Exception -> 0x0026 }
            r4.close()     // Catch:{ Exception -> 0x0026 }
            byte[] r7 = r4.toByteArray()     // Catch:{ Exception -> 0x0026 }
            return r7
        L_0x0021:
            r7 = 0
            r4.write(r6, r7, r5)     // Catch:{ Exception -> 0x0026 }
            goto L_0x0010
        L_0x0026:
            r7 = move-exception
            r0 = r7
            r3 = r4
            r1 = r2
        L_0x002a:
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ Exception -> 0x0035 }
        L_0x002f:
            if (r3 == 0) goto L_0x0034
            r3.close()     // Catch:{ Exception -> 0x0035 }
        L_0x0034:
            throw r0
        L_0x0035:
            r7 = move-exception
            goto L_0x0034
        L_0x0037:
            r7 = move-exception
            r0 = r7
            goto L_0x002a
        L_0x003a:
            r7 = move-exception
            r0 = r7
            r1 = r2
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imepu.picssearch.utils.StreamUtil.file2data(java.lang.String):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0021 A[SYNTHETIC, Splitter:B:12:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0026 A[Catch:{ Exception -> 0x002a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void in2file(java.io.InputStream r6, java.lang.String r7) throws java.lang.Exception {
        /*
            r5 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r5]
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x002c }
            r2.<init>(r7)     // Catch:{ Exception -> 0x002c }
        L_0x000a:
            int r3 = r6.read(r4)     // Catch:{ Exception -> 0x001c }
            if (r3 > 0) goto L_0x0017
            r2.close()     // Catch:{ Exception -> 0x001c }
            r6.close()     // Catch:{ Exception -> 0x001c }
            return
        L_0x0017:
            r5 = 0
            r2.write(r4, r5, r3)     // Catch:{ Exception -> 0x001c }
            goto L_0x000a
        L_0x001c:
            r5 = move-exception
            r0 = r5
            r1 = r2
        L_0x001f:
            if (r6 == 0) goto L_0x0024
            r6.close()     // Catch:{ Exception -> 0x002a }
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ Exception -> 0x002a }
        L_0x0029:
            throw r0
        L_0x002a:
            r5 = move-exception
            goto L_0x0029
        L_0x002c:
            r5 = move-exception
            r0 = r5
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imepu.picssearch.utils.StreamUtil.in2file(java.io.InputStream, java.lang.String):void");
    }

    public static InputStream file2in(String fileName) throws Exception {
        return new FileInputStream(fileName);
    }

    public static void raw2file(Context context, int resID, String fileName) throws Exception {
        InputStream in = null;
        try {
            in = context.getResources().openRawResource(resID);
            in2file(in, fileName);
        } catch (Exception e) {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e2) {
                }
            }
        }
    }

    public static void assets2file(Context context, String assetPath, String filePath) throws Exception {
        InputStream in = null;
        try {
            in = context.getAssets().open(assetPath);
            in2file(in, filePath);
        } catch (Exception e) {
            Exception e2 = e;
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e3) {
                }
            }
            throw e2;
        }
    }
}
