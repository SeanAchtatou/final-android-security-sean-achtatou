package com.imepu.picssearch.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String DATE_FORMAT_DB = "yyyy-MM-dd HH:mm:ss";
    public static final int DAY = 86400000;
    public static final int HOUR = 3600000;
    public static final int MINUTE = 60000;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    private static final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DB);

    public static Date toDate(String str) throws Exception {
        return sdf.parse(str);
    }

    public static Date CurrentDate() {
        return new Date(System.currentTimeMillis());
    }

    public static String toString(Date date) {
        return sdf.format(date);
    }

    public static String toScreen(String str) {
        try {
            Date date = toDate(str);
            long time = differenceTime(new Date(System.currentTimeMillis()), date);
            if (time / 259200000 > 0) {
                return dateFormat.format(date);
            }
            if (time / 86400000 > 0) {
                return String.valueOf(String.valueOf(((int) time) / DAY)) + "日前";
            }
            if (time / 3600000 > 0) {
                return String.valueOf(String.valueOf(((int) time) / HOUR)) + "時間前";
            }
            if (time / 60000 > 0) {
                return String.valueOf(String.valueOf(((int) time) / MINUTE)) + "分前";
            }
            return "0分前";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long differenceTime(Date date1, Date date2) {
        return date1.getTime() - date2.getTime();
    }
}
