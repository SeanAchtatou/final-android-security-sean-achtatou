package com.imepu.picssearch.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.imepu.picssearch.http.PrcmHttpGet;
import com.imepu.picssearch.utils.ImageCache;
import java.lang.ref.SoftReference;

public class PrcmImageView extends ImageView {
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public Runnable end;
    /* access modifiers changed from: private */
    public Runnable start;
    private ImageDownloadTask task = new ImageDownloadTask();
    /* access modifiers changed from: private */
    public String url;

    public PrcmImageView(Context context2) {
        super(context2);
        this.context = context2;
    }

    public PrcmImageView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
    }

    public PrcmImageView(Context context2, AttributeSet attrs, int defStyle) {
        super(context2, attrs, defStyle);
        this.context = context2;
    }

    public PrcmImageView setStartRunnable(Runnable start2) {
        this.start = start2;
        return this;
    }

    public PrcmImageView setEndRunnable(Runnable end2) {
        this.end = end2;
        return this;
    }

    public void setImageUrl(String url2) {
        this.url = url2;
        SoftReference<Bitmap> image = ImageCache.getImage(this.context, url2);
        if (image == null || image.get() == null) {
            this.task.cancel(true);
            this.task = new ImageDownloadTask();
            this.task.execute(url2);
            return;
        }
        setImageBitmap(image.get());
    }

    private class ImageDownloadTask extends AsyncTask<String, Void, SoftReference<Bitmap>> {
        private String url = null;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((SoftReference<Bitmap>) ((SoftReference) obj));
        }

        public ImageDownloadTask() {
            if (PrcmImageView.this.start != null) {
                PrcmImageView.this.start.run();
            }
        }

        /* access modifiers changed from: protected */
        public SoftReference<Bitmap> doInBackground(String... urls) {
            try {
                this.url = urls[0];
                SoftReference<Bitmap> image = PrcmHttpGet.getImage(this.url);
                ImageCache.setImage(PrcmImageView.this.context, this.url, image.get());
                if (isCancelled()) {
                    return null;
                }
                return image;
            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e2) {
                e2.printStackTrace();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(SoftReference<Bitmap> result) {
            if (PrcmImageView.this.url.equals(this.url)) {
                if (result == null || result.get() == null) {
                    result = ImageCache.getImage(PrcmImageView.this.getContext(), this.url);
                }
                if (!(result == null || result.get() == null)) {
                    PrcmImageView.this.setImageBitmap(result.get());
                }
                if (PrcmImageView.this.end != null) {
                    PrcmImageView.this.end.run();
                }
            }
        }
    }
}
