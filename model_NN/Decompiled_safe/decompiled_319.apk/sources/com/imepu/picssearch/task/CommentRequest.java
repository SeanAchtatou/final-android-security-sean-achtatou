package com.imepu.picssearch.task;

public class CommentRequest {
    public static final String ITEM_COMMENTS = "comments";
    private String albumId;
    private Integer count;
    private String item;
    private String pictureId;
    private Integer sinceId;

    public CommentRequest(int count2, int sinceId2) {
        this.count = Integer.valueOf(count2);
        this.sinceId = Integer.valueOf(sinceId2);
    }

    public Integer getCount() {
        return this.count;
    }

    public void setCount(Integer count2) {
        this.count = count2;
    }

    public Integer getSinceId() {
        return this.sinceId;
    }

    public void setSinceId(Integer sinceId2) {
        this.sinceId = sinceId2;
    }

    public String getPictureId() {
        return this.pictureId;
    }

    public void setPictureId(String pictureId2) {
        this.pictureId = pictureId2;
    }

    public void setAlbumId(String albumId2) {
        this.albumId = albumId2;
    }

    public String getAlbumId() {
        return this.albumId;
    }

    public void setOnly(String item2) {
        this.item = item2;
    }

    public String getOnly() {
        return this.item;
    }
}
