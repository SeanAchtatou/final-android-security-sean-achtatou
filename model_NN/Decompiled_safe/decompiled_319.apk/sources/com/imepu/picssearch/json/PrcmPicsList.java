package com.imepu.picssearch.json;

import java.util.ArrayList;

public class PrcmPicsList {
    private ArrayList<PrcmPic> prcmPics;
    private int sinceId;
    private String text;
    private int total;

    public PrcmPicsList(ArrayList<PrcmPic> prmcPics, int sinceId2) {
        this.prcmPics = prmcPics;
        this.sinceId = sinceId2;
    }

    public void setTotal(int total2) {
        this.total = total2;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public ArrayList<PrcmPic> getPrcmPics() {
        return this.prcmPics;
    }

    public int getSinceId() {
        return this.sinceId;
    }

    public int getTotal() {
        return this.total;
    }

    public String getText() {
        return this.text;
    }
}
