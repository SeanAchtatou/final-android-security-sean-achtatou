package com.imepu.picssearch.json;

import com.google.ads.AdActivity;
import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class PrcmPic implements Serializable {
    private static final long serialVersionUID = -5450845761881390556L;
    String createdAt;
    boolean isDecome;
    boolean isWarn;
    PrcmThumbnail largeThumbnail;
    PrcmThumbnail middleThumbnail;
    PrcmThumbnail originalThumbnail;
    String pictureId;
    PrcmThumbnail smallThumbnail;
    String title;
    String type;

    public PrcmPic(JSONObject json) throws JSONException {
        this.pictureId = json.getString("picture_id");
        this.createdAt = json.getString("created_at");
        this.title = json.getString("title");
        this.type = json.getString("type");
        this.isWarn = json.getBoolean("is_warn");
        this.isDecome = json.getBoolean("is_decome");
        JSONObject thumbnails = json.getJSONObject("thumbnails");
        this.smallThumbnail = new PrcmThumbnail(thumbnails.getJSONObject("s"));
        this.originalThumbnail = new PrcmThumbnail(thumbnails.getJSONObject("original"));
        this.largeThumbnail = new PrcmThumbnail(thumbnails.getJSONObject("l"));
        this.middleThumbnail = new PrcmThumbnail(thumbnails.getJSONObject(AdActivity.TYPE_PARAM));
    }

    public String getPictureId() {
        return this.pictureId;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public String getTitle() {
        return this.title;
    }

    public String getType() {
        return this.type;
    }

    public boolean isWarn() {
        return this.isWarn;
    }

    public boolean isDecome() {
        return this.isDecome;
    }

    public PrcmThumbnail getSmallThumbnail() {
        return this.smallThumbnail;
    }

    public PrcmThumbnail getOriginalThumbnail() {
        return this.originalThumbnail;
    }

    public PrcmThumbnail getLargeThumbnail() {
        return this.largeThumbnail;
    }

    public PrcmThumbnail getMiddleThumbnail() {
        return this.middleThumbnail;
    }

    public String getWebUrl() {
        return "http://r0.prcm.jp/gazo/i/" + getPictureId();
    }
}
