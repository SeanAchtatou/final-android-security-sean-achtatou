package com.imepu.picssearch.json;

import java.util.ArrayList;

public class PrcmCommentsList {
    private int count;
    private ArrayList<PrcmComment> prcmComments;
    private int sinceId;

    public PrcmCommentsList(ArrayList<PrcmComment> prcmComments2, int sinceId2, int count2) {
        this.prcmComments = prcmComments2;
        this.sinceId = sinceId2;
        this.count = count2;
    }

    public ArrayList<PrcmComment> getComments() {
        return this.prcmComments;
    }

    public int getSinceId() {
        return this.sinceId;
    }

    public int getCount() {
        return this.count;
    }
}
