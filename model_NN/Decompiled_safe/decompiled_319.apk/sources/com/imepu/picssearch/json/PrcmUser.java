package com.imepu.picssearch.json;

import org.json.JSONException;
import org.json.JSONObject;

public class PrcmUser {
    private boolean blocked;
    private String createdAt;
    private String dispAge;
    private String dispSex;
    private String profileId;
    private String screenName;
    private String userId;

    public PrcmUser() {
    }

    public PrcmUser(JSONObject json) throws JSONException {
        this.userId = json.getString("user_id");
        this.blocked = json.getBoolean("blocked");
        this.screenName = json.getString("screen_name");
        this.dispAge = json.getString("disp_age");
        this.dispSex = json.getString("disp_sex");
        this.profileId = json.getString("profile_id");
        this.createdAt = json.getString("created_at");
    }

    public String getUserId() {
        return this.userId;
    }

    public boolean isBlocked() {
        return this.blocked;
    }

    public String getScreenName() {
        return this.screenName;
    }

    public String getDispAge() {
        return this.dispAge;
    }

    public String getDispSex() {
        return this.dispSex;
    }

    public String getProfileId() {
        return this.profileId;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setScreenName(String screenName2) {
        this.screenName = screenName2;
    }

    public void setDispAge(String dispAge2) {
        this.dispAge = dispAge2;
    }

    public void setDispSex(String dispSex2) {
        this.dispSex = dispSex2;
    }
}
