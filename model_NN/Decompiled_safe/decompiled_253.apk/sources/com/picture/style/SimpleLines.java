package com.picture.style;

import android.graphics.Canvas;
import android.graphics.Paint;
import jp.co.hikesiya.Style;

public class SimpleLines implements Style {
    private Paint paint = new Paint();
    private float prevX;
    private float prevY;

    public SimpleLines(int id) {
        this.paint.setAntiAlias(true);
        if (id == 2001) {
            this.paint.setStrokeWidth(5.0f);
        } else if (id == 2002) {
            this.paint.setStrokeWidth(10.0f);
        } else if (id == 2003) {
            this.paint.setStrokeWidth(18.0f);
        } else {
            this.paint.setStrokeWidth(8.0f);
        }
    }

    public void strokeStart(Canvas c, float x, float y) {
        this.prevX = x;
        this.prevY = y;
    }

    public void stroke(Canvas c, float x, float y) {
        float r = this.paint.getStrokeWidth() / 2.0f;
        c.drawCircle(this.prevX, this.prevY, r, this.paint);
        c.drawLine(this.prevX, this.prevY, x, y, this.paint);
        c.drawCircle(x, y, r, this.paint);
        this.prevX = x;
        this.prevY = y;
    }

    public void setColor(int color) {
        this.paint.setColor(color);
    }

    public int getColor() {
        return this.paint.getColor();
    }

    public void draw(Canvas c) {
    }
}
