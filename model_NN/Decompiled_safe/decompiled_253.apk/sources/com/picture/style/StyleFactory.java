package com.picture.style;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.widget.ImageButton;
import java.util.HashMap;
import java.util.Map;
import jp.co.hikesiya.R;
import jp.co.hikesiya.Style;

public class StyleFactory {
    public static final int PEN_COLOR_BLACK = 5015;
    public static final int PEN_COLOR_BLUE = 5009;
    public static final int PEN_COLOR_BROWN = 5011;
    public static final int PEN_COLOR_CORAL = 5002;
    public static final int PEN_COLOR_DEEPGREEN = 5007;
    public static final int PEN_COLOR_GRAY = 5014;
    public static final int PEN_COLOR_GREEN = 5006;
    public static final int PEN_COLOR_LIGHTBLUE = 5008;
    public static final int PEN_COLOR_LIGHTPURPLE = 5013;
    public static final int PEN_COLOR_NAVY = 5010;
    public static final int PEN_COLOR_ORANGE = 5004;
    public static final int PEN_COLOR_PINK = 5001;
    public static final int PEN_COLOR_PURPLE = 5012;
    public static final int PEN_COLOR_RED = 5003;
    public static final int PEN_COLOR_WHITE = 5000;
    public static final int PEN_COLOR_YELLOW = 5005;
    public static final int SIMPLE_01 = 2001;
    public static final int SIMPLE_02 = 2002;
    public static final int SIMPLE_03 = 2003;
    public static final int STAMP_ARIGATOU_01 = 3307;
    public static final int STAMP_ARIGATOU_02 = 3308;
    public static final int STAMP_BABY_01 = 3006;
    public static final int STAMP_CHU_01 = 3300;
    public static final int STAMP_CHU_02 = 3301;
    public static final int STAMP_FUKIDASI_01_1 = 3200;
    public static final int STAMP_FUKIDASI_01_2 = 3201;
    public static final int STAMP_FUKIDASI_01_3 = 3202;
    public static final int STAMP_FUKIDASI_01_4 = 3203;
    public static final int STAMP_FUKIDASI_02_1 = 3204;
    public static final int STAMP_FUKIDASI_02_2 = 3205;
    public static final int STAMP_FUKIDASI_02_3 = 3206;
    public static final int STAMP_FUKIDASI_02_4 = 3207;
    public static final int STAMP_HANAMARU_01 = 3008;
    public static final int STAMP_HANA_01 = 3001;
    public static final int STAMP_HAPPYBIRTHDAY_01 = 3302;
    public static final int STAMP_HEART_01 = 3002;
    public static final int STAMP_HEART_KORO_01 = 3106;
    public static final int STAMP_HEART_KORO_02 = 3107;
    public static final int STAMP_HIMAWARI_01 = 3000;
    public static final int STAMP_HITOMI_01 = 3011;
    public static final int STAMP_HOHO_01 = 3004;
    public static final int STAMP_ITADAKIMASU_01 = 3311;
    public static final int STAMP_KINOKO_01 = 3003;
    public static final int STAMP_KINTARO_01 = 3007;
    public static final int STAMP_KIRAKIRA_01 = 3100;
    public static final int STAMP_KIRAKIRA_02 = 3101;
    public static final int STAMP_KIRAKIRA_03 = 3102;
    public static final int STAMP_KIRAKIRA_04 = 3104;
    public static final int STAMP_KIRAKIRA_05 = 3105;
    public static final int STAMP_KONNICHIWA_01 = 3309;
    public static final int STAMP_MEGANE_01 = 3010;
    public static final int STAMP_MOTTO_01 = 3009;
    public static final int STAMP_ONPU_01 = 3005;
    public static final int STAMP_SAYONARA_01 = 3310;
    public static final int STAMP_STAR_01 = 3103;
    public static final int STAMP_THANKYOU_01 = 3303;
    public static final int STAMP_THANKYOU_02 = 3304;
    public static final int STAMP_YEAH_01 = 3305;
    public static final int STAMP_YEAH_02 = 3306;
    private static ImageButton arigato01Button;
    private static ImageButton arigato02Button;
    private static ImageButton baby01Button;
    private static ImageButton black;
    private static ImageButton blue;
    private static ImageButton brown;
    private static Map<Integer, Style> cache = new HashMap();
    private static ImageButton chu01Button;
    private static ImageButton chu02Button;
    private static HashMap<Integer, ImageButton> colorStyleMap = new HashMap<>();
    private static ImageButton coral;
    private static int currentStyle = SIMPLE_02;
    private static ImageButton deepgreen;
    private static ImageButton fukidasi01Button_1;
    private static ImageButton fukidasi01Button_2;
    private static ImageButton fukidasi01Button_3;
    private static ImageButton fukidasi01Button_4;
    private static ImageButton fukidasi02Button_1;
    private static ImageButton fukidasi02Button_2;
    private static ImageButton fukidasi02Button_3;
    private static ImageButton fukidasi02Button_4;
    private static ImageButton gray;
    private static ImageButton green;
    private static ImageButton hana01Button;
    private static ImageButton hanamaru01Button;
    private static ImageButton happybirthday01Button;
    private static ImageButton heart01Button;
    private static ImageButton heartKoro01Button;
    private static ImageButton heartKoro02Button;
    private static ImageButton himawari01Button;
    private static ImageButton hitomi01Button;
    private static ImageButton hoho01Button;
    private static ImageButton itadakimasu01Button;
    private static ImageButton kinoko01Button;
    private static ImageButton kintaro01Button;
    private static ImageButton kirakira01Button;
    private static ImageButton kirakira02Button;
    private static ImageButton kirakira03Button;
    private static ImageButton kirakira04Button;
    private static ImageButton kirakira05Button;
    private static ImageButton konnichiwa01Button;
    private static ImageButton lightblue;
    private static ImageButton lightpurple;
    private static ImageButton megane01Button;
    private static ImageButton motto01Button;
    private static ImageButton navy;
    private static ImageButton onpu01Button;
    private static ImageButton orange;
    private static ImageButton pen01;
    private static ImageButton pen02;
    private static ImageButton pen03;
    private static HashMap<Integer, ImageButton> penStyleMap = new HashMap<>();
    private static ImageButton pink;
    private static ImageButton purple;
    private static ImageButton red;
    private static ImageButton sayonara01Button;
    private static HashMap<Integer, ImageButton> stampStyleMap = new HashMap<>();
    private static ImageButton star01Button;
    private static ImageButton thankyou01Button;
    private static ImageButton thankyou02Button;
    private static ImageButton white;
    private static ImageButton yeah01Button;
    private static ImageButton yeah02Button;
    private static ImageButton yellow;

    public static HashMap<Integer, ImageButton> getPenStyleMap(Context c) {
        initPenImageButton(c);
        penStyleMap.put(Integer.valueOf((int) SIMPLE_01), pen01);
        penStyleMap.put(Integer.valueOf((int) SIMPLE_02), pen02);
        penStyleMap.put(Integer.valueOf((int) SIMPLE_03), pen03);
        return penStyleMap;
    }

    public static HashMap<Integer, ImageButton> getColorStyleMap(Context c) {
        initColorImageButton(c);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_WHITE), white);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_PINK), pink);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_CORAL), coral);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_RED), red);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_ORANGE), orange);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_YELLOW), yellow);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_GREEN), green);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_DEEPGREEN), deepgreen);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_LIGHTBLUE), lightblue);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_BLUE), blue);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_NAVY), navy);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_BROWN), brown);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_PURPLE), purple);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_LIGHTPURPLE), lightpurple);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_GRAY), gray);
        colorStyleMap.put(Integer.valueOf((int) PEN_COLOR_BLACK), black);
        return colorStyleMap;
    }

    public static HashMap<Integer, ImageButton> getStampStyleMap(Context c) {
        initStampImageButton(c);
        stampStyleMap.put(Integer.valueOf((int) STAMP_HIMAWARI_01), himawari01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_HANA_01), hana01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_HEART_01), heart01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_KINOKO_01), kinoko01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_HOHO_01), hoho01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_ONPU_01), onpu01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_BABY_01), baby01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_KINTARO_01), kintaro01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_HANAMARU_01), hanamaru01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_MOTTO_01), motto01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_MEGANE_01), megane01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_HITOMI_01), hitomi01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_KIRAKIRA_01), kirakira01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_KIRAKIRA_02), kirakira02Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_KIRAKIRA_03), kirakira03Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_STAR_01), star01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_KIRAKIRA_04), kirakira04Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_KIRAKIRA_05), kirakira05Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_HEART_KORO_01), heartKoro01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_HEART_KORO_02), heartKoro02Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_FUKIDASI_01_1), fukidasi01Button_1);
        stampStyleMap.put(Integer.valueOf((int) STAMP_FUKIDASI_01_2), fukidasi01Button_2);
        stampStyleMap.put(Integer.valueOf((int) STAMP_FUKIDASI_01_3), fukidasi01Button_3);
        stampStyleMap.put(Integer.valueOf((int) STAMP_FUKIDASI_01_4), fukidasi01Button_4);
        stampStyleMap.put(Integer.valueOf((int) STAMP_FUKIDASI_02_1), fukidasi02Button_1);
        stampStyleMap.put(Integer.valueOf((int) STAMP_FUKIDASI_02_2), fukidasi02Button_2);
        stampStyleMap.put(Integer.valueOf((int) STAMP_FUKIDASI_02_3), fukidasi02Button_3);
        stampStyleMap.put(Integer.valueOf((int) STAMP_FUKIDASI_02_4), fukidasi02Button_4);
        stampStyleMap.put(Integer.valueOf((int) STAMP_CHU_01), chu01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_CHU_02), chu02Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_HAPPYBIRTHDAY_01), happybirthday01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_THANKYOU_01), thankyou01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_THANKYOU_02), thankyou02Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_YEAH_01), yeah01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_YEAH_02), yeah02Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_ARIGATOU_01), arigato01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_ARIGATOU_02), arigato02Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_KONNICHIWA_01), konnichiwa01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_SAYONARA_01), sayonara01Button);
        stampStyleMap.put(Integer.valueOf((int) STAMP_ITADAKIMASU_01), itadakimasu01Button);
        return stampStyleMap;
    }

    private static void initPenImageButton(Context c) {
        pen01 = new ImageButton(c);
        pen01.setImageBitmap(resizeImage(R.drawable.select_line_simple_01, c));
        pen01.setTag(Integer.valueOf((int) SIMPLE_01));
        pen02 = new ImageButton(c);
        pen02.setImageBitmap(resizeImage(R.drawable.select_line_simple_02, c));
        pen02.setTag(Integer.valueOf((int) SIMPLE_02));
        pen03 = new ImageButton(c);
        pen03.setImageBitmap(resizeImage(R.drawable.select_line_simple_03, c));
        pen03.setTag(Integer.valueOf((int) SIMPLE_03));
    }

    private static void initColorImageButton(Context c) {
        white = new ImageButton(c);
        white.setImageBitmap(resizeImage(R.drawable.select_color_white, c));
        white.setTag(Integer.valueOf((int) PEN_COLOR_WHITE));
        pink = new ImageButton(c);
        pink.setImageBitmap(resizeImage(R.drawable.select_color_pink, c));
        pink.setTag(Integer.valueOf((int) PEN_COLOR_PINK));
        coral = new ImageButton(c);
        coral.setImageBitmap(resizeImage(R.drawable.select_color_coral, c));
        coral.setTag(Integer.valueOf((int) PEN_COLOR_CORAL));
        red = new ImageButton(c);
        red.setImageBitmap(resizeImage(R.drawable.select_color_red, c));
        red.setTag(Integer.valueOf((int) PEN_COLOR_RED));
        orange = new ImageButton(c);
        orange.setImageBitmap(resizeImage(R.drawable.select_color_orange, c));
        orange.setTag(Integer.valueOf((int) PEN_COLOR_ORANGE));
        yellow = new ImageButton(c);
        yellow.setImageBitmap(resizeImage(R.drawable.select_color_yellow, c));
        yellow.setTag(Integer.valueOf((int) PEN_COLOR_YELLOW));
        green = new ImageButton(c);
        green.setImageBitmap(resizeImage(R.drawable.select_color_green, c));
        green.setTag(Integer.valueOf((int) PEN_COLOR_GREEN));
        deepgreen = new ImageButton(c);
        deepgreen.setImageBitmap(resizeImage(R.drawable.select_color_deepgreen, c));
        deepgreen.setTag(Integer.valueOf((int) PEN_COLOR_DEEPGREEN));
        lightblue = new ImageButton(c);
        lightblue.setImageBitmap(resizeImage(R.drawable.select_color_lightblue, c));
        lightblue.setTag(Integer.valueOf((int) PEN_COLOR_LIGHTBLUE));
        blue = new ImageButton(c);
        blue.setImageBitmap(resizeImage(R.drawable.select_color_blue, c));
        blue.setTag(Integer.valueOf((int) PEN_COLOR_BLUE));
        navy = new ImageButton(c);
        navy.setImageBitmap(resizeImage(R.drawable.select_color_navy, c));
        navy.setTag(Integer.valueOf((int) PEN_COLOR_NAVY));
        brown = new ImageButton(c);
        brown.setImageBitmap(resizeImage(R.drawable.select_color_brown, c));
        brown.setTag(Integer.valueOf((int) PEN_COLOR_BROWN));
        purple = new ImageButton(c);
        purple.setImageBitmap(resizeImage(R.drawable.select_color_purple, c));
        purple.setTag(Integer.valueOf((int) PEN_COLOR_PURPLE));
        lightpurple = new ImageButton(c);
        lightpurple.setImageBitmap(resizeImage(R.drawable.select_color_lightpurple, c));
        lightpurple.setTag(Integer.valueOf((int) PEN_COLOR_LIGHTPURPLE));
        gray = new ImageButton(c);
        gray.setImageBitmap(resizeImage(R.drawable.select_color_gray, c));
        gray.setTag(Integer.valueOf((int) PEN_COLOR_GRAY));
        black = new ImageButton(c);
        black.setImageBitmap(resizeImage(R.drawable.select_color_black, c));
        black.setTag(Integer.valueOf((int) PEN_COLOR_BLACK));
    }

    private static void initStampImageButton(Context c) {
        himawari01Button = new ImageButton(c);
        himawari01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_himawari_01, c));
        himawari01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        himawari01Button.setTag(Integer.valueOf((int) STAMP_HIMAWARI_01));
        hana01Button = new ImageButton(c);
        hana01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_hana_01, c));
        hana01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        hana01Button.setTag(Integer.valueOf((int) STAMP_HANA_01));
        heart01Button = new ImageButton(c);
        heart01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_heart_01, c));
        heart01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        heart01Button.setTag(Integer.valueOf((int) STAMP_HEART_01));
        kinoko01Button = new ImageButton(c);
        kinoko01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_kinoko_01, c));
        kinoko01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        kinoko01Button.setTag(Integer.valueOf((int) STAMP_KINOKO_01));
        hoho01Button = new ImageButton(c);
        hoho01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_hoho_01, c));
        hoho01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        hoho01Button.setTag(Integer.valueOf((int) STAMP_HOHO_01));
        onpu01Button = new ImageButton(c);
        onpu01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_onpu_01, c));
        onpu01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        onpu01Button.setTag(Integer.valueOf((int) STAMP_ONPU_01));
        baby01Button = new ImageButton(c);
        baby01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_baby_01, c));
        baby01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        baby01Button.setTag(Integer.valueOf((int) STAMP_BABY_01));
        kintaro01Button = new ImageButton(c);
        kintaro01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_kintaro_01, c));
        kintaro01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        kintaro01Button.setTag(Integer.valueOf((int) STAMP_KINTARO_01));
        hanamaru01Button = new ImageButton(c);
        hanamaru01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_hanamaru, c));
        hanamaru01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        hanamaru01Button.setTag(Integer.valueOf((int) STAMP_HANAMARU_01));
        motto01Button = new ImageButton(c);
        motto01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_motto, c));
        motto01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        motto01Button.setTag(Integer.valueOf((int) STAMP_MOTTO_01));
        megane01Button = new ImageButton(c);
        megane01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_megane_01, c));
        megane01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        megane01Button.setTag(Integer.valueOf((int) STAMP_MEGANE_01));
        hitomi01Button = new ImageButton(c);
        hitomi01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_hitomi_01, c));
        hitomi01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        hitomi01Button.setTag(Integer.valueOf((int) STAMP_HITOMI_01));
        kirakira01Button = new ImageButton(c);
        kirakira01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_kirakira_01, c));
        kirakira01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        kirakira01Button.setTag(Integer.valueOf((int) STAMP_KIRAKIRA_01));
        kirakira02Button = new ImageButton(c);
        kirakira02Button.setImageBitmap(resizeImage(R.drawable.select_stamp_kirakira_02, c));
        kirakira02Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        kirakira02Button.setTag(Integer.valueOf((int) STAMP_KIRAKIRA_02));
        kirakira03Button = new ImageButton(c);
        kirakira03Button.setImageBitmap(resizeImage(R.drawable.select_stamp_kirakira_03, c));
        kirakira03Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        kirakira03Button.setTag(Integer.valueOf((int) STAMP_KIRAKIRA_03));
        star01Button = new ImageButton(c);
        star01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_star_01, c));
        star01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        star01Button.setTag(Integer.valueOf((int) STAMP_STAR_01));
        kirakira04Button = new ImageButton(c);
        kirakira04Button.setImageBitmap(resizeImage(R.drawable.select_stamp_kirakira_04, c));
        kirakira04Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        kirakira04Button.setTag(Integer.valueOf((int) STAMP_KIRAKIRA_04));
        kirakira05Button = new ImageButton(c);
        kirakira05Button.setImageBitmap(resizeImage(R.drawable.select_stamp_kirakira_05, c));
        kirakira05Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        kirakira05Button.setTag(Integer.valueOf((int) STAMP_KIRAKIRA_05));
        heartKoro01Button = new ImageButton(c);
        heartKoro01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_heart_koro_01, c));
        heartKoro01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        heartKoro01Button.setTag(Integer.valueOf((int) STAMP_HEART_KORO_01));
        heartKoro02Button = new ImageButton(c);
        heartKoro02Button.setImageBitmap(resizeImage(R.drawable.select_stamp_heart_koro_02, c));
        heartKoro02Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        heartKoro02Button.setTag(Integer.valueOf((int) STAMP_HEART_KORO_02));
        fukidasi01Button_1 = new ImageButton(c);
        fukidasi01Button_1.setImageBitmap(resizeImage(R.drawable.select_stamp_fukidasi_01_1, c));
        fukidasi01Button_1.setBackgroundColor(Color.argb(0, 0, 0, 0));
        fukidasi01Button_1.setTag(Integer.valueOf((int) STAMP_FUKIDASI_01_1));
        fukidasi01Button_2 = new ImageButton(c);
        fukidasi01Button_2.setImageBitmap(resizeImage(R.drawable.select_stamp_fukidasi_01_2, c));
        fukidasi01Button_2.setBackgroundColor(Color.argb(0, 0, 0, 0));
        fukidasi01Button_2.setTag(Integer.valueOf((int) STAMP_FUKIDASI_01_2));
        fukidasi01Button_3 = new ImageButton(c);
        fukidasi01Button_3.setImageBitmap(resizeImage(R.drawable.select_stamp_fukidasi_01_3, c));
        fukidasi01Button_3.setBackgroundColor(Color.argb(0, 0, 0, 0));
        fukidasi01Button_3.setTag(Integer.valueOf((int) STAMP_FUKIDASI_01_3));
        fukidasi01Button_4 = new ImageButton(c);
        fukidasi01Button_4.setImageBitmap(resizeImage(R.drawable.select_stamp_fukidasi_01_4, c));
        fukidasi01Button_4.setBackgroundColor(Color.argb(0, 0, 0, 0));
        fukidasi01Button_4.setTag(Integer.valueOf((int) STAMP_FUKIDASI_01_4));
        fukidasi02Button_1 = new ImageButton(c);
        fukidasi02Button_1.setImageBitmap(resizeImage(R.drawable.select_stamp_fukidasi_02_1, c));
        fukidasi02Button_1.setBackgroundColor(Color.argb(0, 0, 0, 0));
        fukidasi02Button_1.setTag(Integer.valueOf((int) STAMP_FUKIDASI_02_1));
        fukidasi02Button_2 = new ImageButton(c);
        fukidasi02Button_2.setImageBitmap(resizeImage(R.drawable.select_stamp_fukidasi_02_2, c));
        fukidasi02Button_2.setBackgroundColor(Color.argb(0, 0, 0, 0));
        fukidasi02Button_2.setTag(Integer.valueOf((int) STAMP_FUKIDASI_02_2));
        fukidasi02Button_3 = new ImageButton(c);
        fukidasi02Button_3.setImageBitmap(resizeImage(R.drawable.select_stamp_fukidasi_02_3, c));
        fukidasi02Button_3.setBackgroundColor(Color.argb(0, 0, 0, 0));
        fukidasi02Button_3.setTag(Integer.valueOf((int) STAMP_FUKIDASI_02_3));
        fukidasi02Button_4 = new ImageButton(c);
        fukidasi02Button_4.setImageBitmap(resizeImage(R.drawable.select_stamp_fukidasi_02_4, c));
        fukidasi02Button_4.setBackgroundColor(Color.argb(0, 0, 0, 0));
        fukidasi02Button_4.setTag(Integer.valueOf((int) STAMP_FUKIDASI_02_4));
        chu01Button = new ImageButton(c);
        chu01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_chu_01, c));
        chu01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        chu01Button.setTag(Integer.valueOf((int) STAMP_CHU_01));
        chu02Button = new ImageButton(c);
        chu02Button.setImageBitmap(resizeImage(R.drawable.select_stamp_chu_02, c));
        chu02Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        chu02Button.setTag(Integer.valueOf((int) STAMP_CHU_02));
        happybirthday01Button = new ImageButton(c);
        happybirthday01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_happybirthday_01, c));
        happybirthday01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        happybirthday01Button.setTag(Integer.valueOf((int) STAMP_HAPPYBIRTHDAY_01));
        thankyou01Button = new ImageButton(c);
        thankyou01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_thankyou_01, c));
        thankyou01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        thankyou01Button.setTag(Integer.valueOf((int) STAMP_THANKYOU_01));
        thankyou02Button = new ImageButton(c);
        thankyou02Button.setImageBitmap(resizeImage(R.drawable.select_stamp_thankyou_02, c));
        thankyou02Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        thankyou02Button.setTag(Integer.valueOf((int) STAMP_THANKYOU_02));
        yeah01Button = new ImageButton(c);
        yeah01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_yeah_01, c));
        yeah01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        yeah01Button.setTag(Integer.valueOf((int) STAMP_YEAH_01));
        yeah02Button = new ImageButton(c);
        yeah02Button.setImageBitmap(resizeImage(R.drawable.select_stamp_yeah_02, c));
        yeah02Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        yeah02Button.setTag(Integer.valueOf((int) STAMP_YEAH_02));
        arigato01Button = new ImageButton(c);
        arigato01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_arigatou_01, c));
        arigato01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        arigato01Button.setTag(Integer.valueOf((int) STAMP_ARIGATOU_01));
        arigato02Button = new ImageButton(c);
        arigato02Button.setImageBitmap(resizeImage(R.drawable.select_stamp_arigatou_02, c));
        arigato02Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        arigato02Button.setTag(Integer.valueOf((int) STAMP_ARIGATOU_02));
        konnichiwa01Button = new ImageButton(c);
        konnichiwa01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_konnichiwa_01, c));
        konnichiwa01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        konnichiwa01Button.setTag(Integer.valueOf((int) STAMP_KONNICHIWA_01));
        sayonara01Button = new ImageButton(c);
        sayonara01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_sayonara_01, c));
        sayonara01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        sayonara01Button.setTag(Integer.valueOf((int) STAMP_SAYONARA_01));
        itadakimasu01Button = new ImageButton(c);
        itadakimasu01Button.setImageBitmap(resizeImage(R.drawable.select_stamp_itadakimasu_01, c));
        itadakimasu01Button.setBackgroundColor(Color.argb(0, 0, 0, 0));
        itadakimasu01Button.setTag(Integer.valueOf((int) STAMP_ITADAKIMASU_01));
    }

    public static void clearCache() {
        cache.clear();
    }

    public static Style getStyle(Context c, int id) {
        if (!cache.containsKey(Integer.valueOf(id))) {
            cache.put(Integer.valueOf(id), getStyleInstance(c, id));
        }
        currentStyle = id;
        return cache.get(Integer.valueOf(id));
    }

    public static Style getStyle(Context c, int id, Bitmap bm) {
        if (!cache.containsKey(Integer.valueOf(id))) {
            cache.put(Integer.valueOf(id), getStyleInstance(c, id, bm));
        }
        currentStyle = id;
        return cache.get(Integer.valueOf(id));
    }

    public static Style getCurrentStyle(Context c) {
        return getStyle(c, currentStyle, null);
    }

    public static int getCurrentId() {
        return currentStyle;
    }

    private static Style getStyleInstance(Context c, int id) {
        switch (id) {
            case SIMPLE_01 /*2001*/:
            case SIMPLE_02 /*2002*/:
            case SIMPLE_03 /*2003*/:
                return new SimpleLines(id);
            case STAMP_HIMAWARI_01 /*3000*/:
            case STAMP_HANA_01 /*3001*/:
            case STAMP_HEART_01 /*3002*/:
            case STAMP_KINOKO_01 /*3003*/:
            case STAMP_HOHO_01 /*3004*/:
            case STAMP_ONPU_01 /*3005*/:
            case STAMP_BABY_01 /*3006*/:
            case STAMP_KINTARO_01 /*3007*/:
            case STAMP_HANAMARU_01 /*3008*/:
            case STAMP_MOTTO_01 /*3009*/:
            case STAMP_MEGANE_01 /*3010*/:
            case STAMP_HITOMI_01 /*3011*/:
            case STAMP_FUKIDASI_01_1 /*3200*/:
            case STAMP_FUKIDASI_01_2 /*3201*/:
            case STAMP_FUKIDASI_01_3 /*3202*/:
            case STAMP_FUKIDASI_01_4 /*3203*/:
            case STAMP_FUKIDASI_02_1 /*3204*/:
            case STAMP_FUKIDASI_02_2 /*3205*/:
            case STAMP_FUKIDASI_02_3 /*3206*/:
            case STAMP_FUKIDASI_02_4 /*3207*/:
            case STAMP_CHU_01 /*3300*/:
            case STAMP_CHU_02 /*3301*/:
            case STAMP_HAPPYBIRTHDAY_01 /*3302*/:
            case STAMP_THANKYOU_01 /*3303*/:
            case STAMP_THANKYOU_02 /*3304*/:
            case STAMP_YEAH_01 /*3305*/:
            case STAMP_YEAH_02 /*3306*/:
            case STAMP_ARIGATOU_01 /*3307*/:
            case STAMP_ARIGATOU_02 /*3308*/:
            case STAMP_KONNICHIWA_01 /*3309*/:
            case STAMP_SAYONARA_01 /*3310*/:
            case STAMP_ITADAKIMASU_01 /*3311*/:
                return new Stamps(c, id);
            case STAMP_KIRAKIRA_01 /*3100*/:
            case STAMP_KIRAKIRA_02 /*3101*/:
            case STAMP_KIRAKIRA_03 /*3102*/:
            case STAMP_STAR_01 /*3103*/:
            case STAMP_KIRAKIRA_04 /*3104*/:
            case STAMP_KIRAKIRA_05 /*3105*/:
            case STAMP_HEART_KORO_01 /*3106*/:
            case STAMP_HEART_KORO_02 /*3107*/:
                return new KoroKoroStamps(c, id);
            default:
                throw new RuntimeException("Invalid style ID");
        }
    }

    private static Style getStyleInstance(Context c, int id, Bitmap bm) {
        switch (id) {
            case SIMPLE_01 /*2001*/:
            case SIMPLE_02 /*2002*/:
            case SIMPLE_03 /*2003*/:
                return new SimpleLines(id);
            case STAMP_HIMAWARI_01 /*3000*/:
            case STAMP_HANA_01 /*3001*/:
            case STAMP_HEART_01 /*3002*/:
            case STAMP_KINOKO_01 /*3003*/:
            case STAMP_HOHO_01 /*3004*/:
            case STAMP_ONPU_01 /*3005*/:
            case STAMP_BABY_01 /*3006*/:
            case STAMP_KINTARO_01 /*3007*/:
            case STAMP_HANAMARU_01 /*3008*/:
            case STAMP_MOTTO_01 /*3009*/:
            case STAMP_MEGANE_01 /*3010*/:
            case STAMP_HITOMI_01 /*3011*/:
            case STAMP_FUKIDASI_01_1 /*3200*/:
            case STAMP_FUKIDASI_01_2 /*3201*/:
            case STAMP_FUKIDASI_01_3 /*3202*/:
            case STAMP_FUKIDASI_01_4 /*3203*/:
            case STAMP_FUKIDASI_02_1 /*3204*/:
            case STAMP_FUKIDASI_02_2 /*3205*/:
            case STAMP_FUKIDASI_02_3 /*3206*/:
            case STAMP_FUKIDASI_02_4 /*3207*/:
            case STAMP_CHU_01 /*3300*/:
            case STAMP_CHU_02 /*3301*/:
            case STAMP_HAPPYBIRTHDAY_01 /*3302*/:
            case STAMP_THANKYOU_01 /*3303*/:
            case STAMP_THANKYOU_02 /*3304*/:
            case STAMP_YEAH_01 /*3305*/:
            case STAMP_YEAH_02 /*3306*/:
            case STAMP_ARIGATOU_01 /*3307*/:
            case STAMP_ARIGATOU_02 /*3308*/:
            case STAMP_KONNICHIWA_01 /*3309*/:
            case STAMP_SAYONARA_01 /*3310*/:
            case STAMP_ITADAKIMASU_01 /*3311*/:
                return new Stamps(c, id);
            case STAMP_KIRAKIRA_01 /*3100*/:
            case STAMP_KIRAKIRA_02 /*3101*/:
            case STAMP_KIRAKIRA_03 /*3102*/:
            case STAMP_STAR_01 /*3103*/:
            case STAMP_KIRAKIRA_04 /*3104*/:
            case STAMP_KIRAKIRA_05 /*3105*/:
            case STAMP_HEART_KORO_01 /*3106*/:
            case STAMP_HEART_KORO_02 /*3107*/:
                return new KoroKoroStamps(c, id);
            default:
                throw new RuntimeException("Invalid style ID");
        }
    }

    private static Bitmap resizeImage(int id, Context c) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDither = true;
        Resources res = c.getResources();
        BitmapFactory.decodeResource(res, id, options);
        float scale = (float) (((double) res.getDisplayMetrics().density) / 1.5d);
        int resizeWidth = (int) (((float) options.outWidth) * scale);
        int resizeHeight = (int) (((float) options.outHeight) * scale);
        options.inJustDecodeBounds = false;
        options.inSampleSize = (int) scale;
        return Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, id, options), resizeWidth, resizeHeight, true);
    }
}
