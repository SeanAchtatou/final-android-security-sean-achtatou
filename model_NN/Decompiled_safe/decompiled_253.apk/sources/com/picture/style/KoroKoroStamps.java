package com.picture.style;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import jp.co.hikesiya.Style;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class KoroKoroStamps implements Style {
    private float drawX;
    private float drawY;
    private float height;
    private Paint paint;
    private float prevX;
    private float prevY;
    Resources res;
    private Bitmap stamp;
    private float width;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Object.<init>():void in method: com.picture.style.KoroKoroStamps.<init>(android.content.Context, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Object.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public KoroKoroStamps(android.content.Context r1, int r2) {
        /*
            r2 = this;
            r2.<init>()
            r0 = 0
            r2.res = r0
            android.graphics.Paint r0 = new android.graphics.Paint
            r0.<init>()
            r2.paint = r0
            android.content.res.Resources r0 = r3.getResources()
            r2.res = r0
            r0 = 3100(0xc1c, float:4.344E-42)
            if (r4 != r0) goto L_0x0036
            r0 = 2130837604(0x7f020064, float:1.7280167E38)
            r2.createStamp(r0)
        L_0x001d:
            android.graphics.Bitmap r0 = r2.stamp
            int r0 = r0.getWidth()
            float r0 = (float) r0
            r2.width = r0
            android.graphics.Bitmap r0 = r2.stamp
            int r0 = r0.getHeight()
            float r0 = (float) r0
            r2.height = r0
            android.graphics.Paint r0 = r2.paint
            r1 = 1
            r0.setAntiAlias(r1)
            return
        L_0x0036:
            r0 = 3101(0xc1d, float:4.345E-42)
            if (r4 != r0) goto L_0x0041
            r0 = 2130837605(0x7f020065, float:1.7280169E38)
            r2.createStamp(r0)
            goto L_0x001d
        L_0x0041:
            r0 = 3102(0xc1e, float:4.347E-42)
            if (r4 != r0) goto L_0x004c
            r0 = 2130837606(0x7f020066, float:1.728017E38)
            r2.createStamp(r0)
            goto L_0x001d
        L_0x004c:
            r0 = 3103(0xc1f, float:4.348E-42)
            if (r4 != r0) goto L_0x0057
            r0 = 2130837614(0x7f02006e, float:1.7280187E38)
            r2.createStamp(r0)
            goto L_0x001d
        L_0x0057:
            r0 = 3104(0xc20, float:4.35E-42)
            if (r4 != r0) goto L_0x0062
            r0 = 2130837607(0x7f020067, float:1.7280173E38)
            r2.createStamp(r0)
            goto L_0x001d
        L_0x0062:
            r0 = 3105(0xc21, float:4.351E-42)
            if (r4 != r0) goto L_0x006d
            r0 = 2130837608(0x7f020068, float:1.7280175E38)
            r2.createStamp(r0)
            goto L_0x001d
        L_0x006d:
            r0 = 3106(0xc22, float:4.352E-42)
            if (r4 != r0) goto L_0x0078
            r0 = 2130837596(0x7f02005c, float:1.728015E38)
            r2.createStamp(r0)
            goto L_0x001d
        L_0x0078:
            r0 = 3107(0xc23, float:4.354E-42)
            if (r4 != r0) goto L_0x0083
            r0 = 2130837597(0x7f02005d, float:1.7280153E38)
            r2.createStamp(r0)
            goto L_0x001d
        L_0x0083:
            android.graphics.Paint r0 = r2.paint
            r1 = 1077936128(0x40400000, float:3.0)
            r0.setStrokeWidth(r1)
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.picture.style.KoroKoroStamps.<init>(android.content.Context, int):void");
    }

    private void createStamp(int id) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDither = true;
        BitmapFactory.decodeResource(this.res, id, options);
        float scale = (float) (((double) this.res.getDisplayMetrics().density) / 1.5d);
        int resizeWidth = (int) (((float) options.outWidth) * scale);
        int resizeHeight = (int) (((float) options.outHeight) * scale);
        options.inJustDecodeBounds = false;
        options.inSampleSize = (int) scale;
        this.stamp = BitmapFactory.decodeResource(this.res, id, options);
        this.stamp = Bitmap.createScaledBitmap(this.stamp, resizeWidth, resizeHeight, true);
    }

    public void strokeStart(Canvas c, float x, float y) {
        this.prevX = x - (this.width / 2.0f);
        this.prevY = y - (this.height / 2.0f);
        c.drawBitmap(this.stamp, this.prevX, this.prevY, this.paint);
    }

    public void stroke(Canvas c, float x, float y) {
        this.drawX = x - (this.width / 2.0f);
        this.drawY = y - (this.height / 2.0f);
        if (Math.abs(this.drawX - this.prevX) >= this.width / 2.0f || Math.abs(this.drawY - this.prevY) >= this.height / 2.0f) {
            c.drawBitmap(this.stamp, this.drawX, this.drawY, this.paint);
            this.prevX = this.drawX;
            this.prevY = this.drawY;
        }
    }

    public void setColor(int color) {
        this.paint.setColor(color);
    }

    public int getColor() {
        return this.paint.getColor();
    }

    public void draw(Canvas c) {
    }
}
