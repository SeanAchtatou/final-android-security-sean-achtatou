package com.picture.style;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import jp.co.hikesiya.R;
import jp.co.hikesiya.Style;

public class Stamps implements Style {
    private float drawX;
    private float drawY;
    private Paint paint = new Paint();
    Resources res = null;
    private Bitmap stamp;

    public Stamps(Context c, int id) {
        this.res = c.getResources();
        if (id == 3000) {
            createStamp(R.drawable.stamp_himawari_01);
        } else if (id == 3001) {
            createStamp(R.drawable.stamp_hana_01);
        } else if (id == 3002) {
            createStamp(R.drawable.stamp_heart_01);
        } else if (id == 3003) {
            createStamp(R.drawable.stamp_kinoko_01);
        } else if (id == 3004) {
            createStamp(R.drawable.stamp_hoho_01);
        } else if (id == 3005) {
            createStamp(R.drawable.stamp_onpu_01);
        } else if (id == 3006) {
            createStamp(R.drawable.stamp_baby_01);
        } else if (id == 3007) {
            createStamp(R.drawable.stamp_kintaro_01);
        } else if (id == 3008) {
            createStamp(R.drawable.stamp_hanamaru);
        } else if (id == 3009) {
            createStamp(R.drawable.stamp_motto);
        } else if (id == 3010) {
            createStamp(R.drawable.stamp_megane_01);
        } else if (id == 3011) {
            createStamp(R.drawable.stamp_hitomi_01);
        } else if (id == 3200) {
            createStamp(R.drawable.stamp_fukidasi_01_1);
        } else if (id == 3201) {
            createStamp(R.drawable.stamp_fukidasi_01_2);
        } else if (id == 3202) {
            createStamp(R.drawable.stamp_fukidasi_01_3);
        } else if (id == 3203) {
            createStamp(R.drawable.stamp_fukidasi_01_4);
        } else if (id == 3204) {
            createStamp(R.drawable.stamp_fukidasi_02_1);
        } else if (id == 3205) {
            createStamp(R.drawable.stamp_fukidasi_02_2);
        } else if (id == 3206) {
            createStamp(R.drawable.stamp_fukidasi_02_3);
        } else if (id == 3207) {
            createStamp(R.drawable.stamp_fukidasi_02_4);
        } else if (id == 3300) {
            createStamp(R.drawable.stamp_chu_01);
        } else if (id == 3301) {
            createStamp(R.drawable.stamp_chu_02);
        } else if (id == 3302) {
            createStamp(R.drawable.stamp_happybirthday_01);
        } else if (id == 3303) {
            createStamp(R.drawable.stamp_thankyou_01);
        } else if (id == 3304) {
            createStamp(R.drawable.stamp_thankyou_02);
        } else if (id == 3305) {
            createStamp(R.drawable.stamp_yeah_01);
        } else if (id == 3306) {
            createStamp(R.drawable.stamp_yeah_02);
        } else if (id == 3307) {
            createStamp(R.drawable.stamp_arigatou_01);
        } else if (id == 3308) {
            createStamp(R.drawable.stamp_arigatou_02);
        } else if (id == 3309) {
            createStamp(R.drawable.stamp_konnichiwa_01);
        } else if (id == 3310) {
            createStamp(R.drawable.stamp_sayonara_01);
        } else if (id == 3311) {
            createStamp(R.drawable.stamp_itadakimasu_01);
        } else {
            this.paint.setStrokeWidth(3.0f);
        }
        this.paint.setAntiAlias(true);
    }

    public Stamps(Context c, int id, Bitmap bm) {
        if (id == 3010) {
            this.stamp = bm;
        } else {
            this.paint.setStrokeWidth(3.0f);
        }
        this.paint.setAntiAlias(true);
    }

    private void createStamp(int id) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDither = true;
        BitmapFactory.decodeResource(this.res, id, options);
        float scale = (float) (((double) this.res.getDisplayMetrics().density) / 1.5d);
        int resizeWidth = (int) (((float) options.outWidth) * scale);
        int resizeHeight = (int) (((float) options.outHeight) * scale);
        options.inJustDecodeBounds = false;
        options.inSampleSize = (int) scale;
        this.stamp = BitmapFactory.decodeResource(this.res, id, options);
        this.stamp = Bitmap.createScaledBitmap(this.stamp, resizeWidth, resizeHeight, true);
    }

    public void strokeStart(Canvas c, float x, float y) {
        this.drawX = x - ((float) (this.stamp.getWidth() / 2));
        this.drawY = y - ((float) (this.stamp.getHeight() / 2));
        c.drawBitmap(this.stamp, this.drawX, this.drawY, this.paint);
    }

    public void stroke(Canvas c, float x, float y) {
        this.drawX = x - ((float) (this.stamp.getWidth() / 2));
        this.drawY = y - ((float) (this.stamp.getHeight() / 2));
        c.drawBitmap(this.stamp, this.drawX, this.drawY, this.paint);
    }

    public void setColor(int color) {
        this.paint.setColor(color);
    }

    public int getColor() {
        return this.paint.getColor();
    }

    public void draw(Canvas c) {
    }
}
