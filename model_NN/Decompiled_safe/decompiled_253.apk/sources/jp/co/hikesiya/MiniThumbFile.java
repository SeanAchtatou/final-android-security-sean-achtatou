package jp.co.hikesiya;

import android.net.Uri;
import android.os.Environment;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Hashtable;
import jp.co.hikesiya.util.Log;

public class MiniThumbFile {
    public static final int BYTES_PER_MINTHUMB = 10000;
    private static final int MINI_THUMB_DATA_FILE_VERSION = 3;
    private static final String TAG = "MiniThumbFile";
    private static Hashtable<String, MiniThumbFile> sThumbFiles = new Hashtable<>();
    private ByteBuffer mBuffer = ByteBuffer.allocateDirect(BYTES_PER_MINTHUMB);
    private FileChannel mChannel;
    private RandomAccessFile mMiniThumbFile;
    private Uri mUri;

    public MiniThumbFile(Uri uri) {
        this.mUri = uri;
    }

    public static synchronized MiniThumbFile instance(Uri uri) {
        MiniThumbFile file;
        synchronized (MiniThumbFile.class) {
            Log.d(TAG, "instance() start.");
            Log.d(TAG, "Uri: " + uri);
            String type = uri.getPathSegments().get(1);
            file = sThumbFiles.get(type);
            Log.d(TAG, "type: " + type);
            Log.d(TAG, "file: " + file);
            if (file == null) {
                Log.d(TAG, "file is null.");
                file = new MiniThumbFile(Uri.parse("content://media/external/" + type + "/media"));
                sThumbFiles.put(type, file);
            }
            Log.d(TAG, "instance() end.");
        }
        return file;
    }

    private String randomAccessFilePath(int version) {
        return String.valueOf(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/DCIM/.thumbnails") + "/.thumbdata" + version + "-" + this.mUri.hashCode();
    }

    private void removeOldFile() {
        Log.d(TAG, "removeOldFile() start.");
        File oldFile = new File(randomAccessFilePath(2));
        if (oldFile.exists()) {
            Log.d(TAG, "oldFile.exists()");
            try {
                oldFile.delete();
            } catch (SecurityException e) {
            }
        }
        Log.d(TAG, "removeOldFile() end.");
    }

    private RandomAccessFile miniThumbDataFile() {
        Log.d(TAG, "miniThumbDataFile() start.");
        if (this.mMiniThumbFile == null) {
            Log.d(TAG, "miniThumbFile is null.");
            removeOldFile();
            String path = randomAccessFilePath(MINI_THUMB_DATA_FILE_VERSION);
            File directory = new File(path).getParentFile();
            Log.d(TAG, "path: " + path);
            Log.d(TAG, "directory: " + directory.toString());
            if (!directory.isDirectory()) {
                Log.d(TAG, "!directory.isDirectory()");
                if (!directory.mkdirs()) {
                    Log.d(TAG, "Unable to create .thumbnails directory " + directory.toString());
                }
            }
            File f = new File(path);
            try {
                this.mMiniThumbFile = new RandomAccessFile(f, "rw");
            } catch (IOException e) {
                try {
                    this.mMiniThumbFile = new RandomAccessFile(f, "r");
                } catch (IOException e2) {
                    Log.d(TAG, "IOException" + e2.toString());
                }
            }
            if (this.mMiniThumbFile != null) {
                this.mChannel = this.mMiniThumbFile.getChannel();
                Log.d(TAG, "mMiniThumbFile != null");
                if (this.mChannel == null) {
                    Log.d(TAG, "mChannel is null.");
                } else {
                    Log.d(TAG, "mChannel is　not null.");
                }
            } else {
                Log.d(TAG, "mMiniThumbFile == null");
            }
        }
        Log.d(TAG, "miniThumbDataFile() end.");
        return this.mMiniThumbFile;
    }

    public synchronized byte[] getMiniThumbFromFile(long id, byte[] data) {
        byte[] bArr;
        Log.d(TAG, "getMiniThumbFromFile() start.");
        if (miniThumbDataFile() == null) {
            bArr = null;
        } else {
            long pos = id * 10000;
            FileLock lock = null;
            try {
                this.mBuffer.clear();
                FileLock lock2 = this.mChannel.lock(pos, 10000, true);
                int size = this.mChannel.read(this.mBuffer, pos);
                if (size > 13) {
                    this.mBuffer.position(0);
                    int length = this.mBuffer.getInt();
                    if (size >= length + 13 && data.length >= length) {
                        this.mBuffer.get(data, 0, length);
                        Log.d(TAG, "return data;");
                        Log.d(TAG, "getMiniThumbFromFile() end.");
                        if (lock2 != null) {
                            try {
                                lock2.release();
                            } catch (IOException e) {
                            }
                        }
                        bArr = data;
                    }
                }
                if (lock2 != null) {
                    try {
                        lock2.release();
                    } catch (IOException e2) {
                    }
                }
            } catch (IOException e3) {
                Log.d(TAG, "got exception when reading thumbnail id=" + id + ", exception: " + e3);
                if (lock != null) {
                    try {
                        lock.release();
                    } catch (IOException e4) {
                    }
                }
            } catch (RuntimeException e5) {
                Log.d(TAG, "Got exception when reading thumbnail, id = " + id + ". " + e5.getClass());
                if (lock != null) {
                    try {
                        lock.release();
                    } catch (IOException e6) {
                    }
                }
            } catch (Throwable th) {
                if (lock != null) {
                    try {
                        lock.release();
                    } catch (IOException e7) {
                    }
                }
                throw th;
            }
            bArr = null;
        }
        return bArr;
    }
}
