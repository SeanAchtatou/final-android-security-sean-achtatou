package jp.co.hikesiya;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.picture.style.StyleFactory;
import java.util.HashMap;
import jp.co.hikesiya.util.Log;

public class List_of_Lines implements View.OnClickListener, DialogInterface.OnClickListener {
    private static final int WC = -2;
    private static int defaultColor = Color.argb(255, 255, 150, 0);
    LinearLayout color1;
    LinearLayout color2;
    LinearLayout color3;
    LinearLayout color4;
    TextView colorMsgView;
    private HashMap<Integer, ImageButton> colorStyleMap = new HashMap<>();
    private Context context;
    private ImageButton currentButton_color;
    private ImageButton currentButton_pen;
    private int currentColor;
    private int currentStyle;
    private View dialogLayout;
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) WC, (int) WC);
    LinearLayout parentLayout;
    LinearLayout pen1;
    LinearLayout pen2;
    TextView penMsgView;
    private HashMap<Integer, ImageButton> penStyleMap = new HashMap<>();
    private AlertDialog pendialog;
    private int setStyle;
    private Surface surface;

    public List_of_Lines(Context context2, Surface surface2) {
        this.context = context2;
        this.surface = surface2;
        this.penStyleMap = StyleFactory.getPenStyleMap(this.context);
        this.colorStyleMap = StyleFactory.getColorStyleMap(this.context);
        setEventListener();
        makeDialogLinearLayout();
        this.dialogLayout = LayoutInflater.from(context2).inflate((int) R.layout.dialog_pen, (ViewGroup) null);
        this.parentLayout = (LinearLayout) this.dialogLayout.findViewById(R.id.pen_dialog_linear);
        setParentLayout();
        makeAlert();
    }

    public void pen_onClick() {
        this.currentStyle = StyleFactory.getCurrentId();
        this.currentColor = this.surface.getPaintColor();
        this.setStyle = this.currentStyle;
        setImageButton();
        this.surface.setStyle(StyleFactory.getStyle(this.context, this.currentStyle));
        this.surface.setPaintColor(this.currentColor);
        setPenBGColor(this.currentButton_pen);
        setColorBGColor(this.currentButton_color);
        this.pendialog.show();
    }

    private void setImageButton() {
        if (this.currentStyle == 2001) {
            this.currentButton_pen = this.penStyleMap.get(Integer.valueOf((int) StyleFactory.SIMPLE_01));
        } else if (this.currentStyle == 2002) {
            this.currentButton_pen = this.penStyleMap.get(Integer.valueOf((int) StyleFactory.SIMPLE_02));
        } else if (this.currentStyle == 2003) {
            this.currentButton_pen = this.penStyleMap.get(Integer.valueOf((int) StyleFactory.SIMPLE_03));
        } else {
            Log.d("List_of_Lines", "setImageButton, SIMPLE_02");
            this.currentButton_pen = this.penStyleMap.get(Integer.valueOf((int) StyleFactory.SIMPLE_02));
            this.currentColor = getDefaultColor();
        }
        if (this.currentColor == Color.argb(255, 255, 255, 255)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_WHITE));
        } else if (this.currentColor == Color.argb(255, 255, 180, 255)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_PINK));
        } else if (this.currentColor == Color.argb(255, 255, 100, 155)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_CORAL));
        } else if (this.currentColor == Color.argb(255, 255, 0, 0)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_RED));
        } else if (this.currentColor == Color.argb(255, 255, 150, 0)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_ORANGE));
        } else if (this.currentColor == Color.argb(255, 255, 255, 0)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_YELLOW));
        } else if (this.currentColor == Color.argb(255, 0, 255, 0)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_GREEN));
        } else if (this.currentColor == Color.argb(255, 40, 155, 40)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_DEEPGREEN));
        } else if (this.currentColor == Color.argb(255, 155, 255, 255)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_LIGHTBLUE));
        } else if (this.currentColor == Color.argb(255, 0, 100, 255)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_BLUE));
        } else if (this.currentColor == Color.argb(255, 0, 0, 240)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_NAVY));
        } else if (this.currentColor == Color.argb(255, 155, 40, 0)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_BROWN));
        } else if (this.currentColor == Color.argb(255, 200, 0, 255)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_PURPLE));
        } else if (this.currentColor == Color.argb(255, 200, 155, 255)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_LIGHTPURPLE));
        } else if (this.currentColor == Color.argb(255, 155, 155, 155)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_GRAY));
        } else if (this.currentColor == Color.argb(255, 0, 0, 0)) {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_BLACK));
        } else {
            this.currentButton_color = this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_ORANGE));
        }
    }

    private void makeAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
        builder.setTitle((int) R.string.dialogPenMenuIndexSelect);
        builder.setPositiveButton((int) R.string.ok, this);
        builder.setNegativeButton((int) R.string.cancel, this);
        builder.setView(this.dialogLayout);
        this.pendialog = builder.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case WC /*-2*/:
                Log.d("List_of_Lines", "Cancel");
                this.surface.setStyle(StyleFactory.getStyle(this.context, this.currentStyle));
                this.surface.setPaintColor(this.currentColor);
                return;
            case ThumbnailUtil.UNCONSTRAINED /*-1*/:
                Log.d("List_of_Lines", "OK");
                if (Integer.toString(this.setStyle).startsWith("2")) {
                    this.surface.setStyle(StyleFactory.getStyle(this.context, this.setStyle));
                    return;
                }
                this.setStyle = StyleFactory.SIMPLE_02;
                this.surface.setStyle(StyleFactory.getStyle(this.context, this.setStyle));
                return;
            default:
                return;
        }
    }

    private void setPenBGColor(ImageButton btn) {
        for (ImageButton ib : this.penStyleMap.values()) {
            ib.setBackgroundColor(Color.argb(0, 0, 0, 0));
        }
        btn.setBackgroundColor(-3355444);
    }

    private void setColorBGColor(ImageButton color) {
        for (ImageButton ib : this.colorStyleMap.values()) {
            ib.setBackgroundColor(Color.argb(0, 0, 0, 0));
        }
        color.setBackgroundColor(-3355444);
    }

    private void setStyle(int id) {
        this.surface.setStyle(StyleFactory.getStyle(this.context, id));
    }

    private void setColor(ImageButton btn) {
        int color = 0;
        if (((Integer) btn.getTag()).intValue() == 5000) {
            color = Color.argb(255, 255, 255, 255);
        } else if (((Integer) btn.getTag()).intValue() == 5001) {
            color = Color.argb(255, 255, 180, 255);
        } else if (((Integer) btn.getTag()).intValue() == 5002) {
            color = Color.argb(255, 255, 100, 155);
        } else if (((Integer) btn.getTag()).intValue() == 5003) {
            color = Color.argb(255, 255, 0, 0);
        } else if (((Integer) btn.getTag()).intValue() == 5004) {
            color = Color.argb(255, 255, 150, 0);
        } else if (((Integer) btn.getTag()).intValue() == 5005) {
            color = Color.argb(255, 255, 255, 0);
        } else if (((Integer) btn.getTag()).intValue() == 5006) {
            color = Color.argb(255, 0, 255, 0);
        } else if (((Integer) btn.getTag()).intValue() == 5007) {
            color = Color.argb(255, 40, 155, 40);
        } else if (((Integer) btn.getTag()).intValue() == 5008) {
            color = Color.argb(255, 155, 255, 255);
        } else if (((Integer) btn.getTag()).intValue() == 5009) {
            color = Color.argb(255, 0, 100, 255);
        } else if (((Integer) btn.getTag()).intValue() == 5010) {
            color = Color.argb(255, 0, 0, 240);
        } else if (((Integer) btn.getTag()).intValue() == 5011) {
            color = Color.argb(255, 155, 40, 0);
        } else if (((Integer) btn.getTag()).intValue() == 5012) {
            color = Color.argb(255, 200, 0, 255);
        } else if (((Integer) btn.getTag()).intValue() == 5013) {
            color = Color.argb(255, 200, 155, 255);
        } else if (((Integer) btn.getTag()).intValue() == 5014) {
            color = Color.argb(255, 155, 155, 155);
        } else if (((Integer) btn.getTag()).intValue() == 5015) {
            color = Color.argb(255, 0, 0, 0);
        }
        this.surface.setPaintColor(color);
    }

    private int getImageButtonSize() {
        int maxSize = 0;
        for (ImageButton ib : this.penStyleMap.values()) {
            int width = ib.getDrawable().getIntrinsicWidth();
            int height = ib.getDrawable().getIntrinsicHeight();
            if (maxSize < width) {
                maxSize = width;
            }
            if (maxSize < height) {
                maxSize = height;
            }
        }
        return maxSize;
    }

    private void setParentLayout() {
        this.parentLayout.addView(this.penMsgView);
        this.parentLayout.addView(this.pen1);
        this.parentLayout.addView(this.colorMsgView);
        this.parentLayout.addView(this.color1);
        this.parentLayout.addView(this.color2);
        this.parentLayout.addView(this.color3);
        this.parentLayout.addView(this.color4);
    }

    private void makeDialogLinearLayout() {
        Log.d("List_of_Lines", "makeDialogLinearLayout is called.");
        this.pen1 = makeDialogLine(this.pen1, new ImageButton[]{this.penStyleMap.get(Integer.valueOf((int) StyleFactory.SIMPLE_01)), this.penStyleMap.get(Integer.valueOf((int) StyleFactory.SIMPLE_02)), this.penStyleMap.get(Integer.valueOf((int) StyleFactory.SIMPLE_03))});
        this.color1 = makeDialogLine(this.color1, new ImageButton[]{this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_WHITE)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_PINK)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_CORAL)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_RED))});
        this.color2 = makeDialogLine(this.color2, new ImageButton[]{this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_ORANGE)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_YELLOW)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_GREEN)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_DEEPGREEN))});
        this.color3 = makeDialogLine(this.color3, new ImageButton[]{this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_LIGHTBLUE)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_BLUE)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_NAVY)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_BROWN))});
        this.color4 = makeDialogLine(this.color4, new ImageButton[]{this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_PURPLE)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_LIGHTPURPLE)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_GRAY)), this.colorStyleMap.get(Integer.valueOf((int) StyleFactory.PEN_COLOR_BLACK))});
        makeDialogTextView();
        Log.d("List_of_Lines", "makeDialogLinearLayout is end.");
    }

    private LinearLayout makeDialogLine(LinearLayout ll, ImageButton[] ibArry) {
        Log.d("List_of_Lines", "makeDialogLine is called.");
        int buttonSize = getImageButtonSize();
        LinearLayout ll2 = new LinearLayout(this.context);
        ll2.setOrientation(0);
        for (ImageButton ib : ibArry) {
            ll2.addView(ib, buttonSize, buttonSize);
        }
        Log.d("List_of_Lines", "makeDialogLine is end.");
        return ll2;
    }

    private void makeDialogTextView() {
        this.penMsgView = new TextView(this.context);
        this.penMsgView.setText((int) R.string.dialogPenMenuIndexPen);
        this.penMsgView.setTextSize(20.0f);
        this.penMsgView.setWidth(300);
        this.penMsgView.setTextColor(Color.argb(255, 255, 255, 255));
        this.colorMsgView = new TextView(this.context);
        this.colorMsgView.setText((int) R.string.dialogPenMenuIndexColor);
        this.colorMsgView.setTextSize(20.0f);
        this.colorMsgView.setPadding(0, 10, 0, 0);
        this.colorMsgView.setTextColor(Color.argb(255, 255, 255, 255));
    }

    private void setEventListener() {
        for (ImageButton ib : this.penStyleMap.values()) {
            ib.setOnClickListener(this);
        }
        for (ImageButton ib2 : this.colorStyleMap.values()) {
            ib2.setOnClickListener(this);
        }
    }

    public void onClick(View v) {
        Log.d("List_of_Lines", "(Integer)v.getTag()" + ((Integer) v.getTag()));
        int btnId = ((Integer) v.getTag()).intValue();
        if (btnId >= 2000 && btnId <= 2999) {
            setPenBGColor((ImageButton) v);
            setStyle(btnId);
            this.setStyle = btnId;
        } else if (btnId >= 5000 && btnId <= 5999) {
            setColorBGColor((ImageButton) v);
            setColor((ImageButton) v);
        }
    }

    private int getDefaultColor() {
        return defaultColor;
    }
}
