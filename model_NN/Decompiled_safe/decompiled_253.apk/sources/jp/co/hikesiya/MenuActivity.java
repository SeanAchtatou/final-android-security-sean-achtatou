package jp.co.hikesiya;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import jp.co.hikesiya.android.common.Agreement;
import jp.co.hikesiya.android.common.OptionMenu;
import jp.co.hikesiya.common.Utility;
import jp.co.hikesiya.util.Log;
import jp.co.nobot.libYieldMaker.libYieldMaker;

public class MenuActivity extends Activity {
    private static final int CANNOT_GET_PATH_FROM_URI = 4;
    private static final int CANNOT_GET_URI = 0;
    private static final int CANNOT_INSERT_NEW_URI = 5;
    private static final int CANNOT_USE_THIS_FILE = 3;
    private static final int CAN_NOT_GET_CAPTURED_IMAGE = 2;
    private static final int CHECK_OK = -1;
    private static final int IS_ACTION_EDIT = -2;
    private static final int IS_NOT_ACTION_EDIT = -3;
    private static final int IS_NOT_IMAGE_DATA = 1;
    private static final String MIMETYPE = "image/jpeg";
    private static final int REQUEST_CODE_CAMERA = 0;
    private static final int REQUEST_CODE_FILE_LIST = 1;
    private String _imageType = null;
    private Context c;
    private boolean canSaveDefaultPhotoInRakugakiFolder = true;
    private int imageNum = 0;
    private String path = null;
    private ProgressDialog progressDialog = null;
    private ProgressDialog progressDialog2 = null;
    private ProgressDialog progressDialog3 = null;

    public void onCreate(Bundle icicle) {
        Log.setDebugFlag(getApplicationContext());
        Log.d("Menu", "onCreate start.");
        Agreement.showAgreement(this);
        super.onCreate(icicle);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        setContentView((int) R.layout.activity_menu);
        this.c = getApplicationContext();
        libYieldMaker mv = (libYieldMaker) findViewById(R.id.menu_imgArea_adArea_advertising);
        mv.setActivity(this);
        mv.setUrl("http://images.ad-maker.info/apps/sjocu5a9vtpk.html");
        mv.startView();
        ((Button) findViewById(R.id.menu_btnArea_albumBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MenuActivity.this.btGallery_onClick();
            }
        });
        ((Button) findViewById(R.id.menu_btnArea_cameraBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MenuActivity.this.CamButton_onClick();
            }
        });
        ((Button) findViewById(R.id.menu_btnArea_fileManagerBtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MenuActivity.this.btFileList_onClick();
            }
        });
        if (!Environment.getExternalStorageState().equals("mounted")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.title);
            builder.setMessage((int) R.string.alertSdcardNotFound);
            builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    MenuActivity.this.finish();
                }
            });
            builder.create();
            builder.show();
        }
        Intent intent = getIntent();
        int resultCode1 = judgeIntent(intent);
        if (resultCode1 == IS_ACTION_EDIT) {
            int resultCode2 = getPathFromIntent(intent);
            if (resultCode2 != -1) {
                showAlert(resultCode2);
            } else if (canUseThisFile(this.path)) {
                startEditPicture(this.path, getString(R.string.imageTypeSelected));
                finish();
            }
        } else if (resultCode1 != IS_NOT_ACTION_EDIT) {
            showAlert(resultCode1);
        }
    }

    private int judgeIntent(Intent intent) {
        Log.d("Menu", "checkGivenIntent() start.");
        if (Utility.isNullOrEmpty(intent)) {
            Log.d("Menu", "intent is null.");
        } else if (!"android.intent.action.EDIT".equals(intent.getAction())) {
            Log.d("Menu", "intent is not ACTION_EDIT.");
        } else if (!Utility.isNullOrEmpty(intent.getDataString())) {
            Log.d("Menu", "RAKUGAKI-CAMERA was called as ACTION_EDIT.");
            return IS_ACTION_EDIT;
        } else {
            Log.d("Menu", "intent.getDataString() is null.");
            return 0;
        }
        Log.d("Menu", "checkGivenIntent() end.");
        return IS_NOT_ACTION_EDIT;
    }

    private int getPathFromIntent(Intent intent) {
        Log.d("Menu", "getPathFromIntent() start.");
        if (Uri.decode(intent.getDataString()).startsWith("content")) {
            this.path = getPathFromUri(intent.getData(), this.c);
            if (Utility.isNullOrEmpty(this.path)) {
                return CANNOT_GET_PATH_FROM_URI;
            }
        } else if (!Uri.decode(intent.getDataString()).startsWith("file")) {
            return 0;
        } else {
            Log.d("Menu", "This URI is identified with file scheme.");
            this.path = Uri.decode(intent.getDataString()).substring(7);
        }
        Log.d("Menu", "getPathFromIntent() end.");
        return -1;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Log.d("Menu", "onStart start.");
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        Log.d("Menu", "onRestart start.");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d("Menu", "onResume start.");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.d("Menu", "onPause start.");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Log.d("Menu", "onStop start.");
        if (this.progressDialog != null) {
            Log.d("Menu", "progressDialog.dismiss();");
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
        if (this.progressDialog2 != null) {
            Log.d("Menu", "progressDialog2.dismiss();");
            this.progressDialog2.dismiss();
            this.progressDialog2 = null;
        }
        if (this.progressDialog3 != null) {
            Log.d("Menu", "progressDialog3.dismiss();");
            this.progressDialog3.dismiss();
            this.progressDialog3 = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Log.d("Menu", "onDestroy start.");
    }

    public void onConfigurationChanged(Configuration newConfig) {
        Log.d("Menu", "called onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() != 0 || event.getKeyCode() != CANNOT_GET_PATH_FROM_URI) {
            return super.dispatchKeyEvent(event);
        }
        finish();
        return false;
    }

    /* access modifiers changed from: private */
    public void CamButton_onClick() {
        Log.d("Menu", "CamButton_onClick starts.");
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setTitle(getString(R.string.progDialogCameraOpen_Title));
        this.progressDialog.setMessage(getString(R.string.progDialogCameraOpen_Message));
        this.progressDialog.setIndeterminate(false);
        this.progressDialog.setProgressStyle(0);
        this.progressDialog.show();
        this.path = makeNewFileName();
        Uri imageUri = Uri.fromFile(new File(this.path));
        Intent intent = new Intent();
        intent.setAction("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", imageUri);
        new Thread(new CameraRunnable(intent)).start();
    }

    private class CameraRunnable implements Runnable {
        private Intent _intent;

        public CameraRunnable(Intent intent) {
            this._intent = intent;
        }

        public void run() {
            Log.d("CameraRunnable", "run() start.");
            MenuActivity.this.startActivityForResult(this._intent, 0);
        }
    }

    /* access modifiers changed from: private */
    public void btFileList_onClick() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: private */
    public void btGallery_onClick() {
        this.progressDialog3 = new ProgressDialog(this);
        this.progressDialog3.setTitle(getString(R.string.progDialogAlbumOpen_Title));
        this.progressDialog3.setMessage(getString(R.string.progDialogAlbumOpen_Message));
        this.progressDialog3.setIndeterminate(false);
        this.progressDialog3.setProgressStyle(0);
        this.progressDialog3.show();
        new Thread(new AlbumRunnable(new Intent(this, RakugakiGalleryActivity.class))).start();
    }

    private class AlbumRunnable implements Runnable {
        private Intent _intent;

        public AlbumRunnable(Intent intent) {
            this._intent = intent;
        }

        public void run() {
            Log.d("AlbumRunnable", "run() start.");
            MenuActivity.this.startActivity(this._intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("Menu", "requestCode: " + requestCode + ", resultCode: " + resultCode + ", data: " + data);
        if (-1 == resultCode) {
            switch (requestCode) {
                case 0:
                    Log.d("Menu", "REQUEST_CODE_CAMERA");
                    int result1 = requestCamera(data);
                    if (result1 != -1) {
                        showAlert(result1);
                        return;
                    }
                    break;
                case 1:
                    Log.d("Menu", "REQUEST_CODE_FILE_LIST");
                    int result2 = requestFileManager(data);
                    Log.d("Menu", "result2: " + result2);
                    if (result2 != -1) {
                        showAlert(result2);
                        return;
                    }
                    break;
            }
            if (isImageData(this.path)) {
                registImageDataInDb(this.c, this.path, MIMETYPE);
                writeImageNumber();
                startEditPicture(this.path, this._imageType);
                return;
            }
            showAlert(1);
            return;
        }
        switch (requestCode) {
            case 0:
                if (this.progressDialog != null) {
                    this.progressDialog.dismiss();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private int requestCamera(Intent data) {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
        this._imageType = getString(R.string.imageTypeTaken);
        if (!isNotEmptyFile(this.path)) {
            this.canSaveDefaultPhotoInRakugakiFolder = false;
            this.path = getPathFromUri(data.getData(), this.c);
            if (Utility.isNullOrEmpty(this.path)) {
                return CANNOT_GET_PATH_FROM_URI;
            }
            if (!isNotEmptyFile(this.path)) {
                return 0;
            }
        }
        return -1;
    }

    private int requestFileManager(Intent data) {
        this._imageType = getString(R.string.imageTypeSelected);
        if (Utility.isNullOrEmpty(data) || Utility.isNullOrEmpty(data.getData())) {
            return 0;
        }
        if (Uri.decode(data.getDataString()).startsWith("content")) {
            Log.d("Menu", "This URI is identified with content scheme.");
            Uri imageUri = data.getData();
            Log.d(getLocalClassName(), "URI: " + imageUri);
            this.path = getPathFromUri(imageUri, this.c);
            if (Utility.isNullOrEmpty(this.path)) {
                return CANNOT_GET_PATH_FROM_URI;
            }
        } else if (!Uri.decode(data.getDataString()).startsWith("file")) {
            return 0;
        } else {
            Log.d("Menu", "This URI is identified with file scheme.");
            this.path = Uri.decode(data.getDataString()).substring(7);
        }
        if (!isNotEmptyFile(this.path)) {
            return 1;
        }
        return -1;
    }

    private boolean isNotEmptyFile(String path2) {
        if (new File(path2).length() > 0) {
            Log.d("Menu", "This file is not empty.");
            return true;
        }
        Log.d("Menu", "This file is empty.");
        return false;
    }

    public String getPathFromUri(Uri uri, Context context) {
        Log.d("Menu", "getPathFromUri() start.");
        String path2 = null;
        try {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{"_data"}, null, null, null);
            if (cursor.moveToFirst()) {
                path2 = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
            }
            if (!Utility.isNullOrEmpty(cursor)) {
                cursor.close();
            }
        } catch (NullPointerException e) {
        }
        return path2;
    }

    public boolean isImageData(String path2) {
        Log.d("Menu", "isImageData() start.");
        Log.d("Menu", "path: " + path2);
        if (Pattern.compile(".*\\.(bmp|jpg|jpeg|png|gif)", CAN_NOT_GET_CAPTURED_IMAGE).matcher(path2).matches()) {
            Log.d("Menu", "This is image data.");
            return true;
        }
        Log.d("Menu", "This is not image data.");
        return false;
    }

    private void startEditPicture(String path2, String _imageType2) {
        this.progressDialog2 = new ProgressDialog(this);
        this.progressDialog2.setTitle(getString(R.string.progDialogLoadPhoto_Title));
        this.progressDialog2.setMessage(getString(R.string.progDialogLoadPhoto_Message));
        this.progressDialog2.setIndeterminate(false);
        this.progressDialog2.setProgressStyle(0);
        this.progressDialog2.show();
        new Thread(new StartEditPictureRunnable(this, path2, _imageType2)).start();
    }

    private class StartEditPictureRunnable implements Runnable {
        private Context _c;
        private String _imageType;
        private String _path;

        public StartEditPictureRunnable(Context c, String path, String imageType) {
            this._c = c;
            this._path = path;
            this._imageType = imageType;
        }

        public void run() {
            Log.d("StartEditPictureRunnable", "run() start.");
            Intent intent = new Intent(this._c, EditPictureActivity.class);
            intent.putExtra(MenuActivity.this.getString(R.string.imageType), this._imageType);
            intent.putExtra(MenuActivity.this.getString(R.string.selectedFileUri), this._path);
            Log.d("Menu", "_imageType: " + this._imageType);
            Log.d("Menu", "_path: " + this._path);
            MenuActivity.this.startActivity(intent);
        }
    }

    private void showAlert(int error_type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.title);
        if (error_type == 0) {
            builder.setMessage((int) R.string.alertCannotGetUri);
        } else if (error_type == 1) {
            builder.setMessage((int) R.string.alertIsNotImageData);
        } else if (error_type == CAN_NOT_GET_CAPTURED_IMAGE) {
            builder.setMessage((int) R.string.alertAbend);
        } else if (error_type == CANNOT_USE_THIS_FILE) {
            builder.setMessage((int) R.string.alertCannotUseThisFile);
        } else if (error_type == CANNOT_GET_PATH_FROM_URI) {
            builder.setMessage((int) R.string.alertCannotGetPathFromUri);
        } else if (error_type == CANNOT_INSERT_NEW_URI) {
            builder.setMessage((int) R.string.alertCannotInsertNewUri);
        } else {
            return;
        }
        builder.setNeutralButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create();
        builder.show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        super.onMenuItemSelected(featureId, item);
        switch (item.getItemId()) {
            case R.id.help:
                OptionMenu.showHelp(this, getString(R.string.help_content));
                return true;
            case R.id.info:
                OptionMenu.showInfo(this);
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        Log.d("Menu", "onSaveInstanceState start");
        super.onSaveInstanceState(bundle);
        if (!Utility.isNull(this.path)) {
            bundle.putString("path", this.path);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        Log.d("Menu", "onRestoreInstanceState start");
        super.onRestoreInstanceState(bundle);
        if (!Utility.isNull(bundle.getString("path"))) {
            this.path = bundle.getString("path");
        }
    }

    private boolean canUseThisFile(String mPath) {
        Log.d("Menu", "canUseThisFile() start.");
        if (mPath == null) {
            Log.d("Menu", "mPath is null.");
            showAlert(CANNOT_USE_THIS_FILE);
            return false;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDither = true;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        BitmapFactory.decodeFile(mPath, options);
        int height = options.outHeight;
        int width = options.outWidth;
        Log.d("Menu", "height: " + height);
        Log.d("Menu", "width: " + width);
        if (height <= 0 || width <= 0) {
            Log.d("Menu", "Bitmap is null.");
            showAlert(CANNOT_USE_THIS_FILE);
            return false;
        }
        Log.d("Menu", "Bitmap is not null.");
        return true;
    }

    private String makeNewFileName() {
        Log.d("Menu", "makeNewFileName is called.");
        this.imageNum = this.c.getSharedPreferences("PREFERENCES_FILE", 0).getInt("number", 0);
        String today = new SimpleDateFormat("yyyyMMdd").format(new Date());
        while (true) {
            this.imageNum++;
            String strPhotoNo = Integer.toString(this.imageNum);
            if (this.imageNum >= 0 && this.imageNum <= 9) {
                strPhotoNo = "000" + strPhotoNo;
            } else if (this.imageNum >= 10 && this.imageNum <= 99) {
                strPhotoNo = "00" + strPhotoNo;
            } else if (this.imageNum >= 100 && this.imageNum <= 999) {
                strPhotoNo = "0" + strPhotoNo;
            } else if (this.imageNum >= 1000) {
            }
            String dirName = String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/Rakugaki/";
            String fullPath = String.valueOf(dirName) + (String.valueOf("rakugaki_" + today + "_" + strPhotoNo) + ".jpeg");
            File dir = new File(dirName);
            if (!dir.exists()) {
                dir.mkdir();
            }
            if (!new File(fullPath).exists()) {
                Log.d("Menu", "makeNewFileName end.\nfileName: " + fullPath);
                return fullPath;
            }
        }
    }

    private void registImageDataInDb(Context c2, String path2, String mimetype) {
        if (this.canSaveDefaultPhotoInRakugakiFolder) {
            new MediaScannerNotifier(c2, path2, MIMETYPE);
        }
    }

    public void writeImageNumber() {
        if (this.canSaveDefaultPhotoInRakugakiFolder) {
            SharedPreferences.Editor editor = this.c.getSharedPreferences("PREFERENCES_FILE", 0).edit();
            editor.putInt("number", this.imageNum);
            editor.commit();
        }
    }
}
