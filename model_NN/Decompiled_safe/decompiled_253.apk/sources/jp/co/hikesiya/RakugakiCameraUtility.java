package jp.co.hikesiya;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Environment;
import android.os.StatFs;

public final class RakugakiCameraUtility {
    public boolean overSDCapacity() {
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        if (((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks()) <= 0) {
            return true;
        }
        return false;
    }

    public void showExitAlert(final Activity act, int msg, int btnMsg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle((int) R.string.title);
        builder.setMessage(msg);
        builder.setNeutralButton(btnMsg, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                act.finish();
            }
        });
        builder.create();
        builder.show();
    }
}
