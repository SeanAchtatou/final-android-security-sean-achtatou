package jp.co.hikesiya.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.regex.Pattern;

public final class Utility {
    private static final String ALPHA_NUMERIC = "^[0-9a-zA-Z]+$";
    private static final String ALPHA_NUMERIC_MARK = "^[!-~]+$";
    private static final String EMPTY_STRING = "";
    private static final String NUMERIC = "^[0-9]+$";

    public static boolean isNull(Object target) {
        if (target == null) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(Object target) {
        if (target == null) {
            return false;
        }
        if (target instanceof Object[]) {
            if (((Object[]) target).length == 0) {
                return true;
            }
        } else if (target instanceof byte[]) {
            if (((byte[]) target).length == 0) {
                return true;
            }
        } else if (target instanceof short[]) {
            if (((short[]) target).length == 0) {
                return true;
            }
        } else if (target instanceof int[]) {
            if (((int[]) target).length == 0) {
                return true;
            }
        } else if (target instanceof long[]) {
            if (((long[]) target).length == 0) {
                return true;
            }
        } else if (target instanceof float[]) {
            if (((float[]) target).length == 0) {
                return true;
            }
        } else if (target instanceof double[]) {
            if (((double[]) target).length == 0) {
                return true;
            }
        } else if (target instanceof char[]) {
            if (((char[]) target).length == 0) {
                return true;
            }
        } else if (target instanceof boolean[]) {
            if (((boolean[]) target).length == 0) {
                return true;
            }
        } else if (target.toString().length() == 0) {
            return true;
        }
        return false;
    }

    public static boolean isNullOrEmpty(Object target) {
        if (isNull(target)) {
            return true;
        }
        if (isEmpty(target)) {
            return true;
        }
        return false;
    }

    private static boolean isMatch(String target, String format) {
        if (isNullOrEmpty(target) || isNullOrEmpty(format)) {
            return false;
        }
        return Pattern.matches(format, target);
    }

    public static boolean isAlphaNumeric(String target) {
        return isMatch(target, ALPHA_NUMERIC);
    }

    public static boolean isNumeric(String target) {
        return isMatch(target, NUMERIC);
    }

    public static boolean isAlphaNumericMark(String target) {
        return isMatch(target, ALPHA_NUMERIC_MARK);
    }

    public static boolean isAscii(String target) {
        if (isNullOrEmpty(target)) {
            return false;
        }
        return target.length() == target.getBytes().length;
    }

    public static boolean equalsString(String src, String dst) {
        if (isNull(src)) {
            src = EMPTY_STRING;
        }
        if (isNull(dst)) {
            dst = EMPTY_STRING;
        }
        return src.equals(dst);
    }

    public static String convertDateToString(Date targetDate, String format) throws IllegalArgumentException {
        if (isNull(targetDate)) {
            return EMPTY_STRING;
        }
        return new SimpleDateFormatEx(format).format(targetDate);
    }

    public static Date convertStringToDate(String dateStr, String format) throws ParseException {
        if (isNullOrEmpty(dateStr)) {
            return null;
        }
        return new SimpleDateFormatEx(format).parse(dateStr);
    }

    public static boolean containsString(String target, String[] searchStr) {
        if (isNullOrEmpty(searchStr)) {
            return false;
        }
        for (String ss : searchStr) {
            if (target.contains(ss)) {
                return true;
            }
        }
        return false;
    }

    public static Object deepCopy(Object target) throws IOException, ClassNotFoundException, NotSerializableException {
        ByteArrayInputStream byteIn;
        if (isNull(target)) {
            return null;
        }
        ByteArrayOutputStream byteOut = null;
        ObjectOutputStream objOut = null;
        ByteArrayInputStream byteIn2 = null;
        ObjectInputStream objIn = null;
        try {
            ByteArrayOutputStream byteOut2 = new ByteArrayOutputStream();
            try {
                ObjectOutputStream objOut2 = new ObjectOutputStream(byteOut2);
                try {
                    objOut2.writeObject(target);
                    byteIn = new ByteArrayInputStream(byteOut2.toByteArray());
                } catch (Throwable th) {
                    th = th;
                    objOut = objOut2;
                    byteOut = byteOut2;
                    closeStreamQuietly(byteOut);
                    closeStreamQuietly(objOut);
                    closeStreamQuietly(byteIn2);
                    closeStreamQuietly(objIn);
                    throw th;
                }
                try {
                    ObjectInputStream objIn2 = new ObjectInputStream(byteIn);
                    try {
                        Object readObject = objIn2.readObject();
                        closeStreamQuietly(byteOut2);
                        closeStreamQuietly(objOut2);
                        closeStreamQuietly(byteIn);
                        closeStreamQuietly(objIn2);
                        return readObject;
                    } catch (Throwable th2) {
                        th = th2;
                        objIn = objIn2;
                        byteIn2 = byteIn;
                        objOut = objOut2;
                        byteOut = byteOut2;
                        closeStreamQuietly(byteOut);
                        closeStreamQuietly(objOut);
                        closeStreamQuietly(byteIn2);
                        closeStreamQuietly(objIn);
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    byteIn2 = byteIn;
                    objOut = objOut2;
                    byteOut = byteOut2;
                    closeStreamQuietly(byteOut);
                    closeStreamQuietly(objOut);
                    closeStreamQuietly(byteIn2);
                    closeStreamQuietly(objIn);
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                byteOut = byteOut2;
                closeStreamQuietly(byteOut);
                closeStreamQuietly(objOut);
                closeStreamQuietly(byteIn2);
                closeStreamQuietly(objIn);
                throw th;
            }
        } catch (Throwable th5) {
            th = th5;
            closeStreamQuietly(byteOut);
            closeStreamQuietly(objOut);
            closeStreamQuietly(byteIn2);
            closeStreamQuietly(objIn);
            throw th;
        }
    }

    public static void closeStreamQuietly(Object stream) {
        if (!isNull(stream)) {
            try {
                if (stream instanceof InputStream) {
                    ((InputStream) stream).close();
                } else if (stream instanceof OutputStream) {
                    ((OutputStream) stream).close();
                }
            } catch (IOException e) {
            }
        }
    }
}
