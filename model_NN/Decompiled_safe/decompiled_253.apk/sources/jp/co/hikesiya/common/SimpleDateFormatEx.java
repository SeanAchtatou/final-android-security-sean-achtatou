package jp.co.hikesiya.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleDateFormatEx extends SimpleDateFormat {
    private static Object mutex = new Object();

    public SimpleDateFormatEx() {
    }

    public SimpleDateFormatEx(String pattern) {
        super(pattern);
    }

    public final Date parseEx(String source) throws ParseException {
        Date date;
        synchronized (mutex) {
            date = parse(source);
        }
        return date;
    }

    public final String formatEx(Date date) {
        String string;
        synchronized (mutex) {
            string = format(date);
        }
        return string;
    }

    public final void setLenientEx(boolean lenient) {
        synchronized (mutex) {
            setLenient(lenient);
        }
    }
}
