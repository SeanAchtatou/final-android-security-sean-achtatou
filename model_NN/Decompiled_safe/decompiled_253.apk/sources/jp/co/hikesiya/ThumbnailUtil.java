package jp.co.hikesiya;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.FileDescriptor;
import java.io.IOException;
import jp.co.hikesiya.util.Log;

public class ThumbnailUtil {
    public static final int MINI_THUMB_MAX_NUM_PIXELS = 16384;
    public static final int MINI_THUMB_TARGET_SIZE = 96;
    public static final boolean NO_NATIVE = false;
    public static final boolean NO_RECYCLE_INPUT = false;
    public static final boolean NO_ROTATE = false;
    private static final String[] PROJECTION = {"_id", "_data"};
    public static final boolean RECYCLE_INPUT = true;
    public static final boolean ROTATE_AS_NEEDED = true;
    private static final String TAG = "ThumbnailUtil";
    public static final int UNCONSTRAINED = -1;
    public static final boolean USE_NATIVE = true;

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0167  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap getThumbnailBitmap(android.content.ContentResolver r10, long r11, android.graphics.BitmapFactory.Options r13, android.net.Uri r14) {
        /*
            java.lang.String r13 = "ThumbnailUtil"
            java.lang.String r0 = "getThumbnailBitmap() start."
            jp.co.hikesiya.util.Log.d(r13, r0)
            r13 = 0
            r8 = 0
            r6 = 0
            android.net.Uri$Builder r0 = r14.buildUpon()     // Catch:{ SQLiteException -> 0x0155, all -> 0x012f }
            java.lang.String r1 = "blocking"
            java.lang.String r2 = "1"
            android.net.Uri$Builder r0 = r0.appendQueryParameter(r1, r2)     // Catch:{ SQLiteException -> 0x0155, all -> 0x012f }
            java.lang.String r1 = "orig_id"
            java.lang.String r2 = java.lang.String.valueOf(r11)     // Catch:{ SQLiteException -> 0x0155, all -> 0x012f }
            android.net.Uri$Builder r0 = r0.appendQueryParameter(r1, r2)     // Catch:{ SQLiteException -> 0x0155, all -> 0x012f }
            android.net.Uri r1 = r0.build()     // Catch:{ SQLiteException -> 0x0155, all -> 0x012f }
            java.lang.String[] r2 = jp.co.hikesiya.ThumbnailUtil.PROJECTION     // Catch:{ SQLiteException -> 0x0155, all -> 0x012f }
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r10
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ SQLiteException -> 0x0155, all -> 0x012f }
            java.lang.String r2 = "ThumbnailUtil"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            java.lang.String r4 = "blockingUri: "
            r3.<init>(r4)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            jp.co.hikesiya.util.Log.d(r2, r1)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            if (r0 != 0) goto L_0x005e
            java.lang.String r10 = "ThumbnailUtil"
            java.lang.String r11 = "Original image is not found."
            jp.co.hikesiya.util.Log.d(r10, r11)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            java.lang.String r10 = "ThumbnailUtil"
            java.lang.String r11 = "getThumbnailBitmap() end."
            jp.co.hikesiya.util.Log.d(r10, r11)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            if (r0 == 0) goto L_0x0057
            r0.close()
        L_0x0057:
            r10 = 0
            r11 = r0
            r12 = r8
            r9 = r13
            r13 = r10
            r10 = r9
        L_0x005d:
            return r13
        L_0x005e:
            jp.co.hikesiya.MiniThumbFile r2 = jp.co.hikesiya.MiniThumbFile.instance(r14)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            r1 = 10000(0x2710, float:1.4013E-41)
            byte[] r1 = new byte[r1]     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            byte[] r2 = r2.getMiniThumbFromFile(r11, r1)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            if (r2 == 0) goto L_0x00dd
            java.lang.String r2 = "ThumbnailUtil"
            java.lang.String r3 = "thumbFile.getMiniThumbFromFile(origId, data) != null"
            jp.co.hikesiya.util.Log.d(r2, r3)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            r2 = 0
            int r3 = r1.length     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            android.graphics.Bitmap r13 = android.graphics.BitmapFactory.decodeByteArray(r1, r2, r3)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            if (r13 != 0) goto L_0x0082
            java.lang.String r1 = "ThumbnailUtil"
            java.lang.String r2 = "couldn't decode byte array."
            jp.co.hikesiya.util.Log.d(r1, r2)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
        L_0x0082:
            if (r13 != 0) goto L_0x0169
            java.lang.String r1 = "ThumbnailUtil"
            java.lang.String r2 = "Create the thumbnail in memory."
            jp.co.hikesiya.util.Log.d(r1, r2)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            android.net.Uri$Builder r14 = r14.buildUpon()     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            java.lang.String r1 = java.lang.String.valueOf(r11)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            android.net.Uri$Builder r14 = r14.appendPath(r1)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            java.lang.String r14 = r14.toString()     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            java.lang.String r1 = "thumbnails"
            java.lang.String r2 = "media"
            java.lang.String r14 = r14.replaceFirst(r1, r2)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            android.net.Uri r3 = android.net.Uri.parse(r14)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            java.lang.String r14 = "ThumbnailUtil"
            java.lang.String r1 = "c is not null."
            jp.co.hikesiya.util.Log.d(r14, r1)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            r0.close()     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            java.lang.String[] r4 = jp.co.hikesiya.ThumbnailUtil.PROJECTION     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r10
            android.database.Cursor r14 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            if (r14 == 0) goto L_0x00c3
            boolean r0 = r14.moveToFirst()     // Catch:{ SQLiteException -> 0x015b, all -> 0x0142 }
            if (r0 != 0) goto L_0x010a
        L_0x00c3:
            java.lang.String r10 = "ThumbnailUtil"
            java.lang.String r11 = "c is null, or can not move to first."
            jp.co.hikesiya.util.Log.d(r10, r11)     // Catch:{ SQLiteException -> 0x015b, all -> 0x0142 }
            java.lang.String r10 = "ThumbnailUtil"
            java.lang.String r11 = "getThumbnailBitmap() end."
            jp.co.hikesiya.util.Log.d(r10, r11)     // Catch:{ SQLiteException -> 0x015b, all -> 0x0142 }
            if (r14 == 0) goto L_0x00d6
            r14.close()
        L_0x00d6:
            r10 = 0
            r11 = r14
            r12 = r8
            r9 = r13
            r13 = r10
            r10 = r9
            goto L_0x005d
        L_0x00dd:
            java.lang.String r1 = "ThumbnailUtil"
            java.lang.String r2 = "thumbFile.getMiniThumbFromFile(origId, data) == null"
            jp.co.hikesiya.util.Log.d(r1, r2)     // Catch:{ SQLiteException -> 0x00e5, all -> 0x013b }
            goto L_0x0082
        L_0x00e5:
            r10 = move-exception
            r12 = r10
            r11 = r0
            r10 = r13
            r13 = r8
        L_0x00ea:
            java.lang.String r14 = "ThumbnailUtil"
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x0150 }
            jp.co.hikesiya.util.Log.d(r14, r12)     // Catch:{ all -> 0x0150 }
            if (r11 == 0) goto L_0x0167
            r11.close()
            r12 = r13
        L_0x00f9:
            java.lang.String r13 = "ThumbnailUtil"
            java.lang.String r14 = "return bitmap."
            jp.co.hikesiya.util.Log.d(r13, r14)
            java.lang.String r13 = "ThumbnailUtil"
            java.lang.String r14 = "getThumbnailBitmap() end."
            jp.co.hikesiya.util.Log.d(r13, r14)
            r13 = r10
            goto L_0x005d
        L_0x010a:
            r0 = 1
            java.lang.String r0 = r14.getString(r0)     // Catch:{ SQLiteException -> 0x015b, all -> 0x0142 }
            java.lang.String r1 = "ThumbnailUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0161, all -> 0x0149 }
            java.lang.String r4 = "filePath: "
            r2.<init>(r4)     // Catch:{ SQLiteException -> 0x0161, all -> 0x0149 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ SQLiteException -> 0x0161, all -> 0x0149 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLiteException -> 0x0161, all -> 0x0149 }
            jp.co.hikesiya.util.Log.d(r1, r2)     // Catch:{ SQLiteException -> 0x0161, all -> 0x0149 }
            android.graphics.Bitmap r10 = createImageThumbnail(r10, r3, r11)     // Catch:{ SQLiteException -> 0x0161, all -> 0x0149 }
            r11 = r14
            r12 = r0
        L_0x0129:
            if (r11 == 0) goto L_0x00f9
            r11.close()
            goto L_0x00f9
        L_0x012f:
            r10 = move-exception
            r11 = r6
            r12 = r8
            r9 = r10
            r10 = r13
            r13 = r9
        L_0x0135:
            if (r11 == 0) goto L_0x013a
            r11.close()
        L_0x013a:
            throw r13
        L_0x013b:
            r10 = move-exception
            r11 = r0
            r12 = r8
            r9 = r10
            r10 = r13
            r13 = r9
            goto L_0x0135
        L_0x0142:
            r10 = move-exception
            r11 = r14
            r12 = r8
            r9 = r10
            r10 = r13
            r13 = r9
            goto L_0x0135
        L_0x0149:
            r10 = move-exception
            r11 = r14
            r12 = r0
            r9 = r10
            r10 = r13
            r13 = r9
            goto L_0x0135
        L_0x0150:
            r12 = move-exception
            r9 = r12
            r12 = r13
            r13 = r9
            goto L_0x0135
        L_0x0155:
            r10 = move-exception
            r12 = r10
            r11 = r6
            r10 = r13
            r13 = r8
            goto L_0x00ea
        L_0x015b:
            r10 = move-exception
            r12 = r10
            r11 = r14
            r10 = r13
            r13 = r8
            goto L_0x00ea
        L_0x0161:
            r10 = move-exception
            r12 = r10
            r11 = r14
            r10 = r13
            r13 = r0
            goto L_0x00ea
        L_0x0167:
            r12 = r13
            goto L_0x00f9
        L_0x0169:
            r11 = r0
            r12 = r8
            r10 = r13
            goto L_0x0129
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.co.hikesiya.ThumbnailUtil.getThumbnailBitmap(android.content.ContentResolver, long, android.graphics.BitmapFactory$Options, android.net.Uri):android.graphics.Bitmap");
    }

    public static Bitmap createImageThumbnail(ContentResolver cr, Uri uri, long origId) {
        Log.d(TAG, "createImageThumbnail() start.");
        Log.d(TAG, "bitmap is null.");
        Bitmap bitmap = makeBitmap(96, MINI_THUMB_MAX_NUM_PIXELS, uri, cr);
        if (bitmap == null) {
            Log.d(TAG, "Fail to makeBitmap, so bitmap is null.");
            return null;
        }
        Log.d(TAG, "createImageThumbnail() end.");
        return bitmap;
    }

    private static void closeSilently(ParcelFileDescriptor c) {
        Log.d(TAG, "closeSilently() start.");
        if (c == null) {
            Log.d(TAG, "c is null.");
            return;
        }
        Log.d(TAG, "c is not null.");
        try {
            c.close();
        } catch (Throwable th) {
        }
    }

    public static Bitmap makeBitmap(int minSideLength, int maxNumOfPixels, Uri uri, ContentResolver cr) {
        Log.d(TAG, "makeBitmap()");
        ParcelFileDescriptor pfd = null;
        try {
            pfd = cr.openFileDescriptor(uri, "r");
            BitmapFactory.Options options = new BitmapFactory.Options();
            if (pfd == null) {
                pfd = cr.openFileDescriptor(uri, "r");
            }
            if (pfd == null) {
                Log.d(TAG, "makeBitmap() end.");
                closeSilently(pfd);
                return null;
            }
            FileDescriptor fd = pfd.getFileDescriptor();
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(fd, null, options);
            if (options.mCancel || options.outWidth == -1 || options.outHeight == -1) {
                Log.d(TAG, "makeBitmap() end.");
                closeSilently(pfd);
                return null;
            }
            options.inSampleSize = computeSampleSize(options, minSideLength, maxNumOfPixels);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap b = BitmapFactory.decodeFileDescriptor(fd, null, options);
            Log.d(TAG, "makeBitmap() end.");
            closeSilently(pfd);
            return b;
        } catch (IOException e) {
            Log.d(TAG, "IO exception", e);
            Log.d(TAG, "makeBitmap() end.");
            closeSilently(pfd);
            return null;
        } catch (OutOfMemoryError e2) {
            Log.d(TAG, "OutOfMemory exception ", e2);
            Log.d(TAG, "makeBitmap() end.");
            closeSilently(pfd);
            return null;
        } catch (Throwable th) {
            Log.d(TAG, "makeBitmap() end.");
            closeSilently(pfd);
            throw th;
        }
    }

    public static int computeSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
        int roundedSize;
        Log.d(TAG, "computeSampleSize() start.");
        int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);
        if (initialSize <= 8) {
            Log.d(TAG, "initialSize <= 8");
            roundedSize = 1;
            while (roundedSize < initialSize) {
                roundedSize <<= 1;
            }
        } else {
            Log.d(TAG, "initialSize > 8");
            roundedSize = ((initialSize + 7) / 8) * 8;
        }
        Log.d(TAG, "roundedSize: " + roundedSize);
        Log.d(TAG, "computeSampleSize() end.");
        return roundedSize;
    }

    private static int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
        int lowerBound;
        int upperBound;
        Log.d(TAG, "computeInitialSampleSize() start.");
        double w = (double) options.outWidth;
        double h = (double) options.outHeight;
        if (maxNumOfPixels == -1) {
            lowerBound = 1;
        } else {
            lowerBound = (int) Math.ceil(Math.sqrt((w * h) / ((double) maxNumOfPixels)));
        }
        if (minSideLength == -1) {
            upperBound = 128;
        } else {
            upperBound = (int) Math.min(Math.floor(w / ((double) minSideLength)), Math.floor(h / ((double) minSideLength)));
        }
        if (upperBound < lowerBound) {
            Log.d(TAG, "upperBound < lowerBound");
            return lowerBound;
        } else if (maxNumOfPixels == -1 && minSideLength == -1) {
            Log.d(TAG, "(maxNumOfPixels == UNCONSTRAINED)&&(minSideLength == UNCONSTRAINED)");
            return 1;
        } else if (minSideLength == -1) {
            Log.d(TAG, "minSideLength == UNCONSTRAINED");
            return lowerBound;
        } else {
            Log.d(TAG, "return upperBound");
            return upperBound;
        }
    }
}
