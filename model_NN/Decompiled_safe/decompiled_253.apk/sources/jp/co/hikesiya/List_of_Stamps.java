package jp.co.hikesiya;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.picture.style.StyleFactory;
import java.util.HashMap;
import java.util.Iterator;
import jp.co.hikesiya.util.Log;

public class List_of_Stamps implements View.OnClickListener, DialogInterface.OnClickListener {
    private static final int DEFAULT_STAMP_ID = 3000;
    private static final int WC = -2;
    private AlertDialog alert;
    LinearLayout alertLayout = null;
    LinearLayout balloonStamp_1 = null;
    LinearLayout balloonStamp_2 = null;
    private Context context;
    private ImageButton currentButton;
    private int currentStyle;
    private View dialogLayout;
    LinearLayout korokoroStamp_1 = null;
    LinearLayout korokoroStamp_2 = null;
    LinearLayout msgStamp_1 = null;
    LinearLayout msgStamp_2 = null;
    LinearLayout msgStamp_3 = null;
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) WC, (int) WC);
    TextView selectBalloonStamp = null;
    TextView selectKorokoroStamp = null;
    TextView selectMessageStamp = null;
    TextView selectStamp = null;
    private int setStyle;
    private HashMap<Integer, ImageButton> stampStyleMap = new HashMap<>();
    LinearLayout stamp_1 = null;
    LinearLayout stamp_2 = null;
    LinearLayout stamp_3 = null;
    private Surface surface;

    public List_of_Stamps(Context context2, Surface surface2) {
        this.context = context2;
        this.surface = surface2;
        this.stampStyleMap = StyleFactory.getStampStyleMap(this.context);
        setEventListener(this.stampStyleMap);
        makeDialogLinearLayout();
        this.dialogLayout = LayoutInflater.from(context2).inflate((int) R.layout.dialog_stamp, (ViewGroup) null);
        this.alertLayout = (LinearLayout) this.dialogLayout.findViewById(R.id.stamp_dialog_linear);
        setImageDialogLine();
        setView();
        makeAlert();
    }

    public void stamp_onClick() {
        this.currentStyle = StyleFactory.getCurrentId();
        this.setStyle = this.currentStyle;
        setImageButton();
        this.surface.setStyle(StyleFactory.getStyle(this.context, this.currentStyle));
        setBackGroundColor(this.currentButton);
        this.alert.show();
    }

    private void setImageButton() {
        Log.d("List_of_Stamps", "setImageButton start.");
        Iterator<Integer> it = this.stampStyleMap.keySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            int styleId = it.next().intValue();
            Log.d("List_of_Stamps", "styleId: " + styleId);
            Log.d("List_of_Stamps", "currentStyle: " + this.currentStyle);
            if (this.currentStyle == styleId) {
                Log.d("List_of_Stamps", "currentStyle == styleId");
                this.currentButton = this.stampStyleMap.get(Integer.valueOf(styleId));
                break;
            }
            Log.d("List_of_Stamps", "currentStyle != styleId");
            this.currentButton = this.stampStyleMap.get(3000);
        }
        Log.d("List_of_Stamps", "setImageButton end.");
    }

    private void makeAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
        builder.setTitle((int) R.string.dialogStampMenuIndexSelect);
        builder.setPositiveButton((int) R.string.ok, this);
        builder.setNegativeButton((int) R.string.cancel, this);
        builder.setView(this.dialogLayout);
        this.alert = builder.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case WC /*-2*/:
                Log.d("List_of_Stamps", "Cancel");
                this.surface.setStyle(StyleFactory.getStyle(this.context, this.currentStyle));
                return;
            case ThumbnailUtil.UNCONSTRAINED /*-1*/:
                Log.d("List_of_Stamps", "OK");
                if (Integer.toString(this.setStyle).startsWith("3")) {
                    this.surface.setStyle(StyleFactory.getStyle(this.context, this.setStyle));
                    return;
                } else if (Integer.toString(this.setStyle).startsWith("4")) {
                    this.surface.setStyle(StyleFactory.getStyle(this.context, this.setStyle));
                    return;
                } else {
                    this.setStyle = 3000;
                    this.surface.setStyle(StyleFactory.getStyle(this.context, this.setStyle));
                    return;
                }
            default:
                return;
        }
    }

    private void setStyle(int id) {
        this.surface.setStyle(StyleFactory.getStyle(this.context, id));
    }

    private void setBackGroundColor(ImageButton stamp) {
        for (ImageButton ib : this.stampStyleMap.values()) {
            ib.setBackgroundColor(Color.argb(0, 0, 0, 0));
        }
        stamp.setBackgroundColor(-3355444);
    }

    private void stampImage_onClick(ImageButton stampImage, int stampId) {
        setBackGroundColor(stampImage);
        setStyle(stampId);
        this.setStyle = stampId;
    }

    private void setView() {
        this.alertLayout.addView(this.selectStamp);
        this.alertLayout.addView(this.stamp_1);
        this.alertLayout.addView(this.stamp_2);
        this.alertLayout.addView(this.stamp_3);
        this.alertLayout.addView(this.selectKorokoroStamp);
        this.alertLayout.addView(this.korokoroStamp_1);
        this.alertLayout.addView(this.korokoroStamp_2);
        this.alertLayout.addView(this.selectBalloonStamp);
        this.alertLayout.addView(this.balloonStamp_1);
        this.alertLayout.addView(this.balloonStamp_2);
        this.alertLayout.addView(this.selectMessageStamp);
        this.alertLayout.addView(this.msgStamp_1);
        this.alertLayout.addView(this.msgStamp_2);
        this.alertLayout.addView(this.msgStamp_3);
    }

    private void makeDialogLinearLayout() {
        Log.d("List_of_Stamps", "makeDialogLinearLayout is called.");
        this.stamp_1 = setDialogLine(this.stamp_1);
        this.stamp_2 = setDialogLine(this.stamp_2);
        this.stamp_3 = setDialogLine(this.stamp_3);
        this.korokoroStamp_1 = setDialogLine(this.korokoroStamp_1);
        this.korokoroStamp_2 = setDialogLine(this.korokoroStamp_2);
        this.balloonStamp_1 = setDialogLine(this.balloonStamp_1);
        this.balloonStamp_2 = setDialogLine(this.balloonStamp_2);
        this.msgStamp_1 = setDialogLine(this.msgStamp_1);
        this.msgStamp_2 = setDialogLine(this.msgStamp_2);
        this.msgStamp_3 = setDialogLine(this.msgStamp_3);
        makeDialogTextView();
        Log.d("List_of_Stamps", "makeDialogLinearLayout is end.");
    }

    private LinearLayout setDialogLine(LinearLayout ll) {
        LinearLayout ll2 = new LinearLayout(this.context);
        ll2.setOrientation(0);
        ll2.setLayoutParams(this.params);
        ll2.setBaselineAligned(true);
        return ll2;
    }

    private void makeDialogTextView() {
        this.selectStamp = setDialogTextView(this.selectStamp, R.string.dialogStampMenuIndexStamp);
        this.selectKorokoroStamp = setDialogTextView(this.selectKorokoroStamp, R.string.dialogStampMenuIndexKorokoroStamp);
        this.selectBalloonStamp = setDialogTextView(this.selectBalloonStamp, R.string.dialogStampMenuIndexBalloonStamp);
        this.selectMessageStamp = setDialogTextView(this.selectMessageStamp, R.string.dialogStampMenuIndexMessageStamp);
    }

    private TextView setDialogTextView(TextView tv, int textId) {
        TextView tv2 = new TextView(this.context);
        tv2.setWidth(300);
        tv2.setText(textId);
        tv2.setTextSize(20.0f);
        tv2.setTextColor(Color.argb(255, 255, 255, 255));
        return tv2;
    }

    private void setImageDialogLine() {
        Log.d("List_of_Stamps", "setImageDialogLine is called.");
        int maxSize = getImageButtonSize();
        this.stamp_1.addView(this.stampStyleMap.get(3000), maxSize, maxSize);
        this.stamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_HANA_01)), maxSize, maxSize);
        this.stamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_HEART_01)), maxSize, maxSize);
        this.stamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_KINOKO_01)), maxSize, maxSize);
        this.stamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_HOHO_01)), maxSize, maxSize);
        this.stamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_ONPU_01)), maxSize, maxSize);
        this.stamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_BABY_01)), maxSize, maxSize);
        this.stamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_KINTARO_01)), maxSize, maxSize);
        this.stamp_3.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_HANAMARU_01)), maxSize, maxSize);
        this.stamp_3.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_MOTTO_01)), maxSize, maxSize);
        this.stamp_3.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_MEGANE_01)), maxSize, maxSize);
        this.stamp_3.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_HITOMI_01)), maxSize, maxSize);
        this.korokoroStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_KIRAKIRA_01)), maxSize, maxSize);
        this.korokoroStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_KIRAKIRA_02)), maxSize, maxSize);
        this.korokoroStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_KIRAKIRA_03)), maxSize, maxSize);
        this.korokoroStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_STAR_01)), maxSize, maxSize);
        this.korokoroStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_KIRAKIRA_04)), maxSize, maxSize);
        this.korokoroStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_KIRAKIRA_05)), maxSize, maxSize);
        this.korokoroStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_HEART_KORO_01)), maxSize, maxSize);
        this.korokoroStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_HEART_KORO_02)), maxSize, maxSize);
        this.balloonStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_FUKIDASI_01_1)), maxSize, maxSize);
        this.balloonStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_FUKIDASI_01_2)), maxSize, maxSize);
        this.balloonStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_FUKIDASI_01_3)), maxSize, maxSize);
        this.balloonStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_FUKIDASI_01_4)), maxSize, maxSize);
        this.balloonStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_FUKIDASI_02_1)), maxSize, maxSize);
        this.balloonStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_FUKIDASI_02_2)), maxSize, maxSize);
        this.balloonStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_FUKIDASI_02_3)), maxSize, maxSize);
        this.balloonStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_FUKIDASI_02_4)), maxSize, maxSize);
        this.msgStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_CHU_01)), maxSize, maxSize);
        this.msgStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_CHU_02)), maxSize, maxSize);
        this.msgStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_HAPPYBIRTHDAY_01)), maxSize, maxSize);
        this.msgStamp_1.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_THANKYOU_01)), maxSize, maxSize);
        this.msgStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_THANKYOU_02)), maxSize, maxSize);
        this.msgStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_YEAH_01)), maxSize, maxSize);
        this.msgStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_YEAH_02)), maxSize, maxSize);
        this.msgStamp_2.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_ARIGATOU_01)), maxSize, maxSize);
        this.msgStamp_3.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_ARIGATOU_02)), maxSize, maxSize);
        this.msgStamp_3.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_KONNICHIWA_01)), maxSize, maxSize);
        this.msgStamp_3.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_SAYONARA_01)), maxSize, maxSize);
        this.msgStamp_3.addView(this.stampStyleMap.get(Integer.valueOf((int) StyleFactory.STAMP_ITADAKIMASU_01)), maxSize, maxSize);
    }

    private void setEventListener(HashMap<Integer, ImageButton> stampStyleMap2) {
        for (ImageButton ib : stampStyleMap2.values()) {
            ib.setOnClickListener(this);
        }
    }

    public void onClick(View v) {
        Log.d("List_of_Stamps", "(Integer)v.getTag()" + ((Integer) v.getTag()));
        stampImage_onClick((ImageButton) v, ((Integer) v.getTag()).intValue());
    }

    private int getImageButtonSize() {
        int maxSize = 0;
        for (ImageButton ib : this.stampStyleMap.values()) {
            int width = ib.getDrawable().getIntrinsicWidth();
            int height = ib.getDrawable().getIntrinsicHeight();
            if (maxSize < width) {
                maxSize = width;
            }
            if (maxSize < height) {
                maxSize = height;
            }
        }
        return maxSize;
    }
}
