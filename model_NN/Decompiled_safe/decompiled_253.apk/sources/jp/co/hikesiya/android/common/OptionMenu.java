package jp.co.hikesiya.android.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.Locale;

public class OptionMenu {
    private static final String ANNOTATION_EN = "*This page contains links to Websites.";
    private static final String ANNOTATION_JA = "※本画面は外部サイトへのリンクが含まれています。";
    private static final int BLUE = Color.rgb(30, 144, 255);
    private static final String COMPANY_EN = "■Company Info";
    private static final String COMPANY_HP_ADDRESS = "http://www.hikesiya.co.jp/index.html";
    private static final String COMPANY_HP_EN = "HIKESIYA Co., Ltd.";
    private static final String COMPANY_HP_JA = "株式会社ひけしや";
    private static final String COMPANY_JA = "■会社情報";
    private static final String CONTACT_EN = "■Contact Us";
    private static final String CONTACT_JA = "■お問い合わせ";
    private static final int GREEN = Color.rgb(0, 204, 0);
    private static final String HELP = "Help";
    private static final String HP_TWITTER = "http://twitter.com/hks_android_spt";
    private static final String INFO = "Info";
    private static final String MAIL = "Mail";
    private static final String MAIL_ADDRESS = "mailto:android@hikesiya.co.jp";
    private static final String OK = "OK";
    private static final String PRODUCTS_EN = "■Products";
    private static final String PRODUCTS_HP_ADDRESS_EN = "http://www.hikesiya.co.jp/android/android_appList_en.html";
    private static final String PRODUCTS_HP_ADDRESS_JA = "http://www.hikesiya.co.jp/android/android_appList_ja.html";
    private static final String PRODUCTS_HP_EN = "List of our Android apps";
    private static final String PRODUCTS_HP_JA = "ひけしやAndroidアプリ一覧";
    private static final String PRODUCTS_JA = "■製品情報";
    private static final int TEXT_SIZE_INFO_ANNOTATION = 12;
    private static final int TEXT_SIZE_INFO_APP_NAME = 18;
    private static final int TEXT_SIZE_INFO_MAIN = 16;
    private static final String TWITTER = "Twitter";
    private static final String VERSION_NAME_PREFIX = " ver";
    private static final int WHITE = Color.rgb(255, 255, 255);

    public static void showHelp(Activity activity, String helpContent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(HELP);
        ScrollView scrollView = new ScrollView(activity);
        LinearLayout alertLayout = new LinearLayout(activity);
        alertLayout.setPadding(10, 0, 10, 0);
        scrollView.addView(alertLayout);
        TextView helpText = new TextView(activity);
        helpText.setText(helpContent);
        helpText.setTextColor(WHITE);
        alertLayout.addView(helpText);
        builder.setView(scrollView);
        builder.setPositiveButton(OK, (DialogInterface.OnClickListener) null);
        builder.create().show();
    }

    /* JADX INFO: Multiple debug info for r1v5 android.widget.TextView: [D('contactView' android.widget.TextView), D('appNameVerView' android.widget.TextView)] */
    /* JADX INFO: Multiple debug info for r1v6 android.widget.LinearLayout: [D('contactView' android.widget.TextView), D('mailTwitterLayout' android.widget.LinearLayout)] */
    /* JADX INFO: Multiple debug info for r1v7 android.widget.TextView: [D('productsView' android.widget.TextView), D('mailTwitterLayout' android.widget.LinearLayout)] */
    /* JADX INFO: Multiple debug info for r1v8 android.widget.TextView: [D('productsView' android.widget.TextView), D('productsHpView' android.widget.TextView)] */
    /* JADX INFO: Multiple debug info for r1v9 android.widget.TextView: [D('companyView' android.widget.TextView), D('productsHpView' android.widget.TextView)] */
    /* JADX INFO: Multiple debug info for r1v10 android.widget.TextView: [D('companyHpView' android.widget.TextView), D('companyView' android.widget.TextView)] */
    /* JADX INFO: Multiple debug info for r1v11 android.widget.TextView: [D('annotationView' android.widget.TextView), D('companyHpView' android.widget.TextView)] */
    public static void showInfo(final Activity activity) {
        String annotation;
        String productsHpStr;
        String productsStr;
        String contactStr;
        String companyStr;
        String companyHpStr;
        if (Locale.JAPAN.equals(Locale.getDefault())) {
            annotation = ANNOTATION_JA;
            productsHpStr = PRODUCTS_HP_JA;
            productsStr = PRODUCTS_JA;
            contactStr = CONTACT_JA;
            companyStr = COMPANY_JA;
            companyHpStr = COMPANY_HP_JA;
        } else {
            annotation = ANNOTATION_EN;
            productsHpStr = PRODUCTS_HP_EN;
            productsStr = PRODUCTS_EN;
            contactStr = CONTACT_EN;
            companyStr = COMPANY_EN;
            companyHpStr = COMPANY_HP_EN;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(INFO);
        ScrollView infoScrollView = new ScrollView(activity);
        LinearLayout infoLayout = new LinearLayout(activity);
        infoLayout.setOrientation(1);
        infoScrollView.addView(infoLayout);
        TextView appNameVerView = new TextView(activity);
        appNameVerView.setPadding(3, 3, 3, 3);
        StringBuffer appNameVersionBuffer = new StringBuffer("");
        try {
            PackageManager pm = activity.getPackageManager();
            if (!CommonUtil.isNullOrEmpty(pm)) {
                PackageInfo info = pm.getPackageInfo(activity.getPackageName(), 0);
                if (!CommonUtil.isNullOrEmpty(info)) {
                    appNameVersionBuffer.append(activity.getString(info.applicationInfo.labelRes));
                    appNameVersionBuffer.append(VERSION_NAME_PREFIX);
                    appNameVersionBuffer.append(info.versionName);
                }
            }
            appNameVerView.setText(appNameVersionBuffer);
            appNameVerView.setTextColor(GREEN);
            appNameVerView.setTextSize(18.0f);
            infoLayout.addView(appNameVerView);
            TextView appNameVerView2 = new TextView(activity);
            appNameVerView2.setPadding(3, 3, 3, 3);
            appNameVerView2.setText(contactStr);
            appNameVerView2.setTextColor(WHITE);
            appNameVerView2.setTextSize(16.0f);
            infoLayout.addView(appNameVerView2);
            LinearLayout mailTwitterLayout = new LinearLayout(activity);
            mailTwitterLayout.setOrientation(0);
            mailTwitterLayout.setPadding(0, 0, 0, 5);
            infoLayout.addView(mailTwitterLayout);
            TextView mailView = new TextView(activity);
            mailView.setPadding(10, 0, 20, 0);
            SpannableString spanMail = new SpannableString(MAIL);
            spanMail.setSpan(new UnderlineSpan(), 0, spanMail.length(), 0);
            mailView.setText(spanMail);
            mailView.setTextColor(BLUE);
            mailView.setTextSize(16.0f);
            mailView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    activity.startActivity(new Intent("android.intent.action.SENDTO", Uri.parse(OptionMenu.MAIL_ADDRESS)));
                }
            });
            mailTwitterLayout.addView(mailView);
            TextView twitterView = new TextView(activity);
            SpannableString spanTwitter = new SpannableString(TWITTER);
            spanTwitter.setSpan(new UnderlineSpan(), 0, spanTwitter.length(), 0);
            twitterView.setText(spanTwitter);
            twitterView.setTextColor(BLUE);
            twitterView.setTextSize(16.0f);
            twitterView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(OptionMenu.HP_TWITTER)));
                }
            });
            mailTwitterLayout.addView(twitterView);
            TextView productsView = new TextView(activity);
            productsView.setPadding(3, 3, 3, 3);
            productsView.setText(productsStr);
            productsView.setTextColor(WHITE);
            productsView.setTextSize(16.0f);
            infoLayout.addView(productsView);
            TextView productsHpView = new TextView(activity);
            productsHpView.setPadding(10, 0, 0, 5);
            SpannableString spanProductsHp = new SpannableString(productsHpStr);
            spanProductsHp.setSpan(new UnderlineSpan(), 0, spanProductsHp.length(), 0);
            productsHpView.setText(spanProductsHp);
            productsHpView.setTextColor(BLUE);
            productsHpView.setTextSize(16.0f);
            infoLayout.addView(productsHpView);
            productsHpView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Uri uri;
                    if (Locale.JAPAN.equals(Locale.getDefault())) {
                        uri = Uri.parse(OptionMenu.PRODUCTS_HP_ADDRESS_JA);
                    } else {
                        uri = Uri.parse(OptionMenu.PRODUCTS_HP_ADDRESS_EN);
                    }
                    activity.startActivity(new Intent("android.intent.action.VIEW", uri));
                }
            });
            TextView productsHpView2 = new TextView(activity);
            productsHpView2.setPadding(3, 3, 3, 3);
            productsHpView2.setText(companyStr);
            productsHpView2.setTextColor(WHITE);
            productsHpView2.setTextSize(16.0f);
            infoLayout.addView(productsHpView2);
            TextView companyView = new TextView(activity);
            companyView.setPadding(10, 0, 0, 5);
            SpannableString spanCompanyHp = new SpannableString(companyHpStr);
            spanCompanyHp.setSpan(new UnderlineSpan(), 0, spanCompanyHp.length(), 0);
            companyView.setText(spanCompanyHp);
            companyView.setTextColor(BLUE);
            companyView.setTextSize(16.0f);
            infoLayout.addView(companyView);
            companyView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(OptionMenu.COMPANY_HP_ADDRESS)));
                }
            });
            TextView companyHpView = new TextView(activity);
            companyHpView.setPadding(5, 5, 5, 5);
            companyHpView.setText(annotation);
            companyHpView.setTextColor(WHITE);
            companyHpView.setTextSize(12.0f);
            infoLayout.addView(companyHpView);
            builder.setView(infoScrollView);
            builder.setPositiveButton(OK, (DialogInterface.OnClickListener) null);
            builder.create().show();
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
