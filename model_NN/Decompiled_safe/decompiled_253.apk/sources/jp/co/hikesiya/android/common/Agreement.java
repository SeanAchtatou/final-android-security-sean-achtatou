package jp.co.hikesiya.android.common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Agreement {
    private static final String ARTICLE_AGREE = "ArticleAgree";
    private static final String EN_AGREEMENT = "Agreement";
    private static final String EN_AGREEMENT_TEXT_01 = "You have to agree to the entire terms of this agreement to use ";
    private static final String EN_AGREEMENT_TEXT_02 = ". Be sure to carefully read and understand all of the rights and restrictions described in this agreement below before install. HIKESIYA Co., Ltd. will treat your install and use of this application as acceptance of the terms from that point onwards.\n\n1.BACKGROUND\nThis agreement is entered into between HIKESIYA Co., Ltd. and the users.\n\n2.DEFINITIONS\nFor the purposes of this agreement, the following terms shall have the definitions provided below.\n(1) \"This Application\" means ";
    private static final String EN_AGREEMENT_TEXT_03 = ".\n(2) \"The Users\" means individuals or corporations who use or may use this application in accordance with all the terms of this agreement.\n\n3.COPYRIGHT\n(1) The copyright of this application is owned by HIKESIYA Co., Ltd..\n(2) This license agreement acknowledges non-exclusive use of this application to the users. It does not convey ownership.\n\n4.LICENSE AGREEMENT\nHIKESIYA Co., Ltd. acknowledges non-exclusive and unalienable right of this application in accordance with the terms below.\n(1) Install this application to the one equipment (devices which meet the system requirement described in the user manual, the same shall apply hereinafter) and use this with the one equipment.\n\n5.PROHIBITION\nThis application is used under the provisions of Article 5. The acts described below are prohibited.\n(1) The users may not change this application entirely or partly, by decompilation, disassemblies or reverse engineering.\n(2) Copyright and other property rights mark will not be erased or deprived.\n(3) The users may not distribute this application to third party.\n(4) The users may not reproduce this application.\n(5) The users may not use this application against The Public Policy.\n\n6.APPLICATION UPDATE\nHIKESIYA Co., Ltd. might correct this application.\nThe users may bear actual cost like communication charge for download.\n\n7.RETURNING APPLICATION\nThe users may not return the purchased application, change the purchase or request repayment to HIKESIYA Co., Ltd.\n\n8.NO SUPPORT OBLIGATION AND EXEMPTIONS\n(1) This application is delivered with the existing state. HIKESIYA Co., Ltd. does not guarantee no software bugs or other defects, suitableness for specified purpose, unprejudicedness the rights of the users or a third party, or any kind of matters.\n(2) HIKESIYA Co., Ltd. has no obligation to fix, maintain or any kind of matters. It also has no obligation to compensate the users for damage caused by this application or demand by a third party.\n(3) HIKESIYA Co., Ltd. has no obligation to more than the reward that the users paid.\n\n9.REVISION DELIVERY\n(1) HIKESIYA Co., Ltd. may deliver revision or successor of this application voluntarily.\n(2) The terms described in this agreement shall also apply to revision or successor when they are delivered.\n\n10.TERM AND TERMINATION\nThis agreement shall become effective on the users' install and cease to be effective if the matters below occur.\n(1) When the users terminate use of this application and uninstall from the equipments.\n(2) When the users violate the terms described in this agreement.\n\n11.SUPPORT SERVICE\nHIKESIYA Co., Ltd. provides the following service.\n(1) Advice concerning operation of this software by E-mail.\n\n12.CHANGE\n(1) HIKESIYA Co., Ltd. may change the terms or add new ones if necessary without advance notice to the users.\n(2) HIKESIYA Co., Ltd. shall treat your continuous use of this application after the change set forth in the preceding paragraph as acceptance of the terms from that point onwards.\n\n13.APPLICABLE LAW AND JURISDICTION\n(1) This Agreement shall be governed by and construed in accordance with the Law of Japan.\n(2) Actions for this agreement shall be filed to Yokohama District Court, exclusive court of first instance.\n\n14.OTHERS\n(1) HIKESIYA Co., Ltd. is willing to publish this application in magazines or homepages. If you want, please give previous notice to the licensor.\n(2) Please refer to support information below.\nhttp://www.hikesiya.co.jp/contact.html\n\nSUPPLEMENTARY PROVISIONS\n\nThis agreement is entered into as of June 30 2011.";
    private static final String EN_BUTTON_AGREE = "I agree";
    private static final String EN_BUTTON_DISAGREE = "I disagree";
    private static final String EN_DIRECTION = " * Please review the license terms below.";
    private static final String JA_AGREEMENT = "利用規約";
    private static final String JA_AGREEMENT_TEXT_01 = "を使用するためには、下記記載の使用許諾書の全ての条項を承諾いただくことが必要です。本ソフトウェアのインストール前に下記の使用許諾書を十分にお読みください。本ソフトウェアをインストールされた方は、使用許諾書の各条項に承諾したものとみなされます。\n\n１.目的\n　本使用許諾書は、株式会社ひけしやと利用者との間の本ソフトウェアに関する使用許諾等について定めます。\n\n２.定義\n　本使用許諾書において、次の各号に掲げる用語の意義は、次の各号に定めるところによります。\n　(1) 「本ソフトウェア」とは、";
    private static final String JA_AGREEMENT_TEXT_02 = "をいいます。\n　(2) 「利用者」とは、個人及び法人を問わず、本使用許諾書に規定する全ての条項を承諾したうえで本ソフトウェアを使用し、又は使用しようとする者をいいます。\n\n３．著作権\n　(1) 本ソフトウェアの著作権は、株式会社ひけしやに帰属します。\n　(2) 本ソフトウェアは、利用者に対し、本使用許諾書に従い、非独占的に使用許諾されるものです。本ソフトウェアの著作権が譲渡されることはありません。\n\n４．使用許諾\n　株式会社ひけしやは、利用者に対し、次の各号に掲げる事項に関し、本ソフトウェアの非独占的かつ譲渡不可能な権利を許諾します。\n　(1) 本ソフトウェアを対象機器1台（操作マニュアルに規定する環境条件に適合するコンピュータをいう。以下同じ。）にインストールして、対象機器上で本ソフトウェアを使用すること。\n\n５．禁止事項\n　利用者は、第４条で規定する使用許諾内容の範囲内でのみ使用するものとし、次の各号に掲げる行為を行うことはできません。\n　(1) 本ソフトウェアの全部又は一部について改変を加えること並びに逆コンパイル又は逆アセンブル、もしくはリバースエンジニアリングにより改変を加えること。\n　(2) 本ソフトウェアに含まれる著作権表示その他の財産権表示を消去又は剥奪すること。\n　(3) 本ソフトウェアを第三者に配布すること。\n　(4) 本ソフトウェアを複製すること。\n　(5) 公の秩序又は善良の風俗に反して使用すること。\n\n６．本ソフトウェアの修正\n　本ソフトウェアについて、株式会社ひけしやの判断によりバージョンアップを行う場合があります。バージョンアップした本ソフトウェアをダウンロードする場合、それによって発生する通信料その他の費用等は利用者が負担するものとします。\n\n７．返品\n　利用者は、本ソフトウェアの購入後、利用者の都合による返品及び購入内容の変更等をすることができないものとし、株式会社ひけしやに対して返金等を求めることはできないものとします。\n\n８．保証の拒絶及び免責\n　(1) 本ソフトウェアは、利用者に対して現状で提供されるものであり、株式会社ひけしやは、本ソフトウェアにプログラミング上の誤りその他の瑕疵のないこと、本ソフトウェアが特定目的に適合すること、並びに本ソフトウェアの利用者又はそれ以外の第三者の権利を侵害するものではないこと、その他いかなる内容についての保証を行うものではありません。\n　(2) 株式会社ひけしやは、本ソフトウェアの補修、保守その他いかなる義務も負わないものとします。また、本ソフトウェアの使用に起因して、利用者に生じた損害又は第三者からの請求に基づく利用者の損害について、原因のいかんを問わず、一切の責任を負わないものとします。\n　(3) 請求原因の如何を問わず、株式会社ひけしやが負う責任は、利用者が実際に支払った本ソフトウェアの対価を上限とします。\n\n９．改訂版又は後継版の提供\n　(1) 株式会社ひけしやは、任意に本ソフトウェアの改訂版又は後継版を提供することができます。\n　(2) 改訂版が使用可能とされたときは、本使用許諾書に規定する条件は、改訂版の使用許諾の条件として適用するものとします。\n\n１０．期間及び解約\n　本使用許諾書に基づく株式会社ひけしやと利用者との間の本ソフトウェアに係る使用許諾の効力は、利用者が本ソフトウェアをインストールしたときに開始し、次の各号に掲げる事由が生じたときに終了するものとします。\n　(1) 利用者が本ソフトウェアの使用を終了し、対象機器から本ソフトウェアを消去又は削除したとき。\n　(2) 利用者が本使用許諾書に規定する条件に違反したとき。\n\n１１．サポートサービス\n　株式会社ひけしやは、次の各号に掲げるサービスを提供するものとします。\n　(1) 電子メールによる本ソフトウェアの操作に関する助言\n\n１２．変更\n　(1) 株式会社ひけしやは、必要があると認めるときは、利用者に対する事前の通知を行うことなく、いつでも本使用許諾書に規定する条項を変更し、又は新たな条項を追加することができます。\n　(2) 前項による本使用許諾書に規定する条件の変更後に、利用者が本ソフトウェアの使用を継続するときは、利用者は、変更又は追加後の条項に同意したものとみなされます。\n\n１３．準拠法及び管轄\n(1) 本使用許諾書には、日本法が適用されるものとします。\n　(2) 本使用許諾書に関する訴訟は、横浜地方裁判所をもって、第一審の専属管轄裁判所とします。\n\n１４．その他\n　(1) 本ソフトウェアは雑誌、HPへの収録を歓迎しています。収録をご希望の方は、著作者へご連絡ください。\n　(2) サポート情報は以下をご参照ください。\n\thttp://www.hikesiya.co.jp/contact.html\n\n附　則\n\n本使用許諾書は平成２３年６月３０日から施行します。";
    private static final String JA_BUTTON_AGREE = "同意する";
    private static final String JA_BUTTON_DISAGREE = "同意しない";
    private static final String JA_DIRECTION = " ＊以下の規約に同意してご利用ください。";
    private static final String PREFERENCES_FILE_NAME = "PreferencesFile";
    public static boolean isAgree = false;

    /* JADX INFO: Multiple debug info for r2v3 java.lang.String: [D('agreementTitle' java.lang.String), D('agreementText' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v3 android.widget.LinearLayout: [D('agreementTextBuffer' java.lang.StringBuffer), D('agreementLayout' android.widget.LinearLayout)] */
    /* JADX INFO: Multiple debug info for r3v8 android.widget.ScrollView: [D('directionView' android.widget.TextView), D('scrollView' android.widget.ScrollView)] */
    /* JADX INFO: Multiple debug info for r4v2 android.widget.LinearLayout: [D('alertLayout' android.widget.LinearLayout), D('direction' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r3v9 android.widget.TextView: [D('agreementView' android.widget.TextView), D('scrollView' android.widget.ScrollView)] */
    /* JADX INFO: Multiple debug info for r0v12 boolean: [D('settings' android.content.SharedPreferences), D('agreeArticle' boolean)] */
    /* JADX INFO: Multiple debug info for r0v13 android.app.AlertDialog: [D('agreeArticle' boolean), D('alert' android.app.AlertDialog)] */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void showAgreement(final android.app.Activity r11) {
        /*
            java.lang.String r0 = ""
            android.content.pm.PackageManager r2 = r11.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00d7 }
            r1 = 0
            boolean r3 = jp.co.hikesiya.android.common.CommonUtil.isNullOrEmpty(r2)     // Catch:{ NameNotFoundException -> 0x00d7 }
            if (r3 != 0) goto L_0x00ff
            java.lang.String r1 = r11.getPackageName()     // Catch:{ NameNotFoundException -> 0x00d7 }
            r3 = 0
            android.content.pm.PackageInfo r1 = r2.getPackageInfo(r1, r3)     // Catch:{ NameNotFoundException -> 0x00d7 }
            boolean r2 = jp.co.hikesiya.android.common.CommonUtil.isNullOrEmpty(r1)     // Catch:{ NameNotFoundException -> 0x00d7 }
            if (r2 != 0) goto L_0x00ff
            android.content.pm.ApplicationInfo r0 = r1.applicationInfo     // Catch:{ NameNotFoundException -> 0x00d7 }
            int r0 = r0.labelRes     // Catch:{ NameNotFoundException -> 0x00d7 }
            java.lang.String r0 = r11.getString(r0)     // Catch:{ NameNotFoundException -> 0x00d7 }
            r3 = r0
            r0 = r1
        L_0x0026:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r0 = ""
            r1.<init>(r0)
            java.util.Locale r0 = java.util.Locale.JAPAN
            java.util.Locale r2 = java.util.Locale.getDefault()
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00de
            java.lang.String r2 = "利用規約"
            java.lang.String r4 = " ＊以下の規約に同意してご利用ください。"
            java.lang.String r0 = "同意する"
            java.lang.String r5 = "同意しない"
            java.lang.String r6 = "　"
            r1.append(r6)
            r1.append(r3)
            java.lang.String r6 = "を使用するためには、下記記載の使用許諾書の全ての条項を承諾いただくことが必要です。本ソフトウェアのインストール前に下記の使用許諾書を十分にお読みください。本ソフトウェアをインストールされた方は、使用許諾書の各条項に承諾したものとみなされます。\n\n１.目的\n　本使用許諾書は、株式会社ひけしやと利用者との間の本ソフトウェアに関する使用許諾等について定めます。\n\n２.定義\n　本使用許諾書において、次の各号に掲げる用語の意義は、次の各号に定めるところによります。\n　(1) 「本ソフトウェア」とは、"
            r1.append(r6)
            r1.append(r3)
            java.lang.String r3 = "をいいます。\n　(2) 「利用者」とは、個人及び法人を問わず、本使用許諾書に規定する全ての条項を承諾したうえで本ソフトウェアを使用し、又は使用しようとする者をいいます。\n\n３．著作権\n　(1) 本ソフトウェアの著作権は、株式会社ひけしやに帰属します。\n　(2) 本ソフトウェアは、利用者に対し、本使用許諾書に従い、非独占的に使用許諾されるものです。本ソフトウェアの著作権が譲渡されることはありません。\n\n４．使用許諾\n　株式会社ひけしやは、利用者に対し、次の各号に掲げる事項に関し、本ソフトウェアの非独占的かつ譲渡不可能な権利を許諾します。\n　(1) 本ソフトウェアを対象機器1台（操作マニュアルに規定する環境条件に適合するコンピュータをいう。以下同じ。）にインストールして、対象機器上で本ソフトウェアを使用すること。\n\n５．禁止事項\n　利用者は、第４条で規定する使用許諾内容の範囲内でのみ使用するものとし、次の各号に掲げる行為を行うことはできません。\n　(1) 本ソフトウェアの全部又は一部について改変を加えること並びに逆コンパイル又は逆アセンブル、もしくはリバースエンジニアリングにより改変を加えること。\n　(2) 本ソフトウェアに含まれる著作権表示その他の財産権表示を消去又は剥奪すること。\n　(3) 本ソフトウェアを第三者に配布すること。\n　(4) 本ソフトウェアを複製すること。\n　(5) 公の秩序又は善良の風俗に反して使用すること。\n\n６．本ソフトウェアの修正\n　本ソフトウェアについて、株式会社ひけしやの判断によりバージョンアップを行う場合があります。バージョンアップした本ソフトウェアをダウンロードする場合、それによって発生する通信料その他の費用等は利用者が負担するものとします。\n\n７．返品\n　利用者は、本ソフトウェアの購入後、利用者の都合による返品及び購入内容の変更等をすることができないものとし、株式会社ひけしやに対して返金等を求めることはできないものとします。\n\n８．保証の拒絶及び免責\n　(1) 本ソフトウェアは、利用者に対して現状で提供されるものであり、株式会社ひけしやは、本ソフトウェアにプログラミング上の誤りその他の瑕疵のないこと、本ソフトウェアが特定目的に適合すること、並びに本ソフトウェアの利用者又はそれ以外の第三者の権利を侵害するものではないこと、その他いかなる内容についての保証を行うものではありません。\n　(2) 株式会社ひけしやは、本ソフトウェアの補修、保守その他いかなる義務も負わないものとします。また、本ソフトウェアの使用に起因して、利用者に生じた損害又は第三者からの請求に基づく利用者の損害について、原因のいかんを問わず、一切の責任を負わないものとします。\n　(3) 請求原因の如何を問わず、株式会社ひけしやが負う責任は、利用者が実際に支払った本ソフトウェアの対価を上限とします。\n\n９．改訂版又は後継版の提供\n　(1) 株式会社ひけしやは、任意に本ソフトウェアの改訂版又は後継版を提供することができます。\n　(2) 改訂版が使用可能とされたときは、本使用許諾書に規定する条件は、改訂版の使用許諾の条件として適用するものとします。\n\n１０．期間及び解約\n　本使用許諾書に基づく株式会社ひけしやと利用者との間の本ソフトウェアに係る使用許諾の効力は、利用者が本ソフトウェアをインストールしたときに開始し、次の各号に掲げる事由が生じたときに終了するものとします。\n　(1) 利用者が本ソフトウェアの使用を終了し、対象機器から本ソフトウェアを消去又は削除したとき。\n　(2) 利用者が本使用許諾書に規定する条件に違反したとき。\n\n１１．サポートサービス\n　株式会社ひけしやは、次の各号に掲げるサービスを提供するものとします。\n　(1) 電子メールによる本ソフトウェアの操作に関する助言\n\n１２．変更\n　(1) 株式会社ひけしやは、必要があると認めるときは、利用者に対する事前の通知を行うことなく、いつでも本使用許諾書に規定する条項を変更し、又は新たな条項を追加することができます。\n　(2) 前項による本使用許諾書に規定する条件の変更後に、利用者が本ソフトウェアの使用を継続するときは、利用者は、変更又は追加後の条項に同意したものとみなされます。\n\n１３．準拠法及び管轄\n(1) 本使用許諾書には、日本法が適用されるものとします。\n　(2) 本使用許諾書に関する訴訟は、横浜地方裁判所をもって、第一審の専属管轄裁判所とします。\n\n１４．その他\n　(1) 本ソフトウェアは雑誌、HPへの収録を歓迎しています。収録をご希望の方は、著作者へご連絡ください。\n　(2) サポート情報は以下をご参照ください。\n\thttp://www.hikesiya.co.jp/contact.html\n\n附　則\n\n本使用許諾書は平成２３年６月３０日から施行します。"
            r1.append(r3)
            r6 = r5
            r3 = r2
        L_0x0058:
            java.lang.String r2 = r1.toString()
            android.app.AlertDialog$Builder r5 = new android.app.AlertDialog$Builder
            r5.<init>(r11)
            r5.setTitle(r3)
            android.widget.LinearLayout r1 = new android.widget.LinearLayout
            r1.<init>(r11)
            r3 = 1
            r1.setOrientation(r3)
            android.widget.TextView r3 = new android.widget.TextView
            r3.<init>(r11)
            r3.setText(r4)
            r1.addView(r3)
            android.widget.ScrollView r3 = new android.widget.ScrollView
            r3.<init>(r11)
            r1.addView(r3)
            android.widget.LinearLayout r4 = new android.widget.LinearLayout
            r4.<init>(r11)
            r7 = 10
            r8 = 0
            r9 = 10
            r10 = 0
            r4.setPadding(r7, r8, r9, r10)
            r3.addView(r4)
            android.widget.TextView r3 = new android.widget.TextView
            r3.<init>(r11)
            r3.setText(r2)
            r4.addView(r3)
            r5.setView(r1)
            jp.co.hikesiya.android.common.Agreement$1 r1 = new jp.co.hikesiya.android.common.Agreement$1
            r1.<init>(r11)
            r5.setPositiveButton(r0, r1)
            jp.co.hikesiya.android.common.Agreement$2 r0 = new jp.co.hikesiya.android.common.Agreement$2
            r0.<init>(r11)
            r5.setNegativeButton(r6, r0)
            jp.co.hikesiya.android.common.Agreement$3 r0 = new jp.co.hikesiya.android.common.Agreement$3
            r0.<init>()
            r5.setOnKeyListener(r0)
            java.lang.String r0 = "PreferencesFile"
            r1 = 0
            android.content.SharedPreferences r0 = r11.getSharedPreferences(r0, r1)
            java.lang.String r1 = "ArticleAgree"
            r2 = 0
            boolean r0 = r0.getBoolean(r1, r2)
            if (r0 != 0) goto L_0x00d6
            android.app.AlertDialog r0 = r5.create()
            jp.co.hikesiya.android.common.Agreement$4 r1 = new jp.co.hikesiya.android.common.Agreement$4
            r1.<init>(r11)
            r0.setOnCancelListener(r1)
            r0.show()
        L_0x00d6:
            return
        L_0x00d7:
            r11 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            r0.<init>(r11)
            throw r0
        L_0x00de:
            java.lang.String r2 = "Agreement"
            java.lang.String r4 = " * Please review the license terms below."
            java.lang.String r0 = "I agree"
            java.lang.String r5 = "I disagree"
            java.lang.String r6 = "You have to agree to the entire terms of this agreement to use "
            r1.append(r6)
            r1.append(r3)
            java.lang.String r6 = ". Be sure to carefully read and understand all of the rights and restrictions described in this agreement below before install. HIKESIYA Co., Ltd. will treat your install and use of this application as acceptance of the terms from that point onwards.\n\n1.BACKGROUND\nThis agreement is entered into between HIKESIYA Co., Ltd. and the users.\n\n2.DEFINITIONS\nFor the purposes of this agreement, the following terms shall have the definitions provided below.\n(1) \"This Application\" means "
            r1.append(r6)
            r1.append(r3)
            java.lang.String r3 = ".\n(2) \"The Users\" means individuals or corporations who use or may use this application in accordance with all the terms of this agreement.\n\n3.COPYRIGHT\n(1) The copyright of this application is owned by HIKESIYA Co., Ltd..\n(2) This license agreement acknowledges non-exclusive use of this application to the users. It does not convey ownership.\n\n4.LICENSE AGREEMENT\nHIKESIYA Co., Ltd. acknowledges non-exclusive and unalienable right of this application in accordance with the terms below.\n(1) Install this application to the one equipment (devices which meet the system requirement described in the user manual, the same shall apply hereinafter) and use this with the one equipment.\n\n5.PROHIBITION\nThis application is used under the provisions of Article 5. The acts described below are prohibited.\n(1) The users may not change this application entirely or partly, by decompilation, disassemblies or reverse engineering.\n(2) Copyright and other property rights mark will not be erased or deprived.\n(3) The users may not distribute this application to third party.\n(4) The users may not reproduce this application.\n(5) The users may not use this application against The Public Policy.\n\n6.APPLICATION UPDATE\nHIKESIYA Co., Ltd. might correct this application.\nThe users may bear actual cost like communication charge for download.\n\n7.RETURNING APPLICATION\nThe users may not return the purchased application, change the purchase or request repayment to HIKESIYA Co., Ltd.\n\n8.NO SUPPORT OBLIGATION AND EXEMPTIONS\n(1) This application is delivered with the existing state. HIKESIYA Co., Ltd. does not guarantee no software bugs or other defects, suitableness for specified purpose, unprejudicedness the rights of the users or a third party, or any kind of matters.\n(2) HIKESIYA Co., Ltd. has no obligation to fix, maintain or any kind of matters. It also has no obligation to compensate the users for damage caused by this application or demand by a third party.\n(3) HIKESIYA Co., Ltd. has no obligation to more than the reward that the users paid.\n\n9.REVISION DELIVERY\n(1) HIKESIYA Co., Ltd. may deliver revision or successor of this application voluntarily.\n(2) The terms described in this agreement shall also apply to revision or successor when they are delivered.\n\n10.TERM AND TERMINATION\nThis agreement shall become effective on the users' install and cease to be effective if the matters below occur.\n(1) When the users terminate use of this application and uninstall from the equipments.\n(2) When the users violate the terms described in this agreement.\n\n11.SUPPORT SERVICE\nHIKESIYA Co., Ltd. provides the following service.\n(1) Advice concerning operation of this software by E-mail.\n\n12.CHANGE\n(1) HIKESIYA Co., Ltd. may change the terms or add new ones if necessary without advance notice to the users.\n(2) HIKESIYA Co., Ltd. shall treat your continuous use of this application after the change set forth in the preceding paragraph as acceptance of the terms from that point onwards.\n\n13.APPLICABLE LAW AND JURISDICTION\n(1) This Agreement shall be governed by and construed in accordance with the Law of Japan.\n(2) Actions for this agreement shall be filed to Yokohama District Court, exclusive court of first instance.\n\n14.OTHERS\n(1) HIKESIYA Co., Ltd. is willing to publish this application in magazines or homepages. If you want, please give previous notice to the licensor.\n(2) Please refer to support information below.\nhttp://www.hikesiya.co.jp/contact.html\n\nSUPPLEMENTARY PROVISIONS\n\nThis agreement is entered into as of June 30 2011."
            r1.append(r3)
            r6 = r5
            r3 = r2
            goto L_0x0058
        L_0x00ff:
            r3 = r0
            r0 = r1
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.co.hikesiya.android.common.Agreement.showAgreement(android.app.Activity):void");
    }

    /* access modifiers changed from: private */
    public static void btAg_onClick(Context con) {
        SharedPreferences.Editor editor = con.getSharedPreferences(PREFERENCES_FILE_NAME, 0).edit();
        editor.putBoolean(ARTICLE_AGREE, true);
        editor.commit();
    }

    /* access modifiers changed from: private */
    public static void btNotAg_onClick(Activity activity) {
        activity.finish();
    }

    public static int calc(int num1, int num2) {
        return num1 + num2;
    }
}
