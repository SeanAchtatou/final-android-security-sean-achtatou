package jp.co.hikesiya.android.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public class CommonUtil {
    private static final int DATE_CALC_0 = 0;
    private static final int DATE_CALC_23 = 23;
    private static final int DATE_CALC_59 = 59;

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class CALC_PATTERN extends Enum<CALC_PATTERN> {
        public static final CALC_PATTERN DAY = new CALC_PATTERN("DAY", CommonUtil.DATE_CALC_0);
        private static final /* synthetic */ CALC_PATTERN[] ENUM$VALUES;
        public static final CALC_PATTERN HOUR = new CALC_PATTERN("HOUR", 1);

        public static CALC_PATTERN valueOf(String str) {
            return (CALC_PATTERN) Enum.valueOf(CALC_PATTERN.class, str);
        }

        public static CALC_PATTERN[] values() {
            CALC_PATTERN[] calc_patternArr = ENUM$VALUES;
            int length = calc_patternArr.length;
            CALC_PATTERN[] calc_patternArr2 = new CALC_PATTERN[length];
            System.arraycopy(calc_patternArr, CommonUtil.DATE_CALC_0, calc_patternArr2, CommonUtil.DATE_CALC_0, length);
            return calc_patternArr2;
        }

        private CALC_PATTERN(String str, int i) {
        }

        static {
            CALC_PATTERN[] calc_patternArr = new CALC_PATTERN[2];
            calc_patternArr[CommonUtil.DATE_CALC_0] = DAY;
            calc_patternArr[1] = HOUR;
            ENUM$VALUES = calc_patternArr;
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class TIME_PATTERN extends Enum<TIME_PATTERN> {
        public static final TIME_PATTERN END_DAY = new TIME_PATTERN("END_DAY", 2);
        private static final /* synthetic */ TIME_PATTERN[] ENUM$VALUES;
        public static final TIME_PATTERN NONE = new TIME_PATTERN("NONE", CommonUtil.DATE_CALC_0);
        public static final TIME_PATTERN START_DAY = new TIME_PATTERN("START_DAY", 1);

        public static TIME_PATTERN valueOf(String str) {
            return (TIME_PATTERN) Enum.valueOf(TIME_PATTERN.class, str);
        }

        public static TIME_PATTERN[] values() {
            TIME_PATTERN[] time_patternArr = ENUM$VALUES;
            int length = time_patternArr.length;
            TIME_PATTERN[] time_patternArr2 = new TIME_PATTERN[length];
            System.arraycopy(time_patternArr, CommonUtil.DATE_CALC_0, time_patternArr2, CommonUtil.DATE_CALC_0, length);
            return time_patternArr2;
        }

        private TIME_PATTERN(String str, int i) {
        }

        static {
            TIME_PATTERN[] time_patternArr = new TIME_PATTERN[3];
            time_patternArr[CommonUtil.DATE_CALC_0] = NONE;
            time_patternArr[1] = START_DAY;
            time_patternArr[2] = END_DAY;
            ENUM$VALUES = time_patternArr;
        }
    }

    private CommonUtil() {
    }

    public static boolean isNull(Object anObj) {
        if (anObj == null) {
            return true;
        }
        return false;
    }

    public static boolean isNullOrEmpty(Object anObj) {
        if (anObj == null) {
            return true;
        }
        if (anObj.toString().length() == 0) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(Object anObj) {
        if (anObj == null) {
            return DATE_CALC_0;
        }
        if (anObj.toString().length() == 0) {
            return true;
        }
        return DATE_CALC_0;
    }

    public static boolean isAlphaNumeric(String aString) {
        if (isNullOrEmpty(aString)) {
            return DATE_CALC_0;
        }
        if (Pattern.matches("^[0-9a-zA-Z]+$", aString)) {
            return true;
        }
        return DATE_CALC_0;
    }

    public static boolean isNumeric(String aString) {
        if (isNullOrEmpty(aString)) {
            return DATE_CALC_0;
        }
        if (Pattern.matches("^[0-9]+$", aString)) {
            return true;
        }
        return DATE_CALC_0;
    }

    public static boolean isAscii(String aString) {
        if (isNullOrEmpty(aString)) {
            return DATE_CALC_0;
        }
        if (Pattern.matches("^\\p{ASCII}+$", aString)) {
            return true;
        }
        return DATE_CALC_0;
    }

    public static boolean isAlphaNumericAndUB(String aString) {
        if (isNullOrEmpty(aString)) {
            return DATE_CALC_0;
        }
        if (Pattern.matches("^[0-9a-zA-Z_]+$", aString)) {
            return true;
        }
        return DATE_CALC_0;
    }

    public static Date getDate() {
        return new Date();
    }

    public static String getDateString(Date aDate, String aFormat) {
        if (isNull(aDate)) {
            return "";
        }
        return new SimpleDateFormat(aFormat).format(aDate);
    }

    public static Date getStringDate(String aString, String aFormat) throws ParseException {
        if (isNullOrEmpty(aString)) {
            return null;
        }
        return new SimpleDateFormat(aFormat).parse(aString);
    }

    public static boolean isStringDate(String aString, String aFormat) {
        if (isNullOrEmpty(aString)) {
            return false;
        }
        boolean result = true;
        try {
            getStringDate(aString, aFormat);
        } catch (ParseException e) {
            result = false;
        }
        return result;
    }

    public static int getMonth(String aString) {
        String[] months = new String[12];
        months[DATE_CALC_0] = "Jan";
        months[1] = "Feb";
        months[2] = "Mar";
        months[3] = "Apr";
        months[4] = "May";
        months[5] = "Jun";
        months[6] = "Jul";
        months[7] = "Aug";
        months[8] = "Sep";
        months[9] = "Oct";
        months[10] = "Nov";
        months[11] = "Dec";
        for (int i = DATE_CALC_0; i < months.length; i++) {
            if (months[i].equals(aString)) {
                return i + 1;
            }
        }
        return DATE_CALC_0;
    }

    public static String getHexString(int aNum, int aPadding) {
        String hex = Integer.toHexString(aNum);
        StringBuffer buf = new StringBuffer();
        for (int i = DATE_CALC_0; i < aPadding - hex.length(); i++) {
            buf.append((int) DATE_CALC_0);
        }
        buf.append(hex);
        return buf.toString();
    }

    public static String replaceAll(String aTarget, String[] aReplace, String[] aValue) {
        String result = aTarget;
        int i = DATE_CALC_0;
        while (i < aReplace.length && i < aValue.length) {
            result = replaceAll(result, aReplace[i], aValue[i]);
            i++;
        }
        return result;
    }

    public static String replaceAll(String aTarget, String aReplace, String aValue) {
        if (isNull(aValue)) {
            aValue = "";
        }
        return aTarget.replaceAll(aReplace, aValue.replaceAll("\\\\", "\\\\\\\\"));
    }

    public static Date getBaseDate(Date aDate, int aSub, CALC_PATTERN aCalc, TIME_PATTERN aTime) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(aDate);
        cal.set(14, DATE_CALC_0);
        if (aCalc == CALC_PATTERN.DAY) {
            cal.add(5, aSub * -1);
        } else {
            cal.add(11, aSub * -1);
        }
        if (aTime == TIME_PATTERN.START_DAY) {
            if (aCalc == CALC_PATTERN.DAY) {
                cal.set(11, DATE_CALC_0);
            }
            cal.set(12, DATE_CALC_0);
            cal.set(13, DATE_CALC_0);
        } else if (aTime == TIME_PATTERN.END_DAY) {
            if (aCalc == CALC_PATTERN.DAY) {
                cal.set(11, DATE_CALC_23);
            }
            cal.set(12, DATE_CALC_59);
            cal.set(13, DATE_CALC_59);
        }
        return cal.getTime();
    }

    public static String getArayString(String[] anArgs, int anIndex) {
        if (anIndex < anArgs.length) {
            return anArgs[anIndex];
        }
        return "";
    }
}
