package jp.co.hikesiya.android.common;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int background = 2130903040;
        public static final int black = 2130903042;
        public static final int blue2 = 2130903046;
        public static final int cornsilk = 2130903048;
        public static final int game_time_text = 2130903041;
        public static final int green = 2130903047;
        public static final int red = 2130903044;
        public static final int white = 2130903043;
        public static final int yellow2 = 2130903045;
    }

    public static final class id {
        public static final int appNameVersion = 2131034112;
        public static final int comView = 2131034116;
        public static final int hpView = 2131034115;
        public static final int mailView = 2131034113;
        public static final int twitterView = 2131034114;
    }

    public static final class layout {
        public static final int menu_option_dialog_info = 2130837504;
    }

    public static final class string {
        public static final int About = 2130968601;
        public static final int Mail = 2130968603;
        public static final int Twitter = 2130968604;
        public static final int Website = 2130968606;
        public static final int annotation = 2130968609;
        public static final int appListFooter = 2130968584;
        public static final int appListMsgAnimal = 2130968593;
        public static final int appListMsgHikae = 2130968592;
        public static final int appListMsgNeoki = 2130968591;
        public static final int appListMsgRakugaki = 2130968594;
        public static final int appListMsgTapNumber = 2130968595;
        public static final int appListNameAnimal = 2130968589;
        public static final int appListNameHikae = 2130968587;
        public static final int appListNameNeoki = 2130968586;
        public static final int appListNameRakugaki = 2130968588;
        public static final int appListNameTapNumber = 2130968590;
        public static final int appListPriceFree = 2130968585;
        public static final int appListTitle = 2130968583;
        public static final int app_name = 2130968577;
        public static final int appname_and_version = 2130968608;
        public static final int company = 2130968610;
        public static final int company_info = 2130968607;
        public static final int contact = 2130968602;
        public static final int email = 2130968597;
        public static final int hello = 2130968576;
        public static final int help_content = 2130968582;
        public static final int help_title = 2130968581;
        public static final int hp = 2130968599;
        public static final int hptop = 2130968600;
        public static final int menu_help = 2130968578;
        public static final int menu_info = 2130968579;
        public static final int menu_other = 2130968580;
        public static final int ok = 2130968596;
        public static final int products = 2130968605;
        public static final int twitter = 2130968598;
    }
}
