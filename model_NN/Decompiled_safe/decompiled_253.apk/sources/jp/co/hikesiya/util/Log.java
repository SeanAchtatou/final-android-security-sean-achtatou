package jp.co.hikesiya.util;

import android.content.Context;
import android.content.pm.PackageManager;
import jp.co.hikesiya.common.Utility;

public class Log {
    private static final String DEFAULT_TAG = "DefaultTag";
    private static final String EMPTY_STR = "\"\"";
    private static boolean IS_DEBUG = true;
    private static final String NULL_STR = "null";

    public static int e(String tag, String msg, Throwable exception) {
        if (Utility.isNull(exception)) {
            return e(tag, msg);
        }
        return android.util.Log.e(isTag(tag), isMsg(msg), exception);
    }

    public static int e(String tag, String msg) {
        return android.util.Log.e(isTag(tag), isMsg(msg));
    }

    public static int w(String tag, String msg, Throwable exception) {
        if (Utility.isNull(exception)) {
            return w(tag, msg);
        }
        return android.util.Log.w(isTag(tag), isMsg(msg), exception);
    }

    public static int w(String tag, String msg) {
        return android.util.Log.w(isTag(tag), isMsg(msg));
    }

    public static int i(String tag, String msg, Throwable exception) {
        if (Utility.isNull(exception)) {
            return i(tag, msg);
        }
        return android.util.Log.i(isTag(tag), isMsg(msg), exception);
    }

    public static int i(String tag, String msg) {
        return android.util.Log.i(isTag(tag), isMsg(msg));
    }

    public static int d(String tag, String msg, Throwable exception) {
        if (!IS_DEBUG) {
            return 0;
        }
        if (Utility.isNull(exception)) {
            return d(tag, msg);
        }
        return android.util.Log.d(isTag(tag), isMsg(msg), exception);
    }

    public static int d(String tag, String msg) {
        if (!IS_DEBUG) {
            return 0;
        }
        return android.util.Log.d(isTag(tag), isMsg(msg));
    }

    public static int v(String tag, String msg, Throwable exception) {
        if (!IS_DEBUG) {
            return 0;
        }
        if (Utility.isNull(exception)) {
            return v(tag, msg);
        }
        return android.util.Log.v(isTag(tag), isMsg(msg), exception);
    }

    public static int v(String tag, String msg) {
        if (!IS_DEBUG) {
            return 0;
        }
        return android.util.Log.v(isTag(tag), isMsg(msg));
    }

    private static String isTag(String tag) {
        String result = tag;
        if (Utility.isNullOrEmpty(result)) {
            return DEFAULT_TAG;
        }
        return result;
    }

    private static String isMsg(String msg) {
        String result = msg;
        if (Utility.isNull(result)) {
            return NULL_STR;
        }
        if (Utility.isEmpty(result)) {
            return EMPTY_STR;
        }
        return result;
    }

    public static void setDebugFlag(Context context) {
        IS_DEBUG = isDebuggable(context);
    }

    private static boolean isDebuggable(Context context) {
        try {
            if ((context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).flags & 2) == 2) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
