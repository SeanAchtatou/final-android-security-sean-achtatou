package jp.co.hikesiya;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;

public class MediaScannerNotifier implements MediaScannerConnection.MediaScannerConnectionClient {
    private MediaScannerConnection mConnection;
    private String mMimeType;
    private String mPath;

    public MediaScannerNotifier(Context context, String path, String mimeType) {
        this.mPath = path;
        this.mMimeType = mimeType;
        this.mConnection = new MediaScannerConnection(context, this);
        this.mConnection.connect();
    }

    public void onMediaScannerConnected() {
        this.mConnection.scanFile(this.mPath, this.mMimeType);
    }

    public void onScanCompleted(String path, Uri uri) {
        this.mConnection.disconnect();
    }
}
