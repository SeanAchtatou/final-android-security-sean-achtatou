package jp.co.hikesiya;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import jp.co.hikesiya.common.Utility;
import jp.co.hikesiya.util.Log;

public class Surface extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public Bitmap bitmap;
    private Context context;
    private StyleController controller;
    private String dirName;
    private DrawingView dv;
    public Bitmap editBitmap;
    ArrayList<Bitmap> editBitmapArray = new ArrayList<>();
    private ScheduledExecutorService executor;
    private String extension = ".jpeg";
    private String fileName;
    private boolean flgOverflow = false;
    public int h;
    private boolean hasUnsavedOperation = false;
    /* access modifiers changed from: private */
    public SurfaceHolder holder;
    private String imageFile;
    private int imageNum;
    private boolean imageOrientation = true;
    private String imagePath;
    private String imagePrefix = "rakugaki_";
    private String imageTitle = "";
    private String mimetype = "image/jpeg";
    public int[] pixels;
    public Bitmap redrawBitmap;
    /* access modifiers changed from: private */
    public boolean redraw_now = false;
    private final int undoLimit = 10;
    public int w;

    public Surface(Context context2, String imageUri) throws IOException {
        super(context2);
        Log.setDebugFlag(context2);
        Log.d("Surface", "start constractor.");
        this.dv = new DrawingView(this, null);
        this.imagePath = imageUri;
        this.holder = getHolder();
        this.holder.addCallback(this);
        setFocusable(true);
        requestFocus();
        setClickable(true);
        this.context = context2;
        this.controller = new StyleController(context2);
        resize(imageUri);
        makeSaveFileName(context2);
        Log.d("Surface", "end constracter.");
    }

    public String getImageTitle() {
        return this.imageTitle;
    }

    public String getImageFile() {
        return this.imageFile;
    }

    public String getDirName() {
        return this.dirName;
    }

    public String getfileName() {
        return this.fileName;
    }

    public int getImageHeight() {
        return this.bitmap.getHeight();
    }

    public int getImageWidth() {
        return this.bitmap.getWidth();
    }

    public boolean getImageOrientation() {
        return this.imageOrientation;
    }

    private void resize(String imageUri) throws IOException {
        int scale;
        float mImageWidth;
        float mImageHeight;
        float dispScale;
        Log.d("Surface", "start resize.");
        boolean resizeFlg = true;
        Resources res = getResources();
        DisplayMetrics metrics = res.getDisplayMetrics();
        int dispW = metrics.widthPixels;
        int dispH = metrics.heightPixels;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDither = true;
        BitmapFactory.decodeFile(imageUri, options);
        if (options.outWidth > options.outHeight) {
            this.imageOrientation = true;
        } else {
            this.imageOrientation = false;
        }
        int orientation = res.getConfiguration().orientation;
        if ((this.imageOrientation && orientation == 1) || (!this.imageOrientation && orientation == 2)) {
            dispW = metrics.heightPixels;
            dispH = metrics.widthPixels;
        }
        if (options.outWidth <= dispW && options.outHeight <= dispH) {
            resizeFlg = false;
        }
        if (!resizeFlg) {
            scale = 1;
        } else {
            scale = Math.max(options.outWidth / dispW, options.outHeight / dispH);
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = scale;
        Bitmap image = BitmapFactory.decodeFile(imageUri, options);
        if (Utility.isNullOrEmpty(image)) {
            Log.d("Surface", "image is null.");
            throw new IOException();
        }
        if (!resizeFlg) {
            mImageWidth = (float) options.outWidth;
            mImageHeight = (float) options.outHeight;
            dispScale = 1.0f;
        } else {
            mImageWidth = (float) image.getWidth();
            mImageHeight = (float) image.getHeight();
            dispScale = Math.min(((float) dispH) / mImageHeight, ((float) dispW) / mImageWidth);
        }
        this.bitmap = Bitmap.createScaledBitmap(image, (int) (mImageWidth * dispScale), (int) (mImageHeight * dispScale), true);
        changeAlphaToBlack();
        this.editBitmap = this.bitmap.copy(Bitmap.Config.RGB_565, true);
        this.redrawBitmap = this.bitmap.copy(Bitmap.Config.RGB_565, true);
        image.recycle();
        Log.d("Surface", "end resize.");
    }

    private void changeAlphaToBlack() {
        Log.d("Surface", "changeAlphaToBlack() start.");
        this.w = this.bitmap.getWidth();
        this.h = this.bitmap.getHeight();
        this.pixels = new int[(this.w * this.h)];
        this.bitmap.getPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
        for (int i = 0; i < this.pixels.length; i++) {
            if (Color.alpha(this.pixels[i]) == 0) {
                this.pixels[i] = Color.argb(255, 0, 0, 0);
            }
        }
        this.bitmap.setPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
        Log.d("Surface", "changeAlphaToBlack() end.");
    }

    private void makeSaveFileName(Context context2) {
        Log.d("Surface", "makeSaveFileName start.");
        boolean checkFlag = false;
        this.dirName = String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/Rakugaki/";
        Log.d("Surface", "dirName is " + this.dirName);
        Log.d("Surface", "imagePath is " + this.imagePath);
        if (this.imagePath.startsWith(this.dirName)) {
            this.imageFile = this.imagePath.substring(this.dirName.length());
            checkFlag = true;
        } else if (this.imagePath.startsWith("/sdcard/Rakugaki/")) {
            this.imageFile = this.imagePath.substring("/sdcard/Rakugaki/".length());
            checkFlag = true;
        }
        if (!checkFlag) {
            makeNewFileName();
        } else if (this.imagePath.endsWith(this.extension)) {
            Log.d("Surface", "Path is Rakugaki.");
            Log.d("Surface", "imageFile is " + this.imageFile);
            this.imageTitle = this.imageFile.substring(0, this.imageFile.length() - this.extension.length());
            Log.d("Surface", "imageTitle is " + this.imageTitle);
            this.fileName = String.valueOf(this.dirName) + this.imageFile;
            Log.d("Surface", "fileName is " + this.fileName);
        } else {
            makeNewFileName();
        }
        Log.d("Surface", "makeSaveFileName end.");
    }

    private void makeNewFileName() {
        Log.d("Surface", "makeNewFileName is called.");
        this.imageNum = this.context.getSharedPreferences("PREFERENCES_FILE", 0).getInt("number", 0);
        String today = new SimpleDateFormat("yyyyMMdd").format(new Date());
        do {
            this.imageNum++;
            String strPhotoNo = Integer.toString(this.imageNum);
            if (this.imageNum >= 0 && this.imageNum <= 9) {
                strPhotoNo = "000" + strPhotoNo;
            } else if (this.imageNum >= 10 && this.imageNum <= 99) {
                strPhotoNo = "00" + strPhotoNo;
            } else if (this.imageNum >= 100 && this.imageNum <= 999) {
                strPhotoNo = "0" + strPhotoNo;
            } else if (this.imageNum >= 1000) {
            }
            this.imageTitle = "rakugaki_" + today + "_" + strPhotoNo;
            this.imageFile = String.valueOf(this.imageTitle) + ".jpeg";
            this.fileName = String.valueOf(this.dirName) + this.imageFile;
            File dir = new File(this.dirName);
            if (!dir.exists()) {
                dir.mkdir();
            }
        } while (new File(this.fileName).exists());
        Log.d("Surface", "makeNewFileName end.");
    }

    public void writeImageNumber(Context context2) {
        SharedPreferences.Editor editor = context2.getSharedPreferences("PREFERENCES_FILE", 0).edit();
        editor.putInt("number", this.imageNum);
        editor.commit();
    }

    private void writeFileScanHistory(Context c) {
        SharedPreferences.Editor editor = c.getSharedPreferences("PREFERENCES_FILE", 0).edit();
        editor.putBoolean("scanFileHistory", true);
        editor.commit();
    }

    private boolean readFileScanHistory(Context c) {
        return c.getSharedPreferences("PREFERENCES_FILE", 0).getBoolean("scanFileHistory", false);
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        Log.d("Surface", "surfaceCreated() start.");
        Log.d("Surface", "surfaceCreated() end.");
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
        Log.d("Surface", "surfaceChanged() start.");
        Log.d("Surface", "surfaceChanged() end.");
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
        Log.d("Surface", "surfaceDestroyed() start.");
        stopExecutorService();
        Log.d("Surface", "surfaceDestroyed() end.");
    }

    public void startExecutorService() {
        Log.d("Surface", "startExecutorService() start.");
        try {
            this.executor = Executors.newSingleThreadScheduledExecutor();
            this.executor.scheduleAtFixedRate(this.dv, 100, 100, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            Log.d("Surface", "startExecutorService() catch Exception.");
            new Thread(this).run();
        }
        Log.d("Surface", "startExecutorService() end.");
    }

    private class DrawingView implements Runnable {
        private DrawingView() {
        }

        /* synthetic */ DrawingView(Surface surface, DrawingView drawingView) {
            this();
        }

        public void run() {
            Canvas canvas = Surface.this.holder.lockCanvas();
            if (canvas != null) {
                if (Surface.this.redraw_now) {
                    canvas.drawBitmap(Surface.this.redrawBitmap, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(Surface.this.editBitmap, 0.0f, 0.0f, (Paint) null);
                }
                Surface.this.holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public void stopExecutorService() {
        Log.d("Surface", "stopExecutorService() start.");
        this.executor.shutdown();
        Log.d("Surface", "stopExecutorService() end.");
    }

    public boolean onTouchEvent(MotionEvent event) {
        Log.d("Surface", "onTouchEvent() start.");
        boolean result = this.controller.selectStyle(event, this);
        Log.d("Surface", "onTouchEvent() end.");
        return result;
    }

    public void stackBitmap() {
        Log.d("Surface", "stackBitmap() start.");
        giveUp();
        this.editBitmapArray.add(this.editBitmap.copy(Bitmap.Config.RGB_565, true));
        this.editBitmap.getPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
        if (this.editBitmapArray.size() > 0) {
            this.hasUnsavedOperation = true;
        }
        Log.d("Surface", "stackBitmap() end.");
    }

    /* access modifiers changed from: package-private */
    public void giveUp() {
        Log.d("Surface", "giveUp() start.");
        if (!this.editBitmapArray.isEmpty() && this.editBitmapArray.size() == 10) {
            this.flgOverflow = true;
        } else if (!this.editBitmapArray.isEmpty() && this.editBitmapArray.size() >= 11) {
            this.editBitmapArray.get(0).recycle();
            this.editBitmapArray.remove(0);
        }
        System.gc();
        Log.d("Surface", "giveUp() end.");
    }

    /* access modifiers changed from: package-private */
    public void undo() {
        Log.d("Surface", "undo() start.");
        Canvas canvas = this.holder.lockCanvas();
        if (canvas != null) {
            Log.d("Surface", "undo() canvas != null");
            if (!this.editBitmapArray.isEmpty() && this.editBitmapArray.size() >= 2) {
                Log.d("Surface", "undo() editBitmapArray.size() >= 2");
                this.editBitmapArray.get(this.editBitmapArray.size() - 1).recycle();
                this.editBitmapArray.remove(this.editBitmapArray.size() - 1);
                this.editBitmap = this.editBitmapArray.get(this.editBitmapArray.size() - 1).copy(Bitmap.Config.RGB_565, true);
                this.editBitmap.getPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
                canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
            } else if (!this.flgOverflow && !this.editBitmapArray.isEmpty() && this.editBitmapArray.size() == 1) {
                Log.d("Surface", "undo() !flgOverflow");
                this.editBitmapArray.get(0).recycle();
                this.editBitmapArray.remove(0);
                this.editBitmap = this.bitmap.copy(Bitmap.Config.RGB_565, true);
                this.editBitmap.getPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
                canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
            } else if (this.flgOverflow && !this.editBitmapArray.isEmpty() && this.editBitmapArray.size() == 1) {
                Log.d("Surface", "undo() flgOverflow");
                this.editBitmap = this.editBitmapArray.get(0).copy(Bitmap.Config.RGB_565, true);
                this.editBitmap.getPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
                canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
            }
            this.holder.unlockCanvasAndPost(canvas);
        }
        System.gc();
        if (this.editBitmapArray.size() == 0) {
            this.hasUnsavedOperation = false;
        }
        Log.d("Surface", "undo() editBitmapArray.size() = " + this.editBitmapArray.size());
        Log.d("Surface", "undo() end.");
    }

    public void drawOnResume() {
        Log.d("Surface", "drawOnResume() start.");
        if (this.redraw_now) {
            Log.d("Surface", "drawOnResume() redraw_now == true");
            redrawFlgOff();
            this.redrawBitmap.setPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
        }
        startExecutorService();
        Log.d("Surface", "drawOnResume() start.");
    }

    public void clearBitmap(Context c) {
        Log.d("Surface", "clearBitmap() start.");
        Canvas canvas = this.holder.lockCanvas();
        if (canvas != null) {
            Log.d("Surface", "clearBitmap() canvas != null");
            this.bitmap.getPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
            this.editBitmap.setPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
            this.redrawBitmap.setPixels(this.pixels, 0, this.w, 0, 0, this.w, this.h);
            this.editBitmapArray.clear();
            canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
            this.holder.unlockCanvasAndPost(canvas);
            this.flgOverflow = false;
            this.controller.clear(c);
            this.hasUnsavedOperation = false;
        }
        Log.d("Surface", "clearBitmap() end.");
    }

    public boolean saveBitmap(Context c, String filename) throws SDCardFullException, FileNotFoundException {
        stopExecutorService();
        try {
            if (new RakugakiCameraUtility().overSDCapacity()) {
                throw new SDCardFullException("");
            }
            String fileName2 = String.valueOf(this.dirName) + filename + this.extension;
            this.editBitmap.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(fileName2));
            this.bitmap = this.editBitmap.copy(Bitmap.Config.RGB_565, true);
            clearBitmap(this.context);
            if (!readFileScanHistory(c)) {
                for (String im : new File(this.dirName).list()) {
                    if (im.startsWith(this.imagePrefix)) {
                        new MediaScannerNotifier(c, String.valueOf(this.dirName) + im, this.mimetype);
                        Log.d("Surface", im);
                    }
                }
                writeFileScanHistory(c);
            }
            new MediaScannerNotifier(c, fileName2, this.mimetype);
            startExecutorService();
            this.hasUnsavedOperation = false;
            this.flgOverflow = false;
            return true;
        } catch (FileNotFoundException e) {
            FileNotFoundException fe = e;
            startExecutorService();
            throw fe;
        }
    }

    public void setStyle(Style style) {
        this.controller.setStyle(style);
    }

    public void setPaintColor(int color) {
        this.controller.setPaintColor(color);
    }

    public int getPaintColor() {
        return this.controller.getPaintColor();
    }

    public void run() {
        stopExecutorService();
    }

    public boolean hasUnsavedOperation() {
        return this.hasUnsavedOperation;
    }

    public SurfaceHolder getSurfaceHolder() {
        return this.holder;
    }

    public Bitmap getEditBitmap() {
        return this.editBitmap;
    }

    public void redrawFlgOn() {
        this.redraw_now = true;
    }

    public void redrawFlgOff() {
        this.redraw_now = false;
    }
}
