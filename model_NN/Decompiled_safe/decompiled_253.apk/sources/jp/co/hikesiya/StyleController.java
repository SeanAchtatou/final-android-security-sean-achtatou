package jp.co.hikesiya;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import com.picture.style.KoroKoroStamps;
import com.picture.style.SimpleLines;
import com.picture.style.Stamps;
import com.picture.style.StyleFactory;
import jp.co.hikesiya.util.Log;

public class StyleController {
    Canvas canvas;
    private int color = Color.argb(255, 255, 150, 0);
    Bitmap editBitmap;
    Canvas editBitmapCanvas;
    SurfaceHolder holder;
    ProgressDialog pd;
    private Style style;
    private boolean toDraw = false;

    public StyleController(Context context) {
        clear(context);
    }

    public void draw(Canvas c) {
        if (this.toDraw) {
            this.style.draw(c);
        }
    }

    public void setStyle(Style style2) {
        this.toDraw = false;
        style2.setColor(this.color);
        this.style = style2;
    }

    public boolean selectStyle(MotionEvent event, Surface surface) {
        Log.d("StyleController", "selectStyle() start.");
        if (this.style instanceof SimpleLines) {
            drawSimpleLines(event, surface);
        } else if (this.style instanceof Stamps) {
            drawStamps(event, surface);
        } else if (this.style instanceof KoroKoroStamps) {
            drawKoroKoroStamps(event, surface);
        }
        Log.d("StyleController", "selectStyle() end.");
        return true;
    }

    private void drawSimpleLines(MotionEvent event, Surface surface) {
        Log.d("StyleController", "drawSimpleLines() start.");
        this.holder = surface.getSurfaceHolder();
        this.canvas = this.holder.lockCanvas();
        float x = event.getX();
        float y = event.getY();
        if (this.canvas != null) {
            Log.d("StyleController", "drawSimpleLines() canvas != null");
            this.editBitmap = surface.getEditBitmap();
            this.editBitmapCanvas = new Canvas(this.editBitmap);
            this.editBitmapCanvas.setBitmap(this.editBitmap);
            switch (event.getAction()) {
                case 0:
                    Log.d("StyleController", "drawSimpleLines() ACTION_DOWN");
                    this.toDraw = true;
                    this.style.strokeStart(this.editBitmapCanvas, x, y);
                    this.canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
                    break;
                case 1:
                    Log.d("StyleController", "drawSimpleLines() ACTION_UP");
                    this.style.stroke(this.editBitmapCanvas, x, y);
                    this.canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
                    surface.stackBitmap();
                    break;
                case 2:
                    Log.d("StyleController", "drawSimpleLines() ACTION_MOVE");
                    this.style.stroke(this.editBitmapCanvas, x, y);
                    this.canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
                    break;
            }
            this.holder.unlockCanvasAndPost(this.canvas);
        }
        Log.d("StyleController", "drawSimpleLines() end.");
    }

    private void drawStamps(MotionEvent event, Surface surface) {
        Log.d("StyleController", "drawStamps() start.");
        this.holder = surface.getSurfaceHolder();
        this.canvas = this.holder.lockCanvas();
        float x = event.getX();
        float y = event.getY();
        if (this.canvas != null) {
            Log.d("StyleController", "drawStamps() canvas != null");
            switch (event.getAction()) {
                case 0:
                    Log.d("StyleController", "drawStamps() ACTION_DOWN");
                    this.toDraw = true;
                    surface.redrawFlgOn();
                    surface.redrawBitmap.setPixels(surface.pixels, 0, surface.w, 0, 0, surface.w, surface.h);
                    this.editBitmapCanvas = new Canvas(surface.redrawBitmap);
                    this.editBitmapCanvas.setBitmap(surface.redrawBitmap);
                    this.style.strokeStart(this.editBitmapCanvas, x, y);
                    this.canvas.drawBitmap(surface.redrawBitmap, 0.0f, 0.0f, (Paint) null);
                    break;
                case 1:
                    Log.d("StyleController", "drawStamps() ACTION_UP");
                    surface.redrawBitmap.setPixels(surface.pixels, 0, surface.w, 0, 0, surface.w, surface.h);
                    this.editBitmapCanvas = new Canvas(surface.redrawBitmap);
                    this.editBitmapCanvas.setBitmap(surface.redrawBitmap);
                    this.style.stroke(this.editBitmapCanvas, x, y);
                    this.canvas.drawBitmap(surface.redrawBitmap, 0.0f, 0.0f, (Paint) null);
                    surface.editBitmap = surface.redrawBitmap;
                    surface.stackBitmap();
                    surface.redrawFlgOff();
                    break;
                case 2:
                    Log.d("StyleController", "drawStamps() ACTION_MOVE");
                    surface.redrawBitmap.setPixels(surface.pixels, 0, surface.w, 0, 0, surface.w, surface.h);
                    this.editBitmapCanvas = new Canvas(surface.redrawBitmap);
                    this.editBitmapCanvas.setBitmap(surface.redrawBitmap);
                    this.style.stroke(this.editBitmapCanvas, x, y);
                    this.canvas.drawBitmap(surface.redrawBitmap, 0.0f, 0.0f, (Paint) null);
                    break;
            }
            this.holder.unlockCanvasAndPost(this.canvas);
        }
        Log.d("StyleController", "drawStamps() end.");
    }

    private void drawKoroKoroStamps(MotionEvent event, Surface surface) {
        Log.d("StyleController", "drawKoroKoroStamps() start.");
        this.holder = surface.getSurfaceHolder();
        this.canvas = this.holder.lockCanvas();
        float x = event.getX();
        float y = event.getY();
        if (this.canvas != null) {
            Log.d("StyleController", "drawKoroKoroStamps() canvas != null");
            this.editBitmap = surface.getEditBitmap();
            this.editBitmapCanvas = new Canvas(this.editBitmap);
            this.editBitmapCanvas.setBitmap(this.editBitmap);
            switch (event.getAction()) {
                case 0:
                    Log.d("StyleController", "drawKoroKoroStamps() ACTION_DOWN");
                    this.toDraw = true;
                    this.style.strokeStart(this.editBitmapCanvas, x, y);
                    this.canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
                    break;
                case 1:
                    Log.d("StyleController", "drawKoroKoroStamps() ACTION_UP");
                    this.style.stroke(this.editBitmapCanvas, x, y);
                    this.canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
                    surface.stackBitmap();
                    break;
                case 2:
                    Log.d("StyleController", "drawKoroKoroStamps() ACTION_MOVE");
                    this.style.stroke(this.editBitmapCanvas, x, y);
                    this.canvas.drawBitmap(this.editBitmap, 0.0f, 0.0f, (Paint) null);
                    break;
            }
            this.holder.unlockCanvasAndPost(this.canvas);
        }
        Log.d("StyleController", "drawKoroKoroStamps() end.");
    }

    public void clear(Context c) {
        this.toDraw = false;
        StyleFactory.clearCache();
        setStyle(StyleFactory.getCurrentStyle(c));
    }

    public void setPaintColor(int color2) {
        this.color = color2;
        this.style.setColor(color2);
    }

    public int getPaintColor() {
        return this.style.getColor();
    }
}
