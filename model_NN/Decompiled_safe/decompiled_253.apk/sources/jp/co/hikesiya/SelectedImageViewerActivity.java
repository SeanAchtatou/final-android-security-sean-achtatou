package jp.co.hikesiya;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.File;
import jp.co.hikesiya.util.Log;

public class SelectedImageViewerActivity extends Activity {
    private final String TAG = "SelectedImageViewer";
    private Bitmap bmp = null;
    private int id = -1;
    private String path = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.setDebugFlag(getApplicationContext());
        Log.d(getLocalClassName(), "onCreate starts.");
        adjustOrientation();
        Intent intent = getIntent();
        this.id = intent.getIntExtra("IMAGE_ID", -1);
        this.path = intent.getStringExtra("IMAGE_FULLPATH");
        resize(this.path);
        ((ImageView) findViewById(R.id.image_image_img)).setImageBitmap(this.bmp);
    }

    private void adjustOrientation() {
        Configuration config = getResources().getConfiguration();
        if (config.orientation == 2) {
            setContentView((int) R.layout.activity_selected_image_viewer_landscape);
        } else if (config.orientation == 1) {
            setContentView((int) R.layout.activity_selected_image_viewer_portrait);
        }
        ((ImageView) findViewById(R.id.image_image_img)).setImageBitmap(this.bmp);
    }

    public void onClick_ButtonStartEditPicture(View v) {
        startEditPicture(this.path);
    }

    public void onClick_ButtonShare(View v) {
        share_rakugaki(this.path);
    }

    public void onClick_ButtonDelete(View v) {
        Log.d(getLocalClassName(), "onClick_ButtonDelete starts.");
        delete_rakugaki(this.id, this.path);
    }

    public void startEditPicture(String _path) {
        Intent intent = new Intent(this, EditPictureActivity.class);
        intent.putExtra(getString(R.string.selectedFileUri), _path);
        startActivity(intent);
    }

    public void share_rakugaki(String _path) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("image/*");
        intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(_path)));
        startActivity(intent);
    }

    public void delete_rakugaki(final int id2, String path2) {
        Log.d("SelectedImageViewer", "delete_rakugaki() start.");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.alertDeleteFileTitle));
        builder.setMessage(String.valueOf(getString(R.string.alertDeleteFile)) + new File(path2).getName());
        builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SelectedImageViewerActivity.this.startDelete(id2);
                SelectedImageViewerActivity.this.finish();
            }
        });
        builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create();
        builder.show();
        Log.d("SelectedImageViewer", "delete_rakugaki() end.");
    }

    /* access modifiers changed from: private */
    public void startDelete(int _id) {
        Log.d("SelectedImageViewer", "startDelete() start.");
        String id2 = Integer.toString(_id);
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String where = "_id = " + id2;
        try {
            getContentResolver().delete(uri, where, null);
        } catch (UnsupportedOperationException e) {
            Log.d("SelectedImageViewer", e.getMessage());
            getContentResolver().delete(ContentUris.withAppendedId(uri, Long.parseLong(id2)), where, null);
        }
        Intent intent = new Intent(getApplicationContext(), RakugakiGalleryActivity.class);
        intent.setFlags(67108864);
        startActivity(intent);
        finish();
        Toast.makeText(this, getString(R.string.completeDeleting), 0).show();
        Log.d("SelectedImageViewer", "startDelete() end.");
    }

    private void resize(String imageUri) {
        boolean imageOrientation;
        int scale;
        float mImageWidth;
        float mImageHeight;
        float dispScale;
        Log.d(getLocalClassName(), "start resize.");
        boolean resizeFlg = true;
        Resources res = getResources();
        DisplayMetrics metrics = res.getDisplayMetrics();
        int dispW = metrics.widthPixels;
        int dispH = metrics.heightPixels;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDither = true;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        BitmapFactory.decodeFile(imageUri, options);
        if (options.outWidth > options.outHeight) {
            imageOrientation = true;
        } else {
            imageOrientation = false;
        }
        int orientation = res.getConfiguration().orientation;
        if ((imageOrientation && orientation == 1) || (!imageOrientation && orientation == 2)) {
            dispW = metrics.heightPixels;
            dispH = metrics.widthPixels;
        }
        if (options.outWidth <= dispW && options.outHeight <= dispH) {
            resizeFlg = false;
        }
        if (!resizeFlg) {
            scale = 1;
        } else {
            scale = Math.max(options.outWidth / dispW, options.outHeight / dispH);
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = scale;
        Bitmap image = BitmapFactory.decodeFile(imageUri, options);
        if (!resizeFlg) {
            mImageWidth = (float) options.outWidth;
            mImageHeight = (float) options.outHeight;
            dispScale = 1.0f;
        } else {
            mImageWidth = (float) image.getWidth();
            mImageHeight = (float) image.getHeight();
            dispScale = Math.min(((float) dispH) / mImageHeight, ((float) dispW) / mImageWidth);
        }
        float mImageWidth2 = mImageWidth * dispScale;
        float mImageHeight2 = mImageHeight * dispScale;
        if (this.bmp != null) {
            this.bmp.recycle();
        }
        this.bmp = Bitmap.createScaledBitmap(image, (int) mImageWidth2, (int) mImageHeight2, true);
        image.recycle();
        Log.d(getLocalClassName(), "end resize.");
    }

    public void onDestroy() {
        Log.d(getLocalClassName(), "onDestroy is called.");
        super.onDestroy();
        this.bmp.recycle();
        this.bmp = null;
        cleanupView(findViewById(R.id.image_viewer_root));
    }

    public void onConfigurationChanged(Configuration newConfig) {
        Log.d("SelectedImageViewer", "onConfigurationChanged starts.");
        super.onConfigurationChanged(newConfig);
        adjustOrientation();
    }

    private void cleanupView(View view) {
        Log.d(getLocalClassName(), "cleanupView is called.");
        if (view instanceof Button) {
            ((Button) view).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else if (view instanceof ImageView) {
            ((ImageView) view).setImageBitmap(null);
        }
        if (view instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) view;
            int size = vg.getChildCount();
            for (int i = 0; i < size; i++) {
                cleanupView(vg.getChildAt(i));
            }
        }
        Log.d(getLocalClassName(), "cleanupView is end.");
    }
}
