package jp.co.hikesiya;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.picture.style.StyleFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import jp.co.hikesiya.common.Utility;
import jp.co.hikesiya.util.Log;

public class EditPictureActivity extends Activity {
    final int FP = -1;
    final int WC = -2;
    Context c = null;
    /* access modifiers changed from: private */
    public String currentFileName = "";
    private String extension = ".jpeg";
    FrameLayout frameLayout;
    public int imageHeight;
    public int imageWidth;
    LinearLayout layout;
    private List_of_Lines line;
    /* access modifiers changed from: private */
    public ProgressDialog pd;
    private ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public AlertDialog saveAsAlertDialog;
    /* access modifiers changed from: private */
    public AlertDialog saveAsDialog;
    /* access modifiers changed from: private */
    public final Runnable saveCallback = new Runnable() {
        public void run() {
            Log.d("EditPicture", "saveCallback is start.");
            if (EditPictureActivity.this.pd != null && EditPictureActivity.this.pd.isShowing()) {
                EditPictureActivity.this.pd.dismiss();
                EditPictureActivity.this.pd = null;
            }
            if (EditPictureActivity.this.saveAsDialog != null && EditPictureActivity.this.saveAsDialog.isShowing()) {
                EditPictureActivity.this.saveAsDialog.dismiss();
                EditPictureActivity.this.saveAsDialog = null;
            }
            EditPictureActivity.this.displaySaveCompleteDialog();
            Log.d("EditPicture", "saveCallback is end.");
        }
    };
    /* access modifiers changed from: private */
    public final Runnable saveCallbackErr = new Runnable() {
        public void run() {
            Log.d("EditPicture", "saveCallbackErr is start.");
            if (EditPictureActivity.this.pd != null && EditPictureActivity.this.pd.isShowing()) {
                EditPictureActivity.this.pd.dismiss();
                EditPictureActivity.this.pd = null;
            }
            EditPictureActivity.this.displaySaveErrorDialog();
            Log.d("EditPicture", "saveCallbackErr is end.");
        }
    };
    /* access modifiers changed from: private */
    public int saveErrorFlg = 0;
    /* access modifiers changed from: private */
    public String saveFileName = "";
    /* access modifiers changed from: private */
    public Handler save_handler = new Handler();
    private List_of_Stamps stamp;
    public Surface surface;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.setDebugFlag(getApplicationContext());
        Log.d("EditPicture", "onCreate was called.");
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        String imageUri = getIntent().getStringExtra(getString(R.string.selectedFileUri));
        Log.d("EditPicture", "imageUri: " + imageUri);
        try {
            this.surface = new Surface(getApplicationContext(), imageUri);
            setFixedOrientation(Boolean.valueOf(this.surface.getImageOrientation()));
            this.imageWidth = this.surface.getImageWidth();
            this.imageHeight = this.surface.getImageHeight();
            this.frameLayout = new FrameLayout(getApplicationContext());
            this.frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            this.layout = new LinearLayout(getApplicationContext());
            this.layout.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            this.layout.setGravity(17);
            this.frameLayout.addView(this.surface, this.imageWidth, this.imageHeight);
            this.layout.addView(this.frameLayout);
            setContentView(this.layout);
            this.currentFileName = this.surface.getImageTitle();
            this.saveFileName = this.currentFileName;
            this.line = new List_of_Lines(this, this.surface);
            this.stamp = new List_of_Stamps(this, this.surface);
            Toast.makeText(this, getString(R.string.toastPushMenuButton), 1).show();
            Log.d("EditPicture", "onCreate() end.");
        } catch (IOException e) {
            Log.d("EditPicture", "Fails initialization.");
            new RakugakiCameraUtility().showExitAlert(this, R.string.alertCannotUseThisFile, R.string.ok);
        }
    }

    private void undo_onClick() {
        Log.d("EditPicture", "undo_onClick() start.");
        this.surface.undo();
        Log.d("EditPicture", "undo_onClick() end.");
    }

    private void pen_onClick() {
        Log.d("EditPicture", "pen_onClick() start.");
        this.line.pen_onClick();
        Log.d("EditPicture", "pen_onClick() end.");
    }

    private void stamp_onClick() {
        Log.d("EditPicture", "stamp_onClick() start.");
        this.stamp.stamp_onClick();
        Log.d("EditPicture", "stamp_onClick() end.");
    }

    private void clear_onClick() {
        Log.d("EditPicture", "clear_onClick() start.");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.alertClearTitle);
        builder.setMessage((int) R.string.alertClear);
        builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                EditPictureActivity.this.surface.clearBitmap(EditPictureActivity.this.getApplicationContext());
            }
        });
        builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create();
        builder.show();
        Log.d("EditPicture", "clear_onClick() end.");
    }

    private void save_onClick() {
        Log.d("EditPicture", "save_onClick() start.");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(String.valueOf(this.currentFileName) + this.extension);
        builder.setItems(new CharSequence[]{getString(R.string.saveSabMenuSave), getString(R.string.saveSabMenuSaveAs)}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        EditPictureActivity.this.savePicture();
                        return;
                    case 1:
                        EditPictureActivity.this.saveAsPicture();
                        return;
                    default:
                        return;
                }
            }
        });
        builder.create().show();
        Log.d("EditPicture", "save_onClick() end.");
    }

    /* access modifiers changed from: private */
    public void savePicture() {
        Log.d("EditPicture", "savePicture() start.");
        this.pd = new ProgressDialog(this);
        this.pd.setProgressStyle(0);
        this.pd.setMessage(getString(R.string.saveDialogMessage));
        this.pd.setCancelable(false);
        this.pd.show();
        new Thread(new Save(this, null)).start();
        Log.d("EditPicture", "savePicture() end.");
    }

    /* access modifiers changed from: private */
    public void saveAsPicture() {
        Log.d("EditPicture", "saveAsPicture() start.");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogLayout = LayoutInflater.from(this).inflate((int) R.layout.dialog_edit_menu_save_as, (ViewGroup) null);
        ((TextView) dialogLayout.findViewById(R.id.edit_menu_save_as_path)).setText(this.surface.getDirName());
        final EditText fileNameEditText = (EditText) dialogLayout.findViewById(R.id.edit_menu_save_as_filename);
        fileNameEditText.setText(this.currentFileName);
        builder.setTitle((int) R.string.saveSabMenuSaveAs);
        builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                EditPictureActivity.this.saveFileName = fileNameEditText.getText().toString();
                if (!EditPictureActivity.this.checkDuplicationFile(EditPictureActivity.this.saveFileName)) {
                    EditPictureActivity.this.savePicture();
                } else {
                    EditPictureActivity.this.makeSaveAsAlertDialog(EditPictureActivity.this.saveFileName);
                }
            }
        });
        builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                EditPictureActivity.this.saveAsDialog.dismiss();
            }
        });
        this.saveAsDialog = builder.setView(dialogLayout).create();
        this.saveAsDialog.show();
        Log.d("EditPicture", "saveAsPicture() end.");
    }

    /* access modifiers changed from: private */
    public boolean checkDuplicationFile(String fileName) {
        Log.d("EditPicture", "checkDuplicationFile() start.");
        boolean ret = false;
        String fileName2 = String.valueOf(fileName) + this.extension;
        Log.d("EditPicture", "filename is " + fileName2);
        File dir = new File(this.surface.getDirName());
        if (!dir.exists()) {
            Log.d("EditPicture", "Rakugaki dir does not exist.");
            dir.mkdir();
        }
        String[] fileList = dir.list();
        int length = fileList.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            String file = fileList[i];
            Log.d("EditPicture", file);
            if (file.equals(fileName2)) {
                Log.d("EditPicture", "filename is duplication." + fileName2);
                ret = true;
                break;
            }
            i++;
        }
        Log.d("EditPicture", "checkDuplicationFile() end.");
        return ret;
    }

    /* access modifiers changed from: private */
    public void makeSaveAsAlertDialog(String filename) {
        Log.d("EditPicture", "makeSaveAsAlertDialog() start.");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogLayout = LayoutInflater.from(this).inflate((int) R.layout.dialog_edit_menu_save_as_alert, (ViewGroup) null);
        builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                EditPictureActivity.this.savePicture();
                EditPictureActivity.this.saveAsAlertDialog.dismiss();
                EditPictureActivity.this.saveAsDialog.dismiss();
            }
        });
        builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                EditPictureActivity.this.saveAsAlertDialog.dismiss();
            }
        });
        this.saveAsAlertDialog = builder.setView(dialogLayout).create();
        this.saveAsAlertDialog.setTitle((int) R.string.saveSabMenuSaveAs);
        this.saveAsAlertDialog.show();
        Log.d("EditPicture", "makeSaveAsAlertDialog() end.");
    }

    private class Save implements Runnable {
        private Save() {
        }

        /* synthetic */ Save(EditPictureActivity editPictureActivity, Save save) {
            this();
        }

        public void run() {
            try {
                Log.d("EditPicture", "Save worker thread is start.");
                EditPictureActivity.this.surface.saveBitmap(EditPictureActivity.this.getApplicationContext(), EditPictureActivity.this.saveFileName);
                EditPictureActivity.this.save_handler.post(EditPictureActivity.this.saveCallback);
            } catch (FileNotFoundException e) {
                EditPictureActivity.this.saveErrorFlg = 0;
                EditPictureActivity.this.save_handler.post(EditPictureActivity.this.saveCallbackErr);
            } catch (SDCardFullException e2) {
                EditPictureActivity.this.saveErrorFlg = 1;
                EditPictureActivity.this.save_handler.post(EditPictureActivity.this.saveCallbackErr);
            }
            Log.d("EditPicture", "Save worker thread is end.");
        }
    }

    /* access modifiers changed from: private */
    public void displaySaveCompleteDialog() {
        Log.d("EditPicture", "displaySaveCompleteDialog() start.");
        this.surface.writeImageNumber(getApplicationContext());
        this.currentFileName = this.saveFileName;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogLayout = LayoutInflater.from(this).inflate((int) R.layout.dialog_edit_menu_save_comp, (ViewGroup) null);
        ((TextView) dialogLayout.findViewById(R.id.edit_menu_save_comp_path)).setText(this.surface.getDirName());
        ((TextView) dialogLayout.findViewById(R.id.edit_menu_save_comp_file)).setText(String.valueOf(this.saveFileName) + this.extension);
        builder.setTitle((int) R.string.alertSaveTitle);
        builder.setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) null);
        builder.setView(dialogLayout).create().show();
        Log.d("EditPicture", "displaySaveCompleteDialog() end.");
    }

    /* access modifiers changed from: private */
    public void displaySaveErrorDialog() {
        Log.d("EditPicture", "displaySaveErrorDialog() start.");
        RakugakiCameraUtility rcu = new RakugakiCameraUtility();
        if (this.saveErrorFlg == 0) {
            Log.d("EditPicture", "saveErrorFlg is 0.");
            callInvalidFileNameDialog();
        } else if (this.saveErrorFlg == 1) {
            Log.d("EditPicture", "saveErrorFlg is 1.");
            rcu.showExitAlert(this, R.string.alertSDCardFull, R.string.SDCardFull);
        }
        Log.d("EditPicture", "displaySaveErrorDialog() end.");
    }

    private void callInvalidFileNameDialog() {
        Log.d("EditPicture", "callInvalidFileNameDialog() start.");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.alertFileNotFoundTitle);
        builder.setMessage((int) R.string.alertFileNotFound);
        builder.setPositiveButton((int) R.string.okButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                EditPictureActivity.this.saveFileName = EditPictureActivity.this.currentFileName;
            }
        });
        builder.create();
        builder.show();
        Log.d("EditPicture", "callInvalidFileNameDialog() end.");
    }

    public void album_onClick() {
        Log.d("EditPicture", "album_onClick() start.");
        if (this.surface.hasUnsavedOperation()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.alertGoToGalleryTitle);
            builder.setMessage((int) R.string.alertGoToGallery);
            builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    EditPictureActivity.this.startRakugakiGallery();
                }
            });
            builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.create();
            builder.show();
        } else {
            startRakugakiGallery();
        }
        Log.d("EditPicture", "album_onClick() end.");
    }

    /* access modifiers changed from: private */
    public void startRakugakiGallery() {
        Log.d("EditPicture", "startRakugakiGallery() start.");
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setTitle(getString(R.string.progDialogAlbumOpen_Title));
        this.progressDialog.setMessage(getString(R.string.progDialogAlbumOpen_Message));
        this.progressDialog.setIndeterminate(false);
        this.progressDialog.setProgressStyle(0);
        this.progressDialog.show();
        Intent intent = new Intent(this, RakugakiGalleryActivity.class);
        intent.setFlags(67108864);
        new Thread(new AlbumRunnable(intent)).start();
        Log.d("EditPicture", "startRakugakiGallery() end.");
    }

    private class AlbumRunnable implements Runnable {
        private Intent _intent;

        public AlbumRunnable(Intent intent) {
            this._intent = intent;
        }

        public void run() {
            Log.d("AlbumRunnable", "run() start.");
            EditPictureActivity.this.setDefaultStyle();
            EditPictureActivity.this.startActivity(this._intent);
            EditPictureActivity.this.finish();
            Log.d("AlbumRunnable", "run() end.");
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.d("EditPicture", "onPause() start.");
        if (this.pd != null && this.pd.isShowing()) {
            this.pd.dismiss();
            this.pd = null;
        }
        if (!Utility.isNullOrEmpty(this.surface)) {
            this.surface.stopExecutorService();
        }
        Log.d("EditPicture", "onPause() end.");
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        Log.d("EditPicture", "dispatchKeyEvent() start.");
        if (event.getAction() == 0 && event.getKeyCode() == 4) {
            backKey_onClick();
            return false;
        }
        Log.d("EditPicture", "dispatchKeyEvent() end.");
        return super.dispatchKeyEvent(event);
    }

    public void backKey_onClick() {
        Log.d("EditPicture", "backKey_onClick() start.");
        if (this.surface.hasUnsavedOperation()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.alertBackToPreviousPageTitle);
            builder.setMessage((int) R.string.alertBackToPreviousPage);
            builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    EditPictureActivity.this.setDefaultStyle();
                    EditPictureActivity.this.finish();
                }
            });
            builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.create();
            builder.show();
        } else {
            setDefaultStyle();
            finish();
        }
        Log.d("EditPicture", "backKey_onClick() end.");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        Log.d("EditPicture", "onCreateOptionsMenu() start.");
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        Log.d("EditPicture", "onCreateOptionsMenu() end.");
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        super.onMenuItemSelected(featureId, item);
        Log.d("EditPicture", "onMenuItemSelected() start.");
        switch (item.getItemId()) {
            case R.id.item0 /*2131230754*/:
                pen_onClick();
                break;
            case R.id.item1 /*2131230755*/:
                stamp_onClick();
                break;
            case R.id.item2 /*2131230756*/:
                undo_onClick();
                break;
            case R.id.item3 /*2131230757*/:
                clear_onClick();
                break;
            case R.id.item4 /*2131230758*/:
                save_onClick();
                break;
            case R.id.item5 /*2131230759*/:
                album_onClick();
                break;
        }
        Log.d("EditPicture", "onMenuItemSelected() end.");
        return true;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        Log.d("EditPicture", "onConfigurationChanged() is called.");
        super.onConfigurationChanged(newConfig);
    }

    public void setFixedOrientation(Boolean flg) {
        Log.d("EditPicture", "setFixedOrientation() is called.");
        if (flg.booleanValue()) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(1);
        }
    }

    public void onStop() {
        Log.d("EditPicture", "onStop() is called.");
        super.onStop();
        if (!Utility.isNullOrEmpty(this.progressDialog)) {
            Log.d("EditPicture", "progressDialog dismiss;");
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }

    public void onDestroy() {
        Log.d("EditPicture", "onDestroy() is called.");
        if (!Utility.isNullOrEmpty(this.surface)) {
            this.surface.bitmap.recycle();
            this.surface.editBitmap.recycle();
            this.surface.redrawBitmap.recycle();
            Iterator<Bitmap> it = this.surface.editBitmapArray.iterator();
            while (it.hasNext()) {
                it.next().recycle();
            }
            this.surface = null;
        }
        super.onDestroy();
    }

    public void onRestart() {
        Log.d("EditPicture", "onRestart() is called.");
        super.onRestart();
    }

    public void onStart() {
        Log.d("EditPicture", "onStart() is called.");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.d("EditPicture", "onResume() is called.");
        if (!Utility.isNullOrEmpty(this.surface)) {
            this.surface.drawOnResume();
        }
        super.onResume();
    }

    /* access modifiers changed from: private */
    public void setDefaultStyle() {
        Log.d("EditPicture", "setDefaultStyle() start.");
        this.surface.setStyle(StyleFactory.getStyle(getApplicationContext(), StyleFactory.SIMPLE_02));
        Log.d("EditPicture", "setDefaultStyle() end.");
    }
}
