package jp.co.hikesiya;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import jp.co.hikesiya.util.Log;
import jp.co.nobot.libYieldMaker.libYieldMaker;

public class RakugakiGalleryActivity extends Activity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, AbsListView.OnScrollListener {
    /* access modifiers changed from: private */
    public ArrayList<Integer> IDs = new ArrayList<>();
    private final String TAG = "RakugakiGallery";
    private ImageAdapter adapter;
    /* access modifiers changed from: private */
    public Bitmap bitmap = null;
    /* access modifiers changed from: private */
    public ContentResolver cr;
    private int id;
    /* access modifiers changed from: private */
    public boolean isScrolling = false;
    /* access modifiers changed from: private */
    public String path;
    private ArrayList<String> pathes = new ArrayList<>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("RakugakiGallery", "onCreate start.");
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_rakugaki_gallery);
        Log.setDebugFlag(getApplicationContext());
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        LinearLayout.LayoutParams mvParams = new LinearLayout.LayoutParams(-1, -2);
        mvParams.height = (int) Math.ceil((double) (50.0f * metrics.density));
        mvParams.weight = 0.0f;
        mvParams.width = (int) Math.ceil((double) (320.0f * metrics.density));
        mvParams.gravity = 80;
        mvParams.bottomMargin = (int) Math.ceil((double) (10.0f * metrics.density));
        libYieldMaker mv = new libYieldMaker(this);
        mv.setLayoutParams(mvParams);
        mv.setActivity(this);
        mv.setUrl("http://images.ad-maker.info/apps/sjocu5a9vtpk.html");
        mv.setBackgroundColor(0);
        mv.startView();
        ((LinearLayout) findViewById(R.id.advLayer)).addView(mv);
        this.cr = getContentResolver();
        getRakugakiImageIDs();
        this.adapter = new ImageAdapter();
        GridView gv = (GridView) findViewById(R.id.image_grid);
        gv.setOnItemClickListener(this);
        gv.setOnItemLongClickListener(this);
        gv.setOnScrollListener(this);
        gv.setAdapter((ListAdapter) this.adapter);
    }

    public void onStart() {
        super.onStart();
        Log.d("RakugakiGallery", "onStart start.");
    }

    public void onResume() {
        super.onResume();
        Log.d("RakugakiGallery", "onResume start.");
    }

    public void onRestart() {
        super.onRestart();
        Log.d("RakugakiGallery", "onRestart start.");
        this.IDs.clear();
        this.pathes.clear();
        getRakugakiImageIDs();
        this.adapter = new ImageAdapter();
        GridView gv = (GridView) findViewById(R.id.image_grid);
        gv.setOnItemClickListener(this);
        gv.setOnItemLongClickListener(this);
        gv.setOnScrollListener(this);
        gv.setAdapter((ListAdapter) this.adapter);
        Log.d("RakugakiGallery", "onRestart end.");
    }

    private void getRakugakiImageIDs() {
        Cursor c = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data", "_size", "mime_type", "date_modified"}, "_size > 0 AND _data LIKE '%/Rakugaki/%' AND mime_type LIKE 'image/%'", null, "date_modified DESC");
        Log.d(getLocalClassName(), "DISPLAYING IMAGES  = " + c.getCount());
        c.moveToFirst();
        if (c.getCount() == 0) {
            showAlert(getString(R.string.alertRakugakiDirDoesNotExist));
        }
        for (int k = 0; k < c.getCount(); k++) {
            try {
                this.IDs.add(Integer.valueOf(c.getInt(c.getColumnIndexOrThrow("_id"))));
                this.pathes.add(c.getString(c.getColumnIndexOrThrow("_data")));
            } catch (IllegalArgumentException e) {
                Log.d(getLocalClassName(), "Column does not exist.");
            }
            c.moveToNext();
        }
    }

    private void showAlert(String message) {
        Log.d("GridViewActivity", "showAlertWithoutChoice() starts.");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.title);
        builder.setMessage(message);
        builder.setNeutralButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RakugakiGalleryActivity.this.finish();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                RakugakiGalleryActivity.this.finish();
            }
        });
        builder.create();
        builder.show();
    }

    private class ImageAdapter extends BaseAdapter {
        private final LayoutInflater mInflater;

        public ImageAdapter() {
            Log.d(RakugakiGalleryActivity.this.getLocalClassName(), "constructor starts.");
            this.mInflater = (LayoutInflater) RakugakiGalleryActivity.this.getApplicationContext().getSystemService("layout_inflater");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView rowView;
            Log.d(RakugakiGalleryActivity.this.getLocalClassName(), "getView starts.");
            Log.d(RakugakiGalleryActivity.this.getLocalClassName(), "position: " + position);
            if (convertView == null) {
                Log.d(RakugakiGalleryActivity.this.getLocalClassName(), "convertView is null.");
            } else {
                Log.d(RakugakiGalleryActivity.this.getLocalClassName(), "convertView is not null.");
                Log.d(RakugakiGalleryActivity.this.getLocalClassName(), "ID of convertView: " + convertView.getId());
            }
            int id = ((Integer) RakugakiGalleryActivity.this.IDs.get(position)).intValue();
            if (convertView == null) {
                rowView = (ImageView) this.mInflater.inflate((int) R.layout.rakugaki_gallery_rowdata, parent, false);
                rowView.setId(position);
            } else {
                rowView = (ImageView) convertView;
            }
            if (!RakugakiGalleryActivity.this.isScrolling) {
                Log.d("RakugakiGallery", "!isScrolling");
                RakugakiGalleryActivity.this.bitmap = ThumbnailUtil.getThumbnailBitmap(RakugakiGalleryActivity.this.cr, (long) id, null, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                rowView.setImageBitmap(RakugakiGalleryActivity.this.bitmap);
                rowView.setTag(null);
            } else {
                Log.d("RakugakiGallery", "isScrolling");
                rowView.setImageResource(R.drawable.album_loading_back);
                rowView.setTag(Integer.valueOf(position));
            }
            Log.d("RakugakiGallery", "id: " + rowView.getId());
            Log.d("RakugakiGallery", "getView ends.");
            return rowView;
        }

        public int getCount() {
            return RakugakiGalleryActivity.this.IDs.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long viewID) {
        Log.d("RakugakiGallery", "onItemClick start.");
        this.id = this.IDs.get(position).intValue();
        this.path = this.pathes.get(position);
        if (canUseThisFile(this.path)) {
            Intent intent = new Intent(this, SelectedImageViewerActivity.class);
            intent.putExtra("IMAGE_FULLPATH", this.path);
            intent.putExtra("IMAGE_ID", this.id);
            startActivity(intent);
            Log.d("RakugakiGallery", "id: " + this.id);
            Log.d("RakugakiGallery", "path: " + this.path);
            Log.d("RakugakiGallery", "onItemClick end.");
        }
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long viewID) {
        this.path = this.pathes.get(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.galleryMenu_Title));
        builder.setItems(new String[]{getString(R.string.galleryMenu_Rakugaki), getString(R.string.galleryMenu_Share), getString(R.string.galleryMenu_Delete)}, new DialogInterface.OnClickListener() {
            private final int MENU_DELETE = 2;
            private final int MENU_EDIT = 0;
            private final int MENU_SHARE = 1;

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        RakugakiGalleryActivity.this.startEditPicture(RakugakiGalleryActivity.this.path);
                        return;
                    case 1:
                        RakugakiGalleryActivity.this.startShare(RakugakiGalleryActivity.this.path);
                        return;
                    case 2:
                        RakugakiGalleryActivity.this.showDeleteDialog(position, RakugakiGalleryActivity.this.path);
                        return;
                    default:
                        return;
                }
            }
        });
        builder.setNeutralButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create();
        builder.show();
        return true;
    }

    /* access modifiers changed from: private */
    public void startEditPicture(String path2) {
        if (canUseThisFile(path2)) {
            Intent intent = new Intent(this, EditPictureActivity.class);
            intent.putExtra(getString(R.string.selectedFileUri), path2);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void startShare(String path2) {
        if (canUseThisFile(path2)) {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("image/*");
            intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(path2)));
            startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void showDeleteDialog(final int position, String path2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.alertDeleteFileTitle));
        builder.setMessage(String.valueOf(getString(R.string.alertDeleteFile)) + new File(path2).getName());
        builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RakugakiGalleryActivity.this.startDelete(position);
                RakugakiGalleryActivity.this.finish();
            }
        });
        builder.setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create();
        builder.show();
    }

    /* access modifiers changed from: private */
    public void startDelete(int position) {
        String id2 = String.valueOf(this.IDs.get(position));
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String where = "_id = " + id2;
        try {
            getContentResolver().delete(uri, where, null);
        } catch (UnsupportedOperationException e) {
            Log.d("RakugakiGallery", e.getMessage());
            getContentResolver().delete(ContentUris.withAppendedId(uri, Long.parseLong(id2)), where, null);
        }
        Intent intent = new Intent(getApplicationContext(), RakugakiGalleryActivity.class);
        intent.setFlags(67108864);
        startActivity(intent);
        Toast.makeText(this, getString(R.string.completeDeleting), 0).show();
    }

    private boolean canUseThisFile(String mPath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inDither = true;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        BitmapFactory.decodeFile(mPath, options);
        int height = options.outHeight;
        int width = options.outWidth;
        Log.d("RakugakiGallery", "height: " + height);
        Log.d("RakugakiGallery", "width: " + width);
        if (height == 0 || width == 0) {
            Log.d("RakugakiGallery", "Bitmap is null.");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.title);
            builder.setMessage((int) R.string.alertCannotUseThisFile);
            builder.setNeutralButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.create();
            builder.show();
            return false;
        }
        Log.d("RakugakiGallery", "Bitmap is not null.");
        return true;
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Log.d(getLocalClassName(), "onScrollStateChanged start.");
        switch (scrollState) {
            case 0:
                Log.d(getLocalClassName(), "SCROLL_STATE_IDLE.");
                this.isScrolling = false;
                int first = view.getFirstVisiblePosition();
                int count = view.getChildCount();
                for (int i = 0; i < count; i++) {
                    ImageView iView = (ImageView) view.getChildAt(i);
                    int position = first + i;
                    Log.d(getLocalClassName(), "id: " + iView.getId());
                    if (iView.getTag() != null) {
                        this.bitmap = ThumbnailUtil.getThumbnailBitmap(this.cr, (long) this.IDs.get(position).intValue(), null, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        iView.setImageBitmap(this.bitmap);
                        iView.setTag(null);
                    }
                }
                break;
            case 1:
                Log.d(getLocalClassName(), "SCROLL_STATE_TOUCH_SCROLL.");
                this.isScrolling = true;
                break;
            case 2:
                Log.d(getLocalClassName(), "SCROLL_STATE_FLING.");
                this.isScrolling = true;
                break;
        }
        Log.d(getLocalClassName(), "onScrollStateChanged end.");
    }
}
