package jp.co.hikesiya;

import android.graphics.Canvas;

public interface Style {
    void draw(Canvas canvas);

    int getColor();

    void setColor(int i);

    void stroke(Canvas canvas, float f, float f2);

    void strokeStart(Canvas canvas, float f, float f2);
}
