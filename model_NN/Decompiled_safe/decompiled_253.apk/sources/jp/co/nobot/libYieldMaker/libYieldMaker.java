package jp.co.nobot.libYieldMaker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class libYieldMaker extends WebView {
    /* access modifiers changed from: private */
    public Activity activity = null;
    private String url;

    public void setActivity(Activity act) {
        this.activity = act;
    }

    public libYieldMaker(Context context) {
        super(context);
    }

    public libYieldMaker(Context context, AttributeSet attributes) {
        super(context, attributes);
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    /* access modifiers changed from: private */
    public boolean isNetworkConnected() {
        NetworkInfo ni = ((ConnectivityManager) this.activity.getSystemService("connectivity")).getActiveNetworkInfo();
        if (ni == null || !ni.isConnected()) {
            setVisibility(8);
            return false;
        }
        setVisibility(0);
        return true;
    }

    public void startView() {
        if (isNetworkConnected()) {
            super.getSettings().setJavaScriptEnabled(true);
            super.clearCache(true);
            super.loadUrl(this.url);
            super.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    try {
                        view.stopLoading();
                        libYieldMaker.this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return true;
                    }
                }

                public void onLoadResource(WebView view, String url) {
                    int i;
                    if (libYieldMaker.this.isNetworkConnected() && (i = url.indexOf("www/delivery/ck.php?oaparams")) != -1 && i < 35) {
                        try {
                            view.stopLoading();
                            libYieldMaker.this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }
}
