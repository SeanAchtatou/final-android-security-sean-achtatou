package sina_weibo;

import android.text.TextUtils;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.net.AsyncWeiboRunner;
import com.weibo.sdk.android.net.RequestListener;

public class SinaWeiboAPI {
    public static final String API_SERVER = "https://api.weibo.com/2";
    public static final String HTTPMETHOD_GET = "GET";
    public static final String HTTPMETHOD_POST = "POST";
    private static final String URL_ACCOUNT = "https://api.weibo.com/2/account";
    private static final String URL_STATUSES = "https://api.weibo.com/2/statuses";
    private static final String URL_USERS = "https://api.weibo.com/2/users";
    private String accessToken;
    private Oauth2AccessToken oAuth2accessToken;

    public SinaWeiboAPI(Oauth2AccessToken oauth2AccessToken) {
        this.oAuth2accessToken = oauth2AccessToken;
        if (this.oAuth2accessToken != null) {
            this.accessToken = this.oAuth2accessToken.getToken();
        }
    }

    private void request(String url, WeiboParameters params, String httpMethod, RequestListener listener) {
        params.add("access_token", this.accessToken);
        AsyncWeiboRunner.request(url, params, httpMethod, listener);
    }

    public void show(long uid, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        request(WeiboShareDao.main_sina_weibo, params, "GET", listener);
    }

    public void update(String content, String lat, String lon, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("status", content);
        if (!TextUtils.isEmpty(lon)) {
            params.add("long", lon);
        }
        if (!TextUtils.isEmpty(lat)) {
            params.add("lat", lat);
        }
        request("https://api.weibo.com/2/statuses/update.json", params, "POST", listener);
    }

    public void upload(String content, String file, String lat, String lon, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("status", content);
        params.add("pic", file);
        if (!TextUtils.isEmpty(lon)) {
            params.add("long", lon);
        }
        if (!TextUtils.isEmpty(lat)) {
            params.add("lat", lat);
        }
        request("https://api.weibo.com/2/statuses/upload.json", params, "POST", listener);
    }

    public void endSession(RequestListener listener) {
        request("https://api.weibo.com/2/account/end_session.json", new WeiboParameters(), "POST", listener);
    }
}
