package sina_weibo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.loadimage.Utils;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.Weibo;
import com.weibo.sdk.android.WeiboAuthListener;
import com.weibo.sdk.android.WeiboDialogError;
import com.weibo.sdk.android.WeiboException;
import com.weibo.sdk.android.net.RequestListener;
import com.weibo.sdk.android.sso.SsoHandler;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class SinaWeiboUtil {
    private static final String TAG = "SinaWeiboUtil";
    /* access modifiers changed from: private */
    public static Oauth2AccessToken mAccessToken;
    private static Context mContext;
    private static SinaWeiboUtil mInstantce;
    private static SsoHandler mSsoHandler;
    private static Weibo mWeibo;
    /* access modifiers changed from: private */
    public WeiboListener listener;

    public SinaWeiboUtil() {
        mWeibo = Weibo.getInstance(Constants.SINA_APP_KEY, Constants.SINA_REDIRECT_URL);
    }

    public static SinaWeiboUtil getInstance(Context context) {
        mContext = context;
        if (mInstantce == null) {
            mInstantce = new SinaWeiboUtil();
        }
        return mInstantce;
    }

    public boolean initSinaWeibo() {
        String token = PerfHelper.getStringData(Constants.PREF_SINA_ACCESS_TOKEN);
        String expiresTime = PerfHelper.getStringData(Constants.PREF_SINA_EXPIRES_TIME);
        String uid = PerfHelper.getStringData(Constants.PREF_SINA_UID);
        String userName = PerfHelper.getStringData(Constants.PREF_SINA_USER_NAME);
        String remindIn = PerfHelper.getStringData(Constants.PREF_SINA_REMIND_IN);
        mAccessToken = new Oauth2AccessToken(token, expiresTime);
        LOG.cstdr(TAG, "accessToken = " + mAccessToken);
        if (ShareApplication.debug) {
            System.out.println("TOKEN值：" + token);
            System.out.println("expiresTime值：" + expiresTime);
            System.out.println("授权是否过期：" + mAccessToken.isSessionValid());
        }
        if (mAccessToken.isSessionValid()) {
            LOG.cstdr(TAG, "access_token 仍在有效期内,无需再次登录: \naccess_token:" + mAccessToken.getToken() + "\n有效期：" + new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(new Date(mAccessToken.getExpiresTime())) + "\nuid:" + uid + "\nuserName:" + userName + "\nremindIn:" + remindIn);
            return true;
        }
        LOG.cstdr(TAG, "使用SSO登录前，请检查手机上是否已经安装新浪微博客户端，目前仅3.0.0及以上微博客户端版本支持SSO；如果未安装，将自动转为Oauth2.0进行认证");
        PerfHelper.setInfo(Constants.PREF_SINA_ACCESS_TOKEN, "");
        PerfHelper.setInfo(Constants.PREF_SINA_UID, "");
        PerfHelper.setInfo(Constants.PREF_SINA_EXPIRES_TIME, 0);
        PerfHelper.setInfo(Constants.PREF_SINA_REMIND_IN, "");
        PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME, "");
        PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME_IMG, "");
        return false;
    }

    public void auth(WeiboListener l) {
        mSsoHandler = new SsoHandler((Activity) mContext, mWeibo);
        mSsoHandler.authorize(new AuthDialogListener());
        this.listener = l;
    }

    class AuthDialogListener implements WeiboAuthListener {
        AuthDialogListener() {
        }

        public void onCancel() {
            LOG.cstdr(SinaWeiboUtil.TAG, "===================AuthDialogListener=Auth cancel==========");
            Utils.showToast("取消授权操作。");
        }

        public void onComplete(Bundle values) {
            LOG.cstdr(SinaWeiboUtil.TAG, "===================AuthDialogListener=onComplete==========");
            for (String key : values.keySet()) {
                LOG.cstdr(SinaWeiboUtil.TAG, "values:key = " + key + " value = " + values.getString(key));
            }
            String token = values.getString("access_token");
            String uid = values.getString(Constants.SINA_UID);
            String userName = values.getString(Constants.SINA_USER_NAME);
            String expiresIn = values.getString("expires_in");
            String remindIn = values.getString(Constants.SINA_REMIND_IN);
            SinaWeiboUtil.mAccessToken = new Oauth2AccessToken(token, expiresIn);
            if (SinaWeiboUtil.mAccessToken.isSessionValid()) {
                PerfHelper.setInfo(Constants.PREF_SINA_ACCESS_TOKEN, token);
                PerfHelper.setInfo(Constants.PREF_SINA_UID, uid);
                PerfHelper.setInfo(Constants.PREF_SINA_EXPIRES_TIME, expiresIn);
                PerfHelper.setInfo(Constants.PREF_SINA_REMIND_IN, remindIn);
                PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME, userName);
                SinaWeiboUtil.this.show(Long.parseLong(uid));
                LOG.cstdr(SinaWeiboUtil.TAG, "isSessionValid~~~~~~~token = " + token + " uid = " + uid + " userName = " + userName + " expiresIn = " + expiresIn + " remindIn = " + remindIn);
            }
        }

        public void onError(WeiboDialogError e) {
            LOG.cstdr(SinaWeiboUtil.TAG, "===================AuthDialogListener=onError=WeiboDialogError = " + e.getMessage());
            Utils.showToast("分享失败，请检查网络连接。出错信息：" + e.getMessage());
        }

        public void onWeiboException(WeiboException e) {
            LOG.cstdr(SinaWeiboUtil.TAG, "===================AuthDialogListener=onWeiboException=WeiboException = " + e.getMessage());
            Utils.showToast("分享失败，请检查网络连接。出错信息：" + e.getMessage());
        }
    }

    public void authCallBack(int requestCode, int resultCode, Intent data) {
        if (mSsoHandler != null) {
            LOG.cstdr(TAG, "=====onActivityResult=mSsoHandler resultCode = " + resultCode + " requestCode = " + requestCode);
            mSsoHandler.authorizeCallBack(requestCode, resultCode, data);
        }
    }

    public void show(long uid) {
        new SinaWeiboAPI(mAccessToken).show(uid, new RequestListener() {
            public void onIOException(IOException e) {
                LOG.cstdr(SinaWeiboUtil.TAG, "onIOException---e = " + e.getMessage());
                Utils.showToast("分享失败，请检查网络连接。出错信息：" + e.getMessage());
            }

            public void onError(WeiboException e) {
                LOG.cstdr(SinaWeiboUtil.TAG, "WeiboException---e = " + e.getMessage());
                Utils.showToast("分享失败，请检查网络连接。出错信息：" + e.getMessage());
            }

            public void onComplete(String json) {
                try {
                    JSONObject object = new JSONObject(json);
                    String userName = object.optString(Constants.SINA_NAME);
                    String userNameimg = object.optString(Constants.SINA_USER_IMG);
                    LOG.cstdr(SinaWeiboUtil.TAG, "show---onComplete---userName = " + userName);
                    PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME, userName);
                    PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME_IMG, userNameimg);
                    if (SinaWeiboUtil.this.listener != null) {
                        SinaWeiboUtil.this.listener.onResult();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void update(String content, String lat, String lon, final Handler mhandler) {
        new SinaWeiboAPI(mAccessToken).update(content, lat, lon, new RequestListener() {
            public void onIOException(IOException e) {
                LOG.cstdr(SinaWeiboUtil.TAG, "onIOException---e = " + e.getMessage());
                Utils.showToast("分享失败，出错信息：" + e.getMessage());
                mhandler.sendEmptyMessage(FinalVariable.remove_footer);
            }

            public void onError(WeiboException e) {
                LOG.cstdr(SinaWeiboUtil.TAG, "onError---e = " + e.getMessage() + " e.getStatusCode() = " + e.getStatusCode());
                mhandler.sendEmptyMessage(FinalVariable.remove_footer);
                if (e.getStatusCode() == 400) {
                    Utils.showToast("分享失败，相同内容短时间内不能分享，请稍候再试吧。出错信息：" + e.getMessage());
                } else {
                    Utils.showToast("分享失败，请检查网络连接。出错信息：" + e.getMessage());
                }
            }

            public void onComplete(String str) {
                mhandler.sendEmptyMessage(FinalVariable.remove_footer);
                LOG.cstdr(SinaWeiboUtil.TAG, "onComplete---str = " + str);
                Utils.showToast("分享成功，去你绑定的新浪微博看看吧！");
            }
        });
    }

    public void upload(String content, String file, String lat, String lon, final Handler mhandler) {
        new SinaWeiboAPI(mAccessToken).upload(content, file, lat, lon, new RequestListener() {
            public void onIOException(IOException e) {
                mhandler.sendEmptyMessage(FinalVariable.remove_footer);
                LOG.cstdr(SinaWeiboUtil.TAG, "onIOException---e = " + e.getMessage());
                Utils.showToast("分享失败，请检查网络连接。出错信息：" + e.getMessage());
            }

            public void onError(WeiboException e) {
                LOG.cstdr(SinaWeiboUtil.TAG, "onError---e = " + e.getMessage() + " e.getStatusCode() = " + e.getStatusCode());
                mhandler.sendEmptyMessage(FinalVariable.remove_footer);
                if (e.getStatusCode() == 400) {
                    Utils.showToast("分享失败，相同内容短时间内不能分享，请稍候再试吧。出错信息：" + e.getMessage());
                } else {
                    Utils.showToast("分享失败，请检查网络连接。出错信息：" + e.getMessage());
                }
            }

            public void onComplete(String str) {
                mhandler.sendEmptyMessage(FinalVariable.remove_footer);
                LOG.cstdr(SinaWeiboUtil.TAG, "onComplete---str = " + str);
                Utils.showToast("分享成功，去你绑定的新浪微博看看吧！");
            }
        });
    }

    public void logout(WeiboListener l) {
        PerfHelper.setInfo(Constants.PREF_SINA_ACCESS_TOKEN, "");
        PerfHelper.setInfo(Constants.PREF_SINA_UID, "");
        PerfHelper.setInfo(Constants.PREF_SINA_EXPIRES_TIME, "");
        PerfHelper.setInfo(Constants.PREF_SINA_REMIND_IN, "");
        PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME, "");
        PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME_IMG, "");
    }

    public boolean isAuth() {
        if (TextUtils.isEmpty(PerfHelper.getStringData(Constants.PREF_SINA_ACCESS_TOKEN))) {
            return false;
        }
        return true;
    }
}
