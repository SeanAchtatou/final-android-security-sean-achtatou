package sina_weibo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.wb.R;
import tengxun_weibo.TencentWeiboUtil;

public class MainActivity extends Activity {
    private static final String TAG = "MainActivity";
    private static Handler mHandler;
    private Button mBtnAddSina;
    private Button mBtnAddTX;
    private Button mBtnAuthSina;
    private Button mBtnAuthTX;
    private Button mBtnLogoutSina;
    private Button mBtnLogoutTX;
    /* access modifiers changed from: private */
    public Context mContext = null;
    private TextView mTvAuthSina;
    /* access modifiers changed from: private */
    public TextView mTvAuthTX;
    private TextView mTvTokenSina;
    /* access modifiers changed from: private */
    public TextView mTvTokenTX;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.i(TAG, "============onCreate==================");
        this.mContext = this;
        initView();
        refreshView();
    }

    private void initView() {
        initTvAuthTX();
        initTvTokenTX();
        initBtnAuthTX();
        initBtnAddTX();
        initBtnLogoutTX();
        initTvAuthSina();
        initTvTokenSina();
        initBtnAuthSina();
        initBtnAddSina();
        initBtnLogoutSina();
    }

    private void initBtnLogoutTX() {
        this.mBtnLogoutTX = (Button) findViewById(R.id.btn_logout_tx);
        this.mBtnLogoutTX.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TencentWeiboUtil.getInstance(MainActivity.this.mContext).logout(new WeiboListener() {
                    public void onResult() {
                        MainActivity.this.refreshView();
                    }
                });
            }
        });
    }

    private void initBtnAddTX() {
        this.mBtnAddTX = (Button) findViewById(R.id.btn_add_tx);
        this.mBtnAddTX.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }

    private void initBtnAuthTX() {
        this.mBtnAuthTX = (Button) findViewById(R.id.btn_auth_tx);
        this.mBtnAuthTX.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }

    private void initTvTokenTX() {
        this.mTvTokenTX = (TextView) findViewById(R.id.tv_token_tx);
    }

    private void initTvAuthTX() {
        this.mTvAuthTX = (TextView) findViewById(R.id.tv_auth_tx);
    }

    private void initBtnLogoutSina() {
        this.mBtnLogoutSina = (Button) findViewById(R.id.btn_logout_sina);
        this.mBtnLogoutSina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SinaWeiboUtil.getInstance(MainActivity.this.mContext).logout(new WeiboListener() {
                    public void onResult() {
                        MainActivity.this.refreshView();
                    }
                });
            }
        });
    }

    private void initBtnAddSina() {
        this.mBtnAddSina = (Button) findViewById(R.id.btn_add_sina);
        this.mBtnAddSina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }

    private void initBtnAuthSina() {
        this.mBtnAuthSina = (Button) findViewById(R.id.btn_auth_sina);
        this.mBtnAuthSina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }

    private void initTvTokenSina() {
        this.mTvTokenSina = (TextView) findViewById(R.id.tv_token_sina);
    }

    private void initTvAuthSina() {
        this.mTvAuthSina = (TextView) findViewById(R.id.tv_auth_sina);
    }

    /* access modifiers changed from: private */
    public void refreshView() {
        String sinaToken = PreferenceUtil.getInstance(this.mContext).getString(Constants.PREF_SINA_ACCESS_TOKEN, "");
        String tencentToken = PreferenceUtil.getInstance(this.mContext).getString(Constants.PREF_TX_ACCESS_TOKEN, "");
        LOG.cstdr("refreshUserView", "sinaToken = " + sinaToken + "   tencentToken = " + tencentToken);
        if (TextUtils.isEmpty(sinaToken) && TextUtils.isEmpty(tencentToken)) {
            this.mTvAuthSina.setText("未授权");
            this.mTvAuthTX.setText("未授权");
            ShareApplication.hander_other.post(new Runnable() {
                public void run() {
                }
            });
        } else if (TextUtils.isEmpty(sinaToken)) {
            TencentWeiboUtil.getInstance(this.mContext).initTencentWeibo(new WeiboListener() {
                public void init(boolean isValid) {
                    LOG.cstdr("txtxt~~~~~~~~~~~~isValid = ", new StringBuilder(String.valueOf(isValid)).toString());
                    if (isValid) {
                        MainActivity.this.mTvAuthTX.setText("腾讯微博已授权");
                        String token = PreferenceUtil.getInstance(MainActivity.this.mContext).getString(Constants.PREF_TX_ACCESS_TOKEN, "");
                        String openId = PreferenceUtil.getInstance(MainActivity.this.mContext).getString(Constants.PREF_TX_OPEN_ID, "");
                        String name = PreferenceUtil.getInstance(MainActivity.this.mContext).getString(Constants.PREF_TX_NAME, "");
                        String expiresTime = PreferenceUtil.getInstance(MainActivity.this.mContext).getString(Constants.PREF_TX_EXPIRES_TIME, "");
                        MainActivity.this.mTvTokenTX.setText("token = " + token + "\nopenId = " + openId + "\nname = " + name + "\nexpiresTime = " + expiresTime + "\nclientIp = " + PreferenceUtil.getInstance(MainActivity.this.mContext).getString(Constants.PREF_TX_CLIENT_IP, ""));
                        ShareApplication.hander_other.post(new Runnable() {
                            public void run() {
                            }
                        });
                        return;
                    }
                    MainActivity.this.mTvAuthTX.setText("授权已过期");
                    ShareApplication.hander_other.post(new Runnable() {
                        public void run() {
                        }
                    });
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LOG.cstdr(TAG, "===================onActivityResult===========requestCode = " + requestCode);
        if (requestCode == 32973) {
            SinaWeiboUtil.getInstance(this.mContext).authCallBack(requestCode, resultCode, data);
        } else if (requestCode == 1) {
            TencentWeiboUtil.getInstance(this.mContext).webAuthOnResult();
        }
    }
}
