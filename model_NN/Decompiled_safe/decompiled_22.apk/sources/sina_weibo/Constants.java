package sina_weibo;

import com.palmtrends.app.ShareApplication;
import com.palmtrends.wb.R;

public interface Constants {
    public static final String PREF_SINA_ACCESS_TOKEN = "SINA_ACCESS_TOKEN";
    public static final String PREF_SINA_EXPIRES_TIME = "SINA_EXPIRES_TIME";
    public static final String PREF_SINA_REMIND_IN = "SINA_REMIND_IN";
    public static final String PREF_SINA_UID = "SINA_UID";
    public static final String PREF_SINA_USER_NAME = "SINA_USER_NAME";
    public static final String PREF_SINA_USER_NAME_IMG = "SINA_USER_NAME_IMG";
    public static final String PREF_TX_ACCESS_TOKEN = "TX_ACCESS_TOKEN";
    public static final String PREF_TX_CLIENT_ID = "TX_CLIENT_ID";
    public static final String PREF_TX_CLIENT_IP = "TX_CLIENT_IP";
    public static final String PREF_TX_EXPIRES_IN = "TX_EXPIRES_IN";
    public static final String PREF_TX_EXPIRES_TIME = "TX_EXPIRES_TIME";
    public static final String PREF_TX_NAME = "TX_NAME";
    public static final String PREF_TX_OPEN_ID = "TX_OPEN_ID";
    public static final String PREF_TX_OPEN_KEY = "TX_OPEN_KEY";
    public static final String PREF_TX_REFRESH_TOKEN = "TX_REFRESH_TOKEN";
    public static final String PREF_TX_UID = "TX_UID";
    public static final String PREF_TX_USER_NAME_IMG = "TX_USER_NAME_IMG";
    public static final String SINA_ACCESS_TOKEN = "access_token";
    public static final String SINA_APP_KEY = "1943145236";
    public static final String SINA_APP_SECRET = "34b5e387de93c7e60ed6b038dbe012ea";
    public static final String SINA_BASEURL = "https://api.weibo.com/oauth2/";
    public static final String SINA_CLIENT_ID = "client_id";
    public static final String SINA_CLIENT_SECRET = "client_secret";
    public static final String SINA_CODE = "code";
    public static final String SINA_DATE_PATTERN = "yyyy/MM/dd HH:mm:ss";
    public static final String SINA_EXPIRES_IN = "expires_in";
    public static final String SINA_GRANT_TYPE = "grant_type";
    public static final String SINA_GRANT_TYPE_VALUE = "authorization_code";
    public static final String SINA_NAME = "name";
    public static final String SINA_REDIRECT_URI = "redirect_uri";
    public static final String SINA_REDIRECT_URL = "http://www.pymob.cn";
    public static final String SINA_REMIND_IN = "remind_in";
    public static final String SINA_SCOPE = "email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,follow_app_official_microblog";
    public static final String SINA_UID = "uid";
    public static final String SINA_USER_IMG = "profile_image_url";
    public static final String SINA_USER_NAME = "userName";
    public static final String TX_API_ACCESS_TOKEN = "access_token";
    public static final String TX_API_APP_KEY = "oauth_consumer_key";
    public static final String TX_API_CLIENT_IP = "clientip";
    public static final String TX_API_COMPATIBLEFLAG = "compatibleflag";
    public static final String TX_API_CONTENT = "content";
    public static final String TX_API_FORMAT = "format";
    public static final String TX_API_LATITUDE = "latitude";
    public static final String TX_API_LONGITUDE = "longitude";
    public static final String TX_API_OAUTH_VERSION = "oauth_version";
    public static final String TX_API_OPEN_ID = "openid";
    public static final String TX_API_OPEN_KEY = "openkey";
    public static final String TX_API_SCOPE = "scope";
    public static final String TX_API_SYNCFLAG = "syncflag";
    public static final String TX_APP_KEY = ShareApplication.share.getResources().getString(R.string.qq_appkey);
    public static final String TX_APP_KEY_SEC = ShareApplication.share.getResources().getString(R.string.qq_appSecret);
    public static final String TX_PIC_URL = "pic_url";
}
