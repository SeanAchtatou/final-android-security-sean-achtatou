package sina_weibo;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import com.tencent.weibo.sdk.android.api.util.SharePersistent;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

public class Util {
    public static boolean isNetworkAvailable(Activity activity) {
        NetworkInfo[] info;
        ConnectivityManager cm = (ConnectivityManager) activity.getApplicationContext().getSystemService("connectivity");
        if (cm == null || (info = cm.getAllNetworkInfo()) == null) {
            return false;
        }
        for (NetworkInfo state : info) {
            if (state.getState() == NetworkInfo.State.CONNECTED) {
                return true;
            }
        }
        return false;
    }

    public static void saveSharePersistent(Context context, String key, String value) {
        SharePersistent.getInstance().put(context, key, value);
    }

    public static void saveSharePersistent(Context context, String key, long value) {
        SharePersistent.getInstance().put(context, key, value);
    }

    public static String getSharePersistent(Context context, String key) {
        return SharePersistent.getInstance().get(context, key);
    }

    public static Long getSharePersistentLong(Context context, String key) {
        return Long.valueOf(SharePersistent.getInstance().getLong(context, key));
    }

    public static void clearSharePersistent(Context context, String key) {
        SharePersistent.getInstance().clear(context, key);
    }

    public static void clearSharePersistent(Context context) {
        SharePersistent mSharePersistent = SharePersistent.getInstance();
        mSharePersistent.clear(context, "ACCESS_TOKEN");
        mSharePersistent.clear(context, "EXPIRES_IN");
        mSharePersistent.clear(context, "OPEN_ID");
        mSharePersistent.clear(context, "OPEN_KEY");
        mSharePersistent.clear(context, "REFRESH_TOKEN");
        mSharePersistent.clear(context, "NAME");
        mSharePersistent.clear(context, "NICK");
        mSharePersistent.clear(context, "CLIENT_ID");
    }

    public static String getLocalIPAddress(Context context) {
        int i = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getIpAddress();
        return String.valueOf(i & 255) + "." + ((i >> 8) & 255) + "." + ((i >> 16) & 255) + "." + ((i >> 24) & 255);
    }

    public static Location getLocation(Context context) {
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            Criteria criteria = new Criteria();
            criteria.setAccuracy(2);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(3);
            criteria.setSpeedRequired(false);
            String currentProvider = locationManager.getBestProvider(criteria, true);
            Log.d("Location", "currentProvider: " + currentProvider);
            return locationManager.getLastKnownLocation(currentProvider);
        } catch (Exception e) {
            return null;
        }
    }

    private String intToIp(int i) {
        return String.valueOf(i & 255) + "." + ((i >> 8) & 255) + "." + ((i >> 16) & 255) + "." + ((i >> 24) & 255);
    }

    public static Drawable loadImageFromUrl(String imageUrl) {
        try {
            URLConnection conn = new URL(imageUrl).openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inSampleSize = 2;
            return new BitmapDrawable(BitmapFactory.decodeStream(is, null, options));
        } catch (Exception e) {
            return null;
        }
    }

    public static Properties getConfig() {
        Properties props = new Properties();
        try {
            props.load(Util.class.getResourceAsStream("/config/config.properties"));
        } catch (IOException e) {
            Log.e("工具包异常", "获取配置文件异常");
            e.printStackTrace();
        }
        return props;
    }
}
