package sina_weibo;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import com.baidu.push.example.Utils;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.MySSLSocketFactory;
import com.palmtrends.wb.R;
import com.tencent.tauth.Constants;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class WeiboShareDao {
    public static final String main_qq_weibo = ("https://open.t.qq.com/api/user/info?format=json&oauth_consumer_key=" + ShareApplication.share.getResources().getString(R.string.qq_appkey));
    public static final String main_sina_weibo = "https://api.weibo.com/2/users/show.json";
    public static final String qq_weibo_token = ("https://open.t.qq.com/cgi-bin/oauth2/access_token?wap=2&grant_type=authorization_code&redirect_uri=http://www.pymob.cn&client_id=" + ShareApplication.share.getResources().getString(R.string.qq_appkey) + "&client_secret=" + ShareApplication.share.getResources().getString(R.string.qq_appSecret));
    public static final String sina_weibo_token = ("https://api.weibo.com/oauth2/access_token?grant_type=authorization_code&redirect_uri=http://www.pymob.cn&client_id=" + ShareApplication.share.getResources().getString(R.string.sina_appkey) + "&client_secret=" + ShareApplication.share.getResources().getString(R.string.sina_appSecret));
    public static String wb_tuijian = "http://push.cms.palmtrends.com/wb/sug2weibo_v2.php";
    public static final String weib_unbind = "http://push.cms.palmtrends.com/wb/unbind_v2.php";

    public static void weibo_get_wbuid(final String sname, final String code, final Handler hand) throws Exception {
        final List<NameValuePair> param = new ArrayList<>();
        new Thread() {
            public void run() {
                Message msg = new Message();
                if (sname.equals("qq")) {
                    try {
                        JSONObject jo = new JSONObject(MySSLSocketFactory.getinfo(String.valueOf(WeiboShareDao.qq_weibo_token) + "&code=" + code, param));
                        if (!jo.getString("errcode ").equals(UploadUtils.SUCCESS)) {
                            msg.what = 10002;
                            msg.obj = "获取用户TOKEN失败" + jo.getString(Utils.RESPONSE_ERRCODE);
                            hand.sendMessage(msg);
                            return;
                        }
                        PerfHelper.setInfo(Constants.PREF_TX_ACCESS_TOKEN, jo.getString("access_token"));
                        PerfHelper.setInfo(Constants.PREF_TX_OPEN_ID, "openid");
                        PerfHelper.setInfo(Constants.PREF_TX_OPEN_KEY, Constants.TX_API_OPEN_KEY);
                        WeiboShareDao.bind_qq_get_userinfo(jo.getString("access_token"), jo.getString("openid"), jo.getString(Constants.TX_API_OPEN_KEY), hand);
                    } catch (Exception e) {
                        msg.what = 10002;
                        msg.obj = "获取用户TOKEN失败";
                        hand.sendMessage(msg);
                        e.printStackTrace();
                    }
                } else {
                    try {
                        JSONObject jo2 = new JSONObject(MySSLSocketFactory.getinfo(String.valueOf(WeiboShareDao.sina_weibo_token) + "&code=" + code, param));
                        if (jo2.has("error")) {
                            msg.what = 10002;
                            msg.obj = "获取用户信息失败" + jo2.getString("error");
                            hand.sendMessage(msg);
                            return;
                        }
                        PerfHelper.setInfo(Constants.PREF_SINA_ACCESS_TOKEN, jo2.getString("access_token"));
                        PerfHelper.setInfo(Constants.PREF_SINA_UID, jo2.getString(Constants.SINA_UID));
                        PerfHelper.setInfo(Constants.PREF_SINA_EXPIRES_TIME, jo2.getString("expires_in"));
                        PerfHelper.setInfo(Constants.PREF_SINA_REMIND_IN, jo2.getString(Constants.SINA_REMIND_IN));
                        WeiboShareDao.bind_get_userinfo(jo2.getString("access_token"), jo2.getString(Constants.SINA_UID), hand);
                    } catch (Exception e2) {
                        msg.what = 10002;
                        msg.obj = "获取用户信息失败";
                        hand.sendMessage(msg);
                        e2.printStackTrace();
                    }
                }
            }
        }.start();
    }

    public static void weibo_get_shortid(String aid, final Handler hand) throws Exception {
        final List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("aid", aid));
        new Thread() {
            public void run() {
                String str;
                Message msg = new Message();
                try {
                    JSONObject jo = new JSONObject(MySSLSocketFactory.getinfo(WeiboShareDao.main_sina_weibo, param));
                    if (jo.getString(Constants.SINA_CODE).equals(UploadUtils.FAILURE)) {
                        str = jo.getString("shorturl");
                        msg.what = FinalVariable.vb_shortid;
                    } else {
                        str = "短链接获取失败," + jo.getString(Constants.PARAM_SEND_MSG);
                        msg.what = 10002;
                    }
                    msg.obj = str;
                    hand.sendMessage(msg);
                } catch (Exception e) {
                    msg.what = 10002;
                    msg.obj = "短链接获取失败";
                    hand.sendMessage(msg);
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static void bind_unbinded(final String sname, final Handler handler) {
        new Thread() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
             arg types: [java.lang.String, int]
             candidates:
              com.utils.PerfHelper.setInfo(java.lang.String, int):void
              com.utils.PerfHelper.setInfo(java.lang.String, long):void
              com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
              com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
            public void run() {
                Message msg = new Message();
                try {
                    List<NameValuePair> param = new ArrayList<>();
                    param.add(new BasicNameValuePair("sname", sname));
                    JSONObject jo = new JSONObject(MySSLSocketFactory.getinfo("http://push.cms.palmtrends.com/wb/unbind_v2.php?sname=" + sname, param));
                    msg.what = jo.getInt(Constants.SINA_CODE);
                    if (msg.what != 1) {
                        switch (msg.what) {
                            case 0:
                                msg.what = 10002;
                                msg.obj = jo.getString(Constants.PARAM_SEND_MSG);
                                break;
                            case 2:
                                msg.what = 10001;
                                if (PerfHelper.getBooleanData(PerfHelper.P_SHARE_STATE + sname)) {
                                    msg.obj = "请重新绑定微博账号";
                                } else {
                                    msg.obj = "请先绑定微博账号";
                                }
                                PerfHelper.setInfo(PerfHelper.P_SHARE_STATE + sname, false);
                                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_ID + sname, "");
                                break;
                        }
                    } else {
                        msg.obj = sname;
                        msg.what = FinalVariable.vb_success;
                        PerfHelper.setInfo(PerfHelper.P_SHARE_STATE + sname, false);
                        PerfHelper.setInfo(PerfHelper.P_SHARE_NAME + sname, "");
                        PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE + sname, "");
                    }
                } catch (Exception e) {
                    msg.obj = "取消绑定失败";
                    com.palmtrends.loadimage.Utils.showToast(msg.obj.toString());
                    e.printStackTrace();
                }
                handler.sendMessage(msg);
            }
        }.start();
    }

    public static void share_article(final String sname, String comment, String picurl, String ispic, final Handler handler) {
        final List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sname", sname));
        param.add(new BasicNameValuePair("action", "share"));
        if (picurl != null && !"".equals(picurl) && !"null".equals(picurl)) {
            param.add(new BasicNameValuePair(Constants.PARAM_APP_ICON, picurl));
        }
        param.add(new BasicNameValuePair("comment", comment));
        param.add(new BasicNameValuePair("onlypic", ispic));
        final Message msg = new Message();
        new Thread() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
             arg types: [java.lang.String, int]
             candidates:
              com.utils.PerfHelper.setInfo(java.lang.String, int):void
              com.utils.PerfHelper.setInfo(java.lang.String, long):void
              com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
              com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
            public void run() {
                try {
                    JSONObject jo = new JSONObject(MySSLSocketFactory.getinfo(WeiboShareDao.main_sina_weibo, param).trim());
                    msg.what = jo.getInt(Constants.SINA_CODE);
                    if (msg.what != 1) {
                        switch (msg.what) {
                            case 0:
                                msg.what = 10002;
                                msg.obj = jo.getString(Constants.PARAM_SEND_MSG);
                                break;
                            case 2:
                                msg.what = 10001;
                                if (PerfHelper.getBooleanData(PerfHelper.P_SHARE_STATE + sname)) {
                                    msg.obj = "请重新绑定微博账号";
                                } else {
                                    msg.obj = "请先绑定微博账号";
                                }
                                PerfHelper.setInfo(PerfHelper.P_SHARE_STATE + sname, false);
                                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_ID + sname, "");
                                break;
                            case 3:
                                msg.what = 10002;
                                msg.obj = "文章已分享";
                                break;
                        }
                    } else {
                        msg.what = FinalVariable.vb_success;
                        msg.obj = "分享成功";
                    }
                } catch (Exception e) {
                    msg.what = 10002;
                    msg.obj = "分享失败";
                    e.printStackTrace();
                }
                handler.sendMessage(msg);
            }
        }.start();
    }

    public static void share_article_renren(final String sname, String comment, String picurl, String aurl, String atitle, final Handler handler) {
        final List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sname", sname));
        param.add(new BasicNameValuePair("action", "share"));
        if (picurl != null && !"".equals(picurl) && !"null".equals(picurl)) {
            param.add(new BasicNameValuePair(Constants.PARAM_APP_ICON, picurl));
        }
        param.add(new BasicNameValuePair("comment", comment));
        param.add(new BasicNameValuePair(Constants.PARAM_URL, aurl));
        if (atitle != null && atitle.length() > 30) {
            atitle = String.valueOf(atitle.substring(0, 25)) + "...";
        }
        param.add(new BasicNameValuePair(Constants.PARAM_TITLE, atitle));
        new Thread() {
            Message msg = new Message();

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
             arg types: [java.lang.String, int]
             candidates:
              com.utils.PerfHelper.setInfo(java.lang.String, int):void
              com.utils.PerfHelper.setInfo(java.lang.String, long):void
              com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
              com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
            public void run() {
                try {
                    JSONObject jo = new JSONObject(MySSLSocketFactory.getinfo(WeiboShareDao.main_sina_weibo, param));
                    this.msg.what = jo.getInt(Constants.SINA_CODE);
                    if (this.msg.what != 1) {
                        switch (this.msg.what) {
                            case 0:
                                this.msg.what = 10002;
                                this.msg.obj = jo.getString(Constants.PARAM_SEND_MSG);
                                break;
                            case 2:
                                this.msg.what = 10001;
                                if (PerfHelper.getBooleanData(PerfHelper.P_SHARE_STATE + sname)) {
                                    this.msg.obj = "请重新绑定微博账号";
                                } else {
                                    this.msg.obj = "请先绑定微博账号";
                                }
                                PerfHelper.setInfo(PerfHelper.P_SHARE_STATE + sname, false);
                                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_ID + sname, "");
                                break;
                            case 3:
                                this.msg.what = 10002;
                                this.msg.obj = "文章已分享";
                                break;
                        }
                    } else {
                        this.msg.what = FinalVariable.vb_success;
                        this.msg.obj = "分享成功";
                    }
                } catch (Exception e) {
                    this.msg.what = 10002;
                    this.msg.obj = "分享失败";
                    e.printStackTrace();
                }
                handler.sendMessage(this.msg);
            }
        }.start();
    }

    public static void bind_get_userinfo(final String token, final String wbid, final Handler hander) throws Exception {
        new Thread() {
            public void run() {
                Message msg = new Message();
                try {
                    JSONObject jo = new JSONObject(MySSLSocketFactory.getinfo_Get("https://api.weibo.com/2/users/show.json?uid=" + wbid + "&access_token=" + token));
                    msg.what = FinalVariable.vb_get_userinfor;
                    msg.obj = jo;
                    hander.sendMessage(msg);
                } catch (Exception e) {
                    msg.what = 10002;
                    msg.obj = "获取用户信息失败";
                    hander.sendMessage(msg);
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static void bind_qq_get_userinfo(final String token, final String openid, final String openkey, final Handler hander) throws Exception {
        new Thread() {
            public void run() {
                Message msg = new Message();
                try {
                    JSONObject jo = new JSONObject(MySSLSocketFactory.getinfo_Get(String.valueOf(WeiboShareDao.main_qq_weibo) + "&access_token=" + token + "&openid=" + openid + "&openkey=" + openkey + "&clientip=" + "xx" + "&oauth_version=2.a"));
                    msg.what = FinalVariable.vb_get_userinfor;
                    msg.obj = jo;
                    hander.sendMessage(msg);
                } catch (Exception e) {
                    msg.what = 10002;
                    msg.obj = "获取用户信息失败";
                    hander.sendMessage(msg);
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static void bind_tuijian(String vbtype, String sug_type, String version, Handler handler) throws Exception {
        final List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sname", vbtype));
        param.add(new BasicNameValuePair("cid", "3"));
        param.add(new BasicNameValuePair("sug_type", sug_type));
        param.add(new BasicNameValuePair("version", version));
        new Thread() {
            public void run() {
                try {
                    new JSONObject(MySSLSocketFactory.getinfo(WeiboShareDao.wb_tuijian, param));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
        }
        return null;
    }

    public byte[] getBitmapByte(Bitmap bitmap) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        try {
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out.toByteArray();
    }
}
