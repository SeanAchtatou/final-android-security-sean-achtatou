package sina_weibo;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceUtil {
    private static final String PREFERENCE_NAME = "WEIBO_SDK_DEMO";
    private static PreferenceUtil preferenceUtil;
    private SharedPreferences.Editor ed;
    private SharedPreferences sp;

    private PreferenceUtil(Context context) {
        init(context);
    }

    public void init(Context context) {
        if (this.sp == null || this.ed == null) {
            try {
                this.sp = context.getSharedPreferences(PREFERENCE_NAME, 0);
                this.ed = this.sp.edit();
            } catch (Exception e) {
            }
        }
    }

    public static PreferenceUtil getInstance(Context context) {
        if (preferenceUtil == null) {
            preferenceUtil = new PreferenceUtil(context);
        }
        return preferenceUtil;
    }

    public void saveLong(String key, long l) {
        this.ed.putLong(key, l);
        this.ed.commit();
    }

    public long getLong(String key, long defaultlong) {
        return this.sp.getLong(key, defaultlong);
    }

    public void saveBoolean(String key, boolean value) {
        this.ed.putBoolean(key, value);
        this.ed.commit();
    }

    public boolean getBoolean(String key, boolean defaultboolean) {
        return this.sp.getBoolean(key, defaultboolean);
    }

    public void saveInt(String key, int value) {
        if (this.ed != null) {
            this.ed.putInt(key, value);
            this.ed.commit();
        }
    }

    public int getInt(String key, int defaultInt) {
        return this.sp.getInt(key, defaultInt);
    }

    public String getString(String key, String defaultInt) {
        return this.sp.getString(key, defaultInt);
    }

    public String getString(Context context, String key, String defaultValue) {
        if (this.sp == null || this.ed == null) {
            this.sp = context.getSharedPreferences(PREFERENCE_NAME, 0);
            this.ed = this.sp.edit();
        }
        if (this.sp != null) {
            return this.sp.getString(key, defaultValue);
        }
        return defaultValue;
    }

    public void saveString(String key, String value) {
        this.ed.putString(key, value);
        this.ed.commit();
    }

    public void remove(String key) {
        this.ed.remove(key);
        this.ed.commit();
    }

    public void destroy() {
        this.sp = null;
        this.ed = null;
        preferenceUtil = null;
    }
}
