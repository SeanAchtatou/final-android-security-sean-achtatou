package crittercism.android;

import android.os.Environment;
import android.os.StatFs;
import java.math.BigInteger;

public final class h {
    public static BigInteger a() {
        BigInteger valueOf = BigInteger.valueOf(-1);
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return BigInteger.valueOf((long) statFs.getAvailableBlocks()).multiply(BigInteger.valueOf((long) statFs.getBlockSize()));
        } catch (Exception e) {
            return valueOf;
        }
    }

    public static BigInteger b() {
        BigInteger valueOf = BigInteger.valueOf(-1);
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return BigInteger.valueOf((long) statFs.getBlockCount()).multiply(BigInteger.valueOf((long) statFs.getBlockSize()));
        } catch (Exception e) {
            return valueOf;
        }
    }

    public static BigInteger c() {
        BigInteger valueOf = BigInteger.valueOf(-1);
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return BigInteger.valueOf((long) statFs.getAvailableBlocks()).multiply(BigInteger.valueOf((long) statFs.getBlockSize()));
        } catch (Exception e) {
            return valueOf;
        }
    }

    public static BigInteger d() {
        BigInteger valueOf = BigInteger.valueOf(-1);
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return BigInteger.valueOf((long) statFs.getBlockCount()).multiply(BigInteger.valueOf((long) statFs.getBlockSize()));
        } catch (Exception e) {
            return valueOf;
        }
    }
}
