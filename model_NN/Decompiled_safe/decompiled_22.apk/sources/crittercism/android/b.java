package crittercism.android;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Debug;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.WindowManager;
import com.baidu.android.pushservice.PushConstants;
import com.crittercism.app.Crittercism;
import com.imofan.android.basic.MFStatInfo;
import crittercism.android.a;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;

public class b {
    final String a = "critter_did";
    final int b = 100;
    private final String c = d.b();
    private c d = null;
    private Context e = null;
    private String f = "";
    private String g = "";
    private String h = "";
    private String i = "";
    private String j = null;
    private int k = 0;
    private boolean l = false;
    private String m = "";
    private JSONObject n = new JSONObject();
    private Object o = new Object();

    static class a implements Callable {
        private static boolean d = false;
        private static Object f;
        private StringBuilder a = new StringBuilder();
        private StringBuilder b = new StringBuilder();
        private String[] c;
        private Process e = null;
        private Thread g;
        private C0004b h;
        private C0004b i;

        public a(Object obj, Thread thread) {
            f = obj;
            this.g = thread;
            if (Build.VERSION.SDK_INT >= 8) {
                this.c = new String[5];
                this.c[0] = "logcat";
                this.c[1] = "-t";
                this.c[2] = "100";
                this.c[3] = "-v";
                this.c[4] = "time";
                return;
            }
            this.c = new String[4];
            this.c[0] = "logcat";
            this.c[1] = "-d";
            this.c[2] = "-v";
            this.c[3] = "time";
        }

        public static boolean a() {
            return d;
        }

        public static void b() {
            d = true;
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: private */
        /* renamed from: d */
        public StringBuilder call() {
            this.h = null;
            this.i = null;
            if (!d) {
                try {
                    this.e = Runtime.getRuntime().exec(this.c);
                    this.h = new C0004b(this.e.getInputStream());
                    this.i = new C0004b(this.e.getErrorStream());
                    this.h.start();
                    this.i.start();
                    this.e.waitFor();
                    if (this.h != null) {
                        this.a = this.h.a();
                    }
                    if (this.i != null) {
                        this.b = this.i.a();
                    }
                    if (this.e != null) {
                        try {
                            this.e.getInputStream().close();
                            this.e.getErrorStream().close();
                            this.e.getOutputStream().close();
                            this.e.destroy();
                        } catch (Exception e2) {
                        }
                    }
                } catch (Exception e3) {
                    d = true;
                    if (this.h != null) {
                        this.a = this.h.a();
                    }
                    if (this.i != null) {
                        this.b = this.i.a();
                    }
                    if (this.e != null) {
                        try {
                            this.e.getInputStream().close();
                            this.e.getErrorStream().close();
                            this.e.getOutputStream().close();
                            this.e.destroy();
                        } catch (Exception e4) {
                        }
                    }
                } catch (Throwable th) {
                    if (this.h != null) {
                        this.a = this.h.a();
                    }
                    if (this.i != null) {
                        this.b = this.i.a();
                    }
                    if (this.e != null) {
                        try {
                            this.e.getInputStream().close();
                            this.e.getErrorStream().close();
                            this.e.getOutputStream().close();
                            this.e.destroy();
                        } catch (Exception e5) {
                        }
                    }
                    throw th;
                }
            }
            return this.a;
        }

        public final void c() {
            synchronized (f) {
                try {
                    this.h.b();
                    this.i.b();
                    if (this.e != null) {
                        this.e.getInputStream().close();
                        this.e.getErrorStream().close();
                        this.e.getOutputStream().close();
                    }
                } catch (Exception e2) {
                }
                try {
                    if (this.e != null) {
                        this.e.destroy();
                    }
                } catch (Exception e3) {
                }
            }
        }
    }

    /* renamed from: crittercism.android.b$b  reason: collision with other inner class name */
    static class C0004b extends Thread {
        private InputStream a;
        private StringBuilder b = new StringBuilder();
        private BufferedReader c = null;

        public C0004b(InputStream inputStream) {
            this.a = inputStream;
        }

        public final StringBuilder a() {
            return this.b;
        }

        public final void b() {
            if (this.c != null) {
                try {
                    this.c.close();
                } catch (Exception e) {
                    this.c = null;
                }
            }
        }

        public final void run() {
            new String();
            this.c = new BufferedReader(new InputStreamReader(this.a));
            while (true) {
                try {
                    String readLine = this.c.readLine();
                    if (readLine != null) {
                        this.b.append(readLine);
                        this.b.append("\n");
                    } else {
                        try {
                            this.c.close();
                            return;
                        } catch (Exception e) {
                            "CrittercismAPI.StreamThread$makeLogcatJsonArray: ERROR closing bufferedReader!!! " + e.getClass().getName();
                            return;
                        }
                    }
                } catch (Exception e2) {
                    try {
                        this.c.close();
                        return;
                    } catch (Exception e3) {
                        "CrittercismAPI.StreamThread$makeLogcatJsonArray: ERROR closing bufferedReader!!! " + e3.getClass().getName();
                        return;
                    }
                } catch (Throwable th) {
                    try {
                        this.c.close();
                    } catch (Exception e4) {
                        "CrittercismAPI.StreamThread$makeLogcatJsonArray: ERROR closing bufferedReader!!! " + e4.getClass().getName();
                    }
                    throw th;
                }
            }
        }
    }

    public b(Context context, String str, String str2, String str3, int i2, String str4) {
        this.e = context;
        this.d = new c(this.c, this.e);
        this.f = str;
        this.g = null;
        this.h = str2;
        this.i = str3;
        this.k = i2;
        this.j = str4;
    }

    private static String b(String str) {
        if (str != null && !str.equals("")) {
            try {
                str = new String(new BigInteger(1, MessageDigest.getInstance("SHA-256").digest(str.getBytes())).toString(16));
            } catch (NoSuchAlgorithmException e2) {
                str = null;
            }
        }
        if (str.equals("")) {
            return null;
        }
        return str;
    }

    private int c(String str) {
        try {
            return this.e.getPackageManager().checkPermission(str, this.e.getPackageName());
        } catch (Exception e2) {
            return -1;
        }
    }

    private double j() {
        try {
            Intent registerReceiver = this.e.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int intExtra = registerReceiver.getIntExtra("level", -1);
            double intExtra2 = (double) registerReceiver.getIntExtra("scale", -1);
            if (intExtra < 0 || intExtra2 <= 0.0d) {
                return 1.0d;
            }
            return ((double) intExtra) / intExtra2;
        } catch (Exception e2) {
            return 1.0d;
        }
    }

    private static long k() {
        int i2 = -1;
        try {
            Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
            Debug.getMemoryInfo(memoryInfo);
            i2 = (memoryInfo.otherPss + memoryInfo.dalvikPss + memoryInfo.nativePss) * 1024;
        } catch (Exception e2) {
        }
        return (long) i2;
    }

    private String l() {
        try {
            return ((TelephonyManager) this.e.getSystemService("phone")).getNetworkOperatorName();
        } catch (Exception e2) {
            return Build.BRAND;
        }
    }

    private JSONObject m() {
        JSONObject jSONObject = new JSONObject();
        try {
            NetworkInfo networkInfo = ((ConnectivityManager) this.e.getSystemService("connectivity")).getNetworkInfo(1);
            jSONObject.put("available", networkInfo.isAvailable());
            jSONObject.put("connected", networkInfo.isConnected());
            if (!networkInfo.isConnected()) {
                jSONObject.put("connecting", networkInfo.isConnectedOrConnecting());
            }
            jSONObject.put("failover", networkInfo.isFailover());
        } catch (Exception e2) {
            b.class.getCanonicalName();
            e2.toString();
        }
        return jSONObject;
    }

    private JSONObject n() {
        JSONObject jSONObject = new JSONObject();
        try {
            NetworkInfo networkInfo = ((ConnectivityManager) this.e.getSystemService("connectivity")).getNetworkInfo(0);
            jSONObject.put("available", networkInfo.isAvailable());
            jSONObject.put("connected", networkInfo.isConnected());
            if (!networkInfo.isConnected()) {
                jSONObject.put("connecting", networkInfo.isConnectedOrConnecting());
            }
            jSONObject.put("failover", networkInfo.isFailover());
            jSONObject.put("roaming", networkInfo.isRoaming());
        } catch (Exception e2) {
            b.class.getCanonicalName();
            e2.toString() + " in getMobileNetworkStatus";
        }
        return jSONObject;
    }

    private String o() {
        try {
            if (c("android.permission.GET_TASKS") == 0) {
                List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) this.e.getSystemService("activity")).getRunningTasks(1);
                "CURRENT Activity ::" + runningTasks.get(0).topActivity.getClassName();
                return runningTasks.get(0).topActivity.flattenToShortString().replace(CookieSpec.PATH_DELIM, "");
            }
        } catch (Exception e2) {
        }
        return "";
    }

    private JSONArray p() {
        StringBuilder sb;
        JSONArray jSONArray = new JSONArray();
        StringBuilder sb2 = new StringBuilder();
        synchronized (this.o) {
            ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
            a aVar = new a(this.o, Thread.currentThread());
            Future submit = newCachedThreadPool.submit(aVar);
            try {
                sb = !a.a() ? (StringBuilder) submit.get(5, TimeUnit.SECONDS) : sb2;
                submit.cancel(true);
                newCachedThreadPool.shutdownNow();
            } catch (Exception e2) {
                a.b();
                aVar.c();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e3) {
                }
                submit.cancel(true);
                newCachedThreadPool.shutdownNow();
                sb = sb2;
            } catch (Throwable th) {
                submit.cancel(true);
                newCachedThreadPool.shutdownNow();
                throw th;
            }
        }
        if (sb.toString().length() > 0) {
            String[] split = sb.toString().split("\n");
            for (String put : split) {
                jSONArray.put(put);
            }
        }
        int length = jSONArray.length();
        if (length <= 100) {
            return jSONArray;
        }
        JSONArray jSONArray2 = new JSONArray();
        for (int i2 = length - 100; i2 < length; i2++) {
            try {
                jSONArray2.put(jSONArray.getString(i2));
            } catch (JSONException e4) {
                "Caught exception in second try-catch of getLogCat():" + e4.getClass().getName();
            }
        }
        return jSONArray2;
    }

    private float q() {
        try {
            return this.e.getResources().getDisplayMetrics().ydpi;
        } catch (Exception e2) {
            return 0.0f;
        }
    }

    private boolean r() {
        int i2 = 30;
        while (!this.l && i2 > 0) {
            try {
                i2--;
                Thread.sleep(1000);
            } catch (Exception e2) {
            }
        }
        return this.l;
    }

    public final String a() {
        return this.f;
    }

    public final JSONObject a(JSONObject jSONObject) {
        String str;
        JSONObject jSONObject2;
        String[] strArr;
        String a2;
        String str2 = null;
        String str3 = new String();
        JSONObject jSONObject3 = new JSONObject();
        JSONObject jSONObject4 = new JSONObject();
        new String();
        try {
            jSONObject4.put("success", 0);
            str3 = jSONObject.getString("requestUrl");
            str = str3;
            jSONObject2 = jSONObject.getJSONObject("requestData");
        } catch (JSONException e2) {
            e2.printStackTrace();
            JSONObject jSONObject5 = jSONObject3;
            str = str3;
            jSONObject2 = jSONObject5;
        } catch (Exception e3) {
            e3.printStackTrace();
            JSONObject jSONObject6 = jSONObject3;
            str = str3;
            jSONObject2 = jSONObject6;
        }
        try {
            if (!jSONObject2.has("filenames") || !jSONObject2.has("filename_prefix")) {
                strArr = null;
            } else {
                new JSONArray();
                JSONArray jSONArray = jSONObject2.getJSONArray("filenames");
                strArr = new String[jSONArray.length()];
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    strArr[i2] = jSONArray.getString(i2);
                }
                str2 = jSONObject2.getString("filename_prefix");
                jSONObject2.remove("filenames");
                jSONObject2.remove("filename_prefix");
            }
            if (strArr == null || str2 == null) {
                a2 = this.d.a(str, jSONObject2);
            } else {
                try {
                    a2 = this.d.a(str, jSONObject2, strArr, str2);
                } catch (f e4) {
                    throw e4;
                } catch (IOException e5) {
                    throw e5;
                } catch (Exception e6) {
                    "Exception in postJsonDataNew: " + e6.getClass().getName();
                    JSONObject jSONObject7 = new JSONObject();
                    try {
                        jSONObject7.put("success", 0);
                        return jSONObject7;
                    } catch (Exception e7) {
                        return jSONObject7;
                    }
                }
            }
            if (a2 == null || a2.equals("")) {
                return jSONObject4;
            }
            JSONObject jSONObject8 = new JSONObject(a2);
            "POSTING JSON DATA: url = " + str + "\tdata = " + jSONObject2.toString();
            "POSTING JSON DATA: response = " + jSONObject8.toString();
            return jSONObject8;
        } catch (Exception e8) {
            e8.printStackTrace();
            return jSONObject4;
        }
    }

    public final JSONObject a(boolean... zArr) {
        Exception exc;
        JSONObject jSONObject;
        int i2 = 0;
        int i3 = 1;
        boolean z = zArr.length > 0 ? zArr[0] : true;
        boolean z2 = zArr.length > 1 ? zArr[1] : false;
        JSONObject jSONObject2 = new JSONObject();
        try {
            JSONObject d2 = d();
            try {
                d2.put("battery_level", j());
                d2.put("memory_usage", k());
                ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                ((ActivityManager) this.e.getSystemService("activity")).getMemoryInfo(memoryInfo);
                if (memoryInfo.lowMemory) {
                    i2 = 1;
                }
                d2.put("low_memory", i2);
                if (c("android.permission.ACCESS_NETWORK_STATE") == 0) {
                    d2.put("wifi", m());
                    d2.put("mobile_network", n());
                }
                d2.put("disk_space_free", h.a().toString());
                d2.put("disk_space_total", h.b().toString());
                d2.put("sd_space_free", h.c().toString());
                d2.put("sd_space_total", h.d().toString());
                int i4 = this.e.getResources().getConfiguration().orientation;
                if (i4 == 0) {
                    Display defaultDisplay = ((WindowManager) this.e.getSystemService("window")).getDefaultDisplay();
                    if (defaultDisplay.getWidth() == defaultDisplay.getHeight()) {
                        i3 = 3;
                    } else if (defaultDisplay.getWidth() > defaultDisplay.getHeight()) {
                        i3 = 2;
                    }
                } else {
                    i3 = i4;
                }
                d2.put("orientation", i3);
                d2.put("activity", o());
                if (z2) {
                    d2.put("metadata", this.n);
                }
                if (z) {
                    if (c("android.permission.READ_LOGS") == 0) {
                        JSONArray p = p();
                        if (p.length() > 0) {
                            try {
                                d2.put("logcat", p);
                                return d2;
                            } catch (Exception e2) {
                                "put logcat EXCEPTION: " + e2.getClass().getName();
                            }
                        }
                        return d2;
                    } else if (Build.VERSION.SDK_INT >= 16 && Crittercism.s()) {
                        JSONArray p2 = p();
                        if (p2.length() > 0) {
                            try {
                                d2.put("logcat", p2);
                                return d2;
                            } catch (Exception e3) {
                                "put logcat EXCEPTION: " + e3.getClass().getName();
                            }
                        }
                    }
                }
                return d2;
            } catch (Exception e4) {
                exc = e4;
                jSONObject = d2;
                "Exception with getStateInfo(): " + exc.getClass().getName();
                return jSONObject;
            }
        } catch (Exception e5) {
            exc = e5;
            jSONObject = jSONObject2;
            "Exception with getStateInfo(): " + exc.getClass().getName();
            return jSONObject;
        }
    }

    public final void a(String str) {
        this.g = str;
    }

    public final String b() {
        String str;
        String string = Settings.Secure.getString(this.e.getContentResolver(), "android_id");
        if (string == null || string.equals("") || "9774d56d682e549c".equals(string)) {
            str = null;
        } else {
            try {
                UUID nameUUIDFromBytes = UUID.nameUUIDFromBytes(string.getBytes("utf8"));
                if (nameUUIDFromBytes != null) {
                    str = nameUUIDFromBytes.toString();
                    if (str != null && str.equals("")) {
                        str = null;
                    }
                }
            } catch (UnsupportedEncodingException e2) {
            }
            str = null;
            str = null;
        }
        if (str == null && this.e.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", Crittercism.a().q()) == 0) {
            str = b(((TelephonyManager) this.e.getSystemService("phone")).getDeviceId());
        }
        return str == null ? UUID.randomUUID().toString() : str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x004a, code lost:
        if (r2.getInt("success") == 1) goto L_0x004c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean b(org.json.JSONObject r7) {
        /*
            r6 = this;
            r0 = 1
            monitor-enter(r6)
            r1 = 0
            java.lang.String r2 = "username"
            boolean r2 = r7.has(r2)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            if (r2 == 0) goto L_0x004e
            java.lang.String r2 = "username"
            java.lang.String r2 = r7.getString(r2)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            r6.m = r2     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
        L_0x0013:
            boolean r2 = r6.r()     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            if (r2 == 0) goto L_0x006e
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            r2.<init>()     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            r3.<init>()     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            org.json.JSONObject r3 = r6.c()     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            java.lang.String r4 = "metadata"
            r3.put(r4, r7)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            java.lang.String r4 = "requestUrl"
            java.lang.String r5 = crittercism.android.a.c.e     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            r2.put(r4, r5)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            java.lang.String r4 = "requestData"
            r2.put(r4, r3)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            org.json.JSONObject r2 = r6.a(r2)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            java.lang.String r3 = "success"
            boolean r3 = r2.has(r3)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            if (r3 == 0) goto L_0x006e
            java.lang.String r3 = "success"
            int r2 = r2.getInt(r3)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            if (r2 != r0) goto L_0x006e
        L_0x004c:
            monitor-exit(r6)
            return r0
        L_0x004e:
            java.lang.String r2 = r6.m     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            if (r2 != 0) goto L_0x0013
            java.lang.String r2 = "username"
            java.lang.String r3 = r6.m     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            r7.put(r2, r3)     // Catch:{ f -> 0x0060, Exception -> 0x0065 }
            goto L_0x0013
        L_0x0060:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0062 }
        L_0x0062:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0065:
            r0 = move-exception
            java.lang.Class<crittercism.android.b> r2 = crittercism.android.b.class
            r2.getCanonicalName()     // Catch:{ all -> 0x0062 }
            r0.toString()     // Catch:{ all -> 0x0062 }
        L_0x006e:
            r0 = r1
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: crittercism.android.b.b(org.json.JSONObject):boolean");
    }

    public final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(PushConstants.EXTRA_APP_ID, this.f);
            if (this.g == null) {
                this.g = b();
            }
            jSONObject.put("hashed_device_id", this.g);
            jSONObject.put("device_name", "android");
            jSONObject.put("library_version", this.h);
        } catch (JSONException e2) {
        } catch (Exception e3) {
            "Exception in getRequiredParams(): " + e3.getClass().getName();
        }
        return jSONObject;
    }

    public final void c(JSONObject jSONObject) {
        this.n = jSONObject;
    }

    public final JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("arch", System.getProperty("os.arch"));
            jSONObject.put("locale", a.b(this.e));
            jSONObject.put("dpi", (double) e());
            jSONObject.put("xdpi", (double) f());
            jSONObject.put("ydpi", (double) q());
            jSONObject.put(Constants.SINA_NAME, "");
            jSONObject.put("system", "android");
            jSONObject.put(MFStatInfo.KEY_MODEL, Build.MODEL);
            jSONObject.put("carrier", l());
            jSONObject.put("app_version", this.i);
            jSONObject.put("system_version", Build.VERSION.RELEASE);
            jSONObject.put("app_version_code", this.k);
            if (this.j == null) {
                return jSONObject;
            }
            jSONObject.put("custom_version_name", this.j);
            return jSONObject;
        } catch (Exception e2) {
            return new JSONObject();
        }
    }

    public final float e() {
        try {
            return this.e.getResources().getDisplayMetrics().density;
        } catch (Exception e2) {
            return 1.0f;
        }
    }

    public final float f() {
        try {
            return this.e.getResources().getDisplayMetrics().xdpi;
        } catch (Exception e2) {
            return 0.0f;
        }
    }

    public final String g() {
        new JSONObject();
        new JSONObject();
        String str = new String();
        try {
            JSONObject c2 = c();
            c2.put("pkg", this.e.getPackageName());
            str = this.d.a(a.c.f, c2);
            if (str != null && !str.equals("")) {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.has("success")) {
                    a.c.f + " response: " + str;
                } else if (jSONObject.getInt("success") == 1) {
                    "app_id: " + jSONObject.getString(PushConstants.EXTRA_APP_ID);
                    "package name: " + jSONObject.getString("pkg");
                    "updated settings: " + jSONObject.getJSONObject("updated_settings");
                }
            }
        } catch (f | IOException | JSONException e2) {
        } catch (Exception e3) {
            "sendPackageName: Exception! " + e3.getClass().getName();
        }
        return str;
    }

    public final void h() {
        this.l = true;
    }

    public final JSONObject i() {
        return this.n;
    }
}
