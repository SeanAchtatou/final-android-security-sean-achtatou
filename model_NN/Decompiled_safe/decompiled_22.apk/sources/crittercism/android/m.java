package crittercism.android;

import android.content.SharedPreferences;
import com.crittercism.app.Crittercism;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import crittercism.android.a;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Map;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public final class m extends j {
    public String c = new String();
    public JSONArray d = new JSONArray();
    public JSONArray e = new JSONArray();
    public JSONObject f = new JSONObject();

    public m() {
        this.a = a.c.d;
        this.b = new Vector();
    }

    private static m a(JSONObject jSONObject) {
        JSONArray jSONArray;
        m mVar = new m();
        JSONObject jSONObject2 = new JSONObject();
        JSONArray jSONArray2 = new JSONArray();
        try {
            if (jSONObject.has("requestData")) {
                jSONObject2 = jSONObject.getJSONObject("requestData");
            }
        } catch (Exception e2) {
            jSONObject2 = new JSONObject();
        }
        try {
            jSONArray = jSONObject2.has("crashes") ? jSONObject2.getJSONArray("crashes") : jSONArray2;
        } catch (Exception e3) {
            jSONArray = new JSONArray();
        }
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                mVar.a((Object) jSONArray.getJSONObject(i));
            } catch (Exception e4) {
                "Exception in SdkCrashes.fromJSON: " + e4.getClass().getName();
            }
        }
        return mVar;
    }

    public static String a() {
        String str = new String();
        if (Crittercism.a() != null) {
            try {
                str = Crittercism.a().j();
            } catch (Exception e2) {
                str = new String();
            }
        }
        return "critter_pendingcrashes_" + str;
    }

    public static m c() {
        JSONObject jSONObject;
        m mVar = new m();
        JSONObject jSONObject2 = new JSONObject();
        try {
            SharedPreferences sharedPreferences = Crittercism.a().k().getSharedPreferences("com.crittercism.crashes", 0);
            jSONObject = new JSONObject(sharedPreferences.getString(a(), new JSONObject().toString()));
            try {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.remove(a());
                if (!edit.commit()) {
                    throw new Exception("failed to remove crashes from Shared Preferences");
                }
            } catch (Exception e2) {
                e = e2;
                "Exception in SdkCrashes.readFromDisk(): " + e.getClass().getName();
                return a(jSONObject);
            }
        } catch (Exception e3) {
            e = e3;
            jSONObject = jSONObject2;
            "Exception in SdkCrashes.readFromDisk(): " + e.getClass().getName();
            return a(jSONObject);
        }
        try {
            return a(jSONObject);
        } catch (Exception e4) {
            return mVar;
        }
    }

    public final void a(Throwable th) {
        int indexOf = th.toString().indexOf(":");
        this.c = indexOf >= 0 ? th.toString().substring(0, indexOf) : th.getClass().getName();
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
            this.c = cause.getClass().getName();
            cause.printStackTrace(printWriter);
        }
        String obj = stringWriter.toString();
        this.d = new JSONArray();
        String[] split = obj.split("\n");
        for (String put : split) {
            this.d.put(put);
        }
    }

    public final JSONObject b() {
        JSONArray jSONArray;
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        new JSONObject();
        new JSONArray();
        try {
            jSONArray = new JSONArray((Collection) this.b);
        } catch (Exception e2) {
            jSONArray = new JSONArray();
        }
        try {
            jSONObject = Crittercism.a().l().c();
            jSONObject.put("crashes", jSONArray);
        } catch (Exception e3) {
            jSONObject = new JSONObject();
        }
        try {
            jSONObject2.put("requestUrl", this.a);
            jSONObject2.put("requestData", jSONObject);
            return jSONObject2;
        } catch (Exception e4) {
            return new JSONObject();
        }
    }

    public final void d() {
        for (Map.Entry next : Thread.getAllStackTraces().entrySet()) {
            JSONObject jSONObject = new JSONObject();
            Thread thread = (Thread) next.getKey();
            try {
                if (thread.getId() != Thread.currentThread().getId()) {
                    jSONObject.put(Constants.SINA_NAME, thread.getName());
                    jSONObject.put(LocaleUtil.INDONESIAN, thread.getId());
                    jSONObject.put("state", thread.getState().name());
                    JSONArray jSONArray = new JSONArray();
                    for (StackTraceElement stackTraceElement : (StackTraceElement[]) next.getValue()) {
                        "  " + stackTraceElement;
                        jSONArray.put(stackTraceElement);
                    }
                    jSONObject.put("stacktrace", jSONArray);
                    this.e.put(jSONObject);
                }
            } catch (Exception e2) {
                "Problem with setBackgroundThreads(): " + e2.getClass().getName();
            }
        }
    }
}
