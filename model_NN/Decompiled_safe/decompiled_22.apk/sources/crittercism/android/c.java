package crittercism.android;

import android.content.Context;
import com.ibm.mqtt.MqttUtils;
import com.palmtrends.loadimage.ImageFetcher;
import crittercism.android.f;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Random;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class c {
    private final DefaultHttpClient a;
    private final String b;
    private Context c;

    public c() {
        this.b = "";
        this.a = a();
    }

    public c(String str, Context context) {
        this.b = str;
        this.a = a();
        this.c = context;
    }

    private String a(HttpRequestBase httpRequestBase) {
        try {
            this.a.getConnectionManager().closeExpiredConnections();
            HttpResponse execute = this.a.execute(httpRequestBase);
            int statusCode = execute.getStatusLine().getStatusCode();
            switch (statusCode) {
                case HttpStatus.SC_OK /*200*/:
                    return EntityUtils.toString(execute.getEntity(), MqttUtils.STRING_ENCODING);
                default:
                    c.class.getCanonicalName();
                    "executeHttpRequest error=" + statusCode + " - " + execute.getStatusLine().toString();
                    execute.getEntity().consumeContent();
                    return "";
            }
        } catch (ConnectTimeoutException e) {
            c.class.getClass().getCanonicalName();
            throw new f(a.a(11), f.a.CONN_TIMEOUT);
        } catch (IOException e2) {
            IOException iOException = e2;
            httpRequestBase.abort();
            for (Header header : httpRequestBase.getAllHeaders()) {
                header.getName() + ": " + header.getValue();
            }
            if (iOException.getMessage() != null && iOException.getMessage().toLowerCase().contains("no route to host")) {
                throw new f(a.a(10), f.a.NO_INTERNET);
            } else if (iOException.getStackTrace()[0].toString().contains("java.net.InetAddress.lookupHostByName")) {
                c.class.getClass().getCanonicalName();
                throw new f(a.a(10), f.a.NO_INTERNET);
            } else {
                throw iOException;
            }
        } catch (Exception e3) {
            c.class.getClass().getCanonicalName();
            "Neither ConnectTimeoutException nor IOException.  Instead: " + e3.getClass().getName();
            return "";
        }
    }

    private static DefaultHttpClient a() {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme(ImageFetcher.HTTP_CACHE_DIR, PlainSocketFactory.getSocketFactory(), 80));
        X509HostnameVerifier x509HostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier(x509HostnameVerifier);
        schemeRegistry.register(new Scheme("https", socketFactory, 443));
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 20000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
        HttpClientParams.setRedirecting(basicHttpParams, false);
        return new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
    }

    public final String a(String str, JSONObject jSONObject) {
        HttpPost httpPost = new HttpPost(str);
        httpPost.addHeader("User-Agent", this.b);
        httpPost.addHeader("Content-Type", "application/json");
        try {
            httpPost.setEntity(new ByteArrayEntity(jSONObject.toString().getBytes("UTF8")));
            "JSON Object for request: " + jSONObject.toString();
            "httpPost Entity is: " + httpPost.getEntity().getContent().toString();
            return a(httpPost);
        } catch (Exception e) {
            throw new IllegalArgumentException("Error encoding http params");
        }
    }

    public final String a(String str, JSONObject jSONObject, String[] strArr, String str2) {
        new String();
        byte[] bArr = new byte[30];
        new Random().nextBytes(bArr);
        String a2 = g.a(bArr);
        "boundary = " + a2;
        HttpPost httpPost = new HttpPost(str);
        httpPost.addHeader("User-Agent", this.b);
        httpPost.addHeader("Content-Type", "multipart/form-data; boundary=" + a2 + "; charset=\"utf-8\"");
        httpPost.addHeader("Accept", "text/plain, application/json");
        httpPost.addHeader("Content-Disposition", "form-data");
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            StringBuilder sb = new StringBuilder();
            sb.append("--" + a2 + "\n");
            sb.append("Content-Disposition: form-data; name=\"" + str2 + "json\";\n");
            sb.append("Content-type: application/json\n\n");
            sb.append(jSONObject.toString() + "\n");
            byteArrayOutputStream.write(sb.toString().getBytes(MqttUtils.STRING_ENCODING));
            for (int i = 0; i < strArr.length; i++) {
                File file = new File(strArr[i]);
                StringBuilder sb2 = new StringBuilder();
                sb2.append("--" + a2 + "\n");
                sb2.append("Content-Disposition: form-data; name=\"" + str2 + Integer.toString(i) + "\"; filename=\"" + file.getName() + "\";\n");
                sb2.append("Content-type: application/octet-stream\n\n");
                byteArrayOutputStream.write(sb2.toString().getBytes(MqttUtils.STRING_ENCODING));
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr2 = new byte[8192];
                while (true) {
                    int read = fileInputStream.read(bArr2);
                    if (read < 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr2, 0, read);
                }
                byteArrayOutputStream.write(("\n").getBytes(MqttUtils.STRING_ENCODING));
                fileInputStream.close();
                byteArrayOutputStream.close();
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append("--" + a2 + "--");
            byteArrayOutputStream.write(sb3.toString().getBytes(MqttUtils.STRING_ENCODING));
            httpPost.setEntity(new ByteArrayEntity(byteArrayOutputStream.toByteArray()));
        } catch (Exception e) {
        }
        return a(httpPost);
    }
}
