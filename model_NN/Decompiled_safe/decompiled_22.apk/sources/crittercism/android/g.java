package crittercism.android;

import com.ibm.mqtt.MqttUtils;
import com.tencent.mm.sdk.contact.RContact;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public final class g {
    private static final byte[] a;
    private static final byte[] b = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    /* access modifiers changed from: private */
    public static final byte[] c;

    public static class a extends FilterOutputStream {
        private boolean a;
        private int b;
        private byte[] c;
        private int d;
        private int e;
        private boolean f;
        private byte[] g;
        private boolean h;

        public final void close() {
            if (this.b > 0) {
                if (this.a) {
                    this.out.write(g.a(this.c, 0, this.b, this.g, 0));
                    this.b = 0;
                } else {
                    throw new IOException("Base64 input not properly padded.");
                }
            }
            super.close();
            this.c = null;
            this.out = null;
        }

        public final void write(int i) {
            if (this.h) {
                this.out.write(i);
            } else if (this.a) {
                byte[] bArr = this.c;
                int i2 = this.b;
                this.b = i2 + 1;
                bArr[i2] = (byte) i;
                if (this.b >= this.d) {
                    this.out.write(g.a(this.c, 0, this.d, this.g, 0));
                    this.e += 4;
                    if (this.f && this.e >= 76) {
                        this.out.write(10);
                        this.e = 0;
                    }
                    this.b = 0;
                }
            } else if (g.c[i & RContact.MM_CONTACTFLAG_ALL] > -5) {
                byte[] bArr2 = this.c;
                int i3 = this.b;
                this.b = i3 + 1;
                bArr2[i3] = (byte) i;
                if (this.b >= this.d) {
                    this.out.write(this.g, 0, g.b(this.c, this.g));
                    this.b = 0;
                }
            } else if (g.c[i & RContact.MM_CONTACTFLAG_ALL] != -5) {
                throw new IOException("Invalid character in Base64 data.");
            }
        }

        public final void write(byte[] bArr, int i, int i2) {
            if (this.h) {
                this.out.write(bArr, i, i2);
                return;
            }
            for (int i3 = 0; i3 < i2; i3++) {
                write(bArr[i + i3]);
            }
        }
    }

    static {
        byte[] bArr;
        try {
            bArr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".getBytes(MqttUtils.STRING_ENCODING);
        } catch (UnsupportedEncodingException e) {
            bArr = b;
        }
        a = bArr;
        byte[] bArr2 = new byte[RContact.MM_CONTACTFLAG_ALL];
        // fill-array-data instruction
        bArr2[0] = -9;
        bArr2[1] = -9;
        bArr2[2] = -9;
        bArr2[3] = -9;
        bArr2[4] = -9;
        bArr2[5] = -9;
        bArr2[6] = -9;
        bArr2[7] = -9;
        bArr2[8] = -9;
        bArr2[9] = -5;
        bArr2[10] = -5;
        bArr2[11] = -9;
        bArr2[12] = -9;
        bArr2[13] = -5;
        bArr2[14] = -9;
        bArr2[15] = -9;
        bArr2[16] = -9;
        bArr2[17] = -9;
        bArr2[18] = -9;
        bArr2[19] = -9;
        bArr2[20] = -9;
        bArr2[21] = -9;
        bArr2[22] = -9;
        bArr2[23] = -9;
        bArr2[24] = -9;
        bArr2[25] = -9;
        bArr2[26] = -9;
        bArr2[27] = -9;
        bArr2[28] = -9;
        bArr2[29] = -9;
        bArr2[30] = -9;
        bArr2[31] = -9;
        bArr2[32] = -5;
        bArr2[33] = -9;
        bArr2[34] = -9;
        bArr2[35] = -9;
        bArr2[36] = -9;
        bArr2[37] = -9;
        bArr2[38] = -9;
        bArr2[39] = -9;
        bArr2[40] = -9;
        bArr2[41] = -9;
        bArr2[42] = -9;
        bArr2[43] = 62;
        bArr2[44] = -9;
        bArr2[45] = -9;
        bArr2[46] = -9;
        bArr2[47] = 63;
        bArr2[48] = 52;
        bArr2[49] = 53;
        bArr2[50] = 54;
        bArr2[51] = 55;
        bArr2[52] = 56;
        bArr2[53] = 57;
        bArr2[54] = 58;
        bArr2[55] = 59;
        bArr2[56] = 60;
        bArr2[57] = 61;
        bArr2[58] = -9;
        bArr2[59] = -9;
        bArr2[60] = -9;
        bArr2[61] = -1;
        bArr2[62] = -9;
        bArr2[63] = -9;
        bArr2[64] = -9;
        bArr2[65] = 0;
        bArr2[66] = 1;
        bArr2[67] = 2;
        bArr2[68] = 3;
        bArr2[69] = 4;
        bArr2[70] = 5;
        bArr2[71] = 6;
        bArr2[72] = 7;
        bArr2[73] = 8;
        bArr2[74] = 9;
        bArr2[75] = 10;
        bArr2[76] = 11;
        bArr2[77] = 12;
        bArr2[78] = 13;
        bArr2[79] = 14;
        bArr2[80] = 15;
        bArr2[81] = 16;
        bArr2[82] = 17;
        bArr2[83] = 18;
        bArr2[84] = 19;
        bArr2[85] = 20;
        bArr2[86] = 21;
        bArr2[87] = 22;
        bArr2[88] = 23;
        bArr2[89] = 24;
        bArr2[90] = 25;
        bArr2[91] = -9;
        bArr2[92] = -9;
        bArr2[93] = -9;
        bArr2[94] = -9;
        bArr2[95] = -9;
        bArr2[96] = -9;
        bArr2[97] = 26;
        bArr2[98] = 27;
        bArr2[99] = 28;
        bArr2[100] = 29;
        bArr2[101] = 30;
        bArr2[102] = 31;
        bArr2[103] = 32;
        bArr2[104] = 33;
        bArr2[105] = 34;
        bArr2[106] = 35;
        bArr2[107] = 36;
        bArr2[108] = 37;
        bArr2[109] = 38;
        bArr2[110] = 39;
        bArr2[111] = 40;
        bArr2[112] = 41;
        bArr2[113] = 42;
        bArr2[114] = 43;
        bArr2[115] = 44;
        bArr2[116] = 45;
        bArr2[117] = 46;
        bArr2[118] = 47;
        bArr2[119] = 48;
        bArr2[120] = 49;
        bArr2[121] = 50;
        bArr2[122] = 51;
        bArr2[123] = -9;
        bArr2[124] = -9;
        bArr2[125] = -9;
        bArr2[126] = -9;
        c = bArr2;
    }

    public static String a(byte[] bArr) {
        return a(bArr, bArr.length);
    }

    private static String a(byte[] bArr, int i) {
        int i2 = (i * 4) / 3;
        byte[] bArr2 = new byte[((i % 3 > 0 ? 4 : 0) + i2 + (i2 / 76))];
        int i3 = i - 2;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (i6 < i3) {
            a(bArr, i6 + 0, 3, bArr2, i5);
            i4 += 4;
            if (i4 == 76) {
                bArr2[i5 + 4] = 10;
                i5++;
                i4 = 0;
            }
            i6 += 3;
            i5 += 4;
        }
        if (i6 < i) {
            a(bArr, i6 + 0, i - i6, bArr2, i5);
            i5 += 4;
        }
        try {
            return new String(bArr2, 0, i5, MqttUtils.STRING_ENCODING);
        } catch (UnsupportedEncodingException e) {
            return new String(bArr2, 0, i5);
        }
    }

    /* access modifiers changed from: private */
    public static byte[] a(byte[] bArr, int i, int i2, byte[] bArr2, int i3) {
        int i4 = 0;
        int i5 = (i2 > 1 ? (bArr[i + 1] << 24) >>> 16 : 0) | (i2 > 0 ? (bArr[i] << 24) >>> 8 : 0);
        if (i2 > 2) {
            i4 = (bArr[i + 2] << 24) >>> 24;
        }
        int i6 = i4 | i5;
        switch (i2) {
            case 1:
                bArr2[i3] = a[i6 >>> 18];
                bArr2[i3 + 1] = a[(i6 >>> 12) & 63];
                bArr2[i3 + 2] = 61;
                bArr2[i3 + 3] = 61;
                break;
            case 2:
                bArr2[i3] = a[i6 >>> 18];
                bArr2[i3 + 1] = a[(i6 >>> 12) & 63];
                bArr2[i3 + 2] = a[(i6 >>> 6) & 63];
                bArr2[i3 + 3] = 61;
                break;
            case 3:
                bArr2[i3] = a[i6 >>> 18];
                bArr2[i3 + 1] = a[(i6 >>> 12) & 63];
                bArr2[i3 + 2] = a[(i6 >>> 6) & 63];
                bArr2[i3 + 3] = a[i6 & 63];
                break;
        }
        return bArr2;
    }

    /* access modifiers changed from: private */
    public static int b(byte[] bArr, byte[] bArr2) {
        if (bArr[2] == 61) {
            bArr2[0] = (byte) ((((c[bArr[0]] & 255) << 18) | ((c[bArr[1]] & 255) << 12)) >>> 16);
            return 1;
        } else if (bArr[3] == 61) {
            int i = ((c[bArr[0]] & 255) << 18) | ((c[bArr[1]] & 255) << 12) | ((c[bArr[2]] & 255) << 6);
            bArr2[0] = (byte) (i >>> 16);
            bArr2[1] = (byte) (i >>> 8);
            return 2;
        } else {
            try {
                byte b2 = ((c[bArr[0]] & 255) << 18) | ((c[bArr[1]] & 255) << 12) | ((c[bArr[2]] & 255) << 6) | (c[bArr[3]] & 255);
                bArr2[0] = (byte) (b2 >> 16);
                bArr2[1] = (byte) (b2 >> 8);
                bArr2[2] = (byte) b2;
                return 3;
            } catch (Exception e) {
                System.out.println(((int) bArr[0]) + ": " + ((int) c[bArr[0]]));
                System.out.println(((int) bArr[1]) + ": " + ((int) c[bArr[1]]));
                System.out.println(((int) bArr[2]) + ": " + ((int) c[bArr[2]]));
                System.out.println(((int) bArr[3]) + ": " + ((int) c[bArr[3]]));
                return -1;
            }
        }
    }
}
