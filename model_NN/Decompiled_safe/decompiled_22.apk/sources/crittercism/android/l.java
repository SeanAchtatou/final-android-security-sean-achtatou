package crittercism.android;

import android.content.Context;
import com.crittercism.app.Crittercism;
import crittercism.android.a;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.json.JSONArray;
import org.json.JSONObject;

public final class l extends j {

    public static class a implements FilenameFilter {
        private String a = new String();

        public a(String str) {
            if (str != null) {
                this.a = str;
            }
        }

        public final boolean accept(File file, String str) {
            return str.endsWith(this.a);
        }
    }

    public l() {
        this.a = a.c.g;
        this.b = new Vector();
    }

    public static l c() {
        String[] list;
        l lVar = new l();
        try {
            Context k = Crittercism.a().k();
            String str = k.getFilesDir().getAbsolutePath() + CookieSpec.PATH_DELIM + Crittercism.a().r();
            String packageName = k.getPackageName();
            if (packageName != null && !packageName.equals("")) {
                File file = new File(str);
                if (file.exists() && (list = file.list(new a(".dmp"))) != null && list.length > 0) {
                    Vector vector = new Vector();
                    for (int i = 0; i < list.length; i++) {
                        "filename = " + file.getAbsolutePath() + CookieSpec.PATH_DELIM + list[i];
                        vector.add(file.getAbsolutePath() + CookieSpec.PATH_DELIM + list[i]);
                    }
                    lVar.a(vector);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lVar;
    }

    public final JSONObject a() {
        JSONArray jSONArray;
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        new JSONObject();
        new JSONArray();
        try {
            jSONArray = new JSONArray((Collection) this.b);
        } catch (Exception e) {
            jSONArray = new JSONArray();
        }
        try {
            jSONObject = Crittercism.a().l().c();
            jSONObject.put("app_state", Crittercism.a().l().d());
            jSONObject.put("num_ndk_crashes", this.b.size());
            jSONObject.put("filenames", jSONArray);
            jSONObject.put("filename_prefix", "ndk_crash_");
        } catch (Exception e2) {
            jSONObject = new JSONObject();
        }
        try {
            jSONObject2.put("requestUrl", this.a);
            jSONObject2.put("requestData", jSONObject);
            return jSONObject2;
        } catch (Exception e3) {
            return new JSONObject();
        }
    }

    public final void b() {
        try {
            if (this.b != null) {
                Iterator it = this.b.iterator();
                while (it.hasNext()) {
                    String str = (String) it.next();
                    "deleting filename: " + str;
                    new File(str).delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
