package crittercism.android;

import com.tencent.mm.sdk.platformtools.LocaleUtil;
import org.json.JSONException;
import org.json.JSONObject;

public final class e {
    public String a = "";
    public String b = "";
    private String c = "";
    private int d = 0;

    public e() {
    }

    private e(String str, int i, String str2) {
        this.c = str;
        this.d = i;
        this.b = str2;
    }

    public static e a(JSONObject jSONObject) {
        try {
            e eVar = new e(jSONObject.getString(LocaleUtil.INDONESIAN), jSONObject.has("votes_left") ? jSONObject.getInt("votes_left") : 0, jSONObject.has("username") ? jSONObject.getString("username") : "anonymous");
            try {
                if (!jSONObject.has("notify")) {
                    return eVar;
                }
                JSONObject jSONObject2 = jSONObject.getJSONObject("notify");
                if (!jSONObject2.has("text")) {
                    return eVar;
                }
                eVar.a = jSONObject2.getString("text");
                return eVar;
            } catch (JSONException e) {
                return eVar;
            }
        } catch (JSONException e2) {
            return null;
        }
    }
}
