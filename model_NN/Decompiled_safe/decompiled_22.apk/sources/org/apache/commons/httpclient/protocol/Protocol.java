package org.apache.commons.httpclient.protocol;

import com.palmtrends.loadimage.ImageFetcher;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.httpclient.util.LangUtils;

public class Protocol {
    private static final Map PROTOCOLS = Collections.synchronizedMap(new HashMap());
    private int defaultPort;
    private String scheme;
    private boolean secure;
    private ProtocolSocketFactory socketFactory;

    public static void registerProtocol(String id, Protocol protocol) {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        } else if (protocol == null) {
            throw new IllegalArgumentException("protocol is null");
        } else {
            PROTOCOLS.put(id, protocol);
        }
    }

    public static void unregisterProtocol(String id) {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        }
        PROTOCOLS.remove(id);
    }

    public static Protocol getProtocol(String id) throws IllegalStateException {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        }
        Protocol protocol = (Protocol) PROTOCOLS.get(id);
        if (protocol == null) {
            return lazyRegisterProtocol(id);
        }
        return protocol;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.protocol.Protocol.<init>(java.lang.String, org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory, int):void
     arg types: [java.lang.String, org.apache.commons.httpclient.protocol.SSLProtocolSocketFactory, int]
     candidates:
      org.apache.commons.httpclient.protocol.Protocol.<init>(java.lang.String, org.apache.commons.httpclient.protocol.ProtocolSocketFactory, int):void
      org.apache.commons.httpclient.protocol.Protocol.<init>(java.lang.String, org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory, int):void */
    private static Protocol lazyRegisterProtocol(String id) throws IllegalStateException {
        if (ImageFetcher.HTTP_CACHE_DIR.equals(id)) {
            Protocol http = new Protocol(ImageFetcher.HTTP_CACHE_DIR, DefaultProtocolSocketFactory.getSocketFactory(), 80);
            registerProtocol(ImageFetcher.HTTP_CACHE_DIR, http);
            return http;
        } else if ("https".equals(id)) {
            Protocol https = new Protocol("https", (SecureProtocolSocketFactory) SSLProtocolSocketFactory.getSocketFactory(), 443);
            registerProtocol("https", https);
            return https;
        } else {
            throw new IllegalStateException(new StringBuffer().append("unsupported protocol: '").append(id).append("'").toString());
        }
    }

    public Protocol(String scheme2, ProtocolSocketFactory factory, int defaultPort2) {
        if (scheme2 == null) {
            throw new IllegalArgumentException("scheme is null");
        } else if (factory == null) {
            throw new IllegalArgumentException("socketFactory is null");
        } else if (defaultPort2 <= 0) {
            throw new IllegalArgumentException(new StringBuffer().append("port is invalid: ").append(defaultPort2).toString());
        } else {
            this.scheme = scheme2;
            this.socketFactory = factory;
            this.defaultPort = defaultPort2;
            this.secure = factory instanceof SecureProtocolSocketFactory;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.protocol.Protocol.<init>(java.lang.String, org.apache.commons.httpclient.protocol.ProtocolSocketFactory, int):void
     arg types: [java.lang.String, org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory, int]
     candidates:
      org.apache.commons.httpclient.protocol.Protocol.<init>(java.lang.String, org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory, int):void
      org.apache.commons.httpclient.protocol.Protocol.<init>(java.lang.String, org.apache.commons.httpclient.protocol.ProtocolSocketFactory, int):void */
    public Protocol(String scheme2, SecureProtocolSocketFactory factory, int defaultPort2) {
        this(scheme2, (ProtocolSocketFactory) factory, defaultPort2);
    }

    public int getDefaultPort() {
        return this.defaultPort;
    }

    public ProtocolSocketFactory getSocketFactory() {
        return this.socketFactory;
    }

    public String getScheme() {
        return this.scheme;
    }

    public boolean isSecure() {
        return this.secure;
    }

    public int resolvePort(int port) {
        return port <= 0 ? getDefaultPort() : port;
    }

    public String toString() {
        return new StringBuffer().append(this.scheme).append(":").append(this.defaultPort).toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Protocol)) {
            return false;
        }
        Protocol p = (Protocol) obj;
        if (this.defaultPort != p.getDefaultPort() || !this.scheme.equalsIgnoreCase(p.getScheme()) || this.secure != p.isSecure() || !this.socketFactory.equals(p.getSocketFactory())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(LangUtils.hashCode(17, this.defaultPort), this.scheme.toLowerCase()), this.secure), this.socketFactory);
    }
}
