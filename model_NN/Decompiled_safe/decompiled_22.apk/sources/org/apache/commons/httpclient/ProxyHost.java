package org.apache.commons.httpclient;

import com.palmtrends.loadimage.ImageFetcher;
import org.apache.commons.httpclient.protocol.Protocol;

public class ProxyHost extends HttpHost {
    public ProxyHost(ProxyHost httpproxy) {
        super(httpproxy);
    }

    public ProxyHost(String hostname, int port) {
        super(hostname, port, Protocol.getProtocol(ImageFetcher.HTTP_CACHE_DIR));
    }

    public ProxyHost(String hostname) {
        this(hostname, -1);
    }

    public Object clone() {
        return new ProxyHost(this);
    }
}
