package org.apache.commons.httpclient;

public class StatusLine {
    private final String httpVersion;
    private final String reasonPhrase;
    private final int statusCode;
    private final String statusLine;

    public StatusLine(String statusLine2) throws HttpException {
        int at;
        int length = statusLine2.length();
        int at2 = 0;
        int start = 0;
        while (true) {
            try {
                at = at2;
                if (!Character.isWhitespace(statusLine2.charAt(at))) {
                    break;
                }
                at2 = at + 1;
                start++;
            } catch (StringIndexOutOfBoundsException e) {
                throw new HttpException(new StringBuffer().append("Status-Line '").append(statusLine2).append("' is not valid").toString());
            }
        }
        int at3 = at + 4;
        try {
            if (!"HTTP".equals(statusLine2.substring(at, at3))) {
                throw new HttpException(new StringBuffer().append("Status-Line '").append(statusLine2).append("' does not start with HTTP").toString());
            }
            int at4 = statusLine2.indexOf(" ", at3);
            if (at4 <= 0) {
                throw new ProtocolException(new StringBuffer().append("Unable to parse HTTP-Version from the status line: '").append(statusLine2).append("'").toString());
            }
            this.httpVersion = statusLine2.substring(start, at4).toUpperCase();
            while (statusLine2.charAt(at4) == ' ') {
                at4++;
            }
            int to = statusLine2.indexOf(" ", at4);
            to = to < 0 ? length : to;
            this.statusCode = Integer.parseInt(statusLine2.substring(at4, to));
            int at5 = to + 1;
            if (at5 < length) {
                this.reasonPhrase = statusLine2.substring(at5).trim();
            } else {
                this.reasonPhrase = "";
            }
            this.statusLine = statusLine2;
        } catch (NumberFormatException e2) {
            throw new ProtocolException(new StringBuffer().append("Unable to parse status code from status line: '").append(statusLine2).append("'").toString());
        } catch (StringIndexOutOfBoundsException e3) {
            throw new HttpException(new StringBuffer().append("Status-Line '").append(statusLine2).append("' is not valid").toString());
        }
    }

    public final int getStatusCode() {
        return this.statusCode;
    }

    public final String getHttpVersion() {
        return this.httpVersion;
    }

    public final String getReasonPhrase() {
        return this.reasonPhrase;
    }

    public final String toString() {
        return this.statusLine;
    }

    public static boolean startsWithHTTP(String s) {
        int at = 0;
        while (Character.isWhitespace(s.charAt(at))) {
            try {
                at++;
            } catch (StringIndexOutOfBoundsException e) {
                return false;
            }
        }
        return "HTTP".equals(s.substring(at, at + 4));
    }
}
