package org.apache.commons.httpclient.protocol;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.params.HttpConnectionParams;

public class DefaultProtocolSocketFactory implements ProtocolSocketFactory {
    static Class class$org$apache$commons$httpclient$protocol$DefaultProtocolSocketFactory;
    private static final DefaultProtocolSocketFactory factory = new DefaultProtocolSocketFactory();

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    static DefaultProtocolSocketFactory getSocketFactory() {
        return factory;
    }

    public Socket createSocket(String host, int port, InetAddress localAddress, int localPort) throws IOException, UnknownHostException {
        return new Socket(host, port, localAddress, localPort);
    }

    public Socket createSocket(String host, int port, InetAddress localAddress, int localPort, HttpConnectionParams params) throws IOException, UnknownHostException, ConnectTimeoutException {
        if (params == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        int timeout = params.getConnectionTimeout();
        if (timeout == 0) {
            return createSocket(host, port, localAddress, localPort);
        }
        Socket socket = ReflectionSocketFactory.createSocket("javax.net.SocketFactory", host, port, localAddress, localPort, timeout);
        if (socket == null) {
            return ControllerThreadSocketFactory.createSocket(this, host, port, localAddress, localPort, timeout);
        }
        return socket;
    }

    public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
        return new Socket(host, port);
    }

    public boolean equals(Object obj) {
        Class cls;
        if (obj != null) {
            Class<?> cls2 = obj.getClass();
            if (class$org$apache$commons$httpclient$protocol$DefaultProtocolSocketFactory == null) {
                cls = class$("org.apache.commons.httpclient.protocol.DefaultProtocolSocketFactory");
                class$org$apache$commons$httpclient$protocol$DefaultProtocolSocketFactory = cls;
            } else {
                cls = class$org$apache$commons$httpclient$protocol$DefaultProtocolSocketFactory;
            }
            if (cls2.equals(cls)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        Class cls;
        if (class$org$apache$commons$httpclient$protocol$DefaultProtocolSocketFactory == null) {
            cls = class$("org.apache.commons.httpclient.protocol.DefaultProtocolSocketFactory");
            class$org$apache$commons$httpclient$protocol$DefaultProtocolSocketFactory = cls;
        } else {
            cls = class$org$apache$commons$httpclient$protocol$DefaultProtocolSocketFactory;
        }
        return cls.hashCode();
    }
}
