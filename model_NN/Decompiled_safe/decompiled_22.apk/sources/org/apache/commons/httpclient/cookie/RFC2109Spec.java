package org.apache.commons.httpclient.cookie;

import com.actionbarsherlock.widget.ActivityChooserView;
import com.tencent.mm.sdk.contact.RContact;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.util.ParameterFormatter;

public class RFC2109Spec extends CookieSpecBase {
    private final ParameterFormatter formatter = new ParameterFormatter();

    public RFC2109Spec() {
        this.formatter.setAlwaysUseQuotes(true);
    }

    public void parseAttribute(NameValuePair attribute, Cookie cookie) throws MalformedCookieException {
        if (attribute == null) {
            throw new IllegalArgumentException("Attribute may not be null.");
        } else if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null.");
        } else {
            String paramName = attribute.getName().toLowerCase();
            String paramValue = attribute.getValue();
            if (paramName.equals("path")) {
                if (paramValue == null) {
                    throw new MalformedCookieException("Missing value for path attribute");
                } else if (paramValue.trim().equals("")) {
                    throw new MalformedCookieException("Blank value for path attribute");
                } else {
                    cookie.setPath(paramValue);
                    cookie.setPathAttributeSpecified(true);
                }
            } else if (!paramName.equals("version")) {
                super.parseAttribute(attribute, cookie);
            } else if (paramValue == null) {
                throw new MalformedCookieException("Missing value for version attribute");
            } else {
                try {
                    cookie.setVersion(Integer.parseInt(paramValue));
                } catch (NumberFormatException e) {
                    throw new MalformedCookieException(new StringBuffer().append("Invalid version: ").append(e.getMessage()).toString());
                }
            }
        }
    }

    public void validate(String host, int port, String path, boolean secure, Cookie cookie) throws MalformedCookieException {
        CookieSpecBase.LOG.trace("enter RFC2109Spec.validate(String, int, String, boolean, Cookie)");
        super.validate(host, port, path, secure, cookie);
        if (cookie.getName().indexOf(32) != -1) {
            throw new MalformedCookieException("Cookie name may not contain blanks");
        } else if (cookie.getName().startsWith(RContact.FAVOUR_CONTACT_SHOW_HEAD_CHAR)) {
            throw new MalformedCookieException("Cookie name may not start with $");
        } else if (cookie.isDomainAttributeSpecified() && !cookie.getDomain().equals(host)) {
            if (!cookie.getDomain().startsWith(".")) {
                throw new MalformedCookieException(new StringBuffer().append("Domain attribute \"").append(cookie.getDomain()).append("\" violates RFC 2109: domain must start with a dot").toString());
            }
            int dotIndex = cookie.getDomain().indexOf(46, 1);
            if (dotIndex < 0 || dotIndex == cookie.getDomain().length() - 1) {
                throw new MalformedCookieException(new StringBuffer().append("Domain attribute \"").append(cookie.getDomain()).append("\" violates RFC 2109: domain must contain an embedded dot").toString());
            }
            String host2 = host.toLowerCase();
            if (!host2.endsWith(cookie.getDomain())) {
                throw new MalformedCookieException(new StringBuffer().append("Illegal domain attribute \"").append(cookie.getDomain()).append("\". Domain of origin: \"").append(host2).append("\"").toString());
            } else if (host2.substring(0, host2.length() - cookie.getDomain().length()).indexOf(46) != -1) {
                throw new MalformedCookieException(new StringBuffer().append("Domain attribute \"").append(cookie.getDomain()).append("\" violates RFC 2109: host minus domain may not contain any dots").toString());
            }
        }
    }

    public boolean domainMatch(String host, String domain) {
        return host.equals(domain) || (domain.startsWith(".") && host.endsWith(domain));
    }

    private void formatParam(StringBuffer buffer, NameValuePair param, int version) {
        if (version < 1) {
            buffer.append(param.getName());
            buffer.append("=");
            if (param.getValue() != null) {
                buffer.append(param.getValue());
                return;
            }
            return;
        }
        this.formatter.format(buffer, param);
    }

    private void formatCookieAsVer(StringBuffer buffer, Cookie cookie, int version) {
        String value = cookie.getValue();
        if (value == null) {
            value = "";
        }
        formatParam(buffer, new NameValuePair(cookie.getName(), value), version);
        if (cookie.getPath() != null && cookie.isPathAttributeSpecified()) {
            buffer.append("; ");
            formatParam(buffer, new NameValuePair("$Path", cookie.getPath()), version);
        }
        if (cookie.getDomain() != null && cookie.isDomainAttributeSpecified()) {
            buffer.append("; ");
            formatParam(buffer, new NameValuePair("$Domain", cookie.getDomain()), version);
        }
    }

    public String formatCookie(Cookie cookie) {
        CookieSpecBase.LOG.trace("enter RFC2109Spec.formatCookie(Cookie)");
        if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        }
        int version = cookie.getVersion();
        StringBuffer buffer = new StringBuffer();
        formatParam(buffer, new NameValuePair("$Version", Integer.toString(version)), version);
        buffer.append("; ");
        formatCookieAsVer(buffer, cookie, version);
        return buffer.toString();
    }

    public String formatCookies(Cookie[] cookies) {
        CookieSpecBase.LOG.trace("enter RFC2109Spec.formatCookieHeader(Cookie[])");
        int version = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        for (Cookie cookie : cookies) {
            if (cookie.getVersion() < version) {
                version = cookie.getVersion();
            }
        }
        StringBuffer buffer = new StringBuffer();
        formatParam(buffer, new NameValuePair("$Version", Integer.toString(version)), version);
        for (Cookie formatCookieAsVer : cookies) {
            buffer.append("; ");
            formatCookieAsVer(buffer, formatCookieAsVer, version);
        }
        return buffer.toString();
    }
}
