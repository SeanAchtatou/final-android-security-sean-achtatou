package org.apache.commons.httpclient;

import java.io.IOException;
import java.net.Socket;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpParams;

public class ProxyClient {
    private HostConfiguration hostConfiguration;
    private HttpClientParams params;
    private HttpState state;

    /* renamed from: org.apache.commons.httpclient.ProxyClient$1  reason: invalid class name */
    class AnonymousClass1 {
    }

    public ProxyClient() {
        this(new HttpClientParams());
    }

    public ProxyClient(HttpClientParams params2) {
        this.state = new HttpState();
        this.params = null;
        this.hostConfiguration = new HostConfiguration();
        if (params2 == null) {
            throw new IllegalArgumentException("Params may not be null");
        }
        this.params = params2;
    }

    public synchronized HttpState getState() {
        return this.state;
    }

    public synchronized void setState(HttpState state2) {
        this.state = state2;
    }

    public synchronized HostConfiguration getHostConfiguration() {
        return this.hostConfiguration;
    }

    public synchronized void setHostConfiguration(HostConfiguration hostConfiguration2) {
        this.hostConfiguration = hostConfiguration2;
    }

    public synchronized HttpClientParams getParams() {
        return this.params;
    }

    public synchronized void setParams(HttpClientParams params2) {
        if (params2 == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        }
        this.params = params2;
    }

    public ConnectResponse connect() throws IOException, HttpException {
        if (getHostConfiguration().getProxyHost() == null) {
            throw new IllegalStateException("proxy host must be configured");
        } else if (getHostConfiguration().getHost() == null) {
            throw new IllegalStateException("destination host must be configured");
        } else {
            ConnectMethod method = new ConnectMethod();
            method.getParams().setDefaults(getParams());
            DummyConnectionManager connectionManager = new DummyConnectionManager();
            connectionManager.setConnectionParams(getParams());
            new HttpMethodDirector(connectionManager, getHostConfiguration(), getParams(), getState()).executeMethod(method);
            ConnectResponse response = new ConnectResponse(null);
            ConnectResponse.access$100(response, method);
            if (method.getStatusCode() == 200) {
                ConnectResponse.access$200(response, connectionManager.getConnection().getSocket());
            } else {
                connectionManager.getConnection().close();
            }
            return response;
        }
    }

    public static class ConnectResponse {
        private ConnectMethod connectMethod;
        private Socket socket;

        ConnectResponse(AnonymousClass1 x0) {
            this();
        }

        static void access$100(ConnectResponse x0, ConnectMethod x1) {
            x0.setConnectMethod(x1);
        }

        static void access$200(ConnectResponse x0, Socket x1) {
            x0.setSocket(x1);
        }

        private ConnectResponse() {
        }

        public ConnectMethod getConnectMethod() {
            return this.connectMethod;
        }

        private void setConnectMethod(ConnectMethod connectMethod2) {
            this.connectMethod = connectMethod2;
        }

        public Socket getSocket() {
            return this.socket;
        }

        private void setSocket(Socket socket2) {
            this.socket = socket2;
        }
    }

    static class DummyConnectionManager implements HttpConnectionManager {
        private HttpParams connectionParams;
        private HttpConnection httpConnection;

        DummyConnectionManager() {
        }

        public void closeIdleConnections(long idleTimeout) {
        }

        public HttpConnection getConnection() {
            return this.httpConnection;
        }

        public void setConnectionParams(HttpParams httpParams) {
            this.connectionParams = httpParams;
        }

        public HttpConnection getConnectionWithTimeout(HostConfiguration hostConfiguration, long timeout) {
            this.httpConnection = new HttpConnection(hostConfiguration);
            this.httpConnection.setHttpConnectionManager(this);
            this.httpConnection.getParams().setDefaults(this.connectionParams);
            return this.httpConnection;
        }

        public HttpConnection getConnection(HostConfiguration hostConfiguration, long timeout) throws HttpException {
            return getConnectionWithTimeout(hostConfiguration, timeout);
        }

        public HttpConnection getConnection(HostConfiguration hostConfiguration) {
            return getConnectionWithTimeout(hostConfiguration, -1);
        }

        public void releaseConnection(HttpConnection conn) {
        }

        public HttpConnectionManagerParams getParams() {
            return null;
        }

        public void setParams(HttpConnectionManagerParams params) {
        }
    }
}
