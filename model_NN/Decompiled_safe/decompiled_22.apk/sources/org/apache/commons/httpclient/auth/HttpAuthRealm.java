package org.apache.commons.httpclient.auth;

public class HttpAuthRealm extends AuthScope {
    public HttpAuthRealm(String domain, String realm) {
        super(domain, -1, realm, AuthScope.ANY_SCHEME);
    }
}
