package javax.mail.internet;

import java.text.ParseException;
import org.apache.commons.httpclient.HttpStatus;

/* compiled from: MailDateFormat */
class MailDateParser {
    int index = 0;
    char[] orig = null;

    public MailDateParser(char[] orig2) {
        this.orig = orig2;
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009 A[Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }, FALL_THROUGH, LOOP:0: B:0:0x0000->B:3:0x0009, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001b A[SYNTHETIC] */
    public void skipUntilNumber() throws java.text.ParseException {
        /*
            r4 = this;
        L_0x0000:
            char[] r1 = r4.orig     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            int r2 = r4.index     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            char r1 = r1[r2]     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            switch(r1) {
                case 48: goto L_0x001b;
                case 49: goto L_0x001b;
                case 50: goto L_0x001b;
                case 51: goto L_0x001b;
                case 52: goto L_0x001b;
                case 53: goto L_0x001b;
                case 54: goto L_0x001b;
                case 55: goto L_0x001b;
                case 56: goto L_0x001b;
                case 57: goto L_0x001b;
                default: goto L_0x0009;
            }     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
        L_0x0009:
            int r1 = r4.index     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            int r1 = r1 + 1
            r4.index = r1     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            goto L_0x0000
        L_0x0010:
            r0 = move-exception
            java.text.ParseException r1 = new java.text.ParseException
            java.lang.String r2 = "No Number Found"
            int r3 = r4.index
            r1.<init>(r2, r3)
            throw r1
        L_0x001b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.MailDateParser.skipUntilNumber():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0008  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void skipWhiteSpace() {
        /*
            r3 = this;
            char[] r1 = r3.orig
            int r0 = r1.length
        L_0x0003:
            int r1 = r3.index
            if (r1 < r0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            char[] r1 = r3.orig
            int r2 = r3.index
            char r1 = r1[r2]
            switch(r1) {
                case 9: goto L_0x0012;
                case 10: goto L_0x0012;
                case 13: goto L_0x0012;
                case 32: goto L_0x0012;
                default: goto L_0x0011;
            }
        L_0x0011:
            goto L_0x0007
        L_0x0012:
            int r1 = r3.index
            int r1 = r1 + 1
            r3.index = r1
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.MailDateParser.skipWhiteSpace():void");
    }

    public int peekChar() throws ParseException {
        if (this.index < this.orig.length) {
            return this.orig[this.index];
        }
        throw new ParseException("No more characters", this.index);
    }

    public void skipChar(char c) throws ParseException {
        if (this.index >= this.orig.length) {
            throw new ParseException("No more characters", this.index);
        } else if (this.orig[this.index] == c) {
            this.index++;
        } else {
            throw new ParseException("Wrong char", this.index);
        }
    }

    public boolean skipIfChar(char c) throws ParseException {
        if (this.index >= this.orig.length) {
            throw new ParseException("No more characters", this.index);
        } else if (this.orig[this.index] != c) {
            return false;
        } else {
            this.index++;
            return true;
        }
    }

    public int parseNumber() throws ParseException {
        int length = this.orig.length;
        boolean gotNum = false;
        int result = 0;
        while (true) {
            if (this.index < length) {
                switch (this.orig[this.index]) {
                    case '0':
                        result *= 10;
                        break;
                    case '1':
                        result = (result * 10) + 1;
                        break;
                    case '2':
                        result = (result * 10) + 2;
                        break;
                    case '3':
                        result = (result * 10) + 3;
                        break;
                    case '4':
                        result = (result * 10) + 4;
                        break;
                    case '5':
                        result = (result * 10) + 5;
                        break;
                    case '6':
                        result = (result * 10) + 6;
                        break;
                    case '7':
                        result = (result * 10) + 7;
                        break;
                    case '8':
                        result = (result * 10) + 8;
                        break;
                    case '9':
                        result = (result * 10) + 9;
                        break;
                    default:
                        if (!gotNum) {
                            throw new ParseException("No Number found", this.index);
                        }
                        break;
                }
                gotNum = true;
                this.index++;
            } else if (!gotNum) {
                throw new ParseException("No Number found", this.index);
            }
        }
        return result;
    }

    public int parseMonth() throws ParseException {
        try {
            char[] cArr = this.orig;
            int i = this.index;
            this.index = i + 1;
            switch (cArr[i]) {
                case 'A':
                case 'a':
                    char[] cArr2 = this.orig;
                    int i2 = this.index;
                    this.index = i2 + 1;
                    char curr = cArr2[i2];
                    if (curr == 'P' || curr == 'p') {
                        char[] cArr3 = this.orig;
                        int i3 = this.index;
                        this.index = i3 + 1;
                        char curr2 = cArr3[i3];
                        if (curr2 == 'R' || curr2 == 'r') {
                            return 3;
                        }
                        throw new ParseException("Bad Month", this.index);
                    }
                    if (curr == 'U' || curr == 'u') {
                        char[] cArr4 = this.orig;
                        int i4 = this.index;
                        this.index = i4 + 1;
                        char curr3 = cArr4[i4];
                        if (curr3 == 'G' || curr3 == 'g') {
                            return 7;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'D':
                case 'd':
                    char[] cArr5 = this.orig;
                    int i5 = this.index;
                    this.index = i5 + 1;
                    char curr4 = cArr5[i5];
                    if (curr4 == 'E' || curr4 == 'e') {
                        char[] cArr6 = this.orig;
                        int i6 = this.index;
                        this.index = i6 + 1;
                        char curr5 = cArr6[i6];
                        if (curr5 == 'C' || curr5 == 'c') {
                            return 11;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'F':
                case HttpStatus.SC_PROCESSING /*102*/:
                    char[] cArr7 = this.orig;
                    int i7 = this.index;
                    this.index = i7 + 1;
                    char curr6 = cArr7[i7];
                    if (curr6 == 'E' || curr6 == 'e') {
                        char[] cArr8 = this.orig;
                        int i8 = this.index;
                        this.index = i8 + 1;
                        char curr7 = cArr8[i8];
                        if (curr7 == 'B' || curr7 == 'b') {
                            return 1;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'J':
                case 'j':
                    char[] cArr9 = this.orig;
                    int i9 = this.index;
                    this.index = i9 + 1;
                    switch (cArr9[i9]) {
                        case 'A':
                        case 'a':
                            char[] cArr10 = this.orig;
                            int i10 = this.index;
                            this.index = i10 + 1;
                            char curr8 = cArr10[i10];
                            if (curr8 == 'N' || curr8 == 'n') {
                                return 0;
                            }
                        case 'U':
                        case 'u':
                            char[] cArr11 = this.orig;
                            int i11 = this.index;
                            this.index = i11 + 1;
                            char curr9 = cArr11[i11];
                            if (curr9 == 'N' || curr9 == 'n') {
                                return 5;
                            }
                            if (curr9 == 'L' || curr9 == 'l') {
                                return 6;
                            }
                            break;
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'M':
                case 'm':
                    char[] cArr12 = this.orig;
                    int i12 = this.index;
                    this.index = i12 + 1;
                    char curr10 = cArr12[i12];
                    if (curr10 == 'A' || curr10 == 'a') {
                        char[] cArr13 = this.orig;
                        int i13 = this.index;
                        this.index = i13 + 1;
                        char curr11 = cArr13[i13];
                        if (curr11 == 'R' || curr11 == 'r') {
                            return 2;
                        }
                        if (curr11 == 'Y' || curr11 == 'y') {
                            return 4;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'N':
                case 'n':
                    char[] cArr14 = this.orig;
                    int i14 = this.index;
                    this.index = i14 + 1;
                    char curr12 = cArr14[i14];
                    if (curr12 == 'O' || curr12 == 'o') {
                        char[] cArr15 = this.orig;
                        int i15 = this.index;
                        this.index = i15 + 1;
                        char curr13 = cArr15[i15];
                        if (curr13 == 'V' || curr13 == 'v') {
                            return 10;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'O':
                case 'o':
                    char[] cArr16 = this.orig;
                    int i16 = this.index;
                    this.index = i16 + 1;
                    char curr14 = cArr16[i16];
                    if (curr14 == 'C' || curr14 == 'c') {
                        char[] cArr17 = this.orig;
                        int i17 = this.index;
                        this.index = i17 + 1;
                        char curr15 = cArr17[i17];
                        if (curr15 == 'T' || curr15 == 't') {
                            return 9;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'S':
                case 's':
                    char[] cArr18 = this.orig;
                    int i18 = this.index;
                    this.index = i18 + 1;
                    char curr16 = cArr18[i18];
                    if (curr16 == 'E' || curr16 == 'e') {
                        char[] cArr19 = this.orig;
                        int i19 = this.index;
                        this.index = i19 + 1;
                        char curr17 = cArr19[i19];
                        if (curr17 == 'P' || curr17 == 'p') {
                            return 8;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                default:
                    throw new ParseException("Bad Month", this.index);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    public int parseTimeZone() throws ParseException {
        if (this.index >= this.orig.length) {
            throw new ParseException("No more characters", this.index);
        }
        char test = this.orig[this.index];
        if (test == '+' || test == '-') {
            return parseNumericTimeZone();
        }
        return parseAlphaTimeZone();
    }

    public int parseNumericTimeZone() throws ParseException {
        boolean switchSign = false;
        char[] cArr = this.orig;
        int i = this.index;
        this.index = i + 1;
        char first = cArr[i];
        if (first == '+') {
            switchSign = true;
        } else if (first != '-') {
            throw new ParseException("Bad Numeric TimeZone", this.index);
        }
        int tz = parseNumber();
        int offset = ((tz / 100) * 60) + (tz % 100);
        if (switchSign) {
            return -offset;
        }
        return offset;
    }

    public int parseAlphaTimeZone() throws ParseException {
        int result;
        boolean foundCommon = false;
        try {
            char[] cArr = this.orig;
            int i = this.index;
            this.index = i + 1;
            switch (cArr[i]) {
                case 'C':
                case 'c':
                    result = 360;
                    foundCommon = true;
                    break;
                case 'E':
                case HttpStatus.SC_SWITCHING_PROTOCOLS /*101*/:
                    result = 300;
                    foundCommon = true;
                    break;
                case 'G':
                case 'g':
                    char[] cArr2 = this.orig;
                    int i2 = this.index;
                    this.index = i2 + 1;
                    char curr = cArr2[i2];
                    if (curr == 'M' || curr == 'm') {
                        char[] cArr3 = this.orig;
                        int i3 = this.index;
                        this.index = i3 + 1;
                        char curr2 = cArr3[i3];
                        if (curr2 == 'T' || curr2 == 't') {
                            result = 0;
                            break;
                        }
                    }
                    throw new ParseException("Bad Alpha TimeZone", this.index);
                case 'M':
                case 'm':
                    result = HttpStatus.SC_METHOD_FAILURE;
                    foundCommon = true;
                    break;
                case 'P':
                case 'p':
                    result = 480;
                    foundCommon = true;
                    break;
                case 'U':
                case 'u':
                    char[] cArr4 = this.orig;
                    int i4 = this.index;
                    this.index = i4 + 1;
                    char curr3 = cArr4[i4];
                    if (curr3 == 'T' || curr3 == 't') {
                        result = 0;
                        break;
                    } else {
                        throw new ParseException("Bad Alpha TimeZone", this.index);
                    }
                default:
                    throw new ParseException("Bad Alpha TimeZone", this.index);
            }
            if (!foundCommon) {
                return result;
            }
            char[] cArr5 = this.orig;
            int i5 = this.index;
            this.index = i5 + 1;
            char curr4 = cArr5[i5];
            if (curr4 == 'S' || curr4 == 's') {
                char[] cArr6 = this.orig;
                int i6 = this.index;
                this.index = i6 + 1;
                char curr5 = cArr6[i6];
                if (curr5 == 'T' || curr5 == 't') {
                    return result;
                }
                throw new ParseException("Bad Alpha TimeZone", this.index);
            } else if (curr4 != 'D' && curr4 != 'd') {
                return result;
            } else {
                char[] cArr7 = this.orig;
                int i7 = this.index;
                this.index = i7 + 1;
                char curr6 = cArr7[i7];
                if (curr6 == 'T' || curr6 != 't') {
                    return result - 60;
                }
                throw new ParseException("Bad Alpha TimeZone", this.index);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ParseException("Bad Alpha TimeZone", this.index);
        }
    }

    /* access modifiers changed from: package-private */
    public int getIndex() {
        return this.index;
    }
}
