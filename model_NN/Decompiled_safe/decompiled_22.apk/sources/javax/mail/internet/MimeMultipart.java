package javax.mail.internet;

import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.LineOutputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.MessageAware;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.MultipartDataSource;
import org.apache.commons.httpclient.HttpState;

public class MimeMultipart extends Multipart {
    private static boolean bmparse;
    private static boolean ignoreMissingBoundaryParameter;
    private static boolean ignoreMissingEndBoundary;
    private boolean complete;
    protected DataSource ds;
    protected boolean parsed;
    private String preamble;

    static {
        boolean z;
        boolean z2 = false;
        ignoreMissingEndBoundary = true;
        ignoreMissingBoundaryParameter = true;
        bmparse = true;
        try {
            String s = System.getProperty("mail.mime.multipart.ignoremissingendboundary");
            ignoreMissingEndBoundary = s == null || !s.equalsIgnoreCase(HttpState.PREEMPTIVE_DEFAULT);
            String s2 = System.getProperty("mail.mime.multipart.ignoremissingboundaryparameter");
            if (s2 == null || !s2.equalsIgnoreCase(HttpState.PREEMPTIVE_DEFAULT)) {
                z = true;
            } else {
                z = false;
            }
            ignoreMissingBoundaryParameter = z;
            String s3 = System.getProperty("mail.mime.multipart.bmparse");
            if (s3 == null || !s3.equalsIgnoreCase(HttpState.PREEMPTIVE_DEFAULT)) {
                z2 = true;
            }
            bmparse = z2;
        } catch (SecurityException e) {
        }
    }

    public MimeMultipart() {
        this("mixed");
    }

    public MimeMultipart(String subtype) {
        this.ds = null;
        this.parsed = true;
        this.complete = true;
        this.preamble = null;
        String boundary = UniqueValue.getUniqueBoundaryValue();
        ContentType cType = new ContentType("multipart", subtype, null);
        cType.setParameter("boundary", boundary);
        this.contentType = cType.toString();
    }

    public MimeMultipart(DataSource ds2) throws MessagingException {
        this.ds = null;
        this.parsed = true;
        this.complete = true;
        this.preamble = null;
        if (ds2 instanceof MessageAware) {
            setParent(((MessageAware) ds2).getMessageContext().getPart());
        }
        if (ds2 instanceof MultipartDataSource) {
            setMultipartDataSource((MultipartDataSource) ds2);
            return;
        }
        this.parsed = false;
        this.ds = ds2;
        this.contentType = ds2.getContentType();
    }

    public synchronized void setSubType(String subtype) throws MessagingException {
        ContentType cType = new ContentType(this.contentType);
        cType.setSubType(subtype);
        this.contentType = cType.toString();
    }

    public synchronized int getCount() throws MessagingException {
        parse();
        return super.getCount();
    }

    public synchronized BodyPart getBodyPart(int index) throws MessagingException {
        parse();
        return super.getBodyPart(index);
    }

    public synchronized BodyPart getBodyPart(String CID) throws MessagingException {
        MimeBodyPart part;
        parse();
        int count = getCount();
        int i = 0;
        while (true) {
            if (i < count) {
                part = (MimeBodyPart) getBodyPart(i);
                String s = part.getContentID();
                if (s != null && s.equals(CID)) {
                    break;
                }
                i++;
            } else {
                part = null;
                break;
            }
        }
        return part;
    }

    public boolean removeBodyPart(BodyPart part) throws MessagingException {
        parse();
        return super.removeBodyPart(part);
    }

    public void removeBodyPart(int index) throws MessagingException {
        parse();
        super.removeBodyPart(index);
    }

    public synchronized void addBodyPart(BodyPart part) throws MessagingException {
        parse();
        super.addBodyPart(part);
    }

    public synchronized void addBodyPart(BodyPart part, int index) throws MessagingException {
        parse();
        super.addBodyPart(part, index);
    }

    public synchronized boolean isComplete() throws MessagingException {
        parse();
        return this.complete;
    }

    public synchronized String getPreamble() throws MessagingException {
        parse();
        return this.preamble;
    }

    public synchronized void setPreamble(String preamble2) throws MessagingException {
        this.preamble = preamble2;
    }

    /* access modifiers changed from: protected */
    public void updateHeaders() throws MessagingException {
        for (int i = 0; i < this.parts.size(); i++) {
            ((MimeBodyPart) this.parts.elementAt(i)).updateHeaders();
        }
    }

    public synchronized void writeTo(OutputStream os) throws IOException, MessagingException {
        parse();
        String boundary = "--" + new ContentType(this.contentType).getParameter("boundary");
        LineOutputStream los = new LineOutputStream(os);
        if (this.preamble != null) {
            byte[] pb = ASCIIUtility.getBytes(this.preamble);
            los.write(pb);
            if (!(pb.length <= 0 || pb[pb.length - 1] == 13 || pb[pb.length - 1] == 10)) {
                los.writeln();
            }
        }
        for (int i = 0; i < this.parts.size(); i++) {
            los.writeln(boundary);
            ((MimeBodyPart) this.parts.elementAt(i)).writeTo(os);
            los.writeln();
        }
        los.writeln(String.valueOf(boundary) + "--");
    }

    /* JADX WARN: Type inference failed for: r32v40, types: [int] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void parse() throws javax.mail.MessagingException {
        /*
            r36 = this;
            monitor-enter(r36)
            r0 = r36
            boolean r0 = r0.parsed     // Catch:{ all -> 0x0013 }
            r32 = r0
            if (r32 == 0) goto L_0x000b
        L_0x0009:
            monitor-exit(r36)
            return
        L_0x000b:
            boolean r32 = javax.mail.internet.MimeMultipart.bmparse     // Catch:{ all -> 0x0013 }
            if (r32 == 0) goto L_0x0016
            r36.parsebm()     // Catch:{ all -> 0x0013 }
            goto L_0x0009
        L_0x0013:
            r32 = move-exception
            monitor-exit(r36)
            throw r32
        L_0x0016:
            r21 = 0
            r29 = 0
            r30 = 0
            r14 = 0
            r0 = r36
            javax.activation.DataSource r0 = r0.ds     // Catch:{ Exception -> 0x00b3 }
            r32 = r0
            java.io.InputStream r21 = r32.getInputStream()     // Catch:{ Exception -> 0x00b3 }
            r0 = r21
            boolean r0 = r0 instanceof java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x00b3 }
            r32 = r0
            if (r32 != 0) goto L_0x004b
            r0 = r21
            boolean r0 = r0 instanceof java.io.BufferedInputStream     // Catch:{ Exception -> 0x00b3 }
            r32 = r0
            if (r32 != 0) goto L_0x004b
            r0 = r21
            boolean r0 = r0 instanceof javax.mail.internet.SharedInputStream     // Catch:{ Exception -> 0x00b3 }
            r32 = r0
            if (r32 != 0) goto L_0x004b
            java.io.BufferedInputStream r22 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00b3 }
            r0 = r22
            r1 = r21
            r0.<init>(r1)     // Catch:{ Exception -> 0x00b3 }
            r21 = r22
        L_0x004b:
            r0 = r21
            boolean r0 = r0 instanceof javax.mail.internet.SharedInputStream     // Catch:{ all -> 0x0013 }
            r32 = r0
            if (r32 == 0) goto L_0x0059
            r0 = r21
            javax.mail.internet.SharedInputStream r0 = (javax.mail.internet.SharedInputStream) r0     // Catch:{ all -> 0x0013 }
            r29 = r0
        L_0x0059:
            javax.mail.internet.ContentType r12 = new javax.mail.internet.ContentType     // Catch:{ all -> 0x0013 }
            r0 = r36
            java.lang.String r0 = r0.contentType     // Catch:{ all -> 0x0013 }
            r32 = r0
            r0 = r32
            r12.<init>(r0)     // Catch:{ all -> 0x0013 }
            r8 = 0
            java.lang.String r32 = "boundary"
            r0 = r32
            java.lang.String r9 = r12.getParameter(r0)     // Catch:{ all -> 0x0013 }
            if (r9 == 0) goto L_0x00c2
            java.lang.StringBuilder r32 = new java.lang.StringBuilder     // Catch:{ all -> 0x0013 }
            java.lang.String r33 = "--"
            r32.<init>(r33)     // Catch:{ all -> 0x0013 }
            r0 = r32
            java.lang.StringBuilder r32 = r0.append(r9)     // Catch:{ all -> 0x0013 }
            java.lang.String r8 = r32.toString()     // Catch:{ all -> 0x0013 }
        L_0x0082:
            com.sun.mail.util.LineInputStream r24 = new com.sun.mail.util.LineInputStream     // Catch:{ IOException -> 0x009f }
            r0 = r24
            r1 = r21
            r0.<init>(r1)     // Catch:{ IOException -> 0x009f }
            r28 = 0
            r26 = 0
        L_0x008f:
            java.lang.String r25 = r24.readLine()     // Catch:{ IOException -> 0x009f }
            if (r25 != 0) goto L_0x00ce
        L_0x0095:
            if (r25 != 0) goto L_0x014b
            javax.mail.MessagingException r32 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x009f }
            java.lang.String r33 = "Missing start boundary"
            r32.<init>(r33)     // Catch:{ IOException -> 0x009f }
            throw r32     // Catch:{ IOException -> 0x009f }
        L_0x009f:
            r23 = move-exception
            javax.mail.MessagingException r32 = new javax.mail.MessagingException     // Catch:{ all -> 0x00ae }
            java.lang.String r33 = "IO Error"
            r0 = r32
            r1 = r33
            r2 = r23
            r0.<init>(r1, r2)     // Catch:{ all -> 0x00ae }
            throw r32     // Catch:{ all -> 0x00ae }
        L_0x00ae:
            r32 = move-exception
            r21.close()     // Catch:{ IOException -> 0x02f1 }
        L_0x00b2:
            throw r32     // Catch:{ all -> 0x0013 }
        L_0x00b3:
            r18 = move-exception
            javax.mail.MessagingException r32 = new javax.mail.MessagingException     // Catch:{ all -> 0x0013 }
            java.lang.String r33 = "No inputstream from datasource"
            r0 = r32
            r1 = r33
            r2 = r18
            r0.<init>(r1, r2)     // Catch:{ all -> 0x0013 }
            throw r32     // Catch:{ all -> 0x0013 }
        L_0x00c2:
            boolean r32 = javax.mail.internet.MimeMultipart.ignoreMissingBoundaryParameter     // Catch:{ all -> 0x0013 }
            if (r32 != 0) goto L_0x0082
            javax.mail.MessagingException r32 = new javax.mail.MessagingException     // Catch:{ all -> 0x0013 }
            java.lang.String r33 = "Missing boundary parameter"
            r32.<init>(r33)     // Catch:{ all -> 0x0013 }
            throw r32     // Catch:{ all -> 0x0013 }
        L_0x00ce:
            int r32 = r25.length()     // Catch:{ IOException -> 0x009f }
            int r20 = r32 + -1
        L_0x00d4:
            if (r20 >= 0) goto L_0x0120
        L_0x00d6:
            r32 = 0
            int r33 = r20 + 1
            r0 = r25
            r1 = r32
            r2 = r33
            java.lang.String r25 = r0.substring(r1, r2)     // Catch:{ IOException -> 0x009f }
            if (r8 == 0) goto L_0x0137
            r0 = r25
            boolean r32 = r0.equals(r8)     // Catch:{ IOException -> 0x009f }
            if (r32 != 0) goto L_0x0095
        L_0x00ee:
            int r32 = r25.length()     // Catch:{ IOException -> 0x009f }
            if (r32 <= 0) goto L_0x008f
            if (r26 != 0) goto L_0x00fe
            java.lang.String r32 = "line.separator"
            java.lang.String r33 = "\n"
            java.lang.String r26 = java.lang.System.getProperty(r32, r33)     // Catch:{ SecurityException -> 0x0147 }
        L_0x00fe:
            if (r28 != 0) goto L_0x010f
            java.lang.StringBuffer r28 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x009f }
            int r32 = r25.length()     // Catch:{ IOException -> 0x009f }
            int r32 = r32 + 2
            r0 = r28
            r1 = r32
            r0.<init>(r1)     // Catch:{ IOException -> 0x009f }
        L_0x010f:
            r0 = r28
            r1 = r25
            java.lang.StringBuffer r32 = r0.append(r1)     // Catch:{ IOException -> 0x009f }
            r0 = r32
            r1 = r26
            r0.append(r1)     // Catch:{ IOException -> 0x009f }
            goto L_0x008f
        L_0x0120:
            r0 = r25
            r1 = r20
            char r11 = r0.charAt(r1)     // Catch:{ IOException -> 0x009f }
            r32 = 32
            r0 = r32
            if (r11 == r0) goto L_0x0134
            r32 = 9
            r0 = r32
            if (r11 != r0) goto L_0x00d6
        L_0x0134:
            int r20 = r20 + -1
            goto L_0x00d4
        L_0x0137:
            java.lang.String r32 = "--"
            r0 = r25
            r1 = r32
            boolean r32 = r0.startsWith(r1)     // Catch:{ IOException -> 0x009f }
            if (r32 == 0) goto L_0x00ee
            r8 = r25
            goto L_0x0095
        L_0x0147:
            r18 = move-exception
            java.lang.String r26 = "\n"
            goto L_0x00fe
        L_0x014b:
            if (r28 == 0) goto L_0x0157
            java.lang.String r32 = r28.toString()     // Catch:{ IOException -> 0x009f }
            r0 = r32
            r1 = r36
            r1.preamble = r0     // Catch:{ IOException -> 0x009f }
        L_0x0157:
            byte[] r6 = com.sun.mail.util.ASCIIUtility.getBytes(r8)     // Catch:{ IOException -> 0x009f }
            int r5 = r6.length     // Catch:{ IOException -> 0x009f }
            r13 = 0
        L_0x015d:
            if (r13 == 0) goto L_0x016c
        L_0x015f:
            r21.close()     // Catch:{ IOException -> 0x02f4 }
        L_0x0162:
            r32 = 1
            r0 = r32
            r1 = r36
            r1.parsed = r0     // Catch:{ all -> 0x0013 }
            goto L_0x0009
        L_0x016c:
            r19 = 0
            if (r29 == 0) goto L_0x0197
            long r30 = r29.getPosition()     // Catch:{ IOException -> 0x009f }
        L_0x0174:
            java.lang.String r25 = r24.readLine()     // Catch:{ IOException -> 0x009f }
            if (r25 == 0) goto L_0x0180
            int r32 = r25.length()     // Catch:{ IOException -> 0x009f }
            if (r32 > 0) goto L_0x0174
        L_0x0180:
            if (r25 != 0) goto L_0x019f
            boolean r32 = javax.mail.internet.MimeMultipart.ignoreMissingEndBoundary     // Catch:{ IOException -> 0x009f }
            if (r32 != 0) goto L_0x018e
            javax.mail.MessagingException r32 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x009f }
            java.lang.String r33 = "missing multipart end boundary"
            r32.<init>(r33)     // Catch:{ IOException -> 0x009f }
            throw r32     // Catch:{ IOException -> 0x009f }
        L_0x018e:
            r32 = 0
            r0 = r32
            r1 = r36
            r1.complete = r0     // Catch:{ IOException -> 0x009f }
            goto L_0x015f
        L_0x0197:
            r0 = r36
            r1 = r21
            javax.mail.internet.InternetHeaders r19 = r0.createInternetHeaders(r1)     // Catch:{ IOException -> 0x009f }
        L_0x019f:
            boolean r32 = r21.markSupported()     // Catch:{ IOException -> 0x009f }
            if (r32 != 0) goto L_0x01ad
            javax.mail.MessagingException r32 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x009f }
            java.lang.String r33 = "Stream doesn't support mark"
            r32.<init>(r33)     // Catch:{ IOException -> 0x009f }
            throw r32     // Catch:{ IOException -> 0x009f }
        L_0x01ad:
            r10 = 0
            if (r29 != 0) goto L_0x020f
            java.io.ByteArrayOutputStream r10 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x009f }
            r10.<init>()     // Catch:{ IOException -> 0x009f }
        L_0x01b5:
            r7 = 1
            r16 = -1
            r17 = -1
        L_0x01ba:
            if (r7 == 0) goto L_0x0281
            int r32 = r5 + 4
            r0 = r32
            int r0 = r0 + 1000
            r32 = r0
            r0 = r21
            r1 = r32
            r0.mark(r1)     // Catch:{ IOException -> 0x009f }
            r20 = 0
        L_0x01cd:
            r0 = r20
            if (r0 < r5) goto L_0x0214
        L_0x01d1:
            r0 = r20
            if (r0 != r5) goto L_0x025e
            int r4 = r21.read()     // Catch:{ IOException -> 0x009f }
            r32 = 45
            r0 = r32
            if (r4 != r0) goto L_0x022d
            int r32 = r21.read()     // Catch:{ IOException -> 0x009f }
            r33 = 45
            r0 = r32
            r1 = r33
            if (r0 != r1) goto L_0x022d
            r32 = 1
            r0 = r32
            r1 = r36
            r1.complete = r0     // Catch:{ IOException -> 0x009f }
            r13 = 1
        L_0x01f4:
            if (r29 == 0) goto L_0x02e1
            r0 = r29
            r1 = r30
            java.io.InputStream r32 = r0.newStream(r1, r14)     // Catch:{ IOException -> 0x009f }
            r0 = r36
            r1 = r32
            javax.mail.internet.MimeBodyPart r27 = r0.createMimeBodyPart(r1)     // Catch:{ IOException -> 0x009f }
        L_0x0206:
            r0 = r36
            r1 = r27
            super.addBodyPart(r1)     // Catch:{ IOException -> 0x009f }
            goto L_0x015d
        L_0x020f:
            long r14 = r29.getPosition()     // Catch:{ IOException -> 0x009f }
            goto L_0x01b5
        L_0x0214:
            int r32 = r21.read()     // Catch:{ IOException -> 0x009f }
            byte r33 = r6[r20]     // Catch:{ IOException -> 0x009f }
            r0 = r33
            r0 = r0 & 255(0xff, float:3.57E-43)
            r33 = r0
            r0 = r32
            r1 = r33
            if (r0 != r1) goto L_0x01d1
            int r20 = r20 + 1
            goto L_0x01cd
        L_0x0229:
            int r4 = r21.read()     // Catch:{ IOException -> 0x009f }
        L_0x022d:
            r32 = 32
            r0 = r32
            if (r4 == r0) goto L_0x0229
            r32 = 9
            r0 = r32
            if (r4 == r0) goto L_0x0229
            r32 = 10
            r0 = r32
            if (r4 == r0) goto L_0x01f4
            r32 = 13
            r0 = r32
            if (r4 != r0) goto L_0x025e
            r32 = 1
            r0 = r21
            r1 = r32
            r0.mark(r1)     // Catch:{ IOException -> 0x009f }
            int r32 = r21.read()     // Catch:{ IOException -> 0x009f }
            r33 = 10
            r0 = r32
            r1 = r33
            if (r0 == r1) goto L_0x01f4
            r21.reset()     // Catch:{ IOException -> 0x009f }
            goto L_0x01f4
        L_0x025e:
            r21.reset()     // Catch:{ IOException -> 0x009f }
            if (r10 == 0) goto L_0x0281
            r32 = -1
            r0 = r16
            r1 = r32
            if (r0 == r1) goto L_0x0281
            r0 = r16
            r10.write(r0)     // Catch:{ IOException -> 0x009f }
            r32 = -1
            r0 = r17
            r1 = r32
            if (r0 == r1) goto L_0x027d
            r0 = r17
            r10.write(r0)     // Catch:{ IOException -> 0x009f }
        L_0x027d:
            r17 = -1
            r16 = r17
        L_0x0281:
            int r3 = r21.read()     // Catch:{ IOException -> 0x009f }
            if (r3 >= 0) goto L_0x029e
            boolean r32 = javax.mail.internet.MimeMultipart.ignoreMissingEndBoundary     // Catch:{ IOException -> 0x009f }
            if (r32 != 0) goto L_0x0293
            javax.mail.MessagingException r32 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x009f }
            java.lang.String r33 = "missing multipart end boundary"
            r32.<init>(r33)     // Catch:{ IOException -> 0x009f }
            throw r32     // Catch:{ IOException -> 0x009f }
        L_0x0293:
            r32 = 0
            r0 = r32
            r1 = r36
            r1.complete = r0     // Catch:{ IOException -> 0x009f }
            r13 = 1
            goto L_0x01f4
        L_0x029e:
            r32 = 13
            r0 = r32
            if (r3 == r0) goto L_0x02aa
            r32 = 10
            r0 = r32
            if (r3 != r0) goto L_0x02d9
        L_0x02aa:
            r7 = 1
            if (r29 == 0) goto L_0x02b5
            long r32 = r29.getPosition()     // Catch:{ IOException -> 0x009f }
            r34 = 1
            long r14 = r32 - r34
        L_0x02b5:
            r16 = r3
            r32 = 13
            r0 = r32
            if (r3 != r0) goto L_0x01ba
            r32 = 1
            r0 = r21
            r1 = r32
            r0.mark(r1)     // Catch:{ IOException -> 0x009f }
            int r3 = r21.read()     // Catch:{ IOException -> 0x009f }
            r32 = 10
            r0 = r32
            if (r3 != r0) goto L_0x02d4
            r17 = r3
            goto L_0x01ba
        L_0x02d4:
            r21.reset()     // Catch:{ IOException -> 0x009f }
            goto L_0x01ba
        L_0x02d9:
            r7 = 0
            if (r10 == 0) goto L_0x01ba
            r10.write(r3)     // Catch:{ IOException -> 0x009f }
            goto L_0x01ba
        L_0x02e1:
            byte[] r32 = r10.toByteArray()     // Catch:{ IOException -> 0x009f }
            r0 = r36
            r1 = r19
            r2 = r32
            javax.mail.internet.MimeBodyPart r27 = r0.createMimeBodyPart(r1, r2)     // Catch:{ IOException -> 0x009f }
            goto L_0x0206
        L_0x02f1:
            r33 = move-exception
            goto L_0x00b2
        L_0x02f4:
            r32 = move-exception
            goto L_0x0162
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.MimeMultipart.parse():void");
    }

    private synchronized void parsebm() throws MessagingException {
        String line;
        int eolLen;
        int inSize;
        byte b;
        MimeBodyPart part;
        String line2;
        if (!this.parsed) {
            SharedInputStream sin = null;
            long start = 0;
            long end = 0;
            try {
                InputStream in = this.ds.getInputStream();
                if (!(in instanceof ByteArrayInputStream) && !(in instanceof BufferedInputStream) && !(in instanceof SharedInputStream)) {
                    in = new BufferedInputStream(in);
                }
                if (in instanceof SharedInputStream) {
                    sin = (SharedInputStream) in;
                }
                String boundary = null;
                String bp = new ContentType(this.contentType).getParameter("boundary");
                if (bp != null) {
                    boundary = "--" + bp;
                } else if (!ignoreMissingBoundaryParameter) {
                    throw new MessagingException("Missing boundary parameter");
                }
                try {
                    LineInputStream lineInputStream = new LineInputStream(in);
                    StringBuffer preamblesb = null;
                    String lineSeparator = null;
                    while (true) {
                        line = lineInputStream.readLine();
                        if (line == null) {
                            break;
                        }
                        int i = line.length() - 1;
                        while (i >= 0) {
                            char c = line.charAt(i);
                            if (c != ' ' && c != 9) {
                                break;
                            }
                            i--;
                        }
                        line = line.substring(0, i + 1);
                        if (boundary == null) {
                            if (line.startsWith("--")) {
                                boundary = line;
                                break;
                            }
                        } else if (line.equals(boundary)) {
                            break;
                        }
                        if (line.length() > 0) {
                            if (lineSeparator == null) {
                                try {
                                    lineSeparator = System.getProperty("line.separator", "\n");
                                } catch (SecurityException e) {
                                    lineSeparator = "\n";
                                }
                            }
                            if (preamblesb == null) {
                                preamblesb = new StringBuffer(line.length() + 2);
                            }
                            preamblesb.append(line).append(lineSeparator);
                        }
                    }
                    if (line != null) {
                        if (preamblesb != null) {
                            this.preamble = preamblesb.toString();
                        }
                        byte[] bndbytes = ASCIIUtility.getBytes(boundary);
                        int bl = bndbytes.length;
                        int[] bcs = new int[256];
                        for (int i2 = 0; i2 < bl; i2++) {
                            bcs[bndbytes[i2]] = i2 + 1;
                        }
                        int[] gss = new int[bl];
                        for (int i3 = bl; i3 > 0; i3--) {
                            int j = bl - 1;
                            while (true) {
                                if (j >= i3) {
                                    if (bndbytes[j] != bndbytes[j - i3]) {
                                        break;
                                    }
                                    gss[j - 1] = i3;
                                    j--;
                                } else {
                                    while (j > 0) {
                                        j--;
                                        gss[j] = i3;
                                    }
                                }
                            }
                        }
                        gss[bl - 1] = 1;
                        boolean done = false;
                        while (true) {
                            if (!done) {
                                InternetHeaders headers = null;
                                if (sin != null) {
                                    start = sin.getPosition();
                                    do {
                                        line2 = lineInputStream.readLine();
                                        if (line2 == null) {
                                            break;
                                        }
                                    } while (line2.length() > 0);
                                    if (line2 == null) {
                                        if (!ignoreMissingEndBoundary) {
                                            throw new MessagingException("missing multipart end boundary");
                                        }
                                        this.complete = false;
                                    }
                                } else {
                                    headers = createInternetHeaders(in);
                                }
                                if (!in.markSupported()) {
                                    throw new MessagingException("Stream doesn't support mark");
                                }
                                ByteArrayOutputStream buf = null;
                                if (sin == null) {
                                    buf = new ByteArrayOutputStream();
                                } else {
                                    end = sin.getPosition();
                                }
                                byte[] inbuf = new byte[bl];
                                byte[] previnbuf = new byte[bl];
                                int prevSize = 0;
                                boolean first = true;
                                while (true) {
                                    in.mark(bl + 4 + 1000);
                                    eolLen = 0;
                                    inSize = readFully(in, inbuf, 0, bl);
                                    if (inSize >= bl) {
                                        int i4 = bl - 1;
                                        while (i4 >= 0 && inbuf[i4] == bndbytes[i4]) {
                                            i4--;
                                        }
                                        if (i4 < 0) {
                                            eolLen = 0;
                                            if (!first && ((b = previnbuf[prevSize - 1]) == 13 || b == 10)) {
                                                eolLen = 1;
                                                if (b == 10 && prevSize >= 2 && previnbuf[prevSize - 2] == 13) {
                                                    eolLen = 2;
                                                }
                                            }
                                            if (first || eolLen > 0) {
                                                if (sin != null) {
                                                    end = (sin.getPosition() - ((long) bl)) - ((long) eolLen);
                                                }
                                                int b2 = in.read();
                                                if (b2 == 45 && in.read() == 45) {
                                                    this.complete = true;
                                                    done = true;
                                                    break;
                                                }
                                                while (true) {
                                                    if (b2 != 32 && b2 != 9) {
                                                        break;
                                                    }
                                                    b2 = in.read();
                                                }
                                                if (b2 == 10) {
                                                    break;
                                                } else if (b2 == 13) {
                                                    in.mark(1);
                                                    if (in.read() != 10) {
                                                        in.reset();
                                                    }
                                                }
                                            }
                                            i4 = 0;
                                        }
                                        int skip = Math.max((i4 + 1) - bcs[inbuf[i4] & Byte.MAX_VALUE], gss[i4]);
                                        if (skip < 2) {
                                            if (sin == null && prevSize > 1) {
                                                buf.write(previnbuf, 0, prevSize - 1);
                                            }
                                            in.reset();
                                            skipFully(in, 1);
                                            if (prevSize >= 1) {
                                                previnbuf[0] = previnbuf[prevSize - 1];
                                                previnbuf[1] = inbuf[0];
                                                prevSize = 2;
                                            } else {
                                                previnbuf[0] = inbuf[0];
                                                prevSize = 1;
                                            }
                                        } else {
                                            if (prevSize > 0 && sin == null) {
                                                buf.write(previnbuf, 0, prevSize);
                                            }
                                            prevSize = skip;
                                            in.reset();
                                            skipFully(in, (long) prevSize);
                                            byte[] tmp = inbuf;
                                            inbuf = previnbuf;
                                            previnbuf = tmp;
                                        }
                                        first = false;
                                    } else if (!ignoreMissingEndBoundary) {
                                        throw new MessagingException("missing multipart end boundary");
                                    } else {
                                        if (sin != null) {
                                            end = sin.getPosition();
                                        }
                                        this.complete = false;
                                        done = true;
                                    }
                                }
                                if (sin != null) {
                                    part = createMimeBodyPart(sin.newStream(start, end));
                                } else {
                                    if (prevSize - eolLen > 0) {
                                        buf.write(previnbuf, 0, prevSize - eolLen);
                                    }
                                    if (!this.complete && inSize > 0) {
                                        buf.write(inbuf, 0, inSize);
                                    }
                                    part = createMimeBodyPart(headers, buf.toByteArray());
                                }
                                super.addBodyPart(part);
                            }
                        }
                        try {
                            in.close();
                        } catch (IOException e2) {
                        }
                        this.parsed = true;
                        break;
                    } else {
                        throw new MessagingException("Missing start boundary");
                    }
                } catch (IOException ioex) {
                    throw new MessagingException("IO Error", ioex);
                } catch (Throwable th) {
                    try {
                        in.close();
                    } catch (IOException e3) {
                    }
                    throw th;
                }
            } catch (Exception ex) {
                throw new MessagingException("No inputstream from datasource", ex);
            }
        }
        return;
    }

    private static int readFully(InputStream in, byte[] buf, int off, int len) throws IOException {
        if (len == 0) {
            return 0;
        }
        int total = 0;
        while (len > 0) {
            int bsize = in.read(buf, off, len);
            if (bsize <= 0) {
                break;
            }
            off += bsize;
            total += bsize;
            len -= bsize;
        }
        if (total <= 0) {
            return -1;
        }
        return total;
    }

    private void skipFully(InputStream in, long offset) throws IOException {
        while (offset > 0) {
            long cur = in.skip(offset);
            if (cur <= 0) {
                throw new EOFException("can't skip");
            }
            offset -= cur;
        }
    }

    /* access modifiers changed from: protected */
    public InternetHeaders createInternetHeaders(InputStream is) throws MessagingException {
        return new InternetHeaders(is);
    }

    /* access modifiers changed from: protected */
    public MimeBodyPart createMimeBodyPart(InternetHeaders headers, byte[] content) throws MessagingException {
        return new MimeBodyPart(headers, content);
    }

    /* access modifiers changed from: protected */
    public MimeBodyPart createMimeBodyPart(InputStream is) throws MessagingException {
        return new MimeBodyPart(is);
    }
}
