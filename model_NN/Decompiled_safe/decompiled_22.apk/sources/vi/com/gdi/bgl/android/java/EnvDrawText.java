package vi.com.gdi.bgl.android.java;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.SparseArray;

public class EnvDrawText {
    public static boolean bBmpChange = false;
    public static Bitmap bmp = null;
    public static int[] buffer = null;
    public static Canvas canvasTemp = null;
    public static SparseArray<a> fontCache = null;
    public static int iWordHightMax = 0;
    public static int iWordWidthMax = 0;
    public static Paint pt = null;

    public static int[] drawText(String str, int i, int i2, int[] iArr, int i3, int i4, int i5, int i6) {
        int measureText;
        a aVar;
        if (pt == null) {
            pt = new Paint();
        } else {
            pt.reset();
        }
        pt.setSubpixelText(true);
        pt.setAntiAlias(true);
        if (!(i2 == 0 || fontCache == null || (aVar = fontCache.get(i2)) == null)) {
            pt.setTypeface(aVar.a);
        }
        pt.setTextSize((float) i);
        int indexOf = str.indexOf(92, 0);
        if (indexOf == -1) {
            Paint.FontMetrics fontMetrics = pt.getFontMetrics();
            int measureText2 = (int) pt.measureText(str);
            int ceil = (int) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
            iArr[0] = measureText2;
            iArr[1] = ceil;
            int pow = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log((double) measureText2) / Math.log(2.0d))));
            int pow2 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log((double) ceil) / Math.log(2.0d))));
            if (iWordWidthMax < pow || iWordHightMax < pow2) {
                bBmpChange = true;
                iWordWidthMax = pow;
                iWordHightMax = pow2;
            }
            iArr[2] = iWordWidthMax;
            iArr[3] = iWordHightMax;
            if (bBmpChange) {
                bmp = Bitmap.createBitmap(iWordWidthMax, iWordHightMax, Bitmap.Config.ARGB_8888);
                canvasTemp = new Canvas(bmp);
            } else {
                bmp.eraseColor(0);
            }
            if ((-16777216 & i5) == 0) {
                canvasTemp.drawColor(33554431);
            } else {
                canvasTemp.drawColor(i5);
            }
            if (i6 != 0) {
                pt.setStrokeWidth((float) i6);
                pt.setStrokeCap(Paint.Cap.ROUND);
                pt.setStrokeJoin(Paint.Join.ROUND);
                pt.setStyle(Paint.Style.STROKE);
                pt.setColor(i4);
                canvasTemp.drawText(str, 0.0f, 0.0f - fontMetrics.ascent, pt);
            }
            pt.setStyle(Paint.Style.FILL);
            pt.setColor(i3);
            canvasTemp.drawText(str, 0.0f, 0.0f - fontMetrics.ascent, pt);
        } else {
            int i7 = indexOf + 1;
            int i8 = 2;
            int measureText3 = (int) pt.measureText(str.substring(0, indexOf));
            while (true) {
                int indexOf2 = str.indexOf(92, i7);
                if (indexOf2 <= 0) {
                    break;
                }
                int measureText4 = (int) pt.measureText(str.substring(i7, indexOf2));
                if (measureText4 > measureText3) {
                    measureText3 = measureText4;
                }
                i7 = indexOf2 + 1;
                i8++;
            }
            if (i7 != str.length() && (measureText = (int) pt.measureText(str.substring(i7, str.length()))) > measureText3) {
                measureText3 = measureText;
            }
            Paint.FontMetrics fontMetrics2 = pt.getFontMetrics();
            int ceil2 = (int) Math.ceil((double) (fontMetrics2.descent - fontMetrics2.ascent));
            int i9 = i8 * ceil2;
            iArr[0] = measureText3;
            iArr[1] = i9;
            int pow3 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log((double) measureText3) / Math.log(2.0d))));
            int pow4 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log((double) i9) / Math.log(2.0d))));
            if (iWordWidthMax < pow3 || iWordHightMax < pow4) {
                bBmpChange = true;
                iWordWidthMax = pow3;
                iWordHightMax = pow4;
            }
            iArr[2] = iWordWidthMax;
            iArr[3] = iWordHightMax;
            if (bBmpChange) {
                bmp = Bitmap.createBitmap(iWordWidthMax, iWordHightMax, Bitmap.Config.ARGB_8888);
                canvasTemp = new Canvas(bmp);
            } else {
                bmp.eraseColor(0);
            }
            if ((-16777216 & i5) == 0) {
                canvasTemp.drawColor(33554431);
            } else {
                canvasTemp.drawColor(i5);
            }
            int i10 = 0;
            int i11 = 0;
            while (true) {
                int indexOf3 = str.indexOf(92, i10);
                if (indexOf3 <= 0) {
                    break;
                }
                String substring = str.substring(i10, indexOf3);
                int measureText5 = (int) pt.measureText(substring);
                i10 = indexOf3 + 1;
                if (i6 != 0) {
                    pt.setStrokeWidth((float) i6);
                    pt.setStrokeCap(Paint.Cap.ROUND);
                    pt.setStrokeJoin(Paint.Join.ROUND);
                    pt.setStyle(Paint.Style.STROKE);
                    pt.setColor(i4);
                    canvasTemp.drawText(substring, (float) ((iArr[0] - measureText5) / 2), ((float) (i11 * ceil2)) - fontMetrics2.ascent, pt);
                }
                pt.setStyle(Paint.Style.FILL);
                pt.setColor(i3);
                canvasTemp.drawText(substring, (float) ((iArr[0] - measureText5) / 2), ((float) (i11 * ceil2)) - fontMetrics2.ascent, pt);
                i11++;
            }
            if (i10 != str.length()) {
                String substring2 = str.substring(i10, str.length());
                int measureText6 = (int) pt.measureText(substring2);
                if (i6 != 0) {
                    pt.setStrokeWidth((float) i6);
                    pt.setStrokeCap(Paint.Cap.ROUND);
                    pt.setStrokeJoin(Paint.Join.ROUND);
                    pt.setStyle(Paint.Style.STROKE);
                    pt.setColor(i4);
                    canvasTemp.drawText(substring2, (float) ((iArr[0] - measureText6) / 2), ((float) (i11 * ceil2)) - fontMetrics2.ascent, pt);
                }
                pt.setStyle(Paint.Style.FILL);
                pt.setColor(i3);
                canvasTemp.drawText(substring2, (float) ((iArr[0] - measureText6) / 2), ((float) (i11 * ceil2)) - fontMetrics2.ascent, pt);
            }
        }
        int i12 = iWordWidthMax * iWordHightMax;
        if (bBmpChange) {
            buffer = new int[i12];
        }
        bmp.getPixels(buffer, 0, iWordWidthMax, 0, 0, iWordWidthMax, iWordHightMax);
        bBmpChange = false;
        return buffer;
    }

    public static short[] getTextSize(String str, int i) {
        int length = str.length();
        if (length == 0) {
            return null;
        }
        Paint paint = new Paint();
        paint.setSubpixelText(true);
        paint.setAntiAlias(true);
        paint.setTextSize((float) i);
        short[] sArr = new short[length];
        for (int i2 = 0; i2 < length; i2++) {
            sArr[i2] = (short) ((int) paint.measureText(str.substring(0, i2 + 1)));
        }
        return sArr;
    }

    public static void registFontCache(int i, Typeface typeface) {
        if (i != 0 && typeface != null) {
            if (fontCache == null) {
                fontCache = new SparseArray<>();
            }
            a aVar = fontCache.get(i);
            if (aVar == null) {
                a aVar2 = new a();
                aVar2.a = typeface;
                aVar2.b++;
                fontCache.put(i, aVar2);
                return;
            }
            aVar.b++;
        }
    }

    public static void removeFontCache(int i) {
        a aVar = fontCache.get(i);
        if (aVar != null) {
            aVar.b--;
            if (aVar.b == 0) {
                fontCache.remove(i);
            }
        }
    }
}
