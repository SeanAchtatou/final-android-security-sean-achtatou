package tengxun_weibo;

public class TencentTO {
    private String accessToken;
    private String appkey;
    private String clientIp;
    private String openId;

    public String getAppkey() {
        return this.appkey;
    }

    public void setAppkey(String appkey2) {
        this.appkey = appkey2;
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    public void setAccessToken(String accessToken2) {
        this.accessToken = accessToken2;
    }

    public String getOpenId() {
        return this.openId;
    }

    public void setOpenId(String openId2) {
        this.openId = openId2;
    }

    public String getClientIp() {
        return this.clientIp;
    }

    public void setClientIp(String clientIp2) {
        this.clientIp = clientIp2;
    }
}
