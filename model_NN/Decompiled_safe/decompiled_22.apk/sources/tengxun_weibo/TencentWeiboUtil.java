package tengxun_weibo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;
import com.palmtrends.loadimage.Utils;
import com.tencent.weibo.sdk.android.api.WeiboAPI;
import com.tencent.weibo.sdk.android.component.sso.AuthHelper;
import com.tencent.weibo.sdk.android.component.sso.OnAuthListener;
import com.tencent.weibo.sdk.android.component.sso.WeiboToken;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.utils.PerfHelper;
import com.weibo.sdk.android.WeiboException;
import com.weibo.sdk.android.net.RequestListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;
import sina_weibo.LOG;
import sina_weibo.PreferenceUtil;
import sina_weibo.Util;
import sina_weibo.WeiboListener;

public class TencentWeiboUtil {
    private static final String TAG = "TencentWeiboUtil";
    /* access modifiers changed from: private */
    public static Context mContext;
    private static TencentTO tencentTO;
    private static TencentWeiboUtil tencentWeiboUtil;
    private static TencentWeiboAPI weiboAPI;
    private WeiboListener listener;

    public TencentWeiboUtil() {
        tencentTO = new TencentTO();
    }

    public static TencentWeiboUtil getInstance(Context context) {
        mContext = context;
        if (tencentWeiboUtil == null) {
            tencentWeiboUtil = new TencentWeiboUtil();
        }
        return tencentWeiboUtil;
    }

    public void webAuthOnResult() {
        LOG.cstdr(TAG, "initTencentWeibo=======listener = " + this.listener);
        if (this.listener != null) {
            this.listener.onResult();
        }
    }

    public void initTencentWeibo(WeiboListener l) {
        String accessToken = PreferenceUtil.getInstance(mContext).getString(Constants.PREF_TX_ACCESS_TOKEN, "");
        if (TextUtils.isEmpty(accessToken)) {
            l.init(false);
            return;
        }
        long expiresTime = Long.parseLong(PreferenceUtil.getInstance(mContext).getString(Constants.PREF_TX_EXPIRES_TIME, ""));
        LOG.cstdr(TAG, "expiresTime = " + expiresTime);
        LOG.cstdr(TAG, "expiresTime - System.currentTimeMillis() = " + (expiresTime - System.currentTimeMillis()));
        if (expiresTime - System.currentTimeMillis() > 0) {
            String openId = PreferenceUtil.getInstance(mContext).getString(Constants.PREF_TX_OPEN_ID, "");
            String clientId = PreferenceUtil.getInstance(mContext).getString(Constants.PREF_TX_CLIENT_ID, "");
            String clientIp = PreferenceUtil.getInstance(mContext).getString(Constants.PREF_TX_CLIENT_IP, "");
            tencentTO.setAccessToken(accessToken);
            tencentTO.setOpenId(openId);
            tencentTO.setAppkey(clientId);
            tencentTO.setClientIp(clientIp);
            l.init(true);
            return;
        }
        l.init(false);
    }

    public void auth(final WeiboListener l) {
        long appId = Long.valueOf(Constants.TX_APP_KEY).longValue();
        String appSecket = Constants.TX_APP_KEY_SEC;
        LOG.cstdr(TAG, "appId = " + appId + " appSecket = " + appSecket);
        AuthHelper.register(mContext, appId, appSecket, new OnAuthListener() {
            public void onWeiboVersionMisMatch() {
                l.init(false);
            }

            public void onWeiBoNotInstalled() {
                l.init(false);
            }

            public void onAuthPassed(String name, WeiboToken token) {
                Util.saveSharePersistent(TencentWeiboUtil.mContext, "ACCESS_TOKEN", token.accessToken);
                Util.saveSharePersistent(TencentWeiboUtil.mContext, "EXPIRES_IN", String.valueOf((token.expiresIn * 1000) + System.currentTimeMillis()));
                Util.saveSharePersistent(TencentWeiboUtil.mContext, "OPEN_ID", token.openID);
                Util.saveSharePersistent(TencentWeiboUtil.mContext, "OPEN_KEY", token.omasKey);
                Util.saveSharePersistent(TencentWeiboUtil.mContext, "REFRESH_TOKEN", "");
                Util.saveSharePersistent(TencentWeiboUtil.mContext, "CLIENT_ID", Constants.TX_APP_KEY);
                Util.saveSharePersistent(TencentWeiboUtil.mContext, "AUTHORIZETIME", String.valueOf(System.currentTimeMillis() / 1000));
                PerfHelper.setInfo(Constants.PREF_TX_NAME, name);
            }

            public void onAuthFail(int result, String error) {
                LOG.cstdr(TencentWeiboUtil.TAG, "onAuthFail---result = " + result + " error = " + error);
                Utils.showToast("授权失败。出错信息：" + error);
            }
        });
        AuthHelper.auth(mContext, "");
    }

    public static String getClientIp() {
        try {
            Enumeration<NetworkInterface> mEnumeration = NetworkInterface.getNetworkInterfaces();
            while (mEnumeration.hasMoreElements()) {
                Enumeration<InetAddress> enumIPAddr = mEnumeration.nextElement().getInetAddresses();
                while (true) {
                    if (enumIPAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIPAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("Error", ex.toString());
        }
        return null;
    }

    private void getUserInfo(final WeiboListener l) {
        weiboAPI = new TencentWeiboAPI(tencentTO);
        weiboAPI.getUserInfo(new RequestListener() {
            public void onIOException(IOException e) {
                LOG.cstdr(TencentWeiboUtil.TAG, "onIOException");
                Utils.showToast("获取用户信息失败。出错信息：" + e.getMessage());
            }

            public void onError(WeiboException e) {
                LOG.cstdr(TencentWeiboUtil.TAG, "onError = " + e.getMessage());
                Utils.showToast("获取用户信息失败。出错信息：" + e.getMessage());
            }

            public void onComplete(String json) {
                LOG.cstdr(TencentWeiboUtil.TAG, "onComplete---json = " + json);
                try {
                    JSONObject data = new JSONObject(json).getJSONObject("data");
                    String name = data.optString("nick");
                    String userimg = data.optString(Constants.SINA_USER_IMG);
                    PerfHelper.setInfo(Constants.PREF_TX_NAME, name);
                    PerfHelper.setInfo(Constants.PREF_TX_USER_NAME_IMG, userimg);
                    if (l != null) {
                        l.onResult();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void addWeibo(String content, Bitmap bm, double longitude, double latitude, HttpCallback httpcb) {
        WeiboAPI weibo = new WeiboAPI(new AccountModel(Util.getSharePersistent(mContext, "ACCESS_TOKEN")));
        if (bm != null) {
            weibo.addPic(mContext, content, "json", longitude, latitude, bm, 0, 0, httpcb, null, 4);
        } else {
            weibo.addWeibo(mContext, content, "json", longitude, latitude, 0, 0, httpcb, null, 4);
        }
    }

    public String addBmParam(Bitmap mBitmap, byte[] val) {
        double size = (double) (val.length / 1024);
        if (size > 400.0d) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            double i = size / 400.0d;
            zoomImage(mBitmap, ((double) mBitmap.getWidth()) / Math.sqrt(i), ((double) mBitmap.getHeight()) / Math.sqrt(i)).compress(Bitmap.CompressFormat.JPEG, 100, baos);
            val = baos.toByteArray();
        }
        StringBuffer buffer = new StringBuffer();
        for (byte b : val) {
            buffer.append((char) b);
        }
        return buffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public Bitmap zoomImage(Bitmap bm, double newWidth, double newHeight) {
        float width = (float) bm.getWidth();
        float height = (float) bm.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) newWidth) / width, ((float) newHeight) / height);
        return Bitmap.createBitmap(bm, 0, 0, (int) width, (int) height, matrix, true);
    }

    public void logout(WeiboListener l) {
        PreferenceUtil.getInstance(mContext).remove(Constants.PREF_TX_ACCESS_TOKEN);
        l.onResult();
    }

    public boolean isAuth() {
        if (TextUtils.isEmpty(PreferenceUtil.getInstance(mContext).getString(Constants.PREF_TX_ACCESS_TOKEN, ""))) {
            return false;
        }
        return true;
    }

    public static String getLocalIPAddress(Context context) {
        int i = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getIpAddress();
        return String.valueOf(i & 255) + "." + ((i >> 8) & 255) + "." + ((i >> 16) & 255) + "." + ((i >> 24) & 255);
    }
}
