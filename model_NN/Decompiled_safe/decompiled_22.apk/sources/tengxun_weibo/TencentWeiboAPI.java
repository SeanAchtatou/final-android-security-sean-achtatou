package tengxun_weibo;

import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.net.AsyncWeiboRunner;
import com.weibo.sdk.android.net.RequestListener;
import sina_weibo.Constants;

public class TencentWeiboAPI {
    public static final String API_SERVER = "https://open.t.qq.com/api";
    public static final String HTTPMETHOD_GET = "GET";
    public static final String HTTPMETHOD_POST = "POST";
    private static final String URL_ADD = "https://open.t.qq.com/api/t/add_pic_url";
    private static final String URL_USER_INFO = "https://open.t.qq.com/api/user/info";
    private TencentTO tencentTO;

    public TencentWeiboAPI(TencentTO tencentTO2) {
        this.tencentTO = tencentTO2;
    }

    private void request(String url, WeiboParameters params, String httpMethod, RequestListener listener) {
        params.add("oauth_consumer_key", this.tencentTO.getAppkey());
        params.add("access_token", this.tencentTO.getAccessToken());
        params.add("openid", this.tencentTO.getOpenId());
        params.add(Constants.TX_API_CLIENT_IP, this.tencentTO.getClientIp());
        params.add(Constants.TX_API_OAUTH_VERSION, "2.a");
        params.add("scope", "all");
        params.add(Constants.TX_API_FORMAT, "json");
        AsyncWeiboRunner.request(url, params, httpMethod, listener);
    }

    public void addWeibo(String content, String picurl, long longitude, long latitude, int syncflag, int compatibleflag, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("content", content);
        params.add(Constants.TX_API_LONGITUDE, longitude);
        params.add(Constants.TX_API_LATITUDE, latitude);
        params.add(Constants.TX_API_SYNCFLAG, syncflag);
        params.add(Constants.TX_API_COMPATIBLEFLAG, compatibleflag);
        request(URL_ADD, params, "POST", listener);
    }

    public void getUserInfo(RequestListener listener) {
        request(URL_USER_INFO, new WeiboParameters(), "GET", listener);
    }
}
