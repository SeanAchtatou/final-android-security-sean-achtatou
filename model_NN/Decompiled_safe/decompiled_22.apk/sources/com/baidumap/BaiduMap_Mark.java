package com.baidumap;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.ItemizedOverlay;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.map.PopupClickListener;
import com.baidu.mapapi.map.PopupOverlay;
import com.baidu.mapapi.map.RouteOverlay;
import com.baidu.mapapi.map.TransitOverlay;
import com.baidu.mapapi.search.MKAddrInfo;
import com.baidu.mapapi.search.MKBusLineResult;
import com.baidu.mapapi.search.MKDrivingRouteResult;
import com.baidu.mapapi.search.MKPlanNode;
import com.baidu.mapapi.search.MKPoiResult;
import com.baidu.mapapi.search.MKRoute;
import com.baidu.mapapi.search.MKSearch;
import com.baidu.mapapi.search.MKSearchListener;
import com.baidu.mapapi.search.MKSuggestionResult;
import com.baidu.mapapi.search.MKTransitRouteResult;
import com.baidu.mapapi.search.MKWalkingRouteResult;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.city_life.entity.CityLifeApplication;
import com.city_life.part_fragment.BMapUtil;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;

public class BaiduMap_Mark extends Activity {
    public GeoPoint Go_Geo = null;
    public String Go_where = "保利花半里";
    public GeoPoint Now_Geo = null;
    public String Which_City = "重庆";
    boolean isFirstLoc = true;
    boolean isLocationClientStop = false;
    boolean isRequest = false;
    private Listitem item;
    LocationData locData = null;
    private float loc_h = 29.564846f;
    private float loc_s = 106.580826f;
    Button mBtnDrive = null;
    Button mBtnTransit = null;
    Button mBtnWalk = null;
    public RelativeLayout mContainers;
    LocationClient mLocClient;
    public View mMain_layout;
    /* access modifiers changed from: private */
    public MapController mMapController = null;
    MyLocationMapView mMapView = null;
    MKSearch mSearch = null;
    public MyLocationListenner myListener = new MyLocationListenner();
    locationOverlay myLocationOverlay = null;
    int nodeIndex = -2;
    /* access modifiers changed from: private */
    public PopupOverlay pop = null;
    /* access modifiers changed from: private */
    public TextView popupText = null;
    RadioGroup.OnCheckedChangeListener radioButtonListener = null;
    Button requestLocButton = null;
    MKRoute route = null;
    int searchType = -1;
    TransitOverlay transit = null;
    private View viewCache = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.baiduactivity_mark);
        findview();
    }

    public void findview() {
        this.item = (Listitem) getIntent().getExtras().get("item");
        this.mMapView = (MyLocationMapView) findViewById(R.id.bmapView);
        this.mMapController = this.mMapView.getController();
        this.mMapController.setCenter(new GeoPoint((int) (((double) this.loc_h) * 1000000.0d), (int) (((double) this.loc_s) * 1000000.0d)));
        this.mMapView.getController().setZoom(14.0f);
        this.mMapView.getController().enableClick(true);
        this.mMapView.setBuiltInZoomControls(true);
        createPaopao();
        this.mLocClient = new LocationClient(this);
        this.locData = new LocationData();
        this.mLocClient.registerLocationListener(this.myListener);
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true);
        option.setCoorType("bd09ll");
        option.setScanSpan(5000);
        this.mLocClient.setLocOption(option);
        this.mLocClient.start();
        this.myLocationOverlay = new locationOverlay(this.mMapView);
        this.myLocationOverlay.setData(this.locData);
        this.mMapView.getOverlays().add(this.myLocationOverlay);
        this.myLocationOverlay.enableCompass();
        this.mMapView.refresh();
        new View.OnClickListener() {
            public void onClick(View v) {
            }
        };
        biaoji(this.item);
    }

    public void setSeach() {
        this.mSearch = new MKSearch();
        this.mSearch.init(((CityLifeApplication) getApplication()).mBMapManager, new MKSearchListener() {
            public void onGetDrivingRouteResult(MKDrivingRouteResult res, int error) {
                if (error != 4) {
                    if (error != 0 || res == null) {
                        Toast.makeText(BaiduMap_Mark.this, "抱歉，未找到结果", 0).show();
                        return;
                    }
                    BaiduMap_Mark.this.searchType = 0;
                    RouteOverlay routeOverlay = new RouteOverlay(BaiduMap_Mark.this, BaiduMap_Mark.this.mMapView);
                    routeOverlay.setData(res.getPlan(0).getRoute(0));
                    BaiduMap_Mark.this.mMapView.getOverlays().clear();
                    BaiduMap_Mark.this.mMapView.getOverlays().add(routeOverlay);
                    BaiduMap_Mark.this.mMapView.refresh();
                    BaiduMap_Mark.this.mMapView.getController().zoomToSpan(routeOverlay.getLatSpanE6(), routeOverlay.getLonSpanE6());
                    BaiduMap_Mark.this.mMapView.getController().animateTo(res.getStart().pt);
                    BaiduMap_Mark.this.route = res.getPlan(0).getRoute(0);
                    BaiduMap_Mark.this.nodeIndex = -1;
                }
            }

            public void onGetTransitRouteResult(MKTransitRouteResult res, int error) {
                if (error != 4) {
                    if (error != 0 || res == null) {
                        Toast.makeText(BaiduMap_Mark.this, "抱歉，未找到结果", 0).show();
                        return;
                    }
                    int planNum = res.getNumPlan();
                    for (int i = 0; i < planNum; i++) {
                        TransitOverlay routeOverlay = new TransitOverlay(BaiduMap_Mark.this, BaiduMap_Mark.this.mMapView);
                        routeOverlay.setData(res.getPlan(i));
                        BaiduMap_Mark.this.mMapView.getOverlays().add(routeOverlay);
                    }
                    BaiduMap_Mark.this.searchType = 1;
                    TransitOverlay routeOverlay2 = new TransitOverlay(BaiduMap_Mark.this, BaiduMap_Mark.this.mMapView);
                    routeOverlay2.setData(res.getPlan(0));
                    BaiduMap_Mark.this.mMapView.getOverlays().clear();
                    BaiduMap_Mark.this.mMapView.getOverlays().add(routeOverlay2);
                    BaiduMap_Mark.this.mMapView.refresh();
                    BaiduMap_Mark.this.mMapView.getController().zoomToSpan(routeOverlay2.getLatSpanE6(), routeOverlay2.getLonSpanE6());
                    BaiduMap_Mark.this.mMapView.getController().animateTo(res.getStart().pt);
                    BaiduMap_Mark.this.transit = routeOverlay2;
                    BaiduMap_Mark.this.nodeIndex = 0;
                }
            }

            public void onGetWalkingRouteResult(MKWalkingRouteResult res, int error) {
                if (error != 4) {
                    if (error != 0 || res == null) {
                        Toast.makeText(BaiduMap_Mark.this, "抱歉，未找到结果", 0).show();
                        return;
                    }
                    BaiduMap_Mark.this.searchType = 2;
                    RouteOverlay routeOverlay = new RouteOverlay(BaiduMap_Mark.this, BaiduMap_Mark.this.mMapView);
                    routeOverlay.setData(res.getPlan(0).getRoute(0));
                    BaiduMap_Mark.this.mMapView.getOverlays().clear();
                    BaiduMap_Mark.this.mMapView.getOverlays().add(routeOverlay);
                    BaiduMap_Mark.this.mMapView.refresh();
                    BaiduMap_Mark.this.mMapView.getController().zoomToSpan(routeOverlay.getLatSpanE6(), routeOverlay.getLonSpanE6());
                    BaiduMap_Mark.this.mMapView.getController().animateTo(res.getStart().pt);
                    BaiduMap_Mark.this.route = res.getPlan(0).getRoute(0);
                    BaiduMap_Mark.this.nodeIndex = -1;
                }
            }

            public void onGetAddrResult(MKAddrInfo res, int error) {
                if (error != 0) {
                    BaiduMap_Mark.this.mSearch.poiSearchInCity(BaiduMap_Mark.this.Which_City, BaiduMap_Mark.this.Go_where);
                    return;
                }
                if (res.type == 0) {
                    String.format("纬度：%f 经度：%f", Double.valueOf(((double) res.geoPt.getLatitudeE6()) / 1000000.0d), Double.valueOf(((double) res.geoPt.getLongitudeE6()) / 1000000.0d));
                }
                ltemizedOverlay itemOverlay = new ltemizedOverlay(null, BaiduMap_Mark.this.mMapView);
                OverlayItem item = new OverlayItem(res.geoPt, "", null);
                Drawable marker = BaiduMap_Mark.this.getResources().getDrawable(R.drawable.icon_marka);
                marker.setBounds(0, 0, marker.getIntrinsicWidth(), marker.getIntrinsicHeight());
                item.setMarker(marker);
                itemOverlay.addItem(item);
                BaiduMap_Mark.this.mMapView.getOverlays().add(itemOverlay);
                BaiduMap_Mark.this.mMapView.refresh();
                BaiduMap_Mark.this.Go_Geo = res.geoPt;
                BaiduMap_Mark.this.mMapController.animateTo(BaiduMap_Mark.this.Go_Geo);
            }

            public void onGetPoiResult(MKPoiResult res, int arg1, int arg2) {
                if (arg2 != 0 || res == null) {
                    Toast.makeText(BaiduMap_Mark.this, "抱歉，未找到结果", 1).show();
                    return;
                }
                BaiduMap_Mark.this.Go_Geo = res.getPoi(0).pt;
                BaiduMap_Mark.this.mSearch.reverseGeocode(BaiduMap_Mark.this.Go_Geo);
            }

            public void onGetBusDetailResult(MKBusLineResult result, int iError) {
            }

            public void onGetSuggestionResult(MKSuggestionResult res, int arg1) {
            }

            public void onGetPoiDetailSearchResult(int type, int iError) {
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void SearchButtonProcess(View v) {
        if (this.pop != null) {
            this.pop.hidePop();
        }
        this.route = null;
        this.transit = null;
        MKPlanNode stNode = new MKPlanNode();
        stNode.pt = this.Now_Geo;
        MKPlanNode enNode = new MKPlanNode();
        enNode.name = this.Go_where;
        if (this.mBtnDrive.equals(v)) {
            this.mSearch.drivingSearch(this.Which_City, stNode, this.Which_City, enNode);
        } else if (this.mBtnTransit.equals(v)) {
            this.mSearch.transitSearch(this.Which_City, stNode, enNode);
        } else if (this.mBtnWalk.equals(v)) {
            this.mSearch.walkingSearch(this.Which_City, stNode, this.Which_City, enNode);
        }
    }

    public void requestLocClick() {
        this.isRequest = true;
        this.mLocClient.requestLocation();
        Toast.makeText(this, "正在定位…", 0).show();
    }

    public void modifyLocationOverlayIcon(Drawable marker) {
        this.myLocationOverlay.setMarker(marker);
        this.mMapView.refresh();
    }

    public void createPaopao() {
        this.viewCache = getLayoutInflater().inflate((int) R.layout.custom_text_view, (ViewGroup) null);
        this.popupText = (TextView) this.viewCache.findViewById(R.id.textcache);
        this.pop = new PopupOverlay(this.mMapView, new PopupClickListener() {
            public void onClickedPopup(int index) {
                Log.v("click", "clickapoapo");
            }
        });
        MyLocationMapView.pop = this.pop;
    }

    public void biaoji(Listitem item2) {
        this.locData.latitude = Double.parseDouble(item2.latitude);
        this.locData.longitude = Double.parseDouble(item2.longitude);
        this.myLocationOverlay.setData(this.locData);
        this.mMapView.refresh();
        if (this.isRequest || this.isFirstLoc) {
            System.out.println("第一次定位点:" + this.locData.latitude);
            System.out.println("第er次定位点:" + this.locData.longitude);
            this.Now_Geo = new GeoPoint((int) (this.locData.latitude * 1000000.0d), (int) (this.locData.longitude * 1000000.0d));
            this.mMapController.animateTo(this.Now_Geo);
            this.isRequest = false;
        }
        this.isFirstLoc = false;
    }

    public class MyLocationListenner implements BDLocationListener {
        public MyLocationListenner() {
        }

        public void onReceiveLocation(BDLocation location) {
            if (location != null && !BaiduMap_Mark.this.isLocationClientStop) {
                BaiduMap_Mark.this.locData.latitude = location.getLatitude();
                BaiduMap_Mark.this.locData.longitude = location.getLongitude();
                BaiduMap_Mark.this.locData.accuracy = location.getRadius();
                BaiduMap_Mark.this.locData.direction = location.getDerect();
                BaiduMap_Mark.this.myLocationOverlay.setData(BaiduMap_Mark.this.locData);
                BaiduMap_Mark.this.mMapView.refresh();
                if (BaiduMap_Mark.this.isRequest || BaiduMap_Mark.this.isFirstLoc) {
                    System.out.println("第一次定位点:" + BaiduMap_Mark.this.locData.latitude);
                    System.out.println("第er次定位点:" + BaiduMap_Mark.this.locData.longitude);
                    BaiduMap_Mark.this.Now_Geo = new GeoPoint((int) (BaiduMap_Mark.this.locData.latitude * 1000000.0d), (int) (BaiduMap_Mark.this.locData.longitude * 1000000.0d));
                    BaiduMap_Mark.this.mMapController.animateTo(BaiduMap_Mark.this.Now_Geo);
                    BaiduMap_Mark.this.isRequest = false;
                }
                BaiduMap_Mark.this.isFirstLoc = false;
            }
        }

        public void onReceivePoi(BDLocation poiLocation) {
            if (poiLocation == null) {
            }
        }
    }

    public class locationOverlay extends MyLocationOverlay {
        public locationOverlay(MapView mapView) {
            super(mapView);
        }

        /* access modifiers changed from: protected */
        public boolean dispatchTap() {
            BaiduMap_Mark.this.popupText.setBackgroundResource(R.drawable.facebace_email_bg);
            BaiduMap_Mark.this.popupText.setText("我的位置");
            BaiduMap_Mark.this.pop.showPopup(BMapUtil.getBitmapFromView(BaiduMap_Mark.this.popupText), new GeoPoint((int) (BaiduMap_Mark.this.locData.latitude * 1000000.0d), (int) (BaiduMap_Mark.this.locData.longitude * 1000000.0d)), 8);
            return true;
        }
    }

    public class ltemizedOverlay extends ItemizedOverlay<OverlayItem> {
        public ltemizedOverlay(Drawable arg0, MapView arg1) {
            super(arg0, arg1);
        }

        public boolean onTap(GeoPoint arg0, MapView arg1) {
            return super.onTap(arg0, arg1);
        }

        /* access modifiers changed from: protected */
        public boolean onTap(int arg0) {
            BaiduMap_Mark.this.popupText.setBackgroundResource(R.drawable.facebace_email_bg);
            BaiduMap_Mark.this.popupText.setText(BaiduMap_Mark.this.Go_where);
            BaiduMap_Mark.this.pop.showPopup(BMapUtil.getBitmapFromView(BaiduMap_Mark.this.popupText), new GeoPoint(BaiduMap_Mark.this.Go_Geo.getLatitudeE6(), BaiduMap_Mark.this.Go_Geo.getLongitudeE6()), 8);
            return super.onTap(arg0);
        }
    }

    public void onPause() {
        this.isLocationClientStop = true;
        this.mMapView.onPause();
        super.onPause();
    }

    public void onResume() {
        this.isLocationClientStop = false;
        this.mMapView.onResume();
        super.onResume();
    }

    public void onDestroy() {
        if (this.mLocClient != null) {
            this.mLocClient.stop();
        }
        this.isLocationClientStop = true;
        this.mMapView.destroy();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString("where", this.Go_where);
        super.onSaveInstanceState(outState);
        this.mMapView.onSaveInstanceState(outState);
    }
}
