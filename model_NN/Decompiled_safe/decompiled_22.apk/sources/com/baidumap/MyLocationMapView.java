package com.baidumap;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.PopupOverlay;

class MyLocationMapView extends MapView {
    static PopupOverlay pop = null;

    public MyLocationMapView(Context context) {
        super(context);
    }

    public MyLocationMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyLocationMapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!super.onTouchEvent(event) && pop != null && event.getAction() == 1) {
            pop.hidePop();
        }
        return true;
    }
}
