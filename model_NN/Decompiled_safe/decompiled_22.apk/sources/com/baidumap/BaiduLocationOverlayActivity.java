package com.baidumap;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.mapapi.map.PopupClickListener;
import com.baidu.mapapi.map.PopupOverlay;
import com.baidu.mapapi.map.RouteOverlay;
import com.baidu.mapapi.map.TransitOverlay;
import com.baidu.mapapi.search.MKAddrInfo;
import com.baidu.mapapi.search.MKBusLineResult;
import com.baidu.mapapi.search.MKDrivingRouteResult;
import com.baidu.mapapi.search.MKPlanNode;
import com.baidu.mapapi.search.MKPoiResult;
import com.baidu.mapapi.search.MKSearch;
import com.baidu.mapapi.search.MKSearchListener;
import com.baidu.mapapi.search.MKSuggestionResult;
import com.baidu.mapapi.search.MKTransitRouteResult;
import com.baidu.mapapi.search.MKWalkingRouteResult;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.city_life.entity.BaseUtils;
import com.city_life.entity.CityLifeApplication;
import com.pengyou.citycommercialarea.R;

public class BaiduLocationOverlayActivity extends Activity {
    private String address = "";
    private Button btn_back;
    private String cityName = "北京";
    private String endLatitude = "";
    private String endLongitude = "";
    private MKPlanNode endNode;
    boolean isFirstLoc = true;
    boolean isLocationClientStop = false;
    boolean isRequest = false;
    LocationData locData = null;
    LocationClient mLocClient;
    /* access modifiers changed from: private */
    public MapController mMapController = null;
    /* access modifiers changed from: private */
    public MapView mMapView = null;
    private MKSearch mSearch = null;
    public MyLocationListenner myListener = new MyLocationListenner();
    locationOverlay myLocationOverlay = null;
    /* access modifiers changed from: private */
    public PopupOverlay pop = null;
    /* access modifiers changed from: private */
    public TextView popupText = null;
    ImageView requestLocButton = null;
    private String startLatitude = "";
    private MKPlanNode startNode;
    private String statrLongitude = "";
    private String travelMode = "";
    private TextView txtv_loadlook;
    private View viewCache = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CityLifeApplication app = (CityLifeApplication) getApplication();
        if (app.mBMapManager == null) {
            app.mBMapManager = new BMapManager(this);
            app.mBMapManager.init(CityLifeApplication.strKey, new CityLifeApplication.MyGeneralListener());
        }
        requestWindowFeature(1);
        setContentView((int) R.layout.baidu_map_locationoverlay);
        this.txtv_loadlook = (TextView) findViewById(R.id.txtv_loadlook_id);
        this.requestLocButton = (ImageView) findViewById(R.id.button1);
        this.requestLocButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaiduLocationOverlayActivity.this.requestLocClick();
            }
        });
        this.mMapView = (MapView) findViewById(R.id.bmapView);
        this.mMapController = this.mMapView.getController();
        this.mMapView.getController().setZoom(14.0f);
        this.mMapView.getController().enableClick(true);
        this.mMapView.setBuiltInZoomControls(true);
        createPaopao();
        this.mLocClient = new LocationClient(this);
        this.locData = new LocationData();
        this.mLocClient.registerLocationListener(this.myListener);
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true);
        option.setCoorType("bd09ll");
        option.setScanSpan(5000);
        this.mLocClient.setLocOption(option);
        this.mLocClient.start();
        this.myLocationOverlay = new locationOverlay(this.mMapView);
        this.myLocationOverlay.setData(this.locData);
        this.mMapView.getOverlays().add(this.myLocationOverlay);
        this.myLocationOverlay.enableCompass();
        this.mMapView.refresh();
        double d = this.locData.longitude;
        double d2 = this.locData.latitude;
        Bundle bundle = getIntent().getBundleExtra("map");
        if ("".equals(bundle) || bundle == null) {
            Toast.makeText(this, "获取的定位信息为空", 1).show();
        } else {
            this.travelMode = bundle.getString("travelMode");
            this.statrLongitude = bundle.getString("statrLongitude");
            this.startLatitude = bundle.getString("startLatitude");
            this.endLongitude = bundle.getString("endLongitude");
            this.endLatitude = bundle.getString("endLatitude");
            this.cityName = bundle.getString("cityName");
            if ("".equals(this.travelMode) || "".equals(this.statrLongitude) || "".equals(this.startLatitude) || "".equals(this.endLongitude) || "".equals(this.endLatitude) || "".equals(this.cityName)) {
                Toast.makeText(this, "信息传递有空值！", 1).show();
            } else {
                this.mMapController.setCenter(new GeoPoint((int) (Double.parseDouble(this.startLatitude) * 1000000.0d), (int) (Double.parseDouble(this.statrLongitude) * 1000000.0d)));
                this.mSearch = new MKSearch();
                this.endNode = new MKPlanNode();
                this.startNode = new MKPlanNode();
                this.mSearch.init(app.mBMapManager, new myMKSearchListener());
                if ("drive".equals(this.travelMode)) {
                    this.txtv_loadlook.setText(getResources().getString(R.string.map_circuit_drive));
                    this.startNode.pt = new GeoPoint((int) (Double.parseDouble(this.startLatitude) * 1000000.0d), (int) (Double.parseDouble(this.statrLongitude) * 1000000.0d));
                    this.endNode.pt = new GeoPoint((int) (Double.parseDouble(this.endLatitude) * 1000000.0d), (int) (Double.parseDouble(this.endLongitude) * 1000000.0d));
                    this.mSearch.drivingSearch(null, this.startNode, null, this.endNode);
                } else if ("bus".equals(this.travelMode)) {
                    this.txtv_loadlook.setText(getResources().getString(R.string.map_circuit_bus));
                    this.startNode.pt = new GeoPoint((int) (Double.parseDouble(this.startLatitude) * 1000000.0d), (int) (Double.parseDouble(this.statrLongitude) * 1000000.0d));
                    this.endNode.pt = new GeoPoint((int) (Double.parseDouble(this.endLatitude) * 1000000.0d), (int) (Double.parseDouble(this.endLongitude) * 1000000.0d));
                    this.mSearch.transitSearch(this.cityName, this.startNode, this.endNode);
                } else if ("walk".equals(this.travelMode)) {
                    this.txtv_loadlook.setText(getResources().getString(R.string.map_circuit_walk));
                    this.startNode.pt = new GeoPoint((int) (Double.parseDouble(this.startLatitude) * 1000000.0d), (int) (Double.parseDouble(this.statrLongitude) * 1000000.0d));
                    this.endNode.pt = new GeoPoint((int) (Double.parseDouble(this.endLatitude) * 1000000.0d), (int) (Double.parseDouble(this.endLongitude) * 1000000.0d));
                    this.mSearch.walkingSearch(null, this.startNode, null, this.endNode);
                }
            }
        }
        this.btn_back = (Button) findViewById(R.id.btn_bml_id);
        this.btn_back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaiduLocationOverlayActivity.this.finish();
            }
        });
    }

    public class myMKSearchListener implements MKSearchListener {
        public myMKSearchListener() {
        }

        public void onGetWalkingRouteResult(MKWalkingRouteResult res, int error) {
            if (error != 0 || res == null) {
                Toast.makeText(BaiduLocationOverlayActivity.this, "抱歉，未找到结果", 0).show();
                return;
            }
            RouteOverlay routeOverlay = new RouteOverlay(BaiduLocationOverlayActivity.this, BaiduLocationOverlayActivity.this.mMapView);
            routeOverlay.setData(res.getPlan(0).getRoute(0));
            BaiduLocationOverlayActivity.this.mMapView.getOverlays().add(routeOverlay);
            BaiduLocationOverlayActivity.this.mMapView.refresh();
            BaiduLocationOverlayActivity.this.mMapView.getController().zoomToSpan(routeOverlay.getLatSpanE6(), routeOverlay.getLonSpanE6());
            BaiduLocationOverlayActivity.this.mMapView.getController().animateTo(res.getStart().pt);
        }

        public void onGetTransitRouteResult(MKTransitRouteResult res, int error) {
            if (error != 0 || res == null) {
                Toast.makeText(BaiduLocationOverlayActivity.this, "由于离目标地较近，无法查到对应线路，请参照步行线路", 0).show();
                return;
            }
            TransitOverlay routeOverlay = new TransitOverlay(BaiduLocationOverlayActivity.this, BaiduLocationOverlayActivity.this.mMapView);
            routeOverlay.setData(res.getPlan(0));
            BaiduLocationOverlayActivity.this.mMapView.getOverlays().add(routeOverlay);
            BaiduLocationOverlayActivity.this.mMapView.refresh();
            BaiduLocationOverlayActivity.this.mMapView.getController().zoomToSpan(routeOverlay.getLatSpanE6(), routeOverlay.getLonSpanE6());
            BaiduLocationOverlayActivity.this.mMapView.getController().animateTo(res.getStart().pt);
        }

        public void onGetDrivingRouteResult(MKDrivingRouteResult res, int error) {
            if (error != 0 || res == null) {
                Toast.makeText(BaiduLocationOverlayActivity.this, "抱歉，未找到结果", 0).show();
                return;
            }
            RouteOverlay routeOverlay = new RouteOverlay(BaiduLocationOverlayActivity.this, BaiduLocationOverlayActivity.this.mMapView);
            routeOverlay.setData(res.getPlan(0).getRoute(0));
            BaiduLocationOverlayActivity.this.mMapView.getOverlays().add(routeOverlay);
            BaiduLocationOverlayActivity.this.mMapView.refresh();
            BaiduLocationOverlayActivity.this.mMapView.getController().zoomToSpan(routeOverlay.getLatSpanE6(), routeOverlay.getLonSpanE6());
            BaiduLocationOverlayActivity.this.mMapView.getController().animateTo(res.getStart().pt);
        }

        public void onGetAddrResult(MKAddrInfo arg0, int arg1) {
        }

        public void onGetBusDetailResult(MKBusLineResult res, int error) {
        }

        public void onGetPoiDetailSearchResult(int arg0, int arg1) {
        }

        public void onGetPoiResult(MKPoiResult arg0, int arg1, int arg2) {
        }

        public void onGetSuggestionResult(MKSuggestionResult arg0, int arg1) {
        }
    }

    public void requestLocClick() {
        this.isRequest = true;
        this.mLocClient.requestLocation();
        Toast.makeText(this, "正在定位…", 0).show();
    }

    public void modifyLocationOverlayIcon(Drawable marker) {
        this.myLocationOverlay.setMarker(marker);
        this.mMapView.refresh();
    }

    public void createPaopao() {
        this.viewCache = getLayoutInflater().inflate((int) R.layout.custom_text_view, (ViewGroup) null);
        this.popupText = (TextView) this.viewCache.findViewById(R.id.textcache);
        this.pop = new PopupOverlay(this.mMapView, new PopupClickListener() {
            public void onClickedPopup(int index) {
                Log.v("click", "clickapoapo");
            }
        });
        MyLocationMapView.pop = this.pop;
    }

    public class MyLocationListenner implements BDLocationListener {
        public MyLocationListenner() {
        }

        public void onReceiveLocation(BDLocation location) {
            if (location != null && !BaiduLocationOverlayActivity.this.isLocationClientStop) {
                BaiduLocationOverlayActivity.this.locData.latitude = location.getLatitude();
                BaiduLocationOverlayActivity.this.locData.longitude = location.getLongitude();
                BaiduLocationOverlayActivity.this.locData.accuracy = location.getRadius();
                BaiduLocationOverlayActivity.this.locData.direction = location.getDerect();
                BaiduLocationOverlayActivity.this.myLocationOverlay.setData(BaiduLocationOverlayActivity.this.locData);
                BaiduLocationOverlayActivity.this.mMapView.refresh();
                if (BaiduLocationOverlayActivity.this.isRequest || BaiduLocationOverlayActivity.this.isFirstLoc) {
                    BaiduLocationOverlayActivity.this.mMapController.animateTo(new GeoPoint((int) (BaiduLocationOverlayActivity.this.locData.latitude * 1000000.0d), (int) (BaiduLocationOverlayActivity.this.locData.longitude * 1000000.0d)));
                    BaiduLocationOverlayActivity.this.isRequest = false;
                }
                BaiduLocationOverlayActivity.this.isFirstLoc = false;
            }
        }

        public void onReceivePoi(BDLocation poiLocation) {
            if (poiLocation == null) {
            }
        }
    }

    public class locationOverlay extends MyLocationOverlay {
        public locationOverlay(MapView mapView) {
            super(mapView);
        }

        /* access modifiers changed from: protected */
        public boolean dispatchTap() {
            BaiduLocationOverlayActivity.this.popupText.setBackgroundResource(R.drawable.popup);
            BaiduLocationOverlayActivity.this.popupText.setText("我的位置");
            BaiduLocationOverlayActivity.this.pop.showPopup(BaseUtils.getBitmapFromView(BaiduLocationOverlayActivity.this.popupText), new GeoPoint((int) (BaiduLocationOverlayActivity.this.locData.latitude * 1000000.0d), (int) (BaiduLocationOverlayActivity.this.locData.longitude * 1000000.0d)), 8);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.isLocationClientStop = true;
        this.mMapView.onPause();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.isLocationClientStop = false;
        this.mMapView.onResume();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mLocClient != null) {
            this.mLocClient.stop();
        }
        this.isLocationClientStop = true;
        this.mMapView.destroy();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.mMapView.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.mMapView.onRestoreInstanceState(savedInstanceState);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
