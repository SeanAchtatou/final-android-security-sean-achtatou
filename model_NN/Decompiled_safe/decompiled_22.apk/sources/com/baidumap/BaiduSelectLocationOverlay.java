package com.baidumap;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MKMapViewListener;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.city_life.entity.CityLifeApplication;
import com.pengyou.citycommercialarea.R;

public class BaiduSelectLocationOverlay extends Activity {
    static MapView mMapView = null;
    Button btn_return = null;
    int index = 0;
    EditText indexText = null;
    LocationData locData = null;
    LocationClient mLocClient;
    /* access modifiers changed from: private */
    public MapController mMapController = null;
    public MKMapViewListener mMapListener = null;
    FrameLayout mMapViewContainer = null;
    public MyLocationListenner myListener = new MyLocationListenner();
    MyLocationOverlay myLocationOverlay = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        CityLifeApplication app = (CityLifeApplication) getApplication();
        if (app.mBMapManager == null) {
            app.mBMapManager = new BMapManager(this);
            app.mBMapManager.init(CityLifeApplication.strKey, new CityLifeApplication.MyGeneralListener());
        }
        setContentView((int) R.layout.baiduactivity_locationoverlay);
        mMapView = (MapView) findViewById(R.id.bmapView);
        this.mMapController = mMapView.getController();
        this.mLocClient = new LocationClient(this);
        this.mLocClient.registerLocationListener(this.myListener);
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true);
        option.setCoorType("bd09ll");
        option.setScanSpan(5000);
        this.mLocClient.setLocOption(option);
        this.mLocClient.start();
        mMapView.getController().setZoom(18.0f);
        mMapView.getController().enableClick(true);
        mMapView.setBuiltInZoomControls(true);
        mMapView.regMapViewListener(CityLifeApplication.getInstance().mBMapManager, this.mMapListener);
        this.myLocationOverlay = new MyLocationOverlay(mMapView);
        this.locData = new LocationData();
        this.myLocationOverlay.setData(this.locData);
        mMapView.getOverlays().add(this.myLocationOverlay);
        this.myLocationOverlay.enableCompass();
        mMapView.refresh();
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_btn_right).setVisibility(8);
        ((TextView) findViewById(R.id.title_title)).setText("个人位置");
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaiduSelectLocationOverlay.this.finish();
            }
        });
    }

    public void requestLocClick() {
        this.mLocClient.requestLocation();
        Toast.makeText(this, "正在定位…", 0).show();
    }

    public class MyLocationListenner implements BDLocationListener {
        public MyLocationListenner() {
        }

        public void onReceiveLocation(BDLocation location) {
            if (location != null) {
                BaiduSelectLocationOverlay.this.locData.latitude = location.getLatitude();
                BaiduSelectLocationOverlay.this.locData.longitude = location.getLongitude();
                BaiduSelectLocationOverlay.this.locData.accuracy = location.getRadius();
                BaiduSelectLocationOverlay.this.locData.direction = location.getDerect();
                BaiduSelectLocationOverlay.this.myLocationOverlay.setData(BaiduSelectLocationOverlay.this.locData);
                BaiduSelectLocationOverlay.mMapView.refresh();
                BaiduSelectLocationOverlay.this.mMapController.animateTo(new GeoPoint((int) (BaiduSelectLocationOverlay.this.locData.latitude * 1000000.0d), (int) (BaiduSelectLocationOverlay.this.locData.longitude * 1000000.0d)), null);
            }
        }

        public void onReceivePoi(BDLocation poiLocation) {
            if (poiLocation == null) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mLocClient != null) {
            this.mLocClient.stop();
        }
        mMapView.destroy();
        super.onDestroy();
    }
}
