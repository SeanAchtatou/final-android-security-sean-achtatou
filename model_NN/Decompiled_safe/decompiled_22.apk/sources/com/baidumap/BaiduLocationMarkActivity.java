package com.baidumap;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.ItemizedOverlay;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.map.PopupClickListener;
import com.baidu.mapapi.map.PopupOverlay;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.entity.CityLifeApplication;
import com.city_life.part_fragment.BMapUtil;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;
import java.util.ArrayList;

public class BaiduLocationMarkActivity extends BaseFragmentActivity {
    /* access modifiers changed from: private */
    public Button button = null;
    private Listitem item;
    /* access modifiers changed from: private */
    public MapView.LayoutParams layoutParam = null;
    /* access modifiers changed from: private */
    public OverlayItem mCurItem = null;
    private ArrayList<OverlayItem> mItems = null;
    double mLat1 = 39.963175d;
    double mLat2 = 39.942821d;
    double mLat3 = 39.939723d;
    double mLat4 = 39.906965d;
    double mLat5 = 39.942057d;
    double mLon1 = 116.400244d;
    double mLon2 = 116.369199d;
    double mLon3 = 116.425541d;
    double mLon4 = 116.401394d;
    double mLon5 = 116.402096d;
    private MapController mMapController = null;
    /* access modifiers changed from: private */
    public MapView mMapView = null;
    /* access modifiers changed from: private */
    public MyOverlay mOverlay = null;
    /* access modifiers changed from: private */
    public PopupOverlay pop = null;
    /* access modifiers changed from: private */
    public View popupInfo = null;
    private View popupLeft = null;
    private View popupRight = null;
    /* access modifiers changed from: private */
    public TextView popupText = null;
    private View viewCache = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CityLifeApplication app = (CityLifeApplication) getApplication();
        if (app.mBMapManager == null) {
            app.mBMapManager = new BMapManager(this);
            app.mBMapManager.init(CityLifeApplication.strKey, new CityLifeApplication.MyGeneralListener());
        }
        setContentView((int) R.layout.baiduactivity_mark);
        this.item = (Listitem) getIntent().getExtras().get("item");
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_btn_right).setVisibility(8);
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaiduLocationMarkActivity.this.finish();
            }
        });
        ((TextView) findViewById(R.id.title_title)).setText("商家位置");
        this.mMapView = (MapView) findViewById(R.id.bmapView);
        this.mMapController = this.mMapView.getController();
        this.mMapController.enableClick(true);
        this.mMapController.setZoom(14.0f);
        this.mMapView.setBuiltInZoomControls(true);
        initOverlay();
        this.mMapController.setCenter(new GeoPoint((int) (Double.parseDouble(this.item.latitude) * 1000000.0d), (int) (Double.parseDouble(this.item.longitude) * 1000000.0d)));
    }

    public void initOverlay() {
        this.mOverlay = new MyOverlay(getResources().getDrawable(R.drawable.icon_marka), this.mMapView);
        OverlayItem item1 = new OverlayItem(new GeoPoint((int) (Double.parseDouble(this.item.latitude) * 1000000.0d), (int) (Double.parseDouble(this.item.longitude) * 1000000.0d)), this.item.title, "");
        item1.setMarker(getResources().getDrawable(R.drawable.nav_turn_via_1));
        this.mOverlay.addItem(item1);
        this.mItems = new ArrayList<>();
        this.mItems.addAll(this.mOverlay.getAllItem());
        this.mMapView.getOverlays().add(this.mOverlay);
        this.mMapView.refresh();
        this.viewCache = getLayoutInflater().inflate((int) R.layout.custom_text_view, (ViewGroup) null);
        this.popupInfo = this.viewCache.findViewById(R.id.popinfo);
        this.popupLeft = this.viewCache.findViewById(R.id.popleft);
        this.popupRight = this.viewCache.findViewById(R.id.popright);
        this.popupText = (TextView) this.viewCache.findViewById(R.id.textcache);
        this.button = new Button(this);
        this.button.setBackgroundResource(R.drawable.popup);
        this.pop = new PopupOverlay(this.mMapView, new PopupClickListener() {
            public void onClickedPopup(int index) {
                if (index != 0 && index == 2) {
                    BaiduLocationMarkActivity.this.mCurItem.setMarker(BaiduLocationMarkActivity.this.getResources().getDrawable(R.drawable.nav_turn_via_1));
                    BaiduLocationMarkActivity.this.mOverlay.updateItem(BaiduLocationMarkActivity.this.mCurItem);
                    BaiduLocationMarkActivity.this.mMapView.refresh();
                }
            }
        });
    }

    public void clearOverlay(View view) {
        this.mOverlay.removeAll();
        if (this.pop != null) {
            this.pop.hidePop();
        }
        this.mMapView.removeView(this.button);
        this.mMapView.refresh();
    }

    public void resetOverlay(View view) {
        clearOverlay(null);
        this.mOverlay.addItem(this.mItems);
        this.mMapView.refresh();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mMapView.onPause();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mMapView.onResume();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mMapView.destroy();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.mMapView.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.mMapView.onRestoreInstanceState(savedInstanceState);
    }

    public class MyOverlay extends ItemizedOverlay {
        public MyOverlay(Drawable defaultMarker, MapView mapView) {
            super(defaultMarker, mapView);
        }

        public boolean onTap(int index) {
            OverlayItem item = getItem(index);
            BaiduLocationMarkActivity.this.mCurItem = item;
            if (index == 4) {
                BaiduLocationMarkActivity.this.button.setText("这是一个系统控件");
                BaiduLocationMarkActivity.this.layoutParam = new MapView.LayoutParams(-2, -2, new GeoPoint((int) (BaiduLocationMarkActivity.this.mLat5 * 1000000.0d), (int) (BaiduLocationMarkActivity.this.mLon5 * 1000000.0d)), 0, -32, 81);
                this.mMapView.addView(BaiduLocationMarkActivity.this.button, BaiduLocationMarkActivity.this.layoutParam);
            } else {
                BaiduLocationMarkActivity.this.popupText.setText(getItem(index).getTitle());
                BaiduLocationMarkActivity.this.pop.showPopup(new Bitmap[]{BMapUtil.getBitmapFromView(BaiduLocationMarkActivity.this.popupInfo)}, item.getPoint(), 32);
            }
            return true;
        }

        public boolean onTap(GeoPoint pt, MapView mMapView) {
            if (BaiduLocationMarkActivity.this.pop == null) {
                return false;
            }
            BaiduLocationMarkActivity.this.pop.hidePop();
            mMapView.removeView(BaiduLocationMarkActivity.this.button);
            return false;
        }
    }
}
