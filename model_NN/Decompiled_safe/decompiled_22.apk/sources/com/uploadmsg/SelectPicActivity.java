package com.uploadmsg;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.Util;

public class SelectPicActivity extends Activity implements View.OnClickListener {
    public static final String KEY_PHOTO_PATH = "photo_path";
    public static final int SELECT_PIC_BY_PICK_PHOTO = 2;
    public static final int SELECT_PIC_BY_TACK_PHOTO = 1;
    private static final String TAG = "SelectPicActivity";
    private Button cancelBtn;
    private LinearLayout dialogLayout;
    private Intent lastIntent;
    private Uri photoUri;
    private String picPath;
    private Button pickPhotoBtn;
    private Button takePhotoBtn;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_pic_layout);
        initView();
    }

    private void initView() {
        this.dialogLayout = (LinearLayout) findViewById(R.id.dialog_layout);
        this.dialogLayout.setOnClickListener(this);
        this.takePhotoBtn = (Button) findViewById(R.id.btn_take_photo);
        this.takePhotoBtn.setOnClickListener(this);
        this.pickPhotoBtn = (Button) findViewById(R.id.btn_pick_photo);
        this.pickPhotoBtn.setOnClickListener(this);
        this.cancelBtn = (Button) findViewById(R.id.btn_cancel);
        this.cancelBtn.setOnClickListener(this);
        this.lastIntent = getIntent();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_layout:
                finish();
                return;
            case R.id.btn_take_photo:
                takePhoto();
                return;
            case R.id.btn_pick_photo:
                pickPhoto();
                return;
            default:
                finish();
                return;
        }
    }

    private void takePhoto() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            this.photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
            intent.putExtra("output", this.photoUri);
            startActivityForResult(intent, 1);
            return;
        }
        Toast.makeText(this, "内存卡不存在", 1).show();
    }

    private void pickPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(intent, 2);
    }

    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            doPhoto(requestCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void doPhoto(int requestCode, Intent data) {
        if (requestCode == 2) {
            if (data == null) {
                Toast.makeText(this, "选择图片文件出错", 1).show();
                return;
            }
            this.photoUri = data.getData();
            if (this.photoUri == null) {
                Toast.makeText(this, "选择图片文件出错", 1).show();
                return;
            }
        }
        String[] pojo = {"_data"};
        Cursor cursor = managedQuery(this.photoUri, pojo, null, null, null);
        if (cursor != null) {
            int columnIndex = cursor.getColumnIndexOrThrow(pojo[0]);
            cursor.moveToFirst();
            this.picPath = cursor.getString(columnIndex);
            cursor.close();
        }
        Log.i(TAG, "imagePath = " + this.picPath);
        if (this.picPath == null || (!this.picPath.endsWith(".png") && !this.picPath.endsWith(".PNG") && !this.picPath.endsWith(Util.PHOTO_DEFAULT_EXT) && !this.picPath.endsWith(".JPG"))) {
            Toast.makeText(this, "选择图片文件不正确", 1).show();
            return;
        }
        this.lastIntent.putExtra(KEY_PHOTO_PATH, this.picPath);
        setResult(-1, this.lastIntent);
        finish();
    }
}
