package com.uploadmsg;

import android.util.Log;
import com.utils.FinalVariable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.UUID;

public class UploadUtil {
    private static final String BOUNDARY = UUID.randomUUID().toString();
    private static final String CHARSET = "utf-8";
    private static final String CONTENT_TYPE = "multipart/form-data";
    private static final String LINE_END = "\r\n";
    private static final String PREFIX = "--";
    private static final String TAG = "UploadUtil";
    public static final int TO_SELECT_PHOTO = 3;
    public static final int TO_UPLOAD_FILE = 1;
    public static final int UPLOAD_FILE_DONE = 2;
    public static final int UPLOAD_FILE_NOT_EXISTS_CODE = 2;
    public static final int UPLOAD_INIT_PROCESS = 4;
    public static final int UPLOAD_IN_PROCESS = 5;
    public static final int UPLOAD_SERVER_ERROR_CODE = 3;
    public static final int UPLOAD_SUCCESS_CODE = 1;
    protected static final int WHAT_TO_UPLOAD = 1;
    protected static final int WHAT_UPLOAD_DONE = 2;
    private static int requestTime = 0;
    private static UploadUtil uploadUtil;
    private int connectTimeout = FinalVariable.vb_success;
    private OnUploadProcessListener onUploadProcessListener;
    private int readTimeOut = FinalVariable.vb_success;

    public interface OnUploadProcessListener {
        void initUpload(int i);

        void onUploadDone(int i, String str);

        void onUploadProcess(int i);
    }

    public interface uploadProcessListener {
    }

    private UploadUtil() {
    }

    public static UploadUtil getInstance() {
        if (uploadUtil == null) {
            uploadUtil = new UploadUtil();
        }
        return uploadUtil;
    }

    public void uploadFile(String filePath, String fileKey, String RequestURL, Map<String, String> param) {
        if (filePath == null) {
            sendMessage(2, "文件不存在");
            return;
        }
        try {
            uploadFile(new File(filePath), fileKey, RequestURL, param);
        } catch (Exception e) {
            sendMessage(2, "文件不存在");
            e.printStackTrace();
        }
    }

    public void uploadFile(File file, String fileKey, String RequestURL, Map<String, String> param) {
        if (file == null || !file.exists()) {
            sendMessage(2, "文件不存在");
            return;
        }
        Log.i(TAG, "请求的URL=" + RequestURL);
        Log.i(TAG, "请求的fileName=" + file.getName());
        Log.i(TAG, "请求的fileKey=" + fileKey);
        final File file2 = file;
        final String str = fileKey;
        final String str2 = RequestURL;
        final Map<String, String> map = param;
        new Thread(new Runnable() {
            public void run() {
                UploadUtil.this.toUploadFile(file2, str, str2, map);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void toUploadFile(File file, String fileKey, String RequestURL, Map<String, String> param) {
        requestTime = 0;
        long requestTime2 = System.currentTimeMillis();
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(RequestURL).openConnection();
            conn.setReadTimeout(this.readTimeOut);
            conn.setConnectTimeout(this.connectTimeout);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Charset", CHARSET);
            conn.setRequestProperty("connection", "keep-alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
            if (param != null && param.size() > 0) {
                for (String key : param.keySet()) {
                    StringBuffer sb = new StringBuffer();
                    sb.append(PREFIX).append(BOUNDARY).append(LINE_END);
                    sb.append("Content-Disposition: form-data; name=\"").append(key).append("\"").append(LINE_END).append(LINE_END);
                    sb.append(param.get(key)).append(LINE_END);
                    String params = sb.toString();
                    Log.i(TAG, String.valueOf(key) + "=" + params + "##");
                    dos.write(params.getBytes());
                }
            }
            StringBuffer sb2 = new StringBuffer();
            sb2.append(PREFIX).append(BOUNDARY).append(LINE_END);
            sb2.append("Content-Disposition:form-data; name=\"" + fileKey + "\"; filename=\"" + file.getName() + "\"" + LINE_END);
            sb2.append("Content-Type:image/pjpeg\r\n");
            sb2.append(LINE_END);
            String params2 = sb2.toString();
            Log.i(TAG, String.valueOf(file.getName()) + "=" + params2 + "##");
            dos.write(params2.getBytes());
            InputStream is = new FileInputStream(file);
            this.onUploadProcessListener.initUpload((int) file.length());
            byte[] bytes = new byte[1024];
            int curLen = 0;
            while (true) {
                int len = is.read(bytes);
                if (len == -1) {
                    break;
                }
                curLen += len;
                dos.write(bytes, 0, len);
                this.onUploadProcessListener.onUploadProcess(curLen);
            }
            is.close();
            dos.write(LINE_END.getBytes());
            dos.write((PREFIX + BOUNDARY + PREFIX + LINE_END).getBytes());
            dos.flush();
            int res = conn.getResponseCode();
            requestTime = (int) ((System.currentTimeMillis() - requestTime2) / 1000);
            Log.e(TAG, "response code:" + res);
            if (res == 200) {
                Log.e(TAG, "request success");
                InputStream input = conn.getInputStream();
                StringBuffer sb1 = new StringBuffer();
                while (true) {
                    int ss = input.read();
                    if (ss == -1) {
                        String result = sb1.toString();
                        Log.e(TAG, "result : " + result);
                        sendMessage(1, "上传结果：" + result);
                        return;
                    }
                    sb1.append((char) ss);
                }
            } else {
                Log.e(TAG, "request error");
                sendMessage(3, "上传失败：code=" + res);
            }
        } catch (MalformedURLException e) {
            sendMessage(3, "上传失败：error=" + e.getMessage());
            e.printStackTrace();
        } catch (IOException e2) {
            sendMessage(3, "上传失败：error=" + e2.getMessage());
            e2.printStackTrace();
        }
    }

    private void weibosUploadFile(File file, String fileKey, String RequestURL, Map<String, String> param) {
        requestTime = 0;
        long requestTime2 = System.currentTimeMillis();
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(RequestURL).openConnection();
            conn.setReadTimeout(this.readTimeOut);
            conn.setConnectTimeout(this.connectTimeout);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Charset", CHARSET);
            conn.setRequestProperty("connection", "keep-alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
            if (param != null && param.size() > 0) {
                for (String key : param.keySet()) {
                    StringBuffer sb = new StringBuffer();
                    sb.append(PREFIX).append(BOUNDARY).append(LINE_END);
                    sb.append("Content-Disposition: form-data; name=\"").append(key).append("\"").append(LINE_END).append(LINE_END);
                    sb.append(param.get(key)).append(LINE_END);
                    String params = sb.toString();
                    Log.i(TAG, String.valueOf(key) + "=" + params + "##");
                    dos.write(params.getBytes());
                }
            }
            StringBuffer sb2 = new StringBuffer();
            sb2.append(PREFIX).append(BOUNDARY).append(LINE_END);
            sb2.append("Content-Disposition:form-data; name=\"" + fileKey + "\"; filename=\"" + file.getName() + "\"" + LINE_END);
            sb2.append("Content-Type:image/pjpeg\r\n");
            sb2.append(LINE_END);
            String params2 = sb2.toString();
            Log.i(TAG, String.valueOf(file.getName()) + "=" + params2 + "##");
            dos.write(params2.getBytes());
            InputStream is = new FileInputStream(file);
            this.onUploadProcessListener.initUpload((int) file.length());
            byte[] bytes = new byte[1024];
            int curLen = 0;
            while (true) {
                int len = is.read(bytes);
                if (len == -1) {
                    break;
                }
                curLen += len;
                dos.write(bytes, 0, len);
                this.onUploadProcessListener.onUploadProcess(curLen);
            }
            is.close();
            dos.write(LINE_END.getBytes());
            dos.write((PREFIX + BOUNDARY + PREFIX + LINE_END).getBytes());
            dos.flush();
            int res = conn.getResponseCode();
            requestTime = (int) ((System.currentTimeMillis() - requestTime2) / 1000);
            Log.e(TAG, "response code:" + res);
            if (res == 200) {
                Log.e(TAG, "request success");
                InputStream input = conn.getInputStream();
                StringBuffer sb1 = new StringBuffer();
                while (true) {
                    int ss = input.read();
                    if (ss == -1) {
                        String result = sb1.toString();
                        Log.e(TAG, "result : " + result);
                        sendMessage(1, "上传结果：" + result);
                        return;
                    }
                    sb1.append((char) ss);
                }
            } else {
                Log.e(TAG, "request error");
                sendMessage(3, "上传失败：code=" + res);
            }
        } catch (MalformedURLException e) {
            sendMessage(3, "上传失败：error=" + e.getMessage());
            e.printStackTrace();
        } catch (IOException e2) {
            sendMessage(3, "上传失败：error=" + e2.getMessage());
            e2.printStackTrace();
        }
    }

    private void sendMessage(int responseCode, String responseMessage) {
        this.onUploadProcessListener.onUploadDone(responseCode, responseMessage);
    }

    public void setOnUploadProcessListener(OnUploadProcessListener onUploadProcessListener2) {
        this.onUploadProcessListener = onUploadProcessListener2;
    }

    public int getReadTimeOut() {
        return this.readTimeOut;
    }

    public void setReadTimeOut(int readTimeOut2) {
        this.readTimeOut = readTimeOut2;
    }

    public int getConnectTimeout() {
        return this.connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout2) {
        this.connectTimeout = connectTimeout2;
    }

    public static int getRequestTime() {
        return requestTime;
    }
}
