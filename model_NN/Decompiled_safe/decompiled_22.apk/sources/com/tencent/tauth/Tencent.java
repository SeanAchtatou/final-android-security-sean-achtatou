package com.tencent.tauth;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.webkit.CookieSyncManager;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.open.AsynLoadImg;
import com.tencent.open.AsynLoadImgBack;
import com.tencent.open.OpenApi;
import com.tencent.open.OpenUi;
import com.tencent.open.TContext;
import com.tencent.open.TemporaryStorage;
import com.tencent.open.TencentStat;
import com.tencent.open.Util;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class Tencent {
    private OpenApi mOpenApi = new OpenApi(this.mTContext);
    private OpenUi mOpenUi = new OpenUi(this.mTContext);
    private TContext mTContext;

    private Tencent(String str, Context context) {
        this.mTContext = new TContext(str, context);
        TencentStat.a(this.mTContext, str);
    }

    public static Tencent createInstance(String str, Context context) {
        try {
            context.getPackageManager().getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.tauth.AuthActivity"), 0);
            return new Tencent(str, context);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Tencent", ("没有在AndroidManifest.xml中检测到com.tencent.tauth.AuthActivity,请加上com.tencent.open.AuthActivity,并配置<data android:scheme=\"tencent" + str + "\" />,详细信息请查看官网文档.") + "\n配置示例如下: \n<activity\n     android:name=\"com.tencent.tauth.AuthActivity\"\n     android:noHistory=\"true\"\n     android:launchMode=\"singleTask\">\n<intent-filter>\n    <action android:name=\"android.intent.action.VIEW\" />\n     <category android:name=\"android.intent.category.DEFAULT\" />\n    <category android:name=\"android.intent.category.BROWSABLE\" />\n    <data android:scheme=\"tencent" + str + "\" />\n" + "</intent-filter>\n" + "</activity>");
            return null;
        }
    }

    public boolean isSessionValid() {
        return this.mTContext.a();
    }

    public String getAccessToken() {
        return this.mTContext.b();
    }

    public void setAccessToken(String str, String str2) {
        this.mTContext.a(str, str2);
    }

    public String getOpenId() {
        return this.mTContext.c();
    }

    public void setOpenId(String str) {
        this.mTContext.a(str);
        TencentStat.b(this.mTContext, str);
    }

    public String getAppId() {
        return this.mTContext.d();
    }

    public int login(Activity activity, String str, IUiListener iUiListener) {
        Bundle bundle = new Bundle();
        bundle.putString("scope", str);
        return this.mOpenUi.a(activity, Constants.ACTION_LOGIN, bundle, iUiListener);
    }

    public int reAuth(Activity activity, String str, IUiListener iUiListener) {
        Bundle bundle = new Bundle();
        bundle.putString("scope", str);
        bundle.putString("isadd", UploadUtils.FAILURE);
        return this.mOpenUi.a(activity, Constants.ACTION_LOGIN, bundle, iUiListener, true);
    }

    public void logout(Context context) {
        CookieSyncManager.createInstance(context);
        setAccessToken(null, null);
        setOpenId(null);
    }

    public int invite(Activity activity, Bundle bundle, IUiListener iUiListener) {
        TencentStat.a(this.mTContext, "requireApi", "invite");
        return this.mOpenUi.a(activity, Constants.ACTION_INVITE, bundle, iUiListener);
    }

    public int story(Activity activity, Bundle bundle, IUiListener iUiListener) {
        TencentStat.a(this.mTContext, "requireApi", "story");
        return this.mOpenUi.a(activity, Constants.ACTION_STORY, bundle, iUiListener);
    }

    public int ask(Activity activity, Bundle bundle, IUiListener iUiListener) {
        bundle.putString("type", "request");
        return this.mOpenUi.a(activity, Constants.ACTION_ASK, bundle, iUiListener);
    }

    public int gift(Activity activity, Bundle bundle, IUiListener iUiListener) {
        bundle.putString("type", "freegift");
        return this.mOpenUi.a(activity, Constants.ACTION_GIFT, bundle, iUiListener);
    }

    public int pay(Activity activity, IUiListener iUiListener) {
        Bundle bundle = new Bundle();
        bundle.putString("scope", "");
        return this.mOpenUi.a(activity, Constants.ACTION_PAY, bundle, iUiListener);
    }

    public int challenge(Activity activity, Bundle bundle, IUiListener iUiListener) {
        bundle.putString("type", "pk");
        return this.mOpenUi.a(activity, Constants.ACTION_CHALLENGE, bundle, iUiListener);
    }

    public int brag(Activity activity, Bundle bundle, IUiListener iUiListener) {
        bundle.putString("type", "brag");
        return this.mOpenUi.a(activity, Constants.ACTION_BRAG, bundle, iUiListener);
    }

    public void getAppFriends(IRequestListener iRequestListener) {
        this.mOpenApi.a(iRequestListener);
    }

    public JSONObject request(String str, Bundle bundle, String str2) {
        return this.mOpenApi.a(this.mTContext.f(), str, bundle, str2);
    }

    public void requestAsync(String str, Bundle bundle, String str2, IRequestListener iRequestListener, Object obj) {
        this.mOpenApi.a(this.mTContext.f(), str, bundle, str2, iRequestListener, obj);
    }

    public boolean onActivityResult(int i, int i2, Intent intent) {
        return this.mOpenUi.a(i, i2, intent);
    }

    public String getSDKVersion() {
        return OpenApi.a();
    }

    public void setAvatar(Activity activity, Bundle bundle) {
        setAvatar(activity, bundle, null);
    }

    public void setAvatar(Activity activity, Bundle bundle, IUiListener iUiListener) {
        bundle.putString(Constants.PARAM_APP_ID, this.mTContext.d());
        bundle.putString("access_token", this.mTContext.b());
        bundle.putLong("expires_in", this.mTContext.e());
        bundle.putString("openid", this.mTContext.c());
        this.mOpenUi.b(activity, Constants.ACTION_AVATAR, bundle, iUiListener);
    }

    public void setAvatar(Activity activity, Bundle bundle, IUiListener iUiListener, int i, int i2) {
        bundle.putInt("exitAnim", i2);
        activity.overridePendingTransition(i, 0);
        setAvatar(activity, bundle, iUiListener);
    }

    public void shareToQQ(Activity activity, Bundle bundle, IUiListener iUiListener) {
        String string = bundle.getString(Constants.PARAM_IMAGE_URL);
        String string2 = bundle.getString(Constants.PARAM_TITLE);
        String string3 = bundle.getString(Constants.PARAM_SUMMARY);
        String string4 = bundle.getString(Constants.PARAM_TARGET_URL);
        if (!Util.e(string) && !string.contains("http://") && !string.contains("https://")) {
            string = "";
        }
        if (!Util.e(string4) && !string4.contains("http://") && !string4.contains("https://")) {
            string4 = "";
        }
        if (!Util.b()) {
            iUiListener.onError(new UiError(-6, Constants.MSG_SHARE_NOSD_ERROR, null));
            Log.v("shareToQQ", Constants.MSG_SHARE_NOSD_ERROR);
        } else if ((!Util.e(string2) || !Util.e(string3) || !Util.e(string)) && !Util.e(string4)) {
            if (!Util.e(string2) && string2.length() > 40) {
                bundle.putString(Constants.PARAM_TITLE, string2.substring(0, 40) + "...");
            }
            if (!Util.e(string3) && string3.length() > 80) {
                bundle.putString(Constants.PARAM_SUMMARY, string3.substring(0, 80) + "...");
            }
            if (Util.b(activity)) {
                shareToMobileQQ(activity, bundle, iUiListener);
            } else {
                shareToH5QQ(activity, bundle, iUiListener);
            }
        } else {
            iUiListener.onError(new UiError(-6, Constants.MSG_PARAM_ERROR, null));
        }
    }

    private void shareToMobileQQ(Activity activity, Bundle bundle, IUiListener iUiListener) {
        Object a = TemporaryStorage.a("shareToQQ", iUiListener);
        if (a != null) {
            ((IUiListener) a).onCancel();
        }
        String string = bundle.getString(Constants.PARAM_IMAGE_URL);
        final String string2 = bundle.getString(Constants.PARAM_TITLE);
        final String string3 = bundle.getString(Constants.PARAM_SUMMARY);
        Log.v("shareToQQ", "imageUrl:" + string + ", title:" + string2 + ",summary:" + string3);
        if (string == null || string.equals("") || !string.contains("http://")) {
            doShareToQQ(activity, bundle, iUiListener);
        } else if (!Util.e(string2) || !Util.e(string3) || Util.b()) {
            final Bundle bundle2 = bundle;
            final IUiListener iUiListener2 = iUiListener;
            final Activity activity2 = activity;
            new AsynLoadImg(activity).a(string, new AsynLoadImgBack() {
                public void saved(int i, String str) {
                    if (i == 0) {
                        bundle2.putString("imageLocalUrl", str);
                    } else if (Util.e(string2) && Util.e(string3)) {
                        iUiListener2.onError(new UiError(-6, Constants.MSG_SHARE_GETIMG_ERROR, null));
                        Log.v("shareToQQ", Constants.MSG_SHARE_GETIMG_ERROR);
                        return;
                    }
                    Tencent.this.doShareToQQ(activity2, bundle2, iUiListener2);
                }
            });
        } else {
            iUiListener.onError(new UiError(-6, Constants.MSG_SHARE_NOSD_ERROR, null));
            Log.v("shareToQQ", Constants.MSG_SHARE_NOSD_ERROR);
        }
    }

    /* access modifiers changed from: private */
    public void doShareToQQ(Activity activity, Bundle bundle, IUiListener iUiListener) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        String str = "mqqapi://share/to_fri?src_type=app&version=1&file_type=news";
        String string = bundle.getString(Constants.PARAM_IMAGE_URL);
        String string2 = bundle.getString(Constants.PARAM_TITLE);
        String string3 = bundle.getString(Constants.PARAM_SUMMARY);
        String string4 = bundle.getString(Constants.PARAM_TARGET_URL);
        String string5 = bundle.getString(Constants.PARAM_APPNAME);
        String string6 = bundle.getString("imageLocalUrl");
        String appId = getAppId();
        if (!Util.e(string)) {
            str = str + "&image_url=" + Base64.encodeToString(string.getBytes(), 2);
        }
        if (!Util.e(string6)) {
            str = str + "&file_data=" + Base64.encodeToString(string6.getBytes(), 2);
        }
        if (!Util.e(string2)) {
            str = str + "&title=" + Base64.encodeToString(string2.getBytes(), 2);
        }
        if (!Util.e(string3)) {
            str = str + "&description=" + Base64.encodeToString(string3.getBytes(), 2);
        }
        if (!Util.e(appId)) {
            str = str + "&share_id=" + appId;
        }
        if (!Util.e(string4)) {
            str = str + "&url=" + Base64.encodeToString(string4.getBytes(), 2);
        }
        if (!Util.e(string5)) {
            if (string5.length() > 20) {
                string5 = string5.substring(0, 20) + "...";
            }
            str = str + "&app_name=" + Base64.encodeToString(string5.getBytes(), 2);
        }
        Log.v("shareToQQ", str);
        TencentStat.a(this.mTContext, "requireApi", "shareToNativeQQ");
        Bundle bundle2 = new Bundle();
        bundle2.putString("scheme", str);
        if (!this.mOpenUi.b(activity, Constants.ACTION_SHARE_QQ, bundle2, iUiListener) && iUiListener != null) {
            iUiListener.onError(new UiError(-6, Constants.MSG_SHARE_TO_QQ_ERROR, null));
        }
    }

    private void shareToH5QQ(Activity activity, Bundle bundle, IUiListener iUiListener) {
        Object a = TemporaryStorage.a("shareToQQ", iUiListener);
        if (a != null) {
            ((IUiListener) a).onCancel();
        }
        if (bundle == null) {
            bundle = new Bundle();
        }
        String fillShareToQQParams = fillShareToQQParams("http://openmobile.qq.com/api/check?page=shareindex.html&style=9", bundle);
        TencentStat.a(this.mTContext, "requireApi", "shareToH5QQ");
        if (!Util.a(activity, fillShareToQQParams) && iUiListener != null) {
            iUiListener.onError(new UiError(-6, Constants.MSG_OPEN_BROWSER_ERROR, null));
        }
    }

    private String fillShareToQQParams(String str, Bundle bundle) {
        bundle.putString("action", "shareToQQ");
        bundle.putString("appId", getAppId());
        bundle.putString("sdkp", "a");
        bundle.putString("sdkv", Constants.SDK_VERSION);
        bundle.putString("status_os", Build.VERSION.RELEASE);
        bundle.putString("status_machine", Build.MODEL);
        if (bundle.containsKey("content") && bundle.getString("content").length() > 40) {
            bundle.putString("content", bundle.getString("content").substring(0, 40) + "...");
        }
        if (bundle.containsKey(Constants.PARAM_SUMMARY) && bundle.getString(Constants.PARAM_SUMMARY).length() > 80) {
            bundle.putString(Constants.PARAM_SUMMARY, bundle.getString(Constants.PARAM_SUMMARY).substring(0, 80) + "...");
        }
        return str + "&" + Util.a(bundle).replaceAll("\\+", "%20");
    }
}
