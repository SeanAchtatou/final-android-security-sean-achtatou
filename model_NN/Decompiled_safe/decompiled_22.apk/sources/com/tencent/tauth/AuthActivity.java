package com.tencent.tauth;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import com.tencent.open.BrowserAuth;
import com.tencent.open.TemporaryStorage;
import com.tencent.open.Util;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class AuthActivity extends Activity {
    private static final String TAG = "AuthActivity";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        handleActionUri(getIntent().getData());
    }

    private void handleActionUri(Uri uri) {
        if (uri == null || uri.toString().equals("")) {
            finish();
            return;
        }
        String uri2 = uri.toString();
        Bundle a = Util.a(uri2.substring(uri2.indexOf("#") + 1));
        String string = a.getString("action");
        if (string == null) {
            execAuthCallback(a, uri2);
        } else if (string.equals("shareToQQ")) {
            execShareToQQCallback(a);
        } else {
            execAuthCallback(a, uri2);
        }
    }

    private void execAuthCallback(Bundle bundle, String str) {
        BrowserAuth a = BrowserAuth.a();
        String string = bundle.getString("serial");
        BrowserAuth.Auth a2 = a.a(string);
        if (a2 != null) {
            if (str.indexOf("://cancel") != -1) {
                a2.a.onCancel();
                a2.b.dismiss();
            } else {
                String string2 = bundle.getString("access_token");
                if (string2 != null) {
                    bundle.putString("access_token", a.a(string2, a2.c));
                }
                JSONObject a3 = Util.a(new JSONObject(), Util.a(bundle));
                String optString = a3.optString("cb");
                if (!"".equals(optString)) {
                    a2.b.a(optString, a3.toString());
                } else {
                    a2.a.onComplete(a3);
                    a2.b.dismiss();
                }
            }
            a.b(string);
        }
        finish();
    }

    private void execShareToQQCallback(Bundle bundle) {
        String str;
        Object a = TemporaryStorage.a("shareToQQ");
        if (a == null) {
            finish();
            return;
        }
        IUiListener iUiListener = (IUiListener) a;
        String string = bundle.getString("result");
        String string2 = bundle.getString("response");
        if (string.equals("cancel")) {
            iUiListener.onCancel();
        } else if (string.equals("error")) {
            iUiListener.onError(new UiError(-6, "unknown error", string2 + ""));
        } else if (string.equals("complete")) {
            if (string2 == null) {
                str = "{\"ret\": 0}";
            } else {
                str = string2;
            }
            try {
                iUiListener.onComplete(new JSONObject(str));
            } catch (JSONException e) {
                e.printStackTrace();
                iUiListener.onError(new UiError(-4, "json error", str + ""));
            }
        }
        finish();
    }
}
