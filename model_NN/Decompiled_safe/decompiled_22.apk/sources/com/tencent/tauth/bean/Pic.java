package com.tencent.tauth.bean;

/* compiled from: ProGuard */
public class Pic {
    private String mAlbumId;
    private int mHeight;
    private String mLloc;
    private String mSloc;
    private int mWidth;

    public Pic(String str, String str2, String str3, int i, int i2) {
        this.mAlbumId = str;
        this.mLloc = str2;
        this.mSloc = str3;
        this.mWidth = i;
        this.mHeight = i2;
    }

    public String getAlbumId() {
        return this.mAlbumId;
    }

    public void setAlbumId(String str) {
        this.mAlbumId = str;
    }

    public String getLloc() {
        return this.mLloc;
    }

    public void setLloc(String str) {
        this.mLloc = str;
    }

    public String getSloc() {
        return this.mSloc;
    }

    public void setSloc(String str) {
        this.mSloc = str;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public void setWidth(int i) {
        this.mWidth = i;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public void setHeight(int i) {
        this.mHeight = i;
    }

    public String toString() {
        return "albumid :" + this.mAlbumId + "\nlloc: " + this.mLloc + "\nsloc: " + this.mSloc + "\nheight: " + this.mHeight + "\nwidth: " + this.mWidth + "\n";
    }
}
