package com.tencent.tauth.bean;

/* compiled from: ProGuard */
public class UserProfile {
    private int mGender;
    private String mIcon_100;
    private String mIcon_30;
    private String mIcon_50;
    private String mRealName;

    public UserProfile(String str, int i, String str2, String str3, String str4) {
        this.mRealName = str;
        this.mGender = i;
        this.mIcon_30 = str2;
        this.mIcon_50 = str3;
        this.mIcon_100 = str4;
    }

    public String getRealName() {
        return this.mRealName;
    }

    public void setNickName(String str) {
        this.mRealName = str;
    }

    public String getIcon_30() {
        return this.mIcon_30;
    }

    public void setIcon_30(String str) {
        this.mIcon_30 = str;
    }

    public String getIcon_50() {
        return this.mIcon_50;
    }

    public void setIcon_50(String str) {
        this.mIcon_50 = str;
    }

    public String getIcon_100() {
        return this.mIcon_100;
    }

    public void setIcon_100(String str) {
        this.mIcon_100 = str;
    }

    public int getGender() {
        return this.mGender;
    }

    public void setGender(int i) {
        this.mGender = i;
    }

    public String toString() {
        return "realName: " + this.mRealName + "\ngender: " + (this.mGender == 0 ? "女" : "男") + "\nicon_30: " + this.mIcon_30 + "\nicon_50: " + this.mIcon_50 + "\nicon_100: " + this.mIcon_100 + "\n";
    }
}
