package com.tencent.weibo.sdk.android.model;

public class AccountModel {
    private String accessToken;
    private long expiresIn;
    private String name;
    private String nike;
    private String openID;
    private String openKey;
    private String refreshToken;

    public AccountModel() {
    }

    public AccountModel(String token) {
        this.accessToken = token;
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    public void setAccessToken(String accessToken2) {
        this.accessToken = accessToken2;
    }

    public long getExpiresIn() {
        return this.expiresIn;
    }

    public void setExpiresIn(long expiresIn2) {
        this.expiresIn = expiresIn2;
    }

    public String getOpenID() {
        return this.openID;
    }

    public void setOpenID(String openID2) {
        this.openID = openID2;
    }

    public String getOpenKey() {
        return this.openKey;
    }

    public void setOpenKey(String openKey2) {
        this.openKey = openKey2;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }

    public void setRefreshToken(String refreshToken2) {
        this.refreshToken = refreshToken2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getNike() {
        return this.nike;
    }

    public void setNike(String nike2) {
        this.nike = nike2;
    }
}
