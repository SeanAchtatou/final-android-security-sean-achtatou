package com.tencent.weibo.sdk.android.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseVO implements Serializable {
    public static final int TYPE_BEAN = 0;
    public static final int TYPE_BEAN_LIST = 3;
    public static final int TYPE_JSON = 4;
    public static final int TYPE_LIST = 1;
    public static final int TYPE_OBJECT = 2;
    private static final long serialVersionUID = 8175948521471886407L;

    public Map<String, Object> analyseHead(JSONObject result) throws JSONException {
        Map<String, Object> map = new HashMap<>();
        JSONArray array = result.getJSONArray("result_list");
        int total = result.getInt("total");
        int p = result.getInt("p");
        int ps = result.getInt("ps");
        boolean isLastPage = result.getBoolean("is_last_list");
        map.put("array", array);
        map.put("total", Integer.valueOf(total));
        map.put("p", Integer.valueOf(p));
        map.put("ps", Integer.valueOf(ps));
        map.put("isLastPage", Boolean.valueOf(isLastPage));
        return map;
    }

    public Object analyseBody(JSONObject result) throws JSONException {
        return null;
    }

    public Object analyseBody(JSONArray result) throws JSONException {
        return null;
    }
}
