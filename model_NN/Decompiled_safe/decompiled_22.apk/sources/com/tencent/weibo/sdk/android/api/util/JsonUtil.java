package com.tencent.weibo.sdk.android.api.util;

import com.tencent.weibo.sdk.android.model.BaseVO;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class JsonUtil {
    public static BaseVO jsonToObject(Class<? extends BaseVO> target, JSONObject json) throws Exception {
        Map<String, Method> map = new HashMap<>();
        for (Method method : target.getMethods()) {
            if (method.getName().startsWith("set")) {
                map.put(method.getName().toLowerCase(), method);
            }
        }
        BaseVO vo = (BaseVO) target.newInstance();
        for (Field field : target.getDeclaredFields()) {
            String typeName = field.getType().getName();
            String name = field.getName();
            try {
                if (typeName.equals("boolean")) {
                    ((Method) map.get("set" + name.toLowerCase())).invoke(vo, Boolean.valueOf(json.getBoolean(name)));
                } else if (typeName.equals(Boolean.class.getName())) {
                    ((Method) map.get("set" + name.toLowerCase())).invoke(vo, Boolean.valueOf(json.getBoolean(name)));
                } else if (typeName.equals("int") || typeName.equals("byte")) {
                    ((Method) map.get("set" + name.toLowerCase())).invoke(vo, Integer.valueOf(json.getInt(name)));
                } else if (typeName.equals(Integer.class.getName()) || typeName.equals(Byte.class.getName())) {
                    ((Method) map.get("set" + name.toLowerCase())).invoke(vo, Integer.valueOf(json.getInt(name)));
                } else if (typeName.equals(String.class.getName())) {
                    ((Method) map.get("set" + name.toLowerCase())).invoke(vo, json.getString(name));
                } else if (typeName.equals("double") || typeName.equals("float")) {
                    ((Method) map.get("set" + name.toLowerCase())).invoke(vo, Double.valueOf(json.getDouble(name)));
                } else if (typeName.equals(Double.class.getName()) || typeName.equals(Float.class.getName())) {
                    ((Method) map.get("set" + name.toLowerCase())).invoke(vo, Double.valueOf(json.getDouble(name)));
                } else if (typeName.equals("long")) {
                    ((Method) map.get("set" + name.toLowerCase())).invoke(vo, Long.valueOf(json.getLong(name)));
                } else if (typeName.equals(Long.class.getName())) {
                    ((Method) map.get("set" + name.toLowerCase())).invoke(vo, Long.valueOf(json.getLong(name)));
                }
            } catch (Exception e) {
            }
        }
        return vo;
    }

    public static List<BaseVO> jsonToList(Class<? extends BaseVO> target, JSONArray array) throws Exception {
        List<BaseVO> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            list.add(jsonToObject(target, array.getJSONObject(i)));
        }
        return list;
    }
}
