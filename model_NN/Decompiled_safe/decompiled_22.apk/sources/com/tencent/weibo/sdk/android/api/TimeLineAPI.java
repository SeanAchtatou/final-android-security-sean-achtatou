package com.tencent.weibo.sdk.android.api;

import android.content.Context;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.tencent.weibo.sdk.android.network.ReqParam;
import sina_weibo.Constants;

public class TimeLineAPI extends BaseAPI {
    private static final String SERVER_URL_HOMETIMELINE = "https://open.t.qq.com/api/statuses/home_timeline";
    private static final String SERVER_URL_HTTIMELINE = "https://open.t.qq.com/api/statuses/ht_timeline_ext";
    private static final String SERVER_URL_USERTIMELINE = "https://open.t.qq.com/api/statuses/user_timeline";

    public TimeLineAPI(AccountModel account) {
        super(account);
    }

    public void getHomeTimeLine(Context context, int pageFlag, int pageTime, int reqnum, int type, int contenttype, String format, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("pageflag", Integer.valueOf(pageFlag));
        mParam.addParam("type", Integer.valueOf(type));
        mParam.addParam("reqnum", Integer.valueOf(reqnum));
        mParam.addParam("pagetime", Integer.valueOf(pageTime));
        mParam.addParam("contenttype", Integer.valueOf(contenttype));
        startRequest(context, SERVER_URL_HOMETIMELINE, mParam, mCallBack, mTargetClass, "GET", resultType);
    }

    public void getUserTimeLine(Context context, int pageFlag, int pageTime, int reqnum, int lastid, String name, String fopenid, int type, int contenttype, String format, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("pageflag", Integer.valueOf(pageFlag));
        mParam.addParam("pagetime", Integer.valueOf(pageTime));
        mParam.addParam("reqnum", Integer.valueOf(reqnum));
        mParam.addParam("lastid", Integer.valueOf(lastid));
        if (name != null && !"".equals(name)) {
            mParam.addParam(Constants.SINA_NAME, name);
        }
        if (fopenid != null && !"".equals(fopenid)) {
            mParam.addParam("fopenid", fopenid);
        }
        mParam.addParam("type", Integer.valueOf(type));
        mParam.addParam("contenttype", Integer.valueOf(contenttype));
        startRequest(context, SERVER_URL_USERTIMELINE, mParam, mCallBack, mTargetClass, "GET", resultType);
    }

    public void getHTTimeLine(Context context, String format, int reqnum, String tweetid, String time, int pageflag, int flag, String httext, String htid, int type, int contenttype, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("pageflag", Integer.valueOf(pageflag));
        mParam.addParam("reqnum", Integer.valueOf(reqnum));
        mParam.addParam("tweetid", tweetid);
        mParam.addParam("time", time);
        mParam.addParam("flag", Integer.valueOf(flag));
        if (httext != null && !"".equals(httext)) {
            mParam.addParam("httext", httext);
        }
        if (htid != null && !"".equals(htid) && !UploadUtils.SUCCESS.equals(htid)) {
            mParam.addParam("htid", htid);
        }
        mParam.addParam("type", Integer.valueOf(type));
        mParam.addParam("contenttype", Integer.valueOf(contenttype));
        startRequest(context, SERVER_URL_HTTIMELINE, mParam, mCallBack, mTargetClass, "GET", resultType);
    }
}
