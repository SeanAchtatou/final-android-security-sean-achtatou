package com.tencent.weibo.sdk.android.api.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.tencent.weibo.sdk.android.model.AccountModel;

public class SharePersistent {
    private static final String FILE_NAME = "ANDROID_SDK";
    private static SharePersistent instance;

    private SharePersistent() {
    }

    public static SharePersistent getInstance() {
        if (instance == null) {
            instance = new SharePersistent();
        }
        return instance;
    }

    public boolean put(Context context, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(FILE_NAME, 0).edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public boolean put(Context context, String key, long value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(FILE_NAME, 0).edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public AccountModel getAccount(Context context) {
        AccountModel account = new AccountModel();
        SharedPreferences settings = context.getSharedPreferences(FILE_NAME, 0);
        account.setAccessToken(settings.getString("ACCESS_TOKEN", ""));
        account.setExpiresIn(settings.getLong("EXPIRES_IN", 0));
        account.setOpenID(settings.getString("OPEN_ID", ""));
        account.setOpenKey(settings.getString("OPEN_KEY", ""));
        account.setRefreshToken(settings.getString("REFRESH_TOKEN", ""));
        account.setName(settings.getString("NAME", ""));
        account.setNike(settings.getString("NICK", ""));
        return account;
    }

    public String get(Context context, String key) {
        return context.getSharedPreferences(FILE_NAME, 0).getString(key, "");
    }

    public long getLong(Context context, String key) {
        return context.getSharedPreferences(FILE_NAME, 0).getLong(key, 0);
    }

    public boolean clear(Context context, String key) {
        return context.getSharedPreferences(FILE_NAME, 0).edit().clear().commit();
    }
}
