package com.tencent.weibo.sdk.android.api.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import com.tencent.weibo.sdk.android.model.Firend;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class FriendAdapter extends BaseExpandableListAdapter {
    private Map<String, List<Firend>> child;
    /* access modifiers changed from: private */
    public Context ctx;
    private List<String> group;

    public FriendAdapter(Context ctx2, List<String> group2, Map<String, List<Firend>> child2) {
        this.ctx = ctx2;
        this.group = group2;
        this.child = child2;
    }

    public Object getChild(int groupPosition, int childPosition) {
        return this.child.get(getGroup(groupPosition)).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return (long) childPosition;
    }

    public List<String> getGroup() {
        return this.group;
    }

    public void setGroup(List<String> group2) {
        this.group = group2;
    }

    public Map<String, List<Firend>> getChild() {
        return this.child;
    }

    public void setChild(Map<String, List<Firend>> child2) {
        this.child = child2;
    }

    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LinearLayout layout = new LinearLayout(this.ctx);
            new LinearLayout.LayoutParams(-1, -2);
            layout.setOrientation(0);
            layout.setGravity(16);
            layout.setPadding(15, 0, 10, 0);
            layout.setBackgroundDrawable(BackGroudSeletor.getdrawble("childbg_", this.ctx));
            ImageView imageView = new ImageView(this.ctx);
            imageView.setId(4502);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(45, 45));
            imageView.setImageDrawable(BackGroudSeletor.getdrawble("logo", this.ctx));
            LinearLayout layout_text = new LinearLayout(this.ctx);
            layout_text.setPadding(10, 0, 0, 0);
            layout_text.setGravity(16);
            layout_text.setOrientation(1);
            TextView textView_name = new TextView(this.ctx);
            TextView textView_content = new TextView(this.ctx);
            textView_name.setTextSize(18.0f);
            textView_name.setId(4500);
            textView_name.setTextColor(Color.parseColor("#32769b"));
            textView_content.setTextSize(12.0f);
            textView_content.setId(4501);
            textView_content.setTextColor(Color.parseColor("#a6c6d5"));
            textView_name.setText(((Firend) getChild(groupPosition, childPosition)).getNick());
            textView_content.setText(((Firend) getChild(groupPosition, childPosition)).getName());
            layout_text.addView(textView_name);
            layout_text.addView(textView_content);
            layout.setBackgroundDrawable(BackGroudSeletor.getdrawble("childbg_", this.ctx));
            layout.addView(imageView);
            layout.addView(layout_text);
            convertView = layout;
            convertView.setTag(layout);
        } else {
            LinearLayout layout2 = (LinearLayout) convertView.getTag();
            ((ImageView) ((LinearLayout) layout2.getTag()).findViewById(4502)).setImageDrawable(BackGroudSeletor.getdrawble("logo", this.ctx));
            ((TextView) layout2.findViewById(4500)).setText(((Firend) getChild(groupPosition, childPosition)).getNick());
            ((TextView) layout2.findViewById(4501)).setText(((Firend) getChild(groupPosition, childPosition)).getName());
        }
        final View t = convertView;
        if (((Firend) getChild(groupPosition, childPosition)).getHeadurl() != null) {
            new AsyncTask<String, Integer, Bitmap>() {
                /* access modifiers changed from: protected */
                public Bitmap doInBackground(String... params) {
                    try {
                        Log.d("image urel", new StringBuilder(String.valueOf(((Firend) FriendAdapter.this.getChild(groupPosition, childPosition)).getHeadurl())).toString());
                        URL url = new URL(((Firend) FriendAdapter.this.getChild(groupPosition, childPosition)).getHeadurl());
                        String responseCode = url.openConnection().getHeaderField(0);
                        if (responseCode.indexOf("200") < 0) {
                            Log.d("图片文件不存在或路径错误，错误代码：", responseCode);
                        }
                        return BitmapFactory.decodeStream(url.openStream());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                        return null;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        return null;
                    }
                }

                /* access modifiers changed from: protected */
                public void onPreExecute() {
                    super.onPreExecute();
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Bitmap result) {
                    ImageView imageView_icon = (ImageView) ((LinearLayout) t.getTag()).findViewById(4502);
                    if (result == null) {
                        imageView_icon.setImageDrawable(BackGroudSeletor.getdrawble("logo", FriendAdapter.this.ctx));
                    } else {
                        imageView_icon.setImageBitmap(result);
                    }
                    super.onPostExecute((Object) result);
                }
            }.execute(((Firend) getChild(groupPosition, childPosition)).getHeadurl());
        } else {
            ((ImageView) ((LinearLayout) convertView.getTag()).findViewById(4502)).setImageDrawable(BackGroudSeletor.getdrawble("logo", this.ctx));
        }
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return this.child.get(this.group.get(groupPosition)).size();
    }

    public Object getGroup(int groupPosition) {
        return this.group.get(groupPosition);
    }

    public int getGroupCount() {
        return this.group.size();
    }

    public long getGroupId(int groupPosition) {
        return (long) groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LinearLayout layout = new LinearLayout(this.ctx);
            TextView textView = new TextView(this.ctx);
            layout.setPadding(30, 0, 0, 0);
            layout.setGravity(16);
            textView.setTextSize(18.0f);
            textView.setTextColor(-1);
            textView.setWidth(40);
            textView.setText(getGroup(groupPosition).toString().toUpperCase());
            layout.addView(textView);
            layout.setBackgroundDrawable(BackGroudSeletor.getdrawble("groupbg_", this.ctx));
            textView.setTag(Integer.valueOf(groupPosition));
            View convertView2 = layout;
            convertView2.setTag(textView);
            return convertView2;
        }
        ((TextView) convertView.getTag()).setText(getGroup(groupPosition).toString().toUpperCase());
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
