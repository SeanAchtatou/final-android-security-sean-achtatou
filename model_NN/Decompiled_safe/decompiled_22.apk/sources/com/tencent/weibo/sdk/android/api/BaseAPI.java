package com.tencent.weibo.sdk.android.api;

import android.content.Context;
import android.util.Log;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.tencent.weibo.sdk.android.network.HttpReqWeiBo;
import com.tencent.weibo.sdk.android.network.HttpService;
import com.tencent.weibo.sdk.android.network.ReqParam;
import com.weibo.sdk.android.Weibo;
import sina_weibo.Constants;

public abstract class BaseAPI {
    public static final String API_SERVER = "https://open.t.qq.com/api";
    public static final String HTTPMETHOD_GET = "GET";
    public static final String HTTPMETHOD_POST = "POST";
    public static final String REQUEST_METHOD_GET = "GET";
    public static final String REQUEST_METHOD_POST = "POST";
    private HttpCallback callback = new HttpCallback() {
        public void onResult(Object object) {
            Log.d("sss", new StringBuilder().append(object).toString());
            if (object != null) {
                String[] params = ((ModelResult) object).getObj().toString().split("&");
                String access_token = params[0].split("=")[1];
                BaseAPI.this.mAccessToken = access_token;
                String expires_in = params[1].split("=")[1];
                String refresh_token = params[2].split("=")[1];
                String openid = params[3].split("=")[1];
                String name = params[4].split("=")[1];
                String nick = params[5].split("=")[1];
                Util.saveSharePersistent(BaseAPI.this.mContext, "ACCESS_TOKEN", access_token);
                Util.saveSharePersistent(BaseAPI.this.mContext, "EXPIRES_IN", expires_in);
                Util.saveSharePersistent(BaseAPI.this.mContext, "OPEN_ID", openid);
                Util.saveSharePersistent(BaseAPI.this.mContext, "REFRESH_TOKEN", refresh_token);
                Util.saveSharePersistent(BaseAPI.this.mContext, "NAME", name);
                Util.saveSharePersistent(BaseAPI.this.mContext, "NICK", nick);
                Util.saveSharePersistent(BaseAPI.this.mContext, "AUTHORIZETIME", String.valueOf(System.currentTimeMillis() / 1000));
                BaseAPI.this.weibo = new HttpReqWeiBo(BaseAPI.this.mContext, BaseAPI.this.mRequestUrl, BaseAPI.this.mmCallBack, BaseAPI.this.mmTargetClass, BaseAPI.this.mRequestMethod, Integer.valueOf(BaseAPI.this.mResultType));
                BaseAPI.this.mParams.addParam("access_token", BaseAPI.this.mAccessToken);
                BaseAPI.this.weibo.setParam(BaseAPI.this.mParams);
                HttpService.getInstance().addImmediateReq(BaseAPI.this.weibo);
            }
        }
    };
    /* access modifiers changed from: private */
    public String mAccessToken;
    private AccountModel mAccount;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public ReqParam mParams;
    /* access modifiers changed from: private */
    public String mRequestMethod;
    /* access modifiers changed from: private */
    public String mRequestUrl;
    /* access modifiers changed from: private */
    public int mResultType;
    /* access modifiers changed from: private */
    public HttpCallback mmCallBack;
    /* access modifiers changed from: private */
    public Class<? extends BaseVO> mmTargetClass;
    /* access modifiers changed from: private */
    public HttpReqWeiBo weibo;

    public BaseAPI(AccountModel account) {
        this.mAccount = account;
        if (this.mAccount != null) {
            this.mAccessToken = this.mAccount.getAccessToken();
        }
    }

    /* access modifiers changed from: protected */
    public void startRequest(Context context, String requestUrl, ReqParam params, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, String requestMethod, int resultType) {
        if (isAuthorizeExpired(context)) {
            this.mContext = context;
            this.mRequestUrl = requestUrl;
            this.mParams = params;
            this.mmCallBack = mCallBack;
            this.mmTargetClass = mTargetClass;
            this.mRequestMethod = requestMethod;
            this.mResultType = resultType;
            this.weibo = new HttpReqWeiBo(context, "https://open.t.qq.com/cgi-bin/oauth2/access_token", this.callback, null, "GET", 4);
            this.weibo.setParam(refreshToken(context));
            HttpService.getInstance().addImmediateReq(this.weibo);
            return;
        }
        this.weibo = new HttpReqWeiBo(context, requestUrl, mCallBack, mTargetClass, requestMethod, Integer.valueOf(resultType));
        params.addParam("access_token", this.mAccessToken);
        this.weibo.setParam(params);
        HttpService.getInstance().addImmediateReq(this.weibo);
    }

    private ReqParam refreshToken(Context context) {
        ReqParam param = new ReqParam();
        String clientId = Util.getSharePersistent(context, "CLIENT_ID");
        String refreshToken = Util.getSharePersistent(context, "REFRESH_TOKEN");
        param.addParam("client_id", clientId);
        param.addParam(Constants.SINA_GRANT_TYPE, Weibo.KEY_REFRESHTOKEN);
        param.addParam(Weibo.KEY_REFRESHTOKEN, refreshToken);
        param.addParam("state", Integer.valueOf((((int) Math.random()) * 1000) + 111));
        return param;
    }

    public boolean isAuthorizeExpired(Context context) {
        String authorizeTimeStr = Util.getSharePersistent(context, "AUTHORIZETIME");
        System.out.println("===== : " + authorizeTimeStr);
        String expiresTime = Util.getSharePersistent(context, "EXPIRES_IN");
        System.out.println("====== : " + expiresTime);
        long currentTime = System.currentTimeMillis() / 1000;
        if (expiresTime == null || authorizeTimeStr == null || Long.valueOf(authorizeTimeStr).longValue() + Long.valueOf(expiresTime).longValue() >= currentTime) {
            return false;
        }
        return true;
    }
}
