package com.tencent.weibo.sdk.android.network;

import android.content.Context;
import android.util.Log;
import com.baidu.push.example.Utils;
import com.city_life.part_asynctask.UploadUtils;
import com.ibm.mqtt.MqttUtils;
import com.tencent.tauth.Constants;
import com.tencent.weibo.sdk.android.api.util.JsonUtil;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpReq;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONArray;
import org.json.JSONObject;

public class HttpReqWeiBo extends HttpReq {
    private Context mContext;
    private Integer mResultType = 0;
    private Class<? extends BaseVO> mTargetClass;
    private Class<? extends BaseVO> mTargetClass2;

    public HttpReqWeiBo(Context context, String url, HttpCallback function, Class<? extends BaseVO> targetClass, String requestMethod, Integer resultType) {
        this.mContext = context;
        this.mHost = HttpConfig.CRM_SERVER_NAME;
        this.mPort = HttpConfig.CRM_SERVER_PORT;
        this.mUrl = url;
        this.mCallBack = function;
        this.mTargetClass = targetClass;
        this.mResultType = resultType;
        this.mMethod = requestMethod;
    }

    public void setmTargetClass(Class<? extends BaseVO> mTargetClass3) {
        this.mTargetClass = mTargetClass3;
    }

    public void setmTargetClass2(Class<? extends BaseVO> mTargetClass22) {
        this.mTargetClass2 = mTargetClass22;
    }

    public void setmResultType(Integer mResultType2) {
        this.mResultType = mResultType2;
    }

    /* access modifiers changed from: protected */
    public Object processResponse(InputStream response) throws Exception {
        int intValue;
        int intValue2;
        int intValue3;
        ModelResult modelResult = new ModelResult();
        if (response != null) {
            InputStreamReader ireader = new InputStreamReader(response);
            BufferedReader breader = new BufferedReader(ireader);
            StringBuffer sb = new StringBuffer();
            while (true) {
                String code = breader.readLine();
                if (code == null) {
                    break;
                }
                sb.append(code);
            }
            breader.close();
            ireader.close();
            Log.d("relst", sb.toString());
            if (sb.toString().indexOf(Utils.RESPONSE_ERRCODE) != -1 || sb.toString().indexOf("access_token") == -1) {
                JSONObject json = new JSONObject(sb.toString());
                BaseVO baseVO = null;
                if (this.mTargetClass != null) {
                    baseVO = (BaseVO) this.mTargetClass.newInstance();
                }
                String errorCode = json.getString(Utils.RESPONSE_ERRCODE);
                String msg = json.getString(Constants.PARAM_SEND_MSG);
                if (errorCode != null && UploadUtils.SUCCESS.equals(errorCode)) {
                    modelResult.setSuccess(true);
                    switch (this.mResultType.intValue()) {
                        case 0:
                            BaseVO vo = JsonUtil.jsonToObject(this.mTargetClass, json);
                            List<BaseVO> list = new ArrayList<>();
                            list.add(vo);
                            modelResult.setList(list);
                            break;
                        case 1:
                            Map<String, Object> map = baseVO.analyseHead(json);
                            List<BaseVO> list2 = JsonUtil.jsonToList(this.mTargetClass, (JSONArray) map.get("array"));
                            if (map.get("total") == null) {
                                intValue = 0;
                            } else {
                                intValue = ((Integer) map.get("total")).intValue();
                            }
                            Integer total = Integer.valueOf(intValue);
                            if (map.get("p") == null) {
                                intValue2 = 1;
                            } else {
                                intValue2 = ((Integer) map.get("p")).intValue();
                            }
                            Integer p = Integer.valueOf(intValue2);
                            if (map.get("ps") == null) {
                                intValue3 = 1;
                            } else {
                                intValue3 = ((Integer) map.get("ps")).intValue();
                            }
                            Integer ps = Integer.valueOf(intValue3);
                            boolean isLastPage = ((Boolean) map.get("isLastPage")).booleanValue();
                            modelResult.setList(list2);
                            modelResult.setTotal(total.intValue());
                            modelResult.setP(p.intValue());
                            modelResult.setPs(ps.intValue());
                            modelResult.setLastPage(isLastPage);
                            break;
                        case 2:
                            modelResult.setObj(JsonUtil.jsonToObject(this.mTargetClass, json));
                            break;
                        case 3:
                            BaseVO basebo = JsonUtil.jsonToObject(this.mTargetClass, json);
                            List<BaseVO> list3 = JsonUtil.jsonToList(this.mTargetClass2, json.getJSONArray("result_list"));
                            modelResult.setObj(basebo);
                            modelResult.setList(list3);
                            break;
                        case 4:
                            modelResult.setObj(json);
                            break;
                    }
                } else {
                    modelResult.setSuccess(false);
                    modelResult.setError_message(msg);
                }
            } else {
                modelResult.setObj(sb.toString());
            }
        }
        return modelResult;
    }

    /* access modifiers changed from: protected */
    public void setReq(HttpMethod method) throws Exception {
        if ("POST".equals(this.mMethod)) {
            PostMethod post = (PostMethod) method;
            String reqParam = this.mParam.toString();
            post.addParameter("Connection", "Keep-Alive");
            post.addParameter("Charset", MqttUtils.STRING_ENCODING);
            post.setRequestEntity(new ByteArrayRequestEntity(this.mParam.toString().getBytes("utf-8")));
        }
    }

    public void setReq(String url) throws Exception {
        if ("POST".equals(this.mMethod)) {
            PostMethod post = new HttpReq.UTF8PostMethod(this.mUrl);
            String reqParam = this.mParam.toString();
            post.setRequestEntity(new ByteArrayRequestEntity(this.mParam.toString().getBytes("utf-8")));
        }
    }
}
