package com.tencent.weibo.sdk.android.component.sso;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import com.ibm.mqtt.MqttUtils;
import com.tencent.weibo.sdk.android.component.sso.tools.Base64;
import com.tencent.weibo.sdk.android.component.sso.tools.Cryptor;
import com.tencent.weibo.sdk.android.component.sso.tools.MD5Tools;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Random;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public final class AuthHelper {
    static final String ENCRYPT_KEY = "&-*)Wb5_U,[^!9'+";
    static final int ERROR_WEIBO_INSTALL_NEEDED = -2;
    static final int ERROR_WEIBO_UPGRADE_NEEDED = -1;
    static final byte SDK_VERSION = 1;
    static final int SUPPORT_WEIBO_MIN_VERSION = 44;
    static final String WEIBO_AUTH_URL = "TencentAuth://weibo";
    static final String WEIBO_PACKAGE = "com.tencent.WBlog";
    static final int WEIBO_VALIDATE_OK = 0;
    protected static String appSecret;
    protected static long appid;
    protected static OnAuthListener listener;
    private static AuthReceiver mReceiver = new AuthReceiver();

    public static final void register(Context context, long appid2, String secret, OnAuthListener listener2) {
        appid = appid2;
        appSecret = secret;
        listener = listener2;
        IntentFilter filter = new IntentFilter("com.tencent.sso.AUTH");
        filter.addCategory("android.intent.category.DEFAULT");
        context.registerReceiver(mReceiver, filter);
    }

    public static final void unregister(Context context) {
        context.unregisterReceiver(mReceiver);
    }

    public static final boolean auth(Context context, String reserver) {
        int checkResult = validateWeiboApp(context);
        if (checkResult == 0) {
            long current = System.currentTimeMillis() / 1000;
            long nonce = (long) Math.abs(new Random().nextInt());
            String apkSignature = getApkSignature(context);
            byte[] signature = generateSignature(appid, appSecret, current, nonce);
            if (signature == null) {
                if (listener != null) {
                    listener.onAuthFail(-1, "");
                }
                return false;
            }
            byte[] signature2 = encypt(signature);
            String packageName = context.getPackageName();
            PackageManager packageManager = context.getPackageManager();
            String thisAppName = "";
            try {
                thisAppName = packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0)).toString();
            } catch (PackageManager.NameNotFoundException e) {
            }
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(WEIBO_AUTH_URL));
            intent.putExtra("com.tencent.sso.APP_ID", appid);
            intent.putExtra("com.tencent.sso.TIMESTAMP", current);
            intent.putExtra("com.tencent.sso.NONCE", nonce);
            intent.putExtra("com.tencent.sso.SDK_VERSION", (byte) SDK_VERSION);
            intent.putExtra("com.tencent.sso.PACKAGE_NAME", packageName);
            intent.putExtra("com.tencent.sso.ICON_MD5", apkSignature);
            intent.putExtra("com.tencent.sso.APP_NAME", thisAppName);
            intent.putExtra("com.tencent.sso.SIGNATURE", signature2);
            intent.putExtra("com.tencent.sso.RESERVER", reserver);
            context.startActivity(intent);
            return true;
        } else if (checkResult == -1) {
            if (listener != null) {
                listener.onWeiboVersionMisMatch();
            }
            return false;
        } else {
            if (checkResult == -2 && listener != null) {
                listener.onWeiBoNotInstalled();
            }
            return false;
        }
    }

    private static int validateWeiboApp(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo pi = packageManager.getPackageInfo(WEIBO_PACKAGE, 16);
            if (pi == null || pi.versionCode < 44 || packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(WEIBO_AUTH_URL)), 65536).size() <= 0) {
                return -1;
            }
            return 0;
        } catch (PackageManager.NameNotFoundException e) {
            return -2;
        }
    }

    private static byte[] generateSignature(long appid2, String appSecret2, long current, long nonce) {
        byte[] signature = null;
        StringBuffer sb = new StringBuffer();
        sb.append(appid2);
        sb.append(current);
        sb.append(nonce);
        sb.append(1);
        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(new SecretKeySpec(appSecret2.getBytes(MqttUtils.STRING_ENCODING), mac.getAlgorithm()));
            signature = mac.doFinal(sb.toString().getBytes(MqttUtils.STRING_ENCODING));
        } catch (Exception e) {
        }
        return Base64.encode(signature).getBytes();
    }

    private static byte[] encypt(byte[] signature) {
        return new Cryptor().encrypt(signature, ENCRYPT_KEY.getBytes());
    }

    private static String getApkSignature(Context context) {
        try {
            X509Certificate cert = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toByteArray()));
            StringBuffer sb = new StringBuffer();
            sb.append(cert.getPublicKey().toString());
            sb.append(cert.getSerialNumber().toString());
            return MD5Tools.toMD5(sb.toString());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (CertificateException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return "";
    }
}
