package com.tencent.weibo.sdk.android.network;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReqParam {
    public Bitmap mBitmap = null;
    private Map<String, String> mParams = new HashMap();

    public void setBitmap(Bitmap bm) {
        this.mBitmap = bm;
    }

    public Map<String, String> getmParams() {
        return this.mParams;
    }

    public void setmParams(Map<String, String> mParams2) {
        this.mParams = mParams2;
    }

    public void addParam(String key, String val) {
        this.mParams.put(key, val);
    }

    public void addParam(String key, byte[] val) {
        double size = (double) (val.length / 1024);
        if (size > 400.0d) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            double i = size / 400.0d;
            zoomImage(this.mBitmap, ((double) this.mBitmap.getWidth()) / Math.sqrt(i), ((double) this.mBitmap.getHeight()) / Math.sqrt(i)).compress(Bitmap.CompressFormat.JPEG, 100, baos);
            val = baos.toByteArray();
        }
        StringBuffer buffer = new StringBuffer();
        for (byte b : val) {
            buffer.append((char) b);
        }
        Log.d("buffer=======", buffer.toString());
        this.mParams.put(key, buffer.toString());
    }

    public void addParam(String key, Object val) {
        this.mParams.put(key, val.toString());
    }

    public String toString() {
        List<String> keyList = new ArrayList<>();
        for (String key : this.mParams.keySet()) {
            keyList.add(key);
        }
        Collections.sort(keyList, new Comparator<String>() {
            public int compare(String str1, String str2) {
                if (str1.compareTo(str2) > 0) {
                    return 1;
                }
                if (str1.compareTo(str2) < 0) {
                    return -1;
                }
                return 0;
            }
        });
        StringBuffer strbuf = new StringBuffer();
        for (String key2 : keyList) {
            if (!key2.equals("pic")) {
                strbuf.append(key2);
                strbuf.append("=");
                strbuf.append(this.mParams.get(key2));
                strbuf.append("&");
            }
        }
        Log.d("p-----", strbuf.toString());
        return strbuf.toString().replaceAll("\n", "").replaceAll("\r", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public Bitmap zoomImage(Bitmap bm, double newWidth, double newHeight) {
        float width = (float) bm.getWidth();
        float height = (float) bm.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) newWidth) / width, ((float) newHeight) / height);
        return Bitmap.createBitmap(bm, 0, 0, (int) width, (int) height, matrix, true);
    }
}
