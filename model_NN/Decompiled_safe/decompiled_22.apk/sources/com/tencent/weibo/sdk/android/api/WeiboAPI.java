package com.tencent.weibo.sdk.android.api;

import android.content.Context;
import android.graphics.Bitmap;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.tencent.weibo.sdk.android.network.ReqParam;
import java.io.ByteArrayOutputStream;
import sina_weibo.Constants;

public class WeiboAPI extends BaseAPI {
    private static final String SERVER_URL_ADD = "https://open.t.qq.com/api/t/add_multi";
    private static final String SERVER_URL_ADDPIC = "https://open.t.qq.com/api/t/add_pic";
    private static final String SERVER_URL_ADDPICURL = "https://open.t.qq.com/api/t/add_pic_url";
    private static final String SERVER_URL_ADDWEIBO = "https://open.t.qq.com/api/t/add";
    private static final String SERVER_URL_RLIST = "https://open.t.qq.com/api/t/re_list";
    private static final String SERVER_URL_VIDEO = "https://open.t.qq.com/api/t/getvideoinfo";

    public WeiboAPI(AccountModel account) {
        super(account);
    }

    public void reAddWeibo(Context context, String content, String picUrl, String videoUrl, String musicUrl, String musicTitle, String musicAuthor, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam("content", content);
        mParam.addParam(Constants.TX_PIC_URL, picUrl);
        mParam.addParam("video_url", videoUrl);
        mParam.addParam("music_url", musicUrl);
        mParam.addParam("music_title", musicTitle);
        mParam.addParam("music_author", musicAuthor);
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam("pageflag", UploadUtils.SUCCESS);
        mParam.addParam("type", UploadUtils.SUCCESS);
        mParam.addParam(Constants.TX_API_FORMAT, "json");
        mParam.addParam("reqnum", "30");
        mParam.addParam("pagetime", UploadUtils.SUCCESS);
        mParam.addParam("contenttype", UploadUtils.SUCCESS);
        startRequest(context, SERVER_URL_ADD, mParam, mCallBack, mTargetClass, "POST", resultType);
    }

    public void getVideoInfo(Context context, String videoUrl, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, "json");
        mParam.addParam("video_url", videoUrl);
        startRequest(context, SERVER_URL_VIDEO, mParam, mCallBack, mTargetClass, "POST", resultType);
    }

    public void addWeibo(Context context, String content, String format, double longitude, double latitude, int syncflag, int compatibleflag, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("content", content);
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        if (longitude != 0.0d) {
            mParam.addParam(Constants.TX_API_LONGITUDE, Double.valueOf(longitude));
        }
        if (latitude != 0.0d) {
            mParam.addParam(Constants.TX_API_LATITUDE, Double.valueOf(latitude));
        }
        mParam.addParam(Constants.TX_API_SYNCFLAG, Integer.valueOf(syncflag));
        mParam.addParam(Constants.TX_API_COMPATIBLEFLAG, Integer.valueOf(compatibleflag));
        startRequest(context, SERVER_URL_ADDWEIBO, mParam, mCallBack, mTargetClass, "POST", resultType);
    }

    public void addPic(Context context, String content, String format, double longitude, double latitude, Bitmap bm, int syncflag, int compatibleflag, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_FORMAT, format);
        if (content == null || "".equals(content)) {
            content = "#分享图片#";
        }
        mParam.addParam("content", content);
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        if (longitude != 0.0d) {
            mParam.addParam(Constants.TX_API_LONGITUDE, Double.valueOf(longitude));
        }
        if (latitude != 0.0d) {
            mParam.addParam(Constants.TX_API_LATITUDE, Double.valueOf(latitude));
        }
        mParam.addParam(Constants.TX_API_SYNCFLAG, Integer.valueOf(syncflag));
        mParam.addParam(Constants.TX_API_COMPATIBLEFLAG, Integer.valueOf(compatibleflag));
        mParam.setBitmap(bm);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        mParam.addParam("pic", baos.toByteArray());
        startRequest(context, SERVER_URL_ADDPIC, mParam, mCallBack, mTargetClass, "POST", resultType);
    }

    public void addPicUrl(Context context, String content, String format, double longitude, double latitude, String pic, int syncflag, int compatibleflag, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("content", content);
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        if (longitude != 0.0d) {
            mParam.addParam(Constants.TX_API_LONGITUDE, Double.valueOf(longitude));
        }
        if (latitude != 0.0d) {
            mParam.addParam(Constants.TX_API_LATITUDE, Double.valueOf(latitude));
        }
        mParam.addParam(Constants.TX_API_SYNCFLAG, Integer.valueOf(syncflag));
        mParam.addParam(Constants.TX_API_COMPATIBLEFLAG, Integer.valueOf(compatibleflag));
        mParam.addParam(Constants.TX_PIC_URL, pic);
        startRequest(context, SERVER_URL_ADDPICURL, mParam, mCallBack, mTargetClass, "POST", resultType);
    }

    public void reList(Context context, String format, int flag, String rootid, int pageflag, String pagetime, int reqnum, String twitterid, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("flag", Integer.valueOf(flag));
        mParam.addParam("rootid", rootid);
        mParam.addParam("pageflag", Integer.valueOf(pageflag));
        mParam.addParam("pagetime", pagetime);
        mParam.addParam("reqnum", Integer.valueOf(reqnum));
        mParam.addParam("twitterid", twitterid);
        startRequest(context, SERVER_URL_RLIST, mParam, mCallBack, mTargetClass, "GET", resultType);
    }
}
