package com.tencent.weibo.sdk.android.network;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class HttpService {
    private static HttpService instance = null;
    private final int TAG_RUNNING;
    private final int TAG_WAITTING;
    private List<HttpReq> mRunningReqList;
    private int mThreadNum;
    private List<HttpReq> mWaitReqList;

    public static HttpService getInstance() {
        if (instance == null) {
            instance = new HttpService();
        }
        return instance;
    }

    private HttpService() {
        this.TAG_RUNNING = 1;
        this.TAG_WAITTING = 0;
        this.mWaitReqList = null;
        this.mRunningReqList = null;
        this.mThreadNum = 4;
        this.mWaitReqList = new LinkedList();
        this.mRunningReqList = new LinkedList();
    }

    public void addImmediateReq(HttpReq req) {
        req.setServiceTag(1);
        this.mRunningReqList.add(req);
        req.execute(new Void[0]);
    }

    public void addNormalReq(HttpReq req) {
        if (this.mRunningReqList.size() < this.mThreadNum) {
            req.setServiceTag(1);
            this.mRunningReqList.add(req);
            req.execute(new Void[0]);
            return;
        }
        req.setServiceTag(0);
        this.mWaitReqList.add(req);
    }

    public void cancelReq(HttpReq req) {
        if (req.getServiceTag() == 1) {
            for (HttpReq oneReq : this.mRunningReqList) {
                if (oneReq == req) {
                    req.cancel(true);
                    this.mRunningReqList.remove(req);
                }
            }
        } else if (req.getServiceTag() == 0) {
            for (HttpReq httpReq : this.mWaitReqList) {
                if (req == httpReq) {
                    this.mWaitReqList.remove(req);
                }
            }
        }
    }

    public void cancelAllReq() {
        for (HttpReq oneReq : this.mRunningReqList) {
            oneReq.cancel(true);
        }
        this.mWaitReqList.clear();
    }

    public void SetAsynchronousTaskNum(int n) {
    }

    public void onReqFinish(HttpReq req) {
        Iterator<HttpReq> it = this.mRunningReqList.iterator();
        while (true) {
            if (it.hasNext()) {
                if (req == it.next()) {
                    it.remove();
                    break;
                }
            } else {
                break;
            }
        }
        if (this.mWaitReqList.size() > 0 && this.mRunningReqList.size() < this.mThreadNum) {
            Iterator<HttpReq> it2 = this.mWaitReqList.iterator();
            HttpReq oneReq = it2.next();
            it2.remove();
            oneReq.setServiceTag(1);
            this.mRunningReqList.add(oneReq);
            oneReq.execute(new Void[0]);
        }
    }
}
