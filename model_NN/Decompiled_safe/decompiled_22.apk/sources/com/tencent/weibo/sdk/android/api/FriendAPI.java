package com.tencent.weibo.sdk.android.api;

import android.content.Context;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.tencent.weibo.sdk.android.network.ReqParam;
import sina_weibo.Constants;

public class FriendAPI extends BaseAPI {
    private static final String SERVER_URL_ADD = "https://open.t.qq.com/api/friends/add";
    private static final String SERVER_URL_CHECK = "https://open.t.qq.com/api/friends/check";
    private static final String SERVER_URL_FANSLIST = "https://open.t.qq.com/api/friends/fanslist";
    private static final String SERVER_URL_GetINTIMATEFRIENDS = "https://open.t.qq.com/api/friends/get_intimate_friends";
    private static final String SERVER_URL_IDOLLIST = "https://open.t.qq.com/api/friends/idollist";
    private static final String SERVER_URL_MUTUALLIST = "https://open.t.qq.com/api/friends/mutual_list";

    public FriendAPI(AccountModel account) {
        super(account);
    }

    public void getMutualList(Context context, String format, String name, String fopenid, int startindex, int reqnum, int install, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("reqnum", Integer.valueOf(reqnum));
        mParam.addParam("install", Integer.valueOf(install));
        mParam.addParam("startindex", Integer.valueOf(startindex));
        if (name != null && !"".equals(name)) {
            mParam.addParam(Constants.SINA_NAME, name);
        }
        if (fopenid != null && !"".equals(fopenid)) {
            mParam.addParam("fopenid", fopenid);
        }
        startRequest(context, "https://open.t.qq.com/api/friends/mutual_list", mParam, mCallBack, mTargetClass, "GET", resultType);
    }

    public void addFriend(Context context, String format, String name, String fopenids, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        if (name != null && !"".equals(name)) {
            mParam.addParam(Constants.SINA_NAME, name);
        }
        if (fopenids != null && !"".equals(fopenids)) {
            mParam.addParam("fopenids", fopenids);
        }
        startRequest(context, SERVER_URL_ADD, mParam, mCallBack, mTargetClass, "POST", resultType);
    }

    public void friendIDolList(Context context, String format, int reqnum, int startindex, int mode, int install, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("reqnum", Integer.valueOf(reqnum));
        mParam.addParam("startindex", Integer.valueOf(startindex));
        mParam.addParam("mode", Integer.valueOf(mode));
        mParam.addParam("install", Integer.valueOf(install));
        startRequest(context, SERVER_URL_IDOLLIST, mParam, mCallBack, mTargetClass, "GET", resultType);
    }

    public void friendFansList(Context context, String format, int reqnum, int startindex, int mode, int install, int sex, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("reqnum", Integer.valueOf(reqnum));
        mParam.addParam("startindex", Integer.valueOf(startindex));
        mParam.addParam("mode", Integer.valueOf(mode));
        mParam.addParam("install", Integer.valueOf(install));
        mParam.addParam("sex", Integer.valueOf(sex));
        startRequest(context, SERVER_URL_FANSLIST, mParam, mCallBack, mTargetClass, "GET", resultType);
    }

    public void friendCheck(Context context, String format, String names, String fopenids, int flag, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("names", names);
        mParam.addParam("fopenids", fopenids);
        mParam.addParam("flag", Integer.valueOf(flag));
        startRequest(context, SERVER_URL_CHECK, mParam, mCallBack, mTargetClass, "GET", resultType);
    }

    public void getIntimateFriends(Context context, String format, int reqnum, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam("reqnum", Integer.valueOf(reqnum));
        startRequest(context, SERVER_URL_GetINTIMATEFRIENDS, mParam, mCallBack, mTargetClass, "GET", resultType);
    }
}
