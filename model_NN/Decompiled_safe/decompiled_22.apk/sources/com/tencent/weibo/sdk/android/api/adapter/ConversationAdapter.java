package com.tencent.weibo.sdk.android.api.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import java.util.List;

public class ConversationAdapter extends BaseAdapter {
    private Context ct;
    private List<String> cvlist;

    public ConversationAdapter(Context ct2, List<String> cvlist2) {
        this.ct = ct2;
        this.cvlist = cvlist2;
    }

    public int getCount() {
        return this.cvlist.size();
    }

    public Object getItem(int position) {
        return this.cvlist.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public List<String> getCvlist() {
        return this.cvlist;
    }

    public void setCvlist(List<String> cvlist2) {
        this.cvlist = cvlist2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LinearLayout layout = new LinearLayout(this.ct);
            new LinearLayout.LayoutParams(-1, -2);
            layout.setBackgroundDrawable(BackGroudSeletor.getdrawble("topic_", this.ct));
            layout.setGravity(16);
            layout.setPadding(10, 0, 10, 0);
            TextView textView = new TextView(this.ct);
            textView.setTextColor(Color.parseColor("#108ece"));
            textView.setText(getItem(position).toString());
            textView.setGravity(16);
            textView.setTextSize(18.0f);
            layout.addView(textView);
            View convertView2 = layout;
            convertView2.setTag(textView);
            return convertView2;
        }
        ((TextView) convertView.getTag()).setText(getItem(position).toString());
        return convertView;
    }
}
