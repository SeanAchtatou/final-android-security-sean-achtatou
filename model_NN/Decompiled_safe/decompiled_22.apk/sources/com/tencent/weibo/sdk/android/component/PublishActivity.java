package com.tencent.weibo.sdk.android.component;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.mm.sdk.conversation.RConversation;
import com.tencent.weibo.sdk.android.api.WeiboAPI;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class PublishActivity extends Activity implements View.OnClickListener, HttpCallback {
    private String accessToken;
    private ImageButton button_camera;
    private ImageButton button_conversation;
    private Button button_esc;
    private ImageButton button_friend;
    /* access modifiers changed from: private */
    public ImageButton button_location;
    private Button button_send;
    /* access modifiers changed from: private */
    public Context context;
    private ProgressDialog dialog;
    /* access modifiers changed from: private */
    public EditText editText_text;
    /* access modifiers changed from: private */
    public String edstring = "";
    /* access modifiers changed from: private */
    public FrameLayout frameLayout_big;
    /* access modifiers changed from: private */
    public FrameLayout frameLayout_icon;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int flag = msg.what;
            if (flag == 5) {
                PublishActivity.this.frameLayout_big.setVisibility(0);
                PublishActivity.this.frameLayout_icon.setVisibility(0);
                return;
            }
            if (flag == 0) {
                PublishActivity.this.popupWindow.showAsDropDown(PublishActivity.this.layout_set);
                InputMethodManager imm = (InputMethodManager) PublishActivity.this.getSystemService("input_method");
                Log.d("alive", new StringBuilder(String.valueOf(imm.isActive())).toString());
                if (imm.isActive()) {
                    imm.hideSoftInputFromWindow(PublishActivity.this.editText_text.getWindowToken(), 0);
                    Log.d("alive", new StringBuilder(String.valueOf(imm.isActive())).toString());
                }
            }
            if (flag == 10) {
                PublishActivity.this.button_location.setBackgroundDrawable(BackGroudSeletor.getdrawble("dingwei_icon_hover2x", PublishActivity.this));
            }
            if (flag == 15) {
                Toast.makeText(PublishActivity.this, "定位失败", 0).show();
                PublishActivity.this.button_location.setBackgroundDrawable(BackGroudSeletor.getdrawble("dingwei_icon2x", PublishActivity.this));
            }
        }
    };
    private ImageView imageView_big;
    private ImageView imageView_bound;
    private ImageView imageView_delete;
    private ImageView imageView_icon;
    private LinearLayout layout_big_delete;
    private LinearLayout layout_imagebound;
    /* access modifiers changed from: private */
    public LinearLayout layout_set;
    private Map<String, String> location;
    private int[] lyout = new int[2];
    private Bitmap mBitmap = null;
    /* access modifiers changed from: private */
    public Location mLocation;
    /* access modifiers changed from: private */
    public PopupWindow popupWindow;
    /* access modifiers changed from: private */
    public TextView textView_num;
    private LinearLayout viewroot;
    private WeiboAPI weiboAPI;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.accessToken = Util.getSharePersistent(getApplicationContext(), "ACCESS_TOKEN");
        if (this.accessToken == null || "".equals(this.accessToken)) {
            Toast.makeText(this, "请先授权", 0).show();
            finish();
            return;
        }
        this.context = getApplicationContext();
        this.weiboAPI = new WeiboAPI(new AccountModel(this.accessToken));
        this.lyout[0] = BackGroudSeletor.getdrawble("test2x", this).getMinimumWidth();
        this.lyout[1] = BackGroudSeletor.getdrawble("test2x", this).getMinimumHeight();
        this.dialog = new ProgressDialog(this);
        this.dialog.setMessage("正在发送请稍后......");
        setContentView((LinearLayout) initview());
        setonclick();
        new Timer().schedule(new TimerTask() {
            public void run() {
                ((InputMethodManager) PublishActivity.this.getSystemService("input_method")).showSoftInput(PublishActivity.this.editText_text, 2);
            }
        }, 400);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        final InputMethodManager imm = (InputMethodManager) getSystemService("input_method");
        if (this.popupWindow == null || !this.popupWindow.isShowing()) {
            new Timer().schedule(new TimerTask() {
                public void run() {
                    imm.showSoftInput(PublishActivity.this.editText_text, 2);
                }
            }, 400);
        } else {
            Log.d("mkl", new StringBuilder(String.valueOf(imm.isActive())).toString());
            imm.hideSoftInputFromWindow(this.editText_text.getWindowToken(), 0);
        }
        if (this.location != null) {
            this.button_location.setBackgroundDrawable(BackGroudSeletor.getdrawble("dingwei_icon_hover2x", this));
        } else {
            this.button_location.setBackgroundDrawable(BackGroudSeletor.getdrawble("dingwei_icon2x", this));
        }
    }

    private View initview() {
        this.viewroot = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        FrameLayout.LayoutParams layoutParams_frame = new FrameLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        this.viewroot.setLayoutParams(layoutParams);
        this.viewroot.setOrientation(1);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        RelativeLayout.LayoutParams fillWrapParams = new RelativeLayout.LayoutParams(-1, -2);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(fillWrapParams);
        relativeLayout.setBackgroundDrawable(BackGroudSeletor.getdrawble("up_bg2x", getApplication()));
        relativeLayout.setGravity(0);
        this.button_esc = new Button(this);
        layoutParams2.addRule(9, -1);
        layoutParams2.addRule(15, -1);
        layoutParams2.topMargin = 10;
        layoutParams2.leftMargin = 10;
        layoutParams2.bottomMargin = 10;
        this.button_esc.setLayoutParams(layoutParams2);
        this.button_esc.setText("取消");
        this.button_esc.setClickable(true);
        this.button_esc.setId(5001);
        this.button_esc.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"quxiao_btn2x", "quxiao_btn_hover"}, this));
        this.button_send = new Button(this);
        RelativeLayout.LayoutParams wrapParamsRight = new RelativeLayout.LayoutParams(-2, -2);
        wrapParamsRight.addRule(11, -1);
        wrapParamsRight.addRule(15, -1);
        wrapParamsRight.topMargin = 10;
        wrapParamsRight.rightMargin = 10;
        wrapParamsRight.bottomMargin = 10;
        this.button_send.setLayoutParams(wrapParamsRight);
        LinearLayout linearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2, 1.0f);
        linearLayout.setLayoutParams(layoutParams4);
        this.button_send.setText("发送");
        this.button_send.setClickable(true);
        this.button_send.setId(5002);
        this.button_send.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"sent_btn_22x", "sent_btn_hover"}, this));
        relativeLayout.addView(this.button_esc);
        relativeLayout.addView(this.button_send);
        LinearLayout layout_content = new LinearLayout(this);
        layout_content.setLayoutParams(layoutParams3);
        layout_content.setLayoutParams(layoutParams3);
        layout_content.setOrientation(1);
        layout_content.setBackgroundColor(-1);
        layout_content.requestFocus();
        this.editText_text = new EditText(this);
        this.editText_text.setBackgroundColor(-1);
        this.editText_text.setMaxLines(4);
        this.editText_text.setMinLines(4);
        this.editText_text.setMinEms(4);
        this.editText_text.setMaxEms(4);
        this.editText_text.setFocusable(true);
        this.editText_text.requestFocus();
        this.editText_text.setText(this.edstring);
        this.editText_text.setSelection(this.edstring.length());
        this.editText_text.setScrollbarFadingEnabled(true);
        this.editText_text.setGravity(48);
        this.editText_text.setMovementMethod(ScrollingMovementMethod.getInstance());
        this.editText_text.setId(5003);
        this.frameLayout_icon = new FrameLayout(this);
        this.frameLayout_icon.setLayoutParams(layoutParams_frame);
        LinearLayout layout_icon = new LinearLayout(this);
        layout_icon.setGravity(21);
        layout_icon.setLayoutParams(new LinearLayout.LayoutParams(54, 45));
        layout_icon.setPadding(0, 0, 2, 0);
        this.imageView_icon = new ImageView(this);
        this.imageView_icon.setId(5004);
        this.imageView_bound = new ImageView(this);
        this.imageView_bound.setId(5005);
        this.imageView_bound.setLayoutParams(new LinearLayout.LayoutParams(54, 45));
        this.imageView_icon.setLayoutParams(new LinearLayout.LayoutParams(33, 33));
        this.imageView_bound.setImageDrawable(BackGroudSeletor.getdrawble("composeimageframe", this));
        this.frameLayout_icon.setVisibility(8);
        layout_icon.addView(this.imageView_icon);
        this.frameLayout_icon.addView(layout_icon);
        this.frameLayout_icon.addView(this.imageView_bound);
        layout_content.addView(this.editText_text);
        layout_content.addView(this.frameLayout_icon);
        this.layout_set = new LinearLayout(this);
        this.layout_set.setLayoutParams(layoutParams3);
        this.layout_set.setBackgroundDrawable(BackGroudSeletor.getdrawble("icon_bg2x", this));
        this.layout_set.setOrientation(0);
        this.layout_set.setGravity(16);
        this.layout_set.setPadding(10, 0, 30, 0);
        LinearLayout layout_function = new LinearLayout(this);
        layout_function.setOrientation(0);
        layout_function.setLayoutParams(layoutParams4);
        LinearLayout layout_friend = new LinearLayout(this);
        layout_friend.setGravity(1);
        layout_friend.setLayoutParams(layoutParams4);
        LinearLayout layout_conversation = new LinearLayout(this);
        layout_conversation.setGravity(1);
        layout_conversation.setLayoutParams(layoutParams4);
        LinearLayout layout_camera = new LinearLayout(this);
        layout_camera.setGravity(1);
        layout_camera.setLayoutParams(layoutParams4);
        LinearLayout layout_location = new LinearLayout(this);
        layout_location.setGravity(1);
        layout_location.setLayoutParams(layoutParams4);
        this.button_friend = new ImageButton(this);
        this.button_friend.setLayoutParams(layoutParams_frame);
        this.button_friend.setId(5006);
        this.button_conversation = new ImageButton(this);
        this.button_conversation.setLayoutParams(layoutParams_frame);
        this.button_conversation.setId(5007);
        this.button_camera = new ImageButton(this);
        this.button_camera.setLayoutParams(layoutParams_frame);
        this.button_camera.setId(5008);
        this.button_location = new ImageButton(this);
        this.button_location.setLayoutParams(layoutParams_frame);
        this.button_location.setId(5009);
        this.button_friend.setBackgroundDrawable(BackGroudSeletor.getdrawble("haoyou_icon2x", this));
        this.button_conversation.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"huati_icon2x", "huati_icon_hover2x"}, this));
        this.button_camera.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"pic_icon2x", "pic_icon_hover2x"}, this));
        this.button_location.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"dingwei_icon2x", "dingwei_icon_hover2x"}, this));
        layout_friend.addView(this.button_friend);
        layout_function.addView(layout_friend);
        layout_conversation.addView(this.button_conversation);
        layout_function.addView(layout_conversation);
        layout_camera.addView(this.button_camera);
        layout_function.addView(layout_camera);
        layout_location.addView(this.button_location);
        layout_function.addView(layout_location);
        this.textView_num = new TextView(this);
        this.textView_num.setText("140");
        this.textView_num.setTextColor(Color.parseColor("#999999"));
        this.textView_num.setGravity(5);
        this.textView_num.setLayoutParams(layoutParams4);
        this.textView_num.setId(5010);
        this.textView_num.setWidth(40);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setLayoutParams(layoutParams4);
        linearLayout2.setGravity(21);
        linearLayout2.addView(this.textView_num);
        this.layout_set.addView(layout_function);
        this.layout_set.addView(linearLayout2);
        LinearLayout layout_image = new LinearLayout(this);
        layout_image.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        layout_image.setGravity(17);
        layout_image.setBackgroundDrawable(BackGroudSeletor.getdrawble("bg", this));
        this.frameLayout_big = new FrameLayout(this);
        this.frameLayout_big.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        this.frameLayout_big.setPadding(10, 10, 0, 0);
        this.layout_imagebound = new LinearLayout(this);
        this.layout_imagebound.setPadding(2, 2, 2, 2);
        this.layout_imagebound.setBackgroundDrawable(BackGroudSeletor.getdrawble("pic_biankuang2x", this));
        this.layout_big_delete = new LinearLayout(this);
        this.layout_big_delete.setLayoutParams(new LinearLayout.LayoutParams(getarea(this.lyout)[0] + 10, getarea(this.lyout)[1] + 10));
        this.layout_imagebound.setGravity(17);
        this.layout_imagebound.setId(5011);
        this.layout_imagebound.setLayoutParams(new LinearLayout.LayoutParams(getarea(this.lyout)[0], getarea(this.lyout)[1]));
        this.imageView_big = new ImageView(this);
        this.imageView_big.setId(5012);
        this.layout_imagebound.addView(this.imageView_big);
        this.imageView_delete = new ImageView(this);
        this.imageView_delete.setId(5013);
        this.imageView_delete.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.imageView_delete.setImageDrawable(BackGroudSeletor.getdrawble("close", this));
        this.layout_big_delete.addView(this.imageView_delete);
        this.frameLayout_big.addView(this.layout_imagebound);
        this.frameLayout_big.addView(this.layout_big_delete);
        this.frameLayout_big.setVisibility(8);
        layout_image.addView(this.frameLayout_big);
        this.viewroot.addView(relativeLayout);
        this.viewroot.addView(layout_content);
        this.viewroot.addView(this.layout_set);
        this.viewroot.addView(layout_image);
        return this.viewroot;
    }

    private void setonclick() {
        this.button_esc.setOnClickListener(this);
        this.button_send.setOnClickListener(this);
        this.editText_text.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    Log.d("contentafter", new StringBuilder(String.valueOf(s.toString().getBytes("gbk").length)).toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                try {
                    PublishActivity.this.edstring = s.toString();
                    String num = new StringBuilder(String.valueOf(140 - (s.toString().getBytes("gbk").length / 2))).toString();
                    Log.d("contentafter", num);
                    PublishActivity.this.textView_num.setText(num);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        this.imageView_bound.setOnClickListener(this);
        this.imageView_delete.setOnClickListener(this);
        this.button_friend.setOnClickListener(this);
        this.button_conversation.setOnClickListener(this);
        this.button_camera.setOnClickListener(this);
        this.button_location.setOnClickListener(this);
    }

    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService("input_method");
        switch (v.getId()) {
            case 5001:
                imm.hideSoftInputFromWindow(this.editText_text.getWindowToken(), 0);
                finish();
                return;
            case 5002:
                String content = this.editText_text.getText().toString();
                if (!"".equals(content) || this.frameLayout_icon.getVisibility() != 8) {
                    if (this.dialog != null && !this.dialog.isShowing()) {
                        this.dialog.show();
                    }
                    if (Integer.parseInt(this.textView_num.getText().toString()) < 0) {
                        Toast.makeText(this, "请重新输入少于140个字的内容", 0).show();
                        return;
                    }
                    double longitude = 0.0d;
                    double latitude = 0.0d;
                    if (this.mLocation != null) {
                        longitude = this.mLocation.getLongitude();
                        latitude = this.mLocation.getLatitude();
                    }
                    if (!this.frameLayout_icon.isShown()) {
                        this.weiboAPI.addWeibo(this.context, content, "json", longitude, latitude, 0, 0, this, null, 4);
                        return;
                    } else if (this.frameLayout_icon.getVisibility() == 0) {
                        this.weiboAPI.addPic(this.context, content, "json", longitude, latitude, this.mBitmap, 0, 0, this, null, 4);
                        return;
                    } else {
                        return;
                    }
                } else {
                    Toast.makeText(this, "无内容发送", 0).show();
                    return;
                }
            case 5003:
            case 5004:
            case 5010:
            case 5011:
            case 5012:
            default:
                return;
            case 5005:
                imm.toggleSoftInput(0, 2);
                return;
            case 5006:
                imm.hideSoftInputFromWindow(this.editText_text.getWindowToken(), 0);
                Intent intent_friend = new Intent();
                intent_friend.setClass(this, FriendActivity.class);
                startActivityForResult(intent_friend, 5006);
                return;
            case 5007:
                imm.hideSoftInputFromWindow(this.editText_text.getWindowToken(), 0);
                Intent intent_conversation = new Intent();
                intent_conversation.setClass(this, ConversationActivity.class);
                startActivityForResult(intent_conversation, 5007);
                return;
            case 5008:
                if (this.popupWindow == null || !this.popupWindow.isShowing()) {
                    this.popupWindow = new PopupWindow(showView(), -1, -1);
                    this.popupWindow.setTouchable(true);
                    new Timer().schedule(new TimerTask() {
                        public void run() {
                            Message msg = PublishActivity.this.handler.obtainMessage();
                            msg.what = 0;
                            PublishActivity.this.handler.sendMessage(msg);
                        }
                    }, 500);
                    return;
                }
                this.popupWindow.dismiss();
                if (imm.isActive()) {
                    imm.toggleSoftInput(0, 2);
                    return;
                }
                return;
            case 5009:
                new Thread(new Runnable() {
                    public void run() {
                        Looper.prepare();
                        Message msg = PublishActivity.this.handler.obtainMessage();
                        msg.what = 15;
                        if (PublishActivity.this.mLocation == null) {
                            PublishActivity.this.mLocation = Util.getLocation(PublishActivity.this.context);
                            if (PublishActivity.this.mLocation != null) {
                                msg.what = 10;
                            }
                        }
                        PublishActivity.this.handler.sendMessage(msg);
                        Looper.loop();
                    }
                }).start();
                return;
            case 5013:
                this.frameLayout_icon.setVisibility(4);
                this.frameLayout_big.setVisibility(8);
                return;
            case 5014:
                this.edstring = this.editText_text.getText().toString();
                startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 2000);
                return;
            case 5015:
                this.edstring = this.editText_text.getText().toString();
                startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 1000);
                return;
            case 5016:
                if (this.popupWindow != null && this.popupWindow.isShowing()) {
                    this.popupWindow.dismiss();
                    this.editText_text.requestFocus();
                    final InputMethodManager inputMethodManager = imm;
                    new Timer().schedule(new TimerTask() {
                        public void run() {
                            if (inputMethodManager.isActive()) {
                                inputMethodManager.toggleSoftInput(0, 2);
                            }
                        }
                    }, 100);
                    return;
                }
                return;
        }
    }

    private View showView() {
        LinearLayout camera = new LinearLayout(this);
        camera.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        camera.setBackgroundDrawable(BackGroudSeletor.getdrawble("bg", this));
        camera.setOrientation(1);
        camera.setPadding(50, 50, 50, 50);
        camera.setGravity(17);
        camera.requestFocus();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -2);
        LinearLayout camera_layout = new LinearLayout(this);
        camera_layout.setLayoutParams(params);
        camera_layout.setPadding(0, 0, 0, 0);
        LinearLayout pic_layout = new LinearLayout(this);
        pic_layout.setLayoutParams(params);
        pic_layout.setPadding(0, 10, 0, 30);
        new LinearLayout(this);
        LinearLayout.LayoutParams button_Params = new LinearLayout.LayoutParams(-1, -2);
        Button camera_button = new Button(this);
        camera_button.setId(5014);
        camera_button.setOnClickListener(this);
        camera_button.setLayoutParams(button_Params);
        camera_button.setText("拍照");
        String[] camera_string = {"btn1_", "btn1_hover_"};
        camera_button.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(camera_string, this));
        Button pic_button = new Button(this);
        pic_button.setId(5015);
        pic_button.setOnClickListener(this);
        pic_button.setLayoutParams(button_Params);
        pic_button.setText("相册");
        pic_button.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(camera_string, this));
        Button esc_bButton = new Button(this);
        esc_bButton.setId(5016);
        esc_bButton.setOnClickListener(this);
        esc_bButton.setLayoutParams(button_Params);
        esc_bButton.setText("取消");
        esc_bButton.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"btn2_", "btn1_hover_"}, this));
        pic_layout.addView(pic_button);
        camera.addView(camera_button);
        camera.addView(pic_layout);
        camera.addView(esc_bButton);
        return camera;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000 && resultCode == -1 && data != null) {
            String[] filePathColumn = {"_data"};
            Cursor cursor = getContentResolver().query(data.getData(), filePathColumn, null, null, null);
            cursor.moveToFirst();
            String picturePath = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));
            Log.d("path", new StringBuilder(String.valueOf(picturePath)).toString());
            int[] iArr = new int[2];
            try {
                FileInputStream fileInputStream = new FileInputStream(picturePath);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = false;
                options.inSampleSize = 6;
                Bitmap bitmap = BitmapFactory.decodeStream(fileInputStream, null, options);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new ByteArrayOutputStream());
                this.mBitmap = bitmap;
                this.lyout[0] = bitmap.getWidth();
                this.lyout[1] = bitmap.getHeight();
                setContentView(initview());
                setonclick();
                this.imageView_icon.setImageDrawable(new BitmapDrawable(bitmap));
                this.imageView_big.setImageDrawable(new BitmapDrawable(bitmap));
                this.frameLayout_icon.setVisibility(0);
                this.frameLayout_big.setVisibility(0);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            cursor.close();
            if (this.popupWindow != null && this.popupWindow.isShowing()) {
                this.popupWindow.dismiss();
                new Timer().schedule(new TimerTask() {
                    public void run() {
                        InputMethodManager i = (InputMethodManager) PublishActivity.this.getSystemService("input_method");
                        Log.d("mks", new StringBuilder(String.valueOf(i.isActive())).toString());
                        i.toggleSoftInput(0, 2);
                        Message msg = PublishActivity.this.handler.obtainMessage();
                        msg.what = 5;
                        PublishActivity.this.handler.sendMessage(msg);
                    }
                }, 100);
            }
        } else if (requestCode == 2000 && resultCode == -1 && data != null) {
            if (this.popupWindow != null && this.popupWindow.isShowing()) {
                this.popupWindow.dismiss();
            }
            Bitmap bitmap2 = (Bitmap) data.getExtras().get("data");
            sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            bitmap2.compress(Bitmap.CompressFormat.PNG, 100, new ByteArrayOutputStream());
            this.mBitmap = bitmap2;
            this.lyout[0] = bitmap2.getWidth();
            this.lyout[1] = bitmap2.getHeight();
            setContentView(initview());
            setonclick();
            this.imageView_icon.setImageDrawable(new BitmapDrawable(bitmap2));
            this.imageView_big.setImageDrawable(new BitmapDrawable(bitmap2));
            this.frameLayout_icon.setVisibility(0);
            this.frameLayout_big.setVisibility(0);
        } else if (requestCode == 5007 && resultCode == -1 && data != null) {
            this.edstring = String.valueOf(this.edstring) + data.getStringExtra(RConversation.OLD_TABLE);
            this.editText_text.setText(this.edstring);
            this.editText_text.setSelection(this.edstring.length());
        } else if (requestCode == 5006 && resultCode == -1 && data != null) {
            this.edstring = String.valueOf(this.edstring) + "@" + data.getStringExtra("firend");
            this.editText_text.setText(this.edstring);
            this.editText_text.setSelection(this.edstring.length());
        }
    }

    public void onResult(Object object) {
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
        if (object != null) {
            ModelResult result = (ModelResult) object;
            if (result.isExpires()) {
                Toast.makeText(this, result.getError_message(), 0).show();
            } else if (result.isSuccess()) {
                Toast.makeText(this, "发送成功", 4000).show();
                Log.d("发送成功", object.toString());
                finish();
            } else {
                Toast.makeText(this, ((ModelResult) object).getError_message(), 4000).show();
            }
        }
    }

    private int[] getarea(int[] area) {
        int[] myarea = new int[2];
        if (area != null) {
            if (area[0] > area[1] && area[0] >= 300) {
                myarea[0] = 300;
                myarea[1] = (int) ((((float) area[1]) / ((float) area[0])) * 300.0f);
            } else if (area[0] > area[1] && area[0] < 300) {
                myarea[0] = area[0];
                myarea[1] = area[1];
            } else if (area[0] < area[1] && area[1] >= 300) {
                myarea[0] = (int) ((((float) area[0]) / ((float) area[1])) * 300.0f);
                myarea[1] = 300;
            } else if (area[0] < area[1] && area[0] < 300) {
                myarea[0] = area[0];
                myarea[1] = area[1];
            } else if (area[0] == area[1] && area[0] >= 300) {
                myarea[0] = 300;
                myarea[1] = 300;
            } else if (area[0] == area[1] && area[0] < 300) {
                myarea[0] = area[0];
                myarea[1] = area[1];
            }
        }
        Log.d("myarea", String.valueOf(myarea[0]) + "....." + myarea[1]);
        return myarea;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public Bitmap zoomImage(Bitmap bm, double newWidth, double newHeight) {
        float width = (float) bm.getWidth();
        float height = (float) bm.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) newWidth) / width, ((float) newHeight) / height);
        return Bitmap.createBitmap(bm, 0, 0, (int) width, (int) height, matrix, true);
    }
}
