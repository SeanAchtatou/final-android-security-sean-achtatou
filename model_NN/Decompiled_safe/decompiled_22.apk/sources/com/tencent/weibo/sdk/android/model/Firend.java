package com.tencent.weibo.sdk.android.model;

public class Firend {
    private String headurl;
    private String name;
    private String nick;

    public String getNick() {
        return this.nick;
    }

    public void setNick(String nick2) {
        this.nick = nick2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getHeadurl() {
        return this.headurl;
    }

    public void setHeadurl(String headurl2) {
        this.headurl = headurl2;
    }
}
