package com.tencent.weibo.sdk.android.component.sso.tools;

import com.tencent.mm.sdk.platformtools.Util;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

public class Cryptor {
    public static final int QUOTIENT = 79764919;
    public static final int SALT_LEN = 2;
    public static final int ZERO_LEN = 7;
    private int contextStart;
    private int crypt;
    private boolean header = true;
    private byte[] key;
    private byte[] out;
    private int padding;
    private byte[] plain;
    private int pos;
    private int preCrypt;
    private byte[] prePlain;
    private Random random = new Random();

    public static byte[] MD5Hash(byte[] byteIn, int nLen) {
        return new byte[2];
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int CRC32Hash(byte[] r8) {
        /*
            int r3 = r8.length
            r5 = -1
            r1 = 0
            r0 = 0
        L_0x0004:
            if (r0 < r3) goto L_0x0009
            r6 = r5 ^ -1
            return r6
        L_0x0009:
            byte r4 = r8[r0]
            r2 = 0
        L_0x000c:
            r6 = 8
            if (r2 < r6) goto L_0x0013
            int r0 = r0 + 1
            goto L_0x0004
        L_0x0013:
            r6 = r4 ^ r5
            int r6 = r6 >>> 31
            r7 = 1
            if (r6 != r7) goto L_0x0027
            int r6 = r5 << 1
            r7 = 79764919(0x4c11db7, float:4.540137E-36)
            r5 = r6 ^ r7
        L_0x0021:
            int r6 = r4 << 1
            byte r4 = (byte) r6
            int r2 = r2 + 1
            goto L_0x000c
        L_0x0027:
            int r5 = r5 << 1
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.weibo.sdk.android.component.sso.tools.Cryptor.CRC32Hash(byte[]):int");
    }

    public static byte[] _4bytesEncryptAFrame(int data, byte[] key2) {
        short[] v = {(short) (65535 & data), (short) (data >>> 16)};
        short[] k = {(short) ((key2[1] << 8) | key2[0]), (short) ((key2[3] << 8) | key2[2]), (short) ((key2[5] << 8) | key2[4]), (short) ((key2[7] << 8) | key2[6])};
        short y = v[0];
        short z = v[1];
        short sum = 0;
        short n = 32;
        while (true) {
            short n2 = n;
            n = (short) (n2 - 1);
            if (n2 <= 0) {
                return new byte[]{(byte) (z >> 8), (byte) (z & 255), (byte) (y >> 8), (byte) (y & 255)};
            }
            sum = (short) (sum + 12895);
            y = (short) (((((z << 4) + k[0]) ^ (z + sum)) ^ ((z >> 5) + k[1])) + y);
            z = (short) (((((y << 4) + k[2]) ^ (y + sum)) ^ ((y >> 5) + k[3])) + z);
        }
    }

    public static int _4bytesDecryptAFrame(long src, byte[] key2) {
        short[] v = {(short) ((int) (65535 & src)), (short) ((int) (src >> 16))};
        short[] k = {(short) ((key2[1] << 8) | key2[0]), (short) ((key2[3] << 8) | key2[2]), (short) ((key2[5] << 8) | key2[4]), (short) ((key2[7] << 8) | key2[6])};
        short n = 32;
        short y = v[0];
        short z = v[1];
        int i = 412640;
        while (true) {
            short sum = (short) i;
            short n2 = n;
            n = (short) (n2 - 1);
            if (n2 <= 0) {
                v[0] = y;
                v[1] = z;
                return (v[1] << 16) | (v[0] & 65535);
            }
            z = (short) (z - ((((y << 4) + k[2]) ^ (y + sum)) ^ ((y >> 5) + k[3])));
            y = (short) (y - ((((z << 4) + k[0]) ^ (z + sum)) ^ ((z >> 5) + k[1])));
            i = sum - 12895;
        }
    }

    public static long getUnsignedInt(byte[] in, int offset, int len) {
        int end;
        long ret = 0;
        if (len > 8) {
            end = offset + 8;
        } else {
            end = offset + len;
        }
        for (int i = offset; i < end; i++) {
            ret = (ret << 8) | ((long) (in[i] & 255));
        }
        return (Util.MAX_32BIT_VALUE & ret) | (ret >>> 32);
    }

    public byte[] decrypt(byte[] in, int offset, int len, byte[] key2) {
        this.preCrypt = 0;
        this.crypt = 0;
        this.key = key2;
        byte[] m = new byte[(offset + 8)];
        if (len % 8 != 0 || len < 16) {
            return null;
        }
        this.prePlain = decipher(in, offset);
        this.pos = this.prePlain[0] & 7;
        int count = (len - this.pos) - 10;
        if (count < 0) {
            return null;
        }
        for (int i = offset; i < m.length; i++) {
            m[i] = 0;
        }
        this.out = new byte[count];
        this.preCrypt = 0;
        this.crypt = 8;
        this.contextStart = 8;
        this.pos++;
        this.padding = 1;
        while (this.padding <= 2) {
            if (this.pos < 8) {
                this.pos++;
                this.padding++;
            }
            if (this.pos == 8) {
                m = in;
                if (!decrypt8Bytes(in, offset, len)) {
                    return null;
                }
            }
        }
        int i2 = 0;
        while (count != 0) {
            if (this.pos < 8) {
                this.out[i2] = (byte) (m[(this.preCrypt + offset) + this.pos] ^ this.prePlain[this.pos]);
                i2++;
                count--;
                this.pos++;
            }
            if (this.pos == 8) {
                m = in;
                this.preCrypt = this.crypt - 8;
                if (!decrypt8Bytes(in, offset, len)) {
                    return null;
                }
            }
        }
        this.padding = 1;
        while (this.padding < 8) {
            if (this.pos < 8) {
                if ((m[(this.preCrypt + offset) + this.pos] ^ this.prePlain[this.pos]) != 0) {
                    return null;
                }
                this.pos++;
            }
            if (this.pos == 8) {
                m = in;
                this.preCrypt = this.crypt;
                if (!decrypt8Bytes(in, offset, len)) {
                    return null;
                }
            }
            this.padding++;
        }
        return this.out;
    }

    public byte[] decrypt(byte[] in, byte[] key2) {
        return decrypt(in, 0, in.length, key2);
    }

    public byte[] encrypt(byte[] in, int offset, int len, byte[] key2) {
        int i;
        this.plain = new byte[8];
        this.prePlain = new byte[8];
        this.pos = 1;
        this.padding = 0;
        this.preCrypt = 0;
        this.crypt = 0;
        this.key = key2;
        this.header = true;
        this.pos = (len + 10) % 8;
        if (this.pos != 0) {
            this.pos = 8 - this.pos;
        }
        this.out = new byte[(this.pos + len + 10)];
        this.plain[0] = (byte) ((rand() & 248) | this.pos);
        for (int i2 = 1; i2 <= this.pos; i2++) {
            this.plain[i2] = (byte) (rand() & 255);
        }
        this.pos++;
        for (int i3 = 0; i3 < 8; i3++) {
            this.prePlain[i3] = 0;
        }
        this.padding = 1;
        while (this.padding <= 2) {
            if (this.pos < 8) {
                byte[] bArr = this.plain;
                int i4 = this.pos;
                this.pos = i4 + 1;
                bArr[i4] = (byte) (rand() & 255);
                this.padding++;
            }
            if (this.pos == 8) {
                encrypt8Bytes();
            }
        }
        int i5 = offset;
        while (len > 0) {
            if (this.pos < 8) {
                byte[] bArr2 = this.plain;
                int i6 = this.pos;
                this.pos = i6 + 1;
                i = i5 + 1;
                bArr2[i6] = in[i5];
                len--;
            } else {
                i = i5;
            }
            if (this.pos == 8) {
                encrypt8Bytes();
            }
            i5 = i;
        }
        this.padding = 1;
        while (this.padding <= 7) {
            if (this.pos < 8) {
                byte[] bArr3 = this.plain;
                int i7 = this.pos;
                this.pos = i7 + 1;
                bArr3[i7] = 0;
                this.padding++;
            }
            if (this.pos == 8) {
                encrypt8Bytes();
            }
        }
        return this.out;
    }

    public byte[] encrypt(byte[] in, byte[] key2) {
        return encrypt(in, 0, in.length, key2);
    }

    private byte[] encipher(byte[] in) {
        int loop = 16;
        try {
            long y = getUnsignedInt(in, 0, 4);
            long z = getUnsignedInt(in, 4, 4);
            long a = getUnsignedInt(this.key, 0, 4);
            long b = getUnsignedInt(this.key, 4, 4);
            long c = getUnsignedInt(this.key, 8, 4);
            long d = getUnsignedInt(this.key, 12, 4);
            long sum = 0;
            long delta = -1640531527 & Util.MAX_32BIT_VALUE;
            while (true) {
                int loop2 = loop;
                loop = loop2 - 1;
                if (loop2 <= 0) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream(8);
                    DataOutputStream dos = new DataOutputStream(baos);
                    dos.writeInt((int) y);
                    dos.writeInt((int) z);
                    dos.close();
                    return baos.toByteArray();
                }
                sum = (sum + delta) & Util.MAX_32BIT_VALUE;
                y = (y + ((((z << 4) + a) ^ (z + sum)) ^ ((z >>> 5) + b))) & Util.MAX_32BIT_VALUE;
                z = (z + ((((y << 4) + c) ^ (y + sum)) ^ ((y >>> 5) + d))) & Util.MAX_32BIT_VALUE;
            }
        } catch (IOException e) {
            return null;
        }
    }

    private byte[] decipher(byte[] in, int offset) {
        int loop = 16;
        try {
            long y = getUnsignedInt(in, offset, 4);
            long z = getUnsignedInt(in, offset + 4, 4);
            long a = getUnsignedInt(this.key, 0, 4);
            long b = getUnsignedInt(this.key, 4, 4);
            long c = getUnsignedInt(this.key, 8, 4);
            long d = getUnsignedInt(this.key, 12, 4);
            long sum = -478700656 & Util.MAX_32BIT_VALUE;
            long delta = -1640531527 & Util.MAX_32BIT_VALUE;
            while (true) {
                int loop2 = loop;
                loop = loop2 - 1;
                if (loop2 <= 0) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream(8);
                    DataOutputStream dos = new DataOutputStream(baos);
                    dos.writeInt((int) y);
                    dos.writeInt((int) z);
                    dos.close();
                    return baos.toByteArray();
                }
                z = (z - ((((y << 4) + c) ^ (y + sum)) ^ ((y >>> 5) + d))) & Util.MAX_32BIT_VALUE;
                y = (y - ((((z << 4) + a) ^ (z + sum)) ^ ((z >>> 5) + b))) & Util.MAX_32BIT_VALUE;
                sum = (sum - delta) & Util.MAX_32BIT_VALUE;
            }
        } catch (IOException e) {
            return null;
        }
    }

    private byte[] decipher(byte[] in) {
        return decipher(in, 0);
    }

    private void encrypt8Bytes() {
        this.pos = 0;
        while (this.pos < 8) {
            if (this.header) {
                byte[] bArr = this.plain;
                int i = this.pos;
                bArr[i] = (byte) (bArr[i] ^ this.prePlain[this.pos]);
            } else {
                byte[] bArr2 = this.plain;
                int i2 = this.pos;
                bArr2[i2] = (byte) (bArr2[i2] ^ this.out[this.preCrypt + this.pos]);
            }
            this.pos++;
        }
        System.arraycopy(encipher(this.plain), 0, this.out, this.crypt, 8);
        this.pos = 0;
        while (this.pos < 8) {
            byte[] bArr3 = this.out;
            int i3 = this.crypt + this.pos;
            bArr3[i3] = (byte) (bArr3[i3] ^ this.prePlain[this.pos]);
            this.pos++;
        }
        System.arraycopy(this.plain, 0, this.prePlain, 0, 8);
        this.preCrypt = this.crypt;
        this.crypt += 8;
        this.pos = 0;
        this.header = false;
    }

    private boolean decrypt8Bytes(byte[] in, int offset, int len) {
        this.pos = 0;
        while (this.pos < 8) {
            if (this.contextStart + this.pos >= len) {
                return true;
            }
            byte[] bArr = this.prePlain;
            int i = this.pos;
            bArr[i] = (byte) (bArr[i] ^ in[(this.crypt + offset) + this.pos]);
            this.pos++;
        }
        this.prePlain = decipher(this.prePlain);
        if (this.prePlain == null) {
            return false;
        }
        this.contextStart += 8;
        this.crypt += 8;
        this.pos = 0;
        return true;
    }

    private int rand() {
        return this.random.nextInt();
    }

    public byte[] decrypt(byte[] in, byte[] key2, int length) {
        byte[] data = decrypt(in, 0, in.length, key2);
        return data == null ? getRandomByte(length) : data;
    }

    private byte[] getRandomByte(int length) {
        byte[] data = new byte[length];
        this.random.nextBytes(data);
        return data;
    }
}
