package com.tencent.weibo.sdk.android.network;

import android.os.AsyncTask;
import android.util.Log;
import com.ibm.mqtt.MqttUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.zip.GZIPInputStream;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.MultipartPostMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public abstract class HttpReq extends AsyncTask<Void, Integer, Object> {
    private final String GET = "GET";
    private final String POST = "POST";
    protected HttpCallback mCallBack = null;
    protected String mHost = null;
    protected String mMethod = null;
    protected ReqParam mParam = new ReqParam();
    protected int mPort = HttpConfig.CRM_SERVER_PORT;
    private int mServiceTag = -1;
    protected String mUrl = null;

    /* access modifiers changed from: protected */
    public abstract Object processResponse(InputStream inputStream) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void setReq(HttpMethod httpMethod) throws Exception;

    public void setServiceTag(int nTag) {
        this.mServiceTag = nTag;
    }

    public int getServiceTag() {
        return this.mServiceTag;
    }

    /* access modifiers changed from: protected */
    public HttpCallback getCallBack() {
        return this.mCallBack;
    }

    public void setParam(ReqParam param) {
        this.mParam = param;
    }

    public void addParam(String key, String value) {
        this.mParam.addParam(key, value);
    }

    public void addParam(String key, Object value) {
        this.mParam.addParam(key, value);
    }

    public Object runReq() throws Exception {
        HttpMethod method;
        HttpClient client = new HttpClient();
        if (this.mMethod.equals("GET")) {
            this.mUrl = String.valueOf(this.mUrl) + "?" + this.mParam.toString().substring(0, this.mParam.toString().length() - 1);
            method = new GetMethod(this.mUrl);
        } else if (!this.mMethod.equals("POST")) {
            throw new Exception("unrecognized http method");
        } else if (this.mParam.getmParams().get("pic") != null) {
            return processResponse(picMethod());
        } else {
            method = new UTF8PostMethod(this.mUrl);
        }
        client.getHostConfiguration().setHost(this.mHost, this.mPort, "https");
        method.setRequestHeader("Content-Type", PostMethod.FORM_URL_ENCODED_CONTENT_TYPE);
        setReq(method);
        int statusCode = client.executeMethod(method);
        Log.d("result", new StringBuilder(String.valueOf(statusCode)).toString());
        if (statusCode != 200) {
            return null;
        }
        return processResponse(method.getResponseBodyAsStream());
    }

    private InputStream picMethod() {
        DefaultHttpClient client = new DefaultHttpClient();
        InputStream result = null;
        HttpPost post = new HttpPost(this.mUrl);
        HttpPost httpPost = post;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        String strparams = this.mParam.toString();
        post.setHeader("Content-Type", String.valueOf(MultipartPostMethod.MULTIPART_FORM_CONTENT_TYPE) + "; boundary=" + "---------------------------7da2137580612");
        if (strparams != null) {
            try {
                if (!strparams.equals("")) {
                    String[] params = strparams.split("&");
                    int length = params.length;
                    for (int i = 0; i < length; i++) {
                        String str = params[i];
                        if (str != null && !str.equals("") && str.indexOf("=") > -1) {
                            String[] p = str.split("=");
                            String value = p.length == 2 ? decode(p[1]) : "";
                            StringBuilder temp = new StringBuilder();
                            temp.append("-----------------------------7da2137580612\r\n");
                            temp.append("Content-Disposition:form-data; name=\"" + p[0] + "\"" + "\r\n");
                            temp.append("\r\n");
                            temp.append(value);
                            temp.append("\r\n");
                            bos.write(temp.toString().getBytes("utf-8"));
                        }
                    }
                    StringBuilder temp2 = new StringBuilder();
                    temp2.append("-----------------------------7da2137580612\r\n");
                    temp2.append("Content-Disposition:form-data; name=\"pic\"; filename=\"123456.jpg\"\r\n");
                    temp2.append("Content-Type:image/jpeg\r\n\r\n");
                    bos.write(temp2.toString().getBytes("utf-8"));
                    char[] pics = this.mParam.getmParams().get("pic").toCharArray();
                    byte[] pic = new byte[pics.length];
                    for (int i2 = 0; i2 < pics.length; i2++) {
                        pic[i2] = (byte) pics[i2];
                    }
                    bos.write(pic);
                    bos.write("---------------------------7da2137580612\r\n".getBytes("utf-8"));
                }
            } catch (IOException e) {
            }
        }
        bos.write("-----------------------------7da2137580612--\r\n".getBytes("utf-8"));
        post.setEntity(new ByteArrayEntity(bos.toByteArray()));
        bos.close();
        HttpResponse response = client.execute(httpPost);
        if (response.getStatusLine().getStatusCode() != 200) {
            InputStream result2 = readHttpResponse(response);
            return null;
        }
        result = readHttpResponse(response);
        return result;
    }

    public static String decode(String s) {
        if (s == null) {
            return "";
        }
        try {
            return URLDecoder.decode(s, MqttUtils.STRING_ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static InputStream readHttpResponse(HttpResponse response) {
        try {
            InputStream inputStream = response.getEntity().getContent();
            Header header = response.getFirstHeader("Content-Encoding");
            if (header != null && header.getValue().toLowerCase().indexOf("gzip") > -1) {
                inputStream = new GZIPInputStream(inputStream);
            }
            return inputStream;
        } catch (IOException | IllegalStateException e) {
            return null;
        }
    }

    public static class UTF8PostMethod extends PostMethod {
        public UTF8PostMethod(String url) {
            super(url);
        }

        public String getRequestCharSet() {
            return MqttUtils.STRING_ENCODING;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Void... params) {
        try {
            return runReq();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object result) {
        if (this.mCallBack != null) {
            this.mCallBack.onResult(result);
        }
        HttpService.getInstance().onReqFinish(this);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        if (this.mCallBack != null) {
            this.mCallBack.onResult(null);
        }
        HttpService.getInstance().onReqFinish(this);
    }
}
