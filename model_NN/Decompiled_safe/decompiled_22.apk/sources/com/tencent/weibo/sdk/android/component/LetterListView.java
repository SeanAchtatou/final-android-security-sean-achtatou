package com.tencent.weibo.sdk.android.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.List;

public class LetterListView extends View {
    List<String> b;
    int choose = -1;
    OnTouchingLetterChangedListener onTouchingLetterChangedListener;
    Paint paint = new Paint();
    boolean showBkg = false;

    public interface OnTouchingLetterChangedListener {
        void onTouchingLetterChanged(int i);
    }

    public LetterListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public LetterListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LetterListView(Context context, List<String> b2) {
        super(context);
        this.b = b2;
    }

    public void setB(List<String> b2) {
        this.b = b2;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.showBkg) {
            canvas.drawColor(Color.parseColor("#00000000"));
        }
        int height = getHeight();
        int width = getWidth() - 30;
        if (this.b.size() > 0) {
            int singleHeight = height / this.b.size();
            for (int i = 0; i < this.b.size(); i++) {
                this.paint.setColor(Color.parseColor("#2796c4"));
                this.paint.setTextSize(17.0f);
                this.paint.setTypeface(Typeface.DEFAULT_BOLD);
                this.paint.setAntiAlias(true);
                if (i == this.choose) {
                    this.paint.setColor(-7829368);
                    this.paint.setFakeBoldText(true);
                }
                canvas.drawText(this.b.get(i).toUpperCase(), ((float) (width / 2)) - (this.paint.measureText(this.b.get(i)) / 2.0f), (float) ((singleHeight * i) + singleHeight), this.paint);
                this.paint.reset();
            }
        }
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        int action = event.getAction();
        float y = event.getY();
        int oldChoose = this.choose;
        OnTouchingLetterChangedListener listener = this.onTouchingLetterChangedListener;
        int c = (int) ((y / ((float) getHeight())) * ((float) this.b.size()));
        switch (action) {
            case 0:
                this.showBkg = true;
                if (oldChoose != c && listener != null && c >= 0 && c < this.b.size()) {
                    listener.onTouchingLetterChanged(c);
                    this.choose = c;
                    invalidate();
                    break;
                }
            case 1:
                this.showBkg = false;
                this.choose = -1;
                invalidate();
                break;
            case 2:
                if (oldChoose != c && listener != null && c >= 0 && c < this.b.size()) {
                    listener.onTouchingLetterChanged(c);
                    this.choose = c;
                    invalidate();
                    break;
                }
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    public void setOnTouchingLetterChangedListener(OnTouchingLetterChangedListener onTouchingLetterChangedListener2) {
        this.onTouchingLetterChangedListener = onTouchingLetterChangedListener2;
    }
}
