package com.tencent.weibo.sdk.android.api;

import android.content.Context;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.tencent.weibo.sdk.android.network.ReqParam;
import sina_weibo.Constants;

public class PublishWeiBoAPI extends BaseAPI {
    public static final String MUTUAL_LIST_URL = "https://open.t.qq.com/api/friends/mutual_list";
    public static final String RECENT_USED_URL = "https://open.t.qq.com/api/ht/recent_used";

    public PublishWeiBoAPI(AccountModel account) {
        super(account);
    }

    public void mutual_list(Context context, HttpCallback callback, Class<? extends BaseVO> responseType, int fopenid, int startindex, int install, int reqnum) {
        ReqParam param = new ReqParam();
        param.addParam(Constants.TX_API_FORMAT, "json");
        param.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        param.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        param.addParam("scope", "all");
        param.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        param.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        if (fopenid != 0) {
            param.addParam("fopenid", Integer.valueOf(fopenid));
        }
        param.addParam("startindex", Integer.valueOf(startindex));
        param.addParam("install", Integer.valueOf(install));
        param.addParam("reqnum", Integer.valueOf(reqnum));
        param.addParam(Constants.SINA_NAME, Util.getSharePersistent(context, "NAME"));
        startRequest(context, MUTUAL_LIST_URL, param, callback, responseType, "GET", 4);
    }

    public void recent_used(Context context, HttpCallback callback, Class<? extends BaseVO> responseType, int reqnum, int page, int sottype) {
        ReqParam param = new ReqParam();
        param.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        param.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        param.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        param.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        param.addParam("scope", "all");
        param.addParam(Constants.TX_API_FORMAT, "json");
        param.addParam("reqnum", Integer.valueOf(reqnum));
        param.addParam("page", Integer.valueOf(page));
        param.addParam("sorttype", Integer.valueOf(sottype));
        startRequest(context, RECENT_USED_URL, param, callback, responseType, "GET", 4);
    }
}
