package com.tencent.weibo.sdk.android.component;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.tencent.mm.sdk.conversation.RConversation;
import com.tencent.weibo.sdk.android.api.PublishWeiBoAPI;
import com.tencent.weibo.sdk.android.api.adapter.ConversationAdapter;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConversationActivity extends Activity implements HttpCallback {
    /* access modifiers changed from: private */
    public ConversationAdapter adapter;
    private ProgressDialog dialog;
    private EditText editText;
    /* access modifiers changed from: private */
    public List<String> list;
    private ListView listView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((LinearLayout) initview());
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Toast.makeText(ConversationActivity.this, new StringBuilder(String.valueOf(arg2)).toString(), 100).show();
            }
        });
        if (this.dialog == null) {
            this.dialog = new ProgressDialog(this);
            this.dialog.setMessage("请稍后...");
            this.dialog.setCancelable(false);
        }
        this.dialog.show();
        new PublishWeiBoAPI(new AccountModel(Util.getSharePersistent(getApplicationContext(), "ACCESS_TOKEN"))).recent_used(this, this, null, 15, 1, 0);
    }

    private View initview() {
        LinearLayout viewroot = new LinearLayout(this);
        LinearLayout.LayoutParams params_viewroot = new LinearLayout.LayoutParams(-1, -1);
        viewroot.setLayoutParams(params_viewroot);
        viewroot.setOrientation(1);
        LinearLayout layout_title = new LinearLayout(this);
        LinearLayout.LayoutParams params_title = new LinearLayout.LayoutParams(-1, -2);
        layout_title.setLayoutParams(params_title);
        layout_title.setOrientation(0);
        layout_title.setBackgroundDrawable(BackGroudSeletor.getdrawble("up_bg2x", this));
        layout_title.setPadding(20, 0, 20, 0);
        layout_title.setGravity(16);
        LinearLayout layout_editext = new LinearLayout(this);
        layout_editext.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        layout_editext.setPadding(0, 0, 12, 0);
        this.editText = new EditText(this);
        this.editText.setSingleLine(true);
        this.editText.setLines(1);
        this.editText.setTextSize(18.0f);
        this.editText.setHint("搜索话题");
        this.editText.setPadding(20, 0, 10, 0);
        this.editText.setBackgroundDrawable(BackGroudSeletor.getdrawble("huati_input2x", this));
        this.editText.setCompoundDrawablesWithIntrinsicBounds(BackGroudSeletor.getdrawble("huati_icon_hover2x", this), (Drawable) null, (Drawable) null, (Drawable) null);
        this.editText.setLayoutParams(params_viewroot);
        Button button_esc = new Button(this);
        button_esc.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"sent_btn_22x", "sent_btn_hover"}, this));
        button_esc.setTextColor(-1);
        button_esc.setText("取消");
        layout_editext.addView(this.editText);
        layout_title.addView(layout_editext);
        layout_title.addView(button_esc);
        viewroot.addView(layout_title);
        LinearLayout layout_list = new LinearLayout(this);
        layout_list.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        this.listView = new ListView(this);
        this.listView.setDivider(BackGroudSeletor.getdrawble("table_lie_", this));
        this.listView.setLayoutParams(params_viewroot);
        this.editText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                List<String> lists = new ArrayList<>();
                for (int i = 0; i < ConversationActivity.this.list.size(); i++) {
                    if (((String) ConversationActivity.this.list.get(i)).contains(s.toString())) {
                        lists.add((String) ConversationActivity.this.list.get(i));
                    }
                }
                ConversationActivity.this.adapter.setCvlist(lists);
                ConversationActivity.this.adapter.notifyDataSetChanged();
                ConversationActivity.this.click(lists);
            }
        });
        LinearLayout layout_foot = new LinearLayout(this);
        Button button = new Button(this);
        layout_foot.setGravity(17);
        button.setLayoutParams(params_title);
        button.setText("abc");
        button.setTextColor(-16777216);
        button_esc.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ConversationActivity.this.finish();
            }
        });
        layout_foot.addView(button);
        layout_list.addView(this.listView);
        viewroot.addView(layout_list);
        return viewroot;
    }

    private List<String> initData(JSONObject jsonObject) {
        List<String> list2 = new ArrayList<>();
        try {
            JSONArray array = jsonObject.getJSONObject("data").getJSONArray("info");
            for (int i = 0; i < array.length(); i++) {
                list2.add("#" + array.getJSONObject(i).getString("text") + "#");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list2;
    }

    /* access modifiers changed from: private */
    public void click(final List<String> li) {
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent();
                intent.setClass(ConversationActivity.this, PublishActivity.class);
                intent.putExtra(RConversation.OLD_TABLE, (String) li.get(arg2));
                ConversationActivity.this.setResult(-1, intent);
                ConversationActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
    }

    public void onResult(Object object) {
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
        if (object != null && ((ModelResult) object).isSuccess()) {
            Log.d("发送成功", ((ModelResult) object).getObj().toString());
            this.list = initData((JSONObject) ((ModelResult) object).getObj());
            this.adapter = new ConversationAdapter(this, this.list);
            this.listView.setAdapter((ListAdapter) this.adapter);
            click(this.list);
        }
    }
}
