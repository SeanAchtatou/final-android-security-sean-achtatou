package com.tencent.weibo.sdk.android.api;

import android.content.Context;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.tencent.weibo.sdk.android.network.ReqParam;
import sina_weibo.Constants;

public class UserAPI extends BaseAPI {
    private static final String SERVER_URL_USERINFO = "https://open.t.qq.com/api/user/info";
    private static final String SERVER_URL_USERINFOS = "https://open.t.qq.com/api/user/infos";
    private static final String SERVER_URL_USEROTHERINFO = "https://open.t.qq.com/api/user/other_info";

    public UserAPI(AccountModel account) {
        super(account);
    }

    public void getUserInfo(Context context, String format, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        startRequest(context, SERVER_URL_USERINFO, mParam, mCallBack, mTargetClass, "GET", resultType);
    }

    public void getUserOtherInfo(Context context, String format, String name, String fopenid, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        if (name != null && !"".equals(name)) {
            mParam.addParam(Constants.SINA_NAME, name);
        }
        if (fopenid != null && !"".equals(fopenid)) {
            mParam.addParam("fopenid", fopenid);
        }
        startRequest(context, SERVER_URL_USEROTHERINFO, mParam, mCallBack, mTargetClass, "GET", resultType);
    }

    public void getUserInfos(Context context, String format, String names, String fopenids, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        if (names != null && !"".equals(names)) {
            mParam.addParam("names", names);
        }
        if (fopenids != null && !"".equals(fopenids)) {
            mParam.addParam("fopenids", fopenids);
        }
        startRequest(context, SERVER_URL_USERINFOS, mParam, mCallBack, mTargetClass, "GET", resultType);
    }
}
