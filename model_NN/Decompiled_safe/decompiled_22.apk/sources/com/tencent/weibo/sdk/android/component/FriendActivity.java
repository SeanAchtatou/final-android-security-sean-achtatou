package com.tencent.weibo.sdk.android.component;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.weibo.sdk.android.api.PublishWeiBoAPI;
import com.tencent.weibo.sdk.android.api.adapter.FriendAdapter;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import com.tencent.weibo.sdk.android.api.util.FirendCompare;
import com.tencent.weibo.sdk.android.api.util.HypyUtil;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.component.LetterListView;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.Firend;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;

public class FriendActivity extends Activity implements LetterListView.OnTouchingLetterChangedListener, HttpCallback {
    private FriendAdapter adapter;
    private Map<String, List<Firend>> child = new HashMap();
    private ProgressDialog dialog;
    private EditText editText;
    private List<String> group = new ArrayList();
    private List<String> groups = new ArrayList();
    private LetterListView letterListView;
    private ExpandableListView listView;
    private Map<String, List<Firend>> newchild = new HashMap();
    private List<String> newgourp = new ArrayList();
    /* access modifiers changed from: private */
    public int[] nums;
    /* access modifiers changed from: private */
    public TextView textView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getdate();
        setContentView((LinearLayout) initview());
    }

    private View initview() {
        LinearLayout linearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams params_viewroot = new LinearLayout.LayoutParams(-1, -1);
        linearLayout.setLayoutParams(params_viewroot);
        linearLayout.setOrientation(1);
        RelativeLayout layout_title = new RelativeLayout(this);
        layout_title.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        layout_title.setGravity(0);
        layout_title.setBackgroundDrawable(BackGroudSeletor.getdrawble("up_bg2x", this));
        RelativeLayout.LayoutParams params_button_back = new RelativeLayout.LayoutParams(-2, -2);
        params_button_back.addRule(15);
        params_button_back.addRule(9, -1);
        params_button_back.addRule(15, -1);
        params_button_back.topMargin = 10;
        params_button_back.leftMargin = 10;
        params_button_back.bottomMargin = 10;
        Button button_back = new Button(this);
        button_back.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"return_btn2x", "return_btn_hover"}, this));
        button_back.setText("  返回");
        button_back.setLayoutParams(params_button_back);
        button_back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FriendActivity.this.finish();
            }
        });
        TextView textView2 = new TextView(this);
        textView2.setText("好友列表");
        textView2.setTextColor(-1);
        textView2.setTextSize(24.0f);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13, -1);
        textView2.setLayoutParams(layoutParams);
        layout_title.addView(textView2);
        layout_title.addView(button_back);
        linearLayout.addView(layout_title);
        LinearLayout layout_find = new LinearLayout(this);
        layout_find.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        layout_find.setOrientation(0);
        layout_find.setGravity(17);
        this.editText = new EditText(this);
        this.editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        this.editText.setPadding(20, 0, 10, 0);
        this.editText.setBackgroundDrawable(BackGroudSeletor.getdrawble("searchbg_", this));
        this.editText.setCompoundDrawablesWithIntrinsicBounds(BackGroudSeletor.getdrawble("search_", this), (Drawable) null, (Drawable) null, (Drawable) null);
        this.editText.setHint("搜索");
        this.editText.setTextSize(18.0f);
        this.editText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                FriendActivity.this.search(s.toString());
            }
        });
        layout_find.addView(this.editText);
        linearLayout.addView(layout_find);
        this.listView = new ExpandableListView(this);
        new FrameLayout.LayoutParams(-1, -1);
        this.listView.setLayoutParams(params_viewroot);
        this.listView.setGroupIndicator(null);
        LinearLayout layout_scroll = new LinearLayout(this);
        layout_scroll.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.textView = new TextView(this);
        layout_scroll.setPadding(30, 0, 0, 0);
        layout_scroll.setGravity(16);
        this.textView.setTextSize(18.0f);
        this.textView.setTextColor(-1);
        this.textView.setText("常用联系人");
        layout_scroll.addView(this.textView);
        layout_scroll.setBackgroundColor(Color.parseColor("#b0bac3"));
        this.letterListView = new LetterListView(this, this.group);
        this.letterListView.setOnTouchingLetterChangedListener(this);
        RelativeLayout.LayoutParams params_letter = new RelativeLayout.LayoutParams(50, -1);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        params_letter.addRule(11);
        this.letterListView.setLayoutParams(params_letter);
        relativeLayout.setLayoutParams(layoutParams2);
        relativeLayout.addView(this.listView);
        relativeLayout.addView(layout_scroll);
        relativeLayout.addView(this.letterListView);
        linearLayout.addView(relativeLayout);
        return linearLayout;
    }

    public void search(String text) {
        this.newgourp.clear();
        this.newchild.clear();
        for (int i = 0; i < this.group.size(); i++) {
            for (int j = 0; j < this.child.get(this.group.get(i)).size(); j++) {
                if (((Firend) this.child.get(this.group.get(i)).get(j)).getNick().contains(text)) {
                    if (this.newchild.get(this.group.get(i)) == null) {
                        List<Firend> list = new ArrayList<>();
                        list.add((Firend) this.child.get(this.group.get(i)).get(j));
                        this.newchild.put(this.group.get(i), list);
                        this.newgourp.add(this.group.get(i));
                    } else {
                        this.newchild.get(this.group.get(i)).add((Firend) this.child.get(this.group.get(i)).get(j));
                    }
                }
            }
        }
        Log.d("size", String.valueOf(this.newgourp.size()) + "---" + this.newchild.size());
        this.nums = new int[this.newgourp.size()];
        for (int i2 = 0; i2 < this.nums.length; i2++) {
            this.nums[i2] = totle(i2);
        }
        this.letterListView.setB(this.newgourp);
        this.adapter.setChild(this.newchild);
        this.adapter.setGroup(this.newgourp);
        this.adapter.notifyDataSetChanged();
        ex(this.newgourp, this.newchild);
    }

    private void getdate() {
        if (this.dialog == null) {
            this.dialog = new ProgressDialog(this);
            this.dialog.setMessage("请稍后...");
            this.dialog.show();
        }
        new PublishWeiBoAPI(new AccountModel(Util.getSharePersistent(getApplicationContext(), "ACCESS_TOKEN"))).mutual_list(this, this, null, 0, 0, 0, 10);
    }

    private void ex(final List<String> groupt, final Map<String, List<Firend>> childs) {
        for (int i = 0; i < groupt.size(); i++) {
            this.listView.expandGroup(i);
        }
        this.listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            public void onGroupExpand(int groupPosition) {
            }
        });
        this.listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        this.listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Intent intent = new Intent();
                intent.setClass(FriendActivity.this, PublishActivity.class);
                intent.putExtra("firend", ((Firend) ((List) childs.get(groupt.get(groupPosition))).get(childPosition)).getNick());
                FriendActivity.this.setResult(-1, intent);
                FriendActivity.this.finish();
                return true;
            }
        });
        this.listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d("first", new StringBuilder(String.valueOf(firstVisibleItem)).toString());
                int i = 0;
                while (i < groupt.size()) {
                    if (i == 0 && firstVisibleItem >= 0 && firstVisibleItem < FriendActivity.this.nums[i]) {
                        FriendActivity.this.textView.setText(((String) groupt.get(i)).toUpperCase());
                        return;
                    } else if (firstVisibleItem >= FriendActivity.this.nums[i] || firstVisibleItem < FriendActivity.this.nums[i - 1]) {
                        i++;
                    } else {
                        FriendActivity.this.textView.setText(((String) groupt.get(i)).toUpperCase());
                        return;
                    }
                }
            }
        });
    }

    public void onTouchingLetterChanged(int c) {
        this.listView.setSelectedGroup(c);
    }

    public void onResult(Object object) {
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
        if (object != null && ((ModelResult) object).isSuccess()) {
            getJsonData((JSONObject) ((ModelResult) object).getObj());
            this.nums = new int[this.group.size()];
            for (int i = 0; i < this.nums.length; i++) {
                this.nums[i] = totle(i);
            }
            this.letterListView.setB(this.group);
            this.adapter = new FriendAdapter(this, this.group, this.child);
            this.listView.setAdapter(this.adapter);
            ex(this.group, this.child);
            Log.d("发送成功", object.toString());
        }
    }

    private void getJsonData(JSONObject json) {
        List<Firend> firends = new ArrayList<>();
        try {
            JSONArray array = json.getJSONObject("data").getJSONArray("info");
            for (int i = 0; i < array.length(); i++) {
                Firend firend = new Firend();
                firend.setNick(((JSONObject) array.get(i)).getString("nick"));
                firend.setName(((JSONObject) array.get(i)).getString(Constants.SINA_NAME));
                firend.setHeadurl(String.valueOf(((JSONObject) array.get(i)).getString("headurl").replaceAll("\\/", CookieSpec.PATH_DELIM)) + "/180");
                firends.add(firend);
            }
            Collections.sort(firends, new FirendCompare());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i2 = 0; i2 < firends.size(); i2++) {
            if (this.child.get(HypyUtil.cn2py(((Firend) firends.get(i2)).getNick()).substring(0, 1).toUpperCase()) != null) {
                this.child.get(HypyUtil.cn2py(((Firend) firends.get(i2)).getNick()).substring(0, 1).toUpperCase()).add((Firend) firends.get(i2));
            } else {
                Log.d("group", HypyUtil.cn2py(((Firend) firends.get(i2)).getNick()).substring(0, 1));
                this.group.add(HypyUtil.cn2py(((Firend) firends.get(i2)).getNick()).substring(0, 1).toUpperCase());
                List<Firend> list = new ArrayList<>();
                list.add((Firend) firends.get(i2));
                this.child.put(HypyUtil.cn2py(((Firend) firends.get(i2)).getNick()).substring(0, 1).toUpperCase(), list);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
    }

    private int totle(int position) {
        if (position == 0) {
            return this.child.get(this.group.get(position)).size() + 1;
        }
        return this.child.get(this.group.get(position)).size() + 1 + totle(position - 1);
    }
}
