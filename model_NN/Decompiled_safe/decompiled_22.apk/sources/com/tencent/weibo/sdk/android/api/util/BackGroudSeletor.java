package com.tencent.weibo.sdk.android.api.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class BackGroudSeletor {
    static int[] EMPTY_STATE_SET = new int[0];
    static int[] ENABLED_STATE_SET = {16842910};
    static int[] PRESSED_ENABLED_STATE_SET = {16842910, 16842919};
    private static String pix = "";

    private BackGroudSeletor() {
    }

    public static StateListDrawable createBgByImageIds(String[] imagename, Context context) {
        StateListDrawable bg = new StateListDrawable();
        Drawable normal = getdrawble(imagename[0], context);
        bg.addState(PRESSED_ENABLED_STATE_SET, getdrawble(imagename[1], context));
        bg.addState(ENABLED_STATE_SET, normal);
        bg.addState(EMPTY_STATE_SET, normal);
        return bg;
    }

    public static Drawable getdrawble(String imagename, Context context) {
        Bitmap bitmap = null;
        try {
            String imagePath = String.valueOf(imagename) + pix + ".png";
            if (!new File(String.valueOf(imagename) + pix + ".png").isFile()) {
                imagePath = String.valueOf(imagename) + "480x800" + ".png";
            }
            return new BitmapDrawable(BitmapFactory.decodeStream(context.getAssets().open(imagePath)));
        } catch (IOException e) {
            if (bitmap != null) {
                bitmap.recycle();
            }
            e.printStackTrace();
            return null;
        }
    }

    public static InputStream zipPic(InputStream is) {
        return null;
    }

    public static String getPix() {
        return pix;
    }

    public static void setPix(String pix2) {
        pix = pix2;
    }
}
