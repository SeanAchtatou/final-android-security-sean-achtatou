package com.tencent.weibo.sdk.android.component.sso;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.weibo.sdk.android.component.sso.tools.Cryptor;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class AuthReceiver extends BroadcastReceiver {
    static final String ACTION = "com.tencent.sso.AUTH";

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION) && intent.getStringExtra("com.tencent.sso.PACKAGE_NAME").equals(context.getPackageName())) {
            if (intent.getBooleanExtra("com.tencent.sso.AUTH_RESULT", false)) {
                String name = intent.getStringExtra("com.tencent.sso.WEIBO_NICK");
                WeiboToken token = revert(new Cryptor().decrypt(intent.getByteArrayExtra("com.tencent.sso.ACCESS_TOKEN"), "&-*)Wb5_U,[^!9'+".getBytes(), 10));
                if (AuthHelper.listener != null) {
                    AuthHelper.listener.onAuthPassed(name, token);
                    return;
                }
                return;
            }
            int erroresult = intent.getIntExtra("com.tencent.sso.RESULT", 1);
            String name2 = intent.getStringExtra("com.tencent.sso.WEIBO_NICK");
            if (AuthHelper.listener != null) {
                AuthHelper.listener.onAuthFail(erroresult, name2);
            }
        }
    }

    private WeiboToken revert(byte[] data) {
        WeiboToken token = new WeiboToken();
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        DataInputStream dis = new DataInputStream(bais);
        try {
            token.accessToken = dis.readUTF();
            token.expiresIn = dis.readLong();
            token.refreshToken = dis.readUTF();
            token.openID = dis.readUTF();
            token.omasToken = dis.readUTF();
            token.omasKey = dis.readUTF();
            if (bais != null) {
                try {
                    bais.close();
                } catch (IOException e) {
                }
            }
            if (dis == null) {
                return token;
            }
            try {
                dis.close();
                return token;
            } catch (IOException e2) {
                return token;
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            if (bais != null) {
                try {
                    bais.close();
                } catch (IOException e4) {
                }
            }
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e5) {
                }
            }
            return null;
        } catch (Throwable th) {
            if (bais != null) {
                try {
                    bais.close();
                } catch (IOException e6) {
                }
            }
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e7) {
                }
            }
            throw th;
        }
    }
}
