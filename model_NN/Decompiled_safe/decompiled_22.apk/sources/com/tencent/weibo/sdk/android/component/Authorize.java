package com.tencent.weibo.sdk.android.component;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import com.tencent.weibo.sdk.android.api.util.Util;

public class Authorize extends Activity {
    public static final int ALERT_DOWNLOAD = 0;
    public static final int ALERT_FAV = 1;
    public static final int ALERT_NETWORK = 4;
    public static final int PROGRESS_H = 3;
    public static int WEBVIEWSTATE_1 = 0;
    Dialog _dialog;
    String _fileName;
    String _url;
    private String clientId = null;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    Handler handle = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 100:
                    Authorize.this.showDialog(4);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean isShow = false;
    private LinearLayout layout = null;
    String path;
    private String redirectUri = null;
    WebView webView;
    int webview_state = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Util.isNetworkAvailable(this)) {
            showDialog(4);
            return;
        }
        DisplayMetrics displaysMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaysMetrics);
        BackGroudSeletor.setPix(String.valueOf(displaysMetrics.widthPixels) + "x" + displaysMetrics.heightPixels);
        try {
            this.clientId = Util.getConfig().getProperty("APP_KEY");
            this.redirectUri = Util.getConfig().getProperty("REDIRECT_URI");
            if (this.clientId == null || "".equals(this.clientId) || this.redirectUri == null || "".equals(this.redirectUri)) {
                Toast.makeText(this, "请在配置文件中填写相应的信息", 0).show();
            }
            Log.d("redirectUri", this.redirectUri);
            getWindow().setFlags(1024, 1024);
            requestWindowFeature(1);
            this.path = "https://open.t.qq.com/cgi-bin/oauth2/authorize?client_id=" + this.clientId + "&response_type=token&redirect_uri=" + this.redirectUri + "&state=" + ((((int) Math.random()) * 1000) + 111);
            initLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initLayout() {
        RelativeLayout.LayoutParams fillParams = new RelativeLayout.LayoutParams(-1, -1);
        RelativeLayout.LayoutParams fillWrapParams = new RelativeLayout.LayoutParams(-1, -2);
        RelativeLayout.LayoutParams wrapParams = new RelativeLayout.LayoutParams(-2, -2);
        this.dialog = new ProgressDialog(this);
        this.dialog.setProgressStyle(0);
        this.dialog.requestWindowFeature(1);
        this.dialog.setMessage("请稍后...");
        this.dialog.setIndeterminate(false);
        this.dialog.setCancelable(false);
        this.dialog.show();
        this.layout = new LinearLayout(this);
        this.layout.setLayoutParams(fillParams);
        this.layout.setOrientation(1);
        RelativeLayout cannelLayout = new RelativeLayout(this);
        cannelLayout.setLayoutParams(fillWrapParams);
        cannelLayout.setBackgroundDrawable(BackGroudSeletor.getdrawble("up_bg2x", getApplication()));
        cannelLayout.setGravity(0);
        Button returnBtn = new Button(this);
        returnBtn.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"quxiao_btn2x", "quxiao_btn_hover"}, getApplication()));
        returnBtn.setText("取消");
        wrapParams.addRule(9, -1);
        wrapParams.addRule(15, -1);
        wrapParams.leftMargin = 10;
        wrapParams.topMargin = 10;
        wrapParams.bottomMargin = 10;
        returnBtn.setLayoutParams(wrapParams);
        returnBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Authorize.this.finish();
            }
        });
        cannelLayout.addView(returnBtn);
        TextView title = new TextView(this);
        title.setText("授权");
        title.setTextColor(-1);
        title.setTextSize(24.0f);
        RelativeLayout.LayoutParams titleParams = new RelativeLayout.LayoutParams(-2, -2);
        titleParams.addRule(13, -1);
        title.setLayoutParams(titleParams);
        cannelLayout.addView(title);
        this.layout.addView(cannelLayout);
        this.webView = new WebView(this);
        this.webView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        WebSettings webSettings = this.webView.getSettings();
        this.webView.setVerticalScrollBarEnabled(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(false);
        this.webView.loadUrl(this.path);
        this.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                Log.d("newProgress", String.valueOf(newProgress) + "..");
            }
        });
        this.webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                Log.d("backurl", url);
                if (url.indexOf("access_token") != -1 && !Authorize.this.isShow) {
                    Authorize.this.jumpResultParser(url);
                }
                if (Authorize.this.dialog != null && Authorize.this.dialog.isShowing()) {
                    Authorize.this.dialog.cancel();
                }
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.indexOf("access_token") == -1 || Authorize.this.isShow) {
                    return false;
                }
                Authorize.this.jumpResultParser(url);
                return false;
            }
        });
        this.layout.addView(this.webView);
        setContentView(this.layout);
    }

    public void jumpResultParser(String result) {
        String[] params = result.split("#")[1].split("&");
        String accessToken = params[0].split("=")[1];
        String expiresIn = params[1].split("=")[1];
        String openid = params[2].split("=")[1];
        String openkey = params[3].split("=")[1];
        String refreshToken = params[4].split("=")[1];
        String str = params[5].split("=")[1];
        String name = params[6].split("=")[1];
        String nick = params[7].split("=")[1];
        Context context = getApplicationContext();
        if (accessToken != null && !"".equals(accessToken)) {
            Util.saveSharePersistent(context, "ACCESS_TOKEN", accessToken);
            Util.saveSharePersistent(context, "EXPIRES_IN", expiresIn);
            Util.saveSharePersistent(context, "OPEN_ID", openid);
            Util.saveSharePersistent(context, "OPEN_KEY", openkey);
            Util.saveSharePersistent(context, "REFRESH_TOKEN", refreshToken);
            Util.saveSharePersistent(context, "NAME", name);
            Util.saveSharePersistent(context, "NICK", nick);
            Util.saveSharePersistent(context, "CLIENT_ID", this.clientId);
            Util.saveSharePersistent(context, "AUTHORIZETIME", String.valueOf(System.currentTimeMillis() / 1000));
            Toast.makeText(this, "授权成功", 0).show();
            finish();
            this.isShow = true;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 3:
                this._dialog = new ProgressDialog(this);
                ((ProgressDialog) this._dialog).setMessage("加载中...");
                break;
            case 4:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setTitle("网络连接异常，是否重新连接？");
                builder2.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (Util.isNetworkAvailable(Authorize.this)) {
                            Authorize.this.webView.loadUrl(Authorize.this.path);
                            return;
                        }
                        Message msg = Message.obtain();
                        msg.what = 100;
                        Authorize.this.handle.sendMessage(msg);
                    }
                });
                builder2.setNegativeButton("否", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Authorize.this.finish();
                    }
                });
                this._dialog = builder2.create();
                break;
        }
        return this._dialog;
    }
}
