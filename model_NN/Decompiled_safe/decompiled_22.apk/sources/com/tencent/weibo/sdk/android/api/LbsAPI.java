package com.tencent.weibo.sdk.android.api;

import android.content.Context;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.BaseVO;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.tencent.weibo.sdk.android.network.ReqParam;
import sina_weibo.Constants;

public class LbsAPI extends BaseAPI {
    private static final String SERVER_URL_GetAROUNDNEW = "https://open.t.qq.com/api/lbs/get_around_new";
    private static final String SERVER_URL_GetAROUNDPEOPLE = "https://open.t.qq.com/api/lbs/get_around_people";

    public LbsAPI(AccountModel account) {
        super(account);
    }

    public void getAroundPeople(Context context, String format, double longitude, double latitude, String pageinfo, int pagesize, int gender, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam(Constants.TX_API_LONGITUDE, Double.valueOf(longitude));
        mParam.addParam(Constants.TX_API_LATITUDE, Double.valueOf(latitude));
        mParam.addParam("pageinfo", pageinfo);
        mParam.addParam("pagesize", Integer.valueOf(pagesize));
        mParam.addParam("gender", Integer.valueOf(gender));
        startRequest(context, SERVER_URL_GetAROUNDPEOPLE, mParam, mCallBack, mTargetClass, "POST", resultType);
    }

    public void getAroundNew(Context context, String format, double longitude, double latitude, String pageinfo, int pagesize, HttpCallback mCallBack, Class<? extends BaseVO> mTargetClass, int resultType) {
        ReqParam mParam = new ReqParam();
        mParam.addParam("scope", "all");
        mParam.addParam(Constants.TX_API_CLIENT_IP, Util.getLocalIPAddress(context));
        mParam.addParam(Constants.TX_API_OAUTH_VERSION, "2.a");
        mParam.addParam("oauth_consumer_key", Util.getSharePersistent(context, "CLIENT_ID"));
        mParam.addParam("openid", Util.getSharePersistent(context, "OPEN_ID"));
        mParam.addParam(Constants.TX_API_FORMAT, format);
        mParam.addParam(Constants.TX_API_LONGITUDE, Double.valueOf(longitude));
        mParam.addParam(Constants.TX_API_LATITUDE, Double.valueOf(latitude));
        mParam.addParam("pageinfo", pageinfo);
        mParam.addParam("pagesize", Integer.valueOf(pagesize));
        startRequest(context, SERVER_URL_GetAROUNDNEW, mParam, mCallBack, mTargetClass, "POST", resultType);
    }
}
