package com.tencent.weibo.sdk.android.component;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.tauth.Constants;
import com.tencent.weibo.sdk.android.api.WeiboAPI;
import com.tencent.weibo.sdk.android.api.adapter.GalleryAdapter;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.ImageInfo;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import java.util.ArrayList;
import org.apache.commons.httpclient.HttpStatus;
import org.json.JSONObject;

public class ReAddActivity extends Activity {
    private String accessToken;
    private WeiboAPI api;
    /* access modifiers changed from: private */
    public EditText content = null;
    private String contentStr = "";
    /* access modifiers changed from: private */
    public Gallery gallery;
    private RelativeLayout galleryLayout = null;
    /* access modifiers changed from: private */
    public ArrayList<ImageInfo> imageList = new ArrayList<>();
    private LinearLayout layout = null;
    /* access modifiers changed from: private */
    public PopupWindow loadingWindow = null;
    private HttpCallback mCallBack = new HttpCallback() {
        public void onResult(Object object) {
            ModelResult result = (ModelResult) object;
            if (result.isExpires()) {
                Toast.makeText(ReAddActivity.this, result.getError_message(), 0).show();
            } else if (result.isSuccess()) {
                Toast.makeText(ReAddActivity.this, "转播成功", 0).show();
                ReAddActivity.this.finish();
            } else {
                Toast.makeText(ReAddActivity.this, result.getError_message(), 0).show();
                ReAddActivity.this.finish();
            }
        }
    };
    private Handler mHandler = null;
    private String musicAuthor = "";
    private String musicPath = "";
    private String musicTitle = "";
    private String picPath = "";
    private ProgressBar progressBar = null;
    /* access modifiers changed from: private */
    public TextView textView_num;
    private HttpCallback videoCallBack = new HttpCallback() {
        public void onResult(Object object) {
            ModelResult result = (ModelResult) object;
            if (result != null) {
                if (!result.isExpires() && result.isSuccess()) {
                    try {
                        JSONObject data = ((JSONObject) result.getObj()).getJSONObject("data");
                        ImageInfo info1 = new ImageInfo();
                        info1.setImagePath(data.getString("minipic"));
                        info1.setImageName(data.getString(Constants.PARAM_TITLE));
                        info1.setPlayPath(data.getString("real"));
                        ReAddActivity.this.imageList.add(info1);
                        ReAddActivity.this.gallery.setAdapter((SpinnerAdapter) new GalleryAdapter(ReAddActivity.this.getApplicationContext(), ReAddActivity.this.loadingWindow, ReAddActivity.this.imageList));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (ReAddActivity.this.loadingWindow != null && ReAddActivity.this.loadingWindow.isShowing()) {
                ReAddActivity.this.loadingWindow.dismiss();
            }
        }
    };
    private String videoPath = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        DisplayMetrics displaysMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaysMetrics);
        BackGroudSeletor.setPix(String.valueOf(displaysMetrics.widthPixels) + "x" + displaysMetrics.heightPixels);
        this.accessToken = Util.getSharePersistent(getApplicationContext(), "ACCESS_TOKEN");
        if (this.accessToken == null || "".equals(this.accessToken)) {
            Toast.makeText(this, "请先授权", 0).show();
            finish();
            return;
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.contentStr = bundle.getString("content");
            this.videoPath = bundle.getString("video_url");
            this.picPath = bundle.getString(sina_weibo.Constants.TX_PIC_URL);
            this.musicPath = bundle.getString("music_url");
            this.musicTitle = bundle.getString("music_title");
            this.musicAuthor = bundle.getString("music_author");
        }
        this.api = new WeiboAPI(new AccountModel(this.accessToken));
        setContentView(initLayout());
    }

    public View initLayout() {
        RelativeLayout.LayoutParams fillParams = new RelativeLayout.LayoutParams(-1, -1);
        RelativeLayout.LayoutParams fillWrapParams = new RelativeLayout.LayoutParams(-1, -2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        this.layout = new LinearLayout(this);
        this.layout.setLayoutParams(fillParams);
        this.layout.setOrientation(1);
        this.layout.setBackgroundDrawable(BackGroudSeletor.getdrawble("readd_bg", getApplication()));
        RelativeLayout cannelLayout = new RelativeLayout(this);
        cannelLayout.setLayoutParams(fillWrapParams);
        cannelLayout.setBackgroundDrawable(BackGroudSeletor.getdrawble("up_bg2x", getApplication()));
        cannelLayout.setGravity(0);
        Button returnBtn = new Button(this);
        returnBtn.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"quxiao_btn2x", "quxiao_btn_hover"}, getApplication()));
        returnBtn.setText("取消");
        layoutParams.addRule(9, -1);
        layoutParams.addRule(15, -1);
        layoutParams.topMargin = 10;
        layoutParams.leftMargin = 10;
        layoutParams.bottomMargin = 10;
        returnBtn.setLayoutParams(layoutParams);
        returnBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReAddActivity.this.finish();
            }
        });
        cannelLayout.addView(returnBtn);
        TextView title = new TextView(this);
        title.setText("转播");
        title.setTextColor(-1);
        title.setTextSize(24.0f);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(13, -1);
        title.setLayoutParams(layoutParams2);
        cannelLayout.addView(title);
        Button reAddBtn = new Button(this);
        reAddBtn.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"sent_btn2x", "sent_btn_hover"}, getApplication()));
        reAddBtn.setText("转播");
        RelativeLayout.LayoutParams wrapParamsRight = new RelativeLayout.LayoutParams(-2, -2);
        wrapParamsRight.addRule(11, -1);
        wrapParamsRight.addRule(15, -1);
        wrapParamsRight.topMargin = 10;
        wrapParamsRight.rightMargin = 10;
        wrapParamsRight.bottomMargin = 10;
        reAddBtn.setLayoutParams(wrapParamsRight);
        reAddBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReAddActivity.this.reAddWeibo();
            }
        });
        cannelLayout.addView(reAddBtn);
        RelativeLayout reAddLayout = new RelativeLayout(this);
        reAddLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, 240));
        RelativeLayout contentLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams contentParams = new RelativeLayout.LayoutParams(440, -1);
        contentParams.addRule(13);
        contentParams.topMargin = 50;
        contentLayout.setLayoutParams(contentParams);
        contentLayout.setBackgroundDrawable(BackGroudSeletor.getdrawble("input_bg", getApplication()));
        this.textView_num = new TextView(this);
        this.textView_num.setText(this.contentStr == null ? "140" : String.valueOf(140 - this.contentStr.length()));
        this.textView_num.setTextColor(Color.parseColor("#999999"));
        this.textView_num.setGravity(5);
        this.textView_num.setTextSize(18.0f);
        RelativeLayout.LayoutParams params_space = new RelativeLayout.LayoutParams(-2, -2);
        params_space.addRule(12, -1);
        params_space.addRule(11, -1);
        params_space.rightMargin = 10;
        this.textView_num.setLayoutParams(params_space);
        contentLayout.addView(this.textView_num);
        this.content = new EditText(this);
        RelativeLayout.LayoutParams contentParams2 = new RelativeLayout.LayoutParams(-1, -2);
        contentParams2.addRule(14);
        contentParams2.addRule(10);
        this.content.setLayoutParams(contentParams2);
        this.content.setMaxLines(4);
        this.content.setMinLines(4);
        this.content.setScrollbarFadingEnabled(true);
        this.content.setGravity(48);
        this.content.setMovementMethod(ScrollingMovementMethod.getInstance());
        this.content.setText(this.contentStr);
        this.content.setSelection(this.contentStr.length());
        this.content.setBackgroundDrawable(null);
        this.content.addTextChangedListener(new TextWatcher() {
            private int selectionEnd;
            private int selectionStart;
            private CharSequence temp;

            public void afterTextChanged(Editable arg0) {
                this.selectionStart = ReAddActivity.this.content.getSelectionStart();
                this.selectionEnd = ReAddActivity.this.content.getSelectionEnd();
                if (this.temp.length() > 140) {
                    Toast.makeText(ReAddActivity.this, "最多可输入140字符", 0).show();
                    arg0.delete(this.selectionStart - 1, this.selectionEnd);
                    int tempSelection = this.selectionStart;
                    ReAddActivity.this.content.setText(arg0);
                    ReAddActivity.this.content.setSelection(tempSelection);
                    return;
                }
                ReAddActivity.this.textView_num.setText(String.valueOf(140 - arg0.length()));
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                this.temp = s;
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        contentLayout.addView(this.content);
        reAddLayout.addView(contentLayout);
        this.galleryLayout = new RelativeLayout(this);
        this.galleryLayout.setLayoutParams(fillParams);
        this.gallery = new Gallery(this);
        RelativeLayout.LayoutParams galleryParams = new RelativeLayout.LayoutParams((int) HttpStatus.SC_SEE_OTHER, (int) HttpStatus.SC_NON_AUTHORITATIVE_INFORMATION);
        galleryParams.addRule(14, -1);
        galleryParams.addRule(10, -1);
        galleryParams.topMargin = 50;
        this.gallery.setLayoutParams(galleryParams);
        this.gallery.setBackgroundDrawable(BackGroudSeletor.getdrawble("pic_biankuang2x", getApplication()));
        requestForGallery();
        this.galleryLayout.addView(this.gallery);
        this.layout.addView(cannelLayout);
        this.layout.addView(reAddLayout);
        if (this.picPath != null && !"".equals(this.picPath) && this.videoPath != null && !"".equals(this.videoPath)) {
            this.layout.addView(this.galleryLayout);
        }
        return this.layout;
    }

    /* access modifiers changed from: protected */
    public void reAddWeibo() {
        this.contentStr = this.content.getText().toString();
        this.api.reAddWeibo(getApplicationContext(), this.contentStr, this.picPath, this.videoPath, this.musicPath, this.musicTitle, this.musicAuthor, this.mCallBack, null, 4);
    }

    public ArrayList<ImageInfo> requestForGallery() {
        if (this.picPath != null) {
            ImageInfo info2 = new ImageInfo();
            info2.setImagePath(this.picPath);
            this.imageList.add(info2);
        }
        if (this.videoPath != null) {
            new ImageInfo();
            this.api.getVideoInfo(getApplicationContext(), this.videoPath, this.videoCallBack, null, 4);
        }
        return this.imageList;
    }
}
