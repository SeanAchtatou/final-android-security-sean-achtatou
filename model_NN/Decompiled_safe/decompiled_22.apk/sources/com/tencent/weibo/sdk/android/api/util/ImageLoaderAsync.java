package com.tencent.weibo.sdk.android.api.util;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;

public class ImageLoaderAsync {

    public interface callBackImage {
        void callback(Drawable drawable, String str);
    }

    public Drawable loadImage(final String imagePath, final callBackImage callback) {
        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                callback.callback((Drawable) msg.obj, imagePath);
            }
        };
        new Thread() {
            public void run() {
                handler.sendMessage(handler.obtainMessage(0, Util.loadImageFromUrl(imagePath)));
            }
        }.start();
        return null;
    }
}
