package com.tencent.weibo.sdk.android.model;

import java.util.List;

public class ModelResult {
    private String error_message = "success";
    private boolean isExpires = false;
    private boolean isLastPage;
    private String lat;
    private List<BaseVO> list;
    private String lon;
    private Object obj;
    private int p;
    private int ps;
    private boolean success = true;
    private int total;

    public boolean isExpires() {
        return this.isExpires;
    }

    public void setExpires(boolean isExpires2) {
        this.isExpires = isExpires2;
    }

    public boolean isLastPage() {
        return this.isLastPage;
    }

    public void setLastPage(boolean isLastPage2) {
        this.isLastPage = isLastPage2;
    }

    public Object getObj() {
        return this.obj;
    }

    public void setObj(Object obj2) {
        this.obj = obj2;
    }

    public void add(BaseVO vo) {
        this.list.add(vo);
    }

    public BaseVO get() {
        return this.list.get(0);
    }

    public String getLon() {
        return this.lon;
    }

    public void setLon(String lon2) {
        this.lon = lon2;
    }

    public String getLat() {
        return this.lat;
    }

    public void setLat(String lat2) {
        this.lat = lat2;
    }

    public int getTotal() {
        return this.total;
    }

    public void setTotal(int total2) {
        this.total = total2;
    }

    public int getP() {
        return this.p;
    }

    public void setP(int p2) {
        this.p = p2;
    }

    public int getPs() {
        return this.ps;
    }

    public void setPs(int ps2) {
        this.ps = ps2;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success2) {
        this.success = success2;
    }

    public String getError_message() {
        return this.error_message;
    }

    public void setError_message(String error_message2) {
        this.error_message = error_message2;
    }

    public List<BaseVO> getList() {
        return this.list;
    }

    public void setList(List<BaseVO> list2) {
        this.list = list2;
    }
}
