package com.tencent.weibo.sdk.android.component;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.os.MessageQueue;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.weibo.sdk.android.api.FriendAPI;
import com.tencent.weibo.sdk.android.api.LbsAPI;
import com.tencent.weibo.sdk.android.api.TimeLineAPI;
import com.tencent.weibo.sdk.android.api.UserAPI;
import com.tencent.weibo.sdk.android.api.WeiboAPI;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.utils.FinalVariable;

public class GeneralInterfaceActivity extends Activity implements View.OnClickListener {
    private String accessToken;
    private Button addPic;
    private Button addPicUrl;
    private Button addWeibo;
    private Context context = null;
    private Button deviceStatus;
    private Button errorReport;
    private FriendAPI friendAPI;
    private Button friendAdd;
    private Button friendCheck;
    private Button friendFunsList;
    private Button friendGetIntimateFriend;
    private Button friendIdolList;
    private Button friendMutualList;
    private Button homeTimeLine;
    private Button htTimeLine;
    private double latitude = 0.0d;
    private LbsAPI lbsAPI;
    private Button lbsGetAroundNew;
    private Button lbsGetAroundPeople;
    /* access modifiers changed from: private */
    public PopupWindow loadingWindow = null;
    private double longitude = 0.0d;
    private HttpCallback mCallBack;
    private Location mLocation;
    private ProgressBar progressBar = null;
    private String requestFormat = "json";
    /* access modifiers changed from: private */
    public ScrollView scrollView = null;
    private Button tReList;
    private TimeLineAPI timeLineAPI;
    private UserAPI userAPI;
    private Button userInfo;
    private Button userInfos;
    private Button userOtherInfo;
    private Button userTimeLine;
    private WeiboAPI weiboAPI;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.accessToken = Util.getSharePersistent(getApplicationContext(), "ACCESS_TOKEN");
        if (this.accessToken == null || "".equals(this.accessToken)) {
            Toast.makeText(this, "请先授权", 0).show();
            finish();
            return;
        }
        AccountModel account = new AccountModel(this.accessToken);
        this.friendAPI = new FriendAPI(account);
        this.timeLineAPI = new TimeLineAPI(account);
        this.weiboAPI = new WeiboAPI(account);
        this.userAPI = new UserAPI(account);
        this.lbsAPI = new LbsAPI(account);
        this.mCallBack = new HttpCallback() {
            public void onResult(Object object) {
                ModelResult result = (ModelResult) object;
                if (GeneralInterfaceActivity.this.loadingWindow != null && GeneralInterfaceActivity.this.loadingWindow.isShowing()) {
                    GeneralInterfaceActivity.this.loadingWindow.dismiss();
                }
                if (result != null) {
                    Toast.makeText(GeneralInterfaceActivity.this, "成功", 0).show();
                    Intent i = new Intent(GeneralInterfaceActivity.this, GeneralDataShowActivity.class);
                    i.putExtra("data", result.getObj().toString());
                    GeneralInterfaceActivity.this.startActivity(i);
                    return;
                }
                Toast.makeText(GeneralInterfaceActivity.this, "发生异常", 0).show();
            }
        };
        this.progressBar = new ProgressBar(this);
        this.loadingWindow = new PopupWindow(this.progressBar, 100, 100);
        this.context = getApplicationContext();
        this.mLocation = Util.getLocation(this.context);
        if (this.mLocation != null) {
            this.longitude = this.mLocation.getLongitude();
            this.latitude = this.mLocation.getLatitude();
        }
        initInterface();
    }

    public void initInterface() {
        this.scrollView = new ScrollView(this);
        TableLayout table = new TableLayout(this);
        table.setLayoutParams(new TableLayout.LayoutParams(-1, -1));
        TableRow row1 = new TableRow(this);
        this.homeTimeLine = new Button(this);
        this.homeTimeLine.setText("主人页时间线");
        this.homeTimeLine.setId(FinalVariable.update);
        this.homeTimeLine.setOnClickListener(this);
        row1.addView(this.homeTimeLine);
        this.userTimeLine = new Button(this);
        this.userTimeLine.setText("客人页时间线");
        this.userTimeLine.setId(FinalVariable.remove_footer);
        this.userTimeLine.setOnClickListener(this);
        row1.addView(this.userTimeLine);
        table.addView(row1);
        TableRow row2 = new TableRow(this);
        this.addWeibo = new Button(this);
        this.addWeibo.setText("普通发表接口");
        this.addWeibo.setId(FinalVariable.change);
        this.addWeibo.setOnClickListener(this);
        row2.addView(this.addWeibo);
        this.addPic = new Button(this);
        this.addPic.setText("发表带图微博");
        this.addPic.setId(FinalVariable.error);
        this.addPic.setOnClickListener(this);
        row2.addView(this.addPic);
        table.addView(row2);
        TableRow row3 = new TableRow(this);
        this.addPicUrl = new Button(this);
        this.addPicUrl.setText("发表带网络图片微博");
        this.addPicUrl.setId(FinalVariable.deletefoot);
        this.addPicUrl.setOnClickListener(this);
        row3.addView(this.addPicUrl);
        this.htTimeLine = new Button(this);
        this.htTimeLine.setText("话题时间线");
        this.htTimeLine.setId(FinalVariable.addfoot);
        this.htTimeLine.setOnClickListener(this);
        row3.addView(this.htTimeLine);
        table.addView(row3);
        TableRow row4 = new TableRow(this);
        this.userInfo = new Button(this);
        this.userInfo.setText("获取用户信息");
        this.userInfo.setId(FinalVariable.nomore);
        this.userInfo.setOnClickListener(this);
        row4.addView(this.userInfo);
        this.userOtherInfo = new Button(this);
        this.userOtherInfo.setText("获取他人信息");
        this.userOtherInfo.setId(FinalVariable.first_load);
        this.userOtherInfo.setOnClickListener(this);
        row4.addView(this.userOtherInfo);
        table.addView(row4);
        TableRow row5 = new TableRow(this);
        this.userInfos = new Button(this);
        this.userInfos.setText("获取一批人信息");
        this.userInfos.setId(FinalVariable.load_image);
        this.userInfos.setOnClickListener(this);
        row5.addView(this.userInfos);
        this.friendAdd = new Button(this);
        this.friendAdd.setText("收听某个用户");
        this.friendAdd.setId(FinalVariable.other);
        this.friendAdd.setOnClickListener(this);
        row5.addView(this.friendAdd);
        table.addView(row5);
        TableRow row6 = new TableRow(this);
        this.friendIdolList = new Button(this);
        this.friendIdolList.setText("获取偶像列表");
        this.friendIdolList.setId(FinalVariable.first_update);
        this.friendIdolList.setOnClickListener(this);
        row6.addView(this.friendIdolList);
        this.friendFunsList = new Button(this);
        this.friendFunsList.setText("获取粉丝列表");
        this.friendFunsList.setId(1012);
        this.friendFunsList.setOnClickListener(this);
        row6.addView(this.friendFunsList);
        table.addView(row6);
        TableRow row7 = new TableRow(this);
        this.friendMutualList = new Button(this);
        this.friendMutualList.setText("获取互听列表");
        this.friendMutualList.setId(1013);
        this.friendMutualList.setOnClickListener(this);
        row7.addView(this.friendMutualList);
        this.friendCheck = new Button(this);
        this.friendCheck.setText("验证好友关系");
        this.friendCheck.setId(1014);
        this.friendCheck.setOnClickListener(this);
        row7.addView(this.friendCheck);
        table.addView(row7);
        TableRow row8 = new TableRow(this);
        this.tReList = new Button(this);
        this.tReList.setText("转播获取转播列表");
        this.tReList.setId(1015);
        this.tReList.setOnClickListener(this);
        row8.addView(this.tReList);
        this.friendGetIntimateFriend = new Button(this);
        this.friendGetIntimateFriend.setText("获取最近联系人");
        this.friendGetIntimateFriend.setId(1016);
        this.friendGetIntimateFriend.setOnClickListener(this);
        row8.addView(this.friendGetIntimateFriend);
        table.addView(row8);
        TableRow row9 = new TableRow(this);
        this.lbsGetAroundPeople = new Button(this);
        this.lbsGetAroundPeople.setText("获取附近的人");
        this.lbsGetAroundPeople.setId(1017);
        this.lbsGetAroundPeople.setOnClickListener(this);
        row9.addView(this.lbsGetAroundPeople);
        this.lbsGetAroundNew = new Button(this);
        this.lbsGetAroundNew.setText("获取身边最新的微博");
        this.lbsGetAroundNew.setId(1018);
        this.lbsGetAroundNew.setOnClickListener(this);
        row9.addView(this.lbsGetAroundNew);
        table.addView(row9);
        TableRow row10 = new TableRow(this);
        this.deviceStatus = new Button(this);
        this.deviceStatus.setText("终端状况");
        this.deviceStatus.setId(1019);
        row10.addView(this.deviceStatus);
        this.errorReport = new Button(this);
        this.errorReport.setText("错误反馈");
        this.errorReport.setId(1020);
        row10.addView(this.errorReport);
        table.addView(row10);
        this.scrollView.addView(table);
        setContentView(this.scrollView);
    }

    public void onClick(View v) {
        Looper.myQueue().addIdleHandler(new MessageQueue.IdleHandler() {
            public boolean queueIdle() {
                GeneralInterfaceActivity.this.loadingWindow.showAtLocation(GeneralInterfaceActivity.this.scrollView, 17, 0, 80);
                return false;
            }
        });
        switch (v.getId()) {
            case FinalVariable.update /*1001*/:
                this.timeLineAPI.getHomeTimeLine(this.context, 0, 0, 30, 0, 0, this.requestFormat, this.mCallBack, null, 4);
                return;
            case FinalVariable.remove_footer /*1002*/:
                this.timeLineAPI.getUserTimeLine(this.context, 0, 0, 30, 0, "api_weibo", null, 0, 0, this.requestFormat, this.mCallBack, null, 4);
                return;
            case FinalVariable.change /*1003*/:
                this.weiboAPI.addWeibo(this.context, "hello world !", this.requestFormat, this.longitude, this.latitude, 0, 0, this.mCallBack, null, 4);
                return;
            case FinalVariable.error /*1004*/:
                try {
                    this.weiboAPI.addPic(this.context, "call telephone OKK", this.requestFormat, this.longitude, this.latitude, BitmapFactory.decodeStream(this.context.getAssets().open("logo")), 0, 0, this.mCallBack, null, 4);
                    return;
                } catch (Exception e) {
                    return;
                }
            case FinalVariable.deletefoot /*1005*/:
                this.weiboAPI.addPicUrl(this.context, "y phone ", this.requestFormat, this.longitude, this.latitude, "http://t2.qpic.cn/mblogpic/9c7e34358608bb61a696/2000", 0, 0, this.mCallBack, null, 4);
                return;
            case FinalVariable.addfoot /*1006*/:
                this.timeLineAPI.getHTTimeLine(this.context, this.requestFormat, 30, UploadUtils.SUCCESS, UploadUtils.SUCCESS, 0, 0, "加油", UploadUtils.SUCCESS, 1, 128, this.mCallBack, null, 4);
                return;
            case FinalVariable.nomore /*1007*/:
                this.userAPI.getUserInfo(this.context, this.requestFormat, this.mCallBack, null, 4);
                return;
            case FinalVariable.first_load /*1008*/:
                this.userAPI.getUserOtherInfo(this.context, this.requestFormat, "api_weibo", null, this.mCallBack, null, 4);
                return;
            case FinalVariable.load_image /*1009*/:
                this.userAPI.getUserInfos(this.context, this.requestFormat, "api_weibo", null, this.mCallBack, null, 4);
                return;
            case FinalVariable.other /*1010*/:
                this.friendAPI.addFriend(this.context, this.requestFormat, "api_weibo", null, this.mCallBack, null, 4);
                return;
            case FinalVariable.first_update /*1011*/:
                this.friendAPI.friendIDolList(this.context, this.requestFormat, 30, 0, 1, 0, this.mCallBack, null, 4);
                return;
            case 1012:
                this.friendAPI.friendFansList(this.context, this.requestFormat, 30, 0, 1, 0, 0, this.mCallBack, null, 4);
                return;
            case 1013:
                this.friendAPI.getMutualList(this.context, this.requestFormat, "api_weibo", null, 0, 30, 0, this.mCallBack, null, 4);
                return;
            case 1014:
                this.friendAPI.friendCheck(this.context, this.requestFormat, "api_weibo", null, 2, this.mCallBack, null, 4);
                return;
            case 1015:
                this.weiboAPI.reList(this.context, this.requestFormat, 2, "112714089895346", 0, UploadUtils.SUCCESS, 30, UploadUtils.SUCCESS, this.mCallBack, null, 4);
                return;
            case 1016:
                this.friendAPI.getIntimateFriends(this.context, this.requestFormat, 30, this.mCallBack, null, 4);
                return;
            case 1017:
                this.lbsAPI.getAroundPeople(this.context, this.requestFormat, this.longitude, this.latitude, "", 20, 0, this.mCallBack, null, 4);
                return;
            case 1018:
                this.lbsAPI.getAroundNew(this.context, this.requestFormat, this.longitude, this.latitude, "", 20, this.mCallBack, null, 4);
                return;
            case 1019:
            case 1020:
            default:
                return;
        }
    }
}
