package com.tencent.weibo.sdk.android.model;

import android.graphics.drawable.Drawable;

public class ImageInfo extends BaseVO {
    private static final long serialVersionUID = 2647179822312867756L;
    private Drawable drawable;
    private String imageName;
    private String imagePath;
    private String playPath;

    public String getImagePath() {
        return this.imagePath;
    }

    public void setImagePath(String imagePath2) {
        this.imagePath = imagePath2;
    }

    public String getImageName() {
        return this.imageName;
    }

    public void setImageName(String imageName2) {
        this.imageName = imageName2;
    }

    public Drawable getDrawable() {
        return this.drawable;
    }

    public void setDrawable(Drawable drawable2) {
        this.drawable = drawable2;
    }

    public String getPlayPath() {
        return this.playPath;
    }

    public void setPlayPath(String playPath2) {
        this.playPath = playPath2;
    }
}
