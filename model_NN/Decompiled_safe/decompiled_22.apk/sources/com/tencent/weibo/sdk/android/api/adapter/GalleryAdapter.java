package com.tencent.weibo.sdk.android.api.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.PopupWindow;
import com.tencent.weibo.sdk.android.api.util.ImageLoaderAsync;
import com.tencent.weibo.sdk.android.model.ImageInfo;
import java.util.ArrayList;

public class GalleryAdapter extends BaseAdapter {
    private ArrayList<ImageInfo> imageList;
    private ImageLoaderAsync imageLoader = new ImageLoaderAsync();
    private Context myContext;
    private PopupWindow popView;

    public GalleryAdapter(Context context, PopupWindow loadingView, ArrayList<ImageInfo> images) {
        this.myContext = context;
        this.imageList = images;
        this.popView = loadingView;
    }

    public int getCount() {
        return this.imageList.size();
    }

    public Object getItem(int position) {
        return this.imageList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ImageView imageView = new ImageView(this.myContext);
        Drawable loadImage = this.imageLoader.loadImage(this.imageList.get(position).getImagePath(), new ImageLoaderAsync.callBackImage() {
            public void callback(Drawable Drawable, String imagePath) {
                if (Drawable != null) {
                    imageView.setImageDrawable(Drawable);
                }
            }
        });
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setLayoutParams(new Gallery.LayoutParams(-1, -1));
        if (this.popView != null && this.popView.isShowing()) {
            this.popView.dismiss();
        }
        return imageView;
    }
}
