package com.tencent.qc.stat.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/* compiled from: ProGuard */
public class StatPreferences {
    private static SharedPreferences a = null;

    static SharedPreferences a(Context context) {
        if (a == null) {
            a = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return a;
    }

    public static long a(Context context, String str, long j) {
        return a(context).getLong("qc_" + str, j);
    }

    public static void b(Context context, String str, long j) {
        SharedPreferences.Editor edit = a(context).edit();
        edit.putLong("qc_" + str, j);
        edit.commit();
    }
}
