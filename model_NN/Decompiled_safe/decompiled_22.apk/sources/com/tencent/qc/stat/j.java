package com.tencent.qc.stat;

/* compiled from: ProGuard */
class j {
    private static int[] a = new int[256];
    private static int[] b = new int[256];

    j() {
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: int[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte[] r7, byte[] r8) {
        /*
            r6 = 256(0x100, float:3.59E-43)
            r0 = 0
            int r2 = r8.length
            r1 = 1
            if (r2 < r1) goto L_0x0009
            if (r2 <= r6) goto L_0x0011
        L_0x0009:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "key must be between 1 and 256 bytes"
            r0.<init>(r1)
            throw r0
        L_0x0011:
            r1 = r0
        L_0x0012:
            if (r1 >= r6) goto L_0x0023
            int[] r3 = com.tencent.qc.stat.j.a
            r3[r1] = r1
            int[] r3 = com.tencent.qc.stat.j.b
            int r4 = r1 % r2
            byte r4 = r8[r4]
            r3[r1] = r4
            int r1 = r1 + 1
            goto L_0x0012
        L_0x0023:
            r1 = r0
            r2 = r0
        L_0x0025:
            if (r1 >= r6) goto L_0x0046
            int[] r3 = com.tencent.qc.stat.j.a
            r3 = r3[r1]
            int r2 = r2 + r3
            int[] r3 = com.tencent.qc.stat.j.b
            r3 = r3[r1]
            int r2 = r2 + r3
            r2 = r2 & 255(0xff, float:3.57E-43)
            int[] r3 = com.tencent.qc.stat.j.a
            r3 = r3[r1]
            int[] r4 = com.tencent.qc.stat.j.a
            int[] r5 = com.tencent.qc.stat.j.a
            r5 = r5[r2]
            r4[r1] = r5
            int[] r4 = com.tencent.qc.stat.j.a
            r4[r2] = r3
            int r1 = r1 + 1
            goto L_0x0025
        L_0x0046:
            int r1 = r7.length
            byte[] r3 = new byte[r1]
            r1 = r0
            r2 = r0
        L_0x004b:
            int r4 = r7.length
            if (r0 >= r4) goto L_0x0081
            int r2 = r2 + 1
            r2 = r2 & 255(0xff, float:3.57E-43)
            int[] r4 = com.tencent.qc.stat.j.a
            r4 = r4[r2]
            int r1 = r1 + r4
            r1 = r1 & 255(0xff, float:3.57E-43)
            int[] r4 = com.tencent.qc.stat.j.a
            r4 = r4[r2]
            int[] r5 = com.tencent.qc.stat.j.a
            int[] r6 = com.tencent.qc.stat.j.a
            r6 = r6[r1]
            r5[r2] = r6
            int[] r5 = com.tencent.qc.stat.j.a
            r5[r1] = r4
            int[] r4 = com.tencent.qc.stat.j.a
            r4 = r4[r2]
            int[] r5 = com.tencent.qc.stat.j.a
            r5 = r5[r1]
            int r4 = r4 + r5
            r4 = r4 & 255(0xff, float:3.57E-43)
            int[] r5 = com.tencent.qc.stat.j.a
            r4 = r5[r4]
            byte r5 = r7[r0]
            r4 = r4 ^ r5
            byte r4 = (byte) r4
            r3[r0] = r4
            int r0 = r0 + 1
            goto L_0x004b
        L_0x0081:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qc.stat.j.a(byte[], byte[]):byte[]");
    }

    public static byte[] b(byte[] bArr, byte[] bArr2) {
        return a(bArr, bArr2);
    }
}
