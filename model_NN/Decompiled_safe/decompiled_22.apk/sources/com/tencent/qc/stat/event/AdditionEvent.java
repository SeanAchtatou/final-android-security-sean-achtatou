package com.tencent.qc.stat.event;

import android.content.Context;
import com.tencent.qc.stat.StatConfig;
import com.tencent.qc.stat.common.StatCommonHelper;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class AdditionEvent extends Event {
    Map a = null;

    public AdditionEvent(Context context, int i, Map map) {
        super(context, i);
        this.a = map;
    }

    public EventType a() {
        return EventType.ADDITION;
    }

    public boolean a(JSONObject jSONObject) {
        StatCommonHelper.a(jSONObject, "qq", StatConfig.k());
        if (this.a == null || this.a.size() <= 0) {
            return true;
        }
        for (Map.Entry entry : this.a.entrySet()) {
            jSONObject.put((String) entry.getKey(), entry.getValue());
        }
        return true;
    }
}
