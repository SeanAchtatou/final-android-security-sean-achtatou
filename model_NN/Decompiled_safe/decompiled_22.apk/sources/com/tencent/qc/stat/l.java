package com.tencent.qc.stat;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.tencent.qc.stat.common.StatCommonHelper;
import com.tencent.qc.stat.common.StatLogger;
import com.tencent.qc.stat.event.Event;
import com.utils.FinalVariable;
import java.util.Arrays;
import java.util.List;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

/* compiled from: ProGuard */
class l {
    static final byte[] c = "03a976511e2cbe3a7f26808fb7af3c05".getBytes();
    private static StatLogger d = StatCommonHelper.b();
    private static long e = -1;
    private static l f = new l();
    private static Context g = null;
    DefaultHttpClient a = null;
    Handler b = null;

    private l() {
        HandlerThread handlerThread = new HandlerThread("StatDispatcher");
        handlerThread.start();
        e = handlerThread.getId();
        this.b = new Handler(handlerThread.getLooper());
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, FinalVariable.vb_success);
        HttpConnectionParams.setSoTimeout(basicHttpParams, FinalVariable.vb_success);
        this.a = new DefaultHttpClient(basicHttpParams);
        this.a.setKeepAliveStrategy(new n(this));
        if (StatConfig.i() != null) {
            this.a.getParams().setParameter("http.route.default-proxy", StatConfig.i());
        }
    }

    static void a(Context context) {
        g = context.getApplicationContext();
    }

    static Context a() {
        return g;
    }

    static l b() {
        return f;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x025d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x025e, code lost:
        com.tencent.qc.stat.l.d.b(r0.toString());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.util.List r9, com.tencent.qc.stat.q r10) {
        /*
            r8 = this;
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r0 = "["
            r3.append(r0)
            r1 = r2
        L_0x000c:
            int r0 = r9.size()
            if (r1 >= r0) goto L_0x002c
            java.lang.Object r0 = r9.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            r3.append(r0)
            int r0 = r9.size()
            int r0 = r0 + -1
            if (r1 == r0) goto L_0x0028
            java.lang.String r0 = ","
            r3.append(r0)
        L_0x0028:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x000c
        L_0x002c:
            java.lang.String r0 = "]"
            r3.append(r0)
            com.tencent.qc.stat.common.StatLogger r0 = com.tencent.qc.stat.l.d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "["
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = com.tencent.qc.stat.StatConfig.j()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = "]Send request("
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = r3.toString()
            int r4 = r4.length()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = "bytes):"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = r3.toString()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r0.b(r1)
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r4 = "http://"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = com.tencent.qc.stat.StatConfig.j()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = "/qqconnectutil/sdk"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            java.lang.String r0 = "Accept-Encoding"
            java.lang.String r4 = "gzip"
            r1.addHeader(r0, r4)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r0 = "Connection"
            java.lang.String r4 = "Keep-Alive"
            r1.setHeader(r0, r4)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r0 = "Cache-Control"
            r1.removeHeaders(r0)     // Catch:{ Exception -> 0x01b7 }
            android.content.Context r0 = com.tencent.qc.stat.l.g     // Catch:{ Exception -> 0x01b7 }
            org.apache.http.HttpHost r4 = com.tencent.qc.stat.common.StatCommonHelper.a(r0)     // Catch:{ Exception -> 0x01b7 }
            if (r4 == 0) goto L_0x00d1
            org.apache.http.impl.client.DefaultHttpClient r0 = r8.a     // Catch:{ Exception -> 0x01b7 }
            org.apache.http.params.HttpParams r0 = r0.getParams()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r2 = "http.route.default-proxy"
            android.content.Context r5 = com.tencent.qc.stat.l.g     // Catch:{ Exception -> 0x01b7 }
            org.apache.http.HttpHost r5 = com.tencent.qc.stat.common.StatCommonHelper.a(r5)     // Catch:{ Exception -> 0x01b7 }
            r0.setParameter(r2, r5)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r0 = "X-Online-Host"
            java.lang.String r2 = "cgi.connect.qq.com"
            r1.addHeader(r0, r2)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r0 = "Accept"
            java.lang.String r2 = "*/*"
            r1.addHeader(r0, r2)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r0 = "Content-Type"
            java.lang.String r2 = "json"
            r1.addHeader(r0, r2)     // Catch:{ Exception -> 0x01b7 }
            r2 = 1
        L_0x00d1:
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x01b7 }
            r5.<init>()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r6 = "UTF-8"
            byte[] r0 = r0.getBytes(r6)     // Catch:{ Exception -> 0x01b7 }
            int r6 = r0.length     // Catch:{ Exception -> 0x01b7 }
            int r3 = r3.length()     // Catch:{ Exception -> 0x01b7 }
            r7 = 256(0x100, float:3.59E-43)
            if (r3 >= r7) goto L_0x01c5
            if (r4 != 0) goto L_0x01ae
            java.lang.String r3 = "Content-Encoding"
            java.lang.String r4 = "rc4"
            r1.addHeader(r3, r4)     // Catch:{ Exception -> 0x01b7 }
        L_0x00f2:
            byte[] r3 = com.tencent.qc.stat.l.c     // Catch:{ Exception -> 0x01b7 }
            byte[] r0 = com.tencent.qc.stat.j.a(r0, r3)     // Catch:{ Exception -> 0x01b7 }
            org.apache.http.entity.ByteArrayEntity r3 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Exception -> 0x01b7 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x01b7 }
            r1.setEntity(r3)     // Catch:{ Exception -> 0x01b7 }
            org.apache.http.impl.client.DefaultHttpClient r0 = r8.a     // Catch:{ Exception -> 0x01b7 }
            org.apache.http.HttpResponse r1 = r0.execute(r1)     // Catch:{ Exception -> 0x01b7 }
            if (r2 == 0) goto L_0x0113
            org.apache.http.impl.client.DefaultHttpClient r0 = r8.a     // Catch:{ Exception -> 0x01b7 }
            org.apache.http.params.HttpParams r0 = r0.getParams()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r2 = "http.route.default-proxy"
            r0.removeParameter(r2)     // Catch:{ Exception -> 0x01b7 }
        L_0x0113:
            org.apache.http.HttpEntity r0 = r1.getEntity()     // Catch:{ Exception -> 0x01b7 }
            org.apache.http.StatusLine r2 = r1.getStatusLine()     // Catch:{ Exception -> 0x01b7 }
            int r2 = r2.getStatusCode()     // Catch:{ Exception -> 0x01b7 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 != r3) goto L_0x0184
            long r2 = r0.getContentLength()     // Catch:{ Exception -> 0x01b7 }
            r4 = 0
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0269
            java.io.InputStream r2 = r0.getContent()     // Catch:{ Exception -> 0x01b7 }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Exception -> 0x01b7 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x01b7 }
            long r4 = r0.getContentLength()     // Catch:{ Exception -> 0x01b7 }
            int r0 = (int) r4     // Catch:{ Exception -> 0x01b7 }
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x01b7 }
            r3.readFully(r0)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r3 = "Content-Encoding"
            org.apache.http.Header r3 = r1.getFirstHeader(r3)     // Catch:{ Exception -> 0x01b7 }
            if (r3 == 0) goto L_0x0181
            java.lang.String r4 = r3.getValue()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r5 = "gzip,rc4"
            boolean r4 = r4.equalsIgnoreCase(r5)     // Catch:{ Exception -> 0x01b7 }
            if (r4 == 0) goto L_0x021f
            byte[] r0 = com.tencent.qc.stat.common.StatCommonHelper.a(r0)     // Catch:{ Exception -> 0x01b7 }
            byte[] r3 = com.tencent.qc.stat.l.c     // Catch:{ Exception -> 0x01b7 }
            byte[] r0 = com.tencent.qc.stat.j.b(r0, r3)     // Catch:{ Exception -> 0x01b7 }
        L_0x015e:
            com.tencent.qc.stat.common.StatLogger r3 = com.tencent.qc.stat.l.d     // Catch:{ JSONException -> 0x025d }
            java.lang.String r4 = new java.lang.String     // Catch:{ JSONException -> 0x025d }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r0, r5)     // Catch:{ JSONException -> 0x025d }
            r3.h(r4)     // Catch:{ JSONException -> 0x025d }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x025d }
            java.lang.String r4 = new java.lang.String     // Catch:{ JSONException -> 0x025d }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r0, r5)     // Catch:{ JSONException -> 0x025d }
            r3.<init>(r4)     // Catch:{ JSONException -> 0x025d }
            java.lang.String r0 = "cfg"
            org.json.JSONObject r0 = r3.getJSONObject(r0)     // Catch:{ JSONException -> 0x025d }
            if (r0 == 0) goto L_0x0181
            com.tencent.qc.stat.StatConfig.a(r0)     // Catch:{ JSONException -> 0x025d }
        L_0x0181:
            r2.close()     // Catch:{ Exception -> 0x01b7 }
        L_0x0184:
            com.tencent.qc.stat.common.StatLogger r0 = com.tencent.qc.stat.l.d     // Catch:{ Exception -> 0x01b7 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b7 }
            r2.<init>()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r3 = "recv response:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x01b7 }
            org.apache.http.StatusLine r1 = r1.getStatusLine()     // Catch:{ Exception -> 0x01b7 }
            int r1 = r1.getStatusCode()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ Exception -> 0x01b7 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01b7 }
            r0.b(r1)     // Catch:{ Exception -> 0x01b7 }
            if (r10 == 0) goto L_0x01ad
            r10.a()     // Catch:{ Exception -> 0x01b7 }
        L_0x01ad:
            return
        L_0x01ae:
            java.lang.String r3 = "X-Content-Encoding"
            java.lang.String r4 = "rc4"
            r1.addHeader(r3, r4)     // Catch:{ Exception -> 0x01b7 }
            goto L_0x00f2
        L_0x01b7:
            r0 = move-exception
            com.tencent.qc.stat.common.StatLogger r1 = com.tencent.qc.stat.l.d     // Catch:{ all -> 0x01c3 }
            r1.b(r0)     // Catch:{ all -> 0x01c3 }
            if (r10 == 0) goto L_0x01ad
            r10.b()     // Catch:{ all -> 0x01c3 }
            goto L_0x01ad
        L_0x01c3:
            r0 = move-exception
            throw r0
        L_0x01c5:
            if (r4 != 0) goto L_0x0217
            java.lang.String r3 = "Content-Encoding"
            java.lang.String r4 = "rc4,gzip"
            r1.addHeader(r3, r4)     // Catch:{ Exception -> 0x01b7 }
        L_0x01ce:
            r3 = 4
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x01b7 }
            r5.write(r3)     // Catch:{ Exception -> 0x01b7 }
            java.util.zip.GZIPOutputStream r3 = new java.util.zip.GZIPOutputStream     // Catch:{ Exception -> 0x01b7 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x01b7 }
            r3.write(r0)     // Catch:{ Exception -> 0x01b7 }
            r3.close()     // Catch:{ Exception -> 0x01b7 }
            byte[] r0 = r5.toByteArray()     // Catch:{ Exception -> 0x01b7 }
            r3 = 0
            r4 = 4
            java.nio.ByteBuffer r3 = java.nio.ByteBuffer.wrap(r0, r3, r4)     // Catch:{ Exception -> 0x01b7 }
            r3.putInt(r6)     // Catch:{ Exception -> 0x01b7 }
            com.tencent.qc.stat.common.StatLogger r3 = com.tencent.qc.stat.l.d     // Catch:{ Exception -> 0x01b7 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b7 }
            r4.<init>()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r5 = "before Gzip:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01b7 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r5 = " bytes, after Gzip:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01b7 }
            int r5 = r0.length     // Catch:{ Exception -> 0x01b7 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r5 = " bytes"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x01b7 }
            r3.h(r4)     // Catch:{ Exception -> 0x01b7 }
            goto L_0x00f2
        L_0x0217:
            java.lang.String r3 = "X-Content-Encoding"
            java.lang.String r4 = "rc4,gzip"
            r1.addHeader(r3, r4)     // Catch:{ Exception -> 0x01b7 }
            goto L_0x01ce
        L_0x021f:
            java.lang.String r4 = r3.getValue()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r5 = "rc4,gzip"
            boolean r4 = r4.equalsIgnoreCase(r5)     // Catch:{ Exception -> 0x01b7 }
            if (r4 == 0) goto L_0x0237
            byte[] r3 = com.tencent.qc.stat.l.c     // Catch:{ Exception -> 0x01b7 }
            byte[] r0 = com.tencent.qc.stat.j.b(r0, r3)     // Catch:{ Exception -> 0x01b7 }
            byte[] r0 = com.tencent.qc.stat.common.StatCommonHelper.a(r0)     // Catch:{ Exception -> 0x01b7 }
            goto L_0x015e
        L_0x0237:
            java.lang.String r4 = r3.getValue()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r5 = "gzip"
            boolean r4 = r4.equalsIgnoreCase(r5)     // Catch:{ Exception -> 0x01b7 }
            if (r4 == 0) goto L_0x0249
            byte[] r0 = com.tencent.qc.stat.common.StatCommonHelper.a(r0)     // Catch:{ Exception -> 0x01b7 }
            goto L_0x015e
        L_0x0249:
            java.lang.String r3 = r3.getValue()     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r4 = "rc4"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ Exception -> 0x01b7 }
            if (r3 == 0) goto L_0x015e
            byte[] r3 = com.tencent.qc.stat.l.c     // Catch:{ Exception -> 0x01b7 }
            byte[] r0 = com.tencent.qc.stat.j.b(r0, r3)     // Catch:{ Exception -> 0x01b7 }
            goto L_0x015e
        L_0x025d:
            r0 = move-exception
            com.tencent.qc.stat.common.StatLogger r3 = com.tencent.qc.stat.l.d     // Catch:{ Exception -> 0x01b7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01b7 }
            r3.b(r0)     // Catch:{ Exception -> 0x01b7 }
            goto L_0x0181
        L_0x0269:
            org.apache.http.util.EntityUtils.toString(r0)     // Catch:{ Exception -> 0x01b7 }
            goto L_0x0184
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qc.stat.l.a(java.util.List, com.tencent.qc.stat.q):void");
    }

    /* access modifiers changed from: package-private */
    public void b(List list, q qVar) {
        if (!list.isEmpty()) {
            this.b.post(new o(this, list, qVar));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Event event, q qVar) {
        b(Arrays.asList(event.d()), qVar);
    }
}
