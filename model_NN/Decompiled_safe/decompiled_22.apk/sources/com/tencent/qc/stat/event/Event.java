package com.tencent.qc.stat.event;

import android.content.Context;
import android.util.Log;
import com.tencent.qc.stat.StatConfig;
import com.tencent.qc.stat.StatStore;
import com.tencent.qc.stat.common.StatCommonHelper;
import com.tencent.qc.stat.common.User;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public abstract class Event {
    protected String b;
    protected long c;
    protected int d;
    protected User e = null;
    protected Context f;

    public abstract EventType a();

    public abstract boolean a(JSONObject jSONObject);

    public long b() {
        return this.c;
    }

    Event(Context context, int i) {
        this.f = context;
        this.b = StatConfig.a(context);
        this.c = System.currentTimeMillis() / 1000;
        this.d = i;
        this.e = StatStore.a(context).b(context);
    }

    public Context c() {
        return this.f;
    }

    public boolean b(JSONObject jSONObject) {
        try {
            StatCommonHelper.a(jSONObject, "ky", this.b);
            jSONObject.put("et", a().a());
            jSONObject.put("ui", this.e.a());
            StatCommonHelper.a(jSONObject, "mc", this.e.b());
            jSONObject.put("si", this.d);
            jSONObject.put("ts", this.c);
            return a(jSONObject);
        } catch (JSONException e2) {
            Log.e("Event", "Failed to encode", e2);
            return false;
        }
    }

    public String d() {
        JSONObject jSONObject = new JSONObject();
        b(jSONObject);
        return jSONObject.toString();
    }
}
