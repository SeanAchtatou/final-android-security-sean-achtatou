package com.tencent.qc.stat;

import android.content.Context;
import com.tencent.qc.stat.common.StatCommonHelper;
import com.tencent.qc.stat.common.StatLogger;
import com.tencent.qc.stat.common.StatPreferences;
import java.util.Iterator;
import org.apache.http.HttpHost;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class StatConfig {
    static r a = new r(2);
    static r b = new r(1);
    static String c = "__HIBERNATE__";
    static String d = "";
    public static boolean e = true;
    private static StatLogger f = StatCommonHelper.b();
    private static StatReportStrategy g = StatReportStrategy.PERIOD;
    private static boolean h = true;
    private static int i = 30000;
    private static int j = 1024;
    private static int k = 30;
    private static int l = 3;
    private static int m = 30;
    private static String n = null;
    private static String o = null;
    private static String p;
    private static String q;
    private static int r = 1440;
    private static int s = 1024;
    private static boolean t = true;
    private static long u = 0;
    private static long v = 300000;

    static boolean a(int i2, int i3, int i4) {
        if (i2 < i3 || i2 > i4) {
            return false;
        }
        return true;
    }

    public static StatReportStrategy a() {
        return g;
    }

    public static void a(StatReportStrategy statReportStrategy) {
        g = statReportStrategy;
        f.h("Change to SendStrategy:" + statReportStrategy.name());
    }

    public static boolean b() {
        return StatCommonHelper.b().a();
    }

    public static boolean c() {
        return h;
    }

    public static void a(boolean z) {
        h = z;
        if (!z) {
            f.c("!!!!!!MTA StatService has been disabled!!!!!!");
        }
    }

    public static int d() {
        return i;
    }

    public static void a(int i2) {
        if (!a(i2, 1000, 86400000)) {
            f.e("setSessionTimoutMillis can not exceed the range of [1000, 24 * 60 * 60 * 1000].");
        } else {
            i = i2;
        }
    }

    public static int e() {
        return m;
    }

    public static int f() {
        return l;
    }

    static int g() {
        return k;
    }

    public static int h() {
        return j;
    }

    static void a(JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase(Integer.toString(b.a))) {
                    a(b, jSONObject.getJSONObject(next));
                } else if (next.equalsIgnoreCase(Integer.toString(a.a))) {
                    a(a, jSONObject.getJSONObject(next));
                } else if (next.equalsIgnoreCase("rs")) {
                    StatReportStrategy a2 = StatReportStrategy.a(jSONObject.getInt(next));
                    if (a2 != null) {
                        g = a2;
                        f.h("Change to ReportStrategy:" + a2.name());
                    }
                } else {
                    return;
                }
            }
        } catch (JSONException e2) {
            f.b((Exception) e2);
        }
    }

    static void a(r rVar, JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase("v")) {
                    rVar.d = jSONObject.getInt(next);
                } else if (next.equalsIgnoreCase("c")) {
                    String string = jSONObject.getString("c");
                    if (string.length() > 0) {
                        rVar.b = new JSONObject(string);
                    }
                } else if (next.equalsIgnoreCase("m")) {
                    rVar.c = jSONObject.getString("m");
                }
            }
            StatStore a2 = StatStore.a(l.a());
            if (a2 != null) {
                a2.a(rVar);
            }
            if (rVar.a == b.a) {
                b(rVar.b);
                c(rVar.b);
            }
        } catch (JSONException e2) {
            f.b((Exception) e2);
        }
    }

    static void b(JSONObject jSONObject) {
        try {
            StatReportStrategy a2 = StatReportStrategy.a(jSONObject.getInt("rs"));
            if (a2 != null) {
                a(a2);
                f.g("Change to ReportStrategy: " + a2.name());
            }
        } catch (JSONException e2) {
            f.h("rs not found.");
        }
    }

    static void a(r rVar) {
        if (rVar.a == b.a) {
            b = rVar;
            b(b.b);
        } else if (rVar.a == a.a) {
            a = rVar;
        }
    }

    static void c(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString(c);
            f.h("hibernateVer:" + string + ", current version:" + "0.6.12");
            long b2 = StatCommonHelper.b(string);
            if (StatCommonHelper.b("0.6.12") <= b2) {
                StatPreferences.b(l.a(), c, b2);
                a(false);
                f.c("MTA has disable for SDK version of " + string + " or lower.");
            }
        } catch (JSONException e2) {
            f.h("__HIBERNATE__ not found.");
        }
    }

    static HttpHost i() {
        if (o == null || o.length() <= 0) {
            return null;
        }
        String str = o;
        String[] split = str.split(":");
        int i2 = 80;
        if (split.length == 2) {
            str = split[0];
            i2 = Integer.parseInt(split[1]);
        }
        return new HttpHost(str, i2);
    }

    static String j() {
        if (n == null || n.length() <= 0) {
            return "cgi.connect.qq.com";
        }
        return n;
    }

    public static String a(Context context) {
        if (p != null) {
            return p;
        }
        p = StatCommonHelper.i(context);
        if (p == null || p.trim().length() == 0) {
            f.e("AppKey can not be null or empty, please read Developer's Guide first!");
        }
        return p;
    }

    public static void a(String str) {
        if (str.length() > 256) {
            f.e("The length of appkey cann't exceed 256 bytes.");
        } else {
            p = str;
        }
    }

    public static String b(Context context) {
        if (q != null) {
            return q;
        }
        q = StatCommonHelper.j(context);
        if (q == null || q.trim().length() == 0) {
            f.e("installChannel can not be null or empty, please read Developer's Guide first!");
        }
        return q;
    }

    public static void b(String str) {
        if (str.length() > 128) {
            f.e("the length of installChannel can not exceed the range of 128 bytes.");
        } else {
            q = str;
        }
    }

    public static String k() {
        return d;
    }

    public static void b(int i2) {
        if (!a(i2, 1, 10080)) {
            f.e("setSendPeriodMinutes can not exceed the range of [1, 7*24*60] minutes.");
        } else {
            r = i2;
        }
    }

    public static int l() {
        return r;
    }

    public static boolean m() {
        return t;
    }

    public static void b(boolean z) {
        t = z;
    }

    public static void c(boolean z) {
        e = z;
    }
}
