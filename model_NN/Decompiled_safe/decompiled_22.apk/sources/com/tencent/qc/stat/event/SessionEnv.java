package com.tencent.qc.stat.event;

import android.content.Context;
import com.tencent.qc.stat.common.Env;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class SessionEnv extends Event {
    private Env a;
    private JSONObject g = null;

    public SessionEnv(Context context, int i, JSONObject jSONObject) {
        super(context, i);
        this.a = new Env(context);
        this.g = jSONObject;
    }

    public EventType a() {
        return EventType.SESSION_ENV;
    }

    public boolean a(JSONObject jSONObject) {
        jSONObject.put("ut", this.e.c());
        if (this.g != null) {
            jSONObject.put("cfg", this.g);
        }
        this.a.a(jSONObject);
        return true;
    }
}
