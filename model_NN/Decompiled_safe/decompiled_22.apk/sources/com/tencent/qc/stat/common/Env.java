package com.tencent.qc.stat.common;

import android.content.Context;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class Env {
    static c a;
    private static JSONObject d = null;
    Integer b = null;
    String c = null;

    static c a(Context context) {
        if (a == null) {
            a = new c(context.getApplicationContext());
        }
        return a;
    }

    public Env(Context context) {
        a(context);
        this.b = StatCommonHelper.o(context.getApplicationContext());
        this.c = StatCommonHelper.n(context);
    }

    public void a(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        if (a != null) {
            a.a(jSONObject2);
        }
        StatCommonHelper.a(jSONObject2, "cn", this.c);
        if (this.b != null) {
            jSONObject2.put("tn", this.b);
        }
        jSONObject.put("ev", jSONObject2);
        if (d != null && d.length() > 0) {
            jSONObject.put("eva", d);
        }
    }
}
