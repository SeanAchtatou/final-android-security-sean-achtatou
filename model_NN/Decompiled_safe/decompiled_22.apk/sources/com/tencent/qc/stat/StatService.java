package com.tencent.qc.stat;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.tencent.qc.stat.common.StatCommonHelper;
import com.tencent.qc.stat.common.StatLogger;
import com.tencent.qc.stat.common.StatPreferences;
import com.tencent.qc.stat.event.AdditionEvent;
import com.tencent.qc.stat.event.CustomEvent;
import com.tencent.qc.stat.event.SessionEnv;
import java.util.Map;
import java.util.WeakHashMap;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class StatService {
    private static Handler a;
    private static Map b = new WeakHashMap();
    private static volatile long c = 0;
    private static volatile long d = 0;
    private static volatile int e = 0;
    /* access modifiers changed from: private */
    public static StatLogger f = StatCommonHelper.b();

    static void a(Context context) {
        if (context != null && a == null && b(context)) {
            HandlerThread handlerThread = new HandlerThread("StatService");
            handlerThread.start();
            l.a(context);
            a = new Handler(handlerThread.getLooper());
            StatStore.a(context);
            StatStore.a(context).b();
            if (StatConfig.a() == StatReportStrategy.APP_LAUNCH) {
                f.h("StatConfig.getStatSendStrategy() == StatReportStrategy.APP_LAUNCH");
                if (StatCommonHelper.h(context)) {
                    StatStore.a(context).a(-1);
                }
            }
        }
    }

    static boolean b(Context context) {
        if (StatCommonHelper.b("0.6.12") > StatPreferences.a(context, StatConfig.c, 0)) {
            return true;
        }
        StatConfig.a(false);
        return false;
    }

    static boolean a(String str) {
        if (str == null || str.length() == 0) {
            return true;
        }
        return false;
    }

    private static Handler d(Context context) {
        a(context);
        return a;
    }

    static JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (StatConfig.b.d != 0) {
                jSONObject2.put("v", StatConfig.b.d);
            }
            jSONObject.put(Integer.toString(StatConfig.b.a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (StatConfig.a.d != 0) {
                jSONObject3.put("v", StatConfig.a.d);
            }
            jSONObject.put(Integer.toString(StatConfig.a.a), jSONObject3);
        } catch (JSONException e2) {
            f.b((Exception) e2);
        }
        return jSONObject;
    }

    static void c(Context context) {
        e = StatCommonHelper.a();
        if (d(context) != null) {
            d(context).post(new t(new SessionEnv(context, e, a())));
        }
    }

    static int a(Context context, boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        if (z && currentTimeMillis - c >= ((long) StatConfig.d())) {
            c(context);
        }
        c = currentTimeMillis;
        if (d == 0) {
            d = StatCommonHelper.c();
        }
        if (currentTimeMillis >= d) {
            d = StatCommonHelper.c();
            if (StatStore.a(context).b(context).c() != 1) {
                StatStore.a(context).b(context).a(1);
            }
            c(context);
        }
        if (e == 0) {
            c(context);
        }
        return e;
    }

    public static void a(Context context, String str) {
        if (str == null) {
            str = "";
        }
        if (!StatConfig.d.equals(str)) {
            StatConfig.d = str;
            a(context, (Map) null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qc.stat.StatService.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.qc.stat.StatService.a(android.content.Context, java.lang.String):void
      com.tencent.qc.stat.StatService.a(android.content.Context, java.util.Map):void
      com.tencent.qc.stat.StatService.a(android.content.Context, boolean):int */
    static void a(Context context, Map map) {
        if (StatConfig.c()) {
            if (context == null) {
                f.e("The Context of StatService.sendAdditionEvent() can not be null!");
                return;
            }
            AdditionEvent additionEvent = new AdditionEvent(context, a(context, false), map);
            if (d(context) != null) {
                d(context).post(new t(additionEvent));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qc.stat.StatService.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.qc.stat.StatService.a(android.content.Context, java.lang.String):void
      com.tencent.qc.stat.StatService.a(android.content.Context, java.util.Map):void
      com.tencent.qc.stat.StatService.a(android.content.Context, boolean):int */
    public static void a(Context context, String str, String... strArr) {
        if (StatConfig.c()) {
            if (context == null) {
                f.e("The Context of StatService.trackCustomEvent() can not be null!");
            } else if (a(str)) {
                f.e("The event_id of StatService.trackCustomEvent() can not be null or empty.");
            } else {
                CustomEvent customEvent = new CustomEvent(context, a(context, false), str);
                customEvent.a(strArr);
                if (d(context) != null) {
                    d(context).post(new t(customEvent));
                }
            }
        }
    }

    public static void b(String str) {
        StatConfig.a("Aqc" + str);
        StatConfig.b("QQConnect");
        StatConfig.c(false);
        StatConfig.b(true);
        StatConfig.a(1800000);
        StatConfig.b(1440);
        StatConfig.a(StatReportStrategy.PERIOD);
    }
}
