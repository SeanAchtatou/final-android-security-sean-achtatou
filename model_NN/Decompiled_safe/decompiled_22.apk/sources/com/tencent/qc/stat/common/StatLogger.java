package com.tencent.qc.stat.common;

import android.util.Log;
import com.tencent.qc.stat.StatConfig;
import org.apache.commons.httpclient.cookie.CookiePolicy;

/* compiled from: ProGuard */
public final class StatLogger {
    private String a = CookiePolicy.DEFAULT;
    private boolean b = true;
    private int c = 2;

    public boolean a() {
        return this.b;
    }

    public void a(boolean z) {
        this.b = z;
    }

    public StatLogger() {
    }

    public StatLogger(String str) {
        this.a = str;
    }

    private String b() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace == null) {
            return null;
        }
        for (StackTraceElement stackTraceElement : stackTrace) {
            if (!stackTraceElement.isNativeMethod() && !stackTraceElement.getClassName().equals(Thread.class.getName()) && !stackTraceElement.getClassName().equals(getClass().getName())) {
                return "[" + Thread.currentThread().getName() + "(" + Thread.currentThread().getId() + "): " + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + "]";
            }
        }
        return null;
    }

    public void a(Object obj) {
        if (this.c <= 4) {
            String b2 = b();
            Log.i(this.a, b2 == null ? obj.toString() : b2 + " - " + obj);
        }
    }

    public void b(Object obj) {
        if (a()) {
            a(obj);
        }
    }

    public void c(Object obj) {
        if (this.c <= 5) {
            String b2 = b();
            Log.w(this.a, b2 == null ? obj.toString() : b2 + " - " + obj);
        }
    }

    public void d(Object obj) {
        if (a()) {
            c(obj);
        }
    }

    public void e(Object obj) {
        if (this.c <= 6) {
            String b2 = b();
            Log.e(this.a, b2 == null ? obj.toString() : b2 + " - " + obj);
        }
    }

    public void a(Exception exc) {
        if (this.c <= 6) {
            StringBuffer stringBuffer = new StringBuffer();
            String b2 = b();
            StackTraceElement[] stackTrace = exc.getStackTrace();
            if (b2 != null) {
                stringBuffer.append(b2 + " - " + exc + "\r\n");
            } else {
                stringBuffer.append(exc + "\r\n");
            }
            if (stackTrace != null && stackTrace.length > 0) {
                for (StackTraceElement stackTraceElement : stackTrace) {
                    if (stackTraceElement != null) {
                        stringBuffer.append("[ " + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + " ]\r\n");
                    }
                }
            }
            Log.e(this.a, stringBuffer.toString());
        }
    }

    public void f(Object obj) {
        if (a()) {
            e(obj);
        }
    }

    public void b(Exception exc) {
        if (StatConfig.b()) {
            a(exc);
        }
    }

    public void g(Object obj) {
        if (this.c <= 3) {
            String b2 = b();
            Log.d(this.a, b2 == null ? obj.toString() : b2 + " - " + obj);
        }
    }

    public void h(Object obj) {
        if (a()) {
            g(obj);
        }
    }
}
