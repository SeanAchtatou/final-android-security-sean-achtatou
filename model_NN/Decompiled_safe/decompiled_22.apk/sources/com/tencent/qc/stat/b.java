package com.tencent.qc.stat;

import java.util.ArrayList;

/* compiled from: ProGuard */
class b implements Runnable {
    final /* synthetic */ int a;
    final /* synthetic */ StatStore b;

    b(StatStore statStore, int i) {
        this.b = statStore;
        this.a = i;
    }

    public void run() {
        int i;
        if (this.b.b != 0) {
            StatStore.e.b("Load " + Integer.toString(this.b.b) + " unsent events");
            ArrayList arrayList = new ArrayList();
            ArrayList<s> arrayList2 = new ArrayList<>();
            int i2 = this.a;
            if (i2 == -1 || i2 > StatConfig.g()) {
                i = StatConfig.g();
            } else {
                i = i2;
            }
            this.b.b -= i;
            this.b.c(arrayList2, i);
            StatStore.e.b("Peek " + Integer.toString(arrayList2.size()) + " unsent events.");
            if (!arrayList2.isEmpty()) {
                this.b.b(arrayList2, 2);
                for (s sVar : arrayList2) {
                    arrayList.add(sVar.b);
                }
                l.b().b(arrayList, new m(this, arrayList2, i));
            }
        }
    }
}
