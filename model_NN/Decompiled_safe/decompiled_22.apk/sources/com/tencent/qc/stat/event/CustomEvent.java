package com.tencent.qc.stat.event;

import android.content.Context;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.Arrays;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class CustomEvent extends Event {
    protected Key a = new Key();
    private long g = -1;

    /* compiled from: ProGuard */
    public class Key {
        String a;
        String[] b;
        Properties c = null;

        public String toString() {
            String str = this.a;
            String str2 = "";
            if (this.b != null && this.b.length > 0) {
                String str3 = this.b[0];
                for (int i = 1; i < this.b.length; i++) {
                    str3 = str3 + "," + this.b[i];
                }
                str2 = "[" + str3 + "]";
            }
            if (this.c != null && this.c.size() > 0) {
                str2 = str2 + this.c.toString();
            }
            return str + str2;
        }

        public int hashCode() {
            int i = 0;
            if (this.a != null) {
                i = this.a.hashCode();
            }
            if (this.b != null) {
                i ^= Arrays.hashCode(this.b);
            }
            if (this.c != null) {
                return i ^ this.c.hashCode();
            }
            return i;
        }

        public boolean equals(Object obj) {
            boolean z;
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Key)) {
                return false;
            }
            Key key = (Key) obj;
            if (!this.a.equals(key.a) || !Arrays.equals(this.b, key.b)) {
                z = false;
            } else {
                z = true;
            }
            if (this.c != null) {
                if (!z || !this.c.equals(key.c)) {
                    return false;
                }
                return true;
            } else if (!z || key.c != null) {
                return false;
            } else {
                return true;
            }
        }
    }

    public void a(String[] strArr) {
        this.a.b = strArr;
    }

    public CustomEvent(Context context, int i, String str) {
        super(context, i);
        this.a.a = str;
    }

    public EventType a() {
        return EventType.CUSTOM;
    }

    public boolean a(JSONObject jSONObject) {
        jSONObject.put("ei", this.a.a);
        if (this.g > 0) {
            jSONObject.put("du", this.g);
        }
        if (this.a.b != null) {
            JSONArray jSONArray = new JSONArray();
            for (String put : this.a.b) {
                jSONArray.put(put);
            }
            jSONObject.put(LocaleUtil.ARABIC, jSONArray);
        }
        if (this.a.c == null) {
            return true;
        }
        jSONObject.put("kv", new JSONObject(this.a.c));
        return true;
    }
}
