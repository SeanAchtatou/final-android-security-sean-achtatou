package com.tencent.qc.stat.event;

import com.utils.FinalVariable;

/* compiled from: ProGuard */
public enum EventType {
    PAGE_VIEW(1),
    SESSION_ENV(2),
    ERROR(3),
    CUSTOM(1000),
    ADDITION(FinalVariable.update);
    
    private int f;

    private EventType(int i) {
        this.f = i;
    }

    public int a() {
        return this.f;
    }
}
