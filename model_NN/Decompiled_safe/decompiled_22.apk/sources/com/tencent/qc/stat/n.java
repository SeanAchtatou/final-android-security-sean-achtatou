package com.tencent.qc.stat;

import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.protocol.HttpContext;

/* compiled from: ProGuard */
class n extends DefaultConnectionKeepAliveStrategy {
    final /* synthetic */ l a;

    n(l lVar) {
        this.a = lVar;
    }

    public long getKeepAliveDuration(HttpResponse httpResponse, HttpContext httpContext) {
        long keepAliveDuration = n.super.getKeepAliveDuration(httpResponse, httpContext);
        if (keepAliveDuration == -1) {
            return 20000;
        }
        return keepAliveDuration;
    }
}
