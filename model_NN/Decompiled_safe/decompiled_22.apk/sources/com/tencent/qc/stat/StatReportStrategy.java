package com.tencent.qc.stat;

/* compiled from: ProGuard */
public enum StatReportStrategy {
    INSTANT(1),
    ONLY_WIFI(2),
    BATCH(3),
    APP_LAUNCH(4),
    DEVELOPER(5),
    PERIOD(6);
    
    int g;

    public static StatReportStrategy[] a() {
        return (StatReportStrategy[]) h.clone();
    }

    private StatReportStrategy(int i) {
        this.g = i;
    }

    public int b() {
        return this.g;
    }

    public static StatReportStrategy a(int i) {
        for (StatReportStrategy statReportStrategy : a()) {
            if (i == statReportStrategy.b()) {
                return statReportStrategy;
            }
        }
        return null;
    }
}
