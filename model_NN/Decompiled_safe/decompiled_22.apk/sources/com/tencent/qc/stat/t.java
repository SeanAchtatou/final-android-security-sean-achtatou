package com.tencent.qc.stat;

import android.content.Context;
import com.tencent.mm.sdk.platformtools.Util;
import com.tencent.qc.stat.common.StatCommonHelper;
import com.tencent.qc.stat.common.StatPreferences;
import com.tencent.qc.stat.event.Event;

/* compiled from: ProGuard */
class t implements Runnable {
    /* access modifiers changed from: private */
    public Event a;
    private StatReportStrategy b = null;

    public t(Event event) {
        this.a = event;
        this.b = StatConfig.a();
    }

    private void a() {
        l.b().a(this.a, new a(this));
    }

    public void run() {
        if (StatConfig.c()) {
            StatService.f.b("Lauch stat task in thread:" + Thread.currentThread().getName());
            Context c = this.a.c();
            if (!StatCommonHelper.h(c)) {
                StatStore.a(c).b(this.a, (q) null);
                return;
            }
            if (StatConfig.m() && StatCommonHelper.g(c)) {
                this.b = StatReportStrategy.INSTANT;
            }
            switch (p.a[this.b.ordinal()]) {
                case 1:
                    a();
                    return;
                case 2:
                    if (StatCommonHelper.e(c)) {
                        a();
                        return;
                    } else {
                        StatStore.a(c).b(this.a, (q) null);
                        return;
                    }
                case 3:
                case 4:
                    StatStore.a(c).b(this.a, (q) null);
                    return;
                case 5:
                    StatStore.a(c).b(this.a, new k(this, c));
                    return;
                case 6:
                    try {
                        StatStore.a(c).b(this.a, (q) null);
                        Long valueOf = Long.valueOf(StatPreferences.a(c, "last_period_ts", 0));
                        Long valueOf2 = Long.valueOf(System.currentTimeMillis());
                        if (Long.valueOf(Long.valueOf(valueOf2.longValue() - valueOf.longValue()).longValue() / Util.MILLSECONDS_OF_MINUTE).longValue() > ((long) StatConfig.l())) {
                            StatStore.a(c).a(-1);
                            StatPreferences.b(c, "last_period_ts", valueOf2.longValue());
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        StatService.f.b(e);
                        return;
                    }
                default:
                    StatService.f.e("Invalid stat strategy:" + StatConfig.a());
                    return;
            }
        }
    }
}
