package com.tencent.qc.stat.common;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.actionbarsherlock.widget.ActivityChooserView;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.mm.sdk.platformtools.Util;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.http.HttpHost;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class StatCommonHelper {
    private static String a = null;
    private static String b = null;
    private static String c = null;
    private static String d = null;
    private static Random e = null;
    private static StatLogger f = null;

    private static Random e() {
        if (e == null) {
            e = new Random();
        }
        return e;
    }

    public static int a() {
        return e().nextInt(ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
    }

    public static byte[] a(byte[] bArr) {
        GZIPInputStream gZIPInputStream = new GZIPInputStream(new ByteArrayInputStream(bArr));
        byte[] bArr2 = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length * 2);
        while (true) {
            int read = gZIPInputStream.read(bArr2);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr2, 0, read);
        }
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & 255;
                if (b3 < 16) {
                    stringBuffer.append(UploadUtils.SUCCESS);
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e2) {
            f.b((Exception) e2);
            return UploadUtils.SUCCESS;
        }
    }

    public static HttpHost a(Context context) {
        if (context == null) {
            return null;
        }
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
            return null;
        }
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return null;
            }
            if (activeNetworkInfo.getTypeName() != null && activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) {
                return null;
            }
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (extraInfo == null) {
                return null;
            }
            if (extraInfo.equals("cmwap") || extraInfo.equals("3gwap") || extraInfo.equals("uniwap")) {
                return new HttpHost("10.0.0.172", 80);
            }
            if (extraInfo.equals("ctwap")) {
                return new HttpHost("10.0.0.200", 80);
            }
            return null;
        } catch (Exception e2) {
            f.b(e2);
        }
    }

    public static String b(Context context) {
        if (a != null) {
            return a;
        }
        a = k(context);
        if (a == null) {
            a = Integer.toString(e().nextInt(ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED));
        }
        return a;
    }

    public static String c(Context context) {
        if (c == null || "" == c) {
            c = f(context);
        }
        return c;
    }

    public static DisplayMetrics d(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getApplicationContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static boolean a(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    public static boolean e(Context context) {
        NetworkInfo[] allNetworkInfo;
        if (a(context, "android.permission.ACCESS_WIFI_STATE")) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService("connectivity");
            if (connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null) {
                return false;
            }
            for (int i = 0; i < allNetworkInfo.length; i++) {
                if (allNetworkInfo[i].getTypeName().equalsIgnoreCase("WIFI") && allNetworkInfo[i].isConnected()) {
                    return true;
                }
            }
            return false;
        }
        f.c("can not get the permission of android.permission.ACCESS_WIFI_STATE");
        return false;
    }

    public static String f(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
            if (wifiManager == null) {
                return "";
            }
            return wifiManager.getConnectionInfo().getMacAddress();
        } catch (Exception e2) {
            f.b(e2);
            return "";
        }
    }

    public static boolean g(Context context) {
        if (a(context, "android.permission.INTERNET")) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable() || !activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) {
                return false;
            }
            return true;
        }
        f.c("can not get the permisson of android.permission.INTERNET");
        return false;
    }

    public static boolean h(Context context) {
        if (a(context, "android.permission.INTERNET")) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
                return true;
            }
            f.d("Network error");
            return false;
        }
        f.c("can not get the permisson of android.permission.INTERNET");
        return false;
    }

    public static String i(Context context) {
        if (b != null) {
            return b;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                String string = applicationInfo.metaData.getString("TA_APPKEY");
                if (string != null) {
                    b = string;
                    return string;
                }
                f.f("Could not read APPKEY meta-data from AndroidManifest.xml");
            }
        } catch (Exception e2) {
            f.f("Could not read APPKEY meta-data from AndroidManifest.xml");
        }
        return null;
    }

    public static String j(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                Object obj = applicationInfo.metaData.get("InstallChannel");
                if (obj != null) {
                    return obj.toString();
                }
                f.f("Could not read InstallChannel meta-data from AndroidManifest.xml");
            }
        } catch (Exception e2) {
            f.f("Could not read InstallChannel meta-data from AndroidManifest.xml");
        }
        return null;
    }

    public static String k(Context context) {
        if (a(context, "android.permission.READ_PHONE_STATE")) {
            String str = "";
            if (m(context)) {
                str = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            }
            if (str != null) {
                return str;
            }
            f.e("deviceId is null");
            return null;
        }
        f.f("Could not get permission of android.permission.READ_PHONE_STATE");
        return "";
    }

    public static String l(Context context) {
        try {
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (str == null) {
                return "";
            }
            return str;
        } catch (Exception e2) {
            Exception exc = e2;
            String str2 = "";
            f.b(exc);
            return str2;
        }
    }

    public static boolean m(Context context) {
        if (context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) != 0) {
            return false;
        }
        return true;
    }

    public static String n(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            String typeName = activeNetworkInfo.getTypeName();
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (typeName != null) {
                if (typeName.equalsIgnoreCase("WIFI")) {
                    return "WIFI";
                }
                if (typeName.equalsIgnoreCase("MOBILE")) {
                    if (extraInfo == null) {
                        return "MOBILE";
                    }
                    return extraInfo;
                } else if (extraInfo == null) {
                    return typeName;
                } else {
                    return extraInfo;
                }
            }
        }
        return null;
    }

    public static Integer o(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            return Integer.valueOf(telephonyManager.getNetworkType());
        }
        return null;
    }

    public static String p(Context context) {
        Exception e2;
        String str;
        try {
            str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (str != null) {
                try {
                    if (str.length() == 0) {
                        return "unknown";
                    }
                    return str;
                } catch (Exception e3) {
                    e2 = e3;
                    f.b(e2);
                    return str;
                }
            }
            return "unknown";
        } catch (Exception e4) {
            Exception exc = e4;
            str = "";
            e2 = exc;
            f.b(e2);
            return str;
        }
    }

    public static void a(JSONObject jSONObject, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            jSONObject.put(str, str2);
        }
    }

    public static int q(Context context) {
        if (a.a()) {
            return 1;
        }
        return 0;
    }

    public static StatLogger b() {
        if (f == null) {
            f = new StatLogger("qc_MtaSDK");
            f.a(false);
        }
        return f;
    }

    public static long c() {
        Calendar instance = Calendar.getInstance();
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return Util.MILLSECONDS_OF_DAY + instance.getTimeInMillis();
    }

    public static Long a(String str, String str2, int i, int i2, Long l) {
        if (str == null || str2 == null) {
            return l;
        }
        if (str2.equalsIgnoreCase(".") || str2.equalsIgnoreCase("|")) {
            str2 = "\\" + str2;
        }
        String[] split = str.split(str2);
        if (split.length != i2) {
            return l;
        }
        try {
            Long l2 = 0L;
            int length = split.length - 1;
            for (int i3 = 0; i3 < length; i3++) {
                l2 = Long.valueOf(l2.longValue() + (((long) i) * Long.valueOf(split[i3]).longValue()));
            }
            return Long.valueOf(l2.longValue() + Long.valueOf(split[length]).longValue());
        } catch (NumberFormatException e2) {
            return l;
        }
    }

    public static long b(String str) {
        return a(str, ".", 100, 3, 0L).longValue();
    }

    public static String d() {
        String path;
        if (!Environment.getExternalStorageState().equals("mounted") || (path = Environment.getExternalStorageDirectory().getPath()) == null) {
            return null;
        }
        StatFs statFs = new StatFs(path);
        return String.valueOf((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / 1000000) + CookieSpec.PATH_DELIM + String.valueOf((((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 1000000);
    }

    public static String a(long j) {
        return new SimpleDateFormat("yyyyMMdd").format(new Date(j));
    }
}
