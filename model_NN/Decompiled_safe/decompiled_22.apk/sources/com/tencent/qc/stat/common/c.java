package com.tencent.qc.stat.common;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import com.tencent.qc.stat.StatConfig;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONObject;

/* compiled from: ProGuard */
class c {
    String a;
    String b;
    DisplayMetrics c;
    int d;
    String e;
    String f;
    String g;
    String h;
    String i;
    String j;
    int k;

    private c(Context context) {
        this.b = "0.6.12";
        this.d = Build.VERSION.SDK_INT;
        this.e = Build.MODEL;
        this.f = Build.MANUFACTURER;
        this.g = Locale.getDefault().getLanguage();
        this.k = 0;
        this.c = StatCommonHelper.d(context);
        this.a = StatCommonHelper.l(context);
        this.h = StatConfig.b(context);
        this.i = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
        this.j = TimeZone.getDefault().getID();
        this.k = StatCommonHelper.q(context);
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        jSONObject.put("sr", this.c.widthPixels + "*" + this.c.heightPixels);
        StatCommonHelper.a(jSONObject, "av", this.a);
        StatCommonHelper.a(jSONObject, "ch", this.h);
        StatCommonHelper.a(jSONObject, "mf", this.f);
        StatCommonHelper.a(jSONObject, "sv", this.b);
        StatCommonHelper.a(jSONObject, "ov", Integer.toString(this.d));
        jSONObject.put("os", 1);
        StatCommonHelper.a(jSONObject, "op", this.i);
        StatCommonHelper.a(jSONObject, "lg", this.g);
        StatCommonHelper.a(jSONObject, "md", this.e);
        StatCommonHelper.a(jSONObject, "tz", this.j);
        if (this.k != 0) {
            jSONObject.put("jb", this.k);
        }
        StatCommonHelper.a(jSONObject, "sd", StatCommonHelper.d());
    }
}
