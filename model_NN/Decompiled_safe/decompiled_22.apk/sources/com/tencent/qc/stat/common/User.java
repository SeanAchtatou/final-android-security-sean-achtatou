package com.tencent.qc.stat.common;

/* compiled from: ProGuard */
public class User {
    private String a = null;
    private String b = null;
    private int c;

    public User(String str, String str2, int i) {
        this.a = str;
        this.b = str2;
        this.c = i;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public void a(int i) {
        this.c = i;
    }

    public int c() {
        return this.c;
    }
}
