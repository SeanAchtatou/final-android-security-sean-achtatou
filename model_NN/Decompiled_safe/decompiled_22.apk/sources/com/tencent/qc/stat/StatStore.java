package com.tencent.qc.stat;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.os.HandlerThread;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.qc.stat.common.StatCommonHelper;
import com.tencent.qc.stat.common.StatLogger;
import com.tencent.qc.stat.common.User;
import com.tencent.qc.stat.event.Event;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class StatStore {
    /* access modifiers changed from: private */
    public static StatLogger e = StatCommonHelper.b();
    private static StatStore f = null;
    Handler a = null;
    volatile int b = 0;
    User c = null;
    /* access modifiers changed from: private */
    public i d;
    private HashMap g = new HashMap();

    public int a() {
        return this.b;
    }

    private StatStore(Context context) {
        HandlerThread handlerThread = new HandlerThread("StatStore");
        handlerThread.start();
        e.d("Launch store thread:" + handlerThread);
        this.a = new Handler(handlerThread.getLooper());
        Context applicationContext = context.getApplicationContext();
        this.d = new i(applicationContext);
        this.d.getWritableDatabase();
        this.d.getReadableDatabase();
        b(applicationContext);
        e();
        this.a.post(new g(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void d() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", (Integer) 1);
        this.d.getWritableDatabase().update("events", contentValues, "status=?", new String[]{Long.toString(2)});
        this.b = (int) DatabaseUtils.queryNumEntries(this.d.getReadableDatabase(), "events");
        e.b("Total " + this.b + " unsent events.");
    }

    public static StatStore a(Context context) {
        if (f == null) {
            f = new StatStore(context);
        }
        return f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x017c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.qc.stat.common.User b(android.content.Context r13) {
        /*
            r12 = this;
            com.tencent.qc.stat.common.User r0 = r12.c
            if (r0 == 0) goto L_0x0007
            com.tencent.qc.stat.common.User r0 = r12.c
        L_0x0006:
            return r0
        L_0x0007:
            r9 = 0
            com.tencent.qc.stat.i r0 = r12.d     // Catch:{ SQLiteException -> 0x016b, all -> 0x0178 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x016b, all -> 0x0178 }
            java.lang.String r1 = "user"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x016b, all -> 0x0178 }
            r0 = 0
            boolean r2 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0182 }
            if (r2 == 0) goto L_0x00cb
            r0 = 0
            java.lang.String r4 = r1.getString(r0)     // Catch:{ SQLiteException -> 0x0182 }
            r0 = 1
            int r7 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x0182 }
            r0 = 2
            java.lang.String r3 = r1.getString(r0)     // Catch:{ SQLiteException -> 0x0182 }
            r0 = 3
            long r5 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x0182 }
            r0 = 1
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x0182 }
            r10 = 1000(0x3e8, double:4.94E-321)
            long r8 = r8 / r10
            r2 = 1
            if (r7 == r2) goto L_0x018a
            r10 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 * r10
            java.lang.String r2 = com.tencent.qc.stat.common.StatCommonHelper.a(r5)     // Catch:{ SQLiteException -> 0x0182 }
            r5 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 * r8
            java.lang.String r5 = com.tencent.qc.stat.common.StatCommonHelper.a(r5)     // Catch:{ SQLiteException -> 0x0182 }
            boolean r2 = r2.equals(r5)     // Catch:{ SQLiteException -> 0x0182 }
            if (r2 != 0) goto L_0x018a
            r2 = 1
        L_0x0056:
            java.lang.String r5 = com.tencent.qc.stat.common.StatCommonHelper.p(r13)     // Catch:{ SQLiteException -> 0x0182 }
            boolean r3 = r3.equals(r5)     // Catch:{ SQLiteException -> 0x0182 }
            if (r3 != 0) goto L_0x0187
            r2 = r2 | 2
            r6 = r2
        L_0x0063:
            java.lang.String r2 = ","
            java.lang.String[] r10 = r4.split(r2)     // Catch:{ SQLiteException -> 0x0182 }
            r3 = 0
            if (r10 == 0) goto L_0x013d
            int r2 = r10.length     // Catch:{ SQLiteException -> 0x0182 }
            if (r2 <= 0) goto L_0x013d
            r2 = 0
            r2 = r10[r2]     // Catch:{ SQLiteException -> 0x0182 }
            r5 = r2
        L_0x0073:
            if (r10 == 0) goto L_0x0145
            int r2 = r10.length     // Catch:{ SQLiteException -> 0x0182 }
            r11 = 2
            if (r2 < r11) goto L_0x0145
            r2 = 1
            r2 = r10[r2]     // Catch:{ SQLiteException -> 0x0182 }
        L_0x007c:
            com.tencent.qc.stat.common.User r10 = new com.tencent.qc.stat.common.User     // Catch:{ SQLiteException -> 0x0182 }
            r10.<init>(r5, r2, r6)     // Catch:{ SQLiteException -> 0x0182 }
            r12.c = r10     // Catch:{ SQLiteException -> 0x0182 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x0182 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r10 = "uid"
            r2.put(r10, r4)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r4 = "user_type"
            java.lang.Integer r10 = java.lang.Integer.valueOf(r6)     // Catch:{ SQLiteException -> 0x0182 }
            r2.put(r4, r10)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r4 = "app_ver"
            java.lang.String r10 = com.tencent.qc.stat.common.StatCommonHelper.p(r13)     // Catch:{ SQLiteException -> 0x0182 }
            r2.put(r4, r10)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r4 = "ts"
            java.lang.Long r8 = java.lang.Long.valueOf(r8)     // Catch:{ SQLiteException -> 0x0182 }
            r2.put(r4, r8)     // Catch:{ SQLiteException -> 0x0182 }
            if (r3 == 0) goto L_0x00bd
            com.tencent.qc.stat.i r3 = r12.d     // Catch:{ SQLiteException -> 0x0182 }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r4 = "user"
            java.lang.String r8 = "uid=?"
            r9 = 1
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ SQLiteException -> 0x0182 }
            r10 = 0
            r9[r10] = r5     // Catch:{ SQLiteException -> 0x0182 }
            r3.update(r4, r2, r8, r9)     // Catch:{ SQLiteException -> 0x0182 }
        L_0x00bd:
            if (r6 == r7) goto L_0x00cb
            com.tencent.qc.stat.i r3 = r12.d     // Catch:{ SQLiteException -> 0x0182 }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r4 = "user"
            r5 = 0
            r3.replace(r4, r5, r2)     // Catch:{ SQLiteException -> 0x0182 }
        L_0x00cb:
            if (r0 != 0) goto L_0x0134
            java.lang.String r2 = com.tencent.qc.stat.common.StatCommonHelper.b(r13)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r3 = com.tencent.qc.stat.common.StatCommonHelper.c(r13)     // Catch:{ SQLiteException -> 0x0182 }
            if (r3 == 0) goto L_0x0184
            int r0 = r3.length()     // Catch:{ SQLiteException -> 0x0182 }
            if (r0 <= 0) goto L_0x0184
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0182 }
            r0.<init>()     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r4 = ","
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r0 = r0.toString()     // Catch:{ SQLiteException -> 0x0182 }
        L_0x00f4:
            r4 = 0
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x0182 }
            r7 = 1000(0x3e8, double:4.94E-321)
            long r5 = r5 / r7
            java.lang.String r7 = com.tencent.qc.stat.common.StatCommonHelper.p(r13)     // Catch:{ SQLiteException -> 0x0182 }
            android.content.ContentValues r8 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x0182 }
            r8.<init>()     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r9 = "uid"
            r8.put(r9, r0)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r0 = "user_type"
            java.lang.Integer r9 = java.lang.Integer.valueOf(r4)     // Catch:{ SQLiteException -> 0x0182 }
            r8.put(r0, r9)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r0 = "app_ver"
            r8.put(r0, r7)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r0 = "ts"
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ SQLiteException -> 0x0182 }
            r8.put(r0, r5)     // Catch:{ SQLiteException -> 0x0182 }
            com.tencent.qc.stat.i r0 = r12.d     // Catch:{ SQLiteException -> 0x0182 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r5 = "user"
            r6 = 0
            r0.insert(r5, r6, r8)     // Catch:{ SQLiteException -> 0x0182 }
            com.tencent.qc.stat.common.User r0 = new com.tencent.qc.stat.common.User     // Catch:{ SQLiteException -> 0x0182 }
            r0.<init>(r2, r3, r4)     // Catch:{ SQLiteException -> 0x0182 }
            r12.c = r0     // Catch:{ SQLiteException -> 0x0182 }
        L_0x0134:
            if (r1 == 0) goto L_0x0139
            r1.close()
        L_0x0139:
            com.tencent.qc.stat.common.User r0 = r12.c
            goto L_0x0006
        L_0x013d:
            java.lang.String r2 = com.tencent.qc.stat.common.StatCommonHelper.b(r13)     // Catch:{ SQLiteException -> 0x0182 }
            r5 = r2
            r4 = r2
            goto L_0x0073
        L_0x0145:
            java.lang.String r2 = com.tencent.qc.stat.common.StatCommonHelper.c(r13)     // Catch:{ SQLiteException -> 0x0182 }
            if (r2 == 0) goto L_0x007c
            int r10 = r2.length()     // Catch:{ SQLiteException -> 0x0182 }
            if (r10 <= 0) goto L_0x007c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0182 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r4 = ","
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ SQLiteException -> 0x0182 }
            java.lang.String r4 = r3.toString()     // Catch:{ SQLiteException -> 0x0182 }
            r3 = 1
            goto L_0x007c
        L_0x016b:
            r0 = move-exception
            r1 = r9
        L_0x016d:
            com.tencent.qc.stat.common.StatLogger r2 = com.tencent.qc.stat.StatStore.e     // Catch:{ all -> 0x0180 }
            r2.b(r0)     // Catch:{ all -> 0x0180 }
            if (r1 == 0) goto L_0x0139
            r1.close()
            goto L_0x0139
        L_0x0178:
            r0 = move-exception
            r1 = r9
        L_0x017a:
            if (r1 == 0) goto L_0x017f
            r1.close()
        L_0x017f:
            throw r0
        L_0x0180:
            r0 = move-exception
            goto L_0x017a
        L_0x0182:
            r0 = move-exception
            goto L_0x016d
        L_0x0184:
            r0 = r2
            goto L_0x00f4
        L_0x0187:
            r6 = r2
            goto L_0x0063
        L_0x018a:
            r2 = r7
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qc.stat.StatStore.b(android.content.Context):com.tencent.qc.stat.common.User");
    }

    /* access modifiers changed from: private */
    public void b(List list) {
        e.b("Delete " + list.size() + " sent events in thread:" + Thread.currentThread());
        try {
            this.d.getWritableDatabase().beginTransaction();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                this.b -= this.d.getWritableDatabase().delete("events", "event_id = ?", new String[]{Long.toString(((s) it.next()).a)});
            }
            this.d.getWritableDatabase().setTransactionSuccessful();
            this.b = (int) DatabaseUtils.queryNumEntries(this.d.getReadableDatabase(), "events");
        } catch (SQLiteException e2) {
            e.b((Exception) e2);
        } finally {
            this.d.getWritableDatabase().endTransaction();
        }
    }

    /* access modifiers changed from: private */
    public void b(List list, int i) {
        e.b("Update " + list.size() + " sending events to status:" + i + " in thread:" + Thread.currentThread());
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("status", Integer.toString(i));
            this.d.getWritableDatabase().beginTransaction();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                s sVar = (s) it.next();
                if (sVar.d + 1 > StatConfig.f()) {
                    this.b -= this.d.getWritableDatabase().delete("events", "event_id=?", new String[]{Long.toString(sVar.a)});
                } else {
                    contentValues.put("send_count", Integer.valueOf(sVar.d + 1));
                    e.b("Update event:" + sVar.a + " for content:" + contentValues);
                    int update = this.d.getWritableDatabase().update("events", contentValues, "event_id=?", new String[]{Long.toString(sVar.a)});
                    if (update <= 0) {
                        e.f("Failed to update db, error code:" + Integer.toString(update));
                    }
                }
            }
            this.d.getWritableDatabase().setTransactionSuccessful();
            this.b = (int) DatabaseUtils.queryNumEntries(this.d.getReadableDatabase(), "events");
        } catch (SQLiteException e2) {
            e.b((Exception) e2);
        } finally {
            this.d.getWritableDatabase().endTransaction();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List list, int i) {
        if (Thread.currentThread().getId() == this.a.getLooper().getThread().getId()) {
            b(list, i);
        } else {
            this.a.post(new h(this, list, i));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List list) {
        if (Thread.currentThread().getId() == this.a.getLooper().getThread().getId()) {
            b(list);
        } else {
            this.a.post(new e(this, list));
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(java.util.List r11, int r12) {
        /*
            r10 = this;
            r9 = 0
            com.tencent.qc.stat.i r0 = r10.d     // Catch:{ SQLiteException -> 0x0066, all -> 0x0059 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0066, all -> 0x0059 }
            java.lang.String r1 = "events"
            r2 = 0
            java.lang.String r3 = "status=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0066, all -> 0x0059 }
            r5 = 0
            r6 = 1
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ SQLiteException -> 0x0066, all -> 0x0059 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0066, all -> 0x0059 }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "event_id"
            java.lang.String r8 = java.lang.Integer.toString(r12)     // Catch:{ SQLiteException -> 0x0066, all -> 0x0059 }
            android.database.Cursor r6 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0066, all -> 0x0059 }
        L_0x0023:
            boolean r0 = r6.moveToNext()     // Catch:{ SQLiteException -> 0x0046, all -> 0x0060 }
            if (r0 == 0) goto L_0x0053
            r0 = 0
            long r1 = r6.getLong(r0)     // Catch:{ SQLiteException -> 0x0046, all -> 0x0060 }
            r0 = 1
            java.lang.String r3 = r6.getString(r0)     // Catch:{ SQLiteException -> 0x0046, all -> 0x0060 }
            r0 = 2
            int r4 = r6.getInt(r0)     // Catch:{ SQLiteException -> 0x0046, all -> 0x0060 }
            r0 = 3
            int r5 = r6.getInt(r0)     // Catch:{ SQLiteException -> 0x0046, all -> 0x0060 }
            com.tencent.qc.stat.s r0 = new com.tencent.qc.stat.s     // Catch:{ SQLiteException -> 0x0046, all -> 0x0060 }
            r0.<init>(r1, r3, r4, r5)     // Catch:{ SQLiteException -> 0x0046, all -> 0x0060 }
            r11.add(r0)     // Catch:{ SQLiteException -> 0x0046, all -> 0x0060 }
            goto L_0x0023
        L_0x0046:
            r0 = move-exception
            r1 = r6
        L_0x0048:
            com.tencent.qc.stat.common.StatLogger r2 = com.tencent.qc.stat.StatStore.e     // Catch:{ all -> 0x0063 }
            r2.b(r0)     // Catch:{ all -> 0x0063 }
            if (r1 == 0) goto L_0x0052
            r1.close()
        L_0x0052:
            return
        L_0x0053:
            if (r6 == 0) goto L_0x0052
            r6.close()
            goto L_0x0052
        L_0x0059:
            r0 = move-exception
        L_0x005a:
            if (r9 == 0) goto L_0x005f
            r9.close()
        L_0x005f:
            throw r0
        L_0x0060:
            r0 = move-exception
            r9 = r6
            goto L_0x005a
        L_0x0063:
            r0 = move-exception
            r9 = r1
            goto L_0x005a
        L_0x0066:
            r0 = move-exception
            r1 = r9
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qc.stat.StatStore.c(java.util.List, int):void");
    }

    /* access modifiers changed from: package-private */
    public void a(Event event, q qVar) {
        if (StatConfig.h() > 0) {
            if (this.b > StatConfig.h()) {
                e.c("Too many events stored in db.");
                this.b -= this.d.getWritableDatabase().delete("events", "event_id in (select event_id from events where timestamp in (select min(timestamp) from events) limit 1)", null);
            }
            ContentValues contentValues = new ContentValues();
            String d2 = event.d();
            contentValues.put("content", d2);
            contentValues.put("send_count", UploadUtils.SUCCESS);
            contentValues.put("status", Integer.toString(1));
            contentValues.put("timestamp", Long.valueOf(event.b()));
            if (this.d.getWritableDatabase().insert("events", null, contentValues) == -1) {
                e.e("Failed to store event:" + d2);
                return;
            }
            this.b++;
            if (qVar != null) {
                qVar.a();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Event event, q qVar) {
        this.a.post(new f(this, event, qVar));
    }

    /* access modifiers changed from: package-private */
    public void a(r rVar) {
        if (rVar != null) {
            this.a.post(new c(this, rVar));
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.a.post(new d(this));
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.a.post(new b(this, i));
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void e() {
        /*
            r9 = this;
            r8 = 0
            com.tencent.qc.stat.i r0 = r9.d     // Catch:{ SQLiteException -> 0x0045, all -> 0x003b }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0045, all -> 0x003b }
            java.lang.String r1 = "keyvalues"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0045, all -> 0x003b }
        L_0x0013:
            boolean r0 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0029 }
            if (r0 == 0) goto L_0x0035
            java.util.HashMap r0 = r9.g     // Catch:{ SQLiteException -> 0x0029 }
            r2 = 0
            java.lang.String r2 = r1.getString(r2)     // Catch:{ SQLiteException -> 0x0029 }
            r3 = 1
            java.lang.String r3 = r1.getString(r3)     // Catch:{ SQLiteException -> 0x0029 }
            r0.put(r2, r3)     // Catch:{ SQLiteException -> 0x0029 }
            goto L_0x0013
        L_0x0029:
            r0 = move-exception
        L_0x002a:
            com.tencent.qc.stat.common.StatLogger r2 = com.tencent.qc.stat.StatStore.e     // Catch:{ all -> 0x0043 }
            r2.b(r0)     // Catch:{ all -> 0x0043 }
            if (r1 == 0) goto L_0x0034
            r1.close()
        L_0x0034:
            return
        L_0x0035:
            if (r1 == 0) goto L_0x0034
            r1.close()
            goto L_0x0034
        L_0x003b:
            r0 = move-exception
            r1 = r8
        L_0x003d:
            if (r1 == 0) goto L_0x0042
            r1.close()
        L_0x0042:
            throw r0
        L_0x0043:
            r0 = move-exception
            goto L_0x003d
        L_0x0045:
            r0 = move-exception
            r1 = r8
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qc.stat.StatStore.e():void");
    }
}
