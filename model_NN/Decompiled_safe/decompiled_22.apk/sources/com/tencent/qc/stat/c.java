package com.tencent.qc.stat;

/* compiled from: ProGuard */
class c implements Runnable {
    final /* synthetic */ r a;
    final /* synthetic */ StatStore b;

    c(StatStore statStore, r rVar) {
        this.b = statStore;
        this.a = rVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ea  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r13 = this;
            r10 = 1
            r9 = 0
            r8 = 0
            com.tencent.qc.stat.r r0 = r13.a
            java.lang.String r11 = r0.a()
            int r0 = r11.length()
            if (r0 <= 0) goto L_0x00b3
            java.lang.String r0 = com.tencent.qc.stat.common.StatCommonHelper.a(r11)
            com.tencent.qc.stat.r r1 = r13.a
            java.lang.String r1 = r1.c
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x00b3
            android.content.ContentValues r12 = new android.content.ContentValues
            r12.<init>()
            java.lang.String r1 = "content"
            com.tencent.qc.stat.r r2 = r13.a
            org.json.JSONObject r2 = r2.b
            java.lang.String r2 = r2.toString()
            r12.put(r1, r2)
            java.lang.String r1 = "md5sum"
            r12.put(r1, r0)
            com.tencent.qc.stat.r r1 = r13.a
            r1.c = r0
            java.lang.String r0 = "version"
            com.tencent.qc.stat.r r1 = r13.a
            int r1 = r1.d
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r12.put(r0, r1)
            com.tencent.qc.stat.StatStore r0 = r13.b     // Catch:{ Exception -> 0x00b4, all -> 0x00c4 }
            com.tencent.qc.stat.i r0 = r0.d     // Catch:{ Exception -> 0x00b4, all -> 0x00c4 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x00b4, all -> 0x00c4 }
            java.lang.String r1 = "config"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00b4, all -> 0x00c4 }
        L_0x005b:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0107 }
            if (r0 == 0) goto L_0x010c
            r0 = 0
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x0107 }
            com.tencent.qc.stat.r r2 = r13.a     // Catch:{ Exception -> 0x0107 }
            int r2 = r2.a     // Catch:{ Exception -> 0x0107 }
            if (r0 != r2) goto L_0x005b
            r0 = r10
        L_0x006d:
            if (r1 == 0) goto L_0x0072
            r1.close()
        L_0x0072:
            if (r10 != r0) goto L_0x00cc
            com.tencent.qc.stat.StatStore r0 = r13.b
            com.tencent.qc.stat.i r0 = r0.d
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()
            java.lang.String r1 = "config"
            java.lang.String r2 = "type=?"
            java.lang.String[] r3 = new java.lang.String[r10]
            com.tencent.qc.stat.r r4 = r13.a
            int r4 = r4.a
            java.lang.String r4 = java.lang.Integer.toString(r4)
            r3[r9] = r4
            int r0 = r0.update(r1, r12, r2, r3)
            long r0 = (long) r0
        L_0x0093:
            r2 = -1
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x00ea
            com.tencent.qc.stat.common.StatLogger r0 = com.tencent.qc.stat.StatStore.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Failed to store cfg:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.e(r1)
        L_0x00b3:
            return
        L_0x00b4:
            r0 = move-exception
            r1 = r8
        L_0x00b6:
            com.tencent.qc.stat.common.StatLogger r2 = com.tencent.qc.stat.StatStore.e     // Catch:{ all -> 0x0105 }
            r2.b(r0)     // Catch:{ all -> 0x0105 }
            if (r1 == 0) goto L_0x0109
            r1.close()
            r0 = r9
            goto L_0x0072
        L_0x00c4:
            r0 = move-exception
            r1 = r8
        L_0x00c6:
            if (r1 == 0) goto L_0x00cb
            r1.close()
        L_0x00cb:
            throw r0
        L_0x00cc:
            java.lang.String r0 = "type"
            com.tencent.qc.stat.r r1 = r13.a
            int r1 = r1.a
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r12.put(r0, r1)
            com.tencent.qc.stat.StatStore r0 = r13.b
            com.tencent.qc.stat.i r0 = r0.d
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()
            java.lang.String r1 = "config"
            long r0 = r0.insert(r1, r8, r12)
            goto L_0x0093
        L_0x00ea:
            com.tencent.qc.stat.common.StatLogger r0 = com.tencent.qc.stat.StatStore.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Sucessed to store cfg:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.b(r1)
            goto L_0x00b3
        L_0x0105:
            r0 = move-exception
            goto L_0x00c6
        L_0x0107:
            r0 = move-exception
            goto L_0x00b6
        L_0x0109:
            r0 = r9
            goto L_0x0072
        L_0x010c:
            r0 = r9
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qc.stat.c.run():void");
    }
}
