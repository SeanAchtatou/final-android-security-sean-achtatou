package com.tencent.open;

import com.tencent.tauth.IUiListener;
import java.util.HashMap;

/* compiled from: ProGuard */
public class BrowserAuth {
    public static BrowserAuth a;
    static final /* synthetic */ boolean d;
    private static int e = 0;
    public HashMap b = new HashMap();
    public final String c = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /* compiled from: ProGuard */
    public class Auth {
        public IUiListener a;
        public TDialog b;
        public String c;
    }

    static {
        boolean z;
        if (!BrowserAuth.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        d = z;
    }

    public static BrowserAuth a() {
        if (a == null) {
            a = new BrowserAuth();
        }
        return a;
    }

    public Auth a(String str) {
        return (Auth) this.b.get(str);
    }

    public static int b() {
        int i = e + 1;
        e = i;
        return i;
    }

    public String a(Auth auth) {
        int b2 = b();
        try {
            this.b.put("" + b2, auth);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return "" + b2;
    }

    public void b(String str) {
        this.b.remove(str);
    }

    public String c() {
        int ceil = (int) Math.ceil((Math.random() * 20.0d) + 3.0d);
        char[] charArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        int length = charArray.length;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < ceil; i++) {
            stringBuffer.append(charArray[(int) (Math.random() * ((double) length))]);
        }
        return stringBuffer.toString();
    }

    public String a(String str, String str2) {
        return b(str, str2);
    }

    private String b(String str, String str2) {
        if (d || str.length() % 2 == 0) {
            StringBuilder sb = new StringBuilder();
            int length = str2.length();
            int length2 = str.length() / 2;
            int i = 0;
            for (int i2 = 0; i2 < length2; i2++) {
                sb.append((char) (Integer.parseInt(str.substring(i2 * 2, (i2 * 2) + 2), 16) ^ str2.charAt(i)));
                i = (i + 1) % length;
            }
            return sb.toString();
        }
        throw new AssertionError();
    }
}
