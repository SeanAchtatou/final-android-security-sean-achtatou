package com.tencent.open;

import java.util.HashMap;

/* compiled from: ProGuard */
public class TemporaryStorage {
    private static HashMap a = new HashMap();

    public static Object a(String str, Object obj) {
        return a.put(str, obj);
    }

    public static Object a(String str) {
        return a.remove(str);
    }
}
