package com.tencent.open;

import android.graphics.Bitmap;
import android.os.Message;
import android.util.Log;
import com.tencent.mm.sdk.platformtools.Util;
import java.io.File;

/* compiled from: ProGuard */
class k implements Runnable {
    final /* synthetic */ AsynLoadImg a;

    k(AsynLoadImg asynLoadImg) {
        this.a = asynLoadImg;
    }

    public void run() {
        boolean z;
        Log.v("AsynLoadImg", "saveFileRunnable:");
        String str = "share_qq_" + Util.f(this.a.c) + Util.PHOTO_DEFAULT_EXT;
        String str2 = AsynLoadImg.e + str;
        File file = new File(str2);
        Message obtainMessage = this.a.g.obtainMessage();
        if (file.exists()) {
            obtainMessage.arg1 = 0;
            obtainMessage.obj = str2;
            Log.v("AsynLoadImg", "file exists: time:" + (System.currentTimeMillis() - this.a.f));
        } else {
            Bitmap a2 = AsynLoadImg.a(this.a.c);
            if (a2 != null) {
                z = this.a.a(a2, str);
            } else {
                Log.v("AsynLoadImg", "saveFileRunnable:get bmp fail---");
                z = false;
            }
            if (z) {
                obtainMessage.arg1 = 0;
                obtainMessage.obj = str2;
            } else {
                obtainMessage.arg1 = 1;
            }
            Log.v("AsynLoadImg", "file not exists: download time:" + (System.currentTimeMillis() - this.a.f));
        }
        this.a.g.sendMessage(obtainMessage);
    }
}
