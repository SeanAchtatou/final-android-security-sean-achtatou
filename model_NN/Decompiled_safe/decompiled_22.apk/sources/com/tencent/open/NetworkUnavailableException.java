package com.tencent.open;

/* compiled from: ProGuard */
public class NetworkUnavailableException extends Exception {
    public NetworkUnavailableException(String str) {
        super(str);
    }
}
