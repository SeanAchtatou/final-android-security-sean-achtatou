package com.tencent.open;

import com.tencent.qc.stat.StatConfig;
import com.tencent.qc.stat.StatService;

/* compiled from: ProGuard */
public class TencentStat {
    public static boolean a(TContext tContext) {
        return OpenConfig.a(tContext.f(), tContext.d()).d("Common_ta_enable");
    }

    public static void b(TContext tContext) {
        if (a(tContext)) {
            StatConfig.a(true);
        } else {
            StatConfig.a(false);
        }
    }

    public static void a(TContext tContext, String str) {
        b(tContext);
        StatService.b(str);
    }

    public static void b(TContext tContext, String str) {
        b(tContext);
        if (str != null) {
            StatService.a(tContext.f(), str);
        }
    }

    public static void a(TContext tContext, String str, String... strArr) {
        b(tContext);
        StatService.a(tContext.f(), str, strArr);
    }
}
