package com.tencent.open;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.open.BrowserAuth;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.lang.ref.WeakReference;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class TDialog extends Dialog {
    static final FrameLayout.LayoutParams a = new FrameLayout.LayoutParams(-1, -1);
    static Toast b = null;
    /* access modifiers changed from: private */
    public static WeakReference c;
    /* access modifiers changed from: private */
    public static WeakReference d;
    private static WeakReference e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public OnTimeListener g;
    private IUiListener h;
    private FrameLayout i;
    /* access modifiers changed from: private */
    public WebView j;
    private FrameLayout k;
    private ProgressBar l;
    /* access modifiers changed from: private */
    public Handler m;
    /* access modifiers changed from: private */
    public boolean n = false;
    private TContext o = null;

    static /* synthetic */ String a(TDialog tDialog, Object obj) {
        String str = tDialog.f + obj;
        tDialog.f = str;
        return str;
    }

    /* compiled from: ProGuard */
    class THandler extends Handler {
        private OnTimeListener mL;

        public THandler(OnTimeListener onTimeListener, Looper looper) {
            super(looper);
            this.mL = onTimeListener;
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    this.mL.onComplete((String) message.obj);
                    return;
                case 2:
                    this.mL.onCancel();
                    return;
                case 3:
                    if (TDialog.c != null && TDialog.c.get() != null) {
                        TDialog.c((Context) TDialog.c.get(), (String) message.obj);
                        return;
                    }
                    return;
                case 4:
                    if (TDialog.d != null && TDialog.d.get() != null) {
                        ((View) TDialog.d.get()).setVisibility(8);
                        return;
                    }
                    return;
                case 5:
                    if (TDialog.c != null && TDialog.c.get() != null) {
                        TDialog.d((Context) TDialog.c.get(), (String) message.obj);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* compiled from: ProGuard */
    class OnTimeListener implements IUiListener {
        private IUiListener mWeakL;

        public OnTimeListener(IUiListener iUiListener) {
            this.mWeakL = iUiListener;
        }

        /* access modifiers changed from: private */
        public void onComplete(String str) {
            try {
                onComplete(Util.d(str));
            } catch (JSONException e) {
                e.printStackTrace();
                onError(new UiError(-4, Constants.MSG_JSON_ERROR, str));
            }
        }

        public void onComplete(JSONObject jSONObject) {
            if (this.mWeakL != null) {
                this.mWeakL.onComplete(jSONObject);
                this.mWeakL = null;
            }
        }

        public void onError(UiError uiError) {
            if (this.mWeakL != null) {
                this.mWeakL.onError(uiError);
                this.mWeakL = null;
            }
        }

        public void onCancel() {
            if (this.mWeakL != null) {
                this.mWeakL.onCancel();
                this.mWeakL = null;
            }
        }
    }

    public TDialog(Context context, String str, IUiListener iUiListener, TContext tContext) {
        super(context, 16973840);
        c = new WeakReference(context);
        this.f = str;
        this.g = new OnTimeListener(iUiListener);
        this.m = new THandler(this.g, context.getMainLooper());
        this.h = iUiListener;
        this.o = tContext;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        c();
        d();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (!this.n) {
            this.g.onCancel();
        }
        super.onStop();
    }

    private void c() {
        this.l = new ProgressBar((Context) c.get());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        this.l.setLayoutParams(layoutParams);
        new TextView((Context) c.get()).setText("test");
        this.k = new FrameLayout((Context) c.get());
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, -2);
        layoutParams2.bottomMargin = 40;
        layoutParams2.leftMargin = 80;
        layoutParams2.rightMargin = 80;
        layoutParams2.topMargin = 40;
        layoutParams2.gravity = 17;
        this.k.setLayoutParams(layoutParams2);
        this.k.setBackgroundResource(17301504);
        this.k.addView(this.l);
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-1, -1);
        this.j = new WebView((Context) c.get());
        this.j.setLayoutParams(layoutParams3);
        this.i = new FrameLayout((Context) c.get());
        layoutParams3.gravity = 17;
        this.i.setLayoutParams(layoutParams3);
        this.i.addView(this.j);
        this.i.addView(this.k);
        d = new WeakReference(this.k);
        setContentView(this.i);
    }

    private void d() {
        this.j.setVerticalScrollBarEnabled(false);
        this.j.setHorizontalScrollBarEnabled(false);
        this.j.setWebViewClient(new FbWebViewClient());
        this.j.setWebChromeClient(new WebChromeClient());
        this.j.clearFormData();
        WebSettings settings = this.j.getSettings();
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setCacheMode(-1);
        settings.setNeedInitialFocus(false);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setJavaScriptEnabled(true);
        if (!(c == null || c.get() == null)) {
            settings.setDatabaseEnabled(true);
            settings.setDatabasePath(((Context) c.get()).getApplicationContext().getDir("databases", 0).getPath());
        }
        settings.setDomStorageEnabled(true);
        this.j.addJavascriptInterface(new JsListener(), "sdk_js_if");
        this.j.loadUrl(this.f);
        this.j.setLayoutParams(a);
        this.j.setVisibility(4);
        this.j.getSettings().setSavePassword(false);
    }

    public void a(String str, String str2) {
        this.j.loadUrl("javascript:" + str + "(" + str2 + ");void(" + System.currentTimeMillis() + ");");
    }

    /* compiled from: ProGuard */
    class JsListener {
        private JsListener() {
        }

        public void onAddShare(String str) {
            onComplete(str);
        }

        public void onInvite(String str) {
            onComplete(str);
        }

        public void onCancelAddShare(int i) {
            onCancel(null);
        }

        public void onCancelLogin() {
            onCancel(null);
        }

        public void onCancelInvite() {
            onCancel(null);
        }

        public void onComplete(String str) {
            TDialog.this.m.obtainMessage(1, str).sendToTarget();
            Log.e("onComplete", str);
            TDialog.this.dismiss();
        }

        public void onCancel(String str) {
            TDialog.this.m.obtainMessage(2, str).sendToTarget();
            TDialog.this.dismiss();
        }

        public void showMsg(String str) {
            TDialog.this.m.obtainMessage(3, str).sendToTarget();
        }

        public void onLoad(String str) {
            TDialog.this.m.obtainMessage(4, str).sendToTarget();
        }
    }

    /* compiled from: ProGuard */
    class FbWebViewClient extends WebViewClient {
        private FbWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Util.a("TDialog", "Redirect URL: " + str);
            if (str.startsWith("auth://browser")) {
                JSONObject c = Util.c(str);
                boolean unused = TDialog.this.n = TDialog.this.e();
                if (!TDialog.this.n) {
                    if (c.optString("fail_cb", null) != null) {
                        TDialog.this.a(c.optString("fail_cb"), "");
                    } else if (c.optInt("fall_to_wv") == 1) {
                        TDialog.a(TDialog.this, TDialog.this.f.indexOf("?") > -1 ? "&" : "?");
                        TDialog.a(TDialog.this, "browser_error=1");
                        TDialog.this.j.loadUrl(TDialog.this.f);
                    } else {
                        String optString = c.optString("redir", null);
                        if (optString != null) {
                            TDialog.this.j.loadUrl(optString);
                        }
                    }
                }
                return true;
            } else if (str.startsWith(ServerSetting.getInstance().getSettingUrl((Context) TDialog.c.get(), 1))) {
                TDialog.this.g.onComplete(Util.c(str));
                TDialog.this.dismiss();
                return true;
            } else if (str.startsWith("auth://cancel")) {
                TDialog.this.g.onCancel();
                TDialog.this.dismiss();
                return true;
            } else if (str.startsWith("auth://close")) {
                TDialog.this.dismiss();
                return true;
            } else if (!str.startsWith("download://")) {
                return false;
            } else {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(Uri.decode(str.substring("download://".length()))));
                if (!(TDialog.c == null || TDialog.c.get() == null)) {
                    ((Context) TDialog.c.get()).startActivity(intent);
                }
                return true;
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            TDialog.this.g.onError(new UiError(i, str, str2));
            if (!(TDialog.c == null || TDialog.c.get() == null)) {
                Toast.makeText((Context) TDialog.c.get(), "网络连接异常或系统错误", 0).show();
            }
            TDialog.this.dismiss();
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            Util.a("TDialog", "Webview loading URL: " + str);
            super.onPageStarted(webView, str, bitmap);
            if (TDialog.d != null && TDialog.d.get() != null) {
                ((View) TDialog.d.get()).setVisibility(0);
            }
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (!(TDialog.d == null || TDialog.d.get() == null)) {
                ((View) TDialog.d.get()).setVisibility(8);
            }
            TDialog.this.j.setVisibility(0);
        }

        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            sslErrorHandler.proceed();
        }
    }

    /* access modifiers changed from: private */
    public static void c(Context context, String str) {
        try {
            JSONObject d2 = Util.d(str);
            int i2 = d2.getInt("type");
            String string = d2.getString(Constants.PARAM_SEND_MSG);
            if (i2 == 0) {
                if (b == null) {
                    b = Toast.makeText(context, string, 0);
                } else {
                    b.setView(b.getView());
                    b.setText(string);
                    b.setDuration(0);
                }
                b.show();
            } else if (i2 == 1) {
                if (b == null) {
                    b = Toast.makeText(context, string, 1);
                } else {
                    b.setView(b.getView());
                    b.setText(string);
                    b.setDuration(1);
                }
                b.show();
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public static void d(Context context, String str) {
        if (context != null && str != null) {
            try {
                JSONObject d2 = Util.d(str);
                int i2 = d2.getInt("action");
                String string = d2.getString(Constants.PARAM_SEND_MSG);
                if (i2 == 1) {
                    if (e == null) {
                        ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage(string);
                        e = new WeakReference(progressDialog);
                        progressDialog.show();
                        return;
                    }
                    ((ProgressDialog) e.get()).setMessage(string);
                    if (!((ProgressDialog) e.get()).isShowing()) {
                        ((ProgressDialog) e.get()).show();
                    }
                } else if (i2 == 0 && e != null && e.get() != null && ((ProgressDialog) e.get()).isShowing()) {
                    ((ProgressDialog) e.get()).dismiss();
                    e = null;
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean e() {
        BrowserAuth a2 = BrowserAuth.a();
        String c2 = a2.c();
        BrowserAuth.Auth auth = new BrowserAuth.Auth();
        auth.a = this.h;
        auth.b = this;
        auth.c = c2;
        String a3 = a2.a(auth);
        String substring = this.f.substring(0, this.f.indexOf("?"));
        Bundle b2 = Util.b(this.f);
        b2.putString("token_key", c2);
        b2.putString("serial", a3);
        b2.putString("browser", UploadUtils.FAILURE);
        this.f = substring + "?" + Util.a(b2);
        if (c == null || c.get() == null) {
            return false;
        }
        return Util.a((Context) c.get(), this.f);
    }
}
