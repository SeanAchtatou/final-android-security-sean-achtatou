package com.tencent.open.cgireport;

import android.content.Context;
import android.os.Bundle;

/* compiled from: ProGuard */
class b extends Thread {
    final /* synthetic */ String a;
    final /* synthetic */ Context b;
    final /* synthetic */ Bundle c;
    final /* synthetic */ ReportManager d;

    b(ReportManager reportManager, String str, Context context, Bundle bundle) {
        this.d = reportManager;
        this.a = str;
        this.b = context;
        this.c = bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.open.cgireport.ReportManager.a(com.tencent.open.cgireport.ReportManager, boolean):boolean
     arg types: [com.tencent.open.cgireport.ReportManager, int]
     candidates:
      com.tencent.open.cgireport.ReportManager.a(com.tencent.open.cgireport.ReportManager, int):int
      com.tencent.open.cgireport.ReportManager.a(com.tencent.open.cgireport.ReportManager, boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x009f A[EDGE_INSN: B:33:0x009f->B:9:0x009f ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r8 = this;
            r5 = 0
            r3 = 1
            r1 = 0
            java.lang.String r0 = "cgi_report_debug"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "ReportManager doUploadItems Thread start, url = "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = r8.a
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
            com.tencent.open.cgireport.ReportManager r0 = r8.d
            android.content.Context r2 = r8.b
            com.tencent.open.OpenConfig r2 = com.tencent.open.OpenConfig.a(r2, r5)
            java.lang.String r4 = "Common_HttpRetryCount"
            int r2 = r2.b(r4)
            int unused = r0.c = r2
            com.tencent.open.cgireport.ReportManager r2 = r8.d
            com.tencent.open.cgireport.ReportManager r0 = r8.d
            int r0 = r0.c
            if (r0 != 0) goto L_0x00c8
            r0 = 3
        L_0x0039:
            int unused = r2.c = r0
            r0 = r1
            r2 = r1
        L_0x003e:
            int r2 = r2 + 1
            java.lang.String r4 = "cgi_report_debug"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "ReportManager doUploadItems Thread request count = "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r2)
            java.lang.String r5 = r5.toString()
            android.util.Log.i(r4, r5)
            android.content.Context r4 = r8.b     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            r5 = 0
            java.lang.String r6 = r8.a     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            org.apache.http.client.HttpClient r4 = com.tencent.open.Util.a(r4, r5, r6)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            org.apache.http.client.methods.HttpPost r5 = new org.apache.http.client.methods.HttpPost     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            java.lang.String r6 = r8.a     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            r5.<init>(r6)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            java.lang.String r6 = "Accept-Encoding"
            java.lang.String r7 = "gzip"
            r5.addHeader(r6, r7)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            java.lang.String r6 = "Content-Type"
            java.lang.String r7 = "application/x-www-form-urlencoded"
            r5.setHeader(r6, r7)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            android.os.Bundle r6 = r8.c     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            java.lang.String r6 = com.tencent.open.Util.a(r6)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            byte[] r6 = r6.getBytes()     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            org.apache.http.entity.ByteArrayEntity r7 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            r7.<init>(r6)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            r5.setEntity(r7)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            org.apache.http.HttpResponse r4 = r4.execute(r5)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            org.apache.http.StatusLine r4 = r4.getStatusLine()     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            int r4 = r4.getStatusCode()     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r4 == r5) goto L_0x00d0
            java.lang.String r4 = "cgi_report_debug"
            java.lang.String r5 = "ReportManager doUploadItems : HttpStatuscode != 200"
            android.util.Log.e(r4, r5)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00ed, Exception -> 0x00f2 }
        L_0x009f:
            com.tencent.open.cgireport.ReportManager r2 = r8.d
            boolean unused = r2.d = r1
            java.lang.String r1 = "cgi_report_debug"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "ReportManager doUploadItems Thread end, url = "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = r8.a
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r1, r2)
            if (r0 != r3) goto L_0x00fe
            java.lang.String r0 = "cgi_report_debug"
            java.lang.String r1 = "ReportManager doUploadItems Thread request success"
            android.util.Log.i(r0, r1)
        L_0x00c7:
            return
        L_0x00c8:
            com.tencent.open.cgireport.ReportManager r0 = r8.d
            int r0 = r0.c
            goto L_0x0039
        L_0x00d0:
            java.lang.String r0 = "cgi_report_debug"
            java.lang.String r4 = "ReportManager doUploadItems Thread success"
            android.util.Log.i(r0, r4)     // Catch:{ ConnectTimeoutException -> 0x011d, SocketTimeoutException -> 0x0119, Exception -> 0x0115 }
            r0 = r3
            goto L_0x009f
        L_0x00d9:
            r4 = move-exception
        L_0x00da:
            r4.printStackTrace()
            java.lang.String r4 = "cgi_report_debug"
            java.lang.String r5 = "ReportManager doUploadItems : ConnectTimeoutException"
            android.util.Log.e(r4, r5)
        L_0x00e4:
            com.tencent.open.cgireport.ReportManager r4 = r8.d
            int r4 = r4.c
            if (r2 < r4) goto L_0x003e
            goto L_0x009f
        L_0x00ed:
            r4 = move-exception
        L_0x00ee:
            r4.printStackTrace()
            goto L_0x00e4
        L_0x00f2:
            r2 = move-exception
        L_0x00f3:
            r2.printStackTrace()
            java.lang.String r2 = "cgi_report_debug"
            java.lang.String r4 = "ReportManager doUploadItems : Exception"
            android.util.Log.e(r2, r4)
            goto L_0x009f
        L_0x00fe:
            java.lang.String r0 = "cgi_report_debug"
            java.lang.String r1 = "ReportManager doUploadItems Thread request failed"
            android.util.Log.e(r0, r1)
            com.tencent.open.cgireport.ReportManager r0 = r8.d
            com.tencent.open.cgireport.ReportDataModal r0 = r0.f
            com.tencent.open.cgireport.ReportManager r1 = r8.d
            java.util.ArrayList r1 = r1.g
            r0.a(r1)
            goto L_0x00c7
        L_0x0115:
            r0 = move-exception
            r2 = r0
            r0 = r3
            goto L_0x00f3
        L_0x0119:
            r0 = move-exception
            r4 = r0
            r0 = r3
            goto L_0x00ee
        L_0x011d:
            r0 = move-exception
            r4 = r0
            r0 = r3
            goto L_0x00da
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.cgireport.b.run():void");
    }
}
