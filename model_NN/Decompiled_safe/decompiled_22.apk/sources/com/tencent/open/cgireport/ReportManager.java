package com.tencent.open.cgireport;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import com.tencent.open.OpenConfig;
import com.tencent.tauth.Constants;
import java.util.ArrayList;
import java.util.Random;

/* compiled from: ProGuard */
public class ReportManager {
    private static ReportManager a = null;
    private long b = 0;
    /* access modifiers changed from: private */
    public int c = 3;
    /* access modifiers changed from: private */
    public boolean d = false;
    private Random e = new Random();
    /* access modifiers changed from: private */
    public ReportDataModal f;
    /* access modifiers changed from: private */
    public ArrayList g = new ArrayList();
    private ArrayList h = new ArrayList();

    public static ReportManager a() {
        if (a == null) {
            a = new ReportManager();
        }
        return a;
    }

    public void a(Context context, String str, long j, long j2, long j3, int i, String str2) {
        if (this.f == null) {
            this.f = new ReportDataModal(context);
        }
        if (a(context)) {
            b(context, str, j, j2, j3, i, str2);
            if (!this.d) {
                if (c(context)) {
                    e(context);
                } else if (d(context)) {
                    e(context);
                }
            }
        }
    }

    private boolean a(Context context) {
        int b2 = OpenConfig.a(context, (String) null).b("Common_CGIReportFrequency");
        Log.d("OpenConfig_test", "config 4:Common_CGIReportTimeinterval     config_value:" + b2);
        if (b2 == 0) {
            b2 = 10;
        }
        Log.d("OpenConfig_test", "config 4:Common_CGIReportTimeinterval     result_value:" + b2);
        if (this.e.nextInt(100) < b2) {
            Log.i("cgi_report_debug", "ReportManager availableForFrequency = ture");
            return true;
        }
        Log.i("cgi_report_debug", "ReportManager availableForFrequency = false");
        return false;
    }

    private void b(Context context, String str, long j, long j2, long j3, int i, String str2) {
        int i2;
        long elapsedRealtime = SystemClock.elapsedRealtime() - j;
        Log.i("cgi_report_debug", "ReportManager updateDB url=" + str + ",resultCode=" + i + ",timeCost=" + elapsedRealtime + ",reqSize=" + j2 + ",rspSize=" + j3);
        int b2 = OpenConfig.a(context, (String) null).b("Common_CGIReportFrequency");
        Log.d("OpenConfig_test", "config 4:Common_CGIReportFrequency     config_value:" + b2);
        if (b2 == 0) {
            b2 = 10;
        }
        Log.d("OpenConfig_test", "config 4:Common_CGIReportFrequency     result_value:" + b2);
        int i3 = 100 / b2;
        if (i3 <= 0) {
            i2 = 1;
        } else {
            i2 = i3 > 100 ? 100 : i3;
        }
        this.f.a(b(context), i2 + "", str, i, elapsedRealtime, j2, j3);
    }

    private String b(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                Log.e("cgi_report_debug", "ReportManager getAPN failed:ConnectivityManager == null");
                return "no_net";
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
                Log.e("cgi_report_debug", "ReportManager getAPN failed:NetworkInfo == null");
                return "no_net";
            } else if (activeNetworkInfo.getTypeName().toUpperCase().equals("WIFI")) {
                Log.i("cgi_report_debug", "ReportManager getAPN type = wifi");
                return "wifi";
            } else {
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (extraInfo == null) {
                    Log.e("cgi_report_debug", "ReportManager getAPN failed:extraInfo == null");
                    return "mobile_unknow";
                }
                String lowerCase = extraInfo.toLowerCase();
                Log.i("cgi_report_debug", "ReportManager getAPN type = " + lowerCase);
                return lowerCase;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            return "unknow";
        }
    }

    private boolean c(Context context) {
        long c2 = OpenConfig.a(context, (String) null).c("Common_CGIReportTimeinterval");
        Log.d("OpenConfig_test", "config 5:Common_CGIReportTimeinterval     config_value:" + c2);
        if (c2 == 0) {
            c2 = 1200;
        }
        Log.d("OpenConfig_test", "config 5:Common_CGIReportTimeinterval     result_value:" + c2);
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        if (this.b == 0 || c2 + this.b <= currentTimeMillis) {
            this.b = currentTimeMillis;
            Log.i("cgi_report_debug", "ReportManager availableForTime = ture");
            return true;
        }
        Log.i("cgi_report_debug", "ReportManager availableForTime = false");
        return false;
    }

    private boolean d(Context context) {
        int b2 = OpenConfig.a(context, (String) null).b("Common_CGIReportMaxcount");
        Log.d("OpenConfig_test", "config 6:Common_CGIReportMaxcount     config_value:" + b2);
        if (b2 == 0) {
            b2 = 20;
        }
        Log.d("OpenConfig_test", "config 6:Common_CGIReportMaxcount     result_value:" + b2);
        if (this.f.e() >= b2) {
            Log.i("cgi_report_debug", "ReportManager availableForCount = ture");
            return true;
        }
        Log.i("cgi_report_debug", "ReportManager availableForCount = false");
        return false;
    }

    private void e(Context context) {
        Log.i("cgi_report_debug", "ReportManager doUpload start");
        this.d = true;
        this.g = this.f.c();
        this.f.b();
        this.h = this.f.d();
        this.f.a();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PARAM_APP_ID, "1000067");
        bundle.putString("releaseversion", "QQConnect_SDK_Android_1_6");
        bundle.putString("device", Build.DEVICE);
        bundle.putString("qua", Constants.SDK_QUA);
        bundle.putString("key", "apn,frequency,commandid,resultcode,tmcost,reqsize,rspsize");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.g.size()) {
                break;
            }
            bundle.putString(i2 + "_1", ((reportItem) this.g.get(i2)).a());
            bundle.putString(i2 + "_2", ((reportItem) this.g.get(i2)).b());
            bundle.putString(i2 + "_3", ((reportItem) this.g.get(i2)).c());
            bundle.putString(i2 + "_4", ((reportItem) this.g.get(i2)).d());
            bundle.putString(i2 + "_5", ((reportItem) this.g.get(i2)).e());
            bundle.putString(i2 + "_6", ((reportItem) this.g.get(i2)).f());
            bundle.putString(i2 + "_7", ((reportItem) this.g.get(i2)).g());
            i = i2 + 1;
        }
        int size = this.g.size();
        while (true) {
            int i3 = size;
            if (i3 < this.h.size() + this.g.size()) {
                int size2 = i3 - this.g.size();
                bundle.putString(i3 + "_1", ((reportItem) this.h.get(size2)).a());
                bundle.putString(i3 + "_2", ((reportItem) this.h.get(size2)).b());
                bundle.putString(i3 + "_3", ((reportItem) this.h.get(size2)).c());
                bundle.putString(i3 + "_4", ((reportItem) this.h.get(size2)).d());
                bundle.putString(i3 + "_5", ((reportItem) this.h.get(size2)).e());
                bundle.putString(i3 + "_6", ((reportItem) this.h.get(size2)).f());
                bundle.putString(i3 + "_7", ((reportItem) this.h.get(size2)).g());
                size = i3 + 1;
            } else {
                a(context, "http://wspeed.qq.com/w.cgi", "POST", bundle);
                return;
            }
        }
    }

    private void a(Context context, String str, String str2, Bundle bundle) {
        new b(this, str, context, bundle).start();
    }
}
