package com.tencent.open.cgireport;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class ReportDataModal {
    private SQLiteDatabase a;
    private a b;
    private int c = c().size();

    public ReportDataModal(Context context) {
        this.b = new a(this, context, "sdk_cgi_report.db", null, 1);
    }

    public synchronized boolean a(String str, String str2, String str3, int i, long j, long j2, long j3) {
        boolean z = false;
        synchronized (this) {
            if (str3.contains("?")) {
                str3 = str3.split("\\?")[0];
            }
            Log.i("cgi_report_debug", "ReportDataModal addNewItem apn=" + str + ",frequency=" + str2 + ",commandid=" + str3 + ",resultCode=" + i + ",costTime=" + j + ",reqSzie=" + j2 + ",rspSize=" + j3);
            ContentValues contentValues = new ContentValues();
            contentValues.put("apn", str + "");
            contentValues.put("frequency", str2 + "");
            contentValues.put("commandid", str3 + "");
            contentValues.put("resultcode", i + "");
            contentValues.put("tmcost", j + "");
            contentValues.put("reqsize", j2 + "");
            contentValues.put("rspsize", j3 + "");
            try {
                this.a = this.b.getWritableDatabase();
                this.a.insertOrThrow("newdata", null, contentValues);
                this.a.close();
                Log.i("cgi_report_debug", "ReportDataModal addNewItem success");
                this.c++;
                z = true;
            } catch (Exception e) {
                Log.e("cgi_report_debug", "ReportDataModal addNewItem failed");
                e.printStackTrace();
            }
        }
        return z;
    }

    public synchronized int a(ArrayList arrayList) {
        int i;
        Log.i("cgi_report_debug", "ReportDataModal backupOldItems count = " + arrayList.size());
        int i2 = 0;
        Iterator it = arrayList.iterator();
        while (true) {
            i = i2;
            if (it.hasNext()) {
                reportItem reportitem = (reportItem) it.next();
                ContentValues contentValues = new ContentValues();
                contentValues.put("apn", reportitem.a());
                contentValues.put("frequency", reportitem.b());
                contentValues.put("commandid", reportitem.c());
                contentValues.put("resultcode", reportitem.d());
                contentValues.put("tmcost", reportitem.e());
                contentValues.put("reqsize", reportitem.f());
                contentValues.put("rspsize", reportitem.g());
                try {
                    this.a = this.b.getWritableDatabase();
                    this.a.insertOrThrow("olddata", null, contentValues);
                    this.a.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                i2 = i + 1;
            } else {
                Log.i("cgi_report_debug", "ReportDataModal backupOldItems succ_count = " + i);
            }
        }
        return i;
    }

    public synchronized boolean a() {
        boolean z = false;
        synchronized (this) {
            Log.i("cgi_report_debug", "ReportDataModal deleteOldItems start");
            try {
                this.a = this.b.getWritableDatabase();
                try {
                    this.a.execSQL("delete from olddata;");
                    this.a.close();
                    Log.i("cgi_report_debug", "ReportDataModal deleteOldItems success");
                    z = true;
                } catch (Exception e) {
                    e.printStackTrace();
                    this.a.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return z;
    }

    public synchronized boolean b() {
        boolean z = false;
        synchronized (this) {
            Log.i("cgi_report_debug", "ReportDataModal deleteNewItems start");
            try {
                this.a = this.b.getWritableDatabase();
                try {
                    this.a.execSQL("delete from newdata;");
                    this.c = 0;
                    this.a.close();
                    Log.i("cgi_report_debug", "ReportDataModal deleteNewItems start");
                    z = true;
                } catch (Exception e) {
                    e.printStackTrace();
                    this.a.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return z;
    }

    public synchronized ArrayList c() {
        ArrayList arrayList;
        Log.i("cgi_report_debug", "ReportDataModal getNewItems start");
        ArrayList arrayList2 = new ArrayList();
        try {
            this.a = this.b.getReadableDatabase();
            Cursor rawQuery = this.a.rawQuery("select * from newdata", new String[0]);
            rawQuery.moveToFirst();
            while (!rawQuery.isAfterLast()) {
                arrayList2.add(new reportItem(rawQuery.getString(rawQuery.getColumnIndex("apn")), rawQuery.getString(rawQuery.getColumnIndex("frequency")), rawQuery.getString(rawQuery.getColumnIndex("commandid")), rawQuery.getString(rawQuery.getColumnIndex("resultcode")), rawQuery.getString(rawQuery.getColumnIndex("tmcost")), rawQuery.getString(rawQuery.getColumnIndex("reqsize")), rawQuery.getString(rawQuery.getColumnIndex("rspsize"))));
                rawQuery.moveToNext();
            }
            rawQuery.close();
            this.a.close();
            Log.i("cgi_report_debug", "ReportDataModal getNewItems success, count = " + arrayList2.size());
            arrayList = arrayList2;
        } catch (Exception e) {
            e.printStackTrace();
            arrayList = arrayList2;
        }
        return arrayList;
    }

    public synchronized ArrayList d() {
        ArrayList arrayList;
        Log.i("cgi_report_debug", "ReportDataModal getOldItems start");
        ArrayList arrayList2 = new ArrayList();
        try {
            this.a = this.b.getReadableDatabase();
            Cursor rawQuery = this.a.rawQuery("select * from olddata", new String[0]);
            rawQuery.moveToFirst();
            while (!rawQuery.isAfterLast()) {
                arrayList2.add(new reportItem(rawQuery.getString(rawQuery.getColumnIndex("apn")), rawQuery.getString(rawQuery.getColumnIndex("frequency")), rawQuery.getString(rawQuery.getColumnIndex("commandid")), rawQuery.getString(rawQuery.getColumnIndex("resultcode")), rawQuery.getString(rawQuery.getColumnIndex("tmcost")), rawQuery.getString(rawQuery.getColumnIndex("reqsize")), rawQuery.getString(rawQuery.getColumnIndex("rspsize"))));
                rawQuery.moveToNext();
            }
            rawQuery.close();
            this.a.close();
            Log.i("cgi_report_debug", "ReportDataModal getOldItems success, count = " + arrayList2.size());
            arrayList = arrayList2;
        } catch (Exception e) {
            e.printStackTrace();
            arrayList = arrayList2;
        }
        return arrayList;
    }

    public int e() {
        Log.i("cgi_report_debug", "ReportDataModal getTotalCount count = " + this.c);
        return this.c;
    }
}
