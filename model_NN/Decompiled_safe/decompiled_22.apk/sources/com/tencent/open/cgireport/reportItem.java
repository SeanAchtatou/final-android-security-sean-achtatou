package com.tencent.open.cgireport;

import android.util.Log;

/* compiled from: ProGuard */
public class reportItem {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;

    public reportItem() {
        this.a = "";
        this.b = "";
        this.c = "";
        this.d = "";
        this.e = "";
        this.f = "";
        this.g = "";
    }

    public reportItem(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        this.a = str + "";
        this.b = str2 + "";
        this.c = str3 + "";
        this.d = str4 + "";
        this.e = str5 + "";
        this.f = str6 + "";
        this.g = str7 + "";
        Log.i("report_debug", "reportItem apn=" + this.a + ",frequency=" + this.b + ",commandid=" + this.c + ",resultcode=" + this.d + "timecost" + this.e + ",reqsize=" + this.f + ",rspsize=" + this.g);
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public String f() {
        return this.g;
    }

    public String g() {
        return this.f;
    }
}
