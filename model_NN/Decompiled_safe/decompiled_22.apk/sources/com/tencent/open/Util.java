package com.tencent.open;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import com.tencent.tauth.Constants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.zip.GZIPInputStream;
import org.apache.commons.httpclient.HttpState;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class Util {
    private static boolean a = true;

    public static String a(Bundle bundle, String str) {
        if (bundle == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int size = bundle.size();
        int i = -1;
        for (String next : bundle.keySet()) {
            int i2 = i + 1;
            Object obj = bundle.get(next);
            if (!(obj instanceof String)) {
                i = i2;
            } else {
                sb.append("Content-Disposition: form-data; name=\"" + next + "\"" + "\r\n" + "\r\n" + ((String) obj));
                if (i2 < size - 1) {
                    sb.append("\r\n--" + str + "\r\n");
                }
                i = i2;
            }
        }
        return sb.toString();
    }

    public static String a(Bundle bundle) {
        if (bundle == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if ((obj instanceof String) || (obj instanceof String[])) {
                if (obj instanceof String[]) {
                    if (z) {
                        z = false;
                    } else {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(next) + "=");
                    String[] stringArray = bundle.getStringArray(next);
                    for (int i = 0; i < stringArray.length; i++) {
                        if (i == 0) {
                            sb.append(URLEncoder.encode(stringArray[i]));
                        } else {
                            sb.append(URLEncoder.encode("," + stringArray[i]));
                        }
                    }
                } else {
                    if (z) {
                        z = false;
                    } else {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(next) + "=" + URLEncoder.encode(bundle.getString(next)));
                }
                z = z;
            }
        }
        return sb.toString();
    }

    public static Bundle a(String str) {
        Bundle bundle = new Bundle();
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                if (split2.length == 2) {
                    bundle.putString(URLDecoder.decode(split2[0]), URLDecoder.decode(split2[1]));
                }
            }
        }
        return bundle;
    }

    public static JSONObject a(JSONObject jSONObject, String str) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        if (str != null) {
            for (String split : str.split("&")) {
                String[] split2 = split.split("=");
                if (split2.length == 2) {
                    try {
                        jSONObject.put(URLDecoder.decode(split2[0]), URLDecoder.decode(split2[1]));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return jSONObject;
    }

    public static Bundle b(String str) {
        try {
            URL url = new URL(str.replace("auth://", "http://"));
            Bundle a2 = a(url.getQuery());
            a2.putAll(a(url.getRef()));
            return a2;
        } catch (MalformedURLException e) {
            return new Bundle();
        }
    }

    public static JSONObject c(String str) {
        try {
            URL url = new URL(str.replace("auth://", "http://"));
            JSONObject a2 = a((JSONObject) null, url.getQuery());
            a(a2, url.getRef());
            return a2;
        } catch (MalformedURLException e) {
            return new JSONObject();
        }
    }

    /* compiled from: ProGuard */
    public class Statistic {
        public String a;
        public long b;
        public long c;

        public Statistic(String str, int i) {
            this.a = str;
            this.b = (long) i;
            if (this.a != null) {
                this.c = (long) this.a.length();
            }
        }
    }

    public static Statistic a(Context context, String str, String str2, Bundle bundle) {
        HttpUriRequest httpUriRequest;
        int i;
        String str3;
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        if (context == null || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || ((activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable())) {
            Bundle bundle2 = new Bundle(bundle);
            String string = bundle2.getString("appid_for_getting_config");
            bundle2.remove("appid_for_getting_config");
            HttpClient a2 = a(context, string, str);
            if (str2.equals("GET")) {
                String a3 = a(bundle2);
                int length = 0 + a3.length();
                if (str.indexOf("?") == -1) {
                    str3 = str + "?";
                } else {
                    str3 = str + "&";
                }
                HttpUriRequest httpGet = new HttpGet(str3 + a3);
                httpGet.addHeader("Accept-Encoding", "gzip");
                int i2 = length;
                httpUriRequest = httpGet;
                i = i2;
            } else if (str2.equals("POST")) {
                HttpUriRequest httpPost = new HttpPost(str);
                httpPost.addHeader("Accept-Encoding", "gzip");
                Bundle bundle3 = new Bundle();
                for (String next : bundle2.keySet()) {
                    Object obj = bundle2.get(next);
                    if (obj instanceof byte[]) {
                        bundle3.putByteArray(next, (byte[]) obj);
                    }
                }
                if (!bundle2.containsKey("method")) {
                    bundle2.putString("method", str2);
                }
                httpPost.setHeader("Content-Type", "multipart/form-data;boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
                httpPost.setHeader("Connection", "Keep-Alive");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byteArrayOutputStream.write("--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes());
                byteArrayOutputStream.write(a(bundle2, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f").getBytes());
                if (!bundle3.isEmpty()) {
                    int size = bundle3.size();
                    byteArrayOutputStream.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes());
                    int i3 = -1;
                    for (String next2 : bundle3.keySet()) {
                        i3++;
                        byteArrayOutputStream.write(("Content-Disposition: form-data; name=\"" + next2 + "\"; filename=\"" + next2 + "\"" + "\r\n").getBytes());
                        byteArrayOutputStream.write("Content-Type: content/unknown\r\n\r\n".getBytes());
                        byteArrayOutputStream.write(bundle3.getByteArray(next2));
                        if (i3 < size - 1) {
                            byteArrayOutputStream.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n".getBytes());
                        }
                    }
                }
                byteArrayOutputStream.write("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f--\r\n".getBytes());
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                i = byteArray.length + 0;
                byteArrayOutputStream.close();
                httpPost.setEntity(new ByteArrayEntity(byteArray));
                httpUriRequest = httpPost;
            } else {
                httpUriRequest = null;
                i = 0;
            }
            HttpResponse execute = a2.execute(httpUriRequest);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return new Statistic(a(execute), i);
            }
            throw new HttpStatusException("http status code error:" + statusCode);
        }
        throw new NetworkUnavailableException("network unavailable");
    }

    private static String a(HttpResponse httpResponse) {
        InputStream inputStream;
        InputStream content = httpResponse.getEntity().getContent();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Header firstHeader = httpResponse.getFirstHeader("Content-Encoding");
        if (firstHeader == null || firstHeader.getValue().toLowerCase().indexOf("gzip") <= -1) {
            inputStream = content;
        } else {
            inputStream = new GZIPInputStream(content);
        }
        byte[] bArr = new byte[512];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return new String(byteArrayOutputStream.toByteArray());
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0048  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.apache.http.client.HttpClient a(android.content.Context r8, java.lang.String r9, java.lang.String r10) {
        /*
            r1 = 0
            com.tencent.open.MySSLSocketFactory r0 = new com.tencent.open.MySSLSocketFactory     // Catch:{ KeyManagementException -> 0x0160, NoSuchAlgorithmException -> 0x0169, KeyStoreException -> 0x0172, UnrecoverableKeyException -> 0x017b }
            r0.<init>()     // Catch:{ KeyManagementException -> 0x0160, NoSuchAlgorithmException -> 0x0169, KeyStoreException -> 0x0172, UnrecoverableKeyException -> 0x017b }
            org.apache.http.conn.ssl.X509HostnameVerifier r1 = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER     // Catch:{ KeyManagementException -> 0x018a, NoSuchAlgorithmException -> 0x0188, KeyStoreException -> 0x0186, UnrecoverableKeyException -> 0x0184 }
            r0.setHostnameVerifier(r1)     // Catch:{ KeyManagementException -> 0x018a, NoSuchAlgorithmException -> 0x0188, KeyStoreException -> 0x0186, UnrecoverableKeyException -> 0x0184 }
        L_0x000b:
            org.apache.http.params.BasicHttpParams r2 = new org.apache.http.params.BasicHttpParams
            r2.<init>()
            com.tencent.open.OpenConfig r1 = com.tencent.open.OpenConfig.a(r8, r9)
            java.lang.String r3 = "Common_HttpConnectionTimeout"
            int r1 = r1.b(r3)
            java.lang.String r3 = "OpenConfig_test"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "config 3:Common_HttpConnectionTimeout     config_value:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r5 = "   appid:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.String r5 = "     url:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r10)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
            if (r1 != 0) goto L_0x004a
            r1 = 15000(0x3a98, float:2.102E-41)
        L_0x004a:
            java.lang.String r3 = "OpenConfig_test"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "config 3:Common_HttpConnectionTimeout     result_value:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r5 = "   appid:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.String r5 = "     url:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r10)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r2, r1)
            com.tencent.open.OpenConfig r1 = com.tencent.open.OpenConfig.a(r8, r9)
            java.lang.String r3 = "Common_SocketConnectionTimeout"
            int r1 = r1.b(r3)
            java.lang.String r3 = "OpenConfig_test"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "config 4:Common_SocketConnectionTimeout   config_value:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r5 = "   appid:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.String r5 = "     url:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r10)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
            if (r1 != 0) goto L_0x00b3
            r1 = 30000(0x7530, float:4.2039E-41)
        L_0x00b3:
            java.lang.String r3 = "OpenConfig_test"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "config 4:Common_SocketConnectionTimeout   result_value:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r5 = "   appid:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.String r5 = "     url:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r10)
            java.lang.String r4 = r4.toString()
            android.util.Log.d(r3, r4)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r2, r1)
            org.apache.http.HttpVersion r1 = org.apache.http.HttpVersion.HTTP_1_1
            org.apache.http.params.HttpProtocolParams.setVersion(r2, r1)
            java.lang.String r1 = "UTF-8"
            org.apache.http.params.HttpProtocolParams.setContentCharset(r2, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "AndroidSDK_"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = android.os.Build.VERSION.SDK
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "_"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = android.os.Build.DEVICE
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "_"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = android.os.Build.VERSION.RELEASE
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            org.apache.http.params.HttpProtocolParams.setUserAgent(r2, r1)
            org.apache.http.conn.scheme.SchemeRegistry r1 = new org.apache.http.conn.scheme.SchemeRegistry
            r1.<init>()
            org.apache.http.conn.scheme.Scheme r3 = new org.apache.http.conn.scheme.Scheme
            java.lang.String r4 = "http"
            org.apache.http.conn.scheme.PlainSocketFactory r5 = org.apache.http.conn.scheme.PlainSocketFactory.getSocketFactory()
            r6 = 80
            r3.<init>(r4, r5, r6)
            r1.register(r3)
            org.apache.http.conn.scheme.Scheme r3 = new org.apache.http.conn.scheme.Scheme
            java.lang.String r4 = "https"
            r5 = 443(0x1bb, float:6.21E-43)
            r3.<init>(r4, r0, r5)
            r1.register(r3)
            org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager r0 = new org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager
            r0.<init>(r2, r1)
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient
            r1.<init>(r0, r2)
            com.tencent.open.Util$NetworkProxy r0 = a(r8)
            if (r0 == 0) goto L_0x015f
            org.apache.http.HttpHost r2 = new org.apache.http.HttpHost
            java.lang.String r3 = r0.a
            int r0 = r0.b
            r2.<init>(r3, r0)
            org.apache.http.params.HttpParams r0 = r1.getParams()
            java.lang.String r3 = "http.route.default-proxy"
            r0.setParameter(r3, r2)
        L_0x015f:
            return r1
        L_0x0160:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x0164:
            r1.printStackTrace()
            goto L_0x000b
        L_0x0169:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x016d:
            r1.printStackTrace()
            goto L_0x000b
        L_0x0172:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x0176:
            r1.printStackTrace()
            goto L_0x000b
        L_0x017b:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x017f:
            r1.printStackTrace()
            goto L_0x000b
        L_0x0184:
            r1 = move-exception
            goto L_0x017f
        L_0x0186:
            r1 = move-exception
            goto L_0x0176
        L_0x0188:
            r1 = move-exception
            goto L_0x016d
        L_0x018a:
            r1 = move-exception
            goto L_0x0164
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.Util.a(android.content.Context, java.lang.String, java.lang.String):org.apache.http.client.HttpClient");
    }

    public static JSONObject d(String str) {
        if (str.equals(HttpState.PREEMPTIVE_DEFAULT)) {
            str = "{value : false}";
        }
        if (str.equals("true")) {
            str = "{value : true}";
        }
        if (str.contains("allback(")) {
            str = str.replaceFirst("[\\s\\S]*allback\\(([\\s\\S]*)\\);[^\\)]*\\z", "$1").trim();
        }
        return new JSONObject(str);
    }

    public static void a(String str, String str2) {
        if (a) {
            Log.d(str, str2);
        }
    }

    public static String a() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces != null && networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            a("Tencent-Util", e.toString());
        }
        return "";
    }

    private static String c(Context context) {
        if (Build.VERSION.SDK_INT >= 11) {
            return System.getProperty("http.proxyHost");
        }
        if (context == null) {
            return Proxy.getDefaultHost();
        }
        String host = Proxy.getHost(context);
        if (e(host)) {
            return Proxy.getDefaultHost();
        }
        return host;
    }

    public static NetworkProxy a(Context context) {
        if (context == null) {
            return null;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return null;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        if (activeNetworkInfo.getType() == 0) {
            String c = c(context);
            int d = d(context);
            if (!e(c) && d >= 0) {
                return new NetworkProxy(c, d, null);
            }
        }
        return null;
    }

    /* compiled from: ProGuard */
    public class NetworkProxy {
        public final String a;
        public final int b;

        /* synthetic */ NetworkProxy(String str, int i, e eVar) {
            this(str, i);
        }

        private NetworkProxy(String str, int i) {
            this.a = str;
            this.b = i;
        }
    }

    private static int d(Context context) {
        if (Build.VERSION.SDK_INT >= 11) {
            String property = System.getProperty("http.proxyPort");
            if (e(property)) {
                return -1;
            }
            try {
                return Integer.parseInt(property);
            } catch (NumberFormatException e) {
                return -1;
            }
        } else if (context == null) {
            return Proxy.getDefaultPort();
        } else {
            int port = Proxy.getPort(context);
            if (port < 0) {
                return Proxy.getDefaultPort();
            }
            return port;
        }
    }

    public static boolean e(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean a(Context context, String str) {
        try {
            a(context, "com.android.browser", "com.android.browser.BrowserActivity", str);
        } catch (Exception e) {
            try {
                a(context, "com.google.android.browser", "com.android.browser.BrowserActivity", str);
            } catch (Exception e2) {
                try {
                    a(context, "com.android.chrome", "com.google.android.apps.chrome.Main", str);
                } catch (Exception e3) {
                    return false;
                }
            }
        }
        return true;
    }

    private static void a(Context context, String str, String str2, String str3) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(str, str2));
        intent.setAction("android.intent.action.VIEW");
        intent.addFlags(1073741824);
        intent.addFlags(268435456);
        intent.setData(Uri.parse(str3));
        context.startActivity(intent);
    }

    public static boolean b(Context context) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo("com.tencent.mobileqq", 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("checkMobileQQ", "error");
            e.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null) {
            return false;
        }
        String str = packageInfo.versionName;
        try {
            Log.d("MobileQQ verson", str);
            String[] split = str.split("\\.");
            int parseInt = Integer.parseInt(split[0]);
            int parseInt2 = Integer.parseInt(split[1]);
            if (parseInt > 4 || (parseInt == 4 && parseInt2 >= 1)) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static String f(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            if (digest == null) {
                return str;
            }
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(a(b >>> 4));
                sb.append(a(b));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return str;
        }
    }

    private static char a(int i) {
        int i2 = i & 15;
        if (i2 < 10) {
            return (char) (i2 + 48);
        }
        return (char) ((i2 - 10) + 97);
    }

    public static void a(Context context, String str, long j, String str2) {
        Bundle bundle = new Bundle();
        bundle.putString("appid_for_getting_config", str2);
        bundle.putString("strValue", str2);
        bundle.putString("nValue", str);
        bundle.putString("qver", Constants.SDK_VERSION);
        if (j != 0) {
            bundle.putLong("elt", j);
        }
        new f(context, bundle).start();
    }

    public static boolean b() {
        File file = null;
        if (Environment.getExternalStorageState().equals("mounted")) {
            file = Environment.getExternalStorageDirectory();
        }
        if (file != null) {
            return true;
        }
        return false;
    }
}
