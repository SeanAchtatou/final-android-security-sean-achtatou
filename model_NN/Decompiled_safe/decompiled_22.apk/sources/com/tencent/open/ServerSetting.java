package com.tencent.open;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
public class ServerSetting {
    public static final int ASK_URL = 8;
    public static final int AUTHORIZE_CGI = 2;
    public static final int BRAG_URL = 7;
    private static final String DEFAULT_CGI_AUTHORIZE = "https://openmobile.qq.com/oauth2.0/m_authorize?";
    private static final String DEFAULT_LOCAL_STORAGE_URI = "http://qzs.qq.com";
    private static final String DEFAULT_REDIRECT_URI = "auth://tauth.qq.com/";
    private static final String DEFAULT_URL_ASK = "http://qzs.qq.com/open/mobile/request/sdk_request.html?";
    private static final String DEFAULT_URL_BRAG = "http://qzs.qq.com/open/mobile/brag/sdk_brag.html?";
    private static final String DEFAULT_URL_GIFT = "http://qzs.qq.com/open/mobile/request/sdk_request.html?";
    private static final String DEFAULT_URL_GRAPH_BASE = "https://openmobile.qq.com/";
    private static final String DEFAULT_URL_INVITE = "http://qzs.qq.com/open/mobile/invite/sdk_invite.html?";
    private static final String DEFAULT_URL_REPORT = "http://wspeed.qq.com/w.cgi";
    private static final String DEFAULT_URL_SEND_STORY = "http://qzs.qq.com/open/mobile/sendstory/sdk_sendstory_v1.3.html?";
    public static final int ENVIRONMENT_EXPERIENCE = 1;
    public static final int ENVIRONMENT_NORMOL = 0;
    public static final int GIFT_URL = 9;
    public static final int GRAPH_BASE_URL = 6;
    public static final int INVITE_URL = 4;
    private static final String KEY_ASK_URL = "AskUrl";
    private static final String KEY_AUTHORIZE_CGI = "AuthorizeCgi";
    private static final String KEY_BRAG_URL = "BragUrl";
    private static final String KEY_GIFT_URL = "GiftUrl";
    private static final String KEY_GRAPH_BASE_URL = "GraphBaseUrl";
    private static final String KEY_INVITE_URL = "InviteUrl";
    private static final String KEY_LOCAL_STORAGE_URl = "LocalStorageUrl";
    private static final String KEY_REDIRECT_URL = "RedirectUrl";
    private static final String KEY_REPORT_URL = "ReportUrl";
    private static final String KEY_STORY_URL = "StoryUrl";
    public static final int LOCAL_STORAGE_URL = 10;
    public static final int REDIRECT_URL = 1;
    public static final int REPORT_URL = 5;
    private static final String SERVER_PREFS = "ServerPrefs";
    private static final String SERVER_TYPE = "ServerType";
    public static final int STORY_URL = 3;
    private static final String TAG = ServerSetting.class.getName();
    private static ServerSetting sServerSetting = null;
    private String sAskUrl = null;
    private String sAuthorizeCgi = null;
    private String sBragUrl = null;
    private String sGiftUrl = null;
    private String sGraphBaseUrl = null;
    private String sInviteUrl = null;
    private String sLocalStorageUrl = null;
    private String sRedirectUrl = null;
    private String sReportUrl = null;
    private String sSendStoryUrl = null;
    private WeakReference sWeakSharedPrefs = null;

    public static ServerSetting getInstance() {
        if (sServerSetting == null) {
            sServerSetting = new ServerSetting();
        }
        return sServerSetting;
    }

    public void changeServer() {
        this.sWeakSharedPrefs = null;
        this.sRedirectUrl = null;
        this.sAuthorizeCgi = null;
        this.sSendStoryUrl = null;
        this.sInviteUrl = null;
        this.sReportUrl = null;
        this.sGraphBaseUrl = null;
        this.sBragUrl = null;
        this.sAskUrl = null;
        this.sGiftUrl = null;
        this.sLocalStorageUrl = null;
    }

    public String getSettingUrl(Context context, int i) {
        if (context != null && (this.sWeakSharedPrefs == null || this.sWeakSharedPrefs.get() == null)) {
            this.sWeakSharedPrefs = new WeakReference(context.getSharedPreferences(SERVER_PREFS, 0));
        }
        switch (i) {
            case 1:
                if (context == null) {
                    return DEFAULT_REDIRECT_URI;
                }
                if (this.sRedirectUrl == null) {
                    this.sRedirectUrl = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_REDIRECT_URL, DEFAULT_REDIRECT_URI);
                }
                return this.sRedirectUrl;
            case 2:
                if (context == null) {
                    return DEFAULT_CGI_AUTHORIZE;
                }
                if (this.sAuthorizeCgi == null) {
                    this.sAuthorizeCgi = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_AUTHORIZE_CGI, DEFAULT_CGI_AUTHORIZE);
                }
                return this.sAuthorizeCgi;
            case 3:
                if (context == null) {
                    return DEFAULT_URL_SEND_STORY;
                }
                if (this.sSendStoryUrl == null) {
                    this.sSendStoryUrl = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_STORY_URL, DEFAULT_URL_SEND_STORY);
                }
                return this.sSendStoryUrl;
            case 4:
                if (context == null) {
                    return DEFAULT_URL_INVITE;
                }
                if (this.sInviteUrl == null) {
                    this.sInviteUrl = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_INVITE_URL, DEFAULT_URL_INVITE);
                }
                return this.sInviteUrl;
            case 5:
                if (context == null) {
                    return DEFAULT_URL_REPORT;
                }
                if (this.sReportUrl == null) {
                    this.sReportUrl = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_REPORT_URL, DEFAULT_URL_REPORT);
                }
                return this.sReportUrl;
            case 6:
                if (context == null) {
                    return "https://openmobile.qq.com/";
                }
                if (this.sGraphBaseUrl == null) {
                    this.sGraphBaseUrl = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_GRAPH_BASE_URL, "https://openmobile.qq.com/");
                }
                return this.sGraphBaseUrl;
            case 7:
                if (context == null) {
                    return DEFAULT_URL_BRAG;
                }
                if (this.sBragUrl == null) {
                    this.sBragUrl = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_BRAG_URL, DEFAULT_URL_BRAG);
                }
                return this.sBragUrl;
            case 8:
                if (context == null) {
                    return "http://qzs.qq.com/open/mobile/request/sdk_request.html?";
                }
                if (this.sAskUrl == null) {
                    this.sAskUrl = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_ASK_URL, "http://qzs.qq.com/open/mobile/request/sdk_request.html?");
                }
                return this.sAskUrl;
            case 9:
                if (context == null) {
                    return "http://qzs.qq.com/open/mobile/request/sdk_request.html?";
                }
                if (this.sGiftUrl == null) {
                    this.sGiftUrl = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_GIFT_URL, "http://qzs.qq.com/open/mobile/request/sdk_request.html?");
                }
                return this.sGiftUrl;
            case 10:
                if (context == null) {
                    return DEFAULT_LOCAL_STORAGE_URI;
                }
                if (this.sLocalStorageUrl == null) {
                    this.sLocalStorageUrl = ((SharedPreferences) this.sWeakSharedPrefs.get()).getString(KEY_LOCAL_STORAGE_URl, DEFAULT_LOCAL_STORAGE_URI);
                }
                return this.sLocalStorageUrl;
            default:
                return "";
        }
    }

    public void setEnvironment(Context context, int i) {
        if (context != null && (this.sWeakSharedPrefs == null || this.sWeakSharedPrefs.get() == null)) {
            this.sWeakSharedPrefs = new WeakReference(context.getSharedPreferences(SERVER_PREFS, 0));
        }
        if (i == 0 || i == 1) {
            switch (i) {
                case 0:
                    SharedPreferences.Editor edit = ((SharedPreferences) this.sWeakSharedPrefs.get()).edit();
                    edit.putInt(SERVER_TYPE, 0);
                    edit.putString(KEY_REDIRECT_URL, DEFAULT_REDIRECT_URI);
                    edit.putString(KEY_AUTHORIZE_CGI, DEFAULT_CGI_AUTHORIZE);
                    edit.putString(KEY_STORY_URL, DEFAULT_URL_SEND_STORY);
                    edit.putString(KEY_INVITE_URL, DEFAULT_URL_INVITE);
                    edit.putString(KEY_REPORT_URL, DEFAULT_URL_REPORT);
                    edit.putString(KEY_GRAPH_BASE_URL, "https://openmobile.qq.com/");
                    edit.putString(KEY_BRAG_URL, DEFAULT_URL_BRAG);
                    edit.putString(KEY_ASK_URL, "http://qzs.qq.com/open/mobile/request/sdk_request.html?");
                    edit.putString(KEY_GIFT_URL, "http://qzs.qq.com/open/mobile/request/sdk_request.html?");
                    edit.putString(KEY_LOCAL_STORAGE_URl, DEFAULT_LOCAL_STORAGE_URI);
                    edit.commit();
                    changeServer();
                    Toast.makeText(context, "已切换到正式环境", 0).show();
                    return;
                case 1:
                    SharedPreferences.Editor edit2 = ((SharedPreferences) this.sWeakSharedPrefs.get()).edit();
                    edit2.putInt(SERVER_TYPE, 1);
                    edit2.putString(KEY_REDIRECT_URL, DEFAULT_REDIRECT_URI);
                    edit2.putString(KEY_AUTHORIZE_CGI, "https://test.openmobile.qq.com/oauth2.0/m_authorize?");
                    edit2.putString(KEY_STORY_URL, "http://testmobile.qq.com/open/mobile/sendstory/sdk_sendstory_v1.3.html?");
                    edit2.putString(KEY_INVITE_URL, "http://testmobile.qq.com/open/mobile/invite/sdk_invite.html?");
                    edit2.putString(KEY_REPORT_URL, DEFAULT_URL_REPORT);
                    edit2.putString(KEY_GRAPH_BASE_URL, "https://test.openmobile.qq.com/");
                    edit2.putString(KEY_BRAG_URL, "http://testmobile.qq.com/open/mobile/brag/sdk_brag.html?");
                    edit2.putString(KEY_ASK_URL, "http://testmobile.qq.com/open/mobile/request/sdk_request.html?");
                    edit2.putString(KEY_GIFT_URL, "http://testmobile.qq.com/open/mobile/request/sdk_request.html?");
                    edit2.putString(KEY_LOCAL_STORAGE_URl, "http://test.openmobile.qq.com");
                    edit2.commit();
                    changeServer();
                    Toast.makeText(context, "已切换到体验环境", 0).show();
                    return;
                default:
                    return;
            }
        } else {
            Log.e(TAG, "切换环境参数错误，正式环境为0，体验环境为1");
        }
    }
}
