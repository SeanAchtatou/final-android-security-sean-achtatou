package com.tencent.open;

import android.webkit.CookieSyncManager;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class b implements IUiListener {
    final /* synthetic */ OpenUi a;
    private IUiListener b;
    private boolean c;

    public b(OpenUi openUi, IUiListener iUiListener, boolean z, boolean z2) {
        this.a = openUi;
        this.b = iUiListener;
        this.c = z;
    }

    public void onComplete(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("access_token");
            String string2 = jSONObject.getString("expires_in");
            String string3 = jSONObject.getString("openid");
            if (!(string == null || this.a.a == null || string3 == null)) {
                this.a.a.a(string, string2);
                this.a.a.a(string3);
                TencentStat.b(this.a.a, string3);
            }
            String string4 = jSONObject.getString(Constants.PARAM_PLATFORM_ID);
            if (string4 != null) {
                try {
                    this.a.a.f().getSharedPreferences(Constants.PREFERENCE_PF, 0).edit().putString(Constants.PARAM_PLATFORM_ID, string4).commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (this.c) {
                CookieSyncManager.getInstance().sync();
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        this.b.onComplete(jSONObject);
    }

    public void onError(UiError uiError) {
        this.b.onError(uiError);
    }

    public void onCancel() {
        this.b.onCancel();
    }
}
