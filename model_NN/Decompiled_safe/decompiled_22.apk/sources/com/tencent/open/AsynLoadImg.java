package com.tencent.open;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: ProGuard */
public class AsynLoadImg {
    /* access modifiers changed from: private */
    public static String e;
    Activity a;
    private String b = "share_qq_";
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public AsynLoadImgBack d;
    /* access modifiers changed from: private */
    public long f;
    /* access modifiers changed from: private */
    public Handler g;
    private Runnable h = new k(this);

    public AsynLoadImg(Activity activity) {
        this.a = activity;
        this.g = new l(this, activity.getMainLooper());
    }

    public void a(String str, AsynLoadImgBack asynLoadImgBack) {
        Log.v("AsynLoadImg", "--save---");
        if (str == null || str.equals("")) {
            asynLoadImgBack.saved(1, null);
        } else if (!Util.b()) {
            asynLoadImgBack.saved(2, null);
        } else {
            e = Environment.getExternalStorageDirectory() + "/tmp/";
            this.f = System.currentTimeMillis();
            this.c = str;
            this.d = asynLoadImgBack;
            new Thread(this.h).start();
        }
    }

    public boolean a(Bitmap bitmap, String str) {
        String str2 = e;
        try {
            File file = new File(str2);
            if (!file.exists()) {
                file.mkdir();
            }
            Log.v("AsynLoadImg", "saveFile:" + str);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(str2 + str)));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bufferedOutputStream);
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            return true;
        } catch (IOException e2) {
            e2.printStackTrace();
            Log.v("AsynLoadImg", "saveFile bmp fail---");
            return false;
        }
    }

    public static Bitmap a(String str) {
        Log.v("AsynLoadImg", "getbitmap:" + str);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            Log.v("AsynLoadImg", "image download finished." + str);
            return decodeStream;
        } catch (IOException e2) {
            e2.printStackTrace();
            Log.v("AsynLoadImg", "getbitmap bmp fail---");
            return null;
        }
    }
}
