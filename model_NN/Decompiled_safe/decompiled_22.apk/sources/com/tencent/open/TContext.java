package com.tencent.open;

import android.content.Context;
import com.city_life.part_asynctask.UploadUtils;

/* compiled from: ProGuard */
public class TContext {
    public String a = "webview";
    private String b;
    private String c;
    private long d = -1;
    private String e;
    private Context f;

    public TContext(String str, Context context) {
        this.b = str;
        a(context);
    }

    public boolean a() {
        if (this.c == null || System.currentTimeMillis() >= this.d) {
            return false;
        }
        return true;
    }

    public String b() {
        return this.c;
    }

    public void a(String str, String str2) {
        this.d = 0;
        this.c = null;
        if (str2 == null) {
            str2 = UploadUtils.SUCCESS;
        }
        this.d = System.currentTimeMillis() + (Long.parseLong(str2) * 1000);
        this.c = str;
    }

    public String c() {
        return this.e;
    }

    public void a(String str) {
        this.e = str;
    }

    public String d() {
        return this.b;
    }

    public long e() {
        return this.d;
    }

    public Context f() {
        return this.f;
    }

    public void a(Context context) {
        this.f = context;
    }
}
