package com.tencent.open;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import com.tencent.tauth.IRequestListener;
import java.io.CharConversionException;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.InvalidObjectException;
import java.io.NotActiveException;
import java.io.NotSerializableException;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.io.SyncFailedException;
import java.io.UTFDataFormatException;
import java.io.UnsupportedEncodingException;
import java.io.WriteAbortedException;
import java.net.BindException;
import java.net.ConnectException;
import java.net.HttpRetryException;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.PortUnreachableException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.FileLockInterruptionException;
import java.nio.charset.MalformedInputException;
import java.nio.charset.UnmappableCharacterException;
import java.util.InvalidPropertiesFormatException;
import java.util.zip.ZipException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLKeyException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import org.apache.http.ConnectionClosedException;
import org.apache.http.MalformedChunkCodingException;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;
import sina_weibo.Constants;

/* compiled from: ProGuard */
public class OpenApi {
    private TContext a;

    public OpenApi(TContext tContext) {
        this.a = tContext;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0130, code lost:
        r12 = -4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0132, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0133, code lost:
        r15 = r14;
        r14 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x014a, code lost:
        r17 = r14;
        r14 = r8;
        r8 = r17;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0162, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0163, code lost:
        r15 = r14;
        r14 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0165, code lost:
        r14.printStackTrace();
        r12 = -8;
        r10 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x016d, code lost:
        if (r13 >= r4) goto L_0x0179;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x016f, code lost:
        r6 = android.os.SystemClock.elapsedRealtime();
        r8 = r15;
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0179, code lost:
        com.tencent.open.cgireport.ReportManager.a().a(r21, r5, r6, 0, 0, -8, r0.a.d());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x018a, code lost:
        throw r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x018b, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x018c, code lost:
        r14 = r3;
        r14.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r12 = java.lang.Integer.parseInt(r14.getMessage().replace("http status code error:", ""));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01b6, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x01b7, code lost:
        r3.printStackTrace();
        r12 = -9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x01bd, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01be, code lost:
        r3.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01c1, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01c2, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01c3, code lost:
        r14 = r3;
        r14.printStackTrace();
        com.tencent.open.cgireport.ReportManager.a().a(r21, r5, r6, 0, 0, -3, r0.a.d());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01dd, code lost:
        throw r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01de, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01df, code lost:
        r14 = r3;
        r14.printStackTrace();
        com.tencent.open.cgireport.ReportManager.a().a(r21, r5, r6, 0, 0, a(r14), r0.a.d());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01fc, code lost:
        throw r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01fd, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01fe, code lost:
        r14 = r3;
        r14.printStackTrace();
        com.tencent.open.cgireport.ReportManager.a().a(r21, r5, r6, 0, 0, -4, r0.a.d());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0218, code lost:
        throw r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0219, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x021a, code lost:
        r14 = r8;
        r15 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x021e, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x021f, code lost:
        r14 = r8;
        r15 = r9;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0132 A[ExcHandler: ConnectTimeoutException (r8v20 'e' org.apache.http.conn.ConnectTimeoutException A[CUSTOM_DECLARE]), Splitter:B:11:0x00fc] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0162 A[ExcHandler: SocketTimeoutException (r8v19 'e' java.net.SocketTimeoutException A[CUSTOM_DECLARE]), Splitter:B:11:0x00fc] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x016f  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x018b A[ExcHandler: HttpStatusException (r3v11 'e' com.tencent.open.HttpStatusException A[CUSTOM_DECLARE]), Splitter:B:8:0x00ee] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01bd A[ExcHandler: NetworkUnavailableException (r3v10 'e' com.tencent.open.NetworkUnavailableException A[CUSTOM_DECLARE]), Splitter:B:8:0x00ee] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01c2 A[ExcHandler: MalformedURLException (r3v8 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:8:0x00ee] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01de A[ExcHandler: IOException (r3v6 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:8:0x00ee] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01fd A[ExcHandler: JSONException (r3v4 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:8:0x00ee] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0223 A[LOOP:0: B:7:0x00e6->B:67:0x0223, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x014a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0179 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject a(android.content.Context r21, java.lang.String r22, android.os.Bundle r23, java.lang.String r24) {
        /*
            r20 = this;
            r0 = r20
            r1 = r22
            r2 = r23
            android.os.Bundle r16 = r0.a(r1, r2)
            java.lang.String r3 = r22.toLowerCase()
            java.lang.String r4 = "http"
            boolean r3 = r3.startsWith(r4)
            if (r3 != 0) goto L_0x0227
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            com.tencent.open.ServerSetting r4 = com.tencent.open.ServerSetting.getInstance()
            r5 = 6
            r0 = r21
            java.lang.String r4 = r4.getSettingUrl(r0, r5)
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r22
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.tencent.open.ServerSetting r5 = com.tencent.open.ServerSetting.getInstance()
            r6 = 6
            r0 = r21
            java.lang.String r5 = r5.getSettingUrl(r0, r6)
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r22
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r5 = r4.toString()
        L_0x0052:
            r0 = r20
            r1 = r22
            r0.a(r1)
            r9 = 0
            long r7 = android.os.SystemClock.elapsedRealtime()
            r6 = 0
            r0 = r20
            com.tencent.open.TContext r4 = r0.a
            java.lang.String r4 = r4.d()
            r0 = r21
            com.tencent.open.OpenConfig r4 = com.tencent.open.OpenConfig.a(r0, r4)
            java.lang.String r10 = "Common_HttpRetryCount"
            int r4 = r4.b(r10)
            java.lang.String r10 = "OpenConfig_test"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "config 1:Common_HttpRetryCount            config_value:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r12 = "   appid:"
            java.lang.StringBuilder r11 = r11.append(r12)
            r0 = r20
            com.tencent.open.TContext r12 = r0.a
            java.lang.String r12 = r12.d()
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r12 = "     url:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r5)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
            if (r4 != 0) goto L_0x00aa
            r4 = 3
        L_0x00aa:
            java.lang.String r10 = "OpenConfig_test"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "config 1:Common_HttpRetryCount            result_value:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r12 = "   appid:"
            java.lang.StringBuilder r11 = r11.append(r12)
            r0 = r20
            com.tencent.open.TContext r12 = r0.a
            java.lang.String r12 = r12.d()
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r12 = "     url:"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r5)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
            r17 = r6
            r18 = r7
            r6 = r18
            r8 = r17
        L_0x00e6:
            int r13 = r8 + 1
            r0 = r21
            r1 = r24
            r2 = r16
            com.tencent.open.Util$Statistic r10 = com.tencent.open.Util.a(r0, r3, r1, r2)     // Catch:{ ConnectTimeoutException -> 0x021e, SocketTimeoutException -> 0x0219, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de, JSONException -> 0x01fd }
            java.lang.String r8 = r10.a     // Catch:{ ConnectTimeoutException -> 0x021e, SocketTimeoutException -> 0x0219, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de, JSONException -> 0x01fd }
            org.json.JSONObject r14 = com.tencent.open.Util.d(r8)     // Catch:{ ConnectTimeoutException -> 0x021e, SocketTimeoutException -> 0x0219, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de, JSONException -> 0x01fd }
            java.lang.String r8 = "oauth2.0/m_me"
            r0 = r22
            boolean r8 = r8.equals(r0)     // Catch:{ ConnectTimeoutException -> 0x0132, SocketTimeoutException -> 0x0162, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de, JSONException -> 0x01fd }
            if (r8 == 0) goto L_0x0113
            if (r14 == 0) goto L_0x0113
            java.lang.String r8 = "openid"
            java.lang.String r8 = r14.getString(r8)     // Catch:{ ConnectTimeoutException -> 0x0132, SocketTimeoutException -> 0x0162, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de, JSONException -> 0x01fd }
            if (r8 == 0) goto L_0x0113
            r0 = r20
            com.tencent.open.TContext r9 = r0.a     // Catch:{ ConnectTimeoutException -> 0x0132, SocketTimeoutException -> 0x0162, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de, JSONException -> 0x01fd }
            r9.a(r8)     // Catch:{ ConnectTimeoutException -> 0x0132, SocketTimeoutException -> 0x0162, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de, JSONException -> 0x01fd }
        L_0x0113:
            java.lang.String r8 = "ret"
            int r12 = r14.getInt(r8)     // Catch:{ JSONException -> 0x012f, ConnectTimeoutException -> 0x0132, SocketTimeoutException -> 0x0162, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de }
        L_0x0119:
            long r8 = r10.b     // Catch:{ ConnectTimeoutException -> 0x0132, SocketTimeoutException -> 0x0162, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de, JSONException -> 0x01fd }
            long r10 = r10.c     // Catch:{ ConnectTimeoutException -> 0x0132, SocketTimeoutException -> 0x0162, HttpStatusException -> 0x018b, NetworkUnavailableException -> 0x01bd, MalformedURLException -> 0x01c2, IOException -> 0x01de, JSONException -> 0x01fd }
        L_0x011d:
            com.tencent.open.cgireport.ReportManager r3 = com.tencent.open.cgireport.ReportManager.a()
            r0 = r20
            com.tencent.open.TContext r4 = r0.a
            java.lang.String r13 = r4.d()
            r4 = r21
            r3.a(r4, r5, r6, r8, r10, r12, r13)
            return r14
        L_0x012f:
            r8 = move-exception
            r12 = -4
            goto L_0x0119
        L_0x0132:
            r8 = move-exception
            r15 = r14
            r14 = r8
        L_0x0135:
            r14.printStackTrace()
            r12 = -7
            r8 = 0
            r10 = 0
            if (r13 >= r4) goto L_0x0150
            long r6 = android.os.SystemClock.elapsedRealtime()
            r17 = r8
            r8 = r15
            r14 = r17
        L_0x0148:
            if (r13 < r4) goto L_0x0223
            r17 = r14
            r14 = r8
            r8 = r17
            goto L_0x011d
        L_0x0150:
            com.tencent.open.cgireport.ReportManager r3 = com.tencent.open.cgireport.ReportManager.a()
            r0 = r20
            com.tencent.open.TContext r4 = r0.a
            java.lang.String r13 = r4.d()
            r4 = r21
            r3.a(r4, r5, r6, r8, r10, r12, r13)
            throw r14
        L_0x0162:
            r8 = move-exception
            r15 = r14
            r14 = r8
        L_0x0165:
            r14.printStackTrace()
            r12 = -8
            r8 = 0
            r10 = 0
            if (r13 >= r4) goto L_0x0179
            long r6 = android.os.SystemClock.elapsedRealtime()
            r17 = r8
            r8 = r15
            r14 = r17
            goto L_0x0148
        L_0x0179:
            com.tencent.open.cgireport.ReportManager r3 = com.tencent.open.cgireport.ReportManager.a()
            r0 = r20
            com.tencent.open.TContext r4 = r0.a
            java.lang.String r13 = r4.d()
            r4 = r21
            r3.a(r4, r5, r6, r8, r10, r12, r13)
            throw r14
        L_0x018b:
            r3 = move-exception
            r14 = r3
            r14.printStackTrace()
            java.lang.String r3 = r14.getMessage()
            java.lang.String r4 = "http status code error:"
            java.lang.String r8 = ""
            java.lang.String r3 = r3.replace(r4, r8)     // Catch:{ Exception -> 0x01b6 }
            int r12 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x01b6 }
        L_0x01a0:
            r8 = 0
            r10 = 0
            com.tencent.open.cgireport.ReportManager r3 = com.tencent.open.cgireport.ReportManager.a()
            r0 = r20
            com.tencent.open.TContext r4 = r0.a
            java.lang.String r13 = r4.d()
            r4 = r21
            r3.a(r4, r5, r6, r8, r10, r12, r13)
            throw r14
        L_0x01b6:
            r3 = move-exception
            r3.printStackTrace()
            r12 = -9
            goto L_0x01a0
        L_0x01bd:
            r3 = move-exception
            r3.printStackTrace()
            throw r3
        L_0x01c2:
            r3 = move-exception
            r14 = r3
            r14.printStackTrace()
            r12 = -3
            r8 = 0
            r10 = 0
            com.tencent.open.cgireport.ReportManager r3 = com.tencent.open.cgireport.ReportManager.a()
            r0 = r20
            com.tencent.open.TContext r4 = r0.a
            java.lang.String r13 = r4.d()
            r4 = r21
            r3.a(r4, r5, r6, r8, r10, r12, r13)
            throw r14
        L_0x01de:
            r3 = move-exception
            r14 = r3
            r14.printStackTrace()
            int r12 = a(r14)
            r8 = 0
            r10 = 0
            com.tencent.open.cgireport.ReportManager r3 = com.tencent.open.cgireport.ReportManager.a()
            r0 = r20
            com.tencent.open.TContext r4 = r0.a
            java.lang.String r13 = r4.d()
            r4 = r21
            r3.a(r4, r5, r6, r8, r10, r12, r13)
            throw r14
        L_0x01fd:
            r3 = move-exception
            r14 = r3
            r14.printStackTrace()
            r12 = -4
            r8 = 0
            r10 = 0
            com.tencent.open.cgireport.ReportManager r3 = com.tencent.open.cgireport.ReportManager.a()
            r0 = r20
            com.tencent.open.TContext r4 = r0.a
            java.lang.String r13 = r4.d()
            r4 = r21
            r3.a(r4, r5, r6, r8, r10, r12, r13)
            throw r14
        L_0x0219:
            r8 = move-exception
            r14 = r8
            r15 = r9
            goto L_0x0165
        L_0x021e:
            r8 = move-exception
            r14 = r8
            r15 = r9
            goto L_0x0135
        L_0x0223:
            r9 = r8
            r8 = r13
            goto L_0x00e6
        L_0x0227:
            r5 = r22
            r3 = r22
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.OpenApi.a(android.content.Context, java.lang.String, android.os.Bundle, java.lang.String):org.json.JSONObject");
    }

    private Bundle a(String str, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString(Constants.TX_API_FORMAT, "json");
        bundle.putString("status_os", Build.VERSION.RELEASE);
        bundle.putString("status_machine", Build.MODEL);
        bundle.putString("status_version", Build.VERSION.SDK);
        bundle.putString("sdkv", com.tencent.tauth.Constants.SDK_VERSION);
        bundle.putString("sdkp", "a");
        if (this.a != null) {
            if (c()) {
                bundle.putString("access_token", b());
            }
            if (!com.tencent.tauth.Constants.GRAPH_OPEN_ID.equals(str)) {
                bundle.putString("oauth_consumer_key", this.a.d() + "");
                if (this.a.c() != null) {
                    bundle.putString("openid", this.a.c() + "");
                }
            }
            bundle.putString("appid_for_getting_config", this.a.d() + "");
            try {
                bundle.putString(com.tencent.tauth.Constants.PARAM_PLATFORM_ID, this.a.f().getSharedPreferences(com.tencent.tauth.Constants.PREFERENCE_PF, 0).getString(com.tencent.tauth.Constants.PARAM_PLATFORM_ID, "openmobile_android"));
            } catch (Exception e) {
                e.printStackTrace();
                bundle.putString(com.tencent.tauth.Constants.PARAM_PLATFORM_ID, "openmobile_android");
            }
        }
        return bundle;
    }

    public void a(Context context, String str, Bundle bundle, String str2, IRequestListener iRequestListener, Object obj) {
        new h(this, context, str, bundle, str2, iRequestListener, obj).start();
    }

    private String b() {
        if (this.a != null) {
            return this.a.b();
        }
        return null;
    }

    private boolean c() {
        return this.a != null && this.a.a();
    }

    public static String a() {
        return com.tencent.tauth.Constants.SDK_VERSION_STRING;
    }

    private void a(String str) {
        if (str.indexOf("add_share") > -1 || str.indexOf("upload_pic") > -1 || str.indexOf("add_topic") > -1 || str.indexOf("set_user_face") > -1 || str.indexOf("add_t") > -1 || str.indexOf("add_pic_t") > -1 || str.indexOf("add_pic_url") > -1 || str.indexOf("add_video") > -1) {
            TencentStat.a(this.a, "requireApi", str);
        }
    }

    public void a(IRequestListener iRequestListener) {
        a(this.a.f(), "user/get_app_friends", null, "GET", iRequestListener, null);
    }

    private static int a(IOException iOException) {
        if (iOException instanceof CharConversionException) {
            return -20;
        }
        if (iOException instanceof MalformedInputException) {
            return -21;
        }
        if (iOException instanceof UnmappableCharacterException) {
            return -22;
        }
        if (iOException instanceof HttpResponseException) {
            return -23;
        }
        if (iOException instanceof ClosedChannelException) {
            return -24;
        }
        if (iOException instanceof ConnectionClosedException) {
            return -25;
        }
        if (iOException instanceof EOFException) {
            return -26;
        }
        if (iOException instanceof FileLockInterruptionException) {
            return -27;
        }
        if (iOException instanceof FileNotFoundException) {
            return -28;
        }
        if (iOException instanceof HttpRetryException) {
            return -29;
        }
        if (iOException instanceof ConnectTimeoutException) {
            return -7;
        }
        if (iOException instanceof SocketTimeoutException) {
            return -8;
        }
        if (iOException instanceof InvalidPropertiesFormatException) {
            return -30;
        }
        if (iOException instanceof MalformedChunkCodingException) {
            return -31;
        }
        if (iOException instanceof MalformedURLException) {
            return -3;
        }
        if (iOException instanceof NoHttpResponseException) {
            return -32;
        }
        if (iOException instanceof InvalidClassException) {
            return -33;
        }
        if (iOException instanceof InvalidObjectException) {
            return -34;
        }
        if (iOException instanceof NotActiveException) {
            return -35;
        }
        if (iOException instanceof NotSerializableException) {
            return -36;
        }
        if (iOException instanceof OptionalDataException) {
            return -37;
        }
        if (iOException instanceof StreamCorruptedException) {
            return -38;
        }
        if (iOException instanceof WriteAbortedException) {
            return -39;
        }
        if (iOException instanceof ProtocolException) {
            return -40;
        }
        if (iOException instanceof SSLHandshakeException) {
            return -41;
        }
        if (iOException instanceof SSLKeyException) {
            return -42;
        }
        if (iOException instanceof SSLPeerUnverifiedException) {
            return -43;
        }
        if (iOException instanceof SSLProtocolException) {
            return -44;
        }
        if (iOException instanceof BindException) {
            return -45;
        }
        if (iOException instanceof ConnectException) {
            return -46;
        }
        if (iOException instanceof NoRouteToHostException) {
            return -47;
        }
        if (iOException instanceof PortUnreachableException) {
            return -48;
        }
        if (iOException instanceof SyncFailedException) {
            return -49;
        }
        if (iOException instanceof UTFDataFormatException) {
            return -50;
        }
        if (iOException instanceof UnknownHostException) {
            return -51;
        }
        if (iOException instanceof UnknownServiceException) {
            return -52;
        }
        if (iOException instanceof UnsupportedEncodingException) {
            return -53;
        }
        if (iOException instanceof ZipException) {
            return -54;
        }
        return -2;
    }
}
