package com.tencent.open;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class a implements IUiListener {
    final /* synthetic */ OpenUi a;
    /* access modifiers changed from: private */
    public IUiListener b;
    private Activity c;
    private Bundle d;
    private String e;
    private String f;
    private String g;
    private String h;
    /* access modifiers changed from: private */
    public Handler i = new n(this);

    public a(OpenUi openUi, Activity activity, Bundle bundle, IUiListener iUiListener, String str, String str2, String str3, String str4) {
        this.a = openUi;
        this.c = activity;
        this.d = bundle;
        this.b = iUiListener;
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.h = str4;
        Log.d("toddtest", "EncrytokenListener appid=" + str + ",  openid=" + str2 + ",  token=" + str3 + ",  params=" + bundle.toString() + ", action=" + str4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.open.OpenUi.a(android.content.Context, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener):int
     arg types: [android.app.Activity, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener]
     candidates:
      com.tencent.open.OpenUi.a(android.app.Activity, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener):int
      com.tencent.open.OpenUi.a(android.content.Context, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener):int */
    public void onComplete(JSONObject jSONObject) {
        String str = null;
        try {
            str = jSONObject.getString(Constants.PARAM_ENCRY_EOKEN);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        Log.d("toddtest", "EncrytokenListener onComplete jsonobj = " + jSONObject.toString());
        if (this.h.equals(Constants.ACTION_CHECK_TOKEN)) {
            if (str == null || str.length() <= 0) {
                a();
            } else {
                a(str);
            }
        } else if (Constants.ACTION_CHALLENGE.equals(this.h) || Constants.ACTION_STORY.equals(this.h) || Constants.ACTION_INVITE.equals(this.h) || Constants.ACTION_BRAG.equals(this.h) || Constants.ACTION_ASK.equals(this.h) || Constants.ACTION_GIFT.equals(this.h)) {
            this.d.putString("encrytoken", str);
            this.a.a((Context) this.c, this.h, this.d, this.b);
        }
        this.a.a();
    }

    public void onError(UiError uiError) {
        if (this.h.equals(Constants.ACTION_CHECK_TOKEN)) {
            a();
        }
    }

    public void onCancel() {
        if (this.h.equals(Constants.ACTION_CHECK_TOKEN)) {
            a();
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        this.a.a.a((String) null);
        this.a.a.a(null, UploadUtils.SUCCESS);
        this.a.a(this.c, Constants.ACTION_LOGIN, this.d, this.b);
    }

    private void a(String str) {
        Log.d("toddtest", "validToken encrytoken=" + str);
        OpenApi openApi = new OpenApi(this.a.a);
        Bundle bundle = new Bundle();
        bundle.putString("oauth_consumer_key", this.e);
        bundle.putString("openid", this.f);
        bundle.putString("access_token", this.g);
        bundle.putString("encrytoken", str);
        openApi.a(this.a.a.f(), "https://openmobile.qq.com/user/user_login_statis", bundle, "POST", new d(this), bundle);
    }
}
