package com.tencent.open;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

/* compiled from: ProGuard */
class l extends Handler {
    final /* synthetic */ AsynLoadImg a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    l(AsynLoadImg asynLoadImg, Looper looper) {
        super(looper);
        this.a = asynLoadImg;
    }

    public void handleMessage(Message message) {
        Log.v("AsynLoadImg", "handleMessage:" + message.arg1);
        if (message.arg1 == 0) {
            this.a.d.saved(message.arg1, (String) message.obj);
        } else {
            this.a.d.saved(message.arg1, null);
        }
    }
}
