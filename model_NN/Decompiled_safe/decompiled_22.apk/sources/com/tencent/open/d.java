package com.tencent.open;

import android.os.Message;
import android.util.Log;
import com.tencent.tauth.IRequestListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class d implements IRequestListener {
    final /* synthetic */ a a;

    public d(a aVar) {
        this.a = aVar;
    }

    public void onUnknowException(Exception exc, Object obj) {
        Log.d("toddtest", exc.getMessage() + "");
        Message message = new Message();
        message.what = -6;
        message.obj = exc.getMessage() + "";
        this.a.i.sendMessage(message);
    }

    public void onSocketTimeoutException(SocketTimeoutException socketTimeoutException, Object obj) {
        Log.d("toddtest", socketTimeoutException.getMessage() + "");
        Message message = new Message();
        message.what = -8;
        message.obj = socketTimeoutException.getMessage() + "";
        this.a.i.sendMessage(message);
    }

    public void onNetworkUnavailableException(NetworkUnavailableException networkUnavailableException, Object obj) {
        Log.d("toddtest", networkUnavailableException.getMessage() + "");
        Message message = new Message();
        message.what = -2;
        message.obj = networkUnavailableException.getMessage() + "";
        this.a.i.sendMessage(message);
    }

    public void onMalformedURLException(MalformedURLException malformedURLException, Object obj) {
        Log.d("toddtest", malformedURLException.getMessage() + "");
        Message message = new Message();
        message.what = -3;
        message.obj = malformedURLException.getMessage() + "";
        this.a.i.sendMessage(message);
    }

    public void onJSONException(JSONException jSONException, Object obj) {
        Log.d("toddtest", jSONException.getMessage() + "");
        Message message = new Message();
        message.what = -4;
        message.obj = jSONException.getMessage() + "";
        this.a.i.sendMessage(message);
    }

    public void onIOException(IOException iOException, Object obj) {
        Log.d("toddtest", iOException.getMessage() + "");
        Message message = new Message();
        message.what = -2;
        message.obj = iOException.getMessage() + "";
        this.a.i.sendMessage(message);
    }

    public void onHttpStatusException(HttpStatusException httpStatusException, Object obj) {
        Log.d("toddtest", httpStatusException.getMessage() + "");
        Message message = new Message();
        message.what = -9;
        message.obj = httpStatusException.getMessage() + "";
        this.a.i.sendMessage(message);
    }

    public void onConnectTimeoutException(ConnectTimeoutException connectTimeoutException, Object obj) {
        Log.d("toddtest", connectTimeoutException.getMessage() + "");
        Message message = new Message();
        message.what = -7;
        message.obj = connectTimeoutException.getMessage() + "";
        this.a.i.sendMessage(message);
    }

    public void onComplete(JSONObject jSONObject, Object obj) {
        Log.d("toddtest", jSONObject.toString() + "");
        Message message = new Message();
        message.what = 0;
        message.obj = jSONObject;
        this.a.i.sendMessage(message);
    }
}
