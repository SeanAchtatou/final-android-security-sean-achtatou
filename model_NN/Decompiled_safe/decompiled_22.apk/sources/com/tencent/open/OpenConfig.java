package com.tencent.open;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.tauth.Constants;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class OpenConfig {
    private static HashMap a = null;
    private static String b = null;
    /* access modifiers changed from: private */
    public Context c = null;
    private String d = null;
    private JSONObject e = null;
    private long f = 0;
    /* access modifiers changed from: private */
    public int g = 0;
    private boolean h = true;

    public static OpenConfig a(Context context, String str) {
        if (a == null) {
            a = new HashMap();
        }
        if (str != null) {
            b = str;
        }
        if (str == null) {
            if (b != null) {
                str = b;
            } else {
                str = UploadUtils.SUCCESS;
            }
        }
        OpenConfig openConfig = (OpenConfig) a.get(str);
        if (openConfig != null) {
            return openConfig;
        }
        OpenConfig openConfig2 = new OpenConfig(context, str);
        a.put(str, openConfig2);
        return openConfig2;
    }

    private OpenConfig(Context context, String str) {
        this.c = context;
        this.d = str;
        a();
        b();
    }

    private void a() {
        try {
            this.e = new JSONObject(e("com.tencent.open.config.json"));
        } catch (JSONException e2) {
            this.e = new JSONObject();
        }
    }

    private String e(String str) {
        InputStream open;
        String str2;
        try {
            if (this.d != null) {
                str2 = str + "." + this.d;
            } else {
                str2 = str;
            }
            open = this.c.openFileInput(str2);
        } catch (FileNotFoundException e2) {
            try {
                open = this.c.getAssets().open(str);
            } catch (IOException e3) {
                e3.printStackTrace();
                return "";
            }
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(open));
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    stringBuffer.append(readLine);
                } else {
                    String stringBuffer2 = stringBuffer.toString();
                    try {
                        open.close();
                        bufferedReader.close();
                        return stringBuffer2;
                    } catch (IOException e4) {
                        e4.printStackTrace();
                        return stringBuffer2;
                    }
                }
            } catch (IOException e5) {
                e5.printStackTrace();
                try {
                    open.close();
                    bufferedReader.close();
                    return "";
                } catch (IOException e6) {
                    e6.printStackTrace();
                    return "";
                }
            } catch (Throwable th) {
                try {
                    open.close();
                    bufferedReader.close();
                } catch (IOException e7) {
                    e7.printStackTrace();
                }
                throw th;
            }
        }
    }

    private void a(String str, String str2) {
        try {
            if (this.d != null) {
                str = str + "." + this.d;
            }
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.c.openFileOutput(str, 0));
            outputStreamWriter.write(str2);
            outputStreamWriter.flush();
            outputStreamWriter.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void b() {
        if (this.g != 0) {
            f("update thread is running, return");
            return;
        }
        this.g = 1;
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PARAM_APP_ID, this.d);
        bundle.putString("appid_for_getting_config", this.d);
        bundle.putString("status_os", Build.VERSION.RELEASE);
        bundle.putString("status_machine", Build.MODEL);
        bundle.putString("status_version", Build.VERSION.SDK);
        bundle.putString("sdkv", Constants.SDK_VERSION);
        bundle.putString("sdkp", "a");
        new c(this, bundle).start();
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        f("cgi back, do update");
        this.e = jSONObject;
        a("com.tencent.open.config.json", jSONObject.toString());
        this.f = SystemClock.elapsedRealtime();
    }

    private void c() {
        int optInt = this.e.optInt("Common_frequency");
        if (optInt == 0) {
            optInt = 1;
        }
        if (SystemClock.elapsedRealtime() - this.f >= ((long) (optInt * 3600000))) {
            b();
        }
    }

    public String a(String str) {
        f("get " + str);
        c();
        return this.e.optString(str);
    }

    public int b(String str) {
        f("get " + str);
        c();
        return this.e.optInt(str);
    }

    public long c(String str) {
        f("get " + str);
        c();
        return this.e.optLong(str);
    }

    public boolean d(String str) {
        f("get " + str);
        c();
        Object opt = this.e.opt(str);
        if (opt == null) {
            return false;
        }
        if (opt instanceof Integer) {
            return !opt.equals(0);
        } else if (opt instanceof Boolean) {
            return ((Boolean) opt).booleanValue();
        } else {
            return false;
        }
    }

    private void f(String str) {
        if (this.h) {
            Log.i("OpenConfig", str + "; appid: " + this.d);
        }
    }
}
