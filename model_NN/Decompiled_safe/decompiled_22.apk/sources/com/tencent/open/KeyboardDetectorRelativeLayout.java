package com.tencent.open;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

/* compiled from: ProGuard */
public class KeyboardDetectorRelativeLayout extends RelativeLayout {
    private static final String a = KeyboardDetectorRelativeLayout.class.getName();
    private Rect b = null;
    private boolean c = false;
    private IKeyboardChanged d = null;

    /* compiled from: ProGuard */
    public interface IKeyboardChanged {
        void onKeyboardHidden();

        void onKeyboardShown(int i);
    }

    public KeyboardDetectorRelativeLayout(Context context) {
        super(context);
        if (this.b == null) {
            this.b = new Rect();
        }
    }

    public KeyboardDetectorRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (this.b == null) {
            this.b = new Rect();
        }
    }

    public void a(IKeyboardChanged iKeyboardChanged) {
        this.d = iKeyboardChanged;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i2);
        Activity activity = (Activity) getContext();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(this.b);
        int height = (activity.getWindowManager().getDefaultDisplay().getHeight() - this.b.top) - size;
        if (!(this.d == null || size == 0)) {
            if (height > 100) {
                this.d.onKeyboardShown((Math.abs(this.b.height()) - getPaddingBottom()) - getPaddingTop());
            } else {
                this.d.onKeyboardHidden();
            }
        }
        super.onMeasure(i, i2);
    }
}
