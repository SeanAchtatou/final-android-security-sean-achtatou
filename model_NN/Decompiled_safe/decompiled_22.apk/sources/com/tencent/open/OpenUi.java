package com.tencent.open;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.city_life.part_asynctask.UploadUtils;
import com.tencent.plus.ImageActivity;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class OpenUi {
    /* access modifiers changed from: private */
    public TContext a;
    private int b = Constants.CODE_REQUEST_MIN;
    private SparseArray c = new SparseArray();

    public OpenUi(TContext tContext) {
        this.a = tContext;
    }

    public int a(Activity activity, String str, Bundle bundle, IUiListener iUiListener) {
        return a(activity, str, bundle, iUiListener, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.open.OpenUi.a(android.content.Context, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener):int
     arg types: [android.app.Activity, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener]
     candidates:
      com.tencent.open.OpenUi.a(android.app.Activity, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener):int
      com.tencent.open.OpenUi.a(android.content.Context, java.lang.String, android.os.Bundle, com.tencent.tauth.IUiListener):int */
    public int a(Activity activity, String str, Bundle bundle, IUiListener iUiListener, boolean z) {
        if (Constants.ACTION_CHALLENGE.equals(str) || Constants.ACTION_BRAG.equals(str) || Constants.ACTION_INVITE.equals(str) || Constants.ACTION_STORY.equals(str) || Constants.ACTION_ASK.equals(str) || Constants.ACTION_GIFT.equals(str)) {
            b(activity, str, bundle, iUiListener, false);
            return 2;
        }
        if (!z) {
            String b2 = this.a.b();
            String c2 = this.a.c();
            String d = this.a.d();
            Log.d("toddtest", "OpenUI showUi");
            if (b2 != null && c2 != null && d != null && b2.length() > 0 && c2.length() > 0 && d.length() > 0 && Constants.ACTION_LOGIN.equals(str)) {
                b(activity, Constants.ACTION_CHECK_TOKEN, bundle, iUiListener, true);
                return 3;
            }
        }
        if (!b(activity, str, bundle, iUiListener)) {
            return a((Context) activity, str, bundle, iUiListener);
        }
        if (!z) {
            return 1;
        }
        Util.a(activity, "10785", 0, this.a.d());
        return 1;
    }

    public String a(Activity activity) {
        try {
            return activity.getPackageManager().getPackageInfo(Constants.PACKAGE_AGENT, 0).versionName;
        } catch (Exception e) {
            return null;
        }
    }

    public void b(Activity activity, String str, Bundle bundle, IUiListener iUiListener, boolean z) {
        String str2;
        Log.d("toddtest", "OpenUI getEncryToken");
        Intent intent = new Intent();
        intent.setClassName(Constants.PACKAGE_AGENT, "com.tencent.open.agent.EncryTokenActivity");
        intent.putExtra("key_request_code", b());
        Intent intent2 = new Intent();
        intent2.setClassName(Constants.PACKAGE_AGENT, "com.tencent.open.agent.AgentActivity");
        a aVar = new a(this, activity, bundle, iUiListener, this.a.d(), this.a.c(), this.a.b(), str);
        if (b(activity.getApplicationContext(), intent)) {
            str2 = "qzone3.5_up";
        } else if (b(activity.getApplicationContext(), intent2)) {
            str2 = "qzone3.4";
        } else {
            str2 = "qzone3.3_below";
        }
        if (str2.equals("qzone3.5_up")) {
            Log.d("toddtest", "OpenUI checkToken qzone exist, version = " + str2);
            intent.putExtra(Constants.KEY_ACTION, Constants.ACTION_CHECK_TOKEN);
            intent.putExtra("oauth_consumer_key", this.a.d());
            intent.putExtra("openid", this.a.c());
            intent.putExtra("access_token", this.a.b());
            int b2 = b();
            activity.startActivityForResult(intent, b2);
            this.c.put(b2, new m(activity, Constants.ACTION_CHECK_TOKEN, bundle, aVar));
            return;
        }
        String b3 = this.a.b();
        String d = this.a.d();
        String c2 = this.a.c();
        if (b3 == null || b3.length() <= 0 || d == null || d.length() <= 0 || c2 == null || c2.length() <= 0) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(Constants.PARAM_ENCRY_EOKEN, "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            aVar.onComplete(jSONObject);
            return;
        }
        String f = Util.f("tencent&sdk&qazxc***14969%%" + b3 + d + c2 + "qzone3.4");
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put(Constants.PARAM_ENCRY_EOKEN, f);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        aVar.onComplete(jSONObject2);
    }

    public void a() {
        String str;
        String b2 = this.a.b();
        String d = this.a.d();
        String c2 = this.a.c();
        if (b2 == null || b2.length() <= 0 || d == null || d.length() <= 0 || c2 == null || c2.length() <= 0) {
            str = null;
        } else {
            str = Util.f("tencent&sdk&qazxc***14969%%" + b2 + d + c2 + "qzone3.4");
        }
        WebView webView = new WebView(this.a.f());
        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setDatabaseEnabled(true);
        String str2 = "<!DOCTYPE HTML><html lang=\"en-US\"><head><meta charset=\"UTF-8\"><title>localStorage Test</title><script type=\"text/javascript\">document.domain = 'qq.com';localStorage[\"" + this.a.c() + "_" + this.a.d() + "\"]=\"" + str + "\";</script></head><body></body></html>";
        String settingUrl = ServerSetting.getInstance().getSettingUrl(this.a.f(), 10);
        webView.loadDataWithBaseURL(settingUrl, str2, "text/html", "utf-8", settingUrl);
    }

    public Intent b(Activity activity) {
        Intent intent = new Intent();
        intent.setClassName(Constants.PACKAGE_QQ, "com.tencent.open.agent.AgentActivity");
        if (activity.getPackageManager().resolveActivity(intent, 0) != null) {
            return intent;
        }
        return null;
    }

    public Intent c(Activity activity) {
        Intent intent = new Intent();
        intent.setClassName(Constants.PACKAGE_AGENT, "com.tencent.open.agent.AgentActivity");
        if (b(activity, intent)) {
            return intent;
        }
        return null;
    }

    private int a(String str, String str2) {
        String[] split = str.split("\\.");
        String[] split2 = str2.split("\\.");
        int i = 0;
        while (i < split.length && i < split2.length) {
            try {
                int parseInt = Integer.parseInt(split[i]);
                int parseInt2 = Integer.parseInt(split2[i]);
                if (parseInt < parseInt2) {
                    return -1;
                }
                if (parseInt > parseInt2) {
                    return 1;
                }
                i++;
            } catch (Exception e) {
                return 0;
            }
        }
        if (split.length > i) {
            return 1;
        }
        if (split2.length <= i) {
            return 0;
        }
        return -1;
    }

    public boolean a(Activity activity, Bundle bundle, IUiListener iUiListener) {
        Intent b2;
        String a2 = a(activity);
        if (a2 == null) {
            b2 = b(activity);
            if (b2 == null) {
                return false;
            }
            this.a.a = "QQ";
        } else {
            String a3 = OpenConfig.a(activity, this.a.d()).a("Common_SSO_QzoneVersion");
            if (!(!TextUtils.isEmpty(a3))) {
                a3 = "3.7";
            }
            if (a(a2, "3.4") < 0 || a(a2, a3) >= 0) {
                b2 = b(activity);
                if (b2 != null) {
                    this.a.a = "QQ";
                } else {
                    b2 = c(activity);
                }
                if (b2 == null) {
                    return false;
                }
                this.a.a = "qzone";
            } else {
                b2 = c(activity);
                if (b2 == null) {
                    return false;
                }
                this.a.a = "qzone";
            }
        }
        Bundle bundle2 = new Bundle(bundle);
        if (this.a != null) {
            bundle2.putString("client_id", this.a.d());
        }
        bundle2.putString(Constants.PARAM_PLATFORM_ID, "openmobile_android");
        bundle2.putString("need_pay", UploadUtils.FAILURE);
        b2.putExtra("key_request_code", b());
        b2.putExtra(Constants.KEY_ACTION, Constants.ACTION_LOGIN);
        b2.putExtra(Constants.KEY_PARAMS, bundle2);
        try {
            int intExtra = b2.getIntExtra("key_request_code", 0);
            activity.startActivityForResult(b2, intExtra);
            this.c.put(intExtra, new m(activity, Constants.ACTION_LOGIN, bundle, new b(this, iUiListener, false, false)));
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    public boolean b(Activity activity, String str, Bundle bundle, IUiListener iUiListener) {
        Intent a2;
        if (Constants.ACTION_LOGIN.equals(str)) {
            return a(activity, bundle, iUiListener);
        }
        Bundle b2 = b(str, bundle);
        if (Constants.ACTION_SHARE_QQ.equals(str)) {
            a2 = a(activity, str, bundle.getString("scheme"));
        } else {
            a2 = a(activity, str);
        }
        if (a2 == null) {
            return false;
        }
        a2.putExtra(Constants.KEY_ACTION, str);
        a2.putExtra(Constants.KEY_PARAMS, b2);
        try {
            int intExtra = a2.getIntExtra("key_request_code", 0);
            activity.startActivityForResult(a2, intExtra);
            if (Constants.ACTION_PAY.equals(str)) {
                iUiListener = new b(this, iUiListener, false, true);
            }
            this.c.put(intExtra, new m(activity, str, bundle, iUiListener));
            return true;
        } catch (ActivityNotFoundException e) {
            Log.e("OpenUi", "not such activity", e);
            return false;
        }
    }

    public boolean a(int i, int i2, Intent intent) {
        Log.v("shareToQQ", "OpenUi onActivityResult:" + i + ",resultCode:" + i2);
        if (i >= 5656 && i <= 6656) {
            m mVar = (m) this.c.get(i);
            this.c.remove(i);
            if (!(mVar == null || mVar.a == null)) {
                if (i2 == -1) {
                    int intExtra = intent.getIntExtra(Constants.KEY_ERROR_CODE, 0);
                    if (intExtra == 0) {
                        String stringExtra = intent.getStringExtra(Constants.KEY_RESPONSE);
                        if (stringExtra != null) {
                            try {
                                mVar.a.onComplete(Util.d(stringExtra));
                            } catch (JSONException e) {
                                mVar.a.onError(new UiError(-4, Constants.MSG_JSON_ERROR, stringExtra));
                            }
                        } else {
                            mVar.a.onComplete(null);
                        }
                    } else {
                        mVar.a.onError(new UiError(intExtra, intent.getStringExtra(Constants.KEY_ERROR_MSG), intent.getStringExtra(Constants.KEY_ERROR_DETAIL)));
                    }
                } else if (i2 == 0) {
                    mVar.a.onCancel();
                }
                return true;
            }
        }
        return false;
    }

    private Intent a(Context context, String str) {
        Intent intent = new Intent();
        if (Constants.ACTION_AVATAR.equals(str)) {
            intent.setClass(context, ImageActivity.class);
            return intent;
        }
        intent.setClassName(Constants.PACKAGE_AGENT, "com.tencent.open.agent.AgentActivity");
        intent.putExtra("key_request_code", b());
        if (!b(context, intent)) {
            return null;
        }
        return intent;
    }

    private Intent a(Context context, String str, String str2) {
        Intent intent;
        try {
            intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
        } catch (Exception e) {
            e.printStackTrace();
            intent = null;
        }
        if (intent == null) {
            return null;
        }
        intent.putExtra("key_request_code", b());
        if (a(context, intent)) {
            return intent;
        }
        return null;
    }

    private boolean a(Context context, Intent intent) {
        if (context.getPackageManager().resolveActivity(intent, 0) == null) {
            return false;
        }
        return true;
    }

    private boolean b(Context context, Intent intent) {
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity == null) {
            return false;
        }
        return b(context, resolveActivity.activityInfo.packageName);
    }

    private boolean b(Context context, String str) {
        try {
            for (Signature charsString : context.getPackageManager().getPackageInfo(str, 64).signatures) {
                if (Util.f(charsString.toCharsString()).equals("ec96e9ac1149251acbb1b0c5777cae95")) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private int b() {
        m mVar;
        do {
            this.b++;
            if (this.b == 6656) {
                this.b = Constants.CODE_REQUEST_MIN;
            }
            mVar = (m) this.c.get(this.b);
            this.c.remove(this.b);
            if (!(mVar == null || mVar.a == null)) {
                mVar.a.onCancel();
                continue;
            }
        } while (mVar != null);
        return this.b;
    }

    public int a(Context context, String str, Bundle bundle, IUiListener iUiListener) {
        CookieSyncManager.createInstance(context);
        String c2 = c(str, a(str, bundle));
        if (Constants.ACTION_LOGIN.equals(str)) {
            iUiListener = new b(this, iUiListener, true, false);
        } else if (Constants.ACTION_PAY.equals(str)) {
            iUiListener = new b(this, iUiListener, true, true);
        }
        if (Constants.ACTION_CHALLENGE.equals(str) || Constants.ACTION_BRAG.equals(str)) {
            new PKDialog(context, c2, iUiListener).show();
            return 2;
        }
        new TDialog(context, c2, iUiListener, this.a).show();
        return 2;
    }

    private Bundle a(String str, Bundle bundle) {
        Bundle bundle2 = new Bundle(bundle);
        if (Constants.ACTION_LOGIN.equals(str) || Constants.ACTION_PAY.equals(str)) {
            if (this.a != null) {
                bundle2.putString("client_id", this.a.d());
                bundle2.putString(Constants.PARAM_PLATFORM_ID, "openmobile_android");
                bundle2.putString("need_pay", UploadUtils.FAILURE);
            }
        } else if (this.a != null) {
            bundle2.putString("oauth_consumer_key", this.a.d());
            if (this.a.a()) {
                bundle2.putString("access_token", this.a.b());
            }
            String c2 = this.a.c();
            if (c2 != null) {
                bundle2.putString("openid", c2);
            }
            try {
                bundle2.putString(Constants.PARAM_PLATFORM_ID, this.a.f().getSharedPreferences(Constants.PREFERENCE_PF, 0).getString(Constants.PARAM_PLATFORM_ID, "openmobile_android"));
            } catch (Exception e) {
                e.printStackTrace();
                bundle2.putString(Constants.PARAM_PLATFORM_ID, "openmobile_android");
            }
        }
        return bundle2;
    }

    private Bundle b(String str, Bundle bundle) {
        Bundle bundle2 = new Bundle(bundle);
        if (Constants.ACTION_STORY.equals(str) && bundle2.containsKey(Constants.PARAM_SHARE_URL)) {
            bundle2.putString(Constants.PARAM_URL, bundle2.getString(Constants.PARAM_SHARE_URL));
        } else if (Constants.ACTION_PAY.equals(str)) {
            if (this.a != null) {
                bundle2.putString("oauth_consumer_key", this.a.d());
                bundle2.putString(Constants.PARAM_PLATFORM_ID, "openmobile_android");
                bundle2.putString("need_pay", UploadUtils.FAILURE);
                String c2 = this.a.c();
                if (c2 != null) {
                    bundle2.putString(Constants.PARAM_HOPEN_ID, c2);
                } else {
                    bundle2.putString(Constants.PARAM_HOPEN_ID, "");
                }
            }
        } else if (this.a != null) {
            bundle2.putString(Constants.PARAM_APP_ID, this.a.d());
            if (this.a.a()) {
                bundle2.putString(Constants.PARAM_KEY_STR, this.a.b());
                bundle2.putString(Constants.PARAM_KEY_TYPE, "0x80");
            }
            String c3 = this.a.c();
            if (c3 != null) {
                bundle2.putString(Constants.PARAM_HOPEN_ID, c3);
            }
            bundle2.putString(Constants.PARAM_PLATFORM, "androidqz");
            try {
                bundle2.putString(Constants.PARAM_PLATFORM_ID, this.a.f().getSharedPreferences(Constants.PREFERENCE_PF, 0).getString(Constants.PARAM_PLATFORM_ID, "openmobile_android"));
            } catch (Exception e) {
                e.printStackTrace();
                bundle2.putString(Constants.PARAM_PLATFORM_ID, "openmobile_android");
            }
        }
        return bundle2;
    }

    private String c(String str, Bundle bundle) {
        bundle.putString("display", "mobile");
        StringBuilder sb = new StringBuilder();
        if (Constants.ACTION_LOGIN.equals(str) || Constants.ACTION_PAY.equals(str)) {
            bundle.putString("response_type", "token");
            bundle.putString(sina_weibo.Constants.SINA_REDIRECT_URI, ServerSetting.getInstance().getSettingUrl(this.a.f(), 1));
            bundle.putString("cancel_display", UploadUtils.FAILURE);
            bundle.putString("switch", UploadUtils.FAILURE);
            bundle.putString("sdkp", "a");
            bundle.putString("sdkv", Constants.SDK_VERSION);
            bundle.putString("status_userip", Util.a());
            bundle.putString("status_os", Build.VERSION.RELEASE);
            bundle.putString("status_version", Build.VERSION.SDK);
            bundle.putString("status_machine", Build.MODEL);
            sb.append(ServerSetting.getInstance().getSettingUrl(this.a.f(), 2));
            sb.append(Util.a(bundle));
        } else if (Constants.ACTION_STORY.equals(str)) {
            sb.append(ServerSetting.getInstance().getSettingUrl(this.a.f(), 3));
            bundle.putString("sdkv", Constants.SDK_VERSION);
            sb.append(Util.a(bundle));
        } else if (Constants.ACTION_INVITE.equals(str)) {
            sb.append(ServerSetting.getInstance().getSettingUrl(this.a.f(), 4));
            bundle.putString("sdkv", Constants.SDK_VERSION);
            sb.append(Util.a(bundle));
        } else if (Constants.ACTION_CHALLENGE.equals(str) || Constants.ACTION_BRAG.equals(str)) {
            sb.append(ServerSetting.getInstance().getSettingUrl(this.a.f(), 7));
            bundle.putString("sdkv", Constants.SDK_VERSION);
            sb.append(Util.a(bundle));
        } else if (Constants.ACTION_ASK.equals(str)) {
            sb.append(ServerSetting.getInstance().getSettingUrl(this.a.f(), 8));
            bundle.putString("sdkv", Constants.SDK_VERSION);
            sb.append(Util.a(bundle));
        } else if (Constants.ACTION_GIFT.equals(str)) {
            sb.append(ServerSetting.getInstance().getSettingUrl(this.a.f(), 9));
            bundle.putString("sdkv", Constants.SDK_VERSION);
            sb.append(Util.a(bundle));
        }
        return sb.toString();
    }
}
