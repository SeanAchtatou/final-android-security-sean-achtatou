package com.tencent.open;

import android.os.Handler;
import android.os.Message;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class n extends Handler {
    final /* synthetic */ a a;

    n(a aVar) {
        this.a = aVar;
    }

    public void handleMessage(Message message) {
        int i;
        if (message.what == 0) {
            try {
                i = Integer.parseInt(((JSONObject) message.obj).getString("ret"));
            } catch (JSONException e) {
                e.printStackTrace();
                this.a.a();
                i = 0;
            }
            if (i == 0) {
                this.a.b.onComplete((JSONObject) message.obj);
            } else {
                this.a.a();
            }
        } else {
            this.a.b.onError(new UiError(message.what, (String) message.obj, null));
        }
    }
}
