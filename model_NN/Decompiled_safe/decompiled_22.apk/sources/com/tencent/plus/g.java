package com.tencent.plus;

import com.tencent.mm.sdk.contact.RContact;
import com.tencent.open.HttpStatusException;
import com.tencent.open.NetworkUnavailableException;
import com.tencent.tauth.IRequestListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
class g implements IRequestListener {
    final /* synthetic */ ImageActivity a;

    g(ImageActivity imageActivity) {
        this.a = imageActivity;
    }

    public void onMalformedURLException(MalformedURLException malformedURLException, Object obj) {
        a(0);
    }

    public void onJSONException(JSONException jSONException, Object obj) {
        a(0);
    }

    public void onIOException(IOException iOException, Object obj) {
        a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.plus.ImageActivity.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, android.graphics.Rect):android.graphics.Rect
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
      com.tencent.plus.ImageActivity.a(java.lang.String, int):void
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, boolean):boolean
      com.tencent.plus.ImageActivity.a(java.lang.String, long):void */
    public void onComplete(JSONObject jSONObject, Object obj) {
        int i = -1;
        try {
            i = jSONObject.getInt("ret");
            if (i == 0) {
                this.a.e.post(new a(this, jSONObject.getString(RContact.COL_NICKNAME)));
                this.a.a("10659", 0L);
            } else {
                this.a.a("10661", 0L);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (i != 0) {
            a(i);
        }
    }

    private void a(int i) {
        if (this.a.l < 2) {
            this.a.e();
        }
    }

    public void onConnectTimeoutException(ConnectTimeoutException connectTimeoutException, Object obj) {
        a(0);
    }

    public void onSocketTimeoutException(SocketTimeoutException socketTimeoutException, Object obj) {
        a(0);
    }

    public void onUnknowException(Exception exc, Object obj) {
        a(0);
    }

    public void onNetworkUnavailableException(NetworkUnavailableException networkUnavailableException, Object obj) {
        a(0);
    }

    public void onHttpStatusException(HttpStatusException httpStatusException, Object obj) {
        a(0);
    }
}
