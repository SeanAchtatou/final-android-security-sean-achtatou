package com.tencent.plus;

import android.graphics.Rect;
import android.view.ViewTreeObserver;

/* compiled from: ProGuard */
class j implements ViewTreeObserver.OnGlobalLayoutListener {
    final /* synthetic */ ImageActivity a;

    j(ImageActivity imageActivity) {
        this.a = imageActivity;
    }

    public void onGlobalLayout() {
        this.a.a.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        Rect unused = this.a.r = this.a.i.a();
        this.a.f.a(this.a.r);
    }
}
