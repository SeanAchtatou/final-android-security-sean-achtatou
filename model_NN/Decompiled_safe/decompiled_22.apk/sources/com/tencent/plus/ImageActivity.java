package com.tencent.plus;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.open.OpenApi;
import com.tencent.open.TContext;
import com.tencent.open.Util;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IRequestListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: ProGuard */
public class ImageActivity extends Activity {
    RelativeLayout a;
    private TContext b;
    private OpenApi c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public Handler e;
    /* access modifiers changed from: private */
    public TouchView f;
    /* access modifiers changed from: private */
    public Button g;
    /* access modifiers changed from: private */
    public Button h;
    /* access modifiers changed from: private */
    public MaskView i;
    private TextView j;
    /* access modifiers changed from: private */
    public ProgressBar k;
    /* access modifiers changed from: private */
    public int l = 0;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public long n = 0;
    private int o = 0;
    private int p = 640;
    private int q = 640;
    /* access modifiers changed from: private */
    public Rect r = new Rect();
    private String s;
    private Bitmap t;
    private View.OnClickListener u = new i(this);
    private View.OnClickListener v = new f(this);
    private IRequestListener w = new h(this);
    private IRequestListener x = new g(this);

    private Bitmap a(String str) {
        int i2 = 1;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Uri parse = Uri.parse(str);
        InputStream openInputStream = getContentResolver().openInputStream(parse);
        BitmapFactory.decodeStream(openInputStream, null, options);
        openInputStream.close();
        int i3 = options.outWidth;
        int i4 = options.outHeight;
        while (i3 * i4 > 4194304) {
            i3 /= 2;
            i4 /= 2;
            i2 *= 2;
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = i2;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(parse), null, options);
    }

    /* access modifiers changed from: private */
    public Drawable b(String str) {
        IOException e2;
        Drawable drawable;
        try {
            InputStream open = getAssets().open(str);
            drawable = Drawable.createFromStream(open, str);
            try {
                open.close();
            } catch (IOException e3) {
                e2 = e3;
                e2.printStackTrace();
                return drawable;
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            drawable = null;
            e2 = iOException;
        }
        return drawable;
    }

    private View a() {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        ViewGroup.LayoutParams layoutParams2 = new ViewGroup.LayoutParams(-1, -1);
        ViewGroup.LayoutParams layoutParams3 = new ViewGroup.LayoutParams(-2, -2);
        this.a = new RelativeLayout(this);
        this.a.setLayoutParams(layoutParams);
        this.a.setBackgroundColor(-16777216);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(layoutParams3);
        this.a.addView(relativeLayout);
        this.f = new TouchView(this);
        this.f.setLayoutParams(layoutParams2);
        this.f.setScaleType(ImageView.ScaleType.MATRIX);
        relativeLayout.addView(this.f);
        this.i = new MaskView(this);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(layoutParams2);
        layoutParams4.addRule(14, -1);
        layoutParams4.addRule(15, -1);
        this.i.setLayoutParams(layoutParams4);
        relativeLayout.addView(this.i);
        LinearLayout linearLayout = new LinearLayout(this);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, DensityUtil.dip2px(this, 80.0f));
        layoutParams5.addRule(14, -1);
        linearLayout.setLayoutParams(layoutParams5);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        this.a.addView(linearLayout);
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(DensityUtil.dip2px(this, 24.0f), DensityUtil.dip2px(this, 24.0f)));
        imageView.setImageDrawable(b("com.tencent.plus.logo.png"));
        linearLayout.addView(imageView);
        this.j = new TextView(this);
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(layoutParams3);
        layoutParams6.leftMargin = DensityUtil.dip2px(this, 7.0f);
        this.j.setLayoutParams(layoutParams6);
        this.j.setEllipsize(TextUtils.TruncateAt.END);
        this.j.setSingleLine();
        this.j.setTextColor(-1);
        this.j.setTextSize(24.0f);
        this.j.setVisibility(8);
        linearLayout.addView(this.j);
        RelativeLayout relativeLayout2 = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-1, DensityUtil.dip2px(this, 60.0f));
        layoutParams7.addRule(12, -1);
        layoutParams7.addRule(9, -1);
        relativeLayout2.setLayoutParams(layoutParams7);
        relativeLayout2.setBackgroundDrawable(b("com.tencent.plus.bar.png"));
        int dip2px = DensityUtil.dip2px(this, 10.0f);
        relativeLayout2.setPadding(dip2px, dip2px, dip2px, 0);
        this.a.addView(relativeLayout2);
        b bVar = new b(this, this);
        int dip2px2 = DensityUtil.dip2px(this, 14.0f);
        int dip2px3 = DensityUtil.dip2px(this, 7.0f);
        this.h = new Button(this);
        this.h.setLayoutParams(new RelativeLayout.LayoutParams(DensityUtil.dip2px(this, 78.0f), DensityUtil.dip2px(this, 45.0f)));
        this.h.setText("取消");
        this.h.setTextColor(-1);
        this.h.setTextSize(18.0f);
        this.h.setPadding(dip2px2, dip2px3, dip2px2, dip2px3);
        bVar.b(this.h);
        relativeLayout2.addView(this.h);
        this.g = new Button(this);
        RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(DensityUtil.dip2px(this, 78.0f), DensityUtil.dip2px(this, 45.0f));
        layoutParams8.addRule(11, -1);
        this.g.setLayoutParams(layoutParams8);
        this.g.setTextColor(-1);
        this.g.setTextSize(18.0f);
        this.g.setPadding(dip2px2, dip2px3, dip2px2, dip2px3);
        this.g.setText("选取");
        bVar.a(this.g);
        relativeLayout2.addView(this.g);
        TextView textView = new TextView(this);
        RelativeLayout.LayoutParams layoutParams9 = new RelativeLayout.LayoutParams(layoutParams3);
        layoutParams9.addRule(13, -1);
        textView.setLayoutParams(layoutParams9);
        textView.setText("移动和缩放");
        textView.setPadding(0, DensityUtil.dip2px(this, 3.0f), 0, 0);
        textView.setTextSize(18.0f);
        textView.setTextColor(-1);
        relativeLayout2.addView(textView);
        this.k = new ProgressBar(this);
        RelativeLayout.LayoutParams layoutParams10 = new RelativeLayout.LayoutParams(layoutParams3);
        layoutParams10.addRule(14, -1);
        layoutParams10.addRule(15, -1);
        this.k.setLayoutParams(layoutParams10);
        this.k.setVisibility(8);
        this.a.addView(this.k);
        return this.a;
    }

    private void b() {
        try {
            this.t = a(this.s);
            if (this.t == null) {
                throw new IOException("cannot read picture: '" + this.s + "'!");
            }
            this.f.setImageBitmap(this.t);
            this.g.setOnClickListener(this.u);
            this.h.setOnClickListener(this.v);
            this.a.getViewTreeObserver().addOnGlobalLayoutListener(new j(this));
        } catch (IOException e2) {
            e2.printStackTrace();
            b(Constants.MSG_IMAGE_ERROR, 1);
            a(-5, null, Constants.MSG_IMAGE_ERROR, e2.getMessage());
            d();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.plus.ImageActivity.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, android.graphics.Rect):android.graphics.Rect
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
      com.tencent.plus.ImageActivity.a(java.lang.String, int):void
      com.tencent.plus.ImageActivity.a(com.tencent.plus.ImageActivity, boolean):boolean
      com.tencent.plus.ImageActivity.a(java.lang.String, long):void */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setRequestedOrientation(1);
        setContentView(a());
        this.e = new Handler();
        Bundle bundleExtra = getIntent().getBundleExtra(Constants.KEY_PARAMS);
        this.s = bundleExtra.getString(Constants.PARAM_AVATAR_URI);
        this.d = bundleExtra.getString(Constants.PARAM_AVATAR_RETURN_ACTIVITY);
        String string = bundleExtra.getString(Constants.PARAM_APP_ID);
        String string2 = bundleExtra.getString("access_token");
        long j2 = bundleExtra.getLong("expires_in");
        String string3 = bundleExtra.getString("openid");
        this.o = bundleExtra.getInt("exitAnim");
        this.b = new TContext(string, getApplicationContext());
        this.b.a(string2, ((j2 - System.currentTimeMillis()) / 1000) + "");
        this.b.a(string3);
        this.c = new OpenApi(this.b);
        b();
        e();
        this.n = System.currentTimeMillis();
        a("10653", 0L);
    }

    public void onBackPressed() {
        setResult(0);
        d();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.f.setImageBitmap(null);
        if (this.t != null && !this.t.isRecycled()) {
            this.t.recycle();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: private */
    public void c() {
        Matrix imageMatrix = this.f.getImageMatrix();
        float[] fArr = new float[9];
        imageMatrix.getValues(fArr);
        float f2 = fArr[2];
        float f3 = fArr[5];
        float f4 = fArr[0];
        float width = ((float) this.p) / ((float) this.r.width());
        int i2 = (int) ((((float) this.r.left) - f2) / f4);
        int i3 = (int) ((((float) this.r.top) - f3) / f4);
        Matrix matrix = new Matrix();
        matrix.set(imageMatrix);
        matrix.postScale(width, width);
        int i4 = (int) (650.0f / f4);
        Bitmap createBitmap = Bitmap.createBitmap(this.t, i2, i3, Math.min(this.t.getWidth() - i2, i4), Math.min(this.t.getHeight() - i3, i4), matrix, true);
        Bitmap createBitmap2 = Bitmap.createBitmap(createBitmap, 0, 0, this.p, this.q);
        createBitmap.recycle();
        a(createBitmap2);
    }

    private void a(Bitmap bitmap) {
        Bundle bundle = new Bundle();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        bitmap.recycle();
        bundle.putByteArray(Constants.PARAM_AVATAR_URI, byteArray);
        this.c.a(this.b.f(), Constants.GRAPH_SET_AVATAR, bundle, "POST", this.w, null);
    }

    /* access modifiers changed from: private */
    public void a(String str, int i2) {
        this.e.post(new e(this, str, i2));
    }

    /* access modifiers changed from: private */
    public void b(String str, int i2) {
        Toast makeText = Toast.makeText(this, str, 1);
        LinearLayout linearLayout = (LinearLayout) makeText.getView();
        ((TextView) linearLayout.getChildAt(0)).setPadding(8, 0, 0, 0);
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(DensityUtil.dip2px(this, 16.0f), DensityUtil.dip2px(this, 16.0f)));
        if (i2 == 0) {
            imageView.setImageDrawable(b("com.tencent.plus.ic_success.png"));
        } else {
            imageView.setImageDrawable(b("com.tencent.plus.ic_error.png"));
        }
        linearLayout.addView(imageView, 0);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        makeText.setView(linearLayout);
        makeText.setGravity(17, 0, 0);
        makeText.show();
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str, String str2, String str3) {
        Intent intent = new Intent();
        intent.putExtra(Constants.KEY_ERROR_CODE, i2);
        intent.putExtra(Constants.KEY_ERROR_MSG, str2);
        intent.putExtra(Constants.KEY_ERROR_DETAIL, str3);
        intent.putExtra(Constants.KEY_RESPONSE, str);
        setResult(-1, intent);
    }

    /* access modifiers changed from: private */
    public void d() {
        finish();
        if (this.o != 0) {
            overridePendingTransition(0, this.o);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        this.l++;
        this.c.a(this.b.f(), Constants.GRAPH_SIMPLE_USER_INFO, null, "GET", this.x, null);
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        String d2 = d(str);
        if (!"".equals(d2)) {
            this.j.setText(d2);
            this.j.setVisibility(0);
        }
    }

    private String d(String str) {
        return str.replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&quot;", "\"").replaceAll("&#39;", "'").replaceAll("&amp;", "&");
    }

    public void a(String str, long j2) {
        Util.a(this, str, j2, this.b.d());
    }
}
