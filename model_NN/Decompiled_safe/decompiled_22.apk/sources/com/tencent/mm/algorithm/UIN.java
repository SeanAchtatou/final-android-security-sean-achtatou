package com.tencent.mm.algorithm;

import com.tencent.mm.sdk.platformtools.Util;

public class UIN extends Number {
    private int a = 0;

    public UIN(int i) {
        this.a = i;
    }

    public UIN(long j) {
        this.a = (int) (-1 & j);
    }

    public static int valueOf(String str) {
        try {
            return new UIN(Long.valueOf(str).longValue()).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public double doubleValue() {
        return ((double) (((long) this.a) | 0)) + 0.0d;
    }

    public float floatValue() {
        return (float) (((double) (((long) this.a) | 0)) + 0.0d);
    }

    public int intValue() {
        return this.a;
    }

    public long longValue() {
        return ((long) this.a) & Util.MAX_32BIT_VALUE;
    }

    public String toString() {
        return "" + (((long) this.a) & Util.MAX_32BIT_VALUE);
    }

    public int value() {
        return this.a;
    }
}
