package com.tencent.mm.sdk.storage;

public class MStorage {
    private final MStorageEvent<IOnStorageChange, String> a = new MStorageEvent<IOnStorageChange, String>() {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void processEvent(Object obj, Object obj2) {
            ((IOnStorageChange) obj).onNotifyChange((String) obj2);
        }
    };

    public interface IOnStorageChange {
        void onNotifyChange(String str);
    }

    public void add(IOnStorageChange iOnStorageChange) {
        this.a.add(iOnStorageChange);
    }

    public void doNotify() {
        this.a.event("*");
        this.a.doNotify();
    }

    public void doNotify(String str) {
        this.a.event(str);
        this.a.doNotify();
    }

    public void lock() {
        this.a.lock();
    }

    public void remove(IOnStorageChange iOnStorageChange) {
        this.a.remove(iOnStorageChange);
    }

    public void unlock() {
        this.a.unlock();
    }
}
