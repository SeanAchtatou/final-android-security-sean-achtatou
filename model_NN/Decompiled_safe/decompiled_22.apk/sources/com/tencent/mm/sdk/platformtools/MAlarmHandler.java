package com.tencent.mm.sdk.platformtools;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import junit.framework.Assert;

public class MAlarmHandler {
    public static final long NEXT_FIRE_INTERVAL = Long.MAX_VALUE;
    private static int a;
    private static Map<Integer, MAlarmHandler> f = new HashMap();
    private static IBumper h;
    private static boolean i = false;
    private final int b;
    private final boolean c;
    private long d = 0;
    private long e = 0;
    private final CallBack g;

    public interface CallBack {
        boolean onTimerExpired();
    }

    public interface IBumper {
        void cancel();

        void prepare();
    }

    public MAlarmHandler(CallBack callBack, boolean z) {
        Assert.assertTrue("bumper not initialized", i);
        this.g = callBack;
        this.c = z;
        if (a >= 8192) {
            a = 0;
        }
        int i2 = a + 1;
        a = i2;
        this.b = i2;
    }

    public static long fire() {
        LinkedList linkedList = new LinkedList();
        long j = Long.MAX_VALUE;
        for (Map.Entry next : f.entrySet()) {
            MAlarmHandler mAlarmHandler = (MAlarmHandler) next.getValue();
            if (mAlarmHandler != null) {
                long ticksToNow = Util.ticksToNow(mAlarmHandler.d);
                if (ticksToNow < 0) {
                    ticksToNow = 0;
                }
                if (ticksToNow > mAlarmHandler.e) {
                    if (!mAlarmHandler.g.onTimerExpired() || !mAlarmHandler.c) {
                        linkedList.add(next.getKey());
                    } else {
                        j = mAlarmHandler.e;
                    }
                    mAlarmHandler.d = Util.currentTicks();
                } else if (mAlarmHandler.e - ticksToNow < j) {
                    j = mAlarmHandler.e - ticksToNow;
                }
            }
            j = j;
        }
        for (int i2 = 0; i2 < linkedList.size(); i2++) {
            f.remove(linkedList.get(i2));
        }
        if (j == Long.MAX_VALUE && h != null) {
            h.cancel();
            Log.v("MicroMsg.MAlarmHandler", "cancel bumper for no more handler");
        }
        return j;
    }

    public static void initAlarmBumper(IBumper iBumper) {
        i = true;
        h = iBumper;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        stopTimer();
        super.finalize();
    }

    public void startTimer(long j) {
        long j2;
        this.e = j;
        this.d = Util.currentTicks();
        long j3 = this.e;
        Log.d("MicroMsg.MAlarmHandler", "check need prepare: check=" + j3);
        long j4 = Long.MAX_VALUE;
        Iterator<Map.Entry<Integer, MAlarmHandler>> it = f.entrySet().iterator();
        while (true) {
            j2 = j4;
            if (!it.hasNext()) {
                break;
            }
            MAlarmHandler mAlarmHandler = (MAlarmHandler) it.next().getValue();
            if (mAlarmHandler != null) {
                long ticksToNow = Util.ticksToNow(mAlarmHandler.d);
                if (ticksToNow < 0) {
                    ticksToNow = 0;
                }
                if (ticksToNow > mAlarmHandler.e) {
                    j4 = mAlarmHandler.e;
                } else if (mAlarmHandler.e - ticksToNow < j2) {
                    j2 = mAlarmHandler.e - ticksToNow;
                }
            }
            j4 = j2;
        }
        boolean z = j2 > j3;
        stopTimer();
        f.put(Integer.valueOf(this.b), this);
        if (h != null && z) {
            Log.v("MicroMsg.MAlarmHandler", "prepare bumper");
            h.prepare();
        }
    }

    public void stopTimer() {
        f.remove(Integer.valueOf(this.b));
    }

    public boolean stopped() {
        return !f.containsKey(Integer.valueOf(this.b));
    }
}
