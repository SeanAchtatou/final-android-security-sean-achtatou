package com.tencent.mm.sdk.platformtools;

import android.os.Build;
import com.tencent.mm.algorithm.MD5;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

public class Log {
    public static final int LEVEL_DEBUG = 1;
    public static final int LEVEL_ERROR = 4;
    public static final int LEVEL_INFO = 2;
    public static final int LEVEL_NONE = 5;
    public static final int LEVEL_VERBOSE = 0;
    public static final int LEVEL_WARNING = 3;
    public static final String MM_LOG = "mm.log";
    public static final String PUSH_LOG = "push.log";
    public static final String SDCARD_LOG_PATH = "/sdcard/tencent/";
    public static final String SYS_LOG = "sys.log";
    private static int a = 0;
    private static String b;
    private static PrintStream c;
    private static long d = 0;
    private static byte[] e = null;
    private static final String f;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("VERSION.RELEASE:[" + Build.VERSION.RELEASE);
        sb.append("] VERSION.CODENAME:[" + Build.VERSION.CODENAME);
        sb.append("] VERSION.INCREMENTAL:[" + Build.VERSION.INCREMENTAL);
        sb.append("] BOARD:[" + Build.BOARD);
        sb.append("] DEVICE:[" + Build.DEVICE);
        sb.append("] DISPLAY:[" + Build.DISPLAY);
        sb.append("] FINGERPRINT:[" + Build.FINGERPRINT);
        sb.append("] HOST:[" + Build.HOST);
        sb.append("] MANUFACTURER:[" + Build.MANUFACTURER);
        sb.append("] MODEL:[" + Build.MODEL);
        sb.append("] PRODUCT:[" + Build.PRODUCT);
        sb.append("] TAGS:[" + Build.TAGS);
        sb.append("] TYPE:[" + Build.TYPE);
        sb.append("] USER:[" + Build.USER + "]");
        f = sb.toString();
    }

    protected Log() {
    }

    public static void d(String str, String str2) {
        d(str, str2, null);
    }

    public static void d(String str, String str2, Object... objArr) {
        if (a <= 1) {
            if (objArr != null) {
                str2 = String.format(str2, objArr);
            }
            android.util.Log.d(str, str2);
            LogHelper.writeToStream(c, e, "D/" + str, str2);
        }
    }

    public static void e(String str, String str2) {
        e(str, str2, null);
    }

    public static void e(String str, String str2, Object... objArr) {
        if (a <= 4) {
            if (objArr != null) {
                str2 = String.format(str2, objArr);
            }
            android.util.Log.e(str, str2);
            LogHelper.writeToStream(c, e, "E/" + str, str2);
        }
    }

    public static int getLevel() {
        return a;
    }

    public static String getSysInfo() {
        return f;
    }

    public static void i(String str, String str2) {
        i(str, str2, null);
    }

    public static void i(String str, String str2, Object... objArr) {
        if (a <= 2) {
            if (objArr != null) {
                str2 = String.format(str2, objArr);
            }
            android.util.Log.i(str, str2);
            LogHelper.writeToStream(c, e, "I/" + str, str2);
        }
    }

    public static void reset() {
        b = null;
        c = null;
        e = null;
    }

    public static void setLevel(int i, boolean z) {
        a = i;
        android.util.Log.w("MicroMsg.SDK.Log", "new log level: " + i);
        if (z) {
            android.util.Log.e("MicroMsg.SDK.Log", "no jni log level support");
        }
    }

    public static void setOutputPath(String str, String str2, String str3, int i) {
        if (str != null && str.length() != 0 && str3 != null && str3.length() != 0) {
            try {
                b = str;
                File file = new File(str);
                if (file.exists()) {
                    c = new PrintStream(new BufferedOutputStream(new FileOutputStream(str)));
                    if (file.length() == 0) {
                        d = System.currentTimeMillis();
                        LogHelper.initLogHeader(c, str2, str3, d, i);
                        long j = d;
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append(str3);
                        stringBuffer.append(j);
                        stringBuffer.append("dfdhgc");
                        e = MD5.getMessageDigest(stringBuffer.toString().getBytes()).substring(7, 21).getBytes();
                        return;
                    }
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(b));
                        bufferedReader.readLine();
                        d = Long.parseLong(bufferedReader.readLine());
                        bufferedReader.close();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (FileNotFoundException e3) {
                e3.printStackTrace();
            }
        }
    }

    public static void v(String str, String str2) {
        v(str, str2, null);
    }

    public static void v(String str, String str2, Object... objArr) {
        if (a <= 0) {
            if (objArr != null) {
                str2 = String.format(str2, objArr);
            }
            android.util.Log.v(str, str2);
            LogHelper.writeToStream(c, e, "V/" + str, str2);
        }
    }

    public static void w(String str, String str2) {
        w(str, str2, null);
    }

    public static void w(String str, String str2, Object... objArr) {
        if (a <= 3) {
            if (objArr != null) {
                str2 = String.format(str2, objArr);
            }
            android.util.Log.w(str, str2);
            LogHelper.writeToStream(c, e, "W/" + str, str2);
        }
    }
}
