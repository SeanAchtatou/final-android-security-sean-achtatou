package com.tencent.mm.sdk.platformtools;

import android.os.Handler;
import android.os.Message;

public class MTimerHandler extends Handler {
    private static int a;
    private final int b;
    private final boolean c;
    private long d = 0;
    private final CallBack e;

    public interface CallBack {
        boolean onTimerExpired();
    }

    public MTimerHandler(CallBack callBack, boolean z) {
        this.e = callBack;
        if (a >= 8192) {
            a = 0;
        }
        int i = a + 1;
        a = i;
        this.b = i;
        this.c = z;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        stopTimer();
        super.finalize();
    }

    public void handleMessage(Message message) {
        if (message.what == this.b && this.e != null && this.e.onTimerExpired() && this.c) {
            sendEmptyMessageDelayed(this.b, this.d);
        }
    }

    public void startTimer(long j) {
        this.d = j;
        stopTimer();
        sendEmptyMessageDelayed(this.b, j);
    }

    public void stopTimer() {
        removeMessages(this.b);
    }

    public boolean stopped() {
        return !hasMessages(this.b);
    }
}
