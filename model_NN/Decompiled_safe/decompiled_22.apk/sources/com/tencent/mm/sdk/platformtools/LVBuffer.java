package com.tencent.mm.sdk.platformtools;

import java.nio.ByteBuffer;

public class LVBuffer {
    public static final int LENGTH_ALLOC_PER_NEW = 4096;
    public static final int MAX_STRING_LENGTH = 2048;
    private ByteBuffer a;
    private boolean b;

    private int a(int i) {
        if (this.a.limit() - this.a.position() <= i) {
            ByteBuffer allocate = ByteBuffer.allocate(this.a.limit() + 4096);
            allocate.put(this.a.array(), 0, this.a.position());
            this.a = allocate;
        }
        return 0;
    }

    public byte[] buildFinish() {
        if (!this.b) {
            throw new Exception("Buffer For Parse");
        }
        a(1);
        this.a.put((byte) 125);
        byte[] bArr = new byte[this.a.position()];
        System.arraycopy(this.a.array(), 0, bArr, 0, bArr.length);
        return bArr;
    }

    public boolean checkGetFinish() {
        return this.a.limit() - this.a.position() <= 1;
    }

    public int getInt() {
        if (!this.b) {
            return this.a.getInt();
        }
        throw new Exception("Buffer For Build");
    }

    public long getLong() {
        if (!this.b) {
            return this.a.getLong();
        }
        throw new Exception("Buffer For Build");
    }

    public String getString() {
        if (this.b) {
            throw new Exception("Buffer For Build");
        }
        int i = this.a.getShort();
        if (i > 2048) {
            this.a = null;
            throw new Exception("Buffer String Length Error");
        } else if (i == 0) {
            return "";
        } else {
            byte[] bArr = new byte[i];
            this.a.get(bArr, 0, i);
            return new String(bArr);
        }
    }

    public int initBuild() {
        this.a = ByteBuffer.allocate(4096);
        this.a.put((byte) 123);
        this.b = true;
        return 0;
    }

    public int initParse(byte[] bArr) {
        int i = (bArr == null || bArr.length == 0) ? -1 : bArr[0] != 123 ? -2 : bArr[bArr.length + -1] != 125 ? -3 : 0;
        if (i != 0) {
            this.a = null;
            throw new Exception("Parse Buffer Check Failed :" + i);
        }
        this.a = ByteBuffer.wrap(bArr);
        this.a.position(1);
        this.b = false;
        return 0;
    }

    public int putInt(int i) {
        if (!this.b) {
            throw new Exception("Buffer For Parse");
        }
        a(4);
        this.a.putInt(i);
        return 0;
    }

    public int putLong(long j) {
        if (!this.b) {
            throw new Exception("Buffer For Parse");
        }
        a(8);
        this.a.putLong(j);
        return 0;
    }

    public int putString(String str) {
        if (!this.b) {
            throw new Exception("Buffer For Parse");
        }
        byte[] bArr = null;
        if (str != null) {
            bArr = str.getBytes();
        }
        if (bArr == null) {
            bArr = new byte[0];
        }
        if (bArr.length > 2048) {
            throw new Exception("Buffer String Length Error");
        }
        a(bArr.length + 2);
        this.a.putShort((short) bArr.length);
        if (bArr.length > 0) {
            this.a.put(bArr);
        }
        return 0;
    }
}
