package com.tencent.mm.sdk.platformtools;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import com.tencent.mm.sdk.platformtools.MTimerHandler;
import com.tencent.mm.sdk.platformtools.PhoneUtil;
import java.util.LinkedList;
import java.util.List;

public class LBSManager extends BroadcastReceiver {
    public static final String FILTER_GPS = "filter_gps";
    public static final int INVALID_ACC = -1000;
    public static final float INVALID_LAT = -1000.0f;
    public static final float INVALID_LNG = -1000.0f;
    public static final int MM_SOURCE_HARDWARE = 0;
    public static final int MM_SOURCE_NET = 1;
    public static final int MM_SOURCE_REPORT_HARWARE = 3;
    public static final int MM_SOURCE_REPORT_NETWORK = 4;
    private static LocationCache b;
    boolean a;
    private OnLocationGotListener c;
    private LocationManager d;
    private Context e;
    private PendingIntent f;
    /* access modifiers changed from: private */
    public boolean g = false;
    private boolean h = false;
    private boolean i = false;
    private int j;
    private MTimerHandler k = new MTimerHandler(new MTimerHandler.CallBack() {
        public boolean onTimerExpired() {
            Log.v("MicroMsg.LBSManager", "get location by GPS failed.");
            LBSManager.this.a = true;
            LBSManager.this.start();
            boolean unused = LBSManager.this.g = false;
            return false;
        }
    }, false);

    static class LocationCache {
        float a = -1000.0f;
        float b = -1000.0f;
        int c = LBSManager.INVALID_ACC;
        long d;
        int e = 1;

        LocationCache() {
        }
    }

    public interface OnLocationGotListener {
        void onLocationGot(float f, float f2, int i, int i2, String str, String str2, boolean z);
    }

    public LBSManager(Context context, OnLocationGotListener onLocationGotListener) {
        this.c = onLocationGotListener;
        this.a = false;
        this.j = 0;
        this.e = context;
        PhoneUtil.getSignalStrength(context);
        this.d = (LocationManager) context.getSystemService("location");
        a();
        this.f = PendingIntent.getBroadcast(context, 0, new Intent(FILTER_GPS), 134217728);
    }

    private boolean a() {
        if (this.d == null) {
            return false;
        }
        try {
            this.d.sendExtraCommand("gps", "force_xtra_injection", null);
            this.d.sendExtraCommand("gps", "force_time_injection", null);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    private void b() {
        this.k.stopTimer();
        this.a = true;
    }

    public static void setLocationCache(float f2, float f3, int i2, int i3) {
        if (i2 != 0) {
            Log.v("MicroMsg.LBSManager", "setLocationCache [" + f2 + "," + f3 + "] acc:" + i2 + " source:" + i3);
            if (b == null) {
                b = new LocationCache();
            }
            b.a = f2;
            b.b = f3;
            b.c = i2;
            b.d = System.currentTimeMillis();
            b.e = i3;
        }
    }

    public String getTelLocation() {
        return PhoneUtil.getCellXml(PhoneUtil.getCellInfoList(this.e));
    }

    public String getWIFILocation() {
        WifiManager wifiManager = (WifiManager) this.e.getSystemService("wifi");
        if (wifiManager == null) {
            Log.e("MicroMsg.LBSManager", "no wifi service");
            return "";
        } else if (wifiManager.getConnectionInfo() == null) {
            Log.e("MicroMsg.LBSManager", "WIFILocation wifi info null");
            return "";
        } else {
            LinkedList linkedList = new LinkedList();
            List<ScanResult> scanResults = wifiManager.getScanResults();
            if (scanResults != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= scanResults.size()) {
                        break;
                    }
                    linkedList.add(new PhoneUtil.MacInfo(scanResults.get(i3).BSSID, scanResults.get(i3).level + ""));
                    i2 = i3 + 1;
                }
            }
            return PhoneUtil.getMacXml(linkedList);
        }
    }

    public boolean isGpsEnable() {
        try {
            return this.d.isProviderEnabled("gps");
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public boolean isNetworkPrividerEnable() {
        try {
            return this.d.isProviderEnabled("network");
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public void onReceive(Context context, Intent intent) {
        Location location = (Location) intent.getExtras().get("location");
        this.j++;
        if (location != null) {
            boolean equals = "gps".equals(location.getProvider());
            if (((equals && location.getAccuracy() <= 200.0f) || (!equals && location.getAccuracy() <= 1000.0f)) && location.getAccuracy() > 0.0f) {
                int i2 = equals ? 0 : 1;
                setLocationCache((float) location.getLatitude(), (float) location.getLongitude(), (int) location.getAccuracy(), i2);
                if (this.c == null) {
                    return;
                }
                if (!this.a || !this.h || !this.i) {
                    String nullAsNil = Util.nullAsNil(getWIFILocation());
                    String nullAsNil2 = Util.nullAsNil(getTelLocation());
                    if (!this.a) {
                        b();
                        this.a = true;
                        Log.v("MicroMsg.LBSManager", "location by provider ok:[" + location.getLatitude() + " , " + location.getLongitude() + "]  " + "accuracy:" + location.getAccuracy() + "  retry count:" + this.j + " isGpsProvider:" + equals);
                        this.c.onLocationGot((float) location.getLatitude(), (float) location.getLongitude(), (int) location.getAccuracy(), i2, nullAsNil, nullAsNil2, true);
                    } else if (!this.h && i2 == 0) {
                        this.h = true;
                        Log.v("MicroMsg.LBSManager", "report location by GPS ok:[" + location.getLatitude() + " , " + location.getLongitude() + "]  " + "accuracy:" + location.getAccuracy() + "  retry count:" + this.j + " isGpsProvider:" + equals);
                        this.c.onLocationGot((float) location.getLatitude(), (float) location.getLongitude(), (int) location.getAccuracy(), 3, nullAsNil, nullAsNil2, true);
                    } else if (!this.i && i2 == 1) {
                        this.i = true;
                        Log.v("MicroMsg.LBSManager", "report location by Network ok:[" + location.getLatitude() + " , " + location.getLongitude() + "]  " + "accuracy:" + location.getAccuracy() + "  retry count:" + this.j + " isGpsProvider:" + equals);
                        this.c.onLocationGot((float) location.getLatitude(), (float) location.getLongitude(), (int) location.getAccuracy(), 4, nullAsNil, nullAsNil2, true);
                    }
                }
            }
        }
    }

    public void removeGpsUpdate() {
        Log.v("MicroMsg.LBSManager", "removed gps update");
        if (this.d != null) {
            this.d.removeUpdates(this.f);
        }
        try {
            this.e.unregisterReceiver(this);
        } catch (Exception e2) {
            Log.v("MicroMsg.LBSManager", "sensor receiver has already unregistered");
        }
    }

    public void removeListener() {
        if (this.d != null) {
            this.d.removeUpdates(this.f);
            Log.v("MicroMsg.LBSManager", "removed gps update on destroy");
        }
        if (this.k != null) {
            b();
        }
        this.c = null;
        this.e = null;
        this.k = null;
        this.d = null;
    }

    public void requestGpsUpdate() {
        if (isGpsEnable() || isNetworkPrividerEnable()) {
            Log.v("MicroMsg.LBSManager", "requested gps update");
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(FILTER_GPS);
            this.e.registerReceiver(this, intentFilter);
            if (isGpsEnable()) {
                this.d.requestLocationUpdates("gps", 500, 0.0f, this.f);
            }
            if (isNetworkPrividerEnable()) {
                this.d.requestLocationUpdates("network", 500, 0.0f, this.f);
            }
        }
    }

    public void start() {
        String nullAsNil = Util.nullAsNil(getWIFILocation());
        String nullAsNil2 = Util.nullAsNil(getTelLocation());
        if (!(isGpsEnable() || isNetworkPrividerEnable()) || this.g) {
            if (!(b == null ? false : System.currentTimeMillis() - b.d <= 180000 && b.c > 0)) {
                this.a = true;
                if (!nullAsNil.equals("") || !nullAsNil2.equals("")) {
                    Log.v("MicroMsg.LBSManager", "get location by network ok, macs : " + nullAsNil + " cell ids :" + nullAsNil2);
                    if (this.c != null) {
                        this.c.onLocationGot(-1000.0f, -1000.0f, INVALID_ACC, 0, nullAsNil, nullAsNil2, true);
                        return;
                    }
                    return;
                }
                Log.v("MicroMsg.LBSManager", "get location by network failed");
                if (this.c != null) {
                    this.c.onLocationGot(-1000.0f, -1000.0f, INVALID_ACC, 0, "", "", false);
                }
            } else if (this.c != null) {
                this.a = true;
                Log.v("MicroMsg.LBSManager", "location by GPS cache ok:[" + b.a + " , " + b.b + "]  " + "accuracy:" + b.c + " source:" + b.e);
                this.c.onLocationGot(b.a, b.b, b.c, b.e, nullAsNil, nullAsNil2, true);
            }
        } else {
            this.g = true;
            this.j = 0;
            requestGpsUpdate();
            this.k.startTimer(3000);
        }
    }
}
