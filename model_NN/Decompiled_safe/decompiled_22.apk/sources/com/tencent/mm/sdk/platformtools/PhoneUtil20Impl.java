package com.tencent.mm.sdk.platformtools;

import android.content.Context;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.tencent.mm.sdk.platformtools.PhoneUtil;
import com.utils.FinalVariable;
import java.util.LinkedList;
import java.util.List;

class PhoneUtil20Impl {
    private static int a = FinalVariable.vb_success;
    /* access modifiers changed from: private */
    public static int b = FinalVariable.vb_success;
    /* access modifiers changed from: private */
    public TelephonyManager c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public PhoneStateListener e = new PhoneStateListener() {
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            if (PhoneUtil20Impl.this.d == 2) {
                int unused = PhoneUtil20Impl.b = signalStrength.getCdmaDbm();
            }
            if (PhoneUtil20Impl.this.d == 1) {
                int unused2 = PhoneUtil20Impl.b = (signalStrength.getGsmSignalStrength() * 2) - 113;
            }
            if (PhoneUtil20Impl.this.c != null) {
                PhoneUtil20Impl.this.c.listen(PhoneUtil20Impl.this.e, 0);
            }
        }
    };

    PhoneUtil20Impl() {
    }

    public List<PhoneUtil.CellInfo> getCellInfoList(Context context) {
        String str;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        LinkedList linkedList = new LinkedList();
        String str2 = "460";
        String str3 = "";
        try {
            String networkOperator = telephonyManager.getNetworkOperator();
            if (networkOperator == null || networkOperator.equals("")) {
                String simOperator = telephonyManager.getSimOperator();
                if (simOperator != null && !simOperator.equals("")) {
                    str2 = simOperator.substring(0, 3);
                    str3 = simOperator.substring(3, 5);
                }
                str = str3;
            } else {
                str2 = networkOperator.substring(0, 3);
                str = networkOperator.substring(3, 5);
            }
            if (telephonyManager.getPhoneType() == 2) {
                try {
                    CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) telephonyManager.getCellLocation();
                    if (cdmaCellLocation != null) {
                        String str4 = b == a ? "" : b + "";
                        if (!(cdmaCellLocation.getBaseStationId() == -1 || cdmaCellLocation.getNetworkId() == -1 || cdmaCellLocation.getSystemId() == -1)) {
                            linkedList.add(new PhoneUtil.CellInfo(str2, str, "", "", str4, PhoneUtil.CELL_CDMA, cdmaCellLocation.getBaseStationId() + "", cdmaCellLocation.getNetworkId() + "", cdmaCellLocation.getSystemId() + ""));
                        }
                    }
                } catch (Exception e2) {
                    try {
                        GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
                        if (gsmCellLocation != null) {
                            int cid = gsmCellLocation.getCid();
                            int lac = gsmCellLocation.getLac();
                            if (!(lac >= 65535 || lac == -1 || cid == -1)) {
                                linkedList.add(new PhoneUtil.CellInfo(str2, str, lac + "", cid + "", "", PhoneUtil.CELL_GSM, "", "", ""));
                            }
                            Log.v("checked", "lac:" + lac + "  cid:" + cid);
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    List<NeighboringCellInfo> neighboringCellInfo = telephonyManager.getNeighboringCellInfo();
                    if (neighboringCellInfo != null && neighboringCellInfo.size() > 0) {
                        for (NeighboringCellInfo neighboringCellInfo2 : neighboringCellInfo) {
                            if (!(neighboringCellInfo2.getCid() == -1 || neighboringCellInfo2.getLac() > 65535 || neighboringCellInfo2.getLac() == -1)) {
                                String str5 = ((neighboringCellInfo2.getRssi() * 2) - 113) + "";
                                Log.v("checked", "lac:" + neighboringCellInfo2.getLac() + "  cid:" + neighboringCellInfo2.getCid() + " dbm:" + str5);
                                linkedList.add(new PhoneUtil.CellInfo(str2, str, neighboringCellInfo2.getLac() + "", neighboringCellInfo2.getCid() + "", str5, PhoneUtil.CELL_GSM, "", "", ""));
                            }
                        }
                    }
                }
            } else {
                try {
                    GsmCellLocation gsmCellLocation2 = (GsmCellLocation) telephonyManager.getCellLocation();
                    if (gsmCellLocation2 != null) {
                        int cid2 = gsmCellLocation2.getCid();
                        int lac2 = gsmCellLocation2.getLac();
                        if (!(lac2 >= 65535 || lac2 == -1 || cid2 == -1)) {
                            linkedList.add(new PhoneUtil.CellInfo(str2, str, lac2 + "", cid2 + "", b == a ? "" : b + "", PhoneUtil.CELL_GSM, "", "", ""));
                        }
                        Log.v("checked", "lac:" + lac2 + "  cid:" + cid2);
                    }
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
                List<NeighboringCellInfo> neighboringCellInfo3 = telephonyManager.getNeighboringCellInfo();
                if (neighboringCellInfo3 != null && neighboringCellInfo3.size() > 0) {
                    for (NeighboringCellInfo neighboringCellInfo4 : neighboringCellInfo3) {
                        if (neighboringCellInfo4.getCid() != -1 && neighboringCellInfo4.getLac() <= 65535) {
                            String str6 = ((neighboringCellInfo4.getRssi() * 2) - 113) + "";
                            Log.v("checked", "lac:" + neighboringCellInfo4.getLac() + "  cid:" + neighboringCellInfo4.getCid() + " dbm:" + str6);
                            linkedList.add(new PhoneUtil.CellInfo(str2, str, neighboringCellInfo4.getLac() + "", neighboringCellInfo4.getCid() + "", str6, PhoneUtil.CELL_GSM, "", "", ""));
                        }
                    }
                }
            }
            return linkedList;
        } catch (Exception e5) {
            e5.printStackTrace();
            return linkedList;
        }
    }

    public void getSignalStrength(Context context) {
        this.c = (TelephonyManager) context.getSystemService("phone");
        this.c.listen(this.e, 256);
        this.d = this.c.getPhoneType();
    }
}
