package com.tencent.mm.sdk.platformtools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorController extends BroadcastReceiver implements SensorEventListener {
    private static float a = 4.2949673E9f;
    private static float d = 0.5f;
    private SensorManager b;
    private float c;
    private SensorEventCallBack e;
    private Sensor f;
    private final boolean g;
    private boolean h = false;
    private boolean i = false;

    public interface SensorEventCallBack {
        void onSensorEvent(boolean z);
    }

    public SensorController(Context context) {
        this.b = (SensorManager) context.getSystemService("sensor");
        this.f = this.b.getDefaultSensor(8);
        this.g = this.f != null;
        this.c = d + 1.0f;
    }

    public boolean isSensorEnable() {
        return this.g;
    }

    public void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.HEADSET_PLUG")) {
            int intExtra = intent.getIntExtra("state", 0);
            if (intExtra == 1) {
                this.h = true;
            }
            if (intExtra == 0) {
                this.h = false;
            }
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (!this.h) {
            float f2 = sensorEvent.values[0];
            switch (sensorEvent.sensor.getType()) {
                case 8:
                    if (f2 < a) {
                        a = f2;
                        d = 0.5f + f2;
                    }
                    if (this.c < d || f2 >= d) {
                        if (this.c <= d && f2 > d && this.e != null) {
                            Log.v("MicroMsg.SensorController", "sensor event true");
                            this.e.onSensorEvent(true);
                        }
                    } else if (this.e != null) {
                        Log.v("MicroMsg.SensorController", "sensor event false");
                        this.e.onSensorEvent(false);
                    }
                    this.c = f2;
                    return;
                default:
                    return;
            }
        }
    }

    public void removeSensorCallBack() {
        Log.v("MicroMsg.SensorController", "sensor callback removed");
        this.b.unregisterListener(this, this.f);
        this.b.unregisterListener(this);
        this.i = false;
        this.e = null;
    }

    public void setSensorCallBack(SensorEventCallBack sensorEventCallBack) {
        Log.v("MicroMsg.SensorController", "sensor callback set");
        if (!this.i) {
            this.b.registerListener(this, this.f, 2);
            this.i = true;
        }
        this.e = sensorEventCallBack;
    }
}
