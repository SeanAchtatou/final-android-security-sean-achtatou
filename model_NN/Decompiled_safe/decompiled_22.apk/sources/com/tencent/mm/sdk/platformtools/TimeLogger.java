package com.tencent.mm.sdk.platformtools;

import android.os.SystemClock;
import java.util.ArrayList;

public class TimeLogger {
    private String a;
    private String b;
    private boolean c;
    private ArrayList<Long> d;
    private ArrayList<String> e;

    public TimeLogger(String str, String str2) {
        reset(str, str2);
    }

    public void addSplit(String str) {
        if (!this.c) {
            this.d.add(Long.valueOf(SystemClock.elapsedRealtime()));
            this.e.add(str);
        }
    }

    public void dumpToLog() {
        if (!this.c) {
            Log.d(this.a, this.b + ": begin");
            long longValue = this.d.get(0).longValue();
            int i = 1;
            long j = longValue;
            while (i < this.d.size()) {
                long longValue2 = this.d.get(i).longValue();
                long longValue3 = this.d.get(i - 1).longValue();
                Log.d(this.a, this.b + ":      " + (longValue2 - longValue3) + " ms, " + this.e.get(i));
                i++;
                j = longValue2;
            }
            Log.d(this.a, this.b + ": end, " + (j - longValue) + " ms");
        }
    }

    public void reset() {
        this.c = false;
        if (!this.c) {
            if (this.d == null) {
                this.d = new ArrayList<>();
                this.e = new ArrayList<>();
            } else {
                this.d.clear();
                this.e.clear();
            }
            addSplit(null);
        }
    }

    public void reset(String str, String str2) {
        this.a = str;
        this.b = str2;
        reset();
    }
}
