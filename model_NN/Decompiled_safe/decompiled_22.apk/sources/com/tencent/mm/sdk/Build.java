package com.tencent.mm.sdk;

public final class Build {
    public static final int SDK_INT = 553779201;
    public static final String SDK_VERSION_NAME = "android 1.2.1 rev 1";
    public static final int TIMELINE_SUPPORTED_SDK_INT = 553779201;

    private Build() {
    }
}
