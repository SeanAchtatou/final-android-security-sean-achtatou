package com.tencent.mm.sdk.platformtools;

import java.util.HashSet;
import java.util.Set;

public final class MMEntryLock {
    private static Set<String> a = new HashSet();

    private MMEntryLock() {
    }

    public static boolean isLocked(String str) {
        return a.contains(str);
    }

    public static boolean lock(String str) {
        if (isLocked(str)) {
            Log.d("MicroMsg.MMEntryLock", "locked-" + str);
            return false;
        }
        Log.d("MicroMsg.MMEntryLock", "lock-" + str);
        return a.add(str);
    }

    public static void unlock(String str) {
        a.remove(str);
        Log.d("MicroMsg.MMEntryLock", "unlock-" + str);
    }
}
