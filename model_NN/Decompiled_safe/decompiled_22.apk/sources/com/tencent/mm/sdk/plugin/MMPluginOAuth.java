package com.tencent.mm.sdk.plugin;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import com.tencent.mm.sdk.platformtools.Log;
import com.tencent.mm.sdk.plugin.MMPluginProviderConstants;
import java.util.HashMap;
import java.util.Map;

public class MMPluginOAuth {
    /* access modifiers changed from: private */
    public final IResult a;
    private final Context b;
    private String c;
    /* access modifiers changed from: private */
    public String d;
    private Handler e;

    public interface IResult {
        void onResult(MMPluginOAuth mMPluginOAuth);

        void onSessionTimeOut();
    }

    public static class Receiver extends BroadcastReceiver {
        private static final Map<String, MMPluginOAuth> a = new HashMap();
        private final MMPluginOAuth b;

        public Receiver() {
            this(null);
        }

        public Receiver(MMPluginOAuth mMPluginOAuth) {
            this.b = mMPluginOAuth;
        }

        public static void register(String str, MMPluginOAuth mMPluginOAuth) {
            a.put(str, mMPluginOAuth);
        }

        public static void unregister(String str) {
            a.remove(str);
        }

        public void onReceive(Context context, Intent intent) {
            final MMPluginOAuth mMPluginOAuth;
            Log.d("MicroMsg.SDK.MMPluginOAuth", "receive oauth result");
            String stringExtra = intent.getStringExtra(MMPluginProviderConstants.PluginIntent.REQUEST_TOKEN);
            final String stringExtra2 = intent.getStringExtra(MMPluginProviderConstants.PluginIntent.ACCESS_TOKEN);
            if (this.b != null) {
                mMPluginOAuth = this.b;
            } else {
                mMPluginOAuth = a.get(stringExtra);
                if (mMPluginOAuth == null) {
                    Log.e("MicroMsg.SDK.MMPluginOAuth", "oauth unregistered, request token = " + stringExtra);
                    return;
                }
                unregister(mMPluginOAuth.d);
            }
            new Handler().post(new Runnable() {
                public void run() {
                    MMPluginOAuth.a(mMPluginOAuth, stringExtra2);
                }
            });
        }
    }

    public MMPluginOAuth(Context context, IResult iResult) {
        this.b = context;
        this.a = iResult;
    }

    static /* synthetic */ void a(MMPluginOAuth mMPluginOAuth, String str) {
        Receiver.unregister(mMPluginOAuth.d);
        mMPluginOAuth.c = str;
        Log.i("MicroMsg.SDK.MMPluginOAuth", "access token: " + str);
        if (mMPluginOAuth.a != null) {
            mMPluginOAuth.a.onResult(mMPluginOAuth);
        }
    }

    public String getAccessToken() {
        return this.c;
    }

    public String getRequestToken() {
        return this.d;
    }

    public void start() {
        start(null);
    }

    public boolean start(Handler handler) {
        boolean z;
        if (handler == null) {
            handler = new Handler();
        }
        this.e = handler;
        Cursor query = this.b.getContentResolver().query(MMPluginProviderConstants.OAuth.CONTENT_URI, null, null, new String[]{this.b.getPackageName(), MMPluginProviderConstants.OAuth.ACTION_REQUEST_TOKEN}, null);
        if (query != null) {
            if (query.moveToFirst() && query.getColumnCount() >= 2) {
                this.d = query.getString(0);
                this.c = query.getString(1);
            }
            query.close();
        }
        Log.i("MicroMsg.SDK.MMPluginOAuth", "request token = " + this.d);
        if (this.d == null) {
            Log.e("MicroMsg.SDK.MMPluginOAuth", "request token failed");
            return false;
        } else if (this.c != null) {
            this.e.post(new Runnable() {
                public void run() {
                    if (MMPluginOAuth.this.a != null) {
                        MMPluginOAuth.this.a.onResult(MMPluginOAuth.this);
                    }
                }
            });
            return true;
        } else {
            Log.d("MicroMsg.SDK.MMPluginOAuth", "begin to show user oauth page");
            Intent intent = new Intent();
            intent.setClassName(MMPluginProviderConstants.PluginIntent.APP_PACKAGE_PATTERN, "com.tencent.mm.plugin.PluginOAuthUI");
            intent.putExtra(MMPluginProviderConstants.PluginIntent.REQUEST_TOKEN, this.d);
            intent.putExtra(MMPluginProviderConstants.PluginIntent.PACKAGE, this.b.getPackageName());
            if (this.b.getPackageManager().resolveActivity(intent, 65536) == null) {
                Log.e("MicroMsg.SDK.MMPluginOAuth", "show oauth page failed, activity not found");
                z = false;
            } else {
                if (!(this.b instanceof Activity)) {
                    intent.setFlags(268435456);
                }
                this.b.startActivity(intent);
                z = true;
            }
            if (!z) {
                return false;
            }
            Receiver.register(this.d, this);
            return true;
        }
    }
}
