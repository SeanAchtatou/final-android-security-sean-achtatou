package com.tencent.mm.sdk.platformtools;

import android.os.Process;
import java.io.File;
import java.util.Scanner;

public final class TrafficStats {
    public static final String DEV_FILE = "/proc/self/net/dev";
    public static final String GPRSLINE = "rmnet0";
    public static final String WIFILINE = "tiwlan0";
    private static long a;
    private static long b;
    private static long c;
    private static long d;
    private static long e;
    private static long f;
    private static long g;
    private static long h;

    private TrafficStats() {
    }

    public static long getMobileRx(long j) {
        return f > j ? f : j;
    }

    public static long getMobileTx(long j) {
        return e > j ? e : j;
    }

    public static long getWifiRx(long j) {
        return h > j ? h : j;
    }

    public static long getWifiTx(long j) {
        return g > j ? g : j;
    }

    public static void reset() {
        a = -1;
        b = -1;
        c = -1;
        d = -1;
        update();
    }

    public static void update() {
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        try {
            Scanner scanner = new Scanner(new File("/proc/" + Process.myPid() + "/net/dev"));
            scanner.nextLine();
            scanner.nextLine();
            while (scanner.hasNext()) {
                String[] split = scanner.nextLine().split("[ :\t]+");
                int i = split[0].length() == 0 ? 1 : 0;
                if (!split[0].equals("lo") && split[i + 0].startsWith("rmnet")) {
                    j += Long.parseLong(split[i + 9]);
                    j2 += Long.parseLong(split[i + 1]);
                }
                if (!split[i + 0].equals("lo") && !split[i + 0].startsWith("rmnet")) {
                    j3 += Long.parseLong(split[i + 9]);
                    j4 += Long.parseLong(split[i + 1]);
                }
            }
            if (a < 0) {
                a = j;
                Log.v("MicroMsg.SDK.TrafficStats", "fix loss newMobileTx %d", Long.valueOf(j));
            }
            if (b < 0) {
                b = j2;
                Log.v("MicroMsg.SDK.TrafficStats", "fix loss newMobileRx %d", Long.valueOf(j2));
            }
            if (c < 0) {
                c = j3;
                Log.v("MicroMsg.SDK.TrafficStats", "fix loss newWifiTx %d", Long.valueOf(j3));
            }
            if (d < 0) {
                d = j4;
                Log.v("MicroMsg.SDK.TrafficStats", "fix loss newWifiRx %d", Long.valueOf(j4));
            }
            if (j4 - d < 0) {
                Log.v("MicroMsg.SDK.TrafficStats", "minu %d", Long.valueOf(j4 - d));
            }
            if (j3 - c < 0) {
                Log.v("MicroMsg.SDK.TrafficStats", "minu %d", Long.valueOf(j3 - c));
            }
            e = j >= a ? j - a : j;
            f = j2 >= b ? j2 - b : j2;
            g = j3 >= c ? j3 - c : j3;
            h = j4 >= d ? j4 - d : j4;
            a = j;
            b = j2;
            c = j3;
            d = j4;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        Log.d("MicroMsg.SDK.TrafficStats", "current system traffic: wifi rx/tx=%d/%d, mobile rx/tx=%d/%d", Long.valueOf(h), Long.valueOf(g), Long.valueOf(f), Long.valueOf(e));
    }

    public static long updateMobileRx(long j) {
        update();
        return getMobileRx(j);
    }

    public static long updateMobileTx(long j) {
        update();
        return getMobileTx(j);
    }

    public static long updateWifiRx(long j) {
        update();
        return getWifiRx(j);
    }

    public static long updateWifiTx(long j) {
        update();
        return getWifiTx(j);
    }
}
