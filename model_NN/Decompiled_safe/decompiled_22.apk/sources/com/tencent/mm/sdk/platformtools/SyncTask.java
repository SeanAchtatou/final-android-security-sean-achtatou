package com.tencent.mm.sdk.platformtools;

import android.os.Handler;

public abstract class SyncTask<R> {
    private R a;
    private Object b;
    private final long c;
    private Runnable d;

    public SyncTask() {
        this(0, null);
    }

    public SyncTask(long j, R r) {
        this.b = new Object();
        this.d = new Runnable() {
            public void run() {
                SyncTask.this.setResult(SyncTask.this.run());
            }
        };
        this.c = j;
        this.a = r;
    }

    public R exec(Handler handler) {
        if (handler == null) {
            Log.d("MicroMsg.SDK.SyncTask", "null handler, task in exec thread, return now");
            return run();
        } else if (Thread.currentThread().getId() == handler.getLooper().getThread().getId()) {
            Log.d("MicroMsg.SDK.SyncTask", "same tid, task in exec thread, return now");
            return run();
        } else {
            handler.post(this.d);
            try {
                synchronized (this.b) {
                    this.b.wait(this.c);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.v("MicroMsg.SDK.SyncTask", "sync task done, return=" + ((Object) this.a));
            return this.a;
        }
    }

    /* access modifiers changed from: protected */
    public abstract R run();

    public void setResult(R r) {
        this.a = r;
        synchronized (this.b) {
            this.b.notify();
        }
    }
}
