package com.tencent.mm.sdk;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import com.tencent.mm.sdk.plugin.MMPluginProviderConstants;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MMSharedPreferences implements SharedPreferences {
    private final ContentResolver a;
    private final String[] b = {"_id", "key", "type", "value"};
    private final HashMap<String, Object> c = new HashMap<>();
    private REditor d = null;

    private static class REditor implements SharedPreferences.Editor {
        private Map<String, Object> a = new HashMap();
        private Set<String> b = new HashSet();
        private boolean c = false;
        private ContentResolver d;

        public REditor(ContentResolver contentResolver) {
            this.d = contentResolver;
        }

        public void apply() {
            commit();
        }

        public SharedPreferences.Editor clear() {
            this.c = true;
            return this;
        }

        public boolean commit() {
            ContentValues contentValues = new ContentValues();
            if (this.c) {
                this.d.delete(MMPluginProviderConstants.SharedPref.CONTENT_URI, null, null);
                this.c = false;
            }
            for (String str : this.b) {
                this.d.delete(MMPluginProviderConstants.SharedPref.CONTENT_URI, "key = ?", new String[]{str});
            }
            for (Map.Entry next : this.a.entrySet()) {
                if (MMPluginProviderConstants.Resolver.unresolveObj(contentValues, next.getValue())) {
                    this.d.update(MMPluginProviderConstants.SharedPref.CONTENT_URI, contentValues, "key = ?", new String[]{(String) next.getKey()});
                }
            }
            return true;
        }

        public SharedPreferences.Editor putBoolean(String str, boolean z) {
            this.a.put(str, Boolean.valueOf(z));
            this.b.remove(str);
            return this;
        }

        public SharedPreferences.Editor putFloat(String str, float f) {
            this.a.put(str, Float.valueOf(f));
            this.b.remove(str);
            return this;
        }

        public SharedPreferences.Editor putInt(String str, int i) {
            this.a.put(str, Integer.valueOf(i));
            this.b.remove(str);
            return this;
        }

        public SharedPreferences.Editor putLong(String str, long j) {
            this.a.put(str, Long.valueOf(j));
            this.b.remove(str);
            return this;
        }

        public SharedPreferences.Editor putString(String str, String str2) {
            this.a.put(str, str2);
            this.b.remove(str);
            return this;
        }

        public SharedPreferences.Editor putStringSet(String str, Set<String> set) {
            return null;
        }

        public SharedPreferences.Editor remove(String str) {
            this.b.add(str);
            return this;
        }
    }

    public MMSharedPreferences(Context context) {
        this.a = context.getContentResolver();
    }

    private Object a(String str) {
        try {
            Cursor query = this.a.query(MMPluginProviderConstants.SharedPref.CONTENT_URI, this.b, "key = ?", new String[]{str}, null);
            if (query == null) {
                return null;
            }
            Object resolveObj = query.moveToFirst() ? MMPluginProviderConstants.Resolver.resolveObj(query.getInt(query.getColumnIndex("type")), query.getString(query.getColumnIndex("value"))) : null;
            query.close();
            return resolveObj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean contains(String str) {
        return a(str) != null;
    }

    public SharedPreferences.Editor edit() {
        if (this.d == null) {
            this.d = new REditor(this.a);
        }
        return this.d;
    }

    public Map<String, ?> getAll() {
        try {
            Cursor query = this.a.query(MMPluginProviderConstants.SharedPref.CONTENT_URI, this.b, null, null, null);
            if (query == null) {
                return null;
            }
            int columnIndex = query.getColumnIndex("key");
            int columnIndex2 = query.getColumnIndex("type");
            int columnIndex3 = query.getColumnIndex("value");
            while (query.moveToNext()) {
                this.c.put(query.getString(columnIndex), MMPluginProviderConstants.Resolver.resolveObj(query.getInt(columnIndex2), query.getString(columnIndex3)));
            }
            query.close();
            return this.c;
        } catch (Exception e) {
            e.printStackTrace();
            return this.c;
        }
    }

    public boolean getBoolean(String str, boolean z) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Boolean)) ? z : ((Boolean) a2).booleanValue();
    }

    public float getFloat(String str, float f) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Float)) ? f : ((Float) a2).floatValue();
    }

    public int getInt(String str, int i) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Integer)) ? i : ((Integer) a2).intValue();
    }

    public long getLong(String str, long j) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Long)) ? j : ((Long) a2).longValue();
    }

    public String getString(String str, String str2) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof String)) ? str2 : (String) a2;
    }

    public Set<String> getStringSet(String str, Set<String> set) {
        return null;
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }
}
