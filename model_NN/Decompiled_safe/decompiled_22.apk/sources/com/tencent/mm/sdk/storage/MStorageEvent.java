package com.tencent.mm.sdk.storage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class MStorageEvent<T, E> {
    private int a = 0;
    private final List<T> b = new ArrayList();
    private final Set<E> c = new HashSet();

    private void a() {
        for (T next : this.b) {
            for (E processEvent : this.c) {
                processEvent(next, processEvent);
            }
        }
        this.c.clear();
    }

    public void add(T t) {
        if (!this.b.contains(t)) {
            this.b.add(t);
        }
    }

    public void doNotify() {
        if (!isLocked()) {
            a();
        }
    }

    public boolean event(E e) {
        return this.c.add(e);
    }

    public boolean isLocked() {
        return this.a > 0;
    }

    public void lock() {
        this.a++;
    }

    /* access modifiers changed from: protected */
    public abstract void processEvent(T t, E e);

    public void remove(T t) {
        this.b.remove(t);
    }

    public void removeAll() {
        this.b.clear();
    }

    public void unlock() {
        this.a--;
        if (this.a <= 0) {
            this.a = 0;
            a();
        }
    }
}
