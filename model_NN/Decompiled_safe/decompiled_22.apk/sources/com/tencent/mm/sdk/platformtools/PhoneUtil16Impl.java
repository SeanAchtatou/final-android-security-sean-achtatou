package com.tencent.mm.sdk.platformtools;

import android.content.Context;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.tencent.mm.sdk.platformtools.PhoneUtil;
import com.utils.FinalVariable;
import java.util.LinkedList;
import java.util.List;

class PhoneUtil16Impl {
    private static int a = FinalVariable.vb_success;
    /* access modifiers changed from: private */
    public static int b = FinalVariable.vb_success;
    /* access modifiers changed from: private */
    public TelephonyManager c;
    /* access modifiers changed from: private */
    public PhoneStateListener d = new PhoneStateListener() {
        public void onSignalStrengthChanged(int i) {
            super.onSignalStrengthChanged(i);
            int unused = PhoneUtil16Impl.b = (i * 2) - 113;
            if (PhoneUtil16Impl.this.c != null) {
                PhoneUtil16Impl.this.c.listen(PhoneUtil16Impl.this.d, 0);
            }
        }
    };

    PhoneUtil16Impl() {
    }

    public List<PhoneUtil.CellInfo> getCellInfoList(Context context) {
        LinkedList linkedList = new LinkedList();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String networkOperator = telephonyManager.getNetworkOperator();
        if (networkOperator == null || networkOperator.equals("")) {
            return linkedList;
        }
        String str = "460";
        String str2 = "";
        try {
            str = networkOperator.substring(0, 3);
            str2 = networkOperator.substring(3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
            if (gsmCellLocation != null) {
                int cid = gsmCellLocation.getCid();
                int lac = gsmCellLocation.getLac();
                if (!(lac >= 65535 || lac == -1 || cid == -1)) {
                    linkedList.add(new PhoneUtil.CellInfo(str, str2, lac + "", cid + "", b == a ? "" : b + "", PhoneUtil.CELL_GSM, "", "", ""));
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        List<NeighboringCellInfo> neighboringCellInfo = telephonyManager.getNeighboringCellInfo();
        if (neighboringCellInfo != null && neighboringCellInfo.size() > 0) {
            for (NeighboringCellInfo neighboringCellInfo2 : neighboringCellInfo) {
                if (neighboringCellInfo2.getCid() != -1) {
                    linkedList.add(new PhoneUtil.CellInfo(str, str2, "", neighboringCellInfo2.getCid() + "", "", PhoneUtil.CELL_GSM, "", "", ""));
                }
            }
        }
        return linkedList;
    }

    public void getSignalStrength(Context context) {
        this.c = (TelephonyManager) context.getSystemService("phone");
        this.c.listen(this.d, 256);
    }
}
