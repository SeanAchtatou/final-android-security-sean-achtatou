package com.tencent.mm.sdk.message;

import android.content.Context;
import android.net.Uri;
import com.tencent.mm.sdk.storage.ContentProviderDB;
import java.util.HashMap;
import java.util.Map;

public class RMsgInfoDB extends ContentProviderDB<RMsgInfoDB> {
    public static final String TABLE = "message";
    private static final Map<String, Uri> a;

    static {
        HashMap hashMap = new HashMap();
        a = hashMap;
        hashMap.put("message", Uri.parse("content://com.tencent.mm.sdk.msginfo.provider/message"));
    }

    public RMsgInfoDB(Context context) {
        super(context);
    }

    public Uri getUriFromTable(String str) {
        return a.get(str);
    }
}
