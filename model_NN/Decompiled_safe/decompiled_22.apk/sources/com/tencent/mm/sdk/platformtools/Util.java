package com.tencent.mm.sdk.platformtools;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.actionbarsherlock.widget.ActivityChooserView;
import com.tencent.mm.algorithm.MD5;
import com.tencent.mm.sdk.plugin.MMPluginProviderConstants;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.Character;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import junit.framework.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public final class Util {
    public static final int BEGIN_TIME = 22;
    public static final int BIT_OF_KB = 10;
    public static final int BIT_OF_MB = 20;
    public static final int BYTE_OF_KB = 1024;
    public static final int BYTE_OF_MB = 1048576;
    public static final String CHINA = "zh_CN";
    public static final int DAY = 0;
    public static final int END_TIME = 8;
    public static final String ENGLISH = "en";
    public static final String HONGKONG = "zh_HK";
    public static final String LANGUAGE_DEFAULT = "language_default";
    public static final int MASK_16BIT = 65535;
    public static final int MASK_32BIT = -1;
    public static final int MASK_4BIT = 15;
    public static final int MASK_8BIT = 255;
    public static final long MAX_32BIT_VALUE = 4294967295L;
    public static final int MAX_ACCOUNT_LENGTH = 20;
    public static final int MAX_DECODE_PICTURE_SIZE = 2764800;
    public static final int MAX_PASSWORD_LENGTH = 9;
    public static final long MILLSECONDS_OF_DAY = 86400000;
    public static final long MILLSECONDS_OF_HOUR = 3600000;
    public static final long MILLSECONDS_OF_MINUTE = 60000;
    public static final long MILLSECONDS_OF_SECOND = 1000;
    public static final long MINUTE_OF_HOUR = 60;
    public static final int MIN_ACCOUNT_LENGTH = 6;
    public static final int MIN_PASSWORD_LENGTH = 4;
    public static final String PHOTO_DEFAULT_EXT = ".jpg";
    public static final long SECOND_OF_MINUTE = 60;
    public static final String TAIWAN = "zh_TW";
    private static final long[] a = {300, 200, 300, 200};
    private static final TimeZone b = TimeZone.getTimeZone("GMT");
    private static final char[] c = {9, 10, 13};
    private static final char[] d = {'<', '>', '\"', '\'', '&'};
    private static final String[] e = {"&lt;", "&gt;", "&quot;", "&apos;", "&amp;"};

    private Util() {
    }

    public static String GetHostIp() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress();
                        }
                    }
                }
            }
        } catch (Exception | SocketException e2) {
        }
        return null;
    }

    public static int UnZipFolder(String str, String str2) {
        try {
            Log.v("XZip", "UnZipFolder(String, String)");
            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(str));
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry != null) {
                    String name = nextEntry.getName();
                    if (nextEntry.isDirectory()) {
                        new File(str2 + File.separator + name.substring(0, name.length() - 1)).mkdirs();
                    } else {
                        File file = new File(str2 + File.separator + name);
                        file.createNewFile();
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = zipInputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                            fileOutputStream.flush();
                        }
                        fileOutputStream.close();
                    }
                } else {
                    zipInputStream.close();
                    return 0;
                }
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return -1;
        } catch (IOException e3) {
            e3.printStackTrace();
            return -2;
        }
    }

    private static int a(char[] cArr, int i, int i2) {
        if (i2 <= 0) {
            return 0;
        }
        if (cArr[i] != '#') {
            new String(cArr, i, i2);
            return 0;
        } else if (i2 <= 1 || !(cArr[i + 1] == 'x' || cArr[i + 1] == 'X')) {
            try {
                return Integer.parseInt(new String(cArr, i + 1, i2 - 1), 10);
            } catch (NumberFormatException e2) {
                return 0;
            }
        } else {
            try {
                return Integer.parseInt(new String(cArr, i + 2, i2 - 2), 16);
            } catch (NumberFormatException e3) {
                return 0;
            }
        }
    }

    private static void a(Map<String, String> map, String str, Node node, int i) {
        if (node.getNodeName().equals("#text")) {
            map.put(str, node.getNodeValue());
        } else if (node.getNodeName().equals("#cdata-section")) {
            map.put(str, node.getNodeValue());
        } else {
            String str2 = str + "." + node.getNodeName() + (i > 0 ? Integer.valueOf(i) : "");
            map.put(str2, node.getNodeValue());
            NamedNodeMap attributes = node.getAttributes();
            if (attributes != null) {
                for (int i2 = 0; i2 < attributes.getLength(); i2++) {
                    Node item = attributes.item(i2);
                    map.put(str2 + ".$" + item.getNodeName(), item.getNodeValue());
                }
            }
            HashMap hashMap = new HashMap();
            NodeList childNodes = node.getChildNodes();
            for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                Node item2 = childNodes.item(i3);
                int nullAsNil = nullAsNil((Integer) hashMap.get(item2.getNodeName()));
                a(map, str2, item2, nullAsNil);
                hashMap.put(item2.getNodeName(), Integer.valueOf(nullAsNil + 1));
            }
        }
    }

    public static byte[] bmpToByteArray(Bitmap bitmap, boolean z) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        if (z) {
            bitmap.recycle();
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return byteArray;
    }

    public static boolean checkPermission(Context context, String str) {
        Assert.assertNotNull(context);
        String packageName = context.getPackageName();
        boolean z = context.getPackageManager().checkPermission(str, packageName) == 0;
        Log.d("MicroMsg.Util", packageName + " has " + (z ? "permission " : "no permission ") + str);
        return z;
    }

    public static boolean checkSDCardFull() {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long blockCount = (long) statFs.getBlockCount();
        long availableBlocks = (long) statFs.getAvailableBlocks();
        if (blockCount <= 0 || blockCount - availableBlocks < 0) {
            return false;
        }
        int i = (int) (((blockCount - availableBlocks) * 100) / blockCount);
        long blockSize = ((long) statFs.getBlockSize()) * ((long) statFs.getFreeBlocks());
        Log.d("MicroMsg.Util", "checkSDCardFull per:" + i + " blockCount:" + blockCount + " availCount:" + availableBlocks + " availSize:" + blockSize);
        return 95 <= i && blockSize <= 52428800;
    }

    public static String convertStreamToString(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    try {
                        break;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (IOException e3) {
                e3.printStackTrace();
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
                throw th;
            }
        }
        inputStream.close();
        return sb.toString();
    }

    public static boolean createThumbNail(String str, int i, int i2, Bitmap.CompressFormat compressFormat, int i3, String str2, String str3) {
        Bitmap extractThumbNail = extractThumbNail(str, i, i2, false);
        if (extractThumbNail == null) {
            return false;
        }
        try {
            saveBitmapToImage(extractThumbNail, i3, compressFormat, str2, str3, true);
            return true;
        } catch (IOException e2) {
            Log.e("MicroMsg.Util", "create thumbnail from orig failed: " + str3);
            return false;
        }
    }

    public static long currentDayInMills() {
        return (nowMilliSecond() / MILLSECONDS_OF_DAY) * MILLSECONDS_OF_DAY;
    }

    public static long currentMonthInMills() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        GregorianCalendar gregorianCalendar2 = new GregorianCalendar(gregorianCalendar.get(1), gregorianCalendar.get(2), 1);
        gregorianCalendar2.setTimeZone(b);
        return gregorianCalendar2.getTimeInMillis();
    }

    public static long currentTicks() {
        return SystemClock.elapsedRealtime();
    }

    public static long currentWeekInMills() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        GregorianCalendar gregorianCalendar2 = new GregorianCalendar(gregorianCalendar.get(1), gregorianCalendar.get(2), gregorianCalendar.get(5));
        gregorianCalendar2.setTimeZone(b);
        gregorianCalendar2.add(6, -(gregorianCalendar.get(7) - gregorianCalendar.getFirstDayOfWeek()));
        return gregorianCalendar2.getTimeInMillis();
    }

    public static long currentYearInMills() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(new GregorianCalendar().get(1), 1, 1);
        gregorianCalendar.setTimeZone(b);
        return gregorianCalendar.getTimeInMillis();
    }

    public static byte[] decodeHexString(String str) {
        if (str == null || str.length() <= 0) {
            return new byte[0];
        }
        try {
            byte[] bArr = new byte[(str.length() / 2)];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) (Integer.parseInt(str.substring(i * 2, (i * 2) + 2), 16) & 255);
            }
            return bArr;
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return new byte[0];
        }
    }

    public static String dumpArray(Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        for (Object append : objArr) {
            sb.append(append);
            sb.append(",");
        }
        return sb.toString();
    }

    public static String dumpHex(byte[] bArr) {
        int i = 0;
        if (bArr == null) {
            return "(null)";
        }
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        int length = bArr.length;
        char[] cArr2 = new char[((length * 3) + (length / 16))];
        for (int i2 = 0; i2 < length; i2++) {
            byte b2 = bArr[i2];
            int i3 = i + 1;
            cArr2[i] = ' ';
            int i4 = i3 + 1;
            cArr2[i3] = cArr[(b2 >>> 4) & 15];
            int i5 = i4 + 1;
            cArr2[i4] = cArr[b2 & 15];
            if (i2 % 16 != 0 || i2 <= 0) {
                i = i5;
            } else {
                i = i5 + 1;
                cArr2[i5] = 10;
            }
        }
        return new String(cArr2);
    }

    public static void dumpMemoryUsage() {
        Log.w("MicroMsg.Util", "memory usage: h=" + getSizeKB((long) Debug.getGlobalAllocSize()) + "|" + getSizeKB((long) Debug.getGlobalFreedSize()) + ", e=" + getSizeKB((long) Debug.getGlobalExternalAllocSize()) + "|" + getSizeKB((long) Debug.getGlobalExternalFreedSize()) + ", n=" + getSizeKB(Debug.getNativeHeapAllocatedSize()) + "|" + getSizeKB(Debug.getNativeHeapFreeSize()) + "|" + getSizeKB(Debug.getNativeHeapSize()));
    }

    public static String encodeHexString(byte[] bArr) {
        StringBuilder sb = new StringBuilder("");
        if (bArr != null) {
            for (int i = 0; i < bArr.length; i++) {
                sb.append(String.format("%02x", Integer.valueOf(bArr[i] & 255)));
            }
        }
        return sb.toString();
    }

    public static String escapeSqlValue(String str) {
        return str != null ? str.replace("\\[", "[[]").replace("%", "").replace("\\^", "").replace("'", "").replace("\\{", "").replace("\\}", "").replace("\"", "") : str;
    }

    public static String escapeStringForXml(String str) {
        boolean z;
        if (str == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if ((charAt >= ' ' || charAt == c[0] || charAt == c[1] || charAt == c[2]) && charAt <= 127) {
                int length2 = d.length - 1;
                while (true) {
                    if (length2 < 0) {
                        z = true;
                        break;
                    } else if (d[length2] == charAt) {
                        stringBuffer.append(e[length2]);
                        z = false;
                        break;
                    } else {
                        length2--;
                    }
                }
                if (z) {
                    stringBuffer.append(charAt);
                }
            } else {
                stringBuffer.append("&#");
                stringBuffer.append(Integer.toString(charAt));
                stringBuffer.append(';');
            }
        }
        return stringBuffer.toString();
    }

    public static String expandEntities(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = 0;
        int i2 = 0;
        int i3 = -1;
        while (i < length) {
            char charAt = str.charAt(i);
            int i4 = i2 + 1;
            cArr[i2] = charAt;
            if (charAt == '&' && i3 == -1) {
                i3 = i4;
            } else if (i3 != -1 && !Character.isLetter(charAt) && !Character.isDigit(charAt) && charAt != '#') {
                if (charAt == ';') {
                    int a2 = a(cArr, i3, (i4 - i3) - 1);
                    if (a2 > 65535) {
                        int i5 = a2 - 65536;
                        cArr[i3 - 1] = (char) ((i5 >> 10) + 55296);
                        cArr[i3] = (char) ((i5 & 1023) + 56320);
                        i3++;
                    } else if (a2 != 0) {
                        cArr[i3 - 1] = (char) a2;
                    } else {
                        i3 = i4;
                    }
                    i4 = i3;
                    i3 = -1;
                } else {
                    i3 = -1;
                }
            }
            i++;
            i2 = i4;
        }
        return new String(cArr, 0, i2);
    }

    public static Bitmap extractThumbNail(String str, int i, int i2, boolean z) {
        int i3;
        int i4;
        Bitmap bitmap;
        boolean z2 = false;
        if (str != null && !str.equals("") && i > 0 && i2 > 0) {
            z2 = true;
        }
        Assert.assertTrue(z2);
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            options.inJustDecodeBounds = true;
            Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
            if (decodeFile != null) {
                decodeFile.recycle();
            }
            Log.d("MicroMsg.Util", "extractThumbNail: round=" + i2 + "x" + i + ", crop=" + z);
            double d2 = (((double) options.outHeight) * 1.0d) / ((double) i);
            double d3 = (((double) options.outWidth) * 1.0d) / ((double) i2);
            Log.d("MicroMsg.Util", "extractThumbNail: extract beX = " + d3 + ", beY = " + d2);
            options.inSampleSize = (int) (z ? d2 > d3 ? d3 : d2 : d2 < d3 ? d3 : d2);
            if (options.inSampleSize <= 1) {
                options.inSampleSize = 1;
            }
            while ((options.outHeight * options.outWidth) / options.inSampleSize > 2764800) {
                options.inSampleSize++;
            }
            if (z) {
                if (d2 > d3) {
                    i3 = i2;
                    i4 = (int) (((((double) i2) * 1.0d) * ((double) options.outHeight)) / ((double) options.outWidth));
                } else {
                    i3 = (int) (((((double) i) * 1.0d) * ((double) options.outWidth)) / ((double) options.outHeight));
                    i4 = i;
                }
            } else if (d2 < d3) {
                i3 = i2;
                i4 = (int) (((((double) i2) * 1.0d) * ((double) options.outHeight)) / ((double) options.outWidth));
            } else {
                i3 = (int) (((((double) i) * 1.0d) * ((double) options.outWidth)) / ((double) options.outHeight));
                i4 = i;
            }
            options.inJustDecodeBounds = false;
            Log.i("MicroMsg.Util", "bitmap required size=" + i3 + "x" + i4 + ", orig=" + options.outWidth + "x" + options.outHeight + ", sample=" + options.inSampleSize);
            Bitmap decodeFile2 = BitmapFactory.decodeFile(str, options);
            if (decodeFile2 == null) {
                Log.e("MicroMsg.Util", "bitmap decode failed");
                return null;
            }
            Log.i("MicroMsg.Util", "bitmap decoded size=" + decodeFile2.getWidth() + "x" + decodeFile2.getHeight());
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeFile2, i3, i4, true);
            if (createScaledBitmap != null) {
                decodeFile2.recycle();
            } else {
                createScaledBitmap = decodeFile2;
            }
            if (z) {
                bitmap = Bitmap.createBitmap(createScaledBitmap, (createScaledBitmap.getWidth() - i2) >> 1, (createScaledBitmap.getHeight() - i) >> 1, i2, i);
                if (bitmap == null) {
                    return createScaledBitmap;
                }
                createScaledBitmap.recycle();
                Log.i("MicroMsg.Util", "bitmap croped size=" + bitmap.getWidth() + "x" + bitmap.getHeight());
            } else {
                bitmap = createScaledBitmap;
            }
            return bitmap;
        } catch (OutOfMemoryError e2) {
            Log.e("MicroMsg.Util", "decode bitmap failed: " + e2.getMessage());
            return null;
        }
    }

    public static String formatSecToMin(int i) {
        return String.format("%d:%02d", Long.valueOf(((long) i) / 60), Long.valueOf(((long) i) % 60));
    }

    public static String formatUnixTime(long j) {
        return new SimpleDateFormat("[yy-MM-dd HH:mm:ss]").format(new Date(1000 * j));
    }

    public static void freeBitmapMap(Map<String, Bitmap> map) {
        for (Map.Entry<String, Bitmap> value : map.entrySet()) {
            Bitmap bitmap = (Bitmap) value.getValue();
            if (bitmap != null) {
                bitmap.recycle();
            }
        }
        map.clear();
    }

    public static String getCutPasswordMD5(String str) {
        if (str == null) {
            str = "";
        }
        return str.length() <= 16 ? getFullPasswordMD5(str) : getFullPasswordMD5(str.substring(0, 16));
    }

    public static String getDeviceId(Context context) {
        if (context == null) {
            return null;
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null) {
                return null;
            }
            String deviceId = telephonyManager.getDeviceId();
            if (deviceId == null) {
                return null;
            }
            return deviceId.trim();
        } catch (SecurityException e2) {
            Log.e("MicroMsg.Util", "getDeviceId failed, security exception");
            return null;
        } catch (Exception e3) {
            e3.printStackTrace();
            return null;
        }
    }

    public static String getFullPasswordMD5(String str) {
        return MD5.getMessageDigest(str.getBytes());
    }

    public static int getHex(String str, int i) {
        if (str == null) {
            return i;
        }
        try {
            return (int) (Long.decode(str).longValue() & MAX_32BIT_VALUE);
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return i;
        }
    }

    public static BitmapFactory.Options getImageOptions(String str) {
        Assert.assertTrue(str != null && !str.equals(""));
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
            if (decodeFile != null) {
                decodeFile.recycle();
            }
        } catch (OutOfMemoryError e2) {
            Log.e("MicroMsg.Util", "decode bitmap failed: " + e2.getMessage());
        }
        return options;
    }

    public static Intent getInstallPackIntent(String str, Context context) {
        Assert.assertTrue(str != null && !str.equals(""));
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.android.package-archive");
        return intent;
    }

    public static int getInt(String str, int i) {
        if (str == null) {
            return i;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return i;
        }
    }

    public static int getIntRandom(int i, int i2) {
        Assert.assertTrue(i > i2);
        return new Random(System.currentTimeMillis()).nextInt((i - i2) + 1) + i2;
    }

    public static String getLine1Number(Context context) {
        if (context != null) {
            try {
                if (((TelephonyManager) context.getSystemService("phone")) == null) {
                    Log.e("MicroMsg.Util", "get line1 number failed, null tm");
                }
            } catch (SecurityException e2) {
                Log.e("MicroMsg.Util", "getLine1Number failed, security exception");
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return null;
    }

    public static long getLong(String str, long j) {
        if (str == null) {
            return j;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return j;
        }
    }

    public static Element getRootElementFromXML(byte[] bArr) {
        try {
            DocumentBuilder newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            if (newDocumentBuilder == null) {
                Log.e("MicroMsg.Util", "new Document Builder failed");
                return null;
            }
            try {
                Document parse = newDocumentBuilder.parse(new ByteArrayInputStream(bArr));
                if (parse != null) {
                    return parse.getDocumentElement();
                }
                Log.e("MicroMsg.Util", "new Document failed");
                return null;
            } catch (SAXException e2) {
                e2.printStackTrace();
                return null;
            } catch (IOException e3) {
                e3.printStackTrace();
                return null;
            }
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        }
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, boolean z, float f) {
        Assert.assertNotNull(bitmap);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setFilterBitmap(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-4144960);
        canvas.drawRoundRect(rectF, f, f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        if (z) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    public static String getSizeKB(long j) {
        if ((j >> 20) > 0) {
            return getSizeMB(j);
        }
        if ((j >> 9) <= 0) {
            return "" + j + "B";
        }
        return "" + (((float) Math.round((((float) j) * 10.0f) / 1024.0f)) / 10.0f) + "KB";
    }

    public static String getSizeMB(long j) {
        return "" + (((float) Math.round((((float) j) * 10.0f) / 1048576.0f)) / 10.0f) + "MB";
    }

    public static String getStack() {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        if (stackTrace == null || stackTrace.length < 2) {
            return "";
        }
        String str = "";
        int i = 1;
        while (i < stackTrace.length && stackTrace[i].getClassName().contains(MMPluginProviderConstants.PluginIntent.APP_PACKAGE_PATTERN)) {
            str = str + "[" + stackTrace[i].getClassName().substring("com.tencent.mm.".length()) + ":" + stackTrace[i].getMethodName() + "]";
            i++;
        }
        return str;
    }

    public static int getSystemVersion(Context context, int i) {
        return context == null ? i : Settings.System.getInt(context.getContentResolver(), "sys.settings_system_version", i);
    }

    public static String getTimeZone() {
        String timeZoneDef = getTimeZoneDef();
        int indexOf = timeZoneDef.indexOf(43);
        if (indexOf == -1) {
            indexOf = timeZoneDef.indexOf(45);
        }
        if (indexOf == -1) {
            return "";
        }
        String substring = timeZoneDef.substring(indexOf, indexOf + 3);
        return substring.charAt(1) == '0' ? substring.substring(0, 1) + substring.substring(2, 3) : substring;
    }

    public static String getTimeZoneDef() {
        int rawOffset = (int) (((long) TimeZone.getDefault().getRawOffset()) / MILLSECONDS_OF_MINUTE);
        char c2 = '+';
        if (rawOffset < 0) {
            c2 = '-';
            rawOffset = -rawOffset;
        }
        return String.format("GMT%s%02d:%02d", Character.valueOf(c2), Long.valueOf(((long) rawOffset) / 60), Long.valueOf(((long) rawOffset) % 60));
    }

    public static String getTimeZoneOffset() {
        TimeZone timeZone = TimeZone.getDefault();
        return String.format("%.2f", Double.valueOf((((double) (((long) (timeZone.getRawOffset() * 100)) / MILLSECONDS_OF_HOUR)) / 100.0d) + ((double) (timeZone.useDaylightTime() ? 1 : 0))));
    }

    public static String getTopActivityName(Context context) {
        try {
            return ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getClassName();
        } catch (Exception e2) {
            e2.printStackTrace();
            return "(null)";
        }
    }

    public static int guessHttpContinueRecvLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 52 + i;
    }

    public static int guessHttpRecvLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 208 + i;
    }

    public static int guessHttpSendLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 224 + i;
    }

    public static int guessTcpConnectLength() {
        return 172;
    }

    public static int guessTcpDisconnectLength() {
        return 156;
    }

    public static int guessTcpRecvLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 40 + i;
    }

    public static int guessTcpSendLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 40 + i;
    }

    public static void installPack(String str, Context context) {
        context.startActivity(getInstallPackIntent(str, context));
    }

    public static boolean isAlpha(char c2) {
        return (c2 >= 'a' && c2 <= 'z') || (c2 >= 'A' && c2 <= 'Z');
    }

    public static boolean isChinese(char c2) {
        Character.UnicodeBlock of = Character.UnicodeBlock.of(c2);
        return of == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || of == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || of == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || of == Character.UnicodeBlock.GENERAL_PUNCTUATION || of == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || of == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS;
    }

    public static boolean isDayTimeNow() {
        int i = new GregorianCalendar().get(11);
        return ((long) i) >= 6 && ((long) i) < 18;
    }

    public static boolean isImgFile(String str) {
        if (str == null || str.length() == 0) {
            Log.e("MicroMsg.Util", "isImgFile, invalid argument");
            return false;
        } else if (str.length() < 3 || !new File(str).exists()) {
            return false;
        } else {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            return options.outWidth > 0 && options.outHeight > 0;
        }
    }

    public static boolean isIntentAvailable(Context context, Intent intent) {
        return context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0;
    }

    public static boolean isLockScreen(Context context) {
        try {
            return ((KeyguardManager) context.getSystemService("keyguard")).inKeyguardRestrictedInputMode();
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean isNightTime(int i, int i2, int i3) {
        if (i2 > i3) {
            return i >= i2 || i <= i3;
        }
        if (i2 < i3) {
            return i <= i3 && i >= i2;
        }
        return true;
    }

    public static boolean isNullOrNil(String str) {
        return str == null || str.length() <= 0;
    }

    public static boolean isNullOrNil(byte[] bArr) {
        return bArr == null || bArr.length <= 0;
    }

    public static boolean isNum(char c2) {
        return c2 >= '0' && c2 <= '9';
    }

    public static boolean isProcessRunning(Context context, String str) {
        for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (next != null && next.processName != null && next.processName.equals(str)) {
                Log.w("MicroMsg.Util", "process " + str + " is running");
                return true;
            }
        }
        Log.w("MicroMsg.Util", "process " + str + " is not running");
        return false;
    }

    public static boolean isServiceRunning(Context context, String str) {
        for (ActivityManager.RunningServiceInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningServices(ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED)) {
            if (next != null && next.service != null && next.service.getClassName().toString().equals(str)) {
                Log.w("MicroMsg.Util", "service " + str + " is running");
                return true;
            }
        }
        Log.w("MicroMsg.Util", "service " + str + " is not running");
        return false;
    }

    public static boolean isTopActivity(Context context) {
        String name = context.getClass().getName();
        String topActivityName = getTopActivityName(context);
        Log.d("MicroMsg.Util", "top activity=" + topActivityName + ", context=" + name);
        return topActivityName.equalsIgnoreCase(name);
    }

    public static boolean isTopApplication(Context context) {
        try {
            String className = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getClassName();
            String packageName = context.getPackageName();
            Log.d("MicroMsg.Util", "top activity=" + className + ", context=" + packageName);
            return className.contains(packageName);
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean isValidAccount(String str) {
        if (str == null) {
            return false;
        }
        String trim = str.trim();
        if (trim.length() < 6 || trim.length() > 20 || !isAlpha(trim.charAt(0))) {
            return false;
        }
        for (int i = 0; i < trim.length(); i++) {
            char charAt = trim.charAt(i);
            if (!isAlpha(charAt) && !isNum(charAt) && charAt != '-' && charAt != '_') {
                return false;
            }
        }
        return true;
    }

    public static boolean isValidEmail(String str) {
        if (str == null || str.length() <= 0) {
            return false;
        }
        return str.trim().matches("^[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$");
    }

    public static boolean isValidPassword(String str) {
        if (str == null || str.length() < 4) {
            return false;
        }
        if (str.length() >= 9) {
            return true;
        }
        try {
            Integer.parseInt(str);
            return false;
        } catch (NumberFormatException e2) {
            return true;
        }
    }

    public static boolean isValidQQNum(String str) {
        if (str == null || str.length() <= 0) {
            return false;
        }
        try {
            long longValue = Long.valueOf(str.trim()).longValue();
            return longValue > 0 && longValue <= MAX_32BIT_VALUE;
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean jump(Context context, String str) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        if (!isIntentAvailable(context, intent)) {
            Log.e("MicroMsg.Util", "jump to url failed, " + str);
            return false;
        }
        context.startActivity(intent);
        return true;
    }

    public static String listToString(List<String> list, String str) {
        if (list == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder("");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return sb.toString();
            }
            if (i2 == list.size() - 1) {
                sb.append(list.get(i2).trim());
            } else {
                sb.append(list.get(i2).trim() + str);
            }
            i = i2 + 1;
        }
    }

    public static long milliSecondsToNow(long j) {
        return System.currentTimeMillis() - j;
    }

    public static long nowMilliSecond() {
        return System.currentTimeMillis();
    }

    public static long nowSecond() {
        return System.currentTimeMillis() / 1000;
    }

    public static int nullAs(Integer num, int i) {
        return num == null ? i : num.intValue();
    }

    public static long nullAs(Long l, long j) {
        return l == null ? j : l.longValue();
    }

    public static String nullAs(String str, String str2) {
        return str == null ? str2 : str;
    }

    public static boolean nullAs(Boolean bool, boolean z) {
        return bool == null ? z : bool.booleanValue();
    }

    public static boolean nullAsFalse(Boolean bool) {
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public static int nullAsInt(Object obj, int i) {
        return obj == null ? i : obj instanceof Integer ? ((Integer) obj).intValue() : obj instanceof Long ? ((Long) obj).intValue() : i;
    }

    public static int nullAsNil(Integer num) {
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    public static long nullAsNil(Long l) {
        if (l == null) {
            return 0;
        }
        return l.longValue();
    }

    public static String nullAsNil(String str) {
        return str == null ? "" : str;
    }

    public static boolean nullAsTrue(Boolean bool) {
        if (bool == null) {
            return true;
        }
        return bool.booleanValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0051, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0052, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0056, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0057, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005c, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00f1, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0051 A[ExcHandler: SAXException (r1v6 'e' org.xml.sax.SAXException A[CUSTOM_DECLARE]), Splitter:B:11:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056 A[ExcHandler: IOException (r1v5 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:11:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b A[ExcHandler: Exception (r1v4 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:11:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0060  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.String> parseXml(java.lang.String r7, java.lang.String r8, java.lang.String r9) {
        /*
            r6 = 0
            r0 = 0
            if (r7 == 0) goto L_0x000a
            int r1 = r7.length()
            if (r1 > 0) goto L_0x000b
        L_0x000a:
            return r0
        L_0x000b:
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            javax.xml.parsers.DocumentBuilderFactory r1 = javax.xml.parsers.DocumentBuilderFactory.newInstance()
            javax.xml.parsers.DocumentBuilder r1 = r1.newDocumentBuilder()     // Catch:{ ParserConfigurationException -> 0x0022 }
            if (r1 != 0) goto L_0x0027
            java.lang.String r1 = "MicroMsg.Util"
            java.lang.String r2 = "new Document Builder failed"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            goto L_0x000a
        L_0x0022:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x0027:
            org.xml.sax.InputSource r3 = new org.xml.sax.InputSource     // Catch:{ DOMException -> 0x004b, SAXException -> 0x0051, IOException -> 0x0056, Exception -> 0x005b }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ DOMException -> 0x004b, SAXException -> 0x0051, IOException -> 0x0056, Exception -> 0x005b }
            byte[] r5 = r7.getBytes()     // Catch:{ DOMException -> 0x004b, SAXException -> 0x0051, IOException -> 0x0056, Exception -> 0x005b }
            r4.<init>(r5)     // Catch:{ DOMException -> 0x004b, SAXException -> 0x0051, IOException -> 0x0056, Exception -> 0x005b }
            r3.<init>(r4)     // Catch:{ DOMException -> 0x004b, SAXException -> 0x0051, IOException -> 0x0056, Exception -> 0x005b }
            if (r9 == 0) goto L_0x003a
            r3.setEncoding(r9)     // Catch:{ DOMException -> 0x004b, SAXException -> 0x0051, IOException -> 0x0056, Exception -> 0x005b }
        L_0x003a:
            org.w3c.dom.Document r3 = r1.parse(r3)     // Catch:{ DOMException -> 0x004b, SAXException -> 0x0051, IOException -> 0x0056, Exception -> 0x005b }
            r3.normalize()     // Catch:{ DOMException -> 0x00f1, SAXException -> 0x0051, IOException -> 0x0056, Exception -> 0x005b }
        L_0x0041:
            if (r3 != 0) goto L_0x0060
            java.lang.String r1 = "MicroMsg.Util"
            java.lang.String r2 = "new Document failed"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            goto L_0x000a
        L_0x004b:
            r1 = move-exception
            r3 = r0
        L_0x004d:
            r1.printStackTrace()
            goto L_0x0041
        L_0x0051:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x0056:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x005b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000a
        L_0x0060:
            org.w3c.dom.Element r1 = r3.getDocumentElement()
            if (r1 != 0) goto L_0x006e
            java.lang.String r1 = "MicroMsg.Util"
            java.lang.String r2 = "getDocumentElement failed"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            goto L_0x000a
        L_0x006e:
            if (r8 == 0) goto L_0x00c3
            java.lang.String r3 = r1.getNodeName()
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x00c3
            java.lang.String r0 = ""
            a(r2, r0, r1, r6)
        L_0x007f:
            java.util.Set r0 = r2.entrySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x0087:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00ee
            java.lang.Object r0 = r3.next()
            r1 = r0
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.String r4 = "MicroMsg.Util"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r5 = "key="
            java.lang.StringBuilder r5 = r0.append(r5)
            java.lang.Object r0 = r1.getKey()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r5 = " value="
            java.lang.StringBuilder r5 = r0.append(r5)
            java.lang.Object r0 = r1.getValue()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.mm.sdk.platformtools.Log.v(r4, r0)
            goto L_0x0087
        L_0x00c3:
            org.w3c.dom.NodeList r1 = r1.getElementsByTagName(r8)
            int r3 = r1.getLength()
            if (r3 > 0) goto L_0x00d6
            java.lang.String r1 = "MicroMsg.Util"
            java.lang.String r2 = "parse item null"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            goto L_0x000a
        L_0x00d6:
            int r0 = r1.getLength()
            r3 = 1
            if (r0 <= r3) goto L_0x00e4
            java.lang.String r0 = "MicroMsg.Util"
            java.lang.String r3 = "parse items more than one"
            com.tencent.mm.sdk.platformtools.Log.w(r0, r3)
        L_0x00e4:
            java.lang.String r0 = ""
            org.w3c.dom.Node r1 = r1.item(r6)
            a(r2, r0, r1, r6)
            goto L_0x007f
        L_0x00ee:
            r0 = r2
            goto L_0x000a
        L_0x00f1:
            r1 = move-exception
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mm.sdk.platformtools.Util.parseXml(java.lang.String, java.lang.String, java.lang.String):java.util.Map");
    }

    public static MediaPlayer playSound(Context context, int i, MediaPlayer.OnCompletionListener onCompletionListener) {
        try {
            AssetFileDescriptor openFd = context.getAssets().openFd(context.getString(i));
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(openFd.getFileDescriptor(), openFd.getStartOffset(), openFd.getLength());
            openFd.close();
            mediaPlayer.prepare();
            mediaPlayer.setLooping(false);
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(onCompletionListener);
            return mediaPlayer;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void playSound(Context context, int i) {
        playSound(context, i, new MediaPlayer.OnCompletionListener() {
            public final void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }

    public static String processXml(String str) {
        return (str == null || str.length() == 0 || Build.VERSION.SDK_INT >= 8) ? str : expandEntities(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static boolean rotate(String str, int i, Bitmap.CompressFormat compressFormat, int i2, String str2, String str3) {
        Bitmap decodeFile = BitmapFactory.decodeFile(str);
        if (decodeFile == null) {
            Log.e("MicroMsg.Util", "rotate: create bitmap fialed");
            return false;
        }
        float width = (float) decodeFile.getWidth();
        float height = (float) decodeFile.getHeight();
        Matrix matrix = new Matrix();
        matrix.setRotate((float) i, width / 2.0f, height / 2.0f);
        Bitmap createBitmap = Bitmap.createBitmap(decodeFile, 0, 0, (int) width, (int) height, matrix, true);
        decodeFile.recycle();
        try {
            saveBitmapToImage(createBitmap, i2, compressFormat, str2, str3, true);
            return true;
        } catch (IOException e2) {
            Log.e("MicroMsg.Util", "create thumbnail from orig failed: " + str3);
            e2.printStackTrace();
            return false;
        }
    }

    public static void saveBitmapToImage(Bitmap bitmap, int i, Bitmap.CompressFormat compressFormat, String str, String str2, boolean z) {
        Assert.assertTrue((str == null || str2 == null) ? false : true);
        Log.d("MicroMsg.Util", "saving to " + str + str2);
        File file = new File(str + str2);
        file.createNewFile();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(compressFormat, i, fileOutputStream);
            fileOutputStream.flush();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
    }

    public static void saveBitmapToImage(Bitmap bitmap, int i, Bitmap.CompressFormat compressFormat, String str, boolean z) {
        Assert.assertTrue(!isNullOrNil(str));
        Log.d("MicroMsg.Util", "saving to " + str);
        File file = new File(str);
        file.createNewFile();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(compressFormat, i, fileOutputStream);
            fileOutputStream.flush();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
    }

    public static long secondsToNow(long j) {
        return (System.currentTimeMillis() / 1000) - j;
    }

    public static void selectPicture(Context context, int i) {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        ((Activity) context).startActivityForResult(Intent.createChooser(intent, null), i);
    }

    public static void shake(Context context, boolean z) {
        Vibrator vibrator = (Vibrator) context.getSystemService("vibrator");
        if (vibrator != null) {
            if (z) {
                vibrator.vibrate(a, -1);
            } else {
                vibrator.cancel();
            }
        }
    }

    public static int[] splitToIntArray(String str) {
        int i = 0;
        if (str == null) {
            return null;
        }
        String[] split = str.split(":");
        ArrayList arrayList = new ArrayList();
        for (String str2 : split) {
            if (str2 != null && str2.length() > 0) {
                try {
                    arrayList.add(Integer.valueOf(Integer.valueOf(str2).intValue()));
                } catch (Exception e2) {
                    e2.printStackTrace();
                    Log.e("MicroMsg.Util", "invalid port num, ignore");
                }
            }
        }
        int[] iArr = new int[arrayList.size()];
        while (true) {
            int i2 = i;
            if (i2 >= iArr.length) {
                return iArr;
            }
            iArr[i2] = ((Integer) arrayList.get(i2)).intValue();
            i = i2 + 1;
        }
    }

    public static List<String> stringsToList(String[] strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String add : strArr) {
            arrayList.add(add);
        }
        return arrayList;
    }

    public static long ticksToNow(long j) {
        return SystemClock.elapsedRealtime() - j;
    }

    public static void transClickToSelect(final View view, final View view2) {
        view.setOnTouchListener(new View.OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                        view2.setSelected(true);
                        break;
                    case 1:
                    case 3:
                    case 4:
                        view2.setSelected(false);
                        break;
                    case 2:
                        view2.setSelected(view.isPressed());
                        break;
                }
                return false;
            }
        });
    }
}
