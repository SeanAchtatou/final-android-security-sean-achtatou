package com.tencent.mm.sdk.storage;

import android.content.ContentValues;
import android.database.Cursor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

class CursorFieldHelper {
    private static final Map<Class<?>, Method> a = new HashMap();
    private static final Map<Class<?>, Method> b = new HashMap();
    private static final Map<Class<?>, String> c = new HashMap();

    static {
        try {
            a.put(byte[].class, CursorFieldHelper.class.getMethod("keep_setBlob", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Short.TYPE, CursorFieldHelper.class.getMethod("keep_setShort", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Short.class, CursorFieldHelper.class.getMethod("keep_setShort", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Boolean.TYPE, CursorFieldHelper.class.getMethod("keep_setBoolean", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Boolean.class, CursorFieldHelper.class.getMethod("keep_setBoolean", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Integer.TYPE, CursorFieldHelper.class.getMethod("keep_setInt", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Integer.class, CursorFieldHelper.class.getMethod("keep_setInt", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Float.TYPE, CursorFieldHelper.class.getMethod("keep_setFloat", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Float.class, CursorFieldHelper.class.getMethod("keep_setFloat", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Double.TYPE, CursorFieldHelper.class.getMethod("keep_setDouble", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Double.class, CursorFieldHelper.class.getMethod("keep_setDouble", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Long.TYPE, CursorFieldHelper.class.getMethod("keep_setLong", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(Long.class, CursorFieldHelper.class.getMethod("keep_setLong", Field.class, Object.class, Cursor.class, Integer.TYPE));
            a.put(String.class, CursorFieldHelper.class.getMethod("keep_setString", Field.class, Object.class, Cursor.class, Integer.TYPE));
            b.put(byte[].class, CursorFieldHelper.class.getMethod("keep_getBlob", Field.class, Object.class, ContentValues.class));
            b.put(Short.TYPE, CursorFieldHelper.class.getMethod("keep_getShort", Field.class, Object.class, ContentValues.class));
            b.put(Short.class, CursorFieldHelper.class.getMethod("keep_getShort", Field.class, Object.class, ContentValues.class));
            b.put(Boolean.TYPE, CursorFieldHelper.class.getMethod("keep_getBoolean", Field.class, Object.class, ContentValues.class));
            b.put(Boolean.class, CursorFieldHelper.class.getMethod("keep_getBoolean", Field.class, Object.class, ContentValues.class));
            b.put(Integer.TYPE, CursorFieldHelper.class.getMethod("keep_getInt", Field.class, Object.class, ContentValues.class));
            b.put(Integer.class, CursorFieldHelper.class.getMethod("keep_getInt", Field.class, Object.class, ContentValues.class));
            b.put(Float.TYPE, CursorFieldHelper.class.getMethod("keep_getFloat", Field.class, Object.class, ContentValues.class));
            b.put(Float.class, CursorFieldHelper.class.getMethod("keep_getFloat", Field.class, Object.class, ContentValues.class));
            b.put(Double.TYPE, CursorFieldHelper.class.getMethod("keep_getDouble", Field.class, Object.class, ContentValues.class));
            b.put(Double.class, CursorFieldHelper.class.getMethod("keep_getDouble", Field.class, Object.class, ContentValues.class));
            b.put(Long.TYPE, CursorFieldHelper.class.getMethod("keep_getLong", Field.class, Object.class, ContentValues.class));
            b.put(Long.class, CursorFieldHelper.class.getMethod("keep_getLong", Field.class, Object.class, ContentValues.class));
            b.put(String.class, CursorFieldHelper.class.getMethod("keep_getString", Field.class, Object.class, ContentValues.class));
            c.put(byte[].class, "BLOB");
            c.put(Short.TYPE, "SHORT");
            c.put(Short.class, "SHORT");
            c.put(Boolean.TYPE, "INTEGER");
            c.put(Boolean.class, "INTEGER");
            c.put(Integer.TYPE, "INTEGER");
            c.put(Integer.class, "INTEGER");
            c.put(Float.TYPE, "FLOAT");
            c.put(Float.class, "FLOAT");
            c.put(Double.TYPE, "DOUBLE");
            c.put(Double.class, "DOUBLE");
            c.put(Long.TYPE, "LONG");
            c.put(Long.class, "LONG");
            c.put(String.class, "TEXT");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    CursorFieldHelper() {
    }

    public static Method get(Class<?> cls, boolean z) {
        return z ? b.get(cls) : a.get(cls);
    }

    public static void keep_getBlob(Field field, Object obj, ContentValues contentValues) {
        try {
            contentValues.put(field.getName().substring("field_".length()), (byte[]) field.get(obj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_getBoolean(Field field, Object obj, ContentValues contentValues) {
        try {
            contentValues.put(field.getName().substring("field_".length()), Integer.valueOf(field.getBoolean(obj) ? 1 : 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_getDouble(Field field, Object obj, ContentValues contentValues) {
        try {
            if (!field.getType().equals(Double.TYPE)) {
                contentValues.put(field.getName().substring("field_".length()), (Double) field.get(obj));
            } else {
                contentValues.put(field.getName().substring("field_".length()), Double.valueOf(field.getDouble(obj)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_getFloat(Field field, Object obj, ContentValues contentValues) {
        try {
            if (!field.getType().equals(Float.TYPE)) {
                contentValues.put(field.getName().substring("field_".length()), (Float) field.get(obj));
            } else {
                contentValues.put(field.getName().substring("field_".length()), Float.valueOf(field.getFloat(obj)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_getInt(Field field, Object obj, ContentValues contentValues) {
        try {
            if (!field.getType().equals(Integer.TYPE)) {
                contentValues.put(field.getName().substring("field_".length()), (Integer) field.get(obj));
            } else {
                contentValues.put(field.getName().substring("field_".length()), Integer.valueOf(field.getInt(obj)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_getLong(Field field, Object obj, ContentValues contentValues) {
        try {
            if (!field.getType().equals(Long.TYPE)) {
                contentValues.put(field.getName().substring("field_".length()), (Long) field.get(obj));
            } else {
                contentValues.put(field.getName().substring("field_".length()), Long.valueOf(field.getLong(obj)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_getShort(Field field, Object obj, ContentValues contentValues) {
        try {
            contentValues.put(field.getName().substring("field_".length()), Short.valueOf(field.getShort(obj)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_getString(Field field, Object obj, ContentValues contentValues) {
        try {
            contentValues.put(field.getName().substring("field_".length()), (String) field.get(obj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_setBlob(Field field, Object obj, Cursor cursor, int i) {
        try {
            field.set(obj, cursor.getBlob(i));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_setBoolean(Field field, Object obj, Cursor cursor, int i) {
        try {
            if (field.getType().equals(Boolean.TYPE)) {
                field.setBoolean(obj, cursor.getInt(i) != 0);
            } else {
                field.set(obj, Integer.valueOf(cursor.getInt(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_setDouble(Field field, Object obj, Cursor cursor, int i) {
        try {
            if (field.getType().equals(Double.TYPE)) {
                field.setDouble(obj, cursor.getDouble(i));
            } else {
                field.set(obj, Double.valueOf(cursor.getDouble(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_setFloat(Field field, Object obj, Cursor cursor, int i) {
        try {
            if (field.getType().equals(Float.TYPE)) {
                field.setFloat(obj, cursor.getFloat(i));
            } else {
                field.set(obj, Float.valueOf(cursor.getFloat(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_setInt(Field field, Object obj, Cursor cursor, int i) {
        try {
            if (field.getType().equals(Integer.TYPE)) {
                field.setInt(obj, cursor.getInt(i));
            } else {
                field.set(obj, Integer.valueOf(cursor.getInt(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_setLong(Field field, Object obj, Cursor cursor, int i) {
        try {
            if (field.getType().equals(Long.TYPE)) {
                field.setLong(obj, cursor.getLong(i));
            } else {
                field.set(obj, Long.valueOf(cursor.getLong(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_setShort(Field field, Object obj, Cursor cursor, int i) {
        try {
            if (field.getType().equals(Short.TYPE)) {
                field.setShort(obj, cursor.getShort(i));
            } else {
                field.set(obj, Short.valueOf(cursor.getShort(i)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void keep_setString(Field field, Object obj, Cursor cursor, int i) {
        try {
            field.set(obj, cursor.getString(i));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String type(Class<?> cls) {
        return c.get(cls);
    }
}
