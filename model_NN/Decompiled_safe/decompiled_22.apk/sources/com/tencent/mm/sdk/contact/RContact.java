package com.tencent.mm.sdk.contact;

import android.content.ContentValues;
import android.database.Cursor;
import com.tencent.mm.sdk.platformtools.LVBuffer;
import com.tencent.mm.sdk.platformtools.Log;
import com.tencent.mm.sdk.platformtools.Util;
import com.tencent.mm.sdk.storage.MAutoDBItem;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class RContact extends MAutoDBItem {
    public static final String[] COLUMNS;
    public static final String COL_ALIAS = "alias";
    public static final String COL_CONREMARK = "conRemark";
    public static final String COL_CONREMARK_PYFULL = "conRemarkPYFull";
    public static final String COL_CONREMARK_PYSHORT = "conRemarkPYShort";
    public static final String COL_DOMAINLIST = "domainList";
    public static final int COL_ID_INVALID_VALUE = -1;
    public static final String COL_NICKNAME = "nickname";
    public static final String COL_PYINITIAL = "pyInitial";
    public static final String COL_QUANPIN = "quanPin";
    public static final String COL_SHOWHEAD = "showHead";
    public static final String COL_TYPE = "type";
    public static final String COL_USERNAME = "username";
    public static final String COL_WEIBOFLAG = "weiboFlag";
    public static final String COL_WEIBONICKNAME = "weiboNickname";
    public static final int DEL_CONTACT_MSG = -1;
    public static final String FAVOUR_CONTACT_SHOW_HEAD_CHAR = "$";
    public static final int FAVOUR_CONTACT_SHOW_HEAD_CODE = 32;
    public static final int MM_CONTACTFLAG_ALL = 127;
    public static final int MM_CONTACTFLAG_BLACKLISTCONTACT = 8;
    public static final int MM_CONTACTFLAG_CHATCONTACT = 2;
    public static final int MM_CONTACTFLAG_CHATROOMCONTACT = 4;
    public static final int MM_CONTACTFLAG_CONTACT = 1;
    public static final int MM_CONTACTFLAG_DOMAINCONTACT = 16;
    public static final int MM_CONTACTFLAG_FAVOURCONTACT = 64;
    public static final int MM_CONTACTFLAG_HIDECONTACT = 32;
    public static final int MM_CONTACTFLAG_NULL = 0;
    public static final int MM_CONTACTIMGFLAG_HAS_HEADIMG = 3;
    public static final int MM_CONTACTIMGFLAG_HAS_NO_HEADIMG = 4;
    public static final int MM_CONTACTIMGFLAG_LOCAL_EXIST = 153;
    public static final int MM_CONTACTIMGFLAG_MODIFY = 2;
    public static final int MM_CONTACTIMGFLAG_NOTMODIFY = 1;
    public static final int MM_CONTACT_BOTTLE = 5;
    public static final int MM_CONTACT_CHATROOM = 2;
    public static final int MM_CONTACT_EMAIL = 3;
    public static final int MM_CONTACT_QQ = 4;
    public static final int MM_CONTACT_QQMICROBLOG = 1;
    public static final int MM_CONTACT_WEIXIN = 0;
    public static final int MM_SEX_FEMALE = 2;
    public static final int MM_SEX_MALE = 1;
    public static final int MM_SEX_UNKNOWN = 0;
    public static final int NOT_IN_CHAT_LIST = 0;
    private static Field[] a;
    private static Map<String, String> x = new HashMap();
    private static Map<String, String> y = new HashMap();
    private int b;
    private int c;
    public long contactId;
    private String d;
    private long e;
    private int f;
    public String field_alias;
    public String field_conRemark;
    public String field_conRemarkPYFull;
    public String field_conRemarkPYShort;
    public String field_domainList;
    public byte[] field_lvbuff;
    public String field_nickname;
    public String field_pyInitial;
    public String field_quanPin;
    public int field_showHead;
    public int field_type;
    public String field_username;
    public int field_weiboFlag;
    public String field_weiboNickname;
    private String g;
    private String h;
    private int i;
    private int j;
    private String k;
    private String l;
    private int m;
    private int n;
    private String o;
    private String p;
    private String q;
    private String r;
    private int s;
    private int t;
    private String u;
    private int v;
    private String w;

    static {
        Field[] validFields = MAutoDBItem.getValidFields(RContact.class);
        a = validFields;
        COLUMNS = MAutoDBItem.getFullColumns(validFields);
    }

    public RContact() {
        reset();
    }

    public RContact(String str) {
        this();
        this.field_username = str == null ? "" : str;
    }

    private static boolean a(char c2) {
        return (c2 >= 'A' && c2 <= 'Z') || (c2 >= 'a' && c2 <= 'z');
    }

    private byte[] a() {
        try {
            LVBuffer lVBuffer = new LVBuffer();
            lVBuffer.initBuild();
            lVBuffer.putInt(this.b);
            lVBuffer.putInt(this.c);
            lVBuffer.putString(this.d);
            lVBuffer.putLong(this.e);
            lVBuffer.putInt(this.f);
            lVBuffer.putString(this.g);
            lVBuffer.putString(this.h);
            lVBuffer.putInt(this.i);
            lVBuffer.putInt(this.j);
            lVBuffer.putString(this.k);
            lVBuffer.putString(this.l);
            lVBuffer.putInt(this.m);
            lVBuffer.putInt(this.n);
            lVBuffer.putString(this.o);
            lVBuffer.putString(this.p);
            lVBuffer.putString(this.q);
            lVBuffer.putString(this.r);
            lVBuffer.putInt(this.s);
            lVBuffer.putInt(this.t);
            lVBuffer.putString(this.u);
            lVBuffer.putInt(this.v);
            lVBuffer.putString(this.w);
            return lVBuffer.buildFinish();
        } catch (Exception e2) {
            Log.e("MicroMsg.Contact", "get value failed");
            e2.printStackTrace();
            return null;
        }
    }

    public static String formatDisplayNick(String str) {
        if (str == null) {
            return null;
        }
        return str.toLowerCase().endsWith("@t.qq.com") ? "@" + str.replace("@t.qq.com", "") : str.toLowerCase().endsWith("@qqim") ? str.replace("@qqim", "") : str;
    }

    public static int getBlackListContactBit() {
        return 8;
    }

    public static int getContactBit() {
        return 1;
    }

    public static int getDomainContactBit() {
        return 16;
    }

    public static int getHiddenContactBit() {
        return 32;
    }

    public static boolean isContact(int i2) {
        return (i2 & 1) != 0;
    }

    public static void setHardCodeAliasMaps(Map<String, String> map) {
        y = map;
    }

    public static void setHardCodeNickMaps(Map<String, String> map) {
        x = map;
    }

    public int calculateShowHead() {
        char c2 = ' ';
        if (this.field_conRemarkPYShort != null && !this.field_conRemarkPYShort.equals("")) {
            c2 = this.field_conRemarkPYShort.charAt(0);
        } else if (this.field_conRemarkPYFull != null && !this.field_conRemarkPYFull.equals("")) {
            c2 = this.field_conRemarkPYFull.charAt(0);
        } else if (this.field_pyInitial != null && !this.field_pyInitial.equals("")) {
            c2 = this.field_pyInitial.charAt(0);
        } else if (this.field_quanPin != null && !this.field_quanPin.equals("")) {
            c2 = this.field_quanPin.charAt(0);
        } else if (this.field_nickname != null && !this.field_nickname.equals("") && a(this.field_nickname.charAt(0))) {
            c2 = this.field_nickname.charAt(0);
        } else if (this.field_username != null && !this.field_username.equals("") && a(this.field_username.charAt(0))) {
            c2 = this.field_username.charAt(0);
        }
        if (c2 >= 'a' && c2 <= 'z') {
            return (char) (c2 - ' ');
        }
        if (c2 == '@') {
            return c2;
        }
        if (c2 < 'A' || c2 > 'Z') {
            return 123;
        }
        return c2;
    }

    public void convertFrom(Cursor cursor) {
        super.convertFrom(cursor);
        this.contactId = (long) ((int) cursor.getLong(cursor.getColumnCount() - 1));
        byte[] bArr = this.field_lvbuff;
        try {
            LVBuffer lVBuffer = new LVBuffer();
            int initParse = lVBuffer.initParse(bArr);
            if (initParse != 0) {
                Log.e("MicroMsg.Contact", "parse LVBuffer error:" + initParse);
                return;
            }
            this.b = lVBuffer.getInt();
            this.c = lVBuffer.getInt();
            this.d = lVBuffer.getString();
            this.e = lVBuffer.getLong();
            this.f = lVBuffer.getInt();
            this.g = lVBuffer.getString();
            this.h = lVBuffer.getString();
            this.i = lVBuffer.getInt();
            this.j = lVBuffer.getInt();
            this.k = lVBuffer.getString();
            this.l = lVBuffer.getString();
            this.m = lVBuffer.getInt();
            this.n = lVBuffer.getInt();
            this.o = lVBuffer.getString();
            this.p = lVBuffer.getString();
            this.q = lVBuffer.getString();
            this.r = lVBuffer.getString();
            this.s = lVBuffer.getInt();
            this.t = lVBuffer.getInt();
            this.u = lVBuffer.getString();
            this.v = lVBuffer.getInt();
            this.w = lVBuffer.getString();
        } catch (Exception e2) {
            Log.e("MicroMsg.Contact", "get value failed");
            e2.printStackTrace();
        }
    }

    public ContentValues convertTo() {
        this.field_lvbuff = a();
        return super.convertTo();
    }

    public Field[] fields() {
        return a;
    }

    public String getAlias() {
        String str = y.get(this.field_username);
        return str == null ? this.field_alias : str;
    }

    public int getChatroomNotify() {
        return this.m;
    }

    public String getCity() {
        return this.q;
    }

    public String getConQQMBlog() {
        return this.l;
    }

    public String getConRemark() {
        return this.field_conRemark;
    }

    public String getConRemarkPYFull() {
        return this.field_conRemarkPYFull;
    }

    public String getConRemarkPYShort() {
        return this.field_conRemarkPYShort;
    }

    public String getConSMBlog() {
        return this.k;
    }

    public int getConType() {
        return this.j;
    }

    public int getContactID() {
        return (int) this.contactId;
    }

    public String getDisplayNick() {
        String str = x.get(this.field_username);
        return str != null ? str : (this.field_nickname == null || this.field_nickname.length() <= 0) ? getDisplayUser() : this.field_nickname;
    }

    public String getDisplayRemark() {
        return (this.field_conRemark == null || this.field_conRemark.trim().equals("")) ? getDisplayNick() : this.field_conRemark;
    }

    public String getDisplayUser() {
        String alias = getAlias();
        if (!Util.isNullOrNil(alias)) {
            return alias;
        }
        String formatDisplayNick = formatDisplayNick(this.field_username);
        return (formatDisplayNick == null || formatDisplayNick.length() == 0) ? this.field_username : formatDisplayNick;
    }

    public String getDistance() {
        return this.r;
    }

    public String getDomainList() {
        return this.field_domainList;
    }

    public String getEmail() {
        return this.g;
    }

    public long getFaceBookId() {
        return this.e;
    }

    public String getFaceBookUsername() {
        return this.d;
    }

    public int getFromType() {
        return this.s;
    }

    public int getImgFlag() {
        return this.b;
    }

    public String getMobile() {
        return this.h;
    }

    public String getNickname() {
        return this.field_nickname;
    }

    public int getPersonalCard() {
        return this.n;
    }

    public String getProvince() {
        return this.p;
    }

    public String getPyInitial() {
        return (this.field_pyInitial == null || this.field_pyInitial.length() < 0) ? getQuanPin() : this.field_pyInitial;
    }

    public String getQuanPin() {
        return (this.field_quanPin == null || this.field_quanPin.length() < 0) ? getNickname() : this.field_quanPin;
    }

    public int getSex() {
        return this.c;
    }

    public int getShowFlag() {
        return this.i;
    }

    public int getShowHead() {
        return this.field_showHead;
    }

    public String getSignature() {
        return this.o;
    }

    public int getSource() {
        return this.t;
    }

    public int getType() {
        return this.field_type;
    }

    public int getUin() {
        return this.f;
    }

    public String getUsername() {
        return this.field_username;
    }

    public int getVerifyFlag() {
        return this.v;
    }

    public String getVerifyInfo() {
        return this.w;
    }

    public String getWeibo() {
        return this.u;
    }

    public int getWeiboFlag() {
        return this.field_weiboFlag;
    }

    public String getWeiboNickName() {
        return this.field_weiboNickname;
    }

    public boolean isBlackListContact() {
        return (this.field_type & 8) != 0;
    }

    public boolean isChatContact() {
        return (this.field_type & 2) != 0;
    }

    public boolean isChatRoomContact() {
        return (this.field_type & 4) != 0;
    }

    public boolean isContact() {
        return isContact(this.field_type);
    }

    public boolean isDomainContact() {
        return (this.field_type & 16) != 0;
    }

    public boolean isFavourContact() {
        return (this.field_type & 64) != 0;
    }

    public boolean isHidden() {
        return (this.field_type & 32) != 0;
    }

    public boolean isImgLocalExist() {
        return 153 == this.b;
    }

    public void reset() {
        this.field_username = "";
        this.field_nickname = "";
        this.field_pyInitial = "";
        this.field_quanPin = "";
        this.field_alias = "";
        this.field_conRemark = "";
        this.field_conRemarkPYShort = "";
        this.field_conRemarkPYFull = "";
        this.field_domainList = "";
        this.field_weiboFlag = 0;
        this.field_weiboNickname = "";
        this.field_showHead = 0;
        this.field_type = 0;
        this.c = 0;
        this.r = "";
        this.s = 0;
        this.f = 0;
        this.g = "";
        this.h = "";
        this.i = 0;
        this.j = 0;
        this.k = "";
        this.l = "";
        this.m = 1;
        this.b = 0;
        this.n = 0;
        this.o = "";
        this.p = "";
        this.q = "";
        this.v = 0;
        this.t = 0;
        this.w = "";
        this.u = "";
        this.e = 0;
        this.d = "";
    }

    public void setAlias(String str) {
        this.field_alias = str;
    }

    public void setBlackList() {
        this.field_type |= 8;
    }

    public void setChatContact() {
        this.field_type |= 2;
    }

    public void setChatroomContact() {
        this.field_type |= 4;
    }

    public void setChatroomNotify(int i2) {
        this.m = i2;
    }

    public void setCity(String str) {
        this.q = str;
    }

    public void setConQQMBlog(String str) {
        this.l = str;
    }

    public void setConRemark(String str) {
        this.field_conRemark = str;
    }

    public void setConRemarkPYFull(String str) {
        this.field_conRemarkPYFull = str;
    }

    public void setConRemarkPYShort(String str) {
        this.field_conRemarkPYShort = str;
    }

    public void setConSMBlog(String str) {
        this.k = str;
    }

    public void setConType(int i2) {
        this.j = i2;
    }

    public void setContact() {
        this.field_type |= 1;
    }

    public void setDistance(String str) {
        this.r = str;
    }

    public void setDomainList(String str) {
        this.field_domainList = str;
    }

    public void setEmail(String str) {
        this.g = str;
    }

    public void setFaceBookId(long j2) {
        this.e = j2;
    }

    public void setFaceBookUsername(String str) {
        this.d = str;
    }

    public void setFavour() {
        this.field_type |= 64;
    }

    public void setFromType(int i2) {
        this.s = i2;
    }

    public void setHidden() {
        this.field_type |= 32;
    }

    public void setImgFlag(int i2) {
        this.b = i2;
    }

    public void setMobile(String str) {
        this.h = str;
    }

    public void setNickname(String str) {
        this.field_nickname = str;
    }

    public void setNullContact() {
        this.field_type = 0;
    }

    public void setPersonalCard(int i2) {
        this.n = i2;
    }

    public void setProvince(String str) {
        this.p = str;
    }

    public void setPyInitial(String str) {
        this.field_pyInitial = str;
    }

    public void setQuanPin(String str) {
        this.field_quanPin = str;
    }

    public void setSex(int i2) {
        this.c = i2;
    }

    public void setShowFlag(int i2) {
        this.i = i2;
    }

    public void setShowHead(int i2) {
        this.field_showHead = i2;
    }

    public void setSignature(String str) {
        this.o = str;
    }

    public void setSource(int i2) {
        this.t = i2;
    }

    public void setType(int i2) {
        this.field_type = i2;
    }

    public void setUin(int i2) {
        this.f = i2;
    }

    public void setUsername(String str) {
        this.field_username = str;
    }

    public void setVerifyFlag(int i2) {
        this.v = i2;
    }

    public void setVerifyInfo(String str) {
        this.w = str;
    }

    public void setWeibo(String str) {
        this.u = str;
    }

    public void setWeiboFlag(int i2) {
        this.field_weiboFlag = i2;
    }

    public void setWeiboNickName(String str) {
        this.field_weiboNickname = str;
    }

    public void unSetBlackList() {
        this.field_type &= -9;
    }

    public void unSetChatContact() {
        this.field_type &= -3;
    }

    public void unSetContact() {
        this.field_type &= -2;
    }

    public void unSetFavour() {
        this.field_type &= -65;
    }

    public void unSetHidden() {
        this.field_type &= -33;
    }
}
