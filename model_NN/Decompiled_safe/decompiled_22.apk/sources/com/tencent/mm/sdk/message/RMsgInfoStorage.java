package com.tencent.mm.sdk.message;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.tencent.mm.sdk.platformtools.Util;
import com.tencent.mm.sdk.storage.ISQLiteDatabase;
import com.tencent.mm.sdk.storage.MStorage;

public class RMsgInfoStorage extends MStorage {
    public static final String AUTHORITY = "com.tencent.mm.sdk.msginfo.provider";
    public static final String PRIMARY_KEY = "msgId";
    private ISQLiteDatabase a = null;

    private RMsgInfoStorage(ISQLiteDatabase iSQLiteDatabase) {
        this.a = iSQLiteDatabase;
    }

    public static RMsgInfoStorage create(Context context) {
        return new RMsgInfoStorage(new RMsgInfoDB(context));
    }

    public int doDelete(long j) {
        int delete = this.a.delete("message", "msgId=?", new String[]{"" + j});
        if (delete > 0) {
            notify();
        }
        return delete;
    }

    public long doInsert(RMsgInfo rMsgInfo) {
        if (rMsgInfo == null || Util.isNullOrNil(rMsgInfo.field_talker)) {
            return -1;
        }
        long insert = this.a.insert("message", "msgId", rMsgInfo.convertTo());
        if (insert <= 0) {
            return insert;
        }
        notify();
        return insert;
    }

    public int doUpdate(long j, RMsgInfo rMsgInfo) {
        if (rMsgInfo == null || Util.isNullOrNil(rMsgInfo.field_talker)) {
            return -1;
        }
        ContentValues convertTo = rMsgInfo.convertTo();
        int update = this.a.update("message", convertTo, "msgId=?", new String[]{"" + j});
        if (update <= 0) {
            return update;
        }
        notify();
        return update;
    }

    public RMsgInfo getMsgById(long j) {
        Cursor query = this.a.query("message", null, "msgId=?", new String[]{"" + j}, null, null, null);
        if (query == null) {
            return null;
        }
        if (query.getCount() == 0) {
            query.close();
            return null;
        }
        RMsgInfo rMsgInfo = new RMsgInfo();
        rMsgInfo.convertFrom(query);
        return rMsgInfo;
    }

    public Cursor getMsgByTalker(String str) {
        return this.a.query("message", null, "talker=?", new String[]{"" + str}, null, null, null);
    }
}
