package com.weibo.sdk.android.api;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import java.net.URLEncoder;

public class ActivityInvokeAPI {
    public static void openSendWeibo(Activity activity, String content) {
        if (activity != null && content != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://sendweibo?content=" + URLEncoder.encode(content)));
            activity.startActivity(intent);
        }
    }

    public static void openSendWeibo(Activity activity, String content, String xid, String poiId, String poiName, String longitude, String latitude) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://sendweibo?content=" + content + "&xid=" + xid + "&poiid=" + poiId + "&poiname=" + poiName + "&longitude=" + longitude + "&latitude=" + latitude));
            activity.startActivity(intent);
        }
    }

    public static void openNearbyPeople(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://nearbypeople"));
            activity.startActivity(intent);
        }
    }

    public static void openNearbyWeibo(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://nearbyweibo"));
            activity.startActivity(intent);
        }
    }

    public static void openUserInfoByNickName(Activity activity, String nickName) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://userinfo?nick=" + nickName));
            activity.startActivity(intent);
        }
    }

    public static void openUserInfoByUid(Activity activity, String uid) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://userinfo?uid=" + uid));
            activity.startActivity(intent);
        }
    }

    public static void openWeiboBrowser(Activity activity, String url) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://browser?url=" + url));
            activity.startActivity(intent);
        }
    }

    public static void openWeibo(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://splash"));
            activity.startActivity(intent);
        }
    }

    public static void openShake(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://shake"));
            activity.startActivity(intent);
        }
    }

    public static void openContact(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://contact"));
            activity.startActivity(intent);
        }
    }

    public static void openUserTrends(Activity activity, String uid) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://usertrends?uid=" + uid));
            activity.startActivity(intent);
        }
    }

    public static void openMessageListByUid(Activity activity, String uid) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://messagelist?uid=" + uid));
            activity.startActivity(intent);
        }
    }

    public static void openMessageListByNickName(Activity activity, String nickName) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://messagelist?nick=" + nickName));
            activity.startActivity(intent);
        }
    }

    public static void openDetail(Activity activity, String blogId) {
        if (activity != null) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setData(Uri.parse("sinaweibo://detail?mblogid=" + blogId));
            activity.startActivity(intent);
        }
    }
}
