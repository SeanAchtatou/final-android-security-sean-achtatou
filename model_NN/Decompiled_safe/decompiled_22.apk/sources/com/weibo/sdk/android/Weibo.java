package com.weibo.sdk.android;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieSyncManager;
import com.weibo.sdk.android.util.Utility;
import sina_weibo.Constants;

public class Weibo {
    public static final String KEY_EXPIRES = "expires_in";
    public static final String KEY_REFRESHTOKEN = "refresh_token";
    public static final String KEY_TOKEN = "access_token";
    public static String URL_OAUTH2_ACCESS_AUTHORIZE = "https://open.weibo.cn/oauth2/authorize";
    private static final String WEIBO_SDK_VERSION = "2.0";
    public static String app_key = "";
    public static boolean isWifi = false;
    private static Weibo mWeiboInstance = null;
    public static String redirecturl = "";
    public Oauth2AccessToken accessToken = null;

    public static synchronized Weibo getInstance(String appKey, String redirectUrl) {
        Weibo weibo;
        synchronized (Weibo.class) {
            if (mWeiboInstance == null) {
                mWeiboInstance = new Weibo();
            }
            app_key = appKey;
            redirecturl = redirectUrl;
            weibo = mWeiboInstance;
        }
        return weibo;
    }

    public void setupConsumerConfig(String appKey, String redirectUrl) {
        app_key = appKey;
        redirecturl = redirectUrl;
    }

    public void authorize(Context context, WeiboAuthListener listener) {
        isWifi = Utility.isWifi(context);
        startAuthDialog(context, listener);
    }

    public void startAuthDialog(Context context, final WeiboAuthListener listener) {
        startDialog(context, new WeiboParameters(), new WeiboAuthListener() {
            public void onComplete(Bundle values) {
                CookieSyncManager.getInstance().sync();
                if (Weibo.this.accessToken == null) {
                    Weibo.this.accessToken = new Oauth2AccessToken();
                }
                Weibo.this.accessToken.setToken(values.getString("access_token"));
                Weibo.this.accessToken.setExpiresIn(values.getString("expires_in"));
                Weibo.this.accessToken.setRefreshToken(values.getString(Weibo.KEY_REFRESHTOKEN));
                if (Weibo.this.accessToken.isSessionValid()) {
                    Log.d("Weibo-authorize", "Login Success! access_token=" + Weibo.this.accessToken.getToken() + " expires=" + Weibo.this.accessToken.getExpiresTime() + " refresh_token=" + Weibo.this.accessToken.getRefreshToken());
                    listener.onComplete(values);
                    return;
                }
                Log.d("Weibo-authorize", "Failed to receive access token");
                listener.onWeiboException(new WeiboException("Failed to receive access token."));
            }

            public void onError(WeiboDialogError error) {
                Log.d("Weibo-authorize", "Login failed: " + error);
                listener.onError(error);
            }

            public void onWeiboException(WeiboException error) {
                Log.d("Weibo-authorize", "Login failed: " + error);
                listener.onWeiboException(error);
            }

            public void onCancel() {
                Log.d("Weibo-authorize", "Login canceled");
                listener.onCancel();
            }
        });
    }

    public void startDialog(Context context, WeiboParameters parameters, WeiboAuthListener listener) {
        parameters.add("client_id", app_key);
        parameters.add("response_type", "token");
        parameters.add(Constants.SINA_REDIRECT_URI, redirecturl);
        parameters.add("display", "mobile");
        if (this.accessToken != null && this.accessToken.isSessionValid()) {
            parameters.add("access_token", this.accessToken.getToken());
        }
        String url = String.valueOf(URL_OAUTH2_ACCESS_AUTHORIZE) + "?" + Utility.encodeUrl(parameters);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            Utility.showAlert(context, "Error", "Application requires permission to access the Internet");
        } else {
            new WeiboDialog(context, url, listener).show();
        }
    }
}
