package com.weibo.sdk.android.api;

import com.tencent.mm.sdk.contact.RContact;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.net.RequestListener;

public class RegisterAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/register";

    public RegisterAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void suggestions(String nickname, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(RContact.COL_NICKNAME, nickname);
        request("https://api.weibo.com/2/register/verify_nickname.json", params, "GET", listener);
    }
}
