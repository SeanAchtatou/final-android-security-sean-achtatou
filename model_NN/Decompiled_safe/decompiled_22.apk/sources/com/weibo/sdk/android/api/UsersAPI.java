package com.weibo.sdk.android.api;

import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.net.RequestListener;
import sina_weibo.Constants;
import sina_weibo.WeiboShareDao;

public class UsersAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/users";

    public UsersAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void show(long uid, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        request(WeiboShareDao.main_sina_weibo, params, "GET", listener);
    }

    public void show(String screen_name, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("screen_name", screen_name);
        request(WeiboShareDao.main_sina_weibo, params, "GET", listener);
    }

    public void domainShow(String domain, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("domain", domain);
        request("https://api.weibo.com/2/users/domain_show.json", params, "GET", listener);
    }

    public void counts(long[] uids, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        StringBuilder strb = new StringBuilder();
        for (long cid : uids) {
            strb.append(String.valueOf(cid)).append(",");
        }
        strb.deleteCharAt(strb.length() - 1);
        params.add("uids", strb.toString());
        request("https://api.weibo.com/2/users/counts.json", params, "GET", listener);
    }
}
