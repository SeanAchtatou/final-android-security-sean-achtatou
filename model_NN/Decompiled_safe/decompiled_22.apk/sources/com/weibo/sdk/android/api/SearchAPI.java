package com.weibo.sdk.android.api;

import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.RequestListener;

public class SearchAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/search";

    public SearchAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void users(String q, int count, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("q", q);
        params.add("count", count);
        request("https://api.weibo.com/2/search/suggestions/users.json", params, "GET", listener);
    }

    public void statuses(String q, int count, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("q", q);
        params.add("count", count);
        request("https://api.weibo.com/2/search/suggestions/statuses.json", params, "GET", listener);
    }

    public void schools(String q, int count, WeiboAPI.SCHOOL_TYPE type, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("q", q);
        params.add("count", count);
        params.add("type", type.ordinal());
        request("https://api.weibo.com/2/search/suggestions/schools.json", params, "GET", listener);
    }

    public void companies(String q, int count, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("q", q);
        params.add("count", count);
        request("https://api.weibo.com/2/search/suggestions/companies.json", params, "GET", listener);
    }

    public void apps(String q, int count, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("q", q);
        params.add("count", count);
        request("https://api.weibo.com/2/search/suggestions/apps.json", params, "GET", listener);
    }

    public void atUsers(String q, int count, WeiboAPI.FRIEND_TYPE type, WeiboAPI.RANGE range, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("q", q);
        params.add("count", count);
        params.add("type", type.ordinal());
        params.add("range", range.ordinal());
        request("https://api.weibo.com/2/search/suggestions/at_users.json", params, "GET", listener);
    }
}
