package com.weibo.sdk.android.api;

import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.RequestListener;
import sina_weibo.Constants;

public class SuggestionsAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/suggestions";

    public SuggestionsAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void usersHot(WeiboAPI.USER_CATEGORY category, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("category", category.name());
        request("https://api.weibo.com/2/suggestions/users/hot.json", params, "GET", listener);
    }

    public void mayInterested(int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/suggestions/users/may_interested.json", params, "GET", listener);
    }

    public void byStatus(String content, int num, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("content", content);
        params.add("num", num);
        request("https://api.weibo.com/2/suggestions/users/may_interested.json", params, "GET", listener);
    }

    public void statusesHot(WeiboAPI.STATUSES_TYPE type, boolean is_pic, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("type", type.ordinal() + 1);
        if (is_pic) {
            params.add("is_pic", 1);
        } else {
            params.add("is_pic", 0);
        }
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/suggestions/statuses/hot.json", params, "GET", listener);
    }

    public void favoritesHot(int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/suggestions/favorites/hot.json", params, "GET", listener);
    }

    public void notInterested(long uid, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        request("https://api.weibo.com/2/suggestions/users/not_interested.json", params, "POST", listener);
    }
}
