package com.weibo.sdk.android.net;

import com.weibo.sdk.android.WeiboException;
import java.io.IOException;

public interface RequestListener {
    void onComplete(String str);

    void onError(WeiboException weiboException);

    void onIOException(IOException iOException);
}
