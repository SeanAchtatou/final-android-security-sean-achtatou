package com.weibo.sdk.android.api;

import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.net.RequestListener;

public class ShortUrlAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/short_url";

    public ShortUrlAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void shorten(String[] url_long, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (url_long != null) {
            for (String add : url_long) {
                params.add("url_long", add);
            }
        }
        request("https://api.weibo.com/2/short_url/shorten.json", params, "GET", listener);
    }

    public void expand(String[] url_short, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (url_short != null) {
            for (String add : url_short) {
                params.add("url_short", add);
            }
        }
        request("https://api.weibo.com/2/short_url/expand.json", params, "GET", listener);
    }

    public void clicks(String[] url_short, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (url_short != null) {
            for (String add : url_short) {
                params.add("url_short", add);
            }
        }
        request("https://api.weibo.com/2/short_url/clicks.json", params, "GET", listener);
    }

    public void referers(String url_short, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("url_short", url_short);
        request("https://api.weibo.com/2/short_url/referers.json", params, "GET", listener);
    }

    public void locations(String url_short, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("url_short", url_short);
        request("https://api.weibo.com/2/short_url/locations.json", params, "GET", listener);
    }

    public void shareCounts(String[] url_short, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (url_short != null) {
            for (String add : url_short) {
                params.add("url_short", add);
            }
        }
        request("https://api.weibo.com/2/short_url/share/counts.json", params, "GET", listener);
    }

    public void shareStatuses(String url_short, long since_id, long max_id, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("url_short", url_short);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/short_url/share/statuses.json", params, "GET", listener);
    }

    public void commentCounts(String[] url_short, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (url_short != null) {
            for (String add : url_short) {
                params.add("url_short", add);
            }
        }
        request("https://api.weibo.com/2/short_url/comment/counts.json", params, "GET", listener);
    }

    public void comments(String url_short, long since_id, long max_id, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("url_short", url_short);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/short_url/comment/comments.json", params, "GET", listener);
    }
}
