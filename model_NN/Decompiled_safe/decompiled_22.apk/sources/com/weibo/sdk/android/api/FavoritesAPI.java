package com.weibo.sdk.android.api;

import com.baidu.android.pushservice.PushConstants;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.net.RequestListener;

public class FavoritesAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/favorites";

    public FavoritesAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void favorites(int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/favorites.json", params, "GET", listener);
    }

    public void ids(int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/favorites/ids.json", params, "GET", listener);
    }

    public void show(long id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        request("https://api.weibo.com/2/favorites/show.json", params, "GET", listener);
    }

    public void byTags(long tid, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("tid", tid);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/favorites/by_tags.json", params, "GET", listener);
    }

    public void tags(int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/favorites/tags.json", params, "GET", listener);
    }

    public void byTagsIds(long tid, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("tid", tid);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/favorites/by_tags/ids.json", params, "GET", listener);
    }

    public void create(long id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        request("https://api.weibo.com/2/favorites/create.json", params, "POST", listener);
    }

    public void destroy(long id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        request("https://api.weibo.com/2/favorites/destroy.json", params, "POST", listener);
    }

    public void destroyBatch(long[] ids, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        StringBuilder strb = new StringBuilder();
        for (long id : ids) {
            strb.append(String.valueOf(id)).append(",");
        }
        strb.deleteCharAt(strb.length() - 1);
        params.add("ids", strb.toString());
        request("https://api.weibo.com/2/favorites/destroy_batch.json", params, "POST", listener);
    }

    public void tagsUpdate(long id, String[] tags, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        StringBuilder strb = new StringBuilder();
        for (String tag : tags) {
            strb.append(tag).append(",");
        }
        strb.deleteCharAt(strb.length() - 1);
        params.add(PushConstants.EXTRA_TAGS, strb.toString());
        request("https://api.weibo.com/2/favorites/tags/update.json", params, "POST", listener);
    }

    public void tagsUpdateBatch(long id, String tag, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("tid", id);
        params.add("tag", tag);
        request("https://api.weibo.com/2/favorites/tags/update_batch.json", params, "POST", listener);
    }

    public void tagsDestroyBatch(long tid, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("tid", tid);
        request("https://api.weibo.com/2/favorites/tags/destroy_batch.json", params, "POST", listener);
    }
}
