package com.weibo.sdk.android.api;

import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.RequestListener;

public class CommentsAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/comments";

    public CommentsAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void show(long id, long since_id, long max_id, int count, int page, WeiboAPI.AUTHOR_FILTER filter_by_author, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        params.add("filter_by_author", filter_by_author.ordinal());
        request("https://api.weibo.com/2/comments/show.json", params, "GET", listener);
    }

    public void byME(long since_id, long max_id, int count, int page, WeiboAPI.SRC_FILTER filter_by_source, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        params.add("filter_by_source", filter_by_source.ordinal());
        request("https://api.weibo.com/2/comments/by_me.json", params, "GET", listener);
    }

    public void toME(long since_id, long max_id, int count, int page, WeiboAPI.AUTHOR_FILTER filter_by_author, WeiboAPI.SRC_FILTER filter_by_source, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        params.add("filter_by_author", filter_by_author.ordinal());
        params.add("filter_by_source", filter_by_source.ordinal());
        request("https://api.weibo.com/2/comments/to_me.json", params, "GET", listener);
    }

    public void timeline(long since_id, long max_id, int count, int page, boolean trim_user, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (trim_user) {
            params.add("trim_user", 1);
        } else {
            params.add("trim_user", 0);
        }
        request("https://api.weibo.com/2/comments/timeline.json", params, "GET", listener);
    }

    public void mentions(long since_id, long max_id, int count, int page, WeiboAPI.AUTHOR_FILTER filter_by_author, WeiboAPI.SRC_FILTER filter_by_source, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        params.add("filter_by_author", filter_by_author.ordinal());
        params.add("filter_by_source", filter_by_source.ordinal());
        request("https://api.weibo.com/2/comments/mentions.json", params, "GET", listener);
    }

    public void showBatch(long[] cids, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        StringBuilder strb = new StringBuilder();
        for (long cid : cids) {
            strb.append(String.valueOf(cid)).append(",");
        }
        strb.deleteCharAt(strb.length() - 1);
        params.add("cids", strb.toString());
        request("https://api.weibo.com/2/comments/show_batch.json", params, "GET", listener);
    }

    public void create(String comment, long id, boolean comment_ori, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("comment", comment);
        params.add(LocaleUtil.INDONESIAN, id);
        if (comment_ori) {
            params.add("comment_ori", 0);
        } else {
            params.add("comment_ori", 1);
        }
        request("https://api.weibo.com/2/comments/create.json", params, "POST", listener);
    }

    public void destroy(long cid, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("cid", cid);
        request("https://api.weibo.com/2/comments/destroy.json", params, "POST", listener);
    }

    public void destroyBatch(long[] ids, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        StringBuilder strb = new StringBuilder();
        for (long cid : ids) {
            strb.append(String.valueOf(cid)).append(",");
        }
        strb.deleteCharAt(strb.length() - 1);
        params.add("ids", strb.toString());
        request("https://api.weibo.com/2/comments/sdestroy_batch.json", params, "POST", listener);
    }

    public void reply(long cid, long id, String comment, boolean without_mention, boolean comment_ori, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("cid", cid);
        params.add(LocaleUtil.INDONESIAN, id);
        params.add("comment", comment);
        if (without_mention) {
            params.add("without_mention", 1);
        } else {
            params.add("without_mention", 0);
        }
        if (comment_ori) {
            params.add("comment_ori", 1);
        } else {
            params.add("comment_ori", 0);
        }
        request("https://api.weibo.com/2/comments/reply.json", params, "POST", listener);
    }
}
