package com.weibo.sdk.android.api;

import android.text.TextUtils;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.RequestListener;

public class AccountAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/account";

    public AccountAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void getPrivacy(RequestListener listener) {
        request("https://api.weibo.com/2/account/get_privacy.json", new WeiboParameters(), "GET", listener);
    }

    public void schoolList(int province, int city, int area, WeiboAPI.SCHOOL_TYPE type, WeiboAPI.CAPITAL capital, String keyword, int count, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("province", province);
        params.add("city", city);
        params.add("area", area);
        params.add("type", type.ordinal() + 1);
        if (!TextUtils.isEmpty(capital.name())) {
            params.add("capital", capital.name());
        } else if (!TextUtils.isEmpty(keyword)) {
            params.add("keyword", keyword);
        }
        params.add("count", count);
        request("https://api.weibo.com/2/account/profile/school_list.json", params, "GET", listener);
    }

    public void rateLimitStatus(RequestListener listener) {
        request("https://api.weibo.com/2/account/rate_limit_status.json", new WeiboParameters(), "GET", listener);
    }

    public void getUid(RequestListener listener) {
        request("https://api.weibo.com/2/account/get_uid.json", new WeiboParameters(), "GET", listener);
    }

    public void endSession(RequestListener listener) {
        request("https://api.weibo.com/2/account/end_session.json", new WeiboParameters(), "POST", listener);
    }
}
