package com.weibo.sdk.android.api;

import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.net.RequestListener;
import sina_weibo.Constants;

public class TrendsAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/trends";

    public TrendsAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void trends(long uid, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/trends.json", params, "GET", listener);
    }

    public void isFollow(String trend_name, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("trend_name", trend_name);
        request("https://api.weibo.com/2/trends/is_follow.json", params, "GET", listener);
    }

    public void hourly(boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/trends/hourly.json", params, "GET", listener);
    }

    public void daily(boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/trends/daily.json", params, "GET", listener);
    }

    public void weekly(boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (base_app) {
            params.add("base_app", 0);
        } else {
            params.add("base_app", 1);
        }
        request("https://api.weibo.com/2/trends/weekly.json", params, "GET", listener);
    }

    public void follow(String trend_name, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("trend_name", trend_name);
        request("https://api.weibo.com/2/trends/follow.json", params, "POST", listener);
    }

    public void destroy(long trend_id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("trend_id", trend_id);
        request("https://api.weibo.com/2/trends/destroy.json", params, "POST", listener);
    }
}
