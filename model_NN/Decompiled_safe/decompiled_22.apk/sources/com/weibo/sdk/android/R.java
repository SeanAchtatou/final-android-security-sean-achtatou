package com.weibo.sdk.android;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static int dialog_title_blue = com.pengyou.citycommercialarea.R.layout.abs__action_bar_tab_bar_view;
        public static int text_num_gray = com.pengyou.citycommercialarea.R.layout.abs__action_bar_tab;
        public static int transparent = com.pengyou.citycommercialarea.R.layout.abs__action_bar_home;
    }

    public static final class dimen {
        public static int weibosdk_dialog_bottom_margin = com.pengyou.citycommercialarea.R.anim.init_in;
        public static int weibosdk_dialog_left_margin = com.pengyou.citycommercialarea.R.anim.gridview_alpha;
        public static int weibosdk_dialog_right_margin = com.pengyou.citycommercialarea.R.anim.init_alphas;
        public static int weibosdk_dialog_top_margin = com.pengyou.citycommercialarea.R.anim.init_alpha;
    }

    public static final class drawable {
        public static int weibosdk_dialog_bg = com.pengyou.citycommercialarea.R.drawable.abs__ab_bottom_solid_dark_holo;
    }
}
