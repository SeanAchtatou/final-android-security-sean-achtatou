package com.weibo.sdk.android.api;

import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.RequestListener;

public class CommonAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/common";

    public CommonAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void getCity(String province, WeiboAPI.CAPITAL capital, String language, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("province", province);
        if (capital != null) {
            params.add("capital", capital.name().toLowerCase());
        }
        params.add("language", language);
        request("https://api.weibo.com/2/common/get_city.json", params, "GET", listener);
    }

    public void getCountry(WeiboAPI.CAPITAL capital, String language, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (capital != null) {
            params.add("capital", capital.name().toLowerCase());
        }
        params.add("language", language);
        request("https://api.weibo.com/2/common/get_country.json", params, "GET", listener);
    }

    public void getTimezone(String language, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("language", language);
        request("https://api.weibo.com/2/common/get_timezone.json", params, "GET", listener);
    }
}
