package com.weibo.sdk.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.NinePatchDrawable;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.weibo.sdk.android.util.Utility;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import sina_weibo.Constants;

public class WeiboDialog extends Dialog {
    static FrameLayout.LayoutParams FILL = new FrameLayout.LayoutParams(-1, -1);
    private static final String TAG = "Weibo-WebView";
    private static int bottom_margin = 0;
    private static int left_margin = 0;
    private static int right_margin = 0;
    private static int theme = 16973840;
    private static int top_margin = 0;
    private RelativeLayout mContent;
    /* access modifiers changed from: private */
    public WeiboAuthListener mListener;
    /* access modifiers changed from: private */
    public ProgressDialog mSpinner;
    private String mUrl;
    /* access modifiers changed from: private */
    public WebView mWebView;
    private RelativeLayout webViewContainer;

    public WeiboDialog(Context context, String url, WeiboAuthListener listener) {
        super(context, theme);
        this.mUrl = url;
        this.mListener = listener;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSpinner = new ProgressDialog(getContext());
        this.mSpinner.requestWindowFeature(1);
        this.mSpinner.setMessage("Loading...");
        this.mSpinner.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                WeiboDialog.this.onBack();
                return false;
            }
        });
        requestWindowFeature(1);
        getWindow().setFeatureDrawableAlpha(0, 0);
        this.mContent = new RelativeLayout(getContext());
        setUpWebView();
        addContentView(this.mContent, new ViewGroup.LayoutParams(-1, -1));
    }

    /* access modifiers changed from: protected */
    public void onBack() {
        try {
            this.mSpinner.dismiss();
            if (this.mWebView != null) {
                this.mWebView.stopLoading();
                this.mWebView.destroy();
            }
        } catch (Exception e) {
        }
        dismiss();
    }

    private void setUpWebView() {
        this.webViewContainer = new RelativeLayout(getContext());
        this.mWebView = new WebView(getContext());
        this.mWebView.setVerticalScrollBarEnabled(false);
        this.mWebView.setHorizontalScrollBarEnabled(false);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.setWebViewClient(new WeiboWebViewClient(this, null));
        this.mWebView.loadUrl(this.mUrl);
        this.mWebView.setLayoutParams(FILL);
        this.mWebView.setVisibility(4);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(-1, -1);
        RelativeLayout.LayoutParams lp0 = new RelativeLayout.LayoutParams(-1, -1);
        this.mContent.setBackgroundColor(0);
        InputStream is = null;
        try {
            is = getContext().getAssets().open("weibosdk_dialog_bg.9.png");
            float density = getContext().getResources().getDisplayMetrics().density;
            lp0.leftMargin = (int) (10.0f * density);
            lp0.topMargin = (int) (10.0f * density);
            lp0.rightMargin = (int) (10.0f * density);
            lp0.bottomMargin = (int) (10.0f * density);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (is == null) {
            try {
                this.webViewContainer.setBackgroundResource(R.drawable.weibosdk_dialog_bg);
            } catch (Exception e2) {
                e2.printStackTrace();
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
            } catch (Throwable th) {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
                throw th;
            }
        } else {
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            this.webViewContainer.setBackgroundDrawable(new NinePatchDrawable(bitmap, bitmap.getNinePatchChunk(), new Rect(0, 0, 0, 0), null));
        }
        if (is != null) {
            try {
                is.close();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
        }
        this.webViewContainer.addView(this.mWebView, lp0);
        this.webViewContainer.setGravity(17);
        if (parseDimens()) {
            lp.leftMargin = left_margin;
            lp.topMargin = top_margin;
            lp.rightMargin = right_margin;
            lp.bottomMargin = bottom_margin;
        } else {
            Resources resources = getContext().getResources();
            lp.leftMargin = resources.getDimensionPixelSize(R.dimen.weibosdk_dialog_left_margin);
            lp.rightMargin = resources.getDimensionPixelSize(R.dimen.weibosdk_dialog_right_margin);
            lp.topMargin = resources.getDimensionPixelSize(R.dimen.weibosdk_dialog_top_margin);
            lp.bottomMargin = resources.getDimensionPixelSize(R.dimen.weibosdk_dialog_bottom_margin);
        }
        this.mContent.addView(this.webViewContainer, lp);
    }

    private class WeiboWebViewClient extends WebViewClient {
        private WeiboWebViewClient() {
        }

        /* synthetic */ WeiboWebViewClient(WeiboDialog weiboDialog, WeiboWebViewClient weiboWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.d(WeiboDialog.TAG, "Redirect URL: " + url);
            if (!url.startsWith("sms:")) {
                return super.shouldOverrideUrlLoading(view, url);
            }
            Intent sendIntent = new Intent("android.intent.action.VIEW");
            sendIntent.putExtra("address", url.replace("sms:", ""));
            sendIntent.setType("vnd.android-dir/mms-sms");
            WeiboDialog.this.getContext().startActivity(sendIntent);
            return true;
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            WeiboDialog.this.mListener.onError(new WeiboDialogError(description, errorCode, failingUrl));
            WeiboDialog.this.dismiss();
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.d(WeiboDialog.TAG, "onPageStarted URL: " + url);
            if (url.startsWith(Weibo.redirecturl)) {
                WeiboDialog.this.handleRedirectUrl(view, url);
                view.stopLoading();
                WeiboDialog.this.dismiss();
                return;
            }
            super.onPageStarted(view, url, favicon);
            WeiboDialog.this.mSpinner.show();
        }

        public void onPageFinished(WebView view, String url) {
            Log.d(WeiboDialog.TAG, "onPageFinished URL: " + url);
            super.onPageFinished(view, url);
            if (WeiboDialog.this.mSpinner.isShowing()) {
                WeiboDialog.this.mSpinner.dismiss();
            }
            WeiboDialog.this.mWebView.setVisibility(0);
        }

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed();
        }
    }

    /* access modifiers changed from: private */
    public void handleRedirectUrl(WebView view, String url) {
        Bundle values = Utility.parseUrl(url);
        String error = values.getString("error");
        String error_code = values.getString("error_code");
        if (error == null && error_code == null) {
            this.mListener.onComplete(values);
        } else if (error.equals("access_denied")) {
            this.mListener.onCancel();
        } else if (error_code == null) {
            this.mListener.onWeiboException(new WeiboException(error, 0));
        } else {
            this.mListener.onWeiboException(new WeiboException(error, Integer.parseInt(error_code)));
        }
    }

    private boolean parseDimens() {
        boolean ret = false;
        AssetManager asseets = getContext().getAssets();
        float density = getContext().getResources().getDisplayMetrics().density;
        InputStream is = null;
        try {
            is = asseets.open("values/dimens.xml");
            XmlPullParser xmlpull = Xml.newPullParser();
            try {
                xmlpull.setInput(is, "utf-8");
                ret = true;
                for (int eventCode = xmlpull.getEventType(); eventCode != 1; eventCode = xmlpull.next()) {
                    switch (eventCode) {
                        case 2:
                            if (xmlpull.getName().equals("dimen")) {
                                String name = xmlpull.getAttributeValue(null, Constants.SINA_NAME);
                                if (!"weibosdk_dialog_left_margin".equals(name)) {
                                    if (!"weibosdk_dialog_top_margin".equals(name)) {
                                        if (!"weibosdk_dialog_right_margin".equals(name)) {
                                            if ("weibosdk_dialog_bottom_margin".equals(name)) {
                                                bottom_margin = (int) (((float) Integer.parseInt(xmlpull.nextText())) * density);
                                                break;
                                            }
                                        } else {
                                            right_margin = (int) (((float) Integer.parseInt(xmlpull.nextText())) * density);
                                            break;
                                        }
                                    } else {
                                        top_margin = (int) (((float) Integer.parseInt(xmlpull.nextText())) * density);
                                        break;
                                    }
                                } else {
                                    left_margin = (int) (((float) Integer.parseInt(xmlpull.nextText())) * density);
                                    break;
                                }
                            }
                            break;
                    }
                }
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        } catch (IOException e3) {
            e3.printStackTrace();
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
            }
            throw th;
        }
        return ret;
    }
}
