package com.weibo.sdk.android;

public class WeiboException extends Exception {
    private static final long serialVersionUID = 475022994858770424L;
    private int statusCode = -1;

    public WeiboException(String msg) {
        super(msg);
    }

    public WeiboException(Exception cause) {
        super(cause);
    }

    public WeiboException(String msg, int statusCode2) {
        super(msg);
        this.statusCode = statusCode2;
    }

    public WeiboException(String msg, Exception cause) {
        super(msg, cause);
    }

    public WeiboException(String msg, Exception cause, int statusCode2) {
        super(msg, cause);
        this.statusCode = statusCode2;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public WeiboException() {
    }

    public WeiboException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public WeiboException(Throwable throwable) {
        super(throwable);
    }

    public WeiboException(int statusCode2) {
        this.statusCode = statusCode2;
    }

    public void setStatusCode(int statusCode2) {
        this.statusCode = statusCode2;
    }
}
