package com.weibo.sdk.android.sso;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.sina.sso.RemoteSSO;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.Weibo;
import com.weibo.sdk.android.WeiboAuthListener;
import com.weibo.sdk.android.WeiboDialogError;
import com.weibo.sdk.android.util.Utility;

public class SsoHandler {
    private static final int DEFAULT_AUTH_ACTIVITY_CODE = 32973;
    private static final String WEIBO_SIGNATURE = "30820295308201fea00302010202044b4ef1bf300d06092a864886f70d010105050030818d310b300906035504061302434e3110300e060355040813074265694a696e673110300e060355040713074265694a696e67312c302a060355040a132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c7464312c302a060355040b132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c74643020170d3130303131343130323831355a180f32303630303130323130323831355a30818d310b300906035504061302434e3110300e060355040813074265694a696e673110300e060355040713074265694a696e67312c302a060355040a132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c7464312c302a060355040b132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c746430819f300d06092a864886f70d010101050003818d00308189028181009d367115bc206c86c237bb56c8e9033111889b5691f051b28d1aa8e42b66b7413657635b44786ea7e85d451a12a82a331fced99c48717922170b7fc9bc1040753c0d38b4cf2b22094b1df7c55705b0989441e75913a1a8bd2bc591aa729a1013c277c01c98cbec7da5ad7778b2fad62b85ac29ca28ced588638c98d6b7df5a130203010001300d06092a864886f70d0101050500038181000ad4b4c4dec800bd8fd2991adfd70676fce8ba9692ae50475f60ec468d1b758a665e961a3aedbece9fd4d7ce9295cd83f5f19dc441a065689d9820faedbb7c4a4c4635f5ba1293f6da4b72ed32fb8795f736a20c95cda776402099054fccefb4a1a558664ab8d637288feceba9508aa907fc1fe2b1ae5a0dec954ed831c0bea4";
    /* access modifiers changed from: private */
    public static String ssoActivityName = "";
    /* access modifiers changed from: private */
    public static String ssoPackageName = "";
    private ServiceConnection conn = null;
    private Oauth2AccessToken mAccessToken = null;
    /* access modifiers changed from: private */
    public Activity mAuthActivity;
    /* access modifiers changed from: private */
    public int mAuthActivityCode;
    /* access modifiers changed from: private */
    public WeiboAuthListener mAuthDialogListener;
    /* access modifiers changed from: private */
    public Weibo mWeibo;

    public SsoHandler(Activity activity, Weibo weibo) {
        this.mAuthActivity = activity;
        this.mWeibo = weibo;
        Weibo.isWifi = Utility.isWifi(activity);
        this.conn = new ServiceConnection() {
            public void onServiceDisconnected(ComponentName name) {
                SsoHandler.this.mWeibo.startAuthDialog(SsoHandler.this.mAuthActivity, SsoHandler.this.mAuthDialogListener);
            }

            public void onServiceConnected(ComponentName name, IBinder service) {
                RemoteSSO remoteSSOservice = RemoteSSO.Stub.asInterface(service);
                try {
                    SsoHandler.ssoPackageName = remoteSSOservice.getPackageName();
                    SsoHandler.ssoActivityName = remoteSSOservice.getActivityName();
                    if (!SsoHandler.this.startSingleSignOn(SsoHandler.this.mAuthActivity, Weibo.app_key, new String[0], SsoHandler.this.mAuthActivityCode)) {
                        SsoHandler.this.mWeibo.startAuthDialog(SsoHandler.this.mAuthActivity, SsoHandler.this.mAuthDialogListener);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public void authorize(WeiboAuthListener listener) {
        authorize(DEFAULT_AUTH_ACTIVITY_CODE, listener);
    }

    private void authorize(int activityCode, WeiboAuthListener listener) {
        this.mAuthActivityCode = activityCode;
        this.mAuthDialogListener = listener;
        if (!bindRemoteSSOService(this.mAuthActivity) && this.mWeibo != null) {
            this.mWeibo.startAuthDialog(this.mAuthActivity, this.mAuthDialogListener);
        }
    }

    private boolean bindRemoteSSOService(Activity activity) {
        return activity.getApplicationContext().bindService(new Intent("com.sina.weibo.remotessoservice"), this.conn, 1);
    }

    /* access modifiers changed from: private */
    public boolean startSingleSignOn(Activity activity, String applicationId, String[] permissions, int activityCode) {
        boolean didSucceed = true;
        Intent intent = new Intent();
        intent.setClassName(ssoPackageName, ssoActivityName);
        intent.putExtra("appKey", applicationId);
        intent.putExtra("redirectUri", Weibo.redirecturl);
        if (permissions.length > 0) {
            intent.putExtra("scope", TextUtils.join(",", permissions));
        }
        if (!validateAppSignatureForIntent(activity, intent)) {
            return false;
        }
        try {
            activity.startActivityForResult(intent, activityCode);
        } catch (ActivityNotFoundException e) {
            didSucceed = false;
        }
        activity.getApplication().unbindService(this.conn);
        return didSucceed;
    }

    private boolean validateAppSignatureForIntent(Activity activity, Intent intent) {
        ResolveInfo resolveInfo = activity.getPackageManager().resolveActivity(intent, 0);
        if (resolveInfo == null) {
            return false;
        }
        try {
            for (Signature signature : activity.getPackageManager().getPackageInfo(resolveInfo.activityInfo.packageName, 64).signatures) {
                if (WEIBO_SIGNATURE.equals(signature.toCharsString())) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void authorizeCallBack(int requestCode, int resultCode, Intent data) {
        if (requestCode != this.mAuthActivityCode) {
            return;
        }
        if (resultCode == -1) {
            String error = data.getStringExtra("error");
            if (error == null) {
                error = data.getStringExtra("error_type");
            }
            if (error == null) {
                if (this.mAccessToken == null) {
                    this.mAccessToken = new Oauth2AccessToken();
                }
                this.mAccessToken.setToken(data.getStringExtra("access_token"));
                this.mAccessToken.setExpiresIn(data.getStringExtra("expires_in"));
                this.mAccessToken.setRefreshToken(data.getStringExtra(Weibo.KEY_REFRESHTOKEN));
                if (this.mAccessToken.isSessionValid()) {
                    Log.d("Weibo-authorize", "Login Success! access_token=" + this.mAccessToken.getToken() + " expires=" + this.mAccessToken.getExpiresTime() + "refresh_token=" + this.mAccessToken.getRefreshToken());
                    this.mAuthDialogListener.onComplete(data.getExtras());
                    return;
                }
                Log.d("Weibo-authorize", "Failed to receive access token by SSO");
                this.mWeibo.startAuthDialog(this.mAuthActivity, this.mAuthDialogListener);
            } else if (error.equals("access_denied") || error.equals("OAuthAccessDeniedException")) {
                Log.d("Weibo-authorize", "Login canceled by user.");
                this.mAuthDialogListener.onCancel();
            } else {
                String description = data.getStringExtra("error_description");
                if (description != null) {
                    error = String.valueOf(error) + ":" + description;
                }
                Log.d("Weibo-authorize", "Login failed: " + error);
                this.mAuthDialogListener.onError(new WeiboDialogError(error, resultCode, description));
            }
        } else if (resultCode != 0) {
        } else {
            if (data != null) {
                Log.d("Weibo-authorize", "Login failed: " + data.getStringExtra("error"));
                this.mAuthDialogListener.onError(new WeiboDialogError(data.getStringExtra("error"), data.getIntExtra("error_code", -1), data.getStringExtra("failing_url")));
                return;
            }
            Log.d("Weibo-authorize", "Login canceled by user.");
            this.mAuthDialogListener.onCancel();
        }
    }
}
