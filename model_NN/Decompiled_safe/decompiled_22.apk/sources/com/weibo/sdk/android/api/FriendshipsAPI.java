package com.weibo.sdk.android.api;

import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.net.RequestListener;
import sina_weibo.Constants;

public class FriendshipsAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/friendships";

    public FriendshipsAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void friends(long uid, int count, int cursor, boolean trim_status, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("cursor", cursor);
        if (trim_status) {
            params.add("trim_status", 1);
        } else {
            params.add("trim_status", 0);
        }
        request("https://api.weibo.com/2/friendships/friends.json", params, "GET", listener);
    }

    public void friends(String screen_name, int count, int cursor, boolean trim_status, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("screen_name", screen_name);
        params.add("count", count);
        params.add("cursor", cursor);
        if (trim_status) {
            params.add("trim_status", 0);
        } else {
            params.add("trim_status", 1);
        }
        request("https://api.weibo.com/2/friendships/friends.json", params, "GET", listener);
    }

    public void inCommon(long uid, long suid, int count, int page, boolean trim_status, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("suid", suid);
        params.add("count", count);
        params.add("page", page);
        if (trim_status) {
            params.add("trim_status", 1);
        } else {
            params.add("trim_status", 0);
        }
        request("https://api.weibo.com/2/friendships/friends/in_common.json", params, "GET", listener);
    }

    public void bilateral(long uid, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/friendships/friends/bilateral.json", params, "GET", listener);
    }

    public void bilateralIds(long uid, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/friendships/friends/bilateral/ids.json", params, "GET", listener);
    }

    public void friendsIds(long uid, int count, int cursor, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("cursor", cursor);
        request("https://api.weibo.com/2/friendships/friends/ids.json", params, "GET", listener);
    }

    public void friendsIds(String screen_name, int count, int cursor, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("screen_name", screen_name);
        params.add("count", count);
        params.add("cursor", cursor);
        request("https://api.weibo.com/2/friendships/friends/ids.json", params, "GET", listener);
    }

    public void followers(long uid, int count, int cursor, boolean trim_status, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("cursor", cursor);
        if (trim_status) {
            params.add("trim_status", 0);
        } else {
            params.add("trim_status", 1);
        }
        request("https://api.weibo.com/2/friendships/followers.json", params, "GET", listener);
    }

    public void followers(String screen_name, int count, int cursor, boolean trim_status, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("screen_name", screen_name);
        params.add("count", count);
        params.add("cursor", cursor);
        if (trim_status) {
            params.add("trim_status", 0);
        } else {
            params.add("trim_status", 1);
        }
        request("https://api.weibo.com/2/friendships/followers.json", params, "GET", listener);
    }

    public void followersIds(long uid, int count, int cursor, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("cursor", cursor);
        request("https://api.weibo.com/2/friendships/followers/ids.json", params, "GET", listener);
    }

    public void followersIds(String screen_name, int count, int cursor, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("screen_name", screen_name);
        params.add("count", count);
        params.add("cursor", cursor);
        request("https://api.weibo.com/2/friendships/followers/ids.json", params, "GET", listener);
    }

    public void followersActive(long uid, int count, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        request("https://api.weibo.com/2/friendships/followers/active.json", params, "GET", listener);
    }

    public void chainFollowers(long uid, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/friendships/friends_chain/followers.json", params, "GET", listener);
    }

    public void show(long source_id, long target_id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("source_id", source_id);
        params.add("target_id", target_id);
        request("https://api.weibo.com/2/friendships/show.json", params, "GET", listener);
    }

    public void show(long source_id, String target_screen_name, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("source_id", source_id);
        params.add("target_screen_name", target_screen_name);
        request("https://api.weibo.com/2/friendships/show.json", params, "GET", listener);
    }

    public void show(String source_screen_name, long target_id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("source_screen_name", source_screen_name);
        params.add("target_id", target_id);
        request("https://api.weibo.com/2/friendships/show.json", params, "GET", listener);
    }

    public void show(String source_screen_name, String target_screen_name, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("target_screen_name", target_screen_name);
        params.add("source_screen_name", source_screen_name);
        request("https://api.weibo.com/2/friendships/show.json", params, "GET", listener);
    }

    public void create(long uid, String screen_name, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("screen_name", screen_name);
        request("https://api.weibo.com/2/friendships/create.json", params, "POST", listener);
    }

    @Deprecated
    public void create(String screen_name, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("screen_name", screen_name);
        request("https://api.weibo.com/2/friendships/create.json", params, "POST", listener);
    }

    public void destroy(long uid, String screen_name, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("screen_name", screen_name);
        request("https://api.weibo.com/2/friendships/destroy.json", params, "POST", listener);
    }

    @Deprecated
    public void destroy(String screen_name, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("screen_name", screen_name);
        request("https://api.weibo.com/2/friendships/destroy.json", params, "POST", listener);
    }
}
