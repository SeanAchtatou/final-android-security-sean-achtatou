package com.weibo.sdk.android.api;

import android.text.TextUtils;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.RequestListener;
import sina_weibo.Constants;

public class StatusesAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/statuses";

    public StatusesAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void publicTimeline(int count, int page, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/statuses/public_timeline.json", params, "GET", listener);
    }

    public void friendsTimeline(long since_id, long max_id, int count, int page, boolean base_app, WeiboAPI.FEATURE feature, boolean trim_user, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        params.add("feature", feature.ordinal());
        if (trim_user) {
            params.add("trim_user", 1);
        } else {
            params.add("trim_user", 0);
        }
        request("https://api.weibo.com/2/statuses/friends_timeline.json", params, "GET", listener);
    }

    public void friendsTimelineIds(long since_id, long max_id, int count, int page, boolean base_app, WeiboAPI.FEATURE feature, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        params.add("feature", feature.ordinal());
        request("https://api.weibo.com/2/statuses/friends_timeline/ids.json", params, "GET", listener);
    }

    public void homeTimeline(long since_id, long max_id, int count, int page, boolean base_app, WeiboAPI.FEATURE feature, boolean trim_user, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        params.add("feature", feature.ordinal());
        if (trim_user) {
            params.add("trim_user", 1);
        } else {
            params.add("trim_user", 0);
        }
        request("https://api.weibo.com/2/statuses/home_timeline.json", params, "GET", listener);
    }

    public void userTimeline(long uid, long since_id, long max_id, int count, int page, boolean base_app, WeiboAPI.FEATURE feature, boolean trim_user, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        params.add("feature", feature.ordinal());
        if (trim_user) {
            params.add("trim_user", 1);
        } else {
            params.add("trim_user", 0);
        }
        request("https://api.weibo.com/2/statuses/user_timeline.json", params, "GET", listener);
    }

    public void userTimeline(String screen_name, long since_id, long max_id, int count, int page, boolean base_app, WeiboAPI.FEATURE feature, boolean trim_user, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("screen_name", screen_name);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        params.add("feature", feature.ordinal());
        if (trim_user) {
            params.add("trim_user", 1);
        } else {
            params.add("trim_user", 0);
        }
        request("https://api.weibo.com/2/statuses/user_timeline.json", params, "GET", listener);
    }

    public void userTimeline(long since_id, long max_id, int count, int page, boolean base_app, WeiboAPI.FEATURE feature, boolean trim_user, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        params.add("feature", feature.ordinal());
        if (trim_user) {
            params.add("trim_user", 1);
        } else {
            params.add("trim_user", 0);
        }
        request("https://api.weibo.com/2/statuses/user_timeline.json", params, "GET", listener);
    }

    public void userTimelineIds(long uid, long since_id, long max_id, int count, int page, boolean base_app, WeiboAPI.FEATURE feature, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        params.add("feature", feature.ordinal());
        request("https://api.weibo.com/2/statuses/user_timeline/ids.json", params, "GET", listener);
    }

    public void userTimelineIds(String screen_name, long since_id, long max_id, int count, int page, boolean base_app, WeiboAPI.FEATURE feature, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("screen_name", screen_name);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        params.add("feature", feature.ordinal());
        request("https://api.weibo.com/2/statuses/user_timeline/ids.json", params, "GET", listener);
    }

    public void repostTimeline(long id, long since_id, long max_id, int count, int page, WeiboAPI.AUTHOR_FILTER filter_by_author, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        params.add("filter_by_author", filter_by_author.ordinal());
        request("https://api.weibo.com/2/statuses/repost_timeline.json", params, "GET", listener);
    }

    public void repostTimelineIds(long id, long since_id, long max_id, int count, int page, WeiboAPI.AUTHOR_FILTER filter_by_author, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        params.add("filter_by_author", filter_by_author.ordinal());
        request("https://api.weibo.com/2/statuses/repost_timeline/ids.json", params, "GET", listener);
    }

    public void repostByMe(long since_id, long max_id, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/statuses/repost_by_me.json", params, "GET", listener);
    }

    public void mentions(long since_id, long max_id, int count, int page, WeiboAPI.AUTHOR_FILTER filter_by_author, WeiboAPI.SRC_FILTER filter_by_source, WeiboAPI.TYPE_FILTER filter_by_type, boolean trim_user, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        params.add("filter_by_author", filter_by_author.ordinal());
        params.add("filter_by_source", filter_by_source.ordinal());
        params.add("filter_by_type", filter_by_type.ordinal());
        if (trim_user) {
            params.add("trim_user", 1);
        } else {
            params.add("trim_user", 0);
        }
        request("https://api.weibo.com/2/statuses/mentions.json", params, "GET", listener);
    }

    public void mentionsIds(long since_id, long max_id, int count, int page, WeiboAPI.AUTHOR_FILTER filter_by_author, WeiboAPI.SRC_FILTER filter_by_source, WeiboAPI.TYPE_FILTER filter_by_type, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        params.add("filter_by_author", filter_by_author.ordinal());
        params.add("filter_by_source", filter_by_source.ordinal());
        params.add("filter_by_type", filter_by_type.ordinal());
        request("https://api.weibo.com/2/statuses/mentions/ids.json", params, "GET", listener);
    }

    public void bilateralTimeline(long since_id, long max_id, int count, int page, boolean base_app, WeiboAPI.FEATURE feature, boolean trim_user, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        params.add("feature", feature.ordinal());
        if (trim_user) {
            params.add("trim_user", 1);
        } else {
            params.add("trim_user", 0);
        }
        request("https://api.weibo.com/2/statuses/bilateral_timeline.json", params, "GET", listener);
    }

    public void show(long id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        request("https://api.weibo.com/2/statuses/show.json", params, "GET", listener);
    }

    public void queryMID(long[] ids, WeiboAPI.TYPE type, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (1 == ids.length) {
            params.add(LocaleUtil.INDONESIAN, ids[0]);
        } else {
            params.add("is_batch", 1);
            StringBuilder strb = new StringBuilder();
            for (long id : ids) {
                strb.append(id).append(",");
            }
            strb.deleteCharAt(strb.length() - 1);
            params.add(LocaleUtil.INDONESIAN, strb.toString());
        }
        params.add("type", type.ordinal());
        request("https://api.weibo.com/2/statuses/querymid.json", params, "GET", listener);
    }

    public void queryID(String[] mids, WeiboAPI.TYPE type, boolean inbox, boolean isBase62, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        if (mids != null) {
            if (1 == mids.length) {
                params.add("mid", mids[0]);
            } else {
                params.add("is_batch", 1);
                StringBuilder strb = new StringBuilder();
                for (String mid : mids) {
                    strb.append(mid).append(",");
                }
                strb.deleteCharAt(strb.length() - 1);
                params.add("mid", strb.toString());
            }
        }
        params.add("type", type.ordinal());
        if (inbox) {
            params.add("inbox", 0);
        } else {
            params.add("inbox", 1);
        }
        if (isBase62) {
            params.add("isBase62", 0);
        } else {
            params.add("isBase62", 1);
        }
        request("https://api.weibo.com/2/statuses/queryid.json", params, "GET", listener);
    }

    public void hotRepostDaily(int count, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/statuses/hot/repost_daily.json", params, "GET", listener);
    }

    public void hotRepostWeekly(int count, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/statuses/hot/repost_weekly.json", params, "GET", listener);
    }

    public void hotCommentsDaily(int count, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/statuses/hot/comments_daily.json", params, "GET", listener);
    }

    public void hotCommentsWeekly(int count, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        if (base_app) {
            params.add("base_app", 0);
        } else {
            params.add("base_app", 1);
        }
        request("https://api.weibo.com/2/statuses/hot/comments_weekly.json", params, "GET", listener);
    }

    public void count(String[] ids, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        StringBuilder strb = new StringBuilder();
        for (String id : ids) {
            strb.append(id).append(",");
        }
        strb.deleteCharAt(strb.length() - 1);
        params.add("ids", strb.toString());
        request("https://api.weibo.com/2/statuses/count.json", params, "GET", listener);
    }

    public void repost(long id, String status, WeiboAPI.COMMENTS_TYPE is_comment, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        params.add("status", status);
        params.add("is_comment", is_comment.ordinal());
        request("https://api.weibo.com/2/statuses/repost.json", params, "POST", listener);
    }

    public void destroy(long id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        request("https://api.weibo.com/2/statuses/destroy.json", params, "POST", listener);
    }

    public void update(String content, String lat, String lon, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("status", content);
        if (!TextUtils.isEmpty(lon)) {
            params.add("long", lon);
        }
        if (!TextUtils.isEmpty(lat)) {
            params.add("lat", lat);
        }
        request("https://api.weibo.com/2/statuses/update.json", params, "POST", listener);
    }

    public void upload(String content, String file, String lat, String lon, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("status", content);
        params.add("pic", file);
        if (!TextUtils.isEmpty(lon)) {
            params.add("long", lon);
        }
        if (!TextUtils.isEmpty(lat)) {
            params.add("lat", lat);
        }
        request("https://api.weibo.com/2/statuses/upload.json", params, "POST", listener);
    }

    public void uploadUrlText(String status, String imageUrl, String lat, String lon, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("status", status);
        params.add(com.tencent.tauth.Constants.PARAM_URL, imageUrl);
        if (!TextUtils.isEmpty(lon)) {
            params.add("long", lon);
        }
        if (!TextUtils.isEmpty(lat)) {
            params.add("lat", lat);
        }
        request("https://api.weibo.com/2/statuses/upload_url_text.json", params, "POST", listener);
    }

    public void emotions(WeiboAPI.EMOTION_TYPE type, WeiboAPI.LANGUAGE language, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("type", type.name());
        params.add("language", language.name());
        request("https://api.weibo.com/2/emotions.json", params, "GET", listener);
    }
}
