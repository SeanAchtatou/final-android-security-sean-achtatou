package com.weibo.sdk.android.api;

import com.baidu.android.pushservice.PushConstants;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.net.RequestListener;
import sina_weibo.Constants;

public class TagsAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/tags";

    public TagsAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void tags(long uid, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/tags.json", params, "GET", listener);
    }

    public void tagsBatch(String[] uids, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        StringBuilder strb = new StringBuilder();
        for (String uid : uids) {
            strb.append(uid).append(",");
        }
        strb.deleteCharAt(strb.length() - 1);
        params.add("uids", strb.toString());
        request("https://api.weibo.com/2/tags/tags_batch.json", params, "GET", listener);
    }

    public void suggestions(int count, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("count", count);
        request("https://api.weibo.com/2/tags/suggestions.json", params, "GET", listener);
    }

    public void create(String[] tags, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        StringBuilder strb = new StringBuilder();
        for (String tag : tags) {
            strb.append(tag).append(",");
        }
        strb.deleteCharAt(strb.length() - 1);
        params.add(PushConstants.EXTRA_TAGS, strb.toString());
        request("https://api.weibo.com/2/tags/create.json", params, "POST", listener);
    }

    public void destroy(long tag_id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("tag_id", tag_id);
        request("https://api.weibo.com/2/tags/destroy.json", params, "POST", listener);
    }

    public void destroyBatch(String[] ids, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        StringBuilder strb = new StringBuilder();
        for (String id : ids) {
            strb.append(id).append(",");
        }
        strb.deleteCharAt(strb.length() - 1);
        params.add("ids", strb.toString());
        request("https://api.weibo.com/2/tags/destroy_batch.json", params, "POST", listener);
    }
}
