package com.weibo.sdk.android.api;

import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.weibo.sdk.android.Oauth2AccessToken;
import com.weibo.sdk.android.WeiboParameters;
import com.weibo.sdk.android.api.WeiboAPI;
import com.weibo.sdk.android.net.RequestListener;
import sina_weibo.Constants;

public class PlaceAPI extends WeiboAPI {
    private static final String SERVER_URL_PRIX = "https://api.weibo.com/2/place";

    public PlaceAPI(Oauth2AccessToken accessToken) {
        super(accessToken);
    }

    public void friendsTimeline(long since_id, long max_id, int count, int page, boolean only_attentions, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (only_attentions) {
            params.add("type", 0);
        } else {
            params.add("type", 1);
        }
        request("https://api.weibo.com/2/place/friends_timeline.json", params, "GET", listener);
    }

    public void userTimeline(long uid, long since_id, long max_id, int count, int page, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/place/user_timeline.json", params, "GET", listener);
    }

    public void poiTimeline(String poiid, long since_id, long max_id, int count, int page, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("poiid", poiid);
        params.add("since_id", since_id);
        params.add("max_id", max_id);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 0);
        } else {
            params.add("base_app", 1);
        }
        request("https://api.weibo.com/2/place/poi_timeline.json", params, "GET", listener);
    }

    public void nearbyTimeline(String lat, String lon, int range, long starttime, long endtime, WeiboAPI.SORT3 sort, int count, int page, boolean base_app, boolean offset, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("lat", lat);
        params.add("long", lon);
        params.add("range", range);
        params.add("starttime", starttime);
        params.add("endtime", endtime);
        params.add("sort", sort.ordinal());
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        if (offset) {
            params.add("offset", 1);
        } else {
            params.add("offset", 0);
        }
        request("https://api.weibo.com/2/place/nearby_timeline.json", params, "GET", listener);
    }

    public void statusesShow(long id, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(LocaleUtil.INDONESIAN, id);
        request("https://api.weibo.com/2/place/statuses/show.json", params, "GET", listener);
    }

    public void usersShow(long uid, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/place/users/show.json", params, "GET", listener);
    }

    public void usersCheckins(long uid, int count, int page, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/place/users/checkins.json", params, "GET", listener);
    }

    public void usersPhotos(long uid, int count, int page, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/place/users/photos.json", params, "GET", listener);
    }

    public void usersTips(long uid, int count, int page, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, uid);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/place/users/tips.json", params, "GET", listener);
    }

    public void poisShow(String poiid, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("poiid", poiid);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/place/pois/show.json", params, "GET", listener);
    }

    public void poisUsers(String poiid, int count, int page, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("poiid", poiid);
        params.add("count", count);
        params.add("page", page);
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/place/pois/users.json", params, "GET", listener);
    }

    public void poisPhotos(String poiid, int count, int page, WeiboAPI.SORT2 sort, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add(Constants.SINA_UID, poiid);
        params.add("count", count);
        params.add("page", page);
        params.add("sort", sort.ordinal());
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/place/pois/photos.json", params, "GET", listener);
    }

    public void poisTips(String poiid, int count, int page, WeiboAPI.SORT2 sort, boolean base_app, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("poiid", poiid);
        params.add("count", count);
        params.add("page", page);
        params.add("sort", sort.ordinal());
        if (base_app) {
            params.add("base_app", 1);
        } else {
            params.add("base_app", 0);
        }
        request("https://api.weibo.com/2/place/pois/tips.json", params, "GET", listener);
    }

    public void poisSearch(String keyword, String city, String category, int count, int page, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("keyword", keyword);
        params.add("city", city);
        params.add("category", category);
        params.add("count", count);
        params.add("page", page);
        request("https://api.weibo.com/2/place/pois/search.json", params, "GET", listener);
    }

    public void poisCategory(int pid, boolean returnALL, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("pid", pid);
        if (returnALL) {
            params.add("flag", 1);
        } else {
            params.add("flag", 0);
        }
        request("https://api.weibo.com/2/place/pois/category.json", params, "GET", listener);
    }

    public void nearbyPois(String lat, String lon, int range, String q, String category, int count, int page, boolean offset, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("lat", lat);
        params.add("long", lon);
        params.add("range", range);
        params.add("q", q);
        params.add("category", category);
        params.add("count", count);
        params.add("page", page);
        if (offset) {
            params.add("offset", 1);
        } else {
            params.add("offset", 0);
        }
        request("https://api.weibo.com/2/place/nearby/pois.json", params, "GET", listener);
    }

    public void nearbyUsers(String lat, String lon, int range, long starttime, long endtime, WeiboAPI.SORT3 sort, int count, int page, boolean offset, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("lat", lat);
        params.add("long", lon);
        params.add("range", range);
        params.add("starttime", starttime);
        params.add("endtime", endtime);
        params.add("sort", sort.ordinal());
        params.add("count", count);
        params.add("page", page);
        if (offset) {
            params.add("offset", 1);
        } else {
            params.add("offset", 0);
        }
        request("https://api.weibo.com/2/place/nearby/users.json", params, "GET", listener);
    }

    public void nearbyPhotos(String lat, String lon, int range, long starttime, long endtime, WeiboAPI.SORT3 sort, int count, int page, boolean offset, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("lat", lat);
        params.add("long", lon);
        params.add("range", range);
        params.add("starttime", starttime);
        params.add("endtime", endtime);
        params.add("sort", sort.ordinal());
        params.add("count", count);
        params.add("page", page);
        if (offset) {
            params.add("offset", 1);
        } else {
            params.add("offset", 0);
        }
        request("https://api.weibo.com/2/place/nearby/photos.json", params, "GET", listener);
    }

    public void poisAddCheckin(String poiid, String status, String pic, boolean isPublic, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("poiid", poiid);
        params.add("status", status);
        params.add("pic", pic);
        if (isPublic) {
            params.add("public", 1);
        } else {
            params.add("public", 0);
        }
        request("https://api.weibo.com/2/place/pois/add_checkin.json", params, "POST", listener);
    }

    public void poisAddPhoto(String poiid, String status, String pic, boolean isPublic, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("poiid", poiid);
        params.add("status", status);
        params.add("pic", pic);
        if (isPublic) {
            params.add("public", 1);
        } else {
            params.add("public", 0);
        }
        request("https://api.weibo.com/2/place/pois/add_photo.json", params, "POST", listener);
    }

    public void poisAddTip(String poiid, String status, boolean isPublic, RequestListener listener) {
        WeiboParameters params = new WeiboParameters();
        params.add("poiid", poiid);
        params.add("status", status);
        if (isPublic) {
            params.add("public", 1);
        } else {
            params.add("public", 0);
        }
        request("https://api.weibo.com/2/place/pois/add_tip.json", params, "POST", listener);
    }
}
