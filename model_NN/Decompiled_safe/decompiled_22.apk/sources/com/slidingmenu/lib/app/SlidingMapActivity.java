package com.slidingmenu.lib.app;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.maps.MapActivity;
import com.slidingmenu.lib.SlidingMenu;

public abstract class SlidingMapActivity extends MapActivity implements SlidingActivityBase {
    private SlidingActivityHelper mHelper;

    /* JADX WARN: Type inference failed for: r1v0, types: [com.slidingmenu.lib.app.SlidingMapActivity, com.google.android.maps.MapActivity, android.app.Activity] */
    public void onCreate(Bundle savedInstanceState) {
        SlidingMapActivity.super.onCreate(savedInstanceState);
        this.mHelper = new SlidingActivityHelper(this);
        this.mHelper.onCreate(savedInstanceState);
    }

    public void onPostCreate(Bundle savedInstanceState) {
        SlidingMapActivity.super.onPostCreate(savedInstanceState);
        this.mHelper.onPostCreate(savedInstanceState);
    }

    public View findViewById(int id) {
        View v = SlidingMapActivity.super.findViewById(id);
        return v != null ? v : this.mHelper.findViewById(id);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        SlidingMapActivity.super.onSaveInstanceState(outState);
        this.mHelper.onSaveInstanceState(outState);
    }

    public void setContentView(int id) {
        setContentView(getLayoutInflater().inflate(id, (ViewGroup) null));
    }

    public void setContentView(View v) {
        setContentView(v, new ViewGroup.LayoutParams(-1, -1));
    }

    public void setContentView(View v, ViewGroup.LayoutParams params) {
        SlidingMapActivity.super.setContentView(v, params);
        this.mHelper.registerAboveContentView(v, params);
    }

    public void setBehindContentView(int id) {
        setBehindContentView(getLayoutInflater().inflate(id, (ViewGroup) null));
    }

    public void setBehindContentView(View v) {
        setBehindContentView(v, new ViewGroup.LayoutParams(-1, -1));
    }

    public void setBehindContentView(View v, ViewGroup.LayoutParams params) {
        this.mHelper.setBehindContentView(v, params);
    }

    public SlidingMenu getSlidingMenu() {
        return this.mHelper.getSlidingMenu();
    }

    public void toggle() {
        this.mHelper.toggle();
    }

    public void showContent() {
        this.mHelper.showContent();
    }

    public void showMenu() {
        this.mHelper.showMenu();
    }

    public void showSecondaryMenu() {
        this.mHelper.showSecondaryMenu();
    }

    public void setSlidingActionBarEnabled(boolean b) {
        this.mHelper.setSlidingActionBarEnabled(b);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        boolean b = this.mHelper.onKeyUp(keyCode, event);
        return b ? b : SlidingMapActivity.super.onKeyUp(keyCode, event);
    }
}
