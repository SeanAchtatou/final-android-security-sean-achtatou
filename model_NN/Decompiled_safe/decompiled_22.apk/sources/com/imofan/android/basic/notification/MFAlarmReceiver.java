package com.imofan.android.basic.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.Date;

public class MFAlarmReceiver extends BroadcastReceiver {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, int):int
      com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, long):long
      com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    public void onReceive(Context context, Intent intent) {
        int time;
        String listenerName;
        if (PreferencesUtils.getPreference(context, "push", "status", false) && (time = new Date(System.currentTimeMillis()).getHours()) <= 23 && time >= 7) {
            if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction()) && (listenerName = PreferencesUtils.getPreference(context, "mfpush", "pushListener", "")) != null && !"".equals(listenerName)) {
                try {
                    MFPNService.startPushService(context, Class.forName(listenerName));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
            context.startService(new Intent(context, MFPNService.class));
        }
    }
}
