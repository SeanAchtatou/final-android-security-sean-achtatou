package com.imofan.android.basic.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import com.imofan.android.basic.Mofang;

public class MFPNService extends Service {
    private static final String TAG = MFPNService.class.getSimpleName();
    private static final long intervalAlarmTime = 300000;
    private static PendingIntent pendingIntent = null;
    private static PowerManager.WakeLock wakeLock;
    private final int MESSAGE_PUSH_SUCCEED = 1;
    private long messageLatestTime;
    /* access modifiers changed from: private */
    public Handler messagePushHandler = new Handler() {
        public void handleMessage(Message msg) {
            MFNotificationListener mfNotificationListener;
            switch (msg.what) {
                case 1:
                    MFMessage mfMessage = (MFMessage) msg.obj;
                    if (!(mfMessage == null || (mfNotificationListener = MFPushUtils.getNotificationListener(MFPNService.this.getApplicationContext())) == null)) {
                        mfNotificationListener.onNotificationReceived(MFPNService.this.getApplicationContext(), String.valueOf(mfMessage.getMsgId()), mfMessage.getMsgContent());
                        break;
                    }
            }
            MFPNService.releaseWakeLock();
            MFPNService.this.stopSelf();
        }
    };

    public IBinder onBind(Intent intent) {
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, long):long
     arg types: [com.imofan.android.basic.notification.MFPNService, java.lang.String, java.lang.String, int]
     candidates:
      com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, int):int
      com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
      com.imofan.android.basic.notification.PreferencesUtils.getPreference(android.content.Context, java.lang.String, java.lang.String, long):long */
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            try {
                sendBroadcast(new Intent("com.imofan.android.basic.notification.MFAlarmReceiver"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return super.onStartCommand(intent, flags, startId);
        }
        acquireWakeLock(this);
        this.messageLatestTime = PreferencesUtils.getPreference((Context) this, "message", "latestTime", 0L);
        new Thread(new MessagePushRunnable()).start();
        return super.onStartCommand(intent, flags, startId);
    }

    private class MessagePushRunnable implements Runnable {
        private MessagePushRunnable() {
        }

        public void run() {
            MFMessage mfMessage = MFPushUtils.getMaxTsMsg(MFPNService.this.getApplicationContext(), MFPushUtils.parseJson(MFPNService.this.getPushContent()));
            if (mfMessage != null) {
                Message msg = MFPNService.this.messagePushHandler.obtainMessage();
                msg.obj = mfMessage;
                msg.what = 1;
                MFPNService.this.messagePushHandler.sendMessage(msg);
                return;
            }
            MFPNService.releaseWakeLock();
            MFPNService.this.stopSelf();
        }
    }

    /* access modifiers changed from: private */
    public String getPushContent() {
        MFParameter mfParameter = new MFParameter();
        String appKey = Mofang.getAppKey(getApplicationContext());
        Log.v(TAG, "AppKey: " + appKey);
        String devID = MFPushUtils.getDevId(getApplicationContext());
        Log.v(TAG, "DevId: " + devID);
        if (appKey == null || devID == null) {
            releaseWakeLock();
            return null;
        }
        long appId = Long.valueOf(appKey.substring(appKey.length() - 8), 16).longValue();
        Log.v(TAG, "AppId: " + appId);
        mfParameter.setDevId(devID);
        mfParameter.setAppId(appId);
        Log.v(TAG, "LatestTime：" + this.messageLatestTime);
        mfParameter.setTimeStamp(this.messageLatestTime);
        Log.v(TAG, "MD5: " + MFParameter.getMd5Verify(getApplicationContext(), appId, this.messageLatestTime));
        mfParameter.setMd5(MFParameter.getMd5Verify(getApplicationContext(), appId, this.messageLatestTime));
        return MFPushUtils.pullMsgByPost(mfParameter);
    }

    public static void startPushService(Context context, Class listenerClass) {
        String devId = MFPushUtils.getDevId(context);
        if (listenerClass != null) {
            MFPushUtils.saveListenerCLass(context, listenerClass);
        }
        if (devId != null && !"".equals(devId)) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
            Intent intent = new Intent(context, MFAlarmReceiver.class);
            if (pendingIntent != null) {
                alarmManager.cancel(pendingIntent);
            }
            pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 268435456);
            alarmManager.setRepeating(0, System.currentTimeMillis(), intervalAlarmTime, pendingIntent);
        }
    }

    public static void stopPushService(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        if (pendingIntent != null) {
            alarmManager.cancel(pendingIntent);
            pendingIntent = null;
        }
    }

    public static void setPushStatus(Context context, boolean status) {
        PreferencesUtils.setPreferences(context, "push", "status", status);
    }

    private static void acquireWakeLock(Context context) {
        if (wakeLock == null) {
            wakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(1, TAG);
            wakeLock.acquire();
        }
    }

    /* access modifiers changed from: private */
    public static void releaseWakeLock() {
        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
            wakeLock = null;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        releaseWakeLock();
    }
}
