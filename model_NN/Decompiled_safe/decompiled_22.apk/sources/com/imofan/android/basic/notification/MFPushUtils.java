package com.imofan.android.basic.notification;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import com.baidu.android.pushservice.PushConstants;
import com.imofan.android.basic.MFStatInfo;
import com.palmtrends.loadimage.ImageFetcher;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MFPushUtils {
    private static final String ALL_LANGUAGE = "all";
    private static final String TAG = "MFPushUtils";

    static String pullMsgByPost(MFParameter mfParameter) {
        StringBuilder sb = new StringBuilder("http://apns.imofan.com/pull/");
        sb.append(mfParameter.getDevId());
        sb.append(CookieSpec.PATH_DELIM + String.valueOf(mfParameter.getAppId()));
        sb.append(CookieSpec.PATH_DELIM + String.valueOf(mfParameter.getTimeStamp()));
        sb.append(CookieSpec.PATH_DELIM + String.valueOf(mfParameter.getMd5()));
        return getJsonFromServer(sb.toString());
    }

    private static String getJsonFromServer(String uri) {
        String message;
        HttpPost post = new HttpPost(uri);
        try {
            HttpParams param = new BasicHttpParams();
            SchemeRegistry schReg = new SchemeRegistry();
            schReg.register(new Scheme(ImageFetcher.HTTP_CACHE_DIR, PlainSocketFactory.getSocketFactory(), 80));
            schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            HttpResponse response = new DefaultHttpClient(new ThreadSafeClientConnManager(param, schReg), param).execute(post);
            int responseStatusCode = response.getStatusLine().getStatusCode();
            if (responseStatusCode == 201 || responseStatusCode == 200) {
                message = EntityUtils.toString(response.getEntity(), "utf-8");
            } else {
                message = null;
            }
        } catch (IOException e) {
            message = null;
            e.printStackTrace();
        } catch (Exception e2) {
            message = null;
            e2.printStackTrace();
        } finally {
            post.abort();
        }
        return message;
    }

    static List<MFMessage> parseJson(String msgContent) {
        if (msgContent == null || "".equals(msgContent)) {
            return null;
        }
        try {
            JSONArray jsonArray = new JSONObject(msgContent).optJSONArray("messages");
            if (jsonArray == null || jsonArray.length() <= 0) {
                return null;
            }
            List<MFMessage> msgs = new ArrayList<>();
            try {
                int length = jsonArray.length();
                for (int i = 0; i < length; i++) {
                    MFMessage mfMessage = new MFMessage();
                    mfMessage.setAppId(jsonArray.optJSONObject(i).optString(PushConstants.EXTRA_APP_ID));
                    mfMessage.setAppVersion(jsonArray.optJSONObject(i).optString(MFStatInfo.KEY_APP_VERSION));
                    mfMessage.setMsgContent(jsonArray.optJSONObject(i).optString("content"));
                    mfMessage.setLanguage(jsonArray.optJSONObject(i).optString("lang"));
                    mfMessage.setOsVersion(jsonArray.optJSONObject(i).optString(MFStatInfo.KEY_OS_VERSION));
                    mfMessage.setMsgId(jsonArray.optJSONObject(i).optInt("push_id"));
                    mfMessage.setTimeStamp(jsonArray.optJSONObject(i).optLong("ts"));
                    msgs.add(mfMessage);
                }
                return msgs;
            } catch (JSONException e) {
                e = e;
                e.printStackTrace();
                return null;
            }
        } catch (JSONException e2) {
            e = e2;
            e.printStackTrace();
            return null;
        }
    }

    static MFMessage getMaxTsMsg(Context context, List<MFMessage> msgs) {
        if (msgs != null && msgs.size() > 0) {
            int size = msgs.size();
            int k = 0;
            long max = msgs.get(0).getTimeStamp();
            for (int i = 0; i < size; i++) {
                if (max < msgs.get(i).getTimeStamp()) {
                    k = i;
                    max = msgs.get(i).getTimeStamp();
                }
            }
            MFMessage mfMessage = msgs.get(k);
            PreferencesUtils.setPreferences(context, "message", "latestTime", mfMessage.getTimeStamp());
            boolean appVerCompareResult = compareAppVersion(context, mfMessage);
            boolean osVerCompareResult = compareOSVersion(mfMessage);
            boolean langCompareResult = compareLaunage(context, mfMessage);
            if (appVerCompareResult && osVerCompareResult && langCompareResult) {
                return msgs.get(k);
            }
        }
        return null;
    }

    private static String getOsVersion() {
        String version = Build.VERSION.RELEASE;
        if (version == null || "".equals(version)) {
            return null;
        }
        return version.substring(0, version.lastIndexOf("."));
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 16384);
            if (pinfo != null) {
                return pinfo.versionCode;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static boolean compareAppVersion(Context context, MFMessage mfMessage) {
        if (mfMessage == null) {
            return false;
        }
        return comparator(Float.parseFloat(String.valueOf(getAppVersion(context))), mfMessage.getAppVersion());
    }

    private static boolean compareOSVersion(MFMessage mfMessage) {
        if (mfMessage == null) {
            return false;
        }
        return comparator(Float.valueOf(getOsVersion()).floatValue(), mfMessage.getOsVersion());
    }

    private static boolean compareLaunage(Context context, MFMessage mfMessage) {
        if (mfMessage == null) {
            return false;
        }
        String localLanguage = context.getResources().getConfiguration().locale.getDisplayLanguage().toLowerCase();
        String serverLanguage = mfMessage.getLanguage();
        if (!ALL_LANGUAGE.equals(serverLanguage) && !localLanguage.equals(serverLanguage)) {
            return false;
        }
        return true;
    }

    private static boolean comparator(float value, String msg) {
        if ("".equals(msg)) {
            return true;
        }
        if (msg == null) {
            return false;
        }
        if (msg.contains(">=") && msg.indexOf("=") > 0) {
            float version = getVersion(msg, "=");
            if (version == -1.0f || value < version) {
                return false;
            }
            return true;
        } else if (msg.contains("<=") && msg.indexOf("=") > 0) {
            float version2 = getVersion(msg, "=");
            if (version2 == -1.0f || value > version2) {
                return false;
            }
            return true;
        } else if (msg.contains(">")) {
            float version3 = getVersion(msg, ">");
            if (version3 == -1.0f || value <= version3) {
                return false;
            }
            return true;
        } else if (msg.contains("<")) {
            float version4 = getVersion(msg, "<");
            if (version4 == -1.0f || value >= version4) {
                return false;
            }
            return true;
        } else if (!msg.contains("=")) {
            return false;
        } else {
            float version5 = getVersion(msg, "=");
            if (version5 == -1.0f || value != version5) {
                return false;
            }
            return true;
        }
    }

    private static float getVersion(String msg, String sign) {
        String ver;
        if (msg == null || "".equals(msg) || sign == null || "".equals(sign) || (ver = msg.substring(msg.indexOf(sign) + 1, msg.length())) == null || "".equals(ver)) {
            return -1.0f;
        }
        return Float.valueOf(ver).floatValue();
    }

    static void saveListenerCLass(Context context, Class listenerClass) {
        String listenerName;
        if (listenerClass != null && (listenerName = listenerClass.getName()) != null && !"".equals(listenerName)) {
            PreferencesUtils.setPreferences(context, "mfpush", "pushListener", listenerName);
        }
    }

    static MFNotificationListener getNotificationListener(Context context) {
        String listenerName = PreferencesUtils.getPreference(context, "mfpush", "pushListener", "");
        if (listenerName == null || listenerName.trim().length() <= 0) {
            return null;
        }
        try {
            return (MFNotificationListener) Class.forName(listenerName).newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (InstantiationException e2) {
            e2.printStackTrace();
            return null;
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
            return null;
        } catch (ClassCastException e4) {
            e4.printStackTrace();
            return null;
        } catch (Exception e5) {
            e5.printStackTrace();
            return null;
        }
    }

    static String getDevId(Context context) {
        return PreferencesUtils.getPreference(context, MFStatInfo.INFO_FILE, MFStatInfo.KEY_DEV_ID, "");
    }
}
