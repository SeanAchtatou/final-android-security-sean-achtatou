package com.imofan.android.basic;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;

public class Mofang {
    private static final String COLLECTOR = "http://c.imofan.com/";
    public static final boolean DEBUG = false;
    public static final String LOG_TAG = "Mofang";
    private static final String REGISTER = "http://m.imofan.com/register/";
    static final String SDK_VERSION = "1.0.2";
    private static final int SEND_WAY_DAY = 2;
    private static final int SEND_WAY_OPEN = 1;
    private static final int SEND_WAY_REAL_TIME = 3;
    private static final int SEND_WAY_WIFI = 4;
    private static int activityId = 0;
    private static String appKey = null;
    private static Class currentActivity = null;
    private static String currentActivityAlias = null;
    private static MFDatabaseManager dbManager = null;
    private static boolean developDebug = false;
    private static boolean init = false;
    private static int maxLeaveTime = 30000;
    private static int maxUseTime = 2160000;
    private static int minUseTime = 3000;
    private static boolean newStart = true;
    private static long resumeTime = -1;
    private static int sendWay = 1;

    public static synchronized void init(Activity activity) {
        synchronized (Mofang.class) {
            if (init) {
                newStart = false;
            } else if (getAppKey(activity) != null) {
                if (dbManager == null) {
                    dbManager = MFDatabaseManager.getInstance(activity);
                }
                MFStatInfo.init(activity);
                int[] intTime = getDateAndHour(System.currentTimeMillis());
                MFStatInfo.updateAppAndDeviceInfo(dbManager, activity, intTime[0], intTime[1]);
                init = true;
            }
        }
    }

    public static String getAppKey(Context context) {
        if (appKey == null) {
            try {
                Object appKeyObj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("MOFANG_APPKEY");
                if (appKeyObj instanceof Integer) {
                    appKey = Integer.toString(((Integer) appKeyObj).intValue());
                } else if (appKeyObj instanceof Long) {
                    appKey = Long.toString((long) ((Long) appKeyObj).intValue());
                } else {
                    appKey = (String) appKeyObj;
                }
                Log.i(LOG_TAG, "imofan appkey: " + appKey);
            } catch (Exception e) {
                Log.e(LOG_TAG, "[getAppKey]Can't find metadata \"MOFANG_APPKEY\" in AndroidManifest.xml");
                e.printStackTrace();
            }
        }
        return appKey;
    }

    public static String getDevId(Context context) {
        try {
            return MFStatInfo.getString(MFStatInfo.KEY_DEV_ID, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void debug() {
        developDebug = true;
    }

    public static boolean isDebug() {
        return developDebug;
    }

    public static void enableCrashCollector(Activity context) {
        init(context);
        MFCrashHandler.init(dbManager);
    }

    public static void onResume(Activity context, String name) {
        init(context);
        resumeTime = System.currentTimeMillis();
        int[] dateAndHour = getDateAndHour(resumeTime);
        currentActivity = context.getClass();
        if (name == null || name.trim().length() <= 0) {
            currentActivityAlias = currentActivity.getName();
        } else {
            currentActivityAlias = name;
        }
        if (activityId > 0) {
            activityId++;
        } else {
            activityId = (int) (resumeTime - ((resumeTime / 1000000000) * 1000000000));
        }
        long activityPauseTime = MFStatInfo.getLong("user_pause_time", -1);
        if (newStart || resumeTime - activityPauseTime > ((long) maxLeaveTime)) {
            MFStatAccessPath.addAccessNode(0, null);
            long appUseDuration = MFStatInfo.getLong("user_duration", 0);
            if (appUseDuration > ((long) minUseTime) && appUseDuration <= ((long) maxUseTime)) {
                recordCloseEvent(appUseDuration);
            }
            MFStatInfo.putLong("user_duration", 0);
            recordOpenEvent(resumeTime);
            startEventSender(context, resumeTime);
        }
        MFStatAccessPath.addAccessNode(activityId, currentActivityAlias);
    }

    public static void onResume(Activity context) {
        onResume(context, null);
    }

    private static void onPause(Activity context, long duration) {
        long pauseTime = System.currentTimeMillis();
        MFStatInfo.putLong("user_pause_time", pauseTime);
        if (resumeTime > 0) {
            long appUseDuration = MFStatInfo.getLong("user_duration", 0);
            long activityUseDuration = pauseTime - resumeTime;
            MFStatInfo.putLong("user_duration", appUseDuration + activityUseDuration);
            if (duration >= 0) {
                if (duration <= ((long) maxUseTime)) {
                    MFStatAccessPath.updateAccessDuration(activityId, duration);
                }
            } else if (activityUseDuration <= ((long) maxUseTime)) {
                MFStatAccessPath.updateAccessDuration(activityId, activityUseDuration);
            }
        }
    }

    public static void onPause(Activity context) {
        onPause(context, -1);
    }

    public static void onEvent(Activity context, String eventKey, String lable, int count) {
        init(context);
        int[] intTime = getDateAndHour(System.currentTimeMillis());
        StringBuilder append = new StringBuilder().append(eventKey).append(":");
        if (lable == null) {
            lable = "-";
        }
        String eventValue = append.append(lable).toString();
        boolean isFirst = false;
        List<MFStatEvent> eventList = MFStatEvent.getEvents(dbManager, "developer", eventValue, intTime[0], false);
        List<MFStatEvent> sentEventList = MFStatEvent.getEvents(dbManager, "developer", eventValue, intTime[0], true);
        if ((eventList == null || eventList.size() == 0) && (sentEventList == null || sentEventList.size() == 0)) {
            isFirst = true;
        }
        MFStatEvent.addEvent(dbManager, "developer", intTime[0], intTime[1], eventValue, count, 0, isFirst, 0);
    }

    public static void onEvent(Activity context, String eventKey, String lable) {
        onEvent(context, eventKey, lable, 1);
    }

    public static void onEvent(Activity context, String eventKey, int count) {
        onEvent(context, eventKey, null, count);
    }

    public static void onEvent(Activity context, String eventKey) {
        onEvent(context, eventKey, null, 1);
    }

    public static void onNotificationReceive(Context context, String notificationId) {
        int[] dataAndHour = getDateAndHour(System.currentTimeMillis());
        if (notificationId != null && notificationId.trim().length() > 0) {
            if (dbManager == null) {
                dbManager = MFDatabaseManager.getInstance(context);
            }
            MFStatEvent.addEvent(dbManager, MFStatEvent.EVENT_NOTIFICATION_RECEIVE, dataAndHour[0], dataAndHour[1], notificationId);
        }
    }

    public static void onNotificationClick(Context context, String notificationId) {
        int[] dataAndHour = getDateAndHour(System.currentTimeMillis());
        if (notificationId != null && notificationId.trim().length() > 0) {
            if (dbManager == null) {
                dbManager = MFDatabaseManager.getInstance(context);
            }
            MFStatEvent.addEvent(dbManager, MFStatEvent.EVENT_NOTIFICATION_OPEN, dataAndHour[0], dataAndHour[1], notificationId);
        }
    }

    public static void onError(Activity context, Throwable exception) {
        init(context);
        JSONObject exceptionJson = new JSONObject();
        try {
            exceptionJson.put(Constants.SINA_NAME, exception.toString());
            exceptionJson.put("stack", Log.getStackTraceString(exception));
            Date now = new Date();
            MFStatEvent.addEvent(dbManager, "crash", Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(now)), Integer.parseInt(new SimpleDateFormat("H").format(now)), exceptionJson.toString());
        } catch (JSONException e) {
        }
    }

    private static void recordOpenEvent(long time) {
        int[] intTime = getDateAndHour(time);
        boolean first = false;
        List<MFStatEvent> todayOpenEventsSent = MFStatEvent.getEvents(dbManager, "open", null, intTime[0], true);
        List<MFStatEvent> todayOpenEvents = MFStatEvent.getEvents(dbManager, "open", null, intTime[0], false);
        if ((todayOpenEvents == null || todayOpenEvents.size() == 0) && (todayOpenEventsSent == null || todayOpenEventsSent.size() == 0)) {
            first = false;
        }
        MFStatEvent.addEvent(dbManager, "open", intTime[0], intTime[1], null, 1, 0, first, 0);
    }

    private static void recordCloseEvent(long duration) {
        long appCloseTime = MFStatInfo.getLong("user_pause_time", System.currentTimeMillis());
        int[] intTime = getDateAndHour(appCloseTime);
        MFStatEvent.addEvent(dbManager, "close", intTime[0], intTime[1], null, 1, duration, false, appCloseTime);
    }

    private static void startEventSender(final Context context, final long time) {
        switch (sendWay) {
            case 2:
            case 3:
            case 4:
                return;
            default:
                new Thread(new Runnable() {
                    public void run() {
                        Mofang.sendEventsToServer(Mofang.generateSendData(context, time), time);
                    }
                }).start();
                return;
        }
    }

    /* JADX WARN: Type inference failed for: r22v22, types: [java.net.URLConnection] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0291, code lost:
        r22 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0292, code lost:
        r13 = r14;
        r6 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0298, code lost:
        r8 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0299, code lost:
        r13 = r14;
        r6 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x029f, code lost:
        r8 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x02a0, code lost:
        r13 = r14;
        r6 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0179, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x017a, code lost:
        android.util.Log.e(com.imofan.android.basic.Mofang.LOG_TAG, "[sendEventsToServer]IOException: reader.close()");
        r8.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0185, code lost:
        r8 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0186, code lost:
        r13 = r14;
        r6 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01a9, code lost:
        r8 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01aa, code lost:
        r13 = r14;
        r6 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01c6, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        android.util.Log.e(com.imofan.android.basic.Mofang.LOG_TAG, "[sendEventsToServer]IOException: reader.close()");
        r8.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01d2, code lost:
        r8 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01d3, code lost:
        r13 = r14;
        r6 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x01e6  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01eb  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x022e A[SYNTHETIC, Splitter:B:116:0x022e] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0233  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0238  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0256 A[SYNTHETIC, Splitter:B:129:0x0256] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x025b  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0274 A[SYNTHETIC, Splitter:B:139:0x0274] */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0279  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0291 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:16:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0298 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:16:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:179:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:181:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:183:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:185:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:187:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x011d A[SYNTHETIC, Splitter:B:34:0x011d] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0136 A[Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0153 A[SYNTHETIC, Splitter:B:41:0x0153] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0185 A[ExcHandler: UnsupportedEncodingException (e java.io.UnsupportedEncodingException), Splitter:B:16:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0194 A[SYNTHETIC, Splitter:B:63:0x0194] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0199  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01a5 A[SYNTHETIC, Splitter:B:71:0x01a5] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01a9 A[ExcHandler: MalformedURLException (e java.net.MalformedURLException), Splitter:B:16:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01b8 A[SYNTHETIC, Splitter:B:81:0x01b8] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01c2  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01d2 A[ExcHandler: ProtocolException (e java.net.ProtocolException), Splitter:B:16:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01e1 A[SYNTHETIC, Splitter:B:97:0x01e1] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x0120=Splitter:B:36:0x0120, B:73:0x01a8=Splitter:B:73:0x01a8} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void sendEventsToServer(com.imofan.android.basic.Mofang.SendData r25, long r26) {
        /*
            r5 = 0
            r6 = 0
            r13 = 0
            org.json.JSONObject r22 = r25.getJson()     // Catch:{ UnsupportedEncodingException -> 0x02b1, MalformedURLException -> 0x02aa, ProtocolException -> 0x02a3, IOException -> 0x0221, Exception -> 0x0249 }
            java.lang.String r22 = r22.toString()     // Catch:{ UnsupportedEncodingException -> 0x02b1, MalformedURLException -> 0x02aa, ProtocolException -> 0x02a3, IOException -> 0x0221, Exception -> 0x0249 }
            java.lang.String r23 = "utf-8"
            byte[] r11 = r22.getBytes(r23)     // Catch:{ UnsupportedEncodingException -> 0x02b1, MalformedURLException -> 0x02aa, ProtocolException -> 0x02a3, IOException -> 0x0221, Exception -> 0x0249 }
            java.util.zip.Deflater r7 = new java.util.zip.Deflater     // Catch:{ UnsupportedEncodingException -> 0x02b1, MalformedURLException -> 0x02aa, ProtocolException -> 0x02a3, IOException -> 0x0221, Exception -> 0x0249 }
            r7.<init>()     // Catch:{ UnsupportedEncodingException -> 0x02b1, MalformedURLException -> 0x02aa, ProtocolException -> 0x02a3, IOException -> 0x0221, Exception -> 0x0249 }
            r7.setInput(r11)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r7.finish()     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r19 = 0
            r22 = 0
            r0 = r22
            byte[] r4 = new byte[r0]     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r22 = 128(0x80, float:1.794E-43)
            r0 = r22
            byte[] r2 = new byte[r0]     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
        L_0x002a:
            boolean r22 = r7.finished()     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            if (r22 != 0) goto L_0x0062
            int r3 = r7.deflate(r2)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            int r0 = r4.length     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r22 = r0
            int r22 = r22 + r3
            r0 = r22
            byte[] r0 = new byte[r0]     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r20 = r0
            r10 = 0
        L_0x0040:
            int r0 = r4.length     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r22 = r0
            r0 = r22
            if (r10 >= r0) goto L_0x004e
            byte r22 = r4[r10]     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r20[r10] = r22     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            int r10 = r10 + 1
            goto L_0x0040
        L_0x004e:
            r10 = 0
        L_0x004f:
            if (r10 >= r3) goto L_0x005d
            int r0 = r4.length     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r22 = r0
            int r22 = r22 + r10
            byte r23 = r2[r10]     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r20[r22] = r23     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            int r10 = r10 + 1
            goto L_0x004f
        L_0x005d:
            r4 = r20
            int r19 = r19 + r3
            goto L_0x002a
        L_0x0062:
            java.net.URL r21 = new java.net.URL     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r22.<init>()     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.lang.String r23 = "http://c.imofan.com/"
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.lang.String r23 = com.imofan.android.basic.Mofang.appKey     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.lang.String r22 = r22.toString()     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r21.<init>(r22)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.net.URLConnection r22 = r21.openConnection()     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r0 = r22
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r5 = r0
            r22 = 1
            r0 = r22
            r5.setDoOutput(r0)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r22 = 1
            r0 = r22
            r5.setDoInput(r0)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r22 = 20000(0x4e20, float:2.8026E-41)
            r0 = r22
            r5.setReadTimeout(r0)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r22 = 20000(0x4e20, float:2.8026E-41)
            r0 = r22
            r5.setConnectTimeout(r0)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.lang.String r22 = "POST"
            r0 = r22
            r5.setRequestMethod(r0)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r22 = 0
            r0 = r22
            r5.setUseCaches(r0)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.lang.String r22 = "Connection"
            java.lang.String r23 = "Keep-Alive"
            r0 = r22
            r1 = r23
            r5.setRequestProperty(r0, r1)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.lang.String r22 = "Content-Type"
            java.lang.String r23 = "application/octet-stream"
            r0 = r22
            r1 = r23
            r5.setRequestProperty(r0, r1)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r5.connect()     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.io.DataOutputStream r14 = new java.io.DataOutputStream     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            java.io.OutputStream r22 = r5.getOutputStream()     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r0 = r22
            r14.<init>(r0)     // Catch:{ UnsupportedEncodingException -> 0x02b4, MalformedURLException -> 0x02ad, ProtocolException -> 0x02a6, IOException -> 0x029c, Exception -> 0x0295, all -> 0x028e }
            r14.write(r4)     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r14.flush()     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            int r9 = r5.getResponseCode()     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r22 = 200(0xc8, float:2.8E-43)
            r0 = r22
            if (r9 != r0) goto L_0x0151
            java.lang.StringBuffer r18 = new java.lang.StringBuffer     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r18.<init>()     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r15 = 0
            java.io.BufferedReader r16 = new java.io.BufferedReader     // Catch:{ IOException -> 0x02bd }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x02bd }
            java.io.InputStream r23 = r5.getInputStream()     // Catch:{ IOException -> 0x02bd }
            r22.<init>(r23)     // Catch:{ IOException -> 0x02bd }
            r0 = r16
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x02bd }
            r12 = 0
        L_0x00fc:
            java.lang.String r12 = r16.readLine()     // Catch:{ IOException -> 0x010e, all -> 0x02b8 }
            if (r12 == 0) goto L_0x0163
            r0 = r18
            java.lang.StringBuffer r22 = r0.append(r12)     // Catch:{ IOException -> 0x010e, all -> 0x02b8 }
            java.lang.String r23 = "\n"
            r22.append(r23)     // Catch:{ IOException -> 0x010e, all -> 0x02b8 }
            goto L_0x00fc
        L_0x010e:
            r8 = move-exception
            r15 = r16
        L_0x0111:
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException: get response"
            android.util.Log.e(r22, r23)     // Catch:{ all -> 0x01a2 }
            r8.printStackTrace()     // Catch:{ all -> 0x01a2 }
            if (r15 == 0) goto L_0x0120
            r15.close()     // Catch:{ IOException -> 0x0179, UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, Exception -> 0x0298, all -> 0x0291 }
        L_0x0120:
            java.lang.String r17 = r18.toString()     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            java.lang.String r22 = "HTTPSQS_PUT_OK"
            r0 = r17
            r1 = r22
            int r22 = r0.indexOf(r1)     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r23 = -1
            r0 = r22
            r1 = r23
            if (r0 <= r1) goto L_0x0151
            com.imofan.android.basic.MFDatabaseManager r22 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            java.util.List r23 = r25.getIdList()     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            com.imofan.android.basic.MFStatEvent.signSentEvent(r22, r23)     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            com.imofan.android.basic.MFDatabaseManager r22 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            com.imofan.android.basic.MFStatEvent.clearAccessPath(r22)     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            com.imofan.android.basic.MFDatabaseManager r22 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            int[] r23 = getDateAndHour(r26)     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r24 = 0
            r23 = r23[r24]     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            com.imofan.android.basic.MFStatEvent.clearHistoryEvents(r22, r23)     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
        L_0x0151:
            if (r14 == 0) goto L_0x0156
            r14.close()     // Catch:{ IOException -> 0x01f0 }
        L_0x0156:
            if (r7 == 0) goto L_0x015b
            r7.end()
        L_0x015b:
            if (r5 == 0) goto L_0x02c0
            r5.disconnect()
            r13 = r14
            r6 = r7
        L_0x0162:
            return
        L_0x0163:
            if (r16 == 0) goto L_0x02c4
            r16.close()     // Catch:{ IOException -> 0x016b, UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, Exception -> 0x0298, all -> 0x0291 }
            r15 = r16
            goto L_0x0120
        L_0x016b:
            r8 = move-exception
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException: reader.close()"
            android.util.Log.e(r22, r23)     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r8.printStackTrace()     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r15 = r16
            goto L_0x0120
        L_0x0179:
            r8 = move-exception
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException: reader.close()"
            android.util.Log.e(r22, r23)     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r8.printStackTrace()     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            goto L_0x0120
        L_0x0185:
            r8 = move-exception
            r13 = r14
            r6 = r7
        L_0x0188:
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]UnsupportedEncodingException"
            android.util.Log.e(r22, r23)     // Catch:{ all -> 0x0271 }
            r8.printStackTrace()     // Catch:{ all -> 0x0271 }
            if (r13 == 0) goto L_0x0197
            r13.close()     // Catch:{ IOException -> 0x01fd }
        L_0x0197:
            if (r6 == 0) goto L_0x019c
            r6.end()
        L_0x019c:
            if (r5 == 0) goto L_0x0162
            r5.disconnect()
            goto L_0x0162
        L_0x01a2:
            r22 = move-exception
        L_0x01a3:
            if (r15 == 0) goto L_0x01a8
            r15.close()     // Catch:{ IOException -> 0x01c6, UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, Exception -> 0x0298, all -> 0x0291 }
        L_0x01a8:
            throw r22     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
        L_0x01a9:
            r8 = move-exception
            r13 = r14
            r6 = r7
        L_0x01ac:
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]MalformedURLException"
            android.util.Log.e(r22, r23)     // Catch:{ all -> 0x0271 }
            r8.printStackTrace()     // Catch:{ all -> 0x0271 }
            if (r13 == 0) goto L_0x01bb
            r13.close()     // Catch:{ IOException -> 0x0209 }
        L_0x01bb:
            if (r6 == 0) goto L_0x01c0
            r6.end()
        L_0x01c0:
            if (r5 == 0) goto L_0x0162
            r5.disconnect()
            goto L_0x0162
        L_0x01c6:
            r8 = move-exception
            java.lang.String r23 = "Mofang"
            java.lang.String r24 = "[sendEventsToServer]IOException: reader.close()"
            android.util.Log.e(r23, r24)     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            r8.printStackTrace()     // Catch:{ UnsupportedEncodingException -> 0x0185, MalformedURLException -> 0x01a9, ProtocolException -> 0x01d2, IOException -> 0x029f, Exception -> 0x0298, all -> 0x0291 }
            goto L_0x01a8
        L_0x01d2:
            r8 = move-exception
            r13 = r14
            r6 = r7
        L_0x01d5:
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]ProtocolException"
            android.util.Log.e(r22, r23)     // Catch:{ all -> 0x0271 }
            r8.printStackTrace()     // Catch:{ all -> 0x0271 }
            if (r13 == 0) goto L_0x01e4
            r13.close()     // Catch:{ IOException -> 0x0215 }
        L_0x01e4:
            if (r6 == 0) goto L_0x01e9
            r6.end()
        L_0x01e9:
            if (r5 == 0) goto L_0x0162
            r5.disconnect()
            goto L_0x0162
        L_0x01f0:
            r8 = move-exception
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException: output.close()"
            android.util.Log.e(r22, r23)
            r8.printStackTrace()
            goto L_0x0156
        L_0x01fd:
            r8 = move-exception
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException: output.close()"
            android.util.Log.e(r22, r23)
            r8.printStackTrace()
            goto L_0x0197
        L_0x0209:
            r8 = move-exception
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException: output.close()"
            android.util.Log.e(r22, r23)
            r8.printStackTrace()
            goto L_0x01bb
        L_0x0215:
            r8 = move-exception
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException: output.close()"
            android.util.Log.e(r22, r23)
            r8.printStackTrace()
            goto L_0x01e4
        L_0x0221:
            r8 = move-exception
        L_0x0222:
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException"
            android.util.Log.e(r22, r23)     // Catch:{ all -> 0x0271 }
            r8.printStackTrace()     // Catch:{ all -> 0x0271 }
            if (r13 == 0) goto L_0x0231
            r13.close()     // Catch:{ IOException -> 0x023d }
        L_0x0231:
            if (r6 == 0) goto L_0x0236
            r6.end()
        L_0x0236:
            if (r5 == 0) goto L_0x0162
            r5.disconnect()
            goto L_0x0162
        L_0x023d:
            r8 = move-exception
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException: output.close()"
            android.util.Log.e(r22, r23)
            r8.printStackTrace()
            goto L_0x0231
        L_0x0249:
            r8 = move-exception
        L_0x024a:
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]Exception"
            android.util.Log.e(r22, r23)     // Catch:{ all -> 0x0271 }
            r8.printStackTrace()     // Catch:{ all -> 0x0271 }
            if (r13 == 0) goto L_0x0259
            r13.close()     // Catch:{ IOException -> 0x0265 }
        L_0x0259:
            if (r6 == 0) goto L_0x025e
            r6.end()
        L_0x025e:
            if (r5 == 0) goto L_0x0162
            r5.disconnect()
            goto L_0x0162
        L_0x0265:
            r8 = move-exception
            java.lang.String r22 = "Mofang"
            java.lang.String r23 = "[sendEventsToServer]IOException: output.close()"
            android.util.Log.e(r22, r23)
            r8.printStackTrace()
            goto L_0x0259
        L_0x0271:
            r22 = move-exception
        L_0x0272:
            if (r13 == 0) goto L_0x0277
            r13.close()     // Catch:{ IOException -> 0x0282 }
        L_0x0277:
            if (r6 == 0) goto L_0x027c
            r6.end()
        L_0x027c:
            if (r5 == 0) goto L_0x0281
            r5.disconnect()
        L_0x0281:
            throw r22
        L_0x0282:
            r8 = move-exception
            java.lang.String r23 = "Mofang"
            java.lang.String r24 = "[sendEventsToServer]IOException: output.close()"
            android.util.Log.e(r23, r24)
            r8.printStackTrace()
            goto L_0x0277
        L_0x028e:
            r22 = move-exception
            r6 = r7
            goto L_0x0272
        L_0x0291:
            r22 = move-exception
            r13 = r14
            r6 = r7
            goto L_0x0272
        L_0x0295:
            r8 = move-exception
            r6 = r7
            goto L_0x024a
        L_0x0298:
            r8 = move-exception
            r13 = r14
            r6 = r7
            goto L_0x024a
        L_0x029c:
            r8 = move-exception
            r6 = r7
            goto L_0x0222
        L_0x029f:
            r8 = move-exception
            r13 = r14
            r6 = r7
            goto L_0x0222
        L_0x02a3:
            r8 = move-exception
            goto L_0x01d5
        L_0x02a6:
            r8 = move-exception
            r6 = r7
            goto L_0x01d5
        L_0x02aa:
            r8 = move-exception
            goto L_0x01ac
        L_0x02ad:
            r8 = move-exception
            r6 = r7
            goto L_0x01ac
        L_0x02b1:
            r8 = move-exception
            goto L_0x0188
        L_0x02b4:
            r8 = move-exception
            r6 = r7
            goto L_0x0188
        L_0x02b8:
            r22 = move-exception
            r15 = r16
            goto L_0x01a3
        L_0x02bd:
            r8 = move-exception
            goto L_0x0111
        L_0x02c0:
            r13 = r14
            r6 = r7
            goto L_0x0162
        L_0x02c4:
            r15 = r16
            goto L_0x0120
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imofan.android.basic.Mofang.sendEventsToServer(com.imofan.android.basic.Mofang$SendData, long):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x041b, code lost:
        r58 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x041c, code lost:
        android.util.Log.e(com.imofan.android.basic.Mofang.LOG_TAG, "[generateSendData]JSONException");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0427, code lost:
        if (isDebug() != false) goto L_0x0429;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0429, code lost:
        r58.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0469, code lost:
        r58 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x046a, code lost:
        android.util.Log.e(com.imofan.android.basic.Mofang.LOG_TAG, "[generateSendData]NoSuchAlgorithmException");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0475, code lost:
        if (isDebug() != false) goto L_0x0477;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0477, code lost:
        r58.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x049e, code lost:
        r58 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x049f, code lost:
        android.util.Log.e(com.imofan.android.basic.Mofang.LOG_TAG, "[generateSendData]UnsupportedEncodingException");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x04aa, code lost:
        if (isDebug() != false) goto L_0x04ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x04ac, code lost:
        r58.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x07cf, code lost:
        if (r21.get(r68 - 1).getActivity() != null) goto L_0x07d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x0829, code lost:
        if (r21.get(r68 + 1).getActivity() != null) goto L_0x082b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x0833, code lost:
        if (r68 != (r21.size() - 1)) goto L_0x0876;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x0835, code lost:
        r85.incExit();
        r22 = r85.getActivity() + ">";
        r109 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x0859, code lost:
        if (r113.containsKey(r22) == false) goto L_0x086b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x085b, code lost:
        r109 = 1 + ((java.lang.Integer) r113.get(r22)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x086b, code lost:
        r113.put(r22, java.lang.Integer.valueOf(r109));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x0876, code lost:
        r96.put(r85.getActivity(), r85);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x041b A[ExcHandler: JSONException (r58v2 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:1:0x0012] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0469 A[ExcHandler: NoSuchAlgorithmException (r58v1 'e' java.security.NoSuchAlgorithmException A[CUSTOM_DECLARE]), Splitter:B:1:0x0012] */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x07f6 A[Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x081b A[Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.imofan.android.basic.Mofang.SendData generateSendData(android.content.Context r139, long r140) {
        /*
            int[] r74 = getDateAndHour(r140)
            java.util.ArrayList r60 = new java.util.ArrayList
            r60.<init>()
            org.json.JSONObject r80 = new org.json.JSONObject
            r80.<init>()
            java.lang.String r3 = "send_time"
            r0 = r140
            double r4 = (double) r0     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r8 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r4 = r4 / r8
            long r4 = java.lang.Math.round(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "install_date"
            java.lang.String r4 = "install_date"
            r5 = 0
            r5 = r74[r5]     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r4 = com.imofan.android.basic.MFStatInfo.getInt(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "dev_id"
            java.lang.String r4 = "dev_id"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "dev_id_udf"
            r4 = 0
            int r77 = com.imofan.android.basic.MFStatInfo.getInt(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = 1
            r0 = r77
            if (r0 != r3) goto L_0x0055
            java.lang.String r3 = "dev_id_udf"
            r0 = r80
            r1 = r77
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0055:
            java.lang.String r3 = "sdk_ver"
            java.lang.String r4 = "sdk_ver"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "app_ver"
            java.lang.String r4 = "app_ver"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "channel"
            java.lang.String r4 = "channel"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "os"
            java.lang.String r4 = "Android"
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "os_ver"
            java.lang.String r4 = "os_ver"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "carrier"
            java.lang.String r4 = "carrier"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r91 = "NONE"
            java.lang.String r3 = "connectivity"
            r0 = r139
            java.lang.Object r36 = r0.getSystemService(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            android.net.ConnectivityManager r36 = (android.net.ConnectivityManager) r36     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = 1
            r0 = r36
            android.net.NetworkInfo r3 = r0.getNetworkInfo(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            android.net.NetworkInfo$State r128 = r3.getState()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            android.net.NetworkInfo$State r3 = android.net.NetworkInfo.State.CONNECTED     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r128
            if (r0 == r3) goto L_0x00c7
            android.net.NetworkInfo$State r3 = android.net.NetworkInfo.State.CONNECTING     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r128
            if (r0 != r3) goto L_0x026e
        L_0x00c7:
            java.lang.String r91 = "WiFi"
        L_0x00c9:
            java.lang.String r3 = "access"
            r0 = r80
            r1 = r91
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "resolution"
            java.lang.String r4 = "resolution"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "model"
            java.lang.String r4 = "model"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "timezone"
            java.lang.String r4 = "timezone"
            r5 = 8
            int r4 = com.imofan.android.basic.MFStatInfo.getInt(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "language"
            java.lang.String r4 = "language"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "country"
            java.lang.String r4 = "country"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "device_id"
            java.lang.String r4 = "device_id"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "mac_addr"
            java.lang.String r4 = "mac_addr"
            r5 = 0
            java.lang.String r4 = com.imofan.android.basic.MFStatInfo.getString(r4, r5)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r80
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r28 = 0
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int[] r50 = com.imofan.android.basic.MFStatEvent.getEventDates(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r50 == 0) goto L_0x070b
            r0 = r50
            int r3 = r0.length     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x070b
            org.json.JSONObject r47 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r47.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r29 = r50
            r0 = r29
            int r0 = r0.length     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r82 = r0
            r69 = 0
            r70 = r69
        L_0x0154:
            r0 = r70
            r1 = r82
            if (r0 >= r1) goto L_0x0702
            r48 = r29[r70]     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            org.json.JSONObject r49 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r49.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r100 = 0
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "install"
            r5 = 0
            r6 = 0
            r0 = r48
            java.util.List r71 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r0, r6)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "upgrade"
            r5 = 0
            r6 = 0
            r0 = r48
            java.util.List r136 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r0, r6)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r71 == 0) goto L_0x0183
            int r3 = r71.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 > 0) goto L_0x018b
        L_0x0183:
            if (r136 == 0) goto L_0x03c6
            int r3 = r136.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x03c6
        L_0x018b:
            org.apache.http.params.BasicHttpParams r108 = new org.apache.http.params.BasicHttpParams     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r108.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = 20000(0x4e20, float:2.8026E-41)
            r0 = r108
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = 20000(0x4e20, float:2.8026E-41)
            r0 = r108
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            org.apache.http.impl.client.DefaultHttpClient r67 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r67
            r1 = r108
            r0.<init>(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r25 = getAppKey(r139)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r25.length()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + -8
            r0 = r25
            java.lang.String r3 = r0.substring(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r4 = 16
            java.lang.Long r3 = java.lang.Long.valueOf(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r24 = r3.toString()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "dev_id"
            java.lang.String r4 = ""
            java.lang.String r54 = com.imofan.android.basic.MFStatInfo.getString(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = 0
            r72 = r74[r3]     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r71 == 0) goto L_0x01e1
            int r3 = r71.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x01e1
            r3 = 0
            r0 = r71
            java.lang.Object r3 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r3 = (com.imofan.android.basic.MFStatEvent) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r72 = r3.getDate()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x01e1:
            java.lang.String r3 = "channel"
            java.lang.String r4 = ""
            java.lang.String r32 = com.imofan.android.basic.MFStatInfo.getString(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "app_ver"
            java.lang.String r4 = ""
            java.lang.String r26 = com.imofan.android.basic.MFStatInfo.getString(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r54
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = ":"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r24
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = ":"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r32
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = ":"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r26
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = ":"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r72
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r130 = r3.toString()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "MD5"
            java.security.MessageDigest r88 = java.security.MessageDigest.getInstance(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            byte[] r3 = r130.getBytes()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r88
            byte[] r57 = r0.digest(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r31.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r68 = 0
        L_0x0247:
            r0 = r57
            int r3 = r0.length     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r68
            if (r0 >= r3) goto L_0x02a2
            byte r3 = r57[r68]     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = r3 & 255(0xff, float:3.57E-43)
            java.lang.String r30 = java.lang.Integer.toHexString(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r30.length()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r4 = 1
            if (r3 != r4) goto L_0x0264
            r3 = 48
            r0 = r31
            r0.append(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0264:
            r0 = r31
            r1 = r30
            r0.append(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r68 = r68 + 1
            goto L_0x0247
        L_0x026e:
            r3 = 0
            r0 = r36
            android.net.NetworkInfo r3 = r0.getNetworkInfo(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            android.net.NetworkInfo$State r128 = r3.getState()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            android.net.NetworkInfo$State r3 = android.net.NetworkInfo.State.CONNECTED     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r128
            if (r0 == r3) goto L_0x0285
            android.net.NetworkInfo$State r3 = android.net.NetworkInfo.State.CONNECTING     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r128
            if (r0 != r3) goto L_0x00c9
        L_0x0285:
            java.lang.String r91 = "2G/3G"
            java.lang.String r3 = "phone"
            r0 = r139
            java.lang.Object r131 = r0.getSystemService(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            android.telephony.TelephonyManager r131 = (android.telephony.TelephonyManager) r131     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r92 = r131.getNetworkType()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            switch(r92) {
                case 1: goto L_0x029a;
                case 2: goto L_0x029a;
                case 3: goto L_0x029e;
                case 4: goto L_0x029a;
                case 5: goto L_0x029e;
                case 6: goto L_0x029e;
                case 7: goto L_0x029a;
                case 8: goto L_0x029e;
                case 9: goto L_0x029e;
                case 10: goto L_0x029e;
                default: goto L_0x0298;
            }     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0298:
            goto L_0x00c9
        L_0x029a:
            java.lang.String r91 = "2G"
            goto L_0x00c9
        L_0x029e:
            java.lang.String r91 = "3G"
            goto L_0x00c9
        L_0x02a2:
            java.lang.String r86 = r31.toString()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "http://m.imofan.com/register/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r86
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r25
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r54
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r72
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r32
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "/"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "UTF-8"
            r0 = r26
            java.lang.String r4 = java.net.URLEncoder.encode(r0, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r138 = r3.toString()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            org.apache.http.client.methods.HttpPost r115 = new org.apache.http.client.methods.HttpPost     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r115
            r1 = r138
            r0.<init>(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "Accept-Encoding"
            java.lang.String r4 = "gzip, deflate"
            r0 = r115
            r0.setHeader(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r124 = 0
            r0 = r67
            r1 = r115
            org.apache.http.HttpResponse r122 = r0.execute(r1)     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            org.apache.http.StatusLine r3 = r122.getStatusLine()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            int r3 = r3.getStatusCode()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r3 != r4) goto L_0x039e
            org.apache.http.HttpEntity r3 = r122.getEntity()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            java.io.InputStream r75 = r3.getContent()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            if (r75 == 0) goto L_0x039e
            java.lang.String r3 = "Content-Encoding"
            r0 = r122
            org.apache.http.Header r38 = r0.getFirstHeader(r3)     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            if (r38 == 0) goto L_0x0350
            java.lang.String r3 = r38.getValue()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            java.lang.String r4 = "gzip"
            boolean r3 = r3.equalsIgnoreCase(r4)     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            if (r3 == 0) goto L_0x0350
            java.util.zip.GZIPInputStream r76 = new java.util.zip.GZIPInputStream     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            r0 = r76
            r1 = r75
            r0.<init>(r1)     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            r75 = r76
        L_0x0350:
            r120 = 0
            java.lang.String r83 = ""
            java.lang.StringBuffer r37 = new java.lang.StringBuffer     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            r37.<init>()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
            java.io.BufferedReader r121 = new java.io.BufferedReader     // Catch:{ all -> 0x0ce1 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x0ce1 }
            r0 = r75
            r3.<init>(r0)     // Catch:{ all -> 0x0ce1 }
            r0 = r121
            r0.<init>(r3)     // Catch:{ all -> 0x0ce1 }
        L_0x0367:
            java.lang.String r83 = r121.readLine()     // Catch:{ all -> 0x037b }
            if (r83 == 0) goto L_0x0390
            r0 = r37
            r1 = r83
            java.lang.StringBuffer r3 = r0.append(r1)     // Catch:{ all -> 0x037b }
            java.lang.String r4 = "\r\n"
            r3.append(r4)     // Catch:{ all -> 0x037b }
            goto L_0x0367
        L_0x037b:
            r3 = move-exception
            r120 = r121
        L_0x037e:
            if (r120 == 0) goto L_0x0383
            r120.close()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
        L_0x0383:
            if (r75 == 0) goto L_0x0388
            r75.close()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
        L_0x0388:
            throw r3     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
        L_0x0389:
            r58 = move-exception
            r58.printStackTrace()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r127 = 0
        L_0x038f:
            return r127
        L_0x0390:
            java.lang.String r124 = r37.toString()     // Catch:{ all -> 0x037b }
            if (r121 == 0) goto L_0x0399
            r121.close()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
        L_0x0399:
            if (r75 == 0) goto L_0x039e
            r75.close()     // Catch:{ UnsupportedEncodingException -> 0x0389, ClientProtocolException -> 0x0444, IOException -> 0x044c, Exception -> 0x0454, JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469 }
        L_0x039e:
            if (r124 == 0) goto L_0x03c6
            java.lang.String r3 = r124.trim()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3.length()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x03c6
            org.json.JSONObject r123 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r123.<init>(r124)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "status"
            r4 = 1
            r0 = r123
            int r129 = r0.optInt(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = 1
            r0 = r129
            if (r0 != r3) goto L_0x03c6
            java.lang.String r3 = "app_ver"
            r4 = 0
            r0 = r123
            java.lang.String r100 = r0.optString(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x03c6:
            if (r100 == 0) goto L_0x047b
            r27 = 0
            android.content.pm.PackageManager r3 = r139.getPackageManager()     // Catch:{ NameNotFoundException -> 0x045c }
            java.lang.String r4 = r139.getPackageName()     // Catch:{ NameNotFoundException -> 0x045c }
            r5 = 16384(0x4000, float:2.2959E-41)
            android.content.pm.PackageInfo r104 = r3.getPackageInfo(r4, r5)     // Catch:{ NameNotFoundException -> 0x045c }
            r0 = r104
            java.lang.String r0 = r0.versionName     // Catch:{ NameNotFoundException -> 0x045c }
            r27 = r0
        L_0x03de:
            if (r27 == 0) goto L_0x03f5
            r0 = r27
            r1 = r100
            boolean r3 = r0.equals(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 != 0) goto L_0x03f5
            java.lang.String r3 = "upgrade"
            r0 = r49
            r1 = r100
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r28 = 1
        L_0x03f5:
            if (r71 == 0) goto L_0x04b1
            int r3 = r71.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x04b1
            java.util.Iterator r69 = r71.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0401:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x04b1
            java.lang.Object r73 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r73 = (com.imofan.android.basic.MFStatEvent) r73     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r73.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x0401
        L_0x041b:
            r58 = move-exception
            java.lang.String r3 = "Mofang"
            java.lang.String r4 = "[generateSendData]JSONException"
            android.util.Log.e(r3, r4)
            boolean r3 = isDebug()
            if (r3 == 0) goto L_0x042c
            r58.printStackTrace()
        L_0x042c:
            com.imofan.android.basic.Mofang$SendData r127 = new com.imofan.android.basic.Mofang$SendData
            r3 = 0
            r0 = r127
            r0.<init>()
            r0 = r127
            r1 = r80
            r0.setJson(r1)
            r0 = r127
            r1 = r60
            r0.setIdList(r1)
            goto L_0x038f
        L_0x0444:
            r58 = move-exception
            r58.printStackTrace()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r127 = 0
            goto L_0x038f
        L_0x044c:
            r58 = move-exception
            r58.printStackTrace()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r127 = 0
            goto L_0x038f
        L_0x0454:
            r58 = move-exception
            r58.printStackTrace()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r127 = 0
            goto L_0x038f
        L_0x045c:
            r58 = move-exception
            java.lang.String r3 = "Mofang"
            java.lang.String r4 = "[generateSendData]Get app version error"
            android.util.Log.e(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r58.printStackTrace()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x03de
        L_0x0469:
            r58 = move-exception
            java.lang.String r3 = "Mofang"
            java.lang.String r4 = "[generateSendData]NoSuchAlgorithmException"
            android.util.Log.e(r3, r4)
            boolean r3 = isDebug()
            if (r3 == 0) goto L_0x042c
            r58.printStackTrace()
            goto L_0x042c
        L_0x047b:
            if (r71 == 0) goto L_0x03f5
            int r3 = r71.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x03f5
            java.lang.String r4 = "install"
            int r3 = r71.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + -1
            r0 = r71
            java.lang.Object r3 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r3 = (com.imofan.android.basic.MFStatEvent) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3.getHour()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r49
            r0.put(r4, r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x03f5
        L_0x049e:
            r58 = move-exception
            java.lang.String r3 = "Mofang"
            java.lang.String r4 = "[generateSendData]UnsupportedEncodingException"
            android.util.Log.e(r3, r4)
            boolean r3 = isDebug()
            if (r3 == 0) goto L_0x042c
            r58.printStackTrace()
            goto L_0x042c
        L_0x04b1:
            if (r136 == 0) goto L_0x04d7
            int r3 = r136.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x04d7
            java.util.Iterator r69 = r136.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x04bd:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x04d7
            java.lang.Object r137 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r137 = (com.imofan.android.basic.MFStatEvent) r137     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r137.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x04bd
        L_0x04d7:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "return"
            r5 = 0
            r6 = 0
            r0 = r48
            java.util.List r126 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r0, r6)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r126 == 0) goto L_0x0511
            int r3 = r126.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x0511
            java.lang.String r3 = "returned"
            r4 = 1
            r0 = r49
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r69 = r126.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x04f7:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0511
            java.lang.Object r125 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r125 = (com.imofan.android.basic.MFStatEvent) r125     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r125.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x04f7
        L_0x0511:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r4 = 0
            r5 = 0
            r6 = 1
            r0 = r48
            java.util.List r23 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r0, r6)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r23 == 0) goto L_0x0524
            int r3 = r23.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 != 0) goto L_0x052c
        L_0x0524:
            java.lang.String r3 = "daily_first"
            r4 = 1
            r0 = r49
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x052c:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "open"
            r5 = 0
            r6 = 0
            r0 = r48
            java.util.List r102 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r0, r6)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r102 == 0) goto L_0x05d4
            int r3 = r102.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x05d4
            org.json.JSONObject r103 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r103.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.HashMap r66 = new java.util.HashMap     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r66.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r69 = r102.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x054e:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x05a1
            java.lang.Object r101 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r101 = (com.imofan.android.basic.MFStatEvent) r101     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r65 = 0
            int r3 = r101.getHour()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r66
            boolean r3 = r0.containsKey(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0580
            int r3 = r101.getHour()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r66
            java.lang.Object r3 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r65 = r3.intValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0580:
            int r3 = r101.getHour()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r4 = r65 + 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r66
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r101.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x054e
        L_0x05a1:
            java.util.Set r3 = r66.keySet()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r78 = r3.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x05a9:
            boolean r3 = r78.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x05cb
            java.lang.Object r81 = r78.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r81 = (java.lang.Integer) r81     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r81.intValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r66
            r1 = r81
            java.lang.Object r4 = r0.get(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r103
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x05a9
        L_0x05cb:
            java.lang.String r3 = "open"
            r0 = r49
            r1 = r103
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x05d4:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "close"
            r5 = 0
            r6 = 0
            r0 = r48
            java.util.List r35 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r0, r6)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r35 == 0) goto L_0x0628
            int r3 = r35.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x0628
            org.json.JSONArray r33 = new org.json.JSONArray     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r33.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r69 = r35.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x05f1:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x061f
            java.lang.Object r34 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r34 = (com.imofan.android.basic.MFStatEvent) r34     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r3 = r34.getDuration()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            double r3 = (double) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r5 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r3 = r3 / r5
            long r3 = java.lang.Math.round(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r33
            r0.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r34.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x05f1
        L_0x061f:
            java.lang.String r3 = "close"
            r0 = r49
            r1 = r33
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0628:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "developer"
            r5 = 0
            r6 = 0
            r0 = r48
            java.util.List r53 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r0, r6)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r53 == 0) goto L_0x06f1
            int r3 = r53.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x06f1
            org.json.JSONObject r55 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r55.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.HashMap r51 = new java.util.HashMap     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r51.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r69 = r53.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x064a:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x06a5
            java.lang.Object r52 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r52 = (com.imofan.android.basic.MFStatEvent) r52     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r52.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r51
            java.lang.Object r84 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r84 = (com.imofan.android.basic.MFStatEvent) r84     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r84 != 0) goto L_0x0672
            com.imofan.android.basic.MFStatEvent r84 = new com.imofan.android.basic.MFStatEvent     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r84.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r52.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r84
            r0.setValue(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0672:
            int r3 = r84.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r4 = r52.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + r4
            r0 = r84
            r0.setCount(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            boolean r3 = r52.isFirst()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x068c
            r3 = 1
            r0 = r84
            r0.setFirst(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x068c:
            java.lang.String r3 = r52.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r51
            r1 = r84
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r52.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x064a
        L_0x06a5:
            java.util.Set r3 = r51.keySet()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r79 = r3.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x06ad:
            boolean r3 = r79.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x06e8
            java.lang.Object r81 = r79.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r81 = (java.lang.String) r81     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            org.json.JSONArray r56 = new org.json.JSONArray     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r56.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r51
            r1 = r81
            java.lang.Object r84 = r0.get(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r84 = (com.imofan.android.basic.MFStatEvent) r84     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r84.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r56
            org.json.JSONArray r4 = r0.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            boolean r3 = r84.isFirst()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x06e6
            r3 = 1
        L_0x06d9:
            r4.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r55
            r1 = r81
            r2 = r56
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x06ad
        L_0x06e6:
            r3 = 0
            goto L_0x06d9
        L_0x06e8:
            java.lang.String r3 = "event"
            r0 = r49
            r1 = r55
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x06f1:
            java.lang.String r3 = java.lang.Integer.toString(r48)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r47
            r1 = r49
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r69 = r70 + 1
            r70 = r69
            goto L_0x0154
        L_0x0702:
            java.lang.String r3 = "data"
            r0 = r80
            r1 = r47
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x070b:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "update"
            r5 = 0
            r6 = 0
            r8 = 0
            java.util.List r135 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r6, r8)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r135 == 0) goto L_0x076e
            int r3 = r135.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x076e
            org.json.JSONArray r133 = new org.json.JSONArray     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r133.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r64 = 0
            java.util.Iterator r69 = r135.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0729:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x075a
            java.lang.Object r134 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r134 = (com.imofan.android.basic.MFStatEvent) r134     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "app_ver"
            java.lang.String r4 = r134.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            boolean r3 = r3.equals(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0743
            r64 = 1
        L_0x0743:
            java.lang.String r3 = r134.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r133
            r0.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r134.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x0729
        L_0x075a:
            if (r28 == 0) goto L_0x0765
            if (r64 != 0) goto L_0x0765
            java.lang.String r3 = "app_ver"
            r0 = r133
            r0.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0765:
            java.lang.String r3 = "update"
            r0 = r80
            r1 = r133
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x076e:
            java.util.List r21 = com.imofan.android.basic.MFStatAccessPath.getAccessNodes()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r21 == 0) goto L_0x0ac6
            int r3 = r21.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x0ac6
            java.util.HashMap r113 = new java.util.HashMap     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r113.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.HashMap r96 = new java.util.HashMap     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r96.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r68 = 0
        L_0x0786:
            int r3 = r21.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r68
            if (r0 >= r3) goto L_0x08cf
            r0 = r21
            r1 = r68
            java.lang.Object r20 = r0.get(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatAccessPath$Node r20 = (com.imofan.android.basic.MFStatAccessPath.Node) r20     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r20.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0881
            java.lang.String r3 = r20.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r96
            java.lang.Object r85 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatAccessPath$Node r85 = (com.imofan.android.basic.MFStatAccessPath.Node) r85     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r85 == 0) goto L_0x0885
            r85.incCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r3 = r85.getDuration()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r5 = r20.getDuration()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r3 = r3 + r5
            r0 = r85
            r0.setDuration(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x07bd:
            r22 = 0
            if (r68 <= 0) goto L_0x07d1
            int r3 = r68 + -1
            r0 = r21
            java.lang.Object r3 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatAccessPath$Node r3 = (com.imofan.android.basic.MFStatAccessPath.Node) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r3.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x07d3
        L_0x07d1:
            if (r68 != 0) goto L_0x08a4
        L_0x07d3:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = ">"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = r85.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r22 = r3.toString()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x07ea:
            r109 = 1
            r0 = r113
            r1 = r22
            boolean r3 = r0.containsKey(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0806
            r0 = r113
            r1 = r22
            java.lang.Object r3 = r0.get(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3.intValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r109 = r109 + r3
        L_0x0806:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r109)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r113
            r1 = r22
            r0.put(r1, r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r21.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + -1
            r0 = r68
            if (r0 >= r3) goto L_0x082b
            int r3 = r68 + 1
            r0 = r21
            java.lang.Object r3 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatAccessPath$Node r3 = (com.imofan.android.basic.MFStatAccessPath.Node) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r3.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0835
        L_0x082b:
            int r3 = r21.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + -1
            r0 = r68
            if (r0 != r3) goto L_0x0876
        L_0x0835:
            r85.incExit()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = r85.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = ">"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r22 = r3.toString()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r109 = 1
            r0 = r113
            r1 = r22
            boolean r3 = r0.containsKey(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x086b
            r0 = r113
            r1 = r22
            java.lang.Object r3 = r0.get(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3.intValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r109 = r109 + r3
        L_0x086b:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r109)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r113
            r1 = r22
            r0.put(r1, r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0876:
            java.lang.String r3 = r85.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r96
            r1 = r85
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0881:
            int r68 = r68 + 1
            goto L_0x0786
        L_0x0885:
            com.imofan.android.basic.MFStatAccessPath$Node r85 = new com.imofan.android.basic.MFStatAccessPath$Node     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r85.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r20.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r85
            r0.setActivity(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = 1
            r0 = r85
            r0.setCount(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r3 = r20.getDuration()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r85
            r0.setDuration(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x07bd
        L_0x08a4:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r4.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r68 + -1
            r0 = r21
            java.lang.Object r3 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatAccessPath$Node r3 = (com.imofan.android.basic.MFStatAccessPath.Node) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r3.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = ">"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = r85.getActivity()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r22 = r3.toString()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x07ea
        L_0x08cf:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "path"
            r5 = 0
            r6 = 0
            r8 = 0
            java.util.List r111 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r6, r8)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r111 == 0) goto L_0x0933
            int r3 = r111.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x0933
            java.util.Iterator r69 = r111.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x08e6:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0933
            java.lang.Object r110 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r110 = (com.imofan.android.basic.MFStatEvent) r110     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r110.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r113
            boolean r3 = r0.containsKey(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0921
            java.lang.String r4 = r110.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r110.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r113
            java.lang.Object r3 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3.intValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r5 = r110.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + r5
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r113
            r0.put(r4, r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x08e6
        L_0x0921:
            java.lang.String r3 = r110.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r4 = r110.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r113
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x08e6
        L_0x0933:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "node"
            r5 = 0
            r6 = 0
            r8 = 0
            java.util.List r95 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r6, r8)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r95 == 0) goto L_0x09d0
            int r3 = r95.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x09d0
            java.util.Iterator r69 = r95.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x094a:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x09d0
            java.lang.Object r94 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r94 = (com.imofan.android.basic.MFStatEvent) r94     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r94.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r96
            java.lang.Object r85 = r0.get(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatAccessPath$Node r85 = (com.imofan.android.basic.MFStatAccessPath.Node) r85     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r85 == 0) goto L_0x099a
            int r3 = r85.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r4 = r94.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + r4
            r0 = r85
            r0.setCount(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r85.getExit()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r4 = r94.getExit()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + r4
            r0 = r85
            r0.setExit(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r3 = r85.getDuration()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r5 = r94.getDuration()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r3 = r3 + r5
            r0 = r85
            r0.setDuration(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r94.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r96
            r1 = r85
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x094a
        L_0x099a:
            com.imofan.android.basic.MFStatAccessPath$Node r85 = new com.imofan.android.basic.MFStatAccessPath$Node     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r85.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r94.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r85
            r0.setActivity(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r94.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r85
            r0.setCount(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r94.getExit()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r85
            r0.setExit(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r3 = r94.getDuration()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r85
            r0.setDuration(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r94.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r96
            r1 = r85
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x094a
        L_0x09d0:
            java.lang.String r3 = "user_activities"
            java.lang.String r4 = ""
            com.imofan.android.basic.MFStatInfo.putString(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent.clearAccessPath(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r113.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x0a3a
            org.json.JSONObject r114 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r114.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Set r3 = r113.keySet()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r112 = r3.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x09ef:
            boolean r3 = r112.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0a31
            java.lang.Object r7 = r112.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "."
            int r3 = r7.lastIndexOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + 1
            java.lang.String r3 = r7.substring(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r113
            java.lang.Object r4 = r0.get(r7)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r114
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "path"
            r5 = 0
            r5 = r74[r5]     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r6 = 1
            r6 = r74[r6]     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r113
            java.lang.Object r8 = r0.get(r7)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r8 = (java.lang.Integer) r8     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r8 = r8.intValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r9 = 0
            r11 = 0
            long r12 = com.imofan.android.basic.Mofang.resumeTime     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent.addEvent(r3, r4, r5, r6, r7, r8, r9, r11, r12)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x09ef
        L_0x0a31:
            java.lang.String r3 = "path"
            r0 = r80
            r1 = r114
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0a3a:
            int r3 = r96.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x0ac6
            org.json.JSONObject r107 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r107.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Set r3 = r96.keySet()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r106 = r3.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0a4d:
            boolean r3 = r106.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0abd
            java.lang.Object r12 = r106.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r96
            java.lang.Object r93 = r0.get(r12)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatAccessPath$Node r93 = (com.imofan.android.basic.MFStatAccessPath.Node) r93     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            org.json.JSONArray r105 = new org.json.JSONArray     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r105.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r93.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r105
            r0.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r93.getExit()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r105
            r0.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r3 = r93.getDuration()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            double r3 = (double) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r5 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r3 = r3 / r5
            long r3 = java.lang.Math.round(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r105
            r0.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "."
            int r3 = r12.lastIndexOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r3 + 1
            java.lang.String r3 = r12.substring(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r107
            r1 = r105
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFDatabaseManager r8 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r9 = "node"
            r3 = 0
            r10 = r74[r3]     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = 1
            r11 = r74[r3]     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r13 = r93.getCount()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r14 = r93.getExit()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r15 = r93.getDuration()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r17 = 0
            long r18 = com.imofan.android.basic.Mofang.resumeTime     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent.addEvent(r8, r9, r10, r11, r12, r13, r14, r15, r17, r18)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x0a4d
        L_0x0abd:
            java.lang.String r3 = "page"
            r0 = r80
            r1 = r107
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0ac6:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "crash"
            r5 = 0
            r6 = 0
            r8 = 0
            java.util.List r41 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r6, r8)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.HashMap r42 = new java.util.HashMap     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r42.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r41 == 0) goto L_0x0b8e
            int r3 = r41.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x0b8e
            java.util.Iterator r69 = r41.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0ae2:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0b8e
            java.lang.Object r59 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r59 = (com.imofan.android.basic.MFStatEvent) r59     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            org.json.JSONObject r61 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r59.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r61
            r0.<init>(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "name"
            r0 = r61
            java.lang.String r62 = r0.optString(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "stack"
            r0 = r61
            java.lang.String r63 = r0.optString(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "MD5"
            java.security.MessageDigest r89 = java.security.MessageDigest.getInstance(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "utf-8"
            r0 = r63
            byte[] r3 = r0.getBytes(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r89
            byte[] r87 = r0.digest(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.StringBuilder r90 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r90.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r29 = r87
            r0 = r29
            int r0 = r0.length     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r82 = r0
            r70 = 0
        L_0x0b2b:
            r0 = r70
            r1 = r82
            if (r0 >= r1) goto L_0x0b43
            byte r30 = r29[r70]     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r30
            r3 = r0 & 255(0xff, float:3.57E-43)
            java.lang.String r3 = java.lang.Integer.toHexString(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r90
            r0.append(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r70 = r70 + 1
            goto L_0x0b2b
        L_0x0b43:
            java.lang.String r3 = r90.toString()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r86 = r3.toLowerCase()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r42
            r1 = r86
            java.lang.Object r40 = r0.get(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.Mofang$CrashData r40 = (com.imofan.android.basic.Mofang.CrashData) r40     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r40 != 0) goto L_0x0b6d
            com.imofan.android.basic.Mofang$CrashData r40 = new com.imofan.android.basic.Mofang$CrashData     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r3 = 0
            r0 = r40
            r0.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r40
            r1 = r62
            r0.setName(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r40
            r1 = r63
            r0.setStack(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0b6d:
            long r3 = r59.getTime()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r40
            r0.addTime(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r42
            r1 = r86
            r2 = r40
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r59.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x0ae2
        L_0x0b8e:
            int r3 = r42.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x0c13
            org.json.JSONObject r39 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r39.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Set r3 = r42.keySet()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r43 = r3.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0ba1:
            boolean r3 = r43.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0c0a
            java.lang.Object r86 = r43.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r86 = (java.lang.String) r86     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r42
            r1 = r86
            java.lang.Object r40 = r0.get(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.Mofang$CrashData r40 = (com.imofan.android.basic.Mofang.CrashData) r40     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            org.json.JSONObject r44 = new org.json.JSONObject     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r44.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "name"
            java.lang.String r4 = r40.getName()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r44
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = "stack"
            java.lang.String r4 = r40.getStack()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r44
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            org.json.JSONArray r132 = new org.json.JSONArray     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r132.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.List r3 = r40.getTimeList()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r69 = r3.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0bdf:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0bf7
            java.lang.Object r3 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            long r45 = r3.longValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r132
            r1 = r45
            r0.put(r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x0bdf
        L_0x0bf7:
            java.lang.String r3 = "time"
            r0 = r44
            r1 = r132
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r39
            r1 = r86
            r2 = r44
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x0ba1
        L_0x0c0a:
            java.lang.String r3 = "crash"
            r0 = r80
            r1 = r39
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0c13:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "notification_receive"
            r5 = 0
            r6 = 0
            r8 = 0
            java.util.List r99 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r6, r8)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r99 == 0) goto L_0x0c79
            int r3 = r99.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x0c79
            org.json.JSONArray r118 = new org.json.JSONArray     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r118.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.HashMap r119 = new java.util.HashMap     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r119.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r69 = r99.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0c34:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0c70
            java.lang.Object r97 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r97 = (com.imofan.android.basic.MFStatEvent) r97     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r97.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r119
            boolean r3 = r0.containsKey(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 != 0) goto L_0x0c34
            java.lang.String r3 = r97.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r118
            r0.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r97.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = r97.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r119
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r97.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x0c34
        L_0x0c70:
            java.lang.String r3 = "push_recv"
            r0 = r80
            r1 = r118
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0c79:
            com.imofan.android.basic.MFDatabaseManager r3 = com.imofan.android.basic.Mofang.dbManager     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = "notification_open"
            r5 = 0
            r6 = 0
            r8 = 0
            java.util.List r98 = com.imofan.android.basic.MFStatEvent.getEvents(r3, r4, r5, r6, r8)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r98 == 0) goto L_0x042c
            int r3 = r98.size()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 <= 0) goto L_0x042c
            org.json.JSONArray r116 = new org.json.JSONArray     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r116.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.HashMap r117 = new java.util.HashMap     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r117.<init>()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.util.Iterator r69 = r98.iterator()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
        L_0x0c9a:
            boolean r3 = r69.hasNext()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 == 0) goto L_0x0cd6
            java.lang.Object r97 = r69.next()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            com.imofan.android.basic.MFStatEvent r97 = (com.imofan.android.basic.MFStatEvent) r97     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r97.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r117
            boolean r3 = r0.containsKey(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            if (r3 != 0) goto L_0x0c9a
            java.lang.String r3 = r97.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r116
            r0.put(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r3 = r97.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.String r4 = r97.getValue()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r117
            r0.put(r3, r4)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            int r3 = r97.getId()     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            r0 = r60
            r0.add(r3)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x0c9a
        L_0x0cd6:
            java.lang.String r3 = "push_open"
            r0 = r80
            r1 = r116
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x041b, NoSuchAlgorithmException -> 0x0469, UnsupportedEncodingException -> 0x049e }
            goto L_0x042c
        L_0x0ce1:
            r3 = move-exception
            goto L_0x037e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imofan.android.basic.Mofang.generateSendData(android.content.Context, long):com.imofan.android.basic.Mofang$SendData");
    }

    private static int[] getDateAndHour(long time) {
        Date today = new Date(time);
        return new int[]{Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(today)), Integer.parseInt(new SimpleDateFormat("H").format(today))};
    }

    private static class SendData {
        private List<Integer> idList;
        private JSONObject json;

        private SendData() {
        }

        public JSONObject getJson() {
            return this.json;
        }

        public void setJson(JSONObject json2) {
            this.json = json2;
        }

        public List<Integer> getIdList() {
            return this.idList;
        }

        public void setIdList(List<Integer> idList2) {
            this.idList = idList2;
        }
    }

    private static class CrashData {
        private String name;
        private String stack;
        private List<Long> timeList;

        private CrashData() {
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public String getStack() {
            return this.stack;
        }

        public void setStack(String stack2) {
            this.stack = stack2;
        }

        public List<Long> getTimeList() {
            return this.timeList;
        }

        public synchronized void addTime(long time) {
            if (this.timeList == null) {
                this.timeList = new ArrayList();
            }
            this.timeList.add(Long.valueOf(Math.round(((double) time) / 1000.0d)));
        }
    }

    public static void main(String[] args) {
        System.out.println("[updateAccessDuration] id = 719155321, accessPath = " + "");
        String str2 = ";719155321,";
        System.out.println("str2 = " + str2);
        String[] str1And3456 = "".split(str2);
        System.out.println("str1And3456.length = " + str1And3456.length);
        System.out.println("str1And3456[0] = " + str1And3456[0]);
        if (str1And3456.length == 1) {
            str2 = "719155321,";
            str1And3456 = new String[]{"", "".substring("".indexOf(",") + 1)};
        }
        System.out.println("str1And3456[1] = " + str1And3456[1]);
        String str1 = str1And3456[0];
        System.out.println("str1 = " + str1);
        System.out.println("str2 = " + str2);
        String str3 = str1And3456[1].substring(0, str1And3456[1].indexOf(","));
        System.out.println("str3 = " + str3);
        System.out.println("str4 = " + ",");
        String str56 = str1And3456[1].substring(str1And3456[1].indexOf(",") + 1);
        String str5 = Long.toString(555);
        System.out.println("str5 = " + str5);
        String str6 = "";
        if (str56.indexOf(";") > 0) {
            str6 = str56.substring(str56.indexOf(";"));
        }
        System.out.println("str6 = " + str6);
        System.out.println("accessPath = " + (str1 + str2 + str3 + "," + str5 + str6));
    }
}
