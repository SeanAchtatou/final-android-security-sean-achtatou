package com.imofan.android.basic.update;

import android.app.Activity;

public interface MFUpdateListener {
    void onDetectedNewVersion(Activity activity, int i, String str, String str2, String str3);

    void onDetectedNothing(Activity activity);

    void onFailed(Activity activity);

    void onPerformUpdate(Activity activity, int i, String str, String str2, String str3);

    void onWifiOff(Activity activity);
}
