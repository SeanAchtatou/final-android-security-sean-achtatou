package com.imofan.android.basic.feedback;

public class MFFeedback {
    public static final int USER_DEVELOPER = 1;
    public static final int USER_NORMAL = 0;
    private String feedback;
    private String timestamp;
    private String userInfo;
    private int userType = 0;

    public MFFeedback(String feedback2) {
        this.feedback = feedback2;
    }

    public int getUserType() {
        return this.userType;
    }

    public void setUserType(int userType2) {
        this.userType = userType2;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp2) {
        this.timestamp = timestamp2;
    }

    public String getFeedback() {
        return this.feedback;
    }

    public void setFeedback(String feedback2) {
        this.feedback = feedback2;
    }

    public String getUserInfo() {
        return this.userInfo;
    }

    public void setUserInfo(String userInfo2) {
        this.userInfo = userInfo2;
    }
}
