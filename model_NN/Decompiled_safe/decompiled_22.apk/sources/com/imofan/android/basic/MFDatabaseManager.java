package com.imofan.android.basic;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MFDatabaseManager extends SQLiteOpenHelper {
    private static final String DATABASE = "mofang_data_analysis.db";
    private static final String INIT_SQL = "CREATE TABLE IF NOT EXISTS event (id INTEGER PRIMARY KEY, date INTEGER NOT NULL, hour INTEGER NOT NULL, event TEXT NOT NULL, value TEXT, count INTEGER DEFAULT 1, exit INTEGER DEFAULT 0, duration INTEGER DEFAULT 0, first INTEGER DEFAULT 0, time NUMERIC NOT NULL, send INTEGER DEFAULT 0);";
    private static final String LOG_TAG = "Mofang_MFDatabaseHelper";
    private static final String UPGRADE_SQL = null;
    private static final int VERSION = 1;
    private static MFDatabaseManager self = null;

    private MFDatabaseManager(Context context) {
        super(context, DATABASE, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public static synchronized MFDatabaseManager getInstance(Context context) {
        MFDatabaseManager mFDatabaseManager;
        synchronized (MFDatabaseManager.class) {
            if (self == null && context != null) {
                self = new MFDatabaseManager(context);
            }
            mFDatabaseManager = self;
        }
        return mFDatabaseManager;
    }

    public void onCreate(SQLiteDatabase database) {
        String[] arr$ = INIT_SQL.split(";");
        int len$ = arr$.length;
        for (int i$ = 0; i$ < len$; i$++) {
            database.execSQL(arr$[i$] + ";");
        }
    }

    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        String[] arr$ = UPGRADE_SQL.split(";");
        int len$ = arr$.length;
        for (int i$ = 0; i$ < len$; i$++) {
            database.execSQL(arr$[i$] + ";");
        }
        onCreate(database);
    }
}
