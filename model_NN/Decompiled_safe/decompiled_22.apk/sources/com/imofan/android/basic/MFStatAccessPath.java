package com.imofan.android.basic;

import java.util.ArrayList;
import java.util.List;

class MFStatAccessPath {
    private static final String LOG_TAG = "Mofang_MFStatAccessPath";

    MFStatAccessPath() {
    }

    static void addAccessNode(int id, String node, long duration) {
        String accessPath;
        String node2;
        String accessPath2 = MFStatInfo.getString("user_activities", null);
        if (accessPath2 == null || accessPath2.trim().length() == 0) {
            accessPath = "";
        } else {
            accessPath = accessPath2 + ";";
        }
        if (node == null || node.trim().length() <= 0) {
            node2 = "Exit";
        } else {
            node2 = node.trim().replaceAll(",", "\\co\\").replaceAll(";", "\\se\\").replaceAll("Exit", "\\Exit\\");
        }
        MFStatInfo.putString("user_activities", accessPath + id + "," + node2 + "," + duration);
    }

    static void addAccessNode(int id, String node) {
        addAccessNode(id, node, 0);
    }

    static void updateAccessDuration(int id, long duration) {
        String accessPath = MFStatInfo.getString("user_activities", null);
        if (accessPath != null && accessPath.trim().length() != 0) {
            String str2 = ";" + id + ",";
            String[] str1And3456 = accessPath.split(str2);
            if (str1And3456.length == 1) {
                str2 = id + ",";
                str1And3456 = new String[]{"", accessPath.substring(accessPath.indexOf(",") + 1)};
            }
            String str1 = str1And3456[0];
            String str3 = str1And3456[1].substring(0, str1And3456[1].indexOf(","));
            String str56 = str1And3456[1].substring(str1And3456[1].indexOf(",") + 1);
            String str5 = Long.toString(duration);
            String str6 = "";
            if (str56.indexOf(";") > 0) {
                str6 = str56.substring(str56.indexOf(";"));
            }
            MFStatInfo.putString("user_activities", str1 + str2 + str3 + "," + str5 + str6);
        }
    }

    static List<Node> getAccessNodes() {
        List<Node> accessNodes = null;
        String accessPath = MFStatInfo.getString("user_activities", null);
        if (accessPath != null && accessPath.trim().length() > 0) {
            accessNodes = new ArrayList<>();
            for (String path : accessPath.split(";")) {
                if (path.trim().length() > 0) {
                    Node accessNode = new Node();
                    String[] info = path.trim().split(",");
                    if (!info[1].equals("Exit")) {
                        info[1] = info[1].replaceAll("\\\\co\\\\", ",").replaceAll("\\\\se\\\\", ";").replaceAll("\\\\Exit\\\\", "Exit");
                        accessNode.setActivity(info[1]);
                    }
                    accessNode.setDuration(Long.parseLong(info[2]));
                    accessNodes.add(accessNode);
                }
            }
        }
        return accessNodes;
    }

    static class Node {
        private static final String EXIT_NODE = "Exit";
        private String activity;
        private int count;
        private long duration;
        private int exit;

        Node() {
        }

        public String getActivity() {
            return this.activity;
        }

        public void setActivity(String activity2) {
            this.activity = activity2;
        }

        public void incCount() {
            this.count++;
        }

        public int getCount() {
            return this.count;
        }

        public void setCount(int count2) {
            this.count = count2;
        }

        public void incExit() {
            this.exit++;
        }

        public int getExit() {
            return this.exit;
        }

        public void setExit(int exit2) {
            this.exit = exit2;
        }

        public long getDuration() {
            return this.duration;
        }

        public void setDuration(long duration2) {
            this.duration = duration2;
        }
    }
}
