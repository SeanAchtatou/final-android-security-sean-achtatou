package com.imofan.android.basic.notification;

import android.content.Context;

public interface MFNotificationListener {
    void onNotificationReceived(Context context, String str, String str2);
}
