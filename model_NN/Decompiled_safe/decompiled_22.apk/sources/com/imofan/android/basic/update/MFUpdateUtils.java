package com.imofan.android.basic.update;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.tauth.Constants;
import java.io.File;
import java.text.SimpleDateFormat;
import org.json.JSONObject;

public class MFUpdateUtils {
    protected static MFAPPInfo getVersionInfo(Context context) {
        MFAPPInfo mfappInfo = null;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 16384);
            MFAPPInfo mfappInfo2 = new MFAPPInfo();
            try {
                mfappInfo2.setPackageName(packageInfo.packageName);
                mfappInfo2.setVersionCode(packageInfo.versionCode);
                mfappInfo2.setVersionName(packageInfo.versionName);
                return mfappInfo2;
            } catch (PackageManager.NameNotFoundException e) {
                e = e;
                mfappInfo = mfappInfo2;
                e.printStackTrace();
                return mfappInfo;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e = e2;
            e.printStackTrace();
            return mfappInfo;
        }
    }

    protected static MFUpdateAPKInfo checkNewVersion(String checkUrl) {
        try {
            String json = MFHttpUtils.invokeText(checkUrl);
            if (json == null || json.trim().length() <= 0) {
                return null;
            }
            MFUpdateAPKInfo mfInfo = new MFUpdateAPKInfo();
            try {
                JSONObject jsonObject = new JSONObject(json);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String time = jsonObject.optString("time");
                if (time != null && !"".equals(time)) {
                    mfInfo.setTime(dateFormat.parse(jsonObject.optString("time")));
                }
                mfInfo.setVersionCode(jsonObject.getInt("versionCode"));
                mfInfo.setVersionName(jsonObject.getString("versionName"));
                mfInfo.setDescription(jsonObject.getString(Constants.PARAM_COMMENT));
                mfInfo.setApkPath(jsonObject.getString(Constants.PARAM_URL));
                return mfInfo;
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return null;
        }
    }

    protected static int getIdByReflection(Context context, String className, String fieldName) {
        try {
            return Class.forName(String.valueOf(context.getPackageName()) + ".R$" + className).getField(fieldName).getInt(fieldName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return 0;
        } catch (SecurityException e2) {
            e2.printStackTrace();
            return 0;
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
            return 0;
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
            return 0;
        } catch (NoSuchFieldException e5) {
            e5.printStackTrace();
            return 0;
        }
    }

    protected static View createUpdateDialog(Activity activity, MFUpdateAPKInfo updateInfo) {
        LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(12, 5, 12, 0);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(-1, -1);
        TextView textView = new TextView(activity);
        textView.setPadding(5, 0, 5, 0);
        textView.setText(replaceBr(updateInfo.getDescription()));
        textView.setTextSize(2, 18.0f);
        textView.setTextColor(-1);
        linearLayout.addView(textView, param);
        return linearLayout;
    }

    public static String replaceBr(String str) {
        if (str == null || "".equals(str)) {
            return "";
        }
        return str.replaceAll("(?i)<br>", "\n").replace("，", ",");
    }

    protected static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    protected static boolean delete(File file) {
        if (!file.exists()) {
            return false;
        }
        if (file.isFile()) {
            return deleteFile(file);
        }
        return deleteDirectory(file, true);
    }

    private static boolean deleteDirectory(File dirFile, boolean includeSelf) {
        return deleteDirectory(dirFile, null, includeSelf, false);
    }

    private static boolean deleteDirectory(File dirFile, String extension, boolean includeSelf, boolean onlyFile) {
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        boolean flag = true;
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (!files[i].isFile()) {
                if (!onlyFile && !(flag = deleteDirectory(files[i], true))) {
                    break;
                }
            } else if ((extension == null || files[i].getName().toLowerCase().endsWith("." + extension.toLowerCase())) && !(flag = deleteFile(files[i]))) {
                break;
            }
        }
        if (!flag) {
            return false;
        }
        if (!includeSelf) {
            return true;
        }
        if (dirFile.delete()) {
            return true;
        }
        return false;
    }

    private static boolean deleteFile(File file) {
        if (!file.isFile() || !file.exists()) {
            return false;
        }
        file.delete();
        return true;
    }
}
