package com.imofan.android.basic.feedback;

import android.app.Activity;
import android.content.SharedPreferences;
import com.city_life.part_asynctask.UploadUtils;
import com.imofan.android.basic.Mofang;
import com.tencent.tauth.Constants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MFFeedbackService {
    private static final String ACCEPT_ENCODING = "gzip, deflate";
    private static int CONNECT_TIMEOUT = 20;
    private static int DATA_TIMEOUT = 40;
    private static String FEEDBACK_URL = "http://m.imofan.com/feedbacks/?app_key=";
    private static final String SP_FILE_NAME = "mffeedbacks";
    private static final int SUBMIT_FAILED = -1;
    private static String UUID_URL = "http://m.imofan.com/uuid.jsp";

    public static void submit(Activity activity, MFFeedback feedback, MFFeedbackSubmitListener feedbackSubmitListener) {
        final Activity activity1 = activity;
        final MFFeedback feedBack1 = feedback;
        final MFFeedbackSubmitListener feedbackSubmitListener1 = feedbackSubmitListener;
        new Thread(new Runnable() {
            public void run() {
                if (activity1 == null || feedBack1 == null || feedBack1.getFeedback().length() > 500 || feedbackSubmitListener1 == null) {
                    feedbackSubmitListener1.onSubmitFailed();
                    return;
                }
                String backJson = MFFeedbackService.getSubmitResponseWithPost(activity1, feedBack1);
                if ("".equals(backJson) || backJson == null) {
                    feedbackSubmitListener1.onSubmitFailed();
                    return;
                }
                Map<String, String> maps = MFFeedbackService.getSubmitBackInfo(backJson);
                if (maps == null) {
                    feedbackSubmitListener1.onSubmitFailed();
                } else if (Integer.parseInt((String) maps.get("status")) == -1) {
                    feedbackSubmitListener1.onSubmitFailed();
                } else {
                    MFFeedbackService.update(activity1, new MFFeedbackReplyListener() {
                        public void onDetectedNothing() {
                        }

                        public void onDetectedNewReplies(List<MFFeedback> list) {
                        }
                    });
                    feedbackSubmitListener1.onSubmitSucceeded(feedBack1);
                }
            }
        }).start();
    }

    public static List<MFFeedback> getLocalAllReply(Activity activity) {
        List<MFFeedback> list = new ArrayList<>();
        String json = activity.getSharedPreferences(SP_FILE_NAME, 0).getString("json", null);
        if (json != null) {
            try {
                JSONArray jsonArray = new JSONObject(json).getJSONArray("feedbacks");
                for (int i = 0; i < jsonArray.length(); i++) {
                    MFFeedback fd = new MFFeedback(jsonArray.getJSONObject(i).getString("feedback"));
                    fd.setUserType(jsonArray.getJSONObject(i).getInt("by"));
                    fd.setTimestamp(jsonArray.getJSONObject(i).getString("timestamp"));
                    list.add(fd);
                }
            } catch (JSONException e) {
                printErr(e);
            }
        }
        return list;
    }

    public static void update(Activity activity, MFFeedbackReplyListener feedbackReplyListener) {
        final Activity activity1 = activity;
        final MFFeedbackReplyListener feedbackReplyListener1 = feedbackReplyListener;
        new Thread(new Runnable() {
            public void run() {
                if (activity1 == null || feedbackReplyListener1 == null) {
                    feedbackReplyListener1.onDetectedNothing();
                    return;
                }
                Activity activity = activity1;
                Activity activity2 = activity1;
                String backJson = MFFeedbackService.getFbRspWithGet(activity1, activity.getSharedPreferences(MFFeedbackService.SP_FILE_NAME, 0).getString("timestamp", UploadUtils.SUCCESS));
                if ("".equals(backJson) || backJson == null) {
                    feedbackReplyListener1.onDetectedNothing();
                    return;
                }
                List<MFFeedback> list = MFFeedbackService.getUpdateBackInfo(backJson);
                if (list == null) {
                    feedbackReplyListener1.onDetectedNothing();
                    return;
                }
                MFFeedbackService.saveReply(activity1, backJson);
                feedbackReplyListener1.onDetectedNewReplies(list);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public static Map<String, String> getSubmitBackInfo(String json) {
        Map<String, String> maps = new HashMap<>();
        try {
            JSONObject jsonObj = new JSONObject(json);
            maps.put("status", jsonObj.getString("status"));
            maps.put(Constants.PARAM_APP_DESC, jsonObj.getString(Constants.PARAM_APP_DESC));
            maps.put("at", jsonObj.getString("at"));
            if (maps.isEmpty()) {
                maps = null;
            }
            return maps;
        } catch (JSONException e) {
            printErr(e);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static List<MFFeedback> getUpdateBackInfo(String json) {
        List<MFFeedback> list = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONObject(json).getJSONArray("feedbacks");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json1 = jsonArray.getJSONObject(i);
                MFFeedback fb = new MFFeedback(json1.getString("text"));
                fb.setUserType(json1.getInt("by"));
                fb.setTimestamp(json1.getString("at"));
                list.add(fb);
            }
            if (list.isEmpty()) {
                list = null;
            }
            return list;
        } catch (JSONException e) {
            printErr(e);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void saveReply(Activity activity, String json) {
        SharedPreferences preferences = activity.getSharedPreferences(SP_FILE_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        List<MFFeedback> list = getUpdateBackInfo(json);
        editor.putString("timestamp", list.get(list.size() - 1).getTimestamp());
        editor.commit();
        if (preferences.getString("json", null) == null) {
            try {
                JSONObject jsonObj = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < list.size(); i++) {
                    JSONObject jo = new JSONObject();
                    jo.put("by", list.get(i).getUserType());
                    jo.put("feedback", list.get(i).getFeedback());
                    jo.put("timestamp", list.get(i).getTimestamp());
                    jsonArray.put(jo);
                }
                jsonObj.put("feedbacks", jsonArray);
                editor.putString("json", jsonObj.toString());
                editor.commit();
            } catch (JSONException e) {
                printErr(e);
            }
        } else {
            try {
                JSONObject jsonObj2 = new JSONObject(preferences.getString("json", null));
                JSONArray jsonArray2 = jsonObj2.getJSONArray("feedbacks");
                for (int i2 = 0; i2 < list.size(); i2++) {
                    JSONObject jo2 = new JSONObject();
                    jo2.put("by", list.get(i2).getUserType());
                    jo2.put("feedback", list.get(i2).getFeedback());
                    jo2.put("timestamp", list.get(i2).getTimestamp());
                    jsonArray2.put(jo2);
                }
                editor.putString("json", jsonObj2.toString());
                editor.commit();
            } catch (JSONException e2) {
                printErr(e2);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0109 A[SYNTHETIC, Splitter:B:22:0x0109] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x010e A[Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getSubmitResponseWithPost(android.app.Activity r18, com.imofan.android.basic.feedback.MFFeedback r19) {
        /*
            com.imofan.android.basic.Mofang.init(r18)
            java.lang.String r0 = com.imofan.android.basic.feedback.MFFeedbackService.FEEDBACK_URL
            org.apache.http.params.BasicHttpParams r8 = new org.apache.http.params.BasicHttpParams
            r8.<init>()
            int r14 = com.imofan.android.basic.feedback.MFFeedbackService.CONNECT_TIMEOUT
            int r14 = r14 * 1000
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r8, r14)
            int r14 = com.imofan.android.basic.feedback.MFFeedbackService.DATA_TIMEOUT
            int r14 = r14 * 1000
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r8, r14)
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient
            r4.<init>(r8)
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.StringBuilder r14 = r14.append(r0)
            java.lang.String r15 = com.imofan.android.basic.Mofang.getAppKey(r18)
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r0 = r14.toString()
            org.apache.http.client.methods.HttpPost r10 = new org.apache.http.client.methods.HttpPost
            r10.<init>(r0)
            java.lang.String r14 = "Accept-Encoding"
            java.lang.String r15 = "gzip, deflate"
            r10.setHeader(r14, r15)
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            org.apache.http.message.BasicNameValuePair r14 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r15 = "dev_id"
            java.lang.String r16 = getUUID(r18)
            r14.<init>(r15, r16)
            r9.add(r14)
            org.apache.http.message.BasicNameValuePair r14 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r15 = "app_ver"
            java.lang.String r16 = "app_ver"
            java.lang.String r17 = ""
            java.lang.String r16 = com.imofan.android.basic.MFStatInfo.getString(r16, r17)
            r14.<init>(r15, r16)
            r9.add(r14)
            org.apache.http.message.BasicNameValuePair r14 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r15 = "os_ver"
            java.lang.String r16 = "os_ver"
            java.lang.String r17 = ""
            java.lang.String r16 = com.imofan.android.basic.MFStatInfo.getString(r16, r17)
            r14.<init>(r15, r16)
            r9.add(r14)
            org.apache.http.message.BasicNameValuePair r14 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r15 = "model"
            java.lang.String r16 = "model"
            java.lang.String r17 = ""
            java.lang.String r16 = com.imofan.android.basic.MFStatInfo.getString(r16, r17)
            r14.<init>(r15, r16)
            r9.add(r14)
            org.apache.http.message.BasicNameValuePair r14 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r15 = "user_info"
            java.lang.String r16 = r19.getUserInfo()
            r14.<init>(r15, r16)
            r9.add(r14)
            org.apache.http.message.BasicNameValuePair r14 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r15 = "text"
            java.lang.String r16 = r19.getFeedback()
            r14.<init>(r15, r16)
            r9.add(r14)
            org.apache.http.client.entity.UrlEncodedFormEntity r14 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            java.lang.String r15 = "UTF-8"
            r14.<init>(r9, r15)     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            r10.setEntity(r14)     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            org.apache.http.HttpResponse r13 = r4.execute(r10)     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            org.apache.http.StatusLine r14 = r13.getStatusLine()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            int r14 = r14.getStatusCode()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            r15 = 200(0xc8, float:2.8E-43)
            if (r14 != r15) goto L_0x0133
            org.apache.http.HttpEntity r14 = r13.getEntity()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            java.io.InputStream r5 = r14.getContent()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            if (r5 != 0) goto L_0x00c9
            r14 = 0
        L_0x00c8:
            return r14
        L_0x00c9:
            java.lang.String r14 = "Content-Encoding"
            org.apache.http.Header r2 = r13.getFirstHeader(r14)     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            if (r2 == 0) goto L_0x00e3
            java.lang.String r14 = r2.getValue()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            java.lang.String r15 = "gzip"
            boolean r14 = r14.equalsIgnoreCase(r15)     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            if (r14 == 0) goto L_0x00e3
            java.util.zip.GZIPInputStream r6 = new java.util.zip.GZIPInputStream     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            r6.<init>(r5)     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            r5 = r6
        L_0x00e3:
            r11 = 0
            java.lang.String r7 = ""
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            r1.<init>()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            java.io.BufferedReader r12 = new java.io.BufferedReader     // Catch:{ all -> 0x0135 }
            java.io.InputStreamReader r14 = new java.io.InputStreamReader     // Catch:{ all -> 0x0135 }
            r14.<init>(r5)     // Catch:{ all -> 0x0135 }
            r12.<init>(r14)     // Catch:{ all -> 0x0135 }
        L_0x00f5:
            java.lang.String r7 = r12.readLine()     // Catch:{ all -> 0x0105 }
            if (r7 == 0) goto L_0x0118
            java.lang.StringBuffer r14 = r1.append(r7)     // Catch:{ all -> 0x0105 }
            java.lang.String r15 = "\r\n"
            r14.append(r15)     // Catch:{ all -> 0x0105 }
            goto L_0x00f5
        L_0x0105:
            r14 = move-exception
            r11 = r12
        L_0x0107:
            if (r11 == 0) goto L_0x010c
            r11.close()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
        L_0x010c:
            if (r5 == 0) goto L_0x0111
            r5.close()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
        L_0x0111:
            throw r14     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
        L_0x0112:
            r3 = move-exception
            printErr(r3)
            r14 = 0
            goto L_0x00c8
        L_0x0118:
            if (r12 == 0) goto L_0x011d
            r12.close()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
        L_0x011d:
            if (r5 == 0) goto L_0x0122
            r5.close()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
        L_0x0122:
            java.lang.String r14 = r1.toString()     // Catch:{ UnsupportedEncodingException -> 0x0112, ClientProtocolException -> 0x0127, IOException -> 0x012d }
            goto L_0x00c8
        L_0x0127:
            r3 = move-exception
            printErr(r3)
            r14 = 0
            goto L_0x00c8
        L_0x012d:
            r3 = move-exception
            printErr(r3)
            r14 = 0
            goto L_0x00c8
        L_0x0133:
            r14 = 0
            goto L_0x00c8
        L_0x0135:
            r14 = move-exception
            goto L_0x0107
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imofan.android.basic.feedback.MFFeedbackService.getSubmitResponseWithPost(android.app.Activity, com.imofan.android.basic.feedback.MFFeedback):java.lang.String");
    }

    private static String getUUID(Activity activity) {
        SharedPreferences preferences = activity.getSharedPreferences(SP_FILE_NAME, 0);
        String UUID = preferences.getString("UUID", null);
        if (UUID != null) {
            return UUID;
        }
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(UUID_URL).openConnection();
            conn.setRequestMethod("GET");
            conn.setReadTimeout(5000);
            String UUID2 = new BufferedReader(new InputStreamReader(conn.getInputStream())).readLine();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("UUID", UUID2);
            editor.commit();
            return UUID2;
        } catch (MalformedURLException e) {
            printErr(e);
            return null;
        } catch (IOException e2) {
            printErr(e2);
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00be A[SYNTHETIC, Splitter:B:28:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00de A[SYNTHETIC, Splitter:B:43:0x00de] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00ea A[SYNTHETIC, Splitter:B:49:0x00ea] */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:66:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x00b8=Splitter:B:24:0x00b8, B:39:0x00d8=Splitter:B:39:0x00d8} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getFbRspWithGet(android.app.Activity r16, java.lang.String r17) {
        /*
            com.imofan.android.basic.Mofang.init(r16)
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = com.imofan.android.basic.feedback.MFFeedbackService.FEEDBACK_URL
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = com.imofan.android.basic.Mofang.getAppKey(r16)
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = "&dev_id="
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = getUUID(r16)
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r15 = "&ts="
            java.lang.StringBuilder r14 = r14.append(r15)
            r0 = r17
            java.lang.StringBuilder r14 = r14.append(r0)
            java.lang.String r1 = r14.toString()
            java.lang.StringBuffer r13 = new java.lang.StringBuffer
            r13.<init>()
            org.apache.http.client.methods.HttpGet r11 = new org.apache.http.client.methods.HttpGet
            r11.<init>(r1)
            java.lang.String r14 = "Accept-Encoding"
            java.lang.String r15 = "gzip, deflate"
            r11.addHeader(r14, r15)
            org.apache.http.params.BasicHttpParams r8 = new org.apache.http.params.BasicHttpParams
            r8.<init>()
            int r14 = com.imofan.android.basic.feedback.MFFeedbackService.CONNECT_TIMEOUT
            int r14 = r14 * 1000
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r8, r14)
            int r14 = com.imofan.android.basic.feedback.MFFeedbackService.DATA_TIMEOUT
            int r14 = r14 * 1000
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r8, r14)
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient
            r4.<init>(r8)
            r9 = 0
            org.apache.http.HttpResponse r12 = r4.execute(r11)     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            org.apache.http.StatusLine r14 = r12.getStatusLine()     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            int r14 = r14.getStatusCode()     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            r15 = 304(0x130, float:4.26E-43)
            if (r14 != r15) goto L_0x007a
            r14 = 0
            if (r9 == 0) goto L_0x0074
            r9.close()     // Catch:{ IOException -> 0x0075 }
        L_0x0074:
            return r14
        L_0x0075:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0074
        L_0x007a:
            org.apache.http.HttpEntity r14 = r12.getEntity()     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            java.io.InputStream r5 = r14.getContent()     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            java.lang.String r14 = "Content-Encoding"
            org.apache.http.Header r2 = r12.getFirstHeader(r14)     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            if (r2 == 0) goto L_0x009c
            java.lang.String r14 = r2.getValue()     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            java.lang.String r15 = "gzip"
            boolean r14 = r14.equalsIgnoreCase(r15)     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            if (r14 == 0) goto L_0x009c
            java.util.zip.GZIPInputStream r6 = new java.util.zip.GZIPInputStream     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            r6.<init>(r5)     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            r5 = r6
        L_0x009c:
            java.io.BufferedReader r10 = new java.io.BufferedReader     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            java.io.InputStreamReader r14 = new java.io.InputStreamReader     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            r14.<init>(r5)     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
            r10.<init>(r14)     // Catch:{ ClientProtocolException -> 0x00f9, IOException -> 0x00d7 }
        L_0x00a6:
            java.lang.String r7 = r10.readLine()     // Catch:{ ClientProtocolException -> 0x00b6, IOException -> 0x00f6, all -> 0x00f3 }
            if (r7 == 0) goto L_0x00c7
            java.lang.StringBuffer r14 = r13.append(r7)     // Catch:{ ClientProtocolException -> 0x00b6, IOException -> 0x00f6, all -> 0x00f3 }
            java.lang.String r15 = "\r\n"
            r14.append(r15)     // Catch:{ ClientProtocolException -> 0x00b6, IOException -> 0x00f6, all -> 0x00f3 }
            goto L_0x00a6
        L_0x00b6:
            r3 = move-exception
            r9 = r10
        L_0x00b8:
            printErr(r3)     // Catch:{ all -> 0x00e7 }
            r14 = 0
            if (r9 == 0) goto L_0x0074
            r9.close()     // Catch:{ IOException -> 0x00c2 }
            goto L_0x0074
        L_0x00c2:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0074
        L_0x00c7:
            if (r10 == 0) goto L_0x00cc
            r10.close()     // Catch:{ IOException -> 0x00d2 }
        L_0x00cc:
            java.lang.String r14 = r13.toString()
            r9 = r10
            goto L_0x0074
        L_0x00d2:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x00cc
        L_0x00d7:
            r3 = move-exception
        L_0x00d8:
            printErr(r3)     // Catch:{ all -> 0x00e7 }
            r14 = 0
            if (r9 == 0) goto L_0x0074
            r9.close()     // Catch:{ IOException -> 0x00e2 }
            goto L_0x0074
        L_0x00e2:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x0074
        L_0x00e7:
            r14 = move-exception
        L_0x00e8:
            if (r9 == 0) goto L_0x00ed
            r9.close()     // Catch:{ IOException -> 0x00ee }
        L_0x00ed:
            throw r14
        L_0x00ee:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x00ed
        L_0x00f3:
            r14 = move-exception
            r9 = r10
            goto L_0x00e8
        L_0x00f6:
            r3 = move-exception
            r9 = r10
            goto L_0x00d8
        L_0x00f9:
            r3 = move-exception
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imofan.android.basic.feedback.MFFeedbackService.getFbRspWithGet(android.app.Activity, java.lang.String):java.lang.String");
    }

    private static void printErr(Exception e) {
        if (Mofang.isDebug()) {
            e.printStackTrace();
        }
    }
}
