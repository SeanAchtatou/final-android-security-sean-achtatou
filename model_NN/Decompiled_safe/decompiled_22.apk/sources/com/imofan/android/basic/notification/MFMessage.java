package com.imofan.android.basic.notification;

public class MFMessage {
    private String appId;
    private String appVersion;
    private String language;
    private String msgContent;
    private int msgId;
    private String osVersion;
    private long timeStamp;

    public int getMsgId() {
        return this.msgId;
    }

    public void setMsgId(int msgId2) {
        this.msgId = msgId2;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId2) {
        this.appId = appId2;
    }

    public String getAppVersion() {
        return this.appVersion;
    }

    public void setAppVersion(String appVersion2) {
        this.appVersion = appVersion2;
    }

    public String getOsVersion() {
        return this.osVersion;
    }

    public void setOsVersion(String osVersion2) {
        this.osVersion = osVersion2;
    }

    public String getMsgContent() {
        return this.msgContent;
    }

    public void setMsgContent(String msgContent2) {
        this.msgContent = msgContent2;
    }

    public long getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(long timeStamp2) {
        this.timeStamp = timeStamp2;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language2) {
        this.language = language2;
    }
}
