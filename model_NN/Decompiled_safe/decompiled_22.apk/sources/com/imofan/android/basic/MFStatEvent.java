package com.imofan.android.basic;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.city_life.part_asynctask.UploadUtils;
import java.util.ArrayList;
import java.util.List;

public class MFStatEvent {
    static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS event (id INTEGER PRIMARY KEY, date INTEGER NOT NULL, hour INTEGER NOT NULL, event TEXT NOT NULL, value TEXT, count INTEGER DEFAULT 1, exit INTEGER DEFAULT 0, duration INTEGER DEFAULT 0, first INTEGER DEFAULT 0, time NUMERIC NOT NULL, send INTEGER DEFAULT 0);";
    static final String EVENT_ACCESS_NODE = "node";
    static final String EVENT_ACCESS_PATH = "path";
    static final String EVENT_COLSE = "close";
    static final String EVENT_CRASH = "crash";
    static final String EVENT_DEVELOPER = "developer";
    static final String EVENT_INSTALL = "install";
    public static final String EVENT_NOTIFICATION_OPEN = "notification_open";
    public static final String EVENT_NOTIFICATION_RECEIVE = "notification_receive";
    static final String EVENT_OPEN = "open";
    static final String EVENT_RETURN_USER = "return";
    static final String EVENT_UPDATE = "update";
    static final String EVENT_UPGRADE = "upgrade";
    static final String FIELD_COUNT = "count";
    static final String FIELD_DATE = "date";
    static final String FIELD_DURATION = "duration";
    static final String FIELD_EVENT = "event";
    static final String FIELD_EXIT = "exit";
    static final String FIELD_FIRST = "first";
    static final String FIELD_HOUR = "hour";
    static final String FIELD_ID = "id";
    static final String FIELD_SEND = "send";
    static final String FIELD_TIME = "time";
    static final String FIELD_VALUE = "value";
    private static final String LOG_TAG = "Mofang_MFEvent";
    static final String TABLE = "event";
    private int count = 0;
    private int date;
    private long duration = 0;
    private String event;
    private int exit = 0;
    private boolean first = false;
    private int hour;
    private int id;
    private boolean send = false;
    private long time = System.currentTimeMillis();
    private String value;

    static MFStatEvent parse(Cursor cur) {
        boolean z = true;
        MFStatEvent event2 = new MFStatEvent();
        if (cur.getColumnIndex("id") > -1) {
            event2.setId(cur.getInt(cur.getColumnIndex("id")));
        }
        if (cur.getColumnIndex(FIELD_DATE) > -1) {
            event2.setDate(cur.getInt(cur.getColumnIndex(FIELD_DATE)));
        }
        if (cur.getColumnIndex(FIELD_HOUR) > -1) {
            event2.setHour(cur.getInt(cur.getColumnIndex(FIELD_HOUR)));
        }
        if (cur.getColumnIndex("event") > -1) {
            event2.setEvent(cur.getString(cur.getColumnIndex("event")));
        }
        if (cur.getColumnIndex("value") > -1) {
            event2.setValue(cur.getString(cur.getColumnIndex("value")));
        }
        if (cur.getColumnIndex(FIELD_COUNT) > -1) {
            event2.setCount(cur.getInt(cur.getColumnIndex(FIELD_COUNT)));
        }
        if (cur.getColumnIndex(FIELD_EXIT) > -1) {
            event2.setExit(cur.getInt(cur.getColumnIndex(FIELD_EXIT)));
        }
        if (cur.getColumnIndex(FIELD_DURATION) > -1) {
            event2.setDuration(cur.getLong(cur.getColumnIndex(FIELD_DURATION)));
        }
        if (cur.getColumnIndex(FIELD_FIRST) > -1) {
            event2.setFirst(cur.getInt(cur.getColumnIndex(FIELD_FIRST)) == 1);
        }
        if (cur.getColumnIndex(FIELD_TIME) > -1) {
            event2.setTime(cur.getLong(cur.getColumnIndex(FIELD_TIME)));
        }
        if (cur.getColumnIndex(FIELD_SEND) > -1) {
            if (cur.getInt(cur.getColumnIndex(FIELD_SEND)) != 1) {
                z = false;
            }
            event2.setSend(z);
        }
        return event2;
    }

    private void persist(MFDatabaseManager dbManager) {
        int i;
        int i2 = 1;
        SQLiteDatabase database = dbManager.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(FIELD_DATE, Integer.valueOf(getDate()));
        cv.put(FIELD_HOUR, Integer.valueOf(getHour()));
        cv.put("event", getEvent());
        if (getValue() != null) {
            cv.put("value", getValue());
        }
        cv.put(FIELD_EXIT, Integer.valueOf(getExit()));
        cv.put(FIELD_COUNT, Integer.valueOf(getCount()));
        cv.put(FIELD_DURATION, Long.valueOf(getDuration()));
        if (isFirst()) {
            i = 1;
        } else {
            i = 0;
        }
        cv.put(FIELD_FIRST, Integer.valueOf(i));
        cv.put(FIELD_TIME, Long.valueOf(getTime()));
        if (!isSend()) {
            i2 = 0;
        }
        cv.put(FIELD_SEND, Integer.valueOf(i2));
        database.insert("event", null, cv);
    }

    static List<MFStatEvent> getEvents(MFDatabaseManager dbManager, String event2, String value2, int date2, boolean send2) {
        String where;
        String where2;
        String where3 = " where send=?";
        List<String> paramList = new ArrayList<>();
        if (send2) {
            paramList.add(UploadUtils.FAILURE);
        } else {
            paramList.add(UploadUtils.SUCCESS);
        }
        if (event2 != null && event2.trim().length() > 0) {
            where3 = where3 + " and event=?";
            paramList.add(event2);
        }
        if (value2 != null && value2.trim().length() > 0) {
            if (where.length() > 0) {
                where = where + " and value=?";
            } else {
                where = "value=?";
            }
            paramList.add(value2);
        }
        if (date2 > 0) {
            if (where.length() > 0) {
                where2 = where + " and date=?";
            } else {
                where2 = "date=?";
            }
            paramList.add(Integer.toString(date2));
        }
        String[] params = new String[paramList.size()];
        for (int i = 0; i < paramList.size(); i++) {
            params[i] = (String) paramList.get(i);
        }
        Cursor cur = null;
        try {
            cur = dbManager.getReadableDatabase().rawQuery("select * from event" + where + " order by " + FIELD_TIME, params);
            if (cur.getCount() > 0) {
                List<MFStatEvent> events = new ArrayList<>(cur.getCount());
                while (cur.moveToNext()) {
                    events.add(parse(cur));
                }
            }
            if (cur != null) {
                cur.close();
            }
            return null;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    static int[] getEventDates(MFDatabaseManager dbManager) {
        int[] dates = null;
        SQLiteDatabase db = dbManager.getReadableDatabase();
        Cursor cur = null;
        try {
            cur = db.rawQuery("select date from event where send=? and event<>? order by date", new String[]{UploadUtils.SUCCESS, EVENT_UPDATE});
            if (cur.getCount() > 0) {
                List<Integer> dateList = new ArrayList<>();
                int lastDate = 0;
                while (cur.moveToNext()) {
                    int date2 = cur.getInt(cur.getColumnIndex(FIELD_DATE));
                    if (date2 > 0 && date2 != lastDate) {
                        dateList.add(Integer.valueOf(date2));
                        lastDate = date2;
                    }
                }
                if (dateList.size() > 0) {
                    dates = new int[dateList.size()];
                    for (int i = 0; i < dateList.size(); i++) {
                        dates[i] = ((Integer) dateList.get(i)).intValue();
                    }
                }
            }
            return dates;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    static void addEvent(MFDatabaseManager dbManager, String event2, int date2, int hour2, String value2, int count2, int exit2, long duration2, boolean first2, long time2) {
        MFStatEvent ev = new MFStatEvent();
        ev.setEvent(event2);
        ev.setDate(date2);
        ev.setHour(hour2);
        ev.setValue(value2);
        ev.setCount(count2);
        ev.setExit(exit2);
        ev.setDuration(duration2);
        ev.setFirst(first2);
        if (time2 > 0) {
            ev.setTime(time2);
        }
        try {
            ev.persist(dbManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void clearAccessPath(MFDatabaseManager dbManager) {
        SQLiteDatabase database = dbManager.getWritableDatabase();
        database.delete("event", "event=?", new String[]{EVENT_ACCESS_PATH});
        database.delete("event", "event=?", new String[]{EVENT_ACCESS_NODE});
    }

    static void clearHistoryEvents(MFDatabaseManager dbManager, int date2) {
        dbManager.getWritableDatabase().delete("event", "send=? and date<?", new String[]{UploadUtils.FAILURE, Integer.toString(date2)});
    }

    static void addEvent(MFDatabaseManager dbManager, String event2, int date2, int hour2, String value2, int count2, long duration2, boolean first2, long time2) {
        addEvent(dbManager, event2, date2, hour2, value2, count2, 0, duration2, first2, time2);
    }

    public static void addEvent(MFDatabaseManager dbManager, String event2, int date2, int hour2, String value2) {
        addEvent(dbManager, event2, date2, hour2, value2, 1, 0, false, 0);
    }

    static void signSentEvent(MFDatabaseManager dbManager, List<Integer> idList) {
        SQLiteDatabase db = dbManager.getWritableDatabase();
        for (Integer intValue : idList) {
            int id2 = intValue.intValue();
            ContentValues cv = new ContentValues();
            cv.put(FIELD_SEND, UploadUtils.FAILURE);
            db.update("event", cv, "id=?", new String[]{Integer.toString(id2)});
        }
    }

    public String toString() {
        return "{id=" + this.id + ", event=" + this.event + ", date=" + this.date + ", hour=" + this.hour + ", value=" + this.value + ", count=" + this.count + ", exit=" + this.exit + ", duration=" + this.duration + ", first=" + this.first + ", time=" + this.time + ", send=" + this.send + "}";
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public int getDate() {
        return this.date;
    }

    public void setDate(int date2) {
        this.date = date2;
    }

    public int getHour() {
        return this.hour;
    }

    public void setHour(int hour2) {
        this.hour = hour2;
    }

    public String getEvent() {
        return this.event;
    }

    public void setEvent(String event2) {
        this.event = event2;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        if (count2 > 0) {
            this.count = count2;
        }
    }

    public int getExit() {
        return this.exit;
    }

    public void setExit(int exit2) {
        if (exit2 > 0) {
            this.exit = exit2;
        }
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration2) {
        if (duration2 > 0) {
            this.duration = duration2;
        }
    }

    public boolean isFirst() {
        return this.first;
    }

    public void setFirst(boolean first2) {
        this.first = first2;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time2) {
        this.time = time2;
    }

    public boolean isSend() {
        return this.send;
    }

    public void setSend(boolean send2) {
        this.send = send2;
    }
}
