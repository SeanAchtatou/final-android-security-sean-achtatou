package com.imofan.android.basic.update;

import java.util.Date;

public class MFUpdateAPKInfo {
    private String apkPath;
    private String description;
    private Date time;
    private int versionCode;
    private String versionName;

    public int getVersionCode() {
        return this.versionCode;
    }

    public void setVersionCode(int versionCode2) {
        this.versionCode = versionCode2;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void setVersionName(String versionName2) {
        this.versionName = versionName2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getApkPath() {
        return this.apkPath;
    }

    public void setApkPath(String apkPath2) {
        this.apkPath = apkPath2;
    }

    public Date getTime() {
        return this.time;
    }

    public void setTime(Date time2) {
        this.time = time2;
    }
}
