package com.imofan.android.basic;

import android.os.Process;
import android.util.Log;
import java.lang.Thread;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;

class MFCrashHandler implements Thread.UncaughtExceptionHandler {
    private static final String LOG_TAG = "Mofang_MFCrashHandler";
    private static MFCrashHandler self;
    private MFDatabaseManager dbManager;

    private MFCrashHandler(MFDatabaseManager dbManager2) {
        this.dbManager = dbManager2;
    }

    static synchronized void init(MFDatabaseManager dbManager2) {
        synchronized (MFCrashHandler.class) {
            if (self == null) {
                self = new MFCrashHandler(dbManager2);
                Thread.setDefaultUncaughtExceptionHandler(self);
            }
        }
    }

    public void uncaughtException(Thread thread, Throwable throwable) {
        Log.e(LOG_TAG, "[uncaughtException]Find uncaught exception");
        throwable.printStackTrace();
        JSONObject exceptionJson = new JSONObject();
        try {
            exceptionJson.put(Constants.SINA_NAME, throwable.toString());
            exceptionJson.put("stack", Log.getStackTraceString(throwable));
            Date now = new Date();
            MFStatEvent.addEvent(this.dbManager, "crash", Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(now)), Integer.parseInt(new SimpleDateFormat("H").format(now)), exceptionJson.toString());
        } catch (JSONException e) {
        }
        Process.killProcess(Process.myPid());
    }
}
