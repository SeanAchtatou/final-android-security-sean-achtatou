package com.imofan.android.basic.update;

public class MFHttpUtils {
    private static final String ACCEPT_ENCODING = "gzip, deflate";
    public static int CONNECT_TIMEOUT = 20;
    public static int DATA_TIMEOUT = 40;

    /* JADX WARNING: Removed duplicated region for block: B:17:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String invokeText(java.lang.String r13) throws java.io.IOException {
        /*
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            org.apache.http.client.methods.HttpGet r8 = new org.apache.http.client.methods.HttpGet
            r8.<init>(r13)
            java.lang.String r11 = "Accept-Encoding"
            java.lang.String r12 = "gzip, deflate"
            r8.addHeader(r11, r12)
            org.apache.http.params.BasicHttpParams r5 = new org.apache.http.params.BasicHttpParams
            r5.<init>()
            int r11 = com.imofan.android.basic.update.MFHttpUtils.CONNECT_TIMEOUT
            int r11 = r11 * 1000
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r5, r11)
            int r11 = com.imofan.android.basic.update.MFHttpUtils.DATA_TIMEOUT
            int r11 = r11 * 1000
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r5, r11)
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient
            r1.<init>(r5)
            r6 = 0
            org.apache.http.HttpResponse r9 = r1.execute(r8)     // Catch:{ all -> 0x0089 }
            org.apache.http.StatusLine r11 = r9.getStatusLine()     // Catch:{ all -> 0x0089 }
            int r11 = r11.getStatusCode()     // Catch:{ all -> 0x0089 }
            r12 = 200(0xc8, float:2.8E-43)
            if (r11 != r12) goto L_0x007f
            org.apache.http.HttpEntity r11 = r9.getEntity()     // Catch:{ all -> 0x0089 }
            java.io.InputStream r2 = r11.getContent()     // Catch:{ all -> 0x0089 }
            java.lang.String r11 = "Content-Encoding"
            org.apache.http.Header r0 = r9.getFirstHeader(r11)     // Catch:{ all -> 0x0089 }
            if (r0 == 0) goto L_0x005c
            java.lang.String r11 = r0.getValue()     // Catch:{ all -> 0x0089 }
            java.lang.String r12 = "gzip"
            boolean r11 = r11.equalsIgnoreCase(r12)     // Catch:{ all -> 0x0089 }
            if (r11 == 0) goto L_0x005c
            java.util.zip.GZIPInputStream r3 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0089 }
            r3.<init>(r2)     // Catch:{ all -> 0x0089 }
            r2 = r3
        L_0x005c:
            java.io.BufferedReader r7 = new java.io.BufferedReader     // Catch:{ all -> 0x0089 }
            java.io.InputStreamReader r11 = new java.io.InputStreamReader     // Catch:{ all -> 0x0089 }
            r11.<init>(r2)     // Catch:{ all -> 0x0089 }
            r7.<init>(r11)     // Catch:{ all -> 0x0089 }
        L_0x0066:
            java.lang.String r4 = r7.readLine()     // Catch:{ all -> 0x0076 }
            if (r4 == 0) goto L_0x007e
            java.lang.StringBuffer r11 = r10.append(r4)     // Catch:{ all -> 0x0076 }
            java.lang.String r12 = "\r\n"
            r11.append(r12)     // Catch:{ all -> 0x0076 }
            goto L_0x0066
        L_0x0076:
            r11 = move-exception
            r6 = r7
        L_0x0078:
            if (r6 == 0) goto L_0x007d
            r6.close()
        L_0x007d:
            throw r11
        L_0x007e:
            r6 = r7
        L_0x007f:
            if (r6 == 0) goto L_0x0084
            r6.close()
        L_0x0084:
            java.lang.String r11 = r10.toString()
            return r11
        L_0x0089:
            r11 = move-exception
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imofan.android.basic.update.MFHttpUtils.invokeText(java.lang.String):java.lang.String");
    }
}
