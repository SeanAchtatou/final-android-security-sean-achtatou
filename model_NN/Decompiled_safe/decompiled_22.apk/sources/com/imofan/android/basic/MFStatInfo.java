package com.imofan.android.basic;

import android.content.Context;
import android.content.SharedPreferences;

public class MFStatInfo {
    public static final String INFO_FILE = "mofang_data_analysis";
    public static final String KEY_APP_VERSION = "app_ver";
    static final String KEY_CARRIER = "carrier";
    static final String KEY_CHANNEL = "channel";
    static final String KEY_COUNTRY = "country";
    static final String KEY_DEVICE_ID = "device_id";
    public static final String KEY_DEV_ID = "dev_id";
    static final String KEY_DEV_ID_UNDEFINED = "dev_id_udf";
    static final String KEY_INSTALL_DATE = "install_date";
    static final String KEY_LANGUAGE = "language";
    static final String KEY_MAC_ADDRESS = "mac_addr";
    public static final String KEY_MODEL = "model";
    static final String KEY_OS = "os";
    public static final String KEY_OS_VERSION = "os_ver";
    static final String KEY_RESOLUTION = "resolution";
    static final String KEY_SDK_VERSION = "sdk_ver";
    static final String KEY_TIMEZONE = "timezone";
    static final String KEY_USER_ACCESS_PATH = "user_activities";
    static final String KEY_USER_DURATION = "user_duration";
    static final String KEY_USER_PAUSE_TIME = "user_pause_time";
    static final String KEY_USER_RETURN = "user_return";
    private static final String LOG_TAG = "Mofang_MFStatInfo";
    private static SharedPreferences sp = null;
    private static SharedPreferences.Editor spEditor = null;

    static synchronized void init(Context context) {
        synchronized (MFStatInfo.class) {
            if (sp == null) {
                sp = context.getSharedPreferences(INFO_FILE, 0);
                spEditor = sp.edit();
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r32v106, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v55, resolved type: android.telephony.TelephonyManager} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void updateAppAndDeviceInfo(com.imofan.android.basic.MFDatabaseManager r35, android.app.Activity r36, int r37, int r38) {
        /*
            java.lang.String r10 = ""
            r29 = 0
            java.lang.String r32 = "phone"
            r0 = r36
            r1 = r32
            java.lang.Object r32 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x040a }
            r0 = r32
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x040a }
            r29 = r0
            java.lang.String r32 = r29.getDeviceId()     // Catch:{ Exception -> 0x040a }
            java.lang.String r10 = r32.toLowerCase()     // Catch:{ Exception -> 0x040a }
            java.lang.String r32 = "000000000000000"
            r0 = r32
            boolean r32 = r10.equals(r0)     // Catch:{ Exception -> 0x040a }
            if (r32 == 0) goto L_0x0028
            java.lang.String r10 = ""
        L_0x0028:
            java.lang.String r32 = "device_id"
            r0 = r32
            putString(r0, r10)     // Catch:{ Exception -> 0x040a }
        L_0x002f:
            java.lang.String r32 = "model"
            java.lang.String r33 = android.os.Build.MODEL
            putString(r32, r33)
            android.util.DisplayMetrics r11 = new android.util.DisplayMetrics
            r11.<init>()
            android.view.WindowManager r32 = r36.getWindowManager()
            android.view.Display r32 = r32.getDefaultDisplay()
            r0 = r32
            r0.getMetrics(r11)
            java.lang.String r32 = "resolution"
            java.lang.StringBuilder r33 = new java.lang.StringBuilder
            r33.<init>()
            int r0 = r11.heightPixels
            r34 = r0
            java.lang.StringBuilder r33 = r33.append(r34)
            java.lang.String r34 = "x"
            java.lang.StringBuilder r33 = r33.append(r34)
            int r0 = r11.widthPixels
            r34 = r0
            java.lang.StringBuilder r33 = r33.append(r34)
            java.lang.String r33 = r33.toString()
            putString(r32, r33)
            java.lang.String r32 = "install_date"
            r33 = 0
            int r15 = getInt(r32, r33)
            if (r15 > 0) goto L_0x02e9
            r15 = r37
            java.lang.String r32 = "install_date"
            r0 = r32
            putInt(r0, r15)
        L_0x007f:
            java.lang.String r32 = "sdk_ver"
            r33 = 0
            java.lang.String r27 = getString(r32, r33)
            java.lang.String r32 = "1.0.2"
            r0 = r32
            r1 = r27
            boolean r32 = r0.equals(r1)
            if (r32 != 0) goto L_0x00ad
            java.lang.String r32 = "sdk_ver"
            java.lang.String r33 = "1.0.2"
            putString(r32, r33)
            if (r27 == 0) goto L_0x00ad
            java.lang.String r32 = "update"
            java.lang.String r33 = "sdk_ver"
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            r4 = r33
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r4)
        L_0x00ad:
            java.lang.String r32 = "app_ver"
            r33 = 0
            java.lang.String r19 = getString(r32, r33)
            r5 = 0
            android.content.pm.PackageManager r32 = r36.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0311 }
            java.lang.String r33 = r36.getPackageName()     // Catch:{ NameNotFoundException -> 0x0311 }
            r34 = 16384(0x4000, float:2.2959E-41)
            android.content.pm.PackageInfo r26 = r32.getPackageInfo(r33, r34)     // Catch:{ NameNotFoundException -> 0x0311 }
            r0 = r26
            java.lang.String r5 = r0.versionName     // Catch:{ NameNotFoundException -> 0x0311 }
        L_0x00c8:
            if (r19 != 0) goto L_0x031e
            java.lang.String r32 = "install"
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r5)
            java.lang.String r32 = "app_ver"
            r0 = r32
            putString(r0, r5)
            java.lang.String r32 = "install_date"
            r0 = r32
            r1 = r37
            putInt(r0, r1)
        L_0x00e7:
            java.lang.String r32 = "channel"
            r33 = 0
            java.lang.String r7 = getString(r32, r33)
            if (r7 != 0) goto L_0x0117
            android.content.pm.PackageManager r32 = r36.getPackageManager()     // Catch:{ NameNotFoundException -> 0x035b }
            java.lang.String r33 = r36.getPackageName()     // Catch:{ NameNotFoundException -> 0x035b }
            r34 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r32 = r32.getApplicationInfo(r33, r34)     // Catch:{ NameNotFoundException -> 0x035b }
            r0 = r32
            android.os.Bundle r0 = r0.metaData     // Catch:{ NameNotFoundException -> 0x035b }
            r32 = r0
            java.lang.String r33 = "MOFANG_CHANNEL"
            java.lang.Object r32 = r32.get(r33)     // Catch:{ NameNotFoundException -> 0x035b }
            r0 = r32
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ NameNotFoundException -> 0x035b }
            r7 = r0
            java.lang.String r32 = "channel"
            r0 = r32
            putString(r0, r7)     // Catch:{ NameNotFoundException -> 0x035b }
        L_0x0117:
            java.lang.String r32 = "os_ver"
            r33 = 0
            java.lang.String r23 = getString(r32, r33)
            java.lang.String r25 = android.os.Build.VERSION.RELEASE
            if (r25 == 0) goto L_0x0149
            r0 = r25
            r1 = r23
            boolean r32 = r0.equals(r1)
            if (r32 != 0) goto L_0x0149
            java.lang.String r32 = "os_ver"
            r0 = r32
            r1 = r25
            putString(r0, r1)
            if (r23 == 0) goto L_0x0149
            java.lang.String r32 = "update"
            java.lang.String r33 = "os_ver"
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            r4 = r33
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r4)
        L_0x0149:
            java.lang.String r32 = "carrier"
            r33 = 0
            java.lang.String r20 = getString(r32, r33)
            r6 = 0
            if (r29 == 0) goto L_0x019e
            java.lang.String r13 = r29.getSubscriberId()
            if (r13 == 0) goto L_0x019e
            java.lang.String r32 = r13.trim()
            int r32 = r32.length()
            if (r32 <= 0) goto L_0x019e
            java.lang.String r32 = "46000"
            r0 = r32
            boolean r32 = r13.startsWith(r0)
            if (r32 != 0) goto L_0x0178
            java.lang.String r32 = "46002"
            r0 = r32
            boolean r32 = r13.startsWith(r0)
            if (r32 == 0) goto L_0x0368
        L_0x0178:
            java.lang.String r6 = "中国移动"
        L_0x017a:
            if (r6 == 0) goto L_0x019e
            r0 = r20
            boolean r32 = r6.equals(r0)
            if (r32 != 0) goto L_0x019e
            java.lang.String r32 = "carrier"
            r0 = r32
            putString(r0, r6)
            if (r20 == 0) goto L_0x019e
            java.lang.String r32 = "update"
            java.lang.String r33 = "carrier"
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            r4 = r33
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r4)
        L_0x019e:
            java.lang.String r32 = "timezone"
            r33 = 8
            int r24 = getInt(r32, r33)
            java.util.TimeZone r32 = java.util.TimeZone.getDefault()
            int r32 = r32.getRawOffset()
            r33 = 3600000(0x36ee80, float:5.044674E-39)
            int r30 = r32 / r33
            r0 = r30
            r1 = r24
            if (r0 == r1) goto L_0x01d3
            java.lang.String r32 = "timezone"
            r0 = r32
            r1 = r30
            putInt(r0, r1)
            java.lang.String r32 = "update"
            java.lang.String r33 = "timezone"
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            r4 = r33
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r4)
        L_0x01d3:
            java.lang.String r32 = "language"
            r33 = 0
            java.lang.String r22 = getString(r32, r33)
            android.content.res.Resources r32 = r36.getResources()
            android.content.res.Configuration r32 = r32.getConfiguration()
            r0 = r32
            java.util.Locale r0 = r0.locale
            r32 = r0
            java.lang.String r17 = r32.getDisplayLanguage()
            if (r17 == 0) goto L_0x0215
            r0 = r17
            r1 = r22
            boolean r32 = r0.equals(r1)
            if (r32 != 0) goto L_0x0215
            java.lang.String r32 = "language"
            r0 = r32
            r1 = r17
            putString(r0, r1)
            if (r22 == 0) goto L_0x0215
            java.lang.String r32 = "update"
            java.lang.String r33 = "language"
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            r4 = r33
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r4)
        L_0x0215:
            java.lang.String r32 = "country"
            r33 = 0
            java.lang.String r21 = getString(r32, r33)
            android.content.res.Resources r32 = r36.getResources()
            android.content.res.Configuration r32 = r32.getConfiguration()
            r0 = r32
            java.util.Locale r0 = r0.locale
            r32 = r0
            java.lang.String r8 = r32.getCountry()
            if (r8 == 0) goto L_0x0253
            r0 = r21
            boolean r32 = r8.equals(r0)
            if (r32 != 0) goto L_0x0253
            java.lang.String r32 = "country"
            r0 = r32
            putString(r0, r8)
            if (r21 == 0) goto L_0x0253
            java.lang.String r32 = "update"
            java.lang.String r33 = "country"
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            r4 = r33
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r4)
        L_0x0253:
            java.lang.String r32 = "dev_id"
            r33 = 0
            java.lang.String r9 = getString(r32, r33)
            if (r9 == 0) goto L_0x0269
            int r32 = r9.length()
            r33 = 32
            r0 = r32
            r1 = r33
            if (r0 == r1) goto L_0x0401
        L_0x0269:
            java.lang.String r32 = "wifi"
            r0 = r36
            r1 = r32
            java.lang.Object r31 = r0.getSystemService(r1)
            android.net.wifi.WifiManager r31 = (android.net.wifi.WifiManager) r31
            android.net.wifi.WifiInfo r14 = r31.getConnectionInfo()
            if (r14 != 0) goto L_0x038a
            java.lang.String r18 = ""
        L_0x027d:
            java.lang.String r32 = "mac_addr"
            r0 = r32
            r1 = r18
            putString(r0, r1)
            java.lang.StringBuilder r32 = new java.lang.StringBuilder
            r32.<init>()
            r0 = r32
            java.lang.StringBuilder r32 = r0.append(r10)
            java.lang.String r33 = "-"
            java.lang.StringBuilder r32 = r32.append(r33)
            r0 = r32
            r1 = r18
            java.lang.StringBuilder r32 = r0.append(r1)
            java.lang.String r28 = r32.toString()
            int r32 = r28.length()
            r33 = 1
            r0 = r32
            r1 = r33
            if (r0 <= r1) goto L_0x03a5
            java.lang.StringBuilder r32 = new java.lang.StringBuilder
            r32.<init>()
            java.lang.String r33 = "dev-"
            java.lang.StringBuilder r32 = r32.append(r33)
            r0 = r32
            r1 = r28
            java.lang.StringBuilder r32 = r0.append(r1)
            java.lang.String r28 = r32.toString()
            java.lang.String r32 = "dev_id_udf"
            r33 = 0
            putInt(r32, r33)
        L_0x02cd:
            byte[] r32 = r28.getBytes()
            java.util.UUID r32 = java.util.UUID.nameUUIDFromBytes(r32)
            java.lang.String r32 = r32.toString()
            java.lang.String r33 = "-"
            java.lang.String r34 = ""
            java.lang.String r9 = r32.replaceAll(r33, r34)
            java.lang.String r32 = "dev_id"
            r0 = r32
            putString(r0, r9)
        L_0x02e8:
            return
        L_0x02e9:
            java.lang.String r32 = "user_return"
            r33 = 0
            boolean r16 = getBoolean(r32, r33)
            if (r16 != 0) goto L_0x007f
            r0 = r37
            if (r0 <= r15) goto L_0x007f
            java.lang.String r32 = "return"
            r33 = 0
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            r4 = r33
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r4)
            java.lang.String r32 = "user_return"
            r33 = 1
            putBoolean(r32, r33)
            goto L_0x007f
        L_0x0311:
            r12 = move-exception
            java.lang.String r32 = "Mofang_MFStatInfo"
            java.lang.String r33 = "[updateAppAndDeviceInfo]Get app version error"
            android.util.Log.e(r32, r33)
            r12.printStackTrace()
            goto L_0x00c8
        L_0x031e:
            if (r5 == 0) goto L_0x00e7
            r0 = r19
            boolean r32 = r5.equals(r0)
            if (r32 != 0) goto L_0x00e7
            java.lang.String r32 = "upgrade"
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            r4 = r19
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r4)
            java.lang.String r32 = "update"
            java.lang.String r33 = "app_ver"
            r0 = r35
            r1 = r32
            r2 = r37
            r3 = r38
            r4 = r33
            com.imofan.android.basic.MFStatEvent.addEvent(r0, r1, r2, r3, r4)
            java.lang.String r32 = "app_ver"
            r0 = r32
            putString(r0, r5)
            java.lang.String r32 = "user_activities"
            java.lang.String r33 = ""
            putString(r32, r33)
            com.imofan.android.basic.MFStatEvent.clearAccessPath(r35)
            goto L_0x00e7
        L_0x035b:
            r12 = move-exception
            java.lang.String r32 = "Mofang_MFStatInfo"
            java.lang.String r33 = "[updateAppAndDeviceInfo]Get app channel error"
            android.util.Log.e(r32, r33)
            r12.printStackTrace()
            goto L_0x0117
        L_0x0368:
            java.lang.String r32 = "46001"
            r0 = r32
            boolean r32 = r13.startsWith(r0)
            if (r32 == 0) goto L_0x0376
            java.lang.String r6 = "中国联通"
            goto L_0x017a
        L_0x0376:
            java.lang.String r32 = "46003"
            r0 = r32
            boolean r32 = r13.startsWith(r0)
            if (r32 == 0) goto L_0x0384
            java.lang.String r6 = "中国电信"
            goto L_0x017a
        L_0x0384:
            java.lang.String r6 = r29.getSimOperatorName()
            goto L_0x017a
        L_0x038a:
            java.lang.String r32 = r14.getMacAddress()
            if (r32 != 0) goto L_0x03a0
            java.lang.String r32 = ""
        L_0x0392:
            java.lang.String r33 = ":"
            java.lang.String r34 = ""
            java.lang.String r32 = r32.replaceAll(r33, r34)
            java.lang.String r18 = r32.toLowerCase()
            goto L_0x027d
        L_0x03a0:
            java.lang.String r32 = r14.getMacAddress()
            goto L_0x0392
        L_0x03a5:
            java.lang.StringBuilder r32 = new java.lang.StringBuilder
            r32.<init>()
            java.lang.String r33 = "udf-"
            java.lang.StringBuilder r32 = r32.append(r33)
            java.lang.String r33 = android.os.Build.MODEL
            java.lang.StringBuilder r32 = r32.append(r33)
            java.lang.String r33 = "-"
            java.lang.StringBuilder r32 = r32.append(r33)
            if (r25 != 0) goto L_0x03c0
            java.lang.String r25 = ""
        L_0x03c0:
            r0 = r32
            r1 = r25
            java.lang.StringBuilder r32 = r0.append(r1)
            java.lang.String r33 = "-"
            java.lang.StringBuilder r32 = r32.append(r33)
            if (r6 != 0) goto L_0x03d2
            java.lang.String r6 = ""
        L_0x03d2:
            r0 = r32
            java.lang.StringBuilder r32 = r0.append(r6)
            java.lang.String r33 = "-"
            java.lang.StringBuilder r32 = r32.append(r33)
            long r33 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r32 = r32.append(r33)
            java.lang.String r33 = "-"
            java.lang.StringBuilder r32 = r32.append(r33)
            double r33 = java.lang.Math.random()
            java.lang.StringBuilder r32 = r32.append(r33)
            java.lang.String r28 = r32.toString()
            java.lang.String r32 = "dev_id_udf"
            r33 = 1
            putInt(r32, r33)
            goto L_0x02cd
        L_0x0401:
            java.lang.String r32 = "dev_id_udf"
            r33 = 0
            putInt(r32, r33)
            goto L_0x02e8
        L_0x040a:
            r32 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imofan.android.basic.MFStatInfo.updateAppAndDeviceInfo(com.imofan.android.basic.MFDatabaseManager, android.app.Activity, int, int):void");
    }

    static void putString(String key, String value) {
        spEditor.putString(key, value);
        spEditor.commit();
    }

    public static String getString(String key, String def) {
        return sp.getString(key, def);
    }

    static void putInt(String key, int value) {
        spEditor.putInt(key, value);
        spEditor.commit();
    }

    static int getInt(String key, int def) {
        return sp.getInt(key, def);
    }

    static void putLong(String key, long value) {
        spEditor.putLong(key, value);
        spEditor.commit();
    }

    static long getLong(String key, long def) {
        return sp.getLong(key, def);
    }

    static void putBoolean(String key, boolean value) {
        spEditor.putBoolean(key, value);
        spEditor.commit();
    }

    static boolean getBoolean(String key, boolean def) {
        return sp.getBoolean(key, def);
    }
}
