package com.imofan.android.basic.update;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.RemoteViews;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.File;
import java.io.IOException;

public class MFUpdateService extends Service {
    /* access modifiers changed from: private */
    public static String CHECKURL = null;
    private static final int CREATE_FILE_FAIL = 3;
    private static final int DOWNLOAD_COMPLETE = 0;
    private static final int DOWNLOAD_FAIL = 2;
    private static final int DOWNLOAD_URL_ERROR = 1;
    /* access modifiers changed from: private */
    public static int appNameID = 0;
    /* access modifiers changed from: private */
    public static int blockViewID;
    /* access modifiers changed from: private */
    public static int iconID = 0;
    public static File internalFileDir;
    private static int layoutID;
    /* access modifiers changed from: private */
    public static MFAPPInfo mfappInfo;
    private static int notificationBarAppNameViewID;
    private static int notificationBarIconViewID;
    private static int processbarViewID;
    /* access modifiers changed from: private */
    public static int textViewID;
    /* access modifiers changed from: private */
    public static String updateApkDownloadUrl;
    public static int updateApkVersionCode;
    public static File updateFile = null;
    /* access modifiers changed from: private */
    public static PendingIntent updatePendingIntent = null;
    private static int updateTotalSize = 0;
    /* access modifiers changed from: private */
    public Handler updateHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    MFUpdateService.this.updateNotification.tickerText = "下载完毕！";
                    MFUpdateService.this.updateNotification.contentView.setViewVisibility(MFUpdateService.blockViewID, 8);
                    MFUpdateService.this.updateNotificationManager.notify(MFUpdateService.iconID, MFUpdateService.this.updateNotification);
                    try {
                        Runtime.getRuntime().exec("chmod 777 " + MFUpdateService.updateFile.getPath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Uri uri = Uri.fromFile(MFUpdateService.updateFile);
                    Intent updateCompleteIntent = new Intent("android.intent.action.VIEW");
                    updateCompleteIntent.setDataAndType(uri, "application/vnd.android.package-archive");
                    PendingIntent unused = MFUpdateService.updatePendingIntent = PendingIntent.getActivity(MFUpdateService.this, MFUpdateService.appNameID, updateCompleteIntent, 134217728);
                    MFUpdateService.this.updateNotification.defaults = 1;
                    MFUpdateService.this.updateNotification.contentIntent = MFUpdateService.updatePendingIntent;
                    MFUpdateService.this.updateNotification.contentView.setTextViewText(MFUpdateService.textViewID, "下载完成，点击安装！");
                    MFUpdateService.this.updateNotification.flags |= 16;
                    MFUpdateService.this.updateNotificationManager.notify(MFUpdateService.iconID, MFUpdateService.this.updateNotification);
                    break;
                case 1:
                    MFUpdateService.this.updateNotification.setLatestEventInfo(MFUpdateService.this, MFUpdateService.this.updateNotificationTitle, "下载失败 ，请重新下载！", MFUpdateService.updatePendingIntent);
                    MFUpdateService.this.updateNotificationManager.notify(MFUpdateService.iconID, MFUpdateService.this.updateNotification);
                    break;
                case 2:
                    MFUpdateService.this.updateNotification.setLatestEventInfo(MFUpdateService.this, MFUpdateService.this.updateNotificationTitle, "下载失败，请重新下载！", MFUpdateService.updatePendingIntent);
                    MFUpdateService.this.updateNotificationManager.notify(MFUpdateService.iconID, MFUpdateService.this.updateNotification);
                    break;
                case 3:
                    MFUpdateService.this.updateNotification.setLatestEventInfo(MFUpdateService.this, MFUpdateService.this.updateNotificationTitle, "下载失败，请检测内部存储是否可以访问！", MFUpdateService.updatePendingIntent);
                    MFUpdateService.this.updateNotificationManager.notify(MFUpdateService.iconID, MFUpdateService.this.updateNotification);
                    break;
            }
            MFUpdateService.this.stopSelf();
            super.handleMessage(msg);
        }
    };
    /* access modifiers changed from: private */
    public Notification updateNotification = null;
    /* access modifiers changed from: private */
    public NotificationManager updateNotificationManager = null;
    /* access modifiers changed from: private */
    public String updateNotificationTitle;
    private Runnable updateRunnable = new Runnable() {
        Message message = MFUpdateService.this.updateHandler.obtainMessage();

        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00b3, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b4, code lost:
            r0.printStackTrace();
            r11.message.what = 3;
            com.imofan.android.basic.update.MFUpdateService.access$1500(r11.this$0).sendMessage(r11.message);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00ef, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00f0, code lost:
            r1.printStackTrace();
            r11.message.what = 2;
            com.imofan.android.basic.update.MFUpdateService.access$1500(r11.this$0).sendMessage(r11.message);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00b3 A[ExcHandler: IOException (r0v0 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r11 = this;
                r10 = 2
                android.os.Message r6 = r11.message     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                r7 = 0
                r6.what = r7     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                java.io.File r6 = com.imofan.android.basic.update.MFUpdateService.updateFile     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                boolean r6 = r6.exists()     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                if (r6 != 0) goto L_0x0038
                java.io.File r6 = com.imofan.android.basic.update.MFUpdateService.updateFile     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                r6.createNewFile()     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
            L_0x0013:
                r4 = 0
                java.lang.String r6 = com.imofan.android.basic.update.MFUpdateService.updateApkDownloadUrl     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                if (r6 == 0) goto L_0x0027
                java.lang.String r6 = ""
                java.lang.String r7 = com.imofan.android.basic.update.MFUpdateService.updateApkDownloadUrl     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                if (r6 == 0) goto L_0x00d5
            L_0x0027:
                android.os.Message r6 = r11.message     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                r7 = 1
                r6.what = r7     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
            L_0x002c:
                com.imofan.android.basic.update.MFUpdateService r6 = com.imofan.android.basic.update.MFUpdateService.this     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                android.os.Handler r6 = r6.updateHandler     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                android.os.Message r7 = r11.message     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                r6.sendMessage(r7)     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
            L_0x0037:
                return
            L_0x0038:
                com.imofan.android.basic.update.MFUpdateService r6 = com.imofan.android.basic.update.MFUpdateService.this     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                android.content.pm.PackageManager r3 = r6.getPackageManager()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.io.File r6 = com.imofan.android.basic.update.MFUpdateService.updateFile     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.String r6 = r6.getPath()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r7 = 1
                android.content.pm.PackageInfo r2 = r3.getPackageArchiveInfo(r6, r7)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.io.PrintStream r6 = java.lang.System.out     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r7.<init>()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.String r8 = "pInfo :"
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r6.println(r7)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.io.PrintStream r6 = java.lang.System.out     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r7.<init>()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.String r8 = "pInfo versionCode:"
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                int r8 = r2.versionCode     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r6.println(r7)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.io.PrintStream r6 = java.lang.System.out     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r7.<init>()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.String r8 = "updateApkVersionCode :"
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                int r8 = com.imofan.android.basic.update.MFUpdateService.updateApkVersionCode     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r6.println(r7)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                if (r2 == 0) goto L_0x0013
                int r6 = com.imofan.android.basic.update.MFUpdateService.updateApkVersionCode     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                if (r6 <= 0) goto L_0x00c9
                int r6 = com.imofan.android.basic.update.MFUpdateService.updateApkVersionCode     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                int r7 = r2.versionCode     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                if (r6 != r7) goto L_0x00c9
                com.imofan.android.basic.update.MFUpdateService r6 = com.imofan.android.basic.update.MFUpdateService.this     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                android.os.Handler r6 = r6.updateHandler     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                android.os.Message r7 = r11.message     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r6.sendMessage(r7)     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                goto L_0x0037
            L_0x00ad:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                goto L_0x0013
            L_0x00b3:
                r0 = move-exception
                r0.printStackTrace()
                android.os.Message r6 = r11.message
                r7 = 3
                r6.what = r7
                com.imofan.android.basic.update.MFUpdateService r6 = com.imofan.android.basic.update.MFUpdateService.this
                android.os.Handler r6 = r6.updateHandler
                android.os.Message r7 = r11.message
                r6.sendMessage(r7)
                goto L_0x0037
            L_0x00c9:
                java.io.File r6 = com.imofan.android.basic.update.MFUpdateService.updateFile     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r6.delete()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                java.io.File r6 = com.imofan.android.basic.update.MFUpdateService.updateFile     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                r6.createNewFile()     // Catch:{ Exception -> 0x00ad, IOException -> 0x00b3 }
                goto L_0x0013
            L_0x00d5:
                com.imofan.android.basic.update.MFUpdateService r6 = com.imofan.android.basic.update.MFUpdateService.this     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                java.lang.String r7 = com.imofan.android.basic.update.MFUpdateService.updateApkDownloadUrl     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                java.io.File r8 = com.imofan.android.basic.update.MFUpdateService.updateFile     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                r9 = 1
                long r4 = r6.downloadUpdateFile(r7, r8, r9)     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                r6 = 0
                int r6 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r6 != 0) goto L_0x002c
                android.os.Message r6 = r11.message     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                r7 = 2
                r6.what = r7     // Catch:{ IOException -> 0x00b3, Exception -> 0x00ef }
                goto L_0x002c
            L_0x00ef:
                r1 = move-exception
                r1.printStackTrace()
                android.os.Message r6 = r11.message
                r6.what = r10
                com.imofan.android.basic.update.MFUpdateService r6 = com.imofan.android.basic.update.MFUpdateService.this
                android.os.Handler r6 = r6.updateHandler
                android.os.Message r7 = r11.message
                r6.sendMessage(r7)
                goto L_0x0037
            */
            throw new UnsupportedOperationException("Method not decompiled: com.imofan.android.basic.update.MFUpdateService.AnonymousClass4.run():void");
        }
    };

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        layoutID = MFUpdateUtils.getIdByReflection(getApplicationContext(), "layout", "mofang_update_notification");
        textViewID = MFUpdateUtils.getIdByReflection(getApplicationContext(), LocaleUtil.INDONESIAN, "mfupdate_notification_progresstext");
        blockViewID = MFUpdateUtils.getIdByReflection(getApplicationContext(), LocaleUtil.INDONESIAN, "mfupdate_notification_progressblock");
        processbarViewID = MFUpdateUtils.getIdByReflection(getApplicationContext(), LocaleUtil.INDONESIAN, "mfupdate_notification_progressbar");
        notificationBarIconViewID = MFUpdateUtils.getIdByReflection(getApplicationContext(), LocaleUtil.INDONESIAN, "mfupdate_notification_icon");
        notificationBarAppNameViewID = MFUpdateUtils.getIdByReflection(getApplicationContext(), LocaleUtil.INDONESIAN, "mfupdate_notification_appname");
        mfappInfo = MFUpdateUtils.getVersionInfo(this);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        iconID = intent.getIntExtra("iconID", 0);
        appNameID = intent.getIntExtra("appNameID", 0);
        updateApkVersionCode = intent.getIntExtra("versionCode", 0);
        updateApkDownloadUrl = intent.getStringExtra("downloadUrl");
        createAPKStorageFile();
        this.updateNotificationTitle = getResources().getString(appNameID);
        this.updateNotificationManager = (NotificationManager) getSystemService("notification");
        this.updateNotification = new Notification();
        this.updateNotification.contentView = new RemoteViews(getApplication().getPackageName(), layoutID);
        Intent updateCompletingIntent = new Intent();
        updateCompletingIntent.setFlags(536870912);
        updateCompletingIntent.setClass(getApplication().getApplicationContext(), MFUpdateService.class);
        updatePendingIntent = PendingIntent.getActivity(this, appNameID, updateCompletingIntent, 268435456);
        this.updateNotification.icon = iconID;
        this.updateNotification.tickerText = "开始下载";
        this.updateNotification.contentIntent = updatePendingIntent;
        this.updateNotification.contentView.setImageViewResource(notificationBarIconViewID, iconID);
        this.updateNotification.contentView.setTextViewText(notificationBarAppNameViewID, this.updateNotificationTitle);
        this.updateNotification.contentView.setProgressBar(processbarViewID, 100, 0, false);
        this.updateNotification.contentView.setTextViewText(textViewID, "0% ");
        this.updateNotificationManager.cancel(iconID);
        this.updateNotificationManager.notify(iconID, this.updateNotification);
        new Thread(this.updateRunnable).start();
        return super.onStartCommand(intent, flags, startId);
    }

    public static void check(final Activity activity, final MFUpdateListener updateListener, final boolean showUpdateDialog, final boolean wifiOnly) {
        mfappInfo = MFUpdateUtils.getVersionInfo(activity);
        CHECKURL = getCheckUrl(activity.getApplicationContext(), mfappInfo);
        new Thread() {
            public void run() {
                if (wifiOnly && 1 == MFNetworkUtils.getNetworkState(activity)) {
                    MFUpdateAPKInfo mfInfo = MFUpdateUtils.checkNewVersion(MFUpdateService.CHECKURL);
                    if (mfInfo == null || mfInfo.getVersionCode() <= MFUpdateService.mfappInfo.getVersionCode()) {
                        if (mfInfo == null || mfInfo.getVersionCode() > MFUpdateService.mfappInfo.getVersionCode()) {
                            updateListener.onFailed(activity);
                        } else {
                            updateListener.onDetectedNothing(activity);
                        }
                    } else if (showUpdateDialog) {
                        activity.runOnUiThread(new UpdateThread(activity, mfInfo, updateListener));
                    } else {
                        updateListener.onDetectedNewVersion(activity, mfInfo.getVersionCode(), mfInfo.getVersionName(), mfInfo.getDescription(), mfInfo.getApkPath());
                    }
                } else if (wifiOnly && -1 == MFNetworkUtils.getWifiIsEnabled(activity)) {
                    updateListener.onWifiOff(activity);
                } else if (!wifiOnly) {
                    MFUpdateAPKInfo mfInfo2 = MFUpdateUtils.checkNewVersion(MFUpdateService.CHECKURL);
                    if (mfInfo2 == null || mfInfo2.getVersionCode() <= MFUpdateService.mfappInfo.getVersionCode()) {
                        if (mfInfo2 == null || mfInfo2.getVersionCode() > MFUpdateService.mfappInfo.getVersionCode()) {
                            updateListener.onFailed(activity);
                        } else {
                            updateListener.onDetectedNothing(activity);
                        }
                    } else if (showUpdateDialog) {
                        activity.runOnUiThread(new UpdateThread(activity, mfInfo2, updateListener));
                    } else {
                        updateListener.onDetectedNewVersion(activity, mfInfo2.getVersionCode(), mfInfo2.getVersionName(), mfInfo2.getDescription(), mfInfo2.getApkPath());
                    }
                }
            }
        }.start();
    }

    public static void check(Activity activity, MFUpdateListener updateListener, boolean showUpdateDialog) {
        check(activity, updateListener, showUpdateDialog, false);
    }

    public static void autoUpdate(final Activity activity, final int appNameID2, final int notificationIconId, final boolean wifiOnly) {
        mfappInfo = MFUpdateUtils.getVersionInfo(activity.getApplicationContext());
        CHECKURL = getCheckUrl(activity.getApplicationContext(), mfappInfo);
        new Thread() {
            public void run() {
                MFUpdateAPKInfo mfInfo;
                if (wifiOnly && 1 == MFNetworkUtils.getNetworkState(activity)) {
                    MFUpdateAPKInfo mfInfo2 = MFUpdateUtils.checkNewVersion(MFUpdateService.CHECKURL);
                    if (mfInfo2 != null && mfInfo2.getVersionCode() > MFUpdateService.mfappInfo.getVersionCode()) {
                        activity.runOnUiThread(new UpdateThread(activity, mfInfo2, appNameID2, notificationIconId));
                    }
                } else if (!wifiOnly && MFNetworkUtils.isNetworkAvailable(activity) && (mfInfo = MFUpdateUtils.checkNewVersion(MFUpdateService.CHECKURL)) != null && mfInfo.getVersionCode() > MFUpdateService.mfappInfo.getVersionCode()) {
                    activity.runOnUiThread(new UpdateThread(activity, mfInfo, appNameID2, notificationIconId));
                }
            }
        }.start();
    }

    public static void autoUpdate(Activity activity, int appNameID2, int notificationIconId) {
        autoUpdate(activity, appNameID2, notificationIconId, false);
    }

    private static String getCheckUrl(Context context, MFAPPInfo mfappInfo2) {
        if (CHECKURL == null) {
            try {
                String mofangAppKey = (String) context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("MOFANG_APPKEY");
                if (mofangAppKey != null && !"".equals(mofangAppKey)) {
                    CHECKURL = "http://m.imofan.com/online_update/?app_key=" + mofangAppKey + "&ver=" + mfappInfo2.getVersionName();
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return CHECKURL;
    }

    private static class UpdateThread implements Runnable {
        /* access modifiers changed from: private */
        public Activity activity;
        /* access modifiers changed from: private */
        public int appNameID;
        /* access modifiers changed from: private */
        public MFUpdateListener mfUpdateListener;
        /* access modifiers changed from: private */
        public int notificationIconResId;
        /* access modifiers changed from: private */
        public MFUpdateAPKInfo updateInfo;

        public UpdateThread(Activity activity2, MFUpdateAPKInfo mfUpdateInfo, int appNameID2, int notificationIconResId2) {
            this.activity = activity2;
            this.updateInfo = mfUpdateInfo;
            this.appNameID = appNameID2;
            this.notificationIconResId = notificationIconResId2;
        }

        public UpdateThread(Activity activity2, MFUpdateAPKInfo mfUpdateInfo, MFUpdateListener listener) {
            this.activity = activity2;
            this.updateInfo = mfUpdateInfo;
            this.mfUpdateListener = listener;
        }

        public void run() {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this.activity);
            alertBuilder.setTitle("软件升级").setPositiveButton("更新", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if (UpdateThread.this.mfUpdateListener != null) {
                        UpdateThread.this.mfUpdateListener.onPerformUpdate(UpdateThread.this.activity, UpdateThread.this.updateInfo.getVersionCode(), UpdateThread.this.updateInfo.getVersionName(), UpdateThread.this.updateInfo.getDescription(), UpdateThread.this.updateInfo.getApkPath());
                        return;
                    }
                    Intent serviceIntent = new Intent(UpdateThread.this.activity, MFUpdateService.class);
                    serviceIntent.putExtra("iconID", UpdateThread.this.notificationIconResId);
                    serviceIntent.putExtra("appNameID", UpdateThread.this.appNameID);
                    serviceIntent.putExtra("downloadUrl", UpdateThread.this.updateInfo.getApkPath());
                    serviceIntent.putExtra("versionCode", UpdateThread.this.updateInfo.getVersionCode());
                    UpdateThread.this.activity.startService(serviceIntent);
                }
            }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            }).setView(MFUpdateUtils.createUpdateDialog(this.activity, this.updateInfo));
            alertBuilder.create().show();
        }
    }

    /* JADX WARN: Type inference failed for: r16v2, types: [java.net.URLConnection] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0037 A[Catch:{ all -> 0x0098 }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x007b A[Catch:{ all -> 0x0098 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00bb A[SYNTHETIC, Splitter:B:35:0x00bb] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long downloadUpdateFile(java.lang.String r24, java.io.File r25, boolean r26) throws java.lang.Exception {
        /*
            r23 = this;
            r4 = 0
            r3 = 0
            r13 = 0
            if (r26 == 0) goto L_0x0017
            r6 = 0
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00a9 }
            r0 = r25
            r7.<init>(r0)     // Catch:{ IOException -> 0x00a9 }
            int r3 = r7.available()     // Catch:{ IOException -> 0x0185, all -> 0x0181 }
            if (r7 == 0) goto L_0x0017
            r7.close()
        L_0x0017:
            r10 = 0
            r11 = 0
            r8 = 0
            java.net.URL r15 = new java.net.URL     // Catch:{ all -> 0x0098 }
            r0 = r24
            r15.<init>(r0)     // Catch:{ all -> 0x0098 }
            java.net.URLConnection r16 = r15.openConnection()     // Catch:{ all -> 0x0098 }
            r0 = r16
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0098 }
            r10 = r0
            java.lang.String r16 = "User-Agent"
            java.lang.String r17 = "PacificHttpClient"
            r0 = r16
            r1 = r17
            r10.setRequestProperty(r0, r1)     // Catch:{ all -> 0x0098 }
            if (r3 <= 0) goto L_0x005b
            java.lang.String r16 = "RANGE"
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ all -> 0x0098 }
            r17.<init>()     // Catch:{ all -> 0x0098 }
            java.lang.String r18 = "bytes="
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0098 }
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r3)     // Catch:{ all -> 0x0098 }
            java.lang.String r18 = "-"
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0098 }
            java.lang.String r17 = r17.toString()     // Catch:{ all -> 0x0098 }
            r0 = r16
            r1 = r17
            r10.setRequestProperty(r0, r1)     // Catch:{ all -> 0x0098 }
        L_0x005b:
            r16 = 10000(0x2710, float:1.4013E-41)
            r0 = r16
            r10.setConnectTimeout(r0)     // Catch:{ all -> 0x0098 }
            r16 = 20000(0x4e20, float:2.8026E-41)
            r0 = r16
            r10.setReadTimeout(r0)     // Catch:{ all -> 0x0098 }
            int r16 = r10.getContentLength()     // Catch:{ all -> 0x0098 }
            com.imofan.android.basic.update.MFUpdateService.updateTotalSize = r16     // Catch:{ all -> 0x0098 }
            int r16 = r10.getResponseCode()     // Catch:{ all -> 0x0098 }
            r17 = 404(0x194, float:5.66E-43)
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x00bb
            java.lang.Exception r16 = new java.lang.Exception     // Catch:{ all -> 0x0098 }
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ all -> 0x0098 }
            r17.<init>()     // Catch:{ all -> 0x0098 }
            java.lang.String r18 = "Cannot find remote file:"
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x0098 }
            r0 = r17
            r1 = r24
            java.lang.StringBuilder r17 = r0.append(r1)     // Catch:{ all -> 0x0098 }
            java.lang.String r17 = r17.toString()     // Catch:{ all -> 0x0098 }
            r16.<init>(r17)     // Catch:{ all -> 0x0098 }
            throw r16     // Catch:{ all -> 0x0098 }
        L_0x0098:
            r16 = move-exception
        L_0x0099:
            if (r10 == 0) goto L_0x009e
            r10.disconnect()
        L_0x009e:
            if (r11 == 0) goto L_0x00a3
            r11.close()
        L_0x00a3:
            if (r8 == 0) goto L_0x00a8
            r8.close()
        L_0x00a8:
            throw r16
        L_0x00a9:
            r5 = move-exception
        L_0x00aa:
            r5.printStackTrace()     // Catch:{ all -> 0x00b4 }
            if (r6 == 0) goto L_0x0017
            r6.close()
            goto L_0x0017
        L_0x00b4:
            r16 = move-exception
        L_0x00b5:
            if (r6 == 0) goto L_0x00ba
            r6.close()
        L_0x00ba:
            throw r16
        L_0x00bb:
            java.io.InputStream r11 = r10.getInputStream()     // Catch:{ all -> 0x0098 }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ all -> 0x0098 }
            r0 = r25
            r1 = r26
            r9.<init>(r0, r1)     // Catch:{ all -> 0x0098 }
            r16 = 4096(0x1000, float:5.74E-42)
            r0 = r16
            byte[] r2 = new byte[r0]     // Catch:{ all -> 0x016d }
            r12 = 0
        L_0x00cf:
            int r12 = r11.read(r2)     // Catch:{ all -> 0x016d }
            if (r12 <= 0) goto L_0x0171
            r16 = 0
            r0 = r16
            r9.write(r2, r0, r12)     // Catch:{ all -> 0x016d }
            long r0 = (long) r12     // Catch:{ all -> 0x016d }
            r16 = r0
            long r13 = r13 + r16
            if (r4 == 0) goto L_0x00fb
            r16 = 100
            long r16 = r16 * r13
            int r18 = com.imofan.android.basic.update.MFUpdateService.updateTotalSize     // Catch:{ all -> 0x016d }
            r0 = r18
            long r0 = (long) r0     // Catch:{ all -> 0x016d }
            r18 = r0
            long r16 = r16 / r18
            r0 = r16
            int r0 = (int) r0     // Catch:{ all -> 0x016d }
            r16 = r0
            int r16 = r16 + -8
            r0 = r16
            if (r0 <= r4) goto L_0x00cf
        L_0x00fb:
            int r4 = r4 + 8
            r0 = r23
            android.app.Notification r0 = r0.updateNotification     // Catch:{ all -> 0x016d }
            r16 = r0
            r0 = r16
            android.widget.RemoteViews r0 = r0.contentView     // Catch:{ all -> 0x016d }
            r16 = r0
            int r17 = com.imofan.android.basic.update.MFUpdateService.processbarViewID     // Catch:{ all -> 0x016d }
            r18 = 100
            r19 = 100
            long r19 = r19 * r13
            int r21 = com.imofan.android.basic.update.MFUpdateService.updateTotalSize     // Catch:{ all -> 0x016d }
            r0 = r21
            long r0 = (long) r0     // Catch:{ all -> 0x016d }
            r21 = r0
            long r19 = r19 / r21
            r0 = r19
            int r0 = (int) r0     // Catch:{ all -> 0x016d }
            r19 = r0
            r20 = 0
            r16.setProgressBar(r17, r18, r19, r20)     // Catch:{ all -> 0x016d }
            r0 = r23
            android.app.Notification r0 = r0.updateNotification     // Catch:{ all -> 0x016d }
            r16 = r0
            r0 = r16
            android.widget.RemoteViews r0 = r0.contentView     // Catch:{ all -> 0x016d }
            r16 = r0
            int r17 = com.imofan.android.basic.update.MFUpdateService.textViewID     // Catch:{ all -> 0x016d }
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ all -> 0x016d }
            r18.<init>()     // Catch:{ all -> 0x016d }
            r19 = 100
            long r19 = r19 * r13
            int r21 = com.imofan.android.basic.update.MFUpdateService.updateTotalSize     // Catch:{ all -> 0x016d }
            r0 = r21
            long r0 = (long) r0     // Catch:{ all -> 0x016d }
            r21 = r0
            long r19 = r19 / r21
            r0 = r19
            int r0 = (int) r0     // Catch:{ all -> 0x016d }
            r19 = r0
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ all -> 0x016d }
            java.lang.String r19 = "%"
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ all -> 0x016d }
            java.lang.String r18 = r18.toString()     // Catch:{ all -> 0x016d }
            r16.setTextViewText(r17, r18)     // Catch:{ all -> 0x016d }
            r0 = r23
            android.app.NotificationManager r0 = r0.updateNotificationManager     // Catch:{ all -> 0x016d }
            r16 = r0
            int r17 = com.imofan.android.basic.update.MFUpdateService.iconID     // Catch:{ all -> 0x016d }
            r0 = r23
            android.app.Notification r0 = r0.updateNotification     // Catch:{ all -> 0x016d }
            r18 = r0
            r16.notify(r17, r18)     // Catch:{ all -> 0x016d }
            goto L_0x00cf
        L_0x016d:
            r16 = move-exception
            r8 = r9
            goto L_0x0099
        L_0x0171:
            if (r10 == 0) goto L_0x0176
            r10.disconnect()
        L_0x0176:
            if (r11 == 0) goto L_0x017b
            r11.close()
        L_0x017b:
            if (r9 == 0) goto L_0x0180
            r9.close()
        L_0x0180:
            return r13
        L_0x0181:
            r16 = move-exception
            r6 = r7
            goto L_0x00b5
        L_0x0185:
            r5 = move-exception
            r6 = r7
            goto L_0x00aa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imofan.android.basic.update.MFUpdateService.downloadUpdateFile(java.lang.String, java.io.File, boolean):long");
    }

    private void createAPKStorageFile() {
        File[] files;
        internalFileDir = getDir("update", 0);
        if (!(internalFileDir == null || !internalFileDir.isDirectory() || (files = internalFileDir.listFiles()) == null)) {
            for (File delete : files) {
                MFUpdateUtils.delete(delete);
            }
        }
        updateFile = new File(internalFileDir.getAbsolutePath(), File.separator + mfappInfo.getPackageName() + "_" + updateApkVersionCode + ".apk");
    }
}
