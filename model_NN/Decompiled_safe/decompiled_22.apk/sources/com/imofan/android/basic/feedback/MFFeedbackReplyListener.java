package com.imofan.android.basic.feedback;

import java.util.List;

public interface MFFeedbackReplyListener {
    void onDetectedNewReplies(List<MFFeedback> list);

    void onDetectedNothing();
}
