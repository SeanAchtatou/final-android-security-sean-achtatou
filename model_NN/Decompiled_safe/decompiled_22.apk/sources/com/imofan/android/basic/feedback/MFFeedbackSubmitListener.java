package com.imofan.android.basic.feedback;

public interface MFFeedbackSubmitListener {
    void onSubmitFailed();

    void onSubmitSucceeded(MFFeedback mFFeedback);
}
