package com.imofan.android.basic.notification;

import android.content.Context;

public class MFParameter {
    private long appId;
    private String devId;
    private long md5;
    private long timeStamp = 0;

    public long getAppId() {
        return this.appId;
    }

    public void setAppId(long appId2) {
        this.appId = appId2;
    }

    public String getDevId() {
        return this.devId;
    }

    public void setDevId(String devId2) {
        this.devId = devId2;
    }

    public long getMd5() {
        return this.md5;
    }

    public void setMd5(long md52) {
        this.md5 = md52;
    }

    public long getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(long timeStamp2) {
        this.timeStamp = timeStamp2;
    }

    public static long getMd5Verify(Context context, long appId2, long timeStamp2) {
        return timeStamp2 % appId2;
    }
}
