package com.imofan.android.basic.notification;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesUtils {
    public static void setPreferences(Context context, String preference, String key, boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(preference, 0).edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void setPreferences(Context context, String preference, String key, long value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(preference, 0).edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static void setPreferences(Context context, String preference, String key, int value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(preference, 0).edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void setPreferences(Context context, String preference, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(preference, 0).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPreference(Context context, String preference, String key, String defaultValue) {
        return context.getSharedPreferences(preference, 0).getString(key, defaultValue);
    }

    public static boolean getPreference(Context context, String preference, String key, boolean defaultValue) {
        return context.getSharedPreferences(preference, 0).getBoolean(key, defaultValue);
    }

    public static long getPreference(Context context, String preference, String key, long defaultValue) {
        return context.getSharedPreferences(preference, 0).getLong(key, defaultValue);
    }

    public static int getPreference(Context context, String preference, String key, int defaultValue) {
        return context.getSharedPreferences(preference, 0).getInt(key, defaultValue);
    }
}
