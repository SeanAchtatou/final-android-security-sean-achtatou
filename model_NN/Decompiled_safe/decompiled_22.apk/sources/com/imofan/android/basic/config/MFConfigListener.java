package com.imofan.android.basic.config;

import android.content.Context;
import java.util.Map;

public interface MFConfigListener {
    void onChanged(Context context, Map<String, String> map);

    void onFailed(Context context);

    void onReceived(Context context, Map<String, String> map);
}
