package com.viewpagerindicator;

public final class R {

    public static final class attr {
        public static final int centered = 2130771974;
        public static final int clipPadding = 2130771985;
        public static final int fadeDelay = 2130771997;
        public static final int fadeLength = 2130771998;
        public static final int fades = 2130771996;
        public static final int fillColor = 2130771978;
        public static final int footerColor = 2130771986;
        public static final int footerIndicatorHeight = 2130771989;
        public static final int footerIndicatorStyle = 2130771988;
        public static final int footerIndicatorUnderlinePadding = 2130771990;
        public static final int footerLineHeight = 2130771987;
        public static final int footerPadding = 2130771991;
        public static final int gapWidth = 2130771984;
        public static final int linePosition = 2130771992;
        public static final int lineWidth = 2130771983;
        public static final int pageColor = 2130771979;
        public static final int radius = 2130771980;
        public static final int selectedBold = 2130771993;
        public static final int selectedColor = 2130771975;
        public static final int snap = 2130771981;
        public static final int strokeColor = 2130771982;
        public static final int strokeWidth = 2130771976;
        public static final int titlePadding = 2130771994;
        public static final int topPadding = 2130771995;
        public static final int unselectedColor = 2130771977;
        public static final int vpiCirclePageIndicatorStyle = 2130771968;
        public static final int vpiIconPageIndicatorStyle = 2130771969;
        public static final int vpiLinePageIndicatorStyle = 2130771970;
        public static final int vpiTabPageIndicatorStyle = 2130771972;
        public static final int vpiTitlePageIndicatorStyle = 2130771971;
        public static final int vpiUnderlinePageIndicatorStyle = 2130771973;
    }

    public static final class bool {
        public static final int default_circle_indicator_centered = 2131361792;
        public static final int default_circle_indicator_snap = 2131361793;
        public static final int default_line_indicator_centered = 2131361794;
        public static final int default_title_indicator_selected_bold = 2131361795;
        public static final int default_underline_indicator_fades = 2131361796;
    }

    public static final class color {
        public static final int default_circle_indicator_fill_color = 2131296264;
        public static final int default_circle_indicator_page_color = 2131296265;
        public static final int default_circle_indicator_stroke_color = 2131296266;
        public static final int default_line_indicator_selected_color = 2131296267;
        public static final int default_line_indicator_unselected_color = 2131296268;
        public static final int default_title_indicator_footer_color = 2131296269;
        public static final int default_title_indicator_selected_color = 2131296270;
        public static final int default_title_indicator_text_color = 2131296271;
        public static final int default_underline_indicator_selected_color = 2131296272;
        public static final int vpi__background_holo_dark = 2131296256;
        public static final int vpi__background_holo_light = 2131296257;
        public static final int vpi__bright_foreground_disabled_holo_dark = 2131296260;
        public static final int vpi__bright_foreground_disabled_holo_light = 2131296261;
        public static final int vpi__bright_foreground_holo_dark = 2131296258;
        public static final int vpi__bright_foreground_holo_light = 2131296259;
        public static final int vpi__bright_foreground_inverse_holo_dark = 2131296262;
        public static final int vpi__bright_foreground_inverse_holo_light = 2131296263;
        public static final int vpi__dark_theme = 2131296339;
        public static final int vpi__light_theme = 2131296340;
    }

    public static final class dimen {
        public static final int default_circle_indicator_radius = 2131492864;
        public static final int default_circle_indicator_stroke_width = 2131492865;
        public static final int default_line_indicator_gap_width = 2131492867;
        public static final int default_line_indicator_line_width = 2131492866;
        public static final int default_line_indicator_stroke_width = 2131492868;
        public static final int default_title_indicator_clip_padding = 2131492869;
        public static final int default_title_indicator_footer_indicator_height = 2131492871;
        public static final int default_title_indicator_footer_indicator_underline_padding = 2131492872;
        public static final int default_title_indicator_footer_line_height = 2131492870;
        public static final int default_title_indicator_footer_padding = 2131492873;
        public static final int default_title_indicator_text_size = 2131492874;
        public static final int default_title_indicator_title_padding = 2131492875;
        public static final int default_title_indicator_top_padding = 2131492876;
    }

    public static final class drawable {
        public static final int vpi__tab_indicator = 2130837935;
        public static final int vpi__tab_selected_focused_holo = 2130837936;
        public static final int vpi__tab_selected_holo = 2130837937;
        public static final int vpi__tab_selected_pressed_holo = 2130837938;
        public static final int vpi__tab_unselected_focused_holo = 2130837939;
        public static final int vpi__tab_unselected_holo = 2130837940;
        public static final int vpi__tab_unselected_pressed_holo = 2130837941;
    }

    public static final class id {
        public static final int bottom = 2131230723;
        public static final int none = 2131230720;
        public static final int top = 2131230724;
        public static final int triangle = 2131230721;
        public static final int underline = 2131230722;
    }

    public static final class integer {
        public static final int default_circle_indicator_orientation = 2131427328;
        public static final int default_title_indicator_footer_indicator_style = 2131427329;
        public static final int default_title_indicator_line_position = 2131427330;
        public static final int default_underline_indicator_fade_delay = 2131427331;
        public static final int default_underline_indicator_fade_length = 2131427332;
    }

    public static final class style {
        public static final int TextAppearance_TabPageIndicator = 2131558403;
        public static final int Theme_PageIndicatorDefaults = 2131558400;
        public static final int Widget = 2131558401;
        public static final int Widget_IconPageIndicator = 2131558404;
        public static final int Widget_TabPageIndicator = 2131558402;
    }

    public static final class styleable {
        public static final int[] CirclePageIndicator = {16842948, 16842964, com.pengyou.citycommercialarea.R.attr.centered, com.pengyou.citycommercialarea.R.attr.strokeWidth, com.pengyou.citycommercialarea.R.attr.fillColor, com.pengyou.citycommercialarea.R.attr.pageColor, com.pengyou.citycommercialarea.R.attr.radius, com.pengyou.citycommercialarea.R.attr.snap, com.pengyou.citycommercialarea.R.attr.strokeColor};
        public static final int CirclePageIndicator_android_background = 1;
        public static final int CirclePageIndicator_android_orientation = 0;
        public static final int CirclePageIndicator_centered = 2;
        public static final int CirclePageIndicator_fillColor = 4;
        public static final int CirclePageIndicator_pageColor = 5;
        public static final int CirclePageIndicator_radius = 6;
        public static final int CirclePageIndicator_snap = 7;
        public static final int CirclePageIndicator_strokeColor = 8;
        public static final int CirclePageIndicator_strokeWidth = 3;
        public static final int[] LinePageIndicator = {16842964, com.pengyou.citycommercialarea.R.attr.centered, com.pengyou.citycommercialarea.R.attr.selectedColor, com.pengyou.citycommercialarea.R.attr.strokeWidth, com.pengyou.citycommercialarea.R.attr.unselectedColor, com.pengyou.citycommercialarea.R.attr.lineWidth, com.pengyou.citycommercialarea.R.attr.gapWidth};
        public static final int LinePageIndicator_android_background = 0;
        public static final int LinePageIndicator_centered = 1;
        public static final int LinePageIndicator_gapWidth = 6;
        public static final int LinePageIndicator_lineWidth = 5;
        public static final int LinePageIndicator_selectedColor = 2;
        public static final int LinePageIndicator_strokeWidth = 3;
        public static final int LinePageIndicator_unselectedColor = 4;
        public static final int[] TitlePageIndicator = {16842901, 16842904, 16842964, com.pengyou.citycommercialarea.R.attr.selectedColor, com.pengyou.citycommercialarea.R.attr.clipPadding, com.pengyou.citycommercialarea.R.attr.footerColor, com.pengyou.citycommercialarea.R.attr.footerLineHeight, com.pengyou.citycommercialarea.R.attr.footerIndicatorStyle, com.pengyou.citycommercialarea.R.attr.footerIndicatorHeight, com.pengyou.citycommercialarea.R.attr.footerIndicatorUnderlinePadding, com.pengyou.citycommercialarea.R.attr.footerPadding, com.pengyou.citycommercialarea.R.attr.linePosition, com.pengyou.citycommercialarea.R.attr.selectedBold, com.pengyou.citycommercialarea.R.attr.titlePadding, com.pengyou.citycommercialarea.R.attr.topPadding};
        public static final int TitlePageIndicator_android_background = 2;
        public static final int TitlePageIndicator_android_textColor = 1;
        public static final int TitlePageIndicator_android_textSize = 0;
        public static final int TitlePageIndicator_clipPadding = 4;
        public static final int TitlePageIndicator_footerColor = 5;
        public static final int TitlePageIndicator_footerIndicatorHeight = 8;
        public static final int TitlePageIndicator_footerIndicatorStyle = 7;
        public static final int TitlePageIndicator_footerIndicatorUnderlinePadding = 9;
        public static final int TitlePageIndicator_footerLineHeight = 6;
        public static final int TitlePageIndicator_footerPadding = 10;
        public static final int TitlePageIndicator_linePosition = 11;
        public static final int TitlePageIndicator_selectedBold = 12;
        public static final int TitlePageIndicator_selectedColor = 3;
        public static final int TitlePageIndicator_titlePadding = 13;
        public static final int TitlePageIndicator_topPadding = 14;
        public static final int[] UnderlinePageIndicator = {16842964, com.pengyou.citycommercialarea.R.attr.selectedColor, com.pengyou.citycommercialarea.R.attr.fades, com.pengyou.citycommercialarea.R.attr.fadeDelay, com.pengyou.citycommercialarea.R.attr.fadeLength};
        public static final int UnderlinePageIndicator_android_background = 0;
        public static final int UnderlinePageIndicator_fadeDelay = 3;
        public static final int UnderlinePageIndicator_fadeLength = 4;
        public static final int UnderlinePageIndicator_fades = 2;
        public static final int UnderlinePageIndicator_selectedColor = 1;
        public static final int[] ViewPagerIndicator = {com.pengyou.citycommercialarea.R.attr.vpiCirclePageIndicatorStyle, com.pengyou.citycommercialarea.R.attr.vpiIconPageIndicatorStyle, com.pengyou.citycommercialarea.R.attr.vpiLinePageIndicatorStyle, com.pengyou.citycommercialarea.R.attr.vpiTitlePageIndicatorStyle, com.pengyou.citycommercialarea.R.attr.vpiTabPageIndicatorStyle, com.pengyou.citycommercialarea.R.attr.vpiUnderlinePageIndicatorStyle};
        public static final int ViewPagerIndicator_vpiCirclePageIndicatorStyle = 0;
        public static final int ViewPagerIndicator_vpiIconPageIndicatorStyle = 1;
        public static final int ViewPagerIndicator_vpiLinePageIndicatorStyle = 2;
        public static final int ViewPagerIndicator_vpiTabPageIndicatorStyle = 4;
        public static final int ViewPagerIndicator_vpiTitlePageIndicatorStyle = 3;
        public static final int ViewPagerIndicator_vpiUnderlinePageIndicatorStyle = 5;
    }
}
