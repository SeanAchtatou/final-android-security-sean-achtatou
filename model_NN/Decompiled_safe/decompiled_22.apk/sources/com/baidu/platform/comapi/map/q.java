package com.baidu.platform.comapi.map;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Message;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import com.baidu.mapapi.map.Overlay;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.d.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.httpclient.HttpStatus;

public class q extends GLSurfaceView implements GestureDetector.OnGestureListener {
    static v a = null;
    static e b = null;
    public static int d = 0;
    private static o n = null;
    private static int o = 0;
    public MapRenderer c;
    t e = null;
    a f = null;
    c g = null;
    y h = null;
    GestureDetector i = new GestureDetector(this);
    List<Overlay> j = new a();
    boolean k = false;
    private boolean l = false;
    private boolean m = false;
    /* access modifiers changed from: private */
    public b p = null;
    private int q = 0;
    private int r = 0;

    private class a<E> extends ArrayList<E> {
        private a() {
        }

        public boolean add(E e) {
            if (e == null) {
                return false;
            }
            if (q.this.p != null) {
                q.this.p.a(e);
            }
            return super.add(e);
        }

        public boolean addAll(Collection<? extends E> collection) {
            for (Object add : collection) {
                add(add);
            }
            return true;
        }

        public void clear() {
            Iterator it = iterator();
            while (it.hasNext()) {
                Object next = it.next();
                it.remove();
                remove(next);
            }
        }

        public E remove(int i) {
            E e = get(i);
            if (super.remove(e)) {
                return e;
            }
            return null;
        }

        public boolean remove(Object obj) {
            if (obj == null) {
                return false;
            }
            if (q.this.p != null) {
                q.this.p.b(obj);
            }
            return super.remove(obj);
        }
    }

    public interface b {
        void a(Object obj);

        void b(Object obj);
    }

    public q(Context context) {
        super(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.b(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.b(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.b(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    private void o() {
        com.baidu.platform.comjni.map.basemap.a b2 = n.b();
        if (b2 != null) {
            int a2 = b2.a(0, 0, "compass");
            if (a2 > 0) {
                d = a2;
                b2.b(a2, true);
                b2.a(a2, true);
                a.a(a2, b.a());
            }
            int a3 = b2.a(0, 0, "logo");
            if (a3 > 0) {
                b2.b(a3, true);
                b2.a(a3, false);
                n.c = a3;
                n.g.put("logo", Integer.valueOf(a3));
            }
            int a4 = b2.a(0, 0, "popup");
            if (a4 > 0) {
                b2.b(a4, true);
                b2.a(a4, false);
                n.a = a4;
                n.e.put("popup", Integer.valueOf(a4));
            }
        }
    }

    public float a(com.baidu.platform.comapi.basestruct.b bVar) {
        if (n == null || n.b() == null) {
            return 3.0f;
        }
        if (bVar.a.a == bVar.b.a || bVar.a.b == bVar.b.b) {
            return 18.0f;
        }
        int abs = Math.abs(bVar.b.a - bVar.a.a);
        int abs2 = Math.abs(bVar.a.b - bVar.b.b);
        c.g();
        c.i();
        int width = getWidth() / 4;
        int height = getHeight() / 4;
        if (width <= 0 || height <= 0) {
            return 18.0f;
        }
        int i2 = 0;
        for (int i3 = abs; i3 > width; i3 >>= 1) {
            i2++;
        }
        int i4 = 0;
        int i5 = abs2;
        while (i5 > height) {
            i5 >>= 1;
            i4++;
        }
        float max = (float) (20 - Math.max(i2, i4));
        if (max < 3.0f) {
            return 3.0f;
        }
        if (max > 19.0f) {
            return 19.0f;
        }
        return max;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.b(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.b(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.b(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    public int a(String str) {
        com.baidu.platform.comjni.map.basemap.a b2 = n.b();
        if (b2 == null) {
            return 0;
        }
        int a2 = str.equals("location") ? b2.a(8, 1000, "location") : b2.a(0, 0, str);
        if (a2 > 0) {
            b2.b(a2, true);
            b2.a(a2, false);
        }
        return a2;
    }

    public void a() {
        o--;
        if (o == 0) {
            n.m();
            n = null;
            a = null;
            b = null;
        }
        this.c = null;
    }

    public void a(int i2) {
        a.a(i2);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        n.a((this.q / 2) + i2, (this.r / 2) + i3);
    }

    public void a(int i2, d dVar) {
        a.a(i2, dVar);
    }

    public void a(Bundle bundle, Context context) {
        if (n == null && a == null) {
            n = new o(context, this);
            a = new v(n);
            if (n != null) {
                n.a(bundle, a);
            }
            o();
            b = new e(n);
        }
        o++;
        if (n != null) {
            n.a(this);
            this.c = new MapRenderer(n.a());
            setRenderer(this.c);
            com.baidu.platform.comjni.map.basemap.a b2 = n.b();
            if (b2 != null) {
                b2.a(bundle);
            }
        }
        setLongClickable(false);
        setFocusable(false);
    }

    /* access modifiers changed from: package-private */
    public void a(GeoPoint geoPoint, Message message, Runnable runnable) {
        s k2 = n.k();
        k2.d = geoPoint.getLongitudeE6();
        k2.e = geoPoint.getLatitudeE6();
        n.a(k2, (int) HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    public void a(a aVar) {
        this.f = aVar;
    }

    public void a(b bVar) {
        this.p = bVar;
    }

    public void a(s sVar) {
        n.a(sVar);
    }

    public void a(t tVar) {
        this.e = tVar;
    }

    public void a(boolean z) {
        this.m = z;
        com.baidu.platform.comjni.map.basemap.a b2 = n.b();
        if (b2 != null) {
            b2.a(this.m);
        }
    }

    public boolean a(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        this.i.onTouchEvent(motionEvent);
        if (n != null) {
            return n.a(motionEvent);
        }
        return false;
    }

    public o b() {
        return n;
    }

    public void b(boolean z) {
        this.l = z;
        com.baidu.platform.comjni.map.basemap.a b2 = n.b();
        if (b2 != null) {
            b2.b(this.l);
        }
    }

    public int c() {
        e eVar = (e) f();
        return Math.abs(eVar.fromPixels(0, 0).getLatitudeE6() - eVar.fromPixels(this.q - 1, this.r - 1).getLatitudeE6());
    }

    public int d() {
        e eVar = (e) f();
        return Math.abs(eVar.fromPixels(this.q - 1, this.r - 1).getLongitudeE6() - eVar.fromPixels(0, 0).getLongitudeE6());
    }

    public List<Overlay> e() {
        return this.j;
    }

    public Projection f() {
        return b;
    }

    public boolean g() {
        return this.m;
    }

    public boolean h() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public double i() {
        return Math.pow(2.0d, (double) (18.0f - k()));
    }

    public GeoPoint j() {
        s k2 = n.k();
        return new GeoPoint(k2.e, k2.d);
    }

    public float k() {
        return n.l();
    }

    public int l() {
        return n.k().b;
    }

    public int m() {
        return n.k().c;
    }

    public s n() {
        return n.k();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (n == null || !n.d()) {
            return false;
        }
        return n.a(motionEvent, motionEvent2, f2, f3);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return (i2 == 21 || i2 == 29) ? n.a(1, 18, 0) == 1 : (i2 == 19 || i2 == 51) ? n.a(1, 19, 0) == 1 : (i2 == 20 || i2 == 47) ? n.a(1, 17, 0) == 1 : (i2 == 22 || i2 == 32) && n.a(1, 16, 0) == 1;
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        return false;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public void onPause() {
        if (n != null) {
            n.b().d();
        }
        super.onPause();
    }

    public void onResume() {
        if (n != null) {
            n.a(this);
            n.b().f();
            n.b().e();
        }
        super.onResume();
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        MapRenderer.a = i3;
        MapRenderer.b = i4;
        this.q = i3;
        this.r = i4;
        MapRenderer.c = 0;
        super.surfaceChanged(surfaceHolder, i2, i3, i4);
        s n2 = n();
        n2.f.a = 0;
        n2.f.c = 0;
        n2.f.d = i4;
        n2.f.b = i3;
        a(n2);
        n.d(this.q, this.r);
    }
}
