package com.baidu.platform.comapi.d;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.baidu.mapapi.VersionInfo;
import com.baidu.platform.comjni.map.commonmemcache.a;
import com.palmtrends.ad.ClientShowAd;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Random;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class c {
    public static float A = 1.0f;
    private static boolean B = true;
    private static int C = 0;
    private static int D = 0;
    static a a = new a();
    static String b = "02";
    static String c;
    static String d;
    static String e;
    static String f;
    static int g;
    static int h;
    static int i;
    static int j;
    static int k;
    static int l;
    static String m;
    static String n;
    static String o;
    static String p;
    static String q;
    static String r;
    static String s = "baidu";
    static String t = "baidu";
    static String u = "";
    static String v = "";
    static String w = "";
    static String x;
    public static Context y;
    public static final int z = Integer.parseInt(Build.VERSION.SDK);

    public static String a() {
        Random random = new Random();
        StringBuffer stringBuffer = new StringBuffer(10);
        for (int i2 = 0; i2 < 10; i2++) {
            stringBuffer.append(random.nextInt(10));
        }
        return stringBuffer.toString();
    }

    public static String a(Context context) {
        Exception e2;
        String str;
        try {
            if (!B) {
                str = a();
                try {
                    FileOutputStream openFileOutput = context.openFileOutput("imei.dat", 32768);
                    openFileOutput.write(("|" + str).getBytes());
                    openFileOutput.close();
                    return str;
                } catch (Exception e3) {
                    e2 = e3;
                }
            } else {
                FileInputStream openFileInput = context.openFileInput("imei.dat");
                byte[] bArr = new byte[openFileInput.available()];
                openFileInput.read(bArr);
                openFileInput.close();
                String str2 = new String(bArr);
                try {
                    return str2.substring(str2.indexOf(124) + 1);
                } catch (Exception e4) {
                    e2 = e4;
                    str = str2;
                }
            }
        } catch (Exception e5) {
            Exception exc = e5;
            str = null;
            e2 = exc;
            e2.printStackTrace();
            return str;
        }
    }

    private static void a(String str) {
        v = str;
    }

    public static void a(String str, String str2) {
        b(str);
        a(str2);
        e();
    }

    public static void b() {
        d();
    }

    private static void b(String str) {
        w = str;
    }

    public static byte[] b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toByteArray();
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static Bundle c() {
        Bundle bundle = new Bundle();
        bundle.putString("cpu", u);
        bundle.putString("resid", b);
        bundle.putString("channel", s);
        bundle.putString("glr", v);
        bundle.putString("glv", w);
        bundle.putString("mb", f());
        bundle.putString("sv", h());
        bundle.putString("os", j());
        bundle.putInt("dpi_x", m());
        bundle.putInt("dpi_y", m());
        bundle.putString("net", p);
        bundle.putString("im", j(y));
        bundle.putString("imrand", a(y));
        bundle.putByteArray("signature", b(y));
        bundle.putString("pcn", y.getPackageName());
        bundle.putInt("screen_x", g());
        bundle.putInt("screen_y", i());
        return bundle;
    }

    public static void c(Context context) {
        y = context;
        x = context.getFilesDir().getAbsolutePath();
        d = Build.MODEL;
        e = "Android" + Build.VERSION.SDK;
        c = context.getPackageName();
        e(context);
        f(context);
        g(context);
        h(context);
        i(context);
        l(context);
        t();
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            C = locationManager.isProviderEnabled("gps") ? 1 : 0;
            D = locationManager.isProviderEnabled("network") ? 1 : 0;
        } catch (Exception e2) {
        }
    }

    public static void d() {
        if (a != null) {
            a.b();
        }
    }

    public static void d(Context context) {
        boolean z2 = true;
        try {
            File file = new File(x);
            if (!file.exists()) {
                file.mkdirs();
            }
            File file2 = new File(x + "/ver.dat");
            byte[] bArr = {2, 1, 2, 0, 0, 0};
            if (file2.exists()) {
                FileInputStream fileInputStream = new FileInputStream(file2);
                byte[] bArr2 = new byte[fileInputStream.available()];
                fileInputStream.read(bArr2);
                fileInputStream.close();
                if (Arrays.equals(bArr2, bArr)) {
                    z2 = false;
                }
            }
            AssetManager assets = context.getAssets();
            File file3 = new File(x + "/channel");
            if (!file3.exists()) {
                InputStream open = assets.open("channel");
                byte[] bArr3 = new byte[open.available()];
                open.read(bArr3);
                s = new String(bArr3).trim();
                byte[] bytes = new String(bArr3).trim().getBytes();
                open.close();
                file3.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file3);
                fileOutputStream.write(bytes);
                fileOutputStream.close();
            } else {
                FileInputStream fileInputStream2 = new FileInputStream(file3);
                byte[] bArr4 = new byte[fileInputStream2.available()];
                fileInputStream2.read(bArr4);
                s = new String(bArr4).trim();
                fileInputStream2.close();
            }
            File file4 = new File(x + "/oem");
            if (!file4.exists()) {
                InputStream open2 = assets.open("oem");
                byte[] bArr5 = new byte[open2.available()];
                open2.read(bArr5);
                t = new String(bArr5).trim();
                byte[] bytes2 = new String(bArr5).trim().getBytes();
                open2.close();
                file4.createNewFile();
                FileOutputStream fileOutputStream2 = new FileOutputStream(file4);
                fileOutputStream2.write(bytes2);
                fileOutputStream2.close();
            } else {
                FileInputStream fileInputStream3 = new FileInputStream(file4);
                byte[] bArr6 = new byte[fileInputStream3.available()];
                fileInputStream3.read(bArr6);
                t = new String(bArr6).trim();
                fileInputStream3.close();
            }
            if (z2) {
                if (file2.exists()) {
                    file2.delete();
                }
                file2.createNewFile();
                FileOutputStream fileOutputStream3 = new FileOutputStream(file2);
                fileOutputStream3.write(bArr);
                fileOutputStream3.close();
                File file5 = new File(x + "/cfg/h");
                if (!file5.exists()) {
                    file5.mkdirs();
                }
                File file6 = new File(x + "/cfg/l");
                if (!file6.exists()) {
                    file6.mkdirs();
                }
                String[] strArr = {"CMRequire.dat", "VerDatset.dat", "cfg/h/ResPack.cfg", "cfg/l/ResPack.cfg", "cfg/h/DVHotcity.cfg", "cfg/l/DVHotcity.cfg", "cfg/l/mapstyle.sty", "cfg/l/satellitestyle.sty", "cfg/l/trafficstyle.sty", "cfg/l/DVDirectory.cfg", "cfg/l/DVVersion.cfg", "cfg/h/mapstyle.sty", "cfg/h/satellitestyle.sty", "cfg/h/trafficstyle.sty", "cfg/h/DVDirectory.cfg", "cfg/h/DVVersion.cfg"};
                String[] strArr2 = {"CMRequire.dat", "VerDatset.dat", "cfg/h/ResPack.rs", "cfg/l/ResPack.rs", "cfg/h/DVHotcity.cfg", "cfg/l/DVHotcity.cfg", "cfg/l/mapstyle.sty", "cfg/l/satellitestyle.sty", "cfg/l/trafficstyle.sty", "cfg/l/DVDirectory.cfg", "cfg/l/DVVersion.cfg", "cfg/h/mapstyle.sty", "cfg/h/satellitestyle.sty", "cfg/h/trafficstyle.sty", "cfg/h/DVDirectory.cfg", "cfg/h/DVVersion.cfg"};
                for (int i2 = 0; i2 < strArr2.length; i2++) {
                    InputStream open3 = assets.open(strArr[i2]);
                    byte[] bArr7 = new byte[open3.available()];
                    open3.read(bArr7);
                    open3.close();
                    File file7 = new File(x + CookieSpec.PATH_DELIM + strArr2[i2]);
                    if (file7.exists()) {
                        file7.delete();
                    }
                    file7.createNewFile();
                    FileOutputStream fileOutputStream4 = new FileOutputStream(file7);
                    fileOutputStream4.write(bArr7);
                    fileOutputStream4.close();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void e() {
        Bundle bundle = new Bundle();
        bundle.putString("cpu", u);
        bundle.putString("resid", b);
        bundle.putString("channel", s);
        bundle.putString("glr", v);
        bundle.putString("glv", w);
        bundle.putString("mb", f());
        bundle.putString("sv", h());
        bundle.putString("os", j());
        bundle.putInt("dpi_x", m());
        bundle.putInt("dpi_y", m());
        bundle.putString("net", p);
        bundle.putString("im", j(y));
        bundle.putString("imrand", a(y));
        bundle.putString("pcn", y.getPackageName());
        bundle.putInt("screen_x", g());
        bundle.putInt("screen_y", i());
        if (a != null) {
            a.a(bundle);
        }
    }

    private static void e(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            f = VersionInfo.getApiVersion();
            g = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            f = ClientShowAd.adv;
            g = 1;
        }
    }

    public static String f() {
        return d;
    }

    private static void f(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Display defaultDisplay = windowManager != null ? windowManager.getDefaultDisplay() : null;
        if (defaultDisplay != null) {
            h = defaultDisplay.getWidth();
            i = defaultDisplay.getHeight();
            defaultDisplay.getMetrics(displayMetrics);
        }
        A = displayMetrics.density;
        j = (int) displayMetrics.xdpi;
        k = (int) displayMetrics.ydpi;
        if (z > 3) {
            l = displayMetrics.densityDpi;
        } else {
            l = 160;
        }
        if (l == 0) {
            l = 160;
        }
    }

    public static int g() {
        return h;
    }

    private static void g(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            m = j(context);
            n = telephonyManager.getSubscriberId();
            o = a(context);
        }
    }

    public static String h() {
        return f;
    }

    private static void h(Context context) {
        q = Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static int i() {
        return i;
    }

    private static void i(Context context) {
        if (((TelephonyManager) context.getSystemService("phone")) != null) {
            r = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        }
    }

    public static String j() {
        return e;
    }

    private static String j(Context context) {
        Exception e2;
        String str;
        try {
            if (!new File(context.getFilesDir().getAbsolutePath() + CookieSpec.PATH_DELIM + "imei.dat").exists()) {
                B = false;
                str = k(context);
                try {
                    FileOutputStream openFileOutput = context.openFileOutput("imei.dat", 32768);
                    openFileOutput.write(str.getBytes());
                    openFileOutput.close();
                    return str;
                } catch (Exception e3) {
                    e2 = e3;
                }
            } else {
                B = true;
                FileInputStream openFileInput = context.openFileInput("imei.dat");
                byte[] bArr = new byte[openFileInput.available()];
                openFileInput.read(bArr);
                openFileInput.close();
                String str2 = new String(bArr);
                try {
                    return str2.substring(0, str2.indexOf(124));
                } catch (Exception e4) {
                    e2 = e4;
                    str = str2;
                }
            }
        } catch (Exception e5) {
            Exception exc = e5;
            str = null;
            e2 = exc;
            e2.printStackTrace();
            return str;
        }
    }

    public static int k() {
        return j;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0015 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String k(android.content.Context r2) {
        /*
            r1 = 0
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r2.getSystemService(r0)     // Catch:{ Exception -> 0x0018 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x0018 }
            if (r0 == 0) goto L_0x001c
            java.lang.String r0 = r0.getDeviceId()     // Catch:{ Exception -> 0x0018 }
        L_0x000f:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x0017
            java.lang.String r0 = "000000000000000"
        L_0x0017:
            return r0
        L_0x0018:
            r0 = move-exception
            r0.printStackTrace()
        L_0x001c:
            r0 = r1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.platform.comapi.d.c.k(android.content.Context):java.lang.String");
    }

    public static int l() {
        return k;
    }

    private static void l(Context context) {
        p = a.a(context);
    }

    public static int m() {
        return l;
    }

    public static String n() {
        return m;
    }

    public static String o() {
        return s;
    }

    public static String p() {
        return c;
    }

    public static String q() {
        return x;
    }

    public static String r() {
        return v;
    }

    public static String s() {
        return w;
    }

    private static void t() {
        if (a != null) {
            a.a();
        }
    }
}
