package com.baidu.platform.comapi.b;

import android.os.Message;
import org.apache.commons.httpclient.HttpStatus;

class d {
    private c a = null;
    private a b = null;
    private e c = null;

    d() {
    }

    public void a() {
        this.c = null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void a(Message message) {
        if (message.what == 2000) {
            if (message.arg1 == 4) {
                if (this.b == null) {
                    return;
                }
                if (message.arg2 != 0) {
                    this.b.a(null, message.arg2);
                    return;
                }
            } else if (this.a == null) {
                return;
            } else {
                if (message.arg2 != 0) {
                    this.a.a(message.arg2);
                    return;
                }
            }
            switch (message.arg1) {
                case 2:
                    if (message.arg2 == 0) {
                        String b2 = this.c.b(2);
                        if (b2 == null || b2.equals("")) {
                            this.a.e(null);
                            return;
                        } else {
                            this.a.e(b2);
                            return;
                        }
                    } else {
                        return;
                    }
                case 4:
                    if (message.arg2 == 0) {
                        String b3 = this.c.b(message.arg1);
                        if (b3 == null || b3.equals("")) {
                            this.b.a(null, message.arg2);
                            return;
                        } else {
                            this.b.a(b3, message.arg2);
                            return;
                        }
                    } else {
                        return;
                    }
                case 6:
                    if (message.arg2 == 0) {
                        String b4 = this.c.b(6);
                        if (b4 == null || b4.equals("")) {
                            this.a.f(null);
                            return;
                        } else {
                            this.a.f(b4);
                            return;
                        }
                    } else {
                        return;
                    }
                case 7:
                    if (message.arg2 == 0) {
                        String b5 = this.c.b(7);
                        if (b5 == null || b5.equals("")) {
                            this.a.b(null);
                            return;
                        } else {
                            this.a.b(b5);
                            return;
                        }
                    } else {
                        return;
                    }
                case 11:
                case 21:
                    if (message.arg2 == 0) {
                        String b6 = this.c.b(11);
                        if (b6 == null || b6.equals("")) {
                            this.a.a((String) null);
                            return;
                        } else {
                            this.a.a(b6);
                            return;
                        }
                    } else {
                        return;
                    }
                case 14:
                    if (message.arg2 == 0) {
                        String b7 = this.c.b(message.arg1);
                        if (b7 == null || b7.trim().length() <= 0) {
                            this.a.j(null);
                            return;
                        } else {
                            this.a.j(b7);
                            return;
                        }
                    } else {
                        return;
                    }
                case 18:
                    if (message.arg2 == 0) {
                        String b8 = this.c.b(18);
                        if (b8 == null || b8.equals("")) {
                            this.a.l(null);
                            return;
                        } else {
                            this.a.l(b8);
                            return;
                        }
                    } else {
                        return;
                    }
                case 20:
                    if (message.arg2 == 0) {
                        String b9 = this.c.b(message.arg1);
                        if (b9 == null || b9.equals("")) {
                            this.a.h(null);
                            return;
                        } else {
                            this.a.h(b9);
                            return;
                        }
                    } else {
                        return;
                    }
                case 23:
                    if (message.arg2 == 0) {
                        String b10 = this.c.b(23);
                        if (b10 == null || b10.equals("")) {
                            this.a.c(null);
                            return;
                        } else {
                            this.a.c(b10);
                            return;
                        }
                    } else {
                        return;
                    }
                case 26:
                case 28:
                    if (message.arg2 == 0) {
                        String b11 = this.c.b(message.arg1);
                        if (b11 == null || b11.equals("")) {
                            this.a.d(null);
                            return;
                        } else {
                            this.a.d(b11);
                            return;
                        }
                    } else {
                        return;
                    }
                case 31:
                    if (message.arg2 == 0) {
                        String b12 = this.c.b(message.arg1);
                        if (b12 == null || b12.equals("")) {
                            this.a.i(null);
                            return;
                        } else {
                            this.a.i(b12);
                            return;
                        }
                    } else {
                        return;
                    }
                case 33:
                    if (message.arg2 == 0) {
                        String b13 = this.c.b(message.arg1);
                        if (b13 == null || b13.trim().length() <= 0) {
                            this.a.n(null);
                            return;
                        } else {
                            this.a.n(b13);
                            return;
                        }
                    } else {
                        return;
                    }
                case 35:
                    if (message.arg2 == 0) {
                        String b14 = this.c.b(35);
                        if (b14.equals("")) {
                            this.a.k(null);
                            return;
                        } else {
                            this.a.k(b14);
                            return;
                        }
                    } else {
                        return;
                    }
                case 44:
                    break;
                case 45:
                    if (message.arg2 == 0) {
                        this.a.a(this.c.b(45));
                        return;
                    }
                    return;
                case 46:
                    if (message.arg2 == 0) {
                        this.a.f(this.c.b(message.arg1));
                        return;
                    }
                    return;
                case 51:
                    if (message.arg2 == 0) {
                        this.c.b(35);
                        break;
                    }
                    break;
                case HttpStatus.SC_INTERNAL_SERVER_ERROR /*500*/:
                case 508:
                    if (message.arg2 == 0) {
                        String b15 = this.c.b(message.arg1);
                        if (b15 == null || b15.trim().length() <= 0) {
                            this.a.g(null);
                            return;
                        } else {
                            this.a.g(b15);
                            return;
                        }
                    } else {
                        return;
                    }
                case 506:
                    if (message.arg2 == 0) {
                        String b16 = this.c.b(506);
                        if (b16 == null || b16.equals("")) {
                            this.a.m(null);
                            return;
                        } else {
                            this.a.m(b16);
                            return;
                        }
                    } else {
                        return;
                    }
                case 510:
                    if (message.arg2 == 0) {
                        String b17 = this.c.b(message.arg1);
                        if (b17 == null || b17.trim().length() <= 0) {
                            this.a.o(null);
                            return;
                        } else {
                            this.a.o(b17);
                            return;
                        }
                    } else {
                        return;
                    }
                default:
                    return;
            }
            if (message.arg2 == 0) {
                String b18 = this.c.b(44);
                if (b18 == null || b18.equals("")) {
                    this.a.k(null);
                } else {
                    this.a.k(b18);
                }
            }
        }
    }

    public void a(c cVar) {
        this.a = cVar;
    }

    public void a(e eVar) {
        this.c = eVar;
    }
}
