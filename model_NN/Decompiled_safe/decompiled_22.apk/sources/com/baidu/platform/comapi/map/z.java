package com.baidu.platform.comapi.map;

import android.os.Bundle;

public class z extends d {
    static z f = null;
    String a = null;
    int b;

    public z(int i) {
        this.d = i;
    }

    public void a(String str) {
        if (str != null) {
            this.a = str;
        }
    }

    public String b() {
        return this.a;
    }

    public Bundle c() {
        Bundle bundle = new Bundle();
        bundle.putInt("routeIndex", this.b);
        return bundle;
    }
}
