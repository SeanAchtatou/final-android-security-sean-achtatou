package com.baidu.platform.comapi.map;

import com.baidu.platform.comapi.basestruct.c;

public class s {
    public float a = -1.0f;
    public int b = -1;
    public int c = -1;
    public int d = -1;
    public int e = -1;
    public b f = new b();
    public a g = new a();
    public long h = 0;
    public long i = 0;
    public boolean j = false;

    public class a {
        public long a = 0;
        public long b = 0;
        public long c = 0;
        public long d = 0;
        public c e = new c(0, 0);
        public c f = new c(0, 0);
        public c g = new c(0, 0);
        public c h = new c(0, 0);

        public a() {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.d != aVar.d) {
                return false;
            }
            if (this.a != aVar.a) {
                return false;
            }
            if (this.b != aVar.b) {
                return false;
            }
            return this.c == aVar.c;
        }

        public int hashCode() {
            return ((((((((int) (this.d ^ (this.d >>> 32))) + 31) * 31) + ((int) (this.a ^ (this.a >>> 32)))) * 31) + ((int) (this.b ^ (this.b >>> 32)))) * 31) + ((int) (this.c ^ (this.c >>> 32)));
        }
    }

    public class b {
        public int a = 0;
        public int b = 0;
        public int c = 0;
        public int d = 0;

        public b() {
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.d != bVar.d) {
                return false;
            }
            if (this.a != bVar.a) {
                return false;
            }
            if (this.b != bVar.b) {
                return false;
            }
            return this.c == bVar.c;
        }

        public int hashCode() {
            return ((((((this.d + 31) * 31) + this.a) * 31) + this.b) * 31) + this.c;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        s sVar = (s) obj;
        if (this.d != sVar.d) {
            return false;
        }
        if (this.e != sVar.e) {
            return false;
        }
        if (this.j != sVar.j) {
            return false;
        }
        if (this.g == null) {
            if (sVar.g != null) {
                return false;
            }
        } else if (!this.g.equals(sVar.g)) {
            return false;
        }
        if (Float.floatToIntBits(this.a) != Float.floatToIntBits(sVar.a)) {
            return false;
        }
        if (this.c != sVar.c) {
            return false;
        }
        if (this.b != sVar.b) {
            return false;
        }
        if (this.i != sVar.i) {
            return false;
        }
        if (this.h != sVar.h) {
            return false;
        }
        return this.f == null ? sVar.f == null : this.f.equals(sVar.f);
    }

    public int hashCode() {
        int i2 = 0;
        int hashCode = ((((((((this.g == null ? 0 : this.g.hashCode()) + (((this.j ? 1 : 0) + ((((this.d + 31) * 31) + this.e) * 31)) * 31)) * 31) + Float.floatToIntBits(this.a)) * 31) + this.c) * 31) + this.b) * 31;
        if (this.f != null) {
            i2 = this.f.hashCode();
        }
        return hashCode + i2;
    }
}
