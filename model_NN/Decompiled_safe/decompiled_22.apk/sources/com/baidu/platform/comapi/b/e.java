package com.baidu.platform.comapi.b;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import com.baidu.platform.comapi.basestruct.b;
import com.baidu.platform.comapi.basestruct.c;
import com.baidu.platform.comjni.map.search.a;
import java.util.ArrayList;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;

public class e {
    private static e b = null;
    private a a = null;
    /* access modifiers changed from: private */
    public d c = null;
    private Handler d = null;
    private int e = 10;
    private Bundle f = null;

    private e() {
    }

    private Bundle a(b bVar) {
        if (bVar == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("type", bVar.a);
        bundle.putString(Constants.SINA_UID, bVar.d);
        if (bVar.b != null) {
            bundle.putInt("x", bVar.b.a());
            bundle.putInt("y", bVar.b.b());
        }
        bundle.putString("keyword", bVar.c);
        return bundle;
    }

    public static e a() {
        if (b == null) {
            b = new e();
            if (!b.d()) {
                b = null;
                return null;
            }
        }
        return b;
    }

    public static void b() {
        if (b != null) {
            if (b.a != null) {
                if (b.d != null) {
                    com.baidu.platform.comjni.engine.a.b(2000, b.d);
                    b.d = null;
                }
                b.a.c();
                b.a = null;
                b.f = null;
                b.c.a();
                b.c = null;
            }
            b = null;
        }
    }

    @SuppressLint({"HandlerLeak"})
    private boolean d() {
        if (this.a != null) {
            return true;
        }
        this.a = new a();
        if (this.a.a() == 0) {
            this.a = null;
            return false;
        }
        this.c = new d();
        this.d = new f(this);
        com.baidu.platform.comjni.engine.a.a(2000, this.d);
        this.c.a(this);
        return true;
    }

    private Bundle e() {
        if (this.f == null) {
            this.f = new Bundle();
        } else {
            this.f.clear();
        }
        return this.f;
    }

    public void a(int i) {
        if (i > 0 && i <= 50) {
            this.e = i;
        }
    }

    public void a(c cVar) {
        this.c.a(cVar);
    }

    public boolean a(b bVar, b bVar2, String str, b bVar3, int i, int i2, Map<String, Object> map) {
        if (str == null || str.equals("")) {
            return false;
        }
        Bundle e2 = e();
        Bundle a2 = a(bVar);
        Bundle a3 = a(bVar2);
        if (a2 == null || a3 == null) {
            return false;
        }
        e2.putBundle("start", a2);
        e2.putBundle("end", a3);
        if (i2 < 3 || i2 > 6) {
            return false;
        }
        e2.putInt("strategy", i2);
        e2.putString("cityid", str);
        if (!(bVar3 == null || bVar3.a == null || bVar3.b == null)) {
            Bundle bundle = new Bundle();
            bundle.putInt("level", i);
            bundle.putInt("ll_x", bVar3.a.a());
            bundle.putInt("ll_y", bVar3.a.b());
            bundle.putInt("ru_x", bVar3.b.a());
            bundle.putInt("ru_y", bVar3.b.b());
            e2.putBundle("mapbound", bundle);
        }
        if (map != null) {
            Bundle bundle2 = new Bundle();
            for (String next : map.keySet()) {
                bundle2.putString(next.toString(), map.get(next).toString());
            }
            e2.putBundle("extparams", bundle2);
        }
        return this.a.d(e2);
    }

    public boolean a(b bVar, b bVar2, String str, String str2, String str3, b bVar3, int i, int i2, int i3, ArrayList<g> arrayList, Map<String, Object> map) {
        String str4;
        String str5;
        if (bVar == null || bVar2 == null) {
            return false;
        }
        if (bVar.b == null && (str2 == null || str2.equals(""))) {
            return false;
        }
        if (bVar2.b == null && (str3 == null || str3.equals(""))) {
            return false;
        }
        Bundle e2 = e();
        e2.putInt("starttype", bVar.a);
        if (bVar.b != null) {
            e2.putInt("startptx", bVar.b.a());
            e2.putInt("startpty", bVar.b.b());
        }
        e2.putString("startkeyword", bVar.c);
        e2.putString("startuid", bVar.d);
        e2.putInt("endtype", bVar2.a);
        if (bVar2.b != null) {
            e2.putInt("endptx", bVar2.b.a());
            e2.putInt("endpty", bVar2.b.b());
        }
        e2.putString("endkeyword", bVar2.c);
        e2.putString("enduid", bVar2.d);
        e2.putInt("level", i);
        if (!(bVar3 == null || bVar3.a == null || bVar3.b == null)) {
            e2.putInt("ll_x", bVar3.a.a());
            e2.putInt("ll_y", bVar3.a.b());
            e2.putInt("ru_x", bVar3.b.a());
            e2.putInt("ru_y", bVar3.b.b());
        }
        e2.putInt("strategy", i2);
        e2.putString("cityid", str);
        e2.putString("st_cityid", str2);
        e2.putString("en_cityid", str3);
        e2.putInt("traffic", i3);
        if (arrayList != null) {
            int size = arrayList.size();
            int i4 = 0;
            String str6 = "";
            String str7 = "";
            int i5 = 0;
            while (i5 < size) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    if (arrayList.get(i5).a != null) {
                        jSONObject.put("type", 1);
                    } else {
                        jSONObject.put("type", 2);
                    }
                    jSONObject.put("keyword", arrayList.get(i5).b);
                    if (arrayList.get(i5).a != null) {
                        jSONObject.put("xy", String.valueOf(arrayList.get(i5).a.a) + "," + String.valueOf(arrayList.get(i5).a.b));
                    }
                    str4 = str7 + arrayList.get(i5).c;
                    try {
                        str5 = str6 + jSONObject.toString();
                        if (i4 != size - 1) {
                            try {
                                str5 = str5 + "|";
                                str4 = str4 + "|";
                            } catch (JSONException e3) {
                            }
                        }
                        i4++;
                    } catch (JSONException e4) {
                        str5 = str6;
                    }
                } catch (JSONException e5) {
                    str4 = str7;
                    str5 = str6;
                }
                i5++;
                str6 = str5;
                str7 = str4;
            }
            e2.putString("wp", str6);
            e2.putString("wpc", str7);
        }
        if (map != null) {
            Bundle bundle = new Bundle();
            for (String next : map.keySet()) {
                bundle.putString(next.toString(), map.get(next).toString());
            }
            e2.putBundle("extparams", bundle);
        }
        return this.a.e(e2);
    }

    public boolean a(b bVar, b bVar2, String str, String str2, String str3, b bVar3, int i, Map<String, Object> map) {
        if (bVar == null || bVar2 == null) {
            return false;
        }
        if (bVar.b == null && (str2 == null || str2.equals(""))) {
            return false;
        }
        if (bVar2.b == null && (str3 == null || str3.equals(""))) {
            return false;
        }
        Bundle e2 = e();
        e2.putInt("starttype", bVar.a);
        if (bVar.b != null) {
            e2.putInt("startptx", bVar.b.a());
            e2.putInt("startpty", bVar.b.b());
        }
        e2.putString("startkeyword", bVar.c);
        e2.putString("startuid", bVar.d);
        e2.putInt("endtype", bVar2.a);
        if (bVar2.b != null) {
            e2.putInt("endptx", bVar2.b.a());
            e2.putInt("endpty", bVar2.b.b());
        }
        e2.putString("endkeyword", bVar2.c);
        e2.putString("enduid", bVar2.d);
        e2.putInt("level", i);
        if (!(bVar3 == null || bVar3.a == null || bVar3.b == null)) {
            e2.putInt("ll_x", bVar3.a.a());
            e2.putInt("ll_y", bVar3.a.b());
            e2.putInt("ru_x", bVar3.b.a());
            e2.putInt("ru_y", bVar3.b.b());
        }
        e2.putString("cityid", str);
        e2.putString("st_cityid", str2);
        e2.putString("en_cityid", str3);
        if (map != null) {
            Bundle bundle = new Bundle();
            for (String next : map.keySet()) {
                bundle.putString(next.toString(), map.get(next).toString());
            }
            e2.putBundle("extparams", bundle);
        }
        return this.a.f(e2);
    }

    public boolean a(c cVar) {
        if (cVar == null) {
            return false;
        }
        int b2 = cVar.b();
        return this.a.a(cVar.a(), b2);
    }

    public boolean a(String str) {
        if (str == null) {
            return false;
        }
        String trim = str.trim();
        if (trim.length() == 0 || trim.length() > 99) {
            return false;
        }
        return this.a.a(trim);
    }

    public boolean a(String str, int i, int i2, int i3, b bVar, b bVar2, Map<String, Object> map) {
        if (str == null) {
            return false;
        }
        String trim = str.trim();
        if (trim.length() == 0 || trim.length() > 99) {
            return false;
        }
        Bundle e2 = e();
        e2.putString("keyword", trim);
        e2.putInt("pagenum", i2);
        e2.putInt("count", this.e);
        e2.putInt("cityid", i);
        e2.putInt("level", i3);
        if (bVar2 != null) {
            Bundle bundle = new Bundle();
            bundle.putInt("ll_x", bVar2.a.a());
            bundle.putInt("ll_y", bVar2.a.b());
            bundle.putInt("ru_x", bVar2.b.a());
            bundle.putInt("ru_y", bVar2.b.b());
            e2.putBundle("mapbound", bundle);
        }
        if (bVar != null) {
            e2.putInt("ll_x", bVar.a.a());
            e2.putInt("ll_y", bVar.a.b());
            e2.putInt("ru_x", bVar.b.a());
            e2.putInt("ru_y", bVar.b.b());
            e2.putInt("loc_x", (bVar.a.a() + bVar.b.a()) / 2);
            e2.putInt("loc_y", (bVar.a.b() + bVar.b.b()) / 2);
        }
        if (map != null) {
            Bundle bundle2 = new Bundle();
            for (String next : map.keySet()) {
                bundle2.putString(next.toString(), map.get(next).toString());
            }
            e2.putBundle("extparams", bundle2);
        }
        return this.a.b(e2);
    }

    public boolean a(String str, int i, int i2, b bVar, int i3, c cVar, Map<String, Object> map) {
        if (bVar == null || str == null) {
            return false;
        }
        String trim = str.trim();
        if (trim.length() == 0 || trim.length() > 99) {
            return false;
        }
        Bundle e2 = e();
        e2.putString("keyword", trim);
        e2.putInt("pagenum", i2);
        e2.putInt("count", this.e);
        e2.putString("cityid", String.valueOf(i));
        e2.putInt("level", i3);
        if (bVar != null) {
            e2.putInt("ll_x", bVar.a.a());
            e2.putInt("ll_y", bVar.a.b());
            e2.putInt("ru_x", bVar.b.a());
            e2.putInt("ru_y", bVar.b.b());
        }
        if (cVar != null) {
            e2.putInt("loc_x", cVar.a);
            e2.putInt("loc_y", cVar.b);
        }
        if (map != null) {
            Bundle bundle = new Bundle();
            for (String next : map.keySet()) {
                bundle.putString(next.toString(), map.get(next).toString());
            }
            e2.putBundle("extparams", bundle);
        }
        return this.a.h(e2);
    }

    public boolean a(String str, int i, String str2, b bVar, int i2, c cVar) {
        if (str == null) {
            return false;
        }
        if (i != 0 && i != 2) {
            return false;
        }
        String trim = str.trim();
        if (trim.length() == 0 || trim.length() > 99) {
            return false;
        }
        Bundle e2 = e();
        e2.putString("keyword", str);
        e2.putInt("type", i);
        e2.putString("cityid", str2);
        Bundle bundle = new Bundle();
        bundle.putInt("level", i2);
        e2.putBundle("mapbound", bundle);
        if (cVar != null) {
            e2.putInt("loc_x", cVar.a);
            e2.putInt("loc_y", cVar.b);
        }
        return this.a.g(e2);
    }

    public boolean a(String str, String str2) {
        if (str2 == null || str == null || str.equals("")) {
            return false;
        }
        String trim = str2.trim();
        if (trim.length() == 0 || trim.length() > 99) {
            return false;
        }
        return this.a.a(str, trim);
    }

    public boolean a(String str, String str2, int i, b bVar, int i2, Map<String, Object> map) {
        if (str == null) {
            return false;
        }
        String trim = str.trim();
        if (trim.length() == 0 || trim.length() > 99) {
            return false;
        }
        Bundle e2 = e();
        e2.putString("keyword", trim);
        e2.putInt("pagenum", i);
        e2.putInt("count", this.e);
        e2.putString("cityid", str2);
        e2.putInt("level", i2);
        if (bVar != null) {
            e2.putInt("ll_x", bVar.a.a());
            e2.putInt("ll_y", bVar.a.b());
            e2.putInt("ru_x", bVar.b.a());
            e2.putInt("ru_y", bVar.b.b());
        }
        if (map != null) {
            Bundle bundle = new Bundle();
            for (String next : map.keySet()) {
                bundle.putString(next.toString(), map.get(next).toString());
            }
            e2.putBundle("extparams", bundle);
        }
        return this.a.a(e2);
    }

    public boolean a(String[] strArr, int i, int i2, int i3, int i4, b bVar, b bVar2, Map<String, Object> map) {
        if (strArr == null || strArr.length < 2 || strArr.length > 10) {
            return false;
        }
        StringBuilder sb = new StringBuilder();
        for (int i5 = 0; i5 < strArr.length; i5++) {
            if (strArr[i5] == null) {
                return false;
            }
            String trim = strArr[i5].trim();
            if (trim.length() == 0 || trim.length() > 99 || strArr[i5].contains("$$")) {
                return false;
            }
            sb.append(trim);
            if (i5 != strArr.length - 1) {
                sb.append("$$");
            }
        }
        if (sb.toString().length() > 99) {
            return false;
        }
        Bundle e2 = e();
        e2.putString("keyword", sb.toString());
        e2.putInt("pagenum", i2);
        e2.putInt("count", this.e);
        e2.putInt("cityid", i);
        e2.putInt("level", i3);
        e2.putInt("radius", i4);
        if (bVar2 != null) {
            Bundle bundle = new Bundle();
            bundle.putInt("ll_x", bVar2.a.a());
            bundle.putInt("ll_y", bVar2.a.b());
            bundle.putInt("ru_x", bVar2.b.a());
            bundle.putInt("ru_y", bVar2.b.b());
            e2.putBundle("mapbound", bundle);
        }
        if (bVar != null) {
            e2.putInt("ll_x", bVar.a.a());
            e2.putInt("ll_y", bVar.a.b());
            e2.putInt("ru_x", bVar.b.a());
            e2.putInt("ru_y", bVar.b.b());
            e2.putInt("loc_x", (bVar.a.a() + bVar.b.a()) / 2);
            e2.putInt("loc_y", (bVar.a.b() + bVar.b.b()) / 2);
        }
        if (map != null) {
            Bundle bundle2 = new Bundle();
            for (String next : map.keySet()) {
                bundle2.putString(next.toString(), map.get(next).toString());
            }
            e2.putBundle("extparams", bundle2);
        }
        return this.a.c(e2);
    }

    /* access modifiers changed from: package-private */
    public String b(int i) {
        String a2 = this.a.a(i);
        if (a2 == null || a2.trim().length() > 0) {
            return a2;
        }
        return null;
    }

    public boolean b(String str, String str2) {
        return this.a.b(str, str2);
    }

    public int c() {
        return this.e;
    }
}
