package com.baidu.platform.comapi.map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.util.FloatMath;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.d.c;
import com.baidu.platform.comjni.tools.JNITools;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class o implements View.OnKeyListener {
    private static int A;
    private static int B;
    private static boolean C;
    private static boolean D;
    private static a E = new a();
    private static int F;
    private static int G;
    private static boolean H;
    private static boolean I;
    private static boolean J;
    private static VelocityTracker K;
    private static long L;
    private static long M;
    private static long N;
    private static long O;
    private static int P = 0;
    private static float Q;
    private static float R;
    private static boolean S;
    private static long T;
    private static long U;
    private static long W = 400;
    private static long X = 500;
    private static long Y = 120;
    public static boolean i = true;
    private static final int p = (ViewConfiguration.getMinimumFlingVelocity() * 3);
    private static int y;
    private static long z;
    private boolean V = false;
    private boolean Z = true;
    public int a = 0;
    private boolean aa = true;
    private GeoPoint ab;
    private boolean ac;
    private int ad;
    private int ae;
    /* access modifiers changed from: private */
    public boolean af = false;
    /* access modifiers changed from: private */
    public boolean ag = false;
    public int b = 0;
    public int c = 0;
    public int d = 0;
    public Map<String, Integer> e = new HashMap();
    public Map<String, Integer> f = new HashMap();
    public Map<String, Integer> g = new HashMap();
    public Map<String, Integer> h = new HashMap();
    private com.baidu.platform.comjni.map.basemap.a j = null;
    private Context k = null;
    /* access modifiers changed from: private */
    public q l = null;
    private int m = 0;
    private Bundle n = new Bundle();
    private Handler o = null;
    private boolean q = true;
    private boolean r = false;
    private boolean s = true;
    private boolean t = true;
    private boolean u = true;
    private int v;
    private int w;
    private int x = 20;

    static class a {
        final int a = 2;
        float b;
        float c;
        float d;
        float e;
        float f;
        float g;
        float h;
        float i;
        boolean j;
        float k;
        float l;
        double m;

        a() {
        }
    }

    @SuppressLint({"HandlerLeak"})
    public o(Context context, q qVar) {
        this.l = qVar;
        this.l.setOnKeyListener(this);
        Display defaultDisplay = ((Activity) context).getWindowManager().getDefaultDisplay();
        this.v = defaultDisplay.getWidth();
        this.w = defaultDisplay.getHeight();
        this.o = new p(this);
        p();
    }

    public static int a(int i2, int i3, int i4, int i5) {
        return com.baidu.platform.comjni.map.basemap.a.b(i2, i3, i4, i5);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(int r9, int r10, int r11) {
        /*
            r8 = this;
            r3 = 1
            r4 = 0
            java.lang.String r0 = ""
            r2 = -1
            com.baidu.platform.comapi.map.q r0 = r8.l     // Catch:{ JSONException -> 0x0067 }
            java.util.List r0 = r0.e()     // Catch:{ JSONException -> 0x0067 }
            int r0 = r0.size()     // Catch:{ JSONException -> 0x0067 }
            int r0 = r0 + -1
            r5 = r0
            r1 = r4
        L_0x0013:
            if (r5 < 0) goto L_0x0070
            com.baidu.platform.comapi.map.q r0 = r8.l     // Catch:{ JSONException -> 0x006c }
            java.util.List r0 = r0.e()     // Catch:{ JSONException -> 0x006c }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ JSONException -> 0x006c }
            com.baidu.platform.comapi.map.u r0 = (com.baidu.platform.comapi.map.u) r0     // Catch:{ JSONException -> 0x006c }
            int r6 = r0.mType     // Catch:{ JSONException -> 0x006c }
            r7 = 27
            if (r6 == r7) goto L_0x002d
            r0 = r1
        L_0x0028:
            int r1 = r5 + -1
            r5 = r1
            r1 = r0
            goto L_0x0013
        L_0x002d:
            int r1 = r0.mLayerID     // Catch:{ JSONException -> 0x006c }
            com.baidu.platform.comjni.map.basemap.a r0 = r8.j     // Catch:{ JSONException -> 0x006c }
            java.lang.String r0 = r0.a(r1, r10, r11, r11)     // Catch:{ JSONException -> 0x006c }
            if (r0 == 0) goto L_0x006e
            java.lang.String r6 = ""
            boolean r6 = r0.equals(r6)     // Catch:{ JSONException -> 0x006c }
            if (r6 != 0) goto L_0x006e
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x006c }
            r5.<init>(r0)     // Catch:{ JSONException -> 0x006c }
            java.lang.String r0 = "dataset"
            org.json.JSONArray r0 = r5.getJSONArray(r0)     // Catch:{ JSONException -> 0x006c }
            r5 = 0
            java.lang.Object r0 = r0.get(r5)     // Catch:{ JSONException -> 0x006c }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ JSONException -> 0x006c }
            java.lang.String r5 = "itemindex"
            int r0 = r0.getInt(r5)     // Catch:{ JSONException -> 0x006c }
            r2 = r3
        L_0x0058:
            if (r9 != r3) goto L_0x0066
            com.baidu.platform.comapi.map.q r3 = r8.l
            com.baidu.platform.comapi.map.t r3 = r3.e
            com.baidu.platform.comapi.basestruct.GeoPoint r4 = new com.baidu.platform.comapi.basestruct.GeoPoint
            r4.<init>(r10, r11)
            r3.a(r0, r4, r1)
        L_0x0066:
            return r2
        L_0x0067:
            r0 = move-exception
            r1 = r4
        L_0x0069:
            r0 = r2
            r2 = r4
            goto L_0x0058
        L_0x006c:
            r0 = move-exception
            goto L_0x0069
        L_0x006e:
            r0 = r1
            goto L_0x0028
        L_0x0070:
            r0 = r2
            r2 = r4
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.platform.comapi.map.o.b(int, int, int):boolean");
    }

    private void e(MotionEvent motionEvent) {
        if (!E.j) {
            U = motionEvent.getDownTime();
            if (U - T >= W) {
                T = U;
            } else if (Math.abs(motionEvent.getX() - Q) >= ((float) Y) || Math.abs(motionEvent.getY() - R) >= ((float) Y)) {
                T = U;
            } else {
                d(motionEvent);
                T = 0;
            }
            Q = motionEvent.getX();
            R = motionEvent.getY();
            int x2 = (int) motionEvent.getX();
            int y2 = (int) motionEvent.getY();
            a(4, 0, (y2 << 16) | x2);
            f(x2, y2);
            S = true;
        }
    }

    public static void f() {
        y = 0;
        z = 0;
        A = 0;
        B = 0;
        C = false;
        D = false;
        E.j = false;
        E.m = 0.0d;
        G = 0;
        F = 0;
        H = false;
        I = false;
        J = false;
    }

    private boolean g(int i2, int i3) {
        ArrayList arrayList;
        ArrayList arrayList2;
        ArrayList arrayList3;
        ArrayList arrayList4;
        ArrayList arrayList5;
        String a2 = this.j.a(-1, i2, i3, (int) (((double) this.x) * c()));
        if (a2 != null) {
            try {
                JSONObject jSONObject = new JSONObject(a2);
                ArrayList arrayList6 = new ArrayList();
                JSONArray jSONArray = jSONObject.getJSONArray("dataset");
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(0);
                int i4 = jSONObject2.getInt("ty");
                if (i4 == 22) {
                    arrayList = new ArrayList();
                    arrayList2 = null;
                    arrayList3 = null;
                    arrayList4 = null;
                    arrayList5 = null;
                } else if (i4 == 3 || i4 == 13 || i4 == 14 || i4 == 16 || i4 == 15 || i4 == 4 || i4 == 28) {
                    arrayList = null;
                    arrayList2 = null;
                    arrayList3 = new ArrayList();
                    arrayList4 = null;
                    arrayList5 = null;
                } else if (i4 == 8 || i4 == 1 || i4 == 2) {
                    arrayList = null;
                    arrayList2 = null;
                    arrayList3 = null;
                    arrayList4 = new ArrayList();
                    arrayList5 = null;
                } else if (i4 == 6) {
                    arrayList = null;
                    arrayList2 = null;
                    arrayList3 = null;
                    arrayList4 = null;
                    arrayList5 = new ArrayList();
                } else if (i4 == 24) {
                    arrayList = null;
                    arrayList2 = new ArrayList();
                    arrayList3 = null;
                    arrayList4 = null;
                    arrayList5 = null;
                } else {
                    arrayList = null;
                    arrayList2 = null;
                    arrayList3 = null;
                    arrayList4 = null;
                    arrayList5 = null;
                }
                for (int i5 = 0; i5 < jSONArray.length(); i5++) {
                    JSONObject jSONObject3 = (JSONObject) jSONArray.get(i5);
                    int i6 = jSONObject3.getInt("ty");
                    if (i6 != 25) {
                        r rVar = new r();
                        if (jSONObject3.has("ud")) {
                            rVar.a = jSONObject3.getString("ud");
                        } else {
                            rVar.a = "";
                        }
                        rVar.c = jSONObject3.optString("tx");
                        if (jSONObject3.has("in")) {
                            rVar.b = jSONObject3.getInt("in");
                        } else {
                            rVar.b = 0;
                        }
                        if (jSONObject3.has("geo")) {
                            String string = jSONObject3.getString("geo");
                            Bundle bundle = new Bundle();
                            bundle.putString("strkey", string);
                            JNITools.TransNodeStr2Pt(bundle);
                            rVar.d = new GeoPoint((int) bundle.getDouble("pty"), (int) bundle.getDouble("ptx"));
                        }
                        rVar.e = i6;
                        if (jSONObject3.has("of")) {
                            rVar.f = jSONObject3.getInt("of");
                        }
                        if (i4 == 22) {
                            f fVar = new f();
                            fVar.a = rVar;
                            fVar.b = jSONObject3.getLong("iest");
                            fVar.c = jSONObject3.getLong("ieend");
                            fVar.d = jSONObject3.getString("iedetail");
                            arrayList.add(fVar);
                        } else if (i4 == 3 || i4 == 13 || i4 == 14 || i4 == 16 || i4 == 15 || i4 == 4 || i4 == 28) {
                            arrayList3.add(rVar);
                        } else if (i4 == 8 || i4 == 1 || i4 == 2) {
                            arrayList4.add(rVar);
                        } else if (i4 == 6) {
                            arrayList5.add(rVar);
                        } else if (i4 == 24) {
                            arrayList2.add(rVar);
                        } else {
                            arrayList6.add(rVar);
                        }
                    }
                }
                switch (i4) {
                    case 1:
                    case 2:
                    case 8:
                        this.l.e.a(arrayList4, jSONObject2.getInt("layerid"));
                        break;
                    case 3:
                    case 4:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 28:
                        this.l.e.b(arrayList3, jSONObject2.getInt("layerid"));
                        break;
                    case 6:
                        this.l.e.c(arrayList5, 0);
                        break;
                    case 18:
                        this.l.e.c(arrayList6, jSONObject2.getInt("layerid"));
                    case 17:
                    case 19:
                        this.l.e.c(arrayList6, 0);
                        break;
                    case 22:
                        this.l.e.a(arrayList);
                        break;
                    case 23:
                        this.l.e.c(arrayList6, jSONObject2.getInt("layerid"));
                        break;
                    case 24:
                        this.l.e.b(arrayList2);
                        break;
                }
                return true;
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void n() {
        if (this.ac) {
            Point point = new Point();
            this.l.f().toPixels(this.ab, point);
            e(point.x - this.ad, point.y - this.ae);
            this.ac = false;
        }
        this.af = false;
        this.ad = 0;
        this.ae = 0;
        this.ab = null;
    }

    /* access modifiers changed from: private */
    public void o() {
        if (this.ac) {
            Point point = new Point();
            this.l.f().toPixels(this.ab, point);
            e(point.x - this.ad, point.y - this.ae);
            this.ac = false;
        }
        this.ag = false;
        this.ad = 0;
        this.ae = 0;
        this.ab = null;
    }

    private void p() {
        com.baidu.platform.comjni.engine.a.a(4000, this.o);
        com.baidu.platform.comjni.engine.a.a(39, this.o);
        com.baidu.platform.comjni.engine.a.a(512, this.o);
    }

    private void q() {
        com.baidu.platform.comjni.engine.a.b(4000, this.o);
        com.baidu.platform.comjni.engine.a.b(39, this.o);
        com.baidu.platform.comjni.engine.a.b(512, this.o);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.m;
    }

    public int a(int i2, int i3, int i4) {
        return a(this.m, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        this.j.c(i2, i3);
    }

    public void a(Bundle bundle, v vVar) {
        if (bundle == null) {
            throw new IllegalArgumentException("IllegalArgument");
        }
        if (this.j == null) {
            this.j = new com.baidu.platform.comjni.map.basemap.a();
            this.j.a();
            this.m = this.j.c();
        }
        if (vVar != null) {
            this.j.a(vVar);
        }
        boolean z2 = false;
        if (c.m() >= 180) {
            z2 = true;
        }
        if (c.m() < 160) {
            this.x = 18;
        } else if (c.m() < 240) {
            this.x = 25;
        } else if (c.m() < 320) {
            this.x = 37;
        } else {
            this.x = 50;
        }
        String string = bundle.getString("modulePath");
        String string2 = bundle.getString("appSdcardPath");
        String string3 = bundle.getString("appCachePath");
        String string4 = bundle.getString("appSecondCachePath");
        int i2 = bundle.getInt("mapTmpMax");
        int i3 = bundle.getInt("domTmpMax");
        int i4 = bundle.getInt("itsTmpMax");
        String str = string + "/cfg/h/";
        String str2 = string + "/cfg/h/";
        String str3 = string2 + "/vmp/h/";
        String str4 = string2 + "/vmp/h/";
        String str5 = string3 + "/tmp/";
        String str6 = string4 + "/tmp/";
        if (!z2) {
            str = string + "/cfg/l/";
            str2 = string + "/cfg/l/";
            str3 = string2 + "/vmp/l/";
            str4 = string2 + "/vmp/l/";
        }
        this.j.a(str, str3, str5, str6, str4, str2, this.v, this.w, c.m(), i2, i3, i4);
        this.j.e();
    }

    public void a(GeoPoint geoPoint) {
        this.l.a(geoPoint, null, null);
    }

    public void a(GeoPoint geoPoint, Message message) {
        this.l.a(geoPoint, message, null);
    }

    public void a(q qVar) {
        this.l = qVar;
        this.l.setOnKeyListener(this);
    }

    public void a(s sVar) {
        int i2 = 0;
        if (this.j != null) {
            this.n.clear();
            this.n.putDouble("level", (double) sVar.a);
            this.n.putDouble("rotation", (double) sVar.b);
            this.n.putDouble("overlooking", (double) sVar.c);
            this.n.putDouble("centerptx", (double) sVar.d);
            this.n.putDouble("centerpty", (double) sVar.e);
            this.n.putInt("left", sVar.f.a);
            this.n.putInt("right", sVar.f.b);
            this.n.putInt("top", sVar.f.c);
            this.n.putInt("bottom", sVar.f.d);
            this.n.putInt("lbx", sVar.g.e.a);
            this.n.putInt("lby", sVar.g.e.b);
            this.n.putInt("ltx", sVar.g.f.a);
            this.n.putInt("lty", sVar.g.f.b);
            this.n.putInt("rtx", sVar.g.g.a);
            this.n.putInt("rty", sVar.g.g.b);
            this.n.putInt("rbx", sVar.g.h.a);
            this.n.putInt("rby", sVar.g.h.b);
            this.n.putLong("yoffset", sVar.i);
            this.n.putLong("xoffset", sVar.h);
            this.n.putInt("animation", 0);
            this.n.putInt("animatime", 0);
            Bundle bundle = this.n;
            if (sVar.j) {
                i2 = 1;
            }
            bundle.putInt("bfpp", i2);
            this.j.a(this.n);
        }
    }

    public void a(s sVar, int i2) {
        int i3 = 1;
        if (this.j != null) {
            this.n.clear();
            this.n.putDouble("level", (double) sVar.a);
            this.n.putDouble("rotation", (double) sVar.b);
            this.n.putDouble("overlooking", (double) sVar.c);
            this.n.putDouble("centerptx", (double) sVar.d);
            this.n.putDouble("centerpty", (double) sVar.e);
            this.n.putInt("left", sVar.f.a);
            this.n.putInt("right", sVar.f.b);
            this.n.putInt("top", sVar.f.c);
            this.n.putInt("bottom", sVar.f.d);
            this.n.putInt("lbx", sVar.g.e.a);
            this.n.putInt("lby", sVar.g.e.b);
            this.n.putInt("ltx", sVar.g.f.a);
            this.n.putInt("lty", sVar.g.f.b);
            this.n.putInt("rtx", sVar.g.g.a);
            this.n.putInt("rty", sVar.g.g.b);
            this.n.putInt("rbx", sVar.g.h.a);
            this.n.putInt("rby", sVar.g.h.b);
            this.n.putLong("xoffset", sVar.h);
            this.n.putLong("yoffset", sVar.i);
            this.n.putInt("animation", 1);
            this.n.putInt("animatime", i2);
            Bundle bundle = this.n;
            if (!sVar.j) {
                i3 = 0;
            }
            bundle.putInt("bfpp", i3);
            this.j.a(this.n);
        }
    }

    public void a(String str) {
        this.l.requestRender();
        if (str == null || str.equals("")) {
            throw new IllegalArgumentException("the path is invalid!");
        }
        this.j.a(str);
    }

    public void a(boolean z2) {
        this.s = z2;
    }

    @SuppressLint({"NewApi", "NewApi", "NewApi", "NewApi"})
    public boolean a(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        E.getClass();
        if (pointerCount == 2) {
            float j2 = ((float) j()) - motionEvent.getY(0);
            float j3 = ((float) j()) - motionEvent.getY(1);
            float x2 = motionEvent.getX(0);
            float x3 = motionEvent.getX(1);
            switch (motionEvent.getAction()) {
                case 5:
                    M = motionEvent.getEventTime();
                    P--;
                    break;
                case 6:
                    O = motionEvent.getEventTime();
                    P++;
                    break;
                case 261:
                    L = motionEvent.getEventTime();
                    P--;
                    break;
                case 262:
                    N = motionEvent.getEventTime();
                    P++;
                    break;
            }
            if (K == null) {
                K = VelocityTracker.obtain();
            }
            K.addMovement(motionEvent);
            int minimumFlingVelocity = ViewConfiguration.getMinimumFlingVelocity();
            K.computeCurrentVelocity(1000, (float) ViewConfiguration.getMaximumFlingVelocity());
            float xVelocity = K.getXVelocity(1);
            float yVelocity = K.getYVelocity(1);
            float xVelocity2 = K.getXVelocity(2);
            float yVelocity2 = K.getYVelocity(2);
            if (Math.abs(xVelocity) > ((float) minimumFlingVelocity) || Math.abs(yVelocity) > ((float) minimumFlingVelocity) || Math.abs(xVelocity2) > ((float) minimumFlingVelocity) || Math.abs(yVelocity2) > ((float) minimumFlingVelocity)) {
                if (E.j) {
                    if (y == 0) {
                        if (((E.h - j2 <= 0.0f || E.i - j3 <= 0.0f) && (E.h - j2 >= 0.0f || E.i - j3 >= 0.0f)) || !this.aa) {
                            y = 2;
                        } else {
                            double atan2 = Math.atan2((double) (j3 - j2), (double) (x3 - x2)) - Math.atan2((double) (E.i - E.h), (double) (E.g - E.f));
                            double sqrt = ((double) FloatMath.sqrt(((x3 - x2) * (x3 - x2)) + ((j3 - j2) * (j3 - j2)))) / E.m;
                            int log = (int) ((Math.log(sqrt) / Math.log(2.0d)) * 10000.0d);
                            int i2 = (int) ((atan2 * 180.0d) / 3.1416d);
                            if ((sqrt > 0.0d && (log > 3000 || log < -3000)) || Math.abs(i2) >= 10) {
                                y = 2;
                            } else if (Math.abs(i2) < 1) {
                                y = 1;
                            }
                        }
                        if (y == 0) {
                            return true;
                        }
                    }
                    if (y == 1 && this.q) {
                        if (!C) {
                            C = true;
                        }
                        if (!H) {
                            H = true;
                        }
                        if (E.h - j2 > 0.0f && E.i - j3 > 0.0f) {
                            a(1, 83, 0);
                        } else if (E.h - j2 < 0.0f && E.i - j3 < 0.0f) {
                            a(1, 87, 0);
                        }
                    } else if (y == 2 || y == 4 || y == 3) {
                        if (!D) {
                            D = true;
                        }
                        double atan22 = Math.atan2((double) (j3 - j2), (double) (x3 - x2)) - Math.atan2((double) (E.i - E.h), (double) (E.g - E.f));
                        double sqrt2 = ((double) FloatMath.sqrt(((x3 - x2) * (x3 - x2)) + ((j3 - j2) * (j3 - j2)))) / E.m;
                        int log2 = (int) ((Math.log(sqrt2) / Math.log(2.0d)) * 10000.0d);
                        double atan23 = Math.atan2((double) (E.l - E.h), (double) (E.k - E.f));
                        double sqrt3 = (double) FloatMath.sqrt(((E.k - E.f) * (E.k - E.f)) + ((E.l - E.h) * (E.l - E.h)));
                        float cos = (float) ((Math.cos(atan23 + atan22) * sqrt3 * sqrt2) + ((double) x2));
                        float sin = (float) ((Math.sin(atan23 + atan22) * sqrt3 * sqrt2) + ((double) j2));
                        int i3 = (int) ((atan22 * 180.0d) / 3.1416d);
                        if (this.aa) {
                            if (sqrt2 > 0.0d && (3 == y || (Math.abs(log2) > 2000 && 2 == y))) {
                                y = 3;
                                if (!J) {
                                    J = true;
                                }
                                if (this.t) {
                                    a(8193, 3, log2);
                                }
                            } else if (this.aa && i3 != 0 && (4 == y || (Math.abs(i3) > 10 && 2 == y))) {
                                y = 4;
                                if (!I) {
                                    I = true;
                                }
                                if (this.u) {
                                    a(8193, 1, i3);
                                }
                            }
                        } else if (Math.abs(xVelocity) > ((float) p) || Math.abs(xVelocity2) > ((float) p)) {
                            y = 3;
                            if (!J) {
                                J = true;
                            }
                            if (this.t) {
                                a(8193, 3, log2);
                            }
                        }
                        this.l.setRenderMode(1);
                        E.k = cos;
                        E.l = sin;
                    }
                }
                if (y == 1 && P == 0) {
                    if (!this.r) {
                        com.baidu.platform.comapi.c.a.a().a("mapview_gesture_3d_enter");
                    } else if (k().c == 0) {
                        com.baidu.platform.comapi.c.a.a().a("mapview_gesture_3d_exit");
                    }
                } else if (y == 4 && P == 0) {
                    if (!this.r) {
                        com.baidu.platform.comapi.c.a.a().a("mapview_gesture_2d_rotate");
                    } else {
                        com.baidu.platform.comapi.c.a.a().a("mapview_gesture_3d_rotate");
                    }
                }
            } else if (y == 0 && P == 0) {
                N = N > O ? N : O;
                L = L < M ? M : L;
                if (N - L < 200 && this.t) {
                    a(8193, 4, 0);
                }
            }
            if (2 != y) {
                E.h = j2;
                E.i = j3;
                E.f = x2;
                E.g = x3;
            }
            if (!E.j) {
                E.k = (float) (i() / 2);
                E.l = (float) (j() / 2);
                E.b = x2;
                E.c = j2;
                E.d = x3;
                E.e = j3;
                E.j = true;
                if (0.0d == E.m) {
                    E.m = (double) FloatMath.sqrt(((E.g - E.f) * (E.g - E.f)) + ((E.i - E.h) * (E.i - E.h)));
                }
            }
            return true;
        }
        if (k().c != 0) {
            this.r = true;
        } else {
            this.r = false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                e(motionEvent);
                break;
            case 1:
                return c(motionEvent);
            case 2:
                b(motionEvent);
                break;
            default:
                return false;
        }
        return true;
    }

    @SuppressLint({"FloatMath"})
    public boolean a(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        float sqrt = (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
        if (sqrt <= 500.0f) {
            return false;
        }
        a(34, (int) (sqrt * 0.6f), (((int) motionEvent2.getY()) << 16) | ((int) motionEvent2.getX()));
        f();
        return true;
    }

    public com.baidu.platform.comjni.map.basemap.a b() {
        return this.j;
    }

    public void b(boolean z2) {
        this.t = z2;
    }

    public boolean b(int i2, int i3) {
        if (this.af || this.ag) {
            return false;
        }
        this.af = true;
        this.ad = i2;
        this.ae = i3;
        this.ab = this.l.f().fromPixels(i2, i3);
        if (this.ab == null) {
            this.af = false;
            return false;
        }
        this.ac = g();
        if (!this.ac) {
            this.af = false;
        }
        return this.ac;
    }

    public boolean b(MotionEvent motionEvent) {
        if (E.j) {
            return true;
        }
        float abs = Math.abs(motionEvent.getX() - Q);
        float abs2 = Math.abs(motionEvent.getY() - R);
        float f2 = (float) (((double) c.A) > 1.5d ? ((double) c.A) * 1.5d : (double) c.A);
        if (S && abs / f2 <= 3.0f && abs2 / f2 <= 3.0f) {
            return true;
        }
        S = false;
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        if (x2 < 0) {
            x2 = 0;
        }
        if (y2 < 0) {
            y2 = 0;
        }
        if (!this.s) {
            return false;
        }
        a(3, 0, (y2 << 16) | x2);
        return false;
    }

    public double c() {
        return this.l.i();
    }

    public void c(boolean z2) {
        this.u = z2;
    }

    public boolean c(int i2, int i3) {
        if (this.af || this.ag) {
            return false;
        }
        this.ag = true;
        this.ad = i2;
        this.ae = i3;
        this.ab = this.l.f().fromPixels(i2, i3);
        if (this.ab == null) {
            this.ag = false;
            return false;
        }
        this.ac = h();
        if (!this.ac) {
            this.ag = false;
        }
        return this.ac;
    }

    public boolean c(MotionEvent motionEvent) {
        int i2 = (E.j || motionEvent.getEventTime() - U >= W || Math.abs(motionEvent.getX() - Q) >= 10.0f || Math.abs(motionEvent.getY() - R) >= 10.0f) ? 0 : 1;
        f();
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        if (f(x2, y2) || b(i2, x2, y2)) {
            return true;
        }
        if (i2 == 1 && this.V && !g(x2, y2)) {
            return false;
        }
        if (i2 == 1 && this.V) {
            return true;
        }
        if (i2 != 0) {
            return false;
        }
        if (x2 < 0) {
            x2 = 0;
        }
        a(5, 0, ((y2 < 0 ? 0 : y2) << 16) | x2);
        return true;
    }

    public void d(int i2, int i3) {
        this.v = i2;
        this.w = i3;
    }

    public void d(MotionEvent motionEvent) {
        if (this.Z) {
            a(8195, (((int) motionEvent.getY()) << 16) | ((int) motionEvent.getX()), ((this.w / 2) << 16) | (this.v / 2));
        }
    }

    public void d(boolean z2) {
        this.q = z2;
    }

    public boolean d() {
        return this.s;
    }

    public void e(int i2, int i3) {
        if (i2 != 0 || i3 != 0) {
            this.l.a(i2, i3);
        }
    }

    public void e(boolean z2) {
        this.Z = z2;
    }

    public boolean e() {
        return this.Z;
    }

    public void f(boolean z2) {
        this.V = z2;
    }

    public boolean f(int i2, int i3) {
        for (String str : this.e.keySet()) {
            Map<String, Integer> map = this.e;
            if (this.j.a(map.get(str).intValue(), i2, i3, (int) (((double) this.x) * c())) != null) {
                return true;
            }
        }
        return false;
    }

    public boolean g() {
        return this.l.k() < 19.0f && a(4096, 0, 0) == 1;
    }

    public boolean h() {
        return this.l.k() > 3.0f && a(FragmentTransaction.TRANSIT_FRAGMENT_OPEN, 0, 0) == 1;
    }

    public int i() {
        return this.v;
    }

    public int j() {
        return this.w;
    }

    public s k() {
        boolean z2 = true;
        if (this.j == null) {
            return null;
        }
        Bundle g2 = this.j.g();
        s sVar = new s();
        sVar.a = (float) g2.getDouble("level");
        sVar.b = (int) g2.getDouble("rotation");
        sVar.c = (int) g2.getDouble("overlooking");
        sVar.d = (int) g2.getDouble("centerptx");
        sVar.e = (int) g2.getDouble("centerpty");
        sVar.f.a = g2.getInt("left");
        sVar.f.b = g2.getInt("right");
        sVar.f.c = g2.getInt("top");
        sVar.f.d = g2.getInt("bottom");
        sVar.g.a = g2.getLong("gleft");
        sVar.g.b = g2.getLong("gright");
        sVar.g.c = g2.getLong("gtop");
        sVar.g.d = g2.getLong("gbottom");
        sVar.g.e.a = g2.getInt("lbx");
        sVar.g.e.b = g2.getInt("lby");
        sVar.g.f.a = g2.getInt("ltx");
        sVar.g.f.b = g2.getInt("lty");
        sVar.g.g.a = g2.getInt("rtx");
        sVar.g.g.b = g2.getInt("rty");
        sVar.g.h.a = g2.getInt("rbx");
        sVar.g.h.b = g2.getInt("rby");
        sVar.h = g2.getLong("xoffset");
        sVar.i = g2.getLong("yoffset");
        if (g2.getInt("bfpp") != 1) {
            z2 = false;
        }
        sVar.j = z2;
        if (sVar.g.a <= -20037508) {
            sVar.g.a = -20037508;
        }
        if (sVar.g.b >= 20037508) {
            sVar.g.b = 20037508;
        }
        if (sVar.g.c >= 20037508) {
            sVar.g.c = 20037508;
        }
        if (sVar.g.d <= -20037508) {
            sVar.g.d = -20037508;
        }
        return sVar;
    }

    public float l() {
        if (this.j == null) {
            return 3.0f;
        }
        return (float) this.j.g().getDouble("level");
    }

    public void m() {
        q();
        this.o = null;
        if (this.j != null) {
            this.j.b();
            this.j = null;
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (this.l != view || keyEvent.getAction() != 0) {
            return false;
        }
        switch (i2) {
            case 19:
                e(0, -50);
                break;
            case 20:
                e(0, 50);
                break;
            case 21:
                e(-50, 0);
                break;
            case 22:
                e(50, 0);
                break;
            default:
                return false;
        }
        return true;
    }
}
