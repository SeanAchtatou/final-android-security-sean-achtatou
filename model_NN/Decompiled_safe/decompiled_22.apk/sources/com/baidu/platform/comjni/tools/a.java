package com.baidu.platform.comjni.tools;

import android.os.Bundle;
import com.baidu.platform.comapi.basestruct.c;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;

public class a {
    public static double a(c cVar, c cVar2) {
        Bundle bundle = new Bundle();
        bundle.putDouble("x1", (double) cVar.a);
        bundle.putDouble("y1", (double) cVar.b);
        bundle.putDouble("x2", (double) cVar2.a);
        bundle.putDouble("y2", (double) cVar2.b);
        JNITools.GetDistanceByMC(bundle);
        return bundle.getDouble("distance");
    }

    public static com.baidu.platform.comapi.basestruct.a a(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("strkey", str);
        JNITools.TransGeoStr2ComplexPt(bundle);
        com.baidu.platform.comapi.basestruct.a aVar = new com.baidu.platform.comapi.basestruct.a();
        Bundle bundle2 = bundle.getBundle("map_bound");
        if (bundle2 != null) {
            Bundle bundle3 = bundle2.getBundle("ll");
            if (bundle3 != null) {
                aVar.b = new c((int) bundle3.getDouble("ptx"), (int) bundle3.getDouble("pty"));
            }
            Bundle bundle4 = bundle2.getBundle(LocaleUtil.RUSSIAN);
            if (bundle4 != null) {
                aVar.c = new c((int) bundle4.getDouble("ptx"), (int) bundle4.getDouble("pty"));
            }
        }
        ParcelItem[] parcelItemArr = (ParcelItem[]) bundle.getParcelableArray("poly_line");
        for (ParcelItem bundle5 : parcelItemArr) {
            if (aVar.d == null) {
                aVar.d = new ArrayList<>();
            }
            Bundle bundle6 = bundle5.getBundle();
            if (bundle6 != null) {
                ParcelItem[] parcelItemArr2 = (ParcelItem[]) bundle6.getParcelableArray("point_array");
                ArrayList arrayList = new ArrayList();
                for (ParcelItem bundle7 : parcelItemArr2) {
                    Bundle bundle8 = bundle7.getBundle();
                    if (bundle8 != null) {
                        arrayList.add(new c((int) bundle8.getDouble("ptx"), (int) bundle8.getDouble("pty")));
                    }
                }
                arrayList.trimToSize();
                aVar.d.add(arrayList);
            }
        }
        aVar.d.trimToSize();
        aVar.a = (int) bundle.getDouble("type");
        return aVar;
    }
}
