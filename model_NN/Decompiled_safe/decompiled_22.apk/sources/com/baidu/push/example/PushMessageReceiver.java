package com.baidu.push.example;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.baidu.android.pushservice.PushConstants;
import com.city_life.ui.InitAcitvity;
import java.util.List;

public class PushMessageReceiver extends BroadcastReceiver {
    public static final String TAG = PushMessageReceiver.class.getSimpleName();
    AlertDialog.Builder builder;

    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, ">>> Receive intent: \r\n" + intent);
        if (intent.getAction().equals(PushConstants.ACTION_MESSAGE)) {
            String message = intent.getExtras().getString(PushConstants.EXTRA_PUSH_MESSAGE_STRING);
            Log.i(TAG, "onMessage: " + message);
            Intent responseIntent = new Intent(Utils.ACTION_MESSAGE);
            responseIntent.putExtra("message", message);
            responseIntent.setClass(context, InitAcitvity.class);
            responseIntent.addFlags(268435456);
            context.startActivity(responseIntent);
        } else if (intent.getAction().equals(PushConstants.ACTION_RECEIVE)) {
            String method = intent.getStringExtra("method");
            int errorCode = intent.getIntExtra(PushConstants.EXTRA_ERROR_CODE, 0);
            String content = new String(intent.getByteArrayExtra("content"));
            Log.d(TAG, "onMessage: method : " + method);
            Log.d(TAG, "onMessage: result : " + errorCode);
            Log.d(TAG, "onMessage: content : " + content);
        } else if (intent.getAction().equals(PushConstants.ACTION_RECEIVER_NOTIFICATION_CLICK)) {
            Log.d(TAG, "intent=" + intent.toUri(0));
            String packageName = context.getPackageName();
            List<ActivityManager.RunningTaskInfo> appTask = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1);
            if (appTask != null && appTask.size() > 0 && !appTask.get(0).topActivity.toString().contains(packageName)) {
                Intent aIntent = new Intent();
                aIntent.addFlags(268435456);
                aIntent.setClass(context, InitAcitvity.class);
                aIntent.putExtra(PushConstants.EXTRA_NOTIFICATION_TITLE, intent.getStringExtra(PushConstants.EXTRA_NOTIFICATION_TITLE));
                aIntent.putExtra(PushConstants.EXTRA_NOTIFICATION_CONTENT, intent.getStringExtra(PushConstants.EXTRA_NOTIFICATION_CONTENT));
                context.startActivity(aIntent);
            }
        }
    }
}
