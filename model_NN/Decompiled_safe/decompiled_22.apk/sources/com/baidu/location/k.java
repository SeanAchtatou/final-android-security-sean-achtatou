package com.baidu.location;

import android.location.Location;
import com.baidu.location.c;
import com.baidu.location.e;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

class k {
    private static double a = 0.1d;
    private static int b = 64;

    /* renamed from: byte  reason: not valid java name */
    private static Location f191byte = null;
    private static File c = null;

    /* renamed from: case  reason: not valid java name */
    private static int f192case = 5;

    /* renamed from: char  reason: not valid java name */
    private static int f193char = 1024;
    private static double d = 100.0d;

    /* renamed from: do  reason: not valid java name */
    private static Location f194do = null;
    private static double e = 0.0d;

    /* renamed from: else  reason: not valid java name */
    private static String f195else = f.g;
    private static ArrayList f = new ArrayList();

    /* renamed from: for  reason: not valid java name */
    private static e.c f196for = null;
    private static int g = 256;

    /* renamed from: goto  reason: not valid java name */
    private static Location f197goto = null;
    private static String h = (f.O + "/yo.dat");
    private static int i = 32;

    /* renamed from: if  reason: not valid java name */
    private static int f198if = 512;

    /* renamed from: int  reason: not valid java name */
    private static int f199int = 128;
    private static int j = 1024;

    /* renamed from: long  reason: not valid java name */
    private static double f200long = 30.0d;

    /* renamed from: new  reason: not valid java name */
    private static int f201new = 0;

    /* renamed from: try  reason: not valid java name */
    private static ArrayList f202try = new ArrayList();

    /* renamed from: void  reason: not valid java name */
    private static ArrayList f203void = new ArrayList();

    k() {
    }

    private static int a(int i2, int i3, int i4, long j2) {
        if (i2 < 0 || i2 > 256 || i3 > 2048 || i4 > 1024 || j2 > 5242880) {
            return -1;
        }
        j.a(f195else, "upload manager start to init cache ...");
        try {
            if (c == null) {
                c = new File(h);
                if (!c.exists()) {
                    File file = new File(f.O);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    if (c.createNewFile()) {
                        j.a(f195else, "upload manager create file success");
                    } else {
                        j.a(f195else, "upload manager create file error...");
                        c = null;
                        return -2;
                    }
                }
            }
            RandomAccessFile randomAccessFile = new RandomAccessFile(c, "rw");
            randomAccessFile.seek((long) i2);
            randomAccessFile.writeInt(0);
            randomAccessFile.writeInt(0);
            randomAccessFile.writeInt(i4);
            randomAccessFile.writeInt(i3);
            randomAccessFile.writeLong(j2);
            randomAccessFile.close();
            j.a(f195else, "cache inited ...");
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -3;
        }
    }

    private static int a(List list, int i2) {
        if (list == null || i2 > 256 || i2 < 0) {
            return -1;
        }
        try {
            if (c == null) {
                c = new File(h);
                if (!c.exists()) {
                    j.a(f195else, "upload man readfile does not exist...");
                    c = null;
                    return -2;
                }
            }
            RandomAccessFile randomAccessFile = new RandomAccessFile(c, "rw");
            if (randomAccessFile.length() < 1) {
                randomAccessFile.close();
                return -3;
            }
            randomAccessFile.seek((long) i2);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            int readInt3 = randomAccessFile.readInt();
            int readInt4 = randomAccessFile.readInt();
            long readLong = randomAccessFile.readLong();
            if (!a(readInt, readInt2, readInt3, readInt4, readLong) || readInt2 < 1) {
                randomAccessFile.close();
                return -4;
            }
            byte[] bArr = new byte[j];
            int i3 = readInt2;
            int i4 = i;
            while (i4 > 0 && i3 > 0) {
                randomAccessFile.seek(((long) ((((readInt + i3) - 1) % readInt3) * readInt4)) + readLong);
                int readInt5 = randomAccessFile.readInt();
                if (readInt5 > 0 && readInt5 < readInt4) {
                    randomAccessFile.read(bArr, 0, readInt5);
                    if (bArr[readInt5 - 1] == 0) {
                        list.add(new String(bArr, 0, readInt5 - 1));
                    }
                }
                i4--;
                i3--;
            }
            randomAccessFile.seek((long) i2);
            randomAccessFile.writeInt(readInt);
            randomAccessFile.writeInt(i3);
            randomAccessFile.writeInt(readInt3);
            randomAccessFile.writeInt(readInt4);
            randomAccessFile.writeLong(readLong);
            randomAccessFile.close();
            return i - i4;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -5;
        }
    }

    public static String a() {
        String str = null;
        if (f203void == null || f203void.size() < 1) {
            a(f203void, f201new);
        }
        if (f203void != null && f203void.size() >= 1) {
            str = (String) f203void.get(0);
            f203void.remove(0);
            j.a(f195else, "upload manager get upload data from q1 ...");
        }
        if (str == null) {
            if (f202try == null || f202try.size() < 1) {
                a(f202try, b);
            }
            if (f202try != null && f202try.size() >= 1) {
                str = (String) f202try.get(0);
                f202try.remove(0);
                j.a(f195else, "upload manager get upload data from q2 ...");
            }
        }
        if (str == null) {
            if (f == null || f.size() < 1) {
                a(f, f199int);
            }
            if (f != null && f.size() >= 1) {
                str = (String) f.get(0);
                f.remove(0);
                j.a(f195else, "upload manager get upload data from q3 ...");
            }
        }
        j.a(f195else, "upload manager get upload data : " + str);
        return str;
    }

    public static void a(double d2, double d3, double d4, double d5) {
        if (d2 <= 0.0d) {
            d2 = e;
        }
        e = d2;
        a = d3;
        if (d4 <= 20.0d) {
            d4 = f200long;
        }
        f200long = d4;
        d = d5;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.baidu.location.e$c} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: com.baidu.location.c$a} */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1 */
    /* JADX WARN: Type inference failed for: r0v3 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(com.baidu.location.c.a r4, com.baidu.location.e.c r5, android.location.Location r6, java.lang.String r7) {
        /*
            r0 = 0
            java.lang.String r1 = com.baidu.location.k.f195else
            java.lang.String r2 = "upload manager insert2UploadQueue..."
            com.baidu.location.j.a(r1, r2)
            if (r4 == 0) goto L_0x002e
            boolean r1 = r4.m93if()
            if (r1 == 0) goto L_0x002e
            boolean r1 = a(r6, r5)
            if (r1 != 0) goto L_0x0017
            r5 = r0
        L_0x0017:
            r0 = 1
            java.lang.String r0 = com.baidu.location.j.a(r4, r5, r6, r7, r0)
            if (r0 == 0) goto L_0x002d
            java.lang.String r0 = com.baidu.location.Jni.m0if(r0)
            m193if(r0)
            com.baidu.location.k.f191byte = r6
            com.baidu.location.k.f197goto = r6
            if (r5 == 0) goto L_0x002d
            com.baidu.location.k.f196for = r5
        L_0x002d:
            return
        L_0x002e:
            if (r5 == 0) goto L_0x0075
            boolean r1 = r5.m111if()
            if (r1 == 0) goto L_0x0075
            boolean r1 = a(r6, r5)
            if (r1 == 0) goto L_0x0075
            boolean r1 = a(r6)
            if (r1 != 0) goto L_0x009d
        L_0x0042:
            r1 = 2
            java.lang.String r0 = com.baidu.location.j.a(r0, r5, r6, r7, r1)
            if (r0 == 0) goto L_0x002d
            java.lang.String r0 = com.baidu.location.Jni.m0if(r0)
            java.lang.String r1 = com.baidu.location.k.f195else
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "upload size:"
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r0.length()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.baidu.location.j.a(r1, r2)
            a(r0)
            com.baidu.location.k.f194do = r6
            com.baidu.location.k.f197goto = r6
            if (r5 == 0) goto L_0x002d
            com.baidu.location.k.f196for = r5
            goto L_0x002d
        L_0x0075:
            boolean r1 = a(r6)
            if (r1 != 0) goto L_0x007c
            r4 = r0
        L_0x007c:
            boolean r1 = a(r6, r5)
            if (r1 != 0) goto L_0x009b
        L_0x0082:
            if (r4 != 0) goto L_0x0086
            if (r0 == 0) goto L_0x002d
        L_0x0086:
            r1 = 3
            java.lang.String r1 = com.baidu.location.j.a(r4, r0, r6, r7, r1)
            if (r1 == 0) goto L_0x002d
            java.lang.String r1 = com.baidu.location.Jni.m0if(r1)
            m190do(r1)
            com.baidu.location.k.f197goto = r6
            if (r0 == 0) goto L_0x002d
            com.baidu.location.k.f196for = r0
            goto L_0x002d
        L_0x009b:
            r0 = r5
            goto L_0x0082
        L_0x009d:
            r0 = r4
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.k.a(com.baidu.location.c$a, com.baidu.location.e$c, android.location.Location, java.lang.String):void");
    }

    public static void a(c.a aVar, e.c cVar, String str, double d2, double d3, String str2) {
        String str3 = String.format("&manll=%.5f|%.5f&manaddr=%s", Double.valueOf(d2), Double.valueOf(d3), str2) + j.a(aVar, cVar, null, str, 1);
        if (str3 != null) {
            m193if(Jni.m0if(str3));
        }
    }

    private static void a(String str) {
        if (f202try != null) {
            j.a(f195else, "insert2WifiQueue...");
            if (f202try.size() <= i) {
                f202try.add(str);
            }
            if (f202try.size() >= i && m191if(f202try, b) < -1) {
                a(b, j, f198if, (long) (g + (j * f198if)));
                m191if(f202try, b);
            }
        }
    }

    private static boolean a(int i2, int i3, int i4, int i5, long j2) {
        return i2 >= 0 && i2 < i4 && i3 >= 0 && i3 <= i4 && i4 >= 0 && i4 <= 1024 && i5 >= 128 && i5 <= 1024;
    }

    private static boolean a(Location location) {
        if (location == null) {
            return false;
        }
        if (f191byte == null) {
            f191byte = location;
            return true;
        }
        double distanceTo = (double) location.distanceTo(f191byte);
        return ((double) location.distanceTo(f197goto)) > ((distanceTo * a) + ((e * distanceTo) * distanceTo)) + f200long;
    }

    private static boolean a(Location location, e.c cVar) {
        if (location == null || cVar == null || cVar.f124do == null) {
            return false;
        }
        if (f194do != null) {
            return ((double) location.distanceTo(f194do)) < d;
        }
        f194do = location;
        return true;
    }

    /* renamed from: do  reason: not valid java name */
    private static void m190do(String str) {
        if (f != null) {
            j.a(f195else, "insert2GpsQueue...");
            if (f.size() <= i) {
                f.add(str);
            }
            if (f.size() >= i && m191if(f, f199int) < -1) {
                a(f199int, j, f193char, (long) (g + (j * f198if * 2)));
                m191if(f, f199int);
            }
        }
    }

    /* renamed from: if  reason: not valid java name */
    private static int m191if(List list, int i2) {
        int i3;
        int i4;
        int i5 = 0;
        if (list == null || list.size() < 1 || i2 > 256 || i2 < 0) {
            return -1;
        }
        try {
            if (c == null) {
                c = new File(h);
                if (!c.exists()) {
                    j.a(f195else, "upload man write file does not exist...");
                    c = null;
                    return -2;
                }
            }
            RandomAccessFile randomAccessFile = new RandomAccessFile(c, "rw");
            if (randomAccessFile.length() < 1) {
                randomAccessFile.close();
                return -3;
            }
            randomAccessFile.seek((long) i2);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            int readInt3 = randomAccessFile.readInt();
            int readInt4 = randomAccessFile.readInt();
            long readLong = randomAccessFile.readLong();
            if (!a(readInt, readInt2, readInt3, readInt4, readLong)) {
                randomAccessFile.close();
                return -4;
            }
            for (int size = list.size(); size > f192case; size--) {
                randomAccessFile.seek(((long) ((((readInt + readInt2) + i5) % readInt3) * readInt4)) + readLong);
                byte[] bytes = (((String) list.get(0)) + 0).getBytes();
                randomAccessFile.writeInt(bytes.length);
                randomAccessFile.write(bytes, 0, bytes.length);
                list.remove(0);
                i5++;
            }
            int i6 = readInt2 + i5;
            if (i6 > readInt3) {
                i3 = (readInt + (i6 - readInt3)) % readInt3;
                i4 = readInt3;
            } else {
                int i7 = i6;
                i3 = readInt;
                i4 = i7;
            }
            randomAccessFile.seek((long) i2);
            randomAccessFile.writeInt(i3);
            randomAccessFile.writeInt(i4);
            randomAccessFile.writeInt(readInt3);
            randomAccessFile.writeInt(readInt4);
            randomAccessFile.writeLong(readLong);
            randomAccessFile.close();
            return i5;
        } catch (IOException e2) {
            e2.printStackTrace();
            return -5;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public static void m192if() {
        j.a(f195else, "upload manager flush...");
        f192case = 0;
        if (m191if(f203void, f201new) < -1) {
            a(f201new, j, f198if, (long) g);
            m191if(f203void, f201new);
        }
        if (m191if(f202try, b) < -1) {
            a(b, j, f198if, (long) (g + (j * f198if)));
            m191if(f202try, b);
        }
        if (m191if(f, f199int) < -1) {
            a(f199int, j, f193char, (long) (g + (j * f198if * 2)));
            m191if(f, f199int);
        }
        f192case = 5;
    }

    /* renamed from: if  reason: not valid java name */
    private static void m193if(String str) {
        if (f203void != null) {
            j.a(f195else, "insert2CellQueue...");
            if (f203void.size() <= i) {
                f203void.add(str);
            }
            if (f203void.size() >= i && m191if(f203void, f201new) < -1) {
                a(f201new, j, f198if, (long) g);
                m191if(f203void, f201new);
            }
        }
    }

    /* renamed from: if  reason: not valid java name */
    public static boolean m194if(Location location) {
        return b.a(f197goto, location);
    }
}
