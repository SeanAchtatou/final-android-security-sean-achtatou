package com.baidu.location;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.httpclient.HttpStatus;

class b {

    /* renamed from: char  reason: not valid java name */
    private static final int f60char = 3000;
    /* access modifiers changed from: private */
    public static String d = null;

    /* renamed from: else  reason: not valid java name */
    private static final int f61else = 3;
    private static final double f = 1.0E-5d;

    /* renamed from: goto  reason: not valid java name */
    private static final String f62goto = "baidu_location_service";
    /* access modifiers changed from: private */
    public static int i = 0;

    /* renamed from: long  reason: not valid java name */
    private static final int f63long = 5;
    /* access modifiers changed from: private */
    public boolean a = false;
    private a b = null;

    /* renamed from: byte  reason: not valid java name */
    private boolean f64byte = true;
    /* access modifiers changed from: private */
    public long c = 0;

    /* renamed from: case  reason: not valid java name */
    private boolean f65case = false;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public String f66do = null;
    /* access modifiers changed from: private */
    public boolean e = false;

    /* renamed from: for  reason: not valid java name */
    private Handler f67for = null;
    private Context g;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */

    /* renamed from: if  reason: not valid java name */
    public String f68if = null;

    /* renamed from: int  reason: not valid java name */
    private final int f69int = HttpStatus.SC_BAD_REQUEST;
    /* access modifiers changed from: private */
    public long j = 0;
    private C0001b k = null;
    private Location l;
    private final long m = 1000;
    /* access modifiers changed from: private */
    public List n = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: new  reason: not valid java name */
    public long f70new = 0;
    /* access modifiers changed from: private */
    public LocationManager o = null;
    /* access modifiers changed from: private */
    public String p = null;

    /* renamed from: try  reason: not valid java name */
    private String f71try = null;
    /* access modifiers changed from: private */

    /* renamed from: void  reason: not valid java name */
    public GpsStatus f72void;

    private class a implements GpsStatus.NmeaListener, GpsStatus.Listener {
        private a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.a(com.baidu.location.b, boolean):void
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.a(com.baidu.location.b, long):long
          com.baidu.location.b.a(com.baidu.location.b, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.b.a(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.a(com.baidu.location.b, android.location.Location):void
          com.baidu.location.b.a(android.location.Location, android.location.Location):boolean
          com.baidu.location.b.a(com.baidu.location.b, boolean):void */
        public void onGpsStatusChanged(int i) {
            if (b.this.o != null) {
                switch (i) {
                    case 2:
                        b.this.a((Location) null);
                        b.this.a(false);
                        int unused = b.i = 0;
                        return;
                    case 3:
                    default:
                        return;
                    case 4:
                        j.a("baidu_location_service", "gps status change");
                        if (b.this.f72void == null) {
                            GpsStatus unused2 = b.this.f72void = b.this.o.getGpsStatus(null);
                        } else {
                            b.this.o.getGpsStatus(b.this.f72void);
                        }
                        int i2 = 0;
                        for (GpsSatellite usedInFix : b.this.f72void.getSatellites()) {
                            i2 = usedInFix.usedInFix() ? i2 + 1 : i2;
                        }
                        j.a("baidu_location_service", "gps nunmber in count:" + i2);
                        if (b.i >= 3 && i2 < 3) {
                            long unused3 = b.this.c = System.currentTimeMillis();
                        }
                        if (i2 < 3) {
                            b.this.a(false);
                        }
                        if (b.i <= 3 && i2 > 3) {
                            b.this.a(true);
                        }
                        int unused4 = b.i = i2;
                        return;
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.if(com.baidu.location.b, boolean):boolean
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.if(com.baidu.location.b, long):long
          com.baidu.location.b.if(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.if(com.baidu.location.b, boolean):boolean */
        public void onNmeaReceived(long j, String str) {
            if (str != null && !str.equals("")) {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - b.this.j > 400 && b.this.e && b.this.n.size() > 0) {
                    try {
                        c cVar = new c(b.this.n, b.this.p, b.this.f66do, b.this.f68if);
                        if (cVar.m72if()) {
                            j.f187long = cVar.c();
                            if (j.f187long > 0) {
                                String unused = b.d = String.format("&ll=%.5f|%.5f&s=%.1f&d=%.1f&ll_r=%d&ll_n=%d&ll_h=%.2f&nmea=%.1f|%.1f&ll_t=%d&g_tp=%d", Double.valueOf(cVar.d()), Double.valueOf(cVar.l()), Double.valueOf(cVar.m66case()), Double.valueOf(cVar.j()), 0, Integer.valueOf(cVar.m71goto()), Double.valueOf(cVar.m76try()), Double.valueOf(cVar.a()), Double.valueOf(cVar.b()), Long.valueOf(currentTimeMillis / 1000), Integer.valueOf(j.f187long));
                            }
                        } else {
                            j.f187long = 0;
                            j.a("baidu_location_service", "nmea invalid");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        j.f187long = 0;
                    }
                    b.this.n.clear();
                    String unused2 = b.this.p = b.this.f66do = b.this.f68if = (String) null;
                    boolean unused3 = b.this.e = false;
                }
                if (str.startsWith(c.x)) {
                    boolean unused4 = b.this.e = true;
                    String unused5 = b.this.p = str.trim();
                } else if (str.startsWith(c.l)) {
                    b.this.n.add(str.trim());
                } else if (str.startsWith(c.g)) {
                    String unused6 = b.this.f66do = str.trim();
                } else if (str.startsWith(c.f73if)) {
                    String unused7 = b.this.f68if = str.trim();
                }
                long unused8 = b.this.j = System.currentTimeMillis();
            }
        }
    }

    /* renamed from: com.baidu.location.b$b  reason: collision with other inner class name */
    private class C0001b implements LocationListener {
        private C0001b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.do(com.baidu.location.b, boolean):boolean
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.do(com.baidu.location.b, long):long
          com.baidu.location.b.do(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.do(com.baidu.location.b, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.a(com.baidu.location.b, boolean):void
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.a(com.baidu.location.b, long):long
          com.baidu.location.b.a(com.baidu.location.b, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.b.a(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.a(com.baidu.location.b, android.location.Location):void
          com.baidu.location.b.a(android.location.Location, android.location.Location):boolean
          com.baidu.location.b.a(com.baidu.location.b, boolean):void */
        public void onLocationChanged(Location location) {
            b.this.a(location);
            boolean unused = b.this.a = false;
            if (b.this.h) {
                b.this.a(true);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.a(com.baidu.location.b, boolean):void
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.a(com.baidu.location.b, long):long
          com.baidu.location.b.a(com.baidu.location.b, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.b.a(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.a(com.baidu.location.b, android.location.Location):void
          com.baidu.location.b.a(android.location.Location, android.location.Location):boolean
          com.baidu.location.b.a(com.baidu.location.b, boolean):void */
        public void onProviderDisabled(String str) {
            b.this.a((Location) null);
            b.this.a(false);
        }

        public void onProviderEnabled(String str) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.a(com.baidu.location.b, boolean):void
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.a(com.baidu.location.b, long):long
          com.baidu.location.b.a(com.baidu.location.b, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.b.a(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.a(com.baidu.location.b, android.location.Location):void
          com.baidu.location.b.a(android.location.Location, android.location.Location):boolean
          com.baidu.location.b.a(com.baidu.location.b, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.do(com.baidu.location.b, boolean):boolean
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.do(com.baidu.location.b, long):long
          com.baidu.location.b.do(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.do(com.baidu.location.b, boolean):boolean */
        public void onStatusChanged(String str, int i, Bundle bundle) {
            switch (i) {
                case 0:
                    b.this.a((Location) null);
                    b.this.a(false);
                    return;
                case 1:
                    long unused = b.this.f70new = System.currentTimeMillis();
                    boolean unused2 = b.this.a = true;
                    b.this.a(false);
                    return;
                case 2:
                    boolean unused3 = b.this.a = false;
                    return;
                default:
                    return;
            }
        }
    }

    public class c {
        public static final String g = "$GPRMC";

        /* renamed from: if  reason: not valid java name */
        public static final String f73if = "$GPGSA";
        public static final String l = "$GPGSV";
        public static final String x = "$GPGGA";
        private String A = "";
        private double B = 0.0d;
        private int C = 1;
        private int a = 10;
        private int b = 2;

        /* renamed from: byte  reason: not valid java name */
        private double f74byte = 1.1d;
        private int c = 0;

        /* renamed from: case  reason: not valid java name */
        private double f75case = 2.3d;

        /* renamed from: char  reason: not valid java name */
        private char f76char = 'N';
        private double d = 500.0d;

        /* renamed from: do  reason: not valid java name */
        private int f77do = 7;
        private double e = 2.2d;

        /* renamed from: else  reason: not valid java name */
        private double f78else = 0.0d;
        private double f = 3.8d;

        /* renamed from: for  reason: not valid java name */
        private int f79for = 0;

        /* renamed from: goto  reason: not valid java name */
        private int f80goto = 70;
        private boolean h = true;
        private boolean i = false;

        /* renamed from: int  reason: not valid java name */
        private int f81int = 20;
        private double j = 0.0d;
        private boolean k = false;

        /* renamed from: long  reason: not valid java name */
        private double f82long = 0.0d;
        private List m = null;
        private double n = 0.0d;

        /* renamed from: new  reason: not valid java name */
        private double f83new = 500.0d;
        private int o = 0;
        private String p = "";
        private boolean q = false;
        private String r = "";
        private boolean s = false;
        public int t = 0;

        /* renamed from: try  reason: not valid java name */
        private String f84try = "";
        private List u = null;
        private String v = "";

        /* renamed from: void  reason: not valid java name */
        private int f85void = 3;
        private int w = 120;
        private double z = 500.0d;

        public class a {
            private int a = 0;

            /* renamed from: do  reason: not valid java name */
            private int f86do = 0;

            /* renamed from: if  reason: not valid java name */
            private int f88if = 0;

            /* renamed from: int  reason: not valid java name */
            private int f89int = 0;

            public a(int i, int i2, int i3, int i4) {
                this.f89int = i;
                this.a = i2;
                this.f88if = i3;
                this.f86do = i4;
            }

            public int a() {
                return this.a;
            }

            /* renamed from: do  reason: not valid java name */
            public int m78do() {
                return this.f86do;
            }

            /* renamed from: for  reason: not valid java name */
            public int m79for() {
                return this.f89int;
            }

            /* renamed from: if  reason: not valid java name */
            public int m80if() {
                return this.f88if;
            }
        }

        public c(List list, String str, String str2, String str3) {
            this.m = list;
            this.f84try = str;
            this.A = str2;
            this.r = str3;
            this.u = new ArrayList();
            m77void();
        }

        private boolean a(String str) {
            if (str == null || str.length() < 4) {
                return false;
            }
            char c2 = 0;
            for (int i2 = 1; i2 < str.length() - 3; i2++) {
                c2 ^= str.charAt(i2);
            }
            return Integer.toHexString(c2).equalsIgnoreCase(str.substring(str.length() + -2, str.length()));
        }

        private double[] a(double d2, double d3) {
            double d4 = 0.0d;
            if (d3 != 0.0d) {
                d4 = Math.toDegrees(Math.atan(d2 / d3));
            } else if (d2 > 0.0d) {
                d4 = 90.0d;
            } else if (d2 < 0.0d) {
                d4 = 270.0d;
            }
            return new double[]{Math.sqrt((d2 * d2) + (d3 * d3)), d4};
        }

        private double[] a(List list) {
            if (list == null || list.size() <= 0) {
                return null;
            }
            double[] dArr = m64if((double) (90 - ((a) list.get(0)).m80if()), (double) ((a) list.get(0)).a());
            if (list.size() > 1) {
                for (int i2 = 1; i2 < list.size(); i2++) {
                    double[] dArr2 = m64if((double) (90 - ((a) list.get(i2)).m80if()), (double) ((a) list.get(i2)).a());
                    dArr[0] = (dArr[0] + dArr2[0]) / 2.0d;
                    dArr[1] = (dArr[1] + dArr2[1]) / 2.0d;
                }
            }
            return dArr;
        }

        /* renamed from: if  reason: not valid java name */
        private double[] m64if(double d2, double d3) {
            return new double[]{Math.sin(Math.toRadians(d3)) * d2, Math.cos(Math.toRadians(d3)) * d2};
        }

        public double a() {
            return this.j;
        }

        public int a(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
            int i2;
            double[] a2;
            int i3;
            if (!this.h) {
                return 0;
            }
            if (z2 && this.q) {
                this.t = 1;
                if (this.f79for >= this.a) {
                    return 1;
                }
                if (this.f79for <= this.f85void) {
                    return 4;
                }
            }
            if (z3 && this.s) {
                this.t = 2;
                if (this.j <= this.f74byte) {
                    return 1;
                }
                if (this.j >= this.e) {
                    return 4;
                }
            }
            if (z4 && this.s) {
                this.t = 3;
                if (this.B <= this.f75case) {
                    return 1;
                }
                if (this.B >= this.f) {
                    return 4;
                }
            }
            if (z5 && this.i) {
                this.t = 4;
                int i4 = 0;
                Iterator it = this.u.iterator();
                while (true) {
                    i3 = i4;
                    if (!it.hasNext()) {
                        break;
                    }
                    i4 = ((a) it.next()).m78do() >= this.f81int ? i3 + 1 : i3;
                }
                if (i3 >= this.f77do) {
                    return 1;
                }
                if (i3 <= this.b) {
                    return 4;
                }
            }
            if (z6 && this.i) {
                this.t = 5;
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                for (int i5 = 0; i5 < 10; i5++) {
                    arrayList.add(new ArrayList());
                }
                int i6 = 0;
                Iterator it2 = this.u.iterator();
                while (true) {
                    i2 = i6;
                    if (!it2.hasNext()) {
                        break;
                    }
                    a aVar = (a) it2.next();
                    if (aVar.m78do() >= 10 && aVar.m80if() >= 1) {
                        ((List) arrayList.get((aVar.m78do() - 10) / 5)).add(aVar);
                        i2++;
                    }
                    i6 = i2;
                }
                if (i2 < 4) {
                    return 4;
                }
                int i7 = 0;
                while (true) {
                    int i8 = i7;
                    if (i8 >= arrayList.size()) {
                        break;
                    }
                    if (!(((List) arrayList.get(i8)).size() == 0 || (a2 = a((List) arrayList.get(i8))) == null)) {
                        arrayList2.add(a2);
                        arrayList3.add(Integer.valueOf(i8));
                    }
                    i7 = i8 + 1;
                }
                if (arrayList2 == null || arrayList2.size() <= 0) {
                    return 4;
                }
                double[] dArr = (double[]) arrayList2.get(0);
                dArr[0] = dArr[0] * ((double) ((Integer) arrayList3.get(0)).intValue());
                dArr[1] = dArr[1] * ((double) ((Integer) arrayList3.get(0)).intValue());
                if (arrayList2.size() > 1) {
                    int i9 = 1;
                    while (true) {
                        int i10 = i9;
                        if (i10 >= arrayList2.size()) {
                            break;
                        }
                        double[] dArr2 = (double[]) arrayList2.get(i10);
                        dArr2[0] = dArr2[0] * ((double) ((Integer) arrayList3.get(i10)).intValue());
                        dArr2[1] = dArr2[1] * ((double) ((Integer) arrayList3.get(i10)).intValue());
                        dArr[0] = (dArr[0] + dArr2[0]) / 2.0d;
                        dArr[1] = (dArr[1] + dArr2[1]) / 2.0d;
                        i9 = i10 + 1;
                    }
                }
                double[] a3 = a(dArr[0], dArr[1]);
                if (a3[0] <= ((double) this.f80goto)) {
                    return 1;
                }
                if (a3[0] >= ((double) this.w)) {
                    return 4;
                }
            }
            this.t = 0;
            return 3;
        }

        public double b() {
            return this.B;
        }

        /* renamed from: byte  reason: not valid java name */
        public String m65byte() {
            return this.v;
        }

        public int c() {
            return a(true, true, true, true, true);
        }

        /* renamed from: case  reason: not valid java name */
        public double m66case() {
            return this.n;
        }

        /* renamed from: char  reason: not valid java name */
        public boolean m67char() {
            return this.q;
        }

        public double d() {
            return this.f83new;
        }

        /* renamed from: do  reason: not valid java name */
        public List m68do() {
            return this.u;
        }

        public String e() {
            return this.r;
        }

        /* renamed from: else  reason: not valid java name */
        public List m69else() {
            return this.m;
        }

        public int f() {
            return this.C;
        }

        /* renamed from: for  reason: not valid java name */
        public double m70for() {
            return this.f82long;
        }

        public int g() {
            return this.c;
        }

        /* renamed from: goto  reason: not valid java name */
        public int m71goto() {
            return this.f79for;
        }

        public boolean h() {
            return this.s;
        }

        public boolean i() {
            return this.k;
        }

        /* renamed from: if  reason: not valid java name */
        public boolean m72if() {
            return this.h;
        }

        /* renamed from: int  reason: not valid java name */
        public String m73int() {
            return this.p;
        }

        public double j() {
            return this.z;
        }

        public String k() {
            return this.A;
        }

        public double l() {
            return this.d;
        }

        /* renamed from: long  reason: not valid java name */
        public boolean m74long() {
            return this.i;
        }

        public int m() {
            return this.o;
        }

        public String n() {
            return this.f84try;
        }

        /* renamed from: new  reason: not valid java name */
        public char m75new() {
            return this.f76char;
        }

        /* renamed from: try  reason: not valid java name */
        public double m76try() {
            return this.f78else;
        }

        /* renamed from: void  reason: not valid java name */
        public void m77void() {
            if (a(this.f84try)) {
                String substring = this.f84try.substring(0, this.f84try.length() - 3);
                int i2 = 0;
                for (int i3 = 0; i3 < substring.length(); i3++) {
                    if (substring.charAt(i3) == ',') {
                        i2++;
                    }
                }
                String[] split = substring.split(",", i2 + 1);
                if (!split[1].equals("") && !split[2].equals("") && !split[4].equals("") && !split[6].equals("") && !split[7].equals("") && !split[9].equals("")) {
                    int i4 = 1;
                    int i5 = 1;
                    if (split[3].equals("S")) {
                        i4 = -1;
                    }
                    if (split[5].equals("W")) {
                        i5 = -1;
                    }
                    this.d = ((double) i4) * (((double) Integer.valueOf(split[2].substring(0, 2)).intValue()) + (Double.valueOf(split[2].substring(2, split[2].length())).doubleValue() / 60.0d));
                    this.f83new = ((double) i5) * (((double) Integer.valueOf(split[4].substring(0, 3)).intValue()) + (Double.valueOf(split[4].substring(3, split[4].length())).doubleValue() / 60.0d));
                    this.f78else = Double.valueOf(split[9]).doubleValue();
                    this.c = Integer.valueOf(split[6]).intValue();
                    this.f79for = Integer.valueOf(split[7]).intValue();
                    this.q = true;
                }
            }
            if (a(this.A)) {
                String substring2 = this.A.substring(0, this.A.length() - 3);
                int i6 = 0;
                for (int i7 = 0; i7 < substring2.length(); i7++) {
                    if (substring2.charAt(i7) == ',') {
                        i6++;
                    }
                }
                String[] split2 = substring2.split(",", i6 + 1);
                if (!split2[9].equals("") && !split2[2].equals("")) {
                    this.f76char = Character.valueOf(split2[2].charAt(0)).charValue();
                    this.n = split2[7].equals("") ? 0.0d : 1.852d * Double.valueOf(split2[7]).doubleValue();
                    this.z = (split2[8].equals("") || split2[8].equalsIgnoreCase("nan")) ? 500.0d : Double.valueOf(split2[8]).doubleValue();
                    this.k = true;
                }
            }
            if (a(this.r)) {
                String substring3 = this.r.substring(0, this.r.length() - 3);
                int i8 = 0;
                for (int i9 = 0; i9 < substring3.length(); i9++) {
                    if (substring3.charAt(i9) == ',') {
                        i8++;
                    }
                }
                String[] split3 = substring3.split(",", i8 + 1);
                if (!split3[2].equals("") && !split3[split3.length - 3].equals("") && !split3[split3.length - 2].equals("") && !split3[split3.length - 1].equals("")) {
                    this.C = Integer.valueOf(split3[2]).intValue();
                    this.B = Double.valueOf(split3[split3.length - 3]).doubleValue();
                    this.j = Double.valueOf(split3[split3.length - 2]).doubleValue();
                    this.f82long = Double.valueOf(split3[split3.length - 1]).doubleValue();
                    this.s = true;
                }
            }
            if (this.m == null || this.m.size() <= 0) {
                this.i = false;
            } else {
                this.i = Integer.valueOf(((String) this.m.get(0)).split(",")[1]).intValue() == this.m.size();
                if (this.i) {
                    Iterator it = this.m.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        String str = (String) it.next();
                        if (!a(str)) {
                            this.i = false;
                            break;
                        }
                        String str2 = str.split(",", 5)[4];
                        String substring4 = str2.substring(0, str2.length() - 3);
                        int i10 = 0;
                        for (int i11 = 0; i11 < substring4.length(); i11++) {
                            if (substring4.charAt(i11) == ',') {
                                i10++;
                            }
                        }
                        String[] split4 = substring4.split(",", i10 + 1);
                        int i12 = 0;
                        while (true) {
                            int i13 = i12;
                            if (i13 < split4.length) {
                                if (!split4[i13].equals("") && !split4[i13 + 1].equals("") && !split4[i13 + 2].equals("")) {
                                    this.o++;
                                    this.u.add(new a(Integer.valueOf(split4[i13]).intValue(), Integer.valueOf(split4[i13 + 2]).intValue(), Integer.valueOf(split4[i13 + 1]).intValue(), split4[i13 + 3].equals("") ? 0 : Integer.valueOf(split4[i13 + 3]).intValue()));
                                }
                                i12 = i13 + 4;
                            }
                        }
                    }
                }
            }
            this.h = this.q && this.s;
        }
    }

    public b(Context context, Handler handler) {
        this.g = context;
        this.f67for = handler;
    }

    /* access modifiers changed from: private */
    public void a(Location location) {
        j.a("baidu_location_service", "set new gpsLocation ...");
        this.l = location;
        if (this.l == null) {
            this.f71try = null;
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            this.l.setTime(currentTimeMillis);
            this.f71try = String.format("&ll=%.5f|%.5f&s=%.1f&d=%.1f&ll_n=%d&ll_t=%d", Double.valueOf(this.l.getLongitude()), Double.valueOf(this.l.getLatitude()), Float.valueOf((float) (((double) this.l.getSpeed()) * 3.6d)), Float.valueOf(this.l.getBearing()), Integer.valueOf(i), Long.valueOf(currentTimeMillis));
        }
        this.f67for.obtainMessage(51).sendToTarget();
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        this.h = z;
        if ((!z || m58case()) && j.f189try != z) {
            j.f189try = z;
            if (j.f190void) {
                this.f67for.obtainMessage(53).sendToTarget();
            }
        }
    }

    public static boolean a(Location location, Location location2) {
        if (location == location2) {
            return false;
        }
        if (location == null || location2 == null) {
            return true;
        }
        float speed = location2.getSpeed();
        float distanceTo = location2.distanceTo(location);
        return ((double) speed) > 10.0d ? distanceTo > j.f179case : ((double) speed) > 2.0d ? distanceTo > j.f185if : Math.abs(location.getLatitude() - location2.getLatitude()) > f || Math.abs(location.getLongitude() - location2.getLongitude()) > f;
    }

    /* renamed from: byte  reason: not valid java name */
    public static String m42byte() {
        return d;
    }

    /* renamed from: if  reason: not valid java name */
    public static String m51if(Location location) {
        if (location == null) {
            return null;
        }
        return String.format("&ll=%.5f|%.5f&s=%.1f&d=%.1f&ll_r=%d&ll_n=%d&ll_h=%.2f&ll_t=%d&g_tp=0", Double.valueOf(location.getLongitude()), Double.valueOf(location.getLatitude()), Float.valueOf((float) (((double) location.getSpeed()) * 3.6d)), Float.valueOf(location.getBearing()), Integer.valueOf((int) (location.hasAccuracy() ? location.getAccuracy() : -1.0f)), Integer.valueOf(i), Double.valueOf(location.hasAltitude() ? location.getAltitude() : 555.0d), Long.valueOf(location.getTime() / 1000));
    }

    public void a() {
        if (this.f65case) {
            if (this.o != null) {
                try {
                    if (this.k != null) {
                        this.o.removeUpdates(this.k);
                    }
                    if (this.b != null) {
                        this.o.removeGpsStatusListener(this.b);
                    }
                } catch (Exception e2) {
                }
            }
            this.k = null;
            this.b = null;
            this.o = null;
            this.f65case = false;
            a(false);
        }
    }

    /* renamed from: case  reason: not valid java name */
    public boolean m58case() {
        if (!m63try()) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (this.a && currentTimeMillis - this.f70new > 3000) {
            return false;
        }
        if (i >= 3) {
            return true;
        }
        return currentTimeMillis - this.c < 3000;
    }

    /* renamed from: do  reason: not valid java name */
    public Location m59do() {
        return this.l;
    }

    /* renamed from: if  reason: not valid java name */
    public String m60if() {
        if (this.l != null) {
            String str = "{\"result\":{\"time\":\"" + j.a() + "\",\"error\":\"61\"},\"content\":{\"point\":{\"x\":" + "\"%f\",\"y\":\"%f\"},\"radius\":\"%d\",\"d\":\"%f\"," + "\"s\":\"%f\",\"n\":\"%d\"}}";
            int accuracy = (int) (this.l.hasAccuracy() ? this.l.getAccuracy() : 10.0f);
            float speed = (float) (((double) this.l.getSpeed()) * 3.6d);
            double[] dArr = Jni.m1if(this.l.getLongitude(), this.l.getLatitude(), "gps2gcj");
            if (dArr[0] <= 0.0d && dArr[1] <= 0.0d) {
                dArr[0] = this.l.getLongitude();
                dArr[1] = this.l.getLatitude();
            }
            String format = String.format(str, Double.valueOf(dArr[0]), Double.valueOf(dArr[1]), Integer.valueOf(accuracy), Float.valueOf(this.l.getBearing()), Float.valueOf(speed), Integer.valueOf(i));
            j.a("baidu_location_service", "wgs84: " + this.l.getLongitude() + " " + this.l.getLatitude() + " gcj02: " + dArr[0] + " " + dArr[1]);
            return format;
        }
        j.a("baidu_location_service", "gps man getGpsJson but gpslocation is null");
        return null;
    }

    /* renamed from: int  reason: not valid java name */
    public void m61int() {
        if (!this.f65case) {
            try {
                this.o = (LocationManager) this.g.getSystemService("location");
                this.k = new C0001b();
                this.b = new a();
                this.o.requestLocationUpdates("gps", 1000, 5.0f, this.k);
                this.o.addGpsStatusListener(this.b);
                this.o.addNmeaListener(this.b);
                this.f65case = true;
            } catch (Exception e2) {
                j.a("baidu_location_service", e2.getMessage());
            }
        }
    }

    /* renamed from: new  reason: not valid java name */
    public String m62new() {
        return this.f71try;
    }

    /* renamed from: try  reason: not valid java name */
    public boolean m63try() {
        return (this.l == null || this.l.getLatitude() == 0.0d || this.l.getLongitude() == 0.0d) ? false : true;
    }
}
