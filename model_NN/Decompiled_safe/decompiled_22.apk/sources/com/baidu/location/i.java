package com.baidu.location;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import java.util.ArrayList;
import java.util.Iterator;

public class i {

    /* renamed from: new  reason: not valid java name */
    public static final String f165new = "android.com.baidu.location.TIMER.NOTIFY";
    private int a = 0;
    private long b = 0;
    /* access modifiers changed from: private */

    /* renamed from: byte  reason: not valid java name */
    public ArrayList f166byte = null;
    private boolean c = false;

    /* renamed from: case  reason: not valid java name */
    private BDLocation f167case = null;

    /* renamed from: char  reason: not valid java name */
    private long f168char = 0;
    /* access modifiers changed from: private */
    public LocationClient d = null;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public String f169do = f.g;

    /* renamed from: else  reason: not valid java name */
    private b f170else = null;

    /* renamed from: for  reason: not valid java name */
    private AlarmManager f171for = null;

    /* renamed from: goto  reason: not valid java name */
    private float f172goto = Float.MAX_VALUE;

    /* renamed from: if  reason: not valid java name */
    private Context f173if = null;

    /* renamed from: int  reason: not valid java name */
    private a f174int = new a();

    /* renamed from: long  reason: not valid java name */
    private boolean f175long = false;

    /* renamed from: try  reason: not valid java name */
    private PendingIntent f176try = null;

    /* renamed from: void  reason: not valid java name */
    private boolean f177void = false;

    public class a implements BDLocationListener {
        public a() {
        }

        public void onReceiveLocation(BDLocation bDLocation) {
            i.this.a(bDLocation);
        }

        public void onReceivePoi(BDLocation bDLocation) {
        }
    }

    public class b extends BroadcastReceiver {
        public b() {
        }

        public void onReceive(Context context, Intent intent) {
            j.a(i.this.f169do, "timer expire,request location...");
            if (i.this.f166byte != null && !i.this.f166byte.isEmpty()) {
                i.this.d.requestNotifyLocation();
            }
        }
    }

    public i(Context context, LocationClient locationClient) {
        this.f173if = context;
        this.d = locationClient;
        this.d.registerNotifyLocationListener(this.f174int);
        this.f171for = (AlarmManager) this.f173if.getSystemService("alarm");
        this.f170else = new b();
        this.c = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r8 = this;
            r1 = 10000(0x2710, float:1.4013E-41)
            r2 = 0
            r3 = 1
            boolean r0 = r8.m177do()
            if (r0 != 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            float r0 = r8.f172goto
            r4 = 1167867904(0x459c4000, float:5000.0)
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0043
            r0 = 600000(0x927c0, float:8.40779E-40)
        L_0x0017:
            boolean r4 = r8.f175long
            if (r4 == 0) goto L_0x005f
            r8.f175long = r2
        L_0x001d:
            int r0 = r8.a
            if (r0 == 0) goto L_0x005d
            long r4 = r8.f168char
            int r0 = r8.a
            long r6 = (long) r0
            long r4 = r4 + r6
            long r6 = java.lang.System.currentTimeMillis()
            long r4 = r4 - r6
            long r6 = (long) r1
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x005d
            r0 = r2
        L_0x0032:
            if (r0 == 0) goto L_0x000a
            r8.a = r1
            long r0 = java.lang.System.currentTimeMillis()
            r8.f168char = r0
            int r0 = r8.a
            long r0 = (long) r0
            r8.a(r0)
            goto L_0x000a
        L_0x0043:
            float r0 = r8.f172goto
            r4 = 1148846080(0x447a0000, float:1000.0)
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x004f
            r0 = 120000(0x1d4c0, float:1.68156E-40)
            goto L_0x0017
        L_0x004f:
            float r0 = r8.f172goto
            r4 = 1140457472(0x43fa0000, float:500.0)
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x005b
            r0 = 60000(0xea60, float:8.4078E-41)
            goto L_0x0017
        L_0x005b:
            r0 = r1
            goto L_0x0017
        L_0x005d:
            r0 = r3
            goto L_0x0032
        L_0x005f:
            r1 = r0
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.i.a():void");
    }

    private void a(long j) {
        if (this.f177void) {
            this.f171for.cancel(this.f176try);
        }
        this.f176try = PendingIntent.getBroadcast(this.f173if, 0, new Intent(f165new), 134217728);
        this.f171for.set(0, System.currentTimeMillis() + j, this.f176try);
        j.a(this.f169do, "timer start:" + j);
    }

    /* access modifiers changed from: private */
    public void a(BDLocation bDLocation) {
        float f;
        j.a(this.f169do, "notify new loation");
        this.f177void = false;
        if (bDLocation.getLocType() != 61 && bDLocation.getLocType() != 161 && bDLocation.getLocType() != 65) {
            a(120000);
        } else if (System.currentTimeMillis() - this.b >= 5000 && this.f166byte != null) {
            this.f167case = bDLocation;
            this.b = System.currentTimeMillis();
            float[] fArr = new float[1];
            float f2 = Float.MAX_VALUE;
            Iterator it = this.f166byte.iterator();
            while (true) {
                f = f2;
                if (!it.hasNext()) {
                    break;
                }
                BDNotifyListener bDNotifyListener = (BDNotifyListener) it.next();
                Location.distanceBetween(bDLocation.getLatitude(), bDLocation.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
                f2 = (fArr[0] - bDNotifyListener.mRadius) - bDLocation.getRadius();
                j.a(this.f169do, "distance:" + f2);
                if (f2 > 0.0f) {
                    if (f2 < f) {
                    }
                    f2 = f;
                } else {
                    if (bDNotifyListener.Notified < 3) {
                        bDNotifyListener.Notified++;
                        bDNotifyListener.onNotify(bDLocation, fArr[0]);
                        if (bDNotifyListener.Notified < 3) {
                            this.f175long = true;
                        }
                    }
                    f2 = f;
                }
            }
            if (f < this.f172goto) {
                this.f172goto = f;
            }
            this.a = 0;
            a();
        }
    }

    /* renamed from: do  reason: not valid java name */
    private boolean m177do() {
        boolean z = false;
        if (this.f166byte == null || this.f166byte.isEmpty()) {
            return false;
        }
        Iterator it = this.f166byte.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            z = ((BDNotifyListener) it.next()).Notified < 3 ? true : z2;
        }
    }

    public void a(BDNotifyListener bDNotifyListener) {
        j.a(this.f169do, bDNotifyListener.mCoorType + "2gcj");
        if (bDNotifyListener.mCoorType != null) {
            if (!bDNotifyListener.mCoorType.equals("gcj02")) {
                double[] dArr = Jni.m1if(bDNotifyListener.mLongitude, bDNotifyListener.mLatitude, bDNotifyListener.mCoorType + "2gcj");
                bDNotifyListener.mLongitudeC = dArr[0];
                bDNotifyListener.mLatitudeC = dArr[1];
                j.a(this.f169do, bDNotifyListener.mCoorType + "2gcj");
                j.a(this.f169do, "coor:" + bDNotifyListener.mLongitude + "," + bDNotifyListener.mLatitude + ":" + bDNotifyListener.mLongitudeC + "," + bDNotifyListener.mLatitudeC);
            }
            if (this.f167case == null || System.currentTimeMillis() - this.b > 300000) {
                this.d.requestNotifyLocation();
            } else {
                float[] fArr = new float[1];
                Location.distanceBetween(this.f167case.getLatitude(), this.f167case.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
                float radius = (fArr[0] - bDNotifyListener.mRadius) - this.f167case.getRadius();
                if (radius > 0.0f) {
                    if (radius < this.f172goto) {
                        this.f172goto = radius;
                    }
                } else if (bDNotifyListener.Notified < 3) {
                    bDNotifyListener.Notified++;
                    bDNotifyListener.onNotify(this.f167case, fArr[0]);
                    if (bDNotifyListener.Notified < 3) {
                        this.f175long = true;
                    }
                }
            }
            a();
        }
    }

    /* renamed from: do  reason: not valid java name */
    public int m179do(BDNotifyListener bDNotifyListener) {
        if (this.f166byte == null) {
            return 0;
        }
        if (this.f166byte.contains(bDNotifyListener)) {
            this.f166byte.remove(bDNotifyListener);
        }
        if (this.f166byte.size() == 0 && this.f177void) {
            this.f171for.cancel(this.f176try);
        }
        return 1;
    }

    /* renamed from: if  reason: not valid java name */
    public int m180if(BDNotifyListener bDNotifyListener) {
        if (this.f166byte == null) {
            this.f166byte = new ArrayList();
        }
        this.f166byte.add(bDNotifyListener);
        bDNotifyListener.isAdded = true;
        bDNotifyListener.mNotifyCache = this;
        if (!this.c) {
            this.f173if.registerReceiver(this.f170else, new IntentFilter(f165new));
            this.c = true;
        }
        if (bDNotifyListener.mCoorType != null) {
            if (!bDNotifyListener.mCoorType.equals("gcj02")) {
                double[] dArr = Jni.m1if(bDNotifyListener.mLongitude, bDNotifyListener.mLatitude, bDNotifyListener.mCoorType + "2gcj");
                bDNotifyListener.mLongitudeC = dArr[0];
                bDNotifyListener.mLatitudeC = dArr[1];
                j.a(this.f169do, bDNotifyListener.mCoorType + "2gcj");
                j.a(this.f169do, "coor:" + bDNotifyListener.mLongitude + "," + bDNotifyListener.mLatitude + ":" + bDNotifyListener.mLongitudeC + "," + bDNotifyListener.mLatitudeC);
            }
            if (this.f167case == null || System.currentTimeMillis() - this.b > 30000) {
                this.d.requestNotifyLocation();
            } else {
                float[] fArr = new float[1];
                Location.distanceBetween(this.f167case.getLatitude(), this.f167case.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
                float radius = (fArr[0] - bDNotifyListener.mRadius) - this.f167case.getRadius();
                if (radius > 0.0f) {
                    if (radius < this.f172goto) {
                        this.f172goto = radius;
                    }
                } else if (bDNotifyListener.Notified < 3) {
                    bDNotifyListener.Notified++;
                    bDNotifyListener.onNotify(this.f167case, fArr[0]);
                    if (bDNotifyListener.Notified < 3) {
                        this.f175long = true;
                    }
                }
            }
            a();
        }
        return 1;
    }

    /* renamed from: if  reason: not valid java name */
    public void m181if() {
        if (this.f177void) {
            this.f171for.cancel(this.f176try);
        }
        this.f167case = null;
        this.b = 0;
        if (this.c) {
            j.a(this.f169do, "unregister...");
            this.f173if.unregisterReceiver(this.f170else);
        }
        this.c = false;
    }
}
