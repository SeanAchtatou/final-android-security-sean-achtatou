package com.baidu.mapapi.search;

import com.baidu.mapapi.utils.DistanceUtil;
import com.baidu.mapapi.utils.d;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import java.util.ArrayList;
import java.util.Iterator;

public class MKRoute {
    public static final int ROUTE_TYPE_BUS_LINE = 3;
    public static final int ROUTE_TYPE_DRIVING = 1;
    public static final int ROUTE_TYPE_UNKNOW = 0;
    public static final int ROUTE_TYPE_WALKING = 2;
    ArrayList<ArrayList<GeoPoint>> a;
    private int b;
    private int c;
    private int d;
    private GeoPoint e;
    private GeoPoint f;
    private ArrayList<ArrayList<GeoPoint>> g;
    private ArrayList<MKStep> h;
    private String i;

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.c = i2;
    }

    /* access modifiers changed from: package-private */
    public void a(GeoPoint geoPoint) {
        this.e = geoPoint;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        StringBuilder sb = new StringBuilder();
        char[] charArray = str.toCharArray();
        boolean z = false;
        for (int i2 = 0; i2 < charArray.length; i2++) {
            if (charArray[i2] == '<') {
                z = true;
            } else if (charArray[i2] == '>') {
                z = false;
            } else if (!z) {
                sb.append(charArray[i2]);
            }
        }
        this.i = sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList<MKStep> arrayList) {
        this.h = arrayList;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        this.b = i2;
    }

    /* access modifiers changed from: package-private */
    public void b(GeoPoint geoPoint) {
        this.f = geoPoint;
    }

    /* access modifiers changed from: package-private */
    public void b(ArrayList<ArrayList<GeoPoint>> arrayList) {
        this.g = arrayList;
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        this.d = i2;
    }

    public void customizeRoute(GeoPoint geoPoint, GeoPoint geoPoint2, GeoPoint[] geoPointArr) {
        if (geoPoint != null && geoPoint2 != null && geoPointArr != null) {
            customizeRoute(geoPoint, geoPoint2, new GeoPoint[][]{geoPointArr});
        }
    }

    public void customizeRoute(GeoPoint geoPoint, GeoPoint geoPoint2, GeoPoint[][] geoPointArr) {
        if (geoPoint != null && geoPoint2 != null && geoPointArr != null) {
            if (geoPoint != null) {
                this.e = geoPoint;
            }
            if (geoPoint2 != null) {
                this.f = geoPoint2;
            }
            this.d = 3;
            double d2 = 0.0d;
            GeoPoint geoPoint3 = null;
            if (geoPointArr != null && geoPointArr.length > 0) {
                this.g = new ArrayList<>();
                for (GeoPoint[] geoPointArr2 : geoPointArr) {
                    if (geoPointArr2 != null) {
                        ArrayList arrayList = new ArrayList();
                        for (int i2 = 0; i2 < geoPointArr2.length; i2++) {
                            if (geoPointArr2[i2] != null) {
                                arrayList.add(geoPointArr2[i2]);
                            }
                        }
                        this.g.add(arrayList);
                    }
                }
                this.a = new ArrayList<>();
                this.h = new ArrayList<>();
                int i3 = 0;
                while (i3 < this.g.size()) {
                    ArrayList arrayList2 = this.g.get(i3);
                    ArrayList arrayList3 = new ArrayList();
                    MKStep mKStep = new MKStep();
                    GeoPoint geoPoint4 = geoPoint3;
                    int i4 = 0;
                    double d3 = d2;
                    GeoPoint geoPoint5 = geoPoint4;
                    while (i4 < arrayList2.size()) {
                        if (i3 == 0 && i4 == 0 && arrayList2.size() > 1) {
                            MKStep mKStep2 = new MKStep();
                            mKStep2.a((GeoPoint) arrayList2.get(i4));
                            mKStep2.a(String.valueOf(this.h.size()));
                            this.h.add(mKStep2);
                        }
                        arrayList3.add(d.b((GeoPoint) arrayList2.get(i4)));
                        double distance = geoPoint5 != null ? d3 + DistanceUtil.getDistance((GeoPoint) arrayList2.get(i4), geoPoint5) : d3;
                        if (i4 == arrayList2.size() - 1) {
                            mKStep.a((GeoPoint) arrayList2.get(i4));
                            mKStep.a(String.valueOf(this.h.size()));
                        }
                        i4++;
                        d3 = distance;
                        geoPoint5 = (GeoPoint) arrayList2.get(i4);
                    }
                    this.a.add(arrayList3);
                    this.h.add(mKStep);
                    i3++;
                    geoPoint3 = geoPoint5;
                    d2 = d3;
                }
                this.c = (int) d2;
            }
        }
    }

    public ArrayList<ArrayList<GeoPoint>> getArrayPoints() {
        if (this.g.size() == 0 && this.d == 1) {
            Iterator<MKStep> it = this.h.iterator();
            while (it.hasNext()) {
                c.a(it.next().b(), this.g, this.a);
            }
        }
        return this.g;
    }

    public int getDistance() {
        return this.c;
    }

    public GeoPoint getEnd() {
        return this.f;
    }

    public int getIndex() {
        return this.b;
    }

    public int getNumSteps() {
        if (this.h != null) {
            return this.h.size();
        }
        return 0;
    }

    public int getRouteType() {
        return this.d;
    }

    public GeoPoint getStart() {
        return this.e;
    }

    public MKStep getStep(int i2) {
        if (this.h == null || i2 < 0 || i2 > this.h.size() - 1) {
            return null;
        }
        return this.h.get(i2);
    }

    public String getTip() {
        return this.i;
    }
}
