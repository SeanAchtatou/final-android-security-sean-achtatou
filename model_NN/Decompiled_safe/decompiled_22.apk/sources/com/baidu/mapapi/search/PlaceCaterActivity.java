package com.baidu.mapapi.search;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.baidu.mapapi.search.a;
import com.baidu.platform.comapi.d.c;
import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import sina_weibo.Constants;

public class PlaceCaterActivity extends Activity implements a.C0002a {
    static ImageView c;
    static boolean d;
    static DisplayMetrics o;
    static Hashtable<Integer, View> q = new Hashtable<>();
    static Handler r = new f();
    private static int s = -2;
    private static int t = -1;
    private static int u = 10;
    private static int v = 5;
    private static int w = 1;
    private static int x = -7566196;
    private static int y = -12487463;
    private static int z = -1710619;
    TextView a;
    TextView b;
    LinearLayout e;
    TextView f;
    TextView g;
    TextView h;
    TextView i;
    TextView j;
    TextView k;
    TextView l;
    TextView m;
    LinearLayout n;
    e p = new e();

    private Bitmap a(String str) {
        try {
            return BitmapFactory.decodeStream(getAssets().open(str));
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private void a(LinearLayout linearLayout, List<d> list) {
        if (list != null && list.size() > 0) {
            this.n.removeAllViews();
            q.clear();
            int size = list.size();
            int i2 = (size / 2) + (size % 2);
            for (int i3 = 0; i3 < i2; i3++) {
                LinearLayout linearLayout2 = new LinearLayout(this);
                linearLayout2.setLayoutParams(new ViewGroup.LayoutParams(t, s));
                linearLayout.addView(linearLayout2);
                LinearLayout linearLayout3 = new LinearLayout(this);
                linearLayout3.setOrientation(0);
                linearLayout3.setLayoutParams(new ViewGroup.LayoutParams(t, s));
                linearLayout3.setPadding(20, 5, 5, 5);
                linearLayout2.addView(linearLayout3);
                ((LinearLayout.LayoutParams) linearLayout3.getLayoutParams()).weight = 1.0f;
                ImageView imageView = new ImageView(this);
                imageView.setLayoutParams(new ViewGroup.LayoutParams((int) (22.0f * o.density), (int) (22.0f * o.density)));
                imageView.setTag(Integer.valueOf(i3 * 2));
                a.a(linearLayout.hashCode(), (i3 * 2) + 1, d.a.replaceAll("#replace#", list.get(i3 * 2).d), this);
                q.put(Integer.valueOf((i3 * 2) + 1), imageView);
                linearLayout3.addView(imageView);
                ((LinearLayout.LayoutParams) imageView.getLayoutParams()).gravity = 17;
                TextView textView = new TextView(this);
                textView.setTag(list.get(i3 * 2));
                textView.setPadding(u, u, u, u);
                textView.setLayoutParams(new ViewGroup.LayoutParams(s, s));
                textView.setClickable(true);
                textView.setText(list.get(i3 * 2).b);
                textView.setTextColor(y);
                textView.setOnClickListener(new h(this));
                linearLayout3.addView(textView);
                ((LinearLayout.LayoutParams) textView.getLayoutParams()).gravity = 17;
                if ((i3 * 2) + 1 < size) {
                    LinearLayout linearLayout4 = new LinearLayout(this);
                    linearLayout4.setPadding(20, 5, 5, 5);
                    linearLayout4.setLayoutParams(new ViewGroup.LayoutParams(t, s));
                    linearLayout2.addView(linearLayout4);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) linearLayout4.getLayoutParams();
                    layoutParams.weight = 1.0f;
                    layoutParams.gravity = 17;
                    ImageView imageView2 = new ImageView(this);
                    imageView2.setLayoutParams(new ViewGroup.LayoutParams((int) (22.0f * o.density), (int) (22.0f * o.density)));
                    list.get((i3 * 2) + 1);
                    a.a(linearLayout.hashCode(), (i3 * 2) + 1 + 1, d.a.replaceAll("#replace#", list.get((i3 * 2) + 1).d), this);
                    q.put(Integer.valueOf((i3 * 2) + 1 + 1), imageView2);
                    linearLayout4.addView(imageView2);
                    ((LinearLayout.LayoutParams) imageView2.getLayoutParams()).gravity = 16;
                    TextView textView2 = new TextView(this);
                    textView2.setTag(list.get((i3 * 2) + 1));
                    textView2.setPadding(u, u, u, u);
                    textView2.setClickable(true);
                    textView2.setTextColor(y);
                    textView2.setText(list.get((i3 * 2) + 1).b);
                    textView2.setOnClickListener(new i(this));
                    linearLayout4.addView(textView2);
                    ((LinearLayout.LayoutParams) textView2.getLayoutParams()).gravity = 17;
                }
            }
        }
    }

    public static boolean isShow() {
        return d;
    }

    /* access modifiers changed from: package-private */
    public void a(float f2) {
        if (this.e != null) {
            this.e.removeAllViews();
            int i2 = (int) f2;
            for (int i3 = 0; i3 < 5; i3++) {
                if (i3 < i2) {
                    ImageView imageView = new ImageView(this);
                    imageView.setImageBitmap(a("place/star_light.png"));
                    imageView.setLayoutParams(new ViewGroup.LayoutParams((int) (o.density * 20.0f), (int) (o.density * 20.0f)));
                    imageView.setPadding(1, 1, 1, 1);
                    this.e.addView(imageView);
                } else {
                    ImageView imageView2 = new ImageView(this);
                    imageView2.setImageBitmap(a("place/star_gray.png"));
                    imageView2.setLayoutParams(new ViewGroup.LayoutParams((int) (o.density * 20.0f), (int) (o.density * 20.0f)));
                    imageView2.setPadding(1, 1, 1, 1);
                    this.e.addView(imageView2);
                }
            }
            TextView textView = new TextView(this);
            textView.setLayoutParams(new ViewGroup.LayoutParams(s, s));
            textView.setText(Float.toString(f2));
            textView.setPadding(10, 0, 10, 0);
            textView.setTextColor(-16777216);
            this.e.addView(textView);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(DisplayMetrics displayMetrics) {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundColor(-3355444);
        linearLayout.setPadding(1, 1, 1, 1);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setPadding(1, 1, 1, 1);
        linearLayout2.setBackgroundColor(-1);
        linearLayout2.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        linearLayout2.setOrientation(1);
        linearLayout.addView(linearLayout2);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) linearLayout2.getLayoutParams();
        layoutParams.rightMargin = w;
        layoutParams.bottomMargin = w;
        layoutParams.topMargin = w;
        layoutParams.leftMargin = w;
        this.a = new TextView(this);
        this.a.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        this.a.setTextSize(18.0f);
        this.a.setText("");
        this.a.setPadding(v, v, v, v);
        this.a.setTextColor(-16777216);
        this.a.setTypeface(Typeface.DEFAULT, 1);
        linearLayout2.addView(this.a);
        ((LinearLayout.LayoutParams) this.a.getLayoutParams()).leftMargin = 1;
        this.b = new TextView(this);
        this.b.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        this.b.setTextSize(16.0f);
        this.b.setPadding(u, u, u, u);
        this.b.setTextColor(x);
        linearLayout2.addView(this.b);
        LinearLayout linearLayout3 = new LinearLayout(this);
        linearLayout3.setBackgroundColor(-1);
        linearLayout3.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linearLayout.addView(linearLayout3);
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) linearLayout3.getLayoutParams();
        layoutParams2.topMargin = w;
        layoutParams2.rightMargin = w;
        layoutParams2.bottomMargin = w;
        layoutParams2.leftMargin = w;
        c = new ImageView(this);
        c.setPadding(5, 5, 5, 5);
        c.setLayoutParams(new ViewGroup.LayoutParams((int) (120.0f * displayMetrics.density), (int) (90.0f * displayMetrics.density)));
        linearLayout3.addView(c);
        LinearLayout linearLayout4 = new LinearLayout(this);
        linearLayout4.setOrientation(1);
        linearLayout4.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        linearLayout4.setPadding(u, u, u, u);
        linearLayout3.addView(linearLayout4);
        ((LinearLayout.LayoutParams) linearLayout4.getLayoutParams()).gravity = 16;
        this.e = new LinearLayout(this);
        this.e.setPadding(2, 2, 2, 2);
        this.e.setOrientation(0);
        linearLayout4.addView(this.e);
        LinearLayout linearLayout5 = new LinearLayout(this);
        linearLayout5.setPadding(2, 2, 2, 2);
        linearLayout5.setLayoutParams(new ViewGroup.LayoutParams(s, s));
        linearLayout4.addView(linearLayout5);
        TextView textView = new TextView(this);
        textView.setTextColor(x);
        textView.setTextSize(16.0f);
        textView.setText("参考价：");
        linearLayout5.addView(textView);
        this.f = new TextView(this);
        this.f.setTextColor(-4712681);
        this.f.setTextSize(16.0f);
        linearLayout5.addView(this.f);
        LinearLayout linearLayout6 = new LinearLayout(this);
        linearLayout6.setPadding(2, 2, 2, 2);
        linearLayout4.addView(linearLayout6);
        this.g = new TextView(this);
        this.g.setPadding(0, 0, 5, 0);
        this.g.setText("口味:3.0");
        this.g.setTextColor(x);
        this.g.setTextSize(12.0f);
        linearLayout6.addView(this.g);
        this.h = new TextView(this);
        this.h.setPadding(0, 0, 5, 0);
        this.h.setText("服务:3.0");
        this.h.setTextColor(x);
        this.h.setTextSize(12.0f);
        linearLayout6.addView(this.h);
        this.i = new TextView(this);
        this.i.setPadding(0, 0, 5, 0);
        this.i.setText("环境:3.0");
        this.i.setTextColor(x);
        this.i.setTextSize(12.0f);
        linearLayout6.addView(this.i);
        LinearLayout linearLayout7 = new LinearLayout(this);
        linearLayout7.setBackgroundColor(-1);
        linearLayout7.setPadding(5, 5, 5, 5);
        linearLayout7.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linearLayout7.setOrientation(0);
        linearLayout.addView(linearLayout7);
        LinearLayout.LayoutParams layoutParams3 = (LinearLayout.LayoutParams) linearLayout7.getLayoutParams();
        layoutParams3.topMargin = w;
        layoutParams3.rightMargin = w;
        layoutParams3.bottomMargin = w;
        layoutParams3.leftMargin = w;
        linearLayout7.setOnClickListener(new g(this));
        ImageView imageView = new ImageView(this);
        imageView.setPadding(5, 5, 5, 5);
        imageView.setLayoutParams(new ViewGroup.LayoutParams((int) (35.0f * displayMetrics.density), (int) (35.0f * displayMetrics.density)));
        imageView.setImageBitmap(a("place/iconphone.png"));
        linearLayout7.addView(imageView);
        ((LinearLayout.LayoutParams) imageView.getLayoutParams()).gravity = 16;
        this.j = new TextView(this);
        this.j.setTextColor(-16777216);
        this.j.setText("(010)4343243");
        this.j.setPadding(5, 5, 5, 5);
        this.j.setTextSize(16.0f);
        this.j.setLayoutParams(new ViewGroup.LayoutParams(s, s));
        linearLayout7.addView(this.j);
        LinearLayout.LayoutParams layoutParams4 = (LinearLayout.LayoutParams) this.j.getLayoutParams();
        layoutParams4.weight = 1.0f;
        layoutParams4.gravity = 16;
        ImageView imageView2 = new ImageView(this);
        imageView2.setLayoutParams(new ViewGroup.LayoutParams(s, s));
        imageView2.setImageBitmap(a("place/arrow.png"));
        imageView2.setPadding(5, 5, 5, 10);
        linearLayout7.addView(imageView2);
        ((LinearLayout.LayoutParams) imageView2.getLayoutParams()).gravity = 16;
        LinearLayout linearLayout8 = new LinearLayout(this);
        linearLayout8.setBackgroundColor(z);
        linearLayout8.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        linearLayout8.setOrientation(1);
        linearLayout.addView(linearLayout8);
        LinearLayout.LayoutParams layoutParams5 = (LinearLayout.LayoutParams) linearLayout8.getLayoutParams();
        layoutParams5.topMargin = w;
        layoutParams5.rightMargin = w;
        layoutParams5.bottomMargin = w;
        layoutParams5.leftMargin = w;
        TextView textView2 = new TextView(this);
        textView2.setTextSize(18.0f);
        textView2.setText("商户简介");
        textView2.setPadding(v, v, v, v);
        textView2.setTextColor(-16777216);
        textView2.setLayoutParams(new ViewGroup.LayoutParams(s, s));
        linearLayout8.addView(textView2);
        this.k = new TextView(this);
        this.k.setBackgroundColor(-1);
        this.k.setTextColor(x);
        this.k.setPadding(u, u, u, u);
        this.k.setTextSize(16.0f);
        this.k.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        linearLayout8.addView(this.k);
        this.l = new TextView(this);
        this.l.setBackgroundColor(-1);
        this.l.setTextColor(x);
        this.l.setPadding(u, u, u, u);
        this.l.setTextSize(16.0f);
        this.l.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        linearLayout8.addView(this.l);
        LinearLayout linearLayout9 = new LinearLayout(this);
        linearLayout9.setBackgroundColor(z);
        linearLayout9.setOrientation(1);
        linearLayout9.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        linearLayout.addView(linearLayout9);
        LinearLayout.LayoutParams layoutParams6 = (LinearLayout.LayoutParams) linearLayout9.getLayoutParams();
        layoutParams6.topMargin = w;
        layoutParams6.rightMargin = w;
        layoutParams6.bottomMargin = w;
        layoutParams6.leftMargin = w;
        TextView textView3 = new TextView(this);
        textView3.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        textView3.setText("评论信息");
        textView3.setPadding(v, v, v, v);
        textView3.setTextColor(-16777216);
        textView3.setTextSize(18.0f);
        linearLayout9.addView(textView3);
        this.m = new TextView(this);
        this.m.setPadding(u, u, u, u);
        this.m.setBackgroundColor(-1);
        this.m.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        this.m.setTextSize(16.0f);
        this.m.setTextColor(x);
        linearLayout9.addView(this.m);
        LinearLayout linearLayout10 = new LinearLayout(this);
        linearLayout10.setBackgroundColor(z);
        linearLayout10.setOrientation(1);
        linearLayout10.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        linearLayout.addView(linearLayout10);
        LinearLayout.LayoutParams layoutParams7 = (LinearLayout.LayoutParams) linearLayout10.getLayoutParams();
        layoutParams7.topMargin = w;
        layoutParams7.rightMargin = w;
        layoutParams7.bottomMargin = w;
        layoutParams7.leftMargin = w;
        TextView textView4 = new TextView(this);
        textView4.setLayoutParams(new ViewGroup.LayoutParams(s, s));
        textView4.setTextSize(18.0f);
        textView4.setPadding(v, v, v, v);
        textView4.setTextColor(-16777216);
        textView4.setText("查看更多");
        linearLayout10.addView(textView4);
        this.n = new LinearLayout(this);
        this.n.setOrientation(1);
        this.n.setBackgroundColor(-1);
        this.n.setLayoutParams(new ViewGroup.LayoutParams(t, s));
        linearLayout10.addView(this.n);
        ScrollView scrollView = new ScrollView(this);
        scrollView.setPadding(5, 5, 0, 5);
        scrollView.setLayoutParams(new ViewGroup.LayoutParams(t, t));
        scrollView.setBackgroundColor(-526345);
        scrollView.addView(linearLayout);
        ((FrameLayout.LayoutParams) linearLayout.getLayoutParams()).rightMargin = 5;
        setContentView(scrollView);
    }

    /* access modifiers changed from: package-private */
    public void a(e eVar) {
        float f2;
        this.a.setText(eVar.a);
        this.b.setText("地址：" + eVar.b);
        this.f.setText("￥" + eVar.g);
        this.g.setText("口味:" + eVar.h);
        this.h.setText("服务:" + eVar.j);
        this.i.setText("环境:" + eVar.i);
        this.j.setText(eVar.c);
        if (eVar.l == null || "".equals(eVar.l)) {
            this.k.setVisibility(8);
        } else {
            this.k.setVisibility(0);
            this.k.setText("推荐菜：" + eVar.l);
        }
        if (eVar.k == null || "".equals(eVar.k)) {
            this.l.setVisibility(8);
        } else {
            this.l.setVisibility(0);
            this.l.setText("商户描述：" + eVar.k);
        }
        if (eVar.m == null || "".equals(eVar.m)) {
            this.m.setVisibility(8);
        } else {
            this.m.setVisibility(0);
            this.m.setText(eVar.m);
        }
        if (eVar.e != null) {
            a.a(c.hashCode(), 0, eVar.e, this);
        }
        try {
            f2 = Float.valueOf(eVar.f).floatValue();
        } catch (NumberFormatException e2) {
            NumberFormatException numberFormatException = e2;
            f2 = 0.0f;
            numberFormatException.printStackTrace();
        }
        a(f2);
        a(this.n, eVar.o);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        d = true;
        Bundle extras = getIntent().getExtras();
        if (!c.a(extras.getString("result"), this.p)) {
            this.p.a = extras.getString(Constants.SINA_NAME);
            this.p.b = extras.getString("addr");
            this.p.c = extras.getString("tel");
            this.p.d = extras.getString(Constants.SINA_UID);
            this.p.e = extras.getString("image");
            this.p.f = extras.getString("overall_rating");
            this.p.g = extras.getString("price");
            this.p.h = extras.getString("taste_rating");
            this.p.i = extras.getString("enviroment_raing");
            this.p.j = extras.getString("service_rating");
            this.p.k = extras.getString(com.tencent.tauth.Constants.PARAM_COMMENT);
            this.p.l = extras.getString("recommendation");
            this.p.m = extras.getString("review");
            this.p.n = extras.getString("user_logo");
            String[] stringArray = extras.getStringArray("aryMoreLinkName");
            String[] stringArray2 = extras.getStringArray("aryMoreLinkUrl");
            String[] stringArray3 = extras.getStringArray("aryMoreLinkCnName");
            if (!(stringArray == null || stringArray2 == null)) {
                for (int i2 = 0; i2 < stringArray2.length; i2++) {
                    if (!"dianping".equals(stringArray[i2])) {
                        d dVar = new d();
                        dVar.d = stringArray[i2];
                        dVar.c = stringArray2[i2];
                        dVar.b = stringArray3[i2];
                        this.p.o.add(dVar);
                    }
                }
            }
        }
        com.baidu.platform.comapi.c.a.a().c();
        o = getResources().getDisplayMetrics();
        a(o);
        a(this.p);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (q != null) {
            q.clear();
        }
        o = null;
        c = null;
        d = false;
        com.baidu.platform.comapi.c.a.b();
        super.onDestroy();
    }

    public void onError(int i2, int i3, String str, Object obj) {
    }

    public void onOk(int i2, int i3, String str, Object obj) {
        if (i2 == c.hashCode()) {
            Message obtainMessage = r.obtainMessage(1);
            obtainMessage.obj = obj;
            obtainMessage.sendToTarget();
        } else if (i2 == this.n.hashCode()) {
            Message obtainMessage2 = r.obtainMessage(2);
            obtainMessage2.obj = obj;
            obtainMessage2.arg1 = i3;
            obtainMessage2.sendToTarget();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        com.baidu.platform.comapi.c.a.a().d();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.p.c == null || "".equals(this.p.c)) {
            com.baidu.platform.comapi.c.a.a().a("pkgname", c.p());
            com.baidu.platform.comapi.c.a.a().a("place_notel_show");
        } else {
            com.baidu.platform.comapi.c.a.a().a("pkgname", c.p());
            com.baidu.platform.comapi.c.a.a().a("place_tel_show");
        }
        super.onResume();
    }
}
