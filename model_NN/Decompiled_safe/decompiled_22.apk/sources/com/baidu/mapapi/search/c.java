package com.baidu.mapapi.search;

import android.os.Bundle;
import com.baidu.mapapi.cloud.CustomPoiInfo;
import com.baidu.mapapi.cloud.DetailResult;
import com.baidu.mapapi.cloud.GeoSearchResult;
import com.baidu.mapapi.utils.d;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.basestruct.a;
import com.baidu.platform.comjni.tools.JNITools;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;

public class c {
    private static Bundle a = new Bundle();

    public static String a(ArrayList<MKTransitRoutePlan> arrayList) {
        if (arrayList == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("result_type", 14);
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < arrayList.size(); i++) {
                MKTransitRoutePlan mKTransitRoutePlan = arrayList.get(i);
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put(Constants.SINA_UID, "");
                JSONObject jSONObject3 = new JSONObject();
                if (mKTransitRoutePlan.getStart() != null) {
                    GeoPoint b = d.b(mKTransitRoutePlan.getStart());
                    jSONObject3.put("x", b.getLongitudeE6());
                    jSONObject3.put("y", b.getLatitudeE6());
                    jSONObject2.put("geopt", jSONObject3);
                }
                jSONObject.put("start_point", jSONObject2);
                JSONObject jSONObject4 = new JSONObject();
                jSONObject4.put(Constants.SINA_UID, "");
                JSONObject jSONObject5 = new JSONObject();
                if (mKTransitRoutePlan.getEnd() != null) {
                    GeoPoint b2 = d.b(mKTransitRoutePlan.getEnd());
                    jSONObject5.put("x", b2.getLongitudeE6());
                    jSONObject5.put("y", b2.getLatitudeE6());
                    jSONObject4.put("geopt", jSONObject5);
                }
                jSONObject.put("end_point", jSONObject4);
                JSONArray jSONArray2 = new JSONArray();
                int numLines = mKTransitRoutePlan.getNumLines();
                for (int i2 = 0; i2 < numLines; i2++) {
                    JSONObject jSONObject6 = new JSONObject();
                    JSONArray jSONArray3 = new JSONArray();
                    MKLine line = mKTransitRoutePlan.getLine(i2);
                    JSONObject jSONObject7 = new JSONObject();
                    jSONObject7.put("type", 3);
                    JSONObject jSONObject8 = new JSONObject();
                    jSONObject8.put("type", line.getType());
                    jSONObject8.put("start_uid", line.getGetOnStop().uid);
                    jSONObject8.put("end_uid", line.getGetOffStop().uid);
                    jSONObject7.put("vehicle", jSONObject8);
                    jSONObject7.put("instructions", line.getTip());
                    JSONObject jSONObject9 = new JSONObject();
                    if (line.getGetOnStop().pt != null) {
                        GeoPoint b3 = d.b(line.getGetOnStop().pt);
                        jSONObject9.put("x", b3.getLongitudeE6());
                        jSONObject9.put("y", b3.getLatitudeE6());
                        jSONObject7.put("start_location_pt", jSONObject9);
                    }
                    JSONObject jSONObject10 = new JSONObject();
                    if (line.getGetOffStop().pt != null) {
                        GeoPoint b4 = d.b(line.getGetOffStop().pt);
                        jSONObject10.put("x", b4.getLongitudeE6());
                        jSONObject10.put("y", b4.getLatitudeE6());
                        jSONObject7.put("end_location_pt", jSONObject10);
                    }
                    ArrayList<GeoPoint> arrayList2 = line.a;
                    JSONArray jSONArray4 = new JSONArray();
                    for (int i3 = 0; i3 < arrayList2.size(); i3++) {
                        JSONObject jSONObject11 = new JSONObject();
                        GeoPoint geoPoint = arrayList2.get(i3);
                        if (geoPoint != null) {
                            jSONObject11.put("x", geoPoint.getLongitudeE6());
                            jSONObject11.put("y", geoPoint.getLatitudeE6());
                            jSONArray4.put(jSONObject11);
                        }
                    }
                    jSONObject7.put("distance", line.getDistance());
                    jSONObject7.put("path_geo_pt", jSONArray4);
                    jSONArray3.put(jSONObject7);
                    jSONObject6.put("busline", jSONArray3);
                    jSONArray2.put(jSONObject6);
                }
                int numRoute = mKTransitRoutePlan.getNumRoute();
                for (int i4 = 0; i4 < numRoute; i4++) {
                    JSONObject jSONObject12 = new JSONObject();
                    JSONArray jSONArray5 = new JSONArray();
                    MKRoute route = mKTransitRoutePlan.getRoute(i4);
                    JSONObject jSONObject13 = new JSONObject();
                    jSONObject13.put("type", 5);
                    new JSONObject();
                    jSONObject13.put("instructions", route.getTip());
                    GeoPoint b5 = d.b(route.getStart());
                    JSONObject jSONObject14 = new JSONObject();
                    jSONObject14.put("x", b5.getLongitudeE6());
                    jSONObject14.put("y", b5.getLatitudeE6());
                    jSONObject13.put("end_location", jSONObject14);
                    ArrayList<ArrayList<GeoPoint>> arrayList3 = route.a;
                    JSONArray jSONArray6 = new JSONArray();
                    for (int i5 = 0; i5 < arrayList3.size(); i5++) {
                        for (int i6 = 0; i6 < arrayList3.get(i5).size(); i6++) {
                            JSONObject jSONObject15 = new JSONObject();
                            GeoPoint geoPoint2 = (GeoPoint) arrayList3.get(i5).get(i6);
                            if (geoPoint2 != null) {
                                jSONObject15.put("x", geoPoint2.getLongitudeE6());
                                jSONObject15.put("y", geoPoint2.getLatitudeE6());
                                jSONArray6.put(jSONObject15);
                            }
                        }
                    }
                    jSONObject13.put("distance", route.getDistance());
                    jSONObject13.put("path_geo_pt", jSONArray6);
                    jSONArray5.put(jSONObject13);
                    jSONObject12.put("busline", jSONArray5);
                    jSONArray2.put(jSONObject12);
                }
                JSONObject jSONObject16 = new JSONObject();
                jSONObject16.put("steps", jSONArray2);
                jSONArray.put(jSONObject16);
            }
            JSONArray jSONArray7 = new JSONArray();
            JSONObject jSONObject17 = new JSONObject();
            jSONObject17.put("legs", jSONArray);
            jSONArray7.put(jSONObject17);
            jSONObject.put("routes", jSONArray7);
        } catch (JSONException e) {
        }
        return jSONObject.toString();
    }

    public static ArrayList<MKCityListInfo> a(JSONObject jSONObject, String str) {
        JSONArray optJSONArray;
        if (jSONObject == null || str == null || str.equals("") || (optJSONArray = jSONObject.optJSONArray(str)) == null || optJSONArray.length() <= 0) {
            return null;
        }
        ArrayList<MKCityListInfo> arrayList = new ArrayList<>();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < optJSONArray.length()) {
                JSONObject jSONObject2 = (JSONObject) optJSONArray.opt(i2);
                MKCityListInfo mKCityListInfo = new MKCityListInfo();
                mKCityListInfo.num = jSONObject2.optInt("num");
                mKCityListInfo.city = jSONObject2.optString(Constants.SINA_NAME);
                arrayList.add(mKCityListInfo);
                i = i2 + 1;
            } else {
                arrayList.trimToSize();
                return arrayList;
            }
        }
    }

    private static ArrayList<MKPoiInfo> a(JSONObject jSONObject, String str, String str2) {
        JSONArray optJSONArray;
        if (jSONObject == null || str == null || "".equals(str) || (optJSONArray = jSONObject.optJSONArray(str)) == null) {
            return null;
        }
        ArrayList<MKPoiInfo> arrayList = new ArrayList<>();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= optJSONArray.length()) {
                return arrayList;
            }
            JSONObject jSONObject2 = (JSONObject) optJSONArray.opt(i2);
            MKPoiInfo mKPoiInfo = new MKPoiInfo();
            mKPoiInfo.address = jSONObject2.optString("addr");
            mKPoiInfo.uid = jSONObject2.optString(Constants.SINA_UID);
            mKPoiInfo.name = jSONObject2.optString(Constants.SINA_NAME);
            mKPoiInfo.pt = e(jSONObject2, "geo");
            mKPoiInfo.city = str2;
            arrayList.add(mKPoiInfo);
            i = i2 + 1;
        }
    }

    static void a(String str, ArrayList<ArrayList<GeoPoint>> arrayList, ArrayList<ArrayList<GeoPoint>> arrayList2) {
        a a2 = com.baidu.platform.comjni.tools.a.a(str);
        if (a2 != null && a2.d != null) {
            ArrayList<ArrayList<com.baidu.platform.comapi.basestruct.c>> arrayList3 = a2.d;
            for (int i = 0; i < arrayList3.size(); i++) {
                ArrayList arrayList4 = arrayList3.get(i);
                ArrayList arrayList5 = new ArrayList(arrayList4.size());
                arrayList.add(arrayList5);
                ArrayList arrayList6 = new ArrayList(arrayList4.size());
                arrayList2.add(arrayList6);
                for (int i2 = 0; i2 < arrayList4.size(); i2++) {
                    com.baidu.platform.comapi.basestruct.c cVar = (com.baidu.platform.comapi.basestruct.c) arrayList4.get(i2);
                    arrayList6.add(new GeoPoint(cVar.b / 100, cVar.a / 100));
                    arrayList5.add(d.a(new GeoPoint(cVar.b / 100, cVar.a / 100)));
                }
                arrayList5.trimToSize();
                arrayList6.trimToSize();
            }
            arrayList.trimToSize();
            arrayList2.trimToSize();
        }
    }

    public static boolean a(String str, DetailResult detailResult) {
        if (!(str == null || detailResult == null)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                detailResult.status = jSONObject.optInt("status");
                detailResult.message = jSONObject.optString("message");
                JSONObject optJSONObject = jSONObject.optJSONObject("content");
                if (optJSONObject != null) {
                    CustomPoiInfo customPoiInfo = new CustomPoiInfo();
                    detailResult.content = customPoiInfo;
                    customPoiInfo.uid = optJSONObject.optInt(Constants.SINA_UID);
                    customPoiInfo.name = optJSONObject.optString(Constants.SINA_NAME);
                    customPoiInfo.address = optJSONObject.optString("addr");
                    customPoiInfo.telephone = optJSONObject.optString("tel");
                    customPoiInfo.postCode = optJSONObject.optString("zip");
                    customPoiInfo.provinceId = optJSONObject.optInt("province_id");
                    customPoiInfo.cityId = optJSONObject.optInt("city_id");
                    customPoiInfo.districtId = optJSONObject.optInt("district_id");
                    customPoiInfo.provinceName = optJSONObject.optString("province");
                    customPoiInfo.cityName = optJSONObject.optString("city");
                    customPoiInfo.districtName = optJSONObject.optString("district");
                    customPoiInfo.latitude = (float) optJSONObject.optDouble(Constants.TX_API_LATITUDE);
                    customPoiInfo.longitude = (float) optJSONObject.optDouble(Constants.TX_API_LONGITUDE);
                    customPoiInfo.databoxId = optJSONObject.optInt("databox_id");
                    customPoiInfo.tag = optJSONObject.optString("tag");
                    JSONObject optJSONObject2 = optJSONObject.optJSONObject("ext");
                    if (optJSONObject2 != null) {
                        for (int i = 0; i < optJSONObject2.length(); i++) {
                            customPoiInfo.poiExtend = new HashMap();
                            Iterator<String> keys = optJSONObject2.keys();
                            while (keys.hasNext()) {
                                String next = keys.next();
                                customPoiInfo.poiExtend.put(next, optJSONObject2.opt(next));
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static boolean a(String str, GeoSearchResult geoSearchResult) {
        if (str == null || geoSearchResult == null) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            geoSearchResult.status = jSONObject.optInt("status");
            geoSearchResult.total = jSONObject.optInt("total");
            geoSearchResult.message = jSONObject.optString("message");
            geoSearchResult.size = jSONObject.optInt("size");
            geoSearchResult.poiList = new ArrayList();
            JSONArray optJSONArray = jSONObject.optJSONArray("content");
            if (optJSONArray != null && optJSONArray.length() > 0) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject jSONObject2 = (JSONObject) optJSONArray.opt(i);
                    CustomPoiInfo customPoiInfo = new CustomPoiInfo();
                    geoSearchResult.poiList.add(customPoiInfo);
                    customPoiInfo.uid = jSONObject2.optInt(Constants.SINA_UID);
                    customPoiInfo.name = jSONObject2.optString(Constants.SINA_NAME);
                    customPoiInfo.address = jSONObject2.optString("addr");
                    customPoiInfo.telephone = jSONObject2.optString("tel");
                    customPoiInfo.postCode = jSONObject2.optString("zip");
                    customPoiInfo.provinceId = jSONObject2.optInt("province_id");
                    customPoiInfo.cityId = jSONObject2.optInt("city_id");
                    customPoiInfo.districtId = jSONObject2.optInt("district_id");
                    customPoiInfo.provinceName = jSONObject2.optString("province");
                    customPoiInfo.cityName = jSONObject2.optString("city");
                    customPoiInfo.districtName = jSONObject2.optString("district");
                    customPoiInfo.latitude = (float) jSONObject2.optDouble(Constants.TX_API_LATITUDE);
                    customPoiInfo.longitude = (float) jSONObject2.optDouble(Constants.TX_API_LONGITUDE);
                    customPoiInfo.databoxId = jSONObject2.optInt("databox_id");
                    customPoiInfo.tag = jSONObject2.optString("tag");
                    JSONObject optJSONObject = jSONObject2.optJSONObject("ext");
                    if (optJSONObject != null) {
                        for (int i2 = 0; i2 < optJSONObject.length(); i2++) {
                            customPoiInfo.poiExtend = new HashMap();
                            Iterator<String> keys = optJSONObject.keys();
                            while (keys.hasNext()) {
                                String next = keys.next();
                                customPoiInfo.poiExtend.put(next, optJSONObject.opt(next));
                            }
                        }
                    }
                }
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(String str, MKAddrInfo mKAddrInfo) {
        if (str == null || "".equals(str) || mKAddrInfo == null) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            mKAddrInfo.strAddr = jSONObject.optString("address");
            mKAddrInfo.strBusiness = jSONObject.optString("business");
            mKAddrInfo.addressComponents = c(jSONObject, "addr_detail");
            mKAddrInfo.type = 1;
            mKAddrInfo.geoPt = f(jSONObject, "point");
            mKAddrInfo.poiList = b(jSONObject, "surround_poi");
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(String str, MKBusLineResult mKBusLineResult) {
        if (str != null) {
            try {
                if (!str.equals("") && mKBusLineResult != null) {
                    JSONObject jSONObject = new JSONObject(str);
                    int optInt = jSONObject.optInt("count");
                    JSONArray optJSONArray = jSONObject.optJSONArray("details");
                    if (optJSONArray == null || optInt <= 0) {
                        return true;
                    }
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                        mKBusLineResult.a(optJSONObject.optString("starttime"));
                        mKBusLineResult.b(optJSONObject.optString("endtime"));
                        mKBusLineResult.a(null, optJSONObject.optString(Constants.SINA_NAME), optJSONObject.optBoolean("ismonticket") ? 1 : 0);
                        mKBusLineResult.getBusRoute().b(new ArrayList());
                        mKBusLineResult.getBusRoute().a = new ArrayList<>();
                        a(optJSONObject.optString("geo"), mKBusLineResult.getBusRoute().getArrayPoints(), mKBusLineResult.getBusRoute().a);
                        mKBusLineResult.getBusRoute().c(3);
                        JSONArray optJSONArray2 = optJSONObject.optJSONArray("stations");
                        if (optJSONArray2 != null) {
                            MKRoute busRoute = mKBusLineResult.getBusRoute();
                            ArrayList arrayList = new ArrayList();
                            busRoute.a(arrayList);
                            for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                                JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                                if (optJSONObject2 != null) {
                                    MKStep mKStep = new MKStep();
                                    mKStep.a(optJSONObject2.optString(Constants.SINA_NAME));
                                    mKStep.a(e(optJSONObject2, "geo"));
                                    arrayList.add(mKStep);
                                    if (i2 == 0) {
                                        busRoute.a(new GeoPoint(mKStep.getPoint().getLatitudeE6(), mKStep.getPoint().getLongitudeE6()));
                                    } else if (i2 == optJSONArray2.length() - 1) {
                                        busRoute.b(new GeoPoint(mKStep.getPoint().getLatitudeE6(), mKStep.getPoint().getLongitudeE6()));
                                    }
                                }
                            }
                        }
                    }
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public static boolean a(String str, MKDrivingRouteResult mKDrivingRouteResult) throws JSONException {
        JSONObject optJSONObject;
        if (str == null || "".equals(str) || mKDrivingRouteResult == null) {
            return false;
        }
        JSONObject jSONObject = new JSONObject(str);
        mKDrivingRouteResult.a(d(jSONObject, "start_point"));
        mKDrivingRouteResult.b(d(jSONObject, "end_point"));
        GeoPoint geoPoint = mKDrivingRouteResult.getStart() != null ? mKDrivingRouteResult.getStart().pt : null;
        GeoPoint geoPoint2 = mKDrivingRouteResult.getEnd() != null ? mKDrivingRouteResult.getEnd().pt : null;
        String str2 = "驾车方案：" + jSONObject.optJSONObject("start_point").optString(Constants.SINA_NAME) + "_" + jSONObject.optJSONObject("end_point").optString(Constants.SINA_NAME);
        JSONObject optJSONObject2 = jSONObject.optJSONObject("routes");
        if (!(optJSONObject2 == null || (optJSONObject = optJSONObject2.optJSONObject("legs")) == null)) {
            ArrayList arrayList = new ArrayList();
            mKDrivingRouteResult.a(arrayList);
            MKRoutePlan mKRoutePlan = new MKRoutePlan();
            arrayList.add(mKRoutePlan);
            int optInt = optJSONObject.optInt("distance");
            mKRoutePlan.a(optInt);
            ArrayList arrayList2 = new ArrayList();
            mKRoutePlan.a(arrayList2);
            MKRoute mKRoute = new MKRoute();
            arrayList2.add(mKRoute);
            mKRoute.a(str2);
            mKRoute.b(0);
            mKRoute.a(optInt);
            mKRoute.b(new ArrayList());
            mKRoute.a = new ArrayList<>();
            if (geoPoint != null) {
                mKRoute.a(new GeoPoint(geoPoint.getLatitudeE6(), geoPoint.getLongitudeE6()));
            }
            if (geoPoint2 != null) {
                mKRoute.b(new GeoPoint(geoPoint2.getLatitudeE6(), geoPoint2.getLongitudeE6()));
            }
            mKRoute.c(1);
            JSONArray optJSONArray = optJSONObject.optJSONArray("steps");
            if (optJSONArray != null) {
                ArrayList arrayList3 = new ArrayList();
                mKRoute.a(arrayList3);
                JSONObject optJSONObject3 = optJSONArray.optJSONObject(0);
                MKStep mKStep = new MKStep();
                mKStep.a(optJSONObject3.optString("start_desc"));
                mKStep.a(optJSONObject3.optInt("direction"));
                mKStep.b(optJSONObject3.optString("start_loc"));
                mKStep.a(e(optJSONObject3, "start_loc"));
                arrayList3.add(mKStep);
                for (int i = 0; i < optJSONArray.length(); i++) {
                    MKStep mKStep2 = new MKStep();
                    JSONObject optJSONObject4 = optJSONArray.optJSONObject(i);
                    if (optJSONObject4 != null) {
                        mKStep2.a(optJSONObject4.optString("end_desc"));
                        mKStep2.a(optJSONObject4.optInt("direction"));
                        System.currentTimeMillis();
                        mKStep2.a(e(optJSONObject4, "end_loc"));
                        arrayList3.add(mKStep2);
                        mKStep2.b(optJSONObject4.optString("path"));
                    }
                }
            }
        }
        mKDrivingRouteResult.a((MKRouteAddrResult) null);
        return true;
    }

    public static boolean a(String str, MKPoiResult mKPoiResult) {
        JSONObject jSONObject;
        if (str == null || "".equals(str)) {
            return false;
        }
        try {
            jSONObject = new JSONObject(str);
        } catch (JSONException e) {
            e.printStackTrace();
            jSONObject = null;
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("citys");
        if (optJSONArray != null && optJSONArray.length() > 0) {
            ArrayList arrayList = new ArrayList();
            if (mKPoiResult != null) {
                mKPoiResult.c(arrayList);
            }
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject jSONObject2 = (JSONObject) optJSONArray.opt(i);
                MKCityListInfo mKCityListInfo = new MKCityListInfo();
                mKCityListInfo.num = jSONObject2.optInt("num");
                mKCityListInfo.city = jSONObject2.optString(Constants.SINA_NAME);
                arrayList.add(mKCityListInfo);
            }
            arrayList.trimToSize();
        }
        return true;
    }

    public static boolean a(String str, MKPoiResult mKPoiResult, int i) {
        if (str != null) {
            try {
                if (!str.equals("") && mKPoiResult != null) {
                    JSONObject jSONObject = new JSONObject(str);
                    int optInt = jSONObject.optInt("total");
                    int optInt2 = jSONObject.optInt("count");
                    mKPoiResult.b(optInt);
                    mKPoiResult.a(optInt2);
                    mKPoiResult.d(i);
                    mKPoiResult.c((optInt % optInt2 > 0 ? 1 : 0) + (optInt / optInt2));
                    JSONObject optJSONObject = jSONObject.optJSONObject("current_city");
                    String optString = optJSONObject != null ? optJSONObject.optString(Constants.SINA_NAME) : null;
                    JSONArray optJSONArray = jSONObject.optJSONArray("pois");
                    if (optJSONArray == null) {
                        return true;
                    }
                    ArrayList arrayList = new ArrayList();
                    mKPoiResult.a(arrayList);
                    for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                        JSONObject optJSONObject2 = optJSONArray.optJSONObject(i2);
                        MKPoiInfo mKPoiInfo = new MKPoiInfo();
                        mKPoiInfo.name = optJSONObject2.optString(Constants.SINA_NAME);
                        mKPoiInfo.address = optJSONObject2.optString("addr");
                        mKPoiInfo.uid = optJSONObject2.optString(Constants.SINA_UID);
                        mKPoiInfo.phoneNum = optJSONObject2.optString("tel");
                        mKPoiInfo.ePoiType = optJSONObject2.optInt("type");
                        if (!(mKPoiInfo.ePoiType == 2 || mKPoiInfo.ePoiType == 4)) {
                            mKPoiInfo.pt = e(optJSONObject2, "geo");
                        }
                        mKPoiInfo.city = optString;
                        JSONObject optJSONObject3 = optJSONObject2.optJSONObject("place");
                        if (optJSONObject3 != null && "cater".equals(optJSONObject3.optString("src_name")) && optJSONObject2.optBoolean("detail")) {
                            mKPoiInfo.hasCaterDetails = true;
                        }
                        arrayList.add(mKPoiInfo);
                    }
                    return true;
                }
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    public static boolean a(String str, MKRouteAddrResult mKRouteAddrResult) {
        if (str == null || "".equals(str) || mKRouteAddrResult == null) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            JSONObject optJSONObject = jSONObject.optJSONObject("address_info");
            if (optJSONObject == null) {
                return false;
            }
            String optString = optJSONObject.optString("st_cityname");
            String optString2 = optJSONObject.optString("en_cityname");
            if (optJSONObject.optBoolean("have_stcitylist")) {
                mKRouteAddrResult.mStartCityList = a(jSONObject, "startcitys");
            } else {
                mKRouteAddrResult.mStartPoiList = a(jSONObject, "startpoints", optString);
            }
            if (optJSONObject.optBoolean("have_encitylist")) {
                mKRouteAddrResult.mEndCityList = a(jSONObject, "endcitys");
            } else {
                mKRouteAddrResult.mEndPoiList = a(jSONObject, "endpoints", optString2);
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("way_points_citylist");
            if (optJSONArray == null || optJSONArray.length() <= 0) {
                return true;
            }
            mKRouteAddrResult.mWpCityList = new ArrayList<>();
            mKRouteAddrResult.mWpPoiList = new ArrayList<>();
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                if (jSONObject2.optBoolean("have_citylist")) {
                    mKRouteAddrResult.mWpCityList.add(a(jSONObject2, "way_points_item"));
                } else {
                    mKRouteAddrResult.mWpCityList.add(null);
                }
                if (jSONObject2.optBoolean("have_poilist")) {
                    mKRouteAddrResult.mWpPoiList.add(a(jSONObject2, "way_points_poilist", ""));
                } else {
                    mKRouteAddrResult.mWpPoiList.add(null);
                }
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(String str, MKSuggestionResult mKSuggestionResult) {
        if (str == null) {
            return false;
        }
        try {
            if (str.equals("") || mKSuggestionResult == null) {
                return false;
            }
            JSONObject jSONObject = new JSONObject(str);
            JSONArray optJSONArray = jSONObject.optJSONArray("cityname");
            JSONArray optJSONArray2 = jSONObject.optJSONArray("poiname");
            JSONArray optJSONArray3 = jSONObject.optJSONArray("districtname");
            if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                ArrayList arrayList = new ArrayList();
                mKSuggestionResult.a(arrayList);
                int length = optJSONArray2.length();
                for (int i = 0; i < length; i++) {
                    MKSuggestionInfo mKSuggestionInfo = new MKSuggestionInfo();
                    mKSuggestionInfo.city = optJSONArray.optString(i);
                    mKSuggestionInfo.key = optJSONArray2.optString(i);
                    mKSuggestionInfo.district = optJSONArray3.optString(i);
                    arrayList.add(mKSuggestionInfo);
                }
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(String str, MKTransitRouteResult mKTransitRouteResult) throws JSONException {
        if (str == null || "".equals(str) || mKTransitRouteResult == null) {
            return false;
        }
        JSONObject jSONObject = new JSONObject(str);
        mKTransitRouteResult.a(d(jSONObject, "start_point"));
        mKTransitRouteResult.b(d(jSONObject, "end_point"));
        JSONArray optJSONArray = jSONObject.optJSONArray("routes");
        if (optJSONArray != null && optJSONArray.length() > 0) {
            ArrayList arrayList = new ArrayList();
            mKTransitRouteResult.a(arrayList);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= optJSONArray.length()) {
                    break;
                }
                JSONObject optJSONObject = ((JSONObject) optJSONArray.opt(i2)).optJSONObject("legs");
                if (optJSONObject != null) {
                    MKTransitRoutePlan mKTransitRoutePlan = new MKTransitRoutePlan();
                    arrayList.add(mKTransitRoutePlan);
                    mKTransitRoutePlan.a(optJSONObject.optInt("distance"));
                    mKTransitRoutePlan.a(e(optJSONObject, "start_geo"));
                    mKTransitRoutePlan.b(e(optJSONObject, "end_geo"));
                    JSONArray optJSONArray2 = optJSONObject.optJSONArray("steps");
                    if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                        ArrayList arrayList2 = new ArrayList();
                        ArrayList arrayList3 = new ArrayList();
                        mKTransitRoutePlan.a(arrayList2);
                        mKTransitRoutePlan.setLine(arrayList3);
                        String str2 = "";
                        for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                            JSONArray optJSONArray3 = optJSONArray2.optJSONObject(i3).optJSONArray("busline");
                            if (optJSONArray3 != null && optJSONArray3.length() > 0) {
                                String str3 = null;
                                JSONObject optJSONObject2 = optJSONArray3.optJSONObject(0);
                                if (optJSONObject2 != null) {
                                    int i4 = optJSONObject2.optInt("type") == 5 ? 2 : 3;
                                    GeoPoint e = e(optJSONObject2, "start_location");
                                    GeoPoint e2 = e(optJSONObject2, "end_location");
                                    if (0 == 0) {
                                        str3 = optJSONObject2.optString("instructions");
                                    }
                                    if (i4 == 2) {
                                        MKRoute mKRoute = new MKRoute();
                                        mKRoute.c(i4);
                                        mKRoute.a(optJSONObject2.optInt("distance"));
                                        mKRoute.a(e);
                                        mKRoute.b(e2);
                                        mKRoute.b(arrayList3.size() - 1);
                                        mKRoute.a(str3);
                                        mKRoute.b(new ArrayList());
                                        mKRoute.a = new ArrayList<>();
                                        a(optJSONObject2.optString("path_geo"), mKRoute.getArrayPoints(), mKRoute.a);
                                        arrayList2.add(mKRoute);
                                    } else {
                                        MKLine mKLine = new MKLine();
                                        mKLine.b(optJSONObject2.optInt("distance"));
                                        MKPoiInfo mKPoiInfo = new MKPoiInfo();
                                        mKPoiInfo.pt = e;
                                        mKLine.a(mKPoiInfo);
                                        MKPoiInfo mKPoiInfo2 = new MKPoiInfo();
                                        mKPoiInfo2.pt = e2;
                                        mKLine.b(mKPoiInfo2);
                                        mKLine.a(new ArrayList());
                                        mKLine.a = new ArrayList<>();
                                        b(optJSONObject2.optString("path_geo"), mKLine.getPoints(), mKLine.a);
                                        JSONObject optJSONObject3 = optJSONObject2.optJSONObject("vehicle");
                                        if (optJSONObject3 != null) {
                                            mKLine.c(optJSONObject3.optInt("type"));
                                            mKPoiInfo.name = optJSONObject3.optString("start_name");
                                            mKPoiInfo.uid = optJSONObject3.optString("start_uid");
                                            mKPoiInfo2.name = optJSONObject3.optString("end_name");
                                            mKPoiInfo2.uid = optJSONObject3.optString("end_uid");
                                            mKLine.a(optJSONObject3.optInt("stop_num"));
                                            mKLine.c(optJSONObject3.optInt("type"));
                                            mKLine.b(optJSONObject3.optString(Constants.SINA_NAME));
                                            if (!str2.equals("")) {
                                                str2 = str2 + "_";
                                            }
                                            str2 = str2 + mKLine.getTitle();
                                        }
                                        mKLine.a(str3);
                                        arrayList3.add(mKLine);
                                    }
                                }
                            }
                        }
                        mKTransitRoutePlan.a(str2);
                    }
                }
                i = i2 + 1;
            }
        }
        mKTransitRouteResult.a((MKRouteAddrResult) null);
        return true;
    }

    public static boolean a(String str, MKWalkingRouteResult mKWalkingRouteResult) throws JSONException {
        JSONObject optJSONObject;
        if (str == null || "".equals(str) || mKWalkingRouteResult == null) {
            return false;
        }
        JSONObject jSONObject = new JSONObject(str);
        mKWalkingRouteResult.a(d(jSONObject, "start_point"));
        mKWalkingRouteResult.b(d(jSONObject, "end_point"));
        GeoPoint geoPoint = mKWalkingRouteResult.getStart() != null ? mKWalkingRouteResult.getStart().pt : null;
        GeoPoint geoPoint2 = mKWalkingRouteResult.getEnd() != null ? mKWalkingRouteResult.getEnd().pt : null;
        String str2 = "步行方案：" + jSONObject.optJSONObject("start_point").optString(Constants.SINA_NAME) + "_" + jSONObject.optJSONObject("end_point").optString(Constants.SINA_NAME);
        JSONObject optJSONObject2 = jSONObject.optJSONObject("routes");
        if (!(optJSONObject2 == null || (optJSONObject = optJSONObject2.optJSONObject("legs")) == null)) {
            ArrayList arrayList = new ArrayList();
            mKWalkingRouteResult.a(arrayList);
            MKRoutePlan mKRoutePlan = new MKRoutePlan();
            arrayList.add(mKRoutePlan);
            int optInt = optJSONObject.optInt("distance");
            mKRoutePlan.a(optInt);
            ArrayList arrayList2 = new ArrayList();
            mKRoutePlan.a(arrayList2);
            MKRoute mKRoute = new MKRoute();
            arrayList2.add(mKRoute);
            mKRoute.a(str2);
            mKRoute.b(0);
            mKRoute.a(optInt);
            if (geoPoint != null) {
                mKRoute.a(new GeoPoint(geoPoint.getLatitudeE6(), geoPoint.getLongitudeE6()));
            }
            if (geoPoint2 != null) {
                mKRoute.b(new GeoPoint(geoPoint2.getLatitudeE6(), geoPoint2.getLongitudeE6()));
            }
            mKRoute.c(2);
            mKRoute.b(new ArrayList());
            mKRoute.a = new ArrayList<>();
            JSONArray optJSONArray = optJSONObject.optJSONArray("steps");
            if (optJSONArray != null) {
                ArrayList arrayList3 = new ArrayList();
                mKRoute.a(arrayList3);
                JSONObject optJSONObject3 = optJSONArray.optJSONObject(0);
                MKStep mKStep = new MKStep();
                mKStep.a(optJSONObject3.optString("start_desc"));
                mKStep.a(optJSONObject3.optInt("direction"));
                mKStep.a(e(optJSONObject3, "start_loc"));
                arrayList3.add(mKStep);
                for (int i = 0; i < optJSONArray.length(); i++) {
                    MKStep mKStep2 = new MKStep();
                    JSONObject optJSONObject4 = optJSONArray.optJSONObject(i);
                    if (optJSONObject4 != null) {
                        mKStep2.a(optJSONObject4.optString("end_desc"));
                        mKStep2.a(optJSONObject4.optInt("direction"));
                        mKStep2.a(e(optJSONObject4, "end_loc"));
                        arrayList3.add(mKStep2);
                        a(optJSONObject4.optString("path"), mKRoute.getArrayPoints(), mKRoute.a);
                    }
                }
            }
        }
        mKWalkingRouteResult.a((MKRouteAddrResult) null);
        return true;
    }

    public static boolean a(String str, e eVar) {
        JSONArray optJSONArray;
        if (str == null || eVar == null) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject != null) {
                JSONObject optJSONObject = jSONObject.optJSONObject("content");
                if (optJSONObject == null) {
                    return false;
                }
                eVar.d = optJSONObject.optString(Constants.SINA_UID);
                eVar.b = optJSONObject.optString("addr");
                eVar.a = optJSONObject.optString(Constants.SINA_NAME);
                JSONObject optJSONObject2 = optJSONObject.optJSONObject("ext");
                if (optJSONObject2 != null) {
                    JSONObject optJSONObject3 = optJSONObject2.optJSONObject("detail_info");
                    if (optJSONObject3 != null) {
                        eVar.i = optJSONObject3.optString("environment_rating");
                        eVar.e = optJSONObject3.optString("image");
                        JSONArray optJSONArray2 = optJSONObject3.optJSONArray("link");
                        if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                            eVar.o = new ArrayList();
                            for (int i = 0; i < optJSONArray2.length(); i++) {
                                JSONObject jSONObject2 = (JSONObject) optJSONArray2.opt(i);
                                if (!"dianping".equals(jSONObject2.optString(Constants.SINA_NAME))) {
                                    d dVar = new d();
                                    dVar.b = jSONObject2.optString("cn_name");
                                    dVar.c = jSONObject2.optString(com.tencent.tauth.Constants.PARAM_URL);
                                    dVar.d = jSONObject2.optString(Constants.SINA_NAME);
                                    eVar.o.add(dVar);
                                }
                            }
                        }
                        eVar.f = optJSONObject3.optString("overall_rating");
                        eVar.c = optJSONObject3.optString("phone");
                        eVar.g = optJSONObject3.optString("price");
                        eVar.j = optJSONObject3.optString("service_rating");
                        eVar.h = optJSONObject3.optString("taste_rating");
                    }
                    JSONObject optJSONObject4 = optJSONObject2.optJSONObject("rich_info");
                    if (optJSONObject4 != null) {
                        eVar.k = optJSONObject4.optString(com.tencent.tauth.Constants.PARAM_COMMENT);
                        eVar.l = optJSONObject4.optString("recommendation");
                    }
                    JSONArray optJSONArray3 = optJSONObject2.optJSONArray("review");
                    if (optJSONArray3 != null && optJSONArray3.length() > 0) {
                        int i2 = 0;
                        loop1:
                        while (true) {
                            if (i2 >= optJSONArray3.length()) {
                                break;
                            }
                            JSONObject jSONObject3 = (JSONObject) optJSONArray3.opt(i2);
                            if (!"dianping.com".equals(jSONObject3.optString("from")) && (optJSONArray = jSONObject3.optJSONArray("info")) != null && optJSONArray.length() > 0) {
                                for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                                    JSONObject optJSONObject5 = optJSONArray.optJSONObject(i3);
                                    if (optJSONObject5 != null) {
                                        eVar.m = optJSONObject5.optString("content");
                                        break loop1;
                                    }
                                }
                                continue;
                            }
                            i2++;
                        }
                    }
                }
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public static String b(ArrayList<MKRoute> arrayList) {
        if (arrayList == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("result_type", 20);
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < arrayList.size(); i++) {
                MKRoute mKRoute = arrayList.get(i);
                if (mKRoute.getRouteType() == 3) {
                    jSONObject.put("result_buslinedetail", true);
                }
                JSONObject jSONObject2 = new JSONObject();
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put(Constants.SINA_UID, "");
                JSONObject jSONObject4 = new JSONObject();
                if (mKRoute.getStart() != null) {
                    GeoPoint b = d.b(mKRoute.getStart());
                    jSONObject4.put("x", b.getLongitudeE6());
                    jSONObject4.put("y", b.getLatitudeE6());
                    jSONObject3.put("geopt", jSONObject4);
                }
                jSONObject2.put("start_point", jSONObject3);
                JSONObject jSONObject5 = new JSONObject();
                jSONObject5.put(Constants.SINA_UID, "");
                JSONObject jSONObject6 = new JSONObject();
                if (mKRoute.getEnd() != null) {
                    GeoPoint b2 = d.b(mKRoute.getEnd());
                    jSONObject6.put("x", b2.getLongitudeE6());
                    jSONObject6.put("y", b2.getLatitudeE6());
                    jSONObject5.put("geopt", jSONObject6);
                }
                jSONObject2.put("end_point", jSONObject5);
                int numSteps = mKRoute.getNumSteps();
                ArrayList<ArrayList<GeoPoint>> arrayList2 = mKRoute.a;
                JSONArray jSONArray2 = new JSONArray();
                for (int i2 = 0; i2 < numSteps; i2++) {
                    JSONObject jSONObject7 = new JSONObject();
                    MKStep step = mKRoute.getStep(i2);
                    jSONObject7.put("direction", step.a());
                    if (step.getPoint() != null) {
                        GeoPoint b3 = d.b(step.getPoint());
                        JSONObject jSONObject8 = new JSONObject();
                        jSONObject8.put("x", b3.getLongitudeE6());
                        jSONObject8.put("y", b3.getLatitudeE6());
                        jSONObject7.put("end_loc_pt", jSONObject8);
                    }
                    jSONObject7.put("end_desc", step.getContent());
                    if (mKRoute.getRouteType() == 3) {
                        if (arrayList2.size() > i2) {
                            ArrayList arrayList3 = arrayList2.get(i2);
                            JSONArray jSONArray3 = new JSONArray();
                            for (int i3 = 0; i3 < arrayList3.size(); i3++) {
                                JSONObject jSONObject9 = new JSONObject();
                                GeoPoint geoPoint = (GeoPoint) arrayList3.get(i3);
                                if (geoPoint != null) {
                                    jSONObject9.put("x", geoPoint.getLongitudeE6());
                                    jSONObject9.put("y", geoPoint.getLatitudeE6());
                                    jSONArray3.put(jSONObject9);
                                }
                            }
                            jSONObject7.put("pathPt", jSONArray3);
                        }
                    } else if (mKRoute.getRouteType() == 1) {
                        if (i2 < numSteps) {
                            jSONObject7.put("path", step.b());
                        }
                    } else if (i2 < numSteps - 1) {
                        ArrayList arrayList4 = arrayList2.get(i2);
                        JSONArray jSONArray4 = new JSONArray();
                        for (int i4 = 0; i4 < arrayList4.size(); i4++) {
                            JSONObject jSONObject10 = new JSONObject();
                            GeoPoint geoPoint2 = (GeoPoint) arrayList4.get(i4);
                            if (geoPoint2 != null) {
                                jSONObject10.put("x", geoPoint2.getLongitudeE6());
                                jSONObject10.put("y", geoPoint2.getLatitudeE6());
                                jSONArray4.put(jSONObject10);
                            }
                        }
                        jSONObject7.put("pathPt", jSONArray4);
                    }
                    jSONArray2.put(jSONObject7);
                }
                jSONObject2.put("steps", jSONArray2);
                jSONArray.put(jSONObject2);
            }
            JSONObject jSONObject11 = new JSONObject();
            jSONObject11.put("legs", jSONArray);
            jSONObject.put("routes", jSONObject11);
        } catch (JSONException e) {
        }
        return jSONObject.toString();
    }

    private static ArrayList<MKPoiInfo> b(JSONObject jSONObject, String str) {
        JSONArray optJSONArray;
        if (jSONObject == null || str == null || "".equals(str) || (optJSONArray = jSONObject.optJSONArray(str)) == null) {
            return null;
        }
        ArrayList<MKPoiInfo> arrayList = new ArrayList<>();
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            MKPoiInfo mKPoiInfo = new MKPoiInfo();
            mKPoiInfo.address = optJSONObject.optString("addr");
            mKPoiInfo.phoneNum = optJSONObject.optString("tel");
            mKPoiInfo.uid = optJSONObject.optString(Constants.SINA_UID);
            mKPoiInfo.postCode = optJSONObject.optString("zip");
            mKPoiInfo.name = optJSONObject.optString(Constants.SINA_NAME);
            mKPoiInfo.pt = f(optJSONObject, "point");
            arrayList.add(mKPoiInfo);
        }
        return arrayList;
    }

    private static void b(String str, ArrayList<GeoPoint> arrayList, ArrayList<GeoPoint> arrayList2) {
        int i = 0;
        a a2 = com.baidu.platform.comjni.tools.a.a(str);
        if (a2 != null && a2.d != null) {
            ArrayList<ArrayList<com.baidu.platform.comapi.basestruct.c>> arrayList3 = a2.d;
            if (arrayList3.size() > 0) {
                ArrayList arrayList4 = arrayList3.get(0);
                while (true) {
                    int i2 = i;
                    if (i2 >= arrayList4.size()) {
                        break;
                    }
                    com.baidu.platform.comapi.basestruct.c cVar = (com.baidu.platform.comapi.basestruct.c) arrayList4.get(i2);
                    arrayList2.add(new GeoPoint(cVar.b / 100, cVar.a / 100));
                    arrayList.add(d.a(new GeoPoint(cVar.b / 100, cVar.a / 100)));
                    i = i2 + 1;
                }
            }
            arrayList.trimToSize();
            arrayList2.trimToSize();
        }
    }

    public static boolean b(String str, MKAddrInfo mKAddrInfo) {
        if (str == null || "".equals(str) || mKAddrInfo == null) {
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject == null || jSONObject.optInt("error") != 0) {
                return false;
            }
            mKAddrInfo.type = 0;
            mKAddrInfo.geoPt = d.a(new GeoPoint(jSONObject.optInt("y"), jSONObject.optInt("x")));
            mKAddrInfo.strAddr = jSONObject.optString("addr");
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean b(String str, MKPoiResult mKPoiResult, int i) {
        ArrayList arrayList = new ArrayList();
        mKPoiResult.b(arrayList);
        try {
            JSONObject jSONObject = new JSONObject(str);
            JSONArray optJSONArray = jSONObject.optJSONArray("content");
            JSONArray optJSONArray2 = jSONObject.optJSONArray("result");
            if (optJSONArray == null || optJSONArray.length() <= 0 || optJSONArray2 == null || optJSONArray2.length() <= 0) {
                return true;
            }
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                MKPoiResult mKPoiResult2 = new MKPoiResult();
                arrayList.add(mKPoiResult2);
                JSONArray optJSONArray3 = optJSONArray.optJSONObject(i2).optJSONArray("cont");
                if (optJSONArray3 != null && optJSONArray3.length() > 0) {
                    ArrayList arrayList2 = new ArrayList();
                    for (int i3 = 0; i3 < optJSONArray3.length(); i3++) {
                        JSONObject optJSONObject = optJSONArray3.optJSONObject(i3);
                        MKPoiInfo mKPoiInfo = new MKPoiInfo();
                        mKPoiInfo.name = optJSONObject.optString(Constants.SINA_NAME);
                        mKPoiInfo.address = optJSONObject.optString("addr");
                        mKPoiInfo.pt = e(optJSONObject, "geo");
                        mKPoiInfo.uid = optJSONObject.optString(Constants.SINA_UID);
                        mKPoiInfo.phoneNum = optJSONObject.optString("tel");
                        mKPoiInfo.ePoiType = optJSONObject.optInt("type");
                        arrayList2.add(mKPoiInfo);
                    }
                    mKPoiResult2.a(arrayList2);
                }
                JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                int optInt = optJSONObject2.optInt("total");
                mKPoiResult2.b(optInt);
                mKPoiResult2.d(optJSONObject2.optInt("page_num"));
                int optInt2 = optJSONObject2.optInt("count");
                mKPoiResult2.a(optInt2);
                mKPoiResult2.d(i);
                mKPoiResult2.c((optInt % optInt2 > 0 ? 1 : 0) + (optInt / optInt2));
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static MKGeocoderAddressComponent c(JSONObject jSONObject, String str) {
        if (jSONObject == null || str == null || "".equals(str)) {
            return null;
        }
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        MKGeocoderAddressComponent mKGeocoderAddressComponent = new MKGeocoderAddressComponent();
        mKGeocoderAddressComponent.city = optJSONObject.optString("city");
        mKGeocoderAddressComponent.district = optJSONObject.optString("district");
        mKGeocoderAddressComponent.province = optJSONObject.optString("province");
        mKGeocoderAddressComponent.street = optJSONObject.optString("street");
        mKGeocoderAddressComponent.streetNumber = optJSONObject.optString("street_number");
        return mKGeocoderAddressComponent;
    }

    public static String c(ArrayList<MKPoiInfo> arrayList) {
        if (arrayList == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("result_type", 11);
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < arrayList.size(); i++) {
                MKPoiInfo mKPoiInfo = arrayList.get(i);
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put(Constants.SINA_UID, mKPoiInfo.uid);
                jSONObject2.put("type", mKPoiInfo.ePoiType);
                jSONObject2.put(Constants.SINA_NAME, mKPoiInfo.name);
                JSONObject jSONObject3 = new JSONObject();
                if (mKPoiInfo.pt != null) {
                    GeoPoint b = d.b(mKPoiInfo.pt);
                    jSONObject3.put("x", b.getLongitudeE6());
                    jSONObject3.put("y", b.getLatitudeE6());
                    jSONObject2.put("geopt", jSONObject3);
                }
                jSONArray.put(jSONObject2);
            }
            if (arrayList.size() > 0) {
                jSONObject.put("pois", jSONArray);
            }
        } catch (JSONException e) {
        }
        return jSONObject.toString();
    }

    private static MKPlanNode d(JSONObject jSONObject, String str) {
        if (jSONObject == null || str == null || "".equals(str)) {
            return null;
        }
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        MKPlanNode mKPlanNode = new MKPlanNode();
        mKPlanNode.name = optJSONObject.optString(Constants.SINA_NAME);
        mKPlanNode.pt = e(optJSONObject, "geo");
        return mKPlanNode;
    }

    private static GeoPoint e(JSONObject jSONObject, String str) {
        if (jSONObject == null || jSONObject == null || str == null) {
            return null;
        }
        String optString = jSONObject.optString(str);
        a.clear();
        a.putString("strkey", optString);
        JNITools.TransGeoStr2Pt(a);
        GeoPoint geoPoint = new GeoPoint(0, 0);
        geoPoint.setLongitudeE6(a.getInt("ptx"));
        geoPoint.setLatitudeE6(a.getInt("pty"));
        return d.a(geoPoint);
    }

    private static GeoPoint f(JSONObject jSONObject, String str) {
        JSONObject optJSONObject;
        if (jSONObject == null || str == null || "".equals(str) || (optJSONObject = jSONObject.optJSONObject(str)) == null) {
            return null;
        }
        return d.a(new GeoPoint(optJSONObject.optInt("y"), optJSONObject.optInt("x")));
    }
}
