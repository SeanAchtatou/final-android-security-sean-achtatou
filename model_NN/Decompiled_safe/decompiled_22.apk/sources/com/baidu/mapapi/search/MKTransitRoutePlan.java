package com.baidu.mapapi.search;

import com.baidu.platform.comapi.basestruct.GeoPoint;
import java.util.ArrayList;

public class MKTransitRoutePlan {
    private int a;
    private String b;
    private ArrayList<MKRoute> c;
    private ArrayList<MKLine> d;
    private GeoPoint e;
    private GeoPoint f;

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.a = i;
    }

    /* access modifiers changed from: package-private */
    public void a(GeoPoint geoPoint) {
        this.e = geoPoint;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList<MKRoute> arrayList) {
        this.c = arrayList;
    }

    /* access modifiers changed from: package-private */
    public void b(GeoPoint geoPoint) {
        this.f = geoPoint;
    }

    public String getContent() {
        return this.b;
    }

    public int getDistance() {
        return this.a;
    }

    public GeoPoint getEnd() {
        return this.f;
    }

    public MKLine getLine(int i) {
        if (this.d == null || i < 0 || i > this.d.size() - 1) {
            return null;
        }
        return this.d.get(i);
    }

    public int getNumLines() {
        if (this.d != null) {
            return this.d.size();
        }
        return 0;
    }

    public int getNumRoute() {
        if (this.c != null) {
            return this.c.size();
        }
        return 0;
    }

    public MKRoute getRoute(int i) {
        if (this.c != null) {
            return this.c.get(i);
        }
        return null;
    }

    public GeoPoint getStart() {
        return this.e;
    }

    public void setLine(ArrayList<MKLine> arrayList) {
        this.d = arrayList;
    }
}
