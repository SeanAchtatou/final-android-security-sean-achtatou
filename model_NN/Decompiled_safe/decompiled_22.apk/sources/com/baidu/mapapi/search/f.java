package com.baidu.mapapi.search;

import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

final class f extends Handler {
    f() {
    }

    public void handleMessage(Message message) {
        ImageView imageView;
        switch (message.what) {
            case 1:
                j jVar = (j) message.obj;
                if (PlaceCaterActivity.c != null) {
                    PlaceCaterActivity.c.setImageBitmap(jVar.a());
                    return;
                }
                return;
            case 2:
                j jVar2 = (j) message.obj;
                if (PlaceCaterActivity.q != null && (imageView = (ImageView) PlaceCaterActivity.q.get(Integer.valueOf(message.arg1))) != null) {
                    imageView.setImageBitmap(jVar2.a());
                    return;
                }
                return;
            default:
                return;
        }
    }
}
