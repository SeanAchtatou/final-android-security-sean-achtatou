package com.baidu.mapapi.search;

import java.util.ArrayList;

public class MKSuggestionResult {
    private int a = 0;
    private ArrayList<MKSuggestionInfo> b;

    /* access modifiers changed from: package-private */
    public void a(ArrayList<MKSuggestionInfo> arrayList) {
        this.b = arrayList;
    }

    public ArrayList<MKSuggestionInfo> getAllSuggestions() {
        return this.b;
    }

    public MKSuggestionInfo getSuggestion(int i) {
        if (this.b == null || i < 0 || i > this.b.size() - 1) {
            return null;
        }
        return this.b.get(i);
    }

    public int getSuggestionNum() {
        if (this.b != null) {
            this.a = this.b.size();
        } else {
            this.a = 0;
        }
        return this.a;
    }
}
