package com.baidu.mapapi.search;

import java.util.ArrayList;

public class MKRoutePlan {
    private int a;
    private ArrayList<MKRoute> b;

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.a = i;
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList<MKRoute> arrayList) {
        this.b = arrayList;
    }

    public int getDistance() {
        return this.a;
    }

    public int getNumRoutes() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public MKRoute getRoute(int i) {
        if (this.b == null || i < 0 || i > this.b.size() - 1) {
            return null;
        }
        return this.b.get(i);
    }
}
