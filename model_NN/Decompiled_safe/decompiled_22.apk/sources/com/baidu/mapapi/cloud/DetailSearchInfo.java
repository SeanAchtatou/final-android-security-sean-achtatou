package com.baidu.mapapi.cloud;

import org.apache.commons.httpclient.cookie.CookieSpec;

public class DetailSearchInfo {
    public String ak;
    public int id;
    public int scope = 1;
    public String sn;
    public int timeStamp = 0;

    /* access modifiers changed from: package-private */
    public String a() {
        if (this.id == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(CookieSpec.PATH_DELIM);
        sb.append(this.id).append('?');
        sb.append("scope=").append(this.scope);
        if (this.ak != null) {
            sb.append("&ak=").append(this.ak);
        }
        if (this.sn != null) {
            sb.append("&sn=").append(this.sn);
            sb.append("&timestamp=").append(this.timeStamp);
        }
        return sb.toString();
    }
}
