package com.baidu.mapapi.map;

import android.widget.Toast;
import com.baidu.platform.comapi.a;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.map.f;
import com.baidu.platform.comapi.map.r;
import com.baidu.platform.comapi.map.s;
import com.baidu.platform.comapi.map.t;
import java.util.List;

class d implements t {
    final /* synthetic */ MapView a;

    d(MapView mapView) {
        this.a = mapView;
    }

    public void a() {
        this.a.a();
    }

    public void a(int i) {
        this.a.c(i);
    }

    public void a(int i, GeoPoint geoPoint, int i2) {
        this.a.a(i, geoPoint, i2);
    }

    public void a(List<f> list) {
    }

    public void a(List<r> list, int i) {
        this.a.a(list.get(0), i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.MapView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float
      com.baidu.mapapi.map.MapView.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, int):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, com.baidu.mapapi.map.Overlay):void
      com.baidu.mapapi.map.MapView.a(com.baidu.platform.comapi.map.r, int):void
      com.baidu.mapapi.map.MapView.a(int, int):void
      com.baidu.mapapi.map.MapView.a(int, com.baidu.platform.comapi.map.d):void
      com.baidu.mapapi.map.MapView.a(boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float
     arg types: [com.baidu.mapapi.map.MapView, int]
     candidates:
      com.baidu.mapapi.map.MapView.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, int):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, com.baidu.mapapi.map.Overlay):void
      com.baidu.mapapi.map.MapView.a(com.baidu.platform.comapi.map.r, int):void
      com.baidu.mapapi.map.MapView.a(int, int):void
      com.baidu.mapapi.map.MapView.a(int, com.baidu.platform.comapi.map.d):void
      com.baidu.mapapi.map.MapView.a(boolean, boolean):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float */
    public void b() {
        float k = this.a.a.k();
        if (k <= 3.0f) {
            if (this.a.i != k) {
                float unused = this.a.i = k;
                Toast.makeText(this.a.getContext(), "已缩小到最小级别", 0).show();
                this.a.a(true, false);
            }
        } else if (k >= 19.0f) {
            if (this.a.i != k) {
                float unused2 = this.a.i = k;
                Toast.makeText(this.a.getContext(), "已放大到最大级别", 0).show();
                this.a.a(false, true);
            }
        } else if (this.a.i != 0.0f) {
            float unused3 = this.a.i = 0.0f;
            this.a.a(true, true);
        }
        if (!(this.a.d.b == null || this.a.d.b.getTarget() == null)) {
            this.a.d.b.getTarget().sendMessage(this.a.d.b);
            this.a.d.b = null;
        }
        if (this.a.o != null && a.a) {
            this.a.o.onMapAnimationFinish();
        }
    }

    public void b(List<r> list) {
    }

    public void b(List<r> list, int i) {
        this.a.c(list.get(0).b, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.MapView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float
      com.baidu.mapapi.map.MapView.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, int):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, com.baidu.mapapi.map.Overlay):void
      com.baidu.mapapi.map.MapView.a(com.baidu.platform.comapi.map.r, int):void
      com.baidu.mapapi.map.MapView.a(int, int):void
      com.baidu.mapapi.map.MapView.a(int, com.baidu.platform.comapi.map.d):void
      com.baidu.mapapi.map.MapView.a(boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float
     arg types: [com.baidu.mapapi.map.MapView, int]
     candidates:
      com.baidu.mapapi.map.MapView.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, int):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, com.baidu.mapapi.map.Overlay):void
      com.baidu.mapapi.map.MapView.a(com.baidu.platform.comapi.map.r, int):void
      com.baidu.mapapi.map.MapView.a(int, int):void
      com.baidu.mapapi.map.MapView.a(int, com.baidu.platform.comapi.map.d):void
      com.baidu.mapapi.map.MapView.a(boolean, boolean):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float */
    public void c() {
        float k = this.a.a.k();
        if (k <= 3.0f) {
            if (this.a.i != k) {
                float unused = this.a.i = k;
                Toast.makeText(this.a.getContext(), "已缩小到最小级别", 0).show();
                this.a.a(true, false);
            }
        } else if (k >= 19.0f) {
            if (this.a.i != k) {
                float unused2 = this.a.i = k;
                Toast.makeText(this.a.getContext(), "已放大到最大级别", 0).show();
                this.a.a(false, true);
            }
        } else if (this.a.i != 0.0f) {
            float unused3 = this.a.i = 0.0f;
            this.a.a(true, true);
        }
        if (this.a.o != null && a.a) {
            this.a.o.onMapMoveFinish();
        }
    }

    public void c(List<r> list, int i) {
        if (list != null && list.size() > 0) {
            r rVar = list.get(0);
            if (rVar.e == 17) {
                MapPoi mapPoi = new MapPoi();
                mapPoi.geoPt = com.baidu.mapapi.utils.d.a(rVar.d);
                mapPoi.strText = rVar.c.replaceAll("\\\\", "");
                mapPoi.offset = rVar.f;
                if (this.a.o != null && a.a) {
                    this.a.o.onClickMapPoi(mapPoi);
                }
            }
            if (rVar.e == 19) {
                s k = this.a.d.a.k();
                k.c = 0;
                k.b = 0;
                this.a.d.a.a(k, 300);
            }
            if (rVar.e == 18) {
                this.a.b(i);
            }
            if (rVar.e == 23) {
                this.a.a(rVar, i);
            }
        }
    }
}
