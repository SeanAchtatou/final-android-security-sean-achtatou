package com.baidu.mapapi.map;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import com.baidu.mapapi.search.MKRoute;
import com.baidu.mapapi.search.MKStep;
import com.baidu.mapapi.search.c;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.map.d;
import com.baidu.platform.comapi.map.x;
import java.util.ArrayList;
import org.apache.commons.httpclient.cookie.CookiePolicy;

public class RouteOverlay extends ItemizedOverlay {
    private ArrayList<a> a = null;
    private MapView b = null;
    private Context c = null;
    private String d = null;
    private d e = null;
    public ArrayList<MKRoute> mRoute = null;

    private class a {
        public String a;
        public GeoPoint b;
        public int c;

        private a() {
        }
    }

    public RouteOverlay(Activity activity, MapView mapView) {
        super(null, mapView);
        this.mType = 12;
        this.c = activity;
        this.b = mapView;
        this.a = new ArrayList<>();
        this.mRoute = new ArrayList<>();
    }

    private void f() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < size(); i++) {
            arrayList.add(createItem(i));
        }
        super.a(arrayList);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.e = new x(this.mType);
        this.mLayerID = this.b.a(CookiePolicy.DEFAULT);
        if (this.mLayerID == 0) {
            throw new RuntimeException("can not create route layer.");
        }
        this.b.a(this.mLayerID, this.e);
    }

    public void animateTo() {
        OverlayItem item;
        if (size() > 0 && (item = getItem(0)) != null) {
            this.b.getController().animateTo(item.mPoint);
        }
    }

    /* access modifiers changed from: protected */
    public OverlayItem createItem(int i) {
        a aVar = this.a.get(i);
        return new OverlayItem(aVar.b, aVar.a, null);
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.d;
    }

    public d getInnerOverlay() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        OverlayItem item = getItem(i);
        this.b.getController().animateTo(item.mPoint);
        if (item.mTitle != null) {
            Toast.makeText(this.c, item.mTitle, 1).show();
        }
        super.onTap(i);
        return true;
    }

    public void setData(MKRoute mKRoute) {
        int i = 3;
        if (mKRoute != null && mKRoute.getStart() != null && mKRoute.getEnd() != null) {
            this.mRoute.add(mKRoute);
            if (mKRoute.getRouteType() != 1) {
                i = mKRoute.getRouteType() == 2 ? 2 : mKRoute.getRouteType() == 3 ? 4 : 0;
            }
            GeoPoint start = mKRoute.getStart();
            if (start != null) {
                a aVar = new a();
                aVar.b = start;
                aVar.c = 0;
                if (i == 4) {
                    aVar.a = mKRoute.getStep(0).getContent();
                }
                this.a.add(aVar);
            }
            int numSteps = mKRoute.getNumSteps();
            if (numSteps != 0) {
                for (int i2 = 0; i2 < numSteps; i2++) {
                    MKStep step = mKRoute.getStep(i2);
                    a aVar2 = new a();
                    aVar2.b = step.getPoint();
                    aVar2.a = step.getContent();
                    aVar2.c = i;
                    this.a.add(aVar2);
                }
            }
            GeoPoint end = mKRoute.getEnd();
            if (end != null) {
                a aVar3 = new a();
                aVar3.b = end;
                aVar3.c = 1;
                this.a.add(aVar3);
            }
            f();
            this.d = c.b(this.mRoute);
        }
    }

    public int size() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }
}
