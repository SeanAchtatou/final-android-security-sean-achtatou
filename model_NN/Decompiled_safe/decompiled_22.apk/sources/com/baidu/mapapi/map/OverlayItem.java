package com.baidu.mapapi.map;

import android.graphics.drawable.Drawable;
import com.baidu.platform.comapi.basestruct.GeoPoint;

public class OverlayItem {
    private int a = 1;
    private Drawable b = null;
    private String c = "";
    protected GeoPoint mPoint;
    protected String mSnippet;
    protected String mTitle;

    public OverlayItem(GeoPoint geoPoint, String str, String str2) {
        this.mPoint = geoPoint;
        this.mTitle = str;
        this.mSnippet = str2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.a = i;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        if (getMarker() == null) {
            return -1;
        }
        return getMarker().hashCode();
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.c;
    }

    public final Drawable getMarker() {
        return this.b;
    }

    public GeoPoint getPoint() {
        return this.mPoint;
    }

    public String getSnippet() {
        return this.mSnippet;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.mPoint = geoPoint;
    }

    public void setMarker(Drawable drawable) {
        this.b = drawable;
    }

    public void setSnippet(String str) {
        this.mSnippet = str;
    }

    public void setTitle(String str) {
        this.mTitle = str;
    }
}
