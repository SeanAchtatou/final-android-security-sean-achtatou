package com.baidu.mapapi.map;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import com.baidu.mapapi.search.MKPoiInfo;
import com.baidu.mapapi.search.c;
import com.baidu.platform.comapi.map.d;
import com.baidu.platform.comapi.map.w;
import java.util.ArrayList;
import org.apache.commons.httpclient.cookie.CookiePolicy;

public class PoiOverlay extends Overlay {
    private MapView a = null;
    private Context b = null;
    private ArrayList<MKPoiInfo> c = null;
    private String d = null;
    private d e;

    public PoiOverlay(Activity activity, MapView mapView) {
        this.mType = 14;
        this.b = activity;
        this.c = new ArrayList<>();
        this.a = mapView;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.e = new w(this.mType);
        this.mLayerID = this.a.a(CookiePolicy.DEFAULT);
        if (this.mLayerID == 0) {
            throw new RuntimeException("can not create poi layer.");
        }
        this.a.a(this.mLayerID, this.e);
    }

    public void animateTo() {
        if (size() > 0) {
            this.a.getController().animateTo(this.c.get(0).pt);
        }
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.d;
    }

    public d getInnerOverlay() {
        return this.e;
    }

    public MKPoiInfo getPoi(int i) {
        if (this.c == null) {
            return null;
        }
        return this.c.get(i);
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        this.a.getController().animateTo(this.c.get(i).pt);
        Toast.makeText(this.b, this.c.get(i).name, 1).show();
        return false;
    }

    public void setData(ArrayList<MKPoiInfo> arrayList) {
        if (arrayList != null && arrayList.size() > 0) {
            this.c.clear();
            for (int i = 0; i < arrayList.size(); i++) {
                this.c.add(arrayList.get(i));
            }
            this.d = c.c(this.c);
        }
    }

    public int size() {
        if (this.c == null) {
            return 0;
        }
        if (this.c.size() <= 10) {
            return this.c.size();
        }
        return 10;
    }
}
