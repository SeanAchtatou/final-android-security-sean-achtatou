package com.baidu.mapapi.map;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ZoomControls;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.utils.d;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.map.Projection;
import com.baidu.platform.comapi.map.n;
import com.baidu.platform.comapi.map.q;
import com.baidu.platform.comapi.map.r;
import com.baidu.platform.comapi.map.s;
import com.baidu.platform.comapi.map.t;
import com.baidu.platform.comapi.map.w;
import com.baidu.platform.comapi.map.z;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class MapView extends ViewGroup {
    q a = null;
    c b = null;
    com.baidu.platform.comapi.map.a c = null;
    /* access modifiers changed from: private */
    public MapController d = null;
    private t e = null;
    private int f = 0;
    private int g = 0;
    private ZoomControls h = null;
    /* access modifiers changed from: private */
    public float i = -1.0f;
    private Projection j = null;
    private int k = 0;
    private int l = 0;
    private q.b m = null;
    private Context n = null;
    /* access modifiers changed from: private */
    public MKMapViewListener o = null;
    private boolean p = false;
    private s q;
    private boolean r = false;
    private boolean s = false;
    private List<Overlay> t;

    public static class LayoutParams extends ViewGroup.LayoutParams {
        public static final int BOTTOM = 80;
        public static final int BOTTOM_CENTER = 81;
        public static final int CENTER = 17;
        public static final int CENTER_HORIZONTAL = 1;
        public static final int CENTER_VERTICAL = 16;
        public static final int LEFT = 3;
        public static final int MODE_MAP = 0;
        public static final int MODE_VIEW = 1;
        public static final int RIGHT = 5;
        public static final int TOP = 48;
        public static final int TOP_LEFT = 51;
        public int alignment;
        public int mode;
        public GeoPoint point;
        public int x;
        public int y;

        public LayoutParams(int i, int i2, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.mode = 1;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3) {
            this(i, i2, geoPoint, 0, 0, i3);
        }

        public LayoutParams(int i, int i2, GeoPoint geoPoint, int i3, int i4, int i5) {
            super(i, i2);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
            this.mode = 0;
            this.point = geoPoint;
            this.x = i3;
            this.y = i4;
            this.alignment = i5;
        }

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.mode = 1;
            this.point = null;
            this.x = 0;
            this.y = 0;
            this.alignment = 51;
        }
    }

    class a implements Projection {
        a() {
        }

        public GeoPoint fromPixels(int i, int i2) {
            GeoPoint fromPixels = MapView.this.a.f().fromPixels(i, i2);
            if (fromPixels == null) {
                return null;
            }
            return d.a(fromPixels);
        }

        public float metersToEquatorPixels(float f) {
            return MapView.this.a.f().metersToEquatorPixels(f);
        }

        public Point toPixels(GeoPoint geoPoint, Point point) {
            return MapView.this.a.f().toPixels(d.b(geoPoint), point);
        }
    }

    private class b implements q.b {
        private b() {
        }

        /* synthetic */ b(MapView mapView, b bVar) {
            this();
        }

        public void a(Object obj) {
            MapView.this.a((Overlay) obj);
        }

        public void b(Object obj) {
            MapView.this.b((Overlay) obj);
        }
    }

    private class c extends View {
        public c(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            MapView.this.a();
        }
    }

    public MapView(Context context) {
        super(context);
        a(context);
        addView(this.a);
        addView(this.b);
    }

    public MapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
        addView(this.a);
        addView(this.b);
    }

    public MapView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context);
        addView(this.a);
        addView(this.b);
    }

    /* access modifiers changed from: private */
    public void a(int i2, GeoPoint geoPoint, int i3) {
        List<Overlay> e2 = this.a.e();
        if (e2 != null) {
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 < e2.size()) {
                    if (e2.get(i5).mType == 27 && geoPoint != null) {
                        if (!((ItemizedOverlay) e2.get(i5)).onTap(getProjection().fromPixels(geoPoint.getLatitudeE6(), geoPoint.getLongitudeE6()), this) && i2 != -1 && i3 == e2.get(i5).mLayerID) {
                            ((ItemizedOverlay) e2.get(i5)).onTap(i2);
                        }
                    }
                    i4 = i5 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private void a(Context context) {
        this.n = context;
        if (this.a == null) {
            this.a = new q(context);
            Bundle bundle = new Bundle();
            bundle.remove("overlooking");
            bundle.remove("rotation");
            bundle.putDouble("centerptx", 1.2958162E7d);
            bundle.putDouble("centerpty", 4825907.0d);
            bundle.putString("modulePath", com.baidu.platform.comapi.d.c.q());
            bundle.putString("appSdcardPath", com.baidu.mapapi.utils.a.a());
            bundle.putString("appCachePath", com.baidu.mapapi.utils.a.b());
            bundle.putString("appSecondCachePath", com.baidu.mapapi.utils.a.c());
            bundle.putInt("mapTmpMax", com.baidu.mapapi.utils.a.d());
            bundle.putInt("domTmpMax", com.baidu.mapapi.utils.a.e());
            bundle.putInt("itsTmpMax", com.baidu.mapapi.utils.a.f());
            this.a.a(bundle, context);
            this.d = new MapController(this);
            this.d.a = this.a.b();
            f();
            this.k = (int) (36.0f * com.baidu.platform.comapi.d.c.A);
            this.l = (int) (40.0f * com.baidu.platform.comapi.d.c.A);
            e();
            d();
            refresh();
            this.a.layout(this.a.getLeft() + 1, this.a.getTop() + 1, this.a.getRight() + 1, this.a.getBottom() + 1);
            this.a.setVisibility(0);
            this.a.setFocusable(true);
            this.h = new ZoomControls(context);
            if (this.h.getParent() == null) {
                this.h.setOnZoomOutClickListener(new b(this));
                this.h.setOnZoomInClickListener(new c(this));
                this.h.setFocusable(true);
                this.h.setVisibility(0);
                this.h.measure(0, 0);
            }
            this.b = new c(context);
            this.b.setBackgroundColor(0);
            this.b.layout(this.a.getLeft() + 1, this.a.getTop() + 1, this.a.getRight() + 1, this.a.getBottom() + 1);
            this.m = new b(this, null);
            this.a.a(this.m);
        }
    }

    private void a(View view, ViewGroup.LayoutParams layoutParams) {
        view.measure(this.f, this.g);
        int i2 = layoutParams.width;
        int i3 = layoutParams.height;
        int measuredWidth = i2 == -1 ? this.f : i2 == -2 ? view.getMeasuredWidth() : i2;
        if (i3 == -1) {
            i3 = this.g;
        } else if (i3 == -2) {
            i3 = view.getMeasuredHeight();
        }
        if (checkLayoutParams(layoutParams)) {
            LayoutParams layoutParams2 = (LayoutParams) layoutParams;
            int i4 = layoutParams2.x;
            int i5 = layoutParams2.y;
            if (layoutParams2.mode == 0 && layoutParams2.point != null) {
                Point pixels = getProjection().toPixels(layoutParams2.point, null);
                i4 = pixels.x + layoutParams2.x;
                i5 = pixels.y + layoutParams2.y;
            }
            switch (layoutParams2.alignment) {
                case 1:
                    i4 -= measuredWidth / 2;
                    break;
                case 5:
                    i4 -= measuredWidth;
                    break;
                case 16:
                    i5 -= i3 / 2;
                    break;
                case 17:
                    i4 -= measuredWidth / 2;
                    i5 -= i3 / 2;
                    break;
                case 80:
                    i5 -= i3;
                    break;
                case LayoutParams.BOTTOM_CENTER /*81*/:
                    i4 -= measuredWidth / 2;
                    i5 -= i3;
                    break;
            }
            view.layout(i4, i5, measuredWidth + i4, i3 + i5);
            return;
        }
        view.layout(0, 0, measuredWidth, i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    /* access modifiers changed from: private */
    public void a(Overlay overlay) {
        if (overlay.mType != 21) {
            if (overlay.mLayerID != 0) {
                throw new RuntimeException("cat not add overlay,overlay already exists in mapview");
            }
            if (overlay.mType == 27) {
                ItemizedOverlay itemizedOverlay = (ItemizedOverlay) overlay;
                itemizedOverlay.a();
                itemizedOverlay.b();
            } else if (overlay.mType == 12) {
                ((RouteOverlay) overlay).a();
            } else if (overlay.mType == 28) {
                ((TransitOverlay) overlay).a();
            } else if (overlay.mType == 14) {
                ((PoiOverlay) overlay).a();
            } else if (overlay.mType == 29) {
                GraphicsOverlay graphicsOverlay = (GraphicsOverlay) overlay;
                graphicsOverlay.a();
                graphicsOverlay.c();
            } else if (overlay.mType == 7) {
                ((MyLocationOverlay) overlay).a();
            } else if (overlay.mType == 30) {
                TextOverlay textOverlay = (TextOverlay) overlay;
                textOverlay.a();
                textOverlay.b();
            }
            if (overlay.mLayerID != 0) {
                this.d.a.b().a(overlay.mLayerID, true);
                this.d.a.b().a(overlay.mLayerID);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(r rVar, int i2) {
        List<Overlay> e2 = this.a.e();
        if (e2 != null) {
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 < e2.size()) {
                    if (e2.get(i4).mLayerID == i2) {
                        if (e2.get(i4).mType == 12) {
                            ((RouteOverlay) e2.get(i4)).onTap(rVar.b);
                        }
                        if (e2.get(i4).mType == 28) {
                            ((TransitOverlay) e2.get(i4)).onTap(rVar.b);
                        }
                    }
                    i3 = i4 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        List<Overlay> e2 = this.a.e();
        if (e2 != null) {
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 < e2.size()) {
                    if (e2.get(i4).mType == 7 && e2.get(i4).mLayerID == i2) {
                        ((MyLocationOverlay) e2.get(i4)).dispatchTap();
                    }
                    i3 = i4 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    /* access modifiers changed from: private */
    public void b(Overlay overlay) {
        if (overlay.mLayerID != 0) {
            if (overlay.mType == 21) {
                this.d.a.b().c(this.d.a.a);
                this.d.a.b().a(this.d.a.a, false);
                return;
            }
            a(overlay.mLayerID);
            this.d.a.b().c(overlay.mLayerID);
            this.d.a.b().a(overlay.mLayerID, false);
            this.d.a.b().a(overlay.mLayerID);
            this.d.a.b().b(overlay.mLayerID);
            overlay.mLayerID = 0;
        }
    }

    /* access modifiers changed from: private */
    public void c(int i2) {
        List<Overlay> e2 = this.a.e();
        if (e2 != null) {
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 < e2.size()) {
                    if (e2.get(i4).mType == 21 && ((PopupOverlay) e2.get(i4)).a != null) {
                        ((PopupOverlay) e2.get(i4)).a.onClickedPopup(i2);
                    }
                    i3 = i4 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(int i2, int i3) {
        List<Overlay> e2 = this.a.e();
        if (e2 != null) {
            int i4 = 0;
            while (true) {
                int i5 = i4;
                if (i5 < e2.size()) {
                    if (e2.get(i5).mType == 14 && e2.get(i5).mLayerID == i3) {
                        ((PoiOverlay) e2.get(i5)).onTap(i2);
                    }
                    i4 = i5 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    private void d() {
        try {
            AssetManager assets = this.n.getAssets();
            InputStream open = com.baidu.platform.comapi.d.c.m() >= 180 ? assets.open("logo_h.png") : assets.open("logo_l.png");
            if (open != null) {
                Bitmap decodeStream = BitmapFactory.decodeStream(open);
                open.close();
                Bundle bundle = new Bundle();
                bundle.putInt("logoaddr", this.d.a.g.get("logo").intValue());
                bundle.putInt("bshow", 1);
                bundle.putInt("imgW", decodeStream.getWidth());
                bundle.putInt("imgH", decodeStream.getHeight());
                bundle.putInt("showLR", 1);
                bundle.putInt("iconwidth", 0);
                bundle.putInt("iconlayer", 1);
                bundle.putInt("bound", 0);
                bundle.putInt("popname", -1288857267);
                ByteBuffer allocate = ByteBuffer.allocate(decodeStream.getWidth() * decodeStream.getHeight() * 4);
                decodeStream.copyPixelsToBuffer(allocate);
                bundle.putByteArray("imgdata", allocate.array());
                this.d.a.b().e(bundle);
                this.d.a.b().a(this.d.a.c, true);
                this.d.a.b().a(this.d.a.c);
            }
        } catch (Exception e2) {
        }
    }

    private void e() {
        com.baidu.platform.comapi.map.b.a().a(String.format("{\"dataset\":[{\"x\":%d,\"y\":%d,\"hidetime\":1000}]}", Integer.valueOf(this.k), Integer.valueOf(this.l)));
        com.baidu.platform.comapi.map.b.a().d();
    }

    private void f() {
        this.e = new d(this);
        this.a.a(this.e);
        this.c = new e(this);
        this.a.a(this.c);
    }

    /* access modifiers changed from: package-private */
    public int a(String str) {
        return this.a.a(str);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = super.getChildAt(i2);
            if (!(childAt == this.b || childAt == this.h || childAt == this.a)) {
                ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
                if ((layoutParams instanceof LayoutParams) && ((LayoutParams) layoutParams).mode == 0) {
                    a(childAt, layoutParams);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.a.a(i2);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        s n2 = this.a.n();
        float f2 = n2.a;
        GeoPoint mapCenter = getMapCenter();
        GeoPoint geoPoint = new GeoPoint(mapCenter.getLatitudeE6() - (i2 / 2), mapCenter.getLongitudeE6() + (i3 / 2));
        GeoPoint geoPoint2 = new GeoPoint(mapCenter.getLatitudeE6() + (i2 / 2), mapCenter.getLongitudeE6() - (i3 / 2));
        GeoPoint b2 = d.b(geoPoint);
        GeoPoint b3 = d.b(geoPoint2);
        com.baidu.platform.comapi.basestruct.c cVar = new com.baidu.platform.comapi.basestruct.c();
        cVar.a(b2.getLongitudeE6());
        cVar.b(b2.getLatitudeE6());
        com.baidu.platform.comapi.basestruct.c cVar2 = new com.baidu.platform.comapi.basestruct.c();
        cVar2.a(b3.getLongitudeE6());
        cVar2.b(b3.getLatitudeE6());
        com.baidu.platform.comapi.basestruct.b bVar = new com.baidu.platform.comapi.basestruct.b();
        bVar.a(cVar);
        bVar.b(cVar2);
        if (bVar != null) {
            f2 = this.a.a(bVar);
        }
        n2.a = f2;
        this.d.a.a(n2);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, com.baidu.platform.comapi.map.d dVar) {
        this.a.a(i2, dVar);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z, boolean z2) {
        this.h.setIsZoomOutEnabled(z2);
        this.h.setIsZoomInEnabled(z);
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3) {
        this.k = i2;
        this.l = i3;
        e();
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.d.zoomIn();
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.d.zoomOut();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void destroy() {
        if (this.a != null) {
            this.a.a();
            this.a = null;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            return onKeyDown(keyEvent.getKeyCode(), keyEvent);
        }
        if (keyEvent.getAction() == 1) {
            return onKeyUp(keyEvent.getKeyCode(), keyEvent);
        }
        return false;
    }

    @Deprecated
    public void displayZoomControls(boolean z) {
        if (!this.p || this.h.getParent() == null) {
            removeView(this.h);
            addView(this.h);
            this.p = true;
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        destroy();
        super.finalize();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return super.generateDefaultLayoutParams();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new LayoutParams(layoutParams);
    }

    public MapController getController() {
        return this.d;
    }

    public boolean getCurrentMap() {
        this.d.a.a(com.baidu.mapapi.utils.a.h() + "/BaiduMapSDK/capture.png");
        return true;
    }

    public int getLatitudeSpan() {
        return this.a.c();
    }

    public int getLongitudeSpan() {
        return this.a.d();
    }

    public GeoPoint getMapCenter() {
        GeoPoint j2 = this.a.j();
        if (j2 == null) {
            return null;
        }
        return d.a(j2);
    }

    public int getMapOverlooking() {
        return this.a.m();
    }

    public int getMapRotation() {
        return this.a.l();
    }

    public int getMaxZoomLevel() {
        return 19;
    }

    public int getMinZoomLevel() {
        return 3;
    }

    public List<Overlay> getOverlays() {
        return this.a.e();
    }

    public Projection getProjection() {
        if (this.j == null) {
            this.j = new a();
        }
        return this.j;
    }

    @Deprecated
    public View getZoomControls() {
        return this.h;
    }

    public float getZoomLevel() {
        return this.a.k();
    }

    public boolean isDoubleClickZooming() {
        return this.d.a.e();
    }

    public boolean isSatellite() {
        return this.a.g();
    }

    public boolean isTraffic() {
        return this.a.h();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (this.a != null && indexOfChild(this.a) == -1) {
            addView(this.a);
        }
        if (this.p) {
            setBuiltInZoomControls(true);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.p && this.h.getParent() != null) {
            removeView(this.h);
        }
        removeView(this.a);
        super.onDetachedFromWindow();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return this.a.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        return this.a.onKeyUp(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        this.f = i4 - i2;
        this.g = i5 - i3;
        ViewGroup.LayoutParams layoutParams = this.b.getLayoutParams();
        layoutParams.width = this.f;
        layoutParams.height = this.g;
        this.b.setVisibility(0);
        this.b.layout(0, 0, this.f, this.g);
        ViewGroup.LayoutParams layoutParams2 = this.a.getLayoutParams();
        layoutParams2.width = this.f;
        layoutParams2.height = this.g;
        this.a.setVisibility(0);
        this.a.layout(0, 0, this.f, this.g);
        if (this.p && this.h != null) {
            ViewGroup.LayoutParams layoutParams3 = this.h.getLayoutParams();
            if (layoutParams3 != null) {
                layoutParams3.height = -2;
                layoutParams3.width = -2;
            }
            this.h.setVisibility(0);
            this.h.measure(i4 - i2, i5 - i3);
            this.h.layout((i4 - 10) - this.h.getMeasuredWidth(), ((i5 - 5) - this.h.getMeasuredHeight()) - i3, i4 - 10, (i5 - 5) - i3);
        }
        int childCount = getChildCount();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = super.getChildAt(i6);
            if (!(childAt == this.b || childAt == this.h || childAt == this.a)) {
                a(childAt, childAt.getLayoutParams());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
    }

    public void onPause() {
        if (this.a != null) {
            this.q = this.a.n();
            this.r = this.a.g();
            this.s = this.a.h();
            this.t = new ArrayList(getOverlays());
            getOverlays().clear();
            this.a.onPause();
        }
    }

    public void onRestoreInstanceState(Bundle bundle) {
        int i2 = bundle.getInt("lat");
        int i3 = bundle.getInt("lon");
        if (!(i2 == 0 || i3 == 0)) {
            this.d.setCenter(new GeoPoint(i2, i3));
        }
        float f2 = (float) bundle.getInt("zoom");
        if (f2 != 0.0f) {
            this.d.setZoom(f2);
        }
        setTraffic(bundle.getBoolean("traffic"));
    }

    public void onResume() {
        if (this.a != null) {
            this.a.onResume();
            if (this.q != null) {
                s n2 = this.a.n();
                this.q.f = n2.f;
                this.a.a(this.q);
            }
            this.a.a(this.r);
            this.a.b(this.s);
            if (this.t != null) {
                getOverlays().clear();
                getOverlays().addAll(this.t);
            }
            refresh();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        GeoPoint mapCenter = getMapCenter();
        bundle.putInt("lat", mapCenter.getLatitudeE6());
        bundle.putInt("lon", mapCenter.getLongitudeE6());
        bundle.putFloat("zoom", getZoomLevel());
        bundle.putBoolean("traffic", isTraffic());
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.f = i2;
        this.g = i3;
        this.d.a.d(i2, i3);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.a.a(motionEvent);
    }

    public void preLoad() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    public void refresh() {
        List<Overlay> overlays = getOverlays();
        if (overlays != null) {
            for (int i2 = 0; i2 < overlays.size(); i2++) {
                if (overlays.get(i2).mType == 27) {
                    ItemizedOverlay itemizedOverlay = (ItemizedOverlay) overlays.get(i2);
                    if (itemizedOverlay.c()) {
                        if (itemizedOverlay.getAllItem().size() <= 0) {
                            this.d.a.b().c(itemizedOverlay.d());
                            this.d.a.b().a(itemizedOverlay.d(), false);
                            this.d.a.b().a(itemizedOverlay.d());
                        } else {
                            this.d.a.b().a(itemizedOverlay.d(), true);
                            this.d.a.b().a(itemizedOverlay.d());
                        }
                        itemizedOverlay.a(false);
                    }
                }
                if (overlays.get(i2).mType == 14) {
                    PoiOverlay poiOverlay = (PoiOverlay) overlays.get(i2);
                    if (poiOverlay.b() != null && !poiOverlay.b().equals("")) {
                        w wVar = (w) poiOverlay.getInnerOverlay();
                        wVar.a(poiOverlay.b());
                        wVar.a(true);
                        wVar.d();
                    }
                }
                if (overlays.get(i2).mType == 12) {
                    RouteOverlay routeOverlay = (RouteOverlay) overlays.get(i2);
                    if (routeOverlay.e() != null && !routeOverlay.e().equals("")) {
                        com.baidu.platform.comapi.map.d innerOverlay = routeOverlay.getInnerOverlay();
                        innerOverlay.a(routeOverlay.e());
                        innerOverlay.a(true);
                        innerOverlay.d();
                    }
                }
                if (overlays.get(i2).mType == 28) {
                    TransitOverlay transitOverlay = (TransitOverlay) overlays.get(i2);
                    if (transitOverlay.e() != null && !transitOverlay.e().equals("")) {
                        z innerOverlay2 = transitOverlay.getInnerOverlay();
                        innerOverlay2.a(transitOverlay.e());
                        innerOverlay2.a(true);
                        innerOverlay2.d();
                    }
                }
                if (overlays.get(i2).mType == 7) {
                    MyLocationOverlay myLocationOverlay = (MyLocationOverlay) overlays.get(i2);
                    if (myLocationOverlay.d() != null && !myLocationOverlay.d().equals("")) {
                        n nVar = (n) myLocationOverlay.b();
                        nVar.a(myLocationOverlay.c());
                        nVar.a(myLocationOverlay.d());
                        nVar.a(true);
                        nVar.d();
                    }
                }
                if (overlays.get(i2).mType == 29) {
                    GraphicsOverlay graphicsOverlay = (GraphicsOverlay) overlays.get(i2);
                    if (graphicsOverlay.d()) {
                        if (graphicsOverlay.getAllGraphics().size() == 0) {
                            this.d.a.b().c(graphicsOverlay.b());
                            this.d.a.b().a(graphicsOverlay.b(), false);
                            this.d.a.b().a(graphicsOverlay.b());
                        } else {
                            this.d.a.b().a(graphicsOverlay.b(), true);
                            this.d.a.b().a(graphicsOverlay.b());
                        }
                        graphicsOverlay.a(false);
                    }
                }
                if (overlays.get(i2).mType == 30) {
                    TextOverlay textOverlay = (TextOverlay) overlays.get(i2);
                    if (textOverlay.size() == 0) {
                        this.d.a.b().c(textOverlay.mLayerID);
                        this.d.a.b().a(textOverlay.mLayerID, false);
                        this.d.a.b().a(textOverlay.mLayerID);
                    } else {
                        this.d.a.b().a(textOverlay.mLayerID, true);
                        this.d.a.b().a(textOverlay.mLayerID);
                    }
                }
            }
        }
    }

    public void regMapViewListener(BMapManager bMapManager, MKMapViewListener mKMapViewListener) {
        if (bMapManager != null) {
            this.o = mKMapViewListener;
        }
    }

    public void setBuiltInZoomControls(boolean z) {
        if (z) {
            if (this.p || this.h.getParent() != null) {
                removeView(this.h);
            }
            addView(this.h);
            this.p = true;
            return;
        }
        this.p = false;
        removeView(this.h);
    }

    public void setDoubleClickZooming(boolean z) {
        this.d.a.e(z);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.a.setOnClickListener(onClickListener);
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.a.setOnLongClickListener(onLongClickListener);
    }

    public void setSatellite(boolean z) {
        this.r = z;
        this.a.a(z);
    }

    public void setTraffic(boolean z) {
        this.s = z;
        this.a.b(z);
    }

    public void setVisibility(int i2) {
        if (this.a != null) {
            this.a.setVisibility(i2);
        }
        super.setVisibility(i2);
    }
}
