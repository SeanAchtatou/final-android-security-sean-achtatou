package com.baidu.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import com.baidu.mapapi.utils.d;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import java.nio.ByteBuffer;

public class PopupOverlay extends Overlay {
    private static int d = 0;
    PopupClickListener a = null;
    private MapView b = null;
    private MapController c = null;

    public PopupOverlay(MapView mapView, PopupClickListener popupClickListener) {
        this.b = mapView;
        this.c = this.b.getController();
        this.mType = 21;
        this.mLayerID = mapView.getController().a.a;
        this.a = popupClickListener;
    }

    private Bitmap a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        if (bitmap == null) {
            return null;
        }
        if (bitmap2 == null && bitmap3 == null) {
            return bitmap;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (bitmap2 != null) {
            i2 = bitmap2.getWidth();
            i = bitmap2.getHeight();
        } else {
            i = 0;
            i2 = 0;
        }
        if (bitmap3 != null) {
            i3 = bitmap3.getWidth();
            i4 = bitmap3.getHeight();
        } else {
            i3 = 0;
        }
        Bitmap createBitmap = Bitmap.createBitmap(i3 + width + i2, Math.max(Math.max(height, i), i4), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        if (bitmap2 != null) {
            canvas.drawBitmap(bitmap2, (float) width, 0.0f, (Paint) null);
        }
        if (bitmap3 != null) {
            canvas.drawBitmap(bitmap3, (float) (width + i2), 0.0f, (Paint) null);
        }
        canvas.save(31);
        canvas.restore();
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    public void hidePop() {
        if (d != 0) {
            this.c.a.b().c(this.c.a.a);
            this.c.a.b().a(this.c.a.a, false);
            this.b.a.e().remove(this);
            d = 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    public void showPopup(Bitmap bitmap, GeoPoint geoPoint, int i) {
        if (geoPoint != null && bitmap != null) {
            this.c.a.b().c(this.c.a.a);
            Bundle bundle = new Bundle();
            bundle.putInt("layeraddr", this.c.a.e.get("popup").intValue());
            bundle.putInt("bshow", 1);
            GeoPoint b2 = d.b(geoPoint);
            bundle.putInt("x", b2.getLongitudeE6());
            bundle.putInt("y", b2.getLatitudeE6());
            bundle.putInt("imgW", bitmap.getWidth());
            bundle.putInt("imgH", bitmap.getHeight());
            bundle.putInt("showLR", 1);
            bundle.putInt("icon0width", 0);
            bundle.putInt("icon1width", 0);
            bundle.putInt("iconlayer", 1);
            bundle.putInt("offset", i);
            bundle.putInt("popname", -1288857266);
            ByteBuffer allocate = ByteBuffer.allocate(bitmap.getWidth() * bitmap.getHeight() * 4);
            bitmap.copyPixelsToBuffer(allocate);
            bundle.putByteArray("imgdata", allocate.array());
            this.c.a.b().b(bundle);
            this.c.a.b().a(this.c.a.a, true);
            this.c.a.b().a(this.c.a.a);
            if (d == 0) {
                this.b.a.e().add(this);
            }
            d++;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    public void showPopup(Bitmap[] bitmapArr, GeoPoint geoPoint, int i) {
        Bitmap a2;
        int i2;
        int i3;
        int i4 = 0;
        if (geoPoint != null && bitmapArr != null && bitmapArr.length != 0 && i >= 0) {
            switch (bitmapArr.length) {
                case 1:
                    i2 = 0;
                    a2 = a(bitmapArr[0], null, null);
                    break;
                case 2:
                    int width = bitmapArr[0] != null ? bitmapArr[0].getWidth() : 0;
                    a2 = a(bitmapArr[0], bitmapArr[1], null);
                    i2 = width;
                    break;
                default:
                    if (bitmapArr.length <= 2) {
                        a2 = null;
                        i2 = 0;
                        break;
                    } else {
                        if (bitmapArr[0] != null) {
                            i3 = bitmapArr[0].getWidth();
                            if (bitmapArr[1] == null) {
                                showPopup(bitmapArr[0], geoPoint, i);
                                return;
                            }
                        } else {
                            i3 = 0;
                        }
                        int width2 = bitmapArr[2] != null ? bitmapArr[2].getWidth() : 0;
                        a2 = a(bitmapArr[0], bitmapArr[1], bitmapArr[2]);
                        i4 = width2;
                        i2 = i3;
                        break;
                    }
            }
            if (a2 != null) {
                this.c.a.b().c(this.c.a.a);
                Bundle bundle = new Bundle();
                bundle.putInt("layeraddr", this.c.a.e.get("popup").intValue());
                bundle.putInt("bshow", 1);
                GeoPoint b2 = d.b(geoPoint);
                bundle.putInt("x", b2.getLongitudeE6());
                bundle.putInt("y", b2.getLatitudeE6());
                bundle.putInt("imgW", a2.getWidth());
                bundle.putInt("imgH", a2.getHeight());
                bundle.putInt("showLR", 1);
                bundle.putInt("icon0width", i2);
                bundle.putInt("icon1width", i4);
                bundle.putInt("iconlayer", 1);
                bundle.putInt("offset", i);
                bundle.putInt("popname", -1288857266);
                ByteBuffer allocate = ByteBuffer.allocate(a2.getWidth() * a2.getHeight() * 4);
                a2.copyPixelsToBuffer(allocate);
                bundle.putByteArray("imgdata", allocate.array());
                this.c.a.b().b(bundle);
                this.c.a.b().a(this.c.a.a, true);
                this.c.a.b().a(this.c.a.a);
                if (d == 0) {
                    this.b.a.e().add(this);
                }
                d++;
            }
        }
    }
}
