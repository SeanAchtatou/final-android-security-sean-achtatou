package com.baidu.mapapi.map;

import android.graphics.Color;
import android.os.Bundle;
import com.baidu.mapapi.utils.d;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vi.com.gdi.bgl.android.java.EnvDrawText;

public class TextOverlay extends Overlay {
    private ArrayList<TextItem> a = new ArrayList<>();
    private MapView b;

    public TextOverlay(MapView mapView) {
        this.b = mapView;
        this.mType = 30;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.mLayerID = this.b.a("text");
        if (this.mLayerID == 0) {
            throw new RuntimeException("can not add text layer");
        }
    }

    public void addText(TextItem textItem) {
        if (textItem != null && textItem.pt != null && textItem.text != null && textItem.fontSize != 0 && textItem.fontColor != null) {
            if (this.mLayerID == 0) {
                this.a.add(textItem);
                return;
            }
            Bundle bundle = new Bundle();
            GeoPoint b2 = d.b(textItem.pt);
            int longitudeE6 = b2.getLongitudeE6();
            int latitudeE6 = b2.getLatitudeE6();
            bundle.putInt("x", longitudeE6);
            bundle.putInt("y", latitudeE6);
            bundle.putInt("fsize", textItem.fontSize);
            bundle.putInt("bgcolor", textItem.bgColor == null ? Color.argb(0, 0, 0, 0) : Color.argb(textItem.bgColor.alpha, textItem.bgColor.blue, textItem.bgColor.green, textItem.bgColor.red));
            bundle.putInt("fcolor", Color.argb(textItem.fontColor.alpha, textItem.fontColor.blue, textItem.fontColor.green, textItem.fontColor.red));
            bundle.putString("str", textItem.text);
            textItem.a(System.currentTimeMillis() + "_" + size());
            bundle.putString(LocaleUtil.INDONESIAN, textItem.a());
            bundle.putInt("align", textItem.align);
            bundle.putInt("textaddr", this.mLayerID);
            if (textItem.typeface != null) {
                EnvDrawText.registFontCache(textItem.typeface.hashCode(), textItem.typeface);
                bundle.putInt("fstyle", textItem.typeface.hashCode());
            } else {
                bundle.putInt("fstyle", 0);
            }
            this.a.add(textItem);
            this.b.getController().a.b().h(bundle);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.a);
        removeAll();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            addText((TextItem) it.next());
        }
    }

    public List<TextItem> getAllText() {
        return this.a;
    }

    public TextItem getText(int i) {
        if (i >= size() || i < 0) {
            return null;
        }
        return this.a.get(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.platform.comjni.map.basemap.a.a(int, int):java.lang.String
      com.baidu.platform.comjni.map.basemap.a.a(int, boolean):void */
    public boolean removeAll() {
        this.b.getController().a.b().c(this.mLayerID);
        this.b.getController().a.b().a(this.mLayerID, false);
        this.b.getController().a.b().a(this.mLayerID);
        this.a.clear();
        return true;
    }

    public boolean removeText(TextItem textItem) {
        Bundle bundle = new Bundle();
        bundle.putInt("textaddr", this.mLayerID);
        if (textItem.a().equals("")) {
            return false;
        }
        bundle.putString(LocaleUtil.INDONESIAN, textItem.a());
        if (!this.b.getController().a.b().i(bundle)) {
            return false;
        }
        if (textItem.typeface != null) {
            EnvDrawText.removeFontCache(textItem.typeface.hashCode());
        }
        this.a.remove(textItem);
        return true;
    }

    public int size() {
        return this.a.size();
    }
}
