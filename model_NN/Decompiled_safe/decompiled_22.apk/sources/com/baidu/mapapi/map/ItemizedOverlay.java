package com.baidu.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.actionbarsherlock.widget.ActivityChooserView;
import com.baidu.mapapi.map.OverlayItem;
import com.baidu.mapapi.utils.d;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comjni.tools.ParcelItem;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class ItemizedOverlay<Item extends OverlayItem> extends Overlay implements Comparator<Integer> {
    private ArrayList<OverlayItem> a = new ArrayList<>();
    private ArrayList<Integer> b = new ArrayList<>();
    private Drawable c;
    private boolean d;
    protected MapView mMapView;

    public ItemizedOverlay(Drawable drawable, MapView mapView) {
        this.mType = 27;
        this.c = drawable;
        this.mMapView = mapView;
        this.mLayerID = 0;
    }

    private void a(List<OverlayItem> list, boolean z) {
        if (this.mLayerID != 0) {
            Bundle bundle = new Bundle();
            bundle.clear();
            ArrayList arrayList = new ArrayList();
            bundle.putInt("itemaddr", this.mLayerID);
            bundle.putInt("bshow", 1);
            if (z) {
                bundle.putString("extparam", "update");
            }
            for (int i = 0; i < list.size(); i++) {
                OverlayItem overlayItem = list.get(i);
                if (overlayItem.getMarker() == null) {
                    overlayItem.setMarker(this.c);
                }
                long currentTimeMillis = System.currentTimeMillis();
                if (!z) {
                    overlayItem.a("" + currentTimeMillis + "_" + i);
                }
                ParcelItem parcelItem = new ParcelItem();
                Bitmap bitmap = ((BitmapDrawable) overlayItem.getMarker()).getBitmap();
                Bundle bundle2 = new Bundle();
                GeoPoint b2 = d.b(overlayItem.getPoint());
                bundle2.putInt("x", b2.getLongitudeE6());
                bundle2.putInt("y", b2.getLatitudeE6());
                bundle2.putInt("imgW", bitmap.getWidth());
                bundle2.putInt("imgH", bitmap.getHeight());
                bundle2.putInt("showLR", 1);
                bundle2.putInt("iconwidth", 0);
                bundle2.putInt("iconlayer", 1);
                bundle2.putInt("bound", overlayItem.a());
                bundle2.putString("popname", "" + overlayItem.c());
                bundle2.putInt("imgindex", overlayItem.b());
                if (z || !a(overlayItem)) {
                    ByteBuffer allocate = ByteBuffer.allocate(bitmap.getWidth() * bitmap.getHeight() * 4);
                    bitmap.copyPixelsToBuffer(allocate);
                    bundle2.putByteArray("imgdata", allocate.array());
                } else {
                    bundle2.putByteArray("imgdata", null);
                }
                parcelItem.setBundle(bundle2);
                arrayList.add(parcelItem);
                if (!z) {
                    this.a.add(overlayItem);
                }
            }
            if (arrayList.size() > 0) {
                ParcelItem[] parcelItemArr = new ParcelItem[arrayList.size()];
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    parcelItemArr[i2] = (ParcelItem) arrayList.get(i2);
                }
                bundle.putParcelableArray("itemdatas", parcelItemArr);
                this.mMapView.getController().a.b().c(bundle);
            }
            this.d = true;
        } else if (!z) {
            this.a.addAll(list);
        }
    }

    private boolean a(OverlayItem overlayItem) {
        Iterator<OverlayItem> it = this.a.iterator();
        while (it.hasNext()) {
            OverlayItem next = it.next();
            if (overlayItem.b() == -1) {
                return false;
            }
            if (next.b() != -1 && overlayItem.b() == next.b()) {
                return true;
            }
        }
        return false;
    }

    private int b(boolean z) {
        if (this.a == null || this.a.size() == 0) {
            return 0;
        }
        int i = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        Iterator<OverlayItem> it = this.a.iterator();
        int i2 = Integer.MIN_VALUE;
        while (true) {
            int i3 = i;
            if (!it.hasNext()) {
                return i2 - i3;
            }
            GeoPoint point = it.next().getPoint();
            i = z ? point.getLatitudeE6() : point.getLongitudeE6();
            if (i > i2) {
                i2 = i;
            }
            if (i >= i3) {
                i = i3;
            }
        }
    }

    protected static void boundCenter(OverlayItem overlayItem) {
        if (overlayItem != null) {
            overlayItem.a(2);
        }
    }

    protected static void boundCenterBottom(OverlayItem overlayItem) {
        if (overlayItem != null) {
            overlayItem.a(1);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.mLayerID = this.mMapView.a("item");
        if (this.mLayerID == 0) {
            throw new RuntimeException("can not add new layer");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList<OverlayItem> arrayList) {
        int size = arrayList.size();
        if (this.b != null) {
            this.b.clear();
            this.b = null;
        }
        if (this.a != null) {
            this.a.clear();
            this.a = null;
        }
        this.a = new ArrayList<>(size);
        this.b = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            this.b.add(i, Integer.valueOf(i));
            OverlayItem overlayItem = arrayList.get(i);
            if (overlayItem.getMarker() == null) {
                overlayItem.setMarker(this.c);
            }
            this.a.add(i, overlayItem);
        }
        Collections.sort(this.b, this);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.d = z;
    }

    public void addItem(OverlayItem overlayItem) {
        if (this.a != null && overlayItem != null) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(overlayItem);
            addItem(arrayList);
        }
    }

    public void addItem(List<OverlayItem> list) {
        a(list, false);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.a);
        removeAll();
        addItem(arrayList);
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.d;
    }

    public int compare(Integer num, Integer num2) {
        GeoPoint point = this.a.get(num.intValue()).getPoint();
        GeoPoint point2 = this.a.get(num2.intValue()).getPoint();
        if (point.getLatitudeE6() > point2.getLatitudeE6()) {
            return -1;
        }
        if (point.getLatitudeE6() < point2.getLatitudeE6()) {
            return 1;
        }
        if (point.getLongitudeE6() < point2.getLongitudeE6()) {
            return -1;
        }
        return point.getLongitudeE6() == point2.getLongitudeE6() ? 0 : 1;
    }

    /* access modifiers changed from: protected */
    public Item createItem(int i) {
        return null;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.mLayerID;
    }

    public ArrayList<OverlayItem> getAllItem() {
        return this.a;
    }

    public GeoPoint getCenter() {
        int indexToDraw = getIndexToDraw(0);
        if (indexToDraw == -1) {
            return null;
        }
        return getItem(indexToDraw).getPoint();
    }

    /* access modifiers changed from: protected */
    public int getIndexToDraw(int i) {
        if (this.a == null || this.a.size() == 0) {
            return -1;
        }
        return i;
    }

    public final OverlayItem getItem(int i) {
        if (this.a == null || this.a.size() <= i || i < 0) {
            return null;
        }
        return this.a.get(i);
    }

    public int getLatSpanE6() {
        return b(true);
    }

    public int getLonSpanE6() {
        return b(false);
    }

    /* access modifiers changed from: protected */
    public boolean hitTest(OverlayItem overlayItem, Drawable drawable, int i, int i2) {
        if (drawable == null) {
            return false;
        }
        Rect bounds = drawable.getBounds();
        bounds.left -= 10;
        bounds.right += 10;
        bounds.bottom += 10;
        bounds.top -= 10;
        boolean contains = bounds.contains(i, i2);
        bounds.left += 10;
        bounds.right -= 10;
        bounds.bottom -= 10;
        bounds.top = 10 + bounds.top;
        return contains;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        return false;
    }

    public boolean onTap(GeoPoint geoPoint, MapView mapView) {
        return false;
    }

    public boolean removeAll() {
        this.mMapView.getController().a.b().c(this.mLayerID);
        this.a.clear();
        this.d = true;
        return true;
    }

    public boolean removeItem(OverlayItem overlayItem) {
        if (this.mLayerID == 0) {
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putInt("itemaddr", this.mLayerID);
        if (overlayItem.c().equals("")) {
            return false;
        }
        bundle.putString(LocaleUtil.INDONESIAN, overlayItem.c());
        if (!this.mMapView.getController().a.b().d(bundle)) {
            return false;
        }
        this.a.remove(overlayItem);
        this.d = true;
        return true;
    }

    public int size() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }

    public boolean updateItem(OverlayItem overlayItem) {
        boolean z;
        if (overlayItem == null) {
            return false;
        }
        if (overlayItem.c().equals("")) {
            return false;
        }
        Iterator<OverlayItem> it = this.a.iterator();
        while (true) {
            if (it.hasNext()) {
                if (overlayItem.c().equals(it.next().c())) {
                    z = true;
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        if (!z) {
            return false;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(overlayItem);
        a(arrayList, true);
        return true;
    }
}
