package com.baidu.mapapi.map;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import com.baidu.mapapi.search.MKLine;
import com.baidu.mapapi.search.MKPoiInfo;
import com.baidu.mapapi.search.MKRoute;
import com.baidu.mapapi.search.MKTransitRoutePlan;
import com.baidu.mapapi.search.c;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.map.z;
import java.util.ArrayList;
import org.apache.commons.httpclient.cookie.CookiePolicy;

public class TransitOverlay extends ItemizedOverlay {
    private ArrayList<a> a = null;
    private MapView b = null;
    private Context c = null;
    private int d = 1;
    private String e = null;
    private z f = null;
    public ArrayList<MKTransitRoutePlan> mPlan = null;

    private class a {
        public String a;
        public GeoPoint b;
        public int c;

        private a() {
        }
    }

    public TransitOverlay(Activity activity, MapView mapView) {
        super(null, mapView);
        this.mType = 28;
        this.c = activity;
        this.b = mapView;
        this.a = new ArrayList<>();
        this.mPlan = new ArrayList<>();
    }

    private void f() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < size(); i++) {
            arrayList.add(createItem(i));
        }
        super.a(arrayList);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f = new z(12);
        this.mLayerID = this.b.a(CookiePolicy.DEFAULT);
        if (this.mLayerID == 0) {
            throw new RuntimeException("can not create transit layer.");
        }
        this.b.a(this.mLayerID, this.f);
    }

    public void animateTo() {
        OverlayItem item;
        if (size() > 0 && (item = getItem(0)) != null) {
            this.b.getController().animateTo(item.mPoint);
        }
    }

    /* access modifiers changed from: protected */
    public OverlayItem createItem(int i) {
        a aVar = this.a.get(i);
        return new OverlayItem(aVar.b, aVar.a, null);
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.e;
    }

    public z getInnerOverlay() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int i) {
        OverlayItem item = getItem(i);
        this.b.getController().animateTo(item.mPoint);
        if (!(item == null || item.mTitle == null)) {
            Toast.makeText(this.c, item.mTitle, 1).show();
        }
        super.onTap(i);
        return true;
    }

    public void setData(MKTransitRoutePlan mKTransitRoutePlan) {
        if (mKTransitRoutePlan != null) {
            int numLines = mKTransitRoutePlan.getNumLines();
            int numRoute = mKTransitRoutePlan.getNumRoute();
            if (numLines != 0 || numRoute != 0) {
                this.mPlan.add(mKTransitRoutePlan);
                GeoPoint start = mKTransitRoutePlan.getStart();
                if (start != null) {
                    a aVar = new a();
                    aVar.b = start;
                    aVar.c = 0;
                    this.a.add(aVar);
                }
                for (int i = 0; i < numLines; i++) {
                    MKLine line = mKTransitRoutePlan.getLine(i);
                    a aVar2 = new a();
                    MKPoiInfo getOnStop = line.getGetOnStop();
                    aVar2.b = getOnStop.pt;
                    aVar2.a = "在" + getOnStop.name + "上车，" + "乘坐" + line.getTitle() + "经过" + String.valueOf(line.getNumViaStops()) + "站";
                    if (i == 0 && this.a.size() > 0) {
                        this.a.get(this.a.size() - 1).a = aVar2.a;
                    }
                    if (line.getType() == 0) {
                        aVar2.c = 2;
                    } else {
                        aVar2.c = 4;
                    }
                    this.a.add(aVar2);
                    MKLine line2 = mKTransitRoutePlan.getLine(i);
                    a aVar3 = new a();
                    MKPoiInfo getOffStop = line.getGetOffStop();
                    aVar3.b = getOffStop.pt;
                    aVar3.a = "在" + getOffStop.name + "下车";
                    int i2 = 0;
                    while (true) {
                        if (i2 >= numRoute) {
                            break;
                        }
                        MKRoute route = mKTransitRoutePlan.getRoute(i2);
                        if (route.getIndex() == i) {
                            aVar3.a += "," + route.getTip();
                            break;
                        }
                        i2++;
                    }
                    if (line2.getType() == 0) {
                        aVar3.c = 2;
                    } else {
                        aVar3.c = 4;
                    }
                    this.a.add(aVar3);
                }
                GeoPoint end = mKTransitRoutePlan.getEnd();
                if (end != null) {
                    a aVar4 = new a();
                    aVar4.b = end;
                    aVar4.c = 1;
                    this.a.add(aVar4);
                }
                f();
                this.e = c.a(this.mPlan);
            }
        }
    }

    public int size() {
        if (this.a == null) {
            return 0;
        }
        return this.a.size();
    }
}
