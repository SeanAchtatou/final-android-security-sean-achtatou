package com.baidu.mapapi.map;

import com.baidu.mapapi.utils.e;
import com.baidu.platform.comapi.map.g;
import com.baidu.platform.comapi.map.h;
import com.baidu.platform.comapi.map.k;
import com.baidu.platform.comapi.map.l;
import java.util.ArrayList;
import java.util.Iterator;

public class MKOfflineMap {
    public static final int TYPE_DOWNLOAD_UPDATE = 0;
    public static final int TYPE_NEW_OFFLINE = 6;
    public static final int TYPE_VER_UPDATE = 4;
    private h a = null;
    /* access modifiers changed from: private */
    public a b = null;
    private a c = null;

    class a implements l {
        a() {
        }

        public void a(int i, int i2) {
            switch (i) {
                case 4:
                    ArrayList<MKOLUpdateElement> allUpdateInfo = MKOfflineMap.this.getAllUpdateInfo();
                    if (allUpdateInfo != null) {
                        for (MKOLUpdateElement next : allUpdateInfo) {
                            if (next.update) {
                                MKOfflineMap.this.b.a(new MKEvent(4, 0, next.cityID));
                            }
                        }
                        return;
                    }
                    return;
                case 5:
                case 7:
                default:
                    return;
                case 6:
                    MKOfflineMap.this.b.a(new MKEvent(6, 0, i2));
                    return;
                case 8:
                    MKOfflineMap.this.b.a(new MKEvent(0, 0, 65535 & (i2 >> 16)));
                    return;
            }
        }
    }

    public void destroy() {
        this.a.d(0);
        this.a.b((l) null);
        h.a();
    }

    public ArrayList<MKOLUpdateElement> getAllUpdateInfo() {
        ArrayList<k> d = this.a.d();
        if (d == null) {
            return null;
        }
        ArrayList<MKOLUpdateElement> arrayList = new ArrayList<>();
        Iterator<k> it = d.iterator();
        while (it.hasNext()) {
            arrayList.add(e.a(it.next().a()));
        }
        return arrayList;
    }

    public ArrayList<MKOLSearchRecord> getHotCityList() {
        ArrayList<g> b2 = this.a.b();
        if (b2 == null) {
            return null;
        }
        ArrayList<MKOLSearchRecord> arrayList = new ArrayList<>();
        Iterator<g> it = b2.iterator();
        while (it.hasNext()) {
            arrayList.add(e.a(it.next()));
        }
        return arrayList;
    }

    public ArrayList<MKOLSearchRecord> getOfflineCityList() {
        ArrayList<g> c2 = this.a.c();
        if (c2 == null) {
            return null;
        }
        ArrayList<MKOLSearchRecord> arrayList = new ArrayList<>();
        Iterator<g> it = c2.iterator();
        while (it.hasNext()) {
            arrayList.add(e.a(it.next()));
        }
        return arrayList;
    }

    public MKOLUpdateElement getUpdateInfo(int i) {
        k f = this.a.f(i);
        if (f == null) {
            return null;
        }
        return e.a(f.a());
    }

    public boolean init(MapController mapController, MKOfflineMapListener mKOfflineMapListener) {
        if (mapController == null) {
            return false;
        }
        this.a = h.a(mapController.a);
        if (this.a == null) {
            return false;
        }
        this.c = new a();
        this.a.a(this.c);
        this.b = new a(mKOfflineMapListener);
        this.c.a(4, 0);
        return true;
    }

    public boolean pause(int i) {
        return this.a.c(i);
    }

    public boolean remove(int i) {
        return this.a.e(i);
    }

    public int scan() {
        return scan(false);
    }

    public int scan(boolean z) {
        int i;
        int i2 = 0;
        ArrayList<k> d = this.a.d();
        if (d != null) {
            i2 = d.size();
            i = i2;
        } else {
            i = 0;
        }
        this.a.a(z);
        ArrayList<k> d2 = this.a.d();
        if (d2 != null) {
            i2 = d2.size();
        }
        return i2 - i;
    }

    public ArrayList<MKOLSearchRecord> searchCity(String str) {
        ArrayList<g> a2 = this.a.a(str);
        if (a2 == null) {
            return null;
        }
        ArrayList<MKOLSearchRecord> arrayList = new ArrayList<>();
        Iterator<g> it = a2.iterator();
        while (it.hasNext()) {
            arrayList.add(e.a(it.next()));
        }
        return arrayList;
    }

    public boolean start(int i) {
        if (this.a == null) {
            return false;
        }
        if (this.a.d() != null) {
            Iterator<k> it = this.a.d().iterator();
            while (it.hasNext()) {
                k next = it.next();
                if (next.a.a == i) {
                    if (next.a.j || next.a.l == 2 || next.a.l == 3) {
                        return this.a.b(i);
                    }
                    return false;
                }
            }
        }
        return this.a.a(i);
    }
}
