package com.baidu.mapapi.map;

import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.baidu.mapapi.utils.d;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.map.o;
import com.baidu.platform.comapi.map.s;

public class MapController {
    o a = null;
    Message b = null;
    private MapView c = null;
    private boolean d = true;
    private boolean e = true;
    private boolean f = true;
    private boolean g = true;

    public MapController(MapView mapView) {
        this.c = mapView;
    }

    public void animateTo(GeoPoint geoPoint) {
        if (geoPoint != null) {
            this.a.a(d.b(geoPoint));
        }
    }

    public void animateTo(GeoPoint geoPoint, Message message) {
        if (geoPoint != null) {
            GeoPoint b2 = d.b(geoPoint);
            this.b = message;
            this.a.a(b2, message);
        }
    }

    public void enableClick(boolean z) {
        this.a.f(z);
    }

    public boolean handleFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return this.a.a(motionEvent, motionEvent2, f2, f3);
    }

    public boolean isOverlookingGesturesEnabled() {
        return this.g;
    }

    public boolean isRotationGesturesEnabled() {
        return this.f;
    }

    public boolean isScrollGesturesEnabled() {
        return this.d;
    }

    public boolean isZoomGesturesEnabled() {
        return this.e;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        return this.a.onKey(view, i, keyEvent);
    }

    public void scrollBy(int i, int i2) {
        this.a.e(i, i2);
    }

    public void setCenter(GeoPoint geoPoint) {
        s k;
        if (geoPoint != null && (k = this.a.k()) != null) {
            GeoPoint b2 = d.b(geoPoint);
            k.d = b2.getLongitudeE6();
            k.e = b2.getLatitudeE6();
            this.a.a(k);
        }
    }

    public void setCompassMargin(int i, int i2) {
        this.c.b(i, i2);
    }

    public void setOverlooking(int i) {
        s k;
        if (i <= 0 && i >= -45 && (k = this.a.k()) != null) {
            k.c = i;
            this.a.a(k, 300);
        }
    }

    public void setOverlookingGesturesEnabled(boolean z) {
        this.g = z;
        this.a.d(z);
    }

    public void setRotation(int i) {
        s k = this.a.k();
        if (k != null) {
            k.b = i;
            this.a.a(k, 300);
        }
    }

    public void setRotationGesturesEnabled(boolean z) {
        this.f = z;
        this.a.c(z);
    }

    public void setScrollGesturesEnabled(boolean z) {
        this.d = z;
        this.a.a(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.MapView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float
      com.baidu.mapapi.map.MapView.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, int):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, com.baidu.mapapi.map.Overlay):void
      com.baidu.mapapi.map.MapView.a(com.baidu.platform.comapi.map.r, int):void
      com.baidu.mapapi.map.MapView.a(int, int):void
      com.baidu.mapapi.map.MapView.a(int, com.baidu.platform.comapi.map.d):void
      com.baidu.mapapi.map.MapView.a(boolean, boolean):void */
    public float setZoom(float f2) {
        s k = this.a.k();
        if (k == null) {
            return -1.0f;
        }
        if (f2 < 3.0f) {
            f2 = 3.0f;
        } else if (f2 > 19.0f) {
            f2 = 19.0f;
        }
        k.a = f2;
        this.a.a(k);
        if (f2 == 3.0f) {
            this.c.a(true, false);
            return f2;
        } else if (f2 == 19.0f) {
            this.c.a(false, true);
            return f2;
        } else {
            this.c.a(true, true);
            return f2;
        }
    }

    public void setZoomGesturesEnabled(boolean z) {
        this.e = z;
        this.a.b(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.MapView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float
      com.baidu.mapapi.map.MapView.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, int):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, com.baidu.mapapi.map.Overlay):void
      com.baidu.mapapi.map.MapView.a(com.baidu.platform.comapi.map.r, int):void
      com.baidu.mapapi.map.MapView.a(int, int):void
      com.baidu.mapapi.map.MapView.a(int, com.baidu.platform.comapi.map.d):void
      com.baidu.mapapi.map.MapView.a(boolean, boolean):void */
    public boolean zoomIn() {
        boolean g2 = this.a.g();
        int l = (int) this.a.l();
        if (l <= 3) {
            this.c.a(true, false);
        } else if (l >= 19) {
            this.c.a(false, true);
        } else {
            this.c.a(true, true);
        }
        return g2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.MapView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float
      com.baidu.mapapi.map.MapView.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, int):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, com.baidu.mapapi.map.Overlay):void
      com.baidu.mapapi.map.MapView.a(com.baidu.platform.comapi.map.r, int):void
      com.baidu.mapapi.map.MapView.a(int, int):void
      com.baidu.mapapi.map.MapView.a(int, com.baidu.platform.comapi.map.d):void
      com.baidu.mapapi.map.MapView.a(boolean, boolean):void */
    public boolean zoomInFixing(int i, int i2) {
        boolean b2 = this.a.b(i, i2);
        int l = (int) this.a.l();
        if (l <= 3) {
            this.c.a(true, false);
        } else if (l >= 19) {
            this.c.a(false, true);
        } else {
            this.c.a(true, true);
        }
        return b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.MapView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float
      com.baidu.mapapi.map.MapView.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, int):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, com.baidu.mapapi.map.Overlay):void
      com.baidu.mapapi.map.MapView.a(com.baidu.platform.comapi.map.r, int):void
      com.baidu.mapapi.map.MapView.a(int, int):void
      com.baidu.mapapi.map.MapView.a(int, com.baidu.platform.comapi.map.d):void
      com.baidu.mapapi.map.MapView.a(boolean, boolean):void */
    public boolean zoomOut() {
        boolean h = this.a.h();
        int l = (int) this.a.l();
        if (l <= 3) {
            this.c.a(true, false);
        } else if (l >= 19) {
            this.c.a(false, true);
        } else {
            this.c.a(true, true);
        }
        return h;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mapapi.map.MapView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, float):float
      com.baidu.mapapi.map.MapView.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, int):void
      com.baidu.mapapi.map.MapView.a(com.baidu.mapapi.map.MapView, com.baidu.mapapi.map.Overlay):void
      com.baidu.mapapi.map.MapView.a(com.baidu.platform.comapi.map.r, int):void
      com.baidu.mapapi.map.MapView.a(int, int):void
      com.baidu.mapapi.map.MapView.a(int, com.baidu.platform.comapi.map.d):void
      com.baidu.mapapi.map.MapView.a(boolean, boolean):void */
    public boolean zoomOutFixing(int i, int i2) {
        boolean c2 = this.a.c(i, i2);
        int l = (int) this.a.l();
        if (l <= 3) {
            this.c.a(true, false);
        } else if (l >= 19) {
            this.c.a(false, true);
        } else {
            this.c.a(true, true);
        }
        return c2;
    }

    public void zoomToSpan(int i, int i2) {
        this.c.a(i, i2);
    }
}
