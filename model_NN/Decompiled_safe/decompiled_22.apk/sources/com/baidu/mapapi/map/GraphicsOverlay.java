package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapapi.utils.d;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;

public class GraphicsOverlay extends Overlay {
    private Bundle a = null;
    private MapView b = null;
    private ArrayList<Graphic> c = null;
    private boolean d = false;

    public GraphicsOverlay(MapView mapView) {
        this.mType = 29;
        this.a = new Bundle();
        this.b = mapView;
        this.c = new ArrayList<>();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.mLayerID = this.b.a("geometry");
        if (this.mLayerID == 0) {
            throw new RuntimeException("can not add geometry layer");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.mLayerID;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.b.getController().a.b().c(b());
        for (int i = 0; i < this.c.size(); i++) {
            Graphic graphic = this.c.get(i);
            ArrayList<GeoPoint> arrayList = graphic.getGeometry().b;
            if (!(arrayList == null || arrayList.size() == 0)) {
                if (graphic.getGeometry().a == 2 || graphic.getGeometry().a == 5) {
                    int size = arrayList.size();
                    if (size >= 2) {
                        int[] iArr = new int[size];
                        int[] iArr2 = new int[size];
                        for (int i2 = 0; i2 < size; i2++) {
                            if (arrayList.get(i2) != null) {
                                GeoPoint b2 = d.b(arrayList.get(i2));
                                iArr[i2] = b2.getLongitudeE6();
                                iArr2[i2] = b2.getLatitudeE6();
                            }
                        }
                        this.a.putIntArray("x", iArr);
                        this.a.putIntArray("y", iArr2);
                    }
                } else if (graphic.getGeometry().a == 3) {
                    if (!(arrayList.size() < 2 || arrayList.get(0) == null || arrayList.get(1) == null)) {
                        int[] iArr3 = new int[5];
                        int[] iArr4 = new int[5];
                        GeoPoint b3 = d.b(arrayList.get(0));
                        iArr3[0] = b3.getLongitudeE6();
                        iArr4[0] = b3.getLatitudeE6();
                        GeoPoint b4 = d.b(new GeoPoint(arrayList.get(0).getLatitudeE6(), arrayList.get(1).getLongitudeE6()));
                        iArr3[1] = b4.getLongitudeE6();
                        iArr4[1] = b4.getLatitudeE6();
                        GeoPoint b5 = d.b(arrayList.get(1));
                        iArr3[2] = b5.getLongitudeE6();
                        iArr4[2] = b5.getLatitudeE6();
                        GeoPoint b6 = d.b(new GeoPoint(arrayList.get(1).getLatitudeE6(), arrayList.get(0).getLongitudeE6()));
                        iArr3[3] = b6.getLongitudeE6();
                        iArr4[3] = b6.getLatitudeE6();
                        iArr3[4] = iArr3[0];
                        iArr4[4] = iArr4[0];
                        this.a.putIntArray("x", iArr3);
                        this.a.putIntArray("y", iArr4);
                    }
                } else if ((graphic.getGeometry().a == 4 || graphic.getGeometry().a == 1) && arrayList.get(0) != null) {
                    GeoPoint b7 = d.b(arrayList.get(0));
                    int[] iArr5 = {b7.getLongitudeE6()};
                    int[] iArr6 = {b7.getLatitudeE6()};
                    this.a.putIntArray("x", iArr5);
                    this.a.putIntArray("y", iArr6);
                }
                this.a.putInt("linewidth", graphic.getSymbol().a);
                this.a.putFloat("red", ((float) graphic.getSymbol().b.red) / 255.0f);
                this.a.putFloat("green", ((float) graphic.getSymbol().b.green) / 255.0f);
                this.a.putFloat("blue", ((float) graphic.getSymbol().b.blue) / 255.0f);
                this.a.putFloat("alpha", ((float) graphic.getSymbol().b.alpha) / 255.0f);
                if (graphic.getGeometry().a == 5) {
                    this.a.putInt("type", 2);
                } else {
                    this.a.putInt("type", graphic.getGeometry().a);
                }
                if (graphic.getGeometry().a == 5) {
                    this.a.putInt("status", 1);
                } else if (graphic.getGeometry().a == 2) {
                    this.a.putInt("status", 0);
                } else {
                    this.a.putInt("status", graphic.getSymbol().c);
                }
                if (graphic.getGeometry().a == 4 || graphic.getGeometry().a == 1) {
                    this.a.putInt("level", graphic.getGeometry().c);
                } else {
                    this.a.putInt("level", (int) this.b.getController().a.l());
                }
                this.a.putInt("geometryaddr", b());
                long currentTimeMillis = System.currentTimeMillis();
                this.a.putString(LocaleUtil.INDONESIAN, String.valueOf(currentTimeMillis));
                this.b.getController().a.b().f(this.a);
                graphic.a(currentTimeMillis);
            }
        }
        this.d = true;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.d;
    }

    public ArrayList<Graphic> getAllGraphics() {
        return this.c;
    }

    public void removeAll() {
        this.b.getController().a.b().c(b());
        this.c.clear();
        this.d = true;
    }

    public void removeGraphic(long j) {
        Bundle bundle = new Bundle();
        bundle.putInt("geometryaddr", b());
        bundle.putString(LocaleUtil.INDONESIAN, String.valueOf(j));
        this.b.getController().a.b().g(bundle);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.c.size()) {
                if (j == this.c.get(i2).getID()) {
                    this.c.remove(i2);
                }
                i = i2 + 1;
            } else {
                this.d = true;
                return;
            }
        }
    }

    public long setData(Graphic graphic) {
        this.a.clear();
        if (graphic == null || graphic.getGeometry() == null || graphic.getSymbol() == null) {
            return 0;
        }
        Geometry geometry = new Geometry();
        geometry.a = graphic.getGeometry().a;
        geometry.b = graphic.getGeometry().b;
        geometry.c = graphic.getGeometry().c;
        Symbol symbol = new Symbol();
        symbol.b = graphic.getSymbol().b;
        symbol.a = graphic.getSymbol().a;
        symbol.c = graphic.getSymbol().c;
        Graphic graphic2 = new Graphic(geometry, symbol);
        if (b() == 0) {
            this.c.add(graphic2);
            return 0;
        }
        ArrayList<GeoPoint> arrayList = graphic.getGeometry().b;
        if (arrayList == null || arrayList.size() == 0) {
            return 0;
        }
        int i = graphic.getSymbol().a;
        if (graphic.getSymbol().b == null) {
            return 0;
        }
        float f = ((float) graphic.getSymbol().b.red) / 255.0f;
        float f2 = ((float) graphic.getSymbol().b.green) / 255.0f;
        float f3 = ((float) graphic.getSymbol().b.blue) / 255.0f;
        float f4 = ((float) graphic.getSymbol().b.alpha) / 255.0f;
        int i2 = graphic.getSymbol().c;
        int i3 = graphic.getGeometry().c;
        if (f < 0.0f || f > 255.0f || f2 < 0.0f || f2 > 255.0f || f3 < 0.0f || f3 > 255.0f || f4 < 0.0f || f4 > 255.0f) {
            return 0;
        }
        if (graphic.getGeometry().a == 2 || graphic.getGeometry().a == 5) {
            int size = arrayList.size();
            if (size < 2) {
                return 0;
            }
            if (i <= 0 && graphic.getGeometry().a == 2) {
                return 0;
            }
            int[] iArr = new int[size];
            int[] iArr2 = new int[size];
            for (int i4 = 0; i4 < size; i4++) {
                if (arrayList.get(i4) == null) {
                    return 0;
                }
                GeoPoint b2 = d.b(arrayList.get(i4));
                iArr[i4] = b2.getLongitudeE6();
                iArr2[i4] = b2.getLatitudeE6();
            }
            this.a.putIntArray("x", iArr);
            this.a.putIntArray("y", iArr2);
        } else if (graphic.getGeometry().a == 3) {
            if (arrayList.size() < 2 || arrayList.get(0) == null || arrayList.get(1) == null || i <= 0 || (i2 != 0 && i2 != 1)) {
                return 0;
            }
            int[] iArr3 = new int[5];
            int[] iArr4 = new int[5];
            GeoPoint b3 = d.b(arrayList.get(0));
            iArr3[0] = b3.getLongitudeE6();
            iArr4[0] = b3.getLatitudeE6();
            GeoPoint b4 = d.b(new GeoPoint(arrayList.get(0).getLatitudeE6(), arrayList.get(1).getLongitudeE6()));
            iArr3[1] = b4.getLongitudeE6();
            iArr4[1] = b4.getLatitudeE6();
            GeoPoint b5 = d.b(arrayList.get(1));
            iArr3[2] = b5.getLongitudeE6();
            iArr4[2] = b5.getLatitudeE6();
            GeoPoint b6 = d.b(new GeoPoint(arrayList.get(1).getLatitudeE6(), arrayList.get(0).getLongitudeE6()));
            iArr3[3] = b6.getLongitudeE6();
            iArr4[3] = b6.getLatitudeE6();
            iArr3[4] = iArr3[0];
            iArr4[4] = iArr4[0];
            this.a.putIntArray("x", iArr3);
            this.a.putIntArray("y", iArr4);
        } else if ((graphic.getGeometry().a != 4 && graphic.getGeometry().a != 1) || arrayList.get(0) == null) {
            return 0;
        } else {
            if ((i2 != 0 && i2 != 1) || i3 <= 0) {
                return 0;
            }
            GeoPoint b7 = d.b(arrayList.get(0));
            int[] iArr5 = {b7.getLongitudeE6()};
            int[] iArr6 = {b7.getLatitudeE6()};
            this.a.putIntArray("x", iArr5);
            this.a.putIntArray("y", iArr6);
        }
        this.a.putInt("linewidth", i);
        this.a.putFloat("red", f);
        this.a.putFloat("green", f2);
        this.a.putFloat("blue", f3);
        this.a.putFloat("alpha", f4);
        if (graphic.getGeometry().a == 5) {
            this.a.putInt("type", 2);
        } else {
            this.a.putInt("type", graphic.getGeometry().a);
        }
        if (graphic.getGeometry().a == 5) {
            this.a.putInt("status", 1);
        } else if (graphic.getGeometry().a == 2) {
            this.a.putInt("status", 0);
        } else {
            this.a.putInt("status", graphic.getSymbol().c);
        }
        if (graphic.getGeometry().a == 4 || graphic.getGeometry().a == 1) {
            this.a.putInt("level", i3);
        } else {
            this.a.putInt("level", (int) this.b.getController().a.l());
        }
        this.a.putInt("geometryaddr", b());
        long currentTimeMillis = System.currentTimeMillis();
        this.a.putString(LocaleUtil.INDONESIAN, String.valueOf(currentTimeMillis));
        this.b.getController().a.b().f(this.a);
        graphic.a(currentTimeMillis);
        graphic2.a(currentTimeMillis);
        this.c.add(graphic2);
        this.d = true;
        return currentTimeMillis;
    }
}
