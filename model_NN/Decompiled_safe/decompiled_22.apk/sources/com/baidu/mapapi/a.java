package com.baidu.mapapi;

import android.os.Handler;
import android.os.Message;

class a extends Handler {
    final /* synthetic */ BMapManager a;

    a(BMapManager bMapManager) {
        this.a = bMapManager;
    }

    public void handleMessage(Message message) {
        if (message.what == 2010) {
            if (message.arg2 == 4 || message.arg2 == 1) {
                this.a.g.a(this.a.e, this.a.f);
                if (!this.a.h && this.a.a != null) {
                    boolean unused = this.a.h = true;
                    this.a.a.onGetNetworkState(2);
                }
            } else if (message.arg2 != 2 && message.arg2 != 0 && this.a.a != null) {
                this.a.a.onGetPermissionState(300);
            }
        } else if (message.what != 65289) {
            if (message.arg2 == 3 && this.a.a != null) {
                this.a.a.onGetNetworkState(3);
            }
            if ((message.arg2 == 2 || message.arg2 == 404 || message.arg2 == 5) && this.a.a != null) {
                this.a.a.onGetNetworkState(2);
            }
        }
    }
}
