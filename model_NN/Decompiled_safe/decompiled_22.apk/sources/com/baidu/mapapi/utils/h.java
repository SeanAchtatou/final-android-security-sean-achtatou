package com.baidu.mapapi.utils;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import com.city_life.part_asynctask.UploadUtils;
import com.ibm.mqtt.MQeTrace;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class h {
    public static String[] a;
    public static String[] b;
    public static String[] c;
    public static int d = 0;
    private static String e = "";
    private static ArrayList<String> f = new ArrayList<>();
    private static ArrayList<String> g = new ArrayList<>();

    private static String a(String str) {
        StatFs statFs = new StatFs(str);
        long availableBlocks = ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        DecimalFormat decimalFormat = new DecimalFormat();
        if (availableBlocks < MQeTrace.GROUP_CHANNEL_MANAGEMENT) {
            return availableBlocks + "B";
        }
        if (availableBlocks < MQeTrace.GROUP_API) {
            decimalFormat.applyPattern(UploadUtils.SUCCESS);
            return decimalFormat.format(((double) availableBlocks) / 1024.0d) + "K";
        } else if (availableBlocks < 1073741824) {
            decimalFormat.applyPattern("0.0");
            return decimalFormat.format(((double) availableBlocks) / 1048576.0d) + "M";
        } else {
            decimalFormat.applyPattern("0.0");
            return decimalFormat.format(((double) availableBlocks) / 1.073741824E9d) + "G";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static void a() {
        f.add(e);
        try {
            Scanner scanner = new Scanner(new File("/proc/mounts"));
            while (scanner.hasNext()) {
                String nextLine = scanner.nextLine();
                if (nextLine.startsWith("/dev/block/vold/")) {
                    String str = nextLine.replace(9, ' ').split(" ")[1];
                    if (!str.equals(e)) {
                        f.add(str);
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void a(Context context) {
        e = Environment.getExternalStorageDirectory().getAbsolutePath();
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 14) {
            z = c(context);
        }
        if (!z) {
            a();
            b();
            c();
            d();
            b(context);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static void b() {
        g.add(e);
        File file = new File("/system/etc/vold.fstab");
        if (file.exists()) {
            try {
                Scanner scanner = new Scanner(file);
                while (scanner.hasNext()) {
                    String nextLine = scanner.nextLine();
                    if (nextLine.startsWith("dev_mount")) {
                        String str = nextLine.replace(9, ' ').split(" ")[2];
                        if (str.contains(":")) {
                            str = str.substring(0, str.indexOf(":"));
                        }
                        if (!str.equals(e)) {
                            g.add(str);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private static void b(Context context) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (f.size() > 0) {
            if (Build.VERSION.SDK_INT < 9) {
                arrayList.add("Auto");
            } else if (!Environment.isExternalStorageRemovable()) {
                arrayList.add("内置存储卡");
            } else {
                arrayList.add("外置存储卡");
            }
            arrayList2.add(a(f.get(0)));
            if (f.size() > 1) {
                for (int i = 1; i < f.size(); i++) {
                    arrayList.add("外置存储卡");
                    arrayList2.add(a(f.get(i)));
                }
            }
        }
        int i2 = 0;
        while (i2 < f.size()) {
            if (!b.a(f.get(i2))) {
                arrayList.remove(i2);
                arrayList2.remove(i2);
                f.remove(i2);
                i2--;
            }
            i2++;
        }
        a = new String[arrayList.size()];
        arrayList.toArray(a);
        b = new String[f.size()];
        f.toArray(b);
        c = new String[f.size()];
        arrayList2.toArray(c);
        d = Math.min(a.length, b.length);
        f.clear();
    }

    private static void c() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < f.size()) {
                if (!g.contains(f.get(i2))) {
                    f.remove(i2);
                    i2--;
                }
                i = i2 + 1;
            } else {
                g.clear();
                return;
            }
        }
    }

    private static boolean c(Context context) {
        StorageManager storageManager = (StorageManager) context.getSystemService("storage");
        if (storageManager != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            try {
                Class<?> cls = Class.forName("android.os.storage.StorageVolume");
                Method method = storageManager.getClass().getMethod("getVolumeList", new Class[0]);
                Method method2 = storageManager.getClass().getMethod("getVolumeState", String.class);
                Method method3 = cls.getMethod("isRemovable", new Class[0]);
                Method method4 = cls.getMethod("getPath", new Class[0]);
                Object[] objArr = (Object[]) method.invoke(storageManager, new Object[0]);
                for (int i = 0; i < objArr.length; i++) {
                    String str = (String) method4.invoke(objArr[i], new Object[0]);
                    boolean booleanValue = ((Boolean) method3.invoke(objArr[i], new Object[0])).booleanValue();
                    if (str != null && !str.equals(e)) {
                        String str2 = (String) method2.invoke(storageManager, str);
                        if (str2 != null && str2.equals("mounted")) {
                            if (booleanValue) {
                                arrayList2.add(str);
                            } else {
                                arrayList.add(str);
                            }
                        }
                    }
                }
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    f.add(arrayList.get(i2));
                    arrayList3.add("内置存储卡");
                    arrayList4.add(a((String) arrayList.get(i2)));
                }
                for (int i3 = 0; i3 < arrayList2.size(); i3++) {
                    f.add(arrayList2.get(i3));
                    arrayList3.add("外置存储卡");
                    arrayList4.add(a((String) arrayList2.get(i3)));
                }
                a = new String[arrayList3.size()];
                arrayList3.toArray(a);
                b = new String[f.size()];
                f.toArray(b);
                c = new String[f.size()];
                arrayList4.toArray(c);
                d = Math.min(a.length, b.length);
                f.clear();
                return true;
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
            } catch (NoSuchMethodException e3) {
                e3.printStackTrace();
            } catch (IllegalArgumentException e4) {
                e4.printStackTrace();
            } catch (IllegalAccessException e5) {
                e5.printStackTrace();
            } catch (InvocationTargetException e6) {
                e6.printStackTrace();
            }
        }
        return false;
    }

    private static void d() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < f.size()) {
                File file = new File(f.get(i2));
                if (!file.exists() || !file.isDirectory() || !file.canWrite()) {
                    f.remove(i2);
                    i2--;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
