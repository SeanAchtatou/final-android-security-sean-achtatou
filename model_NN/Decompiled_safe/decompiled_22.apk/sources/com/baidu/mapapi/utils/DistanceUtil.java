package com.baidu.mapapi.utils;

import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.baidu.platform.comapi.basestruct.c;
import com.baidu.platform.comjni.tools.a;

public class DistanceUtil {
    public static double getDistance(GeoPoint geoPoint, GeoPoint geoPoint2) {
        GeoPoint b = d.b(geoPoint);
        GeoPoint b2 = d.b(geoPoint2);
        if (b == null || b2 == null) {
            return 0.0d;
        }
        return a.a(new c(b.getLongitudeE6(), b.getLatitudeE6()), new c(b2.getLongitudeE6(), b2.getLatitudeE6()));
    }
}
