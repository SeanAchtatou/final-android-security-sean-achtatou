package com.baidu.android.pushservice.util;

import com.tencent.mm.sdk.platformtools.Util;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.InputStream;
import java.math.BigInteger;

public class a {
    byte[] a = new byte[8];
    private DataInputStream b;

    public a(InputStream inputStream) {
        this.b = new DataInputStream(inputStream);
    }

    private int a(int i) {
        int i2 = 0;
        while (i2 < i) {
            int read = this.b.read(this.a, i2, i - i2);
            if (read == -1) {
                return read;
            }
            i2 += read;
        }
        return i2;
    }

    public final int a() {
        if (a(4) >= 0) {
            return ((this.a[3] & 255) << 24) | ((this.a[2] & 255) << 16) | ((this.a[1] & 255) << 8) | (this.a[0] & 255);
        }
        throw new EOFException();
    }

    public final void a(byte[] bArr) {
        this.b.readFully(bArr, 0, bArr.length);
    }

    public final short b() {
        if (a(2) >= 0) {
            return (short) (((this.a[1] & 255) << 8) | (this.a[0] & 255));
        }
        throw new EOFException();
    }

    public final b c() {
        if (a(8) < 0) {
            throw new EOFException();
        }
        byte[] bArr = new byte[8];
        for (int i = 0; i < 8; i++) {
            bArr[i] = this.a[7 - i];
        }
        String bigInteger = new BigInteger(1, bArr).toString();
        byte b2 = ((this.a[7] & 255) << 24) | ((this.a[6] & 255) << 16) | ((this.a[5] & 255) << 8) | (this.a[4] & 255);
        byte b3 = ((this.a[3] & 255) << 24) | ((this.a[2] & 255) << 16) | ((this.a[1] & 255) << 8);
        b bVar = new b();
        bVar.a = bigInteger;
        bVar.b = ((((long) b2) & Util.MAX_32BIT_VALUE) << 32) | (((long) ((this.a[0] & 255) | b3)) & Util.MAX_32BIT_VALUE);
        return bVar;
    }
}
