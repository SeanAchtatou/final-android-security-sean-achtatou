package com.baidu.android.pushservice;

import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.jni.PushSocket;
import com.baidu.android.pushservice.message.c;
import com.baidu.android.pushservice.util.n;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

class f implements Runnable {
    final /* synthetic */ e a;

    f(e eVar) {
        this.a = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, boolean):boolean
     arg types: [com.baidu.android.pushservice.e, int]
     candidates:
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, int):int
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, com.baidu.android.pushservice.i):com.baidu.android.pushservice.i
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, com.baidu.android.pushservice.j):com.baidu.android.pushservice.j
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, java.io.InputStream):java.io.InputStream
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, java.io.OutputStream):java.io.OutputStream
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, java.net.Socket):java.net.Socket
      com.baidu.android.pushservice.e.a(com.baidu.android.pushservice.e, boolean):boolean */
    public void run() {
        if (PushSocket.a) {
            e.a = PushSocket.createSocket(w.b, w.c);
            if (e.a == -1) {
                Log.e("PushConnection", "Create socket err, errno:" + PushSocket.getLastSocketError());
                Boolean unused = e.e = (Boolean) false;
                this.a.f();
                return;
            }
        } else {
            try {
                Socket unused2 = this.a.g = new Socket(w.b, w.c);
                InputStream unused3 = this.a.h = this.a.g.getInputStream();
                OutputStream unused4 = this.a.i = this.a.g.getOutputStream();
            } catch (Throwable th) {
                Log.e("PushConnection", "Connecting exception: " + th);
                this.a.f();
                return;
            }
        }
        if (b.a()) {
            Log.i("PushConnection", "create Socket ok");
            n.a("create Socket ok socketfd" + e.a);
        }
        if (PushSocket.a) {
            this.a.c = new c(this.a.o.getApplicationContext(), this.a);
        } else {
            this.a.c = new c(this.a.o.getApplicationContext(), this.a, this.a.h, this.a.i);
        }
        boolean unused5 = this.a.d = true;
        this.a.a(true);
        if (this.a.l != null) {
            this.a.l.interrupt();
        }
        if (this.a.k != null) {
            this.a.k.interrupt();
        }
        boolean unused6 = this.a.f = false;
        i unused7 = this.a.l = new i(this.a);
        this.a.l.start();
        j unused8 = this.a.k = new j(this.a);
        this.a.k.start();
        if (PushSocket.a) {
            this.a.c.a(e.a);
        } else {
            this.a.c.a();
        }
        Boolean unused9 = e.e = (Boolean) false;
    }
}
