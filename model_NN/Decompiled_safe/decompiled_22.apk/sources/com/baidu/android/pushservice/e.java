package com.baidu.android.pushservice;

import android.content.Context;
import android.os.Handler;
import android.provider.Settings;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.jni.PushSocket;
import com.baidu.android.pushservice.message.a;
import com.baidu.android.pushservice.message.b;
import com.baidu.android.pushservice.util.n;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.LinkedList;

public final class e {
    static int a = -1;
    /* access modifiers changed from: private */
    public static Boolean e = false;
    private static volatile e p;
    private final int A = 7200;
    Handler b = new Handler();
    a c;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public Socket g;
    /* access modifiers changed from: private */
    public InputStream h;
    /* access modifiers changed from: private */
    public OutputStream i;
    /* access modifiers changed from: private */
    public LinkedList j = new LinkedList();
    /* access modifiers changed from: private */
    public j k;
    /* access modifiers changed from: private */
    public i l;
    private boolean m = false;
    /* access modifiers changed from: private */
    public int n = 0;
    /* access modifiers changed from: private */
    public Context o;
    private HashSet q = new HashSet();
    /* access modifiers changed from: private */
    public boolean r;
    private Runnable s = new g(this);
    /* access modifiers changed from: private */
    public Runnable t = new h(this);
    private long u = 0;
    private int[] v = {300, 450, 600};
    private int[] w = {0, 0, 0};
    private final int x = 2;
    private int y = 2;
    private int z = 0;

    private e(Context context) {
        this.o = context;
        h();
    }

    public static e a(Context context) {
        if (p == null) {
            p = new e(context);
        }
        return p;
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        int i2 = this.y;
        if (z2) {
            this.z++;
            this.w[this.y] = 0;
            if (this.v[this.y] * this.z >= 7200) {
                this.z = 0;
                if (this.y < this.v.length - 1 && this.w[this.y + 1] < 2) {
                    this.y++;
                    PushSDK.getInstantce(this.o).setAlarmTimeout(this.v[this.y]);
                }
            }
        } else {
            this.z = 0;
            int[] iArr = this.w;
            int i3 = this.y;
            iArr[i3] = iArr[i3] + 1;
            if (this.y > 0) {
                this.y--;
                PushSDK.getInstantce(this.o).setAlarmTimeout(this.v[this.y]);
            }
        }
        if (PushSettings.c()) {
            Log.d("PushConnection", "RTC stat update from " + this.v[i2] + " to " + this.v[this.y]);
            n.a("RTC stat update from " + this.v[i2] + " to " + this.v[this.y]);
        }
    }

    /* access modifiers changed from: private */
    public synchronized void e() {
        if (this.d || e.booleanValue()) {
            if (b.a()) {
                Log.i("PushConnection", "Connect return. mConnected:" + this.d + " mConnectting:" + e);
            }
        } else if (!y.a().e()) {
            if (b.a()) {
                Log.d("PushConnection", "re-token");
            }
            PushSDK.getInstantce(this.o).sendRequestTokenIntent();
        } else {
            e = true;
            a = -1;
            Thread thread = new Thread(new f(this));
            thread.setName("PushService-PushService-connect");
            thread.start();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (b.a()) {
            Log.i("PushConnection", "disconnectedByPeer, mStoped == " + this.m);
            n.a("disconnectedByPeer, mStoped == " + this.m);
        }
        if (!this.m) {
            g();
            this.n++;
            if (this.n < 3) {
                int i2 = (this.n - 1) * 30 * 1000;
                if (this.n == 1) {
                    i2 = 0;
                }
                this.b.postDelayed(this.s, (long) i2);
                if (b.a()) {
                    Log.i("PushConnection", "Schedule retry-- retry times: " + this.n + " time delay: " + i2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (b.a()) {
            Log.i("PushConnection", "disconnected");
        }
        this.b.removeCallbacks(this.t);
        this.f = true;
        this.d = false;
        a(false);
        synchronized (this.j) {
            this.j.notifyAll();
        }
        try {
            if (this.g != null) {
                this.g.close();
                this.g = null;
            }
            if (this.h != null) {
                this.h.close();
                this.h = null;
            }
            if (this.i != null) {
                this.i.close();
                this.i = null;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (PushSocket.a) {
            PushSocket.closeSocket(a);
        }
        if (this.c != null) {
            this.c.c();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x008e A[Catch:{ all -> 0x00af }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0099 A[SYNTHETIC, Splitter:B:37:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a6 A[SYNTHETIC, Splitter:B:44:0x00a6] */
    /* JADX WARNING: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void h() {
        /*
            r6 = this;
            r0 = 0
            java.io.File r3 = new java.io.File
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r2 = "baidu/pushservice/pushservice.cfg"
            r3.<init>(r1, r2)
            boolean r1 = r3.exists()
            if (r1 == 0) goto L_0x0080
            java.util.Properties r4 = new java.util.Properties
            r4.<init>()
            r2 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0086, all -> 0x00a2 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0086, all -> 0x00a2 }
            r4.load(r1)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r2 = "rtcseed1"
            java.lang.String r2 = r4.getProperty(r2)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r3 = "PushConnection"
            com.baidu.android.common.logging.Log.e(r3, r2)     // Catch:{ Exception -> 0x00b1 }
            if (r2 == 0) goto L_0x00b5
            int r3 = r2.length()     // Catch:{ Exception -> 0x00b1 }
            if (r3 <= 0) goto L_0x00b5
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x00b1 }
            r3 = r2
        L_0x0038:
            java.lang.String r2 = "rtcseed2"
            java.lang.String r2 = r4.getProperty(r2)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r5 = "PushConnection"
            com.baidu.android.common.logging.Log.e(r5, r2)     // Catch:{ Exception -> 0x00b1 }
            if (r2 == 0) goto L_0x00b3
            int r5 = r2.length()     // Catch:{ Exception -> 0x00b1 }
            if (r5 <= 0) goto L_0x00b3
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x00b1 }
        L_0x004f:
            java.lang.String r5 = "rtcseed3"
            java.lang.String r4 = r4.getProperty(r5)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r5 = "PushConnection"
            com.baidu.android.common.logging.Log.e(r5, r4)     // Catch:{ Exception -> 0x00b1 }
            if (r4 == 0) goto L_0x0066
            int r5 = r4.length()     // Catch:{ Exception -> 0x00b1 }
            if (r5 <= 0) goto L_0x0066
            int r0 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x00b1 }
        L_0x0066:
            if (r3 <= 0) goto L_0x007b
            if (r2 <= 0) goto L_0x007b
            if (r0 <= 0) goto L_0x007b
            int[] r4 = r6.v     // Catch:{ Exception -> 0x00b1 }
            r5 = 0
            r4[r5] = r3     // Catch:{ Exception -> 0x00b1 }
            int[] r3 = r6.v     // Catch:{ Exception -> 0x00b1 }
            r4 = 1
            r3[r4] = r2     // Catch:{ Exception -> 0x00b1 }
            int[] r2 = r6.v     // Catch:{ Exception -> 0x00b1 }
            r3 = 2
            r2[r3] = r0     // Catch:{ Exception -> 0x00b1 }
        L_0x007b:
            if (r1 == 0) goto L_0x0080
            r1.close()     // Catch:{ IOException -> 0x0081 }
        L_0x0080:
            return
        L_0x0081:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0080
        L_0x0086:
            r0 = move-exception
            r1 = r2
        L_0x0088:
            boolean r2 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x00af }
            if (r2 == 0) goto L_0x0097
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ all -> 0x00af }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00af }
            r2.println(r0)     // Catch:{ all -> 0x00af }
        L_0x0097:
            if (r1 == 0) goto L_0x0080
            r1.close()     // Catch:{ IOException -> 0x009d }
            goto L_0x0080
        L_0x009d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0080
        L_0x00a2:
            r0 = move-exception
            r1 = r2
        L_0x00a4:
            if (r1 == 0) goto L_0x00a9
            r1.close()     // Catch:{ IOException -> 0x00aa }
        L_0x00a9:
            throw r0
        L_0x00aa:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a9
        L_0x00af:
            r0 = move-exception
            goto L_0x00a4
        L_0x00b1:
            r0 = move-exception
            goto L_0x0088
        L_0x00b3:
            r2 = r0
            goto L_0x004f
        L_0x00b5:
            r3 = r0
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.e.h():void");
    }

    public void a(b bVar) {
        synchronized (this.j) {
            this.j.add(bVar);
            this.j.notify();
        }
    }

    public void a(boolean z2) {
        if (this.o == null) {
            Log.e("PushConnection", "setConnectState, mContext == null");
            return;
        }
        int i2 = 0;
        if (z2) {
            i2 = 1;
        }
        Settings.System.putInt(this.o.getContentResolver(), "com.baidu.pushservice.PushSettings.connect_state", i2);
    }

    public boolean a() {
        return this.d;
    }

    public boolean a(String str) {
        boolean z2 = false;
        if (this.q.contains(str)) {
            this.q.remove(str);
            z2 = true;
        }
        if (this.q.size() >= 100) {
            this.q.clear();
        }
        this.q.add(str);
        return z2;
    }

    public void b() {
        this.n = 0;
        this.m = false;
        e();
    }

    public void c() {
        if (b.a()) {
            Log.i("PushConnection", "---stop---");
        }
        this.f = true;
        this.m = true;
        this.b.removeCallbacks(this.s);
        g();
    }

    public void d() {
        if (this.c != null) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.u > 180000) {
                this.c.d();
                this.u = currentTimeMillis;
                if (b.a()) {
                    Log.i("PushConnection", "sendHeartbeatMessage");
                }
            } else if (b.a()) {
                Log.i("PushConnection", "sendHeartbeatMessage ingnored， because too frequent.");
            }
        }
    }
}
