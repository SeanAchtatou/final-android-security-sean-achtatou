package com.baidu.android.pushservice;

import android.app.PendingIntent;
import android.content.Intent;
import android.view.View;
import com.city_life.part_asynctask.UploadUtils;

class u implements View.OnClickListener {
    final /* synthetic */ PushTestActivity a;

    u(PushTestActivity pushTestActivity) {
        this.a = pushTestActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(PushConstants.ACTION_METHOD);
        intent.putExtra("method", PushConstants.METHOD_DELETE);
        intent.putExtra(PushConstants.EXTRA_APP, PendingIntent.getBroadcast(this.a, 0, new Intent(), 0));
        intent.putExtra("access_token", PushConstants.rsaEncrypt(this.a.a));
        intent.putExtra(PushConstants.EXTRA_MSG_IDS, new String[]{UploadUtils.FAILURE, "2"});
        this.a.sendBroadcast(intent);
    }
}
