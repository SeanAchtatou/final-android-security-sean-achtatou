package com.baidu.android.pushservice;

import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.jni.PushSocket;
import com.baidu.android.pushservice.message.b;
import com.baidu.android.pushservice.message.d;
import com.baidu.android.pushservice.util.n;

class i extends Thread {
    final /* synthetic */ e a;

    i(e eVar) {
        this.a = eVar;
        setName("PushService-PushConnection-readThread");
    }

    public void run() {
        byte[] bArr;
        while (!this.a.f) {
            if (PushSocket.a) {
                try {
                    bArr = PushSocket.a(e.a, this.a.c);
                } catch (Exception e) {
                    bArr = null;
                    Log.e("PushConnection", "Get message exception");
                }
                this.a.b.removeCallbacks(this.a.t);
                if (this.a.r) {
                    boolean unused = this.a.r = false;
                    this.a.b(true);
                }
                if (bArr == null || bArr.length == 0) {
                    Log.i("PushConnection", "Receive err,errno:" + PushSocket.getLastSocketError());
                    this.a.f();
                } else {
                    try {
                        b a2 = this.a.c.a(bArr, bArr.length);
                        if (a2 != null) {
                            try {
                                if (b.a()) {
                                    Log.d("PushConnection", "ReadThread receive msg :" + a2.toString());
                                }
                                this.a.c.b(a2);
                            } catch (Exception e2) {
                                Log.e("PushConnection", "Handle message exception " + n.a(e2));
                                this.a.f();
                            }
                        }
                        int unused2 = this.a.n = 0;
                    } catch (Exception e3) {
                        Log.i("PushConnection", "Read message exception " + n.a(e3));
                        this.a.f();
                    }
                }
            } else {
                try {
                    b b = this.a.c.b();
                    this.a.b.removeCallbacks(this.a.t);
                    if (this.a.r) {
                        boolean unused3 = this.a.r = false;
                        this.a.b(true);
                    }
                    if (b != null) {
                        if (b.a()) {
                            Log.d("PushConnection", "ReadThread receive msg :" + b.toString());
                        }
                        try {
                            this.a.c.b(b);
                            int unused4 = this.a.n = 0;
                        } catch (d e4) {
                            Log.e("PushConnection", "handleMessage exception.");
                            Log.e("PushConnection", e4);
                            this.a.f();
                        }
                    }
                } catch (Exception e5) {
                    Log.e("PushConnection", "ReadThread exception: " + e5);
                    this.a.f();
                }
            }
        }
    }
}
