package com.baidu.android.pushservice.richmedia;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;

class e implements AdapterView.OnItemLongClickListener {
    final /* synthetic */ MediaListActivity a;

    e(MediaListActivity mediaListActivity) {
        this.a = mediaListActivity;
    }

    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        new AlertDialog.Builder(this.a).setTitle("提示").setMessage("确定要删除该记录？").setPositiveButton("确定", new g(this, j)).setNegativeButton("取消", new f(this)).show();
        return true;
    }
}
