package com.baidu.android.pushservice.a;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.net.ConnectManager;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.b;
import com.baidu.android.pushservice.w;
import com.baidu.android.pushservice.y;
import com.tencent.tauth.Constants;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class a implements Runnable {
    protected Context a;
    protected l b;
    protected String c;
    protected boolean d = true;

    public a(l lVar, Context context) {
        this.b = lVar;
        this.a = context.getApplicationContext();
        this.c = w.f;
    }

    private void b(int i, byte[] bArr) {
        Intent intent = new Intent("com.baidu.android.pushservice.action.internal.RECEIVE");
        intent.putExtra("method", this.b.a);
        intent.putExtra(PushConstants.EXTRA_ERROR_CODE, i);
        intent.putExtra("content", bArr);
        intent.putExtra(Constants.PARAM_APP_ID, this.b.f);
        intent.setFlags(32);
        a(intent);
        if (b.a()) {
            Log.d("BaseBaseApiProcessor", "> sendInternalMethodResult  ,method:" + this.b.a + " ,errorCode : " + i + " ,content : " + new String(bArr));
        }
        this.a.sendBroadcast(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.y.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.android.pushservice.y.a(java.lang.String, java.lang.String):void
      com.baidu.android.pushservice.y.a(android.content.Context, boolean):void */
    /* access modifiers changed from: protected */
    public void a() {
        if (this.b != null && !TextUtils.isEmpty(this.b.a)) {
            if (!this.b.a.equals("com.baidu.android.pushservice.action.UNBIND") && TextUtils.isEmpty(this.b.e)) {
                return;
            }
            if (!ConnectManager.isNetworkConnected(this.a)) {
                if (b.a()) {
                    Log.e("BaseBaseApiProcessor", "Network is not useful!");
                }
                a(10001);
                b.a(this.a);
                if (b.a()) {
                    Log.i("BaseBaseApiProcessor", "startPushService BaseApiProcess");
                    return;
                }
                return;
            }
            y a2 = y.a();
            synchronized (a2) {
                if (this.d && !a2.e()) {
                    a2.a(this.a, false);
                    this.d = false;
                    while (!a2.b()) {
                        try {
                            a2.wait();
                        } catch (InterruptedException e) {
                            if (b.a()) {
                                Log.e("BaseBaseApiProcessor", e.getMessage());
                            }
                        }
                    }
                }
            }
            if (!y.a().e()) {
                a(10002);
                return;
            }
            boolean b2 = b();
            if (b.a()) {
                Log.i("BaseBaseApiProcessor", "netWorkConnect connectResult: " + b2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        a(i, PushConstants.getErrorMsg(i).getBytes());
    }

    /* access modifiers changed from: protected */
    public void a(int i, byte[] bArr) {
        if (TextUtils.isEmpty(this.b.b) || !this.b.b.equals("internal")) {
            Intent intent = new Intent(PushConstants.ACTION_RECEIVE);
            intent.putExtra("method", this.b.a);
            intent.putExtra(PushConstants.EXTRA_ERROR_CODE, i);
            intent.putExtra("content", bArr);
            intent.setFlags(32);
            a(intent);
            if (!TextUtils.isEmpty(this.b.e)) {
                intent.setPackage(this.b.e);
                if (b.a()) {
                    Log.d("BaseBaseApiProcessor", "> sendResult to " + this.b.e + " ,method:" + this.b.a + " ,errorCode : " + i + " ,content : " + new String(bArr));
                }
                this.a.sendBroadcast(intent);
                return;
            }
            return;
        }
        b(i, bArr);
    }

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (str != null) {
            if (!str.startsWith("{\"")) {
                str = str.substring(str.indexOf("{\""));
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                int i = jSONObject.getInt("error_code");
                String string = jSONObject.getString(PushConstants.EXTRA_ERROR_CODE);
                String string2 = jSONObject.getString("request_id");
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put(PushConstants.EXTRA_ERROR_CODE, string);
                jSONObject2.put("requestId", string2);
                a(i, jSONObject2.toString().getBytes());
            } catch (JSONException e) {
                Log.e("BaseBaseApiProcessor", e.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(List list) {
        b.a(list);
        if (!TextUtils.isEmpty(this.b.h)) {
            list.add(new BasicNameValuePair("rsa_bduss", this.b.h));
            list.add(new BasicNameValuePair(Constants.PARAM_APP_ID, this.b.f));
        } else if (!TextUtils.isEmpty(this.b.d)) {
            list.add(new BasicNameValuePair("rsa_access_token", this.b.d));
        } else {
            list.add(new BasicNameValuePair("apikey", this.b.i));
        }
    }

    /* access modifiers changed from: protected */
    public String b(String str) {
        return str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0102, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0107, code lost:
        if (com.baidu.android.pushservice.b.a() != false) goto L_0x0109;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0109, code lost:
        com.baidu.android.common.logging.Log.e("BaseBaseApiProcessor", r0.getMessage());
        com.baidu.android.common.logging.Log.i("BaseBaseApiProcessor", "io exception do something ? ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0119, code lost:
        a(10002);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x011e, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x012b, code lost:
        com.baidu.android.common.logging.Log.e("BaseBaseApiProcessor", r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0136, code lost:
        a(10003);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        a((int) com.baidu.android.pushservice.PushConstants.ERROR_UNKNOWN);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0146, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0147, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x014a, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x014b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0102 A[ExcHandler: IOException (r0v7 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:9:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x012b A[Catch:{ IOException -> 0x0102, Exception -> 0x014b, all -> 0x0146 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0136 A[Catch:{ IOException -> 0x0102, Exception -> 0x014b, all -> 0x0146 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0140  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b() {
        /*
            r7 = this;
            r0 = 1
            r1 = 0
            java.lang.String r2 = "BaseBaseApiProcessor"
            java.lang.String r3 = "networkConnect"
            com.baidu.android.common.logging.Log.i(r2, r3)
            java.lang.String r2 = r7.c
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 == 0) goto L_0x001f
            boolean r0 = com.baidu.android.pushservice.b.a()
            if (r0 == 0) goto L_0x001e
            java.lang.String r0 = "BaseBaseApiProcessor"
            java.lang.String r2 = "mUrl is null"
            com.baidu.android.common.logging.Log.e(r0, r2)
        L_0x001e:
            return r1
        L_0x001f:
            boolean r2 = com.baidu.android.pushservice.b.a()
            if (r2 == 0) goto L_0x003f
            java.lang.String r2 = "BaseBaseApiProcessor"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Request Url = "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r7.c
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            com.baidu.android.common.logging.Log.d(r2, r3)
        L_0x003f:
            com.baidu.android.common.net.ProxyHttpClient r3 = new com.baidu.android.common.net.ProxyHttpClient
            android.content.Context r2 = r7.a
            r3.<init>(r2)
            org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.String r4 = r7.c     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r2.<init>(r4)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.String r4 = "Content-Type"
            java.lang.String r5 = "application/x-www-form-urlencoded"
            r2.addHeader(r4, r5)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r4.<init>()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r7.a(r4)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            org.apache.http.client.entity.UrlEncodedFormEntity r5 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.String r6 = "UTF-8"
            r5.<init>(r4, r6)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r2.setEntity(r5)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            org.apache.http.HttpResponse r4 = r3.execute(r2)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            org.apache.http.StatusLine r2 = r4.getStatusLine()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            int r2 = r2.getStatusCode()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r2 != r5) goto L_0x00ae
            org.apache.http.HttpEntity r2 = r4.getEntity()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.String r2 = org.apache.http.util.EntityUtils.toString(r2)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            boolean r4 = com.baidu.android.pushservice.b.a()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            if (r4 == 0) goto L_0x009c
            java.lang.String r4 = "BaseBaseApiProcessor"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r5.<init>()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.String r6 = "<<< networkRegister return string :  "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            com.baidu.android.common.logging.Log.i(r4, r5)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
        L_0x009c:
            java.lang.String r2 = r7.b(r2)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r4 = 0
            byte[] r2 = r2.getBytes()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r7.a(r4, r2)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
        L_0x00a8:
            r3.close()
            r1 = r0
            goto L_0x001e
        L_0x00ae:
            java.lang.String r2 = "BaseBaseApiProcessor"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r5.<init>()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.String r6 = "networkRegister request failed  "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            org.apache.http.StatusLine r6 = r4.getStatusLine()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            com.baidu.android.common.logging.Log.i(r2, r5)     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            org.apache.http.StatusLine r2 = r4.getStatusLine()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            int r2 = r2.getStatusCode()     // Catch:{ IOException -> 0x0102, Exception -> 0x0123 }
            r5 = 503(0x1f7, float:7.05E-43)
            if (r2 != r5) goto L_0x014d
            r2 = r0
        L_0x00d7:
            org.apache.http.HttpEntity r0 = r4.getEntity()     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
            java.lang.String r0 = org.apache.http.util.EntityUtils.toString(r0)     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
            boolean r4 = com.baidu.android.pushservice.b.a()     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
            if (r4 == 0) goto L_0x00fd
            java.lang.String r4 = "BaseBaseApiProcessor"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
            r5.<init>()     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
            java.lang.String r6 = "<<< networkRegister return string :  "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
            com.baidu.android.common.logging.Log.i(r4, r5)     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
        L_0x00fd:
            r7.a(r0)     // Catch:{ IOException -> 0x0102, Exception -> 0x014b }
            r0 = r1
            goto L_0x00a8
        L_0x0102:
            r0 = move-exception
            boolean r2 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x0146 }
            if (r2 == 0) goto L_0x0119
            java.lang.String r2 = "BaseBaseApiProcessor"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0146 }
            com.baidu.android.common.logging.Log.e(r2, r0)     // Catch:{ all -> 0x0146 }
            java.lang.String r0 = "BaseBaseApiProcessor"
            java.lang.String r2 = "io exception do something ? "
            com.baidu.android.common.logging.Log.i(r0, r2)     // Catch:{ all -> 0x0146 }
        L_0x0119:
            r0 = 10002(0x2712, float:1.4016E-41)
            r7.a(r0)     // Catch:{ all -> 0x0146 }
            r3.close()
            goto L_0x001e
        L_0x0123:
            r0 = move-exception
            r2 = r1
        L_0x0125:
            boolean r4 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x0146 }
            if (r4 == 0) goto L_0x0134
            java.lang.String r4 = "BaseBaseApiProcessor"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0146 }
            com.baidu.android.common.logging.Log.e(r4, r0)     // Catch:{ all -> 0x0146 }
        L_0x0134:
            if (r2 == 0) goto L_0x0140
            r0 = 10003(0x2713, float:1.4017E-41)
            r7.a(r0)     // Catch:{ all -> 0x0146 }
        L_0x013b:
            r3.close()
            goto L_0x001e
        L_0x0140:
            r0 = 20001(0x4e21, float:2.8027E-41)
            r7.a(r0)     // Catch:{ all -> 0x0146 }
            goto L_0x013b
        L_0x0146:
            r0 = move-exception
            r3.close()
            throw r0
        L_0x014b:
            r0 = move-exception
            goto L_0x0125
        L_0x014d:
            r2 = r1
            goto L_0x00d7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.a.a.b():boolean");
    }

    public void run() {
        a();
    }
}
