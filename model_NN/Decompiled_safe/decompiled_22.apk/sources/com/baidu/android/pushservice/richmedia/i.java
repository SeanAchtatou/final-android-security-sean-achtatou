package com.baidu.android.pushservice.richmedia;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.tauth.Constants;
import java.util.ArrayList;
import java.util.HashMap;

public class i extends BaseAdapter {
    final /* synthetic */ MediaListActivity a;
    private Context b;
    private ArrayList c;

    public i(MediaListActivity mediaListActivity, Context context, ArrayList arrayList) {
        this.a = mediaListActivity;
        this.b = context;
        this.c = arrayList;
    }

    public int getCount() {
        return this.c.size();
    }

    public Object getItem(int i) {
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(this.b.getApplicationContext()).inflate(this.a.e, (ViewGroup) null);
        inflate.setBackgroundColor(-7829368);
        ((TextView) inflate.findViewById(this.a.h)).setText(((HashMap) this.c.get(i)).get(Constants.PARAM_TITLE).toString());
        ((TextView) inflate.findViewById(this.a.i)).setText(((HashMap) this.c.get(i)).get("fromtext").toString());
        ((TextView) inflate.findViewById(this.a.j)).setText(((HashMap) this.c.get(i)).get("timetext").toString());
        ((ImageView) inflate.findViewById(this.a.g)).setImageDrawable((Drawable) ((HashMap) this.c.get(i)).get(Constants.PARAM_IMG_URL));
        return inflate;
    }
}
