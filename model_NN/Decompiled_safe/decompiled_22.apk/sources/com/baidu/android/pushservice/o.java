package com.baidu.android.pushservice;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.baidu.android.pushservice.richmedia.MediaViewActivity;
import com.baidu.android.pushservice.richmedia.b;
import com.baidu.android.pushservice.richmedia.m;
import com.baidu.android.pushservice.richmedia.r;
import com.baidu.android.pushservice.richmedia.s;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.util.i;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.File;
import org.apache.commons.httpclient.cookie.CookieSpec;

class o implements s {
    public Context a = null;
    public RemoteViews b = null;
    public int c = 0;
    public int d = 0;
    public int e = 0;
    public int f = 0;
    NotificationManager g;
    final /* synthetic */ PushServiceReceiver h;

    o(PushServiceReceiver pushServiceReceiver, Context context) {
        this.h = pushServiceReceiver;
        this.a = context;
        this.g = (NotificationManager) context.getSystemService("notification");
    }

    public void a(b bVar) {
        Resources resources = this.a.getResources();
        String packageName = this.a.getPackageName();
        if (resources != null) {
            int identifier = resources.getIdentifier("bpush_download_progress", "layout", packageName);
            this.b = new RemoteViews(this.a.getPackageName(), identifier);
            if (identifier != 0) {
                this.c = resources.getIdentifier("bpush_download_progress", LocaleUtil.INDONESIAN, packageName);
                this.d = resources.getIdentifier("bpush_progress_percent", LocaleUtil.INDONESIAN, packageName);
                this.e = resources.getIdentifier("bpush_progress_text", LocaleUtil.INDONESIAN, packageName);
                this.f = resources.getIdentifier("bpush_download_icon", LocaleUtil.INDONESIAN, packageName);
                this.b.setImageViewResource(this.f, this.a.getApplicationInfo().icon);
            }
        }
    }

    public void a(b bVar, m mVar) {
        String d2 = bVar.d.d();
        if (mVar.a != mVar.b && this.b != null) {
            int i = (int) ((((double) mVar.a) * 100.0d) / ((double) mVar.b));
            this.b.setTextViewText(this.d, i + "%");
            this.b.setTextViewText(this.e, "正在下载富媒体:" + d2);
            this.b.setProgressBar(this.c, 100, i, false);
            Notification notification = new Notification(17301633, null, System.currentTimeMillis());
            notification.contentView = this.b;
            notification.contentIntent = PendingIntent.getActivity(this.a, 0, new Intent(), 0);
            notification.flags |= 32;
            notification.flags |= 2;
            this.g.notify(d2, 0, notification);
        }
    }

    public void a(b bVar, r rVar) {
        String d2 = bVar.d.d();
        this.g.cancel(d2, 0);
        i a2 = e.a(e.a(this.a), d2);
        if (a2 != null && a2.i == b.f) {
            String str = a2.e;
            String str2 = a2.f;
            String str3 = str + CookieSpec.PATH_DELIM + str2.substring(0, str2.lastIndexOf(".")) + "/index.html";
            Intent intent = new Intent();
            intent.setClass(this.a, MediaViewActivity.class);
            intent.setData(Uri.fromFile(new File(str3)));
            intent.addFlags(268435456);
            this.a.startActivity(intent);
        }
    }

    public void a(b bVar, Throwable th) {
        String d2 = bVar.d.d();
        this.g.cancel(d2, 0);
        Toast makeText = Toast.makeText(this.a, "下载富媒体" + Uri.parse(d2).getAuthority() + "失败", 1);
        makeText.setGravity(17, 0, 0);
        makeText.show();
    }

    public void b(b bVar) {
        this.g.cancel(bVar.d.d(), 0);
    }
}
