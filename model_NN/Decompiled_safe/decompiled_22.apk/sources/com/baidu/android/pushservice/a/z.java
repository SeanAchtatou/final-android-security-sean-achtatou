package com.baidu.android.pushservice.a;

import android.content.Context;
import com.baidu.android.common.logging.Log;
import com.baidu.android.pushservice.b;
import com.tencent.tauth.Constants;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class z extends d {
    public z(l lVar, Context context) {
        super(lVar, context);
    }

    /* access modifiers changed from: protected */
    public void a(List list) {
        b.a(list);
        list.add(new BasicNameValuePair("method", "unbindapp"));
        list.add(new BasicNameValuePair(Constants.PARAM_APP_ID, this.b.f));
        if (b.a()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                Log.d("UnbindApp", "UNBINDAPP param -- " + ((NameValuePair) it.next()).toString());
            }
        }
    }
}
