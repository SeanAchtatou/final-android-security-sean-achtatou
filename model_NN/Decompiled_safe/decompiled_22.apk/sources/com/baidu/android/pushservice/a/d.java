package com.baidu.android.pushservice.a;

import android.content.Context;

public class d extends c {
    protected boolean e = false;

    public d(l lVar, Context context) {
        super(lVar, context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.android.pushservice.b.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.android.pushservice.b.a(android.content.Context, android.content.Intent):void
      com.baidu.android.pushservice.b.a(android.content.Context, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x011d A[LOOP:0: B:19:0x0117->B:21:0x011d, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String b(java.lang.String r7) {
        /*
            r6 = this;
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0096 }
            r0.<init>(r7)     // Catch:{ JSONException -> 0x0096 }
            java.lang.String r1 = "response_params"
            org.json.JSONObject r1 = r0.getJSONObject(r1)     // Catch:{ JSONException -> 0x0096 }
            java.lang.String r2 = "user_id"
            java.lang.String r2 = r1.getString(r2)     // Catch:{ JSONException -> 0x0096 }
            java.lang.String r3 = "appid"
            java.lang.String r3 = r1.getString(r3)     // Catch:{ JSONException -> 0x0096 }
            java.lang.String r4 = "channel_id"
            com.baidu.android.pushservice.y r5 = com.baidu.android.pushservice.y.a()     // Catch:{ JSONException -> 0x0096 }
            java.lang.String r5 = r5.c()     // Catch:{ JSONException -> 0x0096 }
            r1.put(r4, r5)     // Catch:{ JSONException -> 0x0096 }
            com.baidu.android.pushservice.a.l r1 = r6.b     // Catch:{ JSONException -> 0x0096 }
            r1.g = r2     // Catch:{ JSONException -> 0x0096 }
            com.baidu.android.pushservice.a.l r1 = r6.b     // Catch:{ JSONException -> 0x0096 }
            r1.f = r3     // Catch:{ JSONException -> 0x0096 }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x0096 }
            boolean r1 = com.baidu.android.pushservice.b.a()     // Catch:{ JSONException -> 0x013f }
            if (r1 == 0) goto L_0x007e
            java.lang.String r1 = "BaseRegisterProcessor"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x013f }
            r4.<init>()     // Catch:{ JSONException -> 0x013f }
            java.lang.String r5 = "RegisterThread userId :  "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ JSONException -> 0x013f }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ JSONException -> 0x013f }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x013f }
            com.baidu.android.common.logging.Log.i(r1, r2)     // Catch:{ JSONException -> 0x013f }
            java.lang.String r1 = "BaseRegisterProcessor"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x013f }
            r2.<init>()     // Catch:{ JSONException -> 0x013f }
            java.lang.String r4 = "RegisterThread appId :  "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ JSONException -> 0x013f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x013f }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x013f }
            com.baidu.android.common.logging.Log.i(r1, r2)     // Catch:{ JSONException -> 0x013f }
            java.lang.String r1 = "BaseRegisterProcessor"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x013f }
            r2.<init>()     // Catch:{ JSONException -> 0x013f }
            java.lang.String r3 = "RegisterThread content :  "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x013f }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ JSONException -> 0x013f }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x013f }
            com.baidu.android.common.logging.Log.i(r1, r2)     // Catch:{ JSONException -> 0x013f }
        L_0x007e:
            r1 = r0
        L_0x007f:
            com.baidu.android.pushservice.a.l r0 = r6.b
            java.lang.String r0 = r0.b
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00b8
            com.baidu.android.pushservice.a.l r0 = r6.b
            java.lang.String r0 = r0.b
            java.lang.String r2 = "internal"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00b8
        L_0x0095:
            return r1
        L_0x0096:
            r0 = move-exception
            r0 = r7
        L_0x0098:
            boolean r1 = com.baidu.android.pushservice.b.a()
            if (r1 == 0) goto L_0x00b6
            java.lang.String r1 = "BaseRegisterProcessor"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Appid or user_id not found @: \r\n"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r2 = r2.toString()
            com.baidu.android.common.logging.Log.d(r1, r2)
        L_0x00b6:
            r1 = r0
            goto L_0x007f
        L_0x00b8:
            com.baidu.android.pushservice.d r0 = new com.baidu.android.pushservice.d
            r0.<init>()
            com.baidu.android.pushservice.a.l r2 = r6.b
            java.lang.String r2 = r2.e
            r0.a = r2
            com.baidu.android.pushservice.a.l r2 = r6.b
            java.lang.String r2 = r2.f
            r0.b = r2
            com.baidu.android.pushservice.a.l r2 = r6.b
            java.lang.String r2 = r2.g
            r0.c = r2
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r3 = "com.baidu.android.pushservice.action.BIND_SYNC"
            r2.<init>(r3)
            android.content.Context r3 = r6.a
            com.baidu.android.pushservice.a r3 = com.baidu.android.pushservice.a.a(r3)
            boolean r4 = r6.e
            java.lang.String r3 = r3.a(r0, r4)
            java.lang.String r4 = "r_sync_rdata"
            r2.putExtra(r4, r3)
            android.content.Context r3 = r6.a
            com.baidu.android.pushservice.a r3 = com.baidu.android.pushservice.a.a(r3)
            boolean r4 = r6.e
            java.lang.String r0 = r3.b(r0, r4)
            java.lang.String r3 = "r_sync_rdata_v2"
            r2.putExtra(r3, r0)
            java.lang.String r0 = "r_sync_from"
            android.content.Context r3 = r6.a
            java.lang.String r3 = r3.getPackageName()
            r2.putExtra(r0, r3)
            r0 = 32
            r2.setFlags(r0)
            android.content.Context r0 = r6.a
            r0.sendBroadcast(r2)
            android.content.Context r0 = r6.a
            java.util.ArrayList r0 = com.baidu.android.pushservice.util.n.q(r0)
            java.util.Iterator r2 = r0.iterator()
        L_0x0117:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0095
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            android.content.Context r3 = r6.a
            android.content.Intent r3 = com.baidu.android.pushservice.PushConstants.createMethodIntent(r3)
            java.lang.String r4 = "method"
            java.lang.String r5 = "pushservice_restart"
            r3.putExtra(r4, r5)
            r3.setPackage(r0)
            android.content.Context r0 = r6.a
            r0.sendBroadcast(r3)
            android.content.Context r0 = r6.a
            r3 = 0
            com.baidu.android.pushservice.b.a(r0, r3)
            goto L_0x0117
        L_0x013f:
            r1 = move-exception
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.a.d.b(java.lang.String):java.lang.String");
    }
}
