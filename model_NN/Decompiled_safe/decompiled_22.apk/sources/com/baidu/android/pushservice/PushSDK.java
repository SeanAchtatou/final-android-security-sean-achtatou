package com.baidu.android.pushservice;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.LocalServerSocket;
import android.os.Handler;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.net.ConnectManager;
import com.baidu.android.pushservice.util.NoProGuard;
import com.baidu.android.pushservice.util.e;
import com.baidu.android.pushservice.util.n;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PushSDK implements NoProGuard {
    private static int ALARM_TIMEOUT = 600000;
    private static int ALARM_TIMEOUT_BAD_NET = 300000;
    public static final String LOCAL_SOCKET_ADDRESS = "com.baidu.pushservice.singelinstance";
    private static String TAG = "PushSDK";
    private static Context mContext;
    private static Handler mHandler;
    private static Object mIsAlive_lock = new Object();
    private static LocalServerSocket mLocalSocket;
    /* access modifiers changed from: private */
    public static Object mPushConnLock = new Object();
    public static e mPushConnection;
    private static PushSDK mPushSDK = null;
    private int alarmTimeout;
    private Runnable mConnectRunnable = new m(this);
    private Boolean mIsAlive = false;
    private Runnable mRegisterRunnable = new l(this);
    private x mRegistrationService;
    private Runnable mStartRunnable = new k(this);

    private PushSDK(Context context) {
        mHandler = new Handler();
        mContext = context.getApplicationContext();
        PushSettings.a(context.getApplicationContext());
        this.alarmTimeout = ALARM_TIMEOUT;
    }

    private void cancelAlarmRepeat() {
        Intent intent = new Intent();
        intent.putExtra("AlarmAlert", "OK");
        intent.setClass(mContext, PushService.class);
        ((AlarmManager) mContext.getSystemService("alarm")).cancel(PendingIntent.getService(mContext, 0, intent, 268435456));
    }

    public static void destory() {
        if (mPushSDK != null) {
            mPushSDK.doDestory();
        }
    }

    private void doDestory() {
        if (b.a()) {
            Log.d(TAG, "destory");
        }
        synchronized (mIsAlive_lock) {
            try {
                if (mLocalSocket != null) {
                    mLocalSocket.close();
                    mLocalSocket = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (mPushConnection != null) {
                synchronized (mPushConnLock) {
                    mPushConnection.c();
                    mPushConnection = null;
                }
            }
            e.a();
            this.mIsAlive = false;
            mPushSDK = null;
        }
    }

    public static synchronized PushSDK getInstantce(Context context) {
        PushSDK pushSDK;
        synchronized (PushSDK.class) {
            if (mPushSDK == null) {
                mPushSDK = new PushSDK(context);
            }
            pushSDK = mPushSDK;
        }
        return pushSDK;
    }

    private boolean heartbeat() {
        boolean isNetworkConnected = ConnectManager.isNetworkConnected(mContext);
        if (b.a()) {
            Log.d(TAG, "heartbeat networkConnected :" + isNetworkConnected);
        }
        if (!isNetworkConnected || mPushConnection == null) {
            return false;
        }
        if (mPushConnection.a()) {
            mPushConnection.d();
        } else if (!y.a().e()) {
            if (b.a()) {
                Log.i(TAG, "Channel token is not available, start NETWORK REGISTER SERVICE .");
            }
            scheduleRegister();
        } else {
            scheduleConnect();
        }
        return true;
    }

    public static boolean isAlive() {
        if (mPushSDK != null) {
            return mPushSDK.mIsAlive.booleanValue();
        }
        return false;
    }

    private void newPushConnection() {
        synchronized (mPushConnLock) {
            mPushConnection = e.a(mContext);
        }
    }

    private void scheduleConnect() {
        mHandler.removeCallbacks(this.mConnectRunnable);
        mHandler.postDelayed(this.mConnectRunnable, 1000);
    }

    private void scheduleRegister() {
        mHandler.removeCallbacks(this.mRegisterRunnable);
        mHandler.postDelayed(this.mRegisterRunnable, 500);
    }

    private void setAlarmRepeat() {
        cancelAlarmRepeat();
        Intent intent = new Intent();
        intent.putExtra("AlarmAlert", "OK");
        intent.setClass(mContext, PushService.class);
        PendingIntent service = PendingIntent.getService(mContext.getApplicationContext(), 0, intent, 268435456);
        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService("alarm");
        alarmManager.cancel(service);
        alarmManager.setRepeating(0, System.currentTimeMillis() + ((long) ALARM_TIMEOUT), (long) this.alarmTimeout, service);
    }

    private boolean shouldReConnect(Context context) {
        SharedPreferences sharedPreferences;
        if (n.n(context.getApplicationContext()).size() <= 1) {
            if (b.a()) {
                Log.i(TAG, "Only one push app : " + context.getPackageName());
            }
            return false;
        }
        List<ActivityManager.RunningServiceInfo> runningServices = ((ActivityManager) context.getSystemService("activity")).getRunningServices(1000);
        ArrayList arrayList = new ArrayList();
        for (ActivityManager.RunningServiceInfo next : runningServices) {
            if (PushService.class.getName().equals(next.service.getClassName())) {
                arrayList.add(next.service.getPackageName());
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (!context.getPackageName().equals(str)) {
                try {
                    sharedPreferences = context.createPackageContext(str, 2).getSharedPreferences(str + ".push_sync", 1);
                } catch (PackageManager.NameNotFoundException e) {
                    if (b.a()) {
                        Log.e(TAG, e.getMessage());
                    }
                    sharedPreferences = null;
                }
                if (sharedPreferences == null) {
                    if (b.a()) {
                        Log.w(TAG, "App:" + str + " doesn't init Version!");
                    }
                } else if (sharedPreferences.getInt("version2", 0) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean shouldStopSelf(Context context) {
        boolean z;
        SharedPreferences sharedPreferences;
        List n = n.n(context.getApplicationContext());
        if (n.size() > 1) {
            long j = context.getSharedPreferences(context.getPackageName() + ".push_sync", 1).getLong("priority2", 0);
            Iterator it = n.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                String str = ((ResolveInfo) it.next()).activityInfo.packageName;
                if (!context.getPackageName().equals(str)) {
                    try {
                        sharedPreferences = context.createPackageContext(str, 2).getSharedPreferences(str + ".push_sync", 1);
                    } catch (PackageManager.NameNotFoundException e) {
                        if (b.a()) {
                            Log.e(TAG, e.getMessage());
                        }
                        sharedPreferences = null;
                    }
                    if (sharedPreferences != null) {
                        long j2 = sharedPreferences.getLong("priority2", 0);
                        if (j2 > j) {
                            if (b.a()) {
                                Log.d(TAG, "shouldStopSelf-------localPriority = " + j + ";  other packageName = " + str + "--priority =" + j2);
                            }
                            z = true;
                        }
                    } else if (b.a()) {
                        Log.w(TAG, "App:" + str + " doesn't init Version!");
                    }
                }
            }
            return z;
        } else if (!b.a()) {
            return false;
        } else {
            Log.i(TAG, "Only one push app : " + context.getPackageName());
            return false;
        }
    }

    private boolean tryConnect() {
        boolean isNetworkConnected = ConnectManager.isNetworkConnected(mContext);
        if (b.a()) {
            Log.d(TAG, "tryConnect networkConnected :" + isNetworkConnected);
        }
        if (!isNetworkConnected || mPushConnection == null) {
            return false;
        }
        if (!mPushConnection.a()) {
            if (!y.a().e()) {
                if (b.a()) {
                    Log.i(TAG, "Channel token is not available, start NETWORK REGISTER SERVICE .");
                }
                scheduleRegister();
            } else {
                scheduleConnect();
            }
        }
        return true;
    }

    public x getRegistrationService() {
        return this.mRegistrationService;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean handleOnStart(android.content.Intent r7) {
        /*
            r6 = this;
            r2 = 1
            r1 = 0
            boolean r0 = com.baidu.android.pushservice.b.a()
            if (r0 == 0) goto L_0x0026
            java.lang.String r3 = com.baidu.android.pushservice.PushSDK.TAG
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r4 = "handleOnStart intent action = "
            java.lang.StringBuilder r4 = r0.append(r4)
            if (r7 == 0) goto L_0x0048
            java.lang.String r0 = r7.getAction()
        L_0x001b:
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            com.baidu.android.common.logging.Log.d(r3, r0)
        L_0x0026:
            if (r7 != 0) goto L_0x003a
            android.content.Intent r7 = new android.content.Intent
            r7.<init>()
            boolean r0 = com.baidu.android.pushservice.b.a()
            if (r0 == 0) goto L_0x003a
            java.lang.String r0 = com.baidu.android.pushservice.PushSDK.TAG
            java.lang.String r3 = "--- handleOnStart by null intent!"
            com.baidu.android.common.logging.Log.i(r0, r3)
        L_0x003a:
            java.lang.Object r3 = com.baidu.android.pushservice.PushSDK.mIsAlive_lock
            monitor-enter(r3)
            java.lang.Boolean r0 = r6.mIsAlive     // Catch:{ all -> 0x0085 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0085 }
            if (r0 != 0) goto L_0x004b
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            r0 = r1
        L_0x0047:
            return r0
        L_0x0048:
            java.lang.String r0 = ""
            goto L_0x001b
        L_0x004b:
            android.os.Handler r0 = com.baidu.android.pushservice.PushSDK.mHandler     // Catch:{ all -> 0x0085 }
            java.lang.Runnable r4 = r6.mStartRunnable     // Catch:{ all -> 0x0085 }
            r0.removeCallbacks(r4)     // Catch:{ all -> 0x0085 }
            boolean r0 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x0070
            java.lang.String r0 = com.baidu.android.pushservice.PushSDK.TAG     // Catch:{ all -> 0x0085 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0085 }
            r4.<init>()     // Catch:{ all -> 0x0085 }
            java.lang.String r5 = "-- handleOnStart -- "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0085 }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ all -> 0x0085 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0085 }
            com.baidu.android.common.logging.Log.i(r0, r4)     // Catch:{ all -> 0x0085 }
        L_0x0070:
            android.net.LocalServerSocket r0 = com.baidu.android.pushservice.PushSDK.mLocalSocket     // Catch:{ all -> 0x0085 }
            if (r0 != 0) goto L_0x0077
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            r0 = r1
            goto L_0x0047
        L_0x0077:
            java.lang.String r0 = "AlarmAlert"
            java.lang.String r0 = r7.getStringExtra(r0)     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x0088
            boolean r0 = r6.heartbeat()     // Catch:{ all -> 0x0085 }
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            goto L_0x0047
        L_0x0085:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            throw r0
        L_0x0088:
            java.lang.String r0 = "com.baidu.pushservice.action.STOP"
            java.lang.String r4 = r7.getAction()     // Catch:{ all -> 0x0085 }
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x0097
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            r0 = r1
            goto L_0x0047
        L_0x0097:
            if (r7 == 0) goto L_0x00df
            java.lang.String r0 = "pushservice_restart"
            java.lang.String r4 = "method"
            java.lang.String r4 = r7.getStringExtra(r4)     // Catch:{ all -> 0x0085 }
            boolean r0 = r0.equals(r4)     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x00c6
            android.content.Context r0 = com.baidu.android.pushservice.PushSDK.mContext     // Catch:{ all -> 0x0085 }
            java.lang.String r0 = r0.getPackageName()     // Catch:{ all -> 0x0085 }
            java.lang.String r4 = "pkg_name"
            java.lang.String r4 = r7.getStringExtra(r4)     // Catch:{ all -> 0x0085 }
            boolean r0 = android.text.TextUtils.equals(r0, r4)     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x00bc
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            r0 = r2
            goto L_0x0047
        L_0x00bc:
            android.content.Context r0 = com.baidu.android.pushservice.PushSDK.mContext     // Catch:{ all -> 0x0085 }
            r4 = 1000(0x3e8, double:4.94E-321)
            com.baidu.android.pushservice.util.n.a(r0, r4)     // Catch:{ all -> 0x0085 }
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            r0 = r1
            goto L_0x0047
        L_0x00c6:
            com.baidu.android.pushservice.x r0 = r6.mRegistrationService     // Catch:{ all -> 0x0085 }
            boolean r0 = r0.a(r7)     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x00df
            boolean r0 = com.baidu.android.pushservice.b.a()     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x00db
            java.lang.String r0 = com.baidu.android.pushservice.PushSDK.TAG     // Catch:{ all -> 0x0085 }
            java.lang.String r1 = "-- handleOnStart -- intent handled  by mRegistrationService "
            com.baidu.android.common.logging.Log.i(r0, r1)     // Catch:{ all -> 0x0085 }
        L_0x00db:
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            r0 = r2
            goto L_0x0047
        L_0x00df:
            boolean r0 = r6.tryConnect()     // Catch:{ all -> 0x0085 }
            monitor-exit(r3)     // Catch:{ all -> 0x0085 }
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.android.pushservice.PushSDK.handleOnStart(android.content.Intent):boolean");
    }

    public boolean initPushSDK() {
        if (b.a()) {
            Log.d(TAG, "Create PushSDK from : " + mContext.getPackageName());
        }
        n.f(mContext.getApplicationContext());
        if (!n.c(mContext.getApplicationContext()) && !shouldStopSelf(mContext)) {
            synchronized (mIsAlive_lock) {
                if (mLocalSocket == null) {
                    try {
                        mLocalSocket = new LocalServerSocket(n.p(mContext));
                    } catch (Exception e) {
                        if (b.a()) {
                            Log.d(TAG, "--- Socket Adress (" + n.p(mContext) + ") in use --- @ " + mContext.getPackageName());
                        }
                    }
                }
                if (mLocalSocket == null) {
                    return false;
                }
                newPushConnection();
                this.mRegistrationService = new x(mContext);
                setAlarmRepeat();
                mHandler.postDelayed(this.mStartRunnable, 500);
                this.mIsAlive = true;
                return true;
            }
        } else if (!b.a()) {
            return false;
        } else {
            Log.d(TAG, "onCreate shouldStopSelf");
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void sendRequestTokenIntent() {
        if (b.a()) {
            Log.d(TAG, ">> sendRequestTokenIntent");
        }
        b.a(mContext, new Intent("com.baidu.pushservice.action.TOKEN"));
    }

    public void setAlarmTimeout(int i) {
        if (i > 0) {
            this.alarmTimeout = i * 1000;
        }
        setAlarmRepeat();
    }
}
