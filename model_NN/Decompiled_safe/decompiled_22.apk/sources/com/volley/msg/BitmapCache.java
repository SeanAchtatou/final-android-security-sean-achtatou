package com.volley.msg;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import com.android.volley.toolbox.ImageLoader;
import com.baidu.android.common.logging.Log;

public class BitmapCache implements ImageLoader.ImageCache {
    private LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(Log.FILE_LIMETE) {
        /* access modifiers changed from: protected */
        public int sizeOf(String key, Bitmap value) {
            return value.getRowBytes() * value.getHeight();
        }
    };

    public Bitmap getBitmap(String url) {
        return this.mCache.get(url);
    }

    public void putBitmap(String url, Bitmap bitmap) {
        this.mCache.put(url, bitmap);
    }
}
