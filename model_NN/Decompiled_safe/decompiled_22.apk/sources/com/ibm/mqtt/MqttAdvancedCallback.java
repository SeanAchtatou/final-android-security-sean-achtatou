package com.ibm.mqtt;

public interface MqttAdvancedCallback extends MqttSimpleCallback {
    void published(int i);

    void subscribed(int i, byte[] bArr);

    void unsubscribed(int i);
}
