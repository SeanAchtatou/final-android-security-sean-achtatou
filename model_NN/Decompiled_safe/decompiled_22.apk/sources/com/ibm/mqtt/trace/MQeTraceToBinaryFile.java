package com.ibm.mqtt.trace;

import com.city_life.part_asynctask.UploadUtils;
import com.ibm.mqtt.MQeTraceHandler;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class MQeTraceToBinaryFile extends MQeTraceToBinary implements MQeTraceHandler {
    private static final String CURRENT_DIRECTORY = ".";
    private static final boolean DEBUG = false;
    private static final String DEFAULT_DIRECTORY = ".";
    private static final int DEFAULT_FILES_EXISTING_AT_ONCE = 1;
    public static final String DEFAULT_FILE_NAME_PREFIX = "mqe";
    public static final String DEFAULT_FILE_NAME_SUFFIX = ".trc";
    static final short FOOTER_BECAUSE_OF_CLOSE = -24001;
    static final short FOOTER_BECAUSE_OF_FINALISE = -24002;
    static final short FOOTER_BECAUSE_OF_WRAP = -24000;
    public static final long MIN_TRACE_FILE_SIZE = 2096;
    private static final int NEVER_WRAP = -1;
    public static short[] version = {2, 0, 0, 2};
    private long bytesWritten;
    File currentFile;
    private FileOutputStream currentFileOut;
    private String directoryName;
    private int fileNameIndex;
    private String fileNamePrefix;
    private String fileNameSuffix;
    private int filesExistingAtOnce;
    private byte[] footer;
    private long maxFileSizeBeforeWrap;

    public MQeTraceToBinaryFile() {
        this(".", DEFAULT_FILE_NAME_PREFIX, DEFAULT_FILE_NAME_SUFFIX, 1, -1);
    }

    public MQeTraceToBinaryFile(String str, String str2, String str3, int i, long j) {
        this.maxFileSizeBeforeWrap = -1;
        this.filesExistingAtOnce = 1;
        this.fileNameSuffix = DEFAULT_FILE_NAME_SUFFIX;
        this.fileNamePrefix = DEFAULT_FILE_NAME_PREFIX;
        this.directoryName = ".";
        this.fileNameIndex = 0;
        this.bytesWritten = 0;
        this.directoryName = str == null ? "." : str;
        this.fileNamePrefix = str2 == null ? DEFAULT_FILE_NAME_PREFIX : str2;
        this.fileNameSuffix = str3 == null ? DEFAULT_FILE_NAME_SUFFIX : str3;
        i = i < 1 ? 1 : i;
        this.filesExistingAtOnce = i;
        if (i == 1) {
            j = -1;
        } else if (j < MIN_TRACE_FILE_SIZE) {
            j = 2096;
        }
        this.maxFileSizeBeforeWrap = j;
        this.fileNameIndex = 0;
        this.footer = super.getFooter(FOOTER_BECAUSE_OF_CLOSE);
    }

    private final void closeFile() {
        try {
            this.currentFileOut.close();
            this.currentFileOut = null;
        } catch (IOException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void advanceFileNameIndex() {
        this.fileNameIndex++;
        if (this.fileNameIndex >= this.filesExistingAtOnce) {
            this.fileNameIndex = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public void finalise() {
        if (this.currentFileOut != null) {
            writeFooter(FOOTER_BECAUSE_OF_FINALISE);
            closeFile();
        }
    }

    /* access modifiers changed from: package-private */
    public String getPaddedIndex() {
        String str = "";
        int length = Integer.toString(this.filesExistingAtOnce).length();
        String num = Integer.toString(this.fileNameIndex);
        for (int length2 = num.length(); length2 < length; length2++) {
            str = new StringBuffer().append(str).append(UploadUtils.SUCCESS).toString();
        }
        return new StringBuffer().append(str).append(num).toString();
    }

    /* access modifiers changed from: package-private */
    public boolean off() {
        boolean writeFooter = writeFooter(FOOTER_BECAUSE_OF_CLOSE);
        advanceFileNameIndex();
        closeFile();
        return writeFooter;
    }

    /* access modifiers changed from: package-private */
    public boolean on() {
        return openFile();
    }

    /* access modifiers changed from: package-private */
    public boolean openFile() {
        this.currentFile = new File(new StringBuffer().append(this.directoryName).append(System.getProperty("file.separator")).append(this.fileNamePrefix).append(getPaddedIndex()).append(this.fileNameSuffix).toString());
        if (this.currentFile.exists()) {
            this.currentFile.delete();
        }
        try {
            this.bytesWritten = 0;
            this.currentFileOut = new FileOutputStream(this.currentFile);
            writeHeader();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public String throwableStackTrace(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    /* access modifiers changed from: package-private */
    public boolean writeFooter(short s) {
        try {
            this.currentFileOut.write(getFooter(s));
            this.currentFileOut.flush();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean writeHeader() {
        byte[] header = getHeader();
        try {
            this.currentFileOut.write(header);
            this.currentFileOut.flush();
            this.bytesWritten += (long) header.length;
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean writeRecord(byte[] bArr) {
        boolean z = true;
        if (this.maxFileSizeBeforeWrap != -1 && ((long) bArr.length) + this.bytesWritten + ((long) this.footer.length) > this.maxFileSizeBeforeWrap && (z = writeFooter(FOOTER_BECAUSE_OF_WRAP))) {
            advanceFileNameIndex();
            z = openFile();
        }
        if (!z) {
            return z;
        }
        try {
            this.currentFileOut.write(bArr);
            this.bytesWritten += (long) bArr.length;
            this.currentFileOut.flush();
            return z;
        } catch (IOException e) {
            return false;
        }
    }
}
