package com.ibm.mqtt.trace;

import com.ibm.mqtt.MQe;
import com.ibm.mqtt.MQeTraceHandler;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class MQeTraceToBinary implements MQeTraceHandler {
    private static final int ASCII_CHARS_IN_MAX_LENGTH_STRING = 20480;
    static final short DATA_FORMAT_VERSION = 1;
    private static final boolean DEBUG = false;
    public static final int UNICODE_CHARS_IN_MAX_LENGTH_STRING = 10240;
    static final int V1_MAGIC = -17978438;
    static String[] prefixes = {"com.ibm.mqe.adapters.MQe", "a:", "com.ibm.mqe.administration.MQe", "b:", "com.ibm.mqe.attributes.MQe", "c:", "com.ibm.mqe.bindings.MQe", "d:", "com.ibm.mqe.communications.MQe", "e:", "com.ibm.mqe.messagestore.MQe", "f:", "com.ibm.mqe.mqbridge.MQe", "g:", "com.ibm.mqe.registry.MQe", "h:", "com.ibm.mqe.server.MQe", "i:", "com.ibm.mqe.mqemqmessage.MQe", "j:", "com.ibm.mqe.trace.MQe", "k:", "com.ibm.mqe.validation.MQe", "l:", "com.ibm.mqe.MQe", "m:", "com.ibm.mqe.adapters.", "n:", "com.ibm.mqe.administration.", "o:", "com.ibm.mqe.attributes.", "p:", "com.ibm.mqe.bindings.", "q:", "com.ibm.mqe.communications.", "r:", "com.ibm.mqe.messagestore.", "s:", "com.ibm.mqe.mqbridge.", "t:", "com.ibm.mqe.registry.", "u:", "com.ibm.mqe.server.", "v:", "com.ibm.mqe.mqemqmessage.", "w:", "com.ibm.mqe.trace.", "x:", "com.ibm.mqe.validation.", "y:", "com.ibm.mqe.", "z:"};
    public static short[] version = {2, 0, 0, 2};
    private long currentFilter = 0;
    private boolean isOn = false;
    private int recordsOutput = 0;

    MQeTraceToBinary() {
    }

    private static void encodeAsciiString(ByteArrayOutputStream byteArrayOutputStream, String str) throws IOException {
        if (str.length() > ASCII_CHARS_IN_MAX_LENGTH_STRING) {
            str = str.substring(0, 20479);
        }
        byteArrayOutputStream.write(MQe.shortToByte((short) str.length()));
        byteArrayOutputStream.write(MQe.asciiToByte(str));
    }

    private static void encodeUnicodeString(ByteArrayOutputStream byteArrayOutputStream, String str) throws IOException {
        if (str.length() > 10240) {
            str = str.substring(0, 10239);
        }
        byteArrayOutputStream.write(MQe.shortToByte((short) str.length()));
        byteArrayOutputStream.write(MQe.unicodeToByte(str));
    }

    private String shortenClassName(String str) {
        boolean z = false;
        if (str == null || !str.startsWith("com.ibm.mqe.")) {
            return str;
        }
        int i = 0;
        String str2 = str;
        while (!z && i < prefixes.length) {
            if (str.startsWith(prefixes[i])) {
                str2 = new StringBuffer().append(prefixes[i + 1]).append(str.substring(prefixes[i].length())).toString();
                z = true;
            }
            i += 2;
        }
        return str2;
    }

    private synchronized void traceFilteredMessage(Object obj, short s, long j, Object[] objArr) {
        String shortenClassName;
        int hashCode;
        if (this.isOn) {
            long currentTimeMillis = System.currentTimeMillis();
            String thread = Thread.currentThread().toString();
            int hashCode2 = Thread.currentThread().hashCode();
            if (obj == null) {
                shortenClassName = "";
                hashCode = 0;
            } else {
                shortenClassName = shortenClassName(obj.getClass().toString());
                hashCode = obj.hashCode();
            }
            writeRecord(constructRecord(currentTimeMillis, s, thread, hashCode2, shortenClassName, hashCode, j, convertInsertsToStrings(objArr)));
        }
    }

    /* access modifiers changed from: package-private */
    public byte[] constructRecord(long j, short s, String str, int i, String str2, int i2, long j2, String[] strArr) {
        this.recordsOutput++;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byteArrayOutputStream.write(MQe.longToByte(j));
            byteArrayOutputStream.write(MQe.shortToByte(s));
            encodeAsciiString(byteArrayOutputStream, str);
            byteArrayOutputStream.write(MQe.intToByte(i));
            encodeAsciiString(byteArrayOutputStream, str2);
            byteArrayOutputStream.write(MQe.intToByte(i2));
            byteArrayOutputStream.write(MQe.shortToByte((short) strArr.length));
            for (String encodeUnicodeString : strArr) {
                encodeUnicodeString(byteArrayOutputStream, encodeUnicodeString);
            }
        } catch (IOException e) {
        }
        return byteArrayOutputStream.toByteArray();
    }

    /* access modifiers changed from: package-private */
    public String[] convertInsertsToStrings(Object[] objArr) {
        Object obj;
        int length = objArr == null ? 0 : objArr.length;
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            if (objArr[i] == null) {
                strArr[i] = "";
            } else {
                strArr[i] = objArr[i].toString();
            }
        }
        if (length > 0 && (obj = objArr[length - 1]) != null && (obj instanceof Throwable)) {
            int i2 = length - 1;
            strArr[i2] = new StringBuffer().append(strArr[i2]).append("\n").append(throwableStackTrace((Throwable) obj)).toString();
        }
        return strArr;
    }

    /* access modifiers changed from: package-private */
    public byte[] getFooter(short s) {
        return constructRecord(System.currentTimeMillis(), s, Thread.currentThread().toString(), Thread.currentThread().hashCode(), getClass().toString(), hashCode(), 0, new String[0]);
    }

    /* access modifiers changed from: package-private */
    public byte[] getHeader() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.write(MQe.intToByte(V1_MAGIC));
            byteArrayOutputStream.write(MQe.shortToByte(1));
            short[] sArr = MQe.version;
            byteArrayOutputStream.write(MQe.shortToByte(sArr[0]));
            byteArrayOutputStream.write(MQe.shortToByte(sArr[1]));
            byteArrayOutputStream.write(MQe.shortToByte(sArr[2]));
            byteArrayOutputStream.write(MQe.shortToByte(0));
            encodeAsciiString(byteArrayOutputStream, MQe.sccsid);
            byteArrayOutputStream.write(MQe.longToByte(System.currentTimeMillis()));
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean off() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean on() {
        this.recordsOutput = 0;
        return true;
    }

    public void setFilter(long j) {
        if (this.currentFilter == 0) {
            if (j != 0) {
                this.isOn = on();
            }
        } else if (j == 0 && off()) {
            this.isOn = false;
        }
        this.currentFilter = j;
    }

    /* access modifiers changed from: package-private */
    public abstract String throwableStackTrace(Throwable th);

    public void traceMessage(Object obj, short s, long j) {
        traceFilteredMessage(obj, s, j, new Object[0]);
    }

    public void traceMessage(Object obj, short s, long j, Object obj2) {
        traceFilteredMessage(obj, s, j, new Object[]{obj2});
    }

    public void traceMessage(Object obj, short s, long j, Object obj2, Object obj3) {
        traceFilteredMessage(obj, s, j, new Object[]{obj2, obj3});
    }

    public void traceMessage(Object obj, short s, long j, Object obj2, Object obj3, Object obj4) {
        traceFilteredMessage(obj, s, j, new Object[]{obj2, obj3, obj4});
    }

    public void traceMessage(Object obj, short s, long j, Object obj2, Object obj3, Object obj4, Object obj5) {
        traceFilteredMessage(obj, s, j, new Object[]{obj2, obj3, obj4, obj5});
    }

    /* access modifiers changed from: package-private */
    public abstract boolean writeRecord(byte[] bArr);
}
