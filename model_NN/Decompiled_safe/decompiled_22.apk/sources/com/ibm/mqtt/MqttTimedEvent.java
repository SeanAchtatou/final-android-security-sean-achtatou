package com.ibm.mqtt;

public interface MqttTimedEvent {
    long getTime();

    boolean notifyEvent() throws Exception;
}
