package com.ibm.mqtt;

public class MQe {
    public static final char[] Hex = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    public static final String copyright = "Licensed Materials - Property of IBM\nProduct number: 5765-E63\nCopyright IBM Corp. 1999,2002 All Rights Reserved.\nUS Government Users Restriced Rights - use, duplication or\ndisclosure restriced by GSA ADP Schedule Contract with IBM Corp.";
    public static final String sccsid = "mqe_java/source/com/ibm/mqe/MQe.java, MQeBase, la000 1.111";
    public static short[] version = {2, 0, 0, 2};
    private MQeTrace traceService = new MQeTrace();

    public static byte[] asciiToByte(String str) {
        if (str == null) {
            return null;
        }
        byte[] bArr = new byte[str.length()];
        char[] cArr = new char[str.length()];
        str.getChars(0, str.length(), cArr, 0);
        for (int i = 0; i < cArr.length; i++) {
            bArr[i] = (byte) cArr[i];
        }
        return bArr;
    }

    public static String byteToAscii(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        char[] cArr = new char[bArr.length];
        for (int i = 0; i < bArr.length; i++) {
            cArr[i] = (char) (bArr[i] & 255);
        }
        return new String(cArr);
    }

    public static String byteToHex(byte[] bArr) {
        return byteToHex(bArr, 0, bArr.length);
    }

    public static String byteToHex(byte[] bArr, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer(128);
        if (bArr != null) {
            for (int i3 = i; i3 < i + i2; i3++) {
                stringBuffer.append(Hex[(bArr[i3] >> 4) & 15]);
                stringBuffer.append(Hex[bArr[i3] & 15]);
            }
        }
        return stringBuffer.toString();
    }

    public static int byteToInt(byte[] bArr, int i) {
        int i2 = 0;
        for (int i3 = 0; i3 < 4; i3++) {
            i2 = (i2 << 8) + (bArr[i + i3] & 255);
        }
        return i2;
    }

    public static long byteToLong(byte[] bArr, int i) {
        long j = 0;
        for (int i2 = 0; i2 < 8; i2++) {
            j = (j << 8) + ((long) (bArr[i + i2] & 255));
        }
        return j;
    }

    public static short byteToShort(byte[] bArr, int i) {
        return (short) (((((short) bArr[i + 0]) & 255) << 8) + (((short) bArr[i + 1]) & 255));
    }

    public static String byteToUnicode(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        char[] cArr = new char[(bArr.length / 2)];
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] = (char) (((bArr[i * 2] & 255) << 8) | (bArr[(i * 2) + 1] & 255));
        }
        return new String(cArr, 0, cArr.length);
    }

    public static byte[] intToByte(int i) {
        byte[] bArr = new byte[4];
        for (int i2 = 0; i2 < 4; i2++) {
            bArr[3 - i2] = (byte) (i & 255);
            i >>= 8;
        }
        return bArr;
    }

    public static byte[] longToByte(long j) {
        byte[] bArr = new byte[8];
        for (int i = 0; i < 8; i++) {
            bArr[7 - i] = (byte) ((int) (255 & j));
            j >>= 8;
        }
        return bArr;
    }

    public static byte[] shortToByte(short s) {
        return new byte[]{(byte) ((s >> 8) & 255), (byte) (s & 255)};
    }

    public static byte[] unicodeToByte(String str) {
        if (str == null) {
            return null;
        }
        char[] cArr = new char[str.length()];
        str.getChars(0, str.length(), cArr, 0);
        byte[] bArr = new byte[(cArr.length * 2)];
        for (int i = 0; i < cArr.length; i++) {
            bArr[i * 2] = (byte) ((cArr[i] >> 8) & 255);
            bArr[(i * 2) + 1] = (byte) (cArr[i] & 255);
        }
        return bArr;
    }
}
