package com.ibm.mqtt;

import com.utils.FinalVariable;
import java.util.Hashtable;

public abstract class MqttBaseClient extends Mqtt implements Runnable {
    protected static final int conNotify = 1;
    private static int maxOutstanding = 10;
    protected static final int subNotify = 4;
    protected static final int unsubNotify = 5;
    private boolean cleanSession = false;
    private Hashtable grantedQoS = new Hashtable();
    private Object outLock = new Object();
    private boolean outLockNotified = false;
    private MqttHashTable outstandingQueue = null;
    private MqttPersistence persistenceLayer = null;
    private Hashtable qos2PubsArrived = new Hashtable();
    private Object readerControl = new Object();
    private int retryPeriod;
    private MqttTimedEventQueue retryQueue = null;
    private boolean terminated = false;

    protected MqttBaseClient() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:101:0x008e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00a4 A[Catch:{ IndexOutOfBoundsException -> 0x00b8 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void doConnect(com.ibm.mqtt.MqttConnect r13, boolean r14, short r15) throws com.ibm.mqtt.MqttException, com.ibm.mqtt.MqttPersistenceException {
        /*
            r12 = this;
            r6 = 0
            r3 = 1
            r1 = 0
            com.ibm.mqtt.MqttTimedEventQueue r0 = r12.retryQueue
            r0.resetTimedEventQueue()
            com.ibm.mqtt.MqttHashTable r0 = r12.outstandingQueue
            r0.clear()
            java.util.Hashtable r0 = r12.qos2PubsArrived
            r0.clear()
            r12.initialiseOutMsgIds(r6)
            com.ibm.mqtt.MqttPersistence r0 = r12.persistenceLayer
            if (r0 == 0) goto L_0x002a
            boolean r0 = r12.isConnectionLost()
            if (r0 != 0) goto L_0x002a
            com.ibm.mqtt.MqttPersistence r0 = r12.persistenceLayer
            java.lang.String r2 = r13.getClientId()
            java.lang.String r4 = r12.connection
            r0.open(r2, r4)
        L_0x002a:
            if (r14 == 0) goto L_0x0037
            r12.cleanSession = r3
            com.ibm.mqtt.MqttPersistence r0 = r12.persistenceLayer
            if (r0 == 0) goto L_0x0037
            com.ibm.mqtt.MqttPersistence r0 = r12.persistenceLayer
            r0.reset()
        L_0x0037:
            if (r15 <= 0) goto L_0x0056
            com.ibm.mqtt.MqttRetry r0 = new com.ibm.mqtt.MqttRetry
            com.ibm.mqtt.MqttPingreq r2 = new com.ibm.mqtt.MqttPingreq
            r2.<init>()
            int r4 = r15 * 1000
            long r4 = (long) r4
            r0.<init>(r12, r2, r4)
            com.ibm.mqtt.MqttHashTable r2 = r12.outstandingQueue
            monitor-enter(r2)
            com.ibm.mqtt.MqttHashTable r4 = r12.outstandingQueue     // Catch:{ all -> 0x0091 }
            r7 = 0
            r4.put(r7, r0)     // Catch:{ all -> 0x0091 }
            monitor-exit(r2)     // Catch:{ all -> 0x0091 }
            com.ibm.mqtt.MqttTimedEventQueue r2 = r12.retryQueue
            r2.enqueue(r0)
        L_0x0056:
            if (r14 != 0) goto L_0x0147
            com.ibm.mqtt.MqttPersistence r0 = r12.persistenceLayer
            if (r0 == 0) goto L_0x0147
            boolean r0 = r12.isConnectionLost()
            if (r0 != 0) goto L_0x0147
            com.ibm.mqtt.MqttPersistence r0 = r12.persistenceLayer
            byte[][] r7 = r0.getAllSentMessages()
            if (r7 == 0) goto L_0x00f7
            java.util.Vector r8 = new java.util.Vector
            r8.<init>()
            r0 = r1
        L_0x0070:
            int r2 = r7.length
            if (r0 >= r2) goto L_0x00f4
            r9 = r7[r0]     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r2 = r3
            r4 = r3
            r5 = r1
        L_0x0078:
            byte r10 = r9[r4]     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r11 = r10 & 127(0x7f, float:1.78E-43)
            int r11 = r11 * r2
            int r5 = r5 + r11
            int r2 = r2 * 128
            int r4 = r4 + 1
            r10 = r10 & 128(0x80, float:1.794E-43)
            if (r10 != 0) goto L_0x0078
            int r2 = r5 + r4
            int r5 = r9.length     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            if (r2 == r5) goto L_0x0094
            r12.invalidSentMessageRestored(r0)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
        L_0x008e:
            int r0 = r0 + 1
            goto L_0x0070
        L_0x0091:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0091 }
            throw r0
        L_0x0094:
            r2 = 0
            byte r2 = r9[r2]     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            short r2 = com.ibm.mqtt.MqttPacket.getMsgType(r2)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            switch(r2) {
                case 3: goto L_0x00bd;
                case 4: goto L_0x009e;
                case 5: goto L_0x009e;
                case 6: goto L_0x00d7;
                default: goto L_0x009e;
            }     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
        L_0x009e:
            r12.invalidSentMessageRestored(r0)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r2 = r6
        L_0x00a2:
            if (r2 == 0) goto L_0x008e
            com.ibm.mqtt.MqttHashTable r4 = r12.outstandingQueue     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            monitor-enter(r4)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            com.ibm.mqtt.MqttHashTable r5 = r12.outstandingQueue     // Catch:{ all -> 0x00f1 }
            int r9 = r2.getMsgId()     // Catch:{ all -> 0x00f1 }
            long r9 = (long) r9     // Catch:{ all -> 0x00f1 }
            r5.put(r9, r2)     // Catch:{ all -> 0x00f1 }
            monitor-exit(r4)     // Catch:{ all -> 0x00f1 }
            com.ibm.mqtt.MqttTimedEventQueue r4 = r12.retryQueue     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r4.enqueue(r2)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            goto L_0x008e
        L_0x00b8:
            r2 = move-exception
            r12.invalidSentMessageRestored(r0)
            goto L_0x008e
        L_0x00bd:
            com.ibm.mqtt.MqttPublish r5 = new com.ibm.mqtt.MqttPublish     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r5.<init>(r9, r4)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            java.lang.Integer r2 = new java.lang.Integer     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            int r4 = r5.getMsgId()     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r2.<init>(r4)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r8.addElement(r2)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            com.ibm.mqtt.MqttRetry r2 = new com.ibm.mqtt.MqttRetry     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            int r4 = r12.retryPeriod     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            long r9 = (long) r4     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r2.<init>(r12, r5, r9)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            goto L_0x00a2
        L_0x00d7:
            com.ibm.mqtt.MqttPubrel r5 = new com.ibm.mqtt.MqttPubrel     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r5.<init>(r9, r4)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            java.lang.Integer r2 = new java.lang.Integer     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            int r4 = r5.getMsgId()     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r2.<init>(r4)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r8.addElement(r2)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            com.ibm.mqtt.MqttRetry r2 = new com.ibm.mqtt.MqttRetry     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            int r4 = r12.retryPeriod     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            long r9 = (long) r4     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            r2.<init>(r12, r5, r9)     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
            goto L_0x00a2
        L_0x00f1:
            r2 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x00f1 }
            throw r2     // Catch:{ IndexOutOfBoundsException -> 0x00b8 }
        L_0x00f4:
            r12.initialiseOutMsgIds(r8)
        L_0x00f7:
            com.ibm.mqtt.MqttPersistence r0 = r12.persistenceLayer
            byte[][] r7 = r0.getAllReceivedMessages()
            if (r7 == 0) goto L_0x0147
            r0 = r1
        L_0x0100:
            int r2 = r7.length
            if (r0 >= r2) goto L_0x0147
            r8 = r7[r0]     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            r2 = r3
            r4 = r3
            r5 = r1
        L_0x0108:
            byte r9 = r8[r4]     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            r10 = r9 & 127(0x7f, float:1.78E-43)
            int r10 = r10 * r2
            int r5 = r5 + r10
            int r2 = r2 * 128
            int r4 = r4 + 1
            r9 = r9 & 128(0x80, float:1.794E-43)
            if (r9 != 0) goto L_0x0108
            int r2 = r5 + r4
            int r5 = r8.length     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            if (r2 == r5) goto L_0x0121
            r12.invalidReceivedMessageRestored(r0)     // Catch:{ IndexOutOfBoundsException -> 0x013e }
        L_0x011e:
            int r0 = r0 + 1
            goto L_0x0100
        L_0x0121:
            r2 = 0
            byte r2 = r8[r2]     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            short r2 = com.ibm.mqtt.MqttPacket.getMsgType(r2)     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            r5 = 3
            if (r2 != r5) goto L_0x0143
            com.ibm.mqtt.MqttPublish r2 = new com.ibm.mqtt.MqttPublish     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            r2.<init>(r8, r4)     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            java.util.Hashtable r4 = r12.qos2PubsArrived     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            int r5 = r2.getMsgId()     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            r4.put(r5, r2)     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            goto L_0x011e
        L_0x013e:
            r2 = move-exception
            r12.invalidReceivedMessageRestored(r0)
            goto L_0x011e
        L_0x0143:
            r12.invalidReceivedMessageRestored(r0)     // Catch:{ IndexOutOfBoundsException -> 0x013e }
            goto L_0x011e
        L_0x0147:
            r12.registeredException = r6
            r12.setConnectionLost(r1)
            java.lang.Object r1 = r12.readerControl     // Catch:{ MqttException -> 0x015c, Exception -> 0x015e }
            monitor-enter(r1)     // Catch:{ MqttException -> 0x015c, Exception -> 0x015e }
            r12.tcpipConnect(r13)     // Catch:{ all -> 0x0159 }
            java.lang.Object r0 = r12.readerControl     // Catch:{ all -> 0x0159 }
            r0.notify()     // Catch:{ all -> 0x0159 }
            monitor-exit(r1)     // Catch:{ all -> 0x0159 }
            return
        L_0x0159:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0159 }
            throw r0     // Catch:{ MqttException -> 0x015c, Exception -> 0x015e }
        L_0x015c:
            r0 = move-exception
            throw r0
        L_0x015e:
            r0 = move-exception
            com.ibm.mqtt.MqttException r1 = new com.ibm.mqtt.MqttException
            r1.<init>()
            r1.initCause(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ibm.mqtt.MqttBaseClient.doConnect(com.ibm.mqtt.MqttConnect, boolean, short):void");
    }

    private void invalidReceivedMessageRestored(int i) {
        MQeTrace.trace(this, -30037, 1, new Integer(i));
    }

    private void invalidSentMessageRestored(int i) {
        MQeTrace.trace(this, -30036, 1, new Integer(i));
    }

    private MqttPacket messageAck(int i) {
        MqttRetry mqttRetry;
        MqttRetry mqttRetry2 = (MqttRetry) this.outstandingQueue.get((long) i);
        if (mqttRetry2 == null) {
            return null;
        }
        if (mqttRetry2.getQoS() == 2 && mqttRetry2.getMsgType() == 3) {
            return messageAckQoS2(i);
        }
        try {
            if (this.persistenceLayer != null) {
                synchronized (this.persistenceLayer) {
                    this.persistenceLayer.delSentMessage(i);
                }
            }
            synchronized (this.outstandingQueue) {
                mqttRetry = (MqttRetry) this.outstandingQueue.remove((long) i);
            }
            releaseMsgId(i);
            if (mqttRetry != null) {
                int msgType = mqttRetry.getMsgType();
                switch (msgType) {
                    case 3:
                    case 6:
                    case 8:
                    case 10:
                        if (this.outstandingQueue.size() == maxOutstanding - 1) {
                            synchronized (this.outLock) {
                                this.outLockNotified = true;
                                this.outLock.notifyAll();
                            }
                        }
                        notifyAck(msgType, i);
                        break;
                }
            }
        } catch (MqttPersistenceException e) {
        }
        return null;
    }

    private MqttPacket messageAckQoS2(int i) {
        MqttPubrel genPubRelPacket = genPubRelPacket(i, false);
        try {
            if (this.persistenceLayer != null) {
                synchronized (this.persistenceLayer) {
                    this.persistenceLayer.updSentMessage(i, genPubRelPacket.toBytes());
                }
            }
            synchronized (this.outstandingQueue) {
                MqttRetry mqttRetry = (MqttRetry) this.outstandingQueue.remove((long) i);
                if (mqttRetry != null) {
                    mqttRetry.setMessage(genPubRelPacket);
                    this.outstandingQueue.put((long) i, mqttRetry);
                }
            }
        } catch (MqttPersistenceException e) {
        }
        return genPubRelPacket;
    }

    private void sendPacket(MqttPacket mqttPacket) throws MqttException, MqttNotConnectedException {
        boolean z = true;
        long retry = (long) (getRetry() * 1000);
        if (!isSocketConnected()) {
            throw new MqttNotConnectedException();
        } else if (mqttPacket.getQos() > 0) {
            if (this.outstandingQueue.size() >= maxOutstanding) {
                synchronized (this.outLock) {
                    try {
                        if (!this.outLockNotified) {
                            this.outLock.wait();
                        }
                        if (isSocketConnected()) {
                            this.outLockNotified = false;
                        }
                    } catch (InterruptedException e) {
                    }
                }
                if (!isSocketConnected()) {
                    throw new MqttNotConnectedException();
                }
            }
            if (this.persistenceLayer != null) {
                try {
                    synchronized (this.persistenceLayer) {
                        byte[] bytes = mqttPacket.toBytes();
                        if (mqttPacket.getPayload() != null) {
                            bytes = MqttUtils.concatArray(bytes, mqttPacket.getPayload());
                        }
                        this.persistenceLayer.addSentMessage(mqttPacket.getMsgId(), bytes);
                    }
                } catch (MqttPersistenceException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new MqttPersistenceException(new StringBuffer().append("sendPacket - toBytes failed, msgid ").append(mqttPacket.getMsgId()).toString());
                }
            }
            if (getKeepAlivePeriod() > 0) {
                if (this.outstandingQueue.size() <= 1) {
                    z = false;
                }
            } else if (this.outstandingQueue.size() <= 0) {
                z = false;
            }
            if (z) {
                retry = 0;
            }
            MqttRetry mqttRetry = new MqttRetry(this, mqttPacket, retry);
            synchronized (this.outstandingQueue) {
                this.outstandingQueue.put((long) mqttPacket.getMsgId(), mqttRetry);
            }
            this.retryQueue.enqueue(mqttRetry);
            if (retry > 0) {
                try {
                    writePacket(mqttPacket);
                } catch (MqttException e4) {
                }
            }
        } else {
            writePacket(mqttPacket);
        }
    }

    public static void setWindowSize(int i) {
        maxOutstanding = i;
    }

    public void anyErrors() throws MqttException {
        if (this.registeredException != null) {
            throw this.registeredException;
        }
    }

    /* access modifiers changed from: protected */
    public void connect(String str, boolean z, boolean z2, short s, String str2, int i, String str3, boolean z3) throws MqttException, MqttPersistenceException {
        synchronized (this.outLock) {
            this.outLockNotified = false;
        }
        MqttConnect mqttConnect = new MqttConnect();
        mqttConnect.setClientId(str);
        mqttConnect.CleanStart = z;
        mqttConnect.TopicNameCompression = z2;
        mqttConnect.KeepAlive = s;
        if (str2 != null) {
            mqttConnect.Will = true;
            mqttConnect.WillTopic = str2;
            mqttConnect.WillQoS = i;
            mqttConnect.WillRetain = z3;
            mqttConnect.WillMessage = str3;
        } else {
            mqttConnect.Will = false;
        }
        setKeepAlive(s);
        doConnect(mqttConnect, z, s);
    }

    /* access modifiers changed from: protected */
    public void connectionLost() throws Exception {
        synchronized (this.outLock) {
            this.outLockNotified = true;
            this.outLock.notifyAll();
        }
    }

    /* access modifiers changed from: protected */
    public void disconnect() throws MqttPersistenceException {
        MqttPersistence mqttPersistence;
        try {
            setConnectionState(false);
            writePacket(new MqttDisconnect());
            tcpipDisconnect(false);
            synchronized (this.readerControl) {
                if (isSocketConnected()) {
                    try {
                        this.readerControl.wait(30000);
                    } catch (InterruptedException e) {
                    }
                }
            }
            synchronized (this.outLock) {
                this.outLockNotified = true;
                this.outLock.notifyAll();
            }
            this.qos2PubsArrived.clear();
            this.retryQueue.resetTimedEventQueue();
            this.outstandingQueue.clear();
            if (this.cleanSession) {
                this.cleanSession = false;
                if (this.persistenceLayer != null) {
                    this.persistenceLayer.reset();
                }
            }
            if (this.persistenceLayer != null) {
                mqttPersistence = this.persistenceLayer;
                mqttPersistence.close();
            }
        } catch (MqttException e2) {
            tcpipDisconnect(false);
            synchronized (this.readerControl) {
                if (isSocketConnected()) {
                    try {
                        this.readerControl.wait(30000);
                    } catch (InterruptedException e3) {
                    }
                }
                synchronized (this.outLock) {
                    this.outLockNotified = true;
                    this.outLock.notifyAll();
                    this.qos2PubsArrived.clear();
                    this.retryQueue.resetTimedEventQueue();
                    this.outstandingQueue.clear();
                    if (this.cleanSession) {
                        this.cleanSession = false;
                        if (this.persistenceLayer != null) {
                            this.persistenceLayer.reset();
                        }
                    }
                    if (this.persistenceLayer != null) {
                        mqttPersistence = this.persistenceLayer;
                    }
                }
            }
        } catch (Throwable th) {
            tcpipDisconnect(false);
            synchronized (this.readerControl) {
                if (isSocketConnected()) {
                    try {
                        this.readerControl.wait(30000);
                    } catch (InterruptedException e4) {
                    }
                }
                synchronized (this.outLock) {
                    this.outLockNotified = true;
                    this.outLock.notifyAll();
                    this.qos2PubsArrived.clear();
                    this.retryQueue.resetTimedEventQueue();
                    this.outstandingQueue.clear();
                    if (this.cleanSession) {
                        this.cleanSession = false;
                        if (this.persistenceLayer != null) {
                            this.persistenceLayer.reset();
                        }
                    }
                    if (this.persistenceLayer != null) {
                        this.persistenceLayer.close();
                    }
                    throw th;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public MqttPubrel genPubRelPacket(int i, boolean z) {
        MqttPubrel mqttPubrel = new MqttPubrel();
        mqttPubrel.setMsgId(i);
        mqttPubrel.setDup(z);
        return mqttPubrel;
    }

    public int getRetry() {
        return this.retryPeriod / 1000;
    }

    /* access modifiers changed from: protected */
    public byte[] getReturnedQoS(int i) {
        MqttByteArray mqttByteArray = (MqttByteArray) this.grantedQoS.remove(new Integer(i));
        if (mqttByteArray == null) {
            return null;
        }
        return mqttByteArray.getByteArray();
    }

    /* access modifiers changed from: protected */
    public void initialise(String str, MqttPersistence mqttPersistence, Class cls) {
        super.initialise(str, cls);
        this.retryPeriod = FinalVariable.vb_success;
        this.outstandingQueue = new MqttHashTable();
        this.retryQueue = new MqttTimedEventQueue(10, this);
        this.retryQueue.start();
        this.persistenceLayer = mqttPersistence;
    }

    /* access modifiers changed from: protected */
    public abstract void notifyAck(int i, int i2);

    public boolean outstanding(int i) {
        boolean containsKey;
        synchronized (this.outstandingQueue) {
            containsKey = this.outstandingQueue.containsKey((long) i);
        }
        return containsKey;
    }

    public void process(MqttConnack mqttConnack) {
        MQeTrace.trace(this, -30017, MQeTrace.GROUP_INFO, new Integer(mqttConnack.returnCode));
        super.process(mqttConnack);
        notifyAck(1, mqttConnack.returnCode);
    }

    public void process(MqttPuback mqttPuback) {
        MQeTrace.trace(this, -30018, MQeTrace.GROUP_INFO, new Integer(mqttPuback.getMsgId()));
        messageAck(mqttPuback.getMsgId());
    }

    public void process(MqttPubcomp mqttPubcomp) {
        MQeTrace.trace(this, -30019, MQeTrace.GROUP_INFO, new Integer(mqttPubcomp.getMsgId()));
        messageAck(mqttPubcomp.getMsgId());
    }

    public void process(MqttPublish mqttPublish) {
        boolean z = false;
        MQeTrace.trace(this, -30020, MQeTrace.GROUP_INFO, Integer.toString(mqttPublish.getMsgId()), Integer.toString(mqttPublish.getQos()), new Boolean(mqttPublish.isRetain()).toString(), Integer.toString(mqttPublish.getPayload() != null ? mqttPublish.getPayload().length : 0));
        if (mqttPublish.getQos() != 2) {
            try {
                publishArrived(mqttPublish.topicName, mqttPublish.getPayload(), mqttPublish.getQos(), mqttPublish.isRetain());
            } catch (Exception e) {
                System.out.println(new StringBuffer().append("publishArrived Exception caught (QoS ").append(mqttPublish.getQos()).append("):").toString());
                e.printStackTrace();
                z = true;
            }
        }
        if (mqttPublish.getQos() > 0 && !z) {
            MQeTrace.trace(this, -30021, MQeTrace.GROUP_INFO, mqttPublish.getQos() == 1 ? "PUBACK" : "PUBREC", new Integer(mqttPublish.getMsgId()));
            if (mqttPublish.getQos() == 1) {
                MqttPuback mqttPuback = new MqttPuback();
                mqttPuback.setMsgId(mqttPublish.getMsgId());
                try {
                    writePacket(mqttPuback);
                } catch (Exception e2) {
                }
            } else {
                try {
                    if (this.persistenceLayer != null) {
                        try {
                            synchronized (this.persistenceLayer) {
                                byte[] bytes = mqttPublish.toBytes();
                                if (mqttPublish.getPayload() != null) {
                                    bytes = MqttUtils.concatArray(bytes, mqttPublish.getPayload());
                                }
                                this.persistenceLayer.addReceivedMessage(mqttPublish.getMsgId(), bytes);
                            }
                        } catch (MqttPersistenceException e3) {
                            throw e3;
                        } catch (Exception e4) {
                            throw new MqttPersistenceException(new StringBuffer().append("process(MqttPublish) - packet.toBytes() failed - msgid ").append(mqttPublish.getMsgId()).toString());
                        }
                    }
                    this.qos2PubsArrived.put(Integer.toString(mqttPublish.getMsgId()), mqttPublish);
                    MqttPubrec mqttPubrec = new MqttPubrec();
                    mqttPubrec.setMsgId(mqttPublish.getMsgId());
                    try {
                        writePacket(mqttPubrec);
                    } catch (Exception e5) {
                    }
                } catch (MqttPersistenceException e6) {
                }
            }
        }
    }

    public void process(MqttPubrec mqttPubrec) {
        MqttPacket messageAck;
        MQeTrace.trace(this, -30022, MQeTrace.GROUP_INFO, new Integer(mqttPubrec.getMsgId()));
        MqttRetry mqttRetry = (MqttRetry) this.outstandingQueue.get((long) mqttPubrec.getMsgId());
        if ((mqttRetry == null || mqttRetry.getMsgType() != 6) && (messageAck = messageAck(mqttPubrec.getMsgId())) != null) {
            try {
                writePacket(messageAck);
            } catch (Exception e) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void process(com.ibm.mqtt.MqttPubrel r7) {
        /*
            r6 = this;
            r1 = 0
            r0 = -30023(0xffffffffffff8ab9, float:NaN)
            r2 = 2097152(0x200000, double:1.0361308E-317)
            java.lang.Integer r4 = new java.lang.Integer
            int r5 = r7.getMsgId()
            r4.<init>(r5)
            com.ibm.mqtt.MQeTrace.trace(r6, r0, r2, r4)
            java.util.Hashtable r0 = r6.qos2PubsArrived
            int r2 = r7.getMsgId()
            java.lang.String r2 = java.lang.Integer.toString(r2)
            java.lang.Object r0 = r0.get(r2)
            com.ibm.mqtt.MqttPublish r0 = (com.ibm.mqtt.MqttPublish) r0
            if (r0 == 0) goto L_0x0078
            java.lang.String r2 = r0.topicName     // Catch:{ Exception -> 0x006c }
            byte[] r3 = r0.getPayload()     // Catch:{ Exception -> 0x006c }
            int r4 = r0.getQos()     // Catch:{ Exception -> 0x006c }
            boolean r0 = r0.isRetain()     // Catch:{ Exception -> 0x006c }
            r6.publishArrived(r2, r3, r4, r0)     // Catch:{ Exception -> 0x006c }
            r0 = r1
        L_0x0036:
            if (r0 != 0) goto L_0x006b
            java.util.Hashtable r0 = r6.qos2PubsArrived
            int r1 = r7.getMsgId()
            java.lang.String r1 = java.lang.Integer.toString(r1)
            r0.remove(r1)
            r0 = 0
            com.ibm.mqtt.MqttPersistence r1 = r6.persistenceLayer     // Catch:{ MqttPersistenceException -> 0x007d }
            if (r1 == 0) goto L_0x0057
            com.ibm.mqtt.MqttPersistence r1 = r6.persistenceLayer     // Catch:{ MqttPersistenceException -> 0x007d }
            monitor-enter(r1)     // Catch:{ MqttPersistenceException -> 0x007d }
            com.ibm.mqtt.MqttPersistence r2 = r6.persistenceLayer     // Catch:{ all -> 0x007a }
            int r3 = r7.getMsgId()     // Catch:{ all -> 0x007a }
            r2.delReceivedMessage(r3)     // Catch:{ all -> 0x007a }
            monitor-exit(r1)     // Catch:{ all -> 0x007a }
        L_0x0057:
            com.ibm.mqtt.MqttPubcomp r1 = new com.ibm.mqtt.MqttPubcomp
            r1.<init>()
            int r2 = r7.getMsgId()
            r1.setMsgId(r2)
            r6.writePacket(r1)     // Catch:{ Exception -> 0x007f }
        L_0x0066:
            if (r0 == 0) goto L_0x006b
            r6.setRegisteredThrowable(r0)
        L_0x006b:
            return
        L_0x006c:
            r0 = move-exception
            r1 = 1
            java.io.PrintStream r2 = java.lang.System.out
            java.lang.String r3 = "publishArrived Exception caught (QoS 2):"
            r2.println(r3)
            r0.printStackTrace()
        L_0x0078:
            r0 = r1
            goto L_0x0036
        L_0x007a:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x007a }
            throw r0     // Catch:{ MqttPersistenceException -> 0x007d }
        L_0x007d:
            r0 = move-exception
            goto L_0x0057
        L_0x007f:
            r1 = move-exception
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ibm.mqtt.MqttBaseClient.process(com.ibm.mqtt.MqttPubrel):void");
    }

    public void process(MqttSuback mqttSuback) {
        MQeTrace.trace(this, -30024, MQeTrace.GROUP_INFO, new Integer(mqttSuback.getMsgId()));
        this.grantedQoS.put(new Integer(mqttSuback.getMsgId()), new MqttByteArray(mqttSuback.TopicsQoS));
        messageAck(mqttSuback.getMsgId());
    }

    public void process(MqttUnsuback mqttUnsuback) {
        MQeTrace.trace(this, -30025, MQeTrace.GROUP_INFO, new Integer(mqttUnsuback.getMsgId()));
        messageAck(mqttUnsuback.getMsgId());
    }

    /* access modifiers changed from: protected */
    public int publish(String str, byte[] bArr, int i, boolean z) throws MqttException, MqttPersistenceException {
        int nextMsgId = i > 0 ? nextMsgId() : 0;
        sendPacket(genPublishPacket(nextMsgId, i, str, bArr, z, false));
        MQeTrace.trace(this, -30026, MQeTrace.GROUP_INFO, new Integer(nextMsgId), new Integer(i), new Boolean(z));
        return nextMsgId;
    }

    /* access modifiers changed from: protected */
    public abstract void publishArrived(String str, byte[] bArr, int i, boolean z) throws Exception;

    public void run() {
        MQeTrace.trace(this, -30027, MQeTrace.GROUP_INFO);
        synchronized (this.readerControl) {
            while (!isSocketConnected() && !this.terminated) {
                try {
                    this.readerControl.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        if (!this.terminated) {
            long j = (long) this.retryPeriod;
            while (!this.terminated) {
                try {
                    process();
                } catch (Exception e2) {
                    synchronized (this.readerControl) {
                        tcpipDisconnect(true);
                        this.readerControl.notify();
                        if (isConnected()) {
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e3) {
                            }
                            setRegisteredThrowable(null);
                            System.out.println("WMQtt client:Lost connection...");
                            new MqttReconn(this).start();
                        }
                    }
                } catch (Throwable th) {
                    synchronized (this.readerControl) {
                        tcpipDisconnect(true);
                        this.readerControl.notify();
                        setRegisteredThrowable(th);
                    }
                }
                synchronized (this.readerControl) {
                    while (!isSocketConnected() && !this.terminated) {
                        try {
                            this.readerControl.wait();
                        } catch (InterruptedException e4) {
                        }
                    }
                }
            }
        }
        MQeTrace.trace(this, -30028, MQeTrace.GROUP_INFO);
    }

    /* access modifiers changed from: protected */
    public synchronized void setConnectionState(boolean z) {
        super.setConnectionState(z);
        this.retryQueue.canDeliverEvents(z);
    }

    public void setRetry(int i) {
        if (i < 10) {
            i = 10;
        }
        this.retryPeriod = Math.abs(i * 1000);
    }

    /* access modifiers changed from: protected */
    public int subscribe(String[] strArr, int[] iArr) throws MqttException {
        int nextMsgId = nextMsgId();
        byte[] bArr = new byte[iArr.length];
        this.grantedQoS.remove(new Integer(nextMsgId));
        for (int i = 0; i < iArr.length; i++) {
            bArr[i] = (byte) iArr[i];
        }
        MqttSubscribe mqttSubscribe = new MqttSubscribe();
        mqttSubscribe.setMsgId(nextMsgId);
        mqttSubscribe.setQos(1);
        mqttSubscribe.topics = strArr;
        mqttSubscribe.topicsQoS = bArr;
        mqttSubscribe.setDup(false);
        MQeTrace.trace(this, -30029, MQeTrace.GROUP_INFO, new Integer(nextMsgId));
        sendPacket(mqttSubscribe);
        return nextMsgId;
    }

    /* access modifiers changed from: protected */
    public void terminate() {
        synchronized (this.readerControl) {
            this.terminated = true;
            this.readerControl.notify();
        }
        if (this.retryQueue != null) {
            this.retryQueue.close();
        }
    }

    /* access modifiers changed from: protected */
    public int unsubscribe(String[] strArr) throws MqttException {
        int nextMsgId = nextMsgId();
        MqttUnsubscribe mqttUnsubscribe = new MqttUnsubscribe();
        mqttUnsubscribe.setMsgId(nextMsgId);
        mqttUnsubscribe.setQos(1);
        mqttUnsubscribe.topics = strArr;
        mqttUnsubscribe.setDup(false);
        MQeTrace.trace(this, -30030, MQeTrace.GROUP_INFO, new Integer(nextMsgId));
        sendPacket(mqttUnsubscribe);
        return nextMsgId;
    }
}
