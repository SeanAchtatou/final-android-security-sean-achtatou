package com.ibm.mqtt;

public class MqttPublish extends MqttPacket {
    public String topicName;

    public MqttPublish() {
        setMsgType(3);
    }

    public MqttPublish(byte[] bArr, int i) {
        super(bArr);
        setMsgType(3);
        this.topicName = MqttUtils.UTFToString(bArr, i);
        if (getQos() > 0) {
            setMsgId(MqttUtils.toShort(bArr, this.topicName.length() + i + 2));
            setPayload(MqttUtils.SliceByteArray(bArr, this.topicName.length() + i + 4, bArr.length - ((this.topicName.length() + i) + 4)));
            return;
        }
        setPayload(MqttUtils.SliceByteArray(bArr, this.topicName.length() + i + 2, bArr.length - ((this.topicName.length() + i) + 2)));
    }

    private void uncompressTopic() {
    }

    public void compressTopic() {
    }

    public void process(MqttProcessor mqttProcessor) {
        if (mqttProcessor.supportTopicNameCompression()) {
            uncompressTopic();
        }
        mqttProcessor.process(this);
    }

    public byte[] toBytes() {
        byte[] StringToUTF = MqttUtils.StringToUTF(this.topicName);
        if (getQos() > 0) {
            this.message = new byte[(StringToUTF.length + 3)];
        } else {
            this.message = new byte[(StringToUTF.length + 1)];
        }
        this.message[0] = super.toBytes()[0];
        System.arraycopy(StringToUTF, 0, this.message, 1, StringToUTF.length);
        int length = StringToUTF.length + 1;
        if (getQos() > 0) {
            int msgId = getMsgId();
            int i = length + 1;
            this.message[length] = (byte) (msgId / 256);
            int i2 = i + 1;
            this.message[i] = (byte) (msgId % 256);
        }
        createMsgLength();
        return this.message;
    }
}
