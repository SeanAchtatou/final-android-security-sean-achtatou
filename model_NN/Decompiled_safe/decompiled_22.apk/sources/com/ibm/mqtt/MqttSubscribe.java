package com.ibm.mqtt;

import java.util.Vector;

public class MqttSubscribe extends MqttPacket {
    public String[] topics;
    public byte[] topicsQoS;

    public MqttSubscribe() {
        setMsgType(8);
    }

    public MqttSubscribe(byte[] bArr, int i) {
        super(bArr);
        setMsgType(8);
        setMsgId(MqttUtils.toShort(bArr, i));
        Vector topicsWithQoS = MqttUtils.getTopicsWithQoS(MqttUtils.SliceByteArray(bArr, i + 2, bArr.length - (i + 2)));
        int size = topicsWithQoS.size();
        this.topicsQoS = new byte[size];
        this.topics = new String[size];
        for (int i2 = 0; i2 < size; i2++) {
            String obj = topicsWithQoS.elementAt(i2).toString();
            this.topics[i2] = obj.substring(0, obj.length() - 1);
            this.topicsQoS[i2] = (byte) Character.digit(obj.charAt(obj.length() - 1), 10);
        }
    }

    private void uncompressTopic() {
    }

    public void compressTopic() {
    }

    public void process(MqttProcessor mqttProcessor) {
        mqttProcessor.process(this);
    }

    public byte[] toBytes() {
        this.message = new byte[3];
        this.message[0] = super.toBytes()[0];
        int msgId = getMsgId();
        this.message[1] = (byte) (msgId / 256);
        this.message[2] = (byte) (msgId % 256);
        for (int i = 0; i < this.topics.length; i++) {
            byte[] StringToUTF = MqttUtils.StringToUTF(this.topics[i]);
            this.message = MqttUtils.concatArray(MqttUtils.concatArray(this.message, StringToUTF), 0, StringToUTF.length + this.message.length, this.topicsQoS, i, 1);
        }
        createMsgLength();
        return this.message;
    }
}
