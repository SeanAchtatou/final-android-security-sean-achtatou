package com.ibm.mqtt;

public class MqttPersistenceException extends MqttException {
    public MqttPersistenceException() {
    }

    public MqttPersistenceException(String str) {
        super(str);
    }
}
