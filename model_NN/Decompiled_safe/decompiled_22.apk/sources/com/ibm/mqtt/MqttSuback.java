package com.ibm.mqtt;

public class MqttSuback extends MqttPacket {
    public byte[] TopicsQoS;

    public MqttSuback() {
        setMsgType(9);
    }

    public MqttSuback(byte[] bArr, int i) {
        super(bArr);
        setMsgType(9);
        setMsgId(MqttUtils.toShort(bArr, i));
        this.TopicsQoS = MqttUtils.SliceByteArray(bArr, i + 2, bArr.length - (i + 2));
    }

    public void process(MqttProcessor mqttProcessor) {
        mqttProcessor.process(this);
    }

    public byte[] toBytes() {
        this.message = new byte[(this.TopicsQoS.length + 3)];
        this.message[0] = super.toBytes()[0];
        int msgId = getMsgId();
        this.message[1] = (byte) (msgId / 256);
        this.message[2] = (byte) (msgId % 256);
        for (int i = 0; i < this.TopicsQoS.length; i++) {
            this.message[i + 3] = this.TopicsQoS[i];
        }
        createMsgLength();
        return this.message;
    }
}
