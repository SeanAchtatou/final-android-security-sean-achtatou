package com.ibm.mqtt;

public class MqttConnack extends MqttPacket {
    public short returnCode;
    public boolean topicNameCompression;

    public MqttConnack() {
        setMsgType(2);
    }

    public MqttConnack(byte[] bArr, int i) {
        super(bArr);
        setMsgType(2);
        this.topicNameCompression = (bArr[i] & 1) != 0;
        this.returnCode = (short) bArr[i + 1];
    }

    public void process(MqttProcessor mqttProcessor) {
        mqttProcessor.process(this);
    }

    public byte[] toBytes() {
        byte b = 0;
        this.message = new byte[3];
        this.message[0] = super.toBytes()[0];
        byte[] bArr = this.message;
        if (this.topicNameCompression) {
            b = 1;
        }
        bArr[1] = b;
        this.message[2] = (byte) this.returnCode;
        createMsgLength();
        return this.message;
    }
}
