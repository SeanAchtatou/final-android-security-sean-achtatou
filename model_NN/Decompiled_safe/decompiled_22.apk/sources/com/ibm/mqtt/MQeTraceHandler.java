package com.ibm.mqtt;

public interface MQeTraceHandler {
    public static final short[] version = {2, 0, 0, 2};

    void setFilter(long j);

    void traceMessage(Object obj, short s, long j);

    void traceMessage(Object obj, short s, long j, Object obj2);

    void traceMessage(Object obj, short s, long j, Object obj2, Object obj3);

    void traceMessage(Object obj, short s, long j, Object obj2, Object obj3, Object obj4);

    void traceMessage(Object obj, short s, long j, Object obj2, Object obj3, Object obj4, Object obj5);
}
