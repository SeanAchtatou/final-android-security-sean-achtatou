package com.ibm.mqtt;

public class MqttDisconnect extends MqttPacket {
    public MqttDisconnect() {
        setMsgType(14);
    }

    public MqttDisconnect(byte[] bArr) {
        super(bArr);
        setMsgType(14);
    }

    public void process(MqttProcessor mqttProcessor) {
    }

    public byte[] toBytes() {
        this.message = new byte[1];
        this.message[0] = super.toBytes()[0];
        createMsgLength();
        return this.message;
    }
}
