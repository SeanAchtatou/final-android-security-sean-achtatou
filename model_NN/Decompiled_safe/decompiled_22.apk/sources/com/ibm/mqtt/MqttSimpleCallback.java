package com.ibm.mqtt;

public interface MqttSimpleCallback {
    void connectionLost() throws Exception;

    void publishArrived(String str, byte[] bArr, int i, boolean z) throws Exception;
}
