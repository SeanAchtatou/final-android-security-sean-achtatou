package com.ibm.mqtt;

public interface MqttProcessor {
    void process(MqttConnack mqttConnack);

    void process(MqttConnect mqttConnect);

    void process(MqttDisconnect mqttDisconnect);

    void process(MqttPingreq mqttPingreq);

    void process(MqttPingresp mqttPingresp);

    void process(MqttPuback mqttPuback);

    void process(MqttPubcomp mqttPubcomp);

    void process(MqttPublish mqttPublish);

    void process(MqttPubrec mqttPubrec);

    void process(MqttPubrel mqttPubrel);

    void process(MqttSuback mqttSuback);

    void process(MqttSubscribe mqttSubscribe);

    void process(MqttUnsuback mqttUnsuback);

    void process(MqttUnsubscribe mqttUnsubscribe);

    boolean supportTopicNameCompression();
}
