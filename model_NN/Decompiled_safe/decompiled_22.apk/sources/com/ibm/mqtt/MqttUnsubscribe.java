package com.ibm.mqtt;

import java.util.Vector;

public class MqttUnsubscribe extends MqttPacket {
    public String[] topics;

    public MqttUnsubscribe() {
        setMsgType(10);
    }

    public MqttUnsubscribe(byte[] bArr, int i) {
        super(bArr);
        setMsgType(10);
        setMsgId(MqttUtils.toShort(bArr, i));
        Vector UTFToStrings = MqttUtils.UTFToStrings(bArr, i + 2);
        int size = UTFToStrings.size();
        this.topics = new String[size];
        for (int i2 = 0; i2 < size; i2++) {
            this.topics[i2] = UTFToStrings.elementAt(i2).toString();
        }
    }

    private void uncompressTopic() {
    }

    public void compressTopic() {
    }

    public void process(MqttProcessor mqttProcessor) {
        mqttProcessor.process(this);
    }

    public byte[] toBytes() {
        this.message = new byte[3];
        this.message[0] = super.toBytes()[0];
        int msgId = getMsgId();
        this.message[1] = (byte) (msgId / 256);
        this.message[2] = (byte) (msgId % 256);
        for (String StringToUTF : this.topics) {
            this.message = MqttUtils.concatArray(this.message, MqttUtils.StringToUTF(StringToUTF));
        }
        createMsgLength();
        return this.message;
    }
}
