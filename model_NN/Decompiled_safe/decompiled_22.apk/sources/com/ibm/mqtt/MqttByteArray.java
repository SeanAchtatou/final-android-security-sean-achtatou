package com.ibm.mqtt;

public class MqttByteArray {
    private byte[] byteArray = null;

    public MqttByteArray(byte[] bArr) {
        this.byteArray = bArr;
    }

    public byte[] getByteArray() {
        return this.byteArray;
    }
}
