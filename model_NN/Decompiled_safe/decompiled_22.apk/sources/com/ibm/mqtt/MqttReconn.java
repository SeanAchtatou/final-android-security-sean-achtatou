package com.ibm.mqtt;

public class MqttReconn extends Thread {
    private MqttBaseClient client = null;

    public MqttReconn(MqttBaseClient mqttBaseClient) {
        this.client = mqttBaseClient;
    }

    public void run() {
        try {
            this.client.connectionLost();
            this.client.setConnectionLost(false);
        } catch (Throwable th) {
            MqttException mqttException = new MqttException("ConnectionLost exception caught");
            mqttException.initCause(th);
            this.client.setRegisteredThrowable(mqttException);
        }
    }
}
