package com.ibm.mqtt;

import java.util.Enumeration;

public class MqttEnumList implements Enumeration {
    private int count;
    private int index;
    private boolean keys;
    private MqttHashTable mqtt_enum;
    private MqttListItem ptr;
    private int size = this.mqtt_enum.size();

    public MqttEnumList(MqttHashTable mqttHashTable, boolean z) {
        this.mqtt_enum = mqttHashTable;
        this.keys = z;
        this.count = 0;
        this.index = 0;
        MqttListItem mqttListItem = this.mqtt_enum.hashTable[this.index];
        this.ptr = mqttListItem;
        if (mqttListItem == null) {
            this.ptr = advance(this.ptr);
        }
    }

    private MqttListItem advance(MqttListItem mqttListItem) {
        while (this.index + 1 < this.mqtt_enum.m_capacity) {
            this.index++;
            MqttListItem mqttListItem2 = this.mqtt_enum.hashTable[this.index];
            if (mqttListItem2 != null) {
                return mqttListItem2;
            }
        }
        return null;
    }

    public boolean hasMoreElements() {
        return this.count < this.size;
    }

    public Object nextElement() {
        MqttListItem mqttListItem = this.ptr;
        this.ptr = !this.ptr.isEnd() ? this.ptr.next : advance(this.ptr);
        this.count++;
        return this.keys ? new Long(mqttListItem.key) : mqttListItem.data;
    }
}
