package com.ibm.mqtt;

public class MqttUnsuback extends MqttPacket {
    public MqttUnsuback() {
        setMsgType(11);
    }

    public MqttUnsuback(byte[] bArr, int i) {
        super(bArr);
        setMsgType(11);
        setMsgId(MqttUtils.toShort(bArr, i));
    }

    public void process(MqttProcessor mqttProcessor) {
        mqttProcessor.process(this);
    }

    public byte[] toBytes() {
        this.message = new byte[3];
        this.message[0] = super.toBytes()[0];
        int msgId = getMsgId();
        this.message[1] = (byte) (msgId / 256);
        this.message[2] = (byte) (msgId % 256);
        createMsgLength();
        return this.message;
    }
}
