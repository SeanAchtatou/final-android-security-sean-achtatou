package com.ibm.mqtt;

public class MqttException extends Exception {
    private Throwable le;

    public MqttException() {
        this.le = null;
    }

    public MqttException(String str) {
        super(str);
        this.le = null;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MqttException(Throwable th) {
        super(th == null ? null : th.toString());
        this.le = null;
        this.le = th;
    }

    public Throwable getCause() {
        return this.le;
    }

    public Throwable initCause(Throwable th) {
        if (this.le != null) {
            throw new IllegalStateException();
        } else if (th == this) {
            throw new IllegalArgumentException();
        } else {
            this.le = th;
            return this;
        }
    }
}
