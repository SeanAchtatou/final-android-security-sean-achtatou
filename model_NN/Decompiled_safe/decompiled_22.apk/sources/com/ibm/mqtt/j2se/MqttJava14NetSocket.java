package com.ibm.mqtt.j2se;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class MqttJava14NetSocket extends Socket {
    public MqttJava14NetSocket(String str, int i, int i2) throws IOException {
        connect(new InetSocketAddress(str, i), i2);
    }
}
