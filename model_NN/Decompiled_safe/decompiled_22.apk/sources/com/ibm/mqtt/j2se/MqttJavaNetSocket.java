package com.ibm.mqtt.j2se;

import com.ibm.mqtt.MqttAdapter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

public class MqttJavaNetSocket implements MqttAdapter {
    static Class class$java$net$Socket;
    private Socket s = null;
    private boolean useShutdownMethods = false;

    public MqttJavaNetSocket() {
        Class cls;
        try {
            if (class$java$net$Socket == null) {
                cls = class$("java.net.Socket");
                class$java$net$Socket = cls;
            } else {
                cls = class$java$net$Socket;
            }
            cls.getMethod("shutdownInput", null);
            this.useShutdownMethods = true;
        } catch (NoSuchMethodException e) {
        }
    }

    static Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public void close() throws IOException {
        if (this.s != null) {
            this.s.close();
        }
    }

    public void closeInputStream() throws IOException {
        if (this.useShutdownMethods) {
            this.s.shutdownInput();
        } else {
            this.s.getInputStream().close();
        }
    }

    public void closeOutputStream() throws IOException {
        try {
            this.s.setSoLinger(true, 10);
        } catch (SocketException e) {
        }
        if (this.useShutdownMethods) {
            this.s.shutdownOutput();
        } else {
            this.s.getOutputStream().close();
        }
    }

    public InputStream getInputStream() throws IOException {
        if (this.s == null) {
            return null;
        }
        return this.s.getInputStream();
    }

    public OutputStream getOutputStream() throws IOException {
        if (this.s == null) {
            return null;
        }
        return this.s.getOutputStream();
    }

    public void setConnection(String str, int i) throws IOException {
        int lastIndexOf = str.lastIndexOf(58);
        if (lastIndexOf < 6) {
            lastIndexOf = str.indexOf(64);
        }
        try {
            this.s = new MqttJava14NetSocket(str.substring(6, lastIndexOf), Integer.parseInt(str.substring(lastIndexOf + 1)), i * 1000);
        } catch (NoClassDefFoundError e) {
            this.s = new Socket(str.substring(6, lastIndexOf), Integer.parseInt(str.substring(lastIndexOf + 1)));
        }
        if (i > 0) {
            this.s.setSoTimeout((i + 15) * 1000);
        }
    }
}
