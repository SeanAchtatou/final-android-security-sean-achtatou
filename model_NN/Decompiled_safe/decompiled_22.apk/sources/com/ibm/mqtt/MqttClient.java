package com.ibm.mqtt;

public class MqttClient extends MqttBaseClient implements IMqttClient {
    private MqttAdvancedCallback advCallbackHandler;
    private int conRetCode;
    private Object connAckLock;
    private String connection;
    private boolean isAppConnected;
    private MqttPersistence persistenceLayer;
    private Thread reader;
    private MqttSimpleCallback simpleCallbackHandler;
    private Class traceClass;

    protected MqttClient() {
        this.traceClass = null;
        this.persistenceLayer = null;
        this.reader = null;
        this.connAckLock = new Object();
        this.isAppConnected = false;
        this.advCallbackHandler = null;
        this.simpleCallbackHandler = null;
    }

    public MqttClient(String str) throws MqttException {
        this(str, null);
    }

    public MqttClient(String str, MqttPersistence mqttPersistence) throws MqttException {
        this.traceClass = null;
        this.persistenceLayer = null;
        this.reader = null;
        this.connAckLock = new Object();
        this.isAppConnected = false;
        this.advCallbackHandler = null;
        this.simpleCallbackHandler = null;
        initialise(str, mqttPersistence);
    }

    public static final IMqttClient createMqttClient(String str, MqttPersistence mqttPersistence) throws MqttException {
        return new MqttClient(str, mqttPersistence);
    }

    private void invalidApiInvocation() throws MqttException {
        throw new MqttException("MqttClient API called in a callback method! Use a different thread.");
    }

    private Class loadLocalBindings() throws MqttException {
        boolean z = false;
        Class<?> cls = null;
        try {
            cls = Class.forName("com.ibm.mqtt.local.MqttLocalBindingV2");
            z = true;
        } catch (ClassNotFoundException e) {
        }
        if (z) {
            return cls;
        }
        try {
            return Class.forName("com.ibm.mqtt.local.MqttLocalBindingV1");
        } catch (ClassNotFoundException e2) {
            MqttException mqttException = new MqttException("LocalBinding unavailable: Microbroker classes not found");
            mqttException.initCause(e2);
            throw mqttException;
        }
    }

    private Class loadTcpBindings() throws MqttException {
        boolean z = false;
        Class<?> cls = null;
        try {
            cls = Class.forName("com.ibm.mqtt.j2se.MqttJavaNetSocket");
            z = true;
        } catch (ClassNotFoundException e) {
        }
        if (z) {
            return cls;
        }
        try {
            return Class.forName("com.ibm.mqtt.midp.MqttMidpSocket");
        } catch (ClassNotFoundException e2) {
            MqttException mqttException = new MqttException("Cannot locate a J2SE Socket or J2ME StreamConnection class");
            mqttException.initCause(e2);
            throw mqttException;
        }
    }

    private void start(Class cls) throws MqttException {
        try {
            this.reader = new Thread(this);
            this.reader.start();
            super.setRetry(120);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MqttException(e);
        }
    }

    public void connect(String str, boolean z, short s) throws MqttException, MqttPersistenceException, MqttBrokerUnavailableException, MqttNotConnectedException {
        connect(str, z, s, null, 0, null, false);
    }

    public void connect(String str, boolean z, short s, String str2, int i, String str3, boolean z2) throws MqttException, MqttPersistenceException, MqttBrokerUnavailableException, MqttNotConnectedException {
        int i2;
        MQeTrace.trace(this, -30002, 1048580);
        if (!this.isAppConnected || !isSocketConnected()) {
            synchronized (this.connAckLock) {
                this.conRetCode = -1;
                super.connect(str, z, false, s, str2, i, str3, z2);
                try {
                    this.connAckLock.wait((long) (getRetry() * 1000));
                } catch (InterruptedException e) {
                }
                i2 = this.conRetCode;
            }
            MQeTrace.trace(this, -30003, 1048584);
            switch (i2) {
                case 0:
                    this.isAppConnected = true;
                    return;
                case 1:
                    MqttConnect mqttConnect = new MqttConnect();
                    throw new MqttException(new StringBuffer().append("WMQTT protocol name or version not supported:").append(mqttConnect.ProtoName).append(" Version:").append((int) mqttConnect.ProtoVersion).toString());
                case 2:
                    throw new MqttException("WMQTT ClientId is invalid");
                case 3:
                    throw new MqttBrokerUnavailableException("WMQTT Broker is unavailable");
                default:
                    tcpipDisconnect(true);
                    throw new MqttNotConnectedException(new StringBuffer().append("WMQTT ").append(msgTypes[2]).append(" not received").toString());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void connectionLost() throws Exception {
        MQeTrace.trace(this, -30004, MQeTrace.GROUP_INFO);
        super.connectionLost();
        if (this.simpleCallbackHandler != null) {
            this.simpleCallbackHandler.connectionLost();
            return;
        }
        throw new MqttNotConnectedException("WMQtt Connection Lost");
    }

    public void disconnect() throws MqttPersistenceException {
        MQeTrace.trace(this, -30005, 1048580);
        if (this.isAppConnected) {
            super.disconnect();
            this.isAppConnected = false;
        }
        MQeTrace.trace(this, -30006, 1048584);
    }

    public String getConnection() {
        return this.connection;
    }

    public MqttPersistence getPersistence() {
        return this.persistenceLayer;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: protected */
    public void initialise(String str, MqttPersistence mqttPersistence) throws MqttException {
        Class loadTcpBindings;
        this.connection = str;
        this.persistenceLayer = mqttPersistence;
        if (str.startsWith(IMqttClient.LOCAL_ID)) {
            loadTcpBindings = loadLocalBindings();
        } else if (str.startsWith(IMqttClient.TCP_ID)) {
            loadTcpBindings = loadTcpBindings();
            this.connection = str.replace('@', ':');
        } else {
            throw new MqttException(new StringBuffer().append("Unrecognised connection method:").append(str).toString());
        }
        super.initialise(this.connection, this.persistenceLayer, loadTcpBindings);
        try {
            this.traceClass = Class.forName("com.ibm.mqtt.trace.MQeTraceToBinaryFile");
        } catch (ClassNotFoundException e) {
            this.traceClass = null;
        }
        start(loadTcpBindings);
    }

    /* access modifiers changed from: protected */
    public void notifyAck(int i, int i2) {
        switch (i) {
            case 1:
                synchronized (this.connAckLock) {
                    this.conRetCode = i2;
                    this.connAckLock.notifyAll();
                }
                return;
            case 2:
            case 4:
            case 5:
            case 7:
            case 9:
            default:
                return;
            case 3:
            case 6:
                if (this.advCallbackHandler != null) {
                    this.advCallbackHandler.published(i2);
                    return;
                }
                return;
            case 8:
                if (this.advCallbackHandler != null) {
                    this.advCallbackHandler.subscribed(i2, getReturnedQoS(i2));
                    return;
                }
                return;
            case 10:
                if (this.advCallbackHandler != null) {
                    this.advCallbackHandler.unsubscribed(i2);
                    return;
                }
                return;
        }
    }

    public void ping() throws MqttException {
        if (Thread.currentThread().equals(this.reader)) {
            invalidApiInvocation();
        }
        pingOut();
    }

    public int publish(String str, byte[] bArr, int i, boolean z) throws MqttNotConnectedException, MqttPersistenceException, MqttException, IllegalArgumentException {
        MQeTrace.trace(this, -30007, 1048580);
        if (str == null) {
            throw new IllegalArgumentException("NULL topic");
        } else if (bArr == null) {
            throw new IllegalArgumentException("NULL message");
        } else if (str.indexOf(35) > -1 || str.indexOf(43) > -1) {
            throw new IllegalArgumentException("Topic contains '#' or '+'");
        } else {
            if (Thread.currentThread().equals(this.reader)) {
                invalidApiInvocation();
            }
            anyErrors();
            int publish = super.publish(str, bArr, i, z);
            MQeTrace.trace(this, -30008, 1048584);
            return publish;
        }
    }

    /* access modifiers changed from: protected */
    public void publishArrived(String str, byte[] bArr, int i, boolean z) throws Exception {
        MQeTrace.trace(this, -30009, 1048580, Integer.toString(str.length()), str.length() > 30 ? str.substring(0, 31) : str, Integer.toString(bArr.length), MqttUtils.toHexString(bArr, 0, 30));
        if (this.simpleCallbackHandler != null) {
            this.simpleCallbackHandler.publishArrived(str, bArr, i, z);
        }
        MQeTrace.trace(this, -30010, 1048584);
    }

    public void registerAdvancedHandler(MqttAdvancedCallback mqttAdvancedCallback) {
        this.advCallbackHandler = mqttAdvancedCallback;
        this.simpleCallbackHandler = mqttAdvancedCallback;
    }

    public void registerSimpleHandler(MqttSimpleCallback mqttSimpleCallback) {
        this.simpleCallbackHandler = mqttSimpleCallback;
    }

    public void startTrace() throws MqttException {
        if (this.traceClass != null) {
            MQeTrace.setFilter(-1);
            try {
                MQeTrace.setHandler((MQeTraceHandler) this.traceClass.newInstance());
            } catch (Exception e) {
                throw new MqttException(e);
            }
        } else {
            throw new MqttException("Trace classes (com.ibm.mqtt.trace.*) not found.\nCheck they are in wmqtt.jar.");
        }
    }

    public void stopTrace() {
        MQeTrace.setFilter(0);
        MQeTrace.setHandler(null);
    }

    public int subscribe(String[] strArr, int[] iArr) throws MqttNotConnectedException, MqttException, IllegalArgumentException {
        MQeTrace.trace(this, -30011, 1048580);
        if (strArr == null) {
            throw new IllegalArgumentException("NULL topic array");
        } else if (iArr == null) {
            throw new IllegalArgumentException("NULL requested QoS array");
        } else if (strArr.length != iArr.length) {
            throw new IllegalArgumentException(new StringBuffer().append("Array lengths unequal. Topics:").append(strArr.length).append(", QoS:").append(iArr.length).toString());
        } else {
            for (int i = 0; i < strArr.length; i++) {
                if (strArr[i] == null) {
                    throw new IllegalArgumentException(new StringBuffer().append("NULL topic in topic array at index ").append(i).toString());
                }
            }
            if (Thread.currentThread().equals(this.reader)) {
                invalidApiInvocation();
            }
            for (int i2 = 0; i2 < iArr.length; i2++) {
                if (iArr[i2] > 2) {
                    iArr[i2] = 2;
                } else if (iArr[i2] < 0) {
                    iArr[i2] = 0;
                }
            }
            anyErrors();
            int subscribe = super.subscribe(strArr, iArr);
            MQeTrace.trace(this, -30012, 1048584);
            return subscribe;
        }
    }

    public void terminate() {
        terminate(true);
    }

    public void terminate(boolean z) {
        if (this.isAppConnected && z) {
            try {
                disconnect();
            } catch (Exception e) {
            }
        }
        super.terminate();
        try {
            this.reader.join();
        } catch (InterruptedException e2) {
        }
    }

    public int unsubscribe(String[] strArr) throws MqttNotConnectedException, MqttException, IllegalArgumentException {
        MQeTrace.trace(this, -30013, 1048580);
        if (strArr == null) {
            throw new IllegalArgumentException("NULL topic array");
        }
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] == null) {
                throw new IllegalArgumentException(new StringBuffer().append("NULL topic in topic array at index ").append(i).toString());
            }
        }
        if (Thread.currentThread().equals(this.reader)) {
            invalidApiInvocation();
        }
        anyErrors();
        int unsubscribe = super.unsubscribe(strArr);
        MQeTrace.trace(this, -30014, 1048584);
        return unsubscribe;
    }
}
