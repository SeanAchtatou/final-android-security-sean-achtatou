package com.ibm.mqtt;

public class MqttConnect extends MqttPacket {
    public boolean CleanStart;
    protected String ClientId;
    public short KeepAlive;
    public String ProtoName = "MQIsdp";
    public short ProtoVersion = 3;
    public boolean TopicNameCompression;
    public boolean Will;
    public String WillMessage;
    public int WillQoS;
    public boolean WillRetain;
    public String WillTopic;

    public MqttConnect() {
        setMsgType(1);
    }

    public MqttConnect(byte[] bArr) {
        super(bArr);
        setMsgType(1);
    }

    public String getClientId() {
        return this.ClientId;
    }

    public void process(MqttProcessor mqttProcessor) {
    }

    public void setClientId(String str) throws MqttException {
        if (str.length() > 23) {
            throw new MqttException("MQIsdp ClientId > 23 bytes");
        }
        this.ClientId = str;
    }

    public byte[] toBytes() {
        byte b;
        byte b2 = 1;
        this.message = new byte[42];
        this.message[0] = super.toBytes()[0];
        byte[] StringToUTF = MqttUtils.StringToUTF(this.ProtoName);
        System.arraycopy(StringToUTF, 0, this.message, 1, StringToUTF.length);
        int length = StringToUTF.length + 1;
        int i = length + 1;
        this.message[length] = (byte) this.ProtoVersion;
        if (!this.TopicNameCompression) {
            b2 = 0;
        }
        byte b3 = this.CleanStart ? (byte) 2 : 0;
        if (this.Will) {
            b = (byte) ((this.WillRetain ? (byte) 32 : 0) | ((byte) ((this.WillQoS & 3) << 3)) | 4);
        } else {
            b = 0;
        }
        int i2 = i + 1;
        this.message[i] = (byte) (b2 | b3 | b);
        int i3 = i2 + 1;
        this.message[i2] = (byte) (this.KeepAlive / 256);
        int i4 = i3 + 1;
        this.message[i3] = (byte) (this.KeepAlive % 256);
        byte[] StringToUTF2 = MqttUtils.StringToUTF(this.ClientId);
        System.arraycopy(StringToUTF2, 0, this.message, i4, StringToUTF2.length);
        int length2 = i4 + StringToUTF2.length;
        if (this.Will) {
            byte[] StringToUTF3 = MqttUtils.StringToUTF(this.WillTopic);
            byte[] StringToUTF4 = MqttUtils.StringToUTF(this.WillMessage);
            this.message = MqttUtils.concatArray(MqttUtils.concatArray(this.message, 0, length2, StringToUTF3, 0, StringToUTF3.length), StringToUTF4);
            length2 += StringToUTF3.length + StringToUTF4.length;
        }
        this.message = MqttUtils.SliceByteArray(this.message, 0, length2);
        createMsgLength();
        return this.message;
    }
}
