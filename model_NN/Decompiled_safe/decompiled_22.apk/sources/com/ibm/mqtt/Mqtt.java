package com.ibm.mqtt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.commons.httpclient.ConnectMethod;

public class Mqtt implements MqttProcessor {
    public static final short CONNACK = 2;
    public static final short CONNECT = 1;
    public static final short DISCONNECT = 14;
    public static final short PINGREQ = 12;
    public static final short PINGRESP = 13;
    public static final short PUBACK = 4;
    public static final short PUBCOMP = 7;
    public static final short PUBLISH = 3;
    public static final short PUBREC = 5;
    public static final short PUBREL = 6;
    public static final short SUBACK = 9;
    public static final short SUBSCRIBE = 8;
    public static final short UNSUBACK = 11;
    public static final short UNSUBSCRIBE = 10;
    public static final String[] msgTypes = {null, ConnectMethod.NAME, "CONNACK", "PUBLISH", "PUBACK", "PUBREC", "PUBREL", "PUBCOMP", "SUBSCRIBE", "SUBACK", "UNSUBSCRIBE", "UNSUBACK", "PINGREQ", "PINGRESP", "DISCONNECT"};
    private boolean connected = false;
    protected String connection;
    private boolean connectionLost = false;
    private int curMsgId = 0;
    private boolean haveWill = false;
    private boolean isSocketConnected = false;
    private int keepAlivePeriod;
    private Hashtable outMsgIdsAllocated = new Hashtable();
    protected MqttException registeredException = null;
    private MqttAdapter socket = null;
    private Class socketClass = null;
    private Object streamReadLock = new Object();
    private Object streamWriteLock = new Object();
    private DataInputStream stream_in = null;
    private DataOutputStream stream_out = null;
    private boolean topicNameCompression = false;

    protected Mqtt() {
    }

    private MqttPacket decodePacket(byte[] bArr, int i, short s) throws MqttException {
        if (!isSocketConnected()) {
            return null;
        }
        switch (s) {
            case 1:
            case 14:
                return null;
            case 2:
                return new MqttConnack(bArr, i);
            case 3:
                return new MqttPublish(bArr, i);
            case 4:
                return new MqttPuback(bArr, i);
            case 5:
                return new MqttPubrec(bArr, i);
            case 6:
                return new MqttPubrel(bArr, i);
            case 7:
                return new MqttPubcomp(bArr, i);
            case 8:
                return new MqttSubscribe(bArr, i);
            case 9:
                return new MqttSuback(bArr, i);
            case 10:
                return new MqttUnsubscribe(bArr, i);
            case 11:
                return new MqttUnsuback(bArr, i);
            case 12:
                return new MqttPingreq(bArr, i);
            case 13:
                return new MqttPingresp(bArr, i);
            default:
                throw new MqttException(new StringBuffer().append("Mqtt: Unknown message type: ").append((int) s).toString());
        }
    }

    private void setSocketState(boolean z) {
        synchronized (this.streamWriteLock) {
            this.isSocketConnected = z;
        }
    }

    /* access modifiers changed from: protected */
    public final MqttPublish genPublishPacket(int i, int i2, String str, byte[] bArr, boolean z, boolean z2) {
        MqttPublish mqttPublish = new MqttPublish();
        mqttPublish.setMsgId(i);
        mqttPublish.setQos(i2);
        mqttPublish.topicName = str;
        mqttPublish.setPayload(bArr);
        mqttPublish.setDup(z2);
        mqttPublish.setRetain(z);
        if (this.topicNameCompression) {
            mqttPublish.compressTopic();
        }
        return mqttPublish;
    }

    /* access modifiers changed from: protected */
    public int getKeepAlivePeriod() {
        return this.keepAlivePeriod;
    }

    /* access modifiers changed from: protected */
    public boolean hasKeepAlive() {
        return this.keepAlivePeriod > 0;
    }

    /* access modifiers changed from: protected */
    public boolean hasWill() {
        return this.haveWill;
    }

    /* access modifiers changed from: protected */
    public void initialise(String str, Class cls) {
        this.connection = str;
        this.socketClass = cls;
    }

    /* access modifiers changed from: protected */
    public void initialiseOutMsgIds(Vector vector) {
        this.outMsgIdsAllocated.clear();
        this.curMsgId = 1;
        if (vector != null) {
            Enumeration elements = vector.elements();
            while (elements.hasMoreElements()) {
                Integer num = (Integer) elements.nextElement();
                this.outMsgIdsAllocated.put(num, num);
            }
        }
    }

    public synchronized boolean isConnected() {
        return this.connected;
    }

    /* access modifiers changed from: protected */
    public synchronized boolean isConnectionLost() {
        return this.connectionLost;
    }

    /* access modifiers changed from: protected */
    public boolean isSocketConnected() {
        boolean z;
        synchronized (this.streamWriteLock) {
            z = this.isSocketConnected;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public int nextMsgId() throws MqttException {
        if (this.outMsgIdsAllocated.size() == 65535) {
            throw new MqttException("All available msgIds in use:65535");
        }
        boolean z = false;
        while (!z) {
            if (this.curMsgId < 65535) {
                this.curMsgId++;
            } else {
                this.curMsgId = 1;
            }
            Integer num = new Integer(this.curMsgId);
            if (!this.outMsgIdsAllocated.contains(num)) {
                this.outMsgIdsAllocated.put(num, num);
                z = true;
            }
        }
        return this.curMsgId;
    }

    /* access modifiers changed from: protected */
    public void pingOut() throws MqttException {
        writePacket(new MqttPingreq());
    }

    /* access modifiers changed from: protected */
    public void process() throws Exception {
        MqttPacket readPacket = readPacket();
        if (readPacket != null) {
            readPacket.process(this);
        } else {
            System.out.println("Mqtt: Read a null packet from the socket");
        }
    }

    public void process(MqttConnack mqttConnack) {
        if (mqttConnack.returnCode == 0) {
            this.topicNameCompression = mqttConnack.topicNameCompression;
            setConnectionState(true);
        } else if (mqttConnack.returnCode == 1) {
            setConnectionState(false);
        } else if (mqttConnack.returnCode == 2) {
            setConnectionState(false);
        } else if (mqttConnack.returnCode == 3) {
            setConnectionState(false);
        }
        if (mqttConnack.returnCode != 0) {
            tcpipDisconnect(false);
        }
    }

    public void process(MqttConnect mqttConnect) {
    }

    public void process(MqttDisconnect mqttDisconnect) {
    }

    public void process(MqttPingreq mqttPingreq) {
        try {
            writePacket(new MqttPingresp());
        } catch (Exception e) {
        }
    }

    public void process(MqttPingresp mqttPingresp) {
    }

    public void process(MqttPuback mqttPuback) {
    }

    public void process(MqttPubcomp mqttPubcomp) {
    }

    public void process(MqttPublish mqttPublish) {
    }

    public void process(MqttPubrec mqttPubrec) {
    }

    public void process(MqttPubrel mqttPubrel) {
    }

    public void process(MqttSuback mqttSuback) {
    }

    public void process(MqttSubscribe mqttSubscribe) {
    }

    public void process(MqttUnsuback mqttUnsuback) {
    }

    public void process(MqttUnsubscribe mqttUnsubscribe) {
    }

    /* JADX WARN: Type inference failed for: r1v4, types: [int] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.ibm.mqtt.MqttPacket readPacket() throws com.ibm.mqtt.MqttException, java.io.InterruptedIOException, java.io.IOException {
        /*
            r10 = this;
            r1 = 1
            r0 = 0
            r2 = 5
            byte[] r4 = new byte[r2]
            java.lang.Object r8 = r10.streamReadLock
            monitor-enter(r8)
            java.io.DataInputStream r2 = r10.stream_in     // Catch:{ IOException -> 0x001d }
            r3 = 0
            r5 = 1
            int r2 = r2.read(r4, r3, r5)     // Catch:{ IOException -> 0x001d }
            if (r2 >= 0) goto L_0x002b
            java.io.EOFException r0 = new java.io.EOFException     // Catch:{ all -> 0x001a }
            java.lang.String r1 = "DataInputStream.read returned -1"
            r0.<init>(r1)     // Catch:{ all -> 0x001a }
            throw r0     // Catch:{ all -> 0x001a }
        L_0x001a:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x001a }
            throw r0
        L_0x001d:
            r0 = move-exception
            r1 = -30033(0xffffffffffff8aaf, float:NaN)
            r2 = 2097152(0x200000, double:1.0361308E-317)
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x001a }
            com.ibm.mqtt.MQeTrace.trace(r10, r1, r2, r4)     // Catch:{ all -> 0x001a }
            throw r0     // Catch:{ all -> 0x001a }
        L_0x002b:
            r2 = r1
            r3 = r1
            r1 = r0
        L_0x002e:
            java.io.DataInputStream r5 = r10.stream_in     // Catch:{ all -> 0x001a }
            int r5 = r5.read()     // Catch:{ all -> 0x001a }
            byte r5 = (byte) r5     // Catch:{ all -> 0x001a }
            r4[r2] = r5     // Catch:{ all -> 0x001a }
            r6 = r5 & 127(0x7f, float:1.78E-43)
            int r6 = r6 * r3
            int r6 = r6 + r1
            int r1 = r3 * 128
            int r7 = r2 + 1
            r2 = r5 & 128(0x80, float:1.794E-43)
            if (r2 != 0) goto L_0x0076
            int r1 = r6 + r7
            byte[] r9 = new byte[r1]     // Catch:{ all -> 0x001a }
        L_0x0047:
            if (r0 >= r7) goto L_0x0050
            byte r1 = r4[r0]     // Catch:{ all -> 0x001a }
            r9[r0] = r1     // Catch:{ all -> 0x001a }
            int r0 = r0 + 1
            goto L_0x0047
        L_0x0050:
            r1 = -30035(0xffffffffffff8aad, float:NaN)
            r2 = 2097152(0x200000, double:1.0361308E-317)
            java.lang.String r4 = java.lang.Integer.toString(r7)     // Catch:{ all -> 0x001a }
            java.lang.String r5 = java.lang.Integer.toString(r6)     // Catch:{ all -> 0x001a }
            r0 = r10
            com.ibm.mqtt.MQeTrace.trace(r0, r1, r2, r4, r5)     // Catch:{ all -> 0x001a }
            if (r6 <= 0) goto L_0x0068
            java.io.DataInputStream r0 = r10.stream_in     // Catch:{ all -> 0x001a }
            r0.readFully(r9, r7, r6)     // Catch:{ all -> 0x001a }
        L_0x0068:
            r0 = 0
            byte r0 = r9[r0]     // Catch:{ all -> 0x001a }
            int r0 = r0 >>> 4
            r0 = r0 & 15
            short r0 = (short) r0     // Catch:{ all -> 0x001a }
            monitor-exit(r8)     // Catch:{ all -> 0x001a }
            com.ibm.mqtt.MqttPacket r0 = r10.decodePacket(r9, r7, r0)
            return r0
        L_0x0076:
            r2 = r7
            r3 = r1
            r1 = r6
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ibm.mqtt.Mqtt.readPacket():com.ibm.mqtt.MqttPacket");
    }

    /* access modifiers changed from: protected */
    public void releaseMsgId(int i) {
        this.outMsgIdsAllocated.remove(new Integer(i));
    }

    /* access modifiers changed from: protected */
    public synchronized void setConnectionLost(boolean z) {
        this.connectionLost = z;
    }

    /* access modifiers changed from: protected */
    public synchronized void setConnectionState(boolean z) {
        this.connected = z;
    }

    /* access modifiers changed from: protected */
    public void setKeepAlive(int i) {
        this.keepAlivePeriod = i;
    }

    /* access modifiers changed from: protected */
    public void setRegisteredThrowable(Throwable th) {
        if (th == null || (th instanceof MqttException)) {
            this.registeredException = (MqttException) th;
        } else {
            this.registeredException = new MqttException(th);
        }
        setConnectionState(false);
        tcpipDisconnect(true);
        setConnectionLost(true);
    }

    /* access modifiers changed from: protected */
    public void subscribeOut(int i, String[] strArr, byte[] bArr, boolean z) throws Exception {
    }

    public boolean supportTopicNameCompression() {
        return this.topicNameCompression;
    }

    /* access modifiers changed from: protected */
    public void tcpipConnect(MqttConnect mqttConnect) throws IOException, Exception {
        synchronized (this.streamWriteLock) {
            tcpipDisconnect(true);
            try {
                this.socket = (MqttAdapter) this.socketClass.newInstance();
                this.socket.setConnection(this.connection, mqttConnect.KeepAlive);
                setSocketState(true);
                this.stream_in = new DataInputStream(this.socket.getInputStream());
                this.stream_out = new DataOutputStream(this.socket.getOutputStream());
                writePacket(mqttConnect);
            } catch (IOException e) {
                tcpipDisconnect(true);
                throw e;
            } catch (Exception e2) {
                tcpipDisconnect(true);
                throw e2;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void tcpipDisconnect(boolean z) {
        synchronized (this.streamWriteLock) {
            if (this.stream_out != null) {
                try {
                    this.socket.closeOutputStream();
                } catch (IOException e) {
                }
                this.stream_out = null;
            }
            if (z) {
                setSocketState(false);
                if (this.stream_in != null) {
                    try {
                        this.socket.closeInputStream();
                    } catch (IOException e2) {
                    }
                    this.stream_in = null;
                }
                if (this.socket != null) {
                    try {
                        this.socket.close();
                    } catch (IOException e3) {
                    }
                    this.socket = null;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void unsubscribeOut(int i, String[] strArr, boolean z) throws Exception {
    }

    /* access modifiers changed from: protected */
    public final void writePacket(MqttPacket mqttPacket) throws MqttException {
        synchronized (this.streamWriteLock) {
            if (this.stream_out != null) {
                try {
                    byte[] payload = mqttPacket.getPayload();
                    byte[] bytes = mqttPacket.toBytes();
                    mqttPacket.setDup(true);
                    this.stream_out.write(bytes);
                    if (payload != null) {
                        this.stream_out.write(payload);
                    }
                    this.stream_out.flush();
                } catch (IOException e) {
                    MQeTrace.trace(this, -30034, MQeTrace.GROUP_INFO, e.getMessage());
                    tcpipDisconnect(true);
                    throw new MqttException(e);
                } catch (Exception e2) {
                    e2.printStackTrace();
                    tcpipDisconnect(true);
                    throw new MqttException(e2);
                }
            } else {
                throw new MqttNotConnectedException();
            }
        }
    }
}
