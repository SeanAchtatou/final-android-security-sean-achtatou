package com.ibm.mqtt;

public class MqttPingresp extends MqttPacket {
    public MqttPingresp() {
        setMsgType(13);
    }

    public MqttPingresp(byte[] bArr, int i) {
        super(bArr);
        setMsgType(13);
    }

    public void process(MqttProcessor mqttProcessor) {
        mqttProcessor.process(this);
    }

    public byte[] toBytes() {
        this.message = new byte[1];
        this.message[0] = super.toBytes()[0];
        createMsgLength();
        return this.message;
    }
}
