package com.ibm.mqtt;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface MqttAdapter {
    void close() throws IOException;

    void closeInputStream() throws IOException;

    void closeOutputStream() throws IOException;

    InputStream getInputStream() throws IOException;

    OutputStream getOutputStream() throws IOException;

    void setConnection(String str, int i) throws IOException;
}
