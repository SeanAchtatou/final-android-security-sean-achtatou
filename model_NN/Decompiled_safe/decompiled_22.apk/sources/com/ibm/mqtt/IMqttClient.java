package com.ibm.mqtt;

public interface IMqttClient {
    public static final String LOCAL_ID = "local://";
    public static final String TCP_ID = "tcp://";

    void connect(String str, boolean z, short s) throws MqttException, MqttPersistenceException, MqttBrokerUnavailableException, MqttNotConnectedException;

    void connect(String str, boolean z, short s, String str2, int i, String str3, boolean z2) throws MqttException, MqttPersistenceException, MqttBrokerUnavailableException, MqttNotConnectedException;

    void disconnect() throws MqttPersistenceException;

    String getConnection();

    MqttPersistence getPersistence();

    int getRetry();

    boolean isConnected();

    boolean outstanding(int i);

    void ping() throws MqttException;

    int publish(String str, byte[] bArr, int i, boolean z) throws MqttNotConnectedException, MqttPersistenceException, MqttException, IllegalArgumentException;

    void registerAdvancedHandler(MqttAdvancedCallback mqttAdvancedCallback);

    void registerSimpleHandler(MqttSimpleCallback mqttSimpleCallback);

    void setRetry(int i);

    void startTrace() throws MqttException;

    void stopTrace();

    int subscribe(String[] strArr, int[] iArr) throws MqttNotConnectedException, MqttException, IllegalArgumentException;

    void terminate();

    int unsubscribe(String[] strArr) throws MqttNotConnectedException, MqttException, IllegalArgumentException;
}
