package com.ibm.mqtt;

public final class MqttListItem {
    public Object data;
    public long key;
    public MqttListItem next;

    public MqttListItem(long j, MqttListItem mqttListItem, Object obj) {
        this.data = obj;
        this.next = mqttListItem;
        this.key = j;
    }

    public boolean isEnd() {
        return this.next == null;
    }

    public boolean keysMatch(long j) {
        return this.key == j;
    }
}
