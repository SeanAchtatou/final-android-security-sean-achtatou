package com.ibm.mqtt;

public class MqttRetry implements MqttTimedEvent {
    protected long expires;
    private MqttPacket packet;
    private MqttBaseClient sessionRef;

    public MqttRetry(MqttBaseClient mqttBaseClient, MqttPacket mqttPacket, long j) {
        this.packet = mqttPacket;
        this.sessionRef = mqttBaseClient;
        this.expires = System.currentTimeMillis() + j;
    }

    public int getMsgId() {
        return this.packet.getMsgId();
    }

    public int getMsgType() {
        return this.packet.getMsgType();
    }

    public int getQoS() {
        return this.packet.getQos();
    }

    public long getTime() {
        return this.expires;
    }

    public boolean notifyEvent() throws Exception {
        if (!outstanding()) {
            return false;
        }
        if (this.sessionRef.isConnected()) {
            this.sessionRef.writePacket(this.packet);
            MQeTrace.trace(this, -30031, MQeTrace.GROUP_INFO, Mqtt.msgTypes[this.packet.getMsgType()], new Integer(this.packet.getMsgId()));
        }
        if (this.packet.getMsgType() == 12) {
            this.expires = System.currentTimeMillis() + ((long) (this.sessionRef.getKeepAlivePeriod() * 1000));
        } else {
            this.expires = System.currentTimeMillis() + ((long) (this.sessionRef.getRetry() * 1000));
        }
        return true;
    }

    public synchronized boolean outstanding() {
        return this.sessionRef.outstanding(this.packet.getMsgId());
    }

    /* access modifiers changed from: protected */
    public void setMessage(MqttPacket mqttPacket) {
        this.packet = mqttPacket;
    }

    public String toString() {
        return new StringBuffer().append("[").append(Mqtt.msgTypes[this.packet.getMsgType()]).append(" MsgId:").append(this.packet.getMsgId()).append(" Expires:").append(getTime()).append("]").toString();
    }
}
