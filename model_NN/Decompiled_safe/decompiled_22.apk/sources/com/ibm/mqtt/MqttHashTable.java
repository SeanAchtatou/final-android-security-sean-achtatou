package com.ibm.mqtt;

import java.util.Enumeration;

public class MqttHashTable {
    private static final int INITIAL_CAPACITY = 101;
    private static final int LOAD_FACTOR_DENOMINATOR = 4;
    private static final int LOAD_FACTOR_NUMERATOR = 3;
    private static int m_init_capacity;
    public MqttListItem[] hashTable;
    public int m_capacity;
    private int m_ceiling;
    private MqttListItem recycle_bin;
    private int recycle_length;
    private int size;

    public MqttHashTable() {
        this(101);
    }

    public MqttHashTable(int i) {
        this.size = 0;
        this.recycle_length = 0;
        int findPower = findPower(i);
        this.m_capacity = findPower;
        m_init_capacity = findPower;
        this.m_ceiling = (this.m_capacity * 3) / 4;
        this.hashTable = new MqttListItem[this.m_capacity];
    }

    private int findPower(int i) {
        int i2 = 2;
        do {
            i2 *= 2;
        } while (i2 < i);
        return i2;
    }

    private MqttListItem recycleCreate(long j, MqttListItem mqttListItem, Object obj) {
        if (this.recycle_bin == null) {
            return new MqttListItem(j, mqttListItem, obj);
        }
        MqttListItem mqttListItem2 = this.recycle_bin;
        this.recycle_bin = this.recycle_bin.next;
        this.recycle_length--;
        mqttListItem2.key = j;
        mqttListItem2.next = mqttListItem;
        mqttListItem2.data = obj;
        return mqttListItem2;
    }

    private void rehash(int i) {
        MqttListItem[] mqttListItemArr = this.hashTable;
        MqttListItem[] mqttListItemArr2 = new MqttListItem[this.m_capacity];
        this.hashTable = mqttListItemArr2;
        for (int i2 = 0; i2 < i; i2++) {
            MqttListItem mqttListItem = mqttListItemArr[i2];
            while (mqttListItem != null) {
                MqttListItem mqttListItem2 = mqttListItem.next;
                long j = mqttListItem.key;
                int i3 = (int) ((j ^ (j >>> 32)) & ((long) (this.m_capacity - 1)));
                mqttListItem.next = mqttListItemArr2[i3];
                mqttListItemArr2[i3] = mqttListItem;
                mqttListItem = mqttListItem2;
            }
        }
    }

    public void clear() {
        for (int i = 0; i < this.m_capacity; i++) {
            this.hashTable[i] = null;
        }
        this.size = 0;
    }

    public boolean contains(Object obj) {
        for (int i = 0; i < this.m_capacity; i++) {
            for (MqttListItem mqttListItem = this.hashTable[i]; mqttListItem != null; mqttListItem = mqttListItem.next) {
                if (mqttListItem.data.equals(obj)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containsKey(long j) {
        return get(j) != null;
    }

    public Enumeration elements() {
        return new MqttEnumList(this, false);
    }

    public Object get(long j) {
        int i = (int) (((j >>> 32) ^ j) & ((long) (this.m_capacity - 1)));
        MqttListItem mqttListItem = this.hashTable[i];
        for (MqttListItem mqttListItem2 = this.hashTable[i]; mqttListItem2 != null; mqttListItem2 = mqttListItem2.next) {
            if (mqttListItem2.keysMatch(j)) {
                return mqttListItem2.data;
            }
        }
        return null;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public Enumeration keys() {
        return new MqttEnumList(this, true);
    }

    public Object put(long j, Object obj) {
        if (this.size > this.m_ceiling) {
            int i = this.m_capacity;
            this.m_capacity <<= 1;
            this.m_ceiling = (this.m_capacity * 3) / 4;
            rehash(i);
            return put(j, obj);
        }
        int i2 = (int) (((j >>> 32) ^ j) & ((long) (this.m_capacity - 1)));
        MqttListItem mqttListItem = this.hashTable[i2];
        if (mqttListItem == null) {
            this.hashTable[i2] = recycleCreate(j, null, obj);
            this.size++;
            return null;
        }
        for (MqttListItem mqttListItem2 = mqttListItem; mqttListItem2 != null; mqttListItem2 = mqttListItem2.next) {
            if (mqttListItem2.keysMatch(j)) {
                Object obj2 = mqttListItem2.data;
                mqttListItem2.data = obj;
                return obj2;
            }
        }
        this.hashTable[i2] = recycleCreate(j, mqttListItem, obj);
        this.size++;
        return null;
    }

    public Object remove(long j) {
        if (this.size >= this.m_ceiling / 4 || this.size < (m_init_capacity << 1)) {
            int i = (int) (((j >>> 32) ^ j) & ((long) (this.m_capacity - 1)));
            MqttListItem mqttListItem = null;
            for (MqttListItem mqttListItem2 = this.hashTable[i]; mqttListItem2 != null; mqttListItem2 = mqttListItem2.next) {
                if (mqttListItem2.keysMatch(j)) {
                    if (mqttListItem == null) {
                        this.hashTable[i] = mqttListItem2.next;
                    } else {
                        mqttListItem.next = mqttListItem2.next;
                    }
                    this.size--;
                    if (this.recycle_length < this.size / 8) {
                        mqttListItem2.next = this.recycle_bin;
                        this.recycle_bin = mqttListItem2;
                        this.recycle_length++;
                    }
                    return mqttListItem2.data;
                }
                mqttListItem = mqttListItem2;
            }
            return null;
        }
        int i2 = this.m_capacity;
        this.m_capacity >>= 1;
        this.m_ceiling = (this.m_capacity * 3) / 4;
        rehash(i2);
        return remove(j);
    }

    public int size() {
        return this.size;
    }

    public final void view() {
        for (int i = 0; i < this.m_capacity; i++) {
            System.out.print(new StringBuffer().append("\nBucket ").append(i).append(":").toString());
            for (MqttListItem mqttListItem = this.hashTable[i]; mqttListItem != null; mqttListItem = mqttListItem.next) {
                System.out.print(new StringBuffer().append(" ").append(mqttListItem.data.toString()).toString());
            }
        }
        System.out.println(new StringBuffer().append("\nSize = ").append(this.size).append("\n").toString());
    }
}
