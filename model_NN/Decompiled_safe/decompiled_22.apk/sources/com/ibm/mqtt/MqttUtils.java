package com.ibm.mqtt;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

public final class MqttUtils {
    public static final String STRING_ENCODING = "UTF-8";

    public static final byte[] SliceByteArray(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    public static final byte[] StringToUTF(String str) {
        if (str == null) {
            return null;
        }
        try {
            byte[] bytes = str.getBytes(STRING_ENCODING);
            byte[] bArr = new byte[(bytes.length + 2)];
            bArr[0] = new Integer(bytes.length / 256).byteValue();
            bArr[1] = new Integer(bytes.length % 256).byteValue();
            System.arraycopy(bytes, 0, bArr, 2, bytes.length);
            return bArr;
        } catch (UnsupportedEncodingException e) {
            System.out.println("MQTT Client: Unsupported string encoding - UTF-8");
            return null;
        }
    }

    public static final String UTFToString(byte[] bArr) {
        return UTFToString(bArr, 0);
    }

    public static final String UTFToString(byte[] bArr, int i) {
        String str;
        if (bArr == null) {
            return null;
        }
        int i2 = ((bArr[i + 1] & 255) << 0) + ((bArr[i + 0] & 255) << 8);
        if (i2 + 2 > bArr.length) {
            return null;
        }
        if (i2 > 0) {
            try {
                str = new String(bArr, i + 2, i2, STRING_ENCODING);
            } catch (UnsupportedEncodingException e) {
                System.out.println("MQTT Client: Unsupported string encoding - UTF-8");
                str = null;
            }
        } else {
            str = "";
        }
        return str;
    }

    public static final Vector UTFToStrings(byte[] bArr, int i) {
        if (bArr == null) {
            return null;
        }
        Vector vector = new Vector();
        while (i <= bArr.length - 3) {
            int i2 = ((bArr[i] & 255) << 8) + ((bArr[i + 1] & 255) << 0);
            String UTFToString = UTFToString(bArr, i);
            if (UTFToString != null) {
                vector.addElement(UTFToString);
            }
            i += i2 + 2;
        }
        return vector;
    }

    public static final byte[] concatArray(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) {
        byte[] bArr3 = new byte[(i2 + i4)];
        System.arraycopy(bArr, i, bArr3, 0, i2);
        System.arraycopy(bArr2, i3, bArr3, i2, i4);
        return bArr3;
    }

    public static final byte[] concatArray(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
        System.arraycopy(bArr2, 0, bArr3, bArr.length, bArr2.length);
        return bArr3;
    }

    public static final long getExpiry(long j) {
        return (System.currentTimeMillis() / 1000) + ((3 * j) / 2);
    }

    public static final Vector getTopicsWithQoS(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        Vector vector = new Vector();
        int i = 0;
        while (i <= bArr.length - 4) {
            int i2 = ((bArr[i + 1] & 255) << 0) + ((bArr[i] & 255) << 8);
            StringBuffer stringBuffer = new StringBuffer(bArr.length);
            int i3 = i + 2;
            int i4 = i2 + i3;
            while (i3 < i4 && i4 < bArr.length) {
                stringBuffer.append((char) (bArr[i3] & 255));
                i3++;
            }
            if (stringBuffer.toString().length() > 0) {
                i = i3 + 1;
                stringBuffer.append((int) bArr[i3]);
                vector.addElement(stringBuffer.toString());
            } else {
                i = i3;
            }
        }
        return vector;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String toHexString(byte[] r5, int r6, int r7) {
        /*
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r0 = ""
            r2.<init>(r0)
            if (r6 >= 0) goto L_0x000a
            r6 = 0
        L_0x000a:
            r1 = r6
        L_0x000b:
            int r0 = r6 + r7
            if (r1 >= r0) goto L_0x0014
            int r0 = r5.length
            int r0 = r0 + -1
            if (r1 <= r0) goto L_0x0019
        L_0x0014:
            java.lang.String r0 = r2.toString()
            return r0
        L_0x0019:
            byte r0 = r5[r1]
            if (r0 >= 0) goto L_0x001f
            int r0 = r0 + 256
        L_0x001f:
            r3 = 16
            if (r0 >= r3) goto L_0x0041
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.String r4 = "0"
            java.lang.StringBuffer r3 = r3.append(r4)
            java.lang.String r0 = java.lang.Integer.toHexString(r0)
            java.lang.StringBuffer r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            r2.append(r0)
        L_0x003d:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x000b
        L_0x0041:
            java.lang.String r0 = java.lang.Integer.toHexString(r0)
            r2.append(r0)
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ibm.mqtt.MqttUtils.toHexString(byte[], int, int):java.lang.String");
    }

    public static final int toShort(byte[] bArr, int i) {
        return ((((short) bArr[i + 0]) & 255) << 8) + (((short) bArr[i + 1]) & 255);
    }
}
