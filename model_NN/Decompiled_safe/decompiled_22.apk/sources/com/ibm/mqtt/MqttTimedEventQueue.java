package com.ibm.mqtt;

public class MqttTimedEventQueue extends Thread {
    private boolean canDeliverEvents = false;
    private MqttTimedEvent[] m_array;
    private int m_head = 0;
    private int m_tail = 0;
    private boolean running = false;
    private MqttBaseClient session = null;
    private boolean stopping = false;

    public MqttTimedEventQueue(int i, MqttBaseClient mqttBaseClient) {
        this.m_array = new MqttTimedEvent[(i < 1 ? 4 : i)];
        this.session = mqttBaseClient;
    }

    private int adjust(int i) {
        int length = this.m_array.length;
        return i < length ? i : i - length;
    }

    private void expand_array() {
        int length = this.m_array.length;
        MqttTimedEvent[] mqttTimedEventArr = new MqttTimedEvent[(length * 2)];
        System.arraycopy(this.m_array, this.m_head, mqttTimedEventArr, this.m_head, length - this.m_head);
        System.arraycopy(this.m_array, 0, mqttTimedEventArr, length, this.m_tail);
        this.m_tail = length + this.m_tail;
        this.m_array = mqttTimedEventArr;
    }

    public synchronized void canDeliverEvents(boolean z) {
        this.canDeliverEvents = z;
        notifyAll();
    }

    public void close() {
        synchronized (this) {
            this.running = false;
            this.stopping = true;
            notifyAll();
        }
        try {
            join();
        } catch (InterruptedException e) {
        }
    }

    public synchronized void enqueue(MqttTimedEvent mqttTimedEvent) throws MqttException {
        long time = mqttTimedEvent.getTime();
        if (this.m_head == this.m_tail || time < this.m_array[this.m_head].getTime()) {
            int i = this.m_head - 1;
            this.m_head = i;
            if (i < 0) {
                this.m_head = this.m_array.length - 1;
            }
            this.m_array[this.m_head] = mqttTimedEvent;
            if (this.m_head == this.m_tail) {
                expand_array();
            }
            notifyAll();
        } else {
            int length = (this.m_tail < this.m_head ? this.m_tail + this.m_array.length : this.m_tail) - 1;
            while (length >= this.m_head) {
                if (time < this.m_array[adjust(length)].getTime()) {
                    this.m_array[adjust(length + 1)] = this.m_array[adjust(length)];
                    length--;
                } else {
                    this.m_array[adjust(length + 1)] = mqttTimedEvent;
                    this.m_tail = adjust(this.m_tail + 1);
                    if (this.m_head == this.m_tail) {
                        expand_array();
                    }
                }
            }
            String stringBuffer = new StringBuffer().append(new StringBuffer().append("MqttTimedEventQueue enqueue out of bounds").append("\nAdding event:").append(mqttTimedEvent.toString()).toString()).append("\nEvent queue:").append(toString()).toString();
            System.out.println(stringBuffer);
            throw new MqttException(stringBuffer);
        }
    }

    public synchronized boolean isEmpty() {
        return this.m_head == this.m_tail;
    }

    public synchronized void resetTimedEventQueue() {
        synchronized (this) {
            this.m_head = 0;
            this.m_tail = 0;
            for (int i = 0; i < this.m_array.length; i++) {
                this.m_array[i] = null;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x005e, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0095, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0096, code lost:
        if (r2 == false) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        enqueue(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x009b, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00af, code lost:
        r2 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00af A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:28:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            r4 = 1
            r3 = 0
            r1 = 0
            boolean r0 = r10.stopping
            if (r0 != 0) goto L_0x0009
            r10.running = r4
        L_0x0009:
            r2 = r1
        L_0x000a:
            boolean r0 = r10.running
            if (r0 == 0) goto L_0x0018
            boolean r0 = r10.stopping
            if (r0 != 0) goto L_0x0018
            monitor-enter(r10)     // Catch:{ InterruptedException -> 0x002e, Throwable -> 0x009d }
            boolean r0 = r10.stopping     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x0019
            monitor-exit(r10)     // Catch:{ all -> 0x002b }
        L_0x0018:
            return
        L_0x0019:
            boolean r0 = r10.running     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x003e
            int r0 = r10.m_head     // Catch:{ all -> 0x002b }
            int r5 = r10.m_tail     // Catch:{ all -> 0x002b }
            if (r0 == r5) goto L_0x0027
            boolean r0 = r10.canDeliverEvents     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x003e
        L_0x0027:
            r10.wait()     // Catch:{ all -> 0x002b }
            goto L_0x0019
        L_0x002b:
            r0 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x002b }
            throw r0     // Catch:{ InterruptedException -> 0x002e, Throwable -> 0x009d }
        L_0x002e:
            r0 = move-exception
            r0 = r2
        L_0x0030:
            if (r0 == 0) goto L_0x003c
            boolean r2 = r0.notifyEvent()     // Catch:{ Exception -> 0x0094, Throwable -> 0x00af }
            if (r2 == 0) goto L_0x003b
            r10.enqueue(r0)     // Catch:{ Exception -> 0x0094, Throwable -> 0x00af }
        L_0x003b:
            r0 = r1
        L_0x003c:
            r2 = r0
            goto L_0x000a
        L_0x003e:
            boolean r0 = r10.running     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x005d
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x002b }
            com.ibm.mqtt.MqttTimedEvent[] r0 = r10.m_array     // Catch:{ all -> 0x002b }
            int r7 = r10.m_head     // Catch:{ all -> 0x002b }
            r0 = r0[r7]     // Catch:{ all -> 0x002b }
            long r7 = r0.getTime()     // Catch:{ all -> 0x002b }
            int r0 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r0 >= 0) goto L_0x0060
            boolean r0 = r10.running     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x0060
            long r5 = r7 - r5
            r10.wait(r5)     // Catch:{ all -> 0x002b }
        L_0x005d:
            monitor-exit(r10)     // Catch:{ all -> 0x002b }
            r0 = r2
            goto L_0x0030
        L_0x0060:
            boolean r0 = r10.canDeliverEvents     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0078
            com.ibm.mqtt.MqttBaseClient r5 = r10.session     // Catch:{ all -> 0x002b }
            com.ibm.mqtt.MqttTimedEvent[] r0 = r10.m_array     // Catch:{ all -> 0x002b }
            int r6 = r10.m_head     // Catch:{ all -> 0x002b }
            r0 = r0[r6]     // Catch:{ all -> 0x002b }
            com.ibm.mqtt.MqttRetry r0 = (com.ibm.mqtt.MqttRetry) r0     // Catch:{ all -> 0x002b }
            int r0 = r0.getMsgId()     // Catch:{ all -> 0x002b }
            boolean r0 = r5.outstanding(r0)     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x005d
        L_0x0078:
            com.ibm.mqtt.MqttTimedEvent[] r0 = r10.m_array     // Catch:{ all -> 0x002b }
            int r5 = r10.m_head     // Catch:{ all -> 0x002b }
            r2 = r0[r5]     // Catch:{ all -> 0x002b }
            com.ibm.mqtt.MqttTimedEvent[] r0 = r10.m_array     // Catch:{ all -> 0x002b }
            int r5 = r10.m_head     // Catch:{ all -> 0x002b }
            int r6 = r5 + 1
            r10.m_head = r6     // Catch:{ all -> 0x002b }
            r6 = 0
            r0[r5] = r6     // Catch:{ all -> 0x002b }
            int r0 = r10.m_head     // Catch:{ all -> 0x002b }
            com.ibm.mqtt.MqttTimedEvent[] r5 = r10.m_array     // Catch:{ all -> 0x002b }
            int r5 = r5.length     // Catch:{ all -> 0x002b }
            if (r0 != r5) goto L_0x005d
            r0 = 0
            r10.m_head = r0     // Catch:{ all -> 0x002b }
            goto L_0x005d
        L_0x0094:
            r2 = move-exception
            r2 = r3
        L_0x0096:
            if (r2 != 0) goto L_0x003b
            r10.enqueue(r0)     // Catch:{ MqttException -> 0x00ad, Throwable -> 0x00af }
            r2 = r4
            goto L_0x0096
        L_0x009d:
            r0 = move-exception
            r9 = r0
            r0 = r2
            r2 = r9
        L_0x00a1:
            com.ibm.mqtt.MqttBaseClient r5 = r10.session
            if (r5 == 0) goto L_0x00aa
            com.ibm.mqtt.MqttBaseClient r5 = r10.session
            r5.setRegisteredThrowable(r2)
        L_0x00aa:
            r2 = r0
            goto L_0x000a
        L_0x00ad:
            r5 = move-exception
            goto L_0x0096
        L_0x00af:
            r2 = move-exception
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ibm.mqtt.MqttTimedEventQueue.run():void");
    }

    public synchronized String toString() {
        String str;
        int length = this.m_head <= this.m_tail ? this.m_tail : this.m_array.length;
        str = "[";
        for (int i = this.m_head; i < length; i++) {
            str = new StringBuffer().append(str).append(" ").append(this.m_array[i].toString()).toString();
        }
        if (length == this.m_array.length) {
            for (int i2 = 0; i2 < this.m_tail; i2++) {
                str = new StringBuffer().append(str).append(" ").append(this.m_array[i2].toString()).toString();
            }
        }
        if (this.m_head != this.m_tail) {
            str = new StringBuffer().append(str).append(" ").toString();
        }
        return new StringBuffer().append(str).append("]").toString();
    }
}
