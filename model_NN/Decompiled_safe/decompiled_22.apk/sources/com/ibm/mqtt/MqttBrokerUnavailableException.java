package com.ibm.mqtt;

public class MqttBrokerUnavailableException extends MqttException {
    public MqttBrokerUnavailableException() {
    }

    public MqttBrokerUnavailableException(String str) {
        super(str);
    }
}
