package com.ibm.mqtt;

public interface MqttPersistence {
    void addReceivedMessage(int i, byte[] bArr) throws MqttPersistenceException;

    void addSentMessage(int i, byte[] bArr) throws MqttPersistenceException;

    void close();

    void delReceivedMessage(int i) throws MqttPersistenceException;

    void delSentMessage(int i) throws MqttPersistenceException;

    byte[][] getAllReceivedMessages() throws MqttPersistenceException;

    byte[][] getAllSentMessages() throws MqttPersistenceException;

    void open(String str, String str2) throws MqttPersistenceException;

    void reset() throws MqttPersistenceException;

    void updSentMessage(int i, byte[] bArr) throws MqttPersistenceException;
}
