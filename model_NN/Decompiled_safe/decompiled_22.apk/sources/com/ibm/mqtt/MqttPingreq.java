package com.ibm.mqtt;

public class MqttPingreq extends MqttPacket {
    public MqttPingreq() {
        setMsgType(12);
    }

    public MqttPingreq(byte[] bArr, int i) {
        super(bArr);
        setMsgType(12);
    }

    public void process(MqttProcessor mqttProcessor) {
        mqttProcessor.process(this);
    }

    public byte[] toBytes() {
        this.message = new byte[1];
        this.message[0] = super.toBytes()[0];
        createMsgLength();
        return this.message;
    }
}
