package com.ibm.mqtt;

public class MqttNotConnectedException extends MqttException {
    public MqttNotConnectedException() {
    }

    public MqttNotConnectedException(String str) {
        super(str);
    }
}
