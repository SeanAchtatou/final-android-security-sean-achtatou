package com.ibm.mqtt;

public class MQeTrace {
    public static final boolean DEBUG = false;
    public static final long GROUP_ADAPTER_CALLING = 16;
    public static final long GROUP_ADAPTER_RETURNED = 32;
    public static final long GROUP_ADMINISTRATION = 16384;
    public static final long GROUP_API = 1048576;
    public static final long GROUP_BRIDGE = 256;
    public static final long GROUP_CHANNEL_MANAGEMENT = 1024;
    public static final long GROUP_COMMS_INCOMING = 262144;
    public static final long GROUP_COMMS_OUTGOING = 512;
    public static final long GROUP_ENTRY = 4;
    public static final long GROUP_ERROR = 1;
    public static final long GROUP_EXCEPTION = 32768;
    public static final long GROUP_EXIT = 8;
    public static final long GROUP_INFO = 2097152;
    public static final long GROUP_JMS = 65536;
    public static final long GROUP_MASK_ALL = -1;
    public static final long GROUP_MASK_DEFAULT = -8388609;
    public static final long GROUP_MASK_IBM_DEFINED = 281474976710655L;
    public static final long GROUP_MASK_NONE = 0;
    public static final long GROUP_MASK_USER_DEFINED = -281474976710656L;
    public static final long GROUP_MESSAGE_STATE = 2048;
    public static final long GROUP_MESSAGE_STORE = 8192;
    public static final long GROUP_MQSERIES = 8388608;
    public static final long GROUP_QUEUE_MANAGER = 4194304;
    public static final long GROUP_RULE_CALLING = 64;
    public static final long GROUP_RULE_RETURNED = 128;
    public static final long GROUP_SECURITY = 131072;
    public static final long GROUP_THREAD_CONTROL = 4096;
    public static final long GROUP_TRANSACTION = 524288;
    public static final long GROUP_USER_DEFINED_1 = 281474976710656L;
    public static final long GROUP_USER_DEFINED_2 = 562949953421312L;
    public static final long GROUP_USER_DEFINED_3 = 1125899906842624L;
    public static final long GROUP_USER_DEFINED_4 = 2251799813685248L;
    public static final long GROUP_WARNING = 2;
    public static final short TRACE_NUMBER_USER_DEFINED_MAX = Short.MAX_VALUE;
    public static final short TRACE_NUMBER_USER_DEFINED_MIN = 1;
    private static volatile long filterMask = GROUP_MASK_DEFAULT;
    private static String lock = "";
    private static String nullStr = "<null>";
    private static volatile MQeTraceHandler traceHandler = null;
    public static short[] version = {2, 0, 0, 2};

    public static long getFilter() {
        return filterMask;
    }

    public static MQeTraceHandler getHandler() {
        return traceHandler;
    }

    public static void setFilter(long j) {
        synchronized (lock) {
            trace(null, -21300, 1048580);
            long j2 = filterMask;
            filterMask = j;
            if (traceHandler != null) {
                traceHandler.setFilter(j);
            }
            trace(null, -21301, 1048584);
        }
    }

    public static MQeTraceHandler setHandler(MQeTraceHandler mQeTraceHandler) {
        MQeTraceHandler mQeTraceHandler2;
        synchronized (lock) {
            mQeTraceHandler2 = traceHandler;
            traceHandler = mQeTraceHandler;
            if (traceHandler != null) {
                traceHandler.setFilter(getFilter());
            }
            if (mQeTraceHandler2 != null) {
                mQeTraceHandler2.setFilter(0);
            }
        }
        return mQeTraceHandler2;
    }

    public static void trace(Object obj, short s, long j) {
        MQeTraceHandler mQeTraceHandler;
        if ((filterMask & j) != 0 && (mQeTraceHandler = traceHandler) != null) {
            mQeTraceHandler.traceMessage(obj, s, j);
        }
    }

    public static void trace(Object obj, short s, long j, Object obj2) {
        MQeTraceHandler mQeTraceHandler;
        if ((filterMask & j) != 0 && (mQeTraceHandler = traceHandler) != null) {
            mQeTraceHandler.traceMessage(obj, s, j, obj2 == null ? nullStr : obj2);
        }
    }

    public static void trace(Object obj, short s, long j, Object obj2, Object obj3) {
        MQeTraceHandler mQeTraceHandler;
        if ((filterMask & j) != 0 && (mQeTraceHandler = traceHandler) != null) {
            mQeTraceHandler.traceMessage(obj, s, j, obj2 == null ? nullStr : obj2, obj3 == null ? nullStr : obj3);
        }
    }

    public static void trace(Object obj, short s, long j, Object obj2, Object obj3, Object obj4) {
        MQeTraceHandler mQeTraceHandler;
        if ((filterMask & j) != 0 && (mQeTraceHandler = traceHandler) != null) {
            mQeTraceHandler.traceMessage(obj, s, j, obj2 == null ? nullStr : obj2, obj3 == null ? nullStr : obj3, obj4 == null ? nullStr : obj4);
        }
    }

    public static void trace(Object obj, short s, long j, Object obj2, Object obj3, Object obj4, Object obj5) {
        MQeTraceHandler mQeTraceHandler;
        if ((filterMask & j) != 0 && (mQeTraceHandler = traceHandler) != null) {
            mQeTraceHandler.traceMessage(obj, s, j, obj2 == null ? nullStr : obj2, obj3 == null ? nullStr : obj3, obj4 == null ? nullStr : obj4, obj5 == null ? nullStr : obj5);
        }
    }
}
