package com.ibm.mqtt;

public abstract class MqttPacket {
    public static final int MAX_CLIENT_ID_LEN = 23;
    public static final int MAX_MSGID = 65535;
    private boolean dup;
    protected byte[] message;
    private int msgId = 0;
    private int msgLength;
    private short msgType;
    private byte[] payload = null;
    private int qos;
    private boolean retain;

    public MqttPacket() {
    }

    public MqttPacket(byte[] bArr) {
        boolean z = true;
        byte b = bArr[0];
        this.msgType = getMsgType(b);
        this.retain = (b & 1) != 0;
        this.dup = ((b >>> 3) & 1) == 0 ? false : z;
        this.qos = (b >>> 1) & 3;
    }

    protected static short getMsgType(byte b) {
        return (short) ((b >>> 4) & 15);
    }

    /* access modifiers changed from: protected */
    public void createMsgLength() {
        int length = this.message.length - 1;
        if (this.payload != null) {
            length += this.payload.length;
        }
        this.msgLength = length;
        byte[] bArr = new byte[4];
        int i = length;
        int i2 = 0;
        while (true) {
            int i3 = i % 128;
            int i4 = i / 128;
            if (i4 > 0) {
                i3 |= 128;
            }
            int i5 = i2 + 1;
            bArr[i2] = (byte) i3;
            if (i4 <= 0) {
                byte[] bArr2 = new byte[(this.message.length + i5)];
                bArr2[0] = this.message[0];
                System.arraycopy(bArr, 0, bArr2, 1, i5);
                System.arraycopy(this.message, 1, bArr2, i5 + 1, this.message.length - 1);
                this.message = bArr2;
                return;
            }
            i2 = i5;
            i = i4;
        }
    }

    public byte[] getMessage() {
        return this.message;
    }

    public int getMsgId() {
        return this.msgId;
    }

    public int getMsgLength() {
        return this.msgLength;
    }

    public short getMsgType() {
        return this.msgType;
    }

    public byte[] getPayload() {
        return this.payload;
    }

    public int getQos() {
        return this.qos;
    }

    public boolean isDup() {
        return this.dup;
    }

    public boolean isRetain() {
        return this.retain;
    }

    public abstract void process(MqttProcessor mqttProcessor);

    public void setDup(boolean z) {
        this.dup = z;
    }

    public void setMessage(byte[] bArr) {
        this.message = bArr;
    }

    public void setMsgId(int i) {
        this.msgId = i;
    }

    public void setMsgLength(int i) {
        this.msgLength = i;
    }

    public void setMsgType(short s) {
        this.msgType = s;
    }

    public void setPayload(byte[] bArr) {
        this.payload = bArr;
    }

    public void setQos(int i) {
        this.qos = i;
    }

    public void setRetain(boolean z) {
        this.retain = z;
    }

    public byte[] toBytes() {
        int i = 1;
        byte[] bArr = {(byte) ((this.msgType << 4) & 240)};
        if ((this.msgType == 11) || ((this.msgType == 9) | (this.msgType == 8) | (this.msgType == 10))) {
            this.qos = 1;
        }
        byte b = (byte) ((this.qos & 3) << 1);
        byte b2 = (byte) (this.dup ? 8 : 0);
        if (!this.retain) {
            i = 0;
        }
        bArr[0] = (byte) (b2 | ((byte) i) | b | bArr[0]);
        return bArr;
    }
}
