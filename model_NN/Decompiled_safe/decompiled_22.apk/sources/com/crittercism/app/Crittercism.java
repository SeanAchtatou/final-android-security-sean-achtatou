package com.crittercism.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.crittercism.NotificationActivity;
import crittercism.android.b;
import crittercism.android.d;
import crittercism.android.e;
import crittercism.android.i;
import crittercism.android.k;
import crittercism.android.l;
import crittercism.android.m;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import org.apache.commons.httpclient.HttpState;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Crittercism {
    public static final String a = d.b();
    private static Crittercism c = new Crittercism();
    final Handler b = new Handler() {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            Bundle data = message.getData();
            if (data.containsKey("notification")) {
                try {
                    Intent intent = new Intent(Crittercism.this.h, NotificationActivity.class);
                    intent.setFlags(272629760);
                    intent.putExtra("com.crittercism.notification", data.getString("notification"));
                    Crittercism.this.h.startActivity(intent);
                } catch (Exception e) {
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public b d;
    private d e;
    private e f = null;
    private boolean g = false;
    /* access modifiers changed from: private */
    public Context h = null;
    private float i = 1.0f;
    private i j = new i();
    private m k = new m();
    private k l = new k();
    private l m = new l();
    private boolean n = false;
    private boolean o = false;
    private boolean p = false;
    private boolean q = false;
    private String r = "";
    private int s = 0;
    private String t = null;
    private String u = "";
    private String v = "Developer Reply";
    /* access modifiers changed from: private */
    public String w = "com.crittercism/dumps";
    private boolean x = false;

    class a implements Thread.UncaughtExceptionHandler {
        private Thread.UncaughtExceptionHandler b;

        public a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.b = uncaughtExceptionHandler;
        }

        public final void uncaughtException(Thread thread, Throwable th) {
            try {
                StringWriter stringWriter = new StringWriter();
                th.printStackTrace(new PrintWriter(stringWriter));
                if (!Crittercism.getOptOutStatus()) {
                    Log.e("Crittercism", stringWriter.toString());
                }
            } catch (Exception e) {
            }
            try {
                if (Crittercism.a() == null) {
                    throw new Exception("Failed to log error to Crittercism.");
                }
                if (!Crittercism.getOptOutStatus()) {
                    Crittercism.v();
                    Crittercism.a().a(th);
                    Crittercism.a().f();
                    if (Crittercism.a().m().e().size() > 0) {
                        i d = i.d();
                        d.a(Crittercism.a().m().e());
                        Crittercism.a().a(d);
                        i m = Crittercism.a().m();
                        try {
                            SharedPreferences.Editor edit = Crittercism.a().k().getSharedPreferences("com.crittercism.loads", 0).edit();
                            edit.remove(i.a());
                            edit.putString(i.a(), m.c().toString());
                            if (!edit.commit()) {
                                throw new Exception("commit failed");
                            }
                        } catch (Exception e2) {
                        }
                    }
                    if (Crittercism.a().n().e().size() > 0) {
                        m n = Crittercism.a().n();
                        try {
                            SharedPreferences.Editor edit2 = Crittercism.a().k().getSharedPreferences("com.crittercism.crashes", 0).edit();
                            edit2.remove(m.a());
                            edit2.putString(m.a(), n.b().toString());
                            if (!edit2.commit()) {
                                throw new Exception("commit failed");
                            }
                        } catch (Exception e3) {
                        }
                    }
                    if (Crittercism.a().o().e().size() > 0) {
                        k o = Crittercism.a().o();
                        try {
                            SharedPreferences.Editor edit3 = Crittercism.a().k().getSharedPreferences("com.crittercism.exceptions", 0).edit();
                            edit3.remove(k.a());
                            edit3.putString(k.a(), o.b().toString());
                            if (!edit3.commit()) {
                                throw new Exception("commit failed");
                            }
                        } catch (Exception e4) {
                        }
                    }
                }
                if (this.b != null && !(this.b instanceof a)) {
                    this.b.uncaughtException(thread, th);
                }
            } catch (Exception e5) {
                Log.w("Crittercism", "Failed to log error with Crittercism.  Please contact us at support@crittercism.com.");
                "Did not log error to Crittercism.  EXCEPTION: " + e5.getClass().getName();
                if (this.b != null && !(this.b instanceof a)) {
                    this.b.uncaughtException(thread, th);
                }
            } catch (Throwable th2) {
                if (this.b != null && !(this.b instanceof a)) {
                    this.b.uncaughtException(thread, th);
                }
                throw th2;
            }
        }
    }

    private Crittercism() {
    }

    private static long a(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.s", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        long j2 = 0;
        try {
            j2 = simpleDateFormat.parse(simpleDateFormat.format(date)).getTime();
            "longFormat = " + Long.toString(j2) + "\tdate = " + date.toString();
            return j2;
        } catch (Exception e2) {
            return j2;
        }
    }

    public static Crittercism a() {
        return c;
    }

    private static boolean a(Context context) {
        try {
            return context.getSharedPreferences("com.crittercism.prefs", 0).getBoolean("optOutStatus", false);
        } catch (Exception e2) {
            return false;
        }
    }

    private static boolean a(Context context, String str) {
        boolean z;
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences("com.crittercism.prefs", 0);
            z = sharedPreferences.getBoolean("crashedOnLastAppLoad", false);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.remove("crashedOnLastAppLoad");
            if (!edit.commit()) {
                throw new Exception();
            }
        } catch (Exception e2) {
            "Exception when trying to retrieve crashedOnLastAppLoad from SharedPreferences! " + e2.getClass().getName();
            z = false;
        }
        if (!z) {
            String str2 = "";
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(Environment.getExternalStorageDirectory(), "critter_" + str + "_crashedonlastappload.txt"))));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    str2 = str2 + readLine;
                }
                bufferedReader.close();
                if (str2.contains("true")) {
                    z = true;
                }
            } catch (Exception e3) {
                z = false;
            }
        }
        "read crashedOnLastAppLoad as: " + (z ? "true" : HttpState.PREEMPTIVE_DEFAULT);
        return z;
    }

    public static boolean didCrashOnLastAppLoad() {
        if (c == null) {
            Log.w("Crittercism", "Call to didCrashOnLastAppLoad() failed.  Please contact us at support@crittercism.com.");
            return false;
        }
        try {
            return c.o;
        } catch (Exception e2) {
            return false;
        }
    }

    public static String getNotificationTitle() {
        if (c != null) {
            return c.v;
        }
        Log.w("Crittercism", "Call to getNotificationTitle failed.  Please contact us at support@crittercism.com.");
        return "Developer Reply";
    }

    public static boolean getOptOutStatus() {
        if (c == null) {
            Log.w("Crittercism", "Call to getOptOutStatus() failed.  Please contact us at support@crittercism.com.");
            return false;
        }
        try {
            Crittercism crittercism2 = c;
            Context context = c.h;
            c.j();
            return a(context);
        } catch (Exception e2) {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00e6 A[Catch:{ Exception -> 0x01cb }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean init(android.content.Context r13, java.lang.String r14, org.json.JSONObject... r15) {
        /*
            r11 = 1
            r7 = 0
            java.lang.Class<com.crittercism.app.Crittercism> r9 = com.crittercism.app.Crittercism.class
            monitor-enter(r9)
            java.lang.String r0 = "CRITTERCISM_APP_ID"
            boolean r0 = r14.contains(r0)     // Catch:{ all -> 0x01dd }
            if (r0 == 0) goto L_0x0017
            java.lang.String r0 = "Crittercism"
            java.lang.String r1 = "ERROR: Crittercism will not work unless you enter a valid Crittercism App ID. Check your settings page to find the ID."
            android.util.Log.e(r0, r1)     // Catch:{ all -> 0x01dd }
            r0 = r7
        L_0x0015:
            monitor-exit(r9)
            return r0
        L_0x0017:
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ all -> 0x01dd }
            r0.<init>()     // Catch:{ all -> 0x01dd }
            int r1 = r15.length     // Catch:{ all -> 0x01dd }
            if (r1 <= 0) goto L_0x0245
            r0 = 0
            r0 = r15[r0]     // Catch:{ all -> 0x01dd }
            r8 = r0
        L_0x0023:
            com.crittercism.app.Crittercism r0 = com.crittercism.app.Crittercism.c     // Catch:{ all -> 0x01dd }
            if (r0 == 0) goto L_0x01cc
            com.crittercism.app.Crittercism r0 = com.crittercism.app.Crittercism.c     // Catch:{ all -> 0x01dd }
            boolean r0 = r0.g     // Catch:{ all -> 0x01dd }
            if (r0 != 0) goto L_0x01cc
            com.crittercism.app.Crittercism r10 = com.crittercism.app.Crittercism.c     // Catch:{ Exception -> 0x01cb }
            r0 = 1
            r10.g = r0     // Catch:{ Exception -> 0x01cb }
            r10.h = r13     // Catch:{ Exception -> 0x01cb }
            java.lang.String r0 = r10.w()     // Catch:{ Exception -> 0x01cb }
            r10.r = r0     // Catch:{ Exception -> 0x01cb }
            int r0 = r10.x()     // Catch:{ Exception -> 0x01cb }
            r10.s = r0     // Catch:{ Exception -> 0x01cb }
            java.lang.String r0 = "customVersionName"
            boolean r0 = r8.has(r0)     // Catch:{ Exception -> 0x01cb }
            if (r0 == 0) goto L_0x0050
            java.lang.String r0 = "customVersionName"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x01c5 }
            r10.t = r0     // Catch:{ Exception -> 0x01c5 }
        L_0x0050:
            crittercism.android.b r0 = new crittercism.android.b     // Catch:{ Exception -> 0x01cb }
            java.lang.String r3 = com.crittercism.app.Crittercism.a     // Catch:{ Exception -> 0x01cb }
            java.lang.String r4 = r10.r     // Catch:{ Exception -> 0x01cb }
            int r5 = r10.s     // Catch:{ Exception -> 0x01cb }
            java.lang.String r6 = r10.t     // Catch:{ Exception -> 0x01cb }
            r1 = r13
            r2 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01cb }
            r10.d = r0     // Catch:{ Exception -> 0x01cb }
            crittercism.android.b r0 = r10.d     // Catch:{ Exception -> 0x01cb }
            float r0 = r0.e()     // Catch:{ Exception -> 0x01cb }
            r10.i = r0     // Catch:{ Exception -> 0x01cb }
            crittercism.android.d r0 = r10.e     // Catch:{ Exception -> 0x01cb }
            if (r0 != 0) goto L_0x0074
            crittercism.android.d r0 = new crittercism.android.d     // Catch:{ Exception -> 0x01cb }
            r0.<init>()     // Catch:{ Exception -> 0x01cb }
            r10.e = r0     // Catch:{ Exception -> 0x01cb }
        L_0x0074:
            crittercism.android.a.a(r13)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r0 = "installNdk"
            boolean r0 = r8.has(r0)     // Catch:{ Exception -> 0x01cb }
            if (r0 == 0) goto L_0x0242
            java.lang.String r0 = "installNdk"
            boolean r0 = r8.getBoolean(r0)     // Catch:{ JSONException -> 0x01cf, Exception -> 0x01d3 }
            r2 = r0
        L_0x0086:
            java.lang.String r0 = "nativeDumpPath"
            boolean r0 = r8.has(r0)     // Catch:{ Exception -> 0x01cb }
            if (r0 == 0) goto L_0x0096
            java.lang.String r0 = "nativeDumpPath"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x0235 }
            r10.w = r0     // Catch:{ Exception -> 0x0235 }
        L_0x0096:
            java.lang.String r0 = "delaySendingAppLoad"
            boolean r0 = r8.has(r0)     // Catch:{ Exception -> 0x01cb }
            if (r0 == 0) goto L_0x01d8
            java.lang.String r0 = "delaySendingAppLoad"
            boolean r0 = r8.getBoolean(r0)     // Catch:{ Exception -> 0x01d7 }
            r10.x = r0     // Catch:{ Exception -> 0x01d7 }
        L_0x00a6:
            java.lang.String r0 = "shouldCollectLogcat"
            boolean r0 = r8.has(r0)     // Catch:{ Exception -> 0x01cb }
            if (r0 == 0) goto L_0x00b6
            java.lang.String r0 = "shouldCollectLogcat"
            boolean r0 = r8.getBoolean(r0)     // Catch:{ Exception -> 0x01e0 }
            r10.p = r0     // Catch:{ Exception -> 0x01e0 }
        L_0x00b6:
            java.lang.String r0 = "notificationTitle"
            boolean r0 = r8.has(r0)     // Catch:{ Exception -> 0x01cb }
            if (r0 == 0) goto L_0x00c6
            java.lang.String r0 = "notificationTitle"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x01e6 }
            r10.v = r0     // Catch:{ Exception -> 0x01e6 }
        L_0x00c6:
            java.lang.String r0 = "shouldUseAmazonMarket"
            boolean r0 = r8.has(r0)     // Catch:{ Exception -> 0x01cb }
            if (r0 == 0) goto L_0x00d6
            java.lang.String r0 = "shouldUseAmazonMarket"
            boolean r0 = r8.getBoolean(r0)     // Catch:{ Exception -> 0x01ed }
            r10.q = r0     // Catch:{ Exception -> 0x01ed }
        L_0x00d6:
            boolean r0 = a(r13)     // Catch:{ Exception -> 0x01cb }
            r10.n = r0     // Catch:{ Exception -> 0x01cb }
            boolean r0 = a(r13, r14)     // Catch:{ Exception -> 0x01cb }
            r10.o = r0     // Catch:{ Exception -> 0x01cb }
            boolean r0 = r10.n     // Catch:{ Exception -> 0x01cb }
            if (r0 != 0) goto L_0x01bf
            java.lang.String r0 = getNotificationTitle()     // Catch:{ Exception -> 0x01cb }
            r10.v = r0     // Catch:{ Exception -> 0x01cb }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cb }
            java.lang.String r1 = "initialize: notification title is "
            r0.<init>(r1)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r1 = r10.v     // Catch:{ Exception -> 0x01cb }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x01cb }
            r0.toString()     // Catch:{ Exception -> 0x01cb }
            crittercism.android.d r0 = r10.e     // Catch:{ Exception -> 0x01cb }
            android.content.Context r0 = r10.h     // Catch:{ Exception -> 0x01cb }
            java.lang.String r1 = "breadcrumbsFileString"
            java.lang.String r3 = crittercism.android.d.a(r0, r1)     // Catch:{ Exception -> 0x01cb }
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ Exception -> 0x01cb }
            r0.<init>()     // Catch:{ Exception -> 0x01cb }
            java.lang.String r1 = "session_start"
            r0.put(r1)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r1 = u()     // Catch:{ Exception -> 0x01cb }
            r0.put(r1)     // Catch:{ Exception -> 0x01cb }
            org.json.JSONArray r4 = new org.json.JSONArray     // Catch:{ Exception -> 0x01cb }
            r4.<init>()     // Catch:{ Exception -> 0x01cb }
            r4.put(r0)     // Catch:{ Exception -> 0x01cb }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x01cb }
            r1.<init>()     // Catch:{ Exception -> 0x01cb }
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ Exception -> 0x01cb }
            r0.<init>()     // Catch:{ Exception -> 0x01cb }
            if (r3 == 0) goto L_0x023d
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01f3 }
            r1.<init>(r3)     // Catch:{ JSONException -> 0x01f3 }
        L_0x0130:
            java.lang.String r3 = "current_session"
            boolean r3 = r1.has(r3)     // Catch:{ Exception -> 0x01cb }
            if (r3 == 0) goto L_0x0238
            java.lang.String r0 = "current_session"
            org.json.JSONArray r0 = r1.getJSONArray(r0)     // Catch:{ JSONException -> 0x01fb }
            r12 = r0
            r0 = r1
            r1 = r12
        L_0x0141:
            java.lang.String r3 = "previous_session"
            r0.put(r3, r1)     // Catch:{ JSONException -> 0x020b }
            java.lang.String r1 = "current_session"
            r0.put(r1, r4)     // Catch:{ JSONException -> 0x020b }
        L_0x014b:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cb }
            java.lang.String r3 = "Breadcrumbs: "
            r1.<init>(r3)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x01cb }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x01cb }
            r1.toString()     // Catch:{ Exception -> 0x01cb }
            crittercism.android.d r1 = r10.e     // Catch:{ Exception -> 0x01cb }
            android.content.Context r1 = r10.h     // Catch:{ Exception -> 0x01cb }
            java.lang.String r3 = "breadcrumbsFileString"
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01cb }
            crittercism.android.d.a(r1, r3, r0)     // Catch:{ Exception -> 0x01cb }
            java.lang.Thread$UncaughtExceptionHandler r0 = java.lang.Thread.getDefaultUncaughtExceptionHandler()     // Catch:{ Exception -> 0x01cb }
            boolean r1 = r0 instanceof com.crittercism.app.Crittercism.a     // Catch:{ Exception -> 0x01cb }
            if (r1 != 0) goto L_0x017a
            com.crittercism.app.Crittercism$a r1 = new com.crittercism.app.Crittercism$a     // Catch:{ Exception -> 0x01cb }
            r1.<init>(r0)     // Catch:{ Exception -> 0x01cb }
            java.lang.Thread.setDefaultUncaughtExceptionHandler(r1)     // Catch:{ Exception -> 0x01cb }
        L_0x017a:
            crittercism.android.d r0 = r10.e     // Catch:{ Exception -> 0x01cb }
            android.content.Context r0 = r10.h     // Catch:{ Exception -> 0x01cb }
            java.lang.String r1 = "com.crittercism.prefs.did"
            java.lang.String r0 = crittercism.android.d.a(r0, r1)     // Catch:{ Exception -> 0x01cb }
            if (r0 != 0) goto L_0x0213
            crittercism.android.b r0 = r10.d     // Catch:{ Exception -> 0x01cb }
            java.lang.String r0 = r0.b()     // Catch:{ Exception -> 0x01cb }
            crittercism.android.d r1 = r10.e     // Catch:{ Exception -> 0x01cb }
            android.content.Context r1 = r10.h     // Catch:{ Exception -> 0x01cb }
            java.lang.String r3 = "com.crittercism.prefs.did"
            crittercism.android.d.a(r1, r3, r0)     // Catch:{ Exception -> 0x01cb }
        L_0x0195:
            crittercism.android.b r1 = r10.d     // Catch:{ Exception -> 0x01cb }
            r1.a(r0)     // Catch:{ Exception -> 0x01cb }
            crittercism.android.i r0 = r10.j     // Catch:{ Exception -> 0x01cb }
            r0.b()     // Catch:{ Exception -> 0x01cb }
            boolean r0 = r10.x     // Catch:{ Exception -> 0x0232 }
            if (r0 != 0) goto L_0x01a9
            boolean r0 = getOptOutStatus()     // Catch:{ Exception -> 0x0232 }
            if (r0 != r11) goto L_0x0223
        L_0x01a9:
            if (r2 == 0) goto L_0x01b8
            java.lang.Thread r0 = new java.lang.Thread     // Catch:{ Exception -> 0x01cb }
            com.crittercism.app.Crittercism$1 r1 = new com.crittercism.app.Crittercism$1     // Catch:{ Exception -> 0x01cb }
            r1.<init>()     // Catch:{ Exception -> 0x01cb }
            r0.<init>(r1)     // Catch:{ Exception -> 0x01cb }
            r0.start()     // Catch:{ Exception -> 0x01cb }
        L_0x01b8:
            java.lang.String r0 = "Crittercism"
            java.lang.String r1 = "Crittercism Initialized."
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x01cb }
        L_0x01bf:
            boolean r0 = didCrashOnLastAppLoad()     // Catch:{ Exception -> 0x01cb }
            goto L_0x0015
        L_0x01c5:
            r0 = move-exception
            r0 = 0
            r10.t = r0     // Catch:{ Exception -> 0x01cb }
            goto L_0x0050
        L_0x01cb:
            r0 = move-exception
        L_0x01cc:
            r0 = r7
            goto L_0x0015
        L_0x01cf:
            r0 = move-exception
            r2 = r7
            goto L_0x0086
        L_0x01d3:
            r0 = move-exception
            r2 = r7
            goto L_0x0086
        L_0x01d7:
            r0 = move-exception
        L_0x01d8:
            r0 = 0
            r10.x = r0     // Catch:{ Exception -> 0x01cb }
            goto L_0x00a6
        L_0x01dd:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x01e0:
            r0 = move-exception
            r0 = 0
            r10.p = r0     // Catch:{ Exception -> 0x01cb }
            goto L_0x00b6
        L_0x01e6:
            r0 = move-exception
            java.lang.String r0 = "Developer Reply"
            r10.v = r0     // Catch:{ Exception -> 0x01cb }
            goto L_0x00c6
        L_0x01ed:
            r0 = move-exception
            r0 = 0
            r10.q = r0     // Catch:{ Exception -> 0x01cb }
            goto L_0x00d6
        L_0x01f3:
            r1 = move-exception
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x01cb }
            r1.<init>()     // Catch:{ Exception -> 0x01cb }
            goto L_0x0130
        L_0x01fb:
            r0 = move-exception
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x01cb }
            r1.<init>()     // Catch:{ Exception -> 0x01cb }
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ Exception -> 0x01cb }
            r0.<init>()     // Catch:{ Exception -> 0x01cb }
            r12 = r0
            r0 = r1
            r1 = r12
            goto L_0x0141
        L_0x020b:
            r0 = move-exception
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x01cb }
            r0.<init>()     // Catch:{ Exception -> 0x01cb }
            goto L_0x014b
        L_0x0213:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cb }
            java.lang.String r3 = "deviceID is "
            r1.<init>(r3)     // Catch:{ Exception -> 0x01cb }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x01cb }
            r1.toString()     // Catch:{ Exception -> 0x01cb }
            goto L_0x0195
        L_0x0223:
            java.lang.Thread r0 = new java.lang.Thread     // Catch:{ Exception -> 0x0232 }
            com.crittercism.app.Crittercism$4 r1 = new com.crittercism.app.Crittercism$4     // Catch:{ Exception -> 0x0232 }
            r1.<init>()     // Catch:{ Exception -> 0x0232 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0232 }
            r0.start()     // Catch:{ Exception -> 0x0232 }
            goto L_0x01a9
        L_0x0232:
            r0 = move-exception
            goto L_0x01a9
        L_0x0235:
            r0 = move-exception
            goto L_0x0096
        L_0x0238:
            r12 = r0
            r0 = r1
            r1 = r12
            goto L_0x0141
        L_0x023d:
            r12 = r0
            r0 = r1
            r1 = r12
            goto L_0x0141
        L_0x0242:
            r2 = r7
            goto L_0x0086
        L_0x0245:
            r8 = r0
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crittercism.app.Crittercism.init(android.content.Context, java.lang.String, org.json.JSONObject[]):boolean");
    }

    public static synchronized void leaveBreadcrumb(String str) {
        JSONObject jSONObject;
        JSONArray jSONArray;
        JSONObject jSONObject2;
        synchronized (Crittercism.class) {
            if (c == null) {
                Log.w("Crittercism", "Call to leaveBreadcrumb() failed.  Please contact us at support@crittercism.com.");
            } else {
                Crittercism crittercism2 = c;
                if (!crittercism2.n) {
                    d dVar = crittercism2.e;
                    String a2 = d.a(crittercism2.h, "breadcrumbsFileString");
                    JSONArray jSONArray2 = new JSONArray();
                    jSONArray2.put(str);
                    jSONArray2.put(u());
                    JSONObject jSONObject3 = new JSONObject();
                    JSONArray jSONArray3 = new JSONArray();
                    if (a2 != null) {
                        try {
                            jSONObject2 = new JSONObject(a2);
                        } catch (JSONException e2) {
                            jSONObject2 = new JSONObject();
                        }
                        if (jSONObject2.has("current_session")) {
                            try {
                                jSONArray3 = jSONObject2.getJSONArray("current_session");
                                jSONObject = jSONObject2;
                            } catch (JSONException e3) {
                                jSONArray3 = new JSONArray();
                                jSONObject = jSONObject2;
                            }
                        } else {
                            jSONObject = jSONObject2;
                        }
                    } else {
                        jSONObject = jSONObject3;
                    }
                    try {
                        jSONArray3.put(jSONArray2);
                        if (jSONArray3.length() > 50) {
                            jSONArray = new JSONArray();
                            try {
                                jSONArray.put(jSONArray3.getJSONArray(0));
                                for (int i2 = 2; i2 < jSONArray3.length(); i2++) {
                                    jSONArray.put(jSONArray3.getJSONArray(i2));
                                }
                            } catch (JSONException e4) {
                                jSONArray = new JSONArray();
                            }
                        } else {
                            jSONArray = jSONArray3;
                        }
                        try {
                            jSONObject.put("current_session", jSONArray);
                        } catch (JSONException e5) {
                        }
                        "Breadcrumbs: " + jSONObject.toString();
                        "currentSessionJsonArray size: " + Integer.toString(jSONArray.length());
                        d dVar2 = crittercism2.e;
                        d.a(crittercism2.h, "breadcrumbsFileString", jSONObject.toString());
                    } catch (Exception e6) {
                        Log.w("Crittercism", "Failed to leave breadcrumb.  Please contact us at support@crittercism.com.");
                    }
                }
            }
        }
        return;
    }

    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ef A[Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01b1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void logHandledException(java.lang.Throwable r8) {
        /*
            r4 = 50
            java.lang.Class<com.crittercism.app.Crittercism> r2 = com.crittercism.app.Crittercism.class
            monitor-enter(r2)
            com.crittercism.app.Crittercism r0 = com.crittercism.app.Crittercism.c     // Catch:{ all -> 0x01a3 }
            if (r0 != 0) goto L_0x0012
            java.lang.String r0 = "Crittercism"
            java.lang.String r1 = "Call to logHandledException() failed.  Please contact us at support@crittercism.com."
            android.util.Log.w(r0, r1)     // Catch:{ all -> 0x01a3 }
        L_0x0010:
            monitor-exit(r2)
            return
        L_0x0012:
            boolean r0 = getOptOutStatus()     // Catch:{ Exception -> 0x0146 }
            r1 = 1
            if (r0 == r1) goto L_0x0010
            com.crittercism.app.Crittercism r3 = com.crittercism.app.Crittercism.c     // Catch:{ Exception -> 0x0146 }
            crittercism.android.k r0 = r3.l     // Catch:{ Exception -> 0x0146 }
            int r0 = r0.h     // Catch:{ Exception -> 0x0146 }
            if (r0 >= r4) goto L_0x0010
            crittercism.android.k r0 = r3.l     // Catch:{ Exception -> 0x0146 }
            java.util.Vector r0 = r0.e()     // Catch:{ Exception -> 0x0146 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0146 }
            r1 = 5
            if (r0 >= r1) goto L_0x0010
            crittercism.android.k r0 = r3.l     // Catch:{ Exception -> 0x0146 }
            java.util.Vector r0 = r0.e()     // Catch:{ Exception -> 0x0146 }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0146 }
            crittercism.android.k r1 = r3.l     // Catch:{ Exception -> 0x0146 }
            int r1 = r1.h     // Catch:{ Exception -> 0x0146 }
            int r0 = r0 + r1
            if (r0 >= r4) goto L_0x0010
            crittercism.android.k r4 = r3.l     // Catch:{ Exception -> 0x0146 }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x0146 }
            r5.<init>()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0146 }
            r0.<init>()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r1 = ""
            java.lang.String r0 = r8.getMessage()     // Catch:{ Exception -> 0x0169 }
            if (r0 == 0) goto L_0x0057
            java.lang.String r1 = r8.getMessage()     // Catch:{ Exception -> 0x0169 }
        L_0x0057:
            r4.a(r8)     // Catch:{ Exception -> 0x0169 }
            r4.d()     // Catch:{ Exception -> 0x0169 }
            com.crittercism.app.Crittercism r0 = com.crittercism.app.Crittercism.c     // Catch:{ Exception -> 0x0149 }
            crittercism.android.d r0 = r0.e     // Catch:{ Exception -> 0x0149 }
            com.crittercism.app.Crittercism r0 = com.crittercism.app.Crittercism.c     // Catch:{ Exception -> 0x0149 }
            android.content.Context r0 = r0.h     // Catch:{ Exception -> 0x0149 }
            java.lang.String r6 = "breadcrumbsFileString"
            java.lang.String r0 = crittercism.android.d.a(r0, r6)     // Catch:{ Exception -> 0x0149 }
            if (r0 == 0) goto L_0x0074
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x0149 }
            r6.<init>(r0)     // Catch:{ Exception -> 0x0149 }
            r4.f = r6     // Catch:{ Exception -> 0x0149 }
        L_0x0074:
            org.json.JSONObject r0 = r4.f     // Catch:{ Exception -> 0x0169 }
            java.lang.String r6 = "current_session"
            boolean r0 = r0.has(r6)     // Catch:{ Exception -> 0x0169 }
            if (r0 != 0) goto L_0x01ae
            org.json.JSONObject r0 = r4.f     // Catch:{ JSONException -> 0x01a6 }
            java.lang.String r6 = "current_session"
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch:{ JSONException -> 0x01a6 }
            r7.<init>()     // Catch:{ JSONException -> 0x01a6 }
            r0.put(r6, r7)     // Catch:{ JSONException -> 0x01a6 }
            org.json.JSONObject r0 = r4.f     // Catch:{ JSONException -> 0x01a6 }
            java.lang.String r6 = "previous_session"
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch:{ JSONException -> 0x01a6 }
            r7.<init>()     // Catch:{ JSONException -> 0x01a6 }
            r0.put(r6, r7)     // Catch:{ JSONException -> 0x01a6 }
            r0 = r1
        L_0x0097:
            java.lang.String r1 = "app_state"
            com.crittercism.app.Crittercism r6 = com.crittercism.app.Crittercism.c     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            crittercism.android.b r6 = r6.d     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r7 = 2
            boolean[] r7 = new boolean[r7]     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r7 = {1, 1} // fill-array     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            org.json.JSONObject r6 = r6.a(r7)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r5.put(r1, r6)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            java.lang.String r1 = "breadcrumbs"
            org.json.JSONObject r6 = r4.f     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r5.put(r1, r6)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            java.lang.String r1 = "current_thread_id"
            java.lang.Thread r6 = java.lang.Thread.currentThread()     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            long r6 = r6.getId()     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r5.put(r1, r6)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            java.lang.String r1 = "exception_name"
            java.lang.String r6 = r4.c     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r5.put(r1, r6)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            java.lang.String r1 = "exception_reason"
            r5.put(r1, r0)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            java.lang.String r0 = "platform"
            java.lang.String r1 = "android"
            r5.put(r0, r1)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            java.lang.String r0 = "threads"
            org.json.JSONArray r1 = r4.e     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r5.put(r0, r1)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            java.lang.String r0 = "ts"
            java.lang.String r1 = u()     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r5.put(r0, r1)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            long r0 = r0.getId()     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r6 = 1
            int r0 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x01b1
            java.lang.String r0 = "type"
            java.lang.String r1 = crittercism.android.a.C0003a.c     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r5.put(r0, r1)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
        L_0x00f6:
            java.lang.String r0 = "unsymbolized_stacktrace"
            org.json.JSONArray r1 = r4.d     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r5.put(r0, r1)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
        L_0x00fd:
            r4.a(r5)     // Catch:{ Exception -> 0x0146 }
            crittercism.android.k r0 = r3.l     // Catch:{ Exception -> 0x012d }
            java.util.Date r0 = r0.g     // Catch:{ Exception -> 0x012d }
            if (r0 == 0) goto L_0x011e
            if (r0 == 0) goto L_0x0010
            java.util.Date r1 = new java.util.Date     // Catch:{ Exception -> 0x012d }
            r1.<init>()     // Catch:{ Exception -> 0x012d }
            long r4 = a(r1)     // Catch:{ Exception -> 0x012d }
            long r0 = a(r0)     // Catch:{ Exception -> 0x012d }
            long r0 = r4 - r0
            r4 = 60000(0xea60, double:2.9644E-319)
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0010
        L_0x011e:
            java.lang.Thread r0 = new java.lang.Thread     // Catch:{ Exception -> 0x012d }
            com.crittercism.app.Crittercism$8 r1 = new com.crittercism.app.Crittercism$8     // Catch:{ Exception -> 0x012d }
            r1.<init>()     // Catch:{ Exception -> 0x012d }
            r0.<init>(r1)     // Catch:{ Exception -> 0x012d }
            r0.start()     // Catch:{ Exception -> 0x012d }
            goto L_0x0010
        L_0x012d:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0146 }
            java.lang.String r3 = "Exception in logHandledExceptionInstanceMethod: "
            r1.<init>(r3)     // Catch:{ Exception -> 0x0146 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0146 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0146 }
            r0.toString()     // Catch:{ Exception -> 0x0146 }
            goto L_0x0010
        L_0x0146:
            r0 = move-exception
            goto L_0x0010
        L_0x0149:
            r0 = move-exception
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x0169 }
            r6.<init>()     // Catch:{ Exception -> 0x0169 }
            r4.f = r6     // Catch:{ Exception -> 0x0169 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0169 }
            java.lang.String r7 = "Exception making breadcrumbs in HandledExceptions.setBreadcrumbs: "
            r6.<init>(r7)     // Catch:{ Exception -> 0x0169 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0169 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0169 }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ Exception -> 0x0169 }
            r0.toString()     // Catch:{ Exception -> 0x0169 }
            goto L_0x0074
        L_0x0169:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0146 }
            java.lang.String r6 = "Exception in addThrowableToVector: "
            r1.<init>(r6)     // Catch:{ Exception -> 0x0146 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0146 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0146 }
            r0.toString()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0146 }
            r0.<init>()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x0146 }
            r1.<init>()     // Catch:{ Exception -> 0x0146 }
            r4.c = r1     // Catch:{ Exception -> 0x0146 }
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Exception -> 0x0146 }
            r1.<init>()     // Catch:{ Exception -> 0x0146 }
            r4.d = r1     // Catch:{ Exception -> 0x0146 }
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Exception -> 0x0146 }
            r1.<init>()     // Catch:{ Exception -> 0x0146 }
            r4.e = r1     // Catch:{ Exception -> 0x0146 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0146 }
            r1.<init>()     // Catch:{ Exception -> 0x0146 }
            r4.f = r1     // Catch:{ Exception -> 0x0146 }
            goto L_0x0097
        L_0x01a3:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x01a6:
            r0 = move-exception
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x0169 }
            r0.<init>()     // Catch:{ Exception -> 0x0169 }
            r4.f = r0     // Catch:{ Exception -> 0x0169 }
        L_0x01ae:
            r0 = r1
            goto L_0x0097
        L_0x01b1:
            java.lang.String r0 = "type"
            java.lang.String r1 = crittercism.android.a.C0003a.d     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            r5.put(r0, r1)     // Catch:{ JSONException -> 0x01ba, Exception -> 0x01d3 }
            goto L_0x00f6
        L_0x01ba:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0146 }
            java.lang.String r6 = "JSONException in addThrowableToVector: "
            r1.<init>(r6)     // Catch:{ Exception -> 0x0146 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0146 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0146 }
            r0.toString()     // Catch:{ Exception -> 0x0146 }
            goto L_0x00fd
        L_0x01d3:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0146 }
            java.lang.String r6 = "Exception in addThrowableToVector: "
            r1.<init>(r6)     // Catch:{ Exception -> 0x0146 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0146 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0146 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0146 }
            r0.toString()     // Catch:{ Exception -> 0x0146 }
            goto L_0x00fd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crittercism.app.Crittercism.logHandledException(java.lang.Throwable):void");
    }

    public static boolean s() {
        if (c == null) {
            return false;
        }
        return c.p;
    }

    public static void sendAppLoadData() {
        if (c == null) {
            Log.w("Crittercism", "Failed to send app load data.  Please contact us at support@crittercism.com");
            return;
        }
        try {
            if (!c.x) {
                Log.i("Crittercism", "sendAppLoadData() will only send data to Crittercism if \"delaySendingAppLoad\" is set to true in the configuration settings you include in the init call.");
            } else if (!getOptOutStatus()) {
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            Crittercism.a().b();
                            Crittercism.a().d();
                            Crittercism.a().f();
                            Crittercism.a().h();
                        } catch (Exception e) {
                        }
                    }
                }).start();
            }
        } catch (Exception e2) {
        }
    }

    public static void setMetadata(final JSONObject jSONObject) {
        if (c == null) {
            Log.w("Crittercism", "Call to setMetadata() failed.  Please contact us at support@crittercism.com.");
            return;
        }
        try {
            c.d.c(jSONObject);
            if (c.g) {
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            Crittercism.this.d.b(jSONObject);
                        } catch (Exception e) {
                        }
                    }
                }).start();
            } else {
                Log.e("Crittercism", "Initialize the Crittercism library before using its methods.");
            }
        } catch (Exception e2) {
        }
    }

    public static void setOptOutStatus(boolean z) {
        if (c == null) {
            Log.w("Crittercism", "Call to setOptOutStatus() failed.  Please contact us at support@crittercism.com.");
            return;
        }
        try {
            SharedPreferences.Editor edit = c.h.getSharedPreferences("com.crittercism.prefs", 0).edit();
            edit.remove("optOutStatus");
            edit.putBoolean("optOutStatus", z);
            if (!edit.commit()) {
                throw new Exception();
            }
            "Saved optOutStatus as " + (z ? "true" : HttpState.PREEMPTIVE_DEFAULT) + " to SharedPreferences!!";
            c.n = z;
        } catch (Exception e2) {
            try {
                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                if (externalStorageDirectory.canWrite()) {
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(externalStorageDirectory, "critter_" + c.d.a() + "_optoutsetting.txt")));
                    bufferedWriter.write(z ? "true" : HttpState.PREEMPTIVE_DEFAULT);
                    bufferedWriter.close();
                }
            } catch (IOException e3) {
                Log.w("Crittercism", "Failed to store opt out status!!");
            }
        }
    }

    public static void setUsername(String str) {
        if (c == null) {
            Log.w("Crittercism", "Call to setUsername() failed.  Please contact us at support@crittercism.com.");
            return;
        }
        try {
            if (c.g) {
                if (c.f == null) {
                    c.f = new e();
                }
                c.f.b = str;
                JSONObject i2 = c.d.i();
                i2.put("username", str);
                setMetadata(i2);
                return;
            }
            Log.e("Crittercism", "Initialize the Crittercism library before using its methods.");
        } catch (Exception e2) {
        }
    }

    public static String u() {
        new String();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sZ", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(new Date());
    }

    static /* synthetic */ void v() {
        if (c != null) {
            try {
                SharedPreferences.Editor edit = c.h.getSharedPreferences("com.crittercism.prefs", 0).edit();
                edit.remove("crashedOnLastAppLoad");
                edit.putBoolean("crashedOnLastAppLoad", true);
                if (!edit.commit()) {
                    throw new Exception();
                }
            } catch (Exception e2) {
            }
        }
    }

    private String w() {
        try {
            return this.h.getPackageManager().getPackageInfo(this.h.getPackageName(), 0).versionName;
        } catch (Exception e2) {
            return "1.0";
        }
    }

    private int x() {
        try {
            return this.h.getPackageManager().getPackageInfo(this.h.getPackageName(), 0).versionCode;
        } catch (Exception e2) {
            return 0;
        }
    }

    public final void a(i iVar) {
        this.j = iVar;
    }

    public final void a(k kVar) {
        this.l = kVar;
    }

    public final void a(l lVar) {
        this.m = lVar;
    }

    public final void a(m mVar) {
        this.k = mVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b1 A[Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0130  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean a(java.lang.Throwable r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            crittercism.android.m r2 = r6.k     // Catch:{ all -> 0x0122 }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ all -> 0x0122 }
            r3.<init>()     // Catch:{ all -> 0x0122 }
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x0122 }
            r0.<init>()     // Catch:{ all -> 0x0122 }
            java.lang.String r1 = ""
            java.lang.String r0 = r7.getMessage()     // Catch:{ Exception -> 0x00e8 }
            if (r0 == 0) goto L_0x0019
            java.lang.String r1 = r7.getMessage()     // Catch:{ Exception -> 0x00e8 }
        L_0x0019:
            r2.a(r7)     // Catch:{ Exception -> 0x00e8 }
            r2.d()     // Catch:{ Exception -> 0x00e8 }
            com.crittercism.app.Crittercism r0 = com.crittercism.app.Crittercism.c     // Catch:{ Exception -> 0x00c8 }
            crittercism.android.d r0 = r0.e     // Catch:{ Exception -> 0x00c8 }
            com.crittercism.app.Crittercism r0 = com.crittercism.app.Crittercism.c     // Catch:{ Exception -> 0x00c8 }
            android.content.Context r0 = r0.h     // Catch:{ Exception -> 0x00c8 }
            java.lang.String r4 = "breadcrumbsFileString"
            java.lang.String r0 = crittercism.android.d.a(r0, r4)     // Catch:{ Exception -> 0x00c8 }
            if (r0 == 0) goto L_0x0036
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x00c8 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x00c8 }
            r2.f = r4     // Catch:{ Exception -> 0x00c8 }
        L_0x0036:
            org.json.JSONObject r0 = r2.f     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r4 = "current_session"
            boolean r0 = r0.has(r4)     // Catch:{ Exception -> 0x00e8 }
            if (r0 != 0) goto L_0x012d
            org.json.JSONObject r0 = r2.f     // Catch:{ JSONException -> 0x0125 }
            java.lang.String r4 = "current_session"
            org.json.JSONArray r5 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0125 }
            r5.<init>()     // Catch:{ JSONException -> 0x0125 }
            r0.put(r4, r5)     // Catch:{ JSONException -> 0x0125 }
            org.json.JSONObject r0 = r2.f     // Catch:{ JSONException -> 0x0125 }
            java.lang.String r4 = "previous_session"
            org.json.JSONArray r5 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0125 }
            r5.<init>()     // Catch:{ JSONException -> 0x0125 }
            r0.put(r4, r5)     // Catch:{ JSONException -> 0x0125 }
            r0 = r1
        L_0x0059:
            java.lang.String r1 = "app_state"
            com.crittercism.app.Crittercism r4 = com.crittercism.app.Crittercism.c     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            crittercism.android.b r4 = r4.d     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r5 = 2
            boolean[] r5 = new boolean[r5]     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r5 = {1, 1} // fill-array     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            org.json.JSONObject r4 = r4.a(r5)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r3.put(r1, r4)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            java.lang.String r1 = "breadcrumbs"
            org.json.JSONObject r4 = r2.f     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r3.put(r1, r4)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            java.lang.String r1 = "current_thread_id"
            java.lang.Thread r4 = java.lang.Thread.currentThread()     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            long r4 = r4.getId()     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r3.put(r1, r4)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            java.lang.String r1 = "exception_name"
            java.lang.String r4 = r2.c     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r3.put(r1, r4)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            java.lang.String r1 = "exception_reason"
            r3.put(r1, r0)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            java.lang.String r0 = "platform"
            java.lang.String r1 = "android"
            r3.put(r0, r1)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            java.lang.String r0 = "threads"
            org.json.JSONArray r1 = r2.e     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r3.put(r0, r1)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            java.lang.String r0 = "ts"
            java.lang.String r1 = u()     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r3.put(r0, r1)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            long r0 = r0.getId()     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r4 = 1
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x0130
            java.lang.String r0 = "type"
            java.lang.String r1 = crittercism.android.a.C0003a.a     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r3.put(r0, r1)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
        L_0x00b8:
            java.lang.String r0 = "unsymbolized_stacktrace"
            org.json.JSONArray r1 = r2.d     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r3.put(r0, r1)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
        L_0x00bf:
            r2.a(r3)     // Catch:{ all -> 0x0122 }
            boolean r0 = r6.d()     // Catch:{ all -> 0x0122 }
            monitor-exit(r6)
            return r0
        L_0x00c8:
            r0 = move-exception
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x00e8 }
            r4.<init>()     // Catch:{ Exception -> 0x00e8 }
            r2.f = r4     // Catch:{ Exception -> 0x00e8 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r5 = "Exception making breadcrumbs in SdkCrashes.setBreadcrumbs: "
            r4.<init>(r5)     // Catch:{ Exception -> 0x00e8 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x00e8 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x00e8 }
            r0.toString()     // Catch:{ Exception -> 0x00e8 }
            goto L_0x0036
        L_0x00e8:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0122 }
            java.lang.String r4 = "Exception in addThrowableToVector: "
            r1.<init>(r4)     // Catch:{ all -> 0x0122 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x0122 }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x0122 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0122 }
            r0.toString()     // Catch:{ all -> 0x0122 }
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x0122 }
            r0.<init>()     // Catch:{ all -> 0x0122 }
            java.lang.String r1 = new java.lang.String     // Catch:{ all -> 0x0122 }
            r1.<init>()     // Catch:{ all -> 0x0122 }
            r2.c = r1     // Catch:{ all -> 0x0122 }
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ all -> 0x0122 }
            r1.<init>()     // Catch:{ all -> 0x0122 }
            r2.d = r1     // Catch:{ all -> 0x0122 }
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ all -> 0x0122 }
            r1.<init>()     // Catch:{ all -> 0x0122 }
            r2.e = r1     // Catch:{ all -> 0x0122 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ all -> 0x0122 }
            r1.<init>()     // Catch:{ all -> 0x0122 }
            r2.f = r1     // Catch:{ all -> 0x0122 }
            goto L_0x0059
        L_0x0122:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0125:
            r0 = move-exception
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x00e8 }
            r0.<init>()     // Catch:{ Exception -> 0x00e8 }
            r2.f = r0     // Catch:{ Exception -> 0x00e8 }
        L_0x012d:
            r0 = r1
            goto L_0x0059
        L_0x0130:
            java.lang.String r0 = "type"
            java.lang.String r1 = crittercism.android.a.C0003a.b     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            r3.put(r0, r1)     // Catch:{ JSONException -> 0x0138, Exception -> 0x0151 }
            goto L_0x00b8
        L_0x0138:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0122 }
            java.lang.String r4 = "JSONException in addThrowableToVector: "
            r1.<init>(r4)     // Catch:{ all -> 0x0122 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x0122 }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x0122 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0122 }
            r0.toString()     // Catch:{ all -> 0x0122 }
            goto L_0x00bf
        L_0x0151:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0122 }
            java.lang.String r4 = "Exception in addThrowableToVector: "
            r1.<init>(r4)     // Catch:{ all -> 0x0122 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x0122 }
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x0122 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0122 }
            r0.toString()     // Catch:{ all -> 0x0122 }
            goto L_0x00bf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crittercism.app.Crittercism.a(java.lang.Throwable):boolean");
    }

    public final boolean b() {
        boolean z;
        FutureTask futureTask = new FutureTask(new Callable() {
            private static Boolean a() {
                try {
                    Vector e = Crittercism.a().m().e();
                    i d = i.d();
                    d.a(e);
                    Crittercism.a().a(d);
                    "allAppLoads.getPendingDataVector().size() = " + d.e().size();
                } catch (Exception e2) {
                    "Exception in startAppLoadsThreads boolean callable: " + e2.getClass().getName();
                }
                try {
                    if (Crittercism.a().m().e().size() == 0) {
                        return true;
                    }
                } catch (Exception e3) {
                }
                return Boolean.valueOf(Crittercism.a().c());
            }

            public final /* synthetic */ Object call() {
                return a();
            }
        });
        try {
            Executors.newFixedThreadPool(10).execute(futureTask);
            z = false;
            while (!futureTask.isDone()) {
                z = ((Boolean) futureTask.get(2500, TimeUnit.MILLISECONDS)).booleanValue();
            }
        } catch (Exception e2) {
            z = false;
        }
        "sentAppLoads = " + (z ? "TRUE" : "FALSE");
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0042 A[Catch:{ Exception -> 0x0120 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004e A[SYNTHETIC, Splitter:B:23:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0072 A[Catch:{ Exception -> 0x0159 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00b7 A[Catch:{ Exception -> 0x0172 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c() {
        /*
            r8 = this;
            r2 = 0
            r1 = 1
            r0 = 0
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            crittercism.android.i r5 = r8.j     // Catch:{ Exception -> 0x0197 }
            org.json.JSONObject r3 = r5.c()     // Catch:{ Exception -> 0x0197 }
        L_0x0013:
            crittercism.android.b r5 = r8.d     // Catch:{ Exception -> 0x0100 }
            org.json.JSONObject r4 = r5.a(r3)     // Catch:{ Exception -> 0x0100 }
            java.lang.String r3 = "success"
            boolean r3 = r4.has(r3)     // Catch:{ Exception -> 0x018b }
            if (r3 == 0) goto L_0x0034
            java.lang.String r3 = "success"
            int r3 = r4.getInt(r3)     // Catch:{ Exception -> 0x018b }
            if (r3 != r1) goto L_0x0034
            crittercism.android.b r0 = r8.d     // Catch:{ Exception -> 0x0192 }
            r0.h()     // Catch:{ Exception -> 0x0192 }
            crittercism.android.i r0 = r8.j     // Catch:{ Exception -> 0x0192 }
            r0.f()     // Catch:{ Exception -> 0x0192 }
            r0 = r1
        L_0x0034:
            r3 = r0
        L_0x0035:
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x0153 }
            r0.<init>()     // Catch:{ Exception -> 0x0153 }
            java.lang.String r0 = "me"
            boolean r0 = r4.has(r0)     // Catch:{ Exception -> 0x0120 }
            if (r0 == 0) goto L_0x0137
            java.lang.String r0 = "me"
            org.json.JSONObject r0 = r4.getJSONObject(r0)     // Catch:{ Exception -> 0x0120 }
            crittercism.android.e r0 = crittercism.android.e.a(r0)     // Catch:{ Exception -> 0x0120 }
        L_0x004c:
            if (r0 == 0) goto L_0x006a
            crittercism.android.e r5 = r8.f     // Catch:{ Exception -> 0x013a }
            if (r5 == 0) goto L_0x0062
            crittercism.android.e r5 = r8.f     // Catch:{ Exception -> 0x013a }
            java.lang.String r5 = r5.b     // Catch:{ Exception -> 0x013a }
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)     // Catch:{ Exception -> 0x013a }
            if (r5 != 0) goto L_0x0062
            crittercism.android.e r2 = r8.f     // Catch:{ Exception -> 0x013a }
            java.lang.String r2 = r2.b     // Catch:{ Exception -> 0x013a }
        L_0x0062:
            r8.f = r0     // Catch:{ Exception -> 0x013a }
            if (r2 == 0) goto L_0x006a
            crittercism.android.e r0 = r8.f     // Catch:{ Exception -> 0x013a }
            r0.b = r2     // Catch:{ Exception -> 0x013a }
        L_0x006a:
            java.lang.String r0 = "app_settings"
            boolean r0 = r4.has(r0)     // Catch:{ Exception -> 0x0159 }
            if (r0 == 0) goto L_0x00b3
            java.lang.String r0 = "app_settings"
            org.json.JSONObject r0 = r4.getJSONObject(r0)     // Catch:{ Exception -> 0x0159 }
            java.lang.String r2 = "settings"
            boolean r2 = r0.has(r2)     // Catch:{ Exception -> 0x0159 }
            if (r2 == 0) goto L_0x00b3
            java.lang.String r2 = "settings"
            org.json.JSONObject r2 = r0.getJSONObject(r2)     // Catch:{ Exception -> 0x0159 }
            java.lang.String r0 = "need_pkg"
            boolean r0 = r2.has(r0)     // Catch:{ Exception -> 0x0159 }
            if (r0 == 0) goto L_0x00b3
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0159 }
            java.lang.String r0 = "settings need_pkg = "
            r4.<init>(r0)     // Catch:{ Exception -> 0x0159 }
            java.lang.String r0 = "need_pkg"
            int r0 = r2.getInt(r0)     // Catch:{ Exception -> 0x0159 }
            if (r0 != r1) goto L_0x0155
            java.lang.String r0 = "true"
        L_0x009f:
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x0159 }
            r0.toString()     // Catch:{ Exception -> 0x0159 }
            java.lang.String r0 = "need_pkg"
            int r0 = r2.getInt(r0)     // Catch:{ Exception -> 0x0159 }
            if (r0 != r1) goto L_0x00b3
            crittercism.android.b r0 = r8.d     // Catch:{ Exception -> 0x0159 }
            r0.g()     // Catch:{ Exception -> 0x0159 }
        L_0x00b3:
            crittercism.android.e r0 = r8.f     // Catch:{ Exception -> 0x0172 }
            if (r0 == 0) goto L_0x00ff
            crittercism.android.e r0 = r8.f     // Catch:{ Exception -> 0x0172 }
            java.lang.String r1 = r0.a     // Catch:{ Exception -> 0x0172 }
            java.lang.String r2 = ""
            r0.a = r2     // Catch:{ Exception -> 0x0172 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0172 }
            java.lang.String r2 = "pop notification: "
            r0.<init>(r2)     // Catch:{ Exception -> 0x0172 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0172 }
            r0.toString()     // Catch:{ Exception -> 0x0172 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0172 }
            java.lang.String r2 = "username: "
            r0.<init>(r2)     // Catch:{ Exception -> 0x0172 }
            crittercism.android.e r2 = r8.f     // Catch:{ Exception -> 0x0172 }
            java.lang.String r2 = r2.b     // Catch:{ Exception -> 0x0172 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0172 }
            r0.toString()     // Catch:{ Exception -> 0x0172 }
            if (r1 == 0) goto L_0x00ff
            java.lang.String r0 = ""
            boolean r0 = r1.equals(r0)     // Catch:{ Exception -> 0x0172 }
            if (r0 != 0) goto L_0x00ff
            android.os.Handler r0 = r8.b     // Catch:{ Exception -> 0x0172 }
            android.os.Message r0 = android.os.Message.obtain(r0)     // Catch:{ Exception -> 0x0172 }
            android.os.Bundle r2 = new android.os.Bundle     // Catch:{ Exception -> 0x0172 }
            r2.<init>()     // Catch:{ Exception -> 0x0172 }
            java.lang.String r4 = "notification"
            r2.putString(r4, r1)     // Catch:{ Exception -> 0x0172 }
            r0.setData(r2)     // Catch:{ Exception -> 0x0172 }
            r0.sendToTarget()     // Catch:{ Exception -> 0x0172 }
        L_0x00ff:
            return r3
        L_0x0100:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r0
            r0 = r7
        L_0x0105:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "Exception obtaining or handling response object or clearing pending app loads vector in attemptToSendAppLoads "
            r5.<init>(r6)
            java.lang.Class r0 = r0.getClass()
            java.lang.String r0 = r0.getName()
            java.lang.StringBuilder r0 = r5.append(r0)
            r0.toString()
            r7 = r3
            r3 = r4
            r4 = r7
            goto L_0x0035
        L_0x0120:
            r0 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0153 }
            java.lang.String r6 = "Exception getting user object in handleAppLoadResponse: "
            r5.<init>(r6)     // Catch:{ Exception -> 0x0153 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0153 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0153 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ Exception -> 0x0153 }
            r0.toString()     // Catch:{ Exception -> 0x0153 }
        L_0x0137:
            r0 = r2
            goto L_0x004c
        L_0x013a:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0153 }
            java.lang.String r5 = "Exception setting user object in handleAppLoadResponse: "
            r2.<init>(r5)     // Catch:{ Exception -> 0x0153 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0153 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0153 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0153 }
            r0.toString()     // Catch:{ Exception -> 0x0153 }
            goto L_0x006a
        L_0x0153:
            r0 = move-exception
            goto L_0x00ff
        L_0x0155:
            java.lang.String r0 = "false"
            goto L_0x009f
        L_0x0159:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0153 }
            java.lang.String r2 = "Exception setting app settings in handleAppLoadResponse: "
            r1.<init>(r2)     // Catch:{ Exception -> 0x0153 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0153 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0153 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0153 }
            r0.toString()     // Catch:{ Exception -> 0x0153 }
            goto L_0x00b3
        L_0x0172:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0153 }
            java.lang.String r2 = "Exception with user pop notification! "
            r1.<init>(r2)     // Catch:{ Exception -> 0x0153 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x0153 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0153 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0153 }
            r0.toString()     // Catch:{ Exception -> 0x0153 }
            goto L_0x00ff
        L_0x018b:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r0
            r0 = r7
            goto L_0x0105
        L_0x0192:
            r0 = move-exception
            r3 = r4
            r4 = r1
            goto L_0x0105
        L_0x0197:
            r5 = move-exception
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crittercism.app.Crittercism.c():boolean");
    }

    public final boolean d() {
        boolean z;
        FutureTask futureTask = new FutureTask(new Callable() {
            private static Boolean a() {
                try {
                    Vector e = Crittercism.a().n().e();
                    m c = m.c();
                    c.a(e);
                    Crittercism.a().a(c);
                } catch (Exception e2) {
                    "Exception in startCrashingSendingThreads boolean callable: " + e2.getClass().getName();
                }
                try {
                    if (Crittercism.a().n().e().size() == 0) {
                        return true;
                    }
                } catch (Exception e3) {
                }
                return Boolean.valueOf(Crittercism.a().e());
            }

            public final /* synthetic */ Object call() {
                return a();
            }
        });
        try {
            Executors.newFixedThreadPool(10).execute(futureTask);
            z = false;
            while (!futureTask.isDone()) {
                z = ((Boolean) futureTask.get(2500, TimeUnit.MILLISECONDS)).booleanValue();
            }
        } catch (Exception e2) {
            z = false;
        }
        "sentCrashes = " + (z ? "TRUE" : "FALSE");
        return z;
    }

    public final boolean e() {
        Exception e2;
        boolean z = true;
        JSONObject jSONObject = new JSONObject();
        new JSONObject();
        try {
            jSONObject = this.k.b();
        } catch (Exception e3) {
        }
        try {
            JSONObject a2 = this.d.a(jSONObject);
            if (!a2.has("success") || a2.getInt("success") != 1) {
                return false;
            }
            try {
                this.k.f();
                return true;
            } catch (Exception e4) {
                e2 = e4;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            z = false;
            e2 = exc;
            "Exception obtaining or handling response object or clearing pending crashes vector in attemptToSendCrashes " + e2.getClass().getName();
            return z;
        }
    }

    public final boolean f() {
        boolean z;
        FutureTask futureTask = new FutureTask(new Callable() {
            private static Boolean a() {
                try {
                    Vector e = Crittercism.a().o().e();
                    k c = k.c();
                    c.a(e);
                    Crittercism.a().a(c);
                } catch (Exception e2) {
                    "Exception in startExceptionSendingThreads boolean callable: " + e2.getClass().getName();
                }
                try {
                    if (Crittercism.a().o().e().size() == 0) {
                        return true;
                    }
                } catch (Exception e3) {
                }
                return Boolean.valueOf(Crittercism.a().g());
            }

            public final /* synthetic */ Object call() {
                return a();
            }
        });
        try {
            Executors.newFixedThreadPool(10).execute(futureTask);
            z = false;
            while (!futureTask.isDone()) {
                z = ((Boolean) futureTask.get(2500, TimeUnit.MILLISECONDS)).booleanValue();
            }
        } catch (Exception e2) {
            z = false;
        }
        "sentExceptions = " + (z ? "TRUE" : "FALSE");
        return z;
    }

    public final boolean g() {
        Exception e2;
        boolean z;
        JSONObject jSONObject = new JSONObject();
        new JSONObject();
        try {
            jSONObject = this.l.b();
        } catch (Exception e3) {
        }
        try {
            JSONObject a2 = this.d.a(jSONObject);
            if (!a2.has("success") || a2.getInt("success") != 1) {
                z = false;
            } else {
                try {
                    k kVar = this.l;
                    kVar.h = this.l.e().size() + kVar.h;
                    this.l.f();
                    z = true;
                } catch (Exception e4) {
                    Exception exc = e4;
                    z = true;
                    e2 = exc;
                    "Exception obtaining or handling response object or clearing pending exceptions vector in attemptToSendHandledExceptions " + e2.getClass().getName();
                    return z;
                }
            }
            try {
                this.l.g = new Date();
            } catch (Exception e5) {
                e2 = e5;
                "Exception obtaining or handling response object or clearing pending exceptions vector in attemptToSendHandledExceptions " + e2.getClass().getName();
                return z;
            }
        } catch (Exception e6) {
            e2 = e6;
            z = false;
            "Exception obtaining or handling response object or clearing pending exceptions vector in attemptToSendHandledExceptions " + e2.getClass().getName();
            return z;
        }
        return z;
    }

    public final boolean h() {
        boolean z;
        FutureTask futureTask = new FutureTask(new Callable() {
            private static Boolean a() {
                try {
                    Crittercism.a().a(l.c());
                    "pendingNdkCrashes.getPendingDataVector().size = " + Crittercism.a().p().e().size();
                } catch (Exception e) {
                    "Exception in startNdkSendingThreads boolean callable: " + e.getClass().getName();
                    e.printStackTrace();
                }
                try {
                    if (Crittercism.a().p().e().size() == 0) {
                        return true;
                    }
                } catch (Exception e2) {
                }
                return Boolean.valueOf(Crittercism.a().i());
            }

            public final /* synthetic */ Object call() {
                return a();
            }
        });
        try {
            Executors.newFixedThreadPool(10).execute(futureTask);
            z = false;
            while (!futureTask.isDone()) {
                z = ((Boolean) futureTask.get(8000, TimeUnit.MILLISECONDS)).booleanValue();
            }
        } catch (Exception e2) {
            z = false;
        }
        "sentNdkCrashes = " + (z ? "TRUE" : "FALSE");
        return z;
    }

    public final boolean i() {
        boolean z = true;
        boolean z2 = false;
        JSONObject jSONObject = new JSONObject();
        new JSONObject();
        try {
            jSONObject = this.m.a();
        } catch (Exception e2) {
        }
        try {
            JSONObject a2 = this.d.a(jSONObject);
            if (!a2.has("success") || a2.getInt("success") != 1) {
                z = false;
            }
            z2 = z;
        } catch (Exception e3) {
            "Exception obtaining or handling response object or clearing pending ndk filenames vector in attemptToSendNdkCrashes: " + e3.getClass().getName();
        }
        if (z2) {
            try {
                this.m.b();
                this.m.f();
            } catch (Exception e4) {
                "Exception removing ndk dump files from disk in attemptToSendNdkCrashes: " + e4.getClass().getName();
            }
        }
        return z2;
    }

    public final String j() {
        if (this.d != null) {
            return this.d.a();
        }
        Log.w("Crittercism", "Failed to get app id.  Please contact us at support@crittercism.com.");
        return new String();
    }

    public final Context k() {
        return this.h;
    }

    public final b l() {
        return this.d;
    }

    public final i m() {
        return this.j;
    }

    public final m n() {
        return this.k;
    }

    public final k o() {
        return this.l;
    }

    public final l p() {
        return this.m;
    }

    public final String q() {
        try {
            if (this.u == null || this.u.equals("")) {
                this.u = this.h.getPackageName();
            }
        } catch (Exception e2) {
            Log.w("Crittercism", "Call to getPackageName() failed.  Please contact us at support@crittercism.com.");
            this.u = new String();
        }
        return this.u;
    }

    public final String r() {
        return this.w;
    }

    public final int t() {
        try {
            return (int) ((this.d.f() * 10.0f) / 160.0f);
        } catch (Exception e2) {
            return -1;
        }
    }
}
