package com.crittercism.app;

import android.content.Context;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class CrittercismNDK {
    private static final String LIBNAME = "libcrittercism-ndk.so";
    private static String LIBPATH = null;
    private static final int LIBRARYVERSION = 2;

    public static native boolean checkLibraryVersion(int i);

    public static boolean installLib(Context context) {
        String str = "armeabi";
        if (System.getProperty("os.arch").contains("v7")) {
            str = str + "-v7a";
        }
        try {
            new File(LIBPATH).mkdirs();
            FileOutputStream fileOutputStream = new FileOutputStream(new File(LIBPATH + LIBNAME));
            InputStream open = context.getAssets().open(str + "/libcrittercism-ndk.so");
            byte[] bArr = new byte[8192];
            while (true) {
                int read = open.read(bArr);
                if (read >= 0) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    open.close();
                    fileOutputStream.close();
                    System.load(LIBPATH + LIBNAME);
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static native boolean installNdk(String str);

    public static void installNdkLib(Context context, String str) {
        boolean installLib;
        LIBPATH = context.getFilesDir().getAbsolutePath() + "/com.crittercism/lib/";
        String str2 = context.getFilesDir().getAbsolutePath() + CookieSpec.PATH_DELIM + str;
        if (new File(LIBPATH + LIBNAME).exists()) {
            try {
                System.load(LIBPATH + LIBNAME);
                installLib = checkLibraryVersion(2);
            } catch (UnsatisfiedLinkError e) {
                installLib = installLib(context);
            }
            if (!installLib) {
                installLib(context);
            }
        } else {
            installLib(context);
        }
        try {
            if (installNdk(str2)) {
                File file = new File(str2);
                file.getAbsolutePath();
                file.mkdirs();
            }
        } catch (Throwable th) {
        }
    }
}
