package com.pengyou.utils.two_dimension_code;

import android.telephony.PhoneNumberUtils;
import com.actionbarsherlock.widget.ActivityChooserView;
import java.util.regex.Pattern;

final class VCardContactEncoder extends ContactEncoder {
    /* access modifiers changed from: private */
    public static final Pattern NEWLINE = Pattern.compile("\\n");
    /* access modifiers changed from: private */
    public static final Pattern RESERVED_VCARD_CHARS = Pattern.compile("([\\\\,;])");
    private static final char TERMINATOR = '\n';
    private static final Formatter VCARD_FIELD_FORMATTER = new Formatter() {
        public String format(String source) {
            return VCardContactEncoder.NEWLINE.matcher(VCardContactEncoder.RESERVED_VCARD_CHARS.matcher(source).replaceAll("\\\\$1")).replaceAll("");
        }
    };

    VCardContactEncoder() {
    }

    public String[] encode(Iterable<String> names, String organization, Iterable<String> addresses, Iterable<String> phones, Iterable<String> emails, String url, String note) {
        StringBuilder newContents = new StringBuilder(100);
        StringBuilder newDisplayContents = new StringBuilder(100);
        newContents.append("BEGIN:VCARD").append((char) TERMINATOR);
        appendUpToUnique(newContents, newDisplayContents, "N", names, 1, null);
        append(newContents, newDisplayContents, "ORG", organization);
        appendUpToUnique(newContents, newDisplayContents, "ADR", addresses, 1, null);
        appendUpToUnique(newContents, newDisplayContents, "TEL", phones, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, new Formatter() {
            public String format(String source) {
                return PhoneNumberUtils.formatNumber(source);
            }
        });
        appendUpToUnique(newContents, newDisplayContents, "EMAIL", emails, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED, null);
        append(newContents, newDisplayContents, "URL", url);
        append(newContents, newDisplayContents, "NOTE", note);
        newContents.append("END:VCARD").append((char) TERMINATOR);
        return new String[]{newContents.toString(), newDisplayContents.toString()};
    }

    private static void append(StringBuilder newContents, StringBuilder newDisplayContents, String prefix, String value) {
        doAppend(newContents, newDisplayContents, prefix, value, VCARD_FIELD_FORMATTER, TERMINATOR);
    }

    private static void appendUpToUnique(StringBuilder newContents, StringBuilder newDisplayContents, String prefix, Iterable<String> values, int max, Formatter formatter) {
        doAppendUpToUnique(newContents, newDisplayContents, prefix, values, max, formatter, VCARD_FIELD_FORMATTER, TERMINATOR);
    }
}
