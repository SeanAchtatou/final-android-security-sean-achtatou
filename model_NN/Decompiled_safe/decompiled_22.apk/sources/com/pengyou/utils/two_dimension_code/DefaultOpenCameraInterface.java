package com.pengyou.utils.two_dimension_code;

import android.hardware.Camera;

final class DefaultOpenCameraInterface implements OpenCameraInterface {
    DefaultOpenCameraInterface() {
    }

    public Camera open() {
        return Camera.open();
    }
}
