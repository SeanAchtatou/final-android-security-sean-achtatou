package com.pengyou.utils.two_dimension_code;

public final class OpenCameraManager extends PlatformSupportManager<OpenCameraInterface> {
    public OpenCameraManager() {
        super(OpenCameraInterface.class, new DefaultOpenCameraInterface());
        addImplementationClass(9, "com.example.myzxingtest.GingerbreadOpenCameraInterface");
    }
}
