package com.pengyou.utils.two_dimension_code;

import android.app.Activity;
import com.google.zxing.client.result.CalendarParsedResult;
import com.google.zxing.client.result.ParsedResult;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.Util;
import java.text.DateFormat;
import java.util.Date;

public final class CalendarResultHandler extends ResultHandler {
    public CalendarResultHandler(Activity activity, ParsedResult result) {
        super(activity, result);
    }

    public CharSequence getDisplayContents() {
        CalendarParsedResult calResult = (CalendarParsedResult) getResult();
        StringBuilder result = new StringBuilder(100);
        ParsedResult.maybeAppend(calResult.getSummary(), result);
        Date start = calResult.getStart();
        ParsedResult.maybeAppend(format(calResult.isStartAllDay(), start), result);
        Date end = calResult.getEnd();
        if (end != null) {
            if (calResult.isEndAllDay() && !start.equals(end)) {
                end = new Date(end.getTime() - Util.MILLSECONDS_OF_DAY);
            }
            ParsedResult.maybeAppend(format(calResult.isEndAllDay(), end), result);
        }
        ParsedResult.maybeAppend(calResult.getLocation(), result);
        ParsedResult.maybeAppend(calResult.getOrganizer(), result);
        ParsedResult.maybeAppend(calResult.getAttendees(), result);
        ParsedResult.maybeAppend(calResult.getDescription(), result);
        return result.toString();
    }

    private static String format(boolean allDay, Date date) {
        DateFormat format;
        if (date == null) {
            return null;
        }
        if (allDay) {
            format = DateFormat.getDateInstance(2);
        } else {
            format = DateFormat.getDateTimeInstance(2, 2);
        }
        return format.format(date);
    }

    public int getDisplayTitle() {
        return R.string.result_calendar;
    }
}
