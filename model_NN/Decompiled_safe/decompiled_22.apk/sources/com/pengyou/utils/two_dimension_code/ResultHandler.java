package com.pengyou.utils.two_dimension_code;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.ContactsContract;
import android.util.Log;
import com.city_life.part_activiy.Two_dimension_codeInfoActivity;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.ParsedResultType;
import com.google.zxing.client.result.ResultParser;
import com.pengyou.citycommercialarea.R;
import java.util.Locale;
import sina_weibo.Constants;

public abstract class ResultHandler {
    private static final String[] ADDRESS_TYPE_STRINGS = {"home", "work"};
    private static final int[] ADDRESS_TYPE_VALUES = {1, 2};
    private static final String[] EMAIL_TYPE_STRINGS = {"home", "work", "mobile"};
    private static final int[] EMAIL_TYPE_VALUES = {1, 2, 4};
    public static final int MAX_BUTTON_COUNT = 4;
    private static final int NO_TYPE = -1;
    private static final String[] PHONE_TYPE_STRINGS = {"home", "work", "mobile", "fax", "pager", "main"};
    private static final int[] PHONE_TYPE_VALUES = {1, 3, 2, 4, 6, 12};
    private static final String TAG = ResultHandler.class.getSimpleName();
    private final Activity activity;
    private final String customProductSearch;
    private final Result rawResult;
    private final ParsedResult result;

    public abstract int getDisplayTitle();

    ResultHandler(Activity activity2, ParsedResult result2) {
        this(activity2, result2, null);
    }

    ResultHandler(Activity activity2, ParsedResult result2, Result rawResult2) {
        this.result = result2;
        this.activity = activity2;
        this.rawResult = rawResult2;
        this.customProductSearch = parseCustomSearchURL();
    }

    public ParsedResult getResult() {
        return this.result;
    }

    /* access modifiers changed from: package-private */
    public boolean hasCustomProductSearch() {
        return this.customProductSearch != null;
    }

    /* access modifiers changed from: package-private */
    public Activity getActivity() {
        return this.activity;
    }

    public boolean areContentsSecure() {
        return false;
    }

    public CharSequence getDisplayContents() {
        return this.result.getDisplayResult().replace("\r", "");
    }

    public final ParsedResultType getType() {
        return this.result.getType();
    }

    /* access modifiers changed from: package-private */
    public final void addPhoneOnlyContact(String[] phoneNumbers, String[] phoneTypes) {
        addContact(null, null, phoneNumbers, phoneTypes, null, null, null, null, null, null, null, null, null, null);
    }

    /* access modifiers changed from: package-private */
    public final void addEmailOnlyContact(String[] emails, String[] emailTypes) {
        addContact(null, null, null, null, emails, emailTypes, null, null, null, null, null, null, null, null);
    }

    /* access modifiers changed from: package-private */
    public final void addContact(String[] names, String pronunciation, String[] phoneNumbers, String[] phoneTypes, String[] emails, String[] emailTypes, String note, String instantMessenger, String address, String addressType, String org2, String title, String url, String birthday) {
        int type;
        int type2;
        int type3;
        Intent intent = new Intent("android.intent.action.INSERT_OR_EDIT", ContactsContract.Contacts.CONTENT_URI);
        intent.setType("vnd.android.cursor.item/contact");
        putExtra(intent, Constants.SINA_NAME, names != null ? names[0] : null);
        putExtra(intent, "phonetic_name", pronunciation);
        int phoneCount = Math.min(phoneNumbers != null ? phoneNumbers.length : 0, Contents.PHONE_KEYS.length);
        for (int x = 0; x < phoneCount; x++) {
            putExtra(intent, Contents.PHONE_KEYS[x], phoneNumbers[x]);
            if (phoneTypes != null && x < phoneTypes.length && (type3 = toPhoneContractType(phoneTypes[x])) >= 0) {
                intent.putExtra(Contents.PHONE_TYPE_KEYS[x], type3);
            }
        }
        int emailCount = Math.min(emails != null ? emails.length : 0, Contents.EMAIL_KEYS.length);
        for (int x2 = 0; x2 < emailCount; x2++) {
            putExtra(intent, Contents.EMAIL_KEYS[x2], emails[x2]);
            if (emailTypes != null && x2 < emailTypes.length && (type2 = toEmailContractType(emailTypes[x2])) >= 0) {
                intent.putExtra(Contents.EMAIL_TYPE_KEYS[x2], type2);
            }
        }
        StringBuilder aggregatedNotes = new StringBuilder();
        for (String aNote : new String[]{url, birthday, note}) {
            if (aNote != null) {
                if (aggregatedNotes.length() > 0) {
                    aggregatedNotes.append(10);
                }
                aggregatedNotes.append(aNote);
            }
        }
        if (aggregatedNotes.length() > 0) {
            putExtra(intent, "notes", aggregatedNotes.toString());
        }
        putExtra(intent, "im_handle", instantMessenger);
        putExtra(intent, "postal", address);
        if (addressType != null && (type = toAddressContractType(addressType)) >= 0) {
            intent.putExtra("postal_type", type);
        }
        putExtra(intent, "company", org2);
        putExtra(intent, "job_title", title);
        launchIntent(intent);
    }

    private static int toEmailContractType(String typeString) {
        return doToContractType(typeString, EMAIL_TYPE_STRINGS, EMAIL_TYPE_VALUES);
    }

    private static int toPhoneContractType(String typeString) {
        return doToContractType(typeString, PHONE_TYPE_STRINGS, PHONE_TYPE_VALUES);
    }

    private static int toAddressContractType(String typeString) {
        return doToContractType(typeString, ADDRESS_TYPE_STRINGS, ADDRESS_TYPE_VALUES);
    }

    private static int doToContractType(String typeString, String[] types, int[] values) {
        if (typeString == null) {
            return -1;
        }
        for (int i = 0; i < types.length; i++) {
            String type = types[i];
            if (typeString.startsWith(type) || typeString.startsWith(type.toUpperCase(Locale.ENGLISH))) {
                return values[i];
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public void rawLaunchIntent(Intent intent) {
        if (intent != null) {
            intent.addFlags(524288);
            Log.d(TAG, "Launching intent: " + intent + " with extras: " + intent.getExtras());
            this.activity.startActivity(intent);
        }
    }

    /* access modifiers changed from: package-private */
    public void launchIntent(Intent intent) {
        try {
            rawLaunchIntent(intent);
        } catch (ActivityNotFoundException e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
            builder.setTitle((int) R.string.app_name);
            builder.setMessage((int) R.string.msg_intent_failed);
            builder.setPositiveButton((int) R.string.button_ok, (DialogInterface.OnClickListener) null);
            builder.show();
        }
    }

    private static void putExtra(Intent intent, String key, String value) {
        if (value != null && value.length() > 0) {
            intent.putExtra(key, value);
        }
    }

    private String parseCustomSearchURL() {
        String customProductSearch2 = Two_dimension_codeInfoActivity.preferences(this.activity).getString(Two_dimension_codeInfoActivity.KEY_CUSTOM_PRODUCT_SEARCH, null);
        if (customProductSearch2 == null || customProductSearch2.trim().length() != 0) {
            return customProductSearch2;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String fillInCustomSearchURL(String text) {
        if (this.customProductSearch == null) {
            return text;
        }
        String url = this.customProductSearch.replace("%s", text);
        if (this.rawResult != null) {
            url = url.replace("%f", this.rawResult.getBarcodeFormat().toString());
            if (url.contains("%t")) {
                url = url.replace("%t", ResultParser.parseResult(this.rawResult).getType().toString());
            }
        }
        return url;
    }
}
