package com.pengyou.utils.two_dimension_code;

import android.app.Activity;
import android.telephony.PhoneNumberUtils;
import com.google.zxing.client.result.ParsedResult;
import com.pengyou.citycommercialarea.R;

public final class TelResultHandler extends ResultHandler {
    public TelResultHandler(Activity activity, ParsedResult result) {
        super(activity, result);
    }

    public CharSequence getDisplayContents() {
        return PhoneNumberUtils.formatNumber(getResult().getDisplayResult().replace("\r", ""));
    }

    public int getDisplayTitle() {
        return R.string.result_tel;
    }
}
