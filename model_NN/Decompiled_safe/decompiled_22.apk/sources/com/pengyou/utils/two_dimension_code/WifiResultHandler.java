package com.pengyou.utils.two_dimension_code;

import android.app.Activity;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.WifiParsedResult;
import com.pengyou.citycommercialarea.R;

public final class WifiResultHandler extends ResultHandler {
    private final Activity parent;

    public WifiResultHandler(Activity activity, ParsedResult result) {
        super(activity, result);
        this.parent = activity;
    }

    public CharSequence getDisplayContents() {
        WifiParsedResult wifiResult = (WifiParsedResult) getResult();
        StringBuilder contents = new StringBuilder(50);
        ParsedResult.maybeAppend(String.valueOf(this.parent.getString(R.string.wifi_ssid_label)) + 10 + wifiResult.getSsid(), contents);
        ParsedResult.maybeAppend(String.valueOf(this.parent.getString(R.string.wifi_type_label)) + 10 + wifiResult.getNetworkEncryption(), contents);
        return contents.toString();
    }

    public int getDisplayTitle() {
        return R.string.result_wifi;
    }
}
