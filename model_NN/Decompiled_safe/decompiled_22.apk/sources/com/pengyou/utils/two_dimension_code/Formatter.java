package com.pengyou.utils.two_dimension_code;

interface Formatter {
    String format(String str);
}
