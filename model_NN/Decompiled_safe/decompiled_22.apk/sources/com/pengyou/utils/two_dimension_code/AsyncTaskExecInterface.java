package com.pengyou.utils.two_dimension_code;

import android.os.AsyncTask;

public interface AsyncTaskExecInterface {
    <T> void execute(AsyncTask<T, ?, ?> asyncTask, T... tArr);
}
