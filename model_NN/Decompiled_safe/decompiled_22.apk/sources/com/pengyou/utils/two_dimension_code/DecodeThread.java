package com.pengyou.utils.two_dimension_code;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import com.city_life.part_activiy.SaomiaoActivity;
import com.city_life.part_activiy.Two_dimension_codeInfoActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.ResultPointCallback;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public final class DecodeThread extends Thread {
    public static final String BARCODE_BITMAP = "barcode_bitmap";
    private final SaomiaoActivity activity;
    private Handler handler;
    private final CountDownLatch handlerInitLatch = new CountDownLatch(1);
    private final Map<DecodeHintType, Object> hints = new EnumMap(DecodeHintType.class);

    public DecodeThread(SaomiaoActivity activity2, Collection<BarcodeFormat> decodeFormats, String characterSet, ResultPointCallback resultPointCallback) {
        this.activity = activity2;
        if (decodeFormats == null || decodeFormats.isEmpty()) {
            SharedPreferences prefs = Two_dimension_codeInfoActivity.preferences(activity2);
            decodeFormats = EnumSet.noneOf(BarcodeFormat.class);
            if (prefs.getBoolean(Two_dimension_codeInfoActivity.KEY_DECODE_1D, false)) {
                decodeFormats.addAll(DecodeFormatManager.ONE_D_FORMATS);
            }
            if (prefs.getBoolean(Two_dimension_codeInfoActivity.KEY_DECODE_QR, false)) {
                decodeFormats.addAll(DecodeFormatManager.QR_CODE_FORMATS);
            }
            if (prefs.getBoolean(Two_dimension_codeInfoActivity.KEY_DECODE_DATA_MATRIX, false)) {
                decodeFormats.addAll(DecodeFormatManager.DATA_MATRIX_FORMATS);
            }
        }
        this.hints.put(DecodeHintType.POSSIBLE_FORMATS, decodeFormats);
        if (characterSet != null) {
            this.hints.put(DecodeHintType.CHARACTER_SET, characterSet);
        }
        this.hints.put(DecodeHintType.NEED_RESULT_POINT_CALLBACK, resultPointCallback);
    }

    public Handler getHandler() {
        try {
            this.handlerInitLatch.await();
        } catch (InterruptedException e) {
        }
        return this.handler;
    }

    public void run() {
        Looper.prepare();
        this.handler = new DecodeHandler(this.activity, this.hints);
        this.handlerInitLatch.countDown();
        Looper.loop();
    }
}
