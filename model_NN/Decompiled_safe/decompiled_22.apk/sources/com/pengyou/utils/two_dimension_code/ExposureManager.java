package com.pengyou.utils.two_dimension_code;

public final class ExposureManager extends PlatformSupportManager<ExposureInterface> {
    public ExposureManager() {
        super(ExposureInterface.class, new DefaultExposureInterface());
        addImplementationClass(8, "com.example.myzxingtest.FroyoExposureInterface");
    }
}
