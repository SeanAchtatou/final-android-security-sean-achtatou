package com.pengyou.utils.two_dimension_code;

public enum IntentSource {
    NATIVE_APP_INTENT,
    PRODUCT_SEARCH_LINK,
    ZXING_LINK,
    NONE
}
