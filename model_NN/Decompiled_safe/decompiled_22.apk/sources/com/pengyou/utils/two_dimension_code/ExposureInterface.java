package com.pengyou.utils.two_dimension_code;

import android.hardware.Camera;

public interface ExposureInterface {
    void setExposure(Camera.Parameters parameters, boolean z);
}
