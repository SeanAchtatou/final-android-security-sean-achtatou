package com.pengyou.utils.two_dimension_code;

import android.app.Activity;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.URIParsedResult;
import com.pengyou.citycommercialarea.R;
import java.util.Locale;

public final class URIResultHandler extends ResultHandler {
    private static final String[] SECURE_PROTOCOLS = {"otpauth:"};

    public URIResultHandler(Activity activity, ParsedResult result) {
        super(activity, result);
    }

    public int getDisplayTitle() {
        return R.string.result_uri;
    }

    public boolean areContentsSecure() {
        String uri = ((URIParsedResult) getResult()).getURI().toLowerCase(Locale.ENGLISH);
        for (String secure : SECURE_PROTOCOLS) {
            if (uri.startsWith(secure)) {
                return true;
            }
        }
        return false;
    }
}
