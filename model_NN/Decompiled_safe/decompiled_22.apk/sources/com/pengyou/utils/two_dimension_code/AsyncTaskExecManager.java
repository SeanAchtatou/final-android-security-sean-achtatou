package com.pengyou.utils.two_dimension_code;

public final class AsyncTaskExecManager extends PlatformSupportManager<AsyncTaskExecInterface> {
    public AsyncTaskExecManager() {
        super(AsyncTaskExecInterface.class, new DefaultAsyncTaskExecInterface());
        addImplementationClass(11, "com.example.myzxingtest.HoneycombAsyncTaskExecInterface");
    }
}
