package com.pengyou.utils.two_dimension_code;

import android.app.Activity;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.pengyou.citycommercialarea.R;

public final class ISBNResultHandler extends ResultHandler {
    public ISBNResultHandler(Activity activity, ParsedResult result, Result rawResult) {
        super(activity, result, rawResult);
    }

    public int getDisplayTitle() {
        return R.string.result_isbn;
    }
}
