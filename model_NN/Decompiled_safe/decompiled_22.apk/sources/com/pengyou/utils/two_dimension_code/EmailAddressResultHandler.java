package com.pengyou.utils.two_dimension_code;

import android.app.Activity;
import com.google.zxing.client.result.ParsedResult;
import com.pengyou.citycommercialarea.R;

public final class EmailAddressResultHandler extends ResultHandler {
    public EmailAddressResultHandler(Activity activity, ParsedResult result) {
        super(activity, result);
    }

    public int getDisplayTitle() {
        return R.string.result_email_address;
    }
}
