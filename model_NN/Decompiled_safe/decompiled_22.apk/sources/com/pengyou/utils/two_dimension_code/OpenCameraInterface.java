package com.pengyou.utils.two_dimension_code;

import android.hardware.Camera;

public interface OpenCameraInterface {
    Camera open();
}
