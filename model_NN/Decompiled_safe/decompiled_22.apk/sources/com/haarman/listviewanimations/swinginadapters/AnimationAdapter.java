package com.haarman.listviewanimations.swinginadapters;

import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import com.haarman.listviewanimations.BaseAdapterDecorator;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

public abstract class AnimationAdapter extends BaseAdapterDecorator {
    protected static final long DEFAULTANIMATIONDELAYMILLIS = 100;
    protected static final long DEFAULTANIMATIONDURATIONMILLIS = 300;
    private static final long INITIALDELAYMILLIS = 150;
    private long mAnimationStartMillis = -1;
    private SparseArray<AnimationInfo> mAnimators = new SparseArray<>();
    private boolean mHasParentAnimationAdapter;
    private int mLastAnimatedPosition = -1;

    /* access modifiers changed from: protected */
    public abstract long getAnimationDelayMillis();

    /* access modifiers changed from: protected */
    public abstract long getAnimationDurationMillis();

    public abstract Animator[] getAnimators(ViewGroup viewGroup, View view);

    public AnimationAdapter(BaseAdapter baseAdapter) {
        super(baseAdapter);
        if (baseAdapter instanceof AnimationAdapter) {
            ((AnimationAdapter) baseAdapter).setHasParentAnimationAdapter(true);
        }
    }

    public void reset() {
        this.mAnimators.clear();
        this.mLastAnimatedPosition = -1;
        this.mAnimationStartMillis = -1;
        if (getDecoratedBaseAdapter() instanceof AnimationAdapter) {
            ((AnimationAdapter) getDecoratedBaseAdapter()).reset();
        }
    }

    public final View getView(int position, View convertView, ViewGroup parent) {
        int hashCode;
        AnimationInfo animationInfo;
        boolean alreadyStarted = false;
        if (!this.mHasParentAnimationAdapter) {
            if (getAbsListView() == null) {
                throw new IllegalStateException("Call setListView() on this AnimationAdapter before setAdapter()!");
            } else if (!(convertView == null || (animationInfo = this.mAnimators.get((hashCode = convertView.hashCode()))) == null)) {
                if (animationInfo.position != position) {
                    animationInfo.animator.end();
                    this.mAnimators.remove(hashCode);
                } else {
                    alreadyStarted = true;
                }
            }
        }
        View itemView = super.getView(position, convertView, parent);
        if (!this.mHasParentAnimationAdapter && !alreadyStarted) {
            animateViewIfNecessary(position, itemView, parent);
        }
        return itemView;
    }

    private void animateViewIfNecessary(int position, View view, ViewGroup parent) {
        if (position > this.mLastAnimatedPosition && !this.mHasParentAnimationAdapter) {
            animateView(position, parent, view);
            this.mLastAnimatedPosition = position;
        }
    }

    private void animateView(int position, ViewGroup parent, View view) {
        Animator[] childAnimators;
        if (this.mAnimationStartMillis == -1) {
            this.mAnimationStartMillis = System.currentTimeMillis();
        }
        hideView(view);
        if (this.mDecoratedBaseAdapter instanceof AnimationAdapter) {
            childAnimators = ((AnimationAdapter) this.mDecoratedBaseAdapter).getAnimators(parent, view);
        } else {
            childAnimators = new Animator[0];
        }
        Animator[] animators = getAnimators(parent, view);
        Animator alphaAnimator = ObjectAnimator.ofFloat(view, "alpha", 0.0f, 1.0f);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(concatAnimators(childAnimators, animators, alphaAnimator));
        set.setStartDelay(calculateAnimationDelay());
        set.setDuration(getAnimationDurationMillis());
        set.start();
        this.mAnimators.put(view.hashCode(), new AnimationInfo(position, set));
    }

    private void hideView(View view) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "alpha", 0.0f);
        AnimatorSet set = new AnimatorSet();
        set.play(animator);
        set.setDuration(0L);
        set.start();
    }

    private Animator[] concatAnimators(Animator[] childAnimators, Animator[] animators, Animator alphaAnimator) {
        Animator[] allAnimators = new Animator[(childAnimators.length + animators.length + 1)];
        int i = 0;
        while (i < animators.length) {
            allAnimators[i] = animators[i];
            i++;
        }
        for (Animator animator : childAnimators) {
            allAnimators[i] = animator;
            i++;
        }
        allAnimators[allAnimators.length - 1] = alphaAnimator;
        return allAnimators;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private long calculateAnimationDelay() {
        long delay;
        if ((getAbsListView().getLastVisiblePosition() - getAbsListView().getFirstVisiblePosition()) + 1 < this.mLastAnimatedPosition) {
            delay = getAnimationDelayMillis();
            if ((getAbsListView() instanceof GridView) && Build.VERSION.SDK_INT >= 11) {
                delay += getAnimationDelayMillis() * ((long) ((this.mLastAnimatedPosition + 1) % ((GridView) getAbsListView()).getNumColumns()));
            }
        } else {
            delay = ((this.mAnimationStartMillis + INITIALDELAYMILLIS) + (((long) (this.mLastAnimatedPosition + 1)) * getAnimationDelayMillis())) - System.currentTimeMillis();
        }
        return Math.max(0L, delay);
    }

    public void setHasParentAnimationAdapter(boolean hasParentAnimationAdapter) {
        this.mHasParentAnimationAdapter = hasParentAnimationAdapter;
    }

    private class AnimationInfo {
        public Animator animator;
        public int position;

        public AnimationInfo(int position2, Animator animator2) {
            this.position = position2;
            this.animator = animator2;
        }
    }
}
