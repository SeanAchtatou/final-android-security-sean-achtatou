package com.haarman.listviewanimations.swinginadapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.nineoldandroids.animation.Animator;

public abstract class SingleAnimationAdapter extends AnimationAdapter {
    /* access modifiers changed from: protected */
    public abstract Animator getAnimator(ViewGroup viewGroup, View view);

    public SingleAnimationAdapter(BaseAdapter baseAdapter) {
        super(baseAdapter);
    }

    public Animator[] getAnimators(ViewGroup parent, View view) {
        return new Animator[]{getAnimator(parent, view)};
    }
}
