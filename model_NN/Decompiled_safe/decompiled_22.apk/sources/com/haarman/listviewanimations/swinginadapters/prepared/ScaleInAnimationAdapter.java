package com.haarman.listviewanimations.swinginadapters.prepared;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.haarman.listviewanimations.swinginadapters.AnimationAdapter;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;

public class ScaleInAnimationAdapter extends AnimationAdapter {
    private static final float DEFAULTSCALEFROM = 0.8f;
    private long mAnimationDelayMillis;
    private long mAnimationDurationMillis;
    private float mScaleFrom;

    public ScaleInAnimationAdapter(BaseAdapter baseAdapter) {
        this(baseAdapter, DEFAULTSCALEFROM);
    }

    public ScaleInAnimationAdapter(BaseAdapter baseAdapter, float scaleFrom) {
        this(baseAdapter, scaleFrom, 100, 300);
    }

    public ScaleInAnimationAdapter(BaseAdapter baseAdapter, float scaleFrom, long animationDelayMillis, long animationDurationMillis) {
        super(baseAdapter);
        this.mScaleFrom = scaleFrom;
        this.mAnimationDelayMillis = animationDelayMillis;
        this.mAnimationDurationMillis = animationDurationMillis;
    }

    /* access modifiers changed from: protected */
    public long getAnimationDelayMillis() {
        return this.mAnimationDelayMillis;
    }

    /* access modifiers changed from: protected */
    public long getAnimationDurationMillis() {
        return this.mAnimationDurationMillis;
    }

    public Animator[] getAnimators(ViewGroup parent, View view) {
        return new ObjectAnimator[]{ObjectAnimator.ofFloat(view, "scaleX", this.mScaleFrom, 1.0f), ObjectAnimator.ofFloat(view, "scaleY", this.mScaleFrom, 1.0f)};
    }
}
