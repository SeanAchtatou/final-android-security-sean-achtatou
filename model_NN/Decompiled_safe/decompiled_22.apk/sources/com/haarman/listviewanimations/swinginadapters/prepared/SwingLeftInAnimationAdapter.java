package com.haarman.listviewanimations.swinginadapters.prepared;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.haarman.listviewanimations.swinginadapters.SingleAnimationAdapter;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;

public class SwingLeftInAnimationAdapter extends SingleAnimationAdapter {
    private final long mAnimationDelayMillis;
    private final long mAnimationDurationMillis;

    public SwingLeftInAnimationAdapter(BaseAdapter baseAdapter) {
        this(baseAdapter, 100, 300);
    }

    public SwingLeftInAnimationAdapter(BaseAdapter baseAdapter, long animationDelayMillis) {
        this(baseAdapter, animationDelayMillis, 300);
    }

    public SwingLeftInAnimationAdapter(BaseAdapter baseAdapter, long animationDelayMillis, long animationDurationMillis) {
        super(baseAdapter);
        this.mAnimationDelayMillis = animationDelayMillis;
        this.mAnimationDurationMillis = animationDurationMillis;
    }

    /* access modifiers changed from: protected */
    public long getAnimationDelayMillis() {
        return this.mAnimationDelayMillis;
    }

    /* access modifiers changed from: protected */
    public long getAnimationDurationMillis() {
        return this.mAnimationDurationMillis;
    }

    /* access modifiers changed from: protected */
    public Animator getAnimator(ViewGroup parent, View view) {
        return ObjectAnimator.ofFloat(view, "translationX", (float) (0 - parent.getWidth()), 0.0f);
    }
}
