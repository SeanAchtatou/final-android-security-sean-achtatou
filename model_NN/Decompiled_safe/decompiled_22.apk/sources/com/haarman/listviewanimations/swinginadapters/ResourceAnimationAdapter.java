package com.haarman.listviewanimations.swinginadapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorInflater;

public abstract class ResourceAnimationAdapter<T> extends AnimationAdapter {
    private Context mContext;

    /* access modifiers changed from: protected */
    public abstract int getAnimationResourceId();

    public ResourceAnimationAdapter(BaseAdapter baseAdapter, Context context) {
        super(baseAdapter);
        this.mContext = context;
    }

    public Animator[] getAnimators(ViewGroup parent, View view) {
        return new Animator[]{AnimatorInflater.loadAnimator(this.mContext, getAnimationResourceId())};
    }
}
