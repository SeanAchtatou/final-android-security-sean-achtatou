package com.haarman.listviewanimations.itemmanipulation;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AbsListView;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressLint({"Recycle"})
public class SwipeDismissListViewTouchListener implements View.OnTouchListener {
    private long mAnimationTime;
    /* access modifiers changed from: private */
    public OnDismissCallback mCallback;
    /* access modifiers changed from: private */
    public int mDismissAnimationRefCount = 0;
    private int mDownPosition;
    private View mDownView;
    private float mDownX;
    private float mDownY;
    /* access modifiers changed from: private */
    public AbsListView mListView;
    private int mMaxFlingVelocity;
    private int mMinFlingVelocity;
    private boolean mPaused;
    /* access modifiers changed from: private */
    public List<PendingDismissData> mPendingDismisses = new ArrayList();
    private int mSlop;
    private boolean mSwiping;
    private VelocityTracker mVelocityTracker;
    private int mViewWidth = 1;

    public SwipeDismissListViewTouchListener(AbsListView listView, OnDismissCallback callback) {
        ViewConfiguration vc = ViewConfiguration.get(listView.getContext());
        this.mSlop = vc.getScaledTouchSlop();
        this.mMinFlingVelocity = vc.getScaledMinimumFlingVelocity() * 16;
        this.mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
        this.mAnimationTime = (long) listView.getContext().getResources().getInteger(17694720);
        this.mListView = listView;
        this.mCallback = callback;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.mViewWidth < 2) {
            this.mViewWidth = this.mListView.getWidth();
        }
        switch (motionEvent.getActionMasked()) {
            case 0:
                if (this.mPaused) {
                    return false;
                }
                Rect rect = new Rect();
                int childCount = this.mListView.getChildCount();
                int[] listViewCoords = new int[2];
                this.mListView.getLocationOnScreen(listViewCoords);
                int x = ((int) motionEvent.getRawX()) - listViewCoords[0];
                int y = ((int) motionEvent.getRawY()) - listViewCoords[1];
                int i = 0;
                while (true) {
                    if (i < childCount) {
                        View child = this.mListView.getChildAt(i);
                        child.getHitRect(rect);
                        if (rect.contains(x, y)) {
                            this.mDownView = child;
                        } else {
                            i++;
                        }
                    }
                }
                if (this.mDownView != null) {
                    this.mDownX = motionEvent.getRawX();
                    this.mDownY = motionEvent.getRawY();
                    this.mDownPosition = this.mListView.getPositionForView(this.mDownView);
                    this.mVelocityTracker = VelocityTracker.obtain();
                    this.mVelocityTracker.addMovement(motionEvent);
                }
                view.onTouchEvent(motionEvent);
                return true;
            case 1:
                if (this.mVelocityTracker != null) {
                    float deltaX = motionEvent.getRawX() - this.mDownX;
                    this.mVelocityTracker.addMovement(motionEvent);
                    this.mVelocityTracker.computeCurrentVelocity(1000);
                    float velocityX = Math.abs(this.mVelocityTracker.getXVelocity());
                    float velocityY = Math.abs(this.mVelocityTracker.getYVelocity());
                    boolean dismiss = false;
                    boolean dismissRight = false;
                    if (Math.abs(deltaX) > ((float) (this.mViewWidth / 2))) {
                        dismiss = true;
                        dismissRight = deltaX > 0.0f;
                    } else if (((float) this.mMinFlingVelocity) <= velocityX && velocityX <= ((float) this.mMaxFlingVelocity) && velocityY < velocityX) {
                        dismiss = true;
                        dismissRight = this.mVelocityTracker.getXVelocity() > 0.0f;
                    }
                    if (dismiss) {
                        final View downView = this.mDownView;
                        final int downPosition = this.mDownPosition;
                        this.mDismissAnimationRefCount = this.mDismissAnimationRefCount + 1;
                        ViewPropertyAnimator.animate(this.mDownView).translationX((float) (dismissRight ? this.mViewWidth : -this.mViewWidth)).alpha(0.0f).setDuration(this.mAnimationTime).setListener(new AnimatorListenerAdapter() {
                            public void onAnimationEnd(Animator animation) {
                                SwipeDismissListViewTouchListener.this.performDismiss(downView, downPosition);
                            }
                        });
                    } else {
                        ViewPropertyAnimator.animate(this.mDownView).translationX(0.0f).alpha(1.0f).setDuration(this.mAnimationTime).setListener(null);
                    }
                    this.mVelocityTracker = null;
                    this.mDownX = 0.0f;
                    this.mDownView = null;
                    this.mDownPosition = -1;
                    this.mSwiping = false;
                    break;
                }
                break;
            case 2:
                if (this.mVelocityTracker != null && !this.mPaused) {
                    this.mVelocityTracker.addMovement(motionEvent);
                    float deltaX2 = motionEvent.getRawX() - this.mDownX;
                    float deltaY = motionEvent.getRawY() - this.mDownY;
                    if (Math.abs(deltaX2) > ((float) this.mSlop) && Math.abs(deltaX2) > Math.abs(deltaY)) {
                        this.mSwiping = true;
                        this.mListView.requestDisallowInterceptTouchEvent(true);
                        MotionEvent cancelEvent = MotionEvent.obtain(motionEvent);
                        cancelEvent.setAction((motionEvent.getActionIndex() << 8) | 3);
                        this.mListView.onTouchEvent(cancelEvent);
                    }
                    if (this.mSwiping) {
                        ViewHelper.setTranslationX(this.mDownView, deltaX2);
                        ViewHelper.setAlpha(this.mDownView, Math.max(0.0f, Math.min(1.0f, 1.0f - ((2.0f * Math.abs(deltaX2)) / ((float) this.mViewWidth)))));
                        return true;
                    }
                }
                break;
        }
        return false;
    }

    class PendingDismissData implements Comparable<PendingDismissData> {
        public int position;
        public View view;

        public PendingDismissData(int position2, View view2) {
            this.position = position2;
            this.view = view2;
        }

        public int compareTo(PendingDismissData other) {
            return other.position - this.position;
        }
    }

    /* access modifiers changed from: private */
    public void performDismiss(final View dismissView, int dismissPosition) {
        final ViewGroup.LayoutParams lp = dismissView.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt(dismissView.getHeight(), 1).setDuration(this.mAnimationTime);
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                SwipeDismissListViewTouchListener swipeDismissListViewTouchListener = SwipeDismissListViewTouchListener.this;
                swipeDismissListViewTouchListener.mDismissAnimationRefCount = swipeDismissListViewTouchListener.mDismissAnimationRefCount - 1;
                if (SwipeDismissListViewTouchListener.this.mDismissAnimationRefCount == 0) {
                    Collections.sort(SwipeDismissListViewTouchListener.this.mPendingDismisses);
                    int[] dismissPositions = new int[SwipeDismissListViewTouchListener.this.mPendingDismisses.size()];
                    for (int i = SwipeDismissListViewTouchListener.this.mPendingDismisses.size() - 1; i >= 0; i--) {
                        dismissPositions[i] = ((PendingDismissData) SwipeDismissListViewTouchListener.this.mPendingDismisses.get(i)).position;
                    }
                    SwipeDismissListViewTouchListener.this.mCallback.onDismiss(SwipeDismissListViewTouchListener.this.mListView, dismissPositions);
                    for (PendingDismissData pendingDismiss : SwipeDismissListViewTouchListener.this.mPendingDismisses) {
                        ViewHelper.setAlpha(pendingDismiss.view, 1.0f);
                        ViewHelper.setTranslationX(pendingDismiss.view, 0.0f);
                        ViewGroup.LayoutParams lp = pendingDismiss.view.getLayoutParams();
                        lp.height = 0;
                        pendingDismiss.view.setLayoutParams(lp);
                    }
                    SwipeDismissListViewTouchListener.this.mPendingDismisses.clear();
                }
            }
        });
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                lp.height = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                dismissView.setLayoutParams(lp);
            }
        });
        this.mPendingDismisses.add(new PendingDismissData(dismissPosition, dismissView));
        animator.start();
    }
}
