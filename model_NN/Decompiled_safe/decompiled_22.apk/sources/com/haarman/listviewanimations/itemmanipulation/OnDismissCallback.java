package com.haarman.listviewanimations.itemmanipulation;

import android.widget.AbsListView;

public interface OnDismissCallback {
    void onDismiss(AbsListView absListView, int[] iArr);
}
