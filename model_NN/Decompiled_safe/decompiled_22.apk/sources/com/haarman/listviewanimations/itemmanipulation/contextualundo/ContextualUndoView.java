package com.haarman.listviewanimations.itemmanipulation.contextualundo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

@SuppressLint({"ViewConstructor"})
public class ContextualUndoView extends FrameLayout {
    private View mContentView;
    private long mItemId;
    private View mUndoView;

    public ContextualUndoView(Context context, int undoLayoutResourceId) {
        super(context);
        initUndo(undoLayoutResourceId);
    }

    private void initUndo(int undoLayoutResourceId) {
        this.mUndoView = View.inflate(getContext(), undoLayoutResourceId, null);
        addView(this.mUndoView);
    }

    public void updateContentView(View contentView) {
        if (this.mContentView == null) {
            addView(contentView);
        }
        this.mContentView = contentView;
    }

    public View getContentView() {
        return this.mContentView;
    }

    public void setItemId(long itemId) {
        this.mItemId = itemId;
    }

    public long getItemId() {
        return this.mItemId;
    }

    public boolean isContentDisplayed() {
        return this.mContentView.getVisibility() == 0;
    }

    public void displayUndo() {
        this.mContentView.setVisibility(8);
        this.mUndoView.setVisibility(0);
    }

    public void displayContentView() {
        this.mContentView.setVisibility(0);
        this.mUndoView.setVisibility(8);
    }
}
