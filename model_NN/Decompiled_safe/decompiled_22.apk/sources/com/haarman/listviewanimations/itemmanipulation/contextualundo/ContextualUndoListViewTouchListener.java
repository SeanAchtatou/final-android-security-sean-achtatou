package com.haarman.listviewanimations.itemmanipulation.contextualundo;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.AbsListView;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

public class ContextualUndoListViewTouchListener implements View.OnTouchListener {
    private long mAnimationTime;
    /* access modifiers changed from: private */
    public Callback mCallback;
    private int mDownPosition;
    private View mDownView;
    private float mDownX;
    private AbsListView mListView;
    private int mMaxFlingVelocity;
    private int mMinFlingVelocity;
    /* access modifiers changed from: private */
    public boolean mPaused;
    private int mSlop;
    private boolean mSwiping;
    private VelocityTracker mVelocityTracker;
    private int mViewWidth = 1;

    public interface Callback {
        void onListScrolled();

        void onViewSwiped(View view, int i);
    }

    public ContextualUndoListViewTouchListener(AbsListView listView, Callback callback) {
        ViewConfiguration vc = ViewConfiguration.get(listView.getContext());
        this.mSlop = vc.getScaledTouchSlop();
        this.mMinFlingVelocity = vc.getScaledMinimumFlingVelocity();
        this.mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
        this.mAnimationTime = (long) listView.getContext().getResources().getInteger(17694720);
        this.mListView = listView;
        this.mCallback = callback;
    }

    public void setEnabled(boolean enabled) {
        this.mPaused = !enabled;
    }

    public AbsListView.OnScrollListener makeScrollListener() {
        return new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                boolean z = true;
                ContextualUndoListViewTouchListener contextualUndoListViewTouchListener = ContextualUndoListViewTouchListener.this;
                if (scrollState == 1) {
                    z = false;
                }
                contextualUndoListViewTouchListener.setEnabled(z);
                if (ContextualUndoListViewTouchListener.this.mPaused) {
                    ContextualUndoListViewTouchListener.this.mCallback.onListScrolled();
                }
            }

            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
            }
        };
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i;
        if (this.mViewWidth < 2) {
            this.mViewWidth = this.mListView.getWidth();
        }
        switch (motionEvent.getActionMasked()) {
            case 0:
                if (this.mPaused) {
                    return false;
                }
                Rect rect = new Rect();
                int childCount = this.mListView.getChildCount();
                int[] listViewCoords = new int[2];
                this.mListView.getLocationOnScreen(listViewCoords);
                int x = ((int) motionEvent.getRawX()) - listViewCoords[0];
                int y = ((int) motionEvent.getRawY()) - listViewCoords[1];
                int i2 = 0;
                while (true) {
                    if (i2 < childCount) {
                        View child = this.mListView.getChildAt(i2);
                        child.getHitRect(rect);
                        if (rect.contains(x, y)) {
                            this.mDownView = child;
                        } else {
                            i2++;
                        }
                    }
                }
                if (this.mDownView != null) {
                    this.mDownX = motionEvent.getRawX();
                    this.mDownPosition = this.mListView.getPositionForView(this.mDownView);
                    this.mVelocityTracker = VelocityTracker.obtain();
                    this.mVelocityTracker.addMovement(motionEvent);
                }
                view.onTouchEvent(motionEvent);
                return true;
            case 1:
                if (this.mVelocityTracker != null) {
                    float deltaX = motionEvent.getRawX() - this.mDownX;
                    this.mVelocityTracker.addMovement(motionEvent);
                    this.mVelocityTracker.computeCurrentVelocity(1000);
                    float velocityX = Math.abs(this.mVelocityTracker.getXVelocity());
                    float velocityY = Math.abs(this.mVelocityTracker.getYVelocity());
                    boolean dismiss = false;
                    boolean dismissRight = false;
                    if (Math.abs(deltaX) > ((float) (this.mViewWidth / 2))) {
                        dismiss = true;
                        dismissRight = deltaX > 0.0f;
                    } else if (((float) this.mMinFlingVelocity) <= velocityX && velocityX <= ((float) this.mMaxFlingVelocity) && velocityY < velocityX) {
                        dismiss = true;
                        dismissRight = this.mVelocityTracker.getXVelocity() > 0.0f;
                    }
                    if (dismiss) {
                        final View downView = this.mDownView;
                        final int downPosition = this.mDownPosition;
                        ViewPropertyAnimator animate = ViewPropertyAnimator.animate(this.mDownView);
                        if (dismissRight) {
                            i = this.mViewWidth;
                        } else {
                            i = -this.mViewWidth;
                        }
                        animate.translationX((float) i).alpha(0.0f).setDuration(this.mAnimationTime).setListener(new AnimatorListenerAdapter() {
                            public void onAnimationEnd(Animator animation) {
                                ContextualUndoListViewTouchListener.this.mCallback.onViewSwiped(downView, downPosition);
                            }
                        });
                    } else {
                        ViewPropertyAnimator.animate(this.mDownView).translationX(0.0f).alpha(1.0f).setDuration(this.mAnimationTime).setListener(null);
                    }
                    this.mVelocityTracker = null;
                    this.mDownX = 0.0f;
                    this.mDownView = null;
                    this.mDownPosition = -1;
                    this.mSwiping = false;
                    break;
                }
                break;
            case 2:
                if (this.mVelocityTracker != null && !this.mPaused) {
                    this.mVelocityTracker.addMovement(motionEvent);
                    float deltaX2 = motionEvent.getRawX() - this.mDownX;
                    if (Math.abs(deltaX2) > ((float) this.mSlop)) {
                        this.mSwiping = true;
                        this.mListView.requestDisallowInterceptTouchEvent(true);
                        MotionEvent cancelEvent = MotionEvent.obtain(motionEvent);
                        cancelEvent.setAction((motionEvent.getActionIndex() << 8) | 3);
                        this.mListView.onTouchEvent(cancelEvent);
                    }
                    if (this.mSwiping) {
                        ViewHelper.setTranslationX(this.mDownView, deltaX2);
                        ViewHelper.setAlpha(this.mDownView, Math.max(0.0f, Math.min(1.0f, 1.0f - ((2.0f * Math.abs(deltaX2)) / ((float) this.mViewWidth)))));
                        return true;
                    }
                }
                break;
        }
        return false;
    }
}
