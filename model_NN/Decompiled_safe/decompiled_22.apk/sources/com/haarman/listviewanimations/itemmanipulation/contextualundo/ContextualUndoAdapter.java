package com.haarman.listviewanimations.itemmanipulation.contextualundo;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import com.haarman.listviewanimations.BaseAdapterDecorator;
import com.haarman.listviewanimations.itemmanipulation.contextualundo.ContextualUndoListViewTouchListener;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ContextualUndoAdapter extends BaseAdapterDecorator implements ContextualUndoListViewTouchListener.Callback {
    /* access modifiers changed from: private */
    public Map<View, Animator> mActiveAnimators = new ConcurrentHashMap();
    private final int mAnimationTime = 150;
    private long mCurrentRemovedId;
    private ContextualUndoView mCurrentRemovedView;
    /* access modifiers changed from: private */
    public DeleteItemCallback mDeleteItemCallback;
    private final int mUndoActionId;
    private final int mUndoLayoutId;

    public interface DeleteItemCallback {
        void deleteItem(int i);
    }

    public ContextualUndoAdapter(BaseAdapter baseAdapter, int undoLayoutId, int undoActionId) {
        super(baseAdapter);
        this.mUndoLayoutId = undoLayoutId;
        this.mUndoActionId = undoActionId;
        this.mCurrentRemovedId = -1;
    }

    public final View getView(int position, View convertView, ViewGroup parent) {
        ContextualUndoView contextualUndoView = (ContextualUndoView) convertView;
        if (contextualUndoView == null) {
            contextualUndoView = new ContextualUndoView(parent.getContext(), this.mUndoLayoutId);
            contextualUndoView.findViewById(this.mUndoActionId).setOnClickListener(new UndoListener(contextualUndoView));
        }
        contextualUndoView.updateContentView(super.getView(position, contextualUndoView.getContentView(), parent));
        long itemId = getItemId(position);
        if (itemId == this.mCurrentRemovedId) {
            contextualUndoView.displayUndo();
            this.mCurrentRemovedView = contextualUndoView;
        } else {
            contextualUndoView.displayContentView();
        }
        contextualUndoView.setItemId(itemId);
        return contextualUndoView;
    }

    public void setAbsListView(AbsListView listView) {
        super.setAbsListView(listView);
        ContextualUndoListViewTouchListener contextualUndoListViewTouchListener = new ContextualUndoListViewTouchListener(listView, this);
        listView.setOnTouchListener(contextualUndoListViewTouchListener);
        listView.setOnScrollListener(contextualUndoListViewTouchListener.makeScrollListener());
        listView.setRecyclerListener(new RecycleViewListener(this, null));
    }

    public void onViewSwiped(View dismissView, int dismissPosition) {
        ContextualUndoView contextualUndoView = (ContextualUndoView) dismissView;
        if (contextualUndoView.isContentDisplayed()) {
            restoreViewPosition(contextualUndoView);
            contextualUndoView.displayUndo();
            removePreviousContextualUndoIfPresent();
            setCurrentRemovedView(contextualUndoView);
        } else if (this.mCurrentRemovedView != null) {
            performRemoval();
        }
    }

    /* access modifiers changed from: private */
    public void restoreViewPosition(View view) {
        ViewHelper.setAlpha(view, 1.0f);
        ViewHelper.setTranslationX(view, 0.0f);
    }

    private void removePreviousContextualUndoIfPresent() {
        if (this.mCurrentRemovedView != null) {
            performRemoval();
        }
    }

    private void setCurrentRemovedView(ContextualUndoView currentRemovedView) {
        this.mCurrentRemovedView = currentRemovedView;
        this.mCurrentRemovedId = currentRemovedView.getItemId();
    }

    /* access modifiers changed from: private */
    public void clearCurrentRemovedView() {
        this.mCurrentRemovedView = null;
        this.mCurrentRemovedId = -1;
    }

    public void onListScrolled() {
        if (this.mCurrentRemovedView != null) {
            performRemoval();
        }
    }

    private void performRemoval() {
        ValueAnimator animator = ValueAnimator.ofInt(this.mCurrentRemovedView.getHeight(), 1).setDuration(150L);
        animator.addListener(new RemoveViewAnimatorListenerAdapter(this.mCurrentRemovedView));
        animator.addUpdateListener(new RemoveViewAnimatorUpdateListener(this.mCurrentRemovedView));
        animator.start();
        this.mActiveAnimators.put(this.mCurrentRemovedView, animator);
        clearCurrentRemovedView();
    }

    public void setDeleteItemCallback(DeleteItemCallback deleteItemCallback) {
        this.mDeleteItemCallback = deleteItemCallback;
    }

    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putLong("mCurrentRemovedId", this.mCurrentRemovedId);
        return bundle;
    }

    public void onRestoreInstanceState(Parcelable state) {
        this.mCurrentRemovedId = ((Bundle) state).getLong("mCurrentRemovedId", -1);
    }

    private class RemoveViewAnimatorListenerAdapter extends AnimatorListenerAdapter {
        private final View mDismissView;
        private final int mOriginalHeight;

        public RemoveViewAnimatorListenerAdapter(View dismissView) {
            this.mDismissView = dismissView;
            this.mOriginalHeight = dismissView.getHeight();
        }

        public void onAnimationEnd(Animator animation) {
            ContextualUndoAdapter.this.mActiveAnimators.remove(this.mDismissView);
            ContextualUndoAdapter.this.restoreViewPosition(this.mDismissView);
            restoreViewDimension(this.mDismissView);
            deleteCurrentItem();
        }

        private void restoreViewDimension(View view) {
            ViewGroup.LayoutParams lp = view.getLayoutParams();
            lp.height = this.mOriginalHeight;
            view.setLayoutParams(lp);
        }

        private void deleteCurrentItem() {
            ContextualUndoAdapter.this.mDeleteItemCallback.deleteItem(ContextualUndoAdapter.this.getAbsListView().getPositionForView((ContextualUndoView) this.mDismissView));
        }
    }

    private class RemoveViewAnimatorUpdateListener implements ValueAnimator.AnimatorUpdateListener {
        private final View mDismissView;
        private final ViewGroup.LayoutParams mLayoutParams;

        public RemoveViewAnimatorUpdateListener(View dismissView) {
            this.mDismissView = dismissView;
            this.mLayoutParams = dismissView.getLayoutParams();
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.mLayoutParams.height = ((Integer) valueAnimator.getAnimatedValue()).intValue();
            this.mDismissView.setLayoutParams(this.mLayoutParams);
        }
    }

    private class UndoListener implements View.OnClickListener {
        private final ContextualUndoView mContextualUndoView;

        public UndoListener(ContextualUndoView contextualUndoView) {
            this.mContextualUndoView = contextualUndoView;
        }

        public void onClick(View v) {
            ContextualUndoAdapter.this.clearCurrentRemovedView();
            this.mContextualUndoView.displayContentView();
            moveViewOffScreen();
            animateViewComingBack();
        }

        private void moveViewOffScreen() {
            ViewHelper.setTranslationX(this.mContextualUndoView, (float) this.mContextualUndoView.getWidth());
        }

        private void animateViewComingBack() {
            ViewPropertyAnimator.animate(this.mContextualUndoView).translationX(0.0f).setDuration(150).setListener(null);
        }
    }

    private class RecycleViewListener implements AbsListView.RecyclerListener {
        private RecycleViewListener() {
        }

        /* synthetic */ RecycleViewListener(ContextualUndoAdapter contextualUndoAdapter, RecycleViewListener recycleViewListener) {
            this();
        }

        public void onMovedToScrapHeap(View view) {
            Animator animator = (Animator) ContextualUndoAdapter.this.mActiveAnimators.get(view);
            if (animator != null) {
                animator.cancel();
            }
        }
    }
}
