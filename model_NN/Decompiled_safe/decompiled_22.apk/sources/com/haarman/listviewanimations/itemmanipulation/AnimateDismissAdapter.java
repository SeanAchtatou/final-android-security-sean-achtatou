package com.haarman.listviewanimations.itemmanipulation;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.haarman.listviewanimations.BaseAdapterDecorator;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ValueAnimator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class AnimateDismissAdapter<T> extends BaseAdapterDecorator {
    private OnDismissCallback mCallback;

    public AnimateDismissAdapter(BaseAdapter baseAdapter, OnDismissCallback callback) {
        super(baseAdapter);
        this.mCallback = callback;
    }

    public void animateDismiss(int index) {
        animateDismiss(Arrays.asList(Integer.valueOf(index)));
    }

    public void animateDismiss(Collection<Integer> positions) {
        final List<Integer> positionsCopy = new ArrayList<>(positions);
        if (getAbsListView() == null) {
            throw new IllegalStateException("Call setListView() on this AnimateDismissAdapter before calling setAdapter()!");
        }
        List<View> views = getVisibleViewsForPositions(positionsCopy);
        if (!views.isEmpty()) {
            List<Animator> animators = new ArrayList<>();
            for (View view : views) {
                animators.add(createAnimatorForView(view));
            }
            AnimatorSet animatorSet = new AnimatorSet();
            Animator[] animatorsArray = new Animator[animators.size()];
            for (int i = 0; i < animatorsArray.length; i++) {
                animatorsArray[i] = (Animator) animators.get(i);
            }
            animatorSet.playTogether(animatorsArray);
            animatorSet.addListener(new Animator.AnimatorListener() {
                public void onAnimationStart(Animator arg0) {
                }

                public void onAnimationRepeat(Animator arg0) {
                }

                public void onAnimationEnd(Animator arg0) {
                    AnimateDismissAdapter.this.invokeCallback(positionsCopy);
                }

                public void onAnimationCancel(Animator arg0) {
                }
            });
            animatorSet.start();
            return;
        }
        invokeCallback(positionsCopy);
    }

    /* access modifiers changed from: private */
    public void invokeCallback(Collection<Integer> positions) {
        ArrayList<Integer> positionsList = new ArrayList<>(positions);
        Collections.sort(positionsList);
        int[] dismissPositions = new int[positionsList.size()];
        for (int i = 0; i < positionsList.size(); i++) {
            dismissPositions[i] = ((Integer) positionsList.get((positionsList.size() - 1) - i)).intValue();
        }
        this.mCallback.onDismiss(getAbsListView(), dismissPositions);
    }

    private List<View> getVisibleViewsForPositions(Collection<Integer> positions) {
        List<View> views = new ArrayList<>();
        for (int i = 0; i < getAbsListView().getChildCount(); i++) {
            View child = getAbsListView().getChildAt(i);
            if (positions.contains(Integer.valueOf(getAbsListView().getPositionForView(child)))) {
                views.add(child);
            }
        }
        return views;
    }

    private Animator createAnimatorForView(final View view) {
        final ViewGroup.LayoutParams lp = view.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt(view.getHeight(), 0);
        animator.addListener(new Animator.AnimatorListener() {
            public void onAnimationStart(Animator arg0) {
            }

            public void onAnimationRepeat(Animator arg0) {
            }

            public void onAnimationEnd(Animator arg0) {
                lp.height = 0;
                view.setLayoutParams(lp);
            }

            public void onAnimationCancel(Animator arg0) {
            }
        });
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                lp.height = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                view.setLayoutParams(lp);
            }
        });
        return animator;
    }
}
