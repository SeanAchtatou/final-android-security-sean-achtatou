package com.haarman.listviewanimations.itemmanipulation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.haarman.listviewanimations.ArrayAdapter;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ValueAnimator;
import java.util.ArrayList;
import java.util.List;

public abstract class ExpandableListItemAdapter<T> extends ArrayAdapter<T> {
    private static final int DEFAULTCONTENTPARENTRESID = 10001;
    private static final int DEFAULTTITLEPARENTRESID = 10000;
    private int mContentParentResId;
    private Context mContext;
    private int mTitleParentResId;
    private int mViewLayoutResId;
    /* access modifiers changed from: private */
    public List<Long> mVisibleIds;

    public abstract View getContentView(int i, View view, ViewGroup viewGroup);

    public abstract View getTitleView(int i, View view, ViewGroup viewGroup);

    protected ExpandableListItemAdapter(Context context) {
        this(context, null);
    }

    protected ExpandableListItemAdapter(Context context, List<T> items) {
        super(items);
        this.mContext = context;
        this.mTitleParentResId = 10000;
        this.mContentParentResId = 10001;
        this.mVisibleIds = new ArrayList();
    }

    protected ExpandableListItemAdapter(Context context, int layoutResId, int titleParentResId, int contentParentResId) {
        this(context, layoutResId, titleParentResId, contentParentResId, null);
    }

    protected ExpandableListItemAdapter(Context context, int layoutResId, int titleParentResId, int contentParentResId, List<T> items) {
        super(items);
        this.mContext = context;
        this.mViewLayoutResId = layoutResId;
        this.mTitleParentResId = titleParentResId;
        this.mContentParentResId = contentParentResId;
        this.mVisibleIds = new ArrayList();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        ViewGroup view = (ViewGroup) convertView;
        if (view == null) {
            view = createView(parent);
            viewHolder = new ViewHolder(null);
            viewHolder.titleParent = (ViewGroup) view.findViewById(this.mTitleParentResId);
            viewHolder.contentParent = (ViewGroup) view.findViewById(this.mContentParentResId);
            view.setOnClickListener(new TitleViewOnClickListener(this, viewHolder.contentParent, null));
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        View titleView = getTitleView(position, viewHolder.titleView, viewHolder.titleParent);
        if (!titleView.equals(viewHolder.titleView)) {
            viewHolder.titleParent.removeAllViews();
            viewHolder.titleParent.addView(titleView);
        }
        viewHolder.titleView = titleView;
        View contentView = getContentView(position, viewHolder.contentView, viewHolder.contentParent);
        if (!contentView.equals(viewHolder.contentView)) {
            viewHolder.contentParent.removeAllViews();
            viewHolder.contentParent.addView(contentView);
        }
        viewHolder.titleView = titleView;
        viewHolder.contentParent.setVisibility(this.mVisibleIds.contains(Long.valueOf(getItemId(position))) ? 0 : 8);
        viewHolder.contentParent.setTag(Long.valueOf(getItemId(position)));
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private ViewGroup createView(ViewGroup parent) {
        if (this.mViewLayoutResId == 0) {
            return new RootView(this.mContext);
        }
        return (ViewGroup) LayoutInflater.from(this.mContext).inflate(this.mViewLayoutResId, parent, false);
    }

    private static class ViewHolder {
        ViewGroup contentParent;
        View contentView;
        ViewGroup titleParent;
        View titleView;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }

    private static class RootView extends LinearLayout {
        private ViewGroup mContentViewGroup;
        private ViewGroup mTitleViewGroup;

        public RootView(Context context) {
            super(context);
            init();
        }

        private void init() {
            setOrientation(1);
            this.mTitleViewGroup = new FrameLayout(getContext());
            this.mTitleViewGroup.setId(10000);
            addView(this.mTitleViewGroup);
            this.mContentViewGroup = new FrameLayout(getContext());
            this.mContentViewGroup.setId(10001);
            addView(this.mContentViewGroup);
        }
    }

    private class TitleViewOnClickListener implements View.OnClickListener {
        /* access modifiers changed from: private */
        public View mContentParent;

        private TitleViewOnClickListener(View contentParent) {
            this.mContentParent = contentParent;
        }

        /* synthetic */ TitleViewOnClickListener(ExpandableListItemAdapter expandableListItemAdapter, View view, TitleViewOnClickListener titleViewOnClickListener) {
            this(view);
        }

        public void onClick(View view) {
            if (this.mContentParent.getVisibility() == 0) {
                animateCollapsing();
                ExpandableListItemAdapter.this.mVisibleIds.remove(this.mContentParent.getTag());
                return;
            }
            animateExpanding();
            ExpandableListItemAdapter.this.mVisibleIds.add((Long) this.mContentParent.getTag());
        }

        private void animateCollapsing() {
            ValueAnimator animator = createHeightAnimator(this.mContentParent.getHeight(), 0);
            animator.addListener(new Animator.AnimatorListener() {
                public void onAnimationStart(Animator animator) {
                }

                public void onAnimationEnd(Animator animator) {
                    TitleViewOnClickListener.this.mContentParent.setVisibility(8);
                }

                public void onAnimationCancel(Animator animator) {
                }

                public void onAnimationRepeat(Animator animator) {
                }
            });
            animator.start();
        }

        private void animateExpanding() {
            this.mContentParent.setVisibility(0);
            this.mContentParent.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            createHeightAnimator(0, this.mContentParent.getMeasuredHeight()).start();
        }

        private ValueAnimator createHeightAnimator(int start, int end) {
            ValueAnimator animator = ValueAnimator.ofInt(start, end);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int value = ((Integer) valueAnimator.getAnimatedValue()).intValue();
                    ViewGroup.LayoutParams layoutParams = TitleViewOnClickListener.this.mContentParent.getLayoutParams();
                    layoutParams.height = value;
                    TitleViewOnClickListener.this.mContentParent.setLayoutParams(layoutParams);
                }
            });
            return animator;
        }
    }
}
