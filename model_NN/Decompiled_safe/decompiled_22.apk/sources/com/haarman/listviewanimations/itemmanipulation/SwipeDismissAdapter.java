package com.haarman.listviewanimations.itemmanipulation;

import android.widget.AbsListView;
import android.widget.BaseAdapter;
import com.haarman.listviewanimations.BaseAdapterDecorator;

public class SwipeDismissAdapter extends BaseAdapterDecorator {
    private OnDismissCallback mCallback;

    public SwipeDismissAdapter(BaseAdapter baseAdapter, OnDismissCallback callback) {
        super(baseAdapter);
        this.mCallback = callback;
    }

    public void setAbsListView(AbsListView listView) {
        super.setAbsListView(listView);
        listView.setOnTouchListener(new SwipeDismissListViewTouchListener(listView, this.mCallback));
    }
}
