package com.haarman.listviewanimations;

import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class ArrayAdapter<T> extends BaseAdapter {
    private ArrayList<T> mItems;

    public ArrayAdapter() {
        this(null);
    }

    public ArrayAdapter(List<T> items) {
        this.mItems = new ArrayList<>();
        if (items != null) {
            this.mItems.addAll(items);
        }
    }

    public int getCount() {
        return this.mItems.size();
    }

    public T getItem(int position) {
        return this.mItems.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public void add(T item) {
        this.mItems.add(item);
        notifyDataSetChanged();
    }

    public void add(int position, T item) {
        this.mItems.add(position, item);
        notifyDataSetChanged();
    }

    public void addAll(Collection<? extends T> items) {
        this.mItems.addAll(items);
        notifyDataSetChanged();
    }

    public void addAll(T... items) {
        Collections.addAll(this.mItems, items);
        notifyDataSetChanged();
    }

    public void addAll(int position, Collection<? extends T> items) {
        this.mItems.addAll(position, items);
        notifyDataSetChanged();
    }

    public void addAll(int position, T... items) {
        for (int i = position; i < items.length + position; i++) {
            this.mItems.add(i, items[i]);
        }
        notifyDataSetChanged();
    }

    public void clear() {
        this.mItems.clear();
        notifyDataSetChanged();
    }

    public void set(int position, T item) {
        this.mItems.set(position, item);
        notifyDataSetChanged();
    }

    public void remove(T item) {
        this.mItems.remove(item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        this.mItems.remove(position);
        notifyDataSetChanged();
    }

    public void removePositions(Collection<Integer> positions) {
        ArrayList<Integer> positionsList = new ArrayList<>(positions);
        Collections.sort(positionsList);
        Collections.reverse(positionsList);
        Iterator it = positionsList.iterator();
        while (it.hasNext()) {
            this.mItems.remove(((Integer) it.next()).intValue());
        }
        notifyDataSetChanged();
    }

    public void removeAll(Collection<T> items) {
        this.mItems.removeAll(items);
        notifyDataSetChanged();
    }

    public void retainAll(Collection<T> items) {
        this.mItems.retainAll(items);
        notifyDataSetChanged();
    }

    public int indexOf(T item) {
        return this.mItems.indexOf(item);
    }
}
