package com.utils;

import com.example.palmtrends_utils.R;
import com.palmtrends.app.ShareApplication;

public class FinalVariable {
    public static final int addfoot = 1006;
    public static final int change = 1003;
    public static final int deletefoot = 1005;
    public static final int error = 1004;
    public static final int first_load = 1008;
    public static final int first_update = 1011;
    public static final int home_length = 8;
    public static final int length = 15;
    public static final int load_image = 1009;
    public static final int nomore = 1007;
    public static final int other = 1010;
    public static String pid = null;
    public static final int remove_footer = 1002;
    public static int timer = 20;
    public static final int update = 1001;
    public static final int vb_bind = 10001;
    public static final int vb_conten_null = 10003;
    public static final int vb_error = 10002;
    public static final int vb_get_userinfor = 10022;
    public static final int vb_shortid = 10014;
    public static final int vb_success = 10000;
    public static final int vb_text_long = 10021;

    static {
        pid = "";
        pid = ShareApplication.share.getString(R.string.pid);
        PerfHelper.setInfo(PerfHelper.P_APP_ID, pid);
    }
}
