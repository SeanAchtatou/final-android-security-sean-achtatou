package com.utils;

import android.util.Log;
import com.google.zxing.common.StringUtils;
import com.palmtrends.app.ShareApplication;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class ZipUtils {
    private static final int BUFF_SIZE = 1048576;

    public static List<File> GetFileList(String zipFileString, String des) throws Exception {
        Log.v("XZip", "GetFileList(String)");
        List<File> fileList = new ArrayList<>();
        ZipInputStream inZip = new ZipInputStream(new FileInputStream(zipFileString));
        while (true) {
            ZipEntry zipEntry = inZip.getNextEntry();
            if (zipEntry == null) {
                inZip.close();
                return fileList;
            }
            String szName = zipEntry.getName();
            if (zipEntry.isDirectory()) {
                File folder = new File(String.valueOf(des) + CookieSpec.PATH_DELIM + szName);
                if (!folder.exists()) {
                    folder.mkdirs();
                }
            }
        }
    }

    public static void upZipFile(File zipFile, String folderPath) throws ZipException, Exception {
        File desDir = new File(folderPath);
        if (!desDir.exists()) {
            desDir.mkdirs();
        }
        GetFileList(zipFile.getAbsolutePath(), folderPath);
        ZipFile zf = new ZipFile(zipFile);
        Enumeration<?> entries = zf.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            InputStream in = zf.getInputStream(entry);
            String szName = entry.getName();
            if (szName.endsWith(".mp4")) {
                szName = "index.mp4";
            }
            File desFile = new File(new String((String.valueOf(folderPath) + File.separator + szName).getBytes("8859_1"), StringUtils.GB2312));
            File fileParentDir = desFile.getParentFile();
            if (!fileParentDir.exists()) {
                fileParentDir.mkdirs();
            }
            desFile.createNewFile();
            OutputStream out = new FileOutputStream(desFile);
            byte[] buffer = new byte[1048576];
            while (true) {
                int realLength = in.read(buffer);
                if (realLength <= 0) {
                    break;
                }
                out.write(buffer, 0, realLength);
            }
            in.close();
            out.close();
        }
    }

    public static InputStream UpZip(String zipFileString, String fileString) throws Exception {
        Log.v("XZip", "UpZip(String, String)");
        ZipFile zipFile = new ZipFile(zipFileString);
        return zipFile.getInputStream(zipFile.getEntry(fileString));
    }

    public static void UnZipFolder(String zipFileString, String outPathString) throws Exception {
        File outputifle = new File(outPathString);
        if (!outputifle.exists()) {
            outputifle.mkdirs();
        }
        GetFileList(zipFileString, outPathString);
        Log.v("XZip", "UnZipFolder(String, String)");
        ZipInputStream inZip = new ZipInputStream(new FileInputStream(zipFileString));
        while (true) {
            ZipEntry zipEntry = inZip.getNextEntry();
            if (zipEntry == null) {
                inZip.close();
                return;
            }
            String szName = zipEntry.getName();
            if (zipEntry.isDirectory()) {
                new File(String.valueOf(outPathString) + File.separator + szName.substring(0, szName.length() - 1)).mkdirs();
            } else {
                if (szName.endsWith("mp4")) {
                    szName = "index.mp4";
                }
                File file = new File(String.valueOf(outPathString) + File.separator + szName);
                file.createNewFile();
                if (ShareApplication.debug) {
                    System.out.println(String.valueOf(file.getAbsolutePath()) + "-----------");
                }
                FileOutputStream out = new FileOutputStream(file);
                byte[] buffer = new byte[1024];
                while (true) {
                    int len = inZip.read(buffer);
                    if (len == -1) {
                        break;
                    }
                    out.write(buffer, 0, len);
                    out.flush();
                }
                out.close();
            }
        }
    }

    public static void ZipFolder(String srcFileString, String zipFileString) throws Exception {
        Log.v("XZip", "ZipFolder(String, String)");
        ZipOutputStream outZip = new ZipOutputStream(new FileOutputStream(zipFileString));
        File file = new File(srcFileString);
        ZipFiles(String.valueOf(file.getParent()) + File.separator, file.getName(), outZip);
        outZip.finish();
        outZip.close();
    }

    private static void ZipFiles(String folderString, String fileString, ZipOutputStream zipOutputSteam) throws Exception {
        Log.v("XZip", "ZipFiles(String, String, ZipOutputStream)");
        if (zipOutputSteam != null) {
            File file = new File(String.valueOf(folderString) + fileString);
            if (file.isFile()) {
                ZipEntry zipEntry = new ZipEntry(fileString);
                FileInputStream inputStream = new FileInputStream(file);
                zipOutputSteam.putNextEntry(zipEntry);
                byte[] buffer = new byte[4096];
                while (true) {
                    int len = inputStream.read(buffer);
                    if (len == -1) {
                        zipOutputSteam.closeEntry();
                        return;
                    }
                    zipOutputSteam.write(buffer, 0, len);
                }
            } else {
                String[] fileList = file.list();
                if (fileList.length <= 0) {
                    zipOutputSteam.putNextEntry(new ZipEntry(String.valueOf(fileString) + File.separator));
                    zipOutputSteam.closeEntry();
                }
                for (int i = 0; i < fileList.length; i++) {
                    ZipFiles(folderString, String.valueOf(fileString) + File.separator + fileList[i], zipOutputSteam);
                }
            }
        }
    }

    public void finalize() throws Throwable {
    }
}
