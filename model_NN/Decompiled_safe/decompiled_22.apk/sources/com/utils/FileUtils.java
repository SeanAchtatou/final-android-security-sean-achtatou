package com.utils;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import com.example.palmtrends_utils.R;
import com.ibm.mqtt.MQeTrace;
import com.ibm.mqtt.MqttUtils;
import com.palmtrends.app.ShareApplication;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class FileUtils {
    public static String savePath = (Environment.getExternalStorageDirectory() + "/download/image/");
    public static String sdPath = (Environment.getExternalStorageDirectory() + "/.nomedia/citylife/" + ShareApplication.share.getResources().getString(R.string.cache_dir) + CookieSpec.PATH_DELIM);

    static {
        createSdDir("");
    }

    public String getSdPath() {
        return sdPath;
    }

    public static String converPathToName(String path) {
        if (path.endsWith("png") || path.endsWith("jpg")) {
            return path.substring(path.lastIndexOf(CookieSpec.PATH_DELIM) + 1, path.length());
        }
        try {
            return URLEncoder.encode(path.replace("*", ""), MqttUtils.STRING_ENCODING);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return path;
        }
    }

    public static String formatFileSize(long length) {
        if (length >= 1073741824) {
            return String.valueOf((String.valueOf(((float) length) / 1.07374182E9f) + "000").substring(0, String.valueOf(((float) length) / 1.07374182E9f).indexOf(".") + 3)) + "GB";
        } else if (length >= MQeTrace.GROUP_API) {
            return String.valueOf((String.valueOf(((float) length) / 1048576.0f) + "000").substring(0, String.valueOf(((float) length) / 1048576.0f).indexOf(".") + 3)) + "MB";
        } else if (length >= MQeTrace.GROUP_CHANNEL_MANAGEMENT) {
            return String.valueOf((String.valueOf(((float) length) / 1024.0f) + "000").substring(0, String.valueOf(((float) length) / 1024.0f).indexOf(".") + 3)) + "KB";
        } else if (length < MQeTrace.GROUP_CHANNEL_MANAGEMENT) {
            return String.valueOf(Long.toString(length)) + "B";
        } else {
            return null;
        }
    }

    public static File createSdFile(String fileName) throws IOException {
        if (!new File(sdPath).exists()) {
            createSdDir("");
        }
        if (!new File(String.valueOf(sdPath) + "image/").exists()) {
            createSdDir("image/");
        }
        File file = new File(String.valueOf(sdPath) + fileName);
        file.createNewFile();
        return file;
    }

    public static File saveSdFile(String fileName) throws IOException {
        File file = new File(savePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(String.valueOf(savePath) + fileName);
        file2.createNewFile();
        return file2;
    }

    public static File createSdDir(String dirName) {
        File file = new File(String.valueOf(sdPath) + dirName);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public static boolean isFileExist(String fileName) {
        return new File(String.valueOf(sdPath) + fileName).exists();
    }

    public static void deleteFiles() {
        File file = new File(sdPath);
        if (file.exists()) {
            File[] files = file.listFiles();
            int count = files.length;
            for (int i = 0; i < count; i++) {
                if (files[i].isDirectory()) {
                    for (File delete : files[i].listFiles()) {
                        delete.delete();
                    }
                } else {
                    files[i].delete();
                }
            }
        }
    }

    public void copy(String path1, String path2) {
        try {
            FileInputStream fis = new FileInputStream(path1);
            byte[] c = new byte[fis.available()];
            fis.read(c);
            fis.close();
            new File(path2.substring(0, path2.lastIndexOf(CookieSpec.PATH_DELIM) + 1)).mkdirs();
            FileOutputStream fos = new FileOutputStream(path2);
            fos.write(c);
            fos.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public static Drawable getImageSd(String fileName) throws IOException {
        File f = new File(String.valueOf(sdPath) + "image/" + fileName);
        if (f.exists()) {
            return Drawable.createFromPath(f.getAbsolutePath());
        }
        return null;
    }

    public static Drawable getImageSdFromSave(String fileName) throws IOException {
        File f = new File(String.valueOf(savePath) + fileName);
        if (f.exists()) {
            return Drawable.createFromPath(f.getAbsolutePath());
        }
        return null;
    }

    public static void writeToFile(final Bitmap bt, final String icon) {
        new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:16:0x0034 A[SYNTHETIC, Splitter:B:16:0x0034] */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x0043 A[SYNTHETIC, Splitter:B:24:0x0043] */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x004f A[SYNTHETIC, Splitter:B:30:0x004f] */
            /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x003e=Splitter:B:21:0x003e, B:13:0x002f=Splitter:B:13:0x002f} */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r7 = this;
                    android.graphics.Bitmap r4 = r1
                    if (r4 == 0) goto L_0x0022
                    r2 = 0
                    java.lang.String r4 = r2     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x003d }
                    java.lang.String r4 = com.utils.FileUtils.converPathToName(r4)     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x003d }
                    java.io.File r1 = com.utils.FileUtils.saveSdFile(r4)     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x003d }
                    java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x003d }
                    r3.<init>(r1)     // Catch:{ FileNotFoundException -> 0x002e, IOException -> 0x003d }
                    android.graphics.Bitmap r4 = r1     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x0060, all -> 0x005d }
                    android.graphics.Bitmap$CompressFormat r5 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x0060, all -> 0x005d }
                    r6 = 100
                    r4.compress(r5, r6, r3)     // Catch:{ FileNotFoundException -> 0x0063, IOException -> 0x0060, all -> 0x005d }
                    if (r3 == 0) goto L_0x0022
                    r3.close()     // Catch:{ IOException -> 0x0058 }
                L_0x0022:
                    java.lang.String r4 = "已保存到/download/image/下"
                    com.palmtrends.loadimage.Utils.showToast(r4)
                    com.palmtrends.loadimage.Utils.dismissProcessDialog()
                    super.run()
                    return
                L_0x002e:
                    r0 = move-exception
                L_0x002f:
                    r0.printStackTrace()     // Catch:{ all -> 0x004c }
                    if (r2 == 0) goto L_0x0022
                    r2.close()     // Catch:{ IOException -> 0x0038 }
                    goto L_0x0022
                L_0x0038:
                    r0 = move-exception
                    r0.printStackTrace()
                    goto L_0x0022
                L_0x003d:
                    r0 = move-exception
                L_0x003e:
                    r0.printStackTrace()     // Catch:{ all -> 0x004c }
                    if (r2 == 0) goto L_0x0022
                    r2.close()     // Catch:{ IOException -> 0x0047 }
                    goto L_0x0022
                L_0x0047:
                    r0 = move-exception
                    r0.printStackTrace()
                    goto L_0x0022
                L_0x004c:
                    r4 = move-exception
                L_0x004d:
                    if (r2 == 0) goto L_0x0052
                    r2.close()     // Catch:{ IOException -> 0x0053 }
                L_0x0052:
                    throw r4
                L_0x0053:
                    r0 = move-exception
                    r0.printStackTrace()
                    goto L_0x0052
                L_0x0058:
                    r0 = move-exception
                    r0.printStackTrace()
                    goto L_0x0022
                L_0x005d:
                    r4 = move-exception
                    r2 = r3
                    goto L_0x004d
                L_0x0060:
                    r0 = move-exception
                    r2 = r3
                    goto L_0x003e
                L_0x0063:
                    r0 = move-exception
                    r2 = r3
                    goto L_0x002f
                */
                throw new UnsupportedOperationException("Method not decompiled: com.utils.FileUtils.AnonymousClass1.run():void");
            }
        }.start();
    }

    public static void writeCacheToFile(InputStream is, String imagename) throws Exception {
        FileOutputStream output = new FileOutputStream(createSdFile("image/" + imagename));
        byte[] bs = new byte[2048];
        while (true) {
            int count = is.read(bs);
            if (count == -1) {
                break;
            }
            output.write(bs, 0, count);
        }
        if (output != null) {
            try {
                output.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Map readCacheToMap() throws Exception {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(String.valueOf(sdPath) + "t.tmp"));
        int readInt = ois.readInt();
        ois.close();
        return null;
    }
}
