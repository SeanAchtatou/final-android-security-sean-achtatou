package com.utils;

public class JniUtils {
    static native String stringFromJNI(String str);

    public static String getkey() {
        String key = stringFromJNI(PerfHelper.getStringData(PerfHelper.P_USERID));
        PerfHelper.setInfo("md5_key", key);
        return key;
    }

    static {
        System.loadLibrary("md5");
    }
}
