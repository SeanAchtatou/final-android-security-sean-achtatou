package com.utils;

import android.content.Intent;
import android.location.Location;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.palmtrends.app.ShareApplication;
import com.tencent.mm.sdk.platformtools.PhoneUtil;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;

public class GpsUtils {
    public static final boolean DEBUG = false;
    private static final double EARTH_RADIUS = 6378137.0d;
    public static String GPS_FINSH = "com.city_life.gps.finish";
    public static final String TAG = "GpsUtils";

    public static void getLocation() {
        baidulocation();
    }

    public static void baidulocation() {
        if (ShareApplication.debug) {
            System.out.println("开始定位");
        }
        final LocationClient mLocationClient = new LocationClient(ShareApplication.share);
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true);
        option.setCoorType("bd09ll");
        option.setPriority(2);
        option.setPoiExtraInfo(true);
        option.setProdName("gps");
        option.setScanSpan(5000);
        mLocationClient.setLocOption(option);
        mLocationClient.registerLocationListener(new BDLocationListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
             arg types: [java.lang.String, int]
             candidates:
              com.utils.PerfHelper.setInfo(java.lang.String, int):void
              com.utils.PerfHelper.setInfo(java.lang.String, long):void
              com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
              com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
            public void onReceiveLocation(BDLocation location) {
                Intent intent = new Intent(GpsUtils.GPS_FINSH);
                if (location == null) {
                    ShareApplication.share.sendBroadcast(intent);
                    return;
                }
                if (location.getLocType() < 162 || location.getLocType() > 167) {
                    PerfHelper.setInfo(PerfHelper.P_GPS, String.valueOf(location.getLatitude()) + "," + location.getLongitude());
                    PerfHelper.setInfo(PerfHelper.P_GPS_LATI, new StringBuilder(String.valueOf(location.getLatitude())).toString());
                    PerfHelper.setInfo(PerfHelper.P_GPS_LONG, new StringBuilder(String.valueOf(location.getLongitude())).toString());
                    PerfHelper.setInfo(PerfHelper.P_GPS_YES, true);
                } else {
                    PerfHelper.setInfo(PerfHelper.P_GPS, "29.567342,106.572127");
                    PerfHelper.setInfo(PerfHelper.P_GPS_LATI, "29.567342");
                    PerfHelper.setInfo(PerfHelper.P_GPS_LONG, "106.572127");
                    PerfHelper.setInfo(PerfHelper.P_GPS_YES, false);
                }
                if (ShareApplication.debug) {
                    System.out.println("定位结束:" + location.getLatitude() + "," + location.getLongitude());
                }
                ShareApplication.share.sendBroadcast(intent);
                if (location.getLocType() == 161) {
                    PerfHelper.setInfo(PerfHelper.P_NOW_CITY, location.getCity());
                }
                LocationClient.this.stop();
            }

            public void onReceivePoi(BDLocation location) {
                if (location.getLocType() == 161) {
                    location.getCity();
                }
            }
        });
        mLocationClient.start();
    }

    public void LocationOver() {
    }

    public static Location requestNetworkLocation() {
        int cid;
        int lac;
        String mcc;
        String mnc;
        JSONObject object;
        try {
            TelephonyManager telMan = (TelephonyManager) ShareApplication.share.getSystemService("phone");
            String operator = telMan.getNetworkOperator();
            boolean isCdma = false;
            CellLocation cl = telMan.getCellLocation();
            if (cl instanceof GsmCellLocation) {
                cid = ((GsmCellLocation) cl).getCid();
                lac = ((GsmCellLocation) cl).getLac();
                mcc = operator.substring(0, 3);
                mnc = operator.substring(3);
            } else if (!(cl instanceof CdmaCellLocation)) {
                return null;
            } else {
                cid = ((CdmaCellLocation) cl).getBaseStationId();
                lac = ((CdmaCellLocation) cl).getNetworkId();
                mcc = operator.substring(0, 3);
                mnc = String.valueOf(((CdmaCellLocation) cl).getSystemId());
                isCdma = true;
            }
            JSONObject tower = new JSONObject();
            try {
                tower.put("cell_id", cid);
                tower.put("location_area_code", lac);
                tower.put("mobile_country_code", mcc);
                tower.put("mobile_network_code", mnc);
            } catch (JSONException e) {
            }
            JSONArray jarray = new JSONArray();
            jarray.put(tower);
            for (NeighboringCellInfo nci : telMan.getNeighboringCellInfo()) {
                JSONObject tmpTower = new JSONObject();
                try {
                    tmpTower.put("cell_id", nci.getCid());
                    tmpTower.put("location_area_code", nci.getLac());
                    tmpTower.put("mobile_country_code", mcc);
                    tmpTower.put("mobile_network_code", mnc);
                } catch (JSONException e2) {
                }
                jarray.put(tmpTower);
            }
            if (!isCdma) {
                object = createJSONObject("cell_towers", jarray);
            } else {
                object = createCDMAJSONObject("cell_towers", jarray, mcc, mnc);
            }
            String locinfo = null;
            int tryCount = 0;
            while (tryCount < 3 && locinfo == null) {
                tryCount++;
                try {
                    DefaultHttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost("http://www.google.com/loc/json");
                    httpPost.setEntity(new StringEntity(object.toString()));
                    locinfo = EntityUtils.toString(httpClient.execute(httpPost).getEntity());
                } catch (Exception e3) {
                    e3.printStackTrace();
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e4) {
                    }
                }
            }
            if (locinfo == null) {
                return null;
            }
            Location location = new Location("telephone");
            JSONObject locObj = new JSONObject(locinfo).getJSONObject("location");
            if (locObj.has(Constants.TX_API_LATITUDE) && locObj.has(Constants.TX_API_LONGITUDE)) {
                location.setLatitude(locObj.getDouble(Constants.TX_API_LATITUDE));
                location.setLongitude(locObj.getDouble(Constants.TX_API_LONGITUDE));
                location.setAccuracy(locObj.has("accuracy") ? (float) locObj.getDouble("accuracy") : 0.0f);
                location.setTime(System.currentTimeMillis());
                return location;
            }
            return null;
        } catch (Exception e5) {
        }
    }

    private static JSONObject createJSONObject(String arrayName, JSONArray array) {
        JSONObject object = new JSONObject();
        try {
            object.put("version", "1.1.0");
            object.put("host", "maps.google.com");
            object.put(arrayName, array);
        } catch (JSONException e) {
        }
        return object;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private static JSONObject createCDMAJSONObject(String arrayName, JSONArray array, String mcc, String mnc) {
        JSONObject object = new JSONObject();
        try {
            object.put("version", "1.1.0");
            object.put("host", "maps.google.com");
            object.put("home_mobile_country_code", mcc);
            object.put("home_mobile_network_code", mnc);
            object.put("radio_type", PhoneUtil.CELL_CDMA);
            object.put("request_address", true);
            if ("460".equals(mcc)) {
                object.put("address_language", "zh_CN");
            } else {
                object.put("address_language", "en_US");
            }
            object.put(arrayName, array);
        } catch (JSONException e) {
        }
        return object;
    }

    public static synchronized double distanceByLngLat(double lng1, double lat1, double lng2, double lat2) {
        double s;
        synchronized (GpsUtils.class) {
            double radLat1 = (3.141592653589793d * lat1) / 180.0d;
            double radLat2 = (3.141592653589793d * lat2) / 180.0d;
            s = (double) (Math.round(10000.0d * ((2.0d * Math.asin(Math.sqrt(Math.pow(Math.sin((radLat1 - radLat2) / 2.0d), 2.0d) + ((Math.cos(radLat1) * Math.cos(radLat2)) * Math.pow(Math.sin((((3.141592653589793d * lng1) / 180.0d) - ((3.141592653589793d * lng2) / 180.0d)) / 2.0d), 2.0d))))) * EARTH_RADIUS)) / 10000);
        }
        return s;
    }

    public static synchronized double getDistance(double longitude1, double latitude1, double longitude2, double latitude2) {
        double s;
        synchronized (GpsUtils.class) {
            double Lat1 = rad(latitude1);
            double Lat2 = rad(latitude2);
            s = (double) (Math.round(10000.0d * ((2.0d * Math.asin(Math.sqrt(Math.pow(Math.sin((Lat1 - Lat2) / 2.0d), 2.0d) + ((Math.cos(Lat1) * Math.cos(Lat2)) * Math.pow(Math.sin((rad(longitude1) - rad(longitude2)) / 2.0d), 2.0d))))) * EARTH_RADIUS)) / 10000);
        }
        return s;
    }

    private static double rad(double d) {
        return (3.141592653589793d * d) / 180.0d;
    }
}
