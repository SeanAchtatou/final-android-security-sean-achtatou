package com.utils;

import com.ibm.mqtt.MqttUtils;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.httpclient.methods.MultipartPostMethod;

public class HttpUtil {
    public static InputStream post(String actionUrl, Map<String, String> params, FormFile[] files) throws Exception {
        try {
            String BOUNDARY = UUID.randomUUID().toString();
            HttpURLConnection conn = (HttpURLConnection) new URL(actionUrl).openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Charset", MqttUtils.STRING_ENCODING);
            conn.setRequestProperty("Content-Type", String.valueOf(MultipartPostMethod.MULTIPART_FORM_CONTENT_TYPE) + "; boundary=" + BOUNDARY);
            StringBuilder sb = new StringBuilder();
            DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
            if (params != null) {
                try {
                    for (Map.Entry<String, String> entry : params.entrySet()) {
                        sb.append("--");
                        sb.append(BOUNDARY);
                        sb.append("\r\n");
                        sb.append("Content-Disposition: form-data; name=\"" + ((String) entry.getKey()) + "\"\r\n\r\n");
                        sb.append((String) entry.getValue());
                        sb.append("\r\n");
                    }
                    outStream.write(sb.toString().getBytes());
                } catch (Exception e) {
                    e = e;
                    e.printStackTrace();
                    throw new Exception("网络异常：请确认是否已打开网络连接");
                }
            }
            if (files != null) {
                int length = files.length;
                for (int i = 0; i < length; i++) {
                    FormFile file = files[i];
                    StringBuilder split = new StringBuilder();
                    split.append("--");
                    split.append(BOUNDARY);
                    split.append("\r\n");
                    split.append("Content-Disposition: form-data;name=\"" + file.getFormname() + "\";filename=\"" + file.getFilname() + "\"\r\n");
                    split.append("Content-Type: " + file.getContentType() + "\r\n\r\n");
                    outStream.write(split.toString().getBytes());
                    outStream.write(file.getData(), 0, file.getData().length);
                    outStream.write("\r\n".getBytes());
                }
                outStream.write(("--" + BOUNDARY + "--\r\n").getBytes());
            }
            outStream.flush();
            if (conn.getResponseCode() == 200) {
                return conn.getInputStream();
            }
            throw new RuntimeException("请求url失败");
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            throw new Exception("网络异常：请确认是否已打开网络连接");
        }
    }
}
