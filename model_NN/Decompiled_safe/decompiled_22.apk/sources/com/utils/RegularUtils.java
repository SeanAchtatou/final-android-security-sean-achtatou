package com.utils;

import java.util.regex.Pattern;

public class RegularUtils {
    public static boolean getEmail(String line) {
        return Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*").matcher(line).find();
    }
}
