package com.example.palmtrends_utils;

public final class R {

    public static final class bool {
        public static final int offline = 2131361797;
    }

    public static final class dimen {
        public static final int image_detail_pager_margin = 2131492880;
        public static final int image_home_size = 2131492877;
        public static final int image_thumbnail_size = 2131492878;
        public static final int image_thumbnail_spacing = 2131492879;
    }

    public static final class drawable {
        public static final int ic_action_search = 2130837701;
    }

    public static final class layout {
        public static final int activity_main = 2130903072;
    }

    public static final class string {
        public static final int app_name = 2131623938;
        public static final int bind_tuijian = 2131623962;
        public static final int cache_dir = 2131623937;
        public static final int cancel = 2131623956;
        public static final int checkupdate_getnew = 2131623947;
        public static final int checkupdate_hasnew = 2131623946;
        public static final int comment_list_no = 2131623963;
        public static final int done = 2131623955;
        public static final int exit = 2131623948;
        public static final int exit_message = 2131623949;
        public static final int loading = 2131623944;
        public static final int loading_n = 2131623945;
        public static final int network_error = 2131623950;
        public static final int no_data = 2131623952;
        public static final int no_data_comments = 2131623954;
        public static final int no_data_fav = 2131623953;
        public static final int not_null_tip = 2131623960;
        public static final int pid = 2131623936;
        public static final int pull_to_refresh_pull_label = 2131623939;
        public static final int pull_to_refresh_refreshing_label = 2131623941;
        public static final int pull_to_refresh_release_label = 2131623940;
        public static final int pull_to_refresh_tap_label = 2131623942;
        public static final int pull_to_refresh_update = 2131623943;
        public static final int server_error = 2131623951;
        public static final int share_bind_tip = 2131623958;
        public static final int share_success_tip = 2131623959;
        public static final int share_text_tip = 2131623957;
        public static final int too_long_tip = 2131623961;
    }

    public static final class style {
        public static final int AppTheme = 2131558405;
    }
}
