package com.android.volley.toolbox;

import android.os.SystemClock;
import com.android.volley.Cache;
import com.android.volley.VolleyLog;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class DiskBasedCache implements Cache {
    private static final int CACHE_VERSION = 2;
    private static final int DEFAULT_DISK_USAGE_BYTES = 5242880;
    private static final float HYSTERESIS_FACTOR = 0.9f;
    private final Map<String, CacheHeader> mEntries;
    private final int mMaxCacheSizeInBytes;
    private final File mRootDirectory;
    private long mTotalSize;

    public DiskBasedCache(File rootDirectory, int maxCacheSizeInBytes) {
        this.mEntries = new LinkedHashMap(16, 0.75f, true);
        this.mTotalSize = 0;
        this.mRootDirectory = rootDirectory;
        this.mMaxCacheSizeInBytes = maxCacheSizeInBytes;
    }

    public DiskBasedCache(File rootDirectory) {
        this(rootDirectory, DEFAULT_DISK_USAGE_BYTES);
    }

    public synchronized void clear() {
        synchronized (this) {
            File[] files = this.mRootDirectory.listFiles();
            if (files != null) {
                for (File file : files) {
                    file.delete();
                }
            }
            this.mEntries.clear();
            this.mTotalSize = 0;
            VolleyLog.d("Cache cleared.", new Object[0]);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0062 A[SYNTHETIC, Splitter:B:29:0x0062] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.android.volley.Cache.Entry get(java.lang.String r13) {
        /*
            r12 = this;
            r7 = 0
            monitor-enter(r12)
            java.util.Map<java.lang.String, com.android.volley.toolbox.DiskBasedCache$CacheHeader> r8 = r12.mEntries     // Catch:{ all -> 0x0066 }
            java.lang.Object r4 = r8.get(r13)     // Catch:{ all -> 0x0066 }
            com.android.volley.toolbox.DiskBasedCache$CacheHeader r4 = (com.android.volley.toolbox.DiskBasedCache.CacheHeader) r4     // Catch:{ all -> 0x0066 }
            if (r4 != 0) goto L_0x000e
        L_0x000c:
            monitor-exit(r12)
            return r7
        L_0x000e:
            java.io.File r5 = r12.getFileForKey(r13)     // Catch:{ all -> 0x0066 }
            r0 = 0
            com.android.volley.toolbox.DiskBasedCache$CountingInputStream r1 = new com.android.volley.toolbox.DiskBasedCache$CountingInputStream     // Catch:{ IOException -> 0x003d }
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ IOException -> 0x003d }
            r8.<init>(r5)     // Catch:{ IOException -> 0x003d }
            r9 = 0
            r1.<init>(r8, r9)     // Catch:{ IOException -> 0x003d }
            com.android.volley.toolbox.DiskBasedCache.CacheHeader.readHeader(r1)     // Catch:{ IOException -> 0x006e, all -> 0x006b }
            long r8 = r5.length()     // Catch:{ IOException -> 0x006e, all -> 0x006b }
            int r10 = r1.bytesRead     // Catch:{ IOException -> 0x006e, all -> 0x006b }
            long r10 = (long) r10     // Catch:{ IOException -> 0x006e, all -> 0x006b }
            long r8 = r8 - r10
            int r8 = (int) r8     // Catch:{ IOException -> 0x006e, all -> 0x006b }
            byte[] r2 = streamToBytes(r1, r8)     // Catch:{ IOException -> 0x006e, all -> 0x006b }
            com.android.volley.Cache$Entry r8 = r4.toCacheEntry(r2)     // Catch:{ IOException -> 0x006e, all -> 0x006b }
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ IOException -> 0x003b }
        L_0x0039:
            r7 = r8
            goto L_0x000c
        L_0x003b:
            r6 = move-exception
            goto L_0x000c
        L_0x003d:
            r3 = move-exception
        L_0x003e:
            java.lang.String r8 = "%s: %s"
            r9 = 2
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ all -> 0x005f }
            r10 = 0
            java.lang.String r11 = r5.getAbsolutePath()     // Catch:{ all -> 0x005f }
            r9[r10] = r11     // Catch:{ all -> 0x005f }
            r10 = 1
            java.lang.String r11 = r3.toString()     // Catch:{ all -> 0x005f }
            r9[r10] = r11     // Catch:{ all -> 0x005f }
            com.android.volley.VolleyLog.d(r8, r9)     // Catch:{ all -> 0x005f }
            r12.remove(r13)     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x000c
            r0.close()     // Catch:{ IOException -> 0x005d }
            goto L_0x000c
        L_0x005d:
            r6 = move-exception
            goto L_0x000c
        L_0x005f:
            r8 = move-exception
        L_0x0060:
            if (r0 == 0) goto L_0x0065
            r0.close()     // Catch:{ IOException -> 0x0069 }
        L_0x0065:
            throw r8     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r7 = move-exception
            monitor-exit(r12)
            throw r7
        L_0x0069:
            r6 = move-exception
            goto L_0x000c
        L_0x006b:
            r8 = move-exception
            r0 = r1
            goto L_0x0060
        L_0x006e:
            r3 = move-exception
            r0 = r1
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.volley.toolbox.DiskBasedCache.get(java.lang.String):com.android.volley.Cache$Entry");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0053 A[SYNTHETIC, Splitter:B:28:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0058 A[SYNTHETIC, Splitter:B:31:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0061 A[SYNTHETIC, Splitter:B:36:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x004d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void initialize() {
        /*
            r10 = this;
            r6 = 0
            monitor-enter(r10)
            java.io.File r7 = r10.mRootDirectory     // Catch:{ all -> 0x0065 }
            boolean r7 = r7.exists()     // Catch:{ all -> 0x0065 }
            if (r7 != 0) goto L_0x0025
            java.io.File r6 = r10.mRootDirectory     // Catch:{ all -> 0x0065 }
            boolean r6 = r6.mkdirs()     // Catch:{ all -> 0x0065 }
            if (r6 != 0) goto L_0x0023
            java.lang.String r6 = "Unable to create cache dir %s"
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0065 }
            r8 = 0
            java.io.File r9 = r10.mRootDirectory     // Catch:{ all -> 0x0065 }
            java.lang.String r9 = r9.getAbsolutePath()     // Catch:{ all -> 0x0065 }
            r7[r8] = r9     // Catch:{ all -> 0x0065 }
            com.android.volley.VolleyLog.e(r6, r7)     // Catch:{ all -> 0x0065 }
        L_0x0023:
            monitor-exit(r10)
            return
        L_0x0025:
            java.io.File r7 = r10.mRootDirectory     // Catch:{ all -> 0x0065 }
            java.io.File[] r3 = r7.listFiles()     // Catch:{ all -> 0x0065 }
            if (r3 == 0) goto L_0x0023
            int r7 = r3.length     // Catch:{ all -> 0x0065 }
        L_0x002e:
            if (r6 >= r7) goto L_0x0023
            r2 = r3[r6]     // Catch:{ all -> 0x0065 }
            r4 = 0
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0050 }
            r5.<init>(r2)     // Catch:{ IOException -> 0x0050 }
            com.android.volley.toolbox.DiskBasedCache$CacheHeader r1 = com.android.volley.toolbox.DiskBasedCache.CacheHeader.readHeader(r5)     // Catch:{ IOException -> 0x0070, all -> 0x006d }
            long r8 = r2.length()     // Catch:{ IOException -> 0x0070, all -> 0x006d }
            r1.size = r8     // Catch:{ IOException -> 0x0070, all -> 0x006d }
            java.lang.String r8 = r1.key     // Catch:{ IOException -> 0x0070, all -> 0x006d }
            r10.putEntry(r8, r1)     // Catch:{ IOException -> 0x0070, all -> 0x006d }
            if (r5 == 0) goto L_0x0073
            r5.close()     // Catch:{ IOException -> 0x0068 }
            r4 = r5
        L_0x004d:
            int r6 = r6 + 1
            goto L_0x002e
        L_0x0050:
            r0 = move-exception
        L_0x0051:
            if (r2 == 0) goto L_0x0056
            r2.delete()     // Catch:{ all -> 0x005e }
        L_0x0056:
            if (r4 == 0) goto L_0x004d
            r4.close()     // Catch:{ IOException -> 0x005c }
            goto L_0x004d
        L_0x005c:
            r8 = move-exception
            goto L_0x004d
        L_0x005e:
            r6 = move-exception
        L_0x005f:
            if (r4 == 0) goto L_0x0064
            r4.close()     // Catch:{ IOException -> 0x006b }
        L_0x0064:
            throw r6     // Catch:{ all -> 0x0065 }
        L_0x0065:
            r6 = move-exception
            monitor-exit(r10)
            throw r6
        L_0x0068:
            r8 = move-exception
            r4 = r5
            goto L_0x004d
        L_0x006b:
            r7 = move-exception
            goto L_0x0064
        L_0x006d:
            r6 = move-exception
            r4 = r5
            goto L_0x005f
        L_0x0070:
            r0 = move-exception
            r4 = r5
            goto L_0x0051
        L_0x0073:
            r4 = r5
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.volley.toolbox.DiskBasedCache.initialize():void");
    }

    public synchronized void invalidate(String key, boolean fullExpire) {
        Cache.Entry entry = get(key);
        if (entry != null) {
            entry.softTtl = 0;
            if (fullExpire) {
                entry.ttl = 0;
            }
            put(key, entry);
        }
    }

    public synchronized void put(String key, Cache.Entry entry) {
        pruneIfNeeded(entry.data.length);
        File file = getFileForKey(key);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            CacheHeader e = new CacheHeader(key, entry);
            e.writeHeader(fos);
            fos.write(entry.data);
            fos.close();
            putEntry(key, e);
        } catch (IOException e2) {
            if (!file.delete()) {
                VolleyLog.d("Could not clean up file %s", file.getAbsolutePath());
            }
        }
    }

    public synchronized void remove(String key) {
        boolean deleted = getFileForKey(key).delete();
        removeEntry(key);
        if (!deleted) {
            VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", key, getFilenameForKey(key));
        }
    }

    private String getFilenameForKey(String key) {
        int firstHalfLength = key.length() / 2;
        return String.valueOf(String.valueOf(key.substring(0, firstHalfLength).hashCode())) + String.valueOf(key.substring(firstHalfLength).hashCode());
    }

    public File getFileForKey(String key) {
        return new File(this.mRootDirectory, getFilenameForKey(key));
    }

    private void pruneIfNeeded(int neededSpace) {
        if (this.mTotalSize + ((long) neededSpace) >= ((long) this.mMaxCacheSizeInBytes)) {
            if (VolleyLog.DEBUG) {
                VolleyLog.v("Pruning old cache entries.", new Object[0]);
            }
            long before = this.mTotalSize;
            int prunedFiles = 0;
            long startTime = SystemClock.elapsedRealtime();
            Iterator<Map.Entry<String, CacheHeader>> iterator = this.mEntries.entrySet().iterator();
            while (iterator.hasNext()) {
                CacheHeader e = (CacheHeader) iterator.next().getValue();
                if (getFileForKey(e.key).delete()) {
                    this.mTotalSize -= e.size;
                } else {
                    VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", e.key, getFilenameForKey(e.key));
                }
                iterator.remove();
                prunedFiles++;
                if (((float) (this.mTotalSize + ((long) neededSpace))) < ((float) this.mMaxCacheSizeInBytes) * HYSTERESIS_FACTOR) {
                    break;
                }
            }
            if (VolleyLog.DEBUG) {
                VolleyLog.v("pruned %d files, %d bytes, %d ms", Integer.valueOf(prunedFiles), Long.valueOf(this.mTotalSize - before), Long.valueOf(SystemClock.elapsedRealtime() - startTime));
            }
        }
    }

    private void putEntry(String key, CacheHeader entry) {
        if (!this.mEntries.containsKey(key)) {
            this.mTotalSize += entry.size;
        } else {
            this.mTotalSize += entry.size - this.mEntries.get(key).size;
        }
        this.mEntries.put(key, entry);
    }

    private void removeEntry(String key) {
        CacheHeader entry = this.mEntries.get(key);
        if (entry != null) {
            this.mTotalSize -= entry.size;
            this.mEntries.remove(key);
        }
    }

    private static byte[] streamToBytes(InputStream in, int length) throws IOException {
        byte[] bytes = new byte[length];
        int pos = 0;
        while (pos < length) {
            int count = in.read(bytes, pos, length - pos);
            if (count == -1) {
                break;
            }
            pos += count;
        }
        if (pos == length) {
            return bytes;
        }
        throw new IOException("Expected " + length + " bytes, read " + pos + " bytes");
    }

    private static class CacheHeader {
        public String etag;
        public String key;
        public Map<String, String> responseHeaders;
        public long serverDate;
        public long size;
        public long softTtl;
        public long ttl;

        private CacheHeader() {
        }

        public CacheHeader(String key2, Cache.Entry entry) {
            this.key = key2;
            this.size = (long) entry.data.length;
            this.etag = entry.etag;
            this.serverDate = entry.serverDate;
            this.ttl = entry.ttl;
            this.softTtl = entry.softTtl;
            this.responseHeaders = entry.responseHeaders;
        }

        public static CacheHeader readHeader(InputStream is) throws IOException {
            CacheHeader entry = new CacheHeader();
            ObjectInputStream ois = new ObjectInputStream(is);
            if (ois.readByte() != 2) {
                throw new IOException();
            }
            entry.key = ois.readUTF();
            entry.etag = ois.readUTF();
            if (entry.etag.equals("")) {
                entry.etag = null;
            }
            entry.serverDate = ois.readLong();
            entry.ttl = ois.readLong();
            entry.softTtl = ois.readLong();
            entry.responseHeaders = readStringStringMap(ois);
            return entry;
        }

        public Cache.Entry toCacheEntry(byte[] data) {
            Cache.Entry e = new Cache.Entry();
            e.data = data;
            e.etag = this.etag;
            e.serverDate = this.serverDate;
            e.ttl = this.ttl;
            e.softTtl = this.softTtl;
            e.responseHeaders = this.responseHeaders;
            return e;
        }

        public boolean writeHeader(OutputStream os) {
            try {
                ObjectOutputStream oos = new ObjectOutputStream(os);
                oos.writeByte(2);
                oos.writeUTF(this.key);
                oos.writeUTF(this.etag == null ? "" : this.etag);
                oos.writeLong(this.serverDate);
                oos.writeLong(this.ttl);
                oos.writeLong(this.softTtl);
                writeStringStringMap(this.responseHeaders, oos);
                oos.flush();
                return true;
            } catch (IOException e) {
                VolleyLog.d("%s", e.toString());
                return false;
            }
        }

        private static void writeStringStringMap(Map<String, String> map, ObjectOutputStream oos) throws IOException {
            if (map != null) {
                oos.writeInt(map.size());
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    oos.writeUTF((String) entry.getKey());
                    oos.writeUTF((String) entry.getValue());
                }
                return;
            }
            oos.writeInt(0);
        }

        private static Map<String, String> readStringStringMap(ObjectInputStream ois) throws IOException {
            Map<String, String> result;
            int size2 = ois.readInt();
            if (size2 == 0) {
                result = Collections.emptyMap();
            } else {
                result = new HashMap<>(size2);
            }
            for (int i = 0; i < size2; i++) {
                result.put(ois.readUTF().intern(), ois.readUTF().intern());
            }
            return result;
        }
    }

    private static class CountingInputStream extends FilterInputStream {
        /* access modifiers changed from: private */
        public int bytesRead;

        /* synthetic */ CountingInputStream(InputStream inputStream, CountingInputStream countingInputStream) {
            this(inputStream);
        }

        private CountingInputStream(InputStream in) {
            super(in);
            this.bytesRead = 0;
        }

        public int read() throws IOException {
            int result = super.read();
            if (result != -1) {
                this.bytesRead++;
            }
            return result;
        }

        public int read(byte[] buffer, int offset, int count) throws IOException {
            int result = super.read(buffer, offset, count);
            if (result != -1) {
                this.bytesRead += result;
            }
            return result;
        }
    }
}
