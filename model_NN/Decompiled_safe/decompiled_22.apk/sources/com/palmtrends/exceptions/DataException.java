package com.palmtrends.exceptions;

import org.json.JSONException;

public class DataException extends JSONException {
    public static final String code1 = "100011";

    public DataException(String s) {
        super(s);
    }
}
