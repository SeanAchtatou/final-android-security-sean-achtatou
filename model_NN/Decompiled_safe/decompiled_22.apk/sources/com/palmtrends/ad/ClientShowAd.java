package com.palmtrends.ad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.controll.R;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.palmtrends.loadimage.ImageFetcher;
import com.palmtrends.loadimage.Utils;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.FinalVariable;
import com.utils.JniUtils;
import com.utils.PerfHelper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;

public class ClientShowAd {
    public static final String ADNAME = "adname";
    public static final String INIT_AD_TIME_SETTING = "init_ad_time_set";
    public static final String ISPOP = "ispoped";
    public static String ad_main = "http://www.baidu.com/adms/";
    public static final String adv = "1.0.0";
    public static String adview_url;
    public static TranslateAnimation botton_ta_in = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
    public static final TranslateAnimation botton_ta_out = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
    public static final TranslateAnimation left_ta = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
    public static final TranslateAnimation left_ta_out = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
    public static final TranslateAnimation right_ta = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
    public static final TranslateAnimation right_ta_out = new TranslateAnimation(1, 0.0f, 1, 1.0f, 1, 0.0f, 1, 0.0f);
    static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public static final TranslateAnimation top_ta_in = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
    public static final TranslateAnimation top_ta_out = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
    public final int FIXED = 2;
    public final int POP_UP = 1;
    ADItem ad = null;
    private final int botton_out = 2;
    boolean canclose = false;
    public View current;
    AdAdapter current_adapter = null;
    View dombo;
    /* access modifiers changed from: private */
    public adtypeinfo fixed_info;
    public boolean fixed_isclick = false;
    public Handler h;
    String info = "";
    String info_fiexd_out;
    private final int left_out = 1;
    String list_json_title;
    private final int right_out = 3;
    private final int top_out = 4;
    LinearLayout viewcontent;
    View wv_fixed;

    public ClientShowAd() {
        left_ta.setDuration(1000);
        right_ta.setDuration(1000);
        right_ta_out.setDuration(1000);
        left_ta_out.setDuration(1000);
        botton_ta_in.setDuration(1000);
        botton_ta_out.setDuration(1000);
        this.current = null;
        this.h = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        if (ClientShowAd.this.current.getVisibility() == 0) {
                            ClientShowAd.this.current.startAnimation(ClientShowAd.left_ta_out);
                            ClientShowAd.this.current.setVisibility(8);
                            break;
                        }
                        break;
                    case 2:
                        if (ClientShowAd.this.current.getVisibility() == 0) {
                            ClientShowAd.this.current.startAnimation(ClientShowAd.botton_ta_out);
                            ClientShowAd.this.current.setVisibility(8);
                            break;
                        }
                        break;
                    case 3:
                        if (ClientShowAd.this.current.getVisibility() == 0) {
                            ClientShowAd.this.current.startAnimation(ClientShowAd.right_ta_out);
                            ClientShowAd.this.current.setVisibility(8);
                            break;
                        }
                        break;
                    case 4:
                        if (ClientShowAd.this.current.getVisibility() == 0) {
                            ClientShowAd.this.current.startAnimation(ClientShowAd.top_ta_out);
                            ClientShowAd.this.current.setVisibility(8);
                            break;
                        }
                        break;
                }
                super.handleMessage(msg);
            }
        };
    }

    static {
        adview_url = "http://www.baidu.com/?c=show&";
        adview_url = String.valueOf(adview_url) + "gps=" + PerfHelper.getStringData(PerfHelper.P_GPS) + "&pix=" + PerfHelper.getIntData(PerfHelper.P_PHONE_W) + "x" + PerfHelper.getIntData(PerfHelper.P_PHONE_H) + "&e=" + JniUtils.getkey() + "&platform=5&pid=" + FinalVariable.pid + "&mobile=android";
    }

    public static void initFullAd() {
    }

    public static ArrayList<Listitem> addHomeAd() {
        List<NameValuePair> param = new ArrayList<>();
        ArrayList<Listitem> al = new ArrayList<>();
        try {
            JSONObject jsonobj = new JSONObject(JsonAD.getInfo(String.valueOf(adview_url) + "&type=2" + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID), param));
            if (!jsonobj.has(Constants.SINA_CODE) || jsonobj.getInt(Constants.SINA_CODE) != 0) {
                JSONArray ja = jsonobj.getJSONArray("list");
                int count = ja.length();
                for (int i = 0; i < count; i++) {
                    JSONObject j = ja.getJSONObject(i);
                    Listitem li = new Listitem();
                    li.sa = "home_ad_i";
                    li.u_date = "home_ad_i";
                    li.des = j.getString("des");
                    li.icon = j.getString("picname");
                    li.isad = "true";
                    li.other = j.getString(LocaleUtil.INDONESIAN);
                    li.title = j.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                    al.add(li);
                }
                return al;
            }
            Log.i("ClientShowAd", "no hd ad");
            return al;
        } catch (Exception e) {
            if (ShareApplication.debug) {
                Log.i("ClientShowAd", "no home ad");
            }
        }
    }

    public void showAdFIXED(final Context mContext, int type, final AdAdapter la, String key) {
        if (!this.fixed_isclick) {
            if (this.wv_fixed != null) {
                this.current_adapter = la;
                la.setAdview(this.wv_fixed);
                return;
            }
            this.current_adapter = la;
            final WebView adview = new WebView(mContext);
            this.list_json_title = null;
            adview.getSettings().setJavaScriptEnabled(true);
            setWebView(adview);
            adview.setBackgroundColor(0);
            adview.setWebChromeClient(new WebChromeClient() {
                public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                    quotaUpdater.updateQuota(5242880);
                }

                public void onReceivedTitle(WebView view, String title) {
                    ClientShowAd.this.list_json_title = title;
                    try {
                        JSONObject obj = new JSONObject(title);
                        if (obj.has(Constants.SINA_CODE)) {
                            if (obj.getInt(Constants.SINA_CODE) != 1) {
                                ClientShowAd.this.list_json_title = null;
                                return;
                            } else if (obj.has("ad_provider")) {
                                String provider = obj.getString("ad_provider");
                                if ("palmtrends".equals(provider)) {
                                    ClientShowAd.this.fixed_info = null;
                                    return;
                                }
                                ClientShowAd.this.fixed_info = new adtypeinfo();
                                ClientShowAd.this.fixed_info.key = obj.getString("keyorid");
                                ClientShowAd.this.fixed_info.type = provider;
                                ClientShowAd.this.list_json_title = null;
                                return;
                            } else {
                                return;
                            }
                        }
                    } catch (JSONException e1) {
                        if (ShareApplication.debug) {
                            e1.printStackTrace();
                        }
                        ClientShowAd.this.list_json_title = null;
                    }
                    try {
                        new JSONArray(title);
                        ClientShowAd.this.list_json_title = title;
                    } catch (JSONException e) {
                        ClientShowAd.this.list_json_title = null;
                    }
                }
            });
            adview.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    try {
                        if (ClientShowAd.this.list_json_title == null) {
                            return;
                        }
                    } catch (Exception e) {
                        if (ShareApplication.debug) {
                            e.printStackTrace();
                        }
                    }
                    View li = ClientShowAd.this.addFramLayout(adview, 72);
                    la.setAdview(li);
                    ClientShowAd.this.wv_fixed = li;
                    ClientShowAd.this.wv_fixed.setVisibility(0);
                }

                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if ("http://closead/".equals(url)) {
                        adview.loadUrl("");
                        ClientShowAd.this.fixed_isclick = true;
                        ClientShowAd.this.current_adapter.remove();
                        adview.clearHistory();
                    } else {
                        ClientShowAd.dealClick(mContext, url);
                    }
                    return true;
                }
            });
            adview.loadUrl(String.valueOf(adview_url) + "&type=" + type + "&keyword=" + key + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID));
            if (ShareApplication.debug) {
                System.out.println(String.valueOf(adview_url) + "&type=" + type + "&keyword=" + key + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID));
            }
        }
    }

    public View addFramLayout(WebView adview, int h2) {
        FrameLayout li = new FrameLayout(adview.getContext());
        adview.setLayoutParams(new FrameLayout.LayoutParams(-1, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * h2) / 480));
        li.addView(adview);
        return li;
    }

    public void showAdFIXED_Out(final Activity mContext, int type, String key) {
        final LinearLayout view = (LinearLayout) mContext.findViewById(R.id.second_layout);
        final WebView adview = new WebView(mContext);
        setWebView(adview);
        adview.setBackgroundColor(0);
        adview.setWebChromeClient(new WebChromeClient() {
            public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                quotaUpdater.updateQuota(5242880);
            }

            public void onReceivedTitle(WebView view, String title) {
                ClientShowAd.this.info_fiexd_out = title;
                try {
                    JSONObject obj = new JSONObject(title);
                    if (obj.has(Constants.SINA_CODE)) {
                        if (obj.getInt(Constants.SINA_CODE) != 1) {
                            ClientShowAd.this.info_fiexd_out = null;
                            return;
                        } else if (obj.has("ad_provider")) {
                            String provider = obj.getString("ad_provider");
                            if ("palmtrends".equals(provider)) {
                                ClientShowAd.this.fixed_info = null;
                                return;
                            }
                            ClientShowAd.this.fixed_info = new adtypeinfo();
                            ClientShowAd.this.fixed_info.key = obj.getString("keyorid");
                            ClientShowAd.this.fixed_info.type = provider;
                            ClientShowAd.this.info_fiexd_out = null;
                            return;
                        } else {
                            return;
                        }
                    }
                } catch (JSONException e) {
                    ClientShowAd.this.info_fiexd_out = null;
                }
                try {
                    new JSONArray(title);
                    ClientShowAd.this.info_fiexd_out = title;
                } catch (JSONException e2) {
                    ClientShowAd.this.info_fiexd_out = null;
                }
            }
        });
        adview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView weibu, String url) {
                if ("http://closead/".equals(url)) {
                    adview.loadUrl("");
                    adview.setVisibility(8);
                    adview.clearHistory();
                } else {
                    ClientShowAd.dealClick(mContext, url);
                }
                return true;
            }

            public void onPageFinished(WebView webview, String url) {
                super.onPageFinished(webview, url);
                if (ClientShowAd.this.info_fiexd_out == null || "找不到网页".equals(ClientShowAd.this.info_fiexd_out)) {
                    adview.setVisibility(8);
                    return;
                }
                try {
                    LinearLayout.LayoutParams rlp = new LinearLayout.LayoutParams(-1, -2);
                    View content = ClientShowAd.this.addFramLayout(adview, 72);
                    content.setLayoutParams(rlp);
                    view.addView(content);
                    view.setVisibility(0);
                } catch (Exception e) {
                    if (ShareApplication.debug) {
                        e.printStackTrace();
                    }
                }
            }
        });
        adview.loadUrl(String.valueOf(adview_url) + "&type=" + type + "&keyword=" + key + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID));
        if (ShareApplication.debug) {
            System.out.println(String.valueOf(adview_url) + "&type=" + type + "&keyword=" + key + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID));
        }
    }

    public void showAdPOP_UP(Activity mContext, final int type, String key) {
        final RelativeLayout v = (RelativeLayout) mContext.findViewById(R.id.mainroot);
        if (v != null && !sdf.format(new Date()).equals(PerfHelper.getStringData(ISPOP + type))) {
            final WebView adview = new WebView(mContext);
            this.ad = null;
            setWebView(adview);
            adview.setBackgroundColor(0);
            adview.setWebChromeClient(new WebChromeClient() {
                public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                    quotaUpdater.updateQuota(5242880);
                }

                public void onReceivedTitle(WebView view, String title) {
                    try {
                        ClientShowAd.this.ad = JsonAD.getJsonType(title, type);
                        ClientShowAd.this.info = title;
                    } catch (Exception e) {
                        if (ShareApplication.debug) {
                            e.printStackTrace();
                        }
                        ClientShowAd.this.ad = null;
                    }
                }
            });
            final Activity activity = mContext;
            final int i = type;
            adview.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if ("http://closead/".equals(url)) {
                        adview.setVisibility(8);
                        adview.loadUrl("");
                        adview.clearHistory();
                    } else {
                        ClientShowAd.dealClick(activity, url);
                    }
                    return true;
                }

                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    if (ClientShowAd.this.ad != null) {
                        PerfHelper.setInfo(ClientShowAd.ISPOP + i, ClientShowAd.sdf.format(new Date()));
                        int h = (ClientShowAd.this.ad.height * PerfHelper.getIntData(PerfHelper.P_PHONE_W)) / 480;
                        View contentview = ClientShowAd.this.addFramLayout(adview, h);
                        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(-1, h);
                        rlp.addRule(12);
                        contentview.setLayoutParams(rlp);
                        v.addView(contentview);
                        contentview.clearAnimation();
                        if (UploadUtils.FAILURE.equals(new StringBuilder(String.valueOf(ClientShowAd.this.ad.show)).toString())) {
                            ClientShowAd.this.showAd_leftin(contentview, ClientShowAd.this.ad);
                        } else if ("2".equals(new StringBuilder(String.valueOf(ClientShowAd.this.ad.show)).toString())) {
                            ClientShowAd.this.showAd_rightin(contentview, ClientShowAd.this.ad);
                        } else if ("3".equals(new StringBuilder(String.valueOf(ClientShowAd.this.ad.show)).toString())) {
                            ClientShowAd.this.showAd_topin(contentview, ClientShowAd.this.ad);
                        } else if ("4".equals(new StringBuilder(String.valueOf(ClientShowAd.this.ad.show)).toString())) {
                            ClientShowAd.this.showAd_bottonin(contentview, ClientShowAd.this.ad);
                        }
                    }
                }
            });
            adview.loadUrl(String.valueOf(adview_url) + "&type=" + type + "&keyword=" + key + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID));
            if (ShareApplication.debug) {
                System.out.println(String.valueOf(adview_url) + "&type=" + type + "&keyword=" + key + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID));
            }
        } else if (ShareApplication.debug && v == null) {
            try {
                throw new DataException("错误代码:100011");
            } catch (DataException e) {
                if (ShareApplication.debug) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static boolean dealClick(Context mContext, String url) {
        try {
            Uri uri = Uri.parse(url);
            Intent it = new Intent("android.intent.action.VIEW", uri);
            if (url.endsWith(".mp3")) {
                it.setDataAndType(uri, "audio/mp3");
                mContext.startActivity(it);
                return true;
            } else if (url.endsWith(".mp4")) {
                it.setFlags(67108864);
                it.setType("video/mp4");
                it.setDataAndType(uri, "video/mp4");
                mContext.startActivity(it);
                return true;
            } else {
                if (url.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                    Intent i = new Intent();
                    i.setAction(mContext.getResources().getString(R.string.activity_article_showwebinfo));
                    i.putExtra(com.tencent.tauth.Constants.PARAM_URL, url);
                    mContext.startActivity(i);
                    return true;
                }
                return false;
            }
        } catch (Exception e) {
            if (ShareApplication.debug) {
                Utils.showToast("请安装浏览器");
            }
        }
    }

    public void showAd_bottonin(View v, ADItem a) {
        this.current = v;
        v.startAnimation(botton_ta_in);
        v.setVisibility(0);
        new Timer(true).schedule(new TimerTask() {
            public void run() {
                ClientShowAd.this.h.sendEmptyMessage(2);
            }
        }, (long) a.times);
    }

    public void showAd_leftin(View v, ADItem a) {
        this.current = v;
        v.startAnimation(left_ta);
        v.setVisibility(0);
        new Timer(true).schedule(new TimerTask() {
            public void run() {
                ClientShowAd.this.h.sendEmptyMessage(1);
            }
        }, (long) a.times);
    }

    public void showAd_rightin(View v, ADItem a) {
        this.current = v;
        v.startAnimation(right_ta);
        v.setVisibility(0);
        new Timer(true).schedule(new TimerTask() {
            public void run() {
                ClientShowAd.this.h.sendEmptyMessage(3);
            }
        }, (long) a.times);
    }

    public void showAd_topin(View v, ADItem a) {
        this.current = v;
        v.startAnimation(top_ta_in);
        v.setVisibility(0);
        new Timer(true).schedule(new TimerTask() {
            public void run() {
                ClientShowAd.this.h.sendEmptyMessage(4);
            }
        }, (long) a.times);
    }

    static class adtypeinfo {
        String key;
        String type;

        adtypeinfo() {
        }

        public static adtypeinfo getadtypeinfo(String info) throws Exception {
            JSONObject json = new JSONObject(info);
            adtypeinfo ai = new adtypeinfo();
            ai.type = json.getString("ad_provider");
            ai.key = json.getString("keyorid");
            return ai;
        }
    }

    public static void setWebView(WebView wv) {
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setDatabaseEnabled(true);
        wv.getSettings().setDomStorageEnabled(true);
        wv.getSettings().setDatabasePath(ShareApplication.share.getDir("database" + ShareApplication.share.getPackageName(), 0).getPath());
    }
}
