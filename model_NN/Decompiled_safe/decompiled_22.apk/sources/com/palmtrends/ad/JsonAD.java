package com.palmtrends.ad;

import android.content.ContentValues;
import android.util.Log;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.MySSLSocketFactory;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.FileUtils;
import com.utils.PerfHelper;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class JsonAD {
    public static void showFullAd(String url) {
        List<NameValuePair> param = new ArrayList<>();
        String time = PerfHelper.getStringData(ClientShowAd.INIT_AD_TIME_SETTING);
        try {
            JSONObject jsonobj = new JSONObject(getInfo(String.valueOf(url) + "&type=1&lastupdate=" + time, param));
            if (jsonobj.has(Constants.SINA_CODE)) {
                int code = jsonobj.getInt(Constants.SINA_CODE);
                if (code == 0) {
                    Log.i("ClientShowAd", "no full update");
                    return;
                } else if (code == 2) {
                    PerfHelper.setInfo(ShowFullAd.INITAD_DATA, "");
                }
            }
            if (jsonobj.has("lastupdate")) {
                PerfHelper.setInfo(ShowFullAd.INIT_AD_OLDTIME_SETTING, time);
                PerfHelper.setInfo(ClientShowAd.INIT_AD_TIME_SETTING, jsonobj.getString("lastupdate"));
                JSONArray ja = jsonobj.getJSONArray("list");
                int count = ja.length();
                for (int i = 0; i < count; i++) {
                    JSONObject obj = ja.getJSONObject(i);
                    FullAdItem fai = new FullAdItem();
                    fai.id = obj.getString(LocaleUtil.INDONESIAN);
                    fai.invalid = obj.getString("invalid");
                    fai.src = obj.getString("src");
                    if (UploadUtils.FAILURE.equals(fai.invalid)) {
                        ShowFullAd.deleteFiles(String.valueOf(FileUtils.sdPath) + ShowFullAd.ADDIR + CookieSpec.PATH_DELIM + fai.id);
                        new File(String.valueOf(FileUtils.sdPath) + ShowFullAd.ADDIR + CookieSpec.PATH_DELIM + fai.id + ".zip").delete();
                        DBHelper.getDBHelper().delete("fulladinfo", "ad_id=?", new String[]{fai.id});
                    } else {
                        if (!"".equals(fai.src)) {
                            ShowFullAd.getInputStream(String.valueOf(ClientShowAd.ad_main) + fai.src, fai);
                        }
                        insertorupdate(fai.id, obj.toString(), fai.invalid);
                    }
                }
            }
        } catch (Exception e) {
            ShowFullAd.deleteFiles(String.valueOf(FileUtils.sdPath) + ShowFullAd.ADDIR + CookieSpec.PATH_DELIM);
            PerfHelper.setInfo(ClientShowAd.INIT_AD_TIME_SETTING, time);
            if (ShareApplication.debug) {
                e.printStackTrace();
                Log.i("ClientShowAd", "no full ad");
            }
        }
    }

    public static void insertorupdate(String ad_id, String info, String invalid) {
        if (DBHelper.getDBHelper().counts("fulladinfo", "ad_id='" + ad_id + "'") > 0) {
            ContentValues cv = new ContentValues();
            cv.put("info", info);
            cv.put("invalid", invalid);
            DBHelper.getDBHelper().update("fulladinfo", "ad_id=?", new String[]{ad_id}, cv);
            return;
        }
        FullAdStore fas = new FullAdStore();
        fas.ad_id = ad_id;
        fas.info = info;
        fas.invalid = invalid;
        try {
            DBHelper.getDBHelper().insertObject(fas, "fulladinfo");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<FullAdItem> parseFullad(String json) throws Exception {
        DBHelper.getDBHelper().getWritableDatabase().execSQL("CREATE TABLE IF NOT EXISTS fulladinfo (c_id INTEGER primary key autoincrement,ad_id CHAR UNIQUE,info TEXT,invalid CHAR);");
        List<FullAdItem> items = new ArrayList<>();
        List<FullAdStore> adstores = DBHelper.getDBHelper().select("fulladinfo", FullAdStore.class, "invalid='0'", 0, 1000);
        int count = adstores.size();
        for (int i = 0; i < count; i++) {
            JSONObject obj = new JSONObject(((FullAdStore) adstores.get(i)).info);
            FullAdItem fai = new FullAdItem();
            fai.id = obj.getString(LocaleUtil.INDONESIAN);
            fai.type = obj.getString("type");
            fai.url = obj.getString(com.tencent.tauth.Constants.PARAM_URL);
            fai.src = obj.getString("src");
            fai.s_time = obj.getString("s_time");
            fai.e_time = obj.getString("e_time");
            fai.times = obj.getString("times");
            fai.ad_provider = obj.getString("ad_provider");
            fai.keyorid = obj.getString("keyorid");
            fai.invalid = obj.getString("invalid");
            if (UploadUtils.FAILURE.equals(fai.invalid)) {
                ShowFullAd.deleteFiles(String.valueOf(FileUtils.sdPath) + ShowFullAd.ADDIR + CookieSpec.PATH_DELIM + fai.id);
                new File(String.valueOf(FileUtils.sdPath) + ShowFullAd.ADDIR + CookieSpec.PATH_DELIM + fai.id + ".zip").delete();
            } else {
                items.add(fai);
            }
        }
        return items;
    }

    public static ADItem getJsonType(String strs, int type) throws Exception {
        JSONObject obj = new JSONObject(strs);
        ADItem o = new ADItem();
        o.height = obj.getInt("height");
        o.show = obj.getInt("show");
        o.pos = obj.getInt("pos");
        o.times = obj.getInt("times");
        return o;
    }

    public static List<FiexdADItem> getJsonType(String strs) throws Exception {
        JSONArray jsons = new JSONArray(strs);
        int count = jsons.length();
        List<FiexdADItem> li = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            JSONObject obj = jsons.getJSONObject(i);
            FiexdADItem o = new FiexdADItem();
            o.count = obj.getInt("count");
            o.adid = obj.getInt("adid");
            li.add(o);
        }
        return li;
    }

    public static String getInfo(String url, List<NameValuePair> param) throws Exception {
        HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
        HttpPost hp = new HttpPost(url);
        if (ShareApplication.debug) {
            String str = url;
            Iterator<NameValuePair> it = param.iterator();
            while (it.hasNext()) {
                BasicNameValuePair b = (NameValuePair) it.next();
                str = String.valueOf(str) + "&" + b.getName() + "=" + b.getValue();
            }
            System.out.println(str);
        }
        hp.setEntity(new UrlEncodedFormEntity(param, "utf-8"));
        return EntityUtils.toString(httpclient.execute(hp).getEntity(), "utf-8");
    }
}
