package com.palmtrends.ad;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.actionbarsherlock.widget.ActivityChooserView;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseInitAcitvity;
import com.palmtrends.controll.R;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.loadimage.Utils;
import com.utils.FileUtils;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import com.utils.ZipUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class ShowFullAd extends AsyncTask<View, View, FullAdItem> {
    public static String ADDIR = "ad";
    public static final String INITAD_DATA = "initad_data";
    public static final String INIT_AD_OLDTIME_SETTING = "init_ad_oldtime_setting";
    public boolean hasFullAd = false;
    public boolean isclick = false;
    public View view;

    public ShowFullAd(View view2) {
        FullAdItem result;
        this.view = view2;
        try {
            List<FullAdItem> items = JsonAD.parseFullad(PerfHelper.getStringData(INITAD_DATA));
            if (items != null && items.size() != 0) {
                long currenttime = System.currentTimeMillis() / 1000;
                int count = items.size();
                long du = Long.MAX_VALUE;
                int chooseindex = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
                for (int i = 0; i < count; i++) {
                    long d = currenttime - Long.parseLong(items.get(i).s_time);
                    if (currenttime > Long.parseLong(items.get(i).e_time)) {
                        deleteFiles(String.valueOf(FileUtils.sdPath) + ADDIR + CookieSpec.PATH_DELIM + items.get(i).id);
                        new File(String.valueOf(FileUtils.sdPath) + ADDIR + CookieSpec.PATH_DELIM + items.get(i).id + ".zip").delete();
                        DBHelper.getDBHelper().delete("fulladinfo", "ad_id=?", new String[]{items.get(i).id});
                    } else if (d > 0 && d < du) {
                        du = d;
                        chooseindex = i;
                    }
                }
                if (chooseindex <= count && (result = items.get(chooseindex)) != null) {
                    if (!"palmtrends".equals(result.ad_provider)) {
                        this.hasFullAd = true;
                        ((BaseInitAcitvity) view2.getContext()).showOther();
                    } else if ("html".equals(result.type)) {
                        initWebView(result);
                    } else if ("video".equals(result.type)) {
                        initVideoView(result);
                    }
                }
            }
        } catch (Exception e) {
            if (ShareApplication.debug) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public FullAdItem doInBackground(View... params) {
        ClientShowAd.initFullAd();
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(FullAdItem result) {
        super.onPostExecute((Object) result);
    }

    public void initVideoView(final FullAdItem result) {
        RelativeLayout v = (RelativeLayout) ((Activity) this.view.getContext()).findViewById(R.id.mainroot);
        if (!new File(String.valueOf(FileUtils.sdPath) + ADDIR + CookieSpec.PATH_DELIM + result.id + "/index.mp4").exists()) {
            PerfHelper.setInfo(ClientShowAd.INIT_AD_TIME_SETTING, PerfHelper.getStringData(INIT_AD_OLDTIME_SETTING));
            this.hasFullAd = false;
            new File(String.valueOf(FileUtils.sdPath) + ADDIR + CookieSpec.PATH_DELIM + result.id + ".zip").delete();
            return;
        }
        if (result.canClose) {
            addCloseBtn(v);
        }
        this.hasFullAd = true;
        VideoView vv = new VideoView(this.view.getContext());
        RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(-1, -1);
        llp.addRule(13);
        vv.setLayoutParams(llp);
        v.removeAllViews();
        v.setBackgroundColor(Color.parseColor("#000000"));
        v.addView(vv);
        vv.setVideoURI(Uri.parse(String.valueOf(FileUtils.sdPath) + ADDIR + CookieSpec.PATH_DELIM + result.id + "/index.mp4"));
        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                ShowFullAd.this.hasFullAd = false;
                BaseInitAcitvity context = (BaseInitAcitvity) ShowFullAd.this.view.getContext();
                if ("".equals(PerfHelper.getStringData(PerfHelper.P_USERID))) {
                    context.initDataUserInfo();
                } else {
                    context.begin_StartActivity();
                }
                ShowFullAd.this.hasFullAd = true;
            }
        });
        vv.start();
        if (!"".equals(result.url)) {
            final GestureDetector gd = new GestureDetector(new GestureDetector.OnGestureListener() {
                public boolean onSingleTapUp(MotionEvent e) {
                    if (!ClientShowAd.dealClick(ShowFullAd.this.view.getContext(), result.url)) {
                        return false;
                    }
                    ShowFullAd.this.isclick = true;
                    return false;
                }

                public void onShowPress(MotionEvent e) {
                }

                public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                    return false;
                }

                public void onLongPress(MotionEvent e) {
                }

                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    return false;
                }

                public boolean onDown(MotionEvent e) {
                    return false;
                }
            });
            vv.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    gd.onTouchEvent(event);
                    return true;
                }
            });
        }
    }

    public void initWebView(final FullAdItem result) {
        RelativeLayout v = (RelativeLayout) ((Activity) this.view.getContext()).findViewById(R.id.mainroot);
        File file = new File(String.valueOf(FileUtils.sdPath) + ADDIR + CookieSpec.PATH_DELIM + result.id + "/index.html");
        if (ShareApplication.debug) {
            System.out.println(String.valueOf(file.getAbsolutePath()) + "----------" + file.exists());
        }
        if (!file.exists()) {
            PerfHelper.setInfo(ClientShowAd.INIT_AD_TIME_SETTING, PerfHelper.getStringData(INIT_AD_OLDTIME_SETTING));
            this.hasFullAd = false;
            new File(String.valueOf(FileUtils.sdPath) + ADDIR + CookieSpec.PATH_DELIM + result.id + ".zip").delete();
            return;
        }
        if (result.canClose) {
            addCloseBtn(v);
        }
        this.hasFullAd = true;
        v.removeAllViews();
        v.setBackgroundColor(Color.parseColor("#000000"));
        WebView adview = new WebView(this.view.getContext());
        adview.setWebChromeClient(new WebChromeClient() {
            public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                quotaUpdater.updateQuota(5242880);
            }
        });
        ClientShowAd.setWebView(adview);
        adview.setBackgroundColor(0);
        adview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (ClientShowAd.dealClick(view.getContext(), url)) {
                    ShowFullAd.this.isclick = true;
                }
                return true;
            }

            public void onPageFinished(final WebView view, String url) {
                Utils.h.postDelayed(new Runnable() {
                    public void run() {
                        ShowFullAd.this.hasFullAd = false;
                        BaseInitAcitvity context = (BaseInitAcitvity) view.getContext();
                        if ("".equals(PerfHelper.getStringData(PerfHelper.P_USERID))) {
                            context.initDataUserInfo();
                        } else {
                            context.begin_StartActivity();
                        }
                        ShowFullAd.this.hasFullAd = true;
                    }
                }, (long) Integer.parseInt(result.times));
            }
        });
        adview.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        v.addView(adview);
        adview.loadUrl("file://" + FileUtils.sdPath + ADDIR + CookieSpec.PATH_DELIM + result.id + "/index.html?&h=" + px2dip(adview.getContext(), (float) adview.getContext().getResources().getDisplayMetrics().heightPixels) + "&w=" + px2dip(adview.getContext(), (float) adview.getContext().getResources().getDisplayMetrics().widthPixels));
    }

    public void addCloseBtn(RelativeLayout v) {
        TextView tv = new TextView(v.getContext());
        tv.setText("关闭");
        tv.setId(R.id.ids_close);
        new RelativeLayout.LayoutParams(-2, -2).addRule(11);
        tv.setPadding(15, 15, 15, 15);
        v.addView(tv);
    }

    public static InputStream getInputStream(String urls, FullAdItem p) throws Exception {
        File file = new File(String.valueOf(FileUtils.sdPath) + ADDIR);
        if (!file.exists()) {
            file.mkdirs();
        }
        File f = new File(file.getAbsoluteFile() + CookieSpec.PATH_DELIM + p.id + ".zip");
        File filehtml = new File(file.getAbsoluteFile() + CookieSpec.PATH_DELIM + p.id + "/index.html");
        File filemp4 = new File(file.getAbsoluteFile() + CookieSpec.PATH_DELIM + p.id + "/index.mp4");
        if (f.exists() && (filehtml.exists() || filemp4.exists())) {
            return null;
        }
        HttpURLConnection conn = (HttpURLConnection) new URL(urls).openConnection();
        conn.setDoInput(true);
        conn.setConnectTimeout(FinalVariable.vb_success);
        conn.setRequestMethod("GET");
        conn.connect();
        for (int i = 0; i < 4; i++) {
            if (conn.getResponseCode() == 200) {
                InputStream is = conn.getInputStream();
                save(is, p);
                return is;
            }
        }
        return null;
    }

    public static void save(InputStream is, FullAdItem p) throws Exception {
        File file = new File(String.valueOf(FileUtils.sdPath) + ADDIR);
        if (!file.exists()) {
            file.mkdirs();
        }
        File f = new File(file.getAbsoluteFile() + CookieSpec.PATH_DELIM + p.id + ".zip");
        f.createNewFile();
        FileOutputStream fos = new FileOutputStream(f);
        byte[] b = new byte[512000];
        while (true) {
            int x = is.read(b);
            if (x == -1) {
                fos.close();
                is.close();
                ZipUtils.UnZipFolder(f.getAbsolutePath(), file.getAbsoluteFile() + CookieSpec.PATH_DELIM + p.id);
                return;
            }
            fos.write(b, 0, x);
            fos.flush();
        }
    }

    public static int px2dip(Context context, float pxValue) {
        return (int) ((pxValue / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static void deleteFiles(String url) {
        File file = new File(url);
        if (file.exists()) {
            File[] files = file.listFiles();
            int count = files.length;
            for (int i = 0; i < count; i++) {
                if (files[i].isDirectory()) {
                    deleteFiles(files[i].getAbsolutePath());
                } else {
                    files[i].delete();
                }
            }
        }
    }
}
