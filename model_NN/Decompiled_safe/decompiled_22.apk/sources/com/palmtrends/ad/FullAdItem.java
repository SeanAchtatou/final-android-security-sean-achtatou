package com.palmtrends.ad;

public class FullAdItem {
    public String ad_provider = "palmtrends";
    public boolean canClose = false;
    public String e_time;
    public String id;
    public String invalid;
    public String keyorid = "";
    public String s_time;
    public String src;
    public String times;
    public String type;
    public String url;
}
