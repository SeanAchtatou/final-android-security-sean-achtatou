package com.palmtrends.controll;

public final class R {

    public static final class anim {
        public static final int init_in = 2130968579;
        public static final int init_out = 2130968580;
    }

    public static final class array {
        public static final int app_sql_delete = 2131689475;
        public static final int appconfig_sql = 2131689473;
        public static final int appconfig_sql_upgrade = 2131689474;
        public static final int article_list_name = 2131689476;
        public static final int article_wb_list_name_sa = 2131689477;
        public static final int second_names = 2131689472;
    }

    public static final class bool {
        public static final int article_hastitle = 2131361799;
        public static final int hashome = 2131361798;
        public static final int offline = 2131361797;
        public static final int time_tip = 2131361800;
    }

    public static final class dimen {
        public static final int image_detail_pager_margin = 2131492880;
        public static final int image_home_size = 2131492877;
        public static final int image_thumbnail_size = 2131492878;
        public static final int image_thumbnail_spacing = 2131492879;
        public static final int padding_large = 2131492883;
        public static final int padding_medium = 2131492882;
        public static final int padding_small = 2131492881;
    }

    public static final class drawable {
        public static final int back_btn = 2130837626;
        public static final int back_btn_h = 2130837630;
        public static final int back_btn_n = 2130837631;
        public static final int banner = 2130837632;
        public static final int failure_expression = 2130837681;
        public static final int ic_action_search = 2130837701;
        public static final int ic_launcher = 2130837702;
        public static final int ic_pulltorefresh_arrow = 2130837703;
        public static final int ic_push = 2130837704;
        public static final int listhead_title_bg = 2130837736;
        public static final int send_btn = 2130837836;
        public static final int send_h = 2130837837;
        public static final int send_n = 2130837838;
    }

    public static final class id {
        public static final int adfiexdview = 2131231030;
        public static final int articl_big_image = 2131230920;
        public static final int article_pb = 2131230919;
        public static final int article_webvew = 2131230972;
        public static final int but_refurbish_data = 2131231081;
        public static final int footer_pb = 2131231013;
        public static final int footer_text = 2131231014;
        public static final int frameLayout = 2131231001;
        public static final int head_arrowImageView = 2131231019;
        public static final int head_contentLayout = 2131231018;
        public static final int head_icon = 2131231031;
        public static final int head_lastUpdatedTextView = 2131231022;
        public static final int head_progressBar = 2131231020;
        public static final int head_rootLayout = 2131231017;
        public static final int head_tipsTextView = 2131231021;
        public static final int head_title = 2131231032;
        public static final int headerview = 2131231005;
        public static final int ids_close = 2131230725;
        public static final int imageView = 2131231003;
        public static final int image_info = 2131230921;
        public static final int imageview = 2131231026;
        public static final int info = 2131231012;
        public static final int laoding_has_date = 2131230979;
        public static final int laoding_no_date = 2131231080;
        public static final int list_id_list = 2131231028;
        public static final int listitem_date = 2131231045;
        public static final int listitem_des = 2131231040;
        public static final int listitem_icon = 2131231043;
        public static final int listitem_title = 2131231037;
        public static final int loading = 2131230822;
        public static final int loading_text = 2131230824;
        public static final int mainroot = 2131230798;
        public static final int move_items = 2131231008;
        public static final int part_content = 2131230808;
        public static final int pop_title = 2131231027;
        public static final int processbar = 2131230823;
        public static final int progressBar = 2131231002;
        public static final int return_btn = 2131231193;
        public static final int seccond_allitem = 2131231007;
        public static final int second_content = 2131231006;
        public static final int second_items = 2131231009;
        public static final int second_layout = 2131230800;
        public static final int second_left = 2131231010;
        public static final int second_rigth = 2131231011;
        public static final int secondscroll = 2131231141;
        public static final int title = 2131230801;
        public static final int titlebar = 2131230825;
        public static final int web = 2131231191;
        public static final int webview = 2131231192;
        public static final int xlistview_footer_content = 2131231242;
        public static final int xlistview_footer_hint_textview = 2131231244;
        public static final int xlistview_footer_progressbar = 2131231243;
        public static final int xlistview_header_arrow = 2131231246;
        public static final int xlistview_header_content = 2131231245;
        public static final int xlistview_header_hint_textview = 2131231249;
        public static final int xlistview_header_progressbar = 2131231247;
        public static final int xlistview_header_text = 2131231248;
        public static final int xlistview_header_time = 2131231250;
    }

    public static final class integer {
        public static final int mleng = 2131427333;
    }

    public static final class layout {
        public static final int activity_main = 2130903072;
        public static final int article_bigimage = 2130903095;
        public static final int article_fragment = 2130903098;
        public static final int draw_detail_fragment = 2130903112;
        public static final int footer = 2130903114;
        public static final int head = 2130903117;
        public static final int image = 2130903119;
        public static final int list_loadmoresinglerlist = 2130903122;
        public static final int list_singlerlist = 2130903124;
        public static final int listhead = 2130903126;
        public static final int listitem_singlerlist = 2130903144;
        public static final int loading_layout = 2130903149;
        public static final int second_content = 2130903167;
        public static final int second_text = 2130903168;
        public static final int secondscroll = 2130903169;
        public static final int showwebinfo = 2130903178;
        public static final int xlistview_footer = 2130903194;
        public static final int xlistview_header = 2130903195;
    }

    public static final class string {
        public static final int activity_all_finish = 2131623997;
        public static final int activity_article = 2131623967;
        public static final int activity_article_comment = 2131623972;
        public static final int activity_article_comment_list = 2131623971;
        public static final int activity_article_showwebinfo = 2131623973;
        public static final int activity_home = 2131623965;
        public static final int activity_init = 2131623964;
        public static final int activity_main = 2131623966;
        public static final int activity_share = 2131623968;
        public static final int activity_share_bind = 2131623969;
        public static final int activity_share_setting = 2131623970;
        public static final int activity_st_about = 2131623983;
        public static final int activity_st_fav = 2131623981;
        public static final int activity_st_feedback = 2131623982;
        public static final int activity_st_share_manager = 2131623980;
        public static final int activity_st_text = 2131623979;
        public static final int activity_wangqi = 2131623977;
        public static final int activity_wangqiinfo = 2131623978;
        public static final int activity_wb_comment = 2131623975;
        public static final int activity_wb_info = 2131623974;
        public static final int activity_wb_showiamge = 2131623976;
        public static final int alarm_des = 2131623996;
        public static final int alarm_receiver = 2131623986;
        public static final int app_name = 2131623938;
        public static final int article_url = 2131623988;
        public static final int bind_tuijian = 2131623962;
        public static final int cache_dir = 2131623937;
        public static final int cancel = 2131623956;
        public static final int checkupdate_getnew = 2131623947;
        public static final int checkupdate_hasnew = 2131623946;
        public static final int comment_list_no = 2131623963;
        public static final int data_type_error = 2131624003;
        public static final int done = 2131623955;
        public static final int draw_content_url = 2131623992;
        public static final int draw_list = 2131623991;
        public static final int draw_load_down = 2131624011;
        public static final int draw_load_toast = 2131624010;
        public static final int exit = 2131623948;
        public static final int exit_message = 2131623949;
        public static final int fail_to_weixin_quan = 2131624012;
        public static final int home = 2131623989;
        public static final int list_url = 2131623990;
        public static final int loading = 2131623944;
        public static final int loading_n = 2131623945;
        public static final int main = 2131623987;
        public static final int network_error = 2131623950;
        public static final int no_data = 2131623952;
        public static final int no_data_comments = 2131623954;
        public static final int no_data_fav = 2131623953;
        public static final int not_null_tip = 2131623960;
        public static final int pid = 2131623936;
        public static final int pinglun_url = 2131623995;
        public static final int pull_to_refresh_pull_label = 2131623939;
        public static final int pull_to_refresh_refreshing_label = 2131623941;
        public static final int pull_to_refresh_release_label = 2131623940;
        public static final int pull_to_refresh_tap_label = 2131623942;
        public static final int pull_to_refresh_update = 2131623943;
        public static final int push_receiver = 2131623985;
        public static final int push_server = 2131623984;
        public static final int qq_appSecret = 2131624002;
        public static final int qq_appkey = 2131624001;
        public static final int review_url = 2131623993;
        public static final int server_error = 2131623951;
        public static final int share_bind_tip = 2131623958;
        public static final int share_success_tip = 2131623959;
        public static final int share_text_tip = 2131623957;
        public static final int sina_appSecret = 2131624000;
        public static final int sina_appkey = 2131623999;
        public static final int suggest_url = 2131623994;
        public static final int too_long_tip = 2131623961;
        public static final int wx_app_id = 2131623998;
        public static final int xlistview_footer_hint_normal = 2131624008;
        public static final int xlistview_footer_hint_ready = 2131624009;
        public static final int xlistview_header_hint_loading = 2131624006;
        public static final int xlistview_header_hint_normal = 2131624004;
        public static final int xlistview_header_hint_ready = 2131624005;
        public static final int xlistview_header_last_time = 2131624007;
    }

    public static final class style {
        public static final int AppTheme = 2131558405;
        public static final int button_style = 2131558409;
        public static final int c_dialog = 2131558406;
        public static final int listitem_date = 2131558411;
        public static final int listitem_des = 2131558412;
        public static final int listitem_title = 2131558410;
        public static final int loading_pb = 2131558408;
        public static final int loading_text = 2131558407;
        public static final int second_text_style = 2131558413;
        public static final int setting_title = 2131558414;
    }
}
