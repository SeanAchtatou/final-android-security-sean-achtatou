package com.palmtrends.entity;

import org.apache.commons.httpclient.HttpState;

public class AdType extends Entity {
    public String isad = HttpState.PREEMPTIVE_DEFAULT;
    public String ishead = HttpState.PREEMPTIVE_DEFAULT;
    public String nid = "";
    public String sa = "";

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.isad == null ? 0 : this.isad.hashCode()) + 31) * 31;
        if (this.nid != null) {
            i = this.nid.hashCode();
        }
        return hashCode + i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Listitem other = (Listitem) obj;
        if (this.isad == null) {
            if (other.isad != null) {
                return false;
            }
        } else if (!this.isad.equals(other.isad)) {
            return false;
        }
        if (this.nid == null) {
            if (other.nid != null) {
                return false;
            }
            return true;
        } else if (!this.nid.equals(other.nid)) {
            return false;
        } else {
            return true;
        }
    }
}
