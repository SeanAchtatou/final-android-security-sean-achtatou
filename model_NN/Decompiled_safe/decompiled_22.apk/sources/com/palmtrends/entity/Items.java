package com.palmtrends.entity;

import com.city_life.part_asynctask.UploadUtils;

public class Items extends AdType {
    public String address = "";
    public Integer c_id;
    public String des = "";
    public String fuwu = "";
    public String icon = "";
    public String img_list_1 = "";
    public String img_list_2 = "";
    public String latitude = "";
    public String level = "";
    public String longitude = "";
    public String n_mark = "";
    public String other = "";
    public String other1 = "";
    public String other2 = "";
    public String other3 = "";
    public String phone = "";
    public String preferential = "";
    public String shangjia = "";
    public String show_type = UploadUtils.SUCCESS;
    public String title = "";
    public String u_date = "";
    public String vip_id = "";

    public void getMark() {
        this.n_mark = String.valueOf(this.nid) + "_" + this.sa;
    }
}
