package com.palmtrends.entity;

import java.util.Date;

public class WeiboItemInfo extends AdType {
    public String bmiddle_pic;
    public String cCount = "...";
    public Date created_at;
    public String created_str;
    public String id;
    public String name;
    public String original_pic;
    public String profile_image_url;
    public String rCount = "...";
    public WeiboItemInfo retweeted;
    public String retweetedID;
    public String source;
    public String text;
    public String thumbnail_pic;
    public String userID;
    public String userImg;
    public boolean zmark = false;

    public String getUserID() {
        return this.userID;
    }

    public void setUserID(String userID2) {
        this.userID = userID2;
    }

    public String getUserImg() {
        return this.userImg;
    }

    public void setUserImg(String userImg2) {
        this.userImg = userImg2;
    }

    public String getCreated_str() {
        return this.created_str;
    }

    public void setCreated_str(String created_str2) {
        this.created_str = created_str2;
    }

    public boolean getzmark() {
        return this.zmark;
    }

    public void setZmark(boolean zmark2) {
        this.zmark = zmark2;
    }

    public String getcCount() {
        return this.cCount;
    }

    public void setcCount(String cCount2) {
        this.cCount = cCount2;
    }

    public String getrCount() {
        return this.rCount;
    }

    public void setrCount(String rCount2) {
        this.rCount = rCount2;
    }

    public String getRetweetedID() {
        return this.retweetedID;
    }

    public void setRetweetedID(String retweetedID2) {
        this.retweetedID = retweetedID2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public Date getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(Date created_at2) {
        this.created_at = created_at2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source2) {
        this.source = source2;
    }

    public String getThumbnail_pic() {
        return this.thumbnail_pic;
    }

    public void setThumbnail_pic(String thumbnail_pic2) {
        this.thumbnail_pic = thumbnail_pic2;
    }

    public String getBmiddle_pic() {
        return this.bmiddle_pic;
    }

    public void setBmiddle_pic(String bmiddle_pic2) {
        this.bmiddle_pic = bmiddle_pic2;
    }

    public String getOriginal_pic() {
        return this.original_pic;
    }

    public void setOriginal_pic(String original_pic2) {
        this.original_pic = original_pic2;
    }

    public String getProfile_image_url() {
        return this.profile_image_url;
    }

    public void setProfile_image_url(String profile_image_url2) {
        this.profile_image_url = profile_image_url2;
    }

    public WeiboItemInfo getRetweeted() {
        return this.retweeted;
    }

    public void setRetweeted(WeiboItemInfo retweeted2) {
        this.retweeted = retweeted2;
    }
}
