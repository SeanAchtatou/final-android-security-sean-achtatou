package com.palmtrends.app;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Security;
import java.util.ArrayList;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.methods.multipart.FilePart;

public class GMailSender extends Authenticator {
    private String mailhost = "smtp.gmail.com";
    private String password;
    private Session session;
    private String user;

    static {
        Security.addProvider(new JSSEProvider());
    }

    public GMailSender(String user2, String password2) {
        this.user = user2;
        this.password = password2;
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", this.mailhost);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", HttpState.PREEMPTIVE_DEFAULT);
        props.setProperty("mail.smtp.quitwait", HttpState.PREEMPTIVE_DEFAULT);
        this.session = Session.getDefaultInstance(props, this);
    }

    /* access modifiers changed from: protected */
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(this.user, this.password);
    }

    public synchronized void sendMail(String subject, String body, String sender, String recipients, ArrayList<File> files) throws Exception {
        try {
            MimeMessage message = new MimeMessage(this.session);
            new DataHandler(new ByteArrayDataSource(body.getBytes(), "text/plain"));
            message.setSender(new InternetAddress(sender));
            message.setSubject(subject);
            MimeMultipart allMultipart = new MimeMultipart("mixed");
            for (int i = 0; i < files.size(); i++) {
                MimeBodyPart attachPart = new MimeBodyPart();
                FileDataSource fds = new FileDataSource(files.get(i));
                attachPart.setDataHandler(new DataHandler(fds));
                attachPart.setFileName(fds.getName());
                allMultipart.addBodyPart(attachPart);
            }
            message.setContent(allMultipart);
            if (recipients.indexOf(44) > 0) {
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
            } else {
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
            }
            Transport.send(message);
        } catch (Exception e) {
            System.out.println("异常.............." + e);
        }
        return;
    }

    public class ByteArrayDataSource implements DataSource {
        private byte[] data;
        private String type;

        public ByteArrayDataSource(byte[] data2, String type2) {
            this.data = data2;
            this.type = type2;
        }

        public ByteArrayDataSource(byte[] data2) {
            this.data = data2;
        }

        public void setType(String type2) {
            this.type = type2;
        }

        public String getContentType() {
            if (this.type == null) {
                return FilePart.DEFAULT_CONTENT_TYPE;
            }
            return this.type;
        }

        public InputStream getInputStream() throws IOException {
            return new ByteArrayInputStream(this.data);
        }

        public String getName() {
            return "ByteArrayDataSource";
        }

        public OutputStream getOutputStream() throws IOException {
            throw new IOException("Not Supported");
        }
    }
}
