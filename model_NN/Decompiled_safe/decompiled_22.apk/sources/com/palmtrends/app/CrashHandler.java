package com.palmtrends.app;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import android.util.Log;
import com.city_life.part_asynctask.UploadUtils;
import com.example.palmtrends_utils.R;
import com.utils.FormFile;
import com.utils.HttpUtil;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.TreeSet;

public class CrashHandler implements Thread.UncaughtExceptionHandler {
    private static final String APP_NAME = "app_name";
    private static final String CRASH_REPORTER_EXTENSION = ".properties";
    public static final boolean DEBUG = true;
    private static CrashHandler INSTANCE = null;
    private static final String STACK_TRACE = "STACK_TRACE";
    public static final String TAG = "CrashHandler";
    private static final String VERSION_CODE = "versionCode";
    private static final String VERSION_NAME = "versionName";
    public static Activity test;
    String e_msg;
    private Context mContext;
    private Thread.UncaughtExceptionHandler mDefaultHandler;
    private Properties mDeviceCrashInfo = new Properties();

    private CrashHandler() {
    }

    public static CrashHandler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CrashHandler();
        }
        return INSTANCE;
    }

    public void init(Context ctx) {
        this.mContext = ctx;
        this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public void uncaughtException(Thread thread, Throwable ex) {
        if (handleException(ex) || this.mDefaultHandler == null) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                Log.e(TAG, "Error : ", e);
            }
            Process.killProcess(Process.myPid());
            System.exit(10);
            return;
        }
        this.mDefaultHandler.uncaughtException(thread, ex);
    }

    private boolean handleException(Throwable ex) {
        if (ex != null) {
            collectCrashDeviceInfo(this.mContext);
            String saveCrashInfoToFile = saveCrashInfoToFile(ex);
            sendCrashReportsToServer(this.mContext);
        }
        return true;
    }

    public void sendPreviousReportsToServer() {
        sendCrashReportsToServer(this.mContext);
    }

    private void sendCrashReportsToServer(Context ctx) {
        String[] crFiles = getCrashReportFiles(ctx);
        if (crFiles != null && crFiles.length > 0) {
            TreeSet<String> sortedFiles = new TreeSet<>();
            sortedFiles.addAll(Arrays.asList(crFiles));
            FormFile[] files = new FormFile[crFiles.length];
            int i = 0;
            ArrayList<File> imageUris = new ArrayList<>();
            Iterator it = sortedFiles.iterator();
            while (it.hasNext()) {
                File cr = new File(ctx.getFilesDir(), (String) it.next());
                imageUris.add(cr);
                try {
                    files[i] = postReport(cr);
                    i++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            new HashMap();
            new HttpUtil();
        }
    }

    private FormFile postReport(File file) throws Exception {
        String iamgename = String.valueOf(file.getAbsolutePath()) + "_error.properties";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileInputStream fis = new FileInputStream(file);
        byte[] b = new byte[1024];
        int i = fis.read(b);
        while (i != -1) {
            baos.write(b);
            i = fis.read(b);
            baos.flush();
        }
        return new FormFile(iamgename, baos.toByteArray(), null, "application/txt");
    }

    private String[] getCrashReportFiles(Context ctx) {
        return ctx.getFilesDir().list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(CrashHandler.CRASH_REPORTER_EXTENSION);
            }
        });
    }

    private String saveCrashInfoToFile(Throwable ex) {
        Writer info = new StringWriter();
        PrintWriter printWriter = new PrintWriter(info);
        ex.printStackTrace(printWriter);
        for (Throwable cause = ex.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        String result = info.toString();
        printWriter.close();
        this.mDeviceCrashInfo.setProperty(STACK_TRACE, result);
        try {
            String timestamp = new StringBuilder(String.valueOf(System.currentTimeMillis())).toString();
            PackageInfo pi = ShareApplication.share.getPackageManager().getPackageInfo(ShareApplication.share.getPackageName(), 1);
            String fileName = "crash_" + pi.packageName + "_" + pi.versionName + "_" + timestamp + CRASH_REPORTER_EXTENSION;
            FileOutputStream trace = this.mContext.openFileOutput(fileName, 0);
            this.mDeviceCrashInfo.store(trace, UploadUtils.FAILURE);
            trace.flush();
            trace.close();
            return fileName;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "an error occured while writing report file...", e);
            return null;
        }
    }

    public void collectCrashDeviceInfo(Context ctx) {
        try {
            PackageInfo pi = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 1);
            if (pi != null) {
                this.mDeviceCrashInfo.put(VERSION_NAME, pi.versionName == null ? "not set" : pi.versionName);
                this.mDeviceCrashInfo.put(VERSION_CODE, new StringBuilder(String.valueOf(pi.versionCode)).toString());
                this.mDeviceCrashInfo.put(APP_NAME, new StringBuilder(String.valueOf(ShareApplication.share.getResources().getString(R.string.app_name))).toString());
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Error while collect package info", e);
        }
        for (Field field : Build.class.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                this.mDeviceCrashInfo.put(field.getName(), new StringBuilder().append(field.get(null)).toString());
                Log.d(TAG, String.valueOf(field.getName()) + " : " + field.get(null));
            } catch (Exception e2) {
                Log.e(TAG, "Error while collect crash info", e2);
            }
        }
        this.mDeviceCrashInfo.put("phonetype", Build.MODEL);
        this.mDeviceCrashInfo.put("sdk", Build.VERSION.SDK);
    }
}
