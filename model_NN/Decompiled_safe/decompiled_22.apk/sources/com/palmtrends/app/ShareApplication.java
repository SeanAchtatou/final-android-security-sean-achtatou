package com.palmtrends.app;

import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Handler;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.ImageFetcher;
import com.palmtrends.loadimage.Utils;
import java.util.List;

public class ShareApplication extends Application {
    public static final String IMAGE_CACHE_DIR = "images";
    public static boolean debug = false;
    public static Handler hander_other;
    public static List<Listitem> items;
    public static ImageFetcher mImageWorker;
    public static ShareApplication share;

    public void onCreate() {
        super.onCreate();
        share = this;
        hander_other = new Handler();
        Utils.h = new Handler();
    }

    public static String getVersion() {
        try {
            return share.getPackageManager().getPackageInfo(share.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return ClientShowAd.adv;
        }
    }
}
