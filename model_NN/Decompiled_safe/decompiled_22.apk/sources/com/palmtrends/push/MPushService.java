package com.palmtrends.push;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.util.Log;
import com.ibm.mqtt.IMqttClient;
import com.ibm.mqtt.MqttClient;
import com.ibm.mqtt.MqttException;
import com.ibm.mqtt.MqttPersistence;
import com.ibm.mqtt.MqttPersistenceException;
import com.ibm.mqtt.MqttSimpleCallback;
import com.palmtrends.dao.R;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class MPushService extends Service {
    public static final String ACTION = "com.palmtrends.push.server";
    private static final String ACTION_KEEPALIVE = (String.valueOf(MQTT_CLIENT_ID) + ".KEEP_ALIVE");
    private static final String ACTION_RECONNECT = (String.valueOf(MQTT_CLIENT_ID) + ".RECONNECT");
    private static final String ACTION_START = (String.valueOf(MQTT_CLIENT_ID) + ".START");
    private static final String ACTION_STOP = (String.valueOf(MQTT_CLIENT_ID) + ".STOP");
    private static final long INITIAL_RETRY_INTERVAL = 10000;
    private static final long KEEP_ALIVE_INTERVAL = 1680000;
    private static final long MAXIMUM_RETRY_INTERVAL = 1800000;
    /* access modifiers changed from: private */
    public static int MQTT_BROKER_PORT_NUM = 1883;
    /* access modifiers changed from: private */
    public static boolean MQTT_CLEAN_START = true;
    public static String MQTT_CLIENT_ID = "tokudu";
    private static final String MQTT_HOST = "111.10.24.74";
    /* access modifiers changed from: private */
    public static short MQTT_KEEP_ALIVE = 900;
    /* access modifiers changed from: private */
    public static MqttPersistence MQTT_PERSISTENCE = null;
    /* access modifiers changed from: private */
    public static int[] MQTT_QUALITIES_OF_SERVICE = new int[1];
    /* access modifiers changed from: private */
    public static int MQTT_QUALITY_OF_SERVICE = 0;
    /* access modifiers changed from: private */
    public static boolean MQTT_RETAINED_PUBLISH = false;
    private static final int NOTIF_CONNECTED = 0;
    public static String NOTIF_TITLE = "Tokudu";
    public static final String PREF_DEVICE_ID = "deviceID";
    public static final String PREF_RETRY = "retryInterval";
    public static final String PREF_STARTED = "isStarted";
    public static final String TAG = "PalmtrendsPushService";
    private ConnectivityManager mConnMan;
    /* access modifiers changed from: private */
    public MQTTConnection mConnection;
    private BroadcastReceiver mConnectivityChanged = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean hasConnectivity;
            NetworkInfo info = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            if (info == null || !info.isConnected()) {
                hasConnectivity = false;
            } else {
                hasConnectivity = true;
            }
            MPushService.this.log("Connectivity changed: connected=" + hasConnectivity);
            if (hasConnectivity) {
                MPushService.this.reconnectIfNecessary();
            } else if (MPushService.this.mConnection != null) {
                MPushService.this.mConnection.disconnect();
                MPushService.this.cancelReconnect();
                MPushService.this.mConnection = null;
            }
        }
    };
    private ConnectionLog mLog;
    private NotificationManager mNotifMan;
    /* access modifiers changed from: private */
    public long mStartTime;
    private boolean mStarted;

    public static void actionStart(Context ctx) {
    }

    public static void actionStop(Context ctx) {
        Intent i = new Intent(ctx, MPushService.class);
        i.setAction(ACTION_STOP);
        ctx.startService(i);
    }

    public static void actionPing(Context ctx) {
        Intent i = new Intent(ctx, MPushService.class);
        i.setAction(ACTION_KEEPALIVE);
        ctx.startService(i);
    }

    public void onCreate() {
        super.onCreate();
        log("Creating service");
        this.mStartTime = System.currentTimeMillis();
        try {
            this.mLog = new ConnectionLog();
            Log.i(TAG, "Opened log at " + this.mLog.getPath());
        } catch (IOException e) {
            Log.e(TAG, "Failed to open log", e);
        }
        this.mConnMan = (ConnectivityManager) getSystemService("connectivity");
        this.mNotifMan = (NotificationManager) getSystemService("notification");
        handleCrashedService();
    }

    private void handleCrashedService() {
        if (wasStarted()) {
            log("Handling crashed service...");
            stopKeepAlives();
            start();
        }
    }

    public void onDestroy() {
        log("Service destroyed (started=" + this.mStarted + ")");
        actionStart(getApplicationContext());
        if (this.mStarted) {
            stop();
        }
        try {
            if (this.mLog != null) {
                this.mLog.close();
            }
        } catch (IOException e) {
        }
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        log("Service started with intent=" + intent);
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals(ACTION_STOP)) {
                stop();
                stopSelf();
            } else if (intent.getAction().equals(ACTION_START)) {
                start();
            } else if (intent.getAction().equals(ACTION_KEEPALIVE)) {
                keepAlive();
            } else if (intent.getAction().equals(ACTION_RECONNECT) && isNetworkAvailable()) {
                reconnectIfNecessary();
            }
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    /* access modifiers changed from: private */
    public void log(String message) {
        log(message, null);
    }

    /* access modifiers changed from: private */
    public void log(String message, Throwable e) {
        if (e != null) {
            Log.e(TAG, message, e);
        } else {
            Log.i(TAG, message);
        }
        if (this.mLog != null) {
            try {
                this.mLog.println(message);
            } catch (IOException e2) {
            }
        }
    }

    private boolean wasStarted() {
        return PerfHelper.getBooleanData(PREF_STARTED);
    }

    /* access modifiers changed from: private */
    public void setStarted(boolean started) {
        PerfHelper.setInfo(PREF_STARTED, started);
        this.mStarted = started;
    }

    private synchronized void start() {
        log("Starting service...");
        if (this.mStarted) {
            Log.w(TAG, "Attempt to start connection that is already active");
        } else {
            connect();
            registerReceiver(this.mConnectivityChanged, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    private synchronized void stop() {
        if (!this.mStarted) {
            Log.w(TAG, "Attempt to stop connection not active.");
        } else {
            setStarted(false);
            unregisterReceiver(this.mConnectivityChanged);
            cancelReconnect();
            if (this.mConnection != null) {
                this.mConnection.disconnect();
                this.mConnection = null;
            }
        }
    }

    private synchronized void connect() {
        log("Connecting...");
        final String deviceID = PerfHelper.getStringData(PREF_DEVICE_ID);
        if (deviceID == null) {
            log("Device ID not found.");
        } else {
            new Thread() {
                public void run() {
                    String str;
                    try {
                        MPushService.this.mConnection = new MQTTConnection(MPushService.MQTT_HOST, deviceID);
                    } catch (MqttException e) {
                        e.printStackTrace();
                        MPushService mPushService = MPushService.this;
                        StringBuilder sb = new StringBuilder("MqttException: ");
                        if (e.getMessage() != null) {
                            str = e.getMessage();
                        } else {
                            str = "NULL";
                        }
                        mPushService.log(sb.append(str).toString());
                        if (MPushService.this.isNetworkAvailable()) {
                            MPushService.this.scheduleReconnect(MPushService.this.mStartTime);
                        }
                    }
                    MPushService.this.setStarted(true);
                }
            }.start();
        }
    }

    private synchronized void keepAlive() {
        try {
            if (this.mStarted && this.mConnection != null) {
                this.mConnection.sendKeepAlive();
            }
        } catch (MqttException e) {
            log("MqttException: " + (e.getMessage() != null ? e.getMessage() : "NULL"), e);
            this.mConnection.disconnect();
            this.mConnection = null;
            cancelReconnect();
        }
    }

    /* access modifiers changed from: private */
    public void startKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, MPushService.class);
        i.setAction(ACTION_KEEPALIVE);
        ((AlarmManager) getSystemService("alarm")).setRepeating(0, System.currentTimeMillis() + KEEP_ALIVE_INTERVAL, KEEP_ALIVE_INTERVAL, PendingIntent.getService(this, 0, i, 0));
    }

    /* access modifiers changed from: private */
    public void stopKeepAlives() {
        Intent i = new Intent();
        i.setClass(this, MPushService.class);
        i.setAction(ACTION_KEEPALIVE);
        ((AlarmManager) getSystemService("alarm")).cancel(PendingIntent.getService(this, 0, i, 0));
    }

    public void scheduleReconnect(long startTime) {
        long interval;
        long interval2 = PerfHelper.getLongData(PREF_RETRY);
        if (interval2 == 0) {
            interval2 = INITIAL_RETRY_INTERVAL;
        }
        long now = System.currentTimeMillis();
        if (now - startTime < interval2) {
            interval = Math.min(4 * interval2, (long) MAXIMUM_RETRY_INTERVAL);
        } else {
            interval = INITIAL_RETRY_INTERVAL;
        }
        log("Rescheduling connection in " + interval + "ms.");
        PerfHelper.setInfo(PREF_RETRY, interval);
        Intent i = new Intent();
        i.setClass(this, MPushService.class);
        i.setAction(ACTION_RECONNECT);
        ((AlarmManager) getSystemService("alarm")).set(0, now + interval, PendingIntent.getService(this, 0, i, 0));
    }

    public void cancelReconnect() {
        Intent i = new Intent();
        i.setClass(this, MPushService.class);
        i.setAction(ACTION_RECONNECT);
        ((AlarmManager) getSystemService("alarm")).cancel(PendingIntent.getService(this, 0, i, 0));
    }

    /* access modifiers changed from: private */
    public synchronized void reconnectIfNecessary() {
        if (this.mStarted && this.mConnection == null) {
            log("Reconnecting...");
            connect();
        }
    }

    /* access modifiers changed from: private */
    public void showNotification(String text) {
        Intent intent1 = new Intent(getResources().getString(R.string.push_receiver));
        intent1.putExtra("pushdata", text);
        sendBroadcast(intent1);
    }

    /* access modifiers changed from: private */
    public boolean isNetworkAvailable() {
        NetworkInfo info = this.mConnMan.getActiveNetworkInfo();
        if (info == null) {
            return false;
        }
        return info.isConnected();
    }

    private class MQTTConnection implements MqttSimpleCallback {
        IMqttClient mqttClient = null;

        public MQTTConnection(String brokerHostName, String initTopic) throws MqttException {
            this.mqttClient = MqttClient.createMqttClient(IMqttClient.TCP_ID + brokerHostName + "@" + MPushService.MQTT_BROKER_PORT_NUM, MPushService.MQTT_PERSISTENCE);
            this.mqttClient.connect(String.valueOf(FinalVariable.pid) + CookieSpec.PATH_DELIM + PerfHelper.getStringData(MPushService.PREF_DEVICE_ID), MPushService.MQTT_CLEAN_START, MPushService.MQTT_KEEP_ALIVE);
            this.mqttClient.registerSimpleHandler(this);
            String initTopic2 = String.valueOf(FinalVariable.pid) + CookieSpec.PATH_DELIM + PerfHelper.getStringData(PerfHelper.P_APP_ID);
            subscribeToTopic(initTopic2);
            MPushService.this.log("Connection established to " + brokerHostName + " on topic " + initTopic2);
            MPushService.this.mStartTime = System.currentTimeMillis();
            MPushService.this.startKeepAlives();
        }

        public void disconnect() {
            try {
                MPushService.this.stopKeepAlives();
                this.mqttClient.disconnect();
            } catch (MqttPersistenceException e) {
                MPushService.this.log("MqttException" + (e.getMessage() != null ? e.getMessage() : " NULL"), e);
            }
        }

        private void subscribeToTopic(String topicName) throws MqttException {
            if (this.mqttClient == null || !this.mqttClient.isConnected()) {
                MPushService.this.log("Connection errorNo connection");
                return;
            }
            String str = String.valueOf(FinalVariable.pid) + CookieSpec.PATH_DELIM + PerfHelper.getStringData(MPushService.PREF_DEVICE_ID);
            this.mqttClient.subscribe(new String[]{topicName}, MPushService.MQTT_QUALITIES_OF_SERVICE);
        }

        private String[] getTopics() {
            List<PackageInfo> apps = new ArrayList<>();
            List<PackageInfo> paklist = MPushService.this.getPackageManager().getInstalledPackages(0);
            String topics = "";
            for (int i = 0; i < paklist.size(); i++) {
                PackageInfo pak = paklist.get(i);
                int i2 = pak.applicationInfo.flags;
                ApplicationInfo applicationInfo = pak.applicationInfo;
                if ((i2 & 1) <= 0 && pak.packageName.startsWith("com.palmtrends.")) {
                    apps.add(pak);
                    topics = String.valueOf(topics) + pak.packageName + "]";
                }
            }
            return topics.substring(0, topics.length() - 1).split("]");
        }

        private void publishToTopic(String topicName, String message) throws MqttException {
            if (this.mqttClient == null || !this.mqttClient.isConnected()) {
                MPushService.this.log("No connection to public to");
            } else {
                this.mqttClient.publish(topicName, message.getBytes(), MPushService.MQTT_QUALITY_OF_SERVICE, MPushService.MQTT_RETAINED_PUBLISH);
            }
        }

        public void connectionLost() throws Exception {
            MPushService.this.log("Loss of connectionconnection downed");
            MPushService.this.stopKeepAlives();
            MPushService.this.mConnection = null;
        }

        public void publishArrived(String topicName, byte[] payload, int qos, boolean retained) {
            String s = new String(payload);
            MPushService.this.showNotification(s);
            MPushService.this.log("Got message: " + s);
        }

        public void sendKeepAlive() throws MqttException {
            MPushService.this.log("Sending keep alive");
            publishToTopic(String.valueOf(MPushService.MQTT_CLIENT_ID) + "/keepalive", PerfHelper.getStringData(MPushService.PREF_DEVICE_ID));
        }
    }
}
