package com.palmtrends.push;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.R;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.tencent.tauth.Constants;
import com.utils.FinalVariable;
import com.utils.JniUtils;
import com.utils.PerfHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.SAXParserFactory;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class PushReceiver extends BroadcastReceiver {
    public static final int TO_DO_TASK = 10001111;
    public static NotificationManager m_NotificationManager = null;
    public static final String taskName = "AbsMainActivity";
    public Context con;
    Handler handler = new Handler();

    public void onReceive(Context context, Intent intent) {
        this.con = context;
        DBHelper.getDBHelper();
        PerfHelper.getPerferences(this.con);
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            MPushService.actionStart(context);
            PushService.push(context);
        } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if (Utils.isNetworkAvailable(context) && PerfHelper.getBooleanData(PerfHelper.P_PUSH)) {
                new CheckUpdate().start();
            }
        } else if ("com.palmtrends.push.push_receiver".equals(intent.getAction())) {
            if (PerfHelper.getBooleanData(PerfHelper.P_PUSH)) {
                try {
                    JSONObject json = new JSONObject(intent.getStringExtra("pushdata"));
                    if (FinalVariable.pid.equals(json.getString("pid"))) {
                        pushinfo p = new pushinfo();
                        p.info = json.getString(Constants.PARAM_SEND_MSG);
                        p.nid = json.getString("aid");
                        p.pushdate = json.getString("pushdate");
                        sendMessage(p);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (intent.getAction() == null && Utils.isNetworkAvailable(context) && PerfHelper.getBooleanData(PerfHelper.P_PUSH)) {
            new CheckUpdate().start();
        }
    }

    class CheckUpdate extends Thread {
        CheckUpdate() {
        }

        public void run() {
            String update;
            String lastdate = PerfHelper.getStringData("lastupdate");
            if ("".equals(lastdate)) {
                update = new StringBuilder(String.valueOf(new Date().getTime())).toString();
            } else {
                update = lastdate;
            }
            String url = "http://push.cms.palmtrends.com/api/getdevid.php?sid=" + FinalVariable.pid + "&devid=" + Settings.Secure.getString(PushReceiver.this.con.getContentResolver(), "android_id") + "&cid=3&push=1&e=" + JniUtils.getkey() + "&lastupdate=" + update;
            XmlHandler h = new XmlHandler();
            try {
                SAXParserFactory.newInstance().newSAXParser().parse(url, h);
                PushReceiver.this.sendMessage(h.pi);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessage(final pushinfo pi) throws Exception {
        DBHelper.getDBHelper().insertObject(pi, "push");
        this.handler.post(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.palmtrends.push.PushReceiver.setNotiActivity(java.lang.String, int, int, java.lang.String, java.lang.String, android.content.Intent, android.content.Context, boolean):void
             arg types: [java.lang.String, ?, int, java.lang.String, java.lang.String, android.content.Intent, android.content.Context, int]
             candidates:
              com.palmtrends.push.PushReceiver.setNotiActivity(java.lang.String, int, int, java.lang.String, java.lang.String, java.lang.Class<? extends android.app.Activity>, android.content.Context, boolean):void
              com.palmtrends.push.PushReceiver.setNotiActivity(java.lang.String, int, int, java.lang.String, java.lang.String, android.content.Intent, android.content.Context, boolean):void */
            public void run() {
                Intent intent;
                if (pi.info != null && !"".equals(pi.info.trim())) {
                    Listitem li = new Listitem();
                    li.nid = pi.nid;
                    li.title = pi.info;
                    li.getMark();
                    if (li.nid == null || "".equals(li.nid) || UploadUtils.SUCCESS.equals(li.nid)) {
                        intent = new Intent(PushReceiver.this.con.getResources().getString(R.string.activity_init));
                    } else {
                        intent = new Intent(PushReceiver.this.con.getResources().getString(R.string.activity_article));
                        intent.setFlags(268435456);
                        List<Listitem> lis = new ArrayList<>();
                        lis.add(li);
                        ShareApplication.items = lis;
                        intent.putExtra("items", (Serializable) lis);
                    }
                    intent.setFlags(268435456);
                    if (PushReceiver.m_NotificationManager == null) {
                        PushReceiver.m_NotificationManager = (NotificationManager) PushReceiver.this.con.getSystemService("notification");
                    }
                    PushReceiver.setNotiActivity(PushReceiver.taskName, (int) PushReceiver.TO_DO_TASK, R.drawable.ic_push, PushReceiver.this.con.getResources().getString(R.string.app_name), pi.info, intent, PushReceiver.this.con, false);
                }
            }
        });
    }

    public static void setNotiActivity(String str, int nID, int iconId, String info, String content, Class<? extends Activity> activity, Context context, boolean sound) {
        Intent notifyIntent = new Intent(context, activity);
        notifyIntent.setFlags(536870912);
        setNotiActivity(str, nID, iconId, info, content, notifyIntent, context, sound);
    }

    public static void setNotiActivity(String str, int nID, int iconId, String info, String content, Intent intent, Context context, boolean sound) {
        setNotiType(str, nID, iconId, info, content, PendingIntent.getActivity(context, 0, intent, 134217728), context, sound);
    }

    public static void setNotiType(String str, int nID, int iconId, String info, String content, PendingIntent appIntent, Context context, boolean sound) {
        Notification myNoti = new Notification();
        myNoti.icon = iconId;
        myNoti.tickerText = content;
        myNoti.defaults = 1;
        myNoti.flags = 16;
        myNoti.setLatestEventInfo(context, info, content, appIntent);
        m_NotificationManager.notify(TO_DO_TASK, myNoti);
    }

    class XmlHandler extends DefaultHandler {
        pushinfo pi = new pushinfo();
        String type;

        XmlHandler() {
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            this.type = localName;
            if ("lastupdate".equals(localName)) {
                this.pi.nid = attributes.getValue("aid");
                this.pi.pushdate = attributes.getValue("datetime");
                PerfHelper.setInfo("lastupdate", this.pi.pushdate);
            }
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            String temp;
            if (Constants.PARAM_SEND_MSG.equals(this.type) && ch != null && length > 0 && (temp = new String(ch, start, length)) != null && !"".equals(temp.trim())) {
                this.pi.info = temp;
            }
        }
    }
}
