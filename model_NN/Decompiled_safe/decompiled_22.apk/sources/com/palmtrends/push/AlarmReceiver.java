package com.palmtrends.push;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.palmtrends.dao.R;

public class AlarmReceiver extends BroadcastReceiver {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.palmtrends.alarm.action".equals(action)) {
            if (!AlarmTools.isMax() && AlarmTools.isAlert()) {
                Notification notification = new Notification();
                notification.icon = R.drawable.ic_push;
                notification.tickerText = String.valueOf(context.getResources().getString(R.string.app_name)) + "定时提醒";
                notification.defaults = 1;
                notification.flags = 16;
                Intent i = new Intent(context.getResources().getString(R.string.activity_init));
                i.setFlags(268435456);
                i.putExtra("AlarmReceiver", true);
                notification.setLatestEventInfo(context, String.valueOf(context.getResources().getString(R.string.app_name)) + "定时提醒", context.getResources().getString(R.string.alarm_des), PendingIntent.getActivity(context, 0, i, 0));
                ((NotificationManager) context.getSystemService("notification")).notify(R.string.app_name, notification);
            }
        } else if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            if (AlarmTools.isAlert()) {
                AlarmTools.setAlarmTime(context, false);
            } else {
                AlarmTools.setAlarmTime(context, true);
            }
        } else if (!action.equals("android.intent.action.TIME_SET")) {
        } else {
            if (AlarmTools.isAlert()) {
                AlarmTools.setAlarmTime(context, false);
            } else {
                AlarmTools.setAlarmTime(context, true);
            }
        }
    }
}
