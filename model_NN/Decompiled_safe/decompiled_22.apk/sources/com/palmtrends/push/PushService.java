package com.palmtrends.push;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import java.util.Calendar;

public class PushService {
    public static void push(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, PushReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(13, 1);
        ((AlarmManager) context.getSystemService("alarm")).setRepeating(2, SystemClock.elapsedRealtime(), 900000, sender);
    }
}
