package com.palmtrends.push;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.utils.PerfHelper;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class AlarmTools {
    public static String getAlarmDate() {
        String time;
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        Date beginDate = new Date();
        Calendar date = Calendar.getInstance();
        date.setTime(beginDate);
        date.set(5, date.get(5) + 7);
        Date endDate = beginDate;
        try {
            endDate = dft.parse(dft.format(date.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int i = new Random().nextInt(13);
        if (i == 0 || i == 1) {
            time = " 21:0" + (i * 5) + ":00";
        } else if (i == 12) {
            time = " 22:00:00";
        } else {
            time = " 21:" + (i * 5) + ":00";
        }
        Log.i("xl", "提醒时间" + time);
        PerfHelper.setInfo("Alarmalert", String.valueOf(dft.format(new Date())) + time);
        PerfHelper.setInfo("notify", String.valueOf(dft.format(new Date())) + time);
        return String.valueOf(dft.format(endDate)) + time;
    }

    public static boolean isMax() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String time = PerfHelper.getStringData("Alarmalert");
        String notifiy = PerfHelper.getStringData(PerfHelper.P_ALARM_TIME);
        if (time == null || "".equals(time)) {
            return true;
        }
        String times = time.substring(10);
        String time2 = time.substring(0, 16);
        if (notifiy == null || "".equals(notifiy)) {
            return true;
        }
        String notifiy2 = notifiy.substring(0, 16);
        String systemTime = sdf.format(new Date()).toString();
        Date end = null;
        Date begin = null;
        Date noti = null;
        try {
            end = sdf.parse(time2);
            begin = sdf.parse(systemTime);
            noti = sdf.parse(notifiy2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (begin.getTime() > end.getTime()) {
            PerfHelper.setInfo("Alarmalert", String.valueOf(systemTime.split(" ")[0]) + times);
            return true;
        } else if (begin.getTime() < noti.getTime() || begin.getTime() >= end.getTime()) {
            return false;
        } else {
            PerfHelper.setInfo("Alarmalert", String.valueOf(systemTime.split(" ")[0]) + times);
            return false;
        }
    }

    public static void setAlarmTime(Context context, boolean isSet) {
        AlarmManager am = (AlarmManager) context.getSystemService("alarm");
        Intent intent = new Intent("com.palmtrends.alarm.action");
        intent.setClass(context, AlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 268435456);
        long millisecond = getTimeDifference(isSet);
        if (millisecond < 0) {
            millisecond += (long) 604800000;
        }
        am.setRepeating(0, System.currentTimeMillis() + millisecond, (long) 604800000, sender);
    }

    public static boolean isAlert() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String notifiy = PerfHelper.getStringData(PerfHelper.P_ALARM_TIME);
        if (notifiy == null || "".equals(notifiy)) {
            return true;
        }
        String notifiy2 = notifiy.substring(0, 16);
        Date begin = null;
        Date noti = null;
        try {
            begin = sdf.parse(sdf.format(new Date()).toString());
            noti = sdf.parse(notifiy2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (begin.getTime() < noti.getTime()) {
            PerfHelper.setInfo("Alarmalert", PerfHelper.getStringData("notify"));
        }
        if (getTimeDifference(false) % 604800000 == 0) {
            return true;
        }
        return false;
    }

    public static long getTimeDifference(boolean isSet) {
        String gs = "yyyy-MM-dd HH:mm";
        String time = PerfHelper.getStringData(PerfHelper.P_ALARM_TIME);
        if (time == null || "".equals(time)) {
            return 1992;
        }
        String time2 = time.substring(0, 16);
        if (isSet) {
            gs = "yyyy-MM-dd HH:mm:ss";
            time2 = String.valueOf(time2) + ":00";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(gs);
        String systemTime = sdf.format(new Date()).toString();
        Date end = null;
        Date begin = null;
        try {
            end = sdf.parse(time2);
            begin = sdf.parse(systemTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return end.getTime() - begin.getTime();
    }
}
