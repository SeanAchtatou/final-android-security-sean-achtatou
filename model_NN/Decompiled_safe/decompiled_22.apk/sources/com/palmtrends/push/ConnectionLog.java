package com.palmtrends.push;

import android.os.Environment;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConnectionLog {
    private static final SimpleDateFormat TIMESTAMP_FMT = new SimpleDateFormat("[HH:mm:ss] ");
    private String mPath;
    private Writer mWriter;

    public ConnectionLog() throws IOException {
        File logDir = new File(Environment.getExternalStorageDirectory(), "tokudu/log/");
        if (!logDir.exists()) {
            logDir.mkdirs();
            new File(logDir, ".nomedia").createNewFile();
        }
        open(String.valueOf(logDir.getAbsolutePath()) + "/push.log");
    }

    public ConnectionLog(String basePath) throws IOException {
        open(basePath);
    }

    /* access modifiers changed from: protected */
    public void open(String basePath) throws IOException {
        this.mPath = new File(String.valueOf(basePath) + "-" + getTodayString()).getAbsolutePath();
        this.mWriter = new BufferedWriter(new FileWriter(this.mPath), 2048);
        println("Opened log.");
    }

    private static String getTodayString() {
        return new SimpleDateFormat("yyyyMMdd-hhmmss").format(new Date());
    }

    public String getPath() {
        return this.mPath;
    }

    public void println(String message) throws IOException {
        this.mWriter.write(TIMESTAMP_FMT.format(new Date()));
        this.mWriter.write(message);
        this.mWriter.write(10);
        this.mWriter.flush();
    }

    public void close() throws IOException {
        this.mWriter.close();
    }
}
