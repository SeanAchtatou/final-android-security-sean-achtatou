package com.palmtrends.loadimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.palmtrends.app.ShareApplication;
import java.io.File;

public class ImageFetcher extends ImageResizer {
    public static final String HTTP_CACHE_DIR = "http";
    private static final int HTTP_CACHE_SIZE = 10485760;
    private static final String TAG = "ImageFetcher";

    public ImageFetcher(Context context, int imageWidth, int imageHeight) {
        super(context, imageWidth, imageHeight);
        init(context);
    }

    public ImageFetcher(Context context, int imageSize) {
        super(context, imageSize);
        init(context);
    }

    private void init(Context context) {
        checkConnection(context);
    }

    private void checkConnection(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            networkInfo.isConnectedOrConnecting();
        }
    }

    private Bitmap processBitmap(String data) {
        if (ShareApplication.debug) {
            Log.d(TAG, "processBitmap - " + data);
        }
        File f = downloadBitmap(this.mContext, data);
        if (f != null) {
            return decodeSampledBitmapFromFile(f.toString(), this.mImageWidth, this.mImageHeight);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Bitmap processBitmap(Object data) {
        return processBitmap(String.valueOf(data));
    }

    /* JADX WARN: Type inference failed for: r11v9, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00f2 A[SYNTHETIC, Splitter:B:41:0x00f2] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File downloadBitmap(android.content.Context r15, java.lang.String r16) {
        /*
            java.lang.String r11 = "http"
            java.io.File r3 = com.palmtrends.loadimage.DiskLruCache.getDiskCacheDir(r15, r11)
            r11 = 10485760(0xa00000, double:5.180654E-317)
            com.palmtrends.loadimage.DiskLruCache r2 = com.palmtrends.loadimage.DiskLruCache.openCache(r15, r3, r11)
            java.io.File r4 = new java.io.File
            r0 = r16
            java.lang.String r11 = r2.createFilePath(r0)
            r4.<init>(r11)
            r0 = r16
            boolean r11 = r2.containsKey(r0)
            if (r11 == 0) goto L_0x003b
            boolean r11 = com.palmtrends.app.ShareApplication.debug
            if (r11 == 0) goto L_0x003a
            java.lang.String r11 = "ImageFetcher"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r13 = "downloadBitmap - found in http cache - "
            r12.<init>(r13)
            r0 = r16
            java.lang.StringBuilder r12 = r12.append(r0)
            java.lang.String r12 = r12.toString()
            android.util.Log.d(r11, r12)
        L_0x003a:
            return r4
        L_0x003b:
            boolean r11 = com.palmtrends.app.ShareApplication.debug
            if (r11 == 0) goto L_0x0055
            java.lang.String r11 = "ImageFetcher"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r13 = "downloadBitmap - downloading - "
            r12.<init>(r13)
            r0 = r16
            java.lang.StringBuilder r12 = r12.append(r0)
            java.lang.String r12 = r12.toString()
            android.util.Log.d(r11, r12)
        L_0x0055:
            com.palmtrends.loadimage.Utils.disableConnectionReuseIfNecessary()
            r10 = 0
            r7 = 0
            java.net.URL r9 = new java.net.URL     // Catch:{ IOException -> 0x010f }
            r0 = r16
            r9.<init>(r0)     // Catch:{ IOException -> 0x010f }
            java.net.URLConnection r11 = r9.openConnection()     // Catch:{ IOException -> 0x010f }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x010f }
            r10 = r0
            r11 = 25000(0x61a8, float:3.5032E-41)
            r10.setReadTimeout(r11)     // Catch:{ IOException -> 0x010f }
            java.io.BufferedInputStream r6 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x010f }
            java.io.InputStream r11 = r10.getInputStream()     // Catch:{ IOException -> 0x010f }
            r12 = 8192(0x2000, float:1.14794E-41)
            r6.<init>(r11, r12)     // Catch:{ IOException -> 0x010f }
            java.io.BufferedOutputStream r8 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x010f }
            java.io.FileOutputStream r11 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x010f }
            r11.<init>(r4)     // Catch:{ IOException -> 0x010f }
            r12 = 8192(0x2000, float:1.14794E-41)
            r8.<init>(r11, r12)     // Catch:{ IOException -> 0x010f }
        L_0x0085:
            int r1 = r6.read()     // Catch:{ IOException -> 0x00b1, all -> 0x010c }
            r11 = -1
            if (r1 != r11) goto L_0x00ad
            if (r10 == 0) goto L_0x0091
            r10.disconnect()
        L_0x0091:
            if (r8 == 0) goto L_0x003a
            r8.close()     // Catch:{ IOException -> 0x0097 }
            goto L_0x003a
        L_0x0097:
            r5 = move-exception
            java.lang.String r11 = "ImageFetcher"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r13 = "Error in downloadBitmap - "
            r12.<init>(r13)
            java.lang.StringBuilder r12 = r12.append(r5)
            java.lang.String r12 = r12.toString()
            android.util.Log.e(r11, r12)
            goto L_0x003a
        L_0x00ad:
            r8.write(r1)     // Catch:{ IOException -> 0x00b1, all -> 0x010c }
            goto L_0x0085
        L_0x00b1:
            r5 = move-exception
            r7 = r8
        L_0x00b3:
            java.lang.String r11 = "ImageFetcher"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ea }
            java.lang.String r13 = "Error in downloadBitmap - "
            r12.<init>(r13)     // Catch:{ all -> 0x00ea }
            java.lang.StringBuilder r12 = r12.append(r5)     // Catch:{ all -> 0x00ea }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x00ea }
            android.util.Log.e(r11, r12)     // Catch:{ all -> 0x00ea }
            if (r10 == 0) goto L_0x00cc
            r10.disconnect()
        L_0x00cc:
            if (r7 == 0) goto L_0x00d1
            r7.close()     // Catch:{ IOException -> 0x00d4 }
        L_0x00d1:
            r4 = 0
            goto L_0x003a
        L_0x00d4:
            r5 = move-exception
            java.lang.String r11 = "ImageFetcher"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r13 = "Error in downloadBitmap - "
            r12.<init>(r13)
            java.lang.StringBuilder r12 = r12.append(r5)
            java.lang.String r12 = r12.toString()
            android.util.Log.e(r11, r12)
            goto L_0x00d1
        L_0x00ea:
            r11 = move-exception
        L_0x00eb:
            if (r10 == 0) goto L_0x00f0
            r10.disconnect()
        L_0x00f0:
            if (r7 == 0) goto L_0x00f5
            r7.close()     // Catch:{ IOException -> 0x00f6 }
        L_0x00f5:
            throw r11
        L_0x00f6:
            r5 = move-exception
            java.lang.String r12 = "ImageFetcher"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = "Error in downloadBitmap - "
            r13.<init>(r14)
            java.lang.StringBuilder r13 = r13.append(r5)
            java.lang.String r13 = r13.toString()
            android.util.Log.e(r12, r13)
            goto L_0x00f5
        L_0x010c:
            r11 = move-exception
            r7 = r8
            goto L_0x00eb
        L_0x010f:
            r5 = move-exception
            goto L_0x00b3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.palmtrends.loadimage.ImageFetcher.downloadBitmap(android.content.Context, java.lang.String):java.io.File");
    }
}
