package com.palmtrends.loadimage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class RetainFragment extends Fragment {
    private static final String TAG = "RetainFragment";
    private Object mObject;

    public static RetainFragment findOrCreateRetainFragment(FragmentManager fm) {
        RetainFragment mRetainFragment = (RetainFragment) fm.findFragmentByTag(TAG);
        if (mRetainFragment != null) {
            return mRetainFragment;
        }
        RetainFragment mRetainFragment2 = new RetainFragment();
        fm.beginTransaction().add(mRetainFragment2, TAG).commit();
        return mRetainFragment2;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setObject(Object object) {
        this.mObject = object;
    }

    public Object getObject() {
        return this.mObject;
    }
}
