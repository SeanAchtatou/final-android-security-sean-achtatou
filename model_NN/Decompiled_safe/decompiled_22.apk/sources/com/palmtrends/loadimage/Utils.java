package com.palmtrends.loadimage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.StatFs;
import android.view.KeyEvent;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.palmtrends.app.ShareApplication;
import com.utils.FileUtils;
import java.io.File;
import java.util.Calendar;
import org.apache.commons.httpclient.HttpState;

@SuppressLint({"NewApi"})
public class Utils {
    private static final int DATE_DIALOG = 0;
    public static final int IO_BUFFER_SIZE = 8192;
    public static Handler h;
    public static ProgressDialog mypDialog;

    private Utils() {
    }

    public static void disableConnectionReuseIfNecessary() {
        if (hasHttpConnectionBug()) {
            System.setProperty("http.keepAlive", HttpState.PREEMPTIVE_DEFAULT);
        }
    }

    public static int getBitmapSize(Bitmap bitmap) {
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    @SuppressLint({"NewApi"})
    public static boolean isExternalStorageRemovable() {
        return true;
    }

    public static File getExternalCacheDir(Context context) {
        return new File(String.valueOf(FileUtils.sdPath) + "/pic_cache/");
    }

    public static long getUsableSpace(File path) {
        StatFs stats = new StatFs(path.getPath());
        return ((long) stats.getBlockSize()) * ((long) stats.getAvailableBlocks());
    }

    public static int getMemoryClass(Context context) {
        return ((ActivityManager) context.getSystemService("activity")).getMemoryClass();
    }

    public static boolean hasHttpConnectionBug() {
        return Build.VERSION.SDK_INT < 8;
    }

    public static boolean hasExternalCacheDir() {
        return Build.VERSION.SDK_INT >= 8;
    }

    public static boolean hasActionBar() {
        return Build.VERSION.SDK_INT >= 11;
    }

    public static void showToast(final String info) {
        h.post(new Runnable() {
            public void run() {
                if (Utils.mypDialog != null) {
                    Utils.mypDialog.dismiss();
                }
                try {
                    Toast.makeText(ShareApplication.share, info, 2000).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void showToast(final int id) {
        h.post(new Runnable() {
            public void run() {
                if (Utils.mypDialog != null) {
                    Utils.mypDialog.dismiss();
                }
                try {
                    Toast.makeText(ShareApplication.share, ShareApplication.share.getResources().getString(id), 2000).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void showDialog(Context ac, String info, String title) {
        try {
            new AlertDialog.Builder(ac).setPositiveButton("取消", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }).setMessage(info).setTitle(title).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showTimeDialog(Activity ac, int type, final TextView tv) {
        Calendar calendar = Calendar.getInstance();
        if (type == 0) {
            new DatePickerDialog(ac, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                    tv.setText(String.valueOf(year) + "-" + (month + 1) + "-" + dayOfMonth);
                }
            }, calendar.get(1), calendar.get(2), calendar.get(5)).show();
            return;
        }
        new TimePickerDialog(ac, new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                tv.setText(String.valueOf(hourOfDay) + ":" + minute + ":00");
            }
        }, calendar.get(11), calendar.get(12), true).show();
    }

    public static void dismissProcessDialog() {
        h.post(new Runnable() {
            public void run() {
                if (Utils.mypDialog != null) {
                    Utils.mypDialog.dismiss();
                    Utils.mypDialog = null;
                }
            }
        });
    }

    public static ProgressDialog showProcessDialog(Context ac, String message) {
        mypDialog = new ProgressDialog(ac);
        mypDialog.setProgressStyle(0);
        mypDialog.setMessage(message);
        mypDialog.setCancelable(false);
        mypDialog.setIndeterminate(false);
        mypDialog.setCancelable(false);
        mypDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                Utils.h.post(new Runnable() {
                    public void run() {
                        Utils.mypDialog.dismiss();
                    }
                });
                return false;
            }
        });
        try {
            h.post(new Runnable() {
                public void run() {
                    Utils.mypDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mypDialog;
    }

    public static ProgressDialog showProcessDialog(Context ac, int id) {
        if (mypDialog != null) {
            try {
                mypDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mypDialog.setMessage(ac.getResources().getString(id));
            return mypDialog;
        }
        mypDialog = new ProgressDialog(ac);
        mypDialog.setProgressStyle(0);
        mypDialog.setMessage(ac.getResources().getString(id));
        mypDialog.setCancelable(false);
        mypDialog.setIndeterminate(false);
        mypDialog.setCancelable(false);
        mypDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                Utils.mypDialog.dismiss();
                Utils.mypDialog = null;
                return false;
            }
        });
        try {
            mypDialog.show();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return mypDialog;
    }

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo[] info;
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivity == null || (info = connectivity.getAllNetworkInfo()) == null) {
            return false;
        }
        for (NetworkInfo state : info) {
            if (state.getState() == NetworkInfo.State.CONNECTED) {
                return true;
            }
        }
        return false;
    }
}
