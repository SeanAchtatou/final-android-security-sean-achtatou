package com.palmtrends.loadimage;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import com.palmtrends.app.ShareApplication;
import java.lang.ref.WeakReference;

public abstract class ImageWorker {
    private static final int FADE_IN_TIME = 200;
    private static final String TAG = "ImageWorker";
    public DealImage dealImage = null;
    protected Context mContext;
    /* access modifiers changed from: private */
    public boolean mExitTasksEarly = false;
    private boolean mFadeInBitmap = true;
    public ImageCache mImageCache;
    protected ImageWorkerAdapter mImageWorkerAdapter;
    private Bitmap mLoadingBitmap;

    public interface DealImage {
        Bitmap deal(Bitmap bitmap, ImageView imageView);
    }

    public static abstract class ImageWorkerAdapter {
        public abstract Object getItem(int i);

        public abstract int getSize();
    }

    /* access modifiers changed from: protected */
    public abstract Bitmap processBitmap(Object obj);

    protected ImageWorker(Context context) {
        this.mContext = context;
    }

    public void loadImage(Object data, ImageView imageView) {
        Bitmap bitmap = null;
        if (this.mImageCache != null) {
            bitmap = this.mImageCache.getBitmapFromMemCache(String.valueOf(data));
        }
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else if (cancelPotentialWork(data, imageView)) {
            BitmapWorkerTask task = new BitmapWorkerTask(imageView);
            imageView.setImageDrawable(new AsyncDrawable(this.mContext.getResources(), this.mLoadingBitmap, task));
            task.execute(data);
        }
    }

    public void loadImage(int num, ImageView imageView) {
        if (this.mImageWorkerAdapter != null) {
            loadImage(this.mImageWorkerAdapter.getItem(num), imageView);
            return;
        }
        throw new NullPointerException("Data not set, must call setAdapter() first.");
    }

    public void setLoadingImage(Bitmap bitmap) {
        this.mLoadingBitmap = bitmap;
    }

    public void setLoadingImage(int resId) {
        this.mLoadingBitmap = BitmapFactory.decodeResource(this.mContext.getResources(), resId);
    }

    public void setImageCache(ImageCache cacheCallback) {
        this.mImageCache = cacheCallback;
    }

    public ImageCache getImageCache() {
        return this.mImageCache;
    }

    public void setImageFadeIn(boolean fadeIn) {
        this.mFadeInBitmap = fadeIn;
    }

    public void setExitTasksEarly(boolean exitTasksEarly) {
        this.mExitTasksEarly = exitTasksEarly;
    }

    public static void cancelWork(ImageView imageView) {
        BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
        if (bitmapWorkerTask != null) {
            bitmapWorkerTask.cancel(true);
            if (ShareApplication.debug) {
                Log.d(TAG, "cancelWork - cancelled work for " + bitmapWorkerTask.data);
            }
        }
    }

    public static boolean cancelPotentialWork(Object data, ImageView imageView) {
        BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
        if (bitmapWorkerTask == null) {
            return true;
        }
        Object bitmapData = bitmapWorkerTask.data;
        if (bitmapData != null && bitmapData.equals(data)) {
            return false;
        }
        bitmapWorkerTask.cancel(true);
        if (!ShareApplication.debug) {
            return true;
        }
        Log.d(TAG, "cancelPotentialWork - cancelled work for " + data);
        return true;
    }

    /* access modifiers changed from: private */
    public static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                return ((AsyncDrawable) drawable).getBitmapWorkerTask();
            }
        }
        return null;
    }

    private class BitmapWorkerTask extends AsyncTask<Object, Void, Bitmap> {
        /* access modifiers changed from: private */
        public Object data;
        private final WeakReference<ImageView> imageViewReference;

        public BitmapWorkerTask(ImageView imageView) {
            this.imageViewReference = new WeakReference<>(imageView);
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(Object... params) {
            this.data = params[0];
            String dataString = String.valueOf(this.data);
            Bitmap bitmap = null;
            if (ImageWorker.this.mImageCache != null && !isCancelled() && getAttachedImageView() != null && !ImageWorker.this.mExitTasksEarly) {
                bitmap = ImageWorker.this.mImageCache.getBitmapFromDiskCache(dataString);
            }
            if (bitmap == null && !isCancelled() && getAttachedImageView() != null && !ImageWorker.this.mExitTasksEarly) {
                bitmap = ImageWorker.this.processBitmap(params[0]);
            }
            if (!(bitmap == null || ImageWorker.this.mImageCache == null)) {
                ImageWorker.this.mImageCache.addBitmapToCache(dataString, bitmap);
            }
            return bitmap;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            if (isCancelled() || ImageWorker.this.mExitTasksEarly) {
                bitmap = null;
            }
            ImageView imageView = getAttachedImageView();
            if (bitmap != null && imageView != null) {
                ImageWorker.this.setImageBitmap(imageView, bitmap);
            }
        }

        private ImageView getAttachedImageView() {
            ImageView imageView = this.imageViewReference.get();
            if (this == ImageWorker.getBitmapWorkerTask(imageView)) {
                return imageView;
            }
            return null;
        }
    }

    private static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);
            this.bitmapWorkerTaskReference = new WeakReference<>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return this.bitmapWorkerTaskReference.get();
        }
    }

    /* access modifiers changed from: private */
    public void setImageBitmap(ImageView imageView, Bitmap bitmap) {
        if (this.mFadeInBitmap) {
            TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(this.mContext.getResources(), bitmap)});
            imageView.setImageDrawable(td);
            td.startTransition(200);
            return;
        }
        imageView.setImageBitmap(bitmap);
    }

    public void setDealimage(DealImage img) {
        this.dealImage = img;
    }

    public void setAdapter(ImageWorkerAdapter adapter) {
        this.mImageWorkerAdapter = adapter;
    }

    public ImageWorkerAdapter getAdapter() {
        return this.mImageWorkerAdapter;
    }
}
