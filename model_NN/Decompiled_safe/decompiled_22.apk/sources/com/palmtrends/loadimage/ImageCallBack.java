package com.palmtrends.loadimage;

import android.graphics.drawable.Drawable;

public interface ImageCallBack {
    void imageLoaded(Drawable drawable, String str);
}
