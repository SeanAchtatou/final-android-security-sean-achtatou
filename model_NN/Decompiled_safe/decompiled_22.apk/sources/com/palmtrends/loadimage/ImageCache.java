package com.palmtrends.loadimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.LruCache;
import android.util.Log;
import com.palmtrends.app.ShareApplication;
import java.io.File;

public class ImageCache {
    private static final boolean DEFAULT_CLEAR_DISK_CACHE_ON_START = false;
    /* access modifiers changed from: private */
    public static final Bitmap.CompressFormat DEFAULT_COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;
    private static final int DEFAULT_COMPRESS_QUALITY = 70;
    private static final boolean DEFAULT_DISK_CACHE_ENABLED = true;
    private static final int DEFAULT_DISK_CACHE_SIZE = 10485760;
    private static final boolean DEFAULT_MEM_CACHE_ENABLED = true;
    private static final int DEFAULT_MEM_CACHE_SIZE = 5242880;
    private static final String TAG = "ImageCache";
    private DiskLruCache mDiskCache;
    private LruCache<String, Bitmap> mMemoryCache;

    public ImageCache(Context context, ImageCacheParams cacheParams) {
        init(context, cacheParams);
    }

    public ImageCache(Context context, String uniqueName) {
        init(context, new ImageCacheParams(uniqueName));
    }

    public static ImageCache findOrCreateCache(FragmentActivity activity, String uniqueName) {
        return findOrCreateCache(activity, new ImageCacheParams(uniqueName));
    }

    public static ImageCache findOrCreateCache(FragmentActivity activity, ImageCacheParams cacheParams) {
        RetainFragment mRetainFragment = RetainFragment.findOrCreateRetainFragment(activity.getSupportFragmentManager());
        ImageCache imageCache = (ImageCache) mRetainFragment.getObject();
        if (imageCache != null) {
            return imageCache;
        }
        ImageCache imageCache2 = new ImageCache(activity, cacheParams);
        mRetainFragment.setObject(imageCache2);
        return imageCache2;
    }

    private void init(Context context, ImageCacheParams cacheParams) {
        File diskCacheDir = DiskLruCache.getDiskCacheDir(context, cacheParams.uniqueName);
        if (!diskCacheDir.exists()) {
            diskCacheDir.mkdirs();
        }
        if (cacheParams.diskCacheEnabled) {
            this.mDiskCache = DiskLruCache.openCache(context, diskCacheDir, (long) cacheParams.diskCacheSize);
            if (this.mDiskCache == null) {
                cacheParams.diskCacheEnabled = false;
            } else {
                this.mDiskCache.setCompressParams(cacheParams.compressFormat, cacheParams.compressQuality);
                if (cacheParams.clearDiskCacheOnStart) {
                    this.mDiskCache.clearCache();
                }
            }
        }
        if (cacheParams.memoryCacheEnabled) {
            this.mMemoryCache = new LruCache<String, Bitmap>(cacheParams.memCacheSize) {
                /* access modifiers changed from: protected */
                public int sizeOf(String key, Bitmap bitmap) {
                    return Utils.getBitmapSize(bitmap);
                }
            };
        }
    }

    public void addBitmapToCache(String data, Bitmap bitmap) {
        if (data != null && bitmap != null) {
            if (this.mMemoryCache != null && this.mMemoryCache.get(data) == null) {
                this.mMemoryCache.put(data, bitmap);
            }
            if (this.mDiskCache != null && !this.mDiskCache.containsKey(data)) {
                this.mDiskCache.put(data, bitmap);
            }
        }
    }

    public Bitmap getBitmapFromMemCache(String data) {
        Bitmap memBitmap;
        if (this.mMemoryCache == null || (memBitmap = this.mMemoryCache.get(data)) == null) {
            return null;
        }
        if (!ShareApplication.debug) {
            return memBitmap;
        }
        Log.d(TAG, "Memory cache hit");
        return memBitmap;
    }

    public Bitmap getBitmapFromDiskCache(String data) {
        if (this.mDiskCache != null) {
            return this.mDiskCache.get(data);
        }
        return null;
    }

    public void clearCaches() {
        this.mDiskCache.clearCache();
        this.mMemoryCache.evictAll();
    }

    public static class ImageCacheParams {
        public boolean clearDiskCacheOnStart = false;
        public Bitmap.CompressFormat compressFormat = ImageCache.DEFAULT_COMPRESS_FORMAT;
        public int compressQuality = ImageCache.DEFAULT_COMPRESS_QUALITY;
        public boolean diskCacheEnabled = true;
        public int diskCacheSize = 10485760;
        public int memCacheSize = ImageCache.DEFAULT_MEM_CACHE_SIZE;
        public boolean memoryCacheEnabled = true;
        public String uniqueName;

        public ImageCacheParams(String uniqueName2) {
            this.uniqueName = uniqueName2;
        }
    }
}
