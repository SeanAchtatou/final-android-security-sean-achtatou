package com.palmtrends.loadimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import com.example.palmtrends_utils.R;
import com.ibm.mqtt.MqttUtils;
import com.palmtrends.app.ShareApplication;
import com.utils.FileUtils;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class DiskLruCache {
    private static final String CACHE_FILENAME_PREFIX = "cache_";
    private static final int INITIAL_CAPACITY = 32;
    private static final float LOAD_FACTOR = 0.75f;
    private static final int MAX_REMOVALS = 4;
    private static final String TAG = "DiskLruCache";
    private static final FilenameFilter cacheFileFilter = new FilenameFilter() {
        public boolean accept(File dir, String filename) {
            return filename.startsWith(DiskLruCache.CACHE_FILENAME_PREFIX);
        }
    };
    static final boolean ifoffline = ShareApplication.share.getResources().getBoolean(R.bool.offline);
    private int cacheByteSize = 0;
    private int cacheSize = 0;
    private final File mCacheDir;
    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;
    private int mCompressQuality = 70;
    private final Map<String, String> mLinkedHashMap = Collections.synchronizedMap(new LinkedHashMap(32, LOAD_FACTOR, true));
    private long maxCacheByteSize = 5242880;
    private final int maxCacheItemSize = 64;

    public static DiskLruCache openCache(Context context, File cacheDir, long maxByteSize) {
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        if (!cacheDir.isDirectory() || !cacheDir.canWrite()) {
            return null;
        }
        return new DiskLruCache(cacheDir, maxByteSize);
    }

    private DiskLruCache(File cacheDir, long maxByteSize) {
        this.mCacheDir = cacheDir;
        this.maxCacheByteSize = maxByteSize;
    }

    public void put(String key, Bitmap data) {
        synchronized (this.mLinkedHashMap) {
            if (this.mLinkedHashMap.get(key) == null) {
                try {
                    String file = createFilePath(this.mCacheDir, key);
                    if (writeBitmapToFile(data, file)) {
                        put(key, file);
                        flushCache();
                    }
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "Error in put: " + e.getMessage());
                } catch (IOException e2) {
                    Log.e(TAG, "Error in put: " + e2.getMessage());
                }
            }
        }
        return;
    }

    private void put(String key, String file) {
        this.mLinkedHashMap.put(key, file);
        this.cacheSize = this.mLinkedHashMap.size();
        this.cacheByteSize = (int) (((long) this.cacheByteSize) + new File(file).length());
    }

    private void flushCache() {
        int count = 0;
        while (count < 4) {
            if (this.cacheSize > 64 || ((long) this.cacheByteSize) > this.maxCacheByteSize) {
                Map.Entry<String, String> eldestEntry = this.mLinkedHashMap.entrySet().iterator().next();
                File eldestFile = new File((String) eldestEntry.getValue());
                long eldestFileSize = eldestFile.length();
                this.mLinkedHashMap.remove(eldestEntry.getKey());
                eldestFile.delete();
                this.cacheSize = this.mLinkedHashMap.size();
                this.cacheByteSize = (int) (((long) this.cacheByteSize) - eldestFileSize);
                count++;
                if (ShareApplication.debug) {
                    Log.d(TAG, "flushCache - Removed cache file, " + eldestFile + ", " + eldestFileSize);
                }
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap get(java.lang.String r8) {
        /*
            r7 = this;
            java.util.Map<java.lang.String, java.lang.String> r5 = r7.mLinkedHashMap
            monitor-enter(r5)
            java.util.Map<java.lang.String, java.lang.String> r4 = r7.mLinkedHashMap     // Catch:{ all -> 0x0043 }
            java.lang.Object r2 = r4.get(r8)     // Catch:{ all -> 0x0043 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x0043 }
            if (r2 == 0) goto L_0x001e
            boolean r4 = com.palmtrends.app.ShareApplication.debug     // Catch:{ all -> 0x0043 }
            if (r4 == 0) goto L_0x0018
            java.lang.String r4 = "DiskLruCache"
            java.lang.String r6 = "Disk cache hit"
            android.util.Log.d(r4, r6)     // Catch:{ all -> 0x0043 }
        L_0x0018:
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeFile(r2)     // Catch:{ Throwable -> 0x0058 }
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
        L_0x001d:
            return r4
        L_0x001e:
            java.io.File r4 = r7.mCacheDir     // Catch:{ all -> 0x0043 }
            java.lang.String r1 = createFilePath(r4, r8)     // Catch:{ all -> 0x0043 }
            java.io.File r4 = new java.io.File     // Catch:{ all -> 0x0043 }
            r4.<init>(r1)     // Catch:{ all -> 0x0043 }
            boolean r4 = r4.exists()     // Catch:{ all -> 0x0043 }
            if (r4 == 0) goto L_0x0059
            r7.put(r8, r1)     // Catch:{ all -> 0x0043 }
            boolean r4 = com.palmtrends.app.ShareApplication.debug     // Catch:{ all -> 0x0043 }
            if (r4 == 0) goto L_0x003d
            java.lang.String r4 = "DiskLruCache"
            java.lang.String r6 = "Disk cache hit (existing file)"
            android.util.Log.d(r4, r6)     // Catch:{ all -> 0x0043 }
        L_0x003d:
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeFile(r1)     // Catch:{ OutOfMemoryError -> 0x0046 }
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x001d
        L_0x0043:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            throw r4
        L_0x0046:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0043 }
            android.graphics.BitmapFactory$Options r3 = new android.graphics.BitmapFactory$Options     // Catch:{ all -> 0x0043 }
            r3.<init>()     // Catch:{ all -> 0x0043 }
            r4 = 4
            r3.inSampleSize = r4     // Catch:{ all -> 0x0043 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeFile(r1, r3)     // Catch:{ all -> 0x0043 }
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            goto L_0x001d
        L_0x0058:
            r4 = move-exception
        L_0x0059:
            monitor-exit(r5)     // Catch:{ all -> 0x0043 }
            r4 = 0
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.palmtrends.loadimage.DiskLruCache.get(java.lang.String):android.graphics.Bitmap");
    }

    public boolean containsKey(String key) {
        if (this.mLinkedHashMap.containsKey(key)) {
            return true;
        }
        String existingFile = createFilePath(this.mCacheDir, key);
        if (!new File(existingFile).exists()) {
            return false;
        }
        put(key, existingFile);
        return true;
    }

    public void clearCache() {
        clearCache(this.mCacheDir);
    }

    public static void clearCache(Context context, String uniqueName) {
        clearCache(getDiskCacheDir(context, uniqueName));
    }

    private static void clearCache(File cacheDir) {
        File[] files = cacheDir.listFiles(cacheFileFilter);
        if (files != null) {
            for (File delete : files) {
                delete.delete();
            }
        }
    }

    public static File getDiskCacheDir(Context context, String uniqueName) {
        String cachePath;
        if (Environment.getExternalStorageState().equals("mounted") || !Utils.isExternalStorageRemovable()) {
            cachePath = Utils.getExternalCacheDir(context).getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return new File(String.valueOf(cachePath) + File.separator + uniqueName);
    }

    public static String createFilePath(File cacheDir, String key) {
        try {
            if (ifoffline) {
                return String.valueOf(cacheDir.getAbsolutePath()) + File.separator + CACHE_FILENAME_PREFIX + FileUtils.converPathToName(key);
            }
            return String.valueOf(cacheDir.getAbsolutePath()) + File.separator + CACHE_FILENAME_PREFIX + URLEncoder.encode(key.replace("*", ""), MqttUtils.STRING_ENCODING);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "createFilePath - " + e);
            return null;
        }
    }

    public String createFilePath(String key) {
        return createFilePath(this.mCacheDir, key);
    }

    public void setCompressParams(Bitmap.CompressFormat compressFormat, int quality) {
        this.mCompressFormat = compressFormat;
        this.mCompressQuality = quality;
    }

    private boolean writeBitmapToFile(Bitmap bitmap, String file) throws IOException, FileNotFoundException {
        OutputStream out = null;
        try {
            OutputStream out2 = new BufferedOutputStream(new FileOutputStream(file), 8192);
            try {
                boolean compress = bitmap.compress(this.mCompressFormat, this.mCompressQuality, out2);
                if (out2 != null) {
                    out2.close();
                }
                return compress;
            } catch (Throwable th) {
                th = th;
                out = out2;
            }
        } catch (Throwable th2) {
            th = th2;
            if (out != null) {
                out.close();
            }
            throw th;
        }
    }
}
