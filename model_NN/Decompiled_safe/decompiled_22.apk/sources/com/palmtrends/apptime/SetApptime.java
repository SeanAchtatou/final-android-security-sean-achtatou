package com.palmtrends.apptime;

import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.dao.DBHelper;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class SetApptime {
    private String aid;
    private Date date;
    private DBHelper db;
    private String e_time;
    private String mark;
    private String open_mode;
    private String s_time;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String type;
    TimeZone tz = TimeZone.getTimeZone("Asia/Shanghai");

    public SetApptime(Date date2, String type2, String aid2, String open_mode2) {
        this.type = type2;
        this.aid = aid2;
        this.open_mode = open_mode2;
        this.date = date2;
        if (new GregorianCalendar().getTimeZone().getID().toString() != "Asia/Shanghai") {
            this.sdf.getCalendar().setTimeZone(this.tz);
        }
        settime();
    }

    public void settime() {
        this.db = DBHelper.getDBHelper();
        this.s_time = this.sdf.format(this.date);
        this.e_time = this.s_time;
        try {
            this.db.insert_apptime(this.type, this.s_time, this.e_time, UploadUtils.FAILURE, this.aid, this.open_mode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void up_end_time(Date starttime, Date endtime, String mark2) {
        try {
            this.db.up_appendtime(this.s_time, this.sdf.format(endtime), mark2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void up_end_time(Date starttime, Date endtime, String aid2, String mark2) {
        try {
            this.db.up_appendtime(this.s_time, this.sdf.format(endtime), aid2, mark2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
