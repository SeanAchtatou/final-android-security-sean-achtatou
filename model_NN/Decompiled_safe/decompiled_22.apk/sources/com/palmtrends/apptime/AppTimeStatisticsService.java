package com.palmtrends.apptime;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.baidu.android.pushservice.PushConstants;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.dao.DBHelper;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;

public class AppTimeStatisticsService extends Service {
    public static final int UP_APPTIME_ERROR = 2;
    public static final int UP_APPTIME_NO = 0;
    public static final int UP_APPTIME_YES = 1;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                default:
                    return;
                case 1:
                    try {
                        DBHelper.getDBHelper().delete_apptime();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
            }
        }
    };
    private boolean isone = true;
    public Date start_time;
    public SetApptime time;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        new Timer(true).schedule(new TimerTask() {
            public void run() {
                ApptimePost.post(AppTimeStatisticsService.this.gettime(), AppTimeStatisticsService.this.handler);
            }
        }, 1, 300000);
    }

    public void onDestroy() {
        this.time.up_end_time(this.start_time, new Date(), UploadUtils.SUCCESS);
        super.onDestroy();
    }

    public void onStart(Intent intent, int startId) {
        this.start_time = new Date();
        this.time = new SetApptime(this.start_time, PushConstants.EXTRA_APP, UploadUtils.SUCCESS, UploadUtils.SUCCESS);
        super.onStart(intent, startId);
    }

    public JSONArray gettime() {
        try {
            return DBHelper.getDBHelper().get_apptime();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
