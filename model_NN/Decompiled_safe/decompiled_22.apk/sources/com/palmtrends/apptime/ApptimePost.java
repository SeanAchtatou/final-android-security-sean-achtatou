package com.palmtrends.apptime;

import android.os.Handler;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.dao.MySSLSocketFactory;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class ApptimePost {
    /* access modifiers changed from: private */
    public static String Apptime_post_url = "http://dbms.palmtrends.com/api/dbms2.php";

    public static void post(JSONArray jsonArray, final Handler handler) {
        JSONArray json = jsonArray;
        System.out.println("----:" + json);
        if (json != null) {
            final List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("action", "appdata_collection"));
            param.add(new BasicNameValuePair("data", json.toString()));
            new Thread() {
                public void run() {
                    try {
                        String jsonstr = MySSLSocketFactory.getinfo(ApptimePost.Apptime_post_url, param);
                        System.out.println("-fanhui--:" + jsonstr);
                        if (new JSONObject(jsonstr).getString(Constants.SINA_CODE).equals(UploadUtils.FAILURE)) {
                            handler.sendEmptyMessage(1);
                        } else {
                            handler.sendEmptyMessage(0);
                        }
                    } catch (Exception e) {
                        handler.sendEmptyMessage(0);
                    }
                }
            }.start();
        }
    }
}
