package com.palmtrends.basefragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import com.palmtrends.ad.AdAdapter;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.controll.R;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.datasource.JsonParser;
import com.palmtrends.entity.AdType;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.utils.FinalVariable;
import java.io.Serializable;
import java.util.List;
import org.json.JSONException;

@SuppressLint({"HandlerLeak", "NewApi"})
public abstract class BaseFragment<T> extends Fragment {
    private static final String KEY_CONTENT_HEADOBJ = "SinglerListFragment:headobj";
    private static final String KEY_CONTENT_HEADTYPE = "SinglerListFragment:headtype";
    private static final String KEY_CONTENT_OLDTYPE = "SinglerListFragment:oldtype";
    private static final String KEY_CONTENT_PARTTYPE = "SinglerListFragment:parttype";
    public int ad_pos = 1;
    public int ad_type = 3;
    public ClientShowAd clientad;
    public Object headobj;
    public boolean isShowAD = true;
    public boolean isloading = false;
    public Context mContext;
    public Data mData;
    public int mFooter_limit = this.mLength;
    public Handler mHandler;
    public int mHeadType = -1;
    public int mLength = 20;
    public String mOldtype = "";
    public int mPage;
    public String mParttype = "";
    public BaseFragment<T>.ListAdapter mlistAdapter;

    public abstract void addListener();

    public abstract void findView();

    public abstract View getListHeadview(Object obj);

    public abstract View getListItemview(View view, Object obj, int i);

    public abstract void update();

    public View onCreateView(LayoutInflater inflater, ViewGroup containers, Bundle savedInstanceState) {
        if ((this.mParttype == null || this.mParttype.equals("")) && savedInstanceState != null && savedInstanceState.containsKey(KEY_CONTENT_PARTTYPE + getId())) {
            this.mParttype = savedInstanceState.getString(KEY_CONTENT_PARTTYPE + getId());
            this.mOldtype = savedInstanceState.getString(KEY_CONTENT_OLDTYPE + getId());
            this.mHeadType = savedInstanceState.getInt(KEY_CONTENT_HEADTYPE + getId());
            this.headobj = savedInstanceState.getSerializable(KEY_CONTENT_HEADOBJ + getId());
        }
        this.mContext = inflater.getContext();
        this.mLength = this.mContext.getResources().getInteger(R.integer.mleng);
        this.mFooter_limit = this.mLength;
        return super.onCreateView(inflater, containers, savedInstanceState);
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT_PARTTYPE + getId(), this.mParttype);
        outState.putString(KEY_CONTENT_OLDTYPE + getId(), this.mOldtype);
        outState.putInt(KEY_CONTENT_HEADTYPE + getId(), this.mHeadType);
        outState.putSerializable(KEY_CONTENT_HEADOBJ + getId(), (Serializable) this.headobj);
    }

    public void onDestroy() {
        this.headobj = null;
        super.onDestroy();
    }

    public void initType(String type, String partType) {
        this.mOldtype = type;
        this.mParttype = partType;
    }

    public View getListHeadview(Object obj, int type) {
        return null;
    }

    public void fillAd(AdAdapter ad) {
        if (this.mPage == 0 && this.isShowAD && !this.mParttype.startsWith(DBHelper.FAV_FLAG) && !this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            if (this.clientad == null) {
                this.clientad = new ClientShowAd();
            }
            this.clientad.showAdFIXED(this.mContext, this.ad_type, ad, "");
        }
    }

    public void fillHead() {
        if (this.mData != null && this.mData.list != null && this.mData.headtype != -1) {
            this.mHeadType = this.mData.headtype;
            if (this.mData.headtype == 0) {
                Listitem head = (Listitem) this.mData.obj;
                head.ishead = "true";
                this.mData.list.add(0, head);
                return;
            }
            this.headobj = this.mData.obj;
            Listitem head2 = new Listitem();
            head2.ishead = "true";
            this.mData.list.add(0, head2);
        }
    }

    public void initData() {
        this.mPage = 0;
        new Thread() {
            String old;

            public void run() {
                BaseFragment.this.isloading = true;
                this.old = BaseFragment.this.mOldtype;
                try {
                    Data d = BaseFragment.this.getDataFromDB(BaseFragment.this.mOldtype, 0, BaseFragment.this.mLength, BaseFragment.this.mParttype);
                    if (!(d == null || d.list == null || d.list.size() <= 0)) {
                        BaseFragment.this.mData = d;
                        BaseFragment.this.fillHead();
                        BaseFragment.this.mHandler.sendEmptyMessage(FinalVariable.update);
                    }
                    Data d2 = BaseFragment.this.getDataFromNet(Urls.list_url, BaseFragment.this.mOldtype, 0, BaseFragment.this.mLength, true, BaseFragment.this.mParttype);
                    if (BaseFragment.this.mOldtype.equals(this.old)) {
                        BaseFragment.this.mData = d2;
                        BaseFragment.this.fillHead();
                        BaseFragment.this.onDataLoadComplete(d2, true);
                        BaseFragment.this.isloading = false;
                    }
                } catch (Exception e) {
                    BaseFragment.this.onDataError(e);
                } finally {
                    BaseFragment.this.isloading = false;
                }
            }
        }.start();
    }

    public void onDataError(Exception e) {
        e.printStackTrace();
        Message msg = new Message();
        if (e instanceof JSONException) {
            msg.what = FinalVariable.nomore;
            if (e.getMessage() == null || (e.getMessage().indexOf("cannot be converted") == -1 && e.getMessage().indexOf("End of input") == -1)) {
                msg.obj = e.getMessage();
            } else {
                msg.obj = this.mContext.getResources().getString(R.string.data_type_error);
            }
            this.mHandler.sendMessage(msg);
            return;
        }
        msg.what = FinalVariable.error;
        this.mHandler.sendMessage(msg);
    }

    public void onDataLoadComplete(Data data, boolean isFirst) {
        if (data == null || data.list == null) {
            this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
            return;
        }
        if (isFirst) {
            this.mHandler.sendEmptyMessage(FinalVariable.first_update);
        } else {
            this.mHandler.sendEmptyMessage(FinalVariable.update);
        }
        if (data.loadmorestate == 0) {
            this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
        }
    }

    public void loadMore() {
        if (this.mPage == 0) {
            this.mPage++;
        }
        String old = this.mOldtype;
        try {
            Data d = getDataFromDB(this.mOldtype, this.mPage, this.mLength, this.mParttype);
            if (d == null || d.list == null || d.list.size() < this.mLength) {
                if (Utils.isNetworkAvailable(this.mContext)) {
                    d = getDataFromNet(Urls.list_url, this.mOldtype, this.mPage, this.mLength, false, this.mParttype);
                }
                if (d == null || d.list == null || d.list.size() <= 0) {
                    this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                } else if (this.mOldtype.equals(old)) {
                    this.mData = d;
                    onDataLoadComplete(this.mData, false);
                    this.mPage++;
                }
                this.isloading = false;
                return;
            }
            this.mData = d;
            this.mHandler.sendEmptyMessage(FinalVariable.update);
            this.mPage++;
        } catch (Exception e) {
            onDataError(e);
            e.printStackTrace();
        } finally {
            this.isloading = false;
        }
    }

    public void reFlush() {
        String old = this.mOldtype;
        try {
            this.mPage = 0;
            this.isloading = true;
            Data d = getDataFromNet(Urls.list_url, this.mOldtype, 0, this.mLength, true, this.mParttype);
            if (this.mOldtype.equals(old)) {
                if (d != null) {
                    if (d.list != null && d.list.size() > 0) {
                        this.mData = d;
                    }
                }
                fillHead();
                onDataLoadComplete(d, true);
                this.isloading = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Message msg = new Message();
            if (e instanceof JSONException) {
                msg.what = FinalVariable.nomore;
                if (e.getMessage() == null || (e.getMessage().indexOf("cannot be converted") == -1 && e.getMessage().indexOf("End of input") == -1)) {
                    msg.obj = e.getMessage();
                } else {
                    msg.obj = this.mContext.getResources().getString(R.string.data_type_error);
                }
                this.mHandler.sendMessage(msg);
            } else {
                msg.what = FinalVariable.error;
                this.mHandler.sendMessage(msg);
            }
        } finally {
            this.isloading = false;
        }
    }

    public void resumeAction() {
        if (this.mlistAdapter == null) {
            return;
        }
        if (this.mParttype.startsWith(DBHelper.FAV_FLAG)) {
            initData();
        } else {
            this.mlistAdapter.notifyDataSetChanged();
        }
    }

    public void onResume() {
        resumeAction();
        super.onResume();
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count);
        }
        String json = DNDataSource.list_FromNET(url, oldtype, page, count, parttype, isfirst);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public Data parseJson(String json) throws Exception {
        return JsonParser.ParserArticleList(json);
    }

    public Data getDataFromDB(String oldtype, int page, int count, String parttype) throws Exception {
        String json = DNDataSource.list_FromDB(oldtype, page, count, parttype);
        if (json == null || "".equals(json) || "null".equals(json)) {
            return null;
        }
        return parseJson(json);
    }

    public class ListAdapter extends BaseAdapter implements AdAdapter {
        public View adview;
        public List datas;
        String type;

        public ListAdapter(List datas2, String type2) {
            this.datas = datas2;
            this.type = type2;
        }

        public int getCount() {
            return this.datas.size();
        }

        public Object getItem(int position) {
            return this.datas.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View v, ViewGroup parent) {
            T li = this.datas.get(position);
            AdType item = null;
            if (li instanceof AdType) {
                item = li;
            }
            if (item == null || !"true".equals(item.ishead) || position != 0 || BaseFragment.this.mParttype.startsWith(DBHelper.FAV_FLAG)) {
                if (item == null || this.adview == null || !"true".equals(item.isad) || BaseFragment.this.mParttype.startsWith(DBHelper.FAV_FLAG)) {
                    if ((v instanceof FrameLayout) || (v instanceof WebView)) {
                        v = null;
                    }
                    return BaseFragment.this.getListItemview(v, li, position);
                } else if ("home_ad_i".equals(item.sa)) {
                    return BaseFragment.this.getListItemview(v, li, position);
                } else {
                    return this.adview;
                }
            } else if (BaseFragment.this.mHeadType == 0) {
                return BaseFragment.this.getListHeadview(li);
            } else {
                return BaseFragment.this.getListHeadview(BaseFragment.this.headobj, BaseFragment.this.mHeadType);
            }
        }

        public void setAdview(View v) {
            if (this.datas != null && this.datas.size() != 0 && (this.datas.get(0) instanceof AdType)) {
                if ("true".equals(((AdType) this.datas.get(0)).ishead)) {
                    if (!(this.datas == null || this.datas.get(1) == null)) {
                        Listitem li = new Listitem();
                        li.isad = "true";
                        this.datas.add(1, li);
                    }
                } else if (!(this.datas == null || this.datas.get(0) == null)) {
                    Listitem li2 = new Listitem();
                    li2.isad = "true";
                    this.datas.add(0, li2);
                }
                this.adview = v;
                notifyDataSetChanged();
            }
        }

        public void remove() {
            int count = this.datas.size();
            int i = 0;
            while (true) {
                if (i >= count) {
                    break;
                } else if ("true".equals(((AdType) this.datas.get(i)).isad)) {
                    this.datas.remove(i);
                    break;
                } else {
                    i++;
                }
            }
            this.adview = null;
            notifyDataSetChanged();
        }

        public void addDatas(List list) {
            if (this.datas != null) {
                this.datas.addAll(list);
            }
            notifyDataSetChanged();
        }
    }
}
