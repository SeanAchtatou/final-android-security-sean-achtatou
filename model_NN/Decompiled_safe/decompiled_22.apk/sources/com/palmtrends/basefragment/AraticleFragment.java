package com.palmtrends.basefragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.apptime.SetApptime;
import com.palmtrends.baseui.BaseArticleActivity;
import com.palmtrends.baseview.ImageDetailViewPager;
import com.palmtrends.controll.R;
import com.palmtrends.dao.ClientInfo;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.datasource.ImageLoadUtils;
import com.palmtrends.entity.Entity;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.ImageFetcher;
import com.palmtrends.loadimage.ImageWorker;
import com.palmtrends.loadimage.Utils;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.mm.sdk.platformtools.Util;
import com.tencent.tauth.Constants;
import com.utils.FileUtils;
import com.utils.FinalVariable;
import com.utils.JniUtils;
import com.utils.PerfHelper;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class AraticleFragment extends Fragment {
    private static final String IMAGE_DATA_EXTRA = "resId";
    public static final String SCROLL_POSITION = "scroll_position";
    public static FragmentActivity mActivity;
    static boolean mIsSelectingText = false;
    public static ImageDetailViewPager mPager;
    public GestureDetector gd = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (AraticleFragment.this.mlistener != null) {
                AraticleFragment.this.mlistener.onSingleTapConfirmed();
            }
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            if (AraticleFragment.this.mlistener != null) {
                AraticleFragment.this.mlistener.onLongPress();
            }
            super.onLongPress(e);
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            PerfHelper.setInfo(AraticleFragment.SCROLL_POSITION + AraticleFragment.this.item.nid, AraticleFragment.this.mWebView.getScrollY());
            return super.onFling(e1, e2, velocityX, velocityY);
        }

        public boolean onDoubleTap(MotionEvent e) {
            if (AraticleFragment.this.mlistener != null) {
                AraticleFragment.this.mlistener.onDoubleTap();
            }
            return super.onDoubleTap(e);
        }
    });
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FinalVariable.update /*1001*/:
                    AraticleFragment.this.time = new SetApptime(AraticleFragment.this.start_time, "article", AraticleFragment.this.item.nid, "net");
                    String str = "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID) + "&e=" + JniUtils.getkey() + "&platform=a&pid=" + FinalVariable.pid + "&mobile=" + URLEncoder.encode(Build.MODEL);
                    if (ShareApplication.debug) {
                        System.out.println(String.valueOf(AraticleFragment.this.mContext.getResources().getString(R.string.article_url)) + AraticleFragment.this.item.nid + "&fontsize=" + PerfHelper.getStringData(PerfHelper.P_TEXT) + "&mode=" + PerfHelper.getStringData(PerfHelper.P_DATE_MODE) + str);
                    }
                    AraticleFragment.this.mWebView.loadUrl(String.valueOf(AraticleFragment.this.mContext.getResources().getString(R.string.article_url)) + AraticleFragment.this.item.nid + "&fontsize=" + PerfHelper.getStringData(PerfHelper.P_TEXT) + "&mode=" + PerfHelper.getStringData(PerfHelper.P_DATE_MODE) + str);
                    return;
                case FinalVariable.error /*1004*/:
                    Utils.showToast("服务异常请稍后...");
                    return;
                case FinalVariable.vb_shortid /*10014*/:
                    AraticleFragment.this.shorturl = "  " + AraticleFragment.this.item.title + " " + String.valueOf(msg.obj);
                    DNDataSource.updateRead("readitem", AraticleFragment.this.item.n_mark, "shorturl", AraticleFragment.this.shorturl);
                    return;
                default:
                    return;
            }
        }
    };
    boolean isload = true;
    public boolean isotherArticleback = false;
    public Listitem item;
    public View loading;
    public TextView loading_text;
    public int mArticleNum;
    public Context mContext;
    public ImageWorker mImageWorker;
    public WebView mWebView;
    public ImageDetailViewPager.OnViewListener mlistener;
    String oldhtml = "fsfdsfas_23fdsfdsfds432_fsdaf";
    public PopupWindow pop = null;
    public String shorturl = "";
    public Date start_time;
    public SetApptime time;
    public String title;
    public Map<String, String> urls = new HashMap();
    public View view_fav;

    public static AraticleFragment newInstance(int imageNum, ImageDetailViewPager pager, FragmentActivity activity) {
        AraticleFragment f = new AraticleFragment(pager, activity);
        Bundle args = new Bundle();
        args.putInt(IMAGE_DATA_EXTRA, imageNum);
        f.setArguments(args);
        return f;
    }

    public AraticleFragment getAraticleFragment() {
        return this;
    }

    public AraticleFragment() {
    }

    public AraticleFragment(ImageDetailViewPager pager, FragmentActivity activity) {
        mPager = pager;
        mActivity = activity;
        setHasOptionsMenu(true);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mArticleNum = getArguments() != null ? getArguments().getInt(IMAGE_DATA_EXTRA) : -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity = (FragmentActivity) inflater.getContext();
        View v = inflater.inflate(R.layout.article_fragment, container, false);
        this.mWebView = (WebView) v.findViewById(R.id.article_webvew);
        this.mContext = inflater.getContext();
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setDatabaseEnabled(true);
        this.mWebView.getSettings().setDomStorageEnabled(true);
        this.mWebView.getSettings().setDatabasePath(mActivity.getDir("database" + mActivity.getApplicationContext().getPackageName(), 0).getPath());
        this.loading = v.findViewById(R.id.loading);
        this.loading_text = (TextView) this.loading.findViewById(R.id.loading_text);
        this.mWebView.addJavascriptInterface(new LoadHtml(), "loadhtml");
        addoptoins();
        this.loading_text.setTag(String.valueOf(this.mArticleNum) + "_load");
        this.mWebView.setTag(String.valueOf(this.mArticleNum) + "_wb");
        if (mPager != null) {
            this.mlistener = mPager.getOnViewListener();
        }
        this.item = ShareApplication.items.get(this.mArticleNum);
        initdata();
        return v;
    }

    public ImageDetailViewPager.OnViewListener getMlistener() {
        return this.mlistener;
    }

    public void setMlistener(ImageDetailViewPager.OnViewListener mlistener2) {
        this.mlistener = mlistener2;
    }

    public boolean onClickLink(String url) {
        return false;
    }

    public void addoptoins() {
        this.mWebView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                AraticleFragment.this.gd.onTouchEvent(event);
                return false;
            }
        });
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                quotaUpdater.updateQuota(5242880);
            }

            public void onReceivedTitle(WebView view, String t) {
                if (!AraticleFragment.this.item.title.equals(AraticleFragment.this.title)) {
                    AraticleFragment.this.title = t;
                }
                super.onReceivedTitle(view, t);
            }
        });
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                String newurl;
                if (AraticleFragment.this.onClickLink(url)) {
                    return true;
                }
                if (url.endsWith(Util.PHOTO_DEFAULT_EXT) || url.endsWith(".png")) {
                    AraticleFragment.this.showImage(url, AraticleFragment.this.title);
                    return true;
                }
                if (url.startsWith("file")) {
                    newurl = url.replace("file://", Urls.main);
                } else {
                    newurl = url;
                }
                if (newurl.endsWith(".mp3")) {
                    Intent it = new Intent("android.intent.action.VIEW");
                    it.setDataAndType(Uri.parse(newurl), "audio/mp3");
                    AraticleFragment.this.startActivity(it);
                    return true;
                } else if (newurl.endsWith(".mp4")) {
                    Intent it2 = new Intent("android.intent.action.VIEW");
                    it2.setFlags(67108864);
                    Uri uri = Uri.parse(newurl);
                    it2.setType("video/mp4");
                    it2.setDataAndType(uri, "video/mp4");
                    AraticleFragment.this.startActivity(it2);
                    return true;
                } else if (!newurl.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                    return false;
                } else {
                    Intent i = new Intent();
                    i.setAction(AraticleFragment.this.mContext.getResources().getString(R.string.activity_article_showwebinfo));
                    i.putExtra(Constants.PARAM_URL, newurl);
                    AraticleFragment.this.startActivity(i);
                    return true;
                }
            }

            public void onLoadResource(WebView view, final String str) {
                if (str.startsWith("file")) {
                    AraticleFragment.this.loading.setVisibility(8);
                    return;
                }
                if (str.endsWith("png") || str.endsWith("jpg")) {
                    AraticleFragment.this.loading.setVisibility(8);
                    final String imagename = FileUtils.converPathToName(str);
                    AraticleFragment.this.urls.put(str.replace(Urls.main, "").replaceAll("file://", ""), "file://" + FileUtils.sdPath + "image/" + imagename);
                    new Thread() {
                        public void run() {
                            try {
                                if (!FileUtils.isFileExist("image/" + imagename)) {
                                    ImageLoadUtils.downloadFile(str, null);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();
                }
                super.onLoadResource(view, str);
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (ShareApplication.debug) {
                    System.out.println(url);
                }
                if (url.equals("data:back,")) {
                    ArrayList<Listitem> items = BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(AraticleFragment.this.mArticleNum)).toString());
                    if (items != null && items.size() > 0) {
                        items.remove(items.size() - 1);
                        if (items.size() > 0) {
                            AraticleFragment.this.item = (Listitem) items.get(items.size() - 1);
                        } else {
                            AraticleFragment.this.item = ShareApplication.items.get(AraticleFragment.this.mArticleNum);
                        }
                    }
                    AraticleFragment.this.loading.setVisibility(0);
                    AraticleFragment.this.isotherArticleback = true;
                    AraticleFragment.this.initdata();
                } else if (url.startsWith("data")) {
                    AraticleFragment.this.loading.setVisibility(0);
                    AraticleFragment.this.initdata();
                } else if (AraticleFragment.this.isload) {
                    if (AraticleFragment.this.loading != null) {
                        AraticleFragment.this.loading.setVisibility(8);
                    }
                    if (!"file:///android_asset/errorzh.html".equals(url) && AraticleFragment.this.mWebView != null) {
                        if (DBHelper.getDBHelper().counts("readitem", "n_mark='" + AraticleFragment.this.item.n_mark + "'") <= 0) {
                            AraticleFragment.this.mWebView.loadUrl("javascript:window.loadhtml.showHTML('" + AraticleFragment.this.item.n_mark + "', '<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>'," + AraticleFragment.this.mArticleNum + ");");
                        }
                        AraticleFragment.this.mWebView.postInvalidate();
                        AraticleFragment.this.initMode();
                        AraticleFragment.this.onPageFinish();
                        if (AraticleFragment.this.isotherArticleback) {
                            AraticleFragment.this.isotherArticleback = false;
                        }
                    }
                }
            }
        });
    }

    public void initMode() {
        System.out.println("xxxxxxxxx:" + PerfHelper.getStringData(PerfHelper.P_DATE_MODE));
        if ("night".equals(PerfHelper.getStringData(PerfHelper.P_DATE_MODE))) {
            this.mWebView.setBackgroundColor(this.mContext.getResources().getColor(17170444));
            this.loading_text.setTextColor(this.mContext.getResources().getColor(17170443));
            this.mWebView.loadUrl("javascript:dayMode(night)");
            return;
        }
        this.mWebView.setBackgroundColor(this.mContext.getResources().getColor(17170443));
        this.loading_text.setTextColor(this.mContext.getResources().getColor(17170444));
        this.mWebView.loadUrl("javascript:dayMode(day)");
    }

    public void showImage(String url, String title2) {
        View _view = LayoutInflater.from(this.mContext).inflate(R.layout.image, (ViewGroup) null);
        ImageView image = (ImageView) _view.findViewById(R.id.imageview);
        TextView pop_title = (TextView) _view.findViewById(R.id.pop_title);
        final Dialog dialog = new Dialog(this.mContext, R.style.c_dialog);
        dialog.setContentView(_view, new ViewGroup.LayoutParams(-2, -2));
        image.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        if (!url.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
            url = String.valueOf(Urls.main) + url;
        }
        if (getResources().getBoolean(R.bool.article_hastitle)) {
            pop_title.setText(title2);
            pop_title.setVisibility(0);
        }
        ShareApplication.mImageWorker.loadImage(url, image);
    }

    public void onPageFinish() {
    }

    public void initdata() {
        if (BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(this.mArticleNum)).toString()) != null && BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(this.mArticleNum)).toString()).size() > 0) {
            this.item = (Listitem) BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(this.mArticleNum)).toString()).get(BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(this.mArticleNum)).toString()).size() - 1);
        }
        this.mWebView.scrollTo(0, 0);
        if ((mPager == null || mPager.getCurrentItem() == this.mArticleNum) && (mActivity instanceof ImageDetailViewPager.OnArticleOptions)) {
            ((ImageDetailViewPager.OnArticleOptions) mActivity).onInitData(this.item);
        }
        this.loading_text.setVisibility(0);
        if ("night".equals(PerfHelper.getStringData(PerfHelper.P_DATE_MODE))) {
            this.mWebView.setBackgroundColor(this.mContext.getResources().getColor(17170444));
            this.loading_text.setTextColor(this.mContext.getResources().getColor(17170443));
        } else {
            this.mWebView.setBackgroundColor(this.mContext.getResources().getColor(17170443));
            this.loading_text.setTextColor(this.mContext.getResources().getColor(17170444));
        }
        this.start_time = new Date();
        DBHelper db = DBHelper.getDBHelper();
        String[] value = {this.item.n_mark};
        if (db.counts("readitem", "n_mark='" + this.item.n_mark + "'") > 0) {
            this.shorturl = db.select("readitem", "shorturl", "n_mark=?", value);
            this.mWebView.loadDataWithBaseURL(String.valueOf(Urls.main) + "?&fontsize=" + PerfHelper.getStringData(PerfHelper.P_TEXT) + "&mode=" + PerfHelper.getStringData(PerfHelper.P_DATE_MODE) + "&", db.select("readitem", "htmltext", "n_mark=?", new String[]{this.item.n_mark}), "text/html", "utf-8", null);
            if (mPager == null || mPager.getCurrentItem() == this.mArticleNum) {
                DNDataSource.updateRead("readitem", this.item.n_mark, "read", "true");
            }
            this.time = new SetApptime(this.start_time, "article", this.item.nid, "cache");
        } else if ("".equals(PerfHelper.getStringData(PerfHelper.P_USERID))) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        ClientInfo.sendClient_UserInfo();
                        AraticleFragment.this.handler.sendEmptyMessage(FinalVariable.update);
                    } catch (Exception e) {
                        AraticleFragment.this.handler.sendEmptyMessage(FinalVariable.error);
                        e.printStackTrace();
                    }
                }
            }).start();
        } else {
            this.handler.sendEmptyMessage(FinalVariable.update);
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (View.OnClickListener.class.isInstance(getActivity()) && Utils.hasActionBar()) {
            this.mWebView.setOnClickListener((View.OnClickListener) getActivity());
        }
    }

    public class LoadHtml extends Entity {
        public LoadHtml() {
        }

        public synchronized void showHTML(String n_mark, String html, int mArticleNum) {
            if (html.indexOf(AraticleFragment.this.oldhtml) == -1) {
                if (html.indexOf("Vertical centering in valid CSS") == -1) {
                    if (html.indexOf("android-weberror.png") != -1) {
                        AraticleFragment.this.mWebView.loadUrl("file:///android_asset/errorzh.html");
                    } else if (html.indexOf("function") != -1) {
                        String shareURL = AraticleFragment.this.shareURL("/upload[\\w./]*big[\\w./]*[jpg|png]", html);
                        if (shareURL == null && (shareURL = AraticleFragment.this.shareURL("/upload[\\w./]*cross[\\w./]*[jpg|png]", html)) == null) {
                            shareURL = AraticleFragment.this.shareURL("/upload[\\w./]*top[\\w./]*[jpg|png]", html);
                        }
                        AraticleFragment.this.urls.put("/ms_images/plug_iphone4.png", "file://" + FileUtils.sdPath + "/image/plug_iphone4.png");
                        AraticleFragment.this.urls.put("/ms_images/look.png", "file://" + FileUtils.sdPath + "/image/look.png");
                        for (Map.Entry<String, String> item : AraticleFragment.this.urls.entrySet()) {
                            html = html.replaceAll((String) item.getKey(), (String) item.getValue());
                        }
                        synchronized (AraticleFragment.mActivity) {
                            DNDataSource.insertRead(AraticleFragment.this.item.n_mark, html);
                            if (shareURL != null) {
                                DNDataSource.updateRead("readitem", n_mark, "share_image", shareURL);
                            }
                            if (AraticleFragment.mPager == null || AraticleFragment.mPager.getCurrentItem() == mArticleNum) {
                                DNDataSource.updateRead("readitem", AraticleFragment.this.item.n_mark, "read", "true");
                            }
                        }
                    }
                }
            }
        }

        public void showTitle(String title) {
        }

        public void receiverArticle(String json) {
            ArrayList<Listitem> items = BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(AraticleFragment.this.mArticleNum)).toString());
            try {
                JSONObject obj = new JSONObject(json);
                if (items == null) {
                    items = new ArrayList<>();
                }
                Listitem li = new Listitem();
                li.nid = obj.getString(LocaleUtil.INDONESIAN);
                li.sa = AraticleFragment.this.item.sa;
                li.title = AraticleFragment.this.item.title;
                li.getMark();
                if (!items.contains(li)) {
                    items.add(li);
                    AraticleFragment.this.item = li;
                    BaseArticleActivity.o_items.put(new StringBuilder(String.valueOf(AraticleFragment.this.mArticleNum)).toString(), items);
                    AraticleFragment.this.handler.post(new Runnable() {
                        public void run() {
                            AraticleFragment.this.mWebView.loadData("", "", "");
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onPause() {
        super.onPause();
        if (this.time != null) {
            this.time.up_end_time(this.start_time, new Date(), this.item.nid, UploadUtils.FAILURE);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.time != null) {
            this.time.up_end_time(this.start_time, new Date(), this.item.nid, UploadUtils.SUCCESS);
        }
    }

    /* access modifiers changed from: private */
    public String shareURL(String regex, String html) {
        Matcher matcher = Pattern.compile(regex).matcher(html);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return null;
    }

    public void cancelWork() {
        this.mWebView = null;
        this.isload = false;
    }

    public static synchronized void selectAndCopyText(WebView v) {
        synchronized (AraticleFragment.class) {
            try {
                mIsSelectingText = true;
                if (v.getContext().getApplicationInfo().targetSdkVersion <= 5) {
                    WebView.class.getMethod("emulateShiftHeld", Boolean.TYPE).invoke(v, false);
                } else {
                    WebView.class.getMethod("emulateShiftHeld", new Class[0]).invoke(v, new Object[0]);
                }
            } catch (Exception e) {
                emulateShiftHeld(v);
            }
        }
        return;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void emulateShiftHeld(android.webkit.WebView r10) {
        /*
            java.lang.Class<com.palmtrends.basefragment.AraticleFragment> r9 = com.palmtrends.basefragment.AraticleFragment.class
            monitor-enter(r9)
            android.view.KeyEvent r0 = new android.view.KeyEvent     // Catch:{ Exception -> 0x0019, all -> 0x0016 }
            r1 = 0
            r3 = 0
            r5 = 0
            r6 = 59
            r7 = 0
            r8 = 0
            r0.<init>(r1, r3, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0019, all -> 0x0016 }
            r0.dispatch(r10)     // Catch:{ Exception -> 0x0019, all -> 0x0016 }
        L_0x0014:
            monitor-exit(r9)
            return
        L_0x0016:
            r1 = move-exception
            monitor-exit(r9)
            throw r1
        L_0x0019:
            r1 = move-exception
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.palmtrends.basefragment.AraticleFragment.emulateShiftHeld(android.webkit.WebView):void");
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (Integer.parseInt(Build.VERSION.SDK) < 14) {
            menu.add(1, 1, 0, "复制").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() != 1) {
                        return false;
                    }
                    AraticleFragment.selectAndCopyText(AraticleFragment.this.mWebView);
                    return false;
                }
            });
        }
        menu.add(1, 2, 0, "刷新").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == 2) {
                    DBHelper.getDBHelper().delete("readitem", "n_mark=?", new String[]{AraticleFragment.this.item.n_mark});
                    try {
                        AraticleFragment.this.mWebView.clearCache(false);
                        AraticleFragment.this.mWebView.clearHistory();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    AraticleFragment.this.mWebView.loadData("", "", "");
                }
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}
