package com.palmtrends.basefragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.baseview.XListView;
import com.palmtrends.controll.R;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Entity;
import com.palmtrends.loadimage.Utils;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LoadMoreListFragment<T extends Entity> extends BaseFragment<T> implements XListView.IXListViewListener {
    private static final String KEY_CONTENT_ADCONTROLL = "SinglerListFragment:layout_adcontroll";
    private static final String KEY_CONTENT_LAYOUT_ID = "SinglerListFragment:layout_id";
    private static final String KEY_CONTENT_NODTA_TIP = "SinglerListFragment:nodata_tip";
    public ProgressBar footer_pb;
    public TextView footer_text;
    public int layout_id = R.layout.list_loadmoresinglerlist;
    public LinearLayout mContainers;
    public FrameLayout.LayoutParams mHead_Layout;
    public LinearLayout.LayoutParams mIcon_Layout;
    public View mList_footer;
    public XListView mListview;
    public View mLoading;
    public View mLoading_hasdate;
    public View mLoading_nodate;
    public View mMain_layout;
    public int nodata_tip = R.string.no_data;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public View onCreateView(LayoutInflater inflater, ViewGroup vgroup, Bundle savedInstanceState) {
        if ((this.mParttype == null || this.mParttype.equals("")) && savedInstanceState != null && savedInstanceState.containsKey(KEY_CONTENT_NODTA_TIP + getId())) {
            this.nodata_tip = savedInstanceState.getInt(KEY_CONTENT_NODTA_TIP + getId());
            this.layout_id = savedInstanceState.getInt(KEY_CONTENT_LAYOUT_ID + getId());
            this.isShowAD = savedInstanceState.getBoolean(KEY_CONTENT_ADCONTROLL + getId());
        }
        super.onCreateView(inflater, vgroup, savedInstanceState);
        if (this.mMain_layout == null) {
            this.mContainers = new LinearLayout(this.mContext);
            this.mMain_layout = inflater.inflate(this.layout_id, (ViewGroup) null);
            initListFragment(inflater);
            this.mContainers.addView(this.mMain_layout);
        } else {
            this.mContainers.removeAllViews();
            this.mContainers = new LinearLayout(getActivity());
            this.mContainers.addView(this.mMain_layout);
        }
        return this.mContainers;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_CONTENT_NODTA_TIP + getId(), this.nodata_tip);
        outState.putInt(KEY_CONTENT_LAYOUT_ID + getId(), this.layout_id);
        outState.putBoolean(KEY_CONTENT_ADCONTROLL + getId(), this.isShowAD);
        super.onSaveInstanceState(outState);
    }

    public void initListFragment(LayoutInflater inflater) {
        this.mIcon_Layout = new LinearLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 99) / 480, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 88) / 480);
        findView();
        addListener();
        initData();
    }

    public void findView() {
        this.mListview = (XListView) this.mMain_layout.findViewById(R.id.list_id_list);
        this.mListview.setTag(this.mOldtype);
        this.mListview.setPullLoadEnable(true);
        this.mListview.setXListViewListener(this);
        this.mLoading = this.mMain_layout.findViewById(R.id.loading);
        this.mLoading_nodate = this.mMain_layout.findViewById(R.id.laoding_no_date);
        this.mLoading_hasdate = this.mMain_layout.findViewById(R.id.laoding_has_date);
        this.mList_footer = this.mListview.mFooterView;
        this.footer_pb = (ProgressBar) this.mList_footer.findViewById(R.id.xlistview_footer_progressbar);
        this.footer_text = (TextView) this.mList_footer.findViewById(R.id.xlistview_footer_hint_textview);
    }

    public void initfooter() {
        this.mHandler.post(new Runnable() {
            public void run() {
                LoadMoreListFragment.this.footer_text.setText(LoadMoreListFragment.this.mContext.getResources().getString(R.string.xlistview_footer_hint_normal));
                LoadMoreListFragment.this.footer_pb.setVisibility(8);
            }
        });
    }

    public boolean dealClick(Entity entity, int position) {
        return false;
    }

    public void addListener() {
        this.mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
             arg types: [java.lang.String, T]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent} */
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
                T li = (Entity) parent.getItemAtPosition(position);
                if (!LoadMoreListFragment.this.dealClick(li, position) && LoadMoreListFragment.this.mlistAdapter != null && LoadMoreListFragment.this.mlistAdapter.datas.size() > 0) {
                    Intent intent = new Intent();
                    intent.setAction(LoadMoreListFragment.this.mContext.getResources().getString(R.string.activity_article));
                    intent.putExtra("item", (Serializable) li);
                    intent.putExtra("items", (Serializable) LoadMoreListFragment.this.mlistAdapter.datas);
                    intent.putExtra("position", position - LoadMoreListFragment.this.mListview.getHeaderViewsCount());
                    LoadMoreListFragment.this.startActivity(intent);
                }
            }
        });
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                LoadMoreListFragment.this.mLoading.setVisibility(8);
                switch (msg.what) {
                    case FinalVariable.update /*1001*/:
                        LoadMoreListFragment.this.mLoading_nodate.setVisibility(8);
                        LoadMoreListFragment.this.update();
                        return;
                    case FinalVariable.remove_footer /*1002*/:
                        LoadMoreListFragment.this.mLoading_nodate.setVisibility(8);
                        if (!(LoadMoreListFragment.this.mListview == null || LoadMoreListFragment.this.mList_footer == null || LoadMoreListFragment.this.mListview.getFooterViewsCount() <= 0)) {
                            try {
                                LoadMoreListFragment.this.mListview.removeFooterView(LoadMoreListFragment.this.mList_footer);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        LoadMoreListFragment.this.initfooter();
                        return;
                    case FinalVariable.change /*1003*/:
                    case FinalVariable.deletefoot /*1005*/:
                    case FinalVariable.first_load /*1008*/:
                    case FinalVariable.load_image /*1009*/:
                    case FinalVariable.other /*1010*/:
                    default:
                        return;
                    case FinalVariable.error /*1004*/:
                        LoadMoreListFragment.this.initfooter();
                        if (LoadMoreListFragment.this.mData == null || LoadMoreListFragment.this.mData.list.size() <= 0) {
                            LoadMoreListFragment.this.mLoading_nodate.setVisibility(0);
                            LoadMoreListFragment.this.mLoading_nodate.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    new Thread(new Runnable() {
                                        public void run() {
                                            LoadMoreListFragment.this.reFlush();
                                        }
                                    }).start();
                                    LoadMoreListFragment.this.mLoading_nodate.setVisibility(8);
                                }
                            });
                            LoadMoreListFragment.this.mLoading.setVisibility(0);
                        }
                        if (!Utils.isNetworkAvailable(LoadMoreListFragment.this.mContext)) {
                            Utils.showToast(R.string.network_error);
                            LoadMoreListFragment.this.mLoading_nodate.setVisibility(0);
                            return;
                        } else if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            LoadMoreListFragment.this.mLoading_nodate.setVisibility(0);
                            return;
                        } else {
                            Utils.showToast(LoadMoreListFragment.this.nodata_tip);
                            LoadMoreListFragment.this.mLoading_nodate.setVisibility(0);
                            return;
                        }
                    case FinalVariable.addfoot /*1006*/:
                        LoadMoreListFragment.this.mLoading_nodate.setVisibility(8);
                        if (LoadMoreListFragment.this.mListview != null && LoadMoreListFragment.this.mList_footer != null && LoadMoreListFragment.this.mListview.getFooterViewsCount() == 0) {
                            LoadMoreListFragment.this.mListview.addFooterView(LoadMoreListFragment.this.mList_footer);
                            return;
                        }
                        return;
                    case FinalVariable.nomore /*1007*/:
                        LoadMoreListFragment.this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                        if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            LoadMoreListFragment.this.mLoading_nodate.setVisibility(0);
                            return;
                        }
                        Utils.showToast(LoadMoreListFragment.this.nodata_tip);
                        LoadMoreListFragment.this.mLoading_nodate.setVisibility(0);
                        return;
                    case FinalVariable.first_update /*1011*/:
                        LoadMoreListFragment.this.mLoading_nodate.setVisibility(8);
                        if (!(LoadMoreListFragment.this.mlistAdapter == null || LoadMoreListFragment.this.mlistAdapter.datas == null)) {
                            LoadMoreListFragment.this.mlistAdapter.datas.clear();
                            LoadMoreListFragment.this.mlistAdapter = null;
                        }
                        LoadMoreListFragment.this.update();
                        LoadMoreListFragment.this.fillAd(LoadMoreListFragment.this.mlistAdapter);
                        return;
                }
            }
        };
    }

    public void onupdate(Data data) {
    }

    public void update() {
        initfooter();
        this.mLoading.setVisibility(8);
        if (this.mData != null) {
            onupdate(this.mData);
            this.isloading = false;
            if (this.mlistAdapter == null) {
                this.mlistAdapter = new BaseFragment.ListAdapter(this.mData.list, this.mParttype);
                this.mListview.setAdapter((ListAdapter) this.mlistAdapter);
                if (this.mData.list.size() < this.mFooter_limit) {
                    this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                } else {
                    this.mHandler.sendEmptyMessage(FinalVariable.addfoot);
                }
            } else {
                this.mlistAdapter.addDatas(this.mData.list);
                if (this.mData.list.size() < this.mFooter_limit) {
                    this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                } else {
                    this.mHandler.sendEmptyMessage(FinalVariable.addfoot);
                }
            }
        }
    }

    public class GetDataTask extends AsyncTask<String, String, Boolean> {
        public GetDataTask() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... params) {
            if (LoadMoreListFragment.this.isloading) {
                return false;
            }
            if (params == null) {
                LoadMoreListFragment.this.reFlush();
            } else {
                try {
                    LoadMoreListFragment.this.isloading = true;
                    LoadMoreListFragment.this.loadMore();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            if (result.booleanValue()) {
                LoadMoreListFragment.this.onLoad();
            }
            super.onPostExecute((Object) result);
        }
    }

    public View getListHeadview(T t) {
        return null;
    }

    public View getListItemview(View view, Entity entity, int position) {
        return null;
    }

    public void onRefresh() {
        new GetDataTask().execute((Object[]) null);
    }

    public void onLoadMore() {
        if (this.mListview.getFooterViewsCount() > 0) {
            new GetDataTask().execute("loadmore");
        }
    }

    /* access modifiers changed from: private */
    public void onLoad() {
        this.mListview.stopRefresh();
        this.mListview.stopLoadMore();
        String time = this.sdf.format(new Date());
        PerfHelper.setInfo("pull_update_time" + this.mOldtype, time);
        this.mListview.setRefreshTime(time);
    }
}
