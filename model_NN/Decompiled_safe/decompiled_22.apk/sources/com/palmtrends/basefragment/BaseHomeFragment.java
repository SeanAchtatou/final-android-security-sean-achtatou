package com.palmtrends.basefragment;

import android.os.AsyncTask;
import android.view.View;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class BaseHomeFragment extends LoadMoreListFragment<Listitem> {
    public ArrayList<Listitem> ads;
    public boolean isasload;

    public Data getDataFromDB(String oldtype, int page, int count, String parttype) throws Exception {
        String json = DNDataSource.list_FromDB(oldtype, page, count, parttype);
        if (json == null || "".equals(json) || "null".equals(json)) {
            return null;
        }
        return parseHomeData(json);
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return null;
        }
        String json = DNDataSource.list_FromNET(url, oldtype, page, count, parttype, isfirst);
        Data data = parseHomeData(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public Data parseHomeData(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has(Constants.SINA_CODE) || jsonobj.getInt(Constants.SINA_CODE) != 0) {
            JSONArray jsonay = jsonobj.getJSONArray("list");
            if (jsonobj.has("head")) {
                try {
                    JSONObject jsonhead = jsonobj.getJSONObject("head");
                    Listitem head1 = new Listitem();
                    head1.nid = jsonhead.getString(LocaleUtil.INDONESIAN);
                    head1.title = jsonhead.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                    head1.des = jsonhead.getString("des");
                    head1.icon = jsonhead.getString("icon");
                    head1.getMark();
                    head1.ishead = "true";
                    data.obj = head1;
                    data.headtype = 1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                o.title = obj.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                try {
                    if (obj.has("des")) {
                        o.des = obj.getString("des");
                    }
                    if (obj.has("adddate")) {
                        o.u_date = obj.getString("adddate");
                    }
                    o.icon = obj.getString("icon");
                } catch (Exception e2) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }
        throw new DataException(jsonobj.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG));
    }

    public class InitAD extends AsyncTask<Void, Void, ArrayList<Listitem>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((ArrayList<Listitem>) ((ArrayList) obj));
        }

        public InitAD() {
        }

        /* access modifiers changed from: protected */
        public ArrayList<Listitem> doInBackground(Void... params) {
            BaseHomeFragment.this.isasload = true;
            if (BaseHomeFragment.this.ads != null && BaseHomeFragment.this.ads.size() > 0) {
                return BaseHomeFragment.this.ads;
            }
            BaseHomeFragment baseHomeFragment = BaseHomeFragment.this;
            ArrayList<Listitem> addHomeAd = ClientShowAd.addHomeAd();
            baseHomeFragment.ads = addHomeAd;
            return addHomeAd;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(ArrayList<Listitem> result) {
            if (result != null && result.size() > 0) {
                BaseHomeFragment.this.mlistAdapter.adview = new View(BaseHomeFragment.this.mContext);
                BaseHomeFragment.this.mlistAdapter.datas.addAll(0, result);
                BaseHomeFragment.this.mlistAdapter.notifyDataSetChanged();
            }
            BaseHomeFragment.this.isasload = false;
            super.onPostExecute((Object) result);
        }
    }
}
