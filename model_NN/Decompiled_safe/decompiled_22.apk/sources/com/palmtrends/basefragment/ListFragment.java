package com.palmtrends.basefragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.baseview.PullToRefreshListView;
import com.palmtrends.controll.R;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Entity;
import com.palmtrends.loadimage.Utils;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.io.Serializable;

public class ListFragment<T extends Entity> extends BaseFragment<T> {
    private static final String KEY_CONTENT_ADCONTROLL = "SinglerListFragment:layout_adcontroll";
    private static final String KEY_CONTENT_LAYOUT_ID = "SinglerListFragment:layout_id";
    private static final String KEY_CONTENT_NODTA_TIP = "SinglerListFragment:nodata_tip";
    public ProgressBar footer_pb;
    public TextView footer_text;
    public int layout_id = R.layout.list_singlerlist;
    public LinearLayout mContainers;
    public FrameLayout.LayoutParams mHead_Layout;
    public LinearLayout.LayoutParams mIcon_Layout;
    public View mList_footer;
    public ListView mListview;
    public View mLoading;
    public View mMain_layout;
    public int nodata_tip = R.string.no_data;

    public View onCreateView(LayoutInflater inflater, ViewGroup vgroup, Bundle savedInstanceState) {
        if ((this.mParttype == null || this.mParttype.equals("")) && savedInstanceState != null && savedInstanceState.containsKey(KEY_CONTENT_NODTA_TIP)) {
            this.nodata_tip = savedInstanceState.getInt(KEY_CONTENT_NODTA_TIP);
            this.layout_id = savedInstanceState.getInt(KEY_CONTENT_LAYOUT_ID);
            this.isShowAD = savedInstanceState.getBoolean(KEY_CONTENT_ADCONTROLL);
        }
        super.onCreateView(inflater, vgroup, savedInstanceState);
        if (this.mMain_layout == null) {
            this.mContainers = new LinearLayout(this.mContext);
            this.mMain_layout = inflater.inflate(this.layout_id, (ViewGroup) null);
            initListFragment(inflater);
            this.mContainers.addView(this.mMain_layout);
        } else {
            this.mContainers.removeAllViews();
            this.mContainers = new LinearLayout(getActivity());
            this.mContainers.addView(this.mMain_layout);
        }
        return this.mContainers;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_CONTENT_NODTA_TIP, this.nodata_tip);
        outState.putInt(KEY_CONTENT_LAYOUT_ID, this.layout_id);
        outState.putBoolean(KEY_CONTENT_ADCONTROLL, this.isShowAD);
        super.onSaveInstanceState(outState);
    }

    public void initListFragment(LayoutInflater inflater) {
        this.mList_footer = inflater.inflate(R.layout.footer, (ViewGroup) null);
        this.mIcon_Layout = new LinearLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 99) / 480, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 88) / 480);
        findView();
        addListener();
        initData();
    }

    public void findView() {
        this.mListview = (ListView) this.mMain_layout.findViewById(R.id.list_id_list);
        this.mLoading = this.mMain_layout.findViewById(R.id.loading);
        this.footer_pb = (ProgressBar) this.mList_footer.findViewById(R.id.footer_pb);
        this.footer_text = (TextView) this.mList_footer.findViewById(R.id.footer_text);
        this.mListview.addFooterView(this.mList_footer);
    }

    public void initfooter() {
        this.mHandler.post(new Runnable() {
            public void run() {
                ListFragment.this.footer_text.setText(ListFragment.this.mContext.getResources().getString(R.string.loading_n));
                ListFragment.this.footer_pb.setVisibility(8);
            }
        });
    }

    public boolean dealClick(T t, int position) {
        return false;
    }

    public void addListener() {
        this.mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
             arg types: [java.lang.String, T]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent} */
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long arg3) {
                T li = (Entity) parent.getItemAtPosition(position);
                if (!ListFragment.this.dealClick(li, position) && ListFragment.this.mlistAdapter != null && ListFragment.this.mlistAdapter.datas.size() > 0) {
                    Intent intent = new Intent();
                    intent.setAction(ListFragment.this.mContext.getResources().getString(R.string.activity_article));
                    intent.putExtra("item", (Serializable) li);
                    intent.putExtra("items", (Serializable) ListFragment.this.mlistAdapter.datas);
                    intent.putExtra("position", position - ListFragment.this.mListview.getHeaderViewsCount());
                    ListFragment.this.startActivity(intent);
                }
            }
        });
        this.mList_footer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!ListFragment.this.isloading) {
                    try {
                        ListFragment.this.footer_text.setText(ListFragment.this.mContext.getResources().getString(R.string.loading));
                        ListFragment.this.footer_pb.setVisibility(0);
                        ListFragment.this.isloading = true;
                        new LoadDataTask().execute((Object[]) null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        try {
            ((PullToRefreshListView) this.mListview).setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
                public void onRefresh() {
                    new GetDataTask().execute(new Void[0]);
                }
            });
        } catch (Exception e) {
        }
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                ListFragment.this.mLoading.setVisibility(8);
                switch (msg.what) {
                    case FinalVariable.update /*1001*/:
                        ListFragment.this.update();
                        return;
                    case FinalVariable.remove_footer /*1002*/:
                        if (!(ListFragment.this.mListview == null || ListFragment.this.mList_footer == null || ListFragment.this.mListview.getFooterViewsCount() <= 0)) {
                            try {
                                ListFragment.this.mListview.removeFooterView(ListFragment.this.mList_footer);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        ListFragment.this.initfooter();
                        return;
                    case FinalVariable.change /*1003*/:
                    case FinalVariable.deletefoot /*1005*/:
                    case FinalVariable.first_load /*1008*/:
                    case FinalVariable.load_image /*1009*/:
                    case FinalVariable.other /*1010*/:
                    default:
                        return;
                    case FinalVariable.error /*1004*/:
                        ListFragment.this.initfooter();
                        if (!Utils.isNetworkAvailable(ListFragment.this.mContext)) {
                            Utils.showToast(R.string.network_error);
                            return;
                        } else if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            return;
                        } else {
                            Utils.showToast(R.string.network_error);
                            return;
                        }
                    case FinalVariable.addfoot /*1006*/:
                        if (ListFragment.this.mListview != null && ListFragment.this.mList_footer != null && ListFragment.this.mListview.getFooterViewsCount() == 0) {
                            ListFragment.this.mListview.addFooterView(ListFragment.this.mList_footer);
                            return;
                        }
                        return;
                    case FinalVariable.nomore /*1007*/:
                        ListFragment.this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                        if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            return;
                        } else {
                            Utils.showToast(ListFragment.this.nodata_tip);
                            return;
                        }
                    case FinalVariable.first_update /*1011*/:
                        if (!(ListFragment.this.mlistAdapter == null || ListFragment.this.mlistAdapter.datas == null)) {
                            ListFragment.this.mlistAdapter.datas.clear();
                            ListFragment.this.mlistAdapter = null;
                        }
                        ListFragment.this.update();
                        ListFragment.this.fillAd(ListFragment.this.mlistAdapter);
                        return;
                }
            }
        };
    }

    public void onupdate(Data data) {
    }

    public void update() {
        initfooter();
        this.mLoading.setVisibility(8);
        if (this.mData != null) {
            onupdate(this.mData);
            this.mListview.setTag(this.mOldtype);
            this.isloading = false;
            if (this.mlistAdapter == null) {
                this.mlistAdapter = new BaseFragment.ListAdapter(this.mData.list, this.mParttype);
                this.mListview.setAdapter((ListAdapter) this.mlistAdapter);
                if (this.mData.list.size() < this.mFooter_limit) {
                    this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                } else {
                    this.mHandler.sendEmptyMessage(FinalVariable.addfoot);
                }
            } else {
                this.mlistAdapter.addDatas(this.mData.list);
                if (this.mData.list.size() < this.mFooter_limit) {
                    this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                } else {
                    this.mHandler.sendEmptyMessage(FinalVariable.addfoot);
                }
            }
        }
    }

    public class GetDataTask extends AsyncTask<Void, Void, String[]> {
        public GetDataTask() {
        }

        /* access modifiers changed from: protected */
        public String[] doInBackground(Void... params) {
            ListFragment.this.reFlush();
            return new String[0];
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String[] result) {
            if (ListFragment.this.mListview instanceof PullToRefreshListView) {
                ((PullToRefreshListView) ListFragment.this.mListview).onRefreshComplete(ListFragment.this.mOldtype);
            }
            super.onPostExecute((Object) result);
        }
    }

    public class LoadDataTask extends AsyncTask<Void, Void, String[]> {
        public LoadDataTask() {
        }

        /* access modifiers changed from: protected */
        public String[] doInBackground(Void... params) {
            ListFragment.this.loadMore();
            return new String[0];
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String[] result) {
            super.onPostExecute((Object) result);
        }
    }

    public View getListHeadview(T t) {
        return null;
    }

    public View getListItemview(View view, T t, int position) {
        return null;
    }
}
