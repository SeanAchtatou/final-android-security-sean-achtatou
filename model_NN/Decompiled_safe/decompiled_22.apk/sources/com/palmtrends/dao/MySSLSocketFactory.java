package com.palmtrends.dao;

import android.os.Build;
import com.ibm.mqtt.MqttUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.loadimage.ImageFetcher;
import com.utils.FinalVariable;
import com.utils.JniUtils;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

public class MySSLSocketFactory extends SSLSocketFactory {
    private static final int REQUEST_TIMEOUT = 15000;
    private static final int REQUEST_TIMEOUT_SUBMIT = 30000;
    private static final int SO_TIMEOUT = 15000;
    private static final int SO_TIMEOUTT_SUBMIT = 30000;
    SSLContext sslContext = SSLContext.getInstance("TLS");

    public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
        super(truststore);
        TrustManager tm = new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };
        this.sslContext.init(null, new TrustManager[]{tm}, null);
    }

    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
        return this.sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
    }

    public Socket createSocket() throws IOException {
        return this.sslContext.getSocketFactory().createSocket();
    }

    public static HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, MqttUtils.STRING_ENCODING);
            HttpConnectionParams.setConnectionTimeout(params, 15000);
            HttpConnectionParams.setSoTimeout(params, 15000);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme(ImageFetcher.HTTP_CACHE_DIR, PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));
            DefaultHttpClient httpclient = new DefaultHttpClient(new ThreadSafeClientConnManager(params, registry), params);
            httpclient.getParams().setParameter(HttpMethodParams.USER_AGENT, "Palmtrends (Android " + Build.MODEL + "; OS " + Build.VERSION.RELEASE + "; " + ShareApplication.share.getPackageName() + ")");
            return httpclient;
        } catch (Exception e) {
            BasicHttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 15000);
            HttpConnectionParams.setSoTimeout(httpParams, 15000);
            DefaultHttpClient httpclient2 = new DefaultHttpClient(httpParams);
            httpclient2.getParams().setParameter(HttpMethodParams.USER_AGENT, "Palmtrends (Android " + Build.MODEL + "; OS " + Build.VERSION.RELEASE + "; " + ShareApplication.share.getPackageName() + ")");
            return httpclient2;
        }
    }

    public static HttpClient getNewHttpClientsubmit() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, MqttUtils.STRING_ENCODING);
            HttpConnectionParams.setConnectionTimeout(params, 30000);
            HttpConnectionParams.setSoTimeout(params, 30000);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme(ImageFetcher.HTTP_CACHE_DIR, PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));
            DefaultHttpClient httpclient = new DefaultHttpClient(new ThreadSafeClientConnManager(params, registry), params);
            httpclient.getParams().setParameter(HttpMethodParams.USER_AGENT, "Palmtrends (Android " + Build.MODEL + "; OS " + Build.VERSION.RELEASE + "; " + ShareApplication.share.getPackageName() + ")");
            return httpclient;
        } catch (Exception e) {
            BasicHttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
            HttpConnectionParams.setSoTimeout(httpParams, 30000);
            DefaultHttpClient httpclient2 = new DefaultHttpClient(httpParams);
            httpclient2.getParams().setParameter(HttpMethodParams.USER_AGENT, "Palmtrends (Android " + Build.MODEL + "; OS " + Build.VERSION.RELEASE + "; " + ShareApplication.share.getPackageName() + ")");
            return httpclient2;
        }
    }

    public static String getinfo_submit(String url, List<NameValuePair> param) throws Exception {
        param.add(new BasicNameValuePair("mobile", Build.MODEL));
        param.add(new BasicNameValuePair("pid", FinalVariable.pid));
        param.add(new BasicNameValuePair("e", JniUtils.getkey()));
        HttpClient httpclient = getNewHttpClientsubmit();
        HttpPost hp = new HttpPost(url);
        if (ShareApplication.debug) {
            String str = url;
            Iterator<NameValuePair> it = param.iterator();
            while (it.hasNext()) {
                BasicNameValuePair b = (NameValuePair) it.next();
                str = String.valueOf(str) + "&" + b.getName() + "=" + b.getValue();
            }
            System.out.println(str);
        }
        hp.setEntity(new UrlEncodedFormEntity(param, "utf-8"));
        String json = EntityUtils.toString(httpclient.execute(hp).getEntity(), "utf-8");
        if (json == null || json.startsWith("{") || json.startsWith("[")) {
            return json;
        }
        return json.substring(1, json.length());
    }

    public static String getinfo(String url, List<NameValuePair> param) throws Exception {
        param.add(new BasicNameValuePair("mobile", Build.MODEL));
        param.add(new BasicNameValuePair("pid", FinalVariable.pid));
        param.add(new BasicNameValuePair("e", JniUtils.getkey()));
        HttpClient httpclient = getNewHttpClient();
        HttpPost hp = new HttpPost(url);
        if (ShareApplication.debug) {
            String str = url;
            Iterator<NameValuePair> it = param.iterator();
            while (it.hasNext()) {
                BasicNameValuePair b = (NameValuePair) it.next();
                str = String.valueOf(str) + "&" + b.getName() + "=" + b.getValue();
            }
            System.out.println(str);
        }
        hp.setEntity(new UrlEncodedFormEntity(param, "utf-8"));
        String json = EntityUtils.toString(httpclient.execute(hp).getEntity(), "utf-8");
        if (json == null || json.startsWith("{") || json.startsWith("[")) {
            return json;
        }
        return json.substring(1, json.length());
    }

    public static String getinfo_Get(String url) throws Exception {
        String json = EntityUtils.toString(getNewHttpClient().execute(new HttpGet(url)).getEntity(), "utf-8");
        if (json == null || json.startsWith("{") || json.startsWith("[")) {
            return json;
        }
        return json.substring(1, json.length());
    }
}
