package com.palmtrends.dao;

import android.content.res.Resources;
import com.palmtrends.app.ShareApplication;

public class Urls {
    public static final String alarm_time = "dbms.palmtrends.com/api/dbms2.php";
    public static String app_api = (String.valueOf(main) + "/api_v2.php");
    public static String draw_content_url = null;
    public static String draw_list = null;
    public static final String get_checkversion = "http://dbms.palmtrends.com/api/dbms2.php?action=dbms.checkversion";
    public static final String get_run_time = "http://dbms.palmtrends.com/api/dbms2.php?action=dbms.datetime.clientrunning";
    public static final String get_uid = "http://dbms.palmtrends.com/api/dbms2.php?action=dbms.getuid";
    public static String home = null;
    public static String list_url = null;
    public static String main = null;
    public static String pinglun_url = null;
    public static String review_url = null;
    public static final String sina_list = "http://push.cms.palmtrends.com/wb/api_v2.php?action=weiboinfo&sname=sina&wbid=";
    public static String suggest_url;

    static {
        home = "?action=top";
        Resources res = ShareApplication.share.getResources();
        main = res.getString(R.string.main);
        String[] str = {main};
        home = res.getString(R.string.home, str);
        list_url = res.getString(R.string.list_url, str);
        draw_list = res.getString(R.string.draw_list, str);
        draw_content_url = res.getString(R.string.draw_content_url, str);
        review_url = res.getString(R.string.review_url, str);
        suggest_url = res.getString(R.string.suggest_url, str);
        pinglun_url = res.getString(R.string.pinglun_url, str);
    }
}
