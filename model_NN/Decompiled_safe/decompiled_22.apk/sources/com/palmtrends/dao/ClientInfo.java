package com.palmtrends.dao;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import com.city_life.part_asynctask.UploadUtils;
import com.imofan.android.basic.MFStatInfo;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.entity.Entity;
import com.palmtrends.entity.info;
import com.palmtrends.loadimage.Utils;
import com.palmtrends.push.MPushService;
import com.utils.FinalVariable;
import com.utils.JniUtils;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import sina_weibo.Constants;

public class ClientInfo {

    public static class Version extends Entity {
        public String alert;
        public String available;
        public String code;
        public String force;
        public String update_url;
    }

    public static void submit() {
        final List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("action", "local_tip"));
        new Thread(new Runnable() {
            public void run() {
                try {
                    MySSLSocketFactory.getinfo(Urls.alarm_time, param);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static Version check_update(String pid) throws Exception {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("ver", ShareApplication.getVersion()));
        param.add(new BasicNameValuePair("pid", pid));
        param.add(new BasicNameValuePair(Constants.SINA_UID, PerfHelper.getStringData(PerfHelper.P_USERID)));
        param.add(new BasicNameValuePair("client", "android"));
        param.add(new BasicNameValuePair(com.tencent.tauth.Constants.PARAM_PLATFORM, "a"));
        param.add(new BasicNameValuePair("mobile", Build.MODEL));
        param.add(new BasicNameValuePair("e", JniUtils.getkey()));
        return (Version) JsonDao.getJsonObject(Urls.get_checkversion, param, Version.class);
    }

    public static void sendClient_UserInfo() throws Exception {
        String android_id = Settings.Secure.getString(ShareApplication.share.getContentResolver(), "android_id");
        WifiInfo mWifiInfo = ((WifiManager) ShareApplication.share.getSystemService("wifi")).getConnectionInfo();
        PerfHelper.setInfo(MPushService.PREF_DEVICE_ID, android_id);
        String android_id2 = String.valueOf(mWifiInfo.getMacAddress()) + FinalVariable.pid + android_id;
        if (android_id2.length() > 40) {
            android_id2 = android_id2.substring(0, 40);
        }
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("pid", FinalVariable.pid));
        param.add(new BasicNameValuePair("device_id", android_id2));
        param.add(new BasicNameValuePair(com.tencent.tauth.Constants.PARAM_PLATFORM, "a"));
        param.add(new BasicNameValuePair("mobile", Build.MODEL));
        HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
        HttpPost hp = new HttpPost(Urls.get_uid);
        try {
            hp.setEntity(new UrlEncodedFormEntity(param, "utf-8"));
            JSONObject jo = new JSONObject(EntityUtils.toString(httpclient.execute(hp).getEntity(), "utf-8"));
            if (jo.getString(Constants.SINA_CODE).equals(UploadUtils.FAILURE)) {
                PerfHelper.setInfo(PerfHelper.P_USERID, jo.getString(Constants.SINA_UID));
            } else {
                Utils.showToast(R.string.server_error);
            }
        } catch (Exception e) {
            PerfHelper.setInfo(PerfHelper.P_USERID, "11111111");
        }
    }

    public static void senduser_feedback(String email, String comment, final Handler handler) {
        final List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("suggest", comment));
        param.add(new BasicNameValuePair(MFStatInfo.KEY_OS_VERSION, Build.VERSION.RELEASE));
        param.add(new BasicNameValuePair("email", email));
        new Thread() {
            public void run() {
                try {
                    param.add(new BasicNameValuePair("client_ver", ShareApplication.getVersion()));
                    JSONObject obj = new JSONObject(MySSLSocketFactory.getinfo(Urls.suggest_url, param));
                    info i = new info();
                    i.code = obj.getString(Constants.SINA_CODE);
                    i.msg = obj.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG);
                    if (UploadUtils.SUCCESS.equals(i.code)) {
                        handler.sendEmptyMessage(2);
                    } else {
                        handler.sendEmptyMessage(1);
                    }
                } catch (Exception e) {
                    handler.sendEmptyMessage(2);
                }
            }
        }.start();
    }

    public static void check_update(final Context context) {
        new Thread() {
            public void run() {
                try {
                    final Version v = ClientInfo.check_update(FinalVariable.pid);
                    if (v != null && UploadUtils.FAILURE.equals(v.available)) {
                        Handler handler = Utils.h;
                        final Context context = context;
                        handler.post(new Runnable() {
                            public void run() {
                                if (UploadUtils.SUCCESS.equals(v.force)) {
                                    AlertDialog.Builder negativeButton = new AlertDialog.Builder(context).setMessage(v.alert).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    int i = R.string.checkupdate_getnew;
                                    final Version version = v;
                                    final Context context = context;
                                    negativeButton.setPositiveButton(i, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (!version.update_url.startsWith("http://")) {
                                                Utils.showToast("参数错误");
                                                return;
                                            }
                                            try {
                                                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(version.update_url)));
                                            } catch (Exception e) {
                                                Utils.showToast("请安装浏览起");
                                                e.printStackTrace();
                                            }
                                        }
                                    }).show();
                                    return;
                                }
                                AlertDialog.Builder message = new AlertDialog.Builder(context).setMessage(v.alert);
                                int i2 = R.string.checkupdate_getnew;
                                final Version version2 = v;
                                final Context context2 = context;
                                message.setNegativeButton(i2, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (!version2.update_url.startsWith("http://")) {
                                            Utils.showToast("参数错误");
                                            return;
                                        }
                                        try {
                                            context2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(version2.update_url)));
                                        } catch (Exception e) {
                                            Utils.showToast("请安装浏览起");
                                            e.printStackTrace();
                                        }
                                    }
                                }).show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}
