package com.palmtrends.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.palmtrends.app.ShareApplication;
import com.tencent.tauth.Constants;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DBHelper extends SQLiteOpenHelper {
    private static String DATABASE_NAME = null;
    private static final int DATABASE_VERSION = 2;
    public static final String FAV_FLAG = "FAV_TAG";
    private static DBHelper mDBHelper;
    private SQLiteDatabase db;

    public static DBHelper getDBHelper() {
        DATABASE_NAME = String.valueOf(ShareApplication.share.getPackageName()) + "_db";
        if (mDBHelper == null) {
            mDBHelper = new DBHelper(ShareApplication.share);
        }
        return mDBHelper;
    }

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 2);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db2) {
        for (String sql : ShareApplication.share.getResources().getStringArray(R.array.appconfig_sql)) {
            db2.execSQL(sql);
        }
        this.db = db2;
    }

    public void onUpgrade(SQLiteDatabase db2, int oldVersion, int newVersion) {
        for (String sql : ShareApplication.share.getResources().getStringArray(R.array.appconfig_sql_upgrade)) {
            db2.execSQL(sql);
        }
        onCreate(db2);
    }

    public long insert(String url, String value, String listtype) {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        ContentValues cv = new ContentValues();
        cv.put(Constants.PARAM_URL, url);
        cv.put("infos", value);
        cv.put("listtype", listtype);
        return this.db.insert("listinfo", null, cv);
    }

    public long insert(String table, ContentValues cv) {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        return this.db.insert(table, null, cv);
    }

    public long counts(String tableName, String where) {
        long i = 0;
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        Cursor cursor = this.db.query(tableName, new String[]{"count(c_id) t"}, where, null, null, null, null);
        if (cursor.moveToFirst()) {
            i = cursor.getLong(cursor.getColumnIndex("t"));
        }
        cursor.close();
        return i;
    }

    public void update(String tablename, String where_name, String whereValue, String columnName, String value) {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        ContentValues cv = new ContentValues();
        cv.put(columnName, value);
        this.db.update(tablename, cv, String.valueOf(where_name) + "=?", new String[]{whereValue});
    }

    public void update(String tablename, String where, String[] whereValue, ContentValues cv) {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        this.db.update(tablename, cv, where, whereValue);
    }

    @Deprecated
    public void insert(List items, String tablename, Class c) throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        Field[] fs = c.getFields();
        this.db.beginTransaction();
        for (Object o : items) {
            StringBuffer sql = new StringBuffer();
            if (tablename == null) {
                sql.append("insert into " + c.getSimpleName().toLowerCase());
            } else {
                sql.append("insert into " + tablename);
            }
            StringBuffer sql_name = new StringBuffer();
            StringBuffer sql_value = new StringBuffer();
            for (Field f : fs) {
                if (!"c_id".equals(f.getName())) {
                    if (f.getType().getSimpleName().equals("String")) {
                        sql_name.append(String.valueOf(f.getName().toLowerCase()) + ",");
                        sql_value.append("'" + f.get(o) + "'" + ",");
                    } else {
                        sql_name.append(String.valueOf(f.getName().toLowerCase()) + ",");
                        sql_value.append(f.get(o) + ",");
                    }
                }
            }
            sql.append("(" + sql_name.toString().substring(0, sql_name.length() - 1) + ")").append(" ").append("values(").append(sql_value.toString().substring(0, sql_value.length() - 1)).append(");");
            try {
                this.db.execSQL(sql.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.db.setTransactionSuccessful();
        this.db.endTransaction();
    }

    @Deprecated
    public Object insertObject(Object o, String tablename) throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        Class c = o.getClass();
        Field[] fs = c.getFields();
        StringBuffer sql = new StringBuffer();
        if (tablename == null) {
            sql.append("insert into " + c.getSimpleName().toLowerCase());
        } else {
            sql.append("insert into " + tablename);
        }
        StringBuffer sql_name = new StringBuffer();
        StringBuffer sql_value = new StringBuffer();
        for (Field f : fs) {
            if (!"c_id".equals(f.getName())) {
                if (f.getType().getSimpleName().equals("String")) {
                    sql_name.append(String.valueOf(f.getName().toLowerCase()) + ",");
                    sql_value.append("'" + f.get(o) + "'" + ",");
                } else {
                    sql_name.append(String.valueOf(f.getName().toLowerCase()) + ",");
                    sql_value.append(f.get(o) + ",");
                }
            }
        }
        sql.append("(" + sql_name.toString().substring(0, sql_name.length() - 1) + ")").append(" ").append("values(").append(sql_value.toString().substring(0, sql_value.length() - 1)).append(");");
        this.db.execSQL(sql.toString());
        return o;
    }

    public void delete(String tablename, String where, String[] values) {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        this.db.delete(tablename, where, values);
    }

    public String select(String tablename, String fieldName, String where, String[] values) {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        String value = "";
        Cursor cursor = this.db.rawQuery("select " + fieldName + " from " + tablename + " where " + where, values);
        if (fieldName.indexOf(",") != -1) {
            if (cursor.moveToFirst()) {
                for (int i = 0; i < fieldName.split(",").length; i++) {
                    value = String.valueOf(value) + cursor.getString(i) + ",";
                }
            }
        } else if (cursor.moveToFirst() && !cursor.isNull(0)) {
            value = cursor.getString(0);
        }
        cursor.close();
        return value;
    }

    public void deleteall(String[] array) {
        if (this.db == null) {
            this.db = getReadableDatabase();
        }
        for (String sql : array) {
            this.db.execSQL(sql);
        }
    }

    public ArrayList select(String tablename, Class c, String where, int page, int length) throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        this.db = getReadableDatabase();
        Cursor cursor = this.db.query(tablename, null, where, null, null, null, "c_id asc limit " + (page * length) + ", " + length);
        Field[] fs = c.getFields();
        ArrayList al = new ArrayList();
        while (cursor.moveToNext()) {
            Object o = c.newInstance();
            for (Field f : fs) {
                if (f.getType().getSimpleName().equals("Integer")) {
                    f.set(o, Integer.valueOf(cursor.getInt(cursor.getColumnIndex(f.getName().toLowerCase()))));
                } else if (f.getType().getSimpleName().equals("String")) {
                    f.set(o, cursor.getString(cursor.getColumnIndex(f.getName().toLowerCase())));
                }
            }
            al.add(o);
        }
        cursor.close();
        return al;
    }

    public void insert_apptime(String type, String s_time, String e_time, String mark, String aid, String open_mode) throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        this.db.execSQL("INSERT INTO apptime (type,starttime,endtime,mark,aid,open_mode) values('" + type + "','" + s_time + "','" + e_time + "','" + mark + "','" + aid + "','" + open_mode + "')");
    }

    public JSONArray get_apptime() throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        JSONArray jsonArray = new JSONArray();
        Cursor cursor = this.db.query("apptime", new String[]{"type", "starttime", "endtime", "aid", "open_mode"}, "mark='0'", null, null, null, null);
        if (cursor == null) {
            cursor.close();
            return null;
        } else if (cursor.moveToFirst()) {
            do {
                JSONObject js = new JSONObject();
                int type = cursor.getColumnIndex("type");
                int starttime = cursor.getColumnIndex("starttime");
                int endtime = cursor.getColumnIndex("endtime");
                int aid = cursor.getColumnIndex("aid");
                int open_mode = cursor.getColumnIndex("open_mode");
                try {
                    js.put("type", cursor.getString(type));
                    js.put("s_time", cursor.getString(starttime));
                    js.put("e_time", cursor.getString(endtime));
                    js.put("aid", cursor.getString(aid));
                    js.put("open_mode", cursor.getString(open_mode));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray.put(js);
            } while (cursor.moveToNext());
            cursor.close();
            return jsonArray;
        } else {
            cursor.close();
            return null;
        }
    }

    public void delete_apptime() throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        this.db.execSQL("delete from apptime where mark='0'");
    }

    public void up_appendtime(String starttime, String endtime, String mark) throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        this.db.execSQL("update apptime set endtime='" + endtime + "',mark='" + mark + "' where starttime='" + starttime + "'");
    }

    public void up_appendtime(String starttime, String endtime, String aid, String mark) throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        this.db.execSQL("update apptime set endtime='" + endtime + "',mark='" + mark + "' where starttime='" + starttime + "'and aid='" + aid + "'");
    }

    public void up_app_mark() throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        this.db.execSQL("update apptime set mark='0' where mark='1'");
    }

    public void initPart() throws Exception {
        if (this.db == null) {
            this.db = getWritableDatabase();
        }
        this.db.execSQL("update part_list set part_updatetime='00'");
    }
}
