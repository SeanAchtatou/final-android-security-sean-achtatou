package com.palmtrends.dao;

import android.util.Log;
import com.palmtrends.entity.part;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class JsonDao {
    public static Object getJsonObject(String url, List<NameValuePair> nameValuePairs, Class c) throws Exception {
        HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
        HttpPost httppost = new HttpPost(url);
        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "utf-8"));
        String strs = EntityUtils.toString(httpclient.execute(httppost).getEntity(), "utf-8");
        Object o = c.newInstance();
        Field[] fs = c.getFields();
        JSONObject obj = new JSONObject(strs);
        for (Field f : fs) {
            if (f.getType().getSimpleName().equals("Integer")) {
                f.set(o, obj.get(f.getName().toLowerCase()));
            } else if (f.getType().getSimpleName().equals("String")) {
                f.set(o, obj.getString(f.getName().toLowerCase()));
            }
        }
        httpclient.getConnectionManager().shutdown();
        return o;
    }

    public static void getPartList(String url, String[] parts) {
        try {
            String strs = MySSLSocketFactory.getinfo_Get(url);
            Log.i("liyuling", "二级菜单更新    json: " + strs);
            JSONObject json = new JSONObject(strs);
            DBHelper db = DBHelper.getDBHelper();
            for (String str : parts) {
                JSONArray ja = json.getJSONArray(str);
                int count = ja.length();
                if (count > 0) {
                    db.delete("part_list", "part_type=?", new String[]{str});
                }
                List<part> partList = new ArrayList<>();
                for (int i = 0; i < count; i++) {
                    JSONObject obj = ja.getJSONObject(i);
                    part o = new part();
                    o.part_sa = obj.getString("sa");
                    o.part_name = obj.getString(Constants.SINA_NAME);
                    o.part_type = str;
                    o.part_index = Integer.valueOf(i);
                    partList.add(o);
                }
                db.insert(partList, "part_list", part.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getInfo(String url) throws Exception {
        return MySSLSocketFactory.getinfo(url, new ArrayList<>());
    }
}
