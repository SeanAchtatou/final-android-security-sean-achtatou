package com.palmtrends.zoom;

import android.graphics.PointF;

public class ZoomAnimation implements Animation {
    private long animationLengthMS = 200;
    private boolean firstFrame = true;
    private float scaleDiff;
    private float startScale;
    private float startX;
    private float startY;
    private long totalTime = 0;
    private float touchX;
    private float touchY;
    private float xDiff;
    private float yDiff;
    private float zoom;
    private ZoomAnimationListener zoomAnimationListener;

    public boolean update(GestureImageView view, long time) {
        if (this.firstFrame) {
            this.firstFrame = false;
            this.startX = view.getImageX();
            this.startY = view.getImageY();
            this.startScale = view.getScale();
            this.scaleDiff = (this.zoom * this.startScale) - this.startScale;
            if (this.scaleDiff > 0.0f) {
                VectorF vector = new VectorF();
                vector.setStart(new PointF(this.touchX, this.touchY));
                vector.setEnd(new PointF(this.startX, this.startY));
                vector.calculateAngle();
                vector.length = this.zoom * vector.calculateLength();
                vector.calculateEndPoint();
                this.xDiff = vector.end.x - this.startX;
                this.yDiff = vector.end.y - this.startY;
            } else {
                this.xDiff = view.getCenterX() - this.startX;
                this.yDiff = view.getCenterY() - this.startY;
            }
        }
        this.totalTime += time;
        float ratio = ((float) this.totalTime) / ((float) this.animationLengthMS);
        if (ratio < 1.0f) {
            if (ratio > 0.0f) {
                float newScale = (this.scaleDiff * ratio) + this.startScale;
                float newX = (this.xDiff * ratio) + this.startX;
                float newY = (this.yDiff * ratio) + this.startY;
                if (this.zoomAnimationListener != null) {
                    this.zoomAnimationListener.onZoom(newScale, newX, newY);
                }
            }
            return true;
        }
        float newScale2 = this.scaleDiff + this.startScale;
        float newX2 = this.xDiff + this.startX;
        float newY2 = this.yDiff + this.startY;
        if (this.zoomAnimationListener == null) {
            return false;
        }
        this.zoomAnimationListener.onZoom(newScale2, newX2, newY2);
        this.zoomAnimationListener.onComplete();
        return false;
    }

    public void reset() {
        this.firstFrame = true;
        this.totalTime = 0;
    }

    public float getZoom() {
        return this.zoom;
    }

    public void setZoom(float zoom2) {
        this.zoom = zoom2;
    }

    public float getTouchX() {
        return this.touchX;
    }

    public void setTouchX(float touchX2) {
        this.touchX = touchX2;
    }

    public float getTouchY() {
        return this.touchY;
    }

    public void setTouchY(float touchY2) {
        this.touchY = touchY2;
    }

    public long getAnimationLengthMS() {
        return this.animationLengthMS;
    }

    public void setAnimationLengthMS(long animationLengthMS2) {
        this.animationLengthMS = animationLengthMS2;
    }

    public ZoomAnimationListener getZoomAnimationListener() {
        return this.zoomAnimationListener;
    }

    public void setZoomAnimationListener(ZoomAnimationListener zoomAnimationListener2) {
        this.zoomAnimationListener = zoomAnimationListener2;
    }
}
