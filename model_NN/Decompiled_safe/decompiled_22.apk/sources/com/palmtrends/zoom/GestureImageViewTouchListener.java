package com.palmtrends.zoom;

import android.graphics.PointF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.palmtrends.baseview.ImageDetailViewPager;

public class GestureImageViewTouchListener implements View.OnTouchListener {
    private float boundaryBottom = 0.0f;
    private float boundaryLeft = 0.0f;
    private float boundaryRight = 0.0f;
    private float boundaryTop = 0.0f;
    private boolean canDragX = false;
    private boolean canDragY = false;
    private int canvasHeight = 0;
    private int canvasWidth = 0;
    private float centerX = 0.0f;
    private float centerY = 0.0f;
    /* access modifiers changed from: private */
    public final PointF current = new PointF();
    private float currentScale = 1.0f;
    private int displayHeight;
    private int displayWidth;
    private float fitScaleHorizontal = 1.0f;
    private float fitScaleVertical = 1.0f;
    private FlingAnimation flingAnimation;
    private GestureDetector flingDetector;
    private FlingListener flingListener;
    private GestureImageView image;
    private int imageHeight;
    private GestureImageViewListener imageListener;
    private int imageWidth;
    /* access modifiers changed from: private */
    public boolean inZoom = false;
    private float initialDistance;
    private final PointF last = new PointF();
    private float lastScale = 1.0f;
    /* access modifiers changed from: private */
    public ImageDetailViewPager mPager;
    /* access modifiers changed from: private */
    public float maxScale = 5.0f;
    private final PointF midpoint = new PointF();
    /* access modifiers changed from: private */
    public float minScale = 0.25f;
    private MoveAnimation moveAnimation;
    private boolean multiTouch = false;
    private final PointF next = new PointF();
    private final PointF old = new PointF();
    /* access modifiers changed from: private */
    public View.OnClickListener onClickListener;
    private final VectorF pinchVector = new VectorF();
    private final VectorF scaleVector = new VectorF();
    private float startingScale = 0.0f;
    private GestureDetector tapDetector;
    private boolean touched = false;
    private ZoomAnimation zoomAnimation;

    public GestureImageViewTouchListener(final GestureImageView image2, int displayWidth2, int displayHeight2, ImageDetailViewPager pager) {
        this.image = image2;
        this.mPager = pager;
        this.displayWidth = displayWidth2;
        this.displayHeight = displayHeight2;
        this.centerX = ((float) displayWidth2) / 2.0f;
        this.centerY = ((float) displayHeight2) / 2.0f;
        this.imageWidth = image2.getImageWidth();
        this.imageHeight = image2.getImageHeight();
        this.startingScale = image2.getScale();
        this.currentScale = this.startingScale;
        this.lastScale = this.startingScale;
        this.boundaryRight = (float) displayWidth2;
        this.boundaryBottom = (float) displayHeight2;
        this.boundaryLeft = 0.0f;
        this.boundaryTop = 0.0f;
        this.next.x = image2.getImageX();
        this.next.y = image2.getImageY();
        this.flingListener = new FlingListener();
        this.flingAnimation = new FlingAnimation();
        this.zoomAnimation = new ZoomAnimation();
        this.moveAnimation = new MoveAnimation();
        this.flingAnimation.setListener(new FlingAnimationListener() {
            public void onMove(float x, float y) {
                GestureImageViewTouchListener.this.handleDrag(GestureImageViewTouchListener.this.current.x + x, GestureImageViewTouchListener.this.current.y + y);
            }

            public void onComplete() {
            }
        });
        this.zoomAnimation.setZoom(2.0f);
        this.zoomAnimation.setZoomAnimationListener(new ZoomAnimationListener() {
            public void onZoom(float scale, float x, float y) {
                if (scale <= GestureImageViewTouchListener.this.maxScale && scale >= GestureImageViewTouchListener.this.minScale) {
                    GestureImageViewTouchListener.this.handleScale(scale, x, y);
                }
            }

            public void onComplete() {
                GestureImageViewTouchListener.this.inZoom = false;
                GestureImageViewTouchListener.this.handleUp();
            }
        });
        this.moveAnimation.setMoveAnimationListener(new MoveAnimationListener() {
            public void onMove(float x, float y) {
                image2.setPosition(x, y);
                image2.redraw();
            }
        });
        this.tapDetector = new GestureDetector(image2.getContext(), new GestureDetector.SimpleOnGestureListener() {
            public boolean onDoubleTap(MotionEvent e) {
                if (GestureImageViewTouchListener.this.mPager == null || GestureImageViewTouchListener.this.mPager.getOnViewListener() == null) {
                    return true;
                }
                GestureImageViewTouchListener.this.mPager.getOnViewListener().onDoubleTap();
                if (!image2.zoom) {
                    return true;
                }
                GestureImageViewTouchListener.this.startZoom(e);
                return true;
            }

            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (!(GestureImageViewTouchListener.this.mPager == null || GestureImageViewTouchListener.this.mPager.getOnViewListener() == null)) {
                    GestureImageViewTouchListener.this.mPager.getOnViewListener().onSingleTapConfirmed();
                }
                if (GestureImageViewTouchListener.this.inZoom || GestureImageViewTouchListener.this.onClickListener == null) {
                    return false;
                }
                GestureImageViewTouchListener.this.onClickListener.onClick(image2);
                return true;
            }

            public void onLongPress(MotionEvent e) {
                if (!(GestureImageViewTouchListener.this.mPager == null || GestureImageViewTouchListener.this.mPager.getOnViewListener() == null)) {
                    GestureImageViewTouchListener.this.mPager.getOnViewListener().onLongPress();
                }
                super.onLongPress(e);
            }
        });
        this.flingDetector = new GestureDetector(image2.getContext(), this.flingListener);
        this.imageListener = image2.getGestureImageViewListener();
        calculateBoundaries();
    }

    private void startFling() {
        this.flingAnimation.setVelocityX(this.flingListener.getVelocityX());
        this.flingAnimation.setVelocityY(this.flingListener.getVelocityY());
        this.image.animationStart(this.flingAnimation);
    }

    /* access modifiers changed from: private */
    public void startZoom(MotionEvent e) {
        float zoomTo;
        this.inZoom = true;
        this.zoomAnimation.reset();
        if (this.image.isLandscape()) {
            if (this.image.getDeviceOrientation() != 1) {
                int scaledWidth = this.image.getScaledWidth();
                if (scaledWidth == this.canvasWidth) {
                    zoomTo = this.currentScale * 4.0f;
                    this.zoomAnimation.setTouchX(e.getX());
                    this.zoomAnimation.setTouchY(e.getY());
                } else if (scaledWidth < this.canvasWidth) {
                    zoomTo = this.fitScaleHorizontal / this.currentScale;
                    this.zoomAnimation.setTouchX(this.image.getCenterX());
                    this.zoomAnimation.setTouchY(e.getY());
                } else {
                    zoomTo = this.fitScaleHorizontal / this.currentScale;
                    this.zoomAnimation.setTouchX(this.image.getCenterX());
                    this.zoomAnimation.setTouchY(this.image.getCenterY());
                }
            } else if (this.image.getScaledHeight() < this.canvasHeight) {
                zoomTo = this.fitScaleVertical / this.currentScale;
                this.zoomAnimation.setTouchX(e.getX());
                this.zoomAnimation.setTouchY(this.image.getCenterY());
            } else {
                zoomTo = this.fitScaleHorizontal / this.currentScale;
                this.zoomAnimation.setTouchX(this.image.getCenterX());
                this.zoomAnimation.setTouchY(this.image.getCenterY());
            }
        } else if (this.image.getDeviceOrientation() == 1) {
            int scaledHeight = this.image.getScaledHeight();
            if (scaledHeight == this.canvasHeight) {
                zoomTo = this.currentScale * 4.0f;
                this.zoomAnimation.setTouchX(e.getX());
                this.zoomAnimation.setTouchY(e.getY());
            } else if (scaledHeight < this.canvasHeight) {
                zoomTo = this.fitScaleVertical / this.currentScale;
                this.zoomAnimation.setTouchX(e.getX());
                this.zoomAnimation.setTouchY(this.image.getCenterY());
            } else {
                zoomTo = this.fitScaleVertical / this.currentScale;
                this.zoomAnimation.setTouchX(this.image.getCenterX());
                this.zoomAnimation.setTouchY(this.image.getCenterY());
            }
        } else if (this.image.getScaledWidth() < this.canvasWidth) {
            zoomTo = this.fitScaleHorizontal / this.currentScale;
            this.zoomAnimation.setTouchX(this.image.getCenterX());
            this.zoomAnimation.setTouchY(e.getY());
        } else {
            zoomTo = this.fitScaleVertical / this.currentScale;
            this.zoomAnimation.setTouchX(this.image.getCenterX());
            this.zoomAnimation.setTouchY(this.image.getCenterY());
        }
        this.zoomAnimation.setZoom(zoomTo);
        this.image.animationStart(this.zoomAnimation);
    }

    private void stopAnimations() {
        this.image.animationStop();
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (!this.inZoom && !this.tapDetector.onTouchEvent(event)) {
            if (event.getPointerCount() == 1 && this.flingDetector.onTouchEvent(event)) {
                startFling();
            }
            if (event.getAction() == 1) {
                handleUp();
            } else if (event.getAction() == 0) {
                this.old.x = event.getX();
                this.old.y = event.getY();
                stopAnimations();
                this.last.x = event.getX();
                this.last.y = event.getY();
                if (this.imageListener != null) {
                    this.imageListener.onTouch(this.last.x, this.last.y);
                }
                this.touched = true;
            } else if (event.getAction() == 2) {
                if (event.getPointerCount() > 1) {
                    if (this.mPager != null) {
                        this.mPager.isLeft(false);
                        this.mPager.isRight(false);
                    }
                    this.multiTouch = true;
                    if (this.initialDistance > 0.0f) {
                        this.pinchVector.set(event);
                        this.pinchVector.calculateLength();
                        float distance = this.pinchVector.length;
                        if (this.initialDistance != distance) {
                            float newScale = (distance / this.initialDistance) * this.lastScale;
                            if (newScale <= this.maxScale) {
                                this.scaleVector.length *= newScale;
                                this.scaleVector.calculateEndPoint();
                                this.scaleVector.length /= newScale;
                                handleScale(newScale, this.scaleVector.end.x, this.scaleVector.end.y);
                            }
                        }
                    } else {
                        this.initialDistance = MathUtils.distance(event);
                        MathUtils.midpoint(event, this.midpoint);
                        this.scaleVector.setStart(this.midpoint);
                        this.scaleVector.setEnd(this.next);
                        this.scaleVector.calculateLength();
                        this.scaleVector.calculateAngle();
                        this.scaleVector.length /= this.lastScale;
                    }
                } else if (!this.touched) {
                    this.touched = true;
                    this.last.x = event.getX();
                    this.last.y = event.getY();
                    this.next.x = this.image.getImageX();
                    this.next.y = this.image.getImageY();
                } else if (!this.multiTouch) {
                    if (handleDrag(event.getX(), event.getY())) {
                        this.image.redraw();
                    } else if (this.old.x - event.getX() > 9.0f && this.mPager != null) {
                        this.mPager.isLeft(true);
                    } else if (this.old.x - event.getX() < -9.0f && this.mPager != null) {
                        this.mPager.isRight(true);
                    }
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void handleUp() {
        this.multiTouch = false;
        this.initialDistance = 0.0f;
        this.lastScale = this.currentScale;
        if (!this.canDragX) {
            this.next.x = this.centerX;
        }
        if (!this.canDragY) {
            this.next.y = this.centerY;
        }
        boundCoordinates();
        if (!this.canDragX && !this.canDragY) {
            if (this.image.isLandscape()) {
                this.currentScale = this.fitScaleHorizontal;
                this.lastScale = this.fitScaleHorizontal;
            } else {
                this.currentScale = this.fitScaleVertical;
                this.lastScale = this.fitScaleVertical;
            }
        }
        this.image.setScale(this.currentScale);
        this.image.setPosition(this.next.x, this.next.y);
        if (this.imageListener != null) {
            this.imageListener.onScale(this.currentScale);
            this.imageListener.onPosition(this.next.x, this.next.y);
        }
        this.image.redraw();
    }

    /* access modifiers changed from: protected */
    public void handleScale(float scale, float x, float y) {
        this.currentScale = scale;
        if (this.currentScale > this.maxScale) {
            this.currentScale = this.maxScale;
        } else if (this.currentScale < this.minScale) {
            this.currentScale = this.minScale;
        } else {
            this.next.x = x;
            this.next.y = y;
        }
        calculateBoundaries();
        this.image.setScale(this.currentScale);
        this.image.setPosition(this.next.x, this.next.y);
        if (this.imageListener != null) {
            this.imageListener.onScale(this.currentScale);
            this.imageListener.onPosition(this.next.x, this.next.y);
        }
        this.image.redraw();
    }

    /* access modifiers changed from: protected */
    public boolean handleDrag(float x, float y) {
        this.current.x = x;
        this.current.y = y;
        float diffX = this.current.x - this.last.x;
        float diffY = this.current.y - this.last.y;
        if (!(diffX == 0.0f && diffY == 0.0f)) {
            if (this.canDragX) {
                this.next.x += diffX;
            }
            if (this.canDragY) {
                this.next.y += diffY;
            }
            boundCoordinates();
            this.last.x = this.current.x;
            this.last.y = this.current.y;
            if (this.canDragX || this.canDragY) {
                this.image.setPosition(this.next.x, this.next.y);
                if (this.imageListener != null) {
                    this.imageListener.onPosition(this.next.x, this.next.y);
                }
                return true;
            }
        }
        return false;
    }

    public void reset() {
        this.currentScale = this.startingScale;
        this.next.x = this.centerX;
        this.next.y = this.centerY;
        calculateBoundaries();
        this.image.setScale(this.currentScale);
        this.image.setPosition(this.next.x, this.next.y);
        this.image.redraw();
    }

    public float getMaxScale() {
        return this.maxScale;
    }

    public void setMaxScale(float maxScale2) {
        this.maxScale = maxScale2;
    }

    public float getMinScale() {
        return this.minScale;
    }

    public void setMinScale(float minScale2) {
        this.minScale = minScale2;
    }

    public void setOnClickListener(View.OnClickListener onClickListener2) {
        this.onClickListener = onClickListener2;
    }

    /* access modifiers changed from: protected */
    public void setCanvasWidth(int canvasWidth2) {
        this.canvasWidth = canvasWidth2;
    }

    /* access modifiers changed from: protected */
    public void setCanvasHeight(int canvasHeight2) {
        this.canvasHeight = canvasHeight2;
    }

    /* access modifiers changed from: protected */
    public void setFitScaleHorizontal(float fitScale) {
        this.fitScaleHorizontal = fitScale;
    }

    /* access modifiers changed from: protected */
    public void setFitScaleVertical(float fitScaleVertical2) {
        this.fitScaleVertical = fitScaleVertical2;
    }

    /* access modifiers changed from: protected */
    public void boundCoordinates() {
        if (this.next.x < this.boundaryLeft) {
            this.next.x = this.boundaryLeft;
            if (this.mPager != null) {
                this.mPager.isLeft(true);
            }
        } else if (this.next.x > this.boundaryRight) {
            this.next.x = this.boundaryRight;
            if (this.mPager != null) {
                this.mPager.isRight(true);
            }
        }
        if (this.next.y < this.boundaryTop) {
            this.next.y = this.boundaryTop;
        } else if (this.next.y > this.boundaryBottom) {
            this.next.y = this.boundaryBottom;
        }
    }

    /* access modifiers changed from: protected */
    public void calculateBoundaries() {
        boolean z = true;
        int effectiveWidth = Math.round(((float) this.imageWidth) * this.currentScale);
        int effectiveHeight = Math.round(((float) this.imageHeight) * this.currentScale);
        this.canDragX = effectiveWidth > this.displayWidth;
        if (effectiveHeight <= this.displayHeight) {
            z = false;
        }
        this.canDragY = z;
        if (this.canDragX) {
            float diff = ((float) (effectiveWidth - this.displayWidth)) / 2.0f;
            this.boundaryLeft = this.centerX - diff;
            this.boundaryRight = this.centerX + diff;
        }
        if (this.canDragY) {
            float diff2 = ((float) (effectiveHeight - this.displayHeight)) / 2.0f;
            this.boundaryTop = this.centerY - diff2;
            this.boundaryBottom = this.centerY + diff2;
        }
    }
}
