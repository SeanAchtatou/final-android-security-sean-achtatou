package com.palmtrends.zoom;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseview.ImageDetailViewPager;
import com.palmtrends.controll.R;
import java.io.InputStream;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class GestureImageView extends ImageView implements AbsImageDetailIndicator {
    private static /* synthetic */ int[] $SWITCH_TABLE$android$widget$ImageView$ScaleType = null;
    public static final String GLOBAL_NS = "http://schemas.android.com/apk/res/android";
    public static final String LOCAL_NS = "http://schemas.polites.com/android";
    private int alpha;
    private Animator animator;
    private float centerX;
    private float centerY;
    private ColorFilter colorFilter;
    /* access modifiers changed from: private */
    public View.OnTouchListener customOnTouchListener;
    private int deviceOrientation;
    private int displayHeight;
    private int displayWidth;
    private final Semaphore drawLock;
    private Drawable drawable;
    private float fitScaleHorizontal;
    private float fitScaleVertical;
    private GestureImageViewListener gestureImageViewListener;
    /* access modifiers changed from: private */
    public GestureImageViewTouchListener gestureImageViewTouchListener;
    private int hHeight;
    private int hWidth;
    private int imageOrientation;
    private boolean layout;
    private OnPageChangeCallback mCallback;
    private ViewPager.OnPageChangeListener mListener;
    /* access modifiers changed from: private */
    public ImageDetailViewPager mPage;
    private float maxScale;
    private float minScale;
    private View.OnClickListener onClickListener;
    private boolean recycle;
    private int resId;
    private float rotation;
    private float scale;
    private float scaleAdjust;
    private float startingScale;
    private boolean strict;
    GestureDetector tapDetector;
    private float x;
    private float y;
    public boolean zoom;

    public interface OnPageChangeCallback {
        void changeIndex(int i);
    }

    static /* synthetic */ int[] $SWITCH_TABLE$android$widget$ImageView$ScaleType() {
        int[] iArr = $SWITCH_TABLE$android$widget$ImageView$ScaleType;
        if (iArr == null) {
            iArr = new int[ImageView.ScaleType.values().length];
            try {
                iArr[ImageView.ScaleType.CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ImageView.ScaleType.CENTER_CROP.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_CENTER.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_START.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[ImageView.ScaleType.FIT_XY.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            $SWITCH_TABLE$android$widget$ImageView$ScaleType = iArr;
        }
        return iArr;
    }

    public GestureImageView(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs);
    }

    public GestureImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.drawLock = new Semaphore(0);
        this.x = 0.0f;
        this.y = 0.0f;
        this.layout = false;
        this.scaleAdjust = 1.0f;
        this.startingScale = 1.0f;
        this.scale = 1.0f;
        this.maxScale = 5.0f;
        this.minScale = 0.75f;
        this.fitScaleHorizontal = 1.0f;
        this.fitScaleVertical = 1.0f;
        this.rotation = 0.0f;
        this.resId = -1;
        this.recycle = false;
        this.strict = false;
        this.zoom = true;
        this.alpha = 255;
        this.deviceOrientation = -1;
        this.tapDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            public boolean onDoubleTap(MotionEvent e) {
                if (GestureImageView.this.mPage == null || GestureImageView.this.mPage.getOnViewListener() == null) {
                    return true;
                }
                GestureImageView.this.mPage.getOnViewListener().onDoubleTap();
                return true;
            }

            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (GestureImageView.this.mPage == null || GestureImageView.this.mPage == null || GestureImageView.this.mPage.getOnViewListener() == null) {
                    return false;
                }
                GestureImageView.this.mPage.getOnViewListener().onSingleTapConfirmed();
                return false;
            }

            public void onLongPress(MotionEvent e) {
                if (!(GestureImageView.this.mPage == null || GestureImageView.this.mPage.getOnViewListener() == null)) {
                    GestureImageView.this.mPage.getOnViewListener().onLongPress();
                }
                super.onLongPress(e);
            }
        });
        String scaleType = attrs.getAttributeValue(GLOBAL_NS, "scaleType");
        if (scaleType == null || scaleType.trim().length() == 0) {
            setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
        setMinScale(attrs.getAttributeFloatValue(LOCAL_NS, "min-scale", this.minScale));
        setMaxScale(attrs.getAttributeFloatValue(LOCAL_NS, "max-scale", this.maxScale));
        setStrict(attrs.getAttributeBooleanValue(LOCAL_NS, "strict", this.strict));
        setRecycle(attrs.getAttributeBooleanValue(LOCAL_NS, "recycle", this.recycle));
        this.zoom = attrs.getAttributeBooleanValue(LOCAL_NS, "zoom", this.zoom);
        initImage(false);
    }

    public GestureImageView(Context context) {
        super(context);
        this.drawLock = new Semaphore(0);
        this.x = 0.0f;
        this.y = 0.0f;
        this.layout = false;
        this.scaleAdjust = 1.0f;
        this.startingScale = 1.0f;
        this.scale = 1.0f;
        this.maxScale = 5.0f;
        this.minScale = 0.75f;
        this.fitScaleHorizontal = 1.0f;
        this.fitScaleVertical = 1.0f;
        this.rotation = 0.0f;
        this.resId = -1;
        this.recycle = false;
        this.strict = false;
        this.zoom = true;
        this.alpha = 255;
        this.deviceOrientation = -1;
        this.tapDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            public boolean onDoubleTap(MotionEvent e) {
                if (GestureImageView.this.mPage == null || GestureImageView.this.mPage.getOnViewListener() == null) {
                    return true;
                }
                GestureImageView.this.mPage.getOnViewListener().onDoubleTap();
                return true;
            }

            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (GestureImageView.this.mPage == null || GestureImageView.this.mPage == null || GestureImageView.this.mPage.getOnViewListener() == null) {
                    return false;
                }
                GestureImageView.this.mPage.getOnViewListener().onSingleTapConfirmed();
                return false;
            }

            public void onLongPress(MotionEvent e) {
                if (!(GestureImageView.this.mPage == null || GestureImageView.this.mPage.getOnViewListener() == null)) {
                    GestureImageView.this.mPage.getOnViewListener().onLongPress();
                }
                super.onLongPress(e);
            }
        });
        setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        initImage(false);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.drawable == null) {
            this.displayHeight = View.MeasureSpec.getSize(heightMeasureSpec);
            this.displayWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        } else if (getResources().getConfiguration().orientation == 2) {
            this.displayHeight = View.MeasureSpec.getSize(heightMeasureSpec);
            if (getLayoutParams().width == -2) {
                this.displayWidth = Math.round(((float) this.displayHeight) * (((float) getImageWidth()) / ((float) getImageHeight())));
            } else {
                this.displayWidth = View.MeasureSpec.getSize(widthMeasureSpec);
            }
        } else {
            this.displayWidth = View.MeasureSpec.getSize(widthMeasureSpec);
            if (getLayoutParams().height == -2) {
                this.displayHeight = Math.round(((float) this.displayWidth) * (((float) getImageHeight()) / ((float) getImageWidth())));
            } else {
                this.displayHeight = View.MeasureSpec.getSize(heightMeasureSpec);
            }
        }
        setMeasuredDimension(this.displayWidth, this.displayHeight);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed || !this.layout) {
            setupCanvas(this.displayWidth, this.displayHeight, getResources().getConfiguration().orientation);
        }
    }

    /* access modifiers changed from: protected */
    public void setupCanvas(int measuredWidth, int measuredHeight, int orientation) {
        if (this.deviceOrientation != orientation) {
            this.layout = false;
            this.deviceOrientation = orientation;
        }
        if (this.drawable != null && !this.layout) {
            int imageWidth = getImageWidth();
            int imageHeight = getImageHeight();
            this.hWidth = Math.round(((float) imageWidth) / 2.0f);
            this.hHeight = Math.round(((float) imageHeight) / 2.0f);
            int measuredWidth2 = measuredWidth - (getPaddingLeft() + getPaddingRight());
            int measuredHeight2 = measuredHeight - (getPaddingTop() + getPaddingBottom());
            computeCropScale(imageWidth, imageHeight, measuredWidth2, measuredHeight2);
            computeStartingScale(imageWidth, imageHeight, measuredWidth2, measuredHeight2);
            this.scaleAdjust = this.startingScale;
            this.centerX = ((float) measuredWidth2) / 2.0f;
            this.centerY = ((float) measuredHeight2) / 2.0f;
            this.x = this.centerX;
            this.y = this.centerY;
            this.gestureImageViewTouchListener = new GestureImageViewTouchListener(this, measuredWidth2, measuredHeight2, this.mPage);
            if (isLandscape()) {
                this.gestureImageViewTouchListener.setMinScale(this.minScale * this.fitScaleHorizontal);
            } else {
                this.gestureImageViewTouchListener.setMinScale(this.minScale * this.fitScaleVertical);
            }
            this.gestureImageViewTouchListener.setMaxScale(this.maxScale * this.startingScale);
            this.gestureImageViewTouchListener.setFitScaleHorizontal(this.fitScaleHorizontal);
            this.gestureImageViewTouchListener.setFitScaleVertical(this.fitScaleVertical);
            this.gestureImageViewTouchListener.setCanvasWidth(measuredWidth2);
            this.gestureImageViewTouchListener.setCanvasHeight(measuredHeight2);
            this.gestureImageViewTouchListener.setOnClickListener(this.onClickListener);
            this.drawable.setBounds(-this.hWidth, -this.hHeight, this.hWidth, this.hHeight);
            super.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if (!GestureImageView.this.zoom) {
                        GestureImageView.this.tapDetector.onTouchEvent(event);
                        return true;
                    }
                    if (GestureImageView.this.customOnTouchListener != null) {
                        GestureImageView.this.customOnTouchListener.onTouch(v, event);
                    }
                    return GestureImageView.this.gestureImageViewTouchListener.onTouch(v, event);
                }
            });
            this.layout = true;
        }
    }

    /* access modifiers changed from: protected */
    public void computeCropScale(int imageWidth, int imageHeight, int measuredWidth, int measuredHeight) {
        this.fitScaleHorizontal = ((float) measuredWidth) / ((float) imageWidth);
        this.fitScaleVertical = ((float) measuredHeight) / ((float) imageHeight);
        if (this.fitScaleHorizontal > this.fitScaleVertical) {
            this.fitScaleHorizontal = this.fitScaleVertical;
        } else {
            this.fitScaleVertical = this.fitScaleHorizontal;
        }
    }

    /* access modifiers changed from: protected */
    public void computeStartingScale(int imageWidth, int imageHeight, int measuredWidth, int measuredHeight) {
        switch ($SWITCH_TABLE$android$widget$ImageView$ScaleType()[getScaleType().ordinal()]) {
            case 1:
                this.startingScale = 1.0f;
                return;
            case 2:
                this.startingScale = Math.max(((float) measuredHeight) / ((float) imageHeight), ((float) measuredWidth) / ((float) imageWidth));
                return;
            case 3:
                if (isLandscape()) {
                    this.startingScale = this.fitScaleHorizontal;
                    return;
                } else {
                    this.startingScale = this.fitScaleVertical;
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRecycled() {
        Bitmap bitmap;
        if (this.drawable == null || !(this.drawable instanceof BitmapDrawable) || (bitmap = ((BitmapDrawable) this.drawable).getBitmap()) == null) {
            return false;
        }
        return bitmap.isRecycled();
    }

    /* access modifiers changed from: protected */
    public void recycle() {
        Bitmap bitmap;
        if (this.recycle && this.drawable != null && (this.drawable instanceof BitmapDrawable) && (bitmap = ((BitmapDrawable) this.drawable).getBitmap()) != null) {
            bitmap.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.layout) {
            if (this.drawable != null && !isRecycled()) {
                canvas.save();
                float adjustedScale = this.scale * this.scaleAdjust;
                canvas.translate(this.x, this.y);
                if (this.rotation != 0.0f) {
                    canvas.rotate(this.rotation);
                }
                if (adjustedScale != 1.0f) {
                    canvas.scale(adjustedScale, adjustedScale);
                }
                this.drawable.draw(canvas);
                canvas.restore();
            }
            if (this.drawLock.availablePermits() <= 0) {
                this.drawLock.release();
            }
        }
    }

    public boolean waitForDraw(long timeout) throws InterruptedException {
        return this.drawLock.tryAcquire(timeout, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.animator = new Animator(this, "GestureImageViewAnimator");
        this.animator.start();
        if (this.resId >= 0 && this.drawable == null) {
            setImageResource(this.resId);
        }
        super.onAttachedToWindow();
    }

    public void animationStart(Animation animation) {
        if (this.animator != null) {
            this.animator.play(animation);
        }
    }

    public void animationStop() {
        if (this.animator != null) {
            this.animator.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (this.animator != null) {
            this.animator.finish();
        }
        if (this.recycle && this.drawable != null && !isRecycled()) {
            recycle();
            this.drawable = null;
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void initImage(boolean original) {
        if (this.drawable != null) {
            this.drawable.setAlpha(this.alpha);
            this.drawable.setFilterBitmap(true);
            if (this.colorFilter != null) {
                this.drawable.setColorFilter(this.colorFilter);
            }
        }
        if (!original) {
            this.layout = false;
            requestLayout();
            redraw();
        }
    }

    public void setImageBitmap(Bitmap image) {
        setImageBitmap(image, false);
    }

    /* access modifiers changed from: protected */
    public void setImageBitmap(Bitmap image, boolean original) {
        this.drawable = new BitmapDrawable(getResources(), image);
        initImage(original);
    }

    public void setImageDrawable(Drawable drawable2) {
        this.drawable = drawable2;
    }

    public void setImageResource(int id) {
        setImageResource(id, false);
    }

    /* access modifiers changed from: protected */
    public void setImageResource(int id, boolean original) {
        if (this.drawable != null) {
            recycle();
        }
        if (id >= 0) {
            this.resId = id;
            setImageDrawable(getContext().getResources().getDrawable(id));
        }
    }

    public int getScaledWidth() {
        return Math.round(((float) getImageWidth()) * getScale());
    }

    public int getScaledHeight() {
        return Math.round(((float) getImageHeight()) * getScale());
    }

    public int getImageWidth() {
        if (this.drawable != null) {
            return this.drawable.getIntrinsicWidth();
        }
        return 0;
    }

    public int getImageHeight() {
        if (this.drawable != null) {
            return this.drawable.getIntrinsicHeight();
        }
        return 0;
    }

    public void moveBy(float x2, float y2) {
        this.x += x2;
        this.y += y2;
    }

    public void setPosition(float x2, float y2) {
        this.x = x2;
        this.y = y2;
    }

    public void redraw() {
        postInvalidate();
    }

    public void setMinScale(float min) {
        this.minScale = min;
        if (this.gestureImageViewTouchListener != null) {
            this.gestureImageViewTouchListener.setMinScale(this.fitScaleHorizontal * min);
        }
    }

    public void setMaxScale(float max) {
        this.maxScale = max;
        if (this.gestureImageViewTouchListener != null) {
            this.gestureImageViewTouchListener.setMaxScale(this.startingScale * max);
        }
    }

    public void setScale(float scale2) {
        this.scaleAdjust = scale2;
    }

    public float getScale() {
        return this.scaleAdjust;
    }

    public float getImageX() {
        return this.x;
    }

    public float getImageY() {
        return this.y;
    }

    public boolean isStrict() {
        return this.strict;
    }

    public void setStrict(boolean strict2) {
        this.strict = strict2;
    }

    public boolean isRecycle() {
        return this.recycle;
    }

    public void setRecycle(boolean recycle2) {
        this.recycle = recycle2;
    }

    public void reset() {
        this.x = this.centerX;
        this.y = this.centerY;
        this.scaleAdjust = this.startingScale;
        redraw();
    }

    public void setRotation(float rotation2) {
        this.rotation = rotation2;
    }

    public void setGestureImageViewListener(GestureImageViewListener pinchImageViewListener) {
        this.gestureImageViewListener = pinchImageViewListener;
    }

    public GestureImageViewListener getGestureImageViewListener() {
        return this.gestureImageViewListener;
    }

    public Drawable getDrawable() {
        return this.drawable;
    }

    public void setAlpha(int alpha2) {
        this.alpha = alpha2;
        if (this.drawable != null) {
            this.drawable.setAlpha(alpha2);
        }
    }

    public void setColorFilter(ColorFilter cf) {
        this.colorFilter = cf;
        if (this.drawable != null) {
            this.drawable.setColorFilter(cf);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void setImageURI(Uri mUri) {
        Cursor cur;
        InputStream in;
        if ("content".equals(mUri.getScheme())) {
            try {
                String[] orientationColumn = {"orientation"};
                cur = getContext().getContentResolver().query(mUri, orientationColumn, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    this.imageOrientation = cur.getInt(cur.getColumnIndex(orientationColumn[0]));
                }
                in = null;
                in = getContext().getContentResolver().openInputStream(mUri);
                Bitmap bmp = BitmapFactory.decodeStream(in);
                if (this.imageOrientation != 0) {
                    Matrix m = new Matrix();
                    m.postRotate((float) this.imageOrientation);
                    Bitmap rotated = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), m, true);
                    bmp.recycle();
                    setImageDrawable(new BitmapDrawable(getResources(), rotated));
                } else {
                    setImageDrawable(new BitmapDrawable(getResources(), bmp));
                }
                if (in != null) {
                    in.close();
                }
                if (cur != null) {
                    cur.close();
                }
            } catch (Exception e) {
                if (ShareApplication.debug) {
                    Log.w("GestureImageView", "Unable to open content: " + mUri, e);
                }
            } catch (Throwable th) {
                if (in != null) {
                    in.close();
                }
                if (cur != null) {
                    cur.close();
                }
                throw th;
            }
        } else {
            setImageDrawable(Drawable.createFromPath(mUri.toString()));
        }
        if (this.drawable == null) {
            if (ShareApplication.debug) {
                Log.e("GestureImageView", "resolveUri failed on bad bitmap uri: " + mUri);
            }
        }
    }

    public Matrix getImageMatrix() {
        if (!this.strict) {
            return super.getImageMatrix();
        }
        throw new UnsupportedOperationException("Not supported");
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType == ImageView.ScaleType.CENTER || scaleType == ImageView.ScaleType.CENTER_CROP || scaleType == ImageView.ScaleType.CENTER_INSIDE) {
            super.setScaleType(scaleType);
        } else if (this.strict) {
            throw new UnsupportedOperationException("Not supported");
        }
    }

    public void invalidateDrawable(Drawable dr) {
        if (this.strict) {
            throw new UnsupportedOperationException("Not supported");
        }
        super.invalidateDrawable(dr);
    }

    public int[] onCreateDrawableState(int extraSpace) {
        if (!this.strict) {
            return super.onCreateDrawableState(extraSpace);
        }
        throw new UnsupportedOperationException("Not supported");
    }

    public void setAdjustViewBounds(boolean adjustViewBounds) {
        if (this.strict) {
            throw new UnsupportedOperationException("Not supported");
        }
        super.setAdjustViewBounds(adjustViewBounds);
    }

    public void setImageLevel(int level) {
        if (this.strict) {
            throw new UnsupportedOperationException("Not supported");
        }
        super.setImageLevel(level);
    }

    public void setImageMatrix(Matrix matrix) {
        if (this.strict) {
            throw new UnsupportedOperationException("Not supported");
        }
    }

    public void setImageState(int[] state, boolean merge) {
        if (this.strict) {
            throw new UnsupportedOperationException("Not supported");
        }
    }

    public void setSelected(boolean selected) {
        if (this.strict) {
            throw new UnsupportedOperationException("Not supported");
        }
        super.setSelected(selected);
    }

    public void setOnTouchListener(View.OnTouchListener l) {
        this.customOnTouchListener = l;
    }

    public float getCenterX() {
        return this.centerX;
    }

    public float getCenterY() {
        return this.centerY;
    }

    public boolean isLandscape() {
        return getImageWidth() >= getImageHeight();
    }

    public boolean isPortrait() {
        return getImageWidth() <= getImageHeight();
    }

    public void setOnClickListener(View.OnClickListener l) {
        this.onClickListener = l;
        if (this.gestureImageViewTouchListener != null) {
            this.gestureImageViewTouchListener.setOnClickListener(l);
        }
    }

    public boolean isOrientationAligned() {
        if (this.deviceOrientation == 2) {
            return isLandscape();
        }
        if (this.deviceOrientation == 1) {
            return isPortrait();
        }
        return true;
    }

    public int getDeviceOrientation() {
        return this.deviceOrientation;
    }

    public void onPageScrollStateChanged(int state) {
        if (this.mListener != null) {
            this.mListener.onPageScrollStateChanged(state);
        }
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (this.mListener != null) {
            this.mListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }
    }

    public void onPageSelected(int position) {
        if (this.mListener != null) {
            this.mListener.onPageSelected(position);
        }
        try {
            ((GestureImageView) ((FrameLayout) this.mPage.getChildAt(0)).findViewById(R.id.imageView)).gestureImageViewTouchListener.reset();
            ((GestureImageView) ((FrameLayout) this.mPage.getChildAt(1)).findViewById(R.id.imageView)).gestureImageViewTouchListener.reset();
            ((GestureImageView) ((FrameLayout) this.mPage.getChildAt(2)).findViewById(R.id.imageView)).gestureImageViewTouchListener.reset();
        } catch (Exception e) {
        }
        if (this.mCallback != null) {
            this.mCallback.changeIndex(position);
        }
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener listener) {
        this.mListener = listener;
    }

    public void setViewPager(ViewPager pager) {
        this.mPage = (ImageDetailViewPager) pager;
        this.mPage.setOnPageChangeListener(this);
    }

    public void setOnPageChangeCallback(OnPageChangeCallback callback) {
        this.mCallback = callback;
    }
}
