package com.palmtrends.zoom;

public interface Animation {
    boolean update(GestureImageView gestureImageView, long j);
}
