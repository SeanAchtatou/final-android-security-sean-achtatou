package com.palmtrends.zoom;

public interface GestureImageViewListener {
    void onPosition(float f, float f2);

    void onScale(float f);

    void onTouch(float f, float f2);
}
