package com.palmtrends.zoom;

import android.support.v4.view.ViewPager;

public interface AbsImageDetailIndicator extends ViewPager.OnPageChangeListener {
    void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener);

    void setViewPager(ViewPager viewPager);
}
