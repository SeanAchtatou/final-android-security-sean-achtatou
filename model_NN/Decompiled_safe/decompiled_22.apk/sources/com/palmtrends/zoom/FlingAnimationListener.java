package com.palmtrends.zoom;

public interface FlingAnimationListener {
    void onComplete();

    void onMove(float f, float f2);
}
