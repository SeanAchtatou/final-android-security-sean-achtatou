package com.palmtrends.zoom;

public interface MoveAnimationListener {
    void onMove(float f, float f2);
}
