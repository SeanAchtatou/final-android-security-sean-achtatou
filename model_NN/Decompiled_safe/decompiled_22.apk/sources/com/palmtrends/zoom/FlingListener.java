package com.palmtrends.zoom;

import android.view.GestureDetector;
import android.view.MotionEvent;

public class FlingListener extends GestureDetector.SimpleOnGestureListener {
    private float velocityX;
    private float velocityY;

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX2, float velocityY2) {
        this.velocityX = velocityX2;
        this.velocityY = velocityY2;
        return true;
    }

    public float getVelocityX() {
        return this.velocityX;
    }

    public float getVelocityY() {
        return this.velocityY;
    }
}
