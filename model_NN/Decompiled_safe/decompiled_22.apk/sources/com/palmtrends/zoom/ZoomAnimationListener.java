package com.palmtrends.zoom;

public interface ZoomAnimationListener {
    void onComplete();

    void onZoom(float f, float f2, float f3);
}
