package com.palmtrends.zoom;

public class Animator extends Thread {
    private boolean active = false;
    private Animation animation;
    private long lastTime = -1;
    private boolean running = false;
    private GestureImageView view;

    public Animator(GestureImageView view2, String threadName) {
        super(threadName);
        this.view = view2;
    }

    public void run() {
        this.running = true;
        while (this.running) {
            while (this.active && this.animation != null) {
                long time = System.currentTimeMillis();
                this.active = this.animation.update(this.view, time - this.lastTime);
                this.view.redraw();
                this.lastTime = time;
                while (this.active) {
                    try {
                        if (this.view.waitForDraw(32)) {
                            break;
                        }
                    } catch (InterruptedException e) {
                        this.active = false;
                    }
                }
            }
            synchronized (this) {
                if (this.running) {
                    try {
                        wait();
                    } catch (InterruptedException e2) {
                    }
                }
            }
        }
    }

    public synchronized void finish() {
        this.running = false;
        this.active = false;
        notifyAll();
    }

    public void play(Animation transformer) {
        if (this.active) {
            cancel();
        }
        this.animation = transformer;
        activate();
    }

    public synchronized void activate() {
        this.lastTime = System.currentTimeMillis();
        this.active = true;
        notifyAll();
    }

    public void cancel() {
        this.active = false;
    }
}
