package com.palmtrends.zoom;

public class FlingAnimation implements Animation {
    private float factor = 0.85f;
    private FlingAnimationListener listener;
    private float threshold = 10.0f;
    private float velocityX;
    private float velocityY;

    public boolean update(GestureImageView view, long time) {
        float seconds = ((float) time) / 1000.0f;
        float dx = this.velocityX * seconds;
        float dy = this.velocityY * seconds;
        this.velocityX *= this.factor;
        this.velocityY *= this.factor;
        boolean active = Math.abs(this.velocityX) > this.threshold && Math.abs(this.velocityY) > this.threshold;
        if (this.listener != null) {
            this.listener.onMove(dx, dy);
            if (!active) {
                this.listener.onComplete();
            }
        }
        return active;
    }

    public void setVelocityX(float velocityX2) {
        this.velocityX = velocityX2;
    }

    public void setVelocityY(float velocityY2) {
        this.velocityY = velocityY2;
    }

    public void setFactor(float factor2) {
        this.factor = factor2;
    }

    public void setListener(FlingAnimationListener listener2) {
        this.listener = listener2;
    }
}
