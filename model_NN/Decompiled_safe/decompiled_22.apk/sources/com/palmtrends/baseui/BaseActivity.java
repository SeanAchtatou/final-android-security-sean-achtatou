package com.palmtrends.baseui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.controll.R;
import com.palmtrends.loadimage.ImageCache;
import com.palmtrends.loadimage.ImageFetcher;

public abstract class BaseActivity extends FragmentActivity {
    public static String ACTIVITY_FINSH;
    FinishCastReceiver finishBroadCastReceiver;

    public abstract void things(View view);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        requestWindowFeature(1);
        super.onCreate(arg0);
        if (ShareApplication.mImageWorker == null) {
            ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(ShareApplication.IMAGE_CACHE_DIR);
            ImageFetcher mImageWorker = new ImageFetcher(this, 800);
            mImageWorker.setImageCache(ImageCache.findOrCreateCache(this, cacheParams));
            ShareApplication.mImageWorker = mImageWorker;
        }
        setFinish();
    }

    static {
        ACTIVITY_FINSH = "com.palmtrends.activity.finish";
        ACTIVITY_FINSH = ShareApplication.share.getResources().getString(R.string.activity_all_finish);
    }

    public void setFinish() {
        this.finishBroadCastReceiver = new FinishCastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTIVITY_FINSH);
        registerReceiver(this.finishBroadCastReceiver, intentFilter);
    }

    public class FinishCastReceiver extends BroadcastReceiver {
        public FinishCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            BaseActivity.this.finish();
        }
    }

    public void finish() {
        if (this.finishBroadCastReceiver != null) {
            try {
                unregisterReceiver(this.finishBroadCastReceiver);
            } catch (Exception e) {
            }
        }
        super.finish();
    }
}
