package com.palmtrends.baseui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.controll.R;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Items;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.ImageFetcher;
import com.palmtrends.loadimage.Utils;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;
import com.tencent.tauth.Constants;
import com.utils.FileUtils;
import com.utils.FinalVariable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.httpclient.methods.multipart.FilePart;

public class BaseArticleActivity extends BaseActivity {
    private static final int THUMB_SIZE = 150;
    public static final int TIMELINE_SUPPORTED_VERSION = 553779201;
    public static final int WEIXIN_ONE = 0;
    public static final int WEIXIN_QUAN = 1;
    public static Map<String, ArrayList<Listitem>> o_items = new HashMap();
    public IWXAPI api;
    public Listitem mCurrentItem;
    public String picurl = "";
    public String shorturl = "";
    public int weixin_type = 0;
    public Handler wxHandler = new Handler() {
        public void handleMessage(Message message) {
            int i = 1;
            if (message.what == 1001) {
                if (ShareApplication.debug) {
                    System.out.println("微信分享开始:" + BaseArticleActivity.this.picurl);
                }
                WXWebpageObject webpage = new WXWebpageObject();
                webpage.webpageUrl = "http://www.pymob.cn/";
                WXMediaMessage msg = new WXMediaMessage(webpage);
                msg.title = BaseArticleActivity.this.mCurrentItem.title;
                msg.description = BaseArticleActivity.this.mCurrentItem.des;
                if (message.obj != null) {
                    msg.thumbData = Util.bmpToByteArray((Bitmap) message.obj, true);
                } else {
                    msg.thumbData = Util.bmpToByteArray(BitmapFactory.decodeResource(BaseArticleActivity.this.getResources(), R.drawable.ic_launcher), true);
                }
                SendMessageToWX.Req req = new SendMessageToWX.Req();
                req.transaction = BaseArticleActivity.this.buildTransaction("webpage");
                req.message = msg;
                if (BaseArticleActivity.this.weixin_type != 1) {
                    i = 0;
                }
                req.scene = i;
                if (ShareApplication.debug) {
                    System.out.println("微信分享结束:");
                }
                BaseArticleActivity.this.api.sendReq(req);
            } else if (message.what == 10014) {
                BaseArticleActivity.this.shorturl = String.valueOf(message.obj);
                BaseArticleActivity.this.shorturl = "  " + BaseArticleActivity.this.mCurrentItem.title + " " + BaseArticleActivity.this.shorturl;
                BaseArticleActivity.this.sendToWeixin();
                if (DBHelper.getDBHelper().counts("readitem", "n_mark='" + BaseArticleActivity.this.mCurrentItem.n_mark + "'") > 0) {
                    DNDataSource.updateRead("readitem", BaseArticleActivity.this.mCurrentItem.n_mark, "shorturl", BaseArticleActivity.this.shorturl);
                }
            } else if (message.what == 10002) {
                Utils.showToast("资源获取失败！");
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        String appId = getResources().getString(R.string.wx_app_id);
        this.api = WXAPIFactory.createWXAPI(this, appId, false);
        this.api.registerApp(appId);
        o_items.clear();
        if (ShareApplication.debug) {
            System.out.println("微信ID :" + appId);
        }
    }

    public void things(View view) {
    }

    public void shareArticle(Items item) {
        DBHelper db = DBHelper.getDBHelper();
        if (db.counts("readitem", "n_mark='" + item.n_mark + "'") > 0) {
            String[] value = {item.n_mark};
            this.shorturl = db.select("readitem", "shorturl", "n_mark=?", value);
            this.picurl = db.select("readitem", "share_image", "n_mark=?", value);
        }
        Intent i = new Intent();
        i.setAction(getResources().getString(R.string.activity_share));
        i.putExtra("shortID", this.shorturl == null ? "" : this.shorturl);
        i.putExtra("aid", item.nid);
        i.putExtra(Constants.PARAM_TITLE, item.title);
        i.putExtra("shareURL", this.picurl == null ? "" : this.picurl);
        i.putExtra("comment", item.des);
        startActivity(i);
    }

    public void shareEmail(String info, String picurl2, String title) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", info);
        intent.putExtra("android.intent.extra.SUBJECT", title);
        if (picurl2 == null || "".equals(picurl2)) {
            intent.setType("message/rfc822");
        } else {
            File file = new File(String.valueOf(FileUtils.sdPath) + "image/" + FileUtils.converPathToName(picurl2));
            if (file.exists()) {
                intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
                if (file.getName().endsWith(".gz")) {
                    intent.setType("application/x-gzip");
                } else if (file.getName().endsWith(".txt")) {
                    intent.setType("text/plain");
                } else {
                    intent.setType(FilePart.DEFAULT_CONTENT_TYPE);
                }
            } else {
                intent.setType("plain/text");
            }
        }
        try {
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.showToast("请安装邮件客户端");
        }
    }

    /* access modifiers changed from: private */
    public String buildTransaction(String type) {
        if (type == null) {
            return String.valueOf(System.currentTimeMillis());
        }
        return String.valueOf(type) + System.currentTimeMillis();
    }

    public static Bitmap getbitmap(String imageUri) {
        if (ShareApplication.debug) {
            System.out.println("下载分享图片:" + imageUri);
        }
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(imageUri).openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            is.close();
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void sendToWeixin() {
        new Thread() {
            public void run() {
                String url = BaseArticleActivity.this.picurl;
                Bitmap bm = null;
                if (BaseArticleActivity.this.picurl != null && !"".equals(BaseArticleActivity.this.picurl) && !"null".equals(BaseArticleActivity.this.picurl) && !BaseArticleActivity.this.picurl.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                    url = String.valueOf(Urls.main) + BaseArticleActivity.this.picurl;
                }
                if (ShareApplication.debug) {
                    System.out.println("分享图片的地址:" + url);
                }
                if (!"".equals(url)) {
                    String imagename = FileUtils.converPathToName(url);
                    System.out.println("地址:" + FileUtils.sdPath + "image/" + imagename);
                    if (FileUtils.isFileExist("image/" + imagename)) {
                        if (ShareApplication.debug) {
                            System.out.println("缓存读取分享图片:" + url);
                        }
                        try {
                            bm = BitmapFactory.decodeFile(String.valueOf(FileUtils.sdPath) + "image/" + imagename);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            if (ShareApplication.debug) {
                                System.out.println("下载分享图片:" + url);
                            }
                            Drawable drawable = Drawable.createFromStream(new URL(url).openStream(), "image.png");
                            if (drawable != null) {
                                bm = ((BitmapDrawable) drawable).getBitmap();
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                    try {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        double mid = (double) (baos.toByteArray().length / 1024);
                        if (mid > 22.0d) {
                            double i = mid / 22.0d;
                            bm = BaseArticleActivity.zoomImage(bm, ((double) bm.getWidth()) / Math.sqrt(i), ((double) bm.getHeight()) / Math.sqrt(i));
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                } else {
                    bm = BitmapFactory.decodeResource(BaseArticleActivity.this.getResources(), R.drawable.ic_launcher);
                }
                Message msg = new Message();
                msg.what = FinalVariable.update;
                msg.obj = bm;
                BaseArticleActivity.this.wxHandler.sendMessage(msg);
            }
        }.start();
    }

    private Bitmap comp(Bitmap image) {
        if (ShareApplication.debug) {
            System.out.println("开始压缩分享图片:");
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        if (baos.toByteArray().length / 1024 > 1024) {
            baos.reset();
            image.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        newOpts.inJustDecodeBounds = true;
        Bitmap decodeStream = BitmapFactory.decodeStream(isBm, null, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        int be = 1;
        if (w > h && ((float) w) > 480.0f) {
            be = (int) (((float) newOpts.outWidth) / 480.0f);
        } else if (w < h && ((float) h) > 800.0f) {
            be = (int) (((float) newOpts.outHeight) / 800.0f);
        }
        if (be <= 0) {
            be = 1;
        }
        newOpts.inSampleSize = be;
        return compressImage(BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()), null, newOpts));
    }

    private Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        int options = 100;
        while (baos.toByteArray().length / 1024 > 32) {
            baos.reset();
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);
            options -= 10;
        }
        Bitmap bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()), null, null);
        if (ShareApplication.debug) {
            System.out.println("结束压缩分享图片:");
        }
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        float width = (float) bgimage.getWidth();
        float height = (float) bgimage.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(120.0f / width, 120.0f / height);
        return Bitmap.createBitmap(bgimage, 0, 0, (int) width, (int) height, matrix, true);
    }
}
