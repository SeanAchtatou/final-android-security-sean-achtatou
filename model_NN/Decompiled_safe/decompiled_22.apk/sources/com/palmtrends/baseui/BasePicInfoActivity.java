package com.palmtrends.baseui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseview.ImageDetailViewPager;
import com.palmtrends.controll.R;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.ImageCache;
import com.palmtrends.loadimage.ImageFetcher;
import com.palmtrends.loadimage.ImageResizer;
import com.palmtrends.loadimage.Utils;
import com.palmtrends.zoom.GestureImageView;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXImageObject;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.platformtools.Util;
import com.tencent.tauth.Constants;
import com.utils.FileUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URL;
import org.apache.commons.httpclient.methods.multipart.FilePart;

public class BasePicInfoActivity extends BaseActivity implements GestureImageView.OnPageChangeCallback, ImageDetailViewPager.OnViewListener {
    protected static final int DATA_APPEND_CURRENT = 0;
    protected static final int DATA_APPEND_LEFT = 1;
    protected static final int DATA_APPEND_RIGHT = 2;
    protected static final String EXTRA_IMAGE = "extra_image";
    protected static final String IMAGE_CACHE_DIR = "bigimages";
    public static final int TIMELINE_SUPPORTED_VERSION = 553779201;
    public static final int WEIXIN_ONE = 0;
    public static final int WEIXIN_QUAN = 1;
    public static ImageResizer mImageWorker;
    protected IWXAPI api;
    protected String shortID = "";
    public int weixin_type = 0;
    public Handler wxHandler = new Handler() {
        public void handleMessage(Message message) {
            BasePicInfoActivity.this.endToWeixin();
            if (message.what == 0) {
                Utils.showToast("资源获取失败");
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        int longest;
        super.onCreate(arg0);
        requestWindowFeature(1);
        if (mImageWorker == null) {
            int height = getResources().getDisplayMetrics().heightPixels;
            int width = getResources().getDisplayMetrics().widthPixels;
            if (height > width) {
                longest = height;
            } else {
                longest = width;
            }
            mImageWorker = new ImageFetcher(this, longest);
            mImageWorker.setImageCache(ImageCache.findOrCreateCache(this, new ImageCache.ImageCacheParams(IMAGE_CACHE_DIR)));
            mImageWorker.setImageFadeIn(false);
        }
        String appId = getResources().getString(R.string.wx_app_id);
        this.api = WXAPIFactory.createWXAPI(this, appId, false);
        this.api.registerApp(appId);
    }

    public void downloadImage(Listitem current_image) {
        if (current_image != null) {
            Bitmap bt = mImageWorker.mImageCache.getBitmapFromMemCache(String.valueOf(Urls.main) + current_image.icon);
            if (bt == null) {
                bt = mImageWorker.getImageCache().getBitmapFromDiskCache(String.valueOf(Urls.main) + current_image.icon);
            }
            if (bt == null) {
                Utils.showToast(R.string.draw_load_toast);
                return;
            }
            Utils.showProcessDialog(this, R.string.draw_load_down);
            FileUtils.writeToFile(bt, current_image.icon);
        }
    }

    public void shareEmail(String info, String picurl, String title) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("body", info);
        intent.putExtra("subject", title);
        if (picurl != null) {
            File file = new File(String.valueOf(FileUtils.sdPath) + "/pic_cache/http/cache_" + FileUtils.converPathToName(picurl));
            if (ShareApplication.debug) {
                System.out.println(String.valueOf(file.getAbsolutePath()) + "==");
            }
            if (file.exists()) {
                intent.putExtra("android.intent.extra.STREAM", Uri.fromFile(file));
                if (file.getName().endsWith(".gz")) {
                    intent.setType("application/x-gzip");
                } else if (file.getName().endsWith(".txt")) {
                    intent.setType("text/plain");
                } else {
                    intent.setType(FilePart.DEFAULT_CONTENT_TYPE);
                }
            } else {
                intent.setType("plain/text");
            }
        } else {
            intent.setType("plain/text");
        }
        try {
            startActivity(intent);
        } catch (Exception e) {
            Utils.showToast("请安装邮件客户端");
        }
    }

    public void sendToWeixin(final String picUrl) {
        new Thread() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.palmtrends.baseui.BasePicInfoActivity.zoomImage(android.graphics.Bitmap, float, float):android.graphics.Bitmap
             arg types: [android.graphics.Bitmap, int, int]
             candidates:
              com.palmtrends.baseui.BasePicInfoActivity.zoomImage(android.graphics.Bitmap, double, double):android.graphics.Bitmap
              com.palmtrends.baseui.BasePicInfoActivity.zoomImage(android.graphics.Bitmap, float, float):android.graphics.Bitmap */
            public void run() {
                int i = 1;
                String url = "";
                try {
                    WXImageObject imgObj = new WXImageObject();
                    if (picUrl != null && !"".equals(picUrl) && !"null".equals(picUrl) && !picUrl.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                        url = String.valueOf(Urls.main) + picUrl;
                    }
                    if ("".equals(url)) {
                        BasePicInfoActivity.this.wxHandler.sendEmptyMessage(0);
                        return;
                    }
                    Bitmap bm = BasePicInfoActivity.mImageWorker.getImageCache().getBitmapFromDiskCache(url);
                    imgObj.imageUrl = url;
                    if (bm == null) {
                        bm = BitmapFactory.decodeStream(new URL(url).openStream());
                    }
                    Bitmap thumbBmp = BasePicInfoActivity.zoomImage(bm, 150.0f, 150.0f);
                    bm.recycle();
                    WXMediaMessage msg = new WXMediaMessage();
                    msg.mediaObject = imgObj;
                    msg.thumbData = Util.bmpToByteArray(thumbBmp, true);
                    msg.title = BasePicInfoActivity.this.shortID;
                    SendMessageToWX.Req req = new SendMessageToWX.Req();
                    req.transaction = BasePicInfoActivity.this.buildTransaction(Constants.PARAM_IMG_URL);
                    req.message = msg;
                    if (BasePicInfoActivity.this.weixin_type != 1) {
                        i = 0;
                    }
                    req.scene = i;
                    BasePicInfoActivity.this.api.sendReq(req);
                    BasePicInfoActivity.this.wxHandler.sendEmptyMessage(1);
                } catch (Exception e) {
                    e.printStackTrace();
                    BasePicInfoActivity.this.wxHandler.sendEmptyMessage(0);
                }
            }
        }.start();
    }

    public void endToWeixin() {
    }

    /* access modifiers changed from: private */
    public String buildTransaction(String type) {
        if (type == null) {
            return String.valueOf(System.currentTimeMillis());
        }
        return String.valueOf(type) + System.currentTimeMillis();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap zoomImage(Bitmap bgimage, float newWidth, float newHeight) {
        float maxWidth;
        float width = (float) bgimage.getWidth();
        float height = (float) bgimage.getHeight();
        if (width > height) {
            maxWidth = height;
        } else {
            maxWidth = width;
        }
        Bitmap bitmap = Bitmap.createScaledBitmap(Bitmap.createBitmap(bgimage, (int) ((width - maxWidth) / 2.0f), (int) ((height - maxWidth) / 2.0f), (int) maxWidth, (int) maxWidth, (Matrix) null, true), (int) newWidth, (int) newHeight, true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        double mid = (double) (baos.toByteArray().length / 1024);
        Bitmap ret = bitmap;
        if (mid <= 32.0d) {
            return ret;
        }
        double i = (mid / 32.0d) + 1.0d;
        return zoomImage(bitmap, ((double) bitmap.getWidth()) / Math.sqrt(i), ((double) bitmap.getHeight()) / Math.sqrt(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        float width = (float) bgimage.getWidth();
        float height = (float) bgimage.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) newWidth) / width, ((float) newHeight) / height);
        return Bitmap.createBitmap(bgimage, 0, 0, (int) width, (int) height, matrix, true);
    }

    public void onDoubleTap() {
    }

    public void onSingleTapConfirmed() {
    }

    public void onLongPress() {
    }

    public void onLeftOption(boolean left) {
    }

    public void onRightOption(boolean right) {
    }

    public void changeIndex(int index) {
    }

    public void things(View view) {
    }
}
