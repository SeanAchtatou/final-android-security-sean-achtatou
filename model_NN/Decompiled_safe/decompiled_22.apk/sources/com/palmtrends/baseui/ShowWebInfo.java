package com.palmtrends.baseui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.palmtrends.controll.R;
import com.tencent.tauth.Constants;

public class ShowWebInfo extends BaseActivity {
    View load;
    ScaleGestureDetector sgd;
    WebView wv;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showwebinfo);
        String url = getIntent().getStringExtra(Constants.PARAM_URL);
        this.wv = (WebView) findViewById(R.id.webview);
        this.load = findViewById(R.id.loading);
        setWeiView(this.wv);
        this.wv.loadUrl(url);
    }

    public void things(View view) {
    }

    @SuppressLint({"NewApi"})
    public void setWeiView(WebView wv2) {
        wv2.getSettings().setJavaScriptEnabled(true);
        wv2.getSettings().setDatabaseEnabled(true);
        wv2.getSettings().setDomStorageEnabled(true);
        wv2.getSettings().setSupportZoom(true);
        wv2.getSettings().setBuiltInZoomControls(true);
        wv2.getSettings().setUseWideViewPort(true);
        wv2.getSettings().setDatabasePath(getApplicationContext().getDir("database" + getApplicationContext().getPackageName(), 0).getPath());
        wv2.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Uri uri = Uri.parse(url);
                Intent it = new Intent("android.intent.action.VIEW", uri);
                if (url.endsWith(".mp3")) {
                    it.setDataAndType(uri, "audio/mp3");
                    ShowWebInfo.this.startActivity(it);
                    ShowWebInfo.this.finish();
                    return true;
                } else if (!url.endsWith(".mp4")) {
                    return super.shouldOverrideUrlLoading(view, url);
                } else {
                    it.setFlags(67108864);
                    it.setType("video/mp4");
                    it.setDataAndType(uri, "video/mp4");
                    ShowWebInfo.this.startActivity(it);
                    ShowWebInfo.this.finish();
                    return true;
                }
            }

            public void onLoadResource(WebView view, String url) {
                if (url.endsWith("mp3") || url.endsWith("jpg") || url.endsWith("png")) {
                    ShowWebInfo.this.load.setVisibility(8);
                }
                super.onLoadResource(view, url);
            }

            public void onPageFinished(WebView view, String url) {
                if (url.endsWith(".apk")) {
                    ShowWebInfo.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                    ShowWebInfo.this.finish();
                }
                ShowWebInfo.this.load.setVisibility(8);
                super.onPageFinished(view, url);
            }
        });
        this.sgd = new ScaleGestureDetector(this, new ScaleGestureDetector.OnScaleGestureListener() {
            public void onScaleEnd(ScaleGestureDetector detector) {
            }

            public boolean onScaleBegin(ScaleGestureDetector detector) {
                return false;
            }

            public boolean onScale(ScaleGestureDetector detector) {
                return false;
            }
        });
    }

    @SuppressLint({"NewApi"})
    public boolean onTouchEvent(MotionEvent event) {
        if (this.sgd == null) {
            return true;
        }
        this.sgd.onTouchEvent(event);
        return true;
    }

    public void changeModel(View v) {
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }
}
