package com.palmtrends.baseui;

import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.view.KeyEvent;
import android.widget.TabHost;
import com.palmtrends.apptime.AppTimeStatisticsService;
import com.palmtrends.controll.R;
import com.palmtrends.dao.ClientInfo;
import com.palmtrends.push.AlarmTools;
import com.utils.PerfHelper;

public class BaseMainActivity extends ActivityGroup {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!getResources().getBoolean(R.bool.hashome)) {
            if (getResources().getBoolean(R.bool.time_tip)) {
                PerfHelper.setInfo(PerfHelper.P_ALARM_TIME, AlarmTools.getAlarmDate());
                AlarmTools.setAlarmTime(this, true);
            }
            ClientInfo.check_update(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!getResources().getBoolean(R.bool.hashome)) {
            Process.killProcess(Process.myPid());
        }
    }

    public TabHost.TabSpec buildTabSpec(TabHost tab, String tag, Intent intent) {
        TabHost.TabSpec spec = tab.newTabSpec(tag);
        spec.setIndicator(tag);
        spec.setContent(intent);
        return spec;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean flag = getResources().getBoolean(R.bool.hashome);
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (!flag) {
            new AlertDialog.Builder(this).setTitle(R.string.exit).setMessage(getString(R.string.exit_message, new String[]{getString(R.string.app_name)})).setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    BaseMainActivity.this.finish();
                    BaseMainActivity.this.stopService(new Intent(BaseMainActivity.this, AppTimeStatisticsService.class));
                }
            }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            }).show();
            return true;
        }
        finish();
        return true;
    }
}
