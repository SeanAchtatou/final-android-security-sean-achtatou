package com.palmtrends.baseui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import com.palmtrends.apptime.AppTimeStatisticsService;
import com.palmtrends.controll.R;
import com.palmtrends.dao.ClientInfo;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.entity.part;
import com.palmtrends.loadimage.Utils;
import com.palmtrends.push.MPushService;
import com.palmtrends.push.PushService;
import com.utils.FinalVariable;
import com.utils.GpsUtils;
import com.utils.PerfHelper;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BaseInitAcitvity extends BaseActivity {
    private static boolean sdcardAvailabilityDetected;
    private static boolean sdcardAvailable;
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1004) {
                BaseInitAcitvity.this.finish();
                Utils.showToast(R.string.network_error);
                return;
            }
            BaseInitAcitvity.this.begin_StartActivity();
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(arg0);
        PerfHelper.getPerferences(getApplicationContext());
        DBHelper.getDBHelper();
        PerfHelper.setInfo(PerfHelper.P_PHONE_W, getResources().getDisplayMetrics().widthPixels);
        PerfHelper.setInfo(PerfHelper.P_PHONE_H, getResources().getDisplayMetrics().heightPixels);
        GpsUtils.getLocation();
        initDataUserInfo();
    }

    public void begin_StartActivity() {
        MPushService.actionStart(this);
        startService(new Intent(this, AppTimeStatisticsService.class));
        Intent i = new Intent();
        if (getResources().getBoolean(R.bool.hashome)) {
            i.setAction(getResources().getString(R.string.activity_home));
        } else {
            i.setAction(getResources().getString(R.string.activity_main));
        }
        startActivity(i);
        overridePendingTransition(R.anim.init_in, R.anim.init_out);
        finish();
    }

    public void initparts() throws Exception {
        DBHelper db = DBHelper.getDBHelper();
        for (String split : getResources().getStringArray(R.array.second_names)) {
            int index = 0;
            String[] design_data = split.split(";");
            List<part> design_patrs = new ArrayList<>();
            for (String partstr : design_data) {
                part p = new part();
                String[] part = partstr.split(",");
                p.part_name = part[0];
                p.part_index = Integer.valueOf(index);
                index++;
                p.part_sa = part[2];
                p.part_type = part[1];
                design_patrs.add(p);
            }
            db.insert(design_patrs, "part_list", part.class);
        }
    }

    public void initNetPart() {
    }

    public void initDataUserInfo() {
        new Thread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
             arg types: [java.lang.String, int]
             candidates:
              com.utils.PerfHelper.setInfo(java.lang.String, int):void
              com.utils.PerfHelper.setInfo(java.lang.String, long):void
              com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
              com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
            public void run() {
                if ("".equals(PerfHelper.getStringData(PerfHelper.P_USERID)) || "11111111".equals(PerfHelper.getStringData(PerfHelper.P_USERID))) {
                    try {
                        ClientInfo.sendClient_UserInfo();
                        BaseInitAcitvity.this.initparts();
                        PerfHelper.setInfo(PerfHelper.P_PUSH, true);
                        PerfHelper.setInfo(PerfHelper.P_TEXT, "m");
                        PushService.push(BaseInitAcitvity.this);
                        PerfHelper.setInfo(PerfHelper.P_DATE_MODE, "day");
                        BaseInitAcitvity.this.handler.sendEmptyMessage(FinalVariable.update);
                    } catch (Exception e) {
                        BaseInitAcitvity.this.handler.sendEmptyMessage(FinalVariable.error);
                        e.printStackTrace();
                        return;
                    }
                } else {
                    try {
                        Thread.sleep(4000);
                        BaseInitAcitvity.this.handler.sendEmptyMessage(FinalVariable.update);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
                BaseInitAcitvity.this.initNetPart();
            }
        }).start();
    }

    public void things(View view) {
    }

    public static synchronized boolean detectSDCardAvailability() {
        boolean result;
        synchronized (BaseInitAcitvity.class) {
            try {
                new Date();
                File file = new File(Environment.getExternalStorageDirectory() + "/.test");
                boolean result2 = file.createNewFile();
                file.delete();
                result = true;
                sdcardAvailabilityDetected = true;
                sdcardAvailable = true;
            } catch (Exception e) {
                result = false;
                e.printStackTrace();
                sdcardAvailabilityDetected = true;
                sdcardAvailable = false;
            } catch (Throwable th) {
                sdcardAvailabilityDetected = true;
                sdcardAvailable = false;
                throw th;
            }
        }
        return result;
    }

    public static boolean isSDCardAvailable() {
        if (!sdcardAvailabilityDetected) {
            sdcardAvailable = detectSDCardAvailability();
            sdcardAvailabilityDetected = true;
        }
        return sdcardAvailable;
    }

    public void showOther() {
    }
}
