package com.palmtrends.baseui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import com.palmtrends.apptime.AppTimeStatisticsService;
import com.palmtrends.controll.R;
import com.palmtrends.dao.ClientInfo;
import com.palmtrends.push.AlarmTools;
import com.utils.PerfHelper;

public abstract class BaseHomeActivity extends BaseActivity {
    public abstract void things(View view);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        if (getResources().getBoolean(R.bool.time_tip)) {
            PerfHelper.setInfo(PerfHelper.P_ALARM_TIME, AlarmTools.getAlarmDate());
            AlarmTools.setAlarmTime(this, true);
        }
        ClientInfo.check_update(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        new AlertDialog.Builder(this).setTitle(R.string.exit).setMessage(getString(R.string.exit_message, new String[]{getString(R.string.app_name)})).setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BaseHomeActivity.this.sendBroadcast(new Intent(BaseActivity.ACTIVITY_FINSH));
                BaseHomeActivity.this.stopService(new Intent(BaseHomeActivity.this, AppTimeStatisticsService.class));
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        }).show();
        return true;
    }
}
