package com.palmtrends.wb;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.loadimage.Utils;
import com.tencent.tauth.Constants;
import com.utils.FinalVariable;
import com.utils.PerfHelper;

public class WeiboCommentActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public CheckBox checkBox;
    /* access modifiers changed from: private */
    public EditText editText;
    private ImageView fhImageview;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FinalVariable.vb_success /*10000*/:
                    WeiboCommentActivity.this.loading.setVisibility(8);
                    Utils.showToast(msg.obj.toString());
                    if (WeiboCommentActivity.this.type == 1 || WeiboCommentActivity.this.checkBox.isChecked()) {
                        WeiboCommentActivity.this.setResult(FinalVariable.vb_success, WeiboCommentActivity.this.getIntent());
                    }
                    WeiboCommentActivity.this.finish();
                    return;
                case 10001:
                    String pushbind = String.valueOf(WeiboCommentActivity.this.url) + "?pid=" + FinalVariable.pid + "&cid=3" + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID);
                    Intent intent = new Intent();
                    intent.putExtra("m_mainurl", String.valueOf(pushbind) + "&sname=" + WeiboCommentActivity.this.sname);
                    intent.putExtra("sname", WeiboCommentActivity.this.sname);
                    intent.setAction(WeiboCommentActivity.this.getResources().getString(R.string.activity_share_bind));
                    WeiboCommentActivity.this.startActivity(intent);
                    return;
                case 10002:
                    WeiboCommentActivity.this.loading.setVisibility(8);
                    Utils.showToast(msg.obj.toString());
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public View loading;
    private TextView mark_textView;
    /* access modifiers changed from: private */
    public int num = 280;
    private String sid;
    /* access modifiers changed from: private */
    public String sname;
    private TextView titleimage;
    /* access modifiers changed from: private */
    public int type;
    String url = "http://push.cms.palmtrends.com/wb/bind_v2.php";
    /* access modifiers changed from: private */
    public TextView zishu_textView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.activity_pinglun);
        findView();
        this.sid = getIntent().getStringExtra("sid");
        this.sname = getIntent().getStringExtra("sname");
        this.type = getIntent().getIntExtra("type", 1);
        addListener();
    }

    public static int getCharacterNum(String content) {
        if (content == null || "".equals(content)) {
            return 0;
        }
        return content.length() + getChineseNum(content);
    }

    public static int getChineseNum(String s) {
        int num2 = 0;
        char[] myChar = s.toCharArray();
        for (int i = 0; i < myChar.length; i++) {
            if (((char) ((byte) myChar[i])) != myChar[i]) {
                num2++;
            }
        }
        return num2;
    }

    public void findView() {
        this.checkBox = (CheckBox) findViewById(R.id.wb_checkbox);
        this.editText = (EditText) findViewById(R.id.wb_pinglun_conmment);
        this.zishu_textView = (TextView) findViewById(R.id.wb_pinglun_zishu);
        this.titleimage = (TextView) findViewById(R.id.title_title);
        String title = getIntent().getStringExtra(Constants.PARAM_TITLE);
        if (title != null && !"".equals(title)) {
            this.titleimage.setText(title);
        }
        this.mark_textView = (TextView) findViewById(R.id.wb_pinglun_marktext);
        String info = getIntent().getStringExtra("info");
        if (info != null && !"".equals(info)) {
            this.mark_textView.setText(info);
        }
        this.loading = findViewById(R.id.loading);
        this.loading.setVisibility(8);
    }

    public void addListener() {
        this.loading.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.editText.addTextChangedListener(new TextWatcher() {
            private int selectionEnd;
            private int selectionStart;
            private CharSequence temp;

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                this.temp = s;
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                int number = WeiboCommentActivity.this.num - WeiboCommentActivity.getCharacterNum(WeiboCommentActivity.this.editText.getText().toString());
                WeiboCommentActivity.this.zishu_textView.setText(new StringBuilder().append(number / 2).toString());
                if (number / 2 <= -1) {
                    WeiboCommentActivity.this.zishu_textView.setTextColor(WeiboCommentActivity.this.getResources().getColor(R.color.wb_red));
                } else {
                    WeiboCommentActivity.this.zishu_textView.setTextColor(WeiboCommentActivity.this.getResources().getColor(R.color.wb_black));
                }
                this.selectionStart = WeiboCommentActivity.this.editText.getSelectionStart();
                this.selectionEnd = WeiboCommentActivity.this.editText.getSelectionEnd();
                if (WeiboCommentActivity.getCharacterNum(WeiboCommentActivity.this.editText.getText().toString()) >= WeiboCommentActivity.this.num && number / 2 >= 0) {
                    s.delete(this.selectionStart - 1, this.selectionEnd);
                    int tempSelection = this.selectionEnd;
                    WeiboCommentActivity.this.editText.setText(s);
                    WeiboCommentActivity.this.editText.setSelection(tempSelection);
                }
            }
        });
    }

    public void things(View view) {
        int id = view.getId();
        if (id == R.id.title_forword) {
            if (Integer.parseInt(this.zishu_textView.getText().toString()) < 0) {
                Utils.showToast("字数不能超过140个，请编辑后再提交……");
            } else if (PerfHelper.getBooleanData(PerfHelper.P_SHARE_STATE + this.sname)) {
                String content = this.editText.getText().toString();
                if ((this.type == 1 || (this.type == 0 && this.checkBox.isChecked())) && "".equals(content)) {
                    Utils.showToast(R.string.not_null_tip);
                    return;
                }
                this.loading.setVisibility(0);
                if (this.type == 1 || this.checkBox.isChecked()) {
                    WeiboDao.weibo_comment(this.sname, content, this.sid, this.handler);
                }
                if (this.type == 0 || this.checkBox.isChecked()) {
                    WeiboDao.weibo_forwarding(this.sname, content, this.sid, this.handler);
                }
            } else {
                Utils.showToast("微博未绑定");
                String pushbind = String.valueOf(this.url) + "?pid=" + FinalVariable.pid + "&cid=3" + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID);
                Intent intent = new Intent();
                intent.putExtra("m_mainurl", String.valueOf(pushbind) + "&sname=" + this.sname);
                intent.putExtra("sname", this.sname);
                intent.setAction(getResources().getString(R.string.activity_share_bind));
                startActivity(intent);
            }
        } else if (id == R.id.title_back) {
            finish();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }
}
