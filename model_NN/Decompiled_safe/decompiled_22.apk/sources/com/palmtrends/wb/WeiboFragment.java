package com.palmtrends.wb;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Entity;
import com.palmtrends.entity.WeiboItemInfo;
import java.text.SimpleDateFormat;

public class WeiboFragment extends LoadMoreListFragment<WeiboItemInfo> {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public static WeiboFragment newInstance(String type, String partType) {
        WeiboFragment tf = new WeiboFragment();
        tf.initType(type, partType);
        return tf;
    }

    public boolean dealClick(WeiboItemInfo item, int position) {
        Intent intent = new Intent();
        intent.setAction(this.mContext.getResources().getString(R.string.activity_wb_info));
        intent.putExtra("item", item);
        this.mContext.startActivity(intent);
        return true;
    }

    static class Viewitem {
        TextView ccount;
        TextView create_time;
        ImageView nopic;
        TextView rcount;
        FrameLayout re_img;
        LinearLayout retweeted;
        TextView source;
        TextView status;
        TextView text;
        FrameLayout text_img;
        ImageView user_img;
        TextView user_name;
        TextView zf_username;

        Viewitem() {
        }
    }

    public View getListItemview(View itemView, WeiboItemInfo weiboinfo, int position) {
        Viewitem holder;
        if (itemView == null) {
            itemView = LayoutInflater.from(getActivity()).inflate(R.layout.wb_list_items, (ViewGroup) null);
            holder = new Viewitem();
            holder.create_time = (TextView) itemView.findViewById(R.id.wb_create_time);
            holder.nopic = (ImageView) itemView.findViewById(R.id.wb_no_pc);
            holder.user_img = (ImageView) itemView.findViewById(R.id.wb_user_img);
            holder.user_name = (TextView) itemView.findViewById(R.id.wb_user_name);
            holder.text = (TextView) itemView.findViewById(R.id.wb_text);
            holder.text_img = (FrameLayout) itemView.findViewById(R.id.wb_myimageview);
            holder.retweeted = (LinearLayout) itemView.findViewById(R.id.wv_retweeted);
            holder.status = (TextView) itemView.findViewById(R.id.wb_status);
            holder.re_img = (FrameLayout) itemView.findViewById(R.id.retweeted_myimageview);
            holder.source = (TextView) itemView.findViewById(R.id.wb_source);
            holder.ccount = (TextView) itemView.findViewById(R.id.wb_pinglun_count);
            holder.rcount = (TextView) itemView.findViewById(R.id.wb_zhuanfa_count);
            holder.zf_username = (TextView) itemView.findViewById(R.id.wb_zf_username);
            itemView.setTag(holder);
        } else {
            holder = (Viewitem) itemView.getTag();
        }
        WeiboItemInfo info = weiboinfo;
        if (info != null) {
            String img = info.getThumbnail_pic();
            if ("".equals(img) || img == null) {
                holder.text_img.setVisibility(8);
                holder.nopic.setVisibility(8);
            } else {
                ImageView iv = (ImageView) holder.text_img.findViewById(R.id.wb_myimageview_img);
                ShareApplication.mImageWorker.loadImage(img, iv);
                iv.setTag(String.valueOf(info.original_pic) + "b");
                holder.nopic.setVisibility(0);
                holder.text_img.setVisibility(0);
            }
            ShareApplication.mImageWorker.loadImage(info.getProfile_image_url(), holder.user_img);
            holder.user_name.setText(info.getName());
            holder.create_time.setText(this.sdf.format(info.getCreated_at()));
            holder.text.setText(info.getText());
            if (info.getRetweeted() != null) {
                holder.retweeted.setVisibility(0);
                holder.zf_username.setText(info.getRetweeted().getName());
                holder.status.setText(info.getRetweeted().getText());
                String thumbnail = info.getRetweeted().getThumbnail_pic();
                if ("".equals(thumbnail) || thumbnail == null) {
                    holder.re_img.setVisibility(8);
                } else {
                    ImageView iv2 = (ImageView) holder.re_img.findViewById(R.id.wb_retweeted__myimageview_img);
                    iv2.setTag(String.valueOf(info.retweeted.getBmiddle_pic()) + "b");
                    ShareApplication.mImageWorker.loadImage(thumbnail, iv2);
                    holder.nopic.setVisibility(0);
                    holder.re_img.setVisibility(0);
                }
            } else {
                holder.retweeted.setVisibility(8);
            }
            holder.ccount.setText(info.getcCount());
            holder.rcount.setText(info.getrCount());
            holder.source.setText("来自" + ((Object) Html.fromHtml(info.getSource())));
        }
        return itemView;
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        this.mFooter_limit = 5;
        Data data = new Data();
        data.list = WeiboDao.getJsonString_weibo(Urls.sina_list + oldtype + "&page=" + (page + 1) + "&count=" + count);
        return data;
    }

    public View getListHeadview(WeiboItemInfo item) {
        ((Activity) this.mContext).getIntent().getSerializableExtra("item");
        return super.getListHeadview((Entity) item);
    }

    public Data getDataFromDB(String oldtype, int page, int count, String parttype) throws Exception {
        return null;
    }
}
