package com.palmtrends.wb;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.ListFragment;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Items;
import com.palmtrends.entity.WeiboItemInfo;
import com.palmtrends.loadimage.Utils;
import com.palmtrends.wb.WeiboFragment;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.text.SimpleDateFormat;

public class WeiboInfoFragment extends ListFragment<Items> {
    public View.OnClickListener click = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent();
            i.setFlags(536870912);
            i.setClass(WeiboInfoFragment.this.mContext, WeiboShowImageActivity.class);
            i.putExtra("path", v.getTag().toString());
            WeiboInfoFragment.this.startActivity(i);
        }
    };
    View itemView;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    String url = "http://push.cms.palmtrends.com/wb/bind_v2.php";

    public static WeiboInfoFragment newInstance(String type, String partType) {
        WeiboInfoFragment tf = new WeiboInfoFragment();
        tf.initType(type, partType);
        return tf;
    }

    public boolean dealClick(Items item, int position) {
        return true;
    }

    public void findView() {
        super.findView();
        System.out.println("数据加载!+");
        this.footer_text.setText(this.mContext.getResources().getString(R.string.loading));
        this.footer_pb.setVisibility(0);
        this.isShowAD = false;
    }

    public View getListItemview(View itemView2, Items weiboinfo, int position) {
        View itemView3 = LayoutInflater.from(this.mContext).inflate(R.layout.listitem_wb_comments, (ViewGroup) null);
        ((TextView) itemView3.findViewById(R.id.wb_commenter_name)).setText(weiboinfo.title);
        ((TextView) itemView3.findViewById(R.id.wb_comment_time)).setText(weiboinfo.other);
        ((TextView) itemView3.findViewById(R.id.wb_comment)).setText(weiboinfo.des);
        if (position % 2 == 0) {
            itemView3.setBackgroundResource(R.drawable.d_selector);
        } else {
            itemView3.setBackgroundResource(R.drawable.s_selector);
        }
        return itemView3;
    }

    class ViewHolder {
        TextView create_time;
        TextView text;
        TextView user_name;

        ViewHolder() {
        }
    }

    public void addListener() {
        this.mList_footer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!WeiboInfoFragment.this.isloading) {
                    if (PerfHelper.getBooleanData("p_share_state_settingsina")) {
                        try {
                            WeiboInfoFragment.this.footer_text.setText(WeiboInfoFragment.this.mContext.getResources().getString(R.string.loading));
                            WeiboInfoFragment.this.footer_pb.setVisibility(0);
                            WeiboInfoFragment.this.isloading = true;
                            new ListFragment.LoadDataTask().execute((Object[]) null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        String pushbind = String.valueOf(WeiboInfoFragment.this.url) + "?pid=" + FinalVariable.pid + "&cid=3" + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID);
                        Intent intent = new Intent();
                        intent.putExtra("m_mainurl", String.valueOf(pushbind) + "&sname=sina");
                        intent.putExtra("sname", "sina");
                        intent.setAction(WeiboInfoFragment.this.getResources().getString(R.string.activity_share_bind));
                        WeiboInfoFragment.this.startActivityForResult(intent, 1);
                    }
                }
            }
        });
        this.mHandler = new Handler() {
            /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
            public void handleMessage(Message msg) {
                WeiboInfoFragment.this.mLoading.setVisibility(8);
                switch (msg.what) {
                    case FinalVariable.update /*1001*/:
                        break;
                    case FinalVariable.remove_footer /*1002*/:
                        if (WeiboInfoFragment.this.mListview != null && WeiboInfoFragment.this.mList_footer != null && WeiboInfoFragment.this.mListview.getFooterViewsCount() > 0) {
                            if (!PerfHelper.getBooleanData("p_share_state_settingsina")) {
                                WeiboInfoFragment.this.initfooter_nobind();
                                return;
                            } else if (WeiboInfoFragment.this.mlistAdapter.datas.size() == 1) {
                                WeiboInfoFragment.this.footer_text.setText("暂无评论");
                                WeiboInfoFragment.this.footer_pb.setVisibility(8);
                                return;
                            } else {
                                WeiboInfoFragment.this.mListview.removeFooterView(WeiboInfoFragment.this.mList_footer);
                                WeiboInfoFragment.this.initfooter();
                                return;
                            }
                        } else {
                            return;
                        }
                    case FinalVariable.change /*1003*/:
                    case FinalVariable.deletefoot /*1005*/:
                    case FinalVariable.first_load /*1008*/:
                    case FinalVariable.load_image /*1009*/:
                    case FinalVariable.other /*1010*/:
                    default:
                        return;
                    case FinalVariable.error /*1004*/:
                        Utils.showToast(R.string.network_error);
                        WeiboInfoFragment.this.initfooter();
                        return;
                    case FinalVariable.addfoot /*1006*/:
                        if (WeiboInfoFragment.this.mListview != null && WeiboInfoFragment.this.mList_footer != null && WeiboInfoFragment.this.mListview.getFooterViewsCount() == 0) {
                            WeiboInfoFragment.this.mListview.addFooterView(WeiboInfoFragment.this.mList_footer);
                            return;
                        }
                        return;
                    case FinalVariable.nomore /*1007*/:
                        Utils.showToast(WeiboInfoFragment.this.nodata_tip);
                        WeiboInfoFragment.this.initfooter();
                        return;
                    case FinalVariable.first_update /*1011*/:
                        if (!(WeiboInfoFragment.this.mlistAdapter == null || WeiboInfoFragment.this.mlistAdapter.datas == null)) {
                            WeiboInfoFragment.this.mlistAdapter.datas.clear();
                            break;
                        }
                }
                System.out.println(WeiboInfoFragment.this.mData + "==");
                WeiboInfoFragment.this.update();
            }
        };
    }

    public void wbreflash() {
        new ListFragment.GetDataTask().execute(new Void[0]);
    }

    public void initfooter() {
        this.mHandler.post(new Runnable() {
            public void run() {
                WeiboInfoFragment.this.footer_text.setText(WeiboInfoFragment.this.mContext.getResources().getString(R.string.loading_n));
                WeiboInfoFragment.this.footer_pb.setVisibility(8);
            }
        });
    }

    public void initfooter_nobind() {
        this.mHandler.post(new Runnable() {
            public void run() {
                WeiboInfoFragment.this.footer_text.setText(WeiboInfoFragment.this.mContext.getResources().getString(R.string.weibo_nobind));
                WeiboInfoFragment.this.footer_pb.setVisibility(8);
            }
        });
    }

    public Data getDataFromNet(String url2, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        Data data = new Data();
        String url3 = Urls.sina_list + oldtype + "&page=" + (page + 1) + "&count=" + count;
        data.headtype = 1;
        if (PerfHelper.getBooleanData("p_share_state_settingsina")) {
            data.list = WeiboDao.weibo_get_commentlist("sina", oldtype, page + 1, count);
        }
        return data;
    }

    public View getListHeadview(Object obj, int type) {
        WeiboFragment.Viewitem holder;
        System.out.println("标题:");
        if (this.itemView == null) {
            this.itemView = LayoutInflater.from(getActivity()).inflate(R.layout.wb_info_head, (ViewGroup) null);
            holder = new WeiboFragment.Viewitem();
            holder.create_time = (TextView) ((Activity) this.mContext).findViewById(R.id.info_date);
            holder.nopic = (ImageView) ((Activity) this.mContext).findViewById(R.id.info_no_pc);
            holder.user_img = (ImageView) ((Activity) this.mContext).findViewById(R.id.info_logo);
            holder.user_name = (TextView) ((Activity) this.mContext).findViewById(R.id.info_name);
            holder.text = (TextView) this.itemView.findViewById(R.id.wb_text);
            holder.text_img = (FrameLayout) this.itemView.findViewById(R.id.wb_myimageview);
            holder.retweeted = (LinearLayout) this.itemView.findViewById(R.id.wv_retweeted);
            holder.status = (TextView) this.itemView.findViewById(R.id.wb_status);
            holder.re_img = (FrameLayout) this.itemView.findViewById(R.id.retweeted_myimageview);
            holder.source = (TextView) this.itemView.findViewById(R.id.wb_source);
            holder.ccount = (TextView) this.itemView.findViewById(R.id.wb_pinglun_count);
            holder.rcount = (TextView) this.itemView.findViewById(R.id.wb_zhuanfa_count);
            holder.zf_username = (TextView) this.itemView.findViewById(R.id.wb_zf_username);
            this.itemView.setTag(holder);
        } else {
            holder = (WeiboFragment.Viewitem) this.itemView.getTag();
        }
        WeiboItemInfo info = (WeiboItemInfo) ((Activity) this.mContext).getIntent().getSerializableExtra("item");
        if (info != null) {
            String img = info.getThumbnail_pic();
            if ("".equals(img) || img == null) {
                holder.text_img.setVisibility(8);
                holder.nopic.setVisibility(8);
            } else {
                ImageView iv = (ImageView) holder.text_img.findViewById(R.id.wb_myimageview_img);
                iv.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() != 1) {
                            return false;
                        }
                        WeiboInfoFragment.this.click.onClick(v);
                        return false;
                    }
                });
                ShareApplication.mImageWorker.loadImage(img, iv);
                iv.setTag(String.valueOf(info.original_pic) + "b");
                holder.nopic.setVisibility(0);
                holder.text_img.setVisibility(0);
            }
            ShareApplication.mImageWorker.loadImage(info.getProfile_image_url(), holder.user_img);
            holder.user_name.setText(info.getName());
            holder.create_time.setText(this.sdf.format(info.getCreated_at()));
            holder.text.setText(info.getText());
            if (info.getRetweeted() != null) {
                holder.retweeted.setVisibility(0);
                holder.zf_username.setText(info.getRetweeted().getName());
                holder.status.setText(info.getRetweeted().getText());
                String thumbnail = info.getRetweeted().getThumbnail_pic();
                if ("".equals(thumbnail) || thumbnail == null) {
                    holder.re_img.setVisibility(8);
                } else {
                    ImageView iv2 = (ImageView) holder.re_img.findViewById(R.id.wb_retweeted__myimageview_img);
                    iv2.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() != 1) {
                                return false;
                            }
                            WeiboInfoFragment.this.click.onClick(v);
                            return false;
                        }
                    });
                    iv2.setTag(String.valueOf(info.retweeted.original_pic) + "b");
                    ShareApplication.mImageWorker.loadImage(thumbnail, iv2);
                    holder.nopic.setVisibility(0);
                    holder.re_img.setVisibility(0);
                }
            } else {
                holder.retweeted.setVisibility(8);
            }
            holder.ccount.setText(info.getcCount());
            holder.rcount.setText(info.getrCount());
            holder.source.setText("来自" + ((Object) Html.fromHtml(info.getSource())));
        }
        return this.itemView;
    }

    public Data getDataFromDB(String oldtype, int page, int count, String parttype) throws Exception {
        return null;
    }
}
