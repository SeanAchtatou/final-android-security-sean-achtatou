package com.palmtrends.wb;

import android.os.Handler;
import android.os.Message;
import com.palmtrends.dao.MySSLSocketFactory;
import com.palmtrends.entity.Items;
import com.palmtrends.entity.WeiboItemInfo;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.FinalVariable;
import com.utils.JniUtils;
import com.utils.PerfHelper;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class WeiboDao {
    public static final String main_weibo = "http://push.cms.palmtrends.com/wb/api_v2.php";
    public static String pushbind = ("http://push.cms.palmtrends.com/wb/bind_v2.php?pid=" + FinalVariable.pid + "&cid=3" + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID));
    static SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
    static SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static String wb_tuijian = "http://push.cms.palmtrends.com/wb/sug2weibo_v2.php";
    public static final String weib_unbind = "http://push.cms.palmtrends.com/wb/unbind_v2.php";

    public static void weibo_forwarding(final String sname, String comment, String sid, final Handler handler) {
        final List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sname", sname));
        param.add(new BasicNameValuePair("action", "transmit"));
        param.add(new BasicNameValuePair("sid", sid));
        param.add(new BasicNameValuePair("comment", comment));
        new Thread() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
             arg types: [java.lang.String, int]
             candidates:
              com.utils.PerfHelper.setInfo(java.lang.String, int):void
              com.utils.PerfHelper.setInfo(java.lang.String, long):void
              com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
              com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
            public void run() {
                Message msg = new Message();
                try {
                    JSONObject jo = new JSONObject(MySSLSocketFactory.getinfo(WeiboDao.main_weibo, param));
                    msg.what = jo.getInt(Constants.SINA_CODE);
                    if (msg.what != 1) {
                        switch (msg.what) {
                            case 0:
                                msg.what = 10002;
                                msg.obj = jo.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG);
                                break;
                            case 2:
                                msg.what = 10001;
                                if (PerfHelper.getBooleanData(PerfHelper.P_SHARE_STATE + sname)) {
                                    msg.obj = "请重新绑定微博账号";
                                } else {
                                    msg.obj = "请先绑定微博账号";
                                }
                                PerfHelper.setInfo(PerfHelper.P_SHARE_STATE + sname, false);
                                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_ID + sname, "");
                                break;
                            case 3:
                                msg.what = 10002;
                                msg.obj = "微博已转发";
                                break;
                        }
                    } else {
                        msg.what = FinalVariable.vb_success;
                        msg.obj = "转发成功";
                    }
                } catch (Exception e) {
                    msg.what = 10002;
                    msg.obj = "转发失败";
                    e.printStackTrace();
                }
                handler.sendMessage(msg);
            }
        }.start();
    }

    public static void weibo_comment(final String sname, String comment, String sid, final Handler handler) {
        String str = "http://push.cms.palmtrends.com/wb/api_v2.php?sname=" + sname + "&action=comment&sid=" + sid + "&comment=" + comment;
        final List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sname", sname));
        param.add(new BasicNameValuePair("action", "comment"));
        param.add(new BasicNameValuePair("sid", sid));
        param.add(new BasicNameValuePair("comment", comment));
        new Thread() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
             arg types: [java.lang.String, int]
             candidates:
              com.utils.PerfHelper.setInfo(java.lang.String, int):void
              com.utils.PerfHelper.setInfo(java.lang.String, long):void
              com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
              com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
            public void run() {
                Message msg = new Message();
                try {
                    JSONObject jo = new JSONObject(MySSLSocketFactory.getinfo(WeiboDao.main_weibo, param));
                    msg.what = jo.getInt(Constants.SINA_CODE);
                    if (msg.what != 1) {
                        switch (msg.what) {
                            case 0:
                                msg.what = 10002;
                                msg.obj = jo.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG);
                                break;
                            case 2:
                                msg.what = 10001;
                                if (PerfHelper.getBooleanData(PerfHelper.P_SHARE_STATE + sname)) {
                                    msg.obj = "请重新绑定微博账号";
                                } else {
                                    msg.obj = "请先绑定微博账号";
                                }
                                PerfHelper.setInfo(PerfHelper.P_SHARE_STATE + sname, false);
                                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE + sname, "");
                                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_ID + sname, "");
                                break;
                            case 3:
                                msg.what = 10002;
                                msg.obj = "微博已评论";
                                break;
                        }
                    } else {
                        msg.what = FinalVariable.vb_success;
                        msg.obj = "评论成功";
                    }
                } catch (Exception e) {
                    msg.what = 10002;
                    msg.obj = "评论失败";
                    e.printStackTrace();
                }
                handler.sendMessage(msg);
            }
        }.start();
    }

    public static ArrayList weibo_get_commentlist(String sname, String sid, int page, int pagesize) {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sname", sname));
        param.add(new BasicNameValuePair("sid", sid));
        param.add(new BasicNameValuePair("count", new StringBuilder(String.valueOf(pagesize)).toString()));
        param.add(new BasicNameValuePair("page", new StringBuilder().append(page).toString()));
        param.add(new BasicNameValuePair("action", "comments"));
        ArrayList al = new ArrayList();
        try {
            JSONArray json = new JSONArray(MySSLSocketFactory.getinfo(main_weibo, param));
            int count = json.length();
            for (int i = 0; i < count; i++) {
                JSONObject obj = json.getJSONObject(i);
                Items item = new Items();
                item.nid = obj.getString("wbid");
                item.des = obj.getString("content");
                item.other = sdf1.format(sdf.parse(obj.getString("adddate")));
                item.title = obj.getString("username");
                item.icon = obj.getString("head_url");
                al.add(item);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return al;
    }

    public static String getInfo(String url) throws Exception {
        try {
            HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
            String strs = EntityUtils.toString(httpclient.execute(new HttpGet(String.valueOf(url) + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID) + "&e=" + JniUtils.getkey())).getEntity(), "utf-8");
            httpclient.getConnectionManager().shutdown();
            return strs;
        } catch (Exception e) {
            e.printStackTrace();
            throw new SocketException();
        }
    }

    public static ArrayList getJsonString_weibo(String url) throws Exception {
        JSONArray json = new JSONObject(MySSLSocketFactory.getinfo(url, new ArrayList<>())).getJSONArray("statuses");
        ArrayList al = new ArrayList();
        int count = json.length();
        for (int i = 0; i < count; i++) {
            WeiboItemInfo o = new WeiboItemInfo();
            JSONObject obj = json.getJSONObject(i);
            o.setCreated_at(sdf.parse(obj.getString("created_at")));
            o.setcCount(obj.getString("comments_count"));
            JSONObject user = obj.getJSONObject("user");
            o.setProfile_image_url(user.getString(Constants.SINA_USER_IMG));
            o.setName(user.getString(Constants.SINA_NAME));
            o.setUserID(user.getString(LocaleUtil.INDONESIAN));
            o.setId(obj.getString("mid"));
            o.setSource(obj.getString(com.tencent.tauth.Constants.PARAM_SOURCE));
            o.setText(obj.getString("text"));
            o.setrCount(obj.getString("reposts_count"));
            try {
                o.setBmiddle_pic(obj.getString("bmiddle_pic"));
                o.setThumbnail_pic(obj.getString("thumbnail_pic"));
                o.setOriginal_pic(obj.getString("original_pic"));
            } catch (Exception e) {
            }
            try {
                JSONObject re = obj.getJSONObject("retweeted_status");
                WeiboItemInfo ro = new WeiboItemInfo();
                ro.setCreated_at(sdf.parse(re.getString("created_at")));
                ro.setcCount(re.getString("reposts_count"));
                JSONObject user1 = re.getJSONObject("user");
                ro.setProfile_image_url(user1.getString(Constants.SINA_USER_IMG));
                ro.setName(user1.getString(Constants.SINA_NAME));
                ro.setUserID(user1.getString(LocaleUtil.INDONESIAN));
                ro.setId(re.getString(LocaleUtil.INDONESIAN));
                ro.setSource(re.getString(com.tencent.tauth.Constants.PARAM_SOURCE));
                ro.setText(re.getString("text"));
                ro.setrCount(re.getString("reposts_count"));
                try {
                    ro.setBmiddle_pic(re.getString("bmiddle_pic"));
                    ro.setThumbnail_pic(re.getString("thumbnail_pic"));
                    ro.setOriginal_pic(re.getString("original_pic"));
                } catch (Exception e2) {
                }
                o.setRetweeted(ro);
                o.setRetweetedID(ro.getId());
            } catch (Exception e3) {
            }
            al.add(o);
        }
        return al;
    }

    public static String getInfo_weibo(String url) throws Exception {
        HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
        String strs = EntityUtils.toString(httpclient.execute(new HttpGet(String.valueOf(url) + "&e=" + JniUtils.getkey())).getEntity(), "utf-8");
        httpclient.getConnectionManager().shutdown();
        return strs;
    }
}
