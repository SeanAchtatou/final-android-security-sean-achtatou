package com.palmtrends.wb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.entity.WeiboItemInfo;
import com.tencent.tauth.Constants;

public class WeiBoInfoActivity extends BaseActivity {
    Fragment frag = null;
    WeiboInfoFragment weibofragment = null;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weibo_info);
        initFragment();
    }

    public void initFragment() {
        WeiboItemInfo wii = (WeiboItemInfo) getIntent().getSerializableExtra("item");
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(getIntent().getStringExtra("data"));
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.weibofragment = (WeiboInfoFragment) findresult;
        }
        if (this.weibofragment == null) {
            this.weibofragment = WeiboInfoFragment.newInstance(wii.getId(), "wb_info");
            this.weibofragment.layout_id = R.layout.list_weibo;
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.weibofragment;
        fragmentTransaction.add(R.id.part_content, this.frag, new StringBuilder(String.valueOf(R.id.part_content)).toString());
        fragmentTransaction.commit();
    }

    public void things(View view) {
        int id = view.getId();
        if (id == R.id.wb_myimageview_img || view.getId() == R.id.wb_retweeted__myimageview_img) {
            Intent i = new Intent();
            i.setFlags(536870912);
            i.setClass(this, WeiboShowImageActivity.class);
            i.putExtra("path", view.getTag().toString());
            startActivity(i);
        } else if (id == R.id.title_back) {
            finish();
        } else if (id == R.id.title_comment) {
            Intent intent = new Intent();
            intent.setAction(getResources().getString(R.string.activity_wb_comment));
            intent.putExtra("sid", ((WeiboItemInfo) getIntent().getSerializableExtra("item")).getId());
            intent.putExtra("sname", "sina");
            startActivityForResult(intent, 1);
        } else if (id == R.id.title_forwoarding) {
            Intent intent2 = new Intent();
            WeiboItemInfo wii = (WeiboItemInfo) getIntent().getSerializableExtra("item");
            intent2.setAction(getResources().getString(R.string.activity_wb_comment));
            intent2.putExtra("sid", wii.getId());
            intent2.putExtra("sname", "sina");
            intent2.putExtra("type", 0);
            intent2.putExtra(Constants.PARAM_TITLE, "微博转发");
            intent2.putExtra("info", "同时评论给" + wii.getName());
            startActivity(intent2);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int arg0, int arg1, Intent arg2) {
        if (arg1 == 10000) {
            this.weibofragment.wbreflash();
        }
        super.onActivityResult(arg0, arg1, arg2);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }
}
