package com.palmtrends.wb;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import com.palmtrends.datasource.ImageLoadUtils;
import com.palmtrends.entity.DataTransport;
import com.palmtrends.loadimage.Utils;
import com.utils.FileUtils;
import java.io.IOException;

public class WeiboShowImageActivity extends Activity {
    static final int DRAG = 1;
    static final int NONE = 0;
    private static final String TAG = "Touch";
    static final int ZOOM = 2;
    public static boolean is = true;
    View ProgressBar = null;
    Bitmap bit;
    float dist = 1.0f;
    DisplayMetrics dm;
    float h;
    /* access modifiers changed from: private */
    public Handler h1 = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (WeiboShowImageActivity.this.imageView == null || msg.obj == null) {
                Utils.showToast("图片下载失败");
                return;
            }
            if (((BitmapDrawable) ((DataTransport) msg.obj).bit) != null) {
                WeiboShowImageActivity.this.bit = ((BitmapDrawable) ((DataTransport) msg.obj).bit).getBitmap();
                if (WeiboShowImageActivity.this.bit == null) {
                    Utils.showToast("图片下载失败");
                    return;
                }
            }
            WeiboShowImageActivity.this.imageView.setVisibility(0);
            WeiboShowImageActivity.this.ProgressBar.setVisibility(8);
            WeiboShowImageActivity.this.setImage(WeiboShowImageActivity.this.bit);
            WeiboShowImageActivity.this.imageView.invalidate();
        }
    };
    ImageView imageView;
    String imgPath;
    ScrollView mScrollView;
    PointF mid = new PointF();
    int mode = 0;
    float oldDist = 1.0f;
    PointF prev = new PointF();
    PointF start = new PointF();
    float w;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.showimageview);
        this.ProgressBar = findViewById(R.id.ProgressBar);
        this.ProgressBar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.imgPath = bundle.getString("path");
        }
        this.dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        this.imageView = (ImageView) findViewById(R.id.show_img);
        getImg(this.imgPath, "bmiddle_");
    }

    public void back(View v) {
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    public void things(View v) {
        if (v.getId() == R.id.title_down_btn) {
            if (this.bit == null) {
                Utils.showToast("图片正在下载...");
                return;
            }
            Utils.showProcessDialog(this, "正在保存图片...");
            FileUtils.writeToFile(this.bit, this.imgPath);
        } else if (v.getId() == R.id.title_back) {
            finish();
        }
    }

    private void getImg(final String url, final String type) {
        if (!"".equals(url) && url != null) {
            String imagename = String.valueOf(type) + FileUtils.converPathToName(url);
            if (FileUtils.isFileExist("image/" + imagename)) {
                try {
                    this.bit = ((BitmapDrawable) FileUtils.getImageSd(imagename)).getBitmap();
                    this.ProgressBar.setVisibility(8);
                    this.imageView.setVisibility(0);
                    setImage(this.bit);
                    this.imageView.invalidate();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                new Thread(new Runnable() {
                    public void run() {
                        ImageLoadUtils.downloadFile(url, type, WeiboShowImageActivity.this.h1);
                    }
                }).start();
                this.imageView.setTag(url);
            }
        }
    }

    public void setImage(Bitmap bit2) {
        this.imageView.setImageBitmap(bit2);
    }
}
