package com.palmtrends.baseview;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.controll.R;
import com.utils.PerfHelper;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PullToRefreshListView extends ListView {
    private static final int DONE = 3;
    private static final int PULL_To_REFRESH = 1;
    private static final int RATIO = 2;
    private static final int REFRESHING = 2;
    private static final int RELEASE_To_REFRESH = 0;
    private static final String TAG = "abc";
    public static final String update_time = "pull_update_time";
    private RotateAnimation animation;
    private ImageView arrowImageView;
    Handler h = new Handler();
    private int headContentHeight;
    private int headContentWidth;
    /* access modifiers changed from: private */
    public LinearLayout headView;
    private LayoutInflater inflater;
    private boolean isBack;
    private boolean isRecored;
    private TextView lastUpdatedTextView;
    private int movelength;
    private ProgressBar progressBar;
    public OnRefreshListener refreshListener;
    private RotateAnimation reverseAnimation;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private int startY;
    private int state = 3;
    private TextView tipsTextview;
    private TranslateAnimation translateAnimation;

    public interface OnRefreshListener {
        void onRefresh();
    }

    public PullToRefreshListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.headView = (LinearLayout) this.inflater.inflate(R.layout.head, (ViewGroup) null);
        this.arrowImageView = (ImageView) this.headView.findViewById(R.id.head_arrowImageView);
        this.arrowImageView.setMinimumWidth(50);
        this.arrowImageView.setMinimumHeight(50);
        this.progressBar = (ProgressBar) this.headView.findViewById(R.id.head_progressBar);
        this.tipsTextview = (TextView) this.headView.findViewById(R.id.head_tipsTextView);
        this.lastUpdatedTextView = (TextView) this.headView.findViewById(R.id.head_lastUpdatedTextView);
        measureView(this.headView);
        this.headContentHeight = this.headView.getMeasuredHeight();
        this.headContentWidth = this.headView.getMeasuredWidth();
        this.headView.setPadding(0, this.headContentHeight * -1, 0, 0);
        this.headView.invalidate();
        if (ShareApplication.debug) {
            Log.v("size", "width:" + this.headContentWidth + " height:" + this.headContentHeight);
        }
        addHeaderView(this.headView);
        this.animation = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.animation.setInterpolator(new LinearInterpolator());
        this.animation.setDuration(250);
        this.animation.setFillAfter(true);
        this.reverseAnimation = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.reverseAnimation.setInterpolator(new LinearInterpolator());
        this.reverseAnimation.setDuration(250);
        this.reverseAnimation.setFillAfter(true);
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (getFirstVisiblePosition() == 0 && !this.isRecored) {
                    this.startY = (int) event.getY();
                    this.isRecored = true;
                    break;
                }
            case 1:
                if (this.state != 2) {
                    if (this.state == 1) {
                        this.state = 3;
                        changeHeaderViewByState();
                        if (ShareApplication.debug) {
                            Log.v(TAG, "由下拉刷新状态，到done状态");
                        }
                    }
                    if (this.state == 0) {
                        this.state = 2;
                        changeHeaderViewByState();
                        onRefresh();
                    }
                }
                this.isRecored = false;
                this.isBack = false;
                break;
            case 2:
                int tempY = (int) event.getY();
                if (!this.isRecored && getFirstVisiblePosition() == 0) {
                    if (ShareApplication.debug) {
                        Log.v(TAG, "在move时候记录下位置");
                    }
                    this.isRecored = true;
                    this.startY = tempY;
                }
                if (this.state != 2 && this.isRecored) {
                    if (this.state == 0) {
                        if ((tempY - this.startY) / 2 < this.headContentHeight && (tempY - this.startY) / 2 > 0) {
                            this.state = 1;
                            changeHeaderViewByState();
                            if (ShareApplication.debug) {
                                Log.v(TAG, "由松开刷新状态转变到下拉刷新状态");
                            }
                        } else if ((tempY - this.startY) / 2 <= 0) {
                            this.state = 3;
                            changeHeaderViewByState();
                            if (ShareApplication.debug) {
                                Log.v(TAG, "由松开刷新状态转变到done状态");
                            }
                        }
                    }
                    if (this.state == 1) {
                        if ((tempY - this.startY) / 2 >= this.headContentHeight) {
                            this.state = 0;
                            this.isBack = true;
                            changeHeaderViewByState();
                            if (ShareApplication.debug) {
                                Log.v(TAG, "由done或者下拉刷新状态转变到松开刷新");
                            }
                        } else if (tempY - this.startY <= 0) {
                            this.state = 3;
                            changeHeaderViewByState();
                            if (ShareApplication.debug) {
                                Log.v(TAG, "由DOne或者下拉刷新状态转变到done状态");
                            }
                        }
                    }
                    if (this.state == 3 && (tempY - this.startY) / 2 > 0) {
                        this.state = 1;
                        changeHeaderViewByState();
                    }
                    if (this.state == 1) {
                        this.headView.setPadding(0, (this.headContentHeight * -1) + ((tempY - this.startY) / 2), 0, 0);
                        this.headView.invalidate();
                    }
                    if (this.state == 0) {
                        this.headView.setPadding(0, ((tempY - this.startY) / 2) - this.headContentHeight, 0, 0);
                        this.movelength = ((tempY - this.startY) / 2) - this.headContentHeight;
                        this.headView.invalidate();
                        break;
                    }
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    private void changeHeaderViewByState() {
        float z;
        switch (this.state) {
            case 0:
                this.arrowImageView.setVisibility(0);
                this.progressBar.setVisibility(8);
                this.tipsTextview.setVisibility(0);
                this.lastUpdatedTextView.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.startAnimation(this.animation);
                this.tipsTextview.setText(getResources().getString(R.string.pull_to_refresh_release_label));
                return;
            case 1:
                this.progressBar.setVisibility(8);
                this.tipsTextview.setVisibility(0);
                this.lastUpdatedTextView.setVisibility(0);
                if ("".equals(PerfHelper.getStringData("pull_update_time" + getTag()))) {
                    this.lastUpdatedTextView.setText("尚未更新");
                } else {
                    this.lastUpdatedTextView.setText(PerfHelper.getStringData("pull_update_time" + getTag()));
                }
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(0);
                if (this.isBack) {
                    this.isBack = false;
                    this.arrowImageView.clearAnimation();
                    this.arrowImageView.startAnimation(this.reverseAnimation);
                    this.tipsTextview.setText(getResources().getString(R.string.pull_to_refresh_tap_label));
                } else {
                    this.tipsTextview.setText(getResources().getString(R.string.pull_to_refresh_tap_label));
                }
                if (ShareApplication.debug) {
                    Log.v(TAG, "当前状态，下拉刷新");
                    return;
                }
                return;
            case 2:
                float i = ((float) (this.headView.getHeight() - this.headContentHeight)) / ((float) getHeight());
                this.headView.setPadding(0, 0, 0, 0);
                this.translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, i, 1, 0.0f);
                this.translateAnimation.setDuration(300);
                this.translateAnimation.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                    }
                });
                setAnimation(this.translateAnimation);
                this.progressBar.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(8);
                this.tipsTextview.setText(getResources().getString(R.string.pull_to_refresh_refreshing_label));
                this.lastUpdatedTextView.setVisibility(0);
                return;
            case 3:
                int headY = this.headView.getHeight();
                this.headView.setPadding(0, this.headContentHeight * -1, 0, 0);
                if (headY <= this.headContentHeight) {
                    z = ((float) headY) / ((float) getHeight());
                } else {
                    z = ((float) this.headContentHeight) / ((float) getHeight());
                }
                this.translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, z, 1, 0.0f);
                this.translateAnimation.setDuration(300);
                this.translateAnimation.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                    }
                });
                setAnimation(this.translateAnimation);
                this.progressBar.setVisibility(8);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setImageResource(R.drawable.ic_pulltorefresh_arrow);
                this.tipsTextview.setText(getResources().getString(R.string.pull_to_refresh_tap_label));
                this.lastUpdatedTextView.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public void deal(int current) {
        if (this.headContentHeight < current) {
            this.headView.setPadding(0, this.headContentHeight * -1, 0, 0);
            return;
        }
        final int c = current + 45;
        this.h.postDelayed(new Runnable() {
            public void run() {
                PullToRefreshListView.this.headView.setPadding(0, c * -1, 0, 0);
                PullToRefreshListView.this.deal(c);
            }
        }, 1);
    }

    public void setOnRefreshListener(OnRefreshListener refreshListener2) {
        this.refreshListener = refreshListener2;
    }

    public void onRefreshComplete(String oldtype) {
        this.state = 3;
        PerfHelper.setInfo("pull_update_time" + oldtype, String.valueOf(getResources().getString(R.string.pull_to_refresh_update)) + this.sdf.format(new Date()));
        changeHeaderViewByState();
    }

    private void onRefresh() {
        if (this.refreshListener != null) {
            this.refreshListener.onRefresh();
        }
    }

    private void measureView(View child) {
        int childHeightSpec;
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(-1, -2);
        }
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int lpHeight = p.height;
        if (lpHeight > 0) {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(lpHeight, 1073741824);
        } else {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }
}
