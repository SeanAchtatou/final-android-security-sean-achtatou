package com.palmtrends.baseview;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.palmtrends.entity.Listitem;

public class ImageDetailViewPager extends ViewPager {
    private boolean isLeft = true;
    private boolean isMove = true;
    private boolean isRight = true;
    private OnViewListener mListener;

    public interface OnArticleOptions {
        public static final int FAV_ = 2;
        public static final int FAV_CANCLE = 1;

        void onInitData(Listitem listitem);

        void onThings(int i);
    }

    public interface OnViewListener {
        void onDoubleTap();

        void onLeftOption(boolean z);

        void onLongPress();

        void onRightOption(boolean z);

        void onSingleTapConfirmed();
    }

    public ImageDetailViewPager(Context context) {
        super(context);
    }

    public ImageDetailViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.isLeft || this.isRight) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    public void isMove(boolean move) {
        this.isMove = move;
    }

    public void isLeft(boolean left) {
        this.isLeft = left;
        if (this.mListener != null) {
            this.mListener.onLeftOption(left);
        }
    }

    public void isRight(boolean right) {
        this.isRight = right;
        if (this.mListener != null) {
            this.mListener.onRightOption(right);
        }
    }

    public OnViewListener getOnViewListener() {
        return this.mListener;
    }

    public void setOnViewListener(OnViewListener mListener2) {
        this.mListener = mListener2;
    }
}
