package com.palmtrends.baseview;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.controll.R;
import com.palmtrends.entity.part;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;

public class SecondScrollView extends RelativeLayout implements View.OnClickListener {
    public static Fragment frag;
    public Fragment[] all_frag;
    public View content;
    public int content_id;
    public boolean hasAnimation;
    public int init_h;
    public LinearLayout.LayoutParams init_layoutparam;
    public int init_w;
    public View old_item;
    public View old_move_item;
    public List<part> parts;
    public Fragment result;
    public RelativeLayout seccond_allitem;
    public boolean second_canscroll;
    public HorizontalScrollView second_content;
    public LinearLayout second_items;
    public View second_left;
    public LinearLayout second_move_items;
    public View second_pasue;
    public View second_right;
    public View secondscroll;

    public SecondScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.parts = new ArrayList();
        this.second_canscroll = true;
        this.content_id = R.id.secondscroll;
        this.hasAnimation = true;
        this.init_w = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.init_h = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.content = LayoutInflater.from(getContext()).inflate(R.layout.second_content, (ViewGroup) null);
        this.secondscroll = LayoutInflater.from(getContext()).inflate(R.layout.secondscroll, (ViewGroup) null);
    }

    public SecondScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.parts = new ArrayList();
        this.second_canscroll = true;
        this.content_id = R.id.secondscroll;
        this.hasAnimation = true;
        this.init_w = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.init_h = View.MeasureSpec.makeMeasureSpec(0, 0);
    }

    public SecondScrollView(Context context) {
        super(context);
        this.parts = new ArrayList();
        this.second_canscroll = true;
        this.content_id = R.id.secondscroll;
        this.hasAnimation = true;
        this.init_w = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.init_h = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.content = LayoutInflater.from(getContext()).inflate(R.layout.second_content, (ViewGroup) null);
        this.secondscroll = LayoutInflater.from(getContext()).inflate(R.layout.secondscroll, (ViewGroup) null);
        this.seccond_allitem = (RelativeLayout) this.secondscroll.findViewById(R.id.seccond_allitem);
        this.second_left = this.secondscroll.findViewById(R.id.second_left);
        this.second_right = this.secondscroll.findViewById(R.id.second_rigth);
        this.second_content = (HorizontalScrollView) this.secondscroll.findViewById(R.id.second_content);
    }

    public void setsecond_Canscroll(boolean canornot) {
        this.second_canscroll = canornot;
        if (!this.second_canscroll) {
            this.second_right.setVisibility(8);
            this.second_left.setVisibility(8);
            FrameLayout.LayoutParams hl = new FrameLayout.LayoutParams(-1, -2);
            hl.gravity = 17;
            this.seccond_allitem.setLayoutParams(hl);
        }
    }

    public void setsecond_HasAnimation(boolean canornot) {
        this.hasAnimation = canornot;
    }

    public void setScrollContentParam(RelativeLayout.LayoutParams rlp) {
        this.content.setLayoutParams(rlp);
    }

    public View getItem(int index, part info) {
        TextView vi2 = new TextView(getContext());
        vi2.setText(info.part_name);
        vi2.setTextSize(18.0f);
        vi2.setPadding(5, 0, 5, 0);
        vi2.setTextColor(getResources().getColor(17170444));
        return vi2;
    }

    public View getItem_move(int index, part info) {
        ImageView iv = new ImageView(getContext());
        iv.setBackgroundColor(getResources().getColor(17170432));
        return iv;
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        initSecondPart();
    }

    public void addLayout() {
        RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(-1, -1);
        rl.addRule(3, R.id.second_layout);
        this.content.setLayoutParams(rl);
        addView(this.content);
        addView(this.secondscroll);
    }

    public void initSecondPart() {
        boolean isfirst = true;
        this.second_items = (LinearLayout) this.secondscroll.findViewById(R.id.second_items);
        this.second_move_items = (LinearLayout) this.secondscroll.findViewById(R.id.move_items);
        this.second_move_items.removeAllViews();
        this.second_items.removeAllViews();
        removeAllViews();
        this.parts = getParts();
        int count = this.parts.size();
        this.content_id = Math.abs(this.parts.get(0).part_type.hashCode());
        this.content.setId(this.content_id);
        addLayout();
        this.all_frag = new Fragment[count];
        for (int i = 0; i < count; i++) {
            View v = getItem(i, this.parts.get(i));
            v.setTag(String.valueOf(this.parts.get(i).part_sa) + "#" + i);
            if (isfirst) {
                changePart(v);
                this.second_pasue = v;
                isfirst = false;
            }
            this.second_items.addView(v);
            v.setOnClickListener(this);
            if (this.hasAnimation) {
                v.measure(this.init_w, this.init_h);
                this.init_layoutparam = getInit_Secondlayoutparam(v.getMeasuredWidth(), v.getMeasuredHeight());
                View v_Move = getItem_move(i, this.parts.get(i));
                v_Move.setLayoutParams(this.init_layoutparam);
                v_Move.setTag(new StringBuilder(String.valueOf(i)).toString());
                v_Move.setVisibility(4);
                this.second_move_items.addView(v_Move);
            }
        }
        this.old_move_item = this.second_move_items.findViewWithTag(UploadUtils.SUCCESS);
        if (this.old_move_item != null) {
            this.old_move_item.setVisibility(0);
        }
        if (this.second_canscroll) {
            final GestureDetector mGestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
                public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                    if (SecondScrollView.this.second_items.getWidth() == SecondScrollView.this.second_content.getScrollX() + SecondScrollView.this.second_content.getWidth()) {
                        SecondScrollView.this.second_right.setVisibility(4);
                    } else if (SecondScrollView.this.second_content.getScrollX() == 0) {
                        SecondScrollView.this.second_left.setVisibility(4);
                    } else {
                        SecondScrollView.this.second_left.setVisibility(0);
                        SecondScrollView.this.second_right.setVisibility(0);
                    }
                    return super.onScroll(e1, e2, distanceX, distanceY);
                }
            });
            this.second_content.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent arg1) {
                    mGestureDetector.onTouchEvent(arg1);
                    return false;
                }
            });
        }
    }

    public LinearLayout.LayoutParams getInit_Secondlayoutparam(int w, int h) {
        return new LinearLayout.LayoutParams(w, h);
    }

    public void getParts(String type) {
    }

    public void onClick(View v) {
        this.second_pasue = v;
        if (!v.getTag().equals(this.old_item.getTag())) {
            startSlipAnimation(v, this.second_move_items.findViewWithTag(v.getTag().toString().split("#")[1]));
        }
    }

    public void startSlipAnimation(final View view, final View move) {
        if (!this.hasAnimation) {
            changePart(view);
            return;
        }
        Animation animation = new TranslateAnimation(0.0f, (float) ((move.getLeft() + (move.getWidth() / 2)) - (this.old_move_item.getLeft() + (this.old_move_item.getWidth() / 2))), 0.0f, 0.0f);
        animation.setDuration(200);
        animation.setFillAfter(false);
        animation.setFillBefore(false);
        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                SecondScrollView.this.changePart(view);
                SecondScrollView.this.updataCurView(move);
            }
        });
        this.old_move_item.startAnimation(animation);
        this.second_move_items.invalidate();
    }

    public void changePart(View current) {
        changeStyle(current);
        this.old_item = current;
        changeFragment(current);
    }

    public void changeStyle(View current) {
        ((TextView) current).setTextColor(getResources().getColor(17170443));
        if (this.old_item != null) {
            ((TextView) this.old_item).setTextColor(getResources().getColor(17170444));
        }
    }

    public void changeFragment(View current) {
        FragmentManager fm = ((FragmentActivity) getContext()).getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        int current_index = Integer.valueOf(current.getTag().toString().split("#")[1]).intValue();
        this.result = fm.findFragmentByTag(String.valueOf(current.getTag().toString()) + PerfHelper.getStringData("tag"));
        if (frag != null) {
            ft.detach(frag);
        }
        if (this.result != null) {
            this.all_frag[current_index] = this.result;
            ft.attach(this.result);
            frag = this.all_frag[current_index];
        } else {
            if (this.all_frag[current_index] == null) {
                this.all_frag[current_index] = initFragment(current_index, current.getTag().toString().split("#")[0]);
            }
            frag = this.all_frag[current_index];
            ft.add(this.content_id, frag, String.valueOf(current.getTag().toString()) + PerfHelper.getStringData("tag"));
        }
        ft.commit();
    }

    public Fragment initFragment(int index, String fragmentinfo) {
        return null;
    }

    public void updataCurView(View move) {
        this.old_move_item.setVisibility(4);
        move.setVisibility(0);
        this.old_move_item = move;
    }

    public List<part> getParts() {
        List<part> parts2 = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            part p = new part();
            p.part_name = "天下";
            p.part_sa = "tianxia";
            p.part_type = "news";
            parts2.add(p);
        }
        return parts2;
    }
}
