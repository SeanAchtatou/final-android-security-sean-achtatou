package com.palmtrends.baseview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Scroller;
import com.palmtrends.app.ShareApplication;

public class ScrollLayout extends ViewGroup {
    private static final int SNAP_VELOCITY = 600;
    private static final String TAG = "ScrollLayout";
    private static final int TOUCH_STATE_REST = 0;
    private static final int TOUCH_STATE_SCROLLING = 1;
    private static int position = 180;
    private int mCurScreen;
    private int mDefaultScreen;
    private float mLastMotionX;
    private float mLastMotionY;
    private Scroller mScroller;
    private int mTouchSlop;
    private int mTouchState;
    private VelocityTracker mVelocityTracker;

    public ScrollLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScrollLayout(Context context) {
        super(context);
        this.mDefaultScreen = 0;
        this.mTouchState = 0;
        this.mScroller = new Scroller(context);
        this.mCurScreen = this.mDefaultScreen;
        this.mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    public ScrollLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mDefaultScreen = 0;
        this.mTouchState = 0;
        this.mScroller = new Scroller(context);
        this.mCurScreen = this.mDefaultScreen;
        this.mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        int childLeft = 0;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childView = getChildAt(i);
            if (childView.getVisibility() != 8) {
                int childWidth = childView.getMeasuredWidth();
                childView.layout(childLeft, 0, childLeft + childWidth, childView.getMeasuredHeight());
                childLeft += childWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (ShareApplication.debug) {
            Log.e(TAG, "onMeasure");
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = View.MeasureSpec.getSize(widthMeasureSpec);
        if (View.MeasureSpec.getMode(widthMeasureSpec) != 1073741824) {
            throw new IllegalStateException("ScrollLayout only canmCurScreen run at EXACTLY mode!");
        } else if (View.MeasureSpec.getMode(heightMeasureSpec) != 1073741824) {
            throw new IllegalStateException("ScrollLayout only can run at EXACTLY mode!");
        } else {
            int count = getChildCount();
            for (int i = 0; i < count; i++) {
                getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
            }
            scrollTo(this.mCurScreen * width, 0);
        }
    }

    public void snapToDestination() {
        int screenWidth = getWidth();
        int destScreen = (getScrollX() + (screenWidth / 2)) / screenWidth;
        if (destScreen <= 1) {
            snapToScreen(1);
        } else if (destScreen >= getChildCount() - 2) {
            snapToScreen(getChildCount() - 2);
        } else {
            snapToScreen(destScreen);
        }
    }

    public void loadControll(int delta, int whichScreen) {
        if (delta > 0 && this.mCurScreen <= 1) {
            int delta2 = Math.abs(delta) - position;
            this.mScroller.startScroll(getScrollX(), 0, delta2, 0, Math.abs(delta2) * 2);
            this.mCurScreen = whichScreen;
            invalidate();
        } else if (delta >= 0 || this.mCurScreen < getChildCount() - 2) {
            this.mScroller.startScroll(getScrollX(), 0, delta, 0, Math.abs(delta) * 2);
            this.mCurScreen = whichScreen;
            invalidate();
        } else {
            int delta3 = delta + position;
            this.mScroller.startScroll(getScrollX(), 0, delta3, 0, Math.abs(delta3) * 2);
            this.mCurScreen = whichScreen;
            invalidate();
        }
    }

    public void snapToScreen(int whichScreen) {
        int whichScreen2 = Math.max(0, Math.min(whichScreen, getChildCount() - 1));
        if (getScrollX() != getWidth() * whichScreen2) {
            int delta = (getWidth() * whichScreen2) - getScrollX();
            if (this.mCurScreen != whichScreen2) {
                this.mScroller.startScroll(getScrollX(), 0, delta, 0, Math.abs(delta) * 2);
                this.mCurScreen = whichScreen2;
                invalidate();
            } else if (Math.abs(delta) > position) {
                loadControll(delta, whichScreen2);
            } else {
                this.mScroller.startScroll(getScrollX(), 0, delta, 0, Math.abs(delta) * 2);
                this.mCurScreen = whichScreen2;
                invalidate();
            }
        }
    }

    public void setToScreen(int whichScreen) {
        int whichScreen2 = Math.max(0, Math.min(whichScreen, getChildCount() - 1));
        this.mCurScreen = whichScreen2;
        scrollTo(getWidth() * whichScreen2, 0);
    }

    public int getCurScreen() {
        return this.mCurScreen;
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            postInvalidate();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(event);
        int action = event.getAction();
        float x = event.getX();
        float y = event.getY();
        switch (action) {
            case 0:
                if (ShareApplication.debug) {
                    Log.e(TAG, "event down!");
                }
                if (!this.mScroller.isFinished()) {
                    this.mScroller.abortAnimation();
                }
                this.mLastMotionX = x;
                break;
            case 1:
                if (ShareApplication.debug) {
                    Log.e(TAG, "event : up");
                }
                VelocityTracker velocityTracker = this.mVelocityTracker;
                velocityTracker.computeCurrentVelocity(1000);
                int velocityX = (int) velocityTracker.getXVelocity();
                if (ShareApplication.debug) {
                    Log.e(TAG, "velocityX:" + velocityX);
                }
                if (velocityX > SNAP_VELOCITY && this.mCurScreen > 1) {
                    if (ShareApplication.debug) {
                        Log.e(TAG, "snap left");
                    }
                    if (this.mCurScreen <= 1) {
                        snapToScreen(1);
                    } else {
                        snapToScreen(this.mCurScreen - 1);
                    }
                } else if (velocityX >= -600 || this.mCurScreen >= getChildCount() - 1) {
                    snapToDestination();
                } else {
                    if (ShareApplication.debug) {
                        Log.e(TAG, "snap right");
                    }
                    if (this.mCurScreen >= getChildCount() - 2) {
                        snapToScreen(getChildCount() - 2);
                    } else {
                        snapToScreen(this.mCurScreen + 1);
                    }
                }
                if (this.mVelocityTracker != null) {
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = null;
                }
                this.mTouchState = 0;
                break;
            case 2:
                this.mLastMotionX = x;
                scrollBy((int) (this.mLastMotionX - x), 0);
                break;
            case 3:
                this.mTouchState = 0;
                break;
        }
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (ShareApplication.debug) {
            Log.e(TAG, "onInterceptTouchEvent-slop:" + this.mTouchSlop);
        }
        int action = ev.getAction();
        if (action == 2 && this.mTouchState != 0) {
            return true;
        }
        float x = ev.getX();
        float y = ev.getY();
        switch (action) {
            case 0:
                this.mLastMotionX = x;
                this.mLastMotionY = y;
                this.mTouchState = this.mScroller.isFinished() ? 0 : 1;
                break;
            case 1:
            case 3:
                this.mTouchState = 0;
                break;
            case 2:
                if (((int) Math.abs(this.mLastMotionX - x)) > this.mTouchSlop) {
                    this.mTouchState = 1;
                    break;
                }
                break;
        }
        if (this.mTouchState == 0) {
            return false;
        }
        return true;
    }
}
