package com.palmtrends.datasource;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.DataTransport;
import com.palmtrends.loadimage.ImageFetcher;
import com.utils.FileUtils;
import com.utils.FinalVariable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class ImageLoadUtils {
    private static ExecutorService executorService = Executors.newFixedThreadPool(8);
    public static Map<String, SoftReference<Drawable>> imageCache = new HashMap();

    public static void downloadFileForSave(final String imageUrl, final Handler handler) {
        executorService.submit(new Runnable() {
            public void run() {
                String url;
                String imagename = FileUtils.converPathToName(imageUrl);
                if (handler != null) {
                    Message m = new Message();
                    DataTransport dt = new DataTransport();
                    try {
                        dt.bit = FileUtils.getImageSdFromSave(imagename);
                        if (dt.bit != null) {
                            dt.url = imageUrl;
                            m.obj = dt;
                            m.what = FinalVariable.load_image;
                            handler.sendMessage(m);
                            return;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    if (imageUrl.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                        url = imageUrl;
                    } else {
                        url = String.valueOf(Urls.main) + imageUrl.replace(Urls.main, "").replace("file://", "");
                    }
                    Drawable drawable = Drawable.createFromStream(new URL(url).openStream(), "image.png");
                    if (handler != null) {
                        Message m2 = new Message();
                        DataTransport dt2 = new DataTransport();
                        dt2.bit = drawable;
                        dt2.url = imageUrl;
                        m2.obj = dt2;
                        m2.what = FinalVariable.load_image;
                        handler.sendMessage(m2);
                    }
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    String imagename2 = FileUtils.converPathToName(imageUrl);
                    File file = new File(FileUtils.savePath);
                    if (file.exists()) {
                        file.mkdirs();
                    }
                    File file2 = new File(file.getAbsoluteFile() + CookieSpec.PATH_DELIM + imagename2);
                    file2.createNewFile();
                    FileOutputStream output = new FileOutputStream(file2);
                    if (imagename2.endsWith("png")) {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 80, output);
                    } else {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, output);
                    }
                } catch (Exception e2) {
                    throw new RuntimeException(e2);
                }
            }
        });
    }

    public static void downloadFile(final String imageUrl, final Handler handler) {
        executorService.submit(new Runnable() {
            public void run() {
                String url;
                String imagename = FileUtils.converPathToName(imageUrl);
                if (handler != null) {
                    Message m = new Message();
                    DataTransport dt = new DataTransport();
                    try {
                        dt.bit = FileUtils.getImageSd(imagename);
                        if (dt.bit != null) {
                            dt.url = imageUrl;
                            m.obj = dt;
                            m.what = FinalVariable.load_image;
                            handler.sendMessage(m);
                            return;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    if (imageUrl.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                        url = imageUrl;
                    } else {
                        url = String.valueOf(Urls.main) + imageUrl.replace(Urls.main, "").replace("file://", "");
                    }
                    Drawable drawable = Drawable.createFromStream(new URL(url).openStream(), "image.png");
                    if (handler != null) {
                        Message m2 = new Message();
                        DataTransport dt2 = new DataTransport();
                        dt2.bit = drawable;
                        dt2.url = imageUrl;
                        m2.obj = dt2;
                        m2.what = FinalVariable.load_image;
                        handler.sendMessage(m2);
                    }
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    String imagename2 = FileUtils.converPathToName(imageUrl);
                    FileOutputStream output = new FileOutputStream(FileUtils.createSdFile("image/" + imagename2));
                    if (imagename2.endsWith("png")) {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                    } else {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
                    }
                } catch (Exception e2) {
                    throw new RuntimeException(e2);
                }
            }
        });
    }

    public static void downloadFile(final String imageUrl, final String type, final Handler handler) {
        if (imageCache.containsKey(imageUrl)) {
            SoftReference<Drawable> softReference = imageCache.get(imageUrl);
            if (softReference.get() != null) {
                if (handler != null) {
                    Message m = new Message();
                    DataTransport dt = new DataTransport();
                    dt.bit = (Drawable) softReference.get();
                    dt.url = imageUrl;
                    m.obj = dt;
                    m.what = FinalVariable.load_image;
                    handler.sendMessage(m);
                    return;
                }
                return;
            }
        }
        final String imagename = FileUtils.converPathToName(imageUrl);
        if (FileUtils.isFileExist("image/" + type + imagename) && handler != null) {
            try {
                Message m2 = new Message();
                DataTransport dt2 = new DataTransport();
                dt2.bit = FileUtils.getImageSd(String.valueOf(type) + imagename);
                dt2.url = imageUrl;
                m2.obj = dt2;
                m2.what = FinalVariable.load_image;
                handler.sendMessage(m2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        executorService.submit(new Runnable() {
            public void run() {
                URL url;
                try {
                    if (imageUrl.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                        url = new URL(imageUrl);
                    } else {
                        url = new URL(String.valueOf(Urls.main) + imageUrl.replace(Urls.main, ""));
                    }
                    Drawable drawable = Drawable.createFromStream(url.openStream(), "image.png");
                    ImageLoadUtils.imageCache.put(imageUrl, new SoftReference(drawable));
                    if (handler != null) {
                        Message m = new Message();
                        DataTransport dt = new DataTransport();
                        dt.bit = drawable;
                        dt.url = imageUrl;
                        m.obj = dt;
                        m.what = FinalVariable.load_image;
                        handler.sendMessage(m);
                    }
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    FileOutputStream output = new FileOutputStream(FileUtils.createSdFile("image/" + type + imagename));
                    if (imagename.endsWith("png")) {
                        bitmap.compress(Bitmap.CompressFormat.PNG, 75, output);
                    } else {
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, output);
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }
}
