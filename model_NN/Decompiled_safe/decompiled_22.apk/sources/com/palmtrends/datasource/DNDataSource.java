package com.palmtrends.datasource;

import android.content.ContentValues;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.MySSLSocketFactory;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class DNDataSource {
    public static String list_FromNET(String url, String type, int page, int count, String partType, boolean isfirst) throws Exception {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sa", type));
        param.add(new BasicNameValuePair("offset", new StringBuilder(String.valueOf(page * count)).toString()));
        param.add(new BasicNameValuePair("count", new StringBuilder(String.valueOf(count)).toString()));
        return MySSLSocketFactory.getinfo(url, param).replaceAll("'", "‘");
    }

    public static String CityLift_list_FromNET(String url, String type, int page, int count, String partType, boolean isfirst) throws Exception {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("pageNo", new StringBuilder(String.valueOf(page)).toString()));
        param.add(new BasicNameValuePair("pageNum", new StringBuilder(String.valueOf(count)).toString()));
        return MySSLSocketFactory.getinfo(url, param).replaceAll("'", "‘");
    }

    public static String list_FromNET(String url, List<NameValuePair> param) throws Exception {
        return MySSLSocketFactory.getinfo(url, param).replaceAll("'", "‘");
    }

    public static String list_FromDB(String type, int page, int count, String partType) {
        return DBHelper.getDBHelper().select("listinfo", "infos", "url=?", new String[]{String.valueOf(type) + page});
    }

    public static Data list_Fav(String type, int page, int count) throws Exception {
        ArrayList al = DBHelper.getDBHelper().select("listitemfa", Listitem.class, "show_type='" + type + "'", page, count);
        Data d = new Data();
        if (al != null) {
            d.list = al;
        }
        return d;
    }

    public static void insertRead(String mark, String html) {
        DBHelper db = DBHelper.getDBHelper();
        ContentValues cv = new ContentValues();
        cv.put("n_mark", mark);
        cv.put("htmltext", html);
        db.insert("readitem", cv);
    }

    public static void updateRead(String table, String n_mark, String column, String value) {
        DBHelper.getDBHelper().update(table, "n_mark", n_mark, column, value);
    }
}
