package com.palmtrends.datasource;

import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class JsonParser {
    public static Data ParserArticleList(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has(Constants.SINA_CODE) || jsonobj.getInt(Constants.SINA_CODE) != 0) {
            if (jsonobj.has("def")) {
                data.loadmorestate = jsonobj.getInt("def");
            }
            JSONArray jsonay = jsonobj.getJSONArray("list");
            if (jsonobj.has("head")) {
                try {
                    JSONArray ja = jsonobj.getJSONArray("head");
                    if (ja.length() == 1) {
                        JSONObject jsonhead = ja.getJSONObject(0);
                        Listitem head1 = new Listitem();
                        head1.nid = jsonhead.getString(LocaleUtil.INDONESIAN);
                        head1.title = jsonhead.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                        head1.des = jsonhead.getString("des");
                        head1.icon = jsonhead.getString("icon");
                        head1.getMark();
                        head1.ishead = "true";
                        data.obj = head1;
                        data.headtype = 0;
                    } else {
                        int count = ja.length();
                        List<Listitem> li = new ArrayList<>();
                        for (int i = 0; i < count; i++) {
                            JSONObject jsonhead2 = ja.getJSONObject(i);
                            Listitem head12 = new Listitem();
                            head12.nid = jsonhead2.getString(LocaleUtil.INDONESIAN);
                            head12.title = jsonhead2.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                            head12.des = jsonhead2.getString("des");
                            head12.icon = jsonhead2.getString("icon");
                            head12.ishead = "true";
                            head12.getMark();
                            li.add(head12);
                        }
                        data.obj = li;
                        data.headtype = 2;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int count2 = jsonay.length();
            for (int i2 = 0; i2 < count2; i2++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i2);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                o.title = obj.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                try {
                    if (obj.has("des")) {
                        o.des = obj.getString("des");
                    }
                    if (obj.has("adddate")) {
                        o.u_date = obj.getString("adddate");
                    }
                    o.icon = obj.getString("icon");
                } catch (Exception e2) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }
        throw new DataException(jsonobj.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG));
    }
}
