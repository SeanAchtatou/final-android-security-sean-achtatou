package com.city_life.setting;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import com.city_life.sliding.fragments.RedianListFragment;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.dao.DBHelper;
import com.pengyou.citycommercialarea.R;

public class FavActivity extends BaseActivity {
    Fragment findresult = null;
    Fragment frag = null;
    Fragment m_list_frag_1 = null;
    Fragment m_list_frag_2 = null;
    Fragment m_list_frag_3 = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_st_fav);
        initFragment();
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void initFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        this.m_list_frag_1 = RedianListFragment.newInstance("FAV_TAG0", DBHelper.FAV_FLAG);
        fragmentTransaction.replace(R.id.part_content, this.m_list_frag_1).commit();
    }
}
