package com.city_life.setting;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.city_life.ui.WeiboBangdingActivity;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import sina_weibo.BingdingSinaWeibo;
import sina_weibo.Constants;
import sina_weibo.Util;
import sina_weibo.WeiboListener;
import tengxun_weibo.TencentWeiboUtil;

public class ST_ShareManager extends BaseActivity {
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FinalVariable.vb_success /*10000*/:
                    ST_ShareManager.this.initShare();
                    Utils.showToast("解除绑定成功");
                    return;
                case 10001:
                default:
                    return;
                case 10002:
                    Utils.showToast("解除绑定失败");
                    return;
            }
        }
    };
    LinearLayout manager_bangding_title;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_share_manager);
        this.manager_bangding_title = (LinearLayout) findViewById(R.id.manager_content);
        initShare();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        initShare();
        super.onResume();
    }

    public void initShare() {
        this.manager_bangding_title = (LinearLayout) findViewById(R.id.manager_content);
        this.manager_bangding_title.removeAllViews();
        String[] array = getResources().getStringArray(R.array.article_wb_list_name_sa);
        int number = count;
        for (String item : array) {
            if (item.equals("wx")) {
                number--;
            }
        }
        for (String item2 : array) {
            if (item2.startsWith("wx")) {
                number--;
            } else {
                View view = LayoutInflater.from(this).inflate((int) R.layout.listitem_share, (ViewGroup) null);
                ImageView iv = (ImageView) view.findViewById(R.id.manager_icon);
                TextView title = (TextView) view.findViewById(R.id.manager_title);
                TextView name = (TextView) view.findViewById(R.id.manager_name);
                TextView btn = (TextView) view.findViewById(R.id.manager_btn);
                this.manager_bangding_title.addView(view);
                btn.setTag(item2);
                if ("sina".equals(item2)) {
                    title.setText("新浪微博");
                    iv.setImageResource(R.drawable.icon_sina);
                    if (!TextUtils.isEmpty(PerfHelper.getStringData(Constants.PREF_SINA_ACCESS_TOKEN))) {
                        btn.setBackgroundResource(R.drawable.bind_binded);
                        name.setText(PerfHelper.getStringData(Constants.PREF_SINA_USER_NAME));
                        if (PerfHelper.getStringData(Constants.PREF_SINA_USER_NAME_IMG).length() > 2) {
                            ShareApplication.mImageWorker.loadImage(PerfHelper.getStringData(Constants.PREF_SINA_USER_NAME_IMG), iv);
                        } else {
                            iv.setImageResource(R.drawable.icon_sina);
                        }
                    } else {
                        name.setText("");
                        btn.setBackgroundResource(R.drawable.bind_bind);
                    }
                } else if ("qq".equals(item2)) {
                    title.setText("腾讯微博");
                    iv.setImageResource(R.drawable.icon_qq);
                    if (!TextUtils.isEmpty(Util.getSharePersistent(this, "ACCESS_TOKEN"))) {
                        btn.setBackgroundResource(R.drawable.bind_binded);
                        name.setText(PerfHelper.getStringData(Constants.PREF_TX_NAME));
                        if ((String.valueOf(PerfHelper.getStringData(Constants.PREF_TX_USER_NAME_IMG)) + "/120").length() > 5) {
                            ShareApplication.mImageWorker.loadImage(String.valueOf(PerfHelper.getStringData(Constants.PREF_TX_USER_NAME_IMG)) + "/120", iv);
                        } else {
                            iv.setImageResource(R.drawable.icon_qq);
                        }
                    } else {
                        name.setText("");
                        btn.setBackgroundResource(R.drawable.bind_bind);
                    }
                } else if ("renren".equals(item2)) {
                    title.setText("人人网");
                } else if ("kaixin".equals(item2)) {
                    title.setText("开心网");
                }
            }
        }
    }

    public void things(final View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            default:
                if (view.getTag().toString().equals("qq")) {
                    if (!TextUtils.isEmpty(Util.getSharePersistent(this, "ACCESS_TOKEN"))) {
                        Util.clearSharePersistent(this);
                        Utils.showToast("解除绑定成功");
                        initShare();
                        return;
                    }
                    TencentWeiboUtil.getInstance(this).auth(new WeiboListener() {
                        public void init(boolean isValid) {
                            if (!isValid) {
                                Intent intent = new Intent();
                                intent.putExtra("sname", view.getTag().toString());
                                intent.setClass(ST_ShareManager.this, WeiboBangdingActivity.class);
                                ST_ShareManager.this.startActivity(intent);
                            }
                        }
                    });
                    return;
                } else if (!TextUtils.isEmpty(PerfHelper.getStringData(Constants.PREF_SINA_ACCESS_TOKEN))) {
                    PerfHelper.setInfo(Constants.PREF_SINA_ACCESS_TOKEN, "");
                    PerfHelper.setInfo(Constants.PREF_SINA_UID, "");
                    PerfHelper.setInfo(Constants.PREF_SINA_EXPIRES_TIME, 0);
                    PerfHelper.setInfo(Constants.PREF_SINA_REMIND_IN, "");
                    PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME, "");
                    PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME_IMG, "");
                    Utils.showToast("解除绑定成功");
                    initShare();
                    return;
                } else if (doSinaType("com.sina.weibo", 190)) {
                    new BingdingSinaWeibo(this).BingdingSina(new WeiboListener() {
                        public void onResult() {
                            ST_ShareManager.this.handler.postDelayed(new Runnable() {
                                public void run() {
                                    ST_ShareManager.this.initShare();
                                }
                            }, 200);
                        }
                    });
                    return;
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("sname", view.getTag().toString());
                    intent.setClass(this, WeiboBangdingActivity.class);
                    startActivity(intent);
                    return;
                }
        }
    }

    private boolean doSinaType(String packageName, int versionCode) {
        for (PackageInfo pi : getPackageManager().getInstalledPackages(8192)) {
            String pi_packageName = pi.packageName;
            int pi_versionCode = pi.versionCode;
            if (packageName.endsWith(pi_packageName)) {
                if (versionCode > pi_versionCode) {
                    return false;
                }
                if (versionCode <= pi_versionCode) {
                    return true;
                }
            }
        }
        Log.i("test", "未安装该应用，可以安装");
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }
}
