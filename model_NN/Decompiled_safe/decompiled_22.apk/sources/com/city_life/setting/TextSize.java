package com.city_life.setting;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.palmtrends.baseui.BaseActivity;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class TextSize extends BaseActivity implements View.OnClickListener {
    ImageView big;
    ImageView middle;
    ImageView samll;
    TextView textview;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_st_textsize);
        this.big = (ImageView) findViewById(R.id.big);
        this.middle = (ImageView) findViewById(R.id.middle);
        this.samll = (ImageView) findViewById(R.id.samll);
        this.big.setOnClickListener(this);
        this.middle.setOnClickListener(this);
        this.middle.setBackgroundResource(R.drawable.middle_h);
        this.samll.setOnClickListener(this);
        this.textview = (TextView) findViewById(R.id.text);
        String text = PerfHelper.getStringData(PerfHelper.P_TEXT);
        if ("s".equals(text)) {
            this.big.setBackgroundResource(R.drawable.big_n);
            this.middle.setBackgroundResource(R.drawable.middle_n);
            this.samll.setBackgroundResource(R.drawable.small_h);
            this.textview.setTextSize(15.0f);
        } else if ("m".equals(text)) {
            this.big.setBackgroundResource(R.drawable.big_n);
            this.samll.setBackgroundResource(R.drawable.small_n);
            this.middle.setBackgroundResource(R.drawable.middle_h);
            this.textview.setTextSize(17.0f);
        } else if ("b".equals(text)) {
            this.big.setBackgroundResource(R.drawable.big_h);
            this.middle.setBackgroundResource(R.drawable.middle_n);
            this.samll.setBackgroundResource(R.drawable.small_n);
            this.textview.setTextSize(19.0f);
        }
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            default:
                return;
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.big) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "b");
            this.big.setBackgroundResource(R.drawable.big_h);
            this.samll.setBackgroundResource(R.drawable.small_n);
            this.middle.setBackgroundResource(R.drawable.middle_n);
            this.textview.setTextSize(19.0f);
        } else if (v.getId() == R.id.middle) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "m");
            this.middle.setBackgroundResource(R.drawable.middle_h);
            this.big.setBackgroundResource(R.drawable.big_n);
            this.samll.setBackgroundResource(R.drawable.small_n);
            this.textview.setTextSize(17.0f);
        } else if (v.getId() == R.id.samll) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "s");
            this.textview.setTextSize(15.0f);
            this.big.setBackgroundResource(R.drawable.big_n);
            this.middle.setBackgroundResource(R.drawable.middle_n);
            this.samll.setBackgroundResource(R.drawable.small_h);
        }
    }
}
