package com.city_life.setting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class FeedBack extends BaseActivity {
    ActionBar actionBar;
    public EditText email;
    public Handler h = new Handler() {
        public void handleMessage(Message msg) {
            Utils.dismissProcessDialog();
            switch (msg.what) {
                case 1:
                    Utils.showToast("反馈成功");
                    FeedBack.this.finish();
                    return;
                case 2:
                    Utils.showToast("发送失败");
                    return;
                case 3:
                case 4:
                case 5:
                default:
                    return;
            }
        }
    };
    public EditText info;
    public boolean isEmailNotNull = true;
    public int num = 280;
    ImageView send;
    ImageView titleimg;
    TelephonyManager tm;
    Vibrator vibrator;
    public TextView zishu_textView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_st_feedback);
        this.info = (EditText) findViewById(R.id.suggest_info);
        this.email = (EditText) findViewById(R.id.qq_info);
        this.zishu_textView = (TextView) findViewById(R.id.zishu_textView);
        this.zishu_textView.setText(new StringBuilder().append((this.num - getCharacterNum(this.info.getText().toString())) / 2).toString());
        onfindview();
        addListener();
    }

    public void onfindview() {
    }

    public void send() {
        if (this.info.getText() == null || this.info.getText().length() <= 0) {
            Utils.showToast("请输入您的反馈意见");
            return;
        }
        PerfHelper.setInfo("suggest", this.info.getText().toString());
        if (this.info.getText().length() > 1000) {
            Utils.showToast((int) R.string.too_long_tip);
            return;
        }
        String editable = this.email.getText().toString();
        new AlertDialog.Builder(this).setMessage("是否确认提交？").setPositiveButton("是", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Utils.showProcessDialog(FeedBack.this, "正在提交...");
                new Thread() {
                    public void run() {
                        new FeedbackAync().execute(null);
                    }
                }.start();
            }
        }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        }).show();
    }

    public void things(View v) {
        switch (v.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            case R.id.title_send /*2131230886*/:
                if (Integer.parseInt(this.zishu_textView.getText().toString()) < 0) {
                    Utils.showToast("字数不能超过140个,请编辑后再发送……");
                    return;
                } else {
                    send();
                    return;
                }
            default:
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    public void addListener() {
        this.info.addTextChangedListener(new TextWatcher() {
            private int selectionEnd;
            private int selectionStart;
            private CharSequence temp;

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                this.temp = s;
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                int number = FeedBack.this.num - FeedBack.getCharacterNum(FeedBack.this.info.getText().toString());
                FeedBack.this.zishu_textView.setText(new StringBuilder().append(number / 2).toString());
                if (number / 2 <= 0) {
                    FeedBack.this.zishu_textView.setTextColor(FeedBack.this.getResources().getColor(R.color.red));
                } else {
                    FeedBack.this.zishu_textView.setTextColor(FeedBack.this.getResources().getColor(R.color.black));
                }
                this.selectionStart = FeedBack.this.info.getSelectionStart();
                this.selectionEnd = FeedBack.this.info.getSelectionEnd();
                if (FeedBack.getCharacterNum(FeedBack.this.info.getText().toString()) >= FeedBack.this.num && number / 2 >= 0) {
                    s.delete(this.selectionStart - 1, this.selectionEnd);
                    int tempSelection = this.selectionEnd;
                    FeedBack.this.info.setText(s);
                    FeedBack.this.info.setSelection(tempSelection);
                }
            }
        });
    }

    public static int getCharacterNum(String content) {
        if (content == null || "".equals(content)) {
            return 0;
        }
        return content.length() + getChineseNum(content);
    }

    public static int getChineseNum(String s) {
        int num2 = 0;
        char[] myChar = s.toCharArray();
        for (int i = 0; i < myChar.length; i++) {
            if (((char) ((byte) myChar[i])) != myChar[i]) {
                num2++;
            }
        }
        return num2;
    }

    class FeedbackAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public FeedbackAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("backContent", FeedBack.this.info.getText().toString()));
            param.add(new BasicNameValuePair("backPhone", FeedBack.this.email.getText().toString()));
            param.add(new BasicNameValuePair("user", PerfHelper.getStringData(PerfHelper.P_USERID)));
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(FeedBack.this.getResources().getString(R.string.citylife_feedback_url), param);
                if (ShareApplication.debug) {
                    System.out.println("用户反馈返回:" + json);
                }
                mhashmap.put("responseCode", parseJson(json).obj1);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("countNum")) {
                data.obj = jsonobj.getString("countNum");
            }
            return data;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (!Utils.isNetworkAvailable(FeedBack.this)) {
                Utils.showToast((int) R.string.network_error);
                FeedBack.this.finish();
            } else if (result == null) {
                Toast.makeText(FeedBack.this, "提交失败，请稍后再试", 0).show();
            } else if (UploadUtils.FAILURE.equals(result.get("responseCode"))) {
                Toast.makeText(FeedBack.this, "提交失败，请稍后再试", 0).show();
            } else if (UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                Toast.makeText(FeedBack.this, "提交成功", 0).show();
            }
        }
    }
}
