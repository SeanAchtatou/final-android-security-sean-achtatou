package com.city_life.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class Version extends BaseActivity {
    ActionBar actionBar;
    View actionbar_title;
    TextView t_aid;
    TextView t_mail;
    TextView t_uid;
    TextView t_version;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_st_version);
        this.t_uid = (TextView) findViewById(R.id.version_uid);
        this.t_uid.setText("UID: " + PerfHelper.getStringData(PerfHelper.P_USERID));
        this.t_aid = (TextView) findViewById(R.id.version_aid);
        this.t_aid.setText("A M: 1.0.0");
        this.t_mail = (TextView) findViewById(R.id.version_email);
        this.t_version = (TextView) findViewById(R.id.version_code);
        this.t_version.setText("版本号: " + ShareApplication.getVersion());
    }

    public void showTime(View v) {
        String time = PerfHelper.getStringData(PerfHelper.P_ALARM_TIME);
        if (time != null && !"".equals(time)) {
            Utils.showToast("提醒时间：" + time.split(" ")[1].substring(0, 5) + "\n" + "UID: " + PerfHelper.getStringData(PerfHelper.P_USERID) + "\n" + "A M: 1.0.0");
            Intent intent = new Intent();
            intent.setClass(this, FeedBack.class);
            startActivity(intent);
        }
    }

    public void things(View v) {
        if (v.getId() == R.id.title_back) {
            finish();
        }
    }
}
