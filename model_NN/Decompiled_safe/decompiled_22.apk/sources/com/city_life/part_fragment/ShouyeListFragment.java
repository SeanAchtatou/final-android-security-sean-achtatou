package com.city_life.part_fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.city_life.part_activiy.HuodongyaoyiyaoActivity;
import com.city_life.part_activiy.NearListActivity;
import com.city_life.part_activiy.ShouyeTuijianListActivity;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import com.volley.msg.BitmapCache;
import org.json.JSONArray;
import org.json.JSONObject;

public class ShouyeListFragment extends LoadMoreListFragment<Listitem> implements NearListActivity.resume_up {
    Handler bar_ani_hanHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
            }
        }
    };
    ColorStateList csl_n;
    ColorStateList csl_r;
    LinearLayout.LayoutParams frla;
    View headview;
    private ImageLoader mImageLoader;
    RelativeLayout.LayoutParams mLa;
    private RequestQueue mQueue;
    String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    View.OnClickListener oncilc = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction(ShouyeListFragment.this.mContext.getResources().getString(R.string.activity_keyword));
            intent.putExtra("parttpye", v.getTag().toString());
            ShouyeListFragment.this.startActivity(intent);
            ShouyeListFragment.this.getActivity().overridePendingTransition(R.anim.init_in, R.anim.init_out);
        }
    };
    int[] p_count = {R.id.listitem_tag, R.id.listitem_tag_1, R.id.listitem_tag_2};
    private String part_name;
    private NearListActivity.resume_up re_up;
    RelativeLayout.LayoutParams relal;
    TextView[] tags = new TextView[5];

    public static BaseFragment<Listitem> newInstance(String type, String partType) {
        ShouyeListFragment tf = new ShouyeListFragment();
        tf.initType(type, partType);
        return tf;
    }

    public void findView() {
        super.findView();
        this.mQueue = Volley.newRequestQueue(this.mContext);
        this.mImageLoader = new ImageLoader(this.mQueue, new BitmapCache());
        this.csl_r = getResources().getColorStateList(R.color.list_item_title_r);
        this.csl_n = getResources().getColorStateList(R.color.list_item_title_n);
        this.mHead_Layout = new FrameLayout.LayoutParams(-1, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 330) / 640);
        int w = (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 170) / 640;
        int h = (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 130) / 640;
        this.mIcon_Layout = new LinearLayout.LayoutParams(w, h);
        this.mLa = new RelativeLayout.LayoutParams(w, h);
        this.frla = new LinearLayout.LayoutParams(w, h);
        this.relal = new RelativeLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 120) / 480, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 92) / 480);
        this.relal.addRule(11);
        this.relal.rightMargin = 10;
        try {
            this.part_name = DBHelper.getDBHelper().select("part_list", "part_name", "part_sa=?", new String[]{this.mOldtype});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public View getListItemview(View view, Listitem item, int position) {
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_shouyelist, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.listitem_title)).setText(item.title);
        ((TextView) view.findViewById(R.id.listitem_des)).setText(item.des);
        this.mImageLoader.get(item.icon, ImageLoader.getImageListener((ImageView) view.findViewById(R.id.listitem_icon), R.drawable.touming_logo_bg, R.drawable.touming_logo_bg));
        return view;
    }

    public void getDistance(Listitem item) {
    }

    public void addListener() {
        super.addListener();
        if (this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            this.mListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                    final Listitem li = (Listitem) arg0.getItemAtPosition(arg2);
                    AlertDialog show = new AlertDialog.Builder(ShouyeListFragment.this.getActivity()).setMessage("您确认要删除本条记录吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ShouyeListFragment.this.mlistAdapter.datas.remove(arg2 - ShouyeListFragment.this.mListview.getHeaderViewsCount());
                            ShouyeListFragment.this.mlistAdapter.notifyDataSetChanged();
                            DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{li.n_mark});
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                    return false;
                }
            });
        }
    }

    public boolean dealClick(Listitem item, int position) {
        if (this.mlistAdapter == null || this.mlistAdapter.datas.size() <= 0 || item == null) {
            return false;
        }
        Intent intent = new Intent();
        if (item.vip_id.equals(UploadUtils.FAILURE)) {
            intent.setClass(this.mContext, HuodongyaoyiyaoActivity.class);
        } else {
            intent.setClass(this.mContext, ShouyeTuijianListActivity.class);
        }
        intent.putExtra("item", item);
        intent.putExtra(Constants.PARAM_TITLE, item.title);
        intent.putExtra(LocaleUtil.INDONESIAN, item.nid);
        this.mContext.startActivity(intent);
        return true;
    }

    public View getListHeadview(Object obj, int type) {
        System.out.println(String.valueOf(type) + "===");
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        TextView textView = (TextView) this.headview.findViewById(R.id.head_title_des);
        ((ImageView) this.headview.findViewById(R.id.head_icon)).setLayoutParams(this.mHead_Layout);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText("========");
        return this.headview;
    }

    public View getListHeadview(Listitem item) {
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        ImageView iv = (ImageView) this.headview.findViewById(R.id.head_icon);
        ((TextView) this.headview.findViewById(R.id.head_title_des)).setText(item.des);
        iv.setLayoutParams(this.mHead_Layout);
        ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + item.icon, iv);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText(item.title.trim());
        return this.headview;
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has("responseCode") || jsonobj.getInt("responseCode") == 0) {
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                o.sa = String.valueOf(this.mOldtype) + "_" + this.part_name;
                try {
                    if (obj.has(Constants.PARAM_TITLE)) {
                        o.title = obj.getString(Constants.PARAM_TITLE);
                    }
                    if (obj.has("lucky")) {
                        o.vip_id = obj.getString("lucky");
                    }
                    if (obj.has("logo")) {
                        o.icon = obj.getString("logo");
                    }
                    if (obj.has("comment")) {
                        o.des = obj.getString("comment");
                    }
                    if (obj.has("addtime")) {
                        o.u_date = obj.getString("addtime");
                    }
                    if (obj.has("lotteryAddtime")) {
                        o.other = obj.getString("lotteryAddtime");
                    }
                    if (obj.has("rules")) {
                        o.other1 = obj.getString("rules");
                    }
                    if (obj.has("prize")) {
                        o.other2 = obj.getString("prize");
                    }
                    if (obj.has("address")) {
                        o.address = obj.getString("address");
                    }
                    if (obj.has("large")) {
                        o.author = obj.getString("large");
                    }
                } catch (Exception e) {
                }
                o.getMark();
                data.list.add(o);
            }
        } else {
            this.mHandler.sendEmptyMessage(FinalVariable.error);
        }
        return data;
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count);
        }
        String json = DNDataSource.CityLift_list_FromNET(String.valueOf(ShareApplication.share.getResources().getString(R.string.citylife_getShouye_list_url)) + "&cityId=" + PerfHelper.getStringData(PerfHelper.P_CITY_No), oldtype, page + 1, count, parttype, isfirst);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public void onResume() {
        super.onResume();
        if (!this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY))) {
            this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
            new Thread(new Runnable() {
                public void run() {
                    ShouyeListFragment.this.reFlush();
                }
            }).start();
        }
    }

    public void up() {
    }
}
