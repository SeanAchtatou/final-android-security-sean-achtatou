package com.city_life.part_fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.baseview.SecondScrollView;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.entity.part;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;

public class NearByPartFragment extends Fragment {
    private static final String KEY_CONTENT = "FJPartFragment:parttype";
    LinearLayout containers;
    boolean isfirst = false;
    FavoritSecondViews main_view;
    /* access modifiers changed from: private */
    public View mview;
    String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    String parttype;

    public static NearByPartFragment newInstance(String type) {
        NearByPartFragment tf = new NearByPartFragment();
        tf.init(type);
        return tf;
    }

    public void init(String type) {
        this.parttype = type;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (!this.isfirst) {
            this.main_view.onFinishInflate();
            this.isfirst = true;
        }
        super.onViewCreated(view, savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup view, Bundle savedInstanceState) {
        if (this.parttype == null && savedInstanceState != null && savedInstanceState.containsKey(KEY_CONTENT)) {
            this.parttype = savedInstanceState.getString(KEY_CONTENT);
        }
        if (this.main_view == null) {
            this.main_view = new FavoritSecondViews(getActivity());
            this.containers = new LinearLayout(getActivity());
            this.containers.addView(this.main_view);
        } else {
            if (this.containers != null) {
                this.containers.removeAllViews();
            }
            this.containers = new LinearLayout(getActivity());
            this.containers.addView(this.main_view);
        }
        return this.containers;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, this.parttype);
    }

    public void setSecondGone(String part) {
        if (part.equals("nearbypart18")) {
            this.main_view.findViewById(R.id.second_layout).setVisibility(0);
        } else {
            this.main_view.findViewById(R.id.second_layout).setVisibility(8);
        }
    }

    public class FavoritSecondViews extends SecondScrollView {
        public FavoritSecondViews(Context context) {
            super(context);
        }

        public void initSecondPart() {
            boolean isfirst = true;
            this.second_items = (LinearLayout) this.secondscroll.findViewById(R.id.second_items);
            this.second_move_items = (LinearLayout) this.secondscroll.findViewById(R.id.move_items);
            this.second_move_items.removeAllViews();
            this.second_items.removeAllViews();
            removeAllViews();
            this.parts = getParts();
            int count = this.parts.size();
            this.content_id = Math.abs(((part) this.parts.get(0)).part_type.hashCode());
            this.content.setId(this.content_id);
            addLayout();
            this.all_frag = new Fragment[count];
            for (int i = 0; i < count; i++) {
                View v = getItem(i, (part) this.parts.get(i));
                v.setTag(String.valueOf(((part) this.parts.get(i)).part_sa) + "#" + i);
                if (isfirst) {
                    changePart(v);
                    NearByPartFragment.this.mview = v;
                    isfirst = false;
                }
                this.second_items.addView(v);
                v.setOnClickListener(this);
                if (this.hasAnimation) {
                    v.measure(this.init_w, this.init_h);
                    int width = v.getMeasuredWidth();
                    int measuredHeight = v.getMeasuredHeight();
                    this.init_layoutparam = getInit_Secondlayoutparam(width, -2);
                    View v_Move = getItem_move(i, (part) this.parts.get(i));
                    v_Move.setLayoutParams(this.init_layoutparam);
                    v_Move.setTag(new StringBuilder(String.valueOf(i)).toString());
                    v_Move.setVisibility(4);
                    this.second_move_items.addView(v_Move);
                }
            }
            this.old_move_item = this.second_move_items.findViewWithTag(UploadUtils.SUCCESS);
            if (this.old_move_item != null) {
                this.old_move_item.setVisibility(0);
            }
            if (this.second_canscroll) {
                final GestureDetector mGestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
                    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                        if (FavoritSecondViews.this.second_items.getWidth() == FavoritSecondViews.this.second_content.getScrollX() + FavoritSecondViews.this.second_content.getWidth()) {
                            FavoritSecondViews.this.second_right.setVisibility(4);
                        } else if (FavoritSecondViews.this.second_content.getScrollX() == 0) {
                            FavoritSecondViews.this.second_left.setVisibility(4);
                        } else {
                            FavoritSecondViews.this.second_left.setVisibility(0);
                            FavoritSecondViews.this.second_right.setVisibility(0);
                        }
                        return super.onScroll(e1, e2, distanceX, distanceY);
                    }
                });
                this.second_content.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View arg0, MotionEvent arg1) {
                        mGestureDetector.onTouchEvent(arg1);
                        return false;
                    }
                });
            }
            new Handler().postDelayed(new Runnable() {
                public void run() {
                }
            }, 500);
        }

        public void onFinishInflate() {
            setsecond_HasAnimation(true);
            setsecond_Canscroll(true);
            super.onFinishInflate();
        }

        public void onClick(View v) {
            super.onClick(v);
            NearByPartFragment.this.mview = v;
        }

        public View getItem(int index, part info) {
            TextView vi2 = (TextView) LayoutInflater.from(getContext()).inflate((int) R.layout.second_text, (ViewGroup) null);
            vi2.setText(info.part_name);
            vi2.setPadding(14, 7, 14, 5);
            return vi2;
        }

        public View getItem_move(int index, part info) {
            ImageView iv = new ImageView(getContext());
            iv.setBackgroundResource(R.drawable.second_banner_moves);
            return iv;
        }

        public void changeStyle(View current) {
            ((TextView) current).setTextColor(getResources().getColor(17170444));
            if (this.old_item != null) {
                ((TextView) this.old_item).setTextColor(getResources().getColor(R.color.second_color));
            }
            if (this.old_item == null || current.getTag().equals(this.old_item.getTag())) {
            }
        }

        public List<part> getParts() {
            List<part> parts = new ArrayList<>();
            try {
                return DBHelper.getDBHelper().select("part_list", part.class, "part_type='" + NearByPartFragment.this.parttype + "'", 0, 100);
            } catch (Exception e) {
                e.printStackTrace();
                return parts;
            }
        }

        public Fragment initFragment(int index, String fragmentinfo) {
            if (index == 0) {
                return ShiChangZhuFragment.newInstance("ShiChangZhuFragment" + fragmentinfo, "ShiChangZhuFragment" + fragmentinfo);
            }
            return ShiChangZhuPartFragment.newInstance("ShiChangZhuFragment" + fragmentinfo, "ShiChangZhuFragment" + fragmentinfo);
        }
    }

    public void onResume() {
        super.onResume();
        if (this.main_view != null && this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY)) && this.mview != null) {
            this.main_view.changePart(this.mview);
            ((TextView) this.mview).setTextColor(getResources().getColor(17170444));
        }
    }
}
