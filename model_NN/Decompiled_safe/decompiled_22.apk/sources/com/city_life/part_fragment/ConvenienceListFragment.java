package com.city_life.part_fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.city_life.ui.HomeActivity;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.utils.FinalVariable;
import com.utils.GpsUtils;
import com.utils.PerfHelper;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ConvenienceListFragment extends LoadMoreListFragment<Listitem> {
    View headview;
    String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);

    public static BaseFragment<Listitem> newInstance(String type, String partType) {
        ConvenienceListFragment tf = new ConvenienceListFragment();
        tf.initType(type, partType);
        return tf;
    }

    public boolean dealClick(Listitem item, int position) {
        return true;
    }

    public void findView() {
        super.findView();
        this.mHead_Layout = new FrameLayout.LayoutParams(-1, ((PerfHelper.getIntData(PerfHelper.P_PHONE_W) - HomeActivity.dip2px(this.mContext, 20.0f)) * 235) / 445);
        this.mIcon_Layout = new LinearLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 105) / 480, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 93) / 480);
        this.mIcon_Layout.rightMargin = (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 8) / 480;
    }

    public View getListItemview(View view, Listitem item, int position) {
        int first_index;
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_conveniencelist, (ViewGroup) null);
        }
        TextView title = (TextView) view.findViewById(R.id.listitem_title);
        final Listitem listitem = item;
        view.findViewById(R.id.listitem_convrnience_phone).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new Intent();
                Intent intent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + listitem.phone));
                intent.setFlags(268435456);
                ConvenienceListFragment.this.getActivity().startActivity(intent);
            }
        });
        TextView juli = (TextView) view.findViewById(R.id.listitem_juli);
        title.setTextColor(-16777216);
        title.setText(item.title);
        ((TextView) view.findViewById(R.id.listitem_photo_text)).setText(item.phone);
        ((TextView) view.findViewById(R.id.listitem_des)).setText(item.des);
        if (PerfHelper.getBooleanData(PerfHelper.P_GPS_YES)) {
            Double distance = Double.valueOf(GpsUtils.getDistance(Double.parseDouble(item.longitude), Double.parseDouble(item.latitude), Double.parseDouble(PerfHelper.getStringData(PerfHelper.P_GPS_LONG)), Double.parseDouble(PerfHelper.getStringData(PerfHelper.P_GPS_LATI))));
            String distance_str = distance != null ? distance.toString() : null;
            if (distance_str != null && (first_index = distance_str.indexOf(".")) > 0) {
                distance_str = distance_str.substring(0, first_index);
            }
            if (Integer.valueOf(distance_str).intValue() > 1000) {
                DecimalFormat dt = (DecimalFormat) DecimalFormat.getInstance();
                dt.applyPattern("0.00");
                Double size = Double.valueOf(Double.valueOf(distance_str).doubleValue() / 1000.0d);
                if (size.doubleValue() > 100.0d) {
                    juli.setText("未知");
                } else {
                    juli.setText(String.valueOf(dt.format(size)) + "km");
                }
                juli.setTextSize(12.0f);
            } else {
                juli.setText(String.valueOf(distance_str) + "m");
            }
        } else {
            juli.setText("未知");
        }
        return view;
    }

    public void addListener() {
        super.addListener();
        if (this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            this.mListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                    final Listitem li = (Listitem) arg0.getItemAtPosition(arg2);
                    AlertDialog show = new AlertDialog.Builder(ConvenienceListFragment.this.getActivity()).setMessage("您确认要删除本条记录吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ConvenienceListFragment.this.mlistAdapter.datas.remove(arg2 - ConvenienceListFragment.this.mListview.getHeaderViewsCount());
                            ConvenienceListFragment.this.mlistAdapter.notifyDataSetChanged();
                            DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{li.n_mark});
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                    return false;
                }
            });
        }
    }

    public View getListHeadview(Object obj, int type) {
        System.out.println(String.valueOf(type) + "===");
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        ((ImageView) this.headview.findViewById(R.id.head_icon)).setLayoutParams(this.mHead_Layout);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText("========");
        return this.headview;
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count);
        }
        String json = DNDataSource.list_FromNET(String.valueOf(getResources().getString(R.string.citylife_convenience_list_url)) + "cityId=" + PerfHelper.getStringData(PerfHelper.P_CITY_No) + "&convenientId=" + oldtype + "&pageNo=" + (page + 1) + "&pageNum=" + count, oldtype, page, count, parttype, isfirst);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public View getListHeadview(Listitem item) {
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        ImageView iv = (ImageView) this.headview.findViewById(R.id.head_icon);
        iv.setLayoutParams(this.mHead_Layout);
        ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + item.icon, iv);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText(item.title);
        return this.headview;
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has("responseCode") || jsonobj.getInt("responseCode") == 0) {
            JSONArray jsonay = jsonobj.getJSONArray("results");
            if (jsonobj.has("head")) {
                try {
                    JSONArray ja = jsonobj.getJSONArray("head");
                    if (ja.length() == 1) {
                        JSONObject jsonhead = ja.getJSONObject(0);
                        Listitem head1 = new Listitem();
                        head1.nid = jsonhead.getString(LocaleUtil.INDONESIAN);
                        head1.title = jsonhead.getString(Constants.PARAM_TITLE);
                        head1.des = jsonhead.getString("des");
                        head1.icon = jsonhead.getString("icon");
                        head1.getMark();
                        head1.ishead = "true";
                        data.obj = head1;
                        data.headtype = 0;
                    } else {
                        int count = ja.length();
                        List<Listitem> li = new ArrayList<>();
                        for (int i = 0; i < count; i++) {
                            JSONObject jsonhead2 = ja.getJSONObject(i);
                            Listitem head12 = new Listitem();
                            head12.nid = jsonhead2.getString(LocaleUtil.INDONESIAN);
                            head12.title = jsonhead2.getString(Constants.PARAM_TITLE);
                            head12.des = jsonhead2.getString("des");
                            head12.icon = jsonhead2.getString("icon");
                            head12.ishead = "true";
                            head12.getMark();
                            li.add(head12);
                        }
                        data.obj = li;
                        data.headtype = 2;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int count2 = jsonay.length();
            for (int i2 = 0; i2 < count2; i2++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i2);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has(sina_weibo.Constants.SINA_NAME)) {
                        o.title = obj.getString(sina_weibo.Constants.SINA_NAME);
                    }
                    if (obj.has("address")) {
                        o.des = obj.getString("address");
                    }
                    if (obj.has("imgUrl")) {
                        o.icon = obj.getString("imgUrl");
                    }
                    if (obj.has("phone")) {
                        o.phone = obj.getString("phone");
                    }
                    if (obj.has("dim")) {
                        o.latitude = obj.getString("dim");
                    }
                    if (obj.has("log")) {
                        o.longitude = obj.getString("log");
                    }
                } catch (Exception e2) {
                }
                o.getMark();
                data.list.add(o);
            }
        } else {
            this.mHandler.sendEmptyMessage(FinalVariable.error);
        }
        return data;
    }

    public void onResume() {
        super.onResume();
        if (!this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY))) {
            this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
            this.mMain_layout.findViewById(R.id.loading).setVisibility(0);
            new Thread(new Runnable() {
                public void run() {
                    ConvenienceListFragment.this.reFlush();
                }
            }).start();
        }
    }
}
