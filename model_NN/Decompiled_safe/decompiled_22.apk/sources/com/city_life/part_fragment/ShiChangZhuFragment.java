package com.city_life.part_fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.city_life.entity.CityLifeApplication;
import com.city_life.part_activiy.DetailActivity;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.ad.AdAdapter;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.utils.FinalVariable;
import com.utils.GpsUtils;
import com.utils.PerfHelper;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class ShiChangZhuFragment extends LoadMoreListFragment<Listitem> {
    Handler bar_ani_hanHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
            }
        }
    };
    ColorStateList csl_n;
    ColorStateList csl_r;
    LinearLayout.LayoutParams frla;
    View headview;
    RelativeLayout.LayoutParams mLa;
    String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    View.OnClickListener oncilc = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction(ShiChangZhuFragment.this.mContext.getResources().getString(R.string.activity_keyword));
            intent.putExtra("parttpye", v.getTag().toString());
            ShiChangZhuFragment.this.startActivity(intent);
            ShiChangZhuFragment.this.getActivity().overridePendingTransition(R.anim.init_in, R.anim.init_out);
        }
    };
    int[] p_count = {R.id.listitem_tag, R.id.listitem_tag_1, R.id.listitem_tag_2};
    private String part_name;
    RelativeLayout.LayoutParams relal;
    TextView[] tags = new TextView[5];

    public static BaseFragment<Listitem> newInstance(String type, String partType) {
        ShiChangZhuFragment tf = new ShiChangZhuFragment();
        tf.initType(type, partType);
        return tf;
    }

    public void findView() {
        super.findView();
        this.nodata_tip = R.string.shichanglist_no_date;
        this.csl_r = getResources().getColorStateList(R.color.list_item_title_r);
        this.csl_n = getResources().getColorStateList(R.color.list_item_title_n);
        this.mHead_Layout = new FrameLayout.LayoutParams(-1, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 330) / 640);
        int w = (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 170) / 640;
        int h = (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 130) / 640;
        this.mIcon_Layout = new LinearLayout.LayoutParams(w, h);
        this.mLa = new RelativeLayout.LayoutParams(w, h);
        this.frla = new LinearLayout.LayoutParams(w, h);
        this.relal = new RelativeLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 120) / 480, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 92) / 480);
        this.relal.addRule(11);
        this.relal.rightMargin = 10;
        try {
            this.part_name = DBHelper.getDBHelper().select("part_list", "part_name", "part_sa=?", new String[]{this.mOldtype});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public View getListItemview(View view, Listitem item, int position) {
        int first_index;
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_nearbay, (ViewGroup) null);
        }
        ImageView icon = (ImageView) view.findViewById(R.id.listitem_icon);
        ImageView vip = (ImageView) view.findViewById(R.id.listitem_vip);
        TextView title = (TextView) view.findViewById(R.id.listitem_title);
        TextView des = (TextView) view.findViewById(R.id.listitem_didian);
        TextView juli = (TextView) view.findViewById(R.id.listitem_juli);
        TextView youhui = (TextView) view.findViewById(R.id.listitem_youhui_how);
        TextView dianhua = (TextView) view.findViewById(R.id.listitem_photos_text);
        icon.setLayoutParams(this.frla);
        if (item.vip_id.equals(UploadUtils.SUCCESS)) {
            vip.setVisibility(8);
        } else if (item.vip_id.equals(UploadUtils.FAILURE)) {
            vip.setImageResource(R.drawable.iem_vip_1);
            vip.setVisibility(0);
        } else if (item.vip_id.equals("2")) {
            vip.setImageResource(R.drawable.iem_vip_2);
            vip.setVisibility(0);
        } else {
            vip.setVisibility(8);
        }
        youhui.setText(item.preferential);
        dianhua.setText(item.phone);
        title.setText(item.title);
        if (PerfHelper.getBooleanData(PerfHelper.P_GPS_YES)) {
            Double distance = Double.valueOf(GpsUtils.getDistance(Double.parseDouble(item.longitude), Double.parseDouble(item.latitude), Double.parseDouble(PerfHelper.getStringData(PerfHelper.P_GPS_LONG)), Double.parseDouble(PerfHelper.getStringData(PerfHelper.P_GPS_LATI))));
            String distance_str = distance != null ? distance.toString() : null;
            if (distance_str != null && (first_index = distance_str.indexOf(".")) > 0) {
                distance_str = distance_str.substring(0, first_index);
            }
            if (Integer.valueOf(distance_str).intValue() > 1000) {
                DecimalFormat dt = (DecimalFormat) DecimalFormat.getInstance();
                dt.applyPattern("0.00");
                Double size = Double.valueOf(Double.valueOf(distance_str).doubleValue() / 1000.0d);
                if (size.doubleValue() > 100.0d) {
                    juli.setText("未知");
                } else {
                    juli.setText(String.valueOf(dt.format(size)) + "km");
                }
                juli.setTextSize(12.0f);
            } else {
                juli.setText(String.valueOf(distance_str) + "m");
            }
        } else {
            juli.setText("未知");
        }
        if (item.icon == null || item.icon.length() <= 10) {
            icon.setImageResource(R.drawable.default_picture);
        } else {
            ShareApplication.mImageWorker.loadImage(item.icon, icon);
        }
        title.setText(item.title);
        des.setText(item.address);
        return view;
    }

    public void getDistance(Listitem item) {
    }

    public void fillAd(AdAdapter ad) {
        if (this.mPage == 0 && this.isShowAD && !this.mParttype.startsWith(DBHelper.FAV_FLAG) && !this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            new GetShiChangZhuClientListId(this.mOldtype.replace("ShiChangZhuFragment", ""), ad).execute(null);
        }
    }

    public void addListener() {
        super.addListener();
        if (this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            this.mListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                    final Listitem li = (Listitem) arg0.getItemAtPosition(arg2);
                    AlertDialog show = new AlertDialog.Builder(ShiChangZhuFragment.this.getActivity()).setMessage("您确认要删除本条记录吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ShiChangZhuFragment.this.mlistAdapter.datas.remove(arg2 - ShiChangZhuFragment.this.mListview.getHeaderViewsCount());
                            ShiChangZhuFragment.this.mlistAdapter.notifyDataSetChanged();
                            DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{li.n_mark});
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                    return false;
                }
            });
        }
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                ShiChangZhuFragment.this.mLoading.setVisibility(8);
                switch (msg.what) {
                    case FinalVariable.update /*1001*/:
                        ShiChangZhuFragment.this.mLoading_nodate.setVisibility(8);
                        ShiChangZhuFragment.this.update();
                        System.out.println("更新");
                        ShiChangZhuFragment.this.fillAd(ShiChangZhuFragment.this.mlistAdapter);
                        return;
                    case FinalVariable.remove_footer /*1002*/:
                        ShiChangZhuFragment.this.mLoading_nodate.setVisibility(8);
                        if (!(ShiChangZhuFragment.this.mListview == null || ShiChangZhuFragment.this.mList_footer == null || ShiChangZhuFragment.this.mListview.getFooterViewsCount() <= 0)) {
                            try {
                                ShiChangZhuFragment.this.mListview.removeFooterView(ShiChangZhuFragment.this.mList_footer);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        ShiChangZhuFragment.this.initfooter();
                        return;
                    case FinalVariable.change /*1003*/:
                    case FinalVariable.deletefoot /*1005*/:
                    case FinalVariable.first_load /*1008*/:
                    case FinalVariable.load_image /*1009*/:
                    case FinalVariable.other /*1010*/:
                    default:
                        return;
                    case FinalVariable.error /*1004*/:
                        ShiChangZhuFragment.this.initfooter();
                        if (ShiChangZhuFragment.this.mData == null || ShiChangZhuFragment.this.mData.list.size() <= 0) {
                            ShiChangZhuFragment.this.mLoading_nodate.setVisibility(0);
                            ShiChangZhuFragment.this.mLoading_nodate.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    new Thread(new Runnable() {
                                        public void run() {
                                            ShiChangZhuFragment.this.reFlush();
                                        }
                                    }).start();
                                    ShiChangZhuFragment.this.mLoading_nodate.setVisibility(8);
                                }
                            });
                            ShiChangZhuFragment.this.mLoading.setVisibility(0);
                        }
                        if (!Utils.isNetworkAvailable(ShiChangZhuFragment.this.mContext)) {
                            Utils.showToast((int) R.string.network_error);
                            return;
                        } else if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            return;
                        } else {
                            Utils.showToast((int) R.string.network_error);
                            return;
                        }
                    case FinalVariable.addfoot /*1006*/:
                        ShiChangZhuFragment.this.mLoading_nodate.setVisibility(8);
                        if (ShiChangZhuFragment.this.mListview != null && ShiChangZhuFragment.this.mList_footer != null && ShiChangZhuFragment.this.mListview.getFooterViewsCount() == 0) {
                            ShiChangZhuFragment.this.mListview.addFooterView(ShiChangZhuFragment.this.mList_footer);
                            return;
                        }
                        return;
                    case FinalVariable.nomore /*1007*/:
                        ShiChangZhuFragment.this.mLoading_nodate.setVisibility(8);
                        ShiChangZhuFragment.this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                        if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            return;
                        } else {
                            Utils.showToast(ShiChangZhuFragment.this.nodata_tip);
                            return;
                        }
                    case FinalVariable.first_update /*1011*/:
                        ShiChangZhuFragment.this.mLoading_nodate.setVisibility(8);
                        if (!(ShiChangZhuFragment.this.mlistAdapter == null || ShiChangZhuFragment.this.mlistAdapter.datas == null)) {
                            ShiChangZhuFragment.this.mlistAdapter.datas.clear();
                            ShiChangZhuFragment.this.mlistAdapter = null;
                        }
                        ShiChangZhuFragment.this.update();
                        ShiChangZhuFragment.this.fillAd(ShiChangZhuFragment.this.mlistAdapter);
                        return;
                }
            }
        };
    }

    public boolean dealClick(Listitem item, int position) {
        if (this.mlistAdapter == null || this.mlistAdapter.datas.size() <= 0 || item == null) {
            return false;
        }
        Intent intent = new Intent();
        intent.setClass(this.mContext, DetailActivity.class);
        intent.putExtra("item", item);
        intent.putExtra(Constants.PARAM_TITLE, item.title);
        startActivity(intent);
        return true;
    }

    public View getListHeadview(Object obj, int type) {
        System.out.println(String.valueOf(type) + "===");
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        TextView textView = (TextView) this.headview.findViewById(R.id.head_title_des);
        ((ImageView) this.headview.findViewById(R.id.head_icon)).setLayoutParams(this.mHead_Layout);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText("========");
        return this.headview;
    }

    public View getListHeadview(Listitem item) {
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        ImageView iv = (ImageView) this.headview.findViewById(R.id.head_icon);
        ((TextView) this.headview.findViewById(R.id.head_title_des)).setText(item.des);
        iv.setLayoutParams(this.mHead_Layout);
        ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + item.icon, iv);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText(item.title.trim());
        return this.headview;
    }

    public Data parseJson(String json) throws Exception {
        int code;
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (jsonobj.has("responseCode") && (code = jsonobj.getInt("responseCode")) != 0) {
            if (code == -1 || code == -2) {
                this.mHandler.sendEmptyMessage(FinalVariable.nomore);
                return data;
            } else if (code == -2) {
                this.mHandler.sendEmptyMessage(FinalVariable.error);
                return data;
            }
        }
        JSONArray jsonay = jsonobj.getJSONArray("results");
        int count = jsonay.length();
        for (int i = 0; i < count; i++) {
            Listitem o = new Listitem();
            JSONObject obj = jsonay.getJSONObject(i);
            o.nid = obj.getString(LocaleUtil.INDONESIAN);
            o.sa = String.valueOf(this.mOldtype) + "_" + this.part_name;
            try {
                if (obj.has("titile")) {
                    o.title = obj.getString("titile");
                }
                if (obj.has("brief")) {
                    o.des = obj.getString("brief");
                }
                if (obj.has("addtime")) {
                    o.u_date = obj.getString("addtime");
                }
                if (obj.has(sina_weibo.Constants.SINA_NAME)) {
                    o.fuwu = obj.getString(sina_weibo.Constants.SINA_NAME);
                }
                if (obj.has("brief")) {
                    o.shangjia = obj.getString("brief");
                }
                if (obj.has("sellerImgs")) {
                    o.img_list_1 = obj.getString("sellerImgs");
                }
                if (obj.has("productImgs")) {
                    o.img_list_2 = obj.getString("productImgs");
                }
                if (obj.has("address")) {
                    o.address = obj.getString("address");
                }
                if (obj.has("log")) {
                    o.longitude = obj.getString("log");
                }
                if (obj.has("dim")) {
                    o.latitude = obj.getString("dim");
                }
                if (obj.has("level")) {
                    o.level = obj.getString("level");
                }
                if (obj.has("sort")) {
                    o.vip_id = obj.getString("sort");
                }
                if (obj.has("logo")) {
                    o.icon = obj.getString("logo");
                }
                if (obj.has("preferential")) {
                    o.preferential = obj.getString("preferential");
                }
                if (obj.has("phone")) {
                    o.phone = obj.getString("phone");
                }
            } catch (Exception e) {
            }
            o.getMark();
            data.list.add(o);
        }
        return data;
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count);
        }
        String json = DNDataSource.CityLift_list_FromNET(String.valueOf(getResources().getString(R.string.citylife_nearby_list_url)) + "sellerId=-1" + "&cityId=" + PerfHelper.getStringData(PerfHelper.P_CITY_No) + "&log=" + PerfHelper.getStringData(PerfHelper.P_GPS_LONG) + "&dim=" + PerfHelper.getStringData(PerfHelper.P_GPS_LATI) + "&typeId=" + getActivity().getIntent().getStringExtra(LocaleUtil.INDONESIAN), oldtype, page + 1, count, parttype, isfirst);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public void onResume() {
        super.onResume();
        if (!this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY))) {
            this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
            new Thread(new Runnable() {
                public void run() {
                    ShiChangZhuFragment.this.reFlush();
                }
            }).start();
        }
    }

    class GetShiChangZhuClientListId extends AsyncTask<Void, Void, HashMap<String, Object>> {
        AdAdapter adpter;
        String marketid;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public GetShiChangZhuClientListId(String marketid2, AdAdapter adpter2) {
            this.marketid = marketid2;
            this.adpter = adpter2;
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("marketId", this.marketid));
            param.add(new BasicNameValuePair("cityId", PerfHelper.getStringData(PerfHelper.P_CITY_No)));
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(ShareApplication.share.getResources().getString(R.string.citylife_getClient_url), param);
                if (ShareApplication.debug) {
                    System.out.println("查询客户端接口返回:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.list);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            JSONArray jsonay = jsonobj.getJSONArray("results");
            CityLifeApplication.fav_list_id = new ArrayList<>();
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has("conent")) {
                        o.des = obj.getString("conent");
                    }
                    if (obj.has(sina_weibo.Constants.SINA_NAME)) {
                        o.title = obj.getString(sina_weibo.Constants.SINA_NAME);
                    }
                    if (obj.has(Constants.PARAM_URL)) {
                        o.other = obj.getString(Constants.PARAM_URL);
                    }
                    if (obj.has("imgUrl")) {
                        o.icon = obj.getString("imgUrl");
                    }
                } catch (Exception e) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (result != null && !UploadUtils.FAILURE.equals(result.get("responseCode")) && UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                final Listitem item = (Listitem) ((ArrayList) result.get("results")).get(0);
                View view = LayoutInflater.from(ShiChangZhuFragment.this.mContext).inflate((int) R.layout.listitem_shichangzhu_title, (ViewGroup) null);
                ShareApplication.mImageWorker.loadImage(item.icon.split(",")[0], (ImageView) view.findViewById(R.id.listitem_icon));
                ((TextView) view.findViewById(R.id.listitem_title)).setText(item.title);
                ((TextView) view.findViewById(R.id.listitem_des)).setText(item.des);
                view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (!item.other.startsWith("http://")) {
                            Utils.showToast("参数错误");
                            return;
                        }
                        try {
                            ShiChangZhuFragment.this.mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(item.other)));
                        } catch (Exception e) {
                            Utils.showToast("请安装浏览起");
                            e.printStackTrace();
                        }
                    }
                });
                if (this.adpter != null) {
                    this.adpter.setAdview(view);
                }
            }
        }
    }
}
