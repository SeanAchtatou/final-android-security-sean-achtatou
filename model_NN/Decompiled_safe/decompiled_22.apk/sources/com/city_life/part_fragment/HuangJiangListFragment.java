package com.city_life.part_fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.city_life.part_asynctask.UploadUtils;
import com.city_life.ui.HomeActivity;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class HuangJiangListFragment extends LoadMoreListFragment<Listitem> {
    View headview;
    String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    RelativeLayout.LayoutParams relal;

    public static BaseFragment<Listitem> newInstance(String type, String partType) {
        HuangJiangListFragment tf = new HuangJiangListFragment();
        tf.initType(type, partType);
        return tf;
    }

    public boolean dealClick(Listitem item, int position) {
        return true;
    }

    public void findView() {
        super.findView();
        this.mHead_Layout = new FrameLayout.LayoutParams(-1, ((PerfHelper.getIntData(PerfHelper.P_PHONE_W) - HomeActivity.dip2px(this.mContext, 20.0f)) * 235) / 445);
        this.relal = new RelativeLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 120) / 480, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 92) / 480);
        this.relal.leftMargin = 11;
    }

    public View getListItemview(View view, Listitem item, int position) {
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_huangjiang, (ViewGroup) null);
        }
        TextView title = (TextView) view.findViewById(R.id.listitem_title);
        TextView des = (TextView) view.findViewById(R.id.listitem_des);
        ImageView img = (ImageView) view.findViewById(R.id.zhuangtai);
        if (item.other.equals(UploadUtils.FAILURE)) {
            img.setBackgroundResource(R.drawable.jiangpingzhuangtai_wei);
        } else {
            img.setBackgroundResource(R.drawable.jiangpingzhuangtai_yg);
        }
        title.setText(item.des);
        des.setText(item.title);
        return view;
    }

    public void addListener() {
        super.addListener();
        if (this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            this.mListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                    final Listitem li = (Listitem) arg0.getItemAtPosition(arg2);
                    AlertDialog show = new AlertDialog.Builder(HuangJiangListFragment.this.getActivity()).setMessage("您确认要删除本条记录吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            HuangJiangListFragment.this.mlistAdapter.datas.remove(arg2 - HuangJiangListFragment.this.mListview.getHeaderViewsCount());
                            HuangJiangListFragment.this.mlistAdapter.notifyDataSetChanged();
                            DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{li.n_mark});
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                    return false;
                }
            });
        }
    }

    public View getListHeadview(Object obj, int type) {
        System.out.println(String.valueOf(type) + "===");
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        ((ImageView) this.headview.findViewById(R.id.head_icon)).setLayoutParams(this.mHead_Layout);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText("========");
        return this.headview;
    }

    public View getListHeadview(Listitem item) {
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        ImageView iv = (ImageView) this.headview.findViewById(R.id.head_icon);
        iv.setLayoutParams(this.mHead_Layout);
        ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + item.icon, iv);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText(item.title);
        return this.headview;
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count);
        }
        String json = DNDataSource.CityLift_list_FromNET(String.valueOf(getResources().getString(R.string.citylife_yaoyiayo_zhongjiang_url)) + "sms=" + PerfHelper.getStringData("phone"), oldtype, page + 1, count, parttype, isfirst);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has(Constants.SINA_CODE) || jsonobj.getInt(Constants.SINA_CODE) != 0) {
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has("comment")) {
                        o.des = obj.getString("comment");
                    }
                    if (obj.has("address")) {
                        o.address = obj.getString("address");
                    }
                    if (obj.has("time")) {
                        o.title = obj.getString("time");
                    }
                    if (obj.has("author")) {
                        o.author = obj.getString("author");
                    }
                    if (obj.has("addtime")) {
                        o.u_date = obj.getString("addtime");
                    }
                    if (obj.has("imgsList")) {
                        o.icon = obj.getString("imgsList");
                    }
                } catch (Exception e) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }
        throw new DataException(jsonobj.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG));
    }

    class CurrentAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("newsId", "-1"));
            param.add(new BasicNameValuePair("cityId", PerfHelper.P_CITY_No));
            param.add(new BasicNameValuePair("pageNum", "5"));
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(HuangJiangListFragment.this.getResources().getString(R.string.citylife_getHeadNews_list_url), param);
                if (ShareApplication.debug) {
                    System.out.println("头图返回:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.list);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("countNum")) {
                data.obj = jsonobj.getString("countNum");
            }
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has("comment")) {
                        o.des = obj.getString("comment");
                    }
                    if (obj.has("age")) {
                        o.level = obj.getString("age");
                    }
                    if (obj.has("time")) {
                        o.title = obj.getString("time");
                    }
                    if (obj.has("overdue")) {
                        o.other = obj.getString("overdue");
                    }
                    if (obj.has("imgUrl")) {
                        o.icon = obj.getString("imgUrl");
                    }
                } catch (Exception e) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          com.utils.PerfHelper.setInfo(java.lang.String, int):void
          com.utils.PerfHelper.setInfo(java.lang.String, long):void
          com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
          com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (result == null) {
                Toast.makeText(HuangJiangListFragment.this.getActivity(), "请求失败，请稍后再试", 0).show();
            } else if (UploadUtils.FAILURE.equals(result.get("responseCode"))) {
                Toast.makeText(HuangJiangListFragment.this.getActivity(), "登录失败，请输入正确的用户名和密码", 0).show();
            } else if (UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                ArrayList<Listitem> user_listArrayList = (ArrayList) result.get("results");
                PerfHelper.setInfo(PerfHelper.P_USERID, ((Listitem) user_listArrayList.get(0)).nid);
                PerfHelper.setInfo(PerfHelper.P_SHARE_AGE, ((Listitem) user_listArrayList.get(0)).level);
                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME, ((Listitem) user_listArrayList.get(0)).title);
                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE, ((Listitem) user_listArrayList.get(0)).icon);
                if (((Listitem) user_listArrayList.get(0)).other.equals(UploadUtils.FAILURE)) {
                    PerfHelper.setInfo(PerfHelper.P_SHARE_SEX, "男");
                } else {
                    PerfHelper.setInfo(PerfHelper.P_SHARE_SEX, "女");
                }
                PerfHelper.setInfo(PerfHelper.P_USER_LOGIN, true);
            }
        }
    }

    public void onResume() {
        super.onResume();
        if (!this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY))) {
            this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
            new Thread(new Runnable() {
                public void run() {
                    HuangJiangListFragment.this.reFlush();
                }
            }).start();
        }
    }
}
