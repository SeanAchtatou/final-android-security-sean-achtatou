package com.city_life.sliding;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.pengyou.citycommercialarea.R;

public class SampleListFragment extends ListFragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate((int) R.layout.list, (ViewGroup) null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SampleAdapter adapter = new SampleAdapter(getActivity());
        for (int i = 0; i < 20; i++) {
            adapter.add(new SampleItem("Sample List", 17301583));
        }
        setListAdapter(adapter);
    }

    private class SampleItem {
        public int iconRes;
        public String tag;

        public SampleItem(String tag2, int iconRes2) {
            this.tag = tag2;
            this.iconRes = iconRes2;
        }
    }

    public class SampleAdapter extends ArrayAdapter<SampleItem> {
        public SampleAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate((int) R.layout.row, (ViewGroup) null);
            }
            ((ImageView) convertView.findViewById(R.id.row_icon)).setImageResource(((SampleItem) getItem(position)).iconRes);
            ((TextView) convertView.findViewById(R.id.row_title)).setText(((SampleItem) getItem(position)).tag);
            return convertView;
        }
    }
}
