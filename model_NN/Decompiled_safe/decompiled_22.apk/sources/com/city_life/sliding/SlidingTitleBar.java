package com.city_life.sliding;

import android.os.Bundle;
import com.pengyou.citycommercialarea.R;

public class SlidingTitleBar extends BaseActivity {
    public SlidingTitleBar() {
        super(R.string.title_bar_slide);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.content_frame);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SampleListFragment()).commit();
        setSlidingActionBarEnabled(true);
    }
}
