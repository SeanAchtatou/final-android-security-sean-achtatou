package com.city_life.sliding;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import com.pengyou.citycommercialarea.R;
import com.slidingmenu.lib.SlidingMenu;

public class PropertiesActivity extends BaseActivity {
    public PropertiesActivity() {
        super(R.string.properties);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSlidingActionBarEnabled(true);
        setContentView((int) R.layout.properties);
        RadioGroup mode = (RadioGroup) findViewById(R.id.mode);
        mode.check(R.id.left);
        mode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                SlidingMenu sm = PropertiesActivity.this.getSlidingMenu();
                switch (checkedId) {
                    case R.id.left /*2131230742*/:
                        sm.setMode(0);
                        sm.setShadowDrawable((int) R.drawable.shadow);
                        return;
                    case R.id.right /*2131230743*/:
                        sm.setMode(1);
                        sm.setShadowDrawable((int) R.drawable.shadowright);
                        return;
                    case R.id.left_right /*2131231103*/:
                        sm.setMode(2);
                        sm.setSecondaryMenu((int) R.layout.menu_frame_two);
                        PropertiesActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame_two, new SampleListFragment()).commit();
                        sm.setSecondaryShadowDrawable((int) R.drawable.shadowright);
                        sm.setShadowDrawable((int) R.drawable.shadow);
                        return;
                    default:
                        return;
                }
            }
        });
        RadioGroup touchAbove = (RadioGroup) findViewById(R.id.touch_above);
        touchAbove.check(R.id.touch_above_full);
        touchAbove.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.touch_above_full /*2131231105*/:
                        PropertiesActivity.this.getSlidingMenu().setTouchModeAbove(1);
                        return;
                    case R.id.touch_above_margin /*2131231106*/:
                        PropertiesActivity.this.getSlidingMenu().setTouchModeAbove(0);
                        return;
                    case R.id.touch_above_none /*2131231107*/:
                        PropertiesActivity.this.getSlidingMenu().setTouchModeAbove(2);
                        return;
                    default:
                        return;
                }
            }
        });
        SeekBar scrollScale = (SeekBar) findViewById(R.id.scroll_scale);
        scrollScale.setMax(1000);
        scrollScale.setProgress(333);
        scrollScale.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                PropertiesActivity.this.getSlidingMenu().setBehindScrollScale(((float) seekBar.getProgress()) / ((float) seekBar.getMax()));
            }
        });
        SeekBar behindWidth = (SeekBar) findViewById(R.id.behind_width);
        behindWidth.setMax(1000);
        behindWidth.setProgress(750);
        behindWidth.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                PropertiesActivity.this.getSlidingMenu().setBehindWidth((int) (((float) PropertiesActivity.this.getSlidingMenu().getWidth()) * (((float) seekBar.getProgress()) / ((float) seekBar.getMax()))));
                PropertiesActivity.this.getSlidingMenu().requestLayout();
            }
        });
        CheckBox shadowEnabled = (CheckBox) findViewById(R.id.shadow_enabled);
        shadowEnabled.setChecked(true);
        shadowEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int i;
                if (isChecked) {
                    SlidingMenu slidingMenu = PropertiesActivity.this.getSlidingMenu();
                    if (PropertiesActivity.this.getSlidingMenu().getMode() == 0) {
                        i = R.drawable.shadow;
                    } else {
                        i = R.drawable.shadowright;
                    }
                    slidingMenu.setShadowDrawable(i);
                    return;
                }
                PropertiesActivity.this.getSlidingMenu().setShadowDrawable((Drawable) null);
            }
        });
        SeekBar shadowWidth = (SeekBar) findViewById(R.id.shadow_width);
        shadowWidth.setMax(1000);
        shadowWidth.setProgress(75);
        shadowWidth.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                PropertiesActivity.this.getSlidingMenu().setShadowWidth((int) (((float) PropertiesActivity.this.getSlidingMenu().getWidth()) * (((float) seekBar.getProgress()) / ((float) seekBar.getMax()))));
                PropertiesActivity.this.getSlidingMenu().invalidate();
            }
        });
        CheckBox fadeEnabled = (CheckBox) findViewById(R.id.fade_enabled);
        fadeEnabled.setChecked(true);
        fadeEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PropertiesActivity.this.getSlidingMenu().setFadeEnabled(isChecked);
            }
        });
        SeekBar fadeDeg = (SeekBar) findViewById(R.id.fade_degree);
        fadeDeg.setMax(1000);
        fadeDeg.setProgress(666);
        fadeDeg.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                PropertiesActivity.this.getSlidingMenu().setFadeDegree(((float) seekBar.getProgress()) / ((float) seekBar.getMax()));
            }
        });
    }
}
