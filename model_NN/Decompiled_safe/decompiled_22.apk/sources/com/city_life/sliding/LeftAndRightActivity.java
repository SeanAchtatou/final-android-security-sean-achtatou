package com.city_life.sliding;

import android.os.Bundle;
import com.city_life.fragment.HomeFragment;
import com.pengyou.citycommercialarea.R;

public class LeftAndRightActivity extends BaseActivity {
    public LeftAndRightActivity() {
        super(R.string.left_and_right);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSlidingMenu().setMode(2);
        getSlidingMenu().setTouchModeAbove(1);
        setContentView((int) R.layout.content_frame);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, HomeFragment.newInstance("shenghuo", "home")).commit();
        getSlidingMenu().setSecondaryMenu((int) R.layout.menu_frame_two);
        getSlidingMenu().setSecondaryShadowDrawable((int) R.drawable.shadowright);
        getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame_two, new RightSampleListFragment()).commit();
    }
}
