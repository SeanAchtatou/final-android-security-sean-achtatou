package com.city_life.sliding;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.pengyou.citycommercialarea.R;
import com.slidingmenu.lib.SlidingMenu;

public class AttachExample extends FragmentActivity {
    private SlidingMenu menu;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle((int) R.string.attach);
        setContentView((int) R.layout.content_frame);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SampleListFragment()).commit();
        this.menu = new SlidingMenu(this);
        this.menu.setTouchModeAbove(1);
        this.menu.setShadowWidthRes(R.dimen.shadow_width);
        this.menu.setShadowDrawable((int) R.drawable.shadow);
        this.menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        this.menu.setFadeDegree(0.35f);
        this.menu.attachToActivity(this, 1);
        this.menu.setMenu((int) R.layout.menu_frame);
        getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, new SampleListFragment()).commit();
    }

    public void onBackPressed() {
        if (this.menu.isMenuShowing()) {
            this.menu.showContent();
        } else {
            super.onBackPressed();
        }
    }
}
