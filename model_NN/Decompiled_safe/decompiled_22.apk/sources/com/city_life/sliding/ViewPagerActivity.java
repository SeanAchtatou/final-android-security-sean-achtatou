package com.city_life.sliding;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import com.city_life.part_asynctask.UploadUtils;
import com.city_life.sliding.fragments.ColorFragment;
import com.city_life.sliding.fragments.FragmentChangeActivity;
import com.city_life.ui.YCurrentHomeActivity;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.apptime.AppTimeStatisticsService;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class ViewPagerActivity extends YCurrentHomeActivity {
    public static String ACTIVITY_FINSH;
    public static ArrayList<Listitem> v_list = new ArrayList<>();
    public int count = 0;
    FinishCastReceiver finishBroadCastReceiver;
    /* access modifiers changed from: private */
    public int index = 0;
    /* access modifiers changed from: private */
    public boolean isnow = true;
    public Data mData;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FinalVariable.update /*1001*/:
                    ViewPagerActivity.this.update();
                    return;
                case FinalVariable.remove_footer /*1002*/:
                    ViewPagerActivity.this.vp.setCurrentItem(ViewPagerActivity.this.index);
                    return;
                case FinalVariable.change /*1003*/:
                case FinalVariable.deletefoot /*1005*/:
                case FinalVariable.addfoot /*1006*/:
                case FinalVariable.first_load /*1008*/:
                case FinalVariable.load_image /*1009*/:
                case FinalVariable.other /*1010*/:
                default:
                    return;
                case FinalVariable.error /*1004*/:
                    if (!Utils.isNetworkAvailable(ViewPagerActivity.this)) {
                        Utils.showToast((int) R.string.network_error);
                        return;
                    } else if (msg.obj != null) {
                        Utils.showToast(msg.obj.toString());
                        return;
                    } else {
                        Utils.showToast((int) R.string.network_error);
                        return;
                    }
                case FinalVariable.nomore /*1007*/:
                    if (msg.obj != null) {
                        Utils.showToast(msg.obj.toString());
                        return;
                    } else {
                        Utils.showToast(ViewPagerActivity.this.nodata_tip);
                        return;
                    }
                case FinalVariable.first_update /*1011*/:
                    ViewPagerActivity.this.update();
                    return;
            }
        }
    };
    public int mLength = 5;
    public String mOldtype = "top";
    public int mPage;
    public String mParttype = "top";
    ImageView[] n_nowView = new ImageView[5];
    String nodata_tip = "没有更多";
    int[] p_count = {R.id.mark_1, R.id.mark_2, R.id.mark_3, R.id.mark_4, R.id.mark_5};
    ImageView p_oldeview;
    ColorPagerAdapter pager;
    private TimerTask task = new TimerTask() {
        public void run() {
            Message message = new Message();
            message.what = FinalVariable.remove_footer;
            if (ViewPagerActivity.this.index == ViewPagerActivity.this.count - 1) {
                ViewPagerActivity.this.isnow = false;
            }
            if (ViewPagerActivity.this.index == 0) {
                ViewPagerActivity.this.isnow = true;
            }
            if (ViewPagerActivity.this.isnow) {
                ViewPagerActivity viewPagerActivity = ViewPagerActivity.this;
                viewPagerActivity.index = viewPagerActivity.index + 1;
            } else {
                ViewPagerActivity viewPagerActivity2 = ViewPagerActivity.this;
                viewPagerActivity2.index = viewPagerActivity2.index - 1;
            }
            ViewPagerActivity.this.mHandler.sendMessage(message);
        }
    };
    Timer timer;
    ViewPager vp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSlidingActionBarEnabled(true);
        setContentView((int) R.layout.activity_home);
        this.vp = (ViewPager) findViewById(R.id.home_viewpager);
        getSlidingMenu().setMode(1);
        getSlidingMenu().setShadowDrawable((int) R.drawable.shadowright);
        initData();
        getSlidingMenu().setTouchModeAbove(0);
        setFinish();
    }

    public void changePart(View view) {
        view.setSelected(false);
        Intent intent = new Intent();
        intent.setClass(this, FragmentChangeActivity.class);
        startActivity(intent);
    }

    public void things(View view) {
        toggle();
    }

    public class ColorPagerAdapter extends FragmentPagerAdapter {
        private ArrayList<Fragment> mFragments = new ArrayList<>();

        public ColorPagerAdapter(FragmentManager fm) {
            super(fm);
            ViewPagerActivity.this.count = ViewPagerActivity.this.mData.list.size();
            for (int i = 0; i < ViewPagerActivity.this.count; i++) {
                this.mFragments.add(new ColorFragment(((Listitem) ViewPagerActivity.this.mData.list.get(i)).icon, ((Listitem) ViewPagerActivity.this.mData.list.get(i)).nid, String.valueOf(i), (Listitem) ViewPagerActivity.this.mData.list.get(i)));
            }
            ShareApplication.items = ViewPagerActivity.this.mData.list;
        }

        public int getCount() {
            return ViewPagerActivity.this.count;
        }

        public Fragment getItem(int position) {
            return this.mFragments.get(position);
        }
    }

    static {
        ACTIVITY_FINSH = "com.palmtrends.activity.finish";
        ACTIVITY_FINSH = ShareApplication.share.getResources().getString(R.string.activity_all_finish);
    }

    public void setFinish() {
        this.finishBroadCastReceiver = new FinishCastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTIVITY_FINSH);
        registerReceiver(this.finishBroadCastReceiver, intentFilter);
    }

    public class FinishCastReceiver extends BroadcastReceiver {
        public FinishCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            ViewPagerActivity.this.finish();
        }
    }

    public void finish() {
        if (this.finishBroadCastReceiver != null) {
            try {
                unregisterReceiver(this.finishBroadCastReceiver);
            } catch (Exception e) {
            }
        }
        super.finish();
    }

    public void initData() {
        this.mPage = 0;
        new Thread() {
            String old;

            public void run() {
                this.old = ViewPagerActivity.this.mOldtype;
                try {
                    ViewPagerActivity.this.mData = ViewPagerActivity.this.getDataFromDB(ViewPagerActivity.this.mOldtype, 0, ViewPagerActivity.this.mLength, ViewPagerActivity.this.mParttype);
                    if (!(ViewPagerActivity.this.mData == null || ViewPagerActivity.this.mData.list == null || ViewPagerActivity.this.mData.list.size() <= 0)) {
                        ViewPagerActivity.this.mHandler.sendEmptyMessage(FinalVariable.update);
                    }
                    ViewPagerActivity.this.mData = ViewPagerActivity.this.getDataFromNet(String.valueOf(Urls.home) + "&w=" + PerfHelper.getIntData(PerfHelper.P_PHONE_W) + "&h=" + PerfHelper.getIntData(PerfHelper.P_PHONE_H), ViewPagerActivity.this.mOldtype, 0, ViewPagerActivity.this.mLength, true, ViewPagerActivity.this.mParttype);
                    ViewPagerActivity.this.mHandler.sendEmptyMessage(FinalVariable.update);
                    if (!ViewPagerActivity.this.mOldtype.equals(this.old)) {
                    }
                } catch (Exception e) {
                }
            }
        }.start();
    }

    public void update() {
        this.pager = new ColorPagerAdapter(getSupportFragmentManager());
        this.vp.setAdapter(this.pager);
        this.vp.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (ViewPagerActivity.this.timer == null) {
                    return false;
                }
                ViewPagerActivity.this.timer.cancel();
                return false;
            }
        });
        this.count = this.mData.list.size();
        for (int i = 0; i < this.count; i++) {
            this.n_nowView[i] = (ImageView) findViewById(this.p_count[i]);
            this.n_nowView[i].setVisibility(0);
        }
        this.n_nowView[0].setImageResource(R.drawable.home_shuzi_h);
        this.p_oldeview = this.n_nowView[0];
        this.vp.setCurrentItem(0);
        this.vp.setVisibility(0);
        this.vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int arg0) {
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageSelected(int position) {
                ViewPagerActivity.this.p_oldeview.setImageResource(R.drawable.home_shuzi_n);
                ViewPagerActivity.this.n_nowView[position].setImageResource(R.drawable.home_shuzi_h);
                ViewPagerActivity.this.p_oldeview = ViewPagerActivity.this.n_nowView[position];
                ViewPagerActivity.this.index = position;
                if (position == ViewPagerActivity.this.count - 1) {
                    ViewPagerActivity.this.getSlidingMenu().setTouchModeAbove(1);
                } else {
                    ViewPagerActivity.this.getSlidingMenu().setTouchModeAbove(0);
                }
            }
        });
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count2, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count2);
        }
        String json = DNDataSource.list_FromNET(url, oldtype, page, count2, parttype, isfirst);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public Data parseJson(String json) throws Exception {
        return ParserArticleList(json);
    }

    public static Data ParserArticleList(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has(Constants.SINA_CODE) || jsonobj.getInt(Constants.SINA_CODE) != 0) {
            if (jsonobj.has("def")) {
                data.loadmorestate = jsonobj.getInt("def");
            }
            JSONArray jsonay = jsonobj.getJSONArray("list");
            if (jsonobj.has("head")) {
                try {
                    JSONArray ja = jsonobj.getJSONArray("head");
                    if (ja.length() == 1) {
                        JSONObject jsonhead = ja.getJSONObject(0);
                        Listitem head1 = new Listitem();
                        head1.nid = jsonhead.getString(LocaleUtil.INDONESIAN);
                        head1.title = jsonhead.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                        head1.des = jsonhead.getString("des");
                        head1.icon = jsonhead.getString("icon");
                        head1.getMark();
                        head1.ishead = "true";
                        data.obj = head1;
                        data.headtype = 0;
                    } else {
                        int count2 = ja.length();
                        List<Listitem> li = new ArrayList<>();
                        for (int i = 0; i < count2; i++) {
                            JSONObject jsonhead2 = ja.getJSONObject(i);
                            Listitem head12 = new Listitem();
                            head12.nid = jsonhead2.getString(LocaleUtil.INDONESIAN);
                            head12.title = jsonhead2.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                            head12.des = jsonhead2.getString("des");
                            head12.icon = jsonhead2.getString("icon");
                            head12.ishead = "true";
                            head12.getMark();
                            li.add(head12);
                        }
                        data.obj = li;
                        data.headtype = 2;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int count3 = jsonay.length();
            for (int i2 = 0; i2 < count3; i2++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i2);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                o.title = obj.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                try {
                    if (obj.has("des")) {
                        o.des = obj.getString("des");
                    }
                    if (obj.has("adddate")) {
                        o.u_date = obj.getString("adddate");
                    }
                    if (obj.has("sugfrom")) {
                        o.other = obj.getString("sugfrom");
                    }
                    o.icon = obj.getString("icon");
                } catch (Exception e2) {
                }
                o.getMark();
                if (!o.other.equals(UploadUtils.SUCCESS)) {
                    v_list.add(o);
                }
                data.list.add(o);
            }
            return data;
        }
        throw new DataException(jsonobj.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG));
    }

    public Data getDataFromDB(String oldtype, int page, int count2, String parttype) throws Exception {
        String json = DNDataSource.list_FromDB(oldtype, page, count2, parttype);
        if (json == null || "".equals(json) || "null".equals(json)) {
            return null;
        }
        return parseJson(json);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            new AlertDialog.Builder(this).setTitle((int) R.string.exit).setMessage(getString(R.string.exit_message, new String[]{getString(R.string.app_name)})).setPositiveButton((int) R.string.done, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ViewPagerActivity.this.sendBroadcast(new Intent(BaseActivity.ACTIVITY_FINSH));
                    ViewPagerActivity.this.stopService(new Intent(ViewPagerActivity.this, AppTimeStatisticsService.class));
                }
            }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();
        }
        return super.onKeyDown(keyCode, event);
    }
}
