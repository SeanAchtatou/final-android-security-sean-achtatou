package com.city_life.sliding;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.text.Html;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.city_life.anim.CustomScaleAnimation;
import com.city_life.anim.CustomSlideAnimation;
import com.city_life.anim.CustomZoomAnimation;
import com.pengyou.citycommercialarea.R;
import java.net.URLEncoder;

public class ExampleListActivity extends SherlockPreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle((int) R.string.app_name);
        addPreferencesFromResource(R.xml.main);
    }

    public boolean onPreferenceTreeClick(PreferenceScreen screen, Preference pref) {
        Class<?> cls = null;
        String title = pref.getTitle().toString();
        if (title.equals(getString(R.string.properties))) {
            cls = PropertiesActivity.class;
        } else if (title.equals(getString(R.string.attach))) {
            cls = AttachExample.class;
        } else if (!title.equals(getString(R.string.changing_fragments))) {
            if (title.equals(getString(R.string.left_and_right))) {
                cls = LeftAndRightActivity.class;
            } else if (!title.equals(getString(R.string.responsive_ui))) {
                if (title.equals(getString(R.string.viewpager))) {
                    cls = ViewPagerActivity.class;
                } else if (title.equals(getString(R.string.title_bar_slide))) {
                    cls = SlidingTitleBar.class;
                } else if (title.equals(getString(R.string.title_bar_content))) {
                    cls = SlidingContent.class;
                } else if (title.equals(getString(R.string.anim_zoom))) {
                    cls = CustomZoomAnimation.class;
                } else if (title.equals(getString(R.string.anim_scale))) {
                    cls = CustomScaleAnimation.class;
                } else if (title.equals(getString(R.string.anim_slide))) {
                    cls = CustomSlideAnimation.class;
                }
            }
        }
        startActivity(new Intent(this, cls));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.github /*2131231251*/:
                Util.goToGitHub(this);
                return true;
            case R.id.about /*2131231252*/:
                new AlertDialog.Builder(this).setTitle((int) R.string.about).setMessage(Html.fromHtml(getString(R.string.about_msg))).show();
                break;
            case R.id.licenses /*2131231253*/:
                new AlertDialog.Builder(this).setTitle((int) R.string.licenses).setMessage(Html.fromHtml(getString(R.string.apache_license))).show();
                break;
            case R.id.contact /*2131231254*/:
                Intent email = new Intent("android.intent.action.SENDTO");
                email.setData(Uri.parse("mailto:jfeinstein10@gmail.com?subject=" + URLEncoder.encode("SlidingMenu Demos Feedback")));
                try {
                    startActivity(email);
                    break;
                } catch (Exception e) {
                    Toast.makeText(this, (int) R.string.no_email, 0).show();
                    break;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.example_list, menu);
        return true;
    }
}
