package com.city_life.sliding.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ColorFragment extends Fragment {
    String icon_url;
    String id;
    ImageView img;
    Listitem item;
    List<Listitem> listitem = new ArrayList();
    public RelativeLayout mContainers;
    public Context mContext;
    public View mMain_layout;
    String position;

    public ColorFragment(String iconurl, String ids, String positions, Listitem list) {
        this.icon_url = iconurl;
        this.id = ids;
        this.position = positions;
        this.item = list;
        this.listitem.add(list);
        setRetainInstance(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.icon_url = savedInstanceState.getString("icon_url");
            this.id = savedInstanceState.getString(LocaleUtil.INDONESIAN);
            this.position = savedInstanceState.getString("position");
        }
        this.mContext = inflater.getContext();
        if (this.mMain_layout == null) {
            this.mContainers = new RelativeLayout(this.mContext);
            this.mMain_layout = inflater.inflate((int) R.layout.listitem_home, (ViewGroup) null);
            initListFragment(inflater);
            this.mContainers.addView(this.mMain_layout);
        } else {
            this.mContainers.removeAllViews();
            this.mContainers = new RelativeLayout(getActivity());
            this.mContainers.addView(this.mMain_layout);
        }
        return this.mContainers;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("icon_url", this.icon_url);
        outState.putString(LocaleUtil.INDONESIAN, this.id);
        outState.putString("position", this.position);
    }

    public void initListFragment(LayoutInflater inflater) {
        this.img = (ImageView) this.mMain_layout.findViewById(R.id.listitem_home_icon);
        if (this.icon_url != null && this.icon_url.length() > 10) {
            ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + this.icon_url, this.img);
        }
        this.img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                if (ColorFragment.this.item.other.equals(UploadUtils.SUCCESS)) {
                    intent.setAction(ColorFragment.this.mContext.getResources().getString(R.string.activity_pic));
                    intent.putExtra("item", ColorFragment.this.item);
                    intent.putExtra("type", ColorFragment.this.item.other);
                    intent.putExtra("position", Integer.parseInt(ColorFragment.this.position));
                } else {
                    intent.setAction(ColorFragment.this.mContext.getResources().getString(R.string.activity_anim_article));
                    intent.putExtra("item", ColorFragment.this.item);
                    intent.putExtra("type", ColorFragment.this.item.other);
                    intent.putExtra("items", (Serializable) ColorFragment.this.listitem);
                    intent.putExtra("position", 0);
                }
                ColorFragment.this.startActivity(intent);
            }
        });
    }
}
