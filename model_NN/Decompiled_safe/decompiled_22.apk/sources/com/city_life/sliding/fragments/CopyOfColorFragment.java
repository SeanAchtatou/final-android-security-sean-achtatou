package com.city_life.sliding.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.pengyou.citycommercialarea.R;

public class CopyOfColorFragment extends Fragment {
    private int mColorRes;

    public CopyOfColorFragment() {
        this(R.color.white);
    }

    public CopyOfColorFragment(int colorRes) {
        this.mColorRes = -1;
        this.mColorRes = colorRes;
        setRetainInstance(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.mColorRes = savedInstanceState.getInt("mColorRes");
        }
        int color = getResources().getColor(this.mColorRes);
        RelativeLayout v = new RelativeLayout(getActivity());
        v.setBackgroundColor(color);
        return v;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("mColorRes", this.mColorRes);
    }
}
