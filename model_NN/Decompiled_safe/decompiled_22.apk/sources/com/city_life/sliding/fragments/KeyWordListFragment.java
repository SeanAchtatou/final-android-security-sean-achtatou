package com.city_life.sliding.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.PerfHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class KeyWordListFragment extends LoadMoreListFragment<Listitem> {
    View headview;
    FrameLayout.LayoutParams mLa;
    View.OnClickListener oncilc = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction(KeyWordListFragment.this.mContext.getResources().getString(R.string.activity_article));
            intent.putExtra("keyword", v.getTag().toString());
            KeyWordListFragment.this.startActivity(intent);
        }
    };
    private String part_name;
    RelativeLayout.LayoutParams relal;

    public static BaseFragment<Listitem> newInstance(String type, String partType) {
        KeyWordListFragment tf = new KeyWordListFragment();
        tf.initType(type, partType);
        return tf;
    }

    public void findView() {
        super.findView();
        this.mLength = 20;
        this.mHead_Layout = new FrameLayout.LayoutParams(-1, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * HttpStatus.SC_BAD_REQUEST) / 480);
        int w = (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 220) / 480;
        int h = (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 154) / 480;
        this.mIcon_Layout = new LinearLayout.LayoutParams(w, h);
        this.mLa = new FrameLayout.LayoutParams(w, h);
        this.relal = new RelativeLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 164) / 480, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 120) / 480);
        this.relal.addRule(11);
        this.relal.rightMargin = 10;
        this.mListview.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
        try {
            this.part_name = DBHelper.getDBHelper().select("part_list", "part_name", "part_sa=?", new String[]{this.mOldtype});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public View getListItemview(View view, Listitem item, int position) {
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_keyword, (ViewGroup) null);
        }
        TextView title = (TextView) view.findViewById(R.id.listitem_title);
        TextView des = (TextView) view.findViewById(R.id.listitem_des);
        ((TextView) view.findViewById(R.id.listitem_date)).setText(item.u_date);
        if (DBHelper.getDBHelper().counts("readitem", "n_mark='" + item.n_mark + "' and read='true'") <= 0 || this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            title.setTextColor(-16777216);
        } else {
            title.setTextColor(this.mContext.getResources().getColor(R.color.readed));
            des.setTextColor(this.mContext.getResources().getColor(R.color.readed));
        }
        title.setText(item.title);
        des.setText(item.des);
        ImageView img = (ImageView) view.findViewById(R.id.listitem_music);
        if (item.other3.equals(UploadUtils.SUCCESS)) {
            img.setBackgroundResource(R.drawable.tupian_icon);
            img.setVisibility(0);
        } else if (!item.other3.equals(UploadUtils.FAILURE)) {
            img.setBackgroundResource(R.drawable.video_icon);
            img.setVisibility(0);
        } else if (item.other2.equals(UploadUtils.FAILURE)) {
            img.setBackgroundResource(R.drawable.music_icon);
            img.setVisibility(0);
        } else {
            img.setVisibility(8);
        }
        return view;
    }

    public void addListener() {
        super.addListener();
        if (this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            this.mListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                    final Listitem li = (Listitem) arg0.getItemAtPosition(arg2);
                    AlertDialog show = new AlertDialog.Builder(KeyWordListFragment.this.getActivity()).setMessage("您确认要删除本条记录吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            KeyWordListFragment.this.mlistAdapter.datas.remove(arg2 - KeyWordListFragment.this.mListview.getHeaderViewsCount());
                            KeyWordListFragment.this.mlistAdapter.notifyDataSetChanged();
                            DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{li.n_mark});
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                    return false;
                }
            });
        }
    }

    public boolean dealClick(Listitem item, int position) {
        if (this.mlistAdapter == null || this.mlistAdapter.datas.size() <= 0 || item == null) {
            return false;
        }
        Intent intent = new Intent();
        if (item.other3.equals(UploadUtils.SUCCESS)) {
            intent.setAction(this.mContext.getResources().getString(R.string.activity_pic));
        } else if (item.other3.equals(UploadUtils.FAILURE)) {
            intent.setAction(this.mContext.getResources().getString(R.string.activity_anim_article));
        } else {
            intent.setAction(this.mContext.getResources().getString(R.string.activity_anim_article));
            intent.putExtra("type", "2");
        }
        List<Listitem> list = new ArrayList<>();
        list.add(item);
        intent.putExtra("item", item);
        intent.putExtra("items", (Serializable) list);
        intent.putExtra("position", UploadUtils.SUCCESS);
        startActivity(intent);
        return true;
    }

    public View getListHeadview(Object obj, int type) {
        System.out.println(String.valueOf(type) + "===");
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        TextView textView = (TextView) this.headview.findViewById(R.id.head_title_des);
        ((ImageView) this.headview.findViewById(R.id.head_icon)).setLayoutParams(this.mHead_Layout);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText("========");
        return this.headview;
    }

    public View getListHeadview(Listitem item) {
        if (this.headview == null) {
            this.headview = LayoutInflater.from(this.mContext).inflate((int) R.layout.listhead, (ViewGroup) null);
        }
        ImageView iv = (ImageView) this.headview.findViewById(R.id.head_icon);
        ((TextView) this.headview.findViewById(R.id.head_title_des)).setText(item.des);
        iv.setLayoutParams(this.mHead_Layout);
        ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + item.icon, iv);
        ((TextView) this.headview.findViewById(R.id.head_title)).setText(item.title.trim());
        return this.headview;
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has(Constants.SINA_CODE) || jsonobj.getInt(Constants.SINA_CODE) != 0) {
            JSONArray jsonay = jsonobj.getJSONArray("list");
            if (jsonobj.has("head")) {
                try {
                    JSONArray ja = jsonobj.getJSONArray("head");
                    if (ja.length() == 1) {
                        JSONObject jsonhead = ja.getJSONObject(0);
                        Listitem head1 = new Listitem();
                        head1.nid = jsonhead.getString(LocaleUtil.INDONESIAN);
                        head1.title = jsonhead.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                        head1.des = jsonhead.getString("des");
                        head1.icon = jsonhead.getString("icon");
                        head1.u_date = jsonhead.getString("adddate");
                        head1.sa = String.valueOf(this.mOldtype) + "_" + this.part_name;
                        head1.getMark();
                        head1.ishead = "true";
                        data.obj = head1;
                        data.headtype = 0;
                    } else {
                        int count = ja.length();
                        List<Listitem> li = new ArrayList<>();
                        for (int i = 0; i < count; i++) {
                            JSONObject jsonhead2 = ja.getJSONObject(i);
                            Listitem head12 = new Listitem();
                            head12.nid = jsonhead2.getString(LocaleUtil.INDONESIAN);
                            head12.title = jsonhead2.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                            head12.des = jsonhead2.getString("des");
                            head12.icon = jsonhead2.getString("icon");
                            head12.u_date = jsonhead2.getString("adddate");
                            head12.sa = String.valueOf(this.mOldtype) + "_" + this.part_name;
                            head12.ishead = "true";
                            head12.getMark();
                            li.add(head12);
                        }
                        data.obj = li;
                        data.headtype = 2;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int count2 = jsonay.length();
            for (int i2 = 0; i2 < count2; i2++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i2);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                o.title = obj.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                o.sa = String.valueOf(this.mOldtype) + "_" + this.part_name;
                try {
                    if (obj.has("des")) {
                        o.des = obj.getString("des");
                    }
                    if (obj.has("adddate")) {
                        o.u_date = obj.getString("adddate");
                    }
                    if (obj.has("author")) {
                        o.author = obj.getString("author");
                    }
                    if (obj.has("keyword")) {
                        o.other = obj.getString("keyword");
                    }
                    if (obj.has("sugfrom")) {
                        o.other3 = obj.getString("sugfrom");
                    }
                    if (obj.has("mp3")) {
                        o.other2 = obj.getString("mp3");
                    }
                    o.icon = obj.getString("icon");
                } catch (Exception e2) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }
        throw new DataException(jsonobj.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG));
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count);
        }
        System.out.println(String.valueOf(url) + " " + oldtype);
        String json = DNDataSource.list_FromNET(String.valueOf(Urls.app_api) + "?action=searchbq&keyword=", oldtype, page, count, parttype, isfirst);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }
}
