package com.city_life.sliding.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.pengyou.citycommercialarea.R;

public class HelpFragment extends Fragment {
    int icon_url;
    ImageView img;
    public RelativeLayout mContainers;
    public Context mContext;
    public View mMain_layout;

    public HelpFragment() {
        this(0);
    }

    public HelpFragment(int iconurl) {
        this.icon_url = iconurl;
        setRetainInstance(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.icon_url = savedInstanceState.getInt("icon_url");
        }
        this.mContext = inflater.getContext();
        if (this.mMain_layout == null) {
            this.mContainers = new RelativeLayout(this.mContext);
            this.mMain_layout = inflater.inflate((int) R.layout.listitem_help, (ViewGroup) null);
            initListFragment(inflater);
            this.mContainers.addView(this.mMain_layout);
        } else {
            this.mContainers.removeAllViews();
            this.mContainers = new RelativeLayout(getActivity());
            this.mContainers.addView(this.mMain_layout);
        }
        return this.mContainers;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("icon_url", this.icon_url);
    }

    public void initListFragment(LayoutInflater inflater) {
        this.img = (ImageView) this.mMain_layout.findViewById(R.id.listitem_help_icon);
        this.img.setBackgroundResource(this.icon_url);
        this.img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HelpFragment.this.getActivity().finish();
            }
        });
    }
}
