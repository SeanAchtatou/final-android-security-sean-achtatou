package com.city_life.sliding.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.city_life.setting.FavActivity;
import com.city_life.setting.FeedBack;
import com.city_life.setting.ST_ShareManager;
import com.city_life.setting.TextSize;
import com.city_life.setting.Version;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.utils.FileUtils;
import com.utils.PerfHelper;
import java.io.File;

public class SettingMenuFragment extends ListFragment {
    menu_adapter menu = null;
    long size = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate((int) R.layout.list_setting, (ViewGroup) null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.size = 0;
        getFilesInfo(FileUtils.sdPath);
        this.menu = new menu_adapter();
        setListAdapter(this.menu);
    }

    public void onResume() {
        super.onResume();
        if (this.menu != null) {
            this.menu.notifyDataSetChanged();
        }
    }

    class menu_adapter extends BaseAdapter {
        menu_adapter() {
        }

        public int getCount() {
            return 7;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public boolean isEnabled(int position) {
            return super.isEnabled(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(SettingMenuFragment.this.getActivity()).inflate((int) R.layout.listitem_setting_menu, (ViewGroup) null);
            }
            final TextView tag = (TextView) convertView.findViewById(R.id.listitem_item_tag);
            ImageView icon = (ImageView) convertView.findViewById(R.id.listitem_icon);
            ImageView tags = (ImageView) convertView.findViewById(R.id.listitem_item_tag_s);
            TextView title = (TextView) convertView.findViewById(R.id.menu_title);
            switch (position) {
                case 0:
                    tag.setVisibility(8);
                    tags.setVisibility(0);
                    icon.setBackgroundResource(R.drawable.st_text_icon);
                    title.setText("文章字号");
                    break;
                case 1:
                    tag.setVisibility(8);
                    tags.setVisibility(0);
                    icon.setBackgroundResource(R.drawable.st_share_icon);
                    title.setText("分享设置");
                    break;
                case 2:
                    tag.setVisibility(8);
                    tags.setVisibility(0);
                    icon.setBackgroundResource(R.drawable.st_about_icon);
                    title.setText("关于我们");
                    break;
                case 3:
                    tag.setVisibility(8);
                    tags.setVisibility(0);
                    icon.setBackgroundResource(R.drawable.st_fav_icon);
                    title.setText("我的收藏");
                    break;
                case 4:
                    tag.setVisibility(8);
                    tags.setVisibility(0);
                    icon.setBackgroundResource(R.drawable.st_feedback_icon);
                    title.setText("意见反馈");
                    break;
                case 5:
                    tag.setVisibility(0);
                    tags.setVisibility(8);
                    icon.setBackgroundResource(R.drawable.st_clear_icon);
                    title.setText("清除缓存");
                    tag.setText(FileUtils.formatFileSize(SettingMenuFragment.this.size));
                    tag.setBackgroundDrawable(null);
                    break;
                case 6:
                    tag.setVisibility(0);
                    tags.setVisibility(8);
                    if (PerfHelper.getBooleanData(PerfHelper.P_PUSH)) {
                        tag.setBackgroundResource(R.drawable.bind_tuijian_open);
                    } else {
                        tag.setBackgroundResource(R.drawable.bind_tuijian_close);
                    }
                    icon.setBackgroundResource(R.drawable.st_push_icon);
                    title.setText("推送通知");
                    tag.setOnClickListener(new View.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
                         arg types: [java.lang.String, int]
                         candidates:
                          com.utils.PerfHelper.setInfo(java.lang.String, int):void
                          com.utils.PerfHelper.setInfo(java.lang.String, long):void
                          com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
                          com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
                        public void onClick(View v) {
                            if (PerfHelper.getBooleanData(PerfHelper.P_PUSH)) {
                                tag.setBackgroundResource(R.drawable.bind_tuijian_close);
                                PerfHelper.setInfo(PerfHelper.P_PUSH, false);
                                return;
                            }
                            tag.setBackgroundResource(R.drawable.bind_tuijian_open);
                            PerfHelper.setInfo(PerfHelper.P_PUSH, true);
                        }
                    });
                    break;
            }
            return convertView;
        }
    }

    public void onListItemClick(ListView lv, View v, int position, long id) {
        Intent intent = null;
        switch (position) {
            case 0:
                intent = new Intent();
                intent.setClass(getActivity(), TextSize.class);
                break;
            case 1:
                intent = new Intent();
                intent.setClass(getActivity(), ST_ShareManager.class);
                break;
            case 2:
                intent = new Intent();
                intent.setClass(getActivity(), Version.class);
                break;
            case 3:
                intent = new Intent();
                intent.setClass(getActivity(), FavActivity.class);
                break;
            case 4:
                intent = new Intent();
                intent.setClass(getActivity(), FeedBack.class);
                break;
            case 5:
                addlist(v);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    public void addlist(final View tag) {
        tag.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder message = new AlertDialog.Builder(SettingMenuFragment.this.getActivity()).setMessage("是否清除缓存？");
                final View view = tag;
                message.setPositiveButton("是", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.showProcessDialog(SettingMenuFragment.this.getActivity(), "正在清除缓存...");
                        final View view = view;
                        new Thread() {
                            public void run() {
                                ShareApplication.mImageWorker.mImageCache.clearCaches();
                                SettingMenuFragment.this.deleteFilesInfo(FileUtils.sdPath);
                                DBHelper.getDBHelper().deleteall(SettingMenuFragment.this.getActivity().getResources().getStringArray(R.array.app_sql_delete));
                                Utils.dismissProcessDialog();
                                Utils.showToast("缓存清理完毕");
                                Handler handler = Utils.h;
                                final View view = view;
                                handler.post(new Runnable() {
                                    public void run() {
                                        ((TextView) view.findViewById(R.id.listitem_item_tag)).setText("0kb");
                                    }
                                });
                            }
                        }.start();
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
            }
        });
    }

    private void getFilesInfo(String str) {
        File f = new File(str);
        if (f.listFiles() != null) {
            for (File file : f.listFiles()) {
                if (file.isDirectory()) {
                    getFilesInfo(file.getAbsolutePath());
                } else {
                    this.size += file.length();
                }
            }
        }
    }

    public void deleteFilesInfo(String str) {
        File f = new File(str);
        if (f.listFiles() != null) {
            for (File file : f.listFiles()) {
                if (file.isDirectory()) {
                    deleteFilesInfo(file.getAbsolutePath());
                } else {
                    file.delete();
                }
            }
        }
    }
}
