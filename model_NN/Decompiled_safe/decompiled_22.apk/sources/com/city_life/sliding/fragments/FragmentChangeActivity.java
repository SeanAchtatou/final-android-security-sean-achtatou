package com.city_life.sliding.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.city_life.ui.YCurrentHomeActivity;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.apptime.AppTimeStatisticsService;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;

public class FragmentChangeActivity extends YCurrentHomeActivity implements SearchView.OnQueryTextListener {
    ActionBar actionBar;
    Dialog dialog;
    private Fragment mContent;
    private Fragment oldmContent;
    private int oldmContent_pos;
    int oldposition = 0;
    SearchView search;
    SearchListFragment searchContent = null;
    boolean show_search = false;
    TextView title;
    String type = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSlidingActionBarEnabled(true);
        if (savedInstanceState != null) {
            this.mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
        }
        if (this.mContent == null) {
            this.mContent = RedianListFragment.newInstance("xwlist", "xwlist");
        }
        setContentView((int) R.layout.content_frame);
        this.actionBar = getSupportActionBar();
        this.actionBar.setLogo((int) R.drawable.setting_btn_n);
        this.actionBar.setIcon((int) R.drawable.setting_btn_n);
        this.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.banner));
        this.actionBar.setDisplayOptions(4, 4);
        View actionbar_title = LayoutInflater.from(this).inflate((int) R.layout.title_row, (ViewGroup) null);
        this.title = (TextView) actionbar_title.findViewById(R.id.row_title);
        this.actionBar.setCustomView(actionbar_title, new ActionBar.LayoutParams(-2, -1, 17));
        this.actionBar.setDisplayShowCustomEnabled(true);
        this.actionBar.setDisplayHomeAsUpEnabled(true);
        this.actionBar.setDisplayShowHomeEnabled(true);
        this.actionBar.setDisplayUseLogoEnabled(true);
        getSupportFragmentManager().beginTransaction().replace(R.id.mainroot, this.mContent).commit();
        setBehindContentView((int) R.layout.menu_frame);
        getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, new ColorMenuFragment()).commit();
        getSlidingMenu().setTouchModeAbove(1);
        Utils.h.postDelayed(new Runnable() {
            public void run() {
                new ClientShowAd().showAdPOP_UP(FragmentChangeActivity.this, 4, "");
            }
        }, 2000);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.show_search) {
            getSupportMenuInflater().inflate(R.menu.options, menu);
            this.search = (SearchView) menu.findItem(R.id.menu_add_search).getActionView();
            this.search.setOnQueryTextListener(this);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                if (this.oldposition == 6) {
                    switchContent(this.oldmContent, this.oldmContent_pos);
                } else {
                    toggle();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "mContent", this.mContent);
    }

    public void switchContent(final Fragment fragment, final int position) {
        this.mContent = fragment;
        getSlidingMenu().showContent();
        if (this.oldposition != position) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    FragmentTransaction ft = FragmentChangeActivity.this.getSupportFragmentManager().beginTransaction();
                    if (position == 6 || FragmentChangeActivity.this.oldposition == 6) {
                        if (position == 6) {
                            ft.setCustomAnimations(R.anim.init_in, R.anim.init_out);
                        } else {
                            ft.setCustomAnimations(R.animator.slide_right_in, R.animator.slide_right_out);
                            FragmentChangeActivity.this.searchContent = null;
                        }
                    } else if (position > FragmentChangeActivity.this.oldposition) {
                        ft.setCustomAnimations(R.animator.slide_shu_out, R.animator.slide_shu_in);
                    } else {
                        ft.setCustomAnimations(R.animator.slide_xia_out, R.animator.slide_xia_in);
                    }
                    FragmentChangeActivity.this.oldposition = position;
                    ft.replace(R.id.mainroot, fragment).commit();
                    switch (position) {
                        case 0:
                            FragmentChangeActivity.this.title.setText((int) R.string.titlte_news);
                            FragmentChangeActivity.this.show_search = false;
                            FragmentChangeActivity.this.invalidateOptionsMenu();
                            return;
                        case 1:
                            FragmentChangeActivity.this.title.setText((int) R.string.titlte_daogou);
                            FragmentChangeActivity.this.show_search = false;
                            FragmentChangeActivity.this.invalidateOptionsMenu();
                            return;
                        case 2:
                            FragmentChangeActivity.this.title.setText((int) R.string.titlte_wjq);
                            FragmentChangeActivity.this.show_search = true;
                            FragmentChangeActivity.this.invalidateOptionsMenu();
                            return;
                        case 3:
                            FragmentChangeActivity.this.title.setText((int) R.string.titlte_fangan);
                            FragmentChangeActivity.this.show_search = true;
                            FragmentChangeActivity.this.invalidateOptionsMenu();
                            return;
                        case 4:
                            FragmentChangeActivity.this.title.setText((int) R.string.titlte_weibo);
                            FragmentChangeActivity.this.show_search = false;
                            FragmentChangeActivity.this.invalidateOptionsMenu();
                            return;
                        case 5:
                            FragmentChangeActivity.this.title.setText((int) R.string.titlte_setting);
                            FragmentChangeActivity.this.show_search = false;
                            FragmentChangeActivity.this.invalidateOptionsMenu();
                            return;
                        case 6:
                            FragmentChangeActivity.this.title.setText((int) R.string.titlte_search);
                            FragmentChangeActivity.this.show_search = true;
                            FragmentChangeActivity.this.invalidateOptionsMenu();
                            return;
                        default:
                            return;
                    }
                }
            }, 300);
        }
    }

    public void collapseSoftInputMethod() {
        ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(1, 2);
    }

    public boolean onQueryTextSubmit(String query) {
        switch (this.oldposition) {
            case 2:
                this.type = "wjqlist";
                break;
            case 3:
                this.type = "falist";
                break;
        }
        collapseSoftInputMethod();
        this.search.setClose();
        if (this.searchContent == null) {
            this.searchContent = (SearchListFragment) SearchListFragment.newInstance(this.type, query);
            this.oldmContent = this.mContent;
            this.oldmContent_pos = this.oldposition;
            switchContent(this.searchContent, 6);
            return true;
        }
        this.searchContent.setRefresh(this.type, query);
        return true;
    }

    public boolean onQueryTextChange(String newText) {
        return true;
    }

    public void things(View view) {
        if (view.getId() == R.id.wb_myimageview_img || view.getId() == R.id.wb_retweeted__myimageview_img) {
            View _view = LayoutInflater.from(this).inflate((int) R.layout.image, (ViewGroup) null);
            ImageView image = (ImageView) _view.findViewById(R.id.imageview);
            this.dialog = new Dialog(this, R.style.c_dialog);
            this.dialog.setContentView(_view, new ViewGroup.LayoutParams(-2, -2));
            this.dialog.show();
            image.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    FragmentChangeActivity.this.dialog.dismiss();
                }
            });
            ShareApplication.mImageWorker.loadImage(view.getTag(), image);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || getSlidingMenu().isMenuShowing()) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.oldposition == 6) {
            switchContent(this.oldmContent, this.oldmContent_pos);
            return false;
        }
        new AlertDialog.Builder(this).setTitle((int) R.string.exit).setMessage(getString(R.string.exit_message, new String[]{getString(R.string.app_name)})).setPositiveButton((int) R.string.done, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                FragmentChangeActivity.this.sendBroadcast(new Intent(BaseActivity.ACTIVITY_FINSH));
                FragmentChangeActivity.this.stopService(new Intent(FragmentChangeActivity.this, AppTimeStatisticsService.class));
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        }).show();
        return false;
    }
}
