package com.city_life.sliding.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.palmtrends.wb.WeiboFragment;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class ColorMenuFragment extends ListFragment {
    int header_height = 0;
    int statusBarHeight = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate((int) R.layout.list, (ViewGroup) null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new ArrayAdapter(getActivity(), 17367043, 16908308, getResources().getStringArray(R.array.color_names));
        View header = LayoutInflater.from(getActivity()).inflate((int) R.layout.menu_header, (ViewGroup) null);
        this.header_height = (PerfHelper.getIntData(PerfHelper.P_PHONE_H) * 250) / 960;
        header.setLayoutParams(new AbsListView.LayoutParams(-1, this.header_height));
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(new Rect());
        try {
            Class<?> c = Class.forName("com.android.internal.R$dimen");
            this.statusBarHeight = getResources().getDimensionPixelSize(Integer.parseInt(c.getField("status_bar_height").get(c.newInstance()).toString()));
        } catch (Exception e1) {
            this.statusBarHeight = 38;
            e1.printStackTrace();
        }
        System.out.println("状态栏高度:" + this.statusBarHeight);
        getListView().addHeaderView(header, null, false);
        setListAdapter(new menu_adapter());
    }

    class menu_adapter extends BaseAdapter {
        LinearLayout.LayoutParams lis;

        menu_adapter() {
        }

        public int getCount() {
            return 6;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public boolean isEnabled(int position) {
            return super.isEnabled(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(ColorMenuFragment.this.getActivity()).inflate((int) R.layout.listitem_menu, (ViewGroup) null);
            }
            if (PerfHelper.getIntData(PerfHelper.P_PHONE_H) > 1100) {
                this.lis = new LinearLayout.LayoutParams(-1, ((PerfHelper.getIntData(PerfHelper.P_PHONE_H) - ColorMenuFragment.this.header_height) - (ColorMenuFragment.this.statusBarHeight + 13)) / 6);
            } else {
                this.lis = new LinearLayout.LayoutParams(-1, ((PerfHelper.getIntData(PerfHelper.P_PHONE_H) - ColorMenuFragment.this.header_height) - (ColorMenuFragment.this.statusBarHeight + 4)) / 6);
            }
            View vies = convertView.findViewById(R.id.setting_list_views);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.listitem_item_tag);
            ImageView imageView2 = (ImageView) convertView.findViewById(R.id.listitem_icon);
            TextView title = (TextView) convertView.findViewById(R.id.menu_title);
            switch (position) {
                case 0:
                    title.setText("新闻");
                    vies.setLayoutParams(this.lis);
                    break;
                case 1:
                    title.setText("导购");
                    vies.setLayoutParams(this.lis);
                    break;
                case 2:
                    title.setText("微技巧");
                    vies.setLayoutParams(this.lis);
                    break;
                case 3:
                    title.setText("方案");
                    vies.setLayoutParams(this.lis);
                    break;
                case 4:
                    title.setText("微博");
                    vies.setLayoutParams(this.lis);
                    break;
                case 5:
                    title.setText("设置");
                    vies.setLayoutParams(this.lis);
                    break;
            }
            return convertView;
        }
    }

    public void onListItemClick(ListView lv, View v, int position, long id) {
        Fragment newContent = null;
        switch (position - 1) {
            case 0:
                newContent = RedianListFragment.newInstance("xwlist", "xwlist");
                break;
            case 1:
                newContent = TupianListFragment.newInstance("dglist", "dglist");
                break;
            case 2:
                newContent = RedianListFragment.newInstance("wjqlist", "wjqlist");
                break;
            case 3:
                newContent = RedianListFragment.newInstance("falist", "falist");
                break;
            case 4:
                newContent = WeiboFragment.newInstance("1727471254", "weibo");
                break;
            case 5:
                newContent = new SettingMenuFragment();
                break;
        }
        if (newContent != null) {
            switchFragment(newContent, position - 1);
        }
    }

    private void switchFragment(Fragment fragment, int position) {
        if (getActivity() != null && (getActivity() instanceof FragmentChangeActivity)) {
            ((FragmentChangeActivity) getActivity()).switchContent(fragment, position);
        }
    }
}
