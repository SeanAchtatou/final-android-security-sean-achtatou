package com.city_life.sliding;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class Util {
    public static void goToGitHub(Context context) {
        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://github.com/jfeinstein10/slidingmenu")));
    }
}
