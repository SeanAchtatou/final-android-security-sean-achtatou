package com.city_life.sliding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import com.city_life.fragment.SettingFragment;
import com.pengyou.citycommercialarea.R;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingFragmentActivity;
import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends SlidingFragmentActivity {
    protected Fragment mFrag;
    private int mTitleRes;

    public BaseActivity(int titleRes) {
        this.mTitleRes = titleRes;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBehindContentView((int) R.layout.menu_frame);
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        this.mFrag = SettingFragment.newInstance("x");
        t.replace(R.id.menu_frame, this.mFrag);
        t.commit();
        SlidingMenu sm = getSlidingMenu();
        sm.setShadowWidthRes(R.dimen.shadow_width);
        sm.setShadowDrawable((int) R.drawable.shadow);
        sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        sm.setFadeDegree(0.35f);
        sm.setTouchModeAbove(1);
    }

    public class BasePagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragments = new ArrayList();
        private ViewPager mPager;

        public BasePagerAdapter(FragmentManager fm, ViewPager vp) {
            super(fm);
            this.mPager = vp;
            this.mPager.setAdapter(this);
            for (int i = 0; i < 3; i++) {
                addTab(SettingFragment.newInstance("x"));
            }
        }

        public void addTab(Fragment frag) {
            this.mFragments.add(frag);
        }

        public Fragment getItem(int position) {
            return this.mFragments.get(position);
        }

        public int getCount() {
            return this.mFragments.size();
        }
    }
}
