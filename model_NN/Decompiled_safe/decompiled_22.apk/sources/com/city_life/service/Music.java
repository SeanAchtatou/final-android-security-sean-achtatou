package com.city_life.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import org.apache.commons.httpclient.HttpStatus;

public class Music extends Service implements MediaPlayer.OnCompletionListener {
    public static int PAUSE = HttpStatus.SC_PROCESSING;
    public static int PLAY_MAG = HttpStatus.SC_SWITCHING_PROTOCOLS;
    private static boolean isLoop = false;
    public static MediaPlayer mMediaPlayer = null;
    private int MSG;
    private String path = "";

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnCompletionListener(this);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        System.out.println("service onDestroy");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        this.MSG = intent.getIntExtra("MSG", 1);
        System.out.println("��ַ0:" + this.MSG);
        if (this.MSG == PLAY_MAG) {
            if (!this.path.endsWith(".mp3")) {
                System.out.println("��ַ1:" + this.path);
                this.path = intent.getStringExtra("path");
                System.out.println("��ַ2:" + this.path);
                playMusic(this.path);
            } else if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            } else {
                mMediaPlayer.start();
            }
        }
        if (this.MSG == PAUSE) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            } else {
                mMediaPlayer.start();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public void playMusic(String path2) {
        try {
            System.out.println("��ʼ����");
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(path2);
            mMediaPlayer.prepareAsync();
            mMediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCompletion(MediaPlayer mp) {
        mMediaPlayer.start();
    }
}
