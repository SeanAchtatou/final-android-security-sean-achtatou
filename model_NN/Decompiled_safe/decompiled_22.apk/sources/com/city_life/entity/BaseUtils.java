package com.city_life.entity;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseUtils {
    public static boolean isMobileNO(String mobiles) {
        Matcher m = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$").matcher(mobiles);
        Log.i("BaseUtils-->isMobileNO-->", new StringBuilder(String.valueOf(m.matches())).toString());
        return m.matches();
    }

    public static Bitmap getBitmapFromView(View view) {
        view.destroyDrawingCache();
        view.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.setDrawingCacheEnabled(true);
        return view.getDrawingCache(true);
    }
}
