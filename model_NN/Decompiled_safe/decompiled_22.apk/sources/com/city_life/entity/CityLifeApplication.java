package com.city_life.entity;

import android.content.Context;
import android.widget.Toast;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKGeneralListener;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.entity.Listitem;
import java.util.ArrayList;

public class CityLifeApplication extends ShareApplication {
    public static ArrayList<Listitem> fav_list_id = null;
    private static CityLifeApplication mInstance = null;
    public static final String strKey = "7947C3E7568E17E2AC0077232A6AD79188A2F4BA";
    public BMapManager mBMapManager = null;
    public boolean m_bKeyRight = true;

    public void initEngineManager(Context context) {
        if (this.mBMapManager == null) {
            this.mBMapManager = new BMapManager(context);
        }
        if (!this.mBMapManager.init(strKey, new MyGeneralListener())) {
            Toast.makeText(getInstance().getApplicationContext(), "BMapManager  初始化错误!", 1).show();
        }
    }

    public void onCreate() {
        super.onCreate();
        mInstance = this;
        initEngineManager(this);
    }

    public static CityLifeApplication getInstance() {
        return mInstance;
    }

    public static class MyGeneralListener implements MKGeneralListener {
        public void onGetNetworkState(int iError) {
            if (iError == 2) {
                Toast.makeText(CityLifeApplication.getInstance().getApplicationContext(), "您的网络出错啦！", 1).show();
            } else if (iError == 3) {
                Toast.makeText(CityLifeApplication.getInstance().getApplicationContext(), "输入正确的检索条件！", 1).show();
            }
        }

        public void onGetPermissionState(int iError) {
            if (iError == 300) {
                Toast.makeText(CityLifeApplication.getInstance().getApplicationContext(), "请在 DemoApplication.java文件输入正确的授权Key！", 1).show();
                CityLifeApplication.getInstance().m_bKeyRight = false;
            }
        }
    }
}
