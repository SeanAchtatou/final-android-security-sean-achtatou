package com.city_life.entity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.ClientInfo;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.JsonDao;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.part;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.FinalVariable;
import com.utils.JniUtils;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class CityLifeClientInfo extends ClientInfo {
    public static int mFooter_limit = mLength;
    public static int mLength = 20;
    public static String mOldtype = "part_list";
    public static int mPage = 1;
    public static String mParttype = "part_list";

    public static String getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        String citylistjson = DNDataSource.CityLift_list_FromNET(url, oldtype, page, count, parttype, isfirst);
        if (ShareApplication.debug) {
            System.out.println("城市列表返回:" + citylistjson);
        }
        return citylistjson;
    }

    public static void initparts_bianming(String json, String parttype) throws Exception {
        DBHelper db = DBHelper.getDBHelper();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has("responseCode") || jsonobj.getInt("responseCode") == 0) {
            List<part> design_patrs = new ArrayList<>();
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                part o = new part();
                JSONObject obj = jsonay.getJSONObject(i);
                o.part_type = String.valueOf(parttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No);
                o.part_index = Integer.valueOf(i);
                o.part_sa = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has(Constants.SINA_NAME)) {
                        o.part_name = obj.getString(Constants.SINA_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                design_patrs.add(o);
            }
            db.insert(design_patrs, "part_list", part.class);
        }
    }

    public static void initparts_nearby(String json, String parttype) throws Exception {
        DBHelper db = DBHelper.getDBHelper();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has("responseCode") || jsonobj.getInt("responseCode") == 0) {
            List<part> design_patrs = new ArrayList<>();
            String time = String.valueOf(PerfHelper.getStringData("nearbytime")) + PerfHelper.getStringData(PerfHelper.P_CITY_No);
            if (jsonobj.has("areaName")) {
                if (!time.equals(jsonobj.getString("lastTime"))) {
                    PerfHelper.setInfo("nearbytime" + PerfHelper.getStringData(PerfHelper.P_CITY_No), jsonobj.getString("areaName"));
                } else {
                    return;
                }
            }
            JSONArray jsonay = jsonobj.getJSONArray("area");
            int count = jsonay.length();
            part o1 = new part();
            o1.part_type = String.valueOf(parttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No);
            o1.part_index = 0;
            o1.part_sa = UploadUtils.FAILURE;
            o1.part_name = "全部";
            design_patrs.add(o1);
            for (int i = 0; i < count; i++) {
                part o = new part();
                JSONObject obj = jsonay.getJSONObject(i);
                o.part_type = String.valueOf(parttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No);
                o.part_index = Integer.valueOf(i + 1);
                o.part_sa = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has("areaName")) {
                        o.part_name = obj.getString("areaName");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                design_patrs.add(o);
            }
            db.insert(design_patrs, "part_list", part.class);
        }
    }

    public static void citylife_check_update(final Context context) {
        new Thread() {
            public void run() {
                try {
                    final ClientInfo.Version v = CityLifeClientInfo.citylife_check_update(FinalVariable.pid);
                    if (v != null && UploadUtils.FAILURE.equals(v.available)) {
                        Handler handler = Utils.h;
                        final Context context = context;
                        handler.post(new Runnable() {
                            public void run() {
                                if (UploadUtils.SUCCESS.equals(v.force)) {
                                    AlertDialog.Builder negativeButton = new AlertDialog.Builder(context).setMessage(v.alert).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    final ClientInfo.Version version = v;
                                    final Context context = context;
                                    negativeButton.setPositiveButton((int) R.string.checkupdate_getnew, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (!version.update_url.startsWith("http://")) {
                                                Utils.showToast("参数错误");
                                                return;
                                            }
                                            try {
                                                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(version.update_url)));
                                            } catch (Exception e) {
                                                Utils.showToast("请安装浏览起");
                                                e.printStackTrace();
                                            }
                                        }
                                    }).show();
                                    return;
                                }
                                AlertDialog.Builder message = new AlertDialog.Builder(context).setMessage(v.alert);
                                final ClientInfo.Version version2 = v;
                                final Context context2 = context;
                                message.setNegativeButton((int) R.string.checkupdate_getnew, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (!version2.update_url.startsWith("http://")) {
                                            Utils.showToast("参数错误");
                                            return;
                                        }
                                        try {
                                            context2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(version2.update_url)));
                                        } catch (Exception e) {
                                            Utils.showToast("请安装浏览起");
                                            e.printStackTrace();
                                        }
                                    }
                                }).show();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static ClientInfo.Version citylife_check_update(String pid) throws Exception {
        List<NameValuePair> param = new ArrayList<>();
        String version = ShareApplication.getVersion();
        param.add(new BasicNameValuePair("versionNum", ShareApplication.getVersion()));
        param.add(new BasicNameValuePair("appType", "2"));
        param.add(new BasicNameValuePair("mobile", Build.MODEL));
        param.add(new BasicNameValuePair("e", JniUtils.getkey()));
        return (ClientInfo.Version) JsonDao.getJsonObject(ShareApplication.share.getResources().getString(R.string.citylife_checkVersion_list_url), param, ClientInfo.Version.class);
    }
}
