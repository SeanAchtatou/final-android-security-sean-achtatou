package com.city_life.artivleactivity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.actionbarsherlock.app.ActionBar;
import com.imofan.android.basic.Mofang;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.loadimage.ImageCache;
import com.palmtrends.loadimage.ImageFetcher;
import com.tencent.tauth.Constants;

public abstract class BaseFragmentActivity extends FragmentActivity {
    public ActionBar actionBar;
    public View actionbar_title;
    public boolean isNightMode = false;

    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        requestWindowFeature(1);
        if (ShareApplication.mImageWorker == null) {
            ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(ShareApplication.IMAGE_CACHE_DIR);
            ImageFetcher mImageWorker = new ImageFetcher(this, 800);
            mImageWorker.setImageCache(ImageCache.findOrCreateCache(this, cacheParams));
            ShareApplication.mImageWorker = mImageWorker;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Mofang.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (getIntent().getStringExtra(Constants.PARAM_TITLE) != null) {
            Mofang.onResume(this, getIntent().getStringExtra(Constants.PARAM_TITLE));
        } else {
            Mofang.onResume(this);
        }
    }
}
