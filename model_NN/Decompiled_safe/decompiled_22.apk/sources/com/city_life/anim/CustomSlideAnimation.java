package com.city_life.anim;

import android.graphics.Canvas;
import android.view.animation.Interpolator;
import com.pengyou.citycommercialarea.R;
import com.slidingmenu.lib.SlidingMenu;

public class CustomSlideAnimation extends CustomAnimation {
    /* access modifiers changed from: private */
    public static Interpolator interp = new Interpolator() {
        public float getInterpolation(float t) {
            float t2 = t - 1.0f;
            return (t2 * t2 * t2) + 1.0f;
        }
    };

    public CustomSlideAnimation() {
        super(R.string.anim_slide, new SlidingMenu.CanvasTransformer() {
            public void transformCanvas(Canvas canvas, float percentOpen) {
                canvas.translate(0.0f, ((float) canvas.getHeight()) * (1.0f - CustomSlideAnimation.interp.getInterpolation(percentOpen)));
            }
        });
    }
}
