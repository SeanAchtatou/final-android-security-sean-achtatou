package com.city_life.anim;

import android.graphics.Canvas;
import com.pengyou.citycommercialarea.R;
import com.slidingmenu.lib.SlidingMenu;

public class CustomZoomAnimation extends CustomAnimation {
    public CustomZoomAnimation() {
        super(R.string.anim_zoom, new SlidingMenu.CanvasTransformer() {
            public void transformCanvas(Canvas canvas, float percentOpen) {
                float scale = (float) ((((double) percentOpen) * 0.25d) + 0.75d);
                canvas.scale(scale, scale, (float) (canvas.getWidth() / 2), (float) (canvas.getHeight() / 2));
            }
        });
    }
}
