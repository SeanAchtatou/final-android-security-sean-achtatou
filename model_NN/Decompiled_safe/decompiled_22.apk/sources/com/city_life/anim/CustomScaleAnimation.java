package com.city_life.anim;

import android.graphics.Canvas;
import com.pengyou.citycommercialarea.R;
import com.slidingmenu.lib.SlidingMenu;

public class CustomScaleAnimation extends CustomAnimation {
    public CustomScaleAnimation() {
        super(R.string.anim_scale, new SlidingMenu.CanvasTransformer() {
            public void transformCanvas(Canvas canvas, float percentOpen) {
                canvas.scale(percentOpen, 1.0f, 0.0f, 0.0f);
            }
        });
    }
}
