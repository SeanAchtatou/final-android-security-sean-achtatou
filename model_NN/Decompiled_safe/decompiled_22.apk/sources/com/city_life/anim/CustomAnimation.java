package com.city_life.anim;

import android.os.Bundle;
import com.city_life.sliding.BaseActivity;
import com.city_life.sliding.SampleListFragment;
import com.pengyou.citycommercialarea.R;
import com.slidingmenu.lib.SlidingMenu;

public abstract class CustomAnimation extends BaseActivity {
    private SlidingMenu.CanvasTransformer mTransformer;

    public CustomAnimation(int titleRes, SlidingMenu.CanvasTransformer transformer) {
        super(titleRes);
        this.mTransformer = transformer;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.content_frame);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new SampleListFragment()).commit();
        SlidingMenu sm = getSlidingMenu();
        setSlidingActionBarEnabled(true);
        sm.setBehindScrollScale(0.0f);
        sm.setBehindCanvasTransformer(this.mTransformer);
    }
}
