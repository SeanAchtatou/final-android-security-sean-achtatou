package com.city_life.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import com.city_life.fragment.WQListFragment;
import com.palmtrends.baseui.BaseActivity;
import com.pengyou.citycommercialarea.R;

public class FMActivity extends BaseActivity {
    Fragment frag = null;
    Fragment m_list_frag_1 = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_part);
        initFragment();
    }

    public void initFragment() {
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(getIntent().getStringExtra("data"));
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (WQListFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = WQListFragment.newInstance("rd", "rd");
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, "2131230808");
        fragmentTransaction.commit();
    }

    public void things(View view) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
