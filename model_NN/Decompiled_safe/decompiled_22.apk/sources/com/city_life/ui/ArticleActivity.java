package com.city_life.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;
import com.city_life.part_asynctask.UploadUtils;
import com.city_life.view.PathButton;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.AraticleFragment;
import com.palmtrends.baseui.BaseArticleActivity;
import com.palmtrends.baseview.ImageDetailViewPager;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.MySSLSocketFactory;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import sina_weibo.WeiboShareDao;

public class ArticleActivity extends BaseArticleActivity implements ImageDetailViewPager.OnViewListener, PathButton.onCheckedId {
    TranslateAnimation botton_ta_in = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
    TranslateAnimation botton_ta_out = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
    View fav_btn;
    Fragment frag = null;
    List<Listitem> items;
    AraticleFragment mCurrentFrag = null;
    Listitem mCurrentItem;
    private PathButton pathButton;
    int position = 0;
    TranslateAnimation top_ta_in = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
    TranslateAnimation top_ta_out = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
    String total = UploadUtils.SUCCESS;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_article);
        initFragment();
        Utils.h.postDelayed(new Runnable() {
            public void run() {
                new ClientShowAd().showAdPOP_UP(ArticleActivity.this, 6, "");
                new ClientShowAd().showAdFIXED_Out(ArticleActivity.this, 5, "");
            }
        }, 4000);
    }

    public void initFragment() {
        this.top_ta_in.setDuration(400);
        this.top_ta_out.setDuration(400);
        this.botton_ta_in.setDuration(400);
        this.botton_ta_out.setDuration(400);
        this.position = getIntent().getIntExtra("position", 0);
        this.items = (List) getIntent().getSerializableExtra("items");
        ShareApplication.items = this.items;
        this.mCurrentItem = this.items.get(this.position);
        Listitem aditem = new Listitem();
        aditem.isad = "true";
        if (this.items.remove(aditem) && this.position != 0) {
            this.position--;
        }
        Fragment findresult = getSupportFragmentManager().findFragmentByTag("article_activity");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.mCurrentFrag = (AraticleFragment) findresult;
        }
        if (this.mCurrentFrag == null) {
            this.mCurrentFrag = AraticleFragment.newInstance(this.position, null, this);
            this.mCurrentFrag.setMlistener(this);
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.mCurrentFrag;
        fragmentTransaction.add(R.id.part_content, this.frag, "article_activity");
        fragmentTransaction.commit();
        this.fav_btn = findViewById(R.id.title_fav);
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            case R.id.title_comment /*2131230803*/:
                Intent intent = new Intent();
                intent.putExtra("count", this.total);
                intent.putExtra(LocaleUtil.INDONESIAN, this.mCurrentItem.nid);
                intent.setAction(getResources().getString(R.string.activity_article_comment_list));
                startActivity(intent);
                return;
            case R.id.title_comment_num /*2131230804*/:
            case R.id.title_date /*2131230805*/:
            default:
                return;
            case R.id.title_fav /*2131230806*/:
                if (DBHelper.getDBHelper().counts("listitemfa", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
                    DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{this.mCurrentItem.n_mark});
                    Utils.showToast("已取消收藏");
                    view.setBackgroundResource(R.drawable.fav_btn);
                    return;
                }
                try {
                    this.mCurrentItem.show_type = UploadUtils.SUCCESS;
                    DBHelper.getDBHelper().insertObject(this.mCurrentItem, "listitemfa");
                    Utils.showToast("收藏成功");
                    view.setBackgroundResource(R.drawable.faved_btn);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            case R.id.title_share /*2131230807*/:
                if (!Utils.isNetworkAvailable(this)) {
                    Utils.showToast((int) R.string.network_error);
                    return;
                }
                DBHelper db = DBHelper.getDBHelper();
                if (db.counts("readitem", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
                    String[] value = {this.mCurrentItem.n_mark};
                    this.shorturl = db.select("readitem", "shorturl", "n_mark=?", value);
                    this.picurl = db.select("readitem", "share_image", "n_mark=?", value);
                }
                new AlertDialog.Builder(this).setTitle("分享方式").setItems(getResources().getStringArray(R.array.article_list_name), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String str;
                        String sname = ArticleActivity.this.getResources().getStringArray(R.array.article_wb_list_name_sa)[which];
                        if (sname.startsWith("wx")) {
                            if (!ArticleActivity.this.api.isWXAppInstalled()) {
                                new AlertDialog.Builder(ArticleActivity.this).setTitle("提示").setMessage("尚未安装“微信”，无法使用此功能").setPositiveButton("现在安装", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            ArticleActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=腾讯微信")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();
                                    }
                                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                                return;
                            }
                            if (sname.equals("wx")) {
                                ArticleActivity.this.weixin_type = 0;
                            } else {
                                ArticleActivity.this.weixin_type = 1;
                                if (ArticleActivity.this.api.getWXAppSupportAPI() < 553779201) {
                                    Utils.showToast("微信版本过低，暂时不支持分享到朋友圈");
                                    return;
                                }
                            }
                            if ("".equals(ArticleActivity.this.shorturl)) {
                                try {
                                    WeiboShareDao.weibo_get_shortid(ArticleActivity.this.mCurrentItem.nid, ArticleActivity.this.wxHandler);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                ArticleActivity.this.sendToWeixin();
                            }
                        } else if ("yj".equals(sname)) {
                            ArticleActivity.this.shareEmail(ArticleActivity.this.shorturl, ArticleActivity.this.picurl, ArticleActivity.this.mCurrentItem.title);
                        } else {
                            Intent i = new Intent();
                            i.setAction(ArticleActivity.this.getResources().getString(R.string.activity_share));
                            i.putExtra("sname", sname);
                            if (ArticleActivity.this.shorturl == null) {
                                str = "";
                            } else {
                                str = ArticleActivity.this.shorturl;
                            }
                            i.putExtra("shorturl", str);
                            i.putExtra("aid", ArticleActivity.this.mCurrentItem.nid);
                            i.putExtra(Constants.PARAM_TITLE, ArticleActivity.this.mCurrentItem.title);
                            i.putExtra(Constants.PARAM_APP_ICON, ArticleActivity.this.picurl == null ? "" : ArticleActivity.this.picurl);
                            i.putExtra("comment", ArticleActivity.this.mCurrentItem.des);
                            ArticleActivity.this.startActivity(i);
                        }
                    }
                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new Thread() {
            public void run() {
                try {
                    ArticleActivity.this.total = ArticleActivity.this.getCommentCount();
                    Utils.h.post(new Runnable() {
                        public void run() {
                            TextView countview = (TextView) ArticleActivity.this.findViewById(R.id.title_comment_num);
                            if (TextUtils.isEmpty(ArticleActivity.this.total) || UploadUtils.SUCCESS.equals(ArticleActivity.this.total)) {
                                countview.setText(UploadUtils.SUCCESS);
                            } else if (Integer.parseInt(ArticleActivity.this.total) > 1000) {
                                countview.setText("999+");
                            } else {
                                countview.setText(ArticleActivity.this.total);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public String getCommentCount() throws Exception {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("action", "commentcount"));
        param.add(new BasicNameValuePair(LocaleUtil.INDONESIAN, this.mCurrentItem.nid));
        return new JSONObject(MySSLSocketFactory.getinfo(Urls.app_api, param)).getString("count");
    }

    public void onDoubleTap() {
    }

    public void onSingleTapConfirmed() {
    }

    public void onLongPress() {
    }

    public void onLeftOption(boolean left) {
    }

    public void onRightOption(boolean right) {
    }

    public void setCheckedId(int id) {
        switch (id) {
            case 0:
                Intent intent = new Intent();
                intent.putExtra("count", this.total);
                intent.putExtra(LocaleUtil.INDONESIAN, this.mCurrentItem.nid);
                intent.setAction(getResources().getString(R.string.activity_article_comment_list));
                startActivity(intent);
                return;
            case 1:
                if (!Utils.isNetworkAvailable(this)) {
                    Utils.showToast((int) R.string.network_error);
                    return;
                }
                DBHelper db = DBHelper.getDBHelper();
                if (db.counts("readitem", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
                    String[] value = {this.mCurrentItem.n_mark};
                    this.shorturl = db.select("readitem", "shorturl", "n_mark=?", value);
                    this.picurl = db.select("readitem", "share_image", "n_mark=?", value);
                }
                new AlertDialog.Builder(this).setTitle("分享方式").setItems(getResources().getStringArray(R.array.article_list_name), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String str;
                        String sname = ArticleActivity.this.getResources().getStringArray(R.array.article_wb_list_name_sa)[which];
                        if (sname.equals("wx")) {
                            if (!ArticleActivity.this.api.isWXAppInstalled()) {
                                Utils.showToast("尚未安装“微信”，无法使用此功能。");
                            } else if ("".equals(ArticleActivity.this.shorturl)) {
                                try {
                                    WeiboShareDao.weibo_get_shortid(ArticleActivity.this.mCurrentItem.nid, ArticleActivity.this.wxHandler);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                ArticleActivity.this.sendToWeixin();
                            }
                        } else if ("yj".equals(sname)) {
                            ArticleActivity.this.shareEmail(ArticleActivity.this.shorturl, ArticleActivity.this.picurl, ArticleActivity.this.mCurrentItem.title);
                        } else {
                            Intent i = new Intent();
                            i.setAction(ArticleActivity.this.getResources().getString(R.string.activity_share));
                            i.putExtra("sname", sname);
                            if (ArticleActivity.this.shorturl == null) {
                                str = "";
                            } else {
                                str = ArticleActivity.this.shorturl;
                            }
                            i.putExtra("shorturl", str);
                            i.putExtra("aid", ArticleActivity.this.mCurrentItem.nid);
                            i.putExtra(Constants.PARAM_TITLE, ArticleActivity.this.mCurrentItem.title);
                            i.putExtra(Constants.PARAM_APP_ICON, ArticleActivity.this.picurl == null ? "" : ArticleActivity.this.picurl);
                            i.putExtra("comment", ArticleActivity.this.mCurrentItem.des);
                            ArticleActivity.this.startActivity(i);
                        }
                    }
                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
            case 2:
                if (DBHelper.getDBHelper().counts("listitemfa", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
                    DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{this.mCurrentItem.n_mark});
                    Utils.showToast("已取消收藏");
                } else {
                    try {
                        this.mCurrentItem.show_type = UploadUtils.SUCCESS;
                        DBHelper.getDBHelper().insertObject(this.mCurrentItem, "listitemfa");
                        Utils.showToast("收藏成功");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                this.pathButton.setIamges();
                return;
            default:
                return;
        }
    }
}
