package com.city_life.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import com.city_life.fragment.PicListFragment;
import com.palmtrends.baseui.BaseActivity;
import com.pengyou.citycommercialarea.R;

public class PicActivity extends BaseActivity {
    Fragment frag = null;
    Fragment m_list_frag_1 = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_part);
        initFragment();
    }

    public void initFragment() {
        Fragment findresult = getSupportFragmentManager().findFragmentByTag("weibo_fragment");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (PicListFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = PicListFragment.newInstance(getIntent().getStringExtra("parttpye"), "pic");
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, "weibo_fragment");
        fragmentTransaction.commit();
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_setting /*2131230841*/:
                Intent intent = new Intent();
                intent.setAction(getResources().getString(R.string.activity_setting));
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
