package com.city_life.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.Constants;
import sina_weibo.Util;
import sina_weibo.WeiboShareDao;

public class WeiboBangdingActivity extends BaseActivity {
    private String QQBindUrl = "https://open.t.qq.com/cgi-bin/oauth2/authorize?appfrom=android&response_type=token&redirect_uri=http://www.pymob.cn&client_id=";
    private String SinaBindUrl = "https://open.weibo.cn/oauth2/authorize?scope=all&redirect_uri=http://www.pymob.cn&display=mobile&client_id=";
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 10002:
                    Utils.showToast(String.valueOf(msg.obj));
                    if (WeiboBangdingActivity.this.sname.equals("qq")) {
                        Util.clearSharePersistent(WeiboBangdingActivity.this.mContext);
                    } else {
                        PerfHelper.setInfo(Constants.PREF_SINA_ACCESS_TOKEN, "");
                        PerfHelper.setInfo(Constants.PREF_SINA_UID, "");
                        PerfHelper.setInfo(Constants.PREF_SINA_EXPIRES_TIME, 0);
                        PerfHelper.setInfo(Constants.PREF_SINA_REMIND_IN, "");
                        PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME, "");
                        PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME_IMG, "");
                    }
                    WeiboBangdingActivity.this.finish();
                    return;
                case FinalVariable.vb_get_userinfor /*10022*/:
                    WeiboBangdingActivity.this.jo = (JSONObject) msg.obj;
                    if (ShareApplication.debug) {
                        System.out.println("微博绑定返回:" + WeiboBangdingActivity.this.jo);
                    }
                    try {
                        if (!WeiboBangdingActivity.this.sname.equals("qq")) {
                            String username = WeiboBangdingActivity.this.jo.getString(Constants.SINA_NAME);
                            String img = WeiboBangdingActivity.this.jo.getString(Constants.SINA_USER_IMG);
                            PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME, username);
                            PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME_IMG, img);
                        } else if (WeiboBangdingActivity.this.jo.getString("ret").equals(UploadUtils.SUCCESS)) {
                            JSONObject jso = WeiboBangdingActivity.this.jo.getJSONObject("data");
                            String username2 = jso.getString("nick");
                            String img2 = jso.getString("head");
                            PerfHelper.setInfo(Constants.PREF_TX_NAME, username2);
                            PerfHelper.setInfo(Constants.PREF_TX_USER_NAME_IMG, img2);
                        } else {
                            Utils.showToast("用户信息获取失败");
                            if (WeiboBangdingActivity.this.sname.equals("qq")) {
                                Util.clearSharePersistent(WeiboBangdingActivity.this.mContext);
                                return;
                            }
                            return;
                        }
                        Utils.showToast("绑定成功");
                        WeiboBangdingActivity.this.setResult(FinalVariable.vb_success, WeiboBangdingActivity.this.getIntent());
                        WeiboBangdingActivity.this.finish();
                        return;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Utils.showToast("用户信息获取失败");
                        if (WeiboBangdingActivity.this.sname.equals("qq")) {
                            Util.clearSharePersistent(WeiboBangdingActivity.this.mContext);
                        } else {
                            PerfHelper.setInfo(Constants.PREF_SINA_ACCESS_TOKEN, "");
                            PerfHelper.setInfo(Constants.PREF_SINA_UID, "");
                            PerfHelper.setInfo(Constants.PREF_SINA_EXPIRES_TIME, 0);
                            PerfHelper.setInfo(Constants.PREF_SINA_REMIND_IN, "");
                            PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME, "");
                            PerfHelper.setInfo(Constants.PREF_SINA_USER_NAME_IMG, "");
                        }
                        WeiboBangdingActivity.this.finish();
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public JSONObject jo;
    /* access modifiers changed from: private */
    public View loading;
    /* access modifiers changed from: private */
    public String mBindUrl = "";
    Context mContext;
    private RequestQueue mQueue;
    /* access modifiers changed from: private */
    public String sname = "";
    /* access modifiers changed from: private */
    public boolean tuijian_switch = true;
    /* access modifiers changed from: private */
    public ImageView tuijian_switch_imageview;
    private View tuijian_view;
    /* access modifiers changed from: private */
    public WebView webView;

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_bangding);
        this.mQueue = Volley.newRequestQueue(getApplicationContext());
        this.sname = getIntent().getStringExtra("sname");
        this.mContext = getApplicationContext();
        findView();
        addListener();
        if (this.sname.equals("qq")) {
            this.mBindUrl = String.valueOf(this.QQBindUrl) + getResources().getString(R.string.qq_appkey);
        } else {
            this.mBindUrl = String.valueOf(this.SinaBindUrl) + getResources().getString(R.string.sina_appkey);
        }
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new WebViewClient() {
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if (handler != null) {
                    handler.proceed();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
             arg types: [java.lang.String, int]
             candidates:
              com.utils.PerfHelper.setInfo(java.lang.String, int):void
              com.utils.PerfHelper.setInfo(java.lang.String, long):void
              com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
              com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (url.startsWith("http://www.pymob.cn/canceled")) {
                    WeiboBangdingActivity.this.finish();
                    return;
                }
                if (url.startsWith(Constants.SINA_REDIRECT_URL)) {
                    WeiboBangdingActivity.this.webView.clearCache(true);
                    WeiboBangdingActivity.this.webView.clearHistory();
                    WeiboBangdingActivity.this.webView.loadUrl("file:///android_asset/bangdingsuccess.html");
                    if (WeiboBangdingActivity.this.sname.equals("sina")) {
                        try {
                            WeiboShareDao.weibo_get_wbuid(WeiboBangdingActivity.this.sname, url.substring(url.lastIndexOf("=") + 1), WeiboBangdingActivity.this.handler);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (WeiboBangdingActivity.this.sname.equals("qq")) {
                        String[] vs = url.replace("http://www.pymob.cn/#", "").split("&");
                        String token = vs[0].replace("access_token=", "");
                        String time = vs[1].replace("expires_in=", "");
                        String openid = vs[2].replace("openid=", "");
                        String openkey = vs[3].replace("openkey=", "");
                        String refresh_token = vs[4].replace("refresh_token=", "");
                        PerfHelper.setInfo(Constants.PREF_TX_ACCESS_TOKEN, token);
                        PerfHelper.setInfo(Constants.PREF_TX_OPEN_ID, openid);
                        PerfHelper.setInfo(Constants.PREF_TX_OPEN_KEY, openkey);
                        PerfHelper.setInfo(Constants.PREF_TX_REFRESH_TOKEN, refresh_token);
                        PerfHelper.setInfo(Constants.PREF_TX_EXPIRES_TIME, String.valueOf(System.currentTimeMillis() + (Long.valueOf(time).longValue() * 1000)));
                        Util.saveSharePersistent(WeiboBangdingActivity.this.mContext, "ACCESS_TOKEN", token);
                        Util.saveSharePersistent(WeiboBangdingActivity.this.mContext, "EXPIRES_IN", String.valueOf(System.currentTimeMillis() + (Long.valueOf(time).longValue() * 1000)));
                        Util.saveSharePersistent(WeiboBangdingActivity.this.mContext, "OPEN_ID", openid);
                        Util.saveSharePersistent(WeiboBangdingActivity.this.mContext, "OPEN_KEY", openkey);
                        Util.saveSharePersistent(WeiboBangdingActivity.this.mContext, "REFRESH_TOKEN", "");
                        Util.saveSharePersistent(WeiboBangdingActivity.this.mContext, "CLIENT_ID", Constants.TX_APP_KEY);
                        Util.saveSharePersistent(WeiboBangdingActivity.this.mContext, "AUTHORIZETIME", String.valueOf(System.currentTimeMillis() / 1000));
                        try {
                            WeiboShareDao.bind_qq_get_userinfo(token, openid, openkey, WeiboBangdingActivity.this.handler);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    } else {
                        PerfHelper.setInfo(PerfHelper.P_SHARE_STATE + WeiboBangdingActivity.this.sname, true);
                        Utils.showToast("绑定成功");
                        WeiboBangdingActivity.this.finish();
                    }
                }
                super.onPageStarted(view, url, favicon);
            }

            public void onPageFinished(WebView view, String url) {
                WeiboBangdingActivity.this.loading.setVisibility(8);
                super.onPageFinished(view, url);
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                WeiboBangdingActivity.this.loading.setVisibility(0);
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        new Thread() {
            public void run() {
                CookieManager.getInstance().removeAllCookie();
                WeiboBangdingActivity.this.webView.loadUrl(WeiboBangdingActivity.this.mBindUrl);
            }
        }.start();
    }

    public void getQQuser(String token, String openid, String openkey, Handler hander) {
        String url = String.valueOf(WeiboShareDao.main_qq_weibo) + "&access_token=" + token + "&openid=" + openid + "&openkey=" + openkey + "&clientip=" + getLocalIpAddress() + "&oauth_version=2.a";
        System.out.println("请求返回XXXXXXXXX:" + url);
        this.mQueue.add(new JsonObjectRequest(0, url, null, new Response.Listener<JSONObject>() {
            public void onResponse(JSONObject arg0) {
                System.out.println("请求返回XXXXXXXXX:" + arg0);
            }
        }, null));
        this.mQueue.start();
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
        }
        return null;
    }

    public void findView() {
        this.webView = (WebView) findViewById(R.id.wb_bangding_webview);
        this.tuijian_switch_imageview = (ImageView) findViewById(R.id.bind_tuijian_switch);
        if (this.tuijian_switch) {
            this.tuijian_switch_imageview.setBackgroundResource(R.drawable.bind_tuijian_open);
        } else {
            this.tuijian_switch_imageview.setBackgroundResource(R.drawable.bind_tuijian_close);
        }
        this.tuijian_view = findViewById(R.id.bind_tuijian);
        this.loading = findViewById(R.id.loading);
        this.loading = findViewById(R.id.loading);
        this.loading.setVisibility(0);
        TextView tvModuleName = (TextView) findViewById(R.id.bind_bangding_titletext);
        if (this.sname.equals("sina")) {
            tvModuleName.setText("新浪微博绑定");
        } else if (this.sname.equals("qq")) {
            tvModuleName.setText("绑定腾讯微博");
        } else if (this.sname.equals("renren")) {
            tvModuleName.setText("绑定人人网");
        } else if (this.sname.equals("kaixin")) {
            tvModuleName.setText("绑定开心网");
        } else if (this.sname.equals("sinazhuc")) {
            this.tuijian_view.setVisibility(8);
            this.tuijian_switch = false;
        }
    }

    public void addListener() {
        this.tuijian_switch_imageview.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (WeiboBangdingActivity.this.tuijian_switch) {
                    WeiboBangdingActivity.this.tuijian_switch_imageview.setBackgroundResource(R.drawable.bind_tuijian_close);
                    WeiboBangdingActivity.this.tuijian_switch = false;
                    return;
                }
                WeiboBangdingActivity.this.tuijian_switch_imageview.setBackgroundResource(R.drawable.bind_tuijian_open);
                WeiboBangdingActivity.this.tuijian_switch = true;
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            default:
                return;
        }
    }
}
