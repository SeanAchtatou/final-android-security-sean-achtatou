package com.city_life.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.Toast;
import com.baidu.android.pushservice.PushManager;
import com.baidu.push.example.Utils;
import com.city_life.entity.CityLifeApplication;
import com.city_life.part_activiy.NearListActivity;
import com.city_life.part_activiy.ProfessionalmarketActivity;
import com.city_life.part_activiy.ShouyeActivity;
import com.city_life.part_activiy.YingYongHuiActivity;
import com.city_life.part_asynctask.GetFavListId;
import com.imofan.android.basic.update.MFUpdateListener;
import com.imofan.android.basic.update.MFUpdateService;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseMainActivity;
import com.pengyou.citycommercialarea.R;
import com.tencent.tauth.Constants;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class MainActivity extends BaseMainActivity {
    public int currentpart = R.id.m_part_1;
    private long exitTime = 0;
    View mOldView;
    TabHost mTabHost;
    Handler mahandler;
    HashMap<String, Boolean> tabmap = new HashMap<>();
    HashSet<String> tabset = new HashSet<>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_main);
        this.mTabHost = (TabHost) findViewById(R.id.tabhost);
        this.mTabHost.setup(getLocalActivityManager());
        if (savedInstanceState != null) {
            this.currentpart = savedInstanceState.getInt("current_key");
        } else {
            this.currentpart = getIntent().getIntExtra("current_key", R.id.m_part_1);
        }
        changePart(findViewById(this.currentpart));
        if (PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN)) {
            new GetFavListId().execute(null);
        }
        this.mahandler = new Handler();
        PushManager.startWork(getApplicationContext(), 0, Utils.getMetaValue(this, "api_key"));
        List<String> tagsList = getTagsList("city" + PerfHelper.getStringData(PerfHelper.P_CITY_No));
        MFUpdateService.check(this, new MFUpdateListener() {
            public void onWifiOff(Activity activity) {
                if (ShareApplication.debug) {
                    System.out.println("没更新");
                }
            }

            public void onFailed(Activity activity) {
                if (ShareApplication.debug) {
                    System.out.println("没更新");
                }
            }

            public void onDetectedNothing(Activity activity) {
                if (ShareApplication.debug) {
                    System.out.println("没更新");
                }
            }

            public void onDetectedNewVersion(Activity activity, int versionCode, String versionName, String description, final String apk) {
                Log.v("onDetectedNothing :", "检查有更新");
                if (versionCode > Integer.parseInt(ShareApplication.getVersion().replace(".", ""))) {
                    MainActivity.this.mahandler.post(new Runnable() {
                        public void run() {
                            AlertDialog.Builder message = new AlertDialog.Builder(MainActivity.this).setMessage("发现新版本，是否更新");
                            final String str = apk;
                            message.setPositiveButton("是", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (!str.startsWith("http://")) {
                                        com.palmtrends.loadimage.Utils.showToast("参数错误");
                                        return;
                                    }
                                    try {
                                        MainActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                                    } catch (Exception e) {
                                        com.palmtrends.loadimage.Utils.showToast("请安装浏览器");
                                        e.printStackTrace();
                                    }
                                    com.palmtrends.loadimage.Utils.dismissProcessDialog();
                                }
                            }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                }
                            }).show();
                        }
                    });
                }
            }

            public void onPerformUpdate(Activity activity, int versionCode, String versionName, String description, String apk) {
            }
        }, false, true);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("current_key", this.currentpart);
        super.onSaveInstanceState(outState);
    }

    public void changePart(View view) {
        if (this.mOldView == null || view.getId() != this.mOldView.getId()) {
            if (this.mOldView != null) {
                this.mOldView.setBackgroundDrawable(null);
                switch (this.mOldView.getId()) {
                    case R.id.m_part_1 /*2131230843*/:
                        ((ImageView) this.mOldView).setImageResource(R.drawable.main_part_1_n);
                        break;
                    case R.id.m_part_2 /*2131230844*/:
                        ((ImageView) this.mOldView).setImageResource(R.drawable.main_part_2_n);
                        break;
                    case R.id.m_part_3 /*2131230845*/:
                        ((ImageView) this.mOldView).setImageResource(R.drawable.main_part_3_n);
                        break;
                    case R.id.m_part_4 /*2131230846*/:
                        ((ImageView) this.mOldView).setImageResource(R.drawable.main_part_4_n);
                        break;
                    case R.id.m_part_5 /*2131230847*/:
                        ((ImageView) this.mOldView).setImageResource(R.drawable.main_part_5_n);
                        break;
                }
            }
            this.mOldView = view;
            String tag = "";
            Intent intent = new Intent();
            switch (view.getId()) {
                case R.id.m_part_1 /*2131230843*/:
                    tag = "parg1";
                    intent.setClass(this, ShouyeActivity.class);
                    intent.putExtra(Constants.PARAM_TITLE, "一级栏目首页");
                    ((ImageView) view).setImageResource(R.drawable.main_part_1_h);
                    break;
                case R.id.m_part_2 /*2131230844*/:
                    tag = "parg2";
                    intent.setClass(this, ProfessionalmarketActivity.class);
                    intent.putExtra(Constants.PARAM_TITLE, "一级栏目市场");
                    ((ImageView) view).setImageResource(R.drawable.main_part_2_h);
                    break;
                case R.id.m_part_3 /*2131230845*/:
                    tag = "parg3";
                    intent.setClass(this, NearListActivity.class);
                    intent.putExtra(Constants.PARAM_TITLE, "一级栏目附近");
                    ((ImageView) view).setImageResource(R.drawable.main_part_3_h);
                    break;
                case R.id.m_part_4 /*2131230846*/:
                    tag = "parg4";
                    intent.setClass(this, YingYongHuiActivity.class);
                    intent.putExtra(Constants.PARAM_TITLE, "一级栏目资讯");
                    ((ImageView) view).setImageResource(R.drawable.main_part_4_h);
                    break;
                case R.id.m_part_5 /*2131230847*/:
                    tag = "parg5";
                    intent.setClass(this, SettingActivty.class);
                    ((ImageView) view).setImageResource(R.drawable.main_part_5_h);
                    break;
            }
            view.setSelected(false);
            if (this.tabmap.get(tag) == null || !this.tabmap.get(tag).booleanValue()) {
                intent.putExtra("data", tag);
                this.mTabHost.addTab(buildTabSpec(this.mTabHost, tag, intent));
                this.tabmap.put(tag, true);
                this.mTabHost.setCurrentTabByTag(tag);
                return;
            }
            this.mTabHost.setCurrentTabByTag(tag);
        }
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_home /*2131230854*/:
                finish();
                return;
            default:
                return;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() != 4 || event.getAction() != 0) {
            return super.dispatchKeyEvent(event);
        }
        if (System.currentTimeMillis() - this.exitTime > 2000) {
            Toast.makeText(getApplicationContext(), "再按一次退出程序", 0).show();
            this.exitTime = System.currentTimeMillis();
        } else {
            System.exit(0);
            Process.killProcess(Process.myPid());
            finish();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        CityLifeApplication app = (CityLifeApplication) getApplication();
        if (app.mBMapManager != null) {
            app.mBMapManager.destroy();
            app.mBMapManager = null;
        }
        PerfHelper.setInfo(PerfHelper.P_NOW_CITY, "");
        System.exit(0);
        super.onDestroy();
    }

    private List<String> getTagsList(String originalText) {
        List<String> tags = new ArrayList<>();
        int indexOfComma = originalText.indexOf(44);
        while (indexOfComma != -1) {
            tags.add(originalText.substring(0, indexOfComma));
            originalText = originalText.substring(indexOfComma + 1);
            indexOfComma = originalText.indexOf(44);
        }
        tags.add(originalText);
        return tags;
    }
}
