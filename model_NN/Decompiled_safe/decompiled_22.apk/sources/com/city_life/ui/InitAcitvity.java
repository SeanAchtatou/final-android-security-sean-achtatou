package com.city_life.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.search.MKAddrInfo;
import com.baidu.mapapi.search.MKBusLineResult;
import com.baidu.mapapi.search.MKDrivingRouteResult;
import com.baidu.mapapi.search.MKPoiResult;
import com.baidu.mapapi.search.MKSearch;
import com.baidu.mapapi.search.MKSearchListener;
import com.baidu.mapapi.search.MKSuggestionResult;
import com.baidu.mapapi.search.MKTransitRouteResult;
import com.baidu.mapapi.search.MKWalkingRouteResult;
import com.baidu.platform.comapi.basestruct.GeoPoint;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.entity.CityLifeApplication;
import com.city_life.part_asynctask.GetBianMingPartThread;
import com.city_life.part_asynctask.GetNearPartThread;
import com.imofan.android.basic.Mofang;
import com.palmtrends.ad.ShowFullAd;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.FinalVariable;
import com.utils.GpsUtils;
import com.utils.PerfHelper;
import org.json.JSONArray;
import org.json.JSONObject;

public class InitAcitvity extends BaseFragmentActivity {
    private CityLifeApplication ca;
    /* access modifiers changed from: private */
    public String citylistjson = "";
    ShowFullAd fad;
    FinishGPSReceiver finishBroadCastGPSReceiver;
    private GeoPoint gt;
    int i = 0;
    boolean isfrist = true;
    Context mContext;
    public Data mData = null;
    public int mFooter_limit = this.mLength;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FinalVariable.update /*1001*/:
                    InitAcitvity.this.begin_StartActivity();
                    break;
                case FinalVariable.error /*1004*/:
                    if (Utils.isNetworkAvailable(InitAcitvity.this)) {
                        Utils.showToast((int) R.string.server_error);
                        InitAcitvity.this.finish();
                        break;
                    } else {
                        Utils.showToast((int) R.string.network_error);
                        InitAcitvity.this.finish();
                        return;
                    }
                case FinalVariable.first_update /*1011*/:
                    if (!InitAcitvity.this.citylistjson.equals("")) {
                        InitAcitvity.this.getDate(InitAcitvity.this.mOldtype, InitAcitvity.this.mPage, true);
                        break;
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };
    public int mLength = 20;
    public String mOldtype = "citys_list";
    public int mPage = 1;
    public String mParttype = "citys_list";
    private MKSearch mSearch;

    public void onCreate(Bundle arg0) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(arg0);
        this.mContext = this;
        setContentView((int) R.layout.activity_init);
        PerfHelper.getPerferences(getApplicationContext());
        DBHelper.getDBHelper();
        if (PerfHelper.getStringData(PerfHelper.P_USERID).equals("")) {
            PerfHelper.setInfo(PerfHelper.P_USERID, "游客");
        }
        if (PerfHelper.getStringData(PerfHelper.P_CITY).equals("")) {
            PerfHelper.setInfo(PerfHelper.P_CITY, "重庆");
        }
        if (PerfHelper.getStringData(PerfHelper.P_CITY_No).equals("")) {
            PerfHelper.setInfo(PerfHelper.P_CITY_No, "18");
        }
        setGPSFinish();
        PerfHelper.setInfo(PerfHelper.P_PHONE_W, getResources().getDisplayMetrics().widthPixels);
        PerfHelper.setInfo(PerfHelper.P_PHONE_H, getResources().getDisplayMetrics().heightPixels);
        GpsUtils.getLocation();
        initDataUserInfo();
    }

    public void initDataUserInfo() {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Mofang.onResume(this, "启动界面");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Mofang.onPause(this);
    }

    public void begin_StartActivity() {
        Intent i2 = new Intent();
        i2.setClass(this, MainActivity.class);
        startActivity(i2);
        overridePendingTransition(R.anim.init_in, R.anim.init_out);
        finish();
    }

    public void setGPSFinish() {
        this.finishBroadCastGPSReceiver = new FinishGPSReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GpsUtils.GPS_FINSH);
        ShareApplication.share.registerReceiver(this.finishBroadCastGPSReceiver, intentFilter);
    }

    public class FinishGPSReceiver extends BroadcastReceiver {
        public FinishGPSReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (ShareApplication.debug) {
                System.out.println("GPS定位完毕");
            }
            if (InitAcitvity.this.isfrist) {
                InitAcitvity.this.isfrist = false;
                if (!PerfHelper.getStringData(PerfHelper.P_NOW_CITY).equals("")) {
                    if (PerfHelper.getStringData(PerfHelper.P_CITY).equals("")) {
                        PerfHelper.setInfo(PerfHelper.P_CITY, PerfHelper.getStringData(PerfHelper.P_NOW_CITY));
                    }
                    InitAcitvity.this.initData();
                    return;
                }
                InitAcitvity.this.getCity();
            }
        }
    }

    public void initData() {
        new Thread() {
            public void run() {
                boolean isDB = true;
                try {
                    String d = InitAcitvity.this.getDataFromDB(InitAcitvity.this.mOldtype, InitAcitvity.this.mPage, InitAcitvity.this.mLength, InitAcitvity.this.mParttype);
                    if (d != null && d.length() > 0) {
                        InitAcitvity.this.citylistjson = d;
                        isDB = false;
                        InitAcitvity.this.mHandler.sendEmptyMessage(FinalVariable.first_update);
                    }
                    InitAcitvity.this.getDataFromNet(InitAcitvity.this.getResources().getString(R.string.citylife_citys_list_url), InitAcitvity.this.mOldtype, InitAcitvity.this.mPage, InitAcitvity.this.mLength, true, InitAcitvity.this.mParttype);
                    if (isDB) {
                        InitAcitvity.this.mHandler.sendEmptyMessage(FinalVariable.first_update);
                    }
                } catch (Exception e) {
                    InitAcitvity.this.mHandler.sendEmptyMessage(FinalVariable.error);
                }
            }
        }.start();
    }

    public String getDataFromDB(String oldtype, int page, int count, String parttype) throws Exception {
        String json = DNDataSource.list_FromDB(oldtype, page, count, parttype);
        if (json == null || "".equals(json) || "null".equals(json)) {
            return null;
        }
        return json;
    }

    public void getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        this.citylistjson = DNDataSource.CityLift_list_FromNET(url, oldtype, page, count, parttype, isfirst);
        if (this.citylistjson != null && this.citylistjson.length() > 0) {
            if (isfirst) {
                DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
            }
            DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, this.citylistjson, oldtype);
        }
        if (ShareApplication.debug) {
            System.out.println("城市列表返回:" + this.citylistjson);
        }
    }

    public void getDate(String oldtype, int page, boolean isfirst) {
        try {
            this.mData = parseJson(this.citylistjson);
            if (ShareApplication.debug) {
                System.out.println("当前城市ID" + PerfHelper.getStringData(PerfHelper.P_CITY_No));
            }
            new GetBianMingPartThread().start();
            new GetNearPartThread().start();
            this.mHandler.sendEmptyMessage(FinalVariable.update);
        } catch (Exception e) {
            e.printStackTrace();
            this.mHandler.sendEmptyMessage(FinalVariable.error);
        }
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (jsonobj.has("responseCode") && jsonobj.getInt("responseCode") != 0) {
            this.mHandler.sendEmptyMessage(FinalVariable.error);
        }
        JSONArray jsonay = jsonobj.getJSONArray("results");
        int count = jsonay.length();
        for (int i2 = 0; i2 < count; i2++) {
            Listitem o = new Listitem();
            JSONObject obj = jsonay.getJSONObject(i2);
            o.nid = obj.getString(LocaleUtil.INDONESIAN);
            try {
                if (obj.has("cityName")) {
                    o.title = obj.getString("cityName");
                }
                if (obj.has("cityWord")) {
                    o.des = obj.getString("cityWord");
                }
                if (obj.has("author")) {
                    o.author = obj.getString("author");
                }
            } catch (Exception e) {
                this.mHandler.sendEmptyMessage(FinalVariable.error);
            }
            if (PerfHelper.getStringData(PerfHelper.P_CITY).startsWith(o.title)) {
                PerfHelper.setInfo(PerfHelper.P_CITY_No, o.nid);
                PerfHelper.setInfo(PerfHelper.P_CITY, o.title);
            }
            o.getMark();
            data.list.add(o);
        }
        return data;
    }

    public void getCity() {
        this.ca = (CityLifeApplication) getApplication();
        if (this.ca.mBMapManager == null) {
            this.ca.mBMapManager = new BMapManager(this);
            this.ca.mBMapManager.init(CityLifeApplication.strKey, new CityLifeApplication.MyGeneralListener());
        }
        this.gt = new GeoPoint((int) (Double.parseDouble(PerfHelper.getStringData(PerfHelper.P_GPS_LATI)) * 1000000.0d), (int) (Double.parseDouble(PerfHelper.getStringData(PerfHelper.P_GPS_LONG)) * 1000000.0d));
        this.mSearch = new MKSearch();
        this.mSearch.init(this.ca.mBMapManager, new myMKSearchListener());
        this.mSearch.reverseGeocode(this.gt);
    }

    public class myMKSearchListener implements MKSearchListener {
        public myMKSearchListener() {
        }

        public void onGetAddrResult(MKAddrInfo result, int iError) {
            if (iError == 0 && result != null) {
                if (PerfHelper.getStringData(PerfHelper.P_CITY).equals("")) {
                    PerfHelper.setInfo(PerfHelper.P_CITY, result.addressComponents.city);
                }
                PerfHelper.setInfo(PerfHelper.P_NOW_CITY, result.addressComponents.city);
                InitAcitvity.this.initData();
                if (ShareApplication.debug) {
                    System.out.println("通过百度地图获取当前城市：" + result.addressComponents.city);
                }
            }
        }

        public void onGetBusDetailResult(MKBusLineResult arg0, int arg1) {
        }

        public void onGetDrivingRouteResult(MKDrivingRouteResult arg0, int arg1) {
        }

        public void onGetPoiDetailSearchResult(int arg0, int arg1) {
        }

        public void onGetPoiResult(MKPoiResult arg0, int arg1, int arg2) {
        }

        public void onGetSuggestionResult(MKSuggestionResult arg0, int arg1) {
        }

        public void onGetTransitRouteResult(MKTransitRouteResult arg0, int arg1) {
        }

        public void onGetWalkingRouteResult(MKWalkingRouteResult arg0, int arg1) {
        }
    }
}
