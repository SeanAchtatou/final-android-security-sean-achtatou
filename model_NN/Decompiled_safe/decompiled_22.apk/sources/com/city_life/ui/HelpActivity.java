package com.city_life.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.palmtrends.baseui.BaseActivity;
import com.pengyou.citycommercialarea.R;
import java.util.ArrayList;

public class HelpActivity extends BaseActivity {
    ColorPagerAdapter pager;
    ViewPager vp;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_help);
        this.vp = (ViewPager) findViewById(R.id.home_viewpager);
        this.pager = new ColorPagerAdapter(getSupportFragmentManager());
        this.vp.setAdapter(this.pager);
        this.vp.setCurrentItem(0);
        this.vp.setVisibility(0);
    }

    public class ColorPagerAdapter extends FragmentPagerAdapter {
        private ArrayList<Fragment> mFragments = new ArrayList<>();

        public ColorPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public int getCount() {
            return 3;
        }

        public Fragment getItem(int position) {
            return this.mFragments.get(position);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void things(View view) {
    }
}
