package com.city_life.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.dao.MySSLSocketFactory;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class AritcleCommentActivity extends Activity {
    public static final String TAG = "CommentActivity";
    public String mArticleId;
    /* access modifiers changed from: private */
    public RatingBar mBar;
    public TextView mBtnSend;
    private boolean mCommentNeedBind = false;
    public EditText mEditText;
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            int action = msg.what;
            AritcleCommentActivity.this.mLoad.setVisibility(8);
            switch (action) {
                case 1:
                    AritcleCommentActivity.this.mEditText.setText("");
                    Utils.showToast("评论成功");
                    AritcleCommentActivity.this.setResult(2, new Intent());
                    AritcleCommentActivity.this.finish();
                    return;
                case 2:
                    Utils.showToast("评论失败");
                    return;
                default:
                    return;
            }
        }
    };
    public View mLoad;
    public TextView mLoad_Text;
    String name = "游客";
    public int num = 280;
    public TextView zishu_textView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_comment);
        init();
    }

    public void init() {
        this.mArticleId = getIntent().getStringExtra(LocaleUtil.INDONESIAN);
        this.mLoad = findViewById(R.id.loading);
        this.mLoad_Text = (TextView) findViewById(R.id.loading_text);
        this.mLoad_Text.setText("正在发送...");
        this.mLoad.setVisibility(8);
        this.mBar = (RatingBar) findViewById(R.id.rb_addSeller_grade);
        initLayout();
    }

    public void initLayout() {
        this.mEditText = (EditText) findViewById(R.id.comment_article);
        this.mBtnSend = (TextView) findViewById(R.id.send_btn);
        this.zishu_textView = (TextView) findViewById(R.id.zishu_textView);
        this.mEditText.addTextChangedListener(new TextWatcher() {
            public int selectionEnd;
            public int selectionStart;
            public CharSequence temp;

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                this.temp = s;
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                int number = AritcleCommentActivity.this.num - AritcleCommentActivity.getCharacterNum(AritcleCommentActivity.this.mEditText.getText().toString());
                AritcleCommentActivity.this.zishu_textView.setText(new StringBuilder().append(number / 2).toString());
                if (number / 2 <= 0) {
                    AritcleCommentActivity.this.zishu_textView.setTextColor(AritcleCommentActivity.this.getResources().getColor(R.color.red));
                } else {
                    AritcleCommentActivity.this.zishu_textView.setTextColor(AritcleCommentActivity.this.getResources().getColor(17170444));
                }
                this.selectionStart = AritcleCommentActivity.this.mEditText.getSelectionStart();
                this.selectionEnd = AritcleCommentActivity.this.mEditText.getSelectionEnd();
            }
        });
    }

    public void things(View view) {
        int action = view.getId();
        if (action == R.id.comment_back) {
            finish();
        } else if (action == R.id.send_btn) {
            if (PerfHelper.getBooleanData("p_share_state_settingsina")) {
                this.name = PerfHelper.getStringData("p_share_name_settingsina");
            } else if (PerfHelper.getBooleanData("p_share_state_settingqq")) {
                this.name = PerfHelper.getStringData("p_share_name_settingqq");
            }
            final String content = this.mEditText.getText().toString();
            if (TextUtils.isEmpty(content)) {
                Utils.showToast("评论不能为空");
                return;
            }
            View v = getWindow().peekDecorView();
            if (v != null) {
                ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
            if (!"游客".equals(this.name)) {
                this.mLoad.setVisibility(0);
                new Thread() {
                    public void run() {
                        AritcleCommentActivity.this.article_comment(AritcleCommentActivity.this.mArticleId, AritcleCommentActivity.this.name, content, new StringBuilder(String.valueOf(AritcleCommentActivity.this.mBar.getRating())).toString(), AritcleCommentActivity.this.mHandler);
                    }
                }.start();
            } else if (this.mCommentNeedBind) {
                Intent intent = new Intent();
                intent.setAction(getResources().getString(R.string.activity_st_share_manager));
                startActivity(intent);
                Utils.showToast("请先绑定新浪或者腾讯微博，再进行评论");
            } else {
                this.mLoad.setVisibility(0);
                new Thread() {
                    public void run() {
                        AritcleCommentActivity.this.article_comment(AritcleCommentActivity.this.mArticleId, "游客", content, new StringBuilder(String.valueOf(AritcleCommentActivity.this.mBar.getRating())).toString(), AritcleCommentActivity.this.mHandler);
                    }
                }.start();
            }
        }
    }

    public void article_comment(String id, String username, String comment, String level, Handler mHandler2) {
        String str;
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sellerId", id));
        param.add(new BasicNameValuePair("user", username));
        param.add(new BasicNameValuePair("content", comment));
        param.add(new BasicNameValuePair("level", level));
        Message msg = new Message();
        try {
            if (new JSONObject(MySSLSocketFactory.getinfo(getResources().getString(R.string.citylife_putComment_list_url), param)).getString("responseCode").equals(UploadUtils.SUCCESS)) {
                str = "评论成功";
                msg.what = 1;
            } else {
                str = "失败";
                msg.what = 2;
            }
        } catch (Exception e) {
            str = "";
            msg.what = 2;
        }
        msg.obj = str;
        mHandler2.sendMessage(msg);
    }

    public static int getCharacterNum(String content) {
        if (content == null || "".equals(content)) {
            return 0;
        }
        return content.length() + getChineseNum(content);
    }

    public static int getChineseNum(String s) {
        int num2 = 0;
        char[] myChar = s.toCharArray();
        for (int i = 0; i < myChar.length; i++) {
            if (((char) ((byte) myChar[i])) != myChar[i]) {
                num2++;
            }
        }
        return num2;
    }
}
