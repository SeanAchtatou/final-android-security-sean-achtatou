package com.city_life.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import com.city_life.sliding.fragments.KeyWordListFragment;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;

public class KeyWordActivity extends BaseActivity {
    Fragment frag = null;
    Fragment m_list_frag_1 = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_keyword);
        initFragment();
        Utils.h.postDelayed(new Runnable() {
            public void run() {
                new ClientShowAd().showAdPOP_UP(KeyWordActivity.this, 4, "");
            }
        }, 2000);
    }

    public void initFragment() {
        String str = getIntent().getStringExtra("parttpye");
        ((TextView) findViewById(R.id.title_text)).setText(str);
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(getIntent().getStringExtra("data"));
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (KeyWordListFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = KeyWordListFragment.newInstance(str, str);
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, getIntent().getStringExtra("data"));
        fragmentTransaction.commit();
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
