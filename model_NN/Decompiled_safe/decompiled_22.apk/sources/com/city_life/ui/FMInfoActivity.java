package com.city_life.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.city_life.fragment.WQInfoListFragment;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class FMInfoActivity extends BaseActivity {
    Fragment frag = null;
    Fragment m_list_frag_1 = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_wqinfo);
        initFragment();
    }

    public void initFragment() {
        Listitem item = (Listitem) getIntent().getSerializableExtra("item");
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(getIntent().getStringExtra("data"));
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (WQInfoListFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = WQInfoListFragment.newInstance(item.nid, "wangqiinfo");
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, "2131230808");
        fragmentTransaction.commit();
        ((TextView) findViewById(R.id.wq_date)).setText(Html.fromHtml(String.valueOf(item.other) + "年<font color='#000000'><big><big>" + item.other1 + "</big></big></font>月<big><big> <font color='#000000'>" + item.other2 + "</font></big></big>日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "<font color='#878787'>" + item.u_date + "</font>"));
        ((TextView) findViewById(R.id.wq_title)).setText(item.title);
        ((TextView) findViewById(R.id.wq_des)).setText(item.des);
        ImageView image = (ImageView) findViewById(R.id.wq_head);
        RelativeLayout.LayoutParams mIcon_Layout = new RelativeLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 108) / 440, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 157) / 440);
        mIcon_Layout.setMargins(0, 0, 20, 0);
        image.setLayoutParams(mIcon_Layout);
        ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + item.icon, image);
        TextView title_title = (TextView) findViewById(R.id.title_title);
        title_title.setText("封面");
        title_title.setBackgroundResource(0);
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
