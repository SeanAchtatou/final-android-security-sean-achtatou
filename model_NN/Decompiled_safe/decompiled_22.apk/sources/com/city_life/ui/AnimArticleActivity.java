package com.city_life.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.TextView;
import com.city_life.fragment.YcArticleFragment;
import com.city_life.part_asynctask.UploadUtils;
import com.city_life.view.PathButton;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.AraticleFragment;
import com.palmtrends.baseui.BaseArticleActivity;
import com.palmtrends.baseview.ImageDetailViewPager;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.MySSLSocketFactory;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import sina_weibo.WeiboShareDao;

public class AnimArticleActivity extends BaseArticleActivity implements ViewPager.OnPageChangeListener, ImageDetailViewPager.OnViewListener, ImageDetailViewPager.OnArticleOptions, PathButton.onCheckedId, View.OnTouchListener, View.OnClickListener {
    ArticlePagerAdapter apa;
    View article_day;
    TranslateAnimation botton_ta_in = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
    TranslateAnimation botton_ta_out = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
    View fav_btn;
    Fragment frag = null;
    public GestureDetector gd = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return super.onFling(e1, e2, velocityX, velocityY);
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return super.onScroll(e1, e2, distanceX, distanceY);
        }

        public boolean onDoubleTap(MotionEvent e) {
            return super.onDoubleTap(e);
        }
    });
    List<Listitem> items;
    ImageDetailViewPager mViewPager;
    Fragment m_list_frag_1 = null;
    int newtime = 0;
    int nowtime = 0;
    private PathButton pathButton;
    int position = 0;
    TranslateAnimation top_ta_in = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
    TranslateAnimation top_ta_out = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
    String total = UploadUtils.SUCCESS;
    String type = UploadUtils.SUCCESS;
    WebView wv;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_anim_article);
        initFragment();
        Utils.h.postDelayed(new Runnable() {
            public void run() {
                new ClientShowAd().showAdPOP_UP(AnimArticleActivity.this, 6, "");
                new ClientShowAd().showAdFIXED_Out(AnimArticleActivity.this, 5, "");
            }
        }, 4000);
    }

    public void initFragment() {
        this.top_ta_in.setDuration(400);
        this.top_ta_out.setDuration(400);
        this.botton_ta_in.setDuration(400);
        this.botton_ta_out.setDuration(400);
        this.position = getIntent().getIntExtra("position", 0);
        this.items = (List) getIntent().getSerializableExtra("items");
        ShareApplication.items = this.items;
        this.type = getIntent().getStringExtra("type");
        this.mCurrentItem = this.items.get(this.position);
        Listitem aditem = new Listitem();
        aditem.isad = "true";
        if (this.items.remove(aditem) && this.position != 0) {
            this.position--;
        }
        this.mViewPager = (ImageDetailViewPager) findViewById(R.id.view_pager);
        this.apa = new ArticlePagerAdapter(getSupportFragmentManager(), this.items.size());
        this.mViewPager.setAdapter(this.apa);
        this.mViewPager.setCurrentItem(this.position);
        this.mViewPager.setOffscreenPageLimit(1);
        this.mViewPager.setOnPageChangeListener(this);
        this.mViewPager.setOnViewListener(this);
        this.fav_btn = findViewById(R.id.title_fav);
        this.article_day = findViewById(R.id.title_date);
        if ("night".equals(PerfHelper.getStringData(PerfHelper.P_DATE_MODE))) {
            this.article_day.setBackgroundResource(R.drawable.article_day_btn);
        } else {
            this.article_day.setBackgroundResource(R.drawable.article_night_btn);
        }
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebView wv = (WebView) AnimArticleActivity.this.mViewPager.findViewWithTag(String.valueOf(AnimArticleActivity.this.position) + "_wb");
                if (BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(AnimArticleActivity.this.position)).toString()) == null || BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(AnimArticleActivity.this.position)).toString()).size() == 0) {
                    if (BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(AnimArticleActivity.this.position)).toString()) != null) {
                        BaseArticleActivity.o_items.put(new StringBuilder(String.valueOf(AnimArticleActivity.this.position)).toString(), null);
                    }
                    AnimArticleActivity.this.finish();
                    return;
                }
                wv.loadData("", "back", "");
            }
        });
        getComment();
    }

    public void finish() {
        super.finish();
        overridePendingTransition(R.animator.slide_right_in, R.animator.slide_right_out);
    }

    public void getComment() {
        new Thread() {
            public void run() {
                try {
                    AnimArticleActivity.this.total = AnimArticleActivity.this.getCommentCount();
                    Utils.h.post(new Runnable() {
                        public void run() {
                            TextView textView = (TextView) AnimArticleActivity.this.findViewById(R.id.title_comment_num);
                            if (!TextUtils.isEmpty(AnimArticleActivity.this.total) && !UploadUtils.SUCCESS.equals(AnimArticleActivity.this.total)) {
                                Integer.parseInt(AnimArticleActivity.this.total);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public String getCommentCount() throws Exception {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("action", "commentcount"));
        param.add(new BasicNameValuePair(LocaleUtil.INDONESIAN, this.mCurrentItem.nid));
        return new JSONObject(MySSLSocketFactory.getinfo(Urls.app_api, param)).getString("count");
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            case R.id.title_comment /*2131230803*/:
                Intent intent = new Intent();
                intent.putExtra(LocaleUtil.INDONESIAN, this.mCurrentItem.nid);
                intent.setAction(getResources().getString(R.string.activity_article_comment_list));
                startActivity(intent);
                return;
            case R.id.title_comment_num /*2131230804*/:
            default:
                return;
            case R.id.title_date /*2131230805*/:
                WebView wv2 = (WebView) this.mViewPager.findViewWithTag(String.valueOf(this.position) + "_wb");
                if ("night".equals(PerfHelper.getStringData(PerfHelper.P_DATE_MODE))) {
                    PerfHelper.setInfo(PerfHelper.P_DATE_MODE, "day");
                    this.article_day.setBackgroundResource(R.drawable.article_night_btn);
                    wv2.setBackgroundColor(getResources().getColor(17170443));
                    System.out.println("变白");
                    wv2.loadUrl("javascript:dayMode('day')");
                    return;
                }
                wv2.setBackgroundColor(getResources().getColor(17170444));
                wv2.loadUrl("javascript:dayMode('night')");
                System.out.println("变嘿");
                PerfHelper.setInfo(PerfHelper.P_DATE_MODE, "night");
                this.article_day.setBackgroundResource(R.drawable.article_day_btn);
                return;
            case R.id.title_fav /*2131230806*/:
                if (DBHelper.getDBHelper().counts("listitemfa", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
                    DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{this.mCurrentItem.n_mark});
                    Utils.showToast("已取消收藏");
                    view.setBackgroundResource(R.drawable.fav_btn);
                    return;
                }
                try {
                    this.mCurrentItem.show_type = UploadUtils.SUCCESS;
                    DBHelper.getDBHelper().insertObject(this.mCurrentItem, "listitemfa");
                    Utils.showToast("收藏成功");
                    view.setBackgroundResource(R.drawable.faved_btn);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            case R.id.title_share /*2131230807*/:
                if (!Utils.isNetworkAvailable(this)) {
                    Utils.showToast((int) R.string.network_error);
                    return;
                }
                DBHelper db = DBHelper.getDBHelper();
                if (db.counts("readitem", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
                    String[] value = {this.mCurrentItem.n_mark};
                    this.shorturl = db.select("readitem", "shorturl", "n_mark=?", value);
                    this.picurl = db.select("readitem", "share_image", "n_mark=?", value);
                }
                new AlertDialog.Builder(this).setTitle("分享方式").setItems(getResources().getStringArray(R.array.article_list_name), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String str;
                        String sname = AnimArticleActivity.this.getResources().getStringArray(R.array.article_wb_list_name_sa)[which];
                        if (sname.startsWith("wx")) {
                            if (!AnimArticleActivity.this.api.isWXAppInstalled()) {
                                new AlertDialog.Builder(AnimArticleActivity.this).setTitle("提示").setMessage("尚未安装“微信”，无法使用此功能").setPositiveButton("现在安装", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            AnimArticleActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=腾讯微信")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();
                                    }
                                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                                return;
                            }
                            if (sname.equals("wx")) {
                                AnimArticleActivity.this.weixin_type = 0;
                            } else {
                                AnimArticleActivity.this.weixin_type = 1;
                                if (AnimArticleActivity.this.api.getWXAppSupportAPI() < 553779201) {
                                    Utils.showToast("微信版本过低，暂时不支持分享到朋友圈");
                                    return;
                                }
                            }
                            if ("".equals(AnimArticleActivity.this.shorturl)) {
                                try {
                                    WeiboShareDao.weibo_get_shortid(AnimArticleActivity.this.mCurrentItem.nid, AnimArticleActivity.this.wxHandler);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                AnimArticleActivity.this.sendToWeixin();
                            }
                        } else if ("yj".equals(sname)) {
                            AnimArticleActivity.this.shareEmail(AnimArticleActivity.this.shorturl, AnimArticleActivity.this.picurl, AnimArticleActivity.this.mCurrentItem.title);
                        } else {
                            Intent i = new Intent();
                            i.setAction(AnimArticleActivity.this.getResources().getString(R.string.activity_share));
                            i.putExtra("sname", sname);
                            if (AnimArticleActivity.this.shorturl == null) {
                                str = "";
                            } else {
                                str = AnimArticleActivity.this.shorturl;
                            }
                            i.putExtra("shorturl", str);
                            i.putExtra("aid", AnimArticleActivity.this.mCurrentItem.nid);
                            i.putExtra(Constants.PARAM_TITLE, AnimArticleActivity.this.mCurrentItem.title);
                            i.putExtra(Constants.PARAM_APP_ICON, AnimArticleActivity.this.picurl == null ? "" : AnimArticleActivity.this.picurl);
                            i.putExtra("comment", AnimArticleActivity.this.mCurrentItem.des);
                            AnimArticleActivity.this.startActivity(i);
                        }
                    }
                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
        }
    }

    public void onPageSelected(int arg0) {
        new Message().what = 1;
        this.position = arg0;
        this.mCurrentItem = this.items.get(arg0);
        getComment();
        if (DBHelper.getDBHelper().counts("readitem", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
            DNDataSource.updateRead("readitem", this.mCurrentItem.n_mark, "read", "true");
        }
        WebView wv2 = (WebView) this.mViewPager.findViewWithTag(String.valueOf(this.position) + "_wb");
        if ("night".equals(PerfHelper.getStringData(PerfHelper.P_DATE_MODE))) {
            wv2.setBackgroundColor(getResources().getColor(17170444));
            wv2.loadUrl("javascript:dayMode('night')");
            PerfHelper.setInfo(PerfHelper.P_DATE_MODE, "night");
            return;
        }
        PerfHelper.setInfo(PerfHelper.P_DATE_MODE, "day");
        wv2.setBackgroundColor(getResources().getColor(17170443));
        wv2.loadUrl("javascript:dayMode('day')");
    }

    public class ArticlePagerAdapter extends FragmentStatePagerAdapter {
        private final int mSize;

        public ArticlePagerAdapter(FragmentManager fm, int size) {
            super(fm);
            this.mSize = size;
        }

        public int getCount() {
            return this.mSize;
        }

        public Fragment getItem(int position) {
            return YcArticleFragment.newInstance(position, AnimArticleActivity.this.mViewPager, AnimArticleActivity.this);
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            ((AraticleFragment) object).cancelWork();
            try {
                super.destroyItem(container, position, object);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onPageScrollStateChanged(int arg0) {
    }

    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    public void onDoubleTap() {
    }

    public void onSingleTapConfirmed() {
    }

    public void onLongPress() {
    }

    public void onLeftOption(boolean left) {
    }

    public void onRightOption(boolean right) {
    }

    public void onInitData(Listitem item) {
        this.mCurrentItem = item;
        if (DBHelper.getDBHelper().counts("listitemfa", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
            this.fav_btn.setBackgroundResource(R.drawable.faved_btn);
        } else {
            this.fav_btn.setBackgroundResource(R.drawable.fav_btn);
        }
    }

    public void onThings(int things) {
    }

    public void setCheckedId(int id) {
        switch (id) {
            case 0:
                Intent intent = new Intent();
                intent.putExtra("count", this.total);
                intent.putExtra(LocaleUtil.INDONESIAN, this.mCurrentItem.nid);
                intent.setAction(getResources().getString(R.string.activity_article_comment_list));
                startActivity(intent);
                return;
            case 1:
                if (!Utils.isNetworkAvailable(this)) {
                    Utils.showToast((int) R.string.network_error);
                    return;
                }
                DBHelper db = DBHelper.getDBHelper();
                if (db.counts("readitem", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
                    String[] value = {this.mCurrentItem.n_mark};
                    this.shorturl = db.select("readitem", "shorturl", "n_mark=?", value);
                    this.picurl = db.select("readitem", "share_image", "n_mark=?", value);
                }
                new AlertDialog.Builder(this).setTitle("分享方式").setItems(getResources().getStringArray(R.array.article_list_name), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String str;
                        String sname = AnimArticleActivity.this.getResources().getStringArray(R.array.article_wb_list_name_sa)[which];
                        if (sname.startsWith("wx")) {
                            if (!AnimArticleActivity.this.api.isWXAppInstalled()) {
                                new AlertDialog.Builder(AnimArticleActivity.this).setTitle("提示").setMessage("尚未安装“微信”，无法使用此功能").setPositiveButton("现在安装", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            AnimArticleActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=腾讯微信")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();
                                    }
                                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                                return;
                            }
                            if (sname.equals("wx")) {
                                AnimArticleActivity.this.weixin_type = 0;
                            } else {
                                AnimArticleActivity.this.weixin_type = 1;
                                if (AnimArticleActivity.this.api.getWXAppSupportAPI() < 553779201) {
                                    Utils.showToast("微信版本过低，暂时不支持分享到朋友圈");
                                    return;
                                }
                            }
                            if ("".equals(AnimArticleActivity.this.shorturl)) {
                                try {
                                    WeiboShareDao.weibo_get_shortid(AnimArticleActivity.this.mCurrentItem.nid, AnimArticleActivity.this.wxHandler);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                AnimArticleActivity.this.sendToWeixin();
                            }
                        } else if ("yj".equals(sname)) {
                            AnimArticleActivity.this.shareEmail(AnimArticleActivity.this.shorturl, AnimArticleActivity.this.picurl, AnimArticleActivity.this.mCurrentItem.title);
                        } else {
                            Intent i = new Intent();
                            i.setAction(AnimArticleActivity.this.getResources().getString(R.string.activity_share));
                            i.putExtra("sname", sname);
                            if (AnimArticleActivity.this.shorturl == null) {
                                str = "";
                            } else {
                                str = AnimArticleActivity.this.shorturl;
                            }
                            i.putExtra("shorturl", str);
                            i.putExtra("aid", AnimArticleActivity.this.mCurrentItem.nid);
                            i.putExtra(Constants.PARAM_TITLE, AnimArticleActivity.this.mCurrentItem.title);
                            i.putExtra(Constants.PARAM_APP_ICON, AnimArticleActivity.this.picurl == null ? "" : AnimArticleActivity.this.picurl);
                            i.putExtra("comment", AnimArticleActivity.this.mCurrentItem.des);
                            AnimArticleActivity.this.startActivity(i);
                        }
                    }
                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
            case 2:
                if (DBHelper.getDBHelper().counts("listitemfa", "n_mark='" + this.mCurrentItem.n_mark + "'") > 0) {
                    DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{this.mCurrentItem.n_mark});
                    Utils.showToast("已取消收藏");
                } else {
                    try {
                        this.mCurrentItem.show_type = this.type;
                        DBHelper.getDBHelper().insertObject(this.mCurrentItem, "listitemfa");
                        Utils.showToast("收藏成功");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                this.pathButton.setIamges();
                return;
            default:
                return;
        }
    }

    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    public void onClick(View v) {
    }
}
