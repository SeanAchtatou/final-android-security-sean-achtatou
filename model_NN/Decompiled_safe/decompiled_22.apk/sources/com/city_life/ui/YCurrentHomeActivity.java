package com.city_life.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import com.city_life.sliding.BaseActivity;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.ClientInfo;
import com.palmtrends.loadimage.ImageCache;
import com.palmtrends.loadimage.ImageFetcher;
import com.palmtrends.push.AlarmTools;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public abstract class YCurrentHomeActivity extends BaseActivity {
    public static String ACTIVITY_FINSH;
    FinishCastReceiver finishBroadCastReceiver;

    public abstract void things(View view);

    public YCurrentHomeActivity() {
        super(R.string.app_name);
    }

    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        if (ShareApplication.mImageWorker == null) {
            ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(ShareApplication.IMAGE_CACHE_DIR);
            ImageFetcher mImageWorker = new ImageFetcher(this, 800);
            mImageWorker.setImageCache(ImageCache.findOrCreateCache(this, cacheParams));
            ShareApplication.mImageWorker = mImageWorker;
        }
        setFinish();
        if (getResources().getBoolean(R.bool.time_tip)) {
            PerfHelper.setInfo(PerfHelper.P_ALARM_TIME, AlarmTools.getAlarmDate());
            AlarmTools.setAlarmTime(this, true);
        }
        ClientInfo.check_update(this);
    }

    static {
        ACTIVITY_FINSH = "com.palmtrends.activity.finish";
        ACTIVITY_FINSH = ShareApplication.share.getResources().getString(R.string.activity_all_finish);
    }

    public void setFinish() {
        this.finishBroadCastReceiver = new FinishCastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTIVITY_FINSH);
        registerReceiver(this.finishBroadCastReceiver, intentFilter);
    }

    public class FinishCastReceiver extends BroadcastReceiver {
        public FinishCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            YCurrentHomeActivity.this.finish();
        }
    }

    public void finish() {
        if (this.finishBroadCastReceiver != null) {
            try {
                unregisterReceiver(this.finishBroadCastReceiver);
            } catch (Exception e) {
            }
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
