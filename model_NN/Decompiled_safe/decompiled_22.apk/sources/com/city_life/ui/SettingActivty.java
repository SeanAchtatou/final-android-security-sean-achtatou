package com.city_life.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.city_life.entity.CityLifeApplication;
import com.city_life.part_activiy.FavActivity;
import com.city_life.part_activiy.HuojiangListActivity;
import com.city_life.part_activiy.SaomiaoActivity;
import com.city_life.part_activiy.UserLogin;
import com.city_life.part_activiy.UserSMS;
import com.city_life.part_activiy.YaoJiangListActivity;
import com.city_life.part_activiy.YaoYiYaoActivity;
import com.city_life.setting.FeedBack;
import com.city_life.setting.ST_ShareManager;
import com.city_life.setting.Version;
import com.imofan.android.basic.update.MFUpdateListener;
import com.imofan.android.basic.update.MFUpdateService;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.utils.FileUtils;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.io.File;
import sina_weibo.Constants;
import sina_weibo.SinaWeiboUtil;
import sina_weibo.Util;

public class SettingActivty extends BaseActivity implements View.OnClickListener {
    ImageView big;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FinalVariable.vb_success /*10000*/:
                    Utils.showToast("解除绑定成功");
                    return;
                case 10001:
                default:
                    return;
                case 10002:
                    Utils.showToast("解除绑定失败");
                    return;
            }
        }
    };
    Intent intent;
    TextView loging;
    LinearLayout manager_bangding_title;
    Handler mhand;
    ImageView middle;
    TextView push_state;
    ImageView samll;
    TextView setting_denglu_tishi;
    ImageView setting_zhuce_img;
    TextView setting_zhuce_text;
    long size = 0;
    TextView text;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        requestWindowFeature(1);
        super.onCreate(arg0);
        setContentView((int) R.layout.setting);
        init();
        this.mhand = new Handler();
        this.setting_denglu_tishi = (TextView) findViewById(R.id.setting_denglu_text);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.big_content) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "b");
            this.big.setBackgroundResource(R.drawable.check_ed);
            this.samll.setBackgroundResource(0);
            this.middle.setBackgroundResource(0);
        } else if (v.getId() == R.id.middle_content) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "m");
            this.middle.setBackgroundResource(R.drawable.check_ed);
            this.big.setBackgroundResource(0);
            this.samll.setBackgroundResource(0);
        } else if (v.getId() == R.id.small_content) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "s");
            this.big.setBackgroundResource(0);
            this.middle.setBackgroundResource(0);
            this.samll.setBackgroundResource(R.drawable.check_ed);
        }
    }

    public void init() {
        this.size = 0;
        findViewById(R.id.title_left_view).setVisibility(8);
        this.setting_zhuce_img = (ImageView) findViewById(R.id.setting_zhuce_img);
        this.setting_zhuce_text = (TextView) findViewById(R.id.setting_zhuce_text);
        this.loging = (TextView) findViewById(R.id.setting_user_send);
        getFilesInfo(FileUtils.sdPath);
        findViewById(R.id.title_btn_right).setVisibility(8);
        findViewById(R.id.title_back).setVisibility(0);
        ((TextView) findViewById(R.id.title_title)).setText("更多");
        this.text = (TextView) findViewById(R.id.st_clear_size);
        this.text.setText(FileUtils.formatFileSize((long) ((int) this.size)));
        findViewById(R.id.setting_user_send).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingActivty.this.things(v);
            }
        });
        findViewById(R.id.setting_zhuce_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingActivty.this.things(v);
            }
        });
        findViewById(R.id.setting_wpde_jianghao).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingActivty.this.things(v);
            }
        });
        findViewById(R.id.setting_huojiang_msg).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingActivty.this.things(v);
            }
        });
        findViewById(R.id.setting_check_upload).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingActivty.this.things(v);
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.utils.PerfHelper.setInfo(java.lang.String, int):void
      com.utils.PerfHelper.setInfo(java.lang.String, long):void
      com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
      com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            case R.id.setting_zhuce_btn /*2131231146*/:
                this.intent = new Intent();
                if (PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN)) {
                    PerfHelper.setInfo(PerfHelper.P_USER_LOGIN, false);
                    PerfHelper.setInfo(PerfHelper.P_USERID, "");
                    PerfHelper.setInfo(PerfHelper.P_SHARE_AGE, "");
                    PerfHelper.setInfo(PerfHelper.P_SHARE_NAME, "");
                    PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE, "");
                    PerfHelper.setInfo(PerfHelper.P_SHARE_SEX, "");
                    PerfHelper.setInfo(PerfHelper.P_USER_LOGIN, false);
                    CityLifeApplication.fav_list_id = null;
                    Utils.showToast("注销登录成功");
                    this.setting_zhuce_text.setText("注册");
                    this.setting_zhuce_img.setBackgroundResource(R.drawable.zhuche_btn);
                    this.loging.setText("登录");
                    this.setting_denglu_tishi.setText("登录就有机会中大奖哦!!!");
                    ((TextView) findViewById(R.id.setting_user_name)).setText("");
                    return;
                }
                this.intent.setClass(this, UserSMS.class);
                startActivity(this.intent);
                return;
            case R.id.setting_user_send /*2131231152*/:
                this.intent = new Intent();
                if (PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN)) {
                    this.intent.setClass(this, YaoYiYaoActivity.class);
                } else {
                    this.intent.setClass(this, UserLogin.class);
                }
                startActivity(this.intent);
                return;
            case R.id.setting_geren /*2131231155*/:
                this.intent = new Intent();
                this.intent.setClass(this, YaoYiYaoActivity.class);
                startActivity(this.intent);
                return;
            case R.id.setting_share /*2131231158*/:
                this.intent = new Intent();
                this.intent.setClass(this, ST_ShareManager.class);
                startActivity(this.intent);
                return;
            case R.id.setting_wpde_jianghao /*2131231161*/:
                if (!PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN) || PerfHelper.getStringData(PerfHelper.P_USERID).equals("游客")) {
                    Utils.showToast("我的奖号必需登录账号才能查看");
                    return;
                }
                this.intent = new Intent();
                this.intent.setClass(this, YaoJiangListActivity.class);
                startActivity(this.intent);
                return;
            case R.id.setting_huojiang_msg /*2131231163*/:
                if (!PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN) || PerfHelper.getStringData(PerfHelper.P_USERID).equals("游客")) {
                    Utils.showToast("获奖信息必需登录账号才能查看");
                    return;
                }
                this.intent = new Intent();
                this.intent.setClass(this, HuojiangListActivity.class);
                startActivity(this.intent);
                return;
            case R.id.setting_feedback /*2131231164*/:
                this.intent = new Intent();
                this.intent.setClass(this, FeedBack.class);
                startActivity(this.intent);
                return;
            case R.id.setting_fav /*2131231166*/:
                if (!PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN) || PerfHelper.getStringData(PerfHelper.P_USERID).equals("游客")) {
                    Utils.showToast("收藏必需登录账号才能查看");
                    return;
                }
                this.intent = new Intent();
                this.intent.setClass(this, FavActivity.class);
                startActivity(this.intent);
                return;
            case R.id.setting_erweima /*2131231168*/:
                startActivity(new Intent(this, SaomiaoActivity.class));
                return;
            case R.id.setting_check_upload /*2131231170*/:
                MFUpdateService.check(this, new MFUpdateListener() {
                    public void onWifiOff(Activity activity) {
                        Utils.showToast("你目前版本是最新版本");
                    }

                    public void onFailed(Activity activity) {
                        Utils.showToast("你目前版本是最新版本");
                    }

                    public void onDetectedNothing(Activity activity) {
                        Utils.showToast("你目前版本是最新版本");
                    }

                    public void onDetectedNewVersion(Activity activity, int versionCode, String versionName, String description, final String apk) {
                        Log.v("onDetectedNothing :", "检查有更新");
                        if (versionCode > Integer.parseInt(ShareApplication.getVersion().replace(".", ""))) {
                            SettingActivty.this.mhand.post(new Runnable() {
                                public void run() {
                                    AlertDialog.Builder message = new AlertDialog.Builder(SettingActivty.this).setMessage("发现新版本，是否更新");
                                    final String str = apk;
                                    message.setPositiveButton("是", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (!str.startsWith("http://")) {
                                                Utils.showToast("参数错误");
                                                return;
                                            }
                                            try {
                                                SettingActivty.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                                            } catch (Exception e) {
                                                Utils.showToast("请安装浏览器");
                                                e.printStackTrace();
                                            }
                                            Utils.dismissProcessDialog();
                                        }
                                    }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface arg0, int arg1) {
                                        }
                                    }).show();
                                }
                            });
                        } else {
                            Utils.showToast("你目前版本是最新版本");
                        }
                    }

                    public void onPerformUpdate(Activity activity, int versionCode, String versionName, String description, String apk) {
                    }
                }, false, true);
                return;
            case R.id.setting_about /*2131231172*/:
                this.intent = new Intent();
                this.intent.setClass(this, Version.class);
                startActivity(this.intent);
                return;
            case R.id.setting_clear /*2131231173*/:
                new AlertDialog.Builder(this).setMessage("是否清除缓存？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.showProcessDialog(SettingActivty.this, "正在清除缓存...");
                        new Thread() {
                            public void run() {
                                ShareApplication.mImageWorker.mImageCache.clearCaches();
                                SettingActivty.this.deleteFilesInfo(FileUtils.sdPath);
                                DBHelper.getDBHelper().deleteall(SettingActivty.this.getApplicationContext().getResources().getStringArray(R.array.app_sql_delete));
                                Utils.dismissProcessDialog();
                                Utils.showToast("缓存清理完毕");
                                Utils.h.post(new Runnable() {
                                    public void run() {
                                        SettingActivty.this.text.setText("0kb");
                                    }
                                });
                            }
                        }.start();
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
                return;
            default:
                return;
        }
    }

    public void deleteFilesInfo(String str) {
        File f = new File(str);
        if (f.listFiles() != null) {
            for (File file : f.listFiles()) {
                if (file.isDirectory()) {
                    deleteFilesInfo(file.getAbsolutePath());
                } else {
                    file.delete();
                }
            }
        }
    }

    private void getFilesInfo(String str) {
        File f = new File(str);
        if (f.listFiles() != null) {
            for (File file : f.listFiles()) {
                if (file.isDirectory()) {
                    getFilesInfo(file.getAbsolutePath());
                } else {
                    this.size += file.length();
                }
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.title_back)).setText(PerfHelper.getStringData(PerfHelper.P_CITY));
        if (PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN)) {
            this.loging.setText("摇一摇");
            this.setting_zhuce_text.setText("注销");
            this.setting_zhuce_img.setBackgroundResource(R.drawable.setting_zhuxiao_n);
            ((TextView) findViewById(R.id.setting_user_name)).setText(PerfHelper.getStringData(PerfHelper.P_SHARE_NAME));
            this.setting_denglu_tishi.setText("您已经登录赶紧去摇一摇吧!!!");
            return;
        }
        this.loging.setText("登录");
        this.setting_zhuce_text.setText("注册");
        this.setting_zhuce_img.setBackgroundResource(R.drawable.zhuche_btn);
        ((TextView) findViewById(R.id.setting_user_name)).setText("");
        this.setting_denglu_tishi.setText("登录就有机会中大奖哦!!!");
    }

    public void initShare() {
        this.manager_bangding_title = (LinearLayout) findViewById(R.id.manager_content);
        this.manager_bangding_title.removeAllViews();
        String[] array = getResources().getStringArray(R.array.article_wb_list_name_sa);
        int number = count;
        for (String item : array) {
            if (item.equals("wx")) {
                number--;
            }
        }
        for (String item2 : array) {
            if (item2.startsWith("wx")) {
                number--;
            } else {
                View view = LayoutInflater.from(this).inflate((int) R.layout.listitem_share, (ViewGroup) null);
                ImageView iv = (ImageView) view.findViewById(R.id.manager_icon);
                TextView title = (TextView) view.findViewById(R.id.manager_title);
                TextView name = (TextView) view.findViewById(R.id.manager_name);
                TextView btn = (TextView) view.findViewById(R.id.manager_btn);
                this.manager_bangding_title.addView(view);
                btn.setTag(item2);
                if ("sina".equals(item2)) {
                    title.setText("新浪微博");
                    iv.setImageResource(R.drawable.icon_sina);
                    if (!TextUtils.isEmpty(PerfHelper.getStringData(Constants.PREF_SINA_ACCESS_TOKEN))) {
                        btn.setBackgroundResource(R.drawable.bind_binded);
                        name.setText(PerfHelper.getStringData(Constants.PREF_SINA_USER_NAME));
                        if (PerfHelper.getStringData(Constants.PREF_SINA_USER_NAME_IMG).length() > 2) {
                            ShareApplication.mImageWorker.loadImage(PerfHelper.getStringData(Constants.PREF_SINA_USER_NAME_IMG), iv);
                        } else {
                            iv.setImageResource(R.drawable.icon_sina);
                        }
                    } else {
                        name.setText("");
                        btn.setBackgroundResource(R.drawable.bind_bind);
                    }
                } else if ("qq".equals(item2)) {
                    title.setText("腾讯微博");
                    iv.setImageResource(R.drawable.icon_qq);
                    if (!TextUtils.isEmpty(Util.getSharePersistent(getApplicationContext(), "ACCESS_TOKEN"))) {
                        btn.setBackgroundResource(R.drawable.bind_binded);
                        name.setText(PerfHelper.getStringData(Constants.PREF_TX_NAME));
                        if ((String.valueOf(PerfHelper.getStringData(Constants.PREF_TX_USER_NAME_IMG)) + "/120").length() > 5) {
                            ShareApplication.mImageWorker.loadImage(String.valueOf(PerfHelper.getStringData(Constants.PREF_TX_USER_NAME_IMG)) + "/120", iv);
                        } else {
                            iv.setImageResource(R.drawable.icon_qq);
                        }
                    } else {
                        name.setText("");
                        btn.setBackgroundResource(R.drawable.bind_bind);
                    }
                } else if ("renren".equals(item2)) {
                    title.setText("人人网");
                } else if ("kaixin".equals(item2)) {
                    title.setText("开心网");
                }
            }
        }
    }

    private boolean doSinaType(String packageName, int versionCode) {
        for (PackageInfo pi : getPackageManager().getInstalledPackages(8192)) {
            String pi_packageName = pi.packageName;
            int pi_versionCode = pi.versionCode;
            if (packageName.endsWith(pi_packageName)) {
                if (versionCode > pi_versionCode) {
                    return false;
                }
                if (versionCode <= pi_versionCode) {
                    return true;
                }
            }
        }
        Log.i("test", "未安装该应用，可以安装");
        return false;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 32973) {
            SinaWeiboUtil.getInstance(this).authCallBack(requestCode, resultCode, data);
            if (!TextUtils.isEmpty(PerfHelper.getStringData(Constants.PREF_SINA_ACCESS_TOKEN))) {
                Utils.showToast("绑定成功");
            }
        }
    }
}
