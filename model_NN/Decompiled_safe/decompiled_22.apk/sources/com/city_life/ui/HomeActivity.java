package com.city_life.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import com.city_life.fragment.HomeFragment;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.baseui.BaseHomeActivity;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;

public class HomeActivity extends BaseHomeActivity {
    Animation alpha = new AlphaAnimation(1.0f, 0.0f);
    Fragment frag = null;
    boolean isAnim = true;
    View mOld;
    Fragment m_list_frag_1 = null;
    LinearLayout navigation;

    public static int dip2px(Context context, float dpValue) {
        return (int) ((dpValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        return (int) ((pxValue / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_home);
        this.navigation = (LinearLayout) findViewById(R.id.navigation);
        initFragment();
        Utils.h.postDelayed(new Runnable() {
            public void run() {
                new ClientShowAd().showAdPOP_UP(HomeActivity.this, 4, "");
            }
        }, 2000);
    }

    public void initFragment() {
        this.alpha.setDuration(350);
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(getIntent().getStringExtra("data"));
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (HomeFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = HomeFragment.newInstance("shenghuo", "home");
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, "2131230808");
        fragmentTransaction.commit();
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_setting /*2131230841*/:
                Intent intent = new Intent();
                intent.setAction(getResources().getString(R.string.activity_setting));
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    public void changePart(View view) {
        if (this.mOld != null) {
            this.mOld.setSelected(false);
        }
        this.mOld = view;
        if (this.isAnim) {
            int count = this.navigation.getChildCount();
            for (int i = 0; i < count; i++) {
                this.isAnim = false;
                final View v = this.navigation.getChildAt(i);
                if (v.getId() != view.getId()) {
                    AnimationSet set = new AnimationSet(true);
                    Animation a = new TranslateAnimation(0.0f, (float) ((view.getLeft() + (view.getWidth() / 2)) - (v.getLeft() + (v.getWidth() / 2))), 0.0f, 0.0f);
                    a.setDuration(400);
                    set.addAnimation(a);
                    set.addAnimation(this.alpha);
                    this.alpha.setAnimationListener(new Animation.AnimationListener() {
                        public void onAnimationStart(Animation animation) {
                        }

                        public void onAnimationRepeat(Animation animation) {
                        }

                        public void onAnimationEnd(Animation animation) {
                            v.setVisibility(4);
                        }
                    });
                    v.startAnimation(set);
                    a.setAnimationListener(new Animation.AnimationListener() {
                        public void onAnimationStart(Animation animation) {
                        }

                        public void onAnimationRepeat(Animation animation) {
                        }

                        public void onAnimationEnd(Animation animation) {
                            v.setVisibility(0);
                            HomeActivity.this.isAnim = true;
                        }
                    });
                }
            }
            view.setSelected(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        int count = this.navigation.getChildCount();
        for (int i = 0; i < count; i++) {
            this.navigation.getChildAt(i).setVisibility(0);
        }
        this.navigation.postInvalidate();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
