package com.city_life.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.baidu.android.pushservice.PushConstants;
import com.city_life.fragment.PicDetailFragment;
import com.city_life.part_asynctask.UploadUtils;
import com.city_life.view.PathButton;
import com.palmtrends.apptime.SetApptime;
import com.palmtrends.baseview.ImageDetailViewPager;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.MySSLSocketFactory;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.ImageCache;
import com.palmtrends.loadimage.ImageFetcher;
import com.palmtrends.loadimage.ImageResizer;
import com.palmtrends.loadimage.Utils;
import com.palmtrends.zoom.GestureImageView;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.utils.FileUtils;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sina_weibo.WeiboShareDao;

public class PicInfoActivity extends FragmentActivity implements GestureImageView.OnPageChangeCallback, PathButton.onCheckedId {
    private static final String EXTRA_IMAGE = "extra_image";
    private static final String IMAGE_CACHE_DIR = "bigimages";
    public static final int TIMELINE_SUPPORTED_VERSION = 553779201;
    public static final int WEIXIN_ONE = 0;
    public static final int WEIXIN_QUAN = 1;
    public static ImageResizer mImageWorker;
    public static List<Listitem> pics = null;
    public IWXAPI api;
    /* access modifiers changed from: private */
    public Listitem current_image;
    private int current_index = 0;
    /* access modifiers changed from: private */
    public int currents = 0;
    private DBHelper db;
    private TextView des_content;
    private TextView des_date;
    private TextView des_index;
    private TextView des_title;
    private View fav_btn;
    private Fragment frag = null;
    private boolean hasBottonAnimation = true;
    private boolean hasTopAnimation = true;
    String[] imglist;
    private ImageView indicator;
    private boolean isFullScreen = false;
    private List<Listitem> items;
    int length;
    private View load = null;
    /* access modifiers changed from: private */
    public View loading;
    /* access modifiers changed from: private */
    public ImagePagerAdapter mAdapter;
    /* access modifiers changed from: private */
    public Listitem mCurrentItem;
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 99:
                    if (msg.obj != null) {
                        FileUtils.writeToFile((Bitmap) msg.obj, PicInfoActivity.this.current_image.icon);
                        return;
                    } else {
                        Utils.showToast("下载失败");
                        return;
                    }
                case FinalVariable.update /*1001*/:
                    PicInfoActivity.this.loading.setVisibility(8);
                    PicInfoActivity.this.update();
                    return;
                case FinalVariable.remove_footer /*1002*/:
                    PicInfoActivity.this.loading.setVisibility(8);
                    return;
                case FinalVariable.error /*1004*/:
                    Utils.showToast((int) R.string.network_error);
                    return;
                case FinalVariable.nomore /*1007*/:
                    PicInfoActivity.this.loading.setVisibility(8);
                    Utils.showToast((int) R.string.no_data);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public ImageDetailViewPager mViewPager;
    private Fragment m_list_frag_1 = null;
    private int position = 0;
    private ProgressBar processbar;
    private TextView reload;
    private Date start_time;
    private SetApptime time;
    private View top = null;
    String total = UploadUtils.SUCCESS;
    private Vibrator vibrator;
    public int weixin_type = 0;
    public Handler wxHandler = new Handler() {
        public void handleMessage(Message message) {
            int i;
            if (message.what == 1001) {
                WXWebpageObject webpage = new WXWebpageObject();
                webpage.webpageUrl = PicInfoActivity.pics.get(PicInfoActivity.this.currents).icon.trim();
                WXMediaMessage msg = new WXMediaMessage(webpage);
                msg.title = PicInfoActivity.this.mCurrentItem.title;
                msg.description = PicInfoActivity.this.mCurrentItem.des;
                if (message.obj != null) {
                    msg.setThumbImage((Bitmap) message.obj);
                }
                SendMessageToWX.Req req = new SendMessageToWX.Req();
                req.transaction = PicInfoActivity.this.buildTransaction("image");
                req.message = msg;
                if (PicInfoActivity.this.weixin_type == 1) {
                    i = 1;
                } else {
                    i = 0;
                }
                req.scene = i;
                PicInfoActivity.this.api.sendReq(req);
            } else if (message.what == 10014) {
                PicInfoActivity.this.sendToWeixin();
            } else if (message.what == 10002) {
                Utils.showToast("资源获取失败！");
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int longest;
        requestWindowFeature(1);
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_pic);
        this.db = DBHelper.getDBHelper();
        this.vibrator = (Vibrator) getSystemService("vibrator");
        if (mImageWorker == null) {
            int height = getResources().getDisplayMetrics().heightPixels;
            int width = getResources().getDisplayMetrics().widthPixels;
            if (height > width) {
                longest = height;
            } else {
                longest = width;
            }
            mImageWorker = new ImageFetcher(this, longest);
            mImageWorker.setImageCache(ImageCache.findOrCreateCache(this, new ImageCache.ImageCacheParams(IMAGE_CACHE_DIR)));
            mImageWorker.setImageFadeIn(false);
        }
        String appId = getResources().getString(R.string.wx_app_id);
        this.api = WXAPIFactory.createWXAPI(this, appId, false);
        this.api.registerApp(appId);
        findView();
        this.mHandler.sendEmptyMessage(FinalVariable.update);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (PicInfoActivity.this.mAdapter != null) {
                    PicInfoActivity.this.mAdapter.notifyDataSetChanged();
                }
            }
        }, 300);
    }

    public void findView() {
        this.position = getIntent().getIntExtra("position", 0);
        this.mCurrentItem = (Listitem) getIntent().getSerializableExtra("item");
        this.mViewPager = (ImageDetailViewPager) findViewById(R.id.view_pager);
        this.loading = findViewById(R.id.loading);
        this.des_title = (TextView) findViewById(R.id.draw_title);
        this.des_index = (TextView) findViewById(R.id.draw_title_index);
        this.des_content = (TextView) findViewById(R.id.draw_content);
        this.des_date = (TextView) findViewById(R.id.draw_date);
        this.load = findViewById(R.id.loading);
        this.top = findViewById(R.id.title);
        setDrawdesLayoutParam();
        initloadprocess();
        this.fav_btn = findViewById(R.id.title_fav);
    }

    public void setDrawdesLayoutParam() {
    }

    public void initloadprocess() {
        this.processbar = (ProgressBar) findViewById(R.id.processbar);
        this.load.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                return true;
            }
        });
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                onKeyDown(4, null);
                return;
            case R.id.title_fav /*2131230806*/:
            default:
                return;
            case R.id.title_share /*2131230807*/:
                if (!Utils.isNetworkAvailable(this)) {
                    Utils.showToast((int) R.string.network_error);
                    return;
                } else if (pics != null) {
                    new AlertDialog.Builder(this).setTitle("分享方式").setItems(getResources().getStringArray(R.array.article_list_name), new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                         arg types: [java.lang.String, int]
                         candidates:
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                        public void onClick(DialogInterface dialog, int which) {
                            String shortID;
                            if (!Utils.isNetworkAvailable(PicInfoActivity.this)) {
                                Utils.showToast((int) R.string.network_error);
                            } else if (PicInfoActivity.pics != null && PicInfoActivity.pics.size() != 0) {
                                if (PicInfoActivity.pics == null || PicInfoActivity.pics.get(PicInfoActivity.this.currents).title == null || "".equals(PicInfoActivity.pics.get(PicInfoActivity.this.currents).title) || "null".equals(PicInfoActivity.pics.get(PicInfoActivity.this.currents).title)) {
                                    shortID = PicInfoActivity.this.mCurrentItem.title;
                                } else {
                                    shortID = PicInfoActivity.pics.get(PicInfoActivity.this.currents).title;
                                }
                                String sname = PicInfoActivity.this.getResources().getStringArray(R.array.article_wb_list_name_sa)[which];
                                Intent i = new Intent();
                                i.setAction(PicInfoActivity.this.getResources().getString(R.string.activity_share));
                                i.putExtra("sname", sname);
                                i.putExtra("aid", PicInfoActivity.this.mCurrentItem.nid);
                                i.putExtra(Constants.PARAM_TITLE, PicInfoActivity.this.mCurrentItem.title);
                                i.putExtra(Constants.PARAM_APP_ICON, PicInfoActivity.pics.get(PicInfoActivity.this.currents).icon);
                                i.putExtra("shorturl", shortID);
                                i.putExtra("ispic", UploadUtils.FAILURE);
                                i.putExtra("isArticle", false);
                                PicInfoActivity.this.startActivity(i);
                            }
                        }
                    }).setNegativeButton(getResources().getString(R.string.cancel), (DialogInterface.OnClickListener) null).show();
                    return;
                } else {
                    return;
                }
            case R.id.title_download /*2131230856*/:
                if (this.current_image != null) {
                    Bitmap bt = mImageWorker.mImageCache.getBitmapFromMemCache(String.valueOf(Urls.main) + this.current_image.icon);
                    if (bt == null) {
                        bt = mImageWorker.getImageCache().getBitmapFromDiskCache(String.valueOf(Urls.main) + this.current_image.icon);
                    }
                    if (bt == null) {
                        Utils.showToast((int) R.string.draw_load_toast);
                        return;
                    }
                    Utils.showProcessDialog(this, (int) R.string.draw_load_down);
                    FileUtils.writeToFile(bt, this.current_image.icon);
                    return;
                }
                return;
        }
    }

    public void update() {
        int width = PerfHelper.getIntData(PerfHelper.P_PHONE_W);
        System.out.println("DIANSJ:" + this.mCurrentItem.list_type);
        this.imglist = getIntent().getStringArrayExtra("img_list");
        this.length = width / this.imglist.length;
        this.mAdapter = new ImagePagerAdapter(getSupportFragmentManager(), this.imglist);
        this.mViewPager.setOnViewListener(new ImageDetailViewPager.OnViewListener() {
            public void onSingleTapConfirmed() {
                PicInfoActivity.this.hidetitle();
            }

            public void onLongPress() {
            }

            public void onDoubleTap() {
            }

            public void onLeftOption(boolean left) {
            }

            public void onRightOption(boolean right) {
            }
        });
        this.mViewPager.setAdapter(this.mAdapter);
        this.mViewPager.setPageMargin((int) getResources().getDimension(R.dimen.image_detail_pager_margin));
        changeIndex(0);
    }

    public void initData() {
        new Thread() {
            public void run() {
                try {
                    PicInfoActivity.pics = PicInfoActivity.this.getDataFromNet(PicInfoActivity.this.mCurrentItem.nid, PicInfoActivity.this.mCurrentItem.n_mark);
                    if (PicInfoActivity.pics != null) {
                        PicInfoActivity.this.mHandler.sendEmptyMessage(FinalVariable.update);
                    } else {
                        PicInfoActivity.this.mHandler.sendEmptyMessage(FinalVariable.nomore);
                    }
                } catch (Exception e) {
                    if (e instanceof JSONException) {
                        PicInfoActivity.this.mHandler.sendEmptyMessage(FinalVariable.nomore);
                    } else {
                        PicInfoActivity.this.mHandler.sendEmptyMessage(FinalVariable.error);
                    }
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public List getDataFromNet(String id, String n_mark) throws Exception {
        String json = DNDataSource.list_FromDB(String.valueOf(id) + "_" + n_mark, 0, 1000, n_mark);
        this.start_time = new Date();
        if (json == null || "".equals(json) || "null".equals(json)) {
            String url = Urls.draw_content_url;
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair(PushConstants.EXTRA_GID, id));
            String json2 = MySSLSocketFactory.getinfo(url, param);
            if (json2 == null || "".equals(json2) || "null".equals(json2)) {
                return null;
            }
            String json3 = json2.replaceAll("'", "‘");
            this.time = new SetApptime(this.start_time, "article", id, "net");
            Data data = parseJson(json3, id);
            if (!(data == null || data.list == null || data.list.size() <= 0)) {
                this.db.insert(String.valueOf(id) + "_" + n_mark + UploadUtils.SUCCESS, json3, String.valueOf(id) + "_" + n_mark);
            }
            return data.list;
        }
        Data d = parseJson(json, id);
        this.time = new SetApptime(this.start_time, "article", id, "cache");
        return d.list;
    }

    public Data parseJson(String json, String type) throws Exception {
        Data data = new Data();
        JSONArray jsonay = new JSONArray(json);
        List<Listitem> al = new ArrayList<>();
        int count = jsonay.length();
        for (int i = 0; i < count; i++) {
            Listitem o = new Listitem();
            JSONObject obj = jsonay.getJSONObject(i);
            o.sa = type;
            o.icon = obj.getString("icon");
            try {
                if (obj.has(LocaleUtil.INDONESIAN)) {
                    o.nid = obj.getString(LocaleUtil.INDONESIAN);
                } else {
                    o.nid = type;
                }
                if (obj.has(Constants.PARAM_TITLE)) {
                    o.title = obj.getString(Constants.PARAM_TITLE).replaceAll("'", "‘");
                }
                if (obj.has("des")) {
                    o.des = obj.getString("des").replaceAll("'", "‘");
                }
                if (obj.has("adddate")) {
                    o.u_date = obj.getString("adddate");
                }
                if (obj.has("author")) {
                    o.author = obj.getString("author").replaceAll("'", "‘");
                }
            } catch (Exception e) {
            }
            o.getMark();
            al.add(o);
        }
        if (al.size() <= 0) {
            return null;
        }
        data.list = al;
        return data;
    }

    public void changeIndex(int index) {
        try {
            if (this.imglist != null) {
                this.des_index.setText(String.valueOf(index + 1) + CookieSpec.PATH_DELIM + this.imglist.length);
                this.current_image = pics.get(index);
            }
            this.currents = index;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startLeftMove(int currents2) {
        if (this.isFullScreen) {
            AnimationSet animSet = new AnimationSet(false);
            this.indicator.setVisibility(0);
            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
            alpha.setStartOffset(500);
            alpha.setDuration(400);
            animSet.addAnimation(alpha);
            Animation left_move = new TranslateAnimation((float) (this.length * currents2), (float) ((currents2 - 1) * this.length), 0.0f, 0.0f);
            left_move.setStartOffset(100);
            left_move.setDuration(500);
            left_move.setFillAfter(true);
            animSet.addAnimation(left_move);
            animSet.setFillAfter(true);
            this.indicator.startAnimation(animSet);
        }
    }

    private void startRightMove(int currents2) {
        if (this.isFullScreen) {
            AnimationSet animSet = new AnimationSet(false);
            this.indicator.setVisibility(0);
            AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
            alpha.setStartOffset(500);
            alpha.setDuration(400);
            animSet.addAnimation(alpha);
            Animation right_move = new TranslateAnimation((float) (this.length * currents2), (float) ((currents2 + 1) * this.length), 0.0f, 0.0f);
            right_move.setStartOffset(100);
            right_move.setDuration(500);
            right_move.setFillAfter(true);
            animSet.addAnimation(right_move);
            animSet.setFillAfter(true);
            this.indicator.startAnimation(animSet);
        }
    }

    public void hidetitle() {
        this.vibrator.vibrate(30);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    private class ImagePagerAdapter extends FragmentStatePagerAdapter {
        private final String[] imgslist;
        private final int mSize;

        public ImagePagerAdapter(FragmentManager fm, String[] imglist) {
            super(fm);
            this.mSize = imglist.length;
            this.imgslist = imglist;
        }

        public int getCount() {
            return this.mSize;
        }

        public Fragment getItem(int position) {
            System.out.println("大小:" + this.mSize + CookieSpec.PATH_DELIM + this.imgslist[position]);
            return PicDetailFragment.newInstance(this.imgslist[position], PicInfoActivity.this.mViewPager, PicInfoActivity.this);
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            ((PicDetailFragment) object).cancelWork();
            try {
                super.destroyItem(container, position, object);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getCommentCount() throws Exception {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("action", "commentcount"));
        param.add(new BasicNameValuePair(LocaleUtil.INDONESIAN, this.mCurrentItem.nid));
        return new JSONObject(MySSLSocketFactory.getinfo(Urls.app_api, param)).getString("count");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.time != null) {
            this.time.up_end_time(this.start_time, new Date(), this.mCurrentItem.nid, UploadUtils.FAILURE);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.time != null) {
            this.time.up_end_time(this.start_time, new Date(), this.mCurrentItem.nid, UploadUtils.SUCCESS);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    public void setCheckedId(int id) {
        System.out.println("---------------:" + id);
        switch (id) {
            case 0:
                Intent intent = new Intent();
                intent.putExtra("count", this.total);
                intent.putExtra(LocaleUtil.INDONESIAN, this.mCurrentItem.nid);
                intent.setAction(getResources().getString(R.string.activity_article_comment_list));
                startActivity(intent);
                return;
            case 1:
                if (!Utils.isNetworkAvailable(this)) {
                    Utils.showToast((int) R.string.network_error);
                    return;
                } else if (pics != null) {
                    new AlertDialog.Builder(this).setTitle("分享方式").setItems(getResources().getStringArray(R.array.article_list_name), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String shortID;
                            String str;
                            if (PicInfoActivity.pics == null || PicInfoActivity.pics.get(PicInfoActivity.this.currents).title == null || "".equals(PicInfoActivity.pics.get(PicInfoActivity.this.currents).title) || "null".equals(PicInfoActivity.pics.get(PicInfoActivity.this.currents).title)) {
                                shortID = PicInfoActivity.this.mCurrentItem.title;
                            } else {
                                shortID = PicInfoActivity.pics.get(PicInfoActivity.this.currents).title;
                            }
                            String sname = PicInfoActivity.this.getResources().getStringArray(R.array.article_wb_list_name_sa)[which];
                            if (sname.startsWith("wx")) {
                                if (!PicInfoActivity.this.api.isWXAppInstalled()) {
                                    new AlertDialog.Builder(PicInfoActivity.this).setTitle("提示").setMessage("尚未安装“微信”，无法使用此功能").setPositiveButton("现在安装", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            try {
                                                PicInfoActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=腾讯微信")));
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            dialog.dismiss();
                                        }
                                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                                    return;
                                }
                                if (sname.equals("wx")) {
                                    PicInfoActivity.this.weixin_type = 0;
                                } else {
                                    PicInfoActivity.this.weixin_type = 1;
                                    if (PicInfoActivity.this.api.getWXAppSupportAPI() < 553779201) {
                                        Utils.showToast("微信版本过低，暂时不支持分享到朋友圈");
                                        return;
                                    }
                                }
                                if ("".equals(shortID)) {
                                    try {
                                        WeiboShareDao.weibo_get_shortid(PicInfoActivity.this.mCurrentItem.nid, PicInfoActivity.this.wxHandler);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    PicInfoActivity.this.sendToWeixin();
                                }
                            } else if (!"yj".equals(sname)) {
                                Intent i = new Intent();
                                i.setAction(PicInfoActivity.this.getResources().getString(R.string.activity_share));
                                i.putExtra("sname", sname);
                                if (shortID == null) {
                                    shortID = "";
                                }
                                i.putExtra("shorturl", shortID);
                                i.putExtra("aid", PicInfoActivity.this.mCurrentItem.nid);
                                i.putExtra(Constants.PARAM_TITLE, PicInfoActivity.this.mCurrentItem.title);
                                if (PicInfoActivity.pics.get(PicInfoActivity.this.currents).icon == null) {
                                    str = "";
                                } else {
                                    str = PicInfoActivity.pics.get(PicInfoActivity.this.currents).icon;
                                }
                                i.putExtra(Constants.PARAM_APP_ICON, str);
                                i.putExtra("comment", PicInfoActivity.this.mCurrentItem.des);
                                PicInfoActivity.this.startActivity(i);
                            }
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                    return;
                } else {
                    return;
                }
            case 2:
                if (this.current_image != null) {
                    Bitmap bt = mImageWorker.mImageCache.getBitmapFromMemCache(String.valueOf(Urls.main) + this.current_image.icon);
                    if (bt == null) {
                        bt = mImageWorker.getImageCache().getBitmapFromDiskCache(String.valueOf(Urls.main) + this.current_image.icon);
                    }
                    if (bt == null) {
                        Utils.showToast((int) R.string.draw_load_toast);
                        return;
                    }
                    Utils.showProcessDialog(this, (int) R.string.draw_load_down);
                    FileUtils.writeToFile(bt, this.current_image.icon);
                    return;
                }
                return;
            case 3:
                if (this.db.counts("listitemfa", "nid='" + this.mCurrentItem.nid + "' and show_type='" + "1'") > 0) {
                    this.db.delete("listitemfa", "n_mark=? and show_type=?", new String[]{this.mCurrentItem.n_mark, UploadUtils.FAILURE});
                    Utils.showToast("已取消收藏");
                    return;
                }
                try {
                    this.mCurrentItem.show_type = UploadUtils.FAILURE;
                    this.db.insertObject(this.mCurrentItem, "listitemfa");
                    Utils.showToast("收藏成功");
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            default:
                return;
        }
    }

    public void sendToWeixin() {
        new Thread() {
            public void run() {
                String url = "";
                Bitmap bm = null;
                if (PicInfoActivity.pics.get(PicInfoActivity.this.currents).icon != null && !"".equals(PicInfoActivity.pics.get(PicInfoActivity.this.currents).icon) && !"null".equals(PicInfoActivity.pics.get(PicInfoActivity.this.currents).icon) && !PicInfoActivity.pics.get(PicInfoActivity.this.currents).icon.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                    url = String.valueOf(Urls.main) + PicInfoActivity.pics.get(PicInfoActivity.this.currents).icon;
                }
                if (!"".equals(url)) {
                    String imagename = FileUtils.converPathToName(url);
                    if (FileUtils.isFileExist("image/" + imagename)) {
                        try {
                            bm = BitmapFactory.decodeFile(String.valueOf(FileUtils.sdPath) + "image/" + imagename);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Drawable drawable = Drawable.createFromStream(new URL(url).openStream(), "image.png");
                            if (drawable != null) {
                                bm = ((BitmapDrawable) drawable).getBitmap();
                            }
                        } catch (MalformedURLException e2) {
                            e2.printStackTrace();
                        } catch (IOException e3) {
                            e3.printStackTrace();
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                    }
                    try {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        double mid = (double) (baos.toByteArray().length / 1024);
                        if (mid > 32.0d) {
                            double i = mid / 32.0d;
                            bm = PicInfoActivity.zoomImage(bm, ((double) bm.getWidth()) / Math.sqrt(i), ((double) bm.getHeight()) / Math.sqrt(i));
                        }
                    } catch (Exception e5) {
                        e5.printStackTrace();
                    }
                } else {
                    bm = BitmapFactory.decodeResource(PicInfoActivity.this.getResources(), R.drawable.ic_launcher);
                }
                Message msg = new Message();
                msg.what = FinalVariable.update;
                msg.obj = bm;
                PicInfoActivity.this.wxHandler.sendMessage(msg);
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public String buildTransaction(String type) {
        if (type == null) {
            return String.valueOf(System.currentTimeMillis());
        }
        return String.valueOf(type) + System.currentTimeMillis();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        float width = (float) bgimage.getWidth();
        float height = (float) bgimage.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) newWidth) / width, ((float) newHeight) / height);
        return Bitmap.createBitmap(bgimage, 0, 0, (int) width, (int) height, matrix, true);
    }
}
