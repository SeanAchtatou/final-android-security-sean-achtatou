package com.city_life.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.ImageFetcher;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.tauth.Constants;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import com.utils.FileUtils;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import sina_weibo.BingdingSinaWeibo;
import sina_weibo.SinaWeiboUtil;
import sina_weibo.Util;
import sina_weibo.WeiboListener;
import sina_weibo.WeiboShareDao;
import tengxun_weibo.TencentWeiboUtil;

public class WeibofenxiangActivity extends BaseFragmentActivity implements HttpCallback {
    public static String pushbind;
    public String aid;
    public EditText comment_view;
    public ColorStateList csl_h;
    public ColorStateList csl_n;
    public DBHelper db;
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FinalVariable.remove_footer /*1002*/:
                    Utils.dismissProcessDialog();
                    return;
                case FinalVariable.vb_success /*10000*/:
                    Utils.showToast((int) R.string.share_success_tip);
                    WeibofenxiangActivity.this.finish();
                    return;
                case 10001:
                    Utils.showToast((int) R.string.share_bind_tip);
                    return;
                case 10002:
                    Utils.showToast(msg.obj.toString());
                    return;
                case 10003:
                    Utils.showToast((int) R.string.not_null_tip);
                    return;
                case FinalVariable.vb_shortid /*10014*/:
                    String valueOf = String.valueOf(msg.obj);
                    return;
                case FinalVariable.vb_text_long /*10021*/:
                    Utils.showToast((int) R.string.too_long_tip);
                    return;
                default:
                    return;
            }
        }
    };
    private TextView mCommit_img;
    private Listitem mCurrentItem;
    public TextView mShareTitle;
    public String mShorturl;
    /* access modifiers changed from: private */
    public String mWordTip;
    public boolean mark = true;
    public int num = 280;
    public String picurl;
    public String sname;
    public TextView vb_bind;
    private String zhuc_url = "http://weibo.cn/dpool/ttt/h5/reg.php?wm=4068&act=card";
    public TextView zishu_textView;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_share);
        this.db = DBHelper.getDBHelper();
        Intent it = getIntent();
        this.mCurrentItem = (Listitem) it.getExtras().get("item");
        this.sname = it.getStringExtra("sname");
        this.picurl = it.getStringExtra(Constants.PARAM_APP_ICON);
        this.mShorturl = String.valueOf(this.mCurrentItem.title) + "," + this.mCurrentItem.address + ",电话:" + this.mCurrentItem.phone;
        this.csl_h = getResources().getColorStateList(R.color.share_texttoast_color);
        this.csl_n = getResources().getColorStateList(17170444);
        findView();
        initData("");
        addListener();
    }

    public void findView() {
        if (this.picurl == null || "".equals(this.picurl) || "null".equals(this.picurl)) {
            this.picurl = null;
            findViewById(R.id.share_image).setVisibility(8);
        } else {
            String shortpic = this.picurl.replace(Urls.main, "");
            if (FileUtils.isFileExist("image/" + shortpic)) {
                this.picurl = String.valueOf(FileUtils.sdPath) + "image/" + shortpic;
            }
        }
        this.mCommit_img = (TextView) findViewById(R.id.share_btn);
        this.mShareTitle = (TextView) findViewById(R.id.share_title);
        this.zishu_textView = (TextView) findViewById(R.id.share_textview);
        this.vb_bind = (TextView) findViewById(R.id.share_bind_text);
        this.comment_view = (EditText) findViewById(R.id.share_conmment);
        if (this.mShorturl == null || "".equals(this.mShorturl)) {
            try {
                WeiboShareDao.weibo_get_shortid(this.aid, this.handler);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            this.comment_view.setText(this.mShorturl);
        }
        int ns = this.num - getCharacterNum(this.comment_view.getText().toString());
        this.mWordTip = String.format(getResources().getString(R.string.share_text_tip), new StringBuilder().append(ns / 2).toString());
        this.zishu_textView.setText(this.mWordTip);
        onfindView();
    }

    private void fenxiang() {
        Bitmap bitmap;
        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToast((int) R.string.network_error);
        } else if ((this.num - getCharacterNum(this.comment_view.getText().toString())) / 2 < 0) {
            Utils.showToast("字数不能超过140个字，请编辑后再进行分享");
        } else if (this.sname.equals("qq")) {
            if (TextUtils.isEmpty(Util.getSharePersistent(getApplicationContext(), "ACCESS_TOKEN"))) {
                Util.clearSharePersistent(getApplicationContext());
                new AlertDialog.Builder(this).setMessage("腾讯微博未绑定或绑定已经过期，是否重新绑定？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        TencentWeiboUtil.getInstance(WeibofenxiangActivity.this).auth(new WeiboListener() {
                            public void init(boolean isValid) {
                                if (!isValid) {
                                    Intent intent = new Intent();
                                    intent.putExtra("sname", WeibofenxiangActivity.this.sname);
                                    intent.setClass(WeibofenxiangActivity.this, WeiboBangdingActivity.class);
                                    WeibofenxiangActivity.this.startActivity(intent);
                                }
                            }
                        });
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
            } else if (Long.valueOf(Util.getSharePersistent(getApplicationContext(), "EXPIRES_IN")).longValue() - System.currentTimeMillis() > 0) {
                if (TextUtils.isEmpty(this.picurl) || this.picurl.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                    bitmap = null;
                } else {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    Bitmap bitmap2 = BitmapFactory.decodeFile(this.picurl, options);
                    options.inJustDecodeBounds = false;
                    int be = (int) (((float) options.outHeight) / 200.0f);
                    if (be <= 0) {
                        be = 1;
                    }
                    options.inSampleSize = be;
                    bitmap = BitmapFactory.decodeFile(this.picurl, options);
                }
                Utils.showProcessDialog(this, "正在发送...");
                TencentWeiboUtil.getInstance(this).addWeibo(this.mShorturl, bitmap, Double.valueOf(this.mCurrentItem.longitude).doubleValue(), Double.valueOf(this.mCurrentItem.latitude).doubleValue(), this);
            } else {
                Util.clearSharePersistent(getApplicationContext());
                new AlertDialog.Builder(this).setMessage("腾讯微博未绑定或绑定已经过期，是否重新绑定？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        TencentWeiboUtil.getInstance(WeibofenxiangActivity.this).auth(new WeiboListener() {
                            public void init(boolean isValid) {
                                if (!isValid) {
                                    Intent intent = new Intent();
                                    intent.putExtra("sname", WeibofenxiangActivity.this.sname);
                                    intent.setClass(WeibofenxiangActivity.this, WeiboBangdingActivity.class);
                                    WeibofenxiangActivity.this.startActivity(intent);
                                }
                            }
                        });
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
            }
        } else if (TextUtils.isEmpty(PerfHelper.getStringData(sina_weibo.Constants.PREF_SINA_ACCESS_TOKEN)) || !SinaWeiboUtil.getInstance(this).initSinaWeibo()) {
            new AlertDialog.Builder(this).setMessage("新浪微博未绑定或绑定已经过期，是否重新绑定？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    new Thread() {
                        public void run() {
                            if (WeibofenxiangActivity.this.doSinaType("com.sina.weibo", 190)) {
                                new BingdingSinaWeibo(WeibofenxiangActivity.this).BingdingSina(new WeiboListener() {
                                });
                                return;
                            }
                            Intent intent = new Intent();
                            intent.putExtra("sname", WeibofenxiangActivity.this.sname);
                            intent.setClass(WeibofenxiangActivity.this, WeiboBangdingActivity.class);
                            WeibofenxiangActivity.this.startActivity(intent);
                        }
                    }.start();
                }
            }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            }).show();
        } else {
            Utils.showProcessDialog(this, "正在发送...");
            View v = getWindow().peekDecorView();
            if (v != null) {
                ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
            try {
                if (TextUtils.isEmpty(this.picurl) || this.picurl.startsWith(ImageFetcher.HTTP_CACHE_DIR)) {
                    SinaWeiboUtil.getInstance(this).update(this.mShorturl, this.mCurrentItem.latitude, this.mCurrentItem.longitude, this.handler);
                } else {
                    SinaWeiboUtil.getInstance(this).upload(this.mShorturl, this.picurl, this.mCurrentItem.latitude, this.mCurrentItem.longitude, this.handler);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean doSinaType(String packageName, int versionCode) {
        for (PackageInfo pi : getPackageManager().getInstalledPackages(8192)) {
            String pi_packageName = pi.packageName;
            int pi_versionCode = pi.versionCode;
            if (packageName.endsWith(pi_packageName)) {
                if (versionCode > pi_versionCode) {
                    return false;
                }
                if (versionCode <= pi_versionCode) {
                    return true;
                }
            }
        }
        Log.i("test", "未安装该应用，可以安装");
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        reflash();
        super.onResume();
    }

    public void myFinish(View view) {
        finish();
    }

    public void onfindView() {
    }

    public static int getCharacterNum(String content) {
        if (content == null || "".equals(content)) {
            return 0;
        }
        return content.length() + getChineseNum(content);
    }

    public static int getChineseNum(String s) {
        int num2 = 0;
        char[] myChar = s.toCharArray();
        for (int i = 0; i < myChar.length; i++) {
            if (((char) ((byte) myChar[i])) != myChar[i]) {
                num2++;
            }
        }
        return num2;
    }

    public void addListener() {
        this.comment_view.addTextChangedListener(new TextWatcher() {
            private int selectionEnd;
            private int selectionStart;
            private CharSequence temp;

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                this.temp = s;
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                int number = WeibofenxiangActivity.this.num - WeibofenxiangActivity.getCharacterNum(WeibofenxiangActivity.this.comment_view.getText().toString());
                WeibofenxiangActivity.this.mWordTip = String.format(WeibofenxiangActivity.this.getResources().getString(R.string.share_text_tip), new StringBuilder().append(number / 2).toString());
                WeibofenxiangActivity.this.zishu_textView.setText(WeibofenxiangActivity.this.mWordTip);
                if (number / 2 <= 0) {
                    WeibofenxiangActivity.this.zishu_textView.setTextColor(WeibofenxiangActivity.this.csl_h);
                } else {
                    WeibofenxiangActivity.this.zishu_textView.setTextColor(WeibofenxiangActivity.this.csl_n);
                }
                this.selectionStart = WeibofenxiangActivity.this.comment_view.getSelectionStart();
                this.selectionEnd = WeibofenxiangActivity.this.comment_view.getSelectionEnd();
                if (WeibofenxiangActivity.getCharacterNum(WeibofenxiangActivity.this.comment_view.getText().toString()) >= WeibofenxiangActivity.this.num && number / 2 >= 0) {
                    s.delete(this.selectionStart - 1, this.selectionEnd);
                    int tempSelection = this.selectionEnd;
                    WeibofenxiangActivity.this.comment_view.setText(s);
                    WeibofenxiangActivity.this.comment_view.setSelection(tempSelection);
                }
            }
        });
    }

    public void initData(String type) {
        if ((this.num - getCharacterNum(this.comment_view.getText().toString())) / 2 <= 0) {
            Utils.showToast("字数过多,请编辑后再分享……");
            this.zishu_textView.setTextColor(this.csl_h);
        } else {
            this.zishu_textView.setTextColor(this.csl_n);
        }
        if (this.sname.equals("sina")) {
            this.mShareTitle.setText("分享到新浪微博");
        } else if (this.sname.equals("qq")) {
            this.mShareTitle.setText("分享到腾讯微博");
        } else if (this.sname.equals("163")) {
            this.mShareTitle.setText("分享到网易微博");
        } else if (this.sname.equals("sohu")) {
            this.mShareTitle.setText("分享到搜狐微博");
        } else if (this.sname.equals("kaixin")) {
            this.mShareTitle.setText("分享到开心网");
        } else if (this.sname.equals("renren")) {
            this.mShareTitle.setText("分享到人人");
        }
    }

    public void reflash() {
        String token;
        if (this.sname.equals("qq")) {
            token = Util.getSharePersistent(getApplicationContext(), "ACCESS_TOKEN");
        } else {
            token = PerfHelper.getStringData(sina_weibo.Constants.PREF_SINA_ACCESS_TOKEN);
        }
        if (TextUtils.isEmpty(token)) {
            this.vb_bind.setText("未绑定");
        } else if (this.sname.equals("qq")) {
            this.vb_bind.setText(PerfHelper.getStringData(sina_weibo.Constants.PREF_TX_NAME));
        } else {
            this.vb_bind.setText(PerfHelper.getStringData(sina_weibo.Constants.PREF_SINA_USER_NAME));
        }
    }

    public void things(View v) {
        if (v.getId() == R.id.share_btn) {
            if (!this.comment_view.getText().toString().trim().equals("")) {
                fenxiang();
                ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.comment_view.getWindowToken(), 0);
                return;
            }
            Utils.showToast("分享内容不能为空");
        } else if (v.getId() == R.id.title_back) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 32973) {
            SinaWeiboUtil.getInstance(this).authCallBack(requestCode, resultCode, data);
            if (!TextUtils.isEmpty(PerfHelper.getStringData(sina_weibo.Constants.PREF_SINA_ACCESS_TOKEN))) {
                Utils.showToast("绑定成功，点击发送");
            }
        } else if (requestCode == 1) {
            TencentWeiboUtil.getInstance(this).webAuthOnResult();
        }
    }

    public void onResult(Object object) {
        this.handler.sendEmptyMessage(FinalVariable.remove_footer);
        if (object != null) {
            ModelResult result = (ModelResult) object;
            if (result.isExpires()) {
                Utils.showToast("发送失败:" + result.getError_message());
            } else if (result.isSuccess()) {
                Utils.showToast("发送成功,赶快到微博查看吧!");
            } else {
                Utils.showToast("发送失败:" + ((ModelResult) object).getError_message());
            }
        }
    }
}
