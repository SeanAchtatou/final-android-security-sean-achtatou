package com.city_life.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.city_life.sliding.fragments.SearchListFragment;
import com.pengyou.citycommercialarea.R;

public class SearchActivity extends SherlockFragmentActivity implements SearchView.OnQueryTextListener {
    ActionBar actionBar;
    String keyword = "";
    private Fragment mContent = null;
    SearchView search;
    TextView title;
    String type = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.content_frame);
        this.actionBar = getSupportActionBar();
        this.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.banner));
        View actionbar_title = LayoutInflater.from(this).inflate((int) R.layout.title_row, (ViewGroup) null);
        this.title = (TextView) actionbar_title.findViewById(R.id.row_title);
        this.title.setText("搜索结果");
        this.actionBar.setCustomView(actionbar_title, new ActionBar.LayoutParams(-1, -1, 17));
        this.actionBar.setDisplayShowCustomEnabled(true);
        this.keyword = getIntent().getStringExtra("keyword");
        this.type = getIntent().getStringExtra("type");
        if (savedInstanceState != null) {
            this.mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
        }
        if (this.mContent == null) {
            initFragment();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        this.mContent = (SearchListFragment) SearchListFragment.newInstance(this.type, this.keyword);
        fragmentTransaction.replace(R.id.content_frame, this.mContent).commit();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "mContent", this.mContent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.options, menu);
        this.search = (SearchView) menu.findItem(R.id.menu_add_search).getActionView();
        this.search.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public boolean onQueryTextSubmit(String query) {
        if (this.mContent == null) {
            return true;
        }
        ((SearchListFragment) this.mContent).initType(this.type, query);
        ((SearchListFragment) this.mContent).onRefresh();
        return true;
    }

    public boolean onQueryTextChange(String newText) {
        return true;
    }
}
