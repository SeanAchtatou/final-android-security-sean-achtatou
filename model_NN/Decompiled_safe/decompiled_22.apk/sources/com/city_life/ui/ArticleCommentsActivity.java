package com.city_life.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.city_life.fragment.CommentsFragment;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.dao.MySSLSocketFactory;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.Data;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class ArticleCommentsActivity extends BaseActivity {
    public View comment_view;
    public CommentsFragment fragment;
    public FragmentManager fragmentManager = null;
    public TextView how_text;
    /* access modifiers changed from: private */
    public String mId;
    public CommentsFragment.OnReflashTotal mOnReflashTotal = new CommentsFragment.OnReflashTotal() {
        public void onReflash(final Data data) {
            new Handler().post(new Runnable() {
                public void run() {
                    if (data != null) {
                        ArticleCommentsActivity.this.how_text.setText(String.valueOf(data.date) + "条");
                    }
                }
            });
        }
    };
    String total = UploadUtils.SUCCESS;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        requestWindowFeature(1);
        this.mId = getIntent().getStringExtra(LocaleUtil.INDONESIAN);
        setContentView((int) R.layout.article_comments);
        this.comment_view = findViewById(R.id.article_comment_img);
        this.how_text = (TextView) findViewById(R.id.how_comments);
        this.how_text.setText(String.valueOf(getIntent().getStringExtra("count")) + "条");
        this.comment_view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(LocaleUtil.INDONESIAN, ArticleCommentsActivity.this.mId);
                intent.setAction(ArticleCommentsActivity.this.getResources().getString(R.string.activity_article_comment));
                ArticleCommentsActivity.this.startActivityForResult(intent, 1);
            }
        });
        this.fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();
        this.fragment = CommentsFragment.newInstance(this.mId);
        this.fragment.nodata_tip = R.string.comment_list_no;
        this.fragment.mOnReflashTotal = this.mOnReflashTotal;
        fragmentTransaction.add((int) R.id.content, this.fragment);
        fragmentTransaction.commit();
    }

    public void onClick(View v) {
        if (R.id.comment_back == v.getId()) {
            finish();
        }
    }

    public void getComment() {
        new Thread() {
            public void run() {
                try {
                    ArticleCommentsActivity.this.total = ArticleCommentsActivity.this.getCommentCount();
                    Utils.h.post(new Runnable() {
                        public void run() {
                            TextView countview = (TextView) ArticleCommentsActivity.this.findViewById(R.id.how_comments);
                            if (TextUtils.isEmpty(ArticleCommentsActivity.this.total) || UploadUtils.SUCCESS.equals(ArticleCommentsActivity.this.total)) {
                                countview.setText("0条");
                            } else if (Integer.parseInt(ArticleCommentsActivity.this.total) > 1000) {
                                countview.setText("999+条");
                            } else {
                                countview.setText(String.valueOf(ArticleCommentsActivity.this.total) + "条");
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public String getCommentCount() throws Exception {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("action", "commentcount"));
        param.add(new BasicNameValuePair(LocaleUtil.INDONESIAN, this.mId));
        return new JSONObject(MySSLSocketFactory.getinfo(Urls.app_api, param)).getString("count");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == 2) {
            new Thread(new Runnable() {
                public void run() {
                    if (ArticleCommentsActivity.this.fragment != null) {
                        ArticleCommentsActivity.this.fragment.reFlush();
                    }
                }
            }).start();
        }
    }

    public void things(View view) {
    }
}
