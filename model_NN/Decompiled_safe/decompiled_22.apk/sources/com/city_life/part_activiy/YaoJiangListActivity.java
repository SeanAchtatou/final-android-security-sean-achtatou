package com.city_life.part_activiy;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.view.MyLetterListView;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.pengyou.citycommercialarea.R;
import com.tencent.tauth.Constants;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class YaoJiangListActivity extends BaseFragmentActivity {
    private BaseAdapter adapter;
    /* access modifiers changed from: private */
    public HashMap<String, Integer> alphaIndexer;
    /* access modifiers changed from: private */
    public Handler handler;
    private MyLetterListView letterListView;
    /* access modifiers changed from: private */
    public ListView mCityLit;
    public Data mData;
    public int mFooter_limit = this.mLength;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FinalVariable.update /*1001*/:
                    YaoJiangListActivity.this.mCityLit.setAdapter((android.widget.ListAdapter) new ListAdapter(YaoJiangListActivity.this, YaoJiangListActivity.this.mData.list));
                    return;
                case FinalVariable.remove_footer /*1002*/:
                case FinalVariable.change /*1003*/:
                case FinalVariable.error /*1004*/:
                default:
                    return;
            }
        }
    };
    public int mLength = 20;
    public String mOldtype = "citys_list";
    public int mPage = 1;
    public String mParttype = "citys_list";
    /* access modifiers changed from: private */
    public TextView overlay;
    /* access modifiers changed from: private */
    public OverlayThread overlayThread;
    /* access modifiers changed from: private */
    public String[] sections;
    WindowManager windowManager;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.city_list);
        this.mCityLit = (ListView) findViewById(R.id.city_list);
        this.letterListView = (MyLetterListView) findViewById(R.id.cityLetterListView);
        this.letterListView.setOnTouchingLetterChangedListener(new LetterListViewListener(this, null));
        this.alphaIndexer = new HashMap<>();
        this.handler = new Handler();
        this.overlayThread = new OverlayThread(this, null);
        initOverlay();
        ((TextView) findViewById(R.id.title_title)).setText("我的奖号");
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                YaoJiangListActivity.this.finish();
            }
        });
        findViewById(R.id.title_btn_right).setVisibility(8);
        initData();
    }

    public void setAdapter(List<Listitem> list) {
        if (list != null) {
            this.adapter = new ListAdapter(this, getsort_city(list));
            this.mCityLit.setAdapter((android.widget.ListAdapter) this.adapter);
        }
    }

    public ArrayList<Listitem> getsort_city(List<Listitem> list2) {
        ArrayList<Listitem> return_list = new ArrayList<>();
        for (int i = 0; i < MyLetterListView.b.length; i++) {
            for (Listitem cityEntity : list2) {
                if (MyLetterListView.b[i].equals(cityEntity.des)) {
                    return_list.add(cityEntity);
                }
            }
        }
        return return_list;
    }

    private class ListAdapter extends BaseAdapter {
        private LayoutInflater inflater;
        private List<Listitem> list;

        public ListAdapter(Context context, List<Listitem> list2) {
            this.inflater = LayoutInflater.from(context);
            this.list = list2;
            YaoJiangListActivity.this.alphaIndexer = new HashMap();
            YaoJiangListActivity.this.sections = new String[list2.size()];
            for (int i = 0; i < list2.size(); i++) {
                if (!(i + -1 >= 0 ? list2.get(i - 1).des : " ").equals(list2.get(i).des)) {
                    String name = list2.get(i).des;
                    YaoJiangListActivity.this.alphaIndexer.put(name, Integer.valueOf(i));
                    YaoJiangListActivity.this.sections[i] = name;
                }
            }
        }

        public int getCount() {
            return this.list.size();
        }

        public Object getItem(int position) {
            return this.list.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            String previewStr;
            if (convertView == null) {
                convertView = this.inflater.inflate((int) R.layout.city_list_item, (ViewGroup) null);
                holder = new ViewHolder(this, null);
                holder.alpha = (TextView) convertView.findViewById(R.id.alpha);
                holder.name = (TextView) convertView.findViewById(R.id.name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.name.setText(this.list.get(position).title);
            String currentStr = this.list.get(position).des;
            if (position - 1 >= 0) {
                previewStr = this.list.get(position - 1).des;
            } else {
                previewStr = " ";
            }
            if (!previewStr.equals(currentStr)) {
                holder.alpha.setVisibility(0);
                holder.alpha.setText("第" + currentStr + "期摇奖号");
            } else {
                holder.alpha.setVisibility(8);
            }
            return convertView;
        }

        private class ViewHolder {
            TextView alpha;
            TextView name;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(ListAdapter listAdapter, ViewHolder viewHolder) {
                this();
            }
        }
    }

    private void initOverlay() {
        this.overlay = (TextView) LayoutInflater.from(this).inflate((int) R.layout.city_show_right_letter, (ViewGroup) null);
        this.overlay.setVisibility(4);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams(-2, -2, 2, 24, -3);
        this.windowManager = (WindowManager) getSystemService("window");
        this.windowManager.addView(this.overlay, lp);
    }

    private class LetterListViewListener implements MyLetterListView.OnTouchingLetterChangedListener {
        private LetterListViewListener() {
        }

        /* synthetic */ LetterListViewListener(YaoJiangListActivity yaoJiangListActivity, LetterListViewListener letterListViewListener) {
            this();
        }

        public void onTouchingLetterChanged(String s) {
            if (YaoJiangListActivity.this.alphaIndexer.get(s) != null) {
                int position = ((Integer) YaoJiangListActivity.this.alphaIndexer.get(s)).intValue();
                YaoJiangListActivity.this.mCityLit.setSelection(position);
                YaoJiangListActivity.this.overlay.setText(YaoJiangListActivity.this.sections[position]);
                YaoJiangListActivity.this.overlay.setVisibility(0);
                YaoJiangListActivity.this.handler.removeCallbacks(YaoJiangListActivity.this.overlayThread);
                YaoJiangListActivity.this.handler.postDelayed(YaoJiangListActivity.this.overlayThread, 1500);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.windowManager != null && this.overlay != null) {
            this.windowManager.removeView(this.overlay);
        }
    }

    private class OverlayThread implements Runnable {
        private OverlayThread() {
        }

        /* synthetic */ OverlayThread(YaoJiangListActivity yaoJiangListActivity, OverlayThread overlayThread) {
            this();
        }

        public void run() {
            YaoJiangListActivity.this.overlay.setVisibility(8);
        }
    }

    public void initData() {
        new Thread() {
            public void run() {
                try {
                    Data d = YaoJiangListActivity.this.getDataFromDB(YaoJiangListActivity.this.mOldtype, 0, YaoJiangListActivity.this.mLength, YaoJiangListActivity.this.mParttype);
                    if (!(d == null || d.list == null || d.list.size() <= 0)) {
                        YaoJiangListActivity.this.mData = d;
                    }
                    YaoJiangListActivity.this.mData = YaoJiangListActivity.this.getDataFromNet(String.valueOf(YaoJiangListActivity.this.getResources().getString(R.string.citylife_yaoyiayo_jianghaolist_url)) + "iphone=" + PerfHelper.getStringData("phone"), YaoJiangListActivity.this.mOldtype, 0, YaoJiangListActivity.this.mLength, true, YaoJiangListActivity.this.mParttype);
                    YaoJiangListActivity.this.mHandler.sendEmptyMessage(FinalVariable.update);
                } catch (Exception e) {
                }
            }
        }.start();
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        String json = DNDataSource.CityLift_list_FromNET(url, oldtype, page, count, parttype, isfirst);
        Data data = parseJson(json);
        if (!(data == null || data.list == null || data.list.size() <= 0)) {
            if (isfirst) {
                DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
            }
            DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        }
        return data;
    }

    public Data getDataFromDB(String oldtype, int page, int count, String parttype) throws Exception {
        String json = DNDataSource.list_FromDB(oldtype, page, count, parttype);
        if (json == null || "".equals(json) || "null".equals(json)) {
            return null;
        }
        return parseJson(json);
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has("responseCode") || jsonobj.getInt("responseCode") == 0) {
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                JSONObject obj = jsonay.getJSONObject(i);
                String qishu = obj.getString(Constants.PARAM_TITLE);
                JSONArray jarr = obj.getJSONArray("listDraw");
                int count2 = jarr.length();
                for (int z = 0; z < count2; z++) {
                    Listitem o = new Listitem();
                    JSONObject objs = jarr.getJSONObject(z);
                    try {
                        if (objs.has("draw")) {
                            o.title = objs.getString("draw");
                        }
                        o.des = qishu;
                    } catch (Exception e) {
                    }
                    o.getMark();
                    data.list.add(o);
                }
            }
            return data;
        }
        throw new DataException(jsonobj.getString("responseCode"));
    }
}
