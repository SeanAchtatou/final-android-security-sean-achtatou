package com.city_life.part_activiy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.baidumap.BaiduSelectLocationOverlay;
import com.city_life.entity.CityLifeApplication;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.uploadmsg.SelectPicActivity;
import com.uploadmsg.UploadUtil;
import com.utils.PerfHelper;
import java.util.HashMap;
import java.util.Map;

public class UserCenter extends Activity implements UploadUtil.OnUploadProcessListener {
    static String file_str = Environment.getExternalStorageDirectory().getPath();
    static boolean is_down_img = false;
    private static String requestURL = "http://192.168.10.160:8080/fileUpload/p/file!upload";
    Button btn_center_name;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    UserCenter.this.toUploadFile();
                    break;
                case 2:
                    String str = "响应码：" + msg.arg1 + "\n响应信息：" + msg.obj + "\n耗时：" + UploadUtil.getRequestTime() + "秒";
                    break;
            }
            super.handleMessage(msg);
        }
    };
    ImageView imgv_head;
    private String picPath = null;
    private ProgressDialog progressDialog;
    TextView user_age;
    ImageView user_sex;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.user_center);
        this.imgv_head = (ImageView) findViewById(R.id.imgv_head);
        this.user_age = (TextView) findViewById(R.id.user_age);
        this.user_sex = (ImageView) findViewById(R.id.user_sex);
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_btn_right).setVisibility(8);
        ((TextView) findViewById(R.id.title_title)).setText("个人中心");
        this.imgv_head.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        findViewById(R.id.txtv_user_center_usercollect_id).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(UserCenter.this, FavActivity.class);
                UserCenter.this.startActivity(intent);
            }
        });
        findViewById(R.id.txtv_user_center_secondlook_id).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserCenter.this.startActivity(new Intent(UserCenter.this, SaomiaoActivity.class));
            }
        });
        findViewById(R.id.txtv_user_center_nowposition_id).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserCenter.this.startActivity(new Intent(UserCenter.this, BaiduSelectLocationOverlay.class));
            }
        });
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserCenter.this.finish();
            }
        });
        findViewById(R.id.quit_user_center).setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
             arg types: [java.lang.String, int]
             candidates:
              com.utils.PerfHelper.setInfo(java.lang.String, int):void
              com.utils.PerfHelper.setInfo(java.lang.String, long):void
              com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
              com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
            public void onClick(View v) {
                PerfHelper.setInfo(PerfHelper.P_USERID, "");
                PerfHelper.setInfo(PerfHelper.P_SHARE_AGE, "");
                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME, "");
                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE, "");
                PerfHelper.setInfo(PerfHelper.P_SHARE_SEX, "");
                PerfHelper.setInfo(PerfHelper.P_USER_LOGIN, false);
                CityLifeApplication.fav_list_id = null;
                Utils.showToast("退出登录成功");
                UserCenter.this.finish();
            }
        });
        this.btn_center_name = (Button) findViewById(R.id.btn_center_name);
        set_user_headerveiw();
    }

    public void set_user_headerveiw() {
        this.btn_center_name.setText(PerfHelper.getStringData(PerfHelper.P_SHARE_NAME));
        if (PerfHelper.getStringData(PerfHelper.P_SHARE_SEX).equals("男")) {
            this.user_sex.setImageResource(R.drawable.sex_man);
        } else {
            this.user_sex.setImageResource(R.drawable.sex_woman);
        }
        this.user_age.setText(PerfHelper.getStringData(PerfHelper.P_SHARE_AGE));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1 && requestCode == 3) {
            this.picPath = data.getStringExtra(SelectPicActivity.KEY_PHOTO_PATH);
            if (ShareApplication.debug) {
                System.out.println("最终选择的图片=" + this.picPath);
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            this.imgv_head.setImageBitmap(BitmapFactory.decodeFile(this.picPath, options));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onUploadDone(int responseCode, String message) {
    }

    public void onUploadProcess(int uploadSize) {
        Message msg = Message.obtain();
        msg.what = 5;
        msg.arg1 = uploadSize;
        this.handler.sendMessage(msg);
    }

    public void initUpload(int fileSize) {
        Message msg = Message.obtain();
        msg.what = 4;
        msg.arg1 = fileSize;
        this.handler.sendMessage(msg);
    }

    /* access modifiers changed from: private */
    public void toUploadFile() {
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage("正在上传文件...");
        this.progressDialog.show();
        UploadUtil uploadUtil = UploadUtil.getInstance();
        uploadUtil.setOnUploadProcessListener(this);
        Map<String, String> params = new HashMap<>();
        params.put("orderId", "11111");
        uploadUtil.uploadFile(this.picPath, "pic", requestURL, params);
    }
}
