package com.city_life.part_activiy;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.part_asynctask.UploadUtils;
import com.city_life.part_fragment.NearByPartFragment;
import com.city_life.part_fragment.ShiChangZhuFragment;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class ShiChangZhuActivity extends BaseFragmentActivity {
    private Fragment frag = null;
    public String id = "";
    private Fragment m_list_frag_1 = null;
    public String name = "";
    private String type = "";
    /* access modifiers changed from: private */
    public Listitem z_item = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main_part_zixun);
        this.id = getIntent().getStringExtra(LocaleUtil.INDONESIAN);
        this.name = getIntent().getStringExtra(Constants.PARAM_TITLE);
        new CurrentAync().execute(null);
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShiChangZhuActivity.this.finish();
            }
        });
        findViewById(R.id.title_btn_right).setVisibility(0);
        ((TextView) findViewById(R.id.title_btn_right)).setText("入驻");
        findViewById(R.id.title_btn_right).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Dialog dialog = new Dialog(ShiChangZhuActivity.this, R.style.My_Dialog);
                dialog.setContentView((int) R.layout.shichangzhu_dialog);
                TextView titles = (TextView) dialog.findViewById(R.id.listitem_title_s);
                TextView title = (TextView) dialog.findViewById(R.id.listitem_title);
                TextView des = (TextView) dialog.findViewById(R.id.listitem_des);
                TextView phone = (TextView) dialog.findViewById(R.id.listitem_photos_text);
                if (ShiChangZhuActivity.this.z_item != null) {
                    title.setText(ShiChangZhuActivity.this.z_item.title);
                    des.setText(ShiChangZhuActivity.this.z_item.des);
                    phone.setText(ShiChangZhuActivity.this.z_item.phone);
                    titles.setText(String.valueOf(ShiChangZhuActivity.this.name) + "市场商家入驻");
                    dialog.findViewById(R.id.convenience_dhbutton).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            new Intent();
                            Intent intent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + ShiChangZhuActivity.this.z_item.phone));
                            intent.setFlags(268435456);
                            ShiChangZhuActivity.this.startActivity(intent);
                        }
                    });
                } else {
                    title.setText("暂无市场主信息");
                    des.setText(" ");
                    phone.setText("暂无联系电话");
                    titles.setText(String.valueOf(ShiChangZhuActivity.this.name) + "市场商家入驻");
                }
                dialog.show();
            }
        });
        ((TextView) findViewById(R.id.title_title)).setText(this.name);
        findViewById(R.id.rl_bottom).setVisibility(8);
        findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!((EditText) ShiChangZhuActivity.this.findViewById(R.id.et_sendmessage)).getText().toString().trim().equals("")) {
                    Intent intent = new Intent();
                    intent.setClass(ShiChangZhuActivity.this, SearchListActivity.class);
                    intent.putExtra("keyword", ((EditText) ShiChangZhuActivity.this.findViewById(R.id.et_sendmessage)).getText().toString().trim());
                    ShiChangZhuActivity.this.startActivity(intent);
                    return;
                }
                Utils.showToast("关键词不能为空");
            }
        });
        initFragments();
    }

    public void initFragments() {
        this.type = "nearbypart" + PerfHelper.getStringData(PerfHelper.P_CITY_No);
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(this.type);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (DBHelper.getDBHelper().counts("part_list", "part_type='" + this.type + "'") > 0) {
            if (this.frag != null) {
                fragmentTransaction.detach(this.frag);
            }
            if (findresult != null) {
                fragmentTransaction.attach(findresult);
                this.frag = findresult;
                fragmentTransaction.commitAllowingStateLoss();
                return;
            }
            this.frag = NearByPartFragment.newInstance(this.type);
        } else {
            if (this.frag != null) {
                fragmentTransaction.detach(this.frag);
            }
            if (findresult != null) {
                fragmentTransaction.attach(findresult);
                this.frag = findresult;
                fragmentTransaction.commitAllowingStateLoss();
                return;
            }
            this.frag = ShiChangZhuFragment.newInstance("ShiChangZhuFragment" + this.id, "ShiChangZhuFragment" + this.id);
        }
        fragmentTransaction.add(R.id.part_content, this.frag, this.type);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void initFragment() {
        Fragment findresult = getSupportFragmentManager().findFragmentByTag("ShiChangZhuFragment");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (ShiChangZhuFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = ShiChangZhuFragment.newInstance("ShiChangZhuFragment" + this.id, "ShiChangZhuFragment" + this.id);
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, "ShiChangZhuFragment");
        fragmentTransaction.commit();
    }

    public void things(View view) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    private class CurrentAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("typeId", ShiChangZhuActivity.this.id));
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(ShiChangZhuActivity.this.getResources().getString(R.string.citylife_getMarketSeller_url), param);
                if (ShareApplication.debug) {
                    System.out.println("市场主信息返回:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.list);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("countNum")) {
                data.obj = jsonobj.getString("countNum");
            }
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                try {
                    if (obj.has("bmsIntroduction")) {
                        o.des = obj.getString("bmsIntroduction");
                    }
                    if (obj.has("telephone")) {
                        o.phone = obj.getString("telephone");
                    }
                    if (obj.has("bmsUserName")) {
                        o.title = obj.getString("bmsUserName");
                    }
                } catch (Exception e) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (!Utils.isNetworkAvailable(ShiChangZhuActivity.this)) {
                Utils.showToast((int) R.string.network_error);
                ShiChangZhuActivity.this.finish();
            } else if (result != null && !UploadUtils.FAILURE.equals(result.get("responseCode")) && UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                ShiChangZhuActivity.this.z_item = (Listitem) ((ArrayList) result.get("results")).get(0);
            }
        }
    }
}
