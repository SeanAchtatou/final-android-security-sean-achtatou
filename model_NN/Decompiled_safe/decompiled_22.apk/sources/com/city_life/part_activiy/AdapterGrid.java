package com.city_life.part_activiy;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.city_life.ui.PicInfoActivity;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;

public class AdapterGrid extends BaseAdapter {
    Context context;
    String[] listBitmap;
    GridView mGridView;
    Listitem mListitem;

    public AdapterGrid(Context context2, Listitem item, String[] liString, GridView mGridView2) {
        this.context = context2;
        this.listBitmap = liString;
        this.mListitem = item;
        this.mGridView = mGridView2;
    }

    public int getCount() {
        return this.listBitmap.length;
    }

    public Object getItem(int position) {
        return this.listBitmap[position];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.detail_item, (ViewGroup) null);
            holder = new Holder();
            holder.icon = (ImageView) convertView.findViewById(R.id.ItemImage);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        ShareApplication.mImageWorker.loadImage(this.listBitmap[position], holder.icon);
        holder.icon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(AdapterGrid.this.context, PicInfoActivity.class);
                intent.putExtra("item", AdapterGrid.this.mListitem);
                intent.putExtra("img_list", AdapterGrid.this.listBitmap);
                AdapterGrid.this.context.startActivity(intent);
            }
        });
        return convertView;
    }

    static class Holder {
        ImageView icon;

        Holder() {
        }
    }
}
