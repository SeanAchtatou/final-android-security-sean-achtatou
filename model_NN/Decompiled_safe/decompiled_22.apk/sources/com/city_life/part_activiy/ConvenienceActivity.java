package com.city_life.part_activiy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.fragment.PartFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class ConvenienceActivity extends BaseFragmentActivity {
    private Fragment frag = null;
    private Fragment mContent;
    private Fragment m_list_frag_1 = null;
    String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    private String type = "";
    private TextView where;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main_part_ac);
        findViewById(R.id.title_back).setVisibility(8);
        findViewById(R.id.title_btn_right).setVisibility(0);
        ((TextView) findViewById(R.id.title_title)).setText("便民");
        findViewById(R.id.title_btn_right).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(ConvenienceActivity.this, CityListActivity.class);
                ConvenienceActivity.this.startActivity(intent);
            }
        });
        initFragment();
    }

    public void initFragment() {
        this.type = "convenience" + PerfHelper.getStringData(PerfHelper.P_CITY_No);
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(this.type);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (DBHelper.getDBHelper().counts("part_list", "part_type='" + this.type + "'") > 0) {
            if (this.frag != null) {
                fragmentTransaction.detach(this.frag);
            }
            if (findresult != null) {
                fragmentTransaction.attach(findresult);
                this.frag = findresult;
                fragmentTransaction.commitAllowingStateLoss();
                return;
            }
            this.frag = PartFragment.newInstance(this.type);
            fragmentTransaction.add(R.id.part_content, this.frag, this.type);
            fragmentTransaction.commitAllowingStateLoss();
            return;
        }
        Utils.showToast("暂无该城市便民资讯");
    }

    public void things(View view) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.title_btn_right)).setText(PerfHelper.getStringData(PerfHelper.P_CITY));
        if (!this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY))) {
            this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
            initFragment();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
