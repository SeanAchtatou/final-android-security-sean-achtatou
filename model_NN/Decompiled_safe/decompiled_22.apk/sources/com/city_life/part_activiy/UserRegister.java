package com.city_life.part_activiy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class UserRegister extends Activity implements View.OnClickListener {
    private View btn_register;
    private View btn_return;
    /* access modifiers changed from: private */
    public String codext;
    /* access modifiers changed from: private */
    public EditText edit_nickName;
    private EditText edit_postbox;
    /* access modifiers changed from: private */
    public EditText edit_pwd;
    private EditText edit_pwd_affirm;
    private RelativeLayout mLayoutMan;
    private RelativeLayout mLayoutWoman;
    int sex = 1;
    private int sex_register = 1;
    /* access modifiers changed from: private */
    public String sms_str;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.user_register);
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_btn_right).setVisibility(8);
        ((TextView) findViewById(R.id.title_title)).setText("用户注册");
        this.sms_str = getIntent().getStringExtra("sms");
        this.codext = getIntent().getStringExtra("codext");
        initView();
    }

    private void initView() {
        this.mLayoutMan = (RelativeLayout) findViewById(R.id.rl_register_choose_man);
        this.mLayoutWoman = (RelativeLayout) findViewById(R.id.rl_register_choose_woman);
        this.btn_return = findViewById(R.id.title_back);
        this.btn_register = findViewById(R.id.btn_user_register_accomplish_register);
        this.edit_postbox = (EditText) findViewById(R.id.editt_user_register_postbox);
        this.edit_nickName = (EditText) findViewById(R.id.editt_user_register_nickName);
        this.edit_pwd = (EditText) findViewById(R.id.editt_user_register_pwd);
        this.edit_pwd_affirm = (EditText) findViewById(R.id.editt_user_register_pwd_affirm);
        this.btn_return.setOnClickListener(this);
        this.btn_register.setOnClickListener(this);
        this.mLayoutMan.setOnClickListener(this);
        this.mLayoutWoman.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            case R.id.rl_register_choose_man /*2131231216*/:
                this.sex_register = 1;
                this.mLayoutMan.setBackgroundResource(R.drawable.sex_click);
                this.mLayoutWoman.setBackgroundResource(R.color.color_white);
                this.sex = 1;
                return;
            case R.id.rl_register_choose_woman /*2131231217*/:
                this.sex_register = 2;
                this.mLayoutWoman.setBackgroundResource(R.drawable.sex_click);
                this.mLayoutMan.setBackgroundResource(R.color.color_white);
                this.sex = 2;
                return;
            case R.id.btn_user_register_accomplish_register /*2131231220*/:
                int j = this.edit_pwd.getText().toString().length();
                int k = this.edit_pwd_affirm.getText().toString().length();
                if (this.edit_nickName.getText().toString().trim().length() <= 0 || this.edit_pwd.getText().toString().trim().length() <= 0) {
                    Toast.makeText(this, "注册信息不能为空", 0).show();
                    return;
                } else if (j < 6 || k < 6) {
                    Toast.makeText(this, "注意：密码 的长度不能低于6位", 0).show();
                    return;
                } else if (j > 16 || k > 16) {
                    Toast.makeText(this, "注意：密码的长度不能高于16位", 0).show();
                    return;
                } else if (!this.edit_pwd.getText().toString().equals(this.edit_pwd_affirm.getText().toString())) {
                    Toast.makeText(this, "注意：两次输入的密码必须一致", 0).show();
                    return;
                } else {
                    new AlertDialog.Builder(this).setMessage("确认注册？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Utils.showProcessDialog(UserRegister.this, "正在注册...");
                            new Thread() {
                                public void run() {
                                    new CurrentAync().execute(null);
                                }
                            }.start();
                        }
                    }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show();
                    return;
                }
            default:
                return;
        }
    }

    class CurrentAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            ArrayList<NameValuePair> list = new ArrayList<>();
            BasicNameValuePair mPair01 = new BasicNameValuePair("nick", UserRegister.this.edit_nickName.getText().toString());
            BasicNameValuePair mPair02 = new BasicNameValuePair("password", UserRegister.this.edit_pwd.getText().toString());
            BasicNameValuePair mPair03 = new BasicNameValuePair("sms", UserRegister.this.sms_str);
            BasicNameValuePair mPair04 = new BasicNameValuePair("codeExt", UserRegister.this.codext);
            BasicNameValuePair mPair05 = new BasicNameValuePair("sex", "保密");
            list.add(mPair01);
            list.add(mPair02);
            list.add(mPair03);
            list.add(mPair04);
            list.add(mPair05);
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(UserRegister.this.getResources().getString(R.string.citylife_registerUserInfo_url), list);
                if (ShareApplication.debug) {
                    System.out.println("QQ绑定注册返回:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.list);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            Listitem o = new Listitem();
            JSONObject obj = jsonobj.getJSONObject("user");
            o.nid = obj.getString(LocaleUtil.INDONESIAN);
            try {
                if (obj.has("conent")) {
                    o.des = obj.getString("conent");
                }
                if (obj.has("age")) {
                    o.level = obj.getString("age");
                }
                if (obj.has("nickName")) {
                    o.title = obj.getString("nickName");
                }
                if (obj.has("sex")) {
                    o.other = obj.getString("sex");
                }
                if (obj.has("imgUrl")) {
                    o.icon = obj.getString("imgUrl");
                }
            } catch (Exception e) {
            }
            o.getMark();
            data.list.add(o);
            return data;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          com.utils.PerfHelper.setInfo(java.lang.String, int):void
          com.utils.PerfHelper.setInfo(java.lang.String, long):void
          com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
          com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (result == null || UploadUtils.FAILURE.equals(result.get("responseCode"))) {
                Toast.makeText(UserRegister.this, "请求失败，请稍后再试", 0).show();
            } else if ("2".equals(result.get("responseCode"))) {
                Toast.makeText(UserRegister.this, "注册失败，昵称已经存在", 0).show();
            } else if ("3".equals(result.get("responseCode"))) {
                Toast.makeText(UserRegister.this, "注册失败，邮箱已经存在", 0).show();
            } else if (UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                ArrayList<Listitem> user_listArrayList = (ArrayList) result.get("results");
                PerfHelper.setInfo(PerfHelper.P_USERID, ((Listitem) user_listArrayList.get(0)).nid);
                PerfHelper.setInfo(PerfHelper.P_SHARE_AGE, ((Listitem) user_listArrayList.get(0)).level);
                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME, ((Listitem) user_listArrayList.get(0)).title);
                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE, ((Listitem) user_listArrayList.get(0)).icon);
                if (((Listitem) user_listArrayList.get(0)).other.equals(UploadUtils.FAILURE)) {
                    PerfHelper.setInfo(PerfHelper.P_SHARE_SEX, "男");
                } else {
                    PerfHelper.setInfo(PerfHelper.P_SHARE_SEX, "女");
                }
                PerfHelper.setInfo(PerfHelper.P_USER_LOGIN, true);
                Toast.makeText(UserRegister.this, "注册成功，并登录", 0).show();
                UserRegister.this.finish();
            }
        }
    }
}
