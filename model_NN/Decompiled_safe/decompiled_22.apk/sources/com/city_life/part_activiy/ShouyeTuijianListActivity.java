package com.city_life.part_activiy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.part_fragment.ShouYeTuiJianListFragment;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.utils.PerfHelper;

public class ShouyeTuijianListActivity extends BaseFragmentActivity {
    private Fragment frag = null;
    String id = "";
    String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    private String type = "";
    resume_up upone;

    public interface resume_up {
        void up();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main_part_zixun);
        this.id = getIntent().getStringExtra(LocaleUtil.INDONESIAN);
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShouyeTuijianListActivity.this.finish();
            }
        });
        findViewById(R.id.rl_bottom).setVisibility(8);
        ((TextView) findViewById(R.id.title_title)).setText(getIntent().getStringExtra(Constants.PARAM_TITLE));
        this.type = "shouyetuijian" + this.id;
        initFragment();
    }

    public void initFragment() {
        this.type = "shouyetuijian" + this.id;
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(this.type);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (this.frag != null) {
            fragmentTransaction.detach(this.frag);
        }
        if (findresult != null) {
            fragmentTransaction.attach(findresult);
            this.frag = findresult;
            fragmentTransaction.commitAllowingStateLoss();
            return;
        }
        this.frag = ShouYeTuiJianListFragment.newInstance(this.type, this.type);
        fragmentTransaction.add(R.id.part_content, this.frag, this.type);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void things(View view) {
    }

    public void setUp(resume_up ups) {
        this.upone = ups;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY))) {
            this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
            initFragment();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
