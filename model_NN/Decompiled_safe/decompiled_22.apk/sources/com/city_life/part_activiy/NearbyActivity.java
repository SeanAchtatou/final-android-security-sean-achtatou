package com.city_life.part_activiy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import com.city_life.part_fragment.NearByListFragment;
import com.palmtrends.baseui.BaseActivity;
import com.pengyou.citycommercialarea.R;

public class NearbyActivity extends BaseActivity {
    private Fragment frag = null;
    private Fragment mContent;
    private Fragment m_list_frag_1 = null;
    private String type = "";
    private TextView where;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main_part_ac);
        findViewById(R.id.title_back).setVisibility(8);
        findViewById(R.id.title_btn_right).setVisibility(8);
        ((TextView) findViewById(R.id.title_title)).setText("附近");
        initFragment();
    }

    public void initFragment() {
        Fragment findresult = getSupportFragmentManager().findFragmentByTag("NearByListFragment");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (NearByListFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = NearByListFragment.newInstance("NearByListFragment", "NearByListFragment");
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, "NearByListFragment");
        fragmentTransaction.commit();
    }

    public void things(View view) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
