package com.city_life.part_activiy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.part_fragment.ZixunListFragment;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class ZiXunListActivity extends BaseFragmentActivity {
    private Fragment frag = null;
    private Fragment mContent;
    private Fragment m_list_frag_1 = null;
    private TextView where;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main_part_ac);
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_btn_right).setVisibility(8);
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(ZiXunListActivity.this, CityListActivity.class);
                ZiXunListActivity.this.startActivity(intent);
            }
        });
        ((TextView) findViewById(R.id.title_title)).setText("资讯");
        initFragment();
    }

    public void initFragment() {
        Fragment findresult = getSupportFragmentManager().findFragmentByTag("ZixunListFragment");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (ZixunListFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = ZixunListFragment.newInstance("ZixunListFragment", "ZixunListFragment");
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, "ZixunListFragment");
        fragmentTransaction.commit();
    }

    public void things(View view) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.title_back)).setText(PerfHelper.getStringData(PerfHelper.P_CITY));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
