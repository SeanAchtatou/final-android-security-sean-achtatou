package com.city_life.part_activiy;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.fragment.MarketViewpager;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;
import com.viewpagerindicator.PageIndicator;
import com.viewpagerindicator.UnderlinePageIndicator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class ProfessionalmarketActivity extends BaseFragmentActivity {
    View actionbar_title;
    ArrayList<Listitem> adapter_list;
    private Button btn_page_city;
    int count = 1;
    boolean data_is_fail = true;
    /* access modifiers changed from: private */
    public GridPagerAdapter grid_pager_dapter;
    boolean isLogin;
    public int mFooter_limit = this.mLength;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            ProfessionalmarketActivity.this.pageNo = 1;
            switch (msg.what) {
                case 0:
                    new CurrentAync().execute(null);
                    return;
                case 1:
                    ProfessionalmarketActivity.this.point_gate_internet_data();
                    return;
                default:
                    return;
            }
        }
    };
    PageIndicator mIndicator;
    public int mLength = 20;
    public String mOldtype = "citys_list";
    public int mPage = 1;
    public String mParttype = "citys_list";
    /* access modifiers changed from: private */
    public ViewPager mViewPager;
    /* access modifiers changed from: private */
    public String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    int pageNo = 1;
    int pageNum = 12;
    private TextView where;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ShareApplication.debug) {
            System.out.println("加载:市场");
        }
        setContentView((int) R.layout.professionalmarket_layout);
        findViewById(R.id.title_back).setVisibility(0);
        ((TextView) findViewById(R.id.title_back)).setText(PerfHelper.getStringData(PerfHelper.P_CITY));
        this.where = (TextView) findViewById(R.id.title_btn_right);
        this.where.setVisibility(8);
        this.mViewPager = (ViewPager) findViewById(R.id.listv_viewpager);
        this.mIndicator = (UnderlinePageIndicator) findViewById(R.id.indicator);
        this.mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int arg0) {
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
        findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!((EditText) ProfessionalmarketActivity.this.findViewById(R.id.et_sendmessage)).getText().toString().trim().equals("")) {
                    Intent intent = new Intent();
                    intent.setClass(ProfessionalmarketActivity.this, SearchListActivity.class);
                    intent.putExtra("keyword", ((EditText) ProfessionalmarketActivity.this.findViewById(R.id.et_sendmessage)).getText().toString().trim());
                    ProfessionalmarketActivity.this.startActivity(intent);
                    return;
                }
                Utils.showToast("关键词不能为空");
            }
        });
        this.pageNo = 1;
        new CurrentAync().execute(null);
        findViewById(R.id.title_left_view).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(ProfessionalmarketActivity.this, CityListActivity.class);
                ProfessionalmarketActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.title_back)).setText(PerfHelper.getStringData(PerfHelper.P_CITY));
        if (!this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY))) {
            findViewById(R.id.refurbish_linear).setVisibility(0);
            findViewById(R.id.add_internetdata_linaer).setVisibility(8);
            findViewById(R.id.linear_refurbish).setVisibility(0);
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    new CurrentAync().execute(null);
                }
            }, 1000);
        }
    }

    public void point_gate_internet_data() {
        Button button = (Button) findViewById(R.id.but_refurbish_data);
        findViewById(R.id.add_internetdata_linaer).setVisibility(0);
        findViewById(R.id.linear_refurbish).setVisibility(8);
        findViewById(R.id.add_internetdata_linaer).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new CurrentAync().execute(null);
                ProfessionalmarketActivity.this.findViewById(R.id.linear_refurbish).setVisibility(0);
                ProfessionalmarketActivity.this.findViewById(R.id.add_internetdata_linaer).setVisibility(8);
            }
        });
    }

    class CurrentAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("pageNo", String.valueOf(ProfessionalmarketActivity.this.pageNo)));
            param.add(new BasicNameValuePair("pageNum", String.valueOf(ProfessionalmarketActivity.this.pageNum)));
            param.add(new BasicNameValuePair("cityId", PerfHelper.getStringData(PerfHelper.P_CITY_No)));
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                Data date = parseJson(DNDataSource.list_FromNET(ProfessionalmarketActivity.this.getResources().getString(R.string.citylife_market_list_url), param));
                mhashmap.put("countNum", date.obj);
                mhashmap.put("responseCode", date.obj1);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            if (ShareApplication.debug) {
                System.out.println("市场返回:" + json);
            }
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    data.obj = UploadUtils.SUCCESS;
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("countNum")) {
                data.obj = jsonobj.getString("countNum");
            }
            return data;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            int size;
            super.onPostExecute((Object) result);
            if (result == null || UploadUtils.FAILURE.equals(result.get("countNum"))) {
                ProfessionalmarketActivity.this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
                ProfessionalmarketActivity.this.data_is_fail = true;
                ProfessionalmarketActivity.this.point_gate_internet_data();
            } else if ("-2".equals(result.get("responseCode")) || "-1".equals(result.get("responseCode"))) {
                ProfessionalmarketActivity.this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
                ProfessionalmarketActivity.this.data_is_fail = true;
                Toast.makeText(ProfessionalmarketActivity.this, ProfessionalmarketActivity.this.getResources().getString(R.string.no_data), 0).show();
                ProfessionalmarketActivity.this.point_gate_internet_data();
            } else if (UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                ArrayList<MarketViewpager> fragments = new ArrayList<>();
                int sizes = Integer.parseInt(result.get("countNum").toString());
                if (PerfHelper.getIntData(PerfHelper.P_PHONE_H) > 830) {
                    size = sizes % 12 == 0 ? sizes / 12 : (sizes / 12) + 1;
                } else {
                    size = sizes % 9 == 0 ? sizes / 9 : (sizes / 9) + 1;
                }
                for (int i = 0; i < size; i++) {
                    fragments.add(new MarketViewpager(i + 1));
                }
                if (ProfessionalmarketActivity.this.grid_pager_dapter == null) {
                    ProfessionalmarketActivity.this.grid_pager_dapter = new GridPagerAdapter(ProfessionalmarketActivity.this.getSupportFragmentManager(), fragments);
                    ProfessionalmarketActivity.this.mViewPager.setAdapter(ProfessionalmarketActivity.this.grid_pager_dapter);
                    ProfessionalmarketActivity.this.grid_pager_dapter.notifyDataSetChanged();
                    ProfessionalmarketActivity.this.mIndicator.setViewPager(ProfessionalmarketActivity.this.mViewPager);
                } else {
                    ProfessionalmarketActivity.this.grid_pager_dapter.setFragments(ProfessionalmarketActivity.this.getSupportFragmentManager(), fragments, size);
                    ProfessionalmarketActivity.this.mViewPager.setCurrentItem(0);
                    ProfessionalmarketActivity.this.mIndicator.notifyDataSetChanged();
                }
                ProfessionalmarketActivity.this.findViewById(R.id.refurbish_linear).setVisibility(8);
                ProfessionalmarketActivity.this.data_is_fail = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    }

    public class GridPagerAdapter extends FragmentPagerAdapter {
        private FragmentManager fm;
        private ArrayList<MarketViewpager> fraglist = null;
        private int size;

        public GridPagerAdapter(FragmentManager fm2, int sizes) {
            super(fm2);
            if (ShareApplication.debug) {
                System.out.println("屏幕高度:" + PerfHelper.getIntData(PerfHelper.P_PHONE_H));
            }
            if (PerfHelper.getIntData(PerfHelper.P_PHONE_H) > 830) {
                this.size = sizes % 12 == 0 ? sizes / 12 : (sizes / 12) + 1;
            } else {
                this.size = sizes % 9 == 0 ? sizes / 9 : (sizes / 9) + 1;
            }
        }

        public GridPagerAdapter(FragmentManager fm2, ArrayList<MarketViewpager> fraglist2) {
            super(fm2);
            this.fraglist = fraglist2;
            this.size = fraglist2.size();
        }

        public int getCount() {
            return this.size;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }

        public void setFragments(FragmentManager fm2, ArrayList<MarketViewpager> fragments, int size2) {
            this.fm = fm2;
            if (this.fraglist != null) {
                FragmentTransaction ft = fm2.beginTransaction();
                Iterator<MarketViewpager> it = this.fraglist.iterator();
                while (it.hasNext()) {
                    ft.remove(it.next());
                }
                ft.commit();
                fm2.executePendingTransactions();
            }
            this.size = size2;
            this.fraglist = fragments;
            notifyDataSetChanged();
        }

        public int getItemPosition(Object object) {
            return -2;
        }

        public Fragment getItem(int arg0) {
            return this.fraglist.get(arg0);
        }
    }
}
