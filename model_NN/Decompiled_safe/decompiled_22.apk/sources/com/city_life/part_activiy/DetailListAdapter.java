package com.city_life.part_activiy;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.pengyou.citycommercialarea.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailListAdapter extends BaseExpandableListAdapter {
    private List<List<Map<String, Object>>> childList = new ArrayList();
    private boolean flag = false;
    private ImageView imgView;
    private LayoutInflater layoutInflater;
    private Context mContext;
    private Map<String, Object> mapAddress = new HashMap();
    /* access modifiers changed from: private */
    public Handler mhandler;
    private List<Map<String, Object>> parentList = new ArrayList();

    public DetailListAdapter(Context mContext2, List<Map<String, Object>> parentList2, List<List<Map<String, Object>>> childList2, Map<String, Object> mapAddress2, Handler mhandler2) {
        this.mContext = mContext2;
        this.parentList = parentList2;
        this.childList = childList2;
        this.mapAddress = mapAddress2;
        this.layoutInflater = LayoutInflater.from(mContext2);
        this.mhandler = mhandler2;
    }

    public Object getChild(int groupPosition, int childPosition) {
        return ((Map) this.childList.get(groupPosition).get(childPosition)).get("Title").toString();
    }

    public long getChildId(int groupPosition, int childPosition) {
        return (long) childPosition;
    }

    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = this.layoutInflater.inflate((int) R.layout.detail_grade_list_child, (ViewGroup) null);
        }
        convertView.setBackgroundResource(R.drawable.list_selector);
        ((ImageView) convertView.findViewById(R.id.detail_xlcxlayoutimageview)).setImageResource(Integer.valueOf(((Map) this.childList.get(groupPosition).get(childPosition)).get("Head").toString()).intValue());
        ((TextView) convertView.findViewById(R.id.detail_layout_xlcxtextview)).setText(((Map) this.childList.get(groupPosition).get(childPosition)).get("Title").toString());
        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DetailListAdapter.this.mhandler.obtainMessage(childPosition).sendToTarget();
            }
        });
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return this.childList.get(groupPosition).size();
    }

    public Object getGroup(int groupPosition) {
        return this.parentList.get(groupPosition).get("List").toString();
    }

    public int getGroupCount() {
        return this.parentList.size();
    }

    public long getGroupId(int groupPosition) {
        return (long) groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = this.layoutInflater.inflate((int) R.layout.detail_grade_list_parent, (ViewGroup) null);
        }
        convertView.setBackgroundResource(R.drawable.list_selector);
        if (this.flag) {
            this.imgView = (ImageView) convertView.findViewById(R.id.detail_layout_jiantou);
            this.imgView.setBackgroundResource(R.drawable.jiantou2);
            this.flag = false;
        } else {
            this.imgView = (ImageView) convertView.findViewById(R.id.detail_layout_jiantou);
            this.imgView.setBackgroundResource(R.drawable.jiantou);
            this.flag = true;
        }
        ((TextView) convertView.findViewById(R.id.detail_layout_dztextview)).setText(this.parentList.get(groupPosition).get("List").toString());
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
