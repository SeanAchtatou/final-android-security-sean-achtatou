package com.city_life.part_activiy;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mapapi.search.MKSearch;
import com.baidumap.BaiduLocationMarkActivity;
import com.baidumap.BaiduLocationOverlayActivity;
import com.city_life.entity.CityLifeApplication;
import com.city_life.part_asynctask.GetFavListId;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseArticleActivity;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.datasource.ImageLoadUtils;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.utils.FileUtils;
import com.utils.PerfHelper;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class DetailActivity extends BaseArticleActivity implements View.OnClickListener {
    ArrayList<Listitem> adapter_list;
    String address;
    /* access modifiers changed from: private */
    public ImageView btn_collect;
    private ImageView btn_comment;
    private TextView btn_return;
    private String[] childTitle;
    String cityName;
    private ExpandableListView eblist_routeQuery;
    String endLatitude;
    String endLongitude;
    String endPlace;
    private GridView gv_businesses;
    private GridView gv_productPicture;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    DetailActivity.this.travelMode = "bus";
                    DetailActivity.this.cityName = "重庆市";
                    if (DetailActivity.this.statrLongitude.equals("null") || DetailActivity.this.statrLongitude.trim().length() <= 0 || DetailActivity.this.startLatitude.equals("null") || DetailActivity.this.startLatitude.trim().length() <= 0 || DetailActivity.this.endLongitude.equals("null") || DetailActivity.this.endLongitude.trim().length() <= 0 || DetailActivity.this.endLatitude.equals("null") || DetailActivity.this.endLatitude.trim().length() <= 0) {
                        Toast.makeText(DetailActivity.this, "对不起由于无法获取相关定位信息，无法使用该功能", 1).show();
                        return;
                    } else {
                        DetailActivity.this.toMapActivity(DetailActivity.this.cityName, DetailActivity.this.travelMode, DetailActivity.this.statrLongitude, DetailActivity.this.startLatitude, DetailActivity.this.endLongitude, DetailActivity.this.endLatitude, DetailActivity.this.address, DetailActivity.this);
                        return;
                    }
                case 1:
                    DetailActivity.this.travelMode = "drive";
                    DetailActivity.this.cityName = "重庆市";
                    if (DetailActivity.this.statrLongitude.equals("null") || DetailActivity.this.statrLongitude.trim().length() <= 0 || DetailActivity.this.startLatitude.equals("null") || DetailActivity.this.startLatitude.trim().length() <= 0 || DetailActivity.this.endLongitude.equals("null") || DetailActivity.this.endLongitude.trim().length() <= 0 || DetailActivity.this.endLatitude.equals("null") || DetailActivity.this.endLatitude.trim().length() <= 0) {
                        Toast.makeText(DetailActivity.this, "对不起由于无法获取相关定位信息，无法使用该功能", 1).show();
                        return;
                    } else {
                        DetailActivity.this.toMapActivity(DetailActivity.this.cityName, DetailActivity.this.travelMode, DetailActivity.this.statrLongitude, DetailActivity.this.startLatitude, DetailActivity.this.endLongitude, DetailActivity.this.endLatitude, DetailActivity.this.address, DetailActivity.this);
                        return;
                    }
                case 2:
                    DetailActivity.this.travelMode = "walk";
                    DetailActivity.this.cityName = "重庆市";
                    if (DetailActivity.this.statrLongitude.equals("null") || DetailActivity.this.statrLongitude.trim().length() <= 0 || DetailActivity.this.startLatitude.equals("null") || DetailActivity.this.startLatitude.trim().length() <= 0 || DetailActivity.this.endLongitude.equals("null") || DetailActivity.this.endLongitude.trim().length() <= 0 || DetailActivity.this.endLatitude.equals("null") || DetailActivity.this.endLatitude.trim().length() <= 0) {
                        Toast.makeText(DetailActivity.this, "对不起由于无法获取相关定位信息，无法使用该功能", 1).show();
                        return;
                    } else {
                        DetailActivity.this.toMapActivity(DetailActivity.this.cityName, DetailActivity.this.travelMode, DetailActivity.this.statrLongitude, DetailActivity.this.startLatitude, DetailActivity.this.endLongitude, DetailActivity.this.endLatitude, DetailActivity.this.address, DetailActivity.this);
                        return;
                    }
                default:
                    return;
            }
        }
    };
    private int[] headImage = {R.drawable.gjlx, R.drawable.zjlx, R.drawable.bx};
    private ImageView img_collection;
    private ImageView imgv_sc_listv;
    private String[] listName;
    /* access modifiers changed from: private */
    public ListView listview;
    CommentAdapter listview_adapter;
    View.OnClickListener onclick_lister = new View.OnClickListener() {
        public void onClick(View v) {
            new Intent();
            Intent intent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + DetailActivity.this.mCurrentItem.phone));
            intent.setFlags(268435456);
            DetailActivity.this.startActivity(intent);
        }
    };
    String pinglun_how = UploadUtils.SUCCESS;
    private RatingBar rb_grade_below;
    private RatingBar rb_grade_top;
    private RelativeLayout rl_businessLocation;
    private RelativeLayout rl_phone;
    private RelativeLayout rl_seeTheComment;
    private MKSearch searchModel;
    String startLatitude;
    String startPlace;
    String statrLongitude;
    private TextView text_address;
    private TextView text_businessClass_below;
    private TextView text_businessClass_top;
    private TextView text_businessIntroduction;
    private TextView text_companyName;
    private TextView text_companyPhone;
    private TextView text_preferential;
    private TextView text_productIntroduction;
    String travelMode = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.article_detail_activity);
        if (getIntent().getExtras() != null) {
            this.mCurrentItem = (Listitem) getIntent().getExtras().get("item");
        }
        initView();
        if (this.mCurrentItem != null) {
            this.rb_grade_top.setRating(Float.valueOf(this.mCurrentItem.level).floatValue());
            this.text_companyName.setText(this.mCurrentItem.title);
            this.text_preferential.setText(this.mCurrentItem.preferential);
            this.text_companyPhone.setText(this.mCurrentItem.phone);
            this.text_productIntroduction.setText(this.mCurrentItem.fuwu);
            this.text_businessIntroduction.setText(this.mCurrentItem.shangjia);
            this.text_address.setText(this.mCurrentItem.address);
        }
        init();
        this.eblist_routeQuery.setAdapter(new DetailListAdapter(this, getParentList(), getChildList(), null, this.handler));
        this.eblist_routeQuery.setGroupIndicator(null);
        this.eblist_routeQuery.setDivider(null);
        this.eblist_routeQuery.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            public void onGroupExpand(int groupPosition) {
            }
        });
        this.statrLongitude = new StringBuilder(String.valueOf(PerfHelper.getStringData(PerfHelper.P_GPS_LONG))).toString();
        this.startLatitude = new StringBuilder(String.valueOf(PerfHelper.getStringData(PerfHelper.P_GPS_LATI))).toString();
        this.endLongitude = this.mCurrentItem.longitude;
        this.endLatitude = this.mCurrentItem.latitude;
        this.address = this.mCurrentItem.address;
        new CurrentAync().execute(null);
    }

    public void toMapActivity(String cityName2, String travelMode2, String statrLongitude2, String startLatitude2, String endLongitude2, String endLatitude2, String address2, Context context) {
        if ("".equals(travelMode2) || "".equals(statrLongitude2) || "".equals(startLatitude2) || "".equals(endLongitude2) || "".equals(endLatitude2) || "".equals(cityName2) || "".equals(address2)) {
            Toast.makeText(context, "传递地址信息不能有空值", 1).show();
            return;
        }
        Intent intent = new Intent(context, BaiduLocationOverlayActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("cityName", cityName2);
        bundle.putString("travelMode", travelMode2);
        bundle.putString("statrLongitude", statrLongitude2);
        bundle.putString("startLatitude", startLatitude2);
        bundle.putString("endLongitude", endLongitude2);
        bundle.putString("endLatitude", endLatitude2);
        bundle.putString("address", address2);
        intent.putExtra("map", bundle);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }

    private void initView() {
        this.imgv_sc_listv = (ImageView) findViewById(R.id.imgv_sc_listv);
        this.btn_return = (TextView) findViewById(R.id.title_back);
        this.btn_comment = (ImageView) findViewById(R.id.btn_detail_comment);
        this.btn_collect = (ImageView) findViewById(R.id.btn_detail_collect);
        this.text_address = (TextView) findViewById(R.id.textv_detail_address);
        this.text_companyName = (TextView) findViewById(R.id.textv_detail_company_name);
        this.text_companyPhone = (TextView) findViewById(R.id.textv_detail_phone_number);
        this.text_preferential = (TextView) findViewById(R.id.textv_detail_preferential);
        this.text_businessClass_top = (TextView) findViewById(R.id.textv_detail_review_rating_top);
        this.text_businessClass_below = (TextView) findViewById(R.id.textv_detail_review_rating_below);
        this.text_productIntroduction = (TextView) findViewById(R.id.textv_detail_product_introduction);
        this.text_businessIntroduction = (TextView) findViewById(R.id.textv_detail_business_introduction);
        this.rb_grade_top = (RatingBar) findViewById(R.id.rb_detail_grade_top);
        this.rb_grade_below = (RatingBar) findViewById(R.id.rb_detail_grade_below);
        this.rl_businessLocation = (RelativeLayout) findViewById(R.id.rl_detail_business_location);
        this.rl_phone = (RelativeLayout) findViewById(R.id.rl_detail_phone);
        this.rl_seeTheComment = (RelativeLayout) findViewById(R.id.rl_detail_see_the_comments);
        this.gv_productPicture = (GridView) findViewById(R.id.gv_detail_product_picture);
        this.gv_businesses = (GridView) findViewById(R.id.gv_detail_businesses);
        this.listview = (ListView) findViewById(R.id.listv_detail_comment);
        this.eblist_routeQuery = (ExpandableListView) findViewById(R.id.expandableListView);
        this.listName = getResources().getStringArray(R.array.routeQuery);
        this.childTitle = getResources().getStringArray(R.array.queryMode);
        this.btn_return.setOnClickListener(this);
        this.btn_comment.setOnClickListener(this);
        this.btn_collect.setOnClickListener(this);
        findViewById(R.id.btn_share).setOnClickListener(this);
        this.text_companyPhone.setOnClickListener(this);
        this.rl_businessLocation.setOnClickListener(this);
        this.rl_seeTheComment.setOnClickListener(this);
        this.rl_phone.setEnabled(true);
        this.rl_phone.setFocusable(true);
        boolean isFav = false;
        final String sharepic = this.mCurrentItem.icon.split(",")[0];
        final String shortpic = sharepic.replace(Urls.main, "");
        new Thread() {
            public void run() {
                try {
                    if (!FileUtils.isFileExist("image/" + shortpic)) {
                        ImageLoadUtils.downloadFile(sharepic, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
        if (PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN) && CityLifeApplication.fav_list_id != null) {
            ArrayList<Listitem> items = CityLifeApplication.fav_list_id;
            int i = 0;
            while (true) {
                if (i >= items.size()) {
                    break;
                } else if (items.get(i).nid.equals(this.mCurrentItem.nid)) {
                    this.btn_collect.setBackgroundResource(R.drawable.fav_btn);
                    isFav = true;
                    this.btn_collect.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            new AlertDialog.Builder(DetailActivity.this).setMessage("是否取消收藏？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Utils.showProcessDialog(DetailActivity.this, "正在取消收藏...");
                                    new Thread() {
                                        public void run() {
                                            new CollectAync(false).execute(null);
                                        }
                                    }.start();
                                }
                            }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                }
                            }).show();
                        }
                    });
                    break;
                } else {
                    i++;
                }
            }
        }
        if (!isFav) {
            this.btn_collect.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new AlertDialog.Builder(DetailActivity.this).setMessage("是否收藏？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Utils.showProcessDialog(DetailActivity.this, "正在收藏...");
                            new Thread() {
                                public void run() {
                                    new CollectAync(true).execute(null);
                                }
                            }.start();
                        }
                    }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show();
                }
            });
        }
    }

    public List<Map<String, Object>> getParentList() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (String put : this.listName) {
            Map<String, Object> curGroupMap = new HashMap<>();
            list.add(curGroupMap);
            curGroupMap.put("List", put);
        }
        return list;
    }

    public List<List<Map<String, Object>>> getChildList() {
        List<List<Map<String, Object>>> list = new ArrayList<>();
        for (int i = 0; i < this.listName.length; i++) {
            List<Map<String, Object>> children = new ArrayList<>();
            for (int j = 0; j < this.childTitle.length; j++) {
                Map<String, Object> curChildMap = new HashMap<>();
                children.add(curChildMap);
                curChildMap.put("Title", this.childTitle[j]);
                curChildMap.put("Head", Integer.valueOf(this.headImage[j]));
            }
            list.add(children);
        }
        return list;
    }

    private void init() {
        ArrayList<Integer> list_imgUrl = new ArrayList<>();
        int screenW = getWindowManager().getDefaultDisplay().getWidth();
        int columnWidth = (int) getNewX(screenW, 117);
        int horizontalSpacing = (int) getNewX(screenW, 2);
        list_imgUrl.add(Integer.valueOf((int) R.drawable.default_picture));
        list_imgUrl.add(Integer.valueOf((int) R.drawable.ic_launcher));
        list_imgUrl.add(Integer.valueOf((int) R.drawable.default_picture));
        list_imgUrl.add(Integer.valueOf((int) R.drawable.ic_launcher));
        list_imgUrl.add(Integer.valueOf((int) R.drawable.default_picture));
        list_imgUrl.add(Integer.valueOf((int) R.drawable.ic_launcher));
        String[] str_pro = this.mCurrentItem.img_list_1.split(",");
        String[] str_seller = this.mCurrentItem.img_list_2.split(",");
        if (ShareApplication.debug) {
            System.out.println("图片集1:" + this.mCurrentItem.img_list_1);
            System.out.println("图片集2:" + this.mCurrentItem.img_list_2);
            System.out.println("图片集2:" + str_seller.length);
        }
        AdapterGrid adapter01 = new AdapterGrid(this, this.mCurrentItem, str_seller, this.gv_productPicture);
        if (str_seller.length < 1) {
            findViewById(R.id.linearLayout).setVisibility(8);
        } else {
            this.gv_productPicture.setAdapter((ListAdapter) adapter01);
        }
        AdapterGrid adapter02 = new AdapterGrid(this, this.mCurrentItem, str_pro, this.gv_businesses);
        if (str_pro.length < 1) {
            findViewById(R.id.linearLayouts).setVisibility(8);
        } else {
            this.gv_businesses.setAdapter((ListAdapter) adapter02);
        }
        this.gv_productPicture.setColumnWidth(columnWidth);
        this.gv_businesses.setColumnWidth(columnWidth);
        int size_pro = str_pro.length;
        int size_sel = str_seller.length;
        this.gv_productPicture.setNumColumns(size_sel);
        this.gv_businesses.setNumColumns(size_pro);
        LinearLayout.LayoutParams params_pro = new LinearLayout.LayoutParams((size_pro * columnWidth) + ((size_pro - 1) * horizontalSpacing), -2);
        this.gv_productPicture.setLayoutParams(new LinearLayout.LayoutParams((size_sel * columnWidth) + ((size_sel - 1) * horizontalSpacing), -2));
        this.gv_businesses.setLayoutParams(params_pro);
        ShareApplication.mImageWorker.loadImage(this.mCurrentItem.icon, this.imgv_sc_listv);
    }

    private float getNewX(int screenW, int oldX) {
        return ((float) (screenW * oldX)) / 480.0f;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 2) {
            findViewById(R.id.refurbish_linear).setVisibility(0);
            new CurrentAync().execute(null);
        }
    }

    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            case R.id.btn_detail_collect /*2131230926*/:
            default:
                return;
            case R.id.btn_detail_comment /*2131230927*/:
                Intent intent2 = new Intent();
                intent2.putExtra(LocaleUtil.INDONESIAN, this.mCurrentItem.nid);
                intent2.putExtra("count", this.pinglun_how);
                intent2.setAction(getResources().getString(R.string.activity_article_comment));
                startActivityForResult(intent2, 1);
                return;
            case R.id.btn_share /*2131230928*/:
                String[] str_pro = this.mCurrentItem.icon.split(",");
                this.picurl = str_pro[0];
                this.shorturl = str_pro[0];
                new AlertDialog.Builder(this).setTitle("分享方式").setItems(getResources().getStringArray(R.array.article_list_name), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String sname = DetailActivity.this.getResources().getStringArray(R.array.article_wb_list_name_sa)[which];
                        if (sname.startsWith("wx")) {
                            if (!DetailActivity.this.api.isWXAppInstalled()) {
                                new AlertDialog.Builder(DetailActivity.this).setTitle("提示").setMessage("尚未安装“微信”，无法使用此功能").setPositiveButton("现在安装", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            DetailActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=腾讯微信")));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();
                                    }
                                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                                return;
                            }
                            if (sname.equals("wx")) {
                                DetailActivity.this.weixin_type = 0;
                            } else {
                                DetailActivity.this.weixin_type = 1;
                                if (DetailActivity.this.api.getWXAppSupportAPI() < 553779201) {
                                    Utils.showToast("微信版本过低，暂时不支持分享到朋友圈");
                                    return;
                                }
                            }
                            DetailActivity.this.sendToWeixin();
                        } else if ("yj".equals(sname)) {
                            DetailActivity.this.shareEmail(DetailActivity.this.shorturl, DetailActivity.this.picurl, DetailActivity.this.mCurrentItem.title);
                        } else {
                            Intent i = new Intent();
                            i.setAction(DetailActivity.this.getResources().getString(R.string.activity_share));
                            i.putExtra("sname", sname);
                            i.putExtra("shorturl", DetailActivity.this.mCurrentItem.phone);
                            i.putExtra("aid", DetailActivity.this.mCurrentItem.nid);
                            i.putExtra(Constants.PARAM_APP_ICON, DetailActivity.this.picurl == null ? "" : DetailActivity.this.picurl);
                            i.putExtra("item", DetailActivity.this.mCurrentItem);
                            DetailActivity.this.startActivity(i);
                        }
                    }
                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
            case R.id.rl_detail_business_location /*2131230929*/:
                Intent intents = new Intent();
                intents.putExtra("item", this.mCurrentItem);
                intents.setClass(this, BaiduLocationMarkActivity.class);
                startActivity(intents);
                return;
            case R.id.rl_detail_see_the_comments /*2131230960*/:
                intent.putExtra("mCurrentItem", this.mCurrentItem);
                intent.putExtra(LocaleUtil.INDONESIAN, this.mCurrentItem.nid);
                intent.putExtra("count", this.pinglun_how);
                intent.setAction(getResources().getString(R.string.activity_article_comment_list));
                startActivityForResult(intent, 1);
                return;
            case R.id.textv_detail_phone_number /*2131230971*/:
                Intent intent3 = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.mCurrentItem.phone));
                intent3.setFlags(268435456);
                startActivity(intent3);
                return;
        }
    }

    public String getSDPath() {
        File sdDir = null;
        if (Environment.getExternalStorageState().equals("mounted")) {
            sdDir = Environment.getExternalStorageDirectory();
        }
        return sdDir.toString();
    }

    class CollectAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        boolean isFav;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CollectAync(boolean isFav2) {
            this.isFav = isFav2;
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            String json;
            if (!PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN)) {
                HashMap<String, Object> mhashmap = new HashMap<>();
                mhashmap.put("responseCode", "-3");
                return mhashmap;
            }
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("sellerId", DetailActivity.this.mCurrentItem.nid));
            param.add(new BasicNameValuePair("userId", PerfHelper.getStringData(PerfHelper.P_USERID)));
            HashMap<String, Object> mhashmap2 = new HashMap<>();
            try {
                if (this.isFav) {
                    json = DNDataSource.list_FromNET(DetailActivity.this.getResources().getString(R.string.citylife_putCollect_list_url), param);
                } else {
                    json = DNDataSource.list_FromNET(DetailActivity.this.getResources().getString(R.string.citylife_delCollect_list_url), param);
                }
                if (ShareApplication.debug) {
                    System.out.println("收藏返回:" + json);
                }
                mhashmap2.put("responseCode", parseJson(json).obj1);
                return mhashmap2;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("countNum")) {
                data.obj = jsonobj.getString("countNum");
            }
            return data;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (this.isFav) {
                if (result == null || UploadUtils.FAILURE.equals(result.get("responseCode"))) {
                    Toast.makeText(DetailActivity.this, "请求失败，请稍后再试", 0).show();
                } else if ("2".equals(result.get("responseCode"))) {
                    Toast.makeText(DetailActivity.this, "对不起，无法重复收藏", 0).show();
                } else if (UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                    Toast.makeText(DetailActivity.this, "用户收藏成功", 0).show();
                    DetailActivity.this.btn_collect.setBackgroundResource(R.drawable.fav_btn);
                    DetailActivity.this.btn_collect.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            new AlertDialog.Builder(DetailActivity.this).setMessage("是否取消收藏？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Utils.showProcessDialog(DetailActivity.this, "正在取消收藏...");
                                    new Thread() {
                                        public void run() {
                                            new CollectAync(false).execute(null);
                                        }
                                    }.start();
                                }
                            }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                }
                            }).show();
                        }
                    });
                    new GetFavListId().execute(null);
                } else if ("-3".equals(result.get("responseCode"))) {
                    Toast.makeText(DetailActivity.this, "游客无法执行该操作,请先登录", 0).show();
                }
            } else if (result == null || UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                Toast.makeText(DetailActivity.this, "取消收藏成功", 0).show();
                DetailActivity.this.btn_collect.setBackgroundResource(R.drawable.faved_btn);
                DetailActivity.this.btn_collect.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        new AlertDialog.Builder(DetailActivity.this).setMessage("是否收藏？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Utils.showProcessDialog(DetailActivity.this, "正在收藏...");
                                new Thread() {
                                    public void run() {
                                        new CollectAync(true).execute(null);
                                    }
                                }.start();
                            }
                        }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                            }
                        }).show();
                    }
                });
                new GetFavListId().execute(null);
            } else {
                Toast.makeText(DetailActivity.this, "取消收藏失败", 0).show();
            }
        }
    }

    class CurrentAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        TextView countNum;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("sellerId", DetailActivity.this.mCurrentItem.nid));
            param.add(new BasicNameValuePair("pageNo", UploadUtils.FAILURE));
            param.add(new BasicNameValuePair("pageNum", "3"));
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(DetailActivity.this.getResources().getString(R.string.citylife_getComment_list_url), param);
                if (ShareApplication.debug) {
                    System.out.println("评论返回:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("countNum", date.obj);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.list);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("countNum")) {
                data.obj = jsonobj.getString("countNum");
            }
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has("conent")) {
                        o.des = obj.getString("conent");
                    }
                    if (obj.has("level")) {
                        o.level = obj.getString("level");
                    }
                    if (obj.has("nickName")) {
                        o.title = obj.getString("nickName");
                    }
                    if (obj.has("addtime")) {
                        o.u_date = obj.getString("addtime");
                    }
                } catch (Exception e) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            if (result == null || UploadUtils.FAILURE.equals(result.get("responseCode"))) {
                DetailActivity.this.findViewById(R.id.refurbish_linear).setVisibility(8);
                DetailActivity.this.findViewById(R.id.look_pinglun).setVisibility(8);
                return;
            }
            if ("-2".equals(result.get("responseCode")) || "-1".equals(result.get("responseCode"))) {
                DetailActivity.this.findViewById(R.id.refurbish_linear).setVisibility(8);
                DetailActivity.this.findViewById(R.id.look_pinglun).setVisibility(8);
            } else if (UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                DetailActivity.this.adapter_list = (ArrayList) result.get("results");
                DetailActivity.this.listview_adapter = new CommentAdapter(DetailActivity.this, DetailActivity.this.adapter_list);
                DetailActivity.this.listview.setAdapter((ListAdapter) DetailActivity.this.listview_adapter);
                DetailActivity.this.findViewById(R.id.look_pinglun).setVisibility(0);
                this.countNum = (TextView) DetailActivity.this.findViewById(R.id.detail_pltshtext);
                DetailActivity.this.pinglun_how = result.get("countNum").toString();
                this.countNum.setText(String.valueOf(result.get("countNum").toString()) + "条");
            }
            if (result.get("collectNo") != null && !"-2".equals(result.get("collectNo").toString().trim())) {
                System.out.println("2 " + Integer.valueOf(result.get("collectNo").toString().trim()) + "未收藏" + result.get("collectNo").toString().trim());
            }
            DetailActivity.this.findViewById(R.id.refurbish_linear).setVisibility(8);
        }
    }
}
