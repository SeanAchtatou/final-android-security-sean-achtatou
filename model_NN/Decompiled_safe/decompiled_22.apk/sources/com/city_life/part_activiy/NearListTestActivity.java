package com.city_life.part_activiy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.part_fragment.NearByPartFragment;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class NearListTestActivity extends BaseFragmentActivity {
    private Fragment frag = null;
    private Fragment mContent;
    private Fragment m_list_frag_1 = null;
    private Fragment m_list_frag_2 = null;
    String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    private String type = "";
    private TextView where;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main_part_ac);
        findViewById(R.id.title_back).setVisibility(8);
        findViewById(R.id.title_btn_right).setVisibility(0);
        findViewById(R.id.title_btn_right).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(NearListTestActivity.this, CityListActivity.class);
                NearListTestActivity.this.startActivity(intent);
            }
        });
        ((TextView) findViewById(R.id.title_title)).setText("附近");
        initFragment();
    }

    public void initFragment() {
        this.type = "nearbypart" + PerfHelper.getStringData(PerfHelper.P_CITY_No);
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(this.type);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (NearByPartFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = NearByPartFragment.newInstance(this.type);
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, this.type);
        fragmentTransaction.commit();
    }

    public void things(View view) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.title_btn_right)).setText(PerfHelper.getStringData(PerfHelper.P_CITY));
        if (!this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY))) {
            this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
            ((NearByPartFragment) this.m_list_frag_1).setSecondGone("nearbypart" + PerfHelper.getStringData(PerfHelper.P_CITY_No));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
