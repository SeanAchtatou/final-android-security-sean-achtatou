package com.city_life.part_activiy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;
import com.volley.msg.BitmapCache;

public class HuodongyaoyiyaoActivity extends BaseActivity {
    Listitem item;
    private ImageLoader mImageLoader;
    private RequestQueue mQueue;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_shouye_yaoyiyao);
        if (getIntent().getExtras() != null) {
            this.item = (Listitem) getIntent().getExtras().get("item");
        }
        this.mQueue = Volley.newRequestQueue(this);
        this.mImageLoader = new ImageLoader(this.mQueue, new BitmapCache());
        initFragment();
    }

    public void initFragment() {
        ImageView headicon = (ImageView) findViewById(R.id.yaojiang_head);
        if (this.item.icon != null && this.item.icon.length() > 10) {
            this.mImageLoader.get(this.item.author, ImageLoader.getImageListener(headicon, R.drawable.gengduo_bg_img, R.drawable.gengduo_bg_img));
        }
        ((TextView) findViewById(R.id.kaijiang_time)).setText(this.item.other);
        ((TextView) findViewById(R.id.huodong_time)).setText("从" + this.item.u_date + "至" + this.item.other);
        ((TextView) findViewById(R.id.huodong_guize)).setText(this.item.other1);
        ((TextView) findViewById(R.id.huodong_jiangping)).setText(this.item.other2);
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HuodongyaoyiyaoActivity.this.things(v);
            }
        });
        findViewById(R.id.yaoyiyao_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HuodongyaoyiyaoActivity.this.things(v);
            }
        });
    }

    public void things(View view) {
        switch (view.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            case R.id.yaoyiyao_btn /*2131230878*/:
                if (!PerfHelper.getBooleanData(PerfHelper.P_USER_LOGIN) || PerfHelper.getStringData(PerfHelper.P_USERID).equals("游客")) {
                    Intent intent = new Intent();
                    intent.setClass(this, UserLogin.class);
                    startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent();
                intent2.setClass(this, YaoYiYaoActivity.class);
                startActivity(intent2);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
