package com.city_life.part_activiy;

import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.tauth.Constants;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class YaoYiYaoActivity extends BaseActivity implements SensorEventListener {
    private static final int SENSOR_SHAKE = 10;
    /* access modifiers changed from: private */
    public ImageView cishu;
    public int cishus;
    public int cishus2;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 10:
                    Log.i("uri", "检测到摇晃，执行操作！");
                    YaoYiYaoActivity.this.yao_logo.startAnimation(AnimationUtils.loadAnimation(YaoYiYaoActivity.this.getApplicationContext(), R.animator.yaoyiyao_rotate));
                    if (YaoYiYaoActivity.this.cishus2 == 0 || YaoYiYaoActivity.this.cishus2 < 0) {
                        Utils.showToast("你已经没有摇奖次数了，明天再来吧!!!");
                        return;
                    }
                    new CurrentAync().execute(null);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public TextView haoma;
    /* access modifiers changed from: private */
    public MediaPlayer player;
    Random r = new Random();
    private SensorManager sensorManager;
    private Vibrator vibrator;
    /* access modifiers changed from: private */
    public ImageView yao_logo;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_yaoyiyao);
        this.sensorManager = (SensorManager) getSystemService("sensor");
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.player = MediaPlayer.create(this, (int) R.raw.shake_sound_male);
        this.yao_logo = (ImageView) findViewById(R.id.yaoyiyao_logo);
        this.haoma = (TextView) findViewById(R.id.yaoyaiyao_no);
        this.cishu = (ImageView) findViewById(R.id.yaoyiyao_cishu);
        new CurrentAyncCisu().execute(null);
        Utils.showProcessDialog(this, "正在查询摇奖次数,请稍后...");
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                YaoYiYaoActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.sensorManager != null) {
            this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(1), 2);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.sensorManager.unregisterListener(this);
        super.onStop();
        this.player.stop();
        Log.i(Constants.PARAM_URL, "player.stop()");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.sensorManager.unregisterListener(this);
        super.onPause();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;
        float x = values[0];
        float y = values[1];
        float z = values[2];
        if (Math.abs(x) > ((float) 15) || Math.abs(y) > ((float) 15) || Math.abs(z) > ((float) 15)) {
            try {
                this.player.start();
                this.vibrator.vibrate(500);
                final Message msg = new Message();
                msg.what = 10;
                this.player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        YaoYiYaoActivity.this.player.pause();
                        YaoYiYaoActivity.this.player.seekTo(0);
                        YaoYiYaoActivity.this.handler.sendMessage(msg);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void things(View view) {
    }

    class CurrentAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            ArrayList<NameValuePair> list = new ArrayList<>();
            list.add(new BasicNameValuePair("sex", UploadUtils.FAILURE));
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(String.valueOf(YaoYiYaoActivity.this.getResources().getString(R.string.citylife_yaoyiyao_url)) + PerfHelper.getStringData("phone"), list);
                if (ShareApplication.debug) {
                    System.out.println("要一要:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.obj);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("draw")) {
                data.obj = jsonobj.getString("draw");
            }
            YaoYiYaoActivity.this.cishus = jsonobj.getInt("lack");
            return data;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (result != null && !UploadUtils.FAILURE.equals(result.get("responseCode")) && !"2".equals(result.get("responseCode")) && !"3".equals(result.get("responseCode")) && UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                String str = (String) result.get("results");
                switch (Integer.valueOf(YaoYiYaoActivity.this.cishus).intValue()) {
                    case 0:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_0);
                        YaoYiYaoActivity.this.haoma.setText(str);
                        YaoYiYaoActivity.this.cishus2 = 0;
                        Utils.showToast("摇奖成功，请留意你的摇奖号和摇奖结果");
                        return;
                    case 1:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_1);
                        YaoYiYaoActivity.this.haoma.setText(str);
                        Utils.showToast("摇奖成功，请留意你的摇奖号和摇奖结果");
                        return;
                    case 2:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_2);
                        YaoYiYaoActivity.this.haoma.setText(str);
                        Utils.showToast("摇奖成功，请留意你的摇奖号和摇奖结果");
                        return;
                    case 3:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_3);
                        YaoYiYaoActivity.this.haoma.setText(str);
                        Utils.showToast("摇奖成功，请留意你的摇奖号和摇奖结果");
                        return;
                    case 4:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_4);
                        YaoYiYaoActivity.this.haoma.setText(str);
                        Utils.showToast("摇奖成功，请留意你的摇奖号和摇奖结果");
                        return;
                    case 5:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_5);
                        YaoYiYaoActivity.this.haoma.setText(str);
                        Utils.showToast("摇奖成功，请留意你的摇奖号和摇奖结果");
                        return;
                    default:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_0);
                        YaoYiYaoActivity.this.haoma.setText(UploadUtils.SUCCESS);
                        Utils.showToast("你已经没有摇奖次数了，明天再来吧!!!");
                        return;
                }
            }
        }
    }

    class CurrentAyncCisu extends AsyncTask<Void, Void, HashMap<String, Object>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAyncCisu() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            ArrayList<NameValuePair> list = new ArrayList<>();
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET("http://api.pymob.cn:8091/cityLife/dataExchange/myLackLotter.action?iphone=" + PerfHelper.getStringData("phone"), list);
                if (ShareApplication.debug) {
                    System.out.println("要一要:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.obj);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("draw")) {
                data.obj = jsonobj.getString("draw");
            }
            if (jsonobj.has("lack")) {
                YaoYiYaoActivity.this.cishus2 = jsonobj.getInt("lack");
            }
            return data;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (result != null && !UploadUtils.FAILURE.equals(result.get("responseCode")) && !"2".equals(result.get("responseCode")) && !"3".equals(result.get("responseCode")) && UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                String str = (String) result.get("results");
                switch (Integer.valueOf(YaoYiYaoActivity.this.cishus2).intValue()) {
                    case 0:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_0);
                        return;
                    case 1:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_1);
                        return;
                    case 2:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_2);
                        return;
                    case 3:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_3);
                        return;
                    case 4:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_4);
                        return;
                    case 5:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_5);
                        return;
                    default:
                        YaoYiYaoActivity.this.cishu.setImageResource(R.drawable.yaoyiyao_0);
                        return;
                }
            }
        }
    }
}
