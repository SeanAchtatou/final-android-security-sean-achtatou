package com.city_life.part_activiy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.city_life.part_fragment.SearchListFragment;
import com.palmtrends.baseui.BaseActivity;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;

public class SearchListActivity extends BaseActivity {
    private Fragment frag = null;
    public String id = "";
    public String keyword = "";
    /* access modifiers changed from: private */
    public SearchListFragment m_list_frag_1 = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main_part_zixun);
        this.keyword = getIntent().getStringExtra("keyword");
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchListActivity.this.finish();
            }
        });
        findViewById(R.id.title_btn_right).setVisibility(8);
        ((TextView) findViewById(R.id.title_title)).setText("搜索");
        findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String keys = ((EditText) SearchListActivity.this.findViewById(R.id.et_sendmessage)).getText().toString().trim();
                if (!keys.equals("")) {
                    if (SearchListActivity.this.m_list_frag_1 != null) {
                        SearchListActivity.this.m_list_frag_1.keyword_update(keys, keys);
                    }
                    ((InputMethodManager) SearchListActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(((EditText) SearchListActivity.this.findViewById(R.id.et_sendmessage)).getWindowToken(), 0);
                    return;
                }
                Utils.showToast("关键词不能为空");
            }
        });
        initFragment();
    }

    public void initFragment() {
        Fragment findresult = getSupportFragmentManager().findFragmentByTag("SearchListFragment");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (findresult != null) {
            this.m_list_frag_1 = (SearchListFragment) findresult;
        }
        if (this.m_list_frag_1 == null) {
            this.m_list_frag_1 = (SearchListFragment) SearchListFragment.newInstance(this.keyword, this.keyword);
        }
        if (this.frag != null) {
            fragmentTransaction.remove(this.frag);
        }
        this.frag = this.m_list_frag_1;
        fragmentTransaction.add(R.id.part_content, this.frag, "SearchListFragment");
        fragmentTransaction.commit();
    }

    public void things(View view) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
