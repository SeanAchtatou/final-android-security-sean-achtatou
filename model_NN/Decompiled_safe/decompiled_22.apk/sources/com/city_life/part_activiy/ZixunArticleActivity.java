package com.city_life.part_activiy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.city_life.ui.PicInfoActivity;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;

public class ZixunArticleActivity extends Activity {
    private View btn_back;
    private TabHost current_tabhost;
    private ImageView img_news;
    Listitem select_entity;
    private TextView txt_content;
    private TextView txt_resource;
    private TextView txt_shorNews;
    private TextView txt_time;
    private TextView txt_title;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.activity_article_zixun);
        if (getIntent().getExtras() != null) {
            this.select_entity = (Listitem) getIntent().getExtras().get("item");
        }
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_btn_right).setVisibility(8);
        ((TextView) findViewById(R.id.title_title)).setText("资讯详情");
        initView();
        this.btn_back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ZixunArticleActivity.this.finish();
            }
        });
        this.img_news.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(ZixunArticleActivity.this, PicInfoActivity.class);
                intent.putExtra("item", ZixunArticleActivity.this.select_entity);
                intent.putExtra("img_list", ZixunArticleActivity.this.select_entity.icon.split(","));
                ZixunArticleActivity.this.startActivity(intent);
            }
        });
    }

    public void initView() {
        this.btn_back = findViewById(R.id.title_back);
        this.txt_title = (TextView) findViewById(R.id.txtview_information_title_id);
        this.txt_time = (TextView) findViewById(R.id.txtview_information_time_id);
        this.txt_resource = (TextView) findViewById(R.id.txtview_information_source_id);
        this.txt_shorNews = (TextView) findViewById(R.id.txtview_information_shortNews_id);
        this.txt_content = (TextView) findViewById(R.id.txtview_information_news_id);
        this.img_news = (ImageView) findViewById(R.id.imgview_information_id);
        if (this.select_entity != null) {
            this.txt_title.setText(this.select_entity.title);
            this.txt_time.setText(this.select_entity.u_date);
            this.txt_resource.setText(this.select_entity.author);
            this.txt_shorNews.setText("暂无简讯");
            this.txt_content.setText(this.select_entity.des);
            if (this.select_entity.icon == null || this.select_entity.icon.length() <= 10) {
                this.img_news.setVisibility(8);
                return;
            }
            ShareApplication.mImageWorker.loadImage(this.select_entity.icon.split(",")[0].trim(), this.img_news);
            this.img_news.setVisibility(0);
        }
    }
}
