package com.city_life.part_activiy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.part_fragment.NearByListFragment;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class NearListActivity extends BaseFragmentActivity {
    private Fragment frag = null;
    String nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
    private String type = "";
    resume_up upone;

    public interface resume_up {
        void up();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main_part_fj);
        findViewById(R.id.title_back).setVisibility(0);
        ((TextView) findViewById(R.id.title_back)).setText(PerfHelper.getStringData(PerfHelper.P_CITY));
        findViewById(R.id.title_btn_right).setVisibility(8);
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(NearListActivity.this, CityListActivity.class);
                NearListActivity.this.startActivity(intent);
            }
        });
        ((TextView) findViewById(R.id.title_title)).setText("附近");
        this.type = "nearbypart" + PerfHelper.getStringData(PerfHelper.P_CITY_No);
        initFragment();
    }

    public void initFragment() {
        this.type = "nearbypart" + PerfHelper.getStringData(PerfHelper.P_CITY_No);
        Fragment findresult = getSupportFragmentManager().findFragmentByTag(this.type);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (this.frag != null) {
            fragmentTransaction.detach(this.frag);
        }
        if (findresult != null) {
            fragmentTransaction.attach(findresult);
            this.frag = findresult;
            fragmentTransaction.commitAllowingStateLoss();
            return;
        }
        this.frag = NearByListFragment.newInstance(this.type, this.type);
        fragmentTransaction.add(R.id.part_fj_content, this.frag, this.type);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void things(View view) {
    }

    public void setUp(resume_up ups) {
        this.upone = ups;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.title_back)).setText(PerfHelper.getStringData(PerfHelper.P_CITY));
        if (!this.nowcity.equals(PerfHelper.getStringData(PerfHelper.P_CITY))) {
            this.nowcity = PerfHelper.getStringData(PerfHelper.P_CITY);
            initFragment();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
