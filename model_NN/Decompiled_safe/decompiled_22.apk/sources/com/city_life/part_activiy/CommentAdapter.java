package com.city_life.part_activiy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;
import java.util.ArrayList;

public class CommentAdapter extends BaseAdapter {
    private Context context;
    LayoutInflater layout_inflater;
    private ArrayList<Listitem> list_entity;

    public CommentAdapter(Context context2, ArrayList<Listitem> list_entity2) {
        this.context = context2;
        this.layout_inflater = LayoutInflater.from(context2);
        this.list_entity = list_entity2;
    }

    public int getCount() {
        return this.list_entity.size();
    }

    public Object getItem(int arg0) {
        return this.list_entity.get(arg0);
    }

    public long getItemId(int arg0) {
        return (long) arg0;
    }

    static class ViewHolder {
        RatingBar comment_layout_listview_ratingbar;
        TextView commentlistview_nametext;
        TextView commentlistview_nrtextview;
        TextView commentlistview_time;

        ViewHolder() {
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder view_holder;
        if (convertView == null) {
            convertView = this.layout_inflater.inflate((int) R.layout.comment_layout_listview, (ViewGroup) null);
            view_holder = new ViewHolder();
            view_holder.comment_layout_listview_ratingbar = (RatingBar) convertView.findViewById(R.id.comment_layout_listview_ratingbar);
            view_holder.commentlistview_nametext = (TextView) convertView.findViewById(R.id.commentlistview_nametext);
            view_holder.commentlistview_nrtextview = (TextView) convertView.findViewById(R.id.commentlistview_nrtextview);
            view_holder.commentlistview_time = (TextView) convertView.findViewById(R.id.commentlistview_time);
            convertView.setTag(view_holder);
        } else {
            view_holder = (ViewHolder) convertView.getTag();
        }
        Listitem current_entity = this.list_entity.get(position);
        view_holder.comment_layout_listview_ratingbar.setRating(Float.parseFloat(current_entity.level));
        view_holder.commentlistview_nametext.setText(current_entity.title);
        view_holder.commentlistview_nrtextview.setText(current_entity.des);
        view_holder.commentlistview_time.setText(current_entity.u_date);
        return convertView;
    }
}
