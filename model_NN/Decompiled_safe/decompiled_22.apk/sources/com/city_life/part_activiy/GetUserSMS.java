package com.city_life.part_activiy;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import sina_weibo.Constants;

public class GetUserSMS extends Activity implements View.OnClickListener {
    private View btn_register;
    private View btn_return;
    private EditText code_str;
    private EditText edit_nickName;
    /* access modifiers changed from: private */
    public EditText edit_postbox;
    private EditText edit_pwd_affirm;
    /* access modifiers changed from: private */
    public int recLen = 60;
    /* access modifiers changed from: private */
    public TextView send_sms;
    int sex = 1;
    private int sex_register = 1;
    String sms_code = "";
    Timer timer = new Timer();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.user_sms_register);
        ((TextView) findViewById(R.id.tishi_text)).setText("输入手机号，获得验证码，验证成功可修改密码");
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_btn_right).setVisibility(8);
        ((TextView) findViewById(R.id.title_title)).setText("找回密码");
        initView();
    }

    private void initView() {
        this.btn_return = findViewById(R.id.title_back);
        this.btn_register = findViewById(R.id.btn_user_register_accomplish_register);
        this.edit_postbox = (EditText) findViewById(R.id.editt_user_register_postbox);
        this.send_sms = (TextView) findViewById(R.id.send_sms);
        this.code_str = (EditText) findViewById(R.id.editt_user_register_nickName);
        this.send_sms.setOnClickListener(this);
        this.btn_return.setOnClickListener(this);
        this.btn_register.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            case R.id.rl_register_choose_man /*2131231216*/:
                this.sex_register = 1;
                this.sex = 1;
                return;
            case R.id.rl_register_choose_woman /*2131231217*/:
                this.sex_register = 2;
                this.sex = 2;
                return;
            case R.id.btn_user_register_accomplish_register /*2131231220*/:
                if (this.edit_postbox.getText().toString().equals("")) {
                    Toast.makeText(this, "手机号不能为空", 0).show();
                    return;
                } else if (isNumberic(this.edit_postbox.getText().toString())) {
                    Toast.makeText(this, "手机号只能为数字", 0).show();
                    return;
                } else if (this.code_str.getText().toString().equals(this.sms_code)) {
                    Utils.showToast("验证成功，请完成注册信息");
                    Intent intent = new Intent();
                    intent.setClass(this, GetUserSMSqueren.class);
                    intent.putExtra("sms", this.edit_postbox.getText().toString().trim());
                    intent.putExtra("codext", this.code_str.getText().toString().trim());
                    startActivity(intent);
                    finish();
                    return;
                } else {
                    Utils.showToast("验证码错误，请重新输入");
                    return;
                }
            case R.id.send_sms /*2131231222*/:
                if (this.edit_postbox.getText().toString().equals("")) {
                    Toast.makeText(this, "手机号不能为空", 0).show();
                    return;
                } else if (isNumberic(this.edit_postbox.getText().toString().trim())) {
                    Toast.makeText(this, "手机号只能为数字", 0).show();
                    return;
                } else {
                    new CurrentAync().execute(null);
                    return;
                }
            default:
                return;
        }
    }

    public static boolean isNumberic(String num) {
        try {
            Double.parseDouble(num);
            return false;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    class CurrentAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        TimerTask task = new TimerTask() {
            public void run() {
                GetUserSMS.this.runOnUiThread(new Runnable() {
                    public void run() {
                        GetUserSMS access$2 = GetUserSMS.this;
                        access$2.recLen = access$2.recLen - 1;
                        GetUserSMS.this.send_sms.setText("(" + GetUserSMS.this.recLen + ")秒后再次发送");
                        GetUserSMS.this.send_sms.setBackgroundResource(R.drawable.send_sms_huise);
                        GetUserSMS.this.send_sms.setClickable(false);
                        if (GetUserSMS.this.recLen < 0) {
                            GetUserSMS.this.send_sms.setBackgroundResource(R.drawable.send_sms_btn);
                            GetUserSMS.this.send_sms.setText("获取验证码");
                            GetUserSMS.this.send_sms.setClickable(true);
                        }
                    }
                });
            }
        };

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            ArrayList<NameValuePair> list = new ArrayList<>();
            list.add(new BasicNameValuePair("type", "pengyou"));
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(String.valueOf(GetUserSMS.this.getResources().getString(R.string.citylife_getuser_sms_url)) + GetUserSMS.this.edit_postbox.getText().toString(), list);
                if (ShareApplication.debug) {
                    System.out.println("短信注册返回:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.obj);
                GetUserSMS.this.sms_code = String.valueOf(date.obj);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has(Constants.SINA_CODE)) {
                data.obj = jsonobj.getString(Constants.SINA_CODE);
            }
            return data;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (result == null || UploadUtils.FAILURE.equals(result.get("responseCode"))) {
                Toast.makeText(GetUserSMS.this, "用户名已存在", 0).show();
            } else if ("2".equals(result.get("responseCode"))) {
                Toast.makeText(GetUserSMS.this, "验证码发送失败，请稍后再试", 0).show();
            } else if ("3".equals(result.get("responseCode"))) {
                Toast.makeText(GetUserSMS.this, "验证码发送失败，请稍后再试", 0).show();
            } else if (UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                Toast.makeText(GetUserSMS.this, "验证码成功，请注意查收", 0).show();
                GetUserSMS.this.timer.schedule(this.task, 1000, 1000);
            }
        }
    }
}
