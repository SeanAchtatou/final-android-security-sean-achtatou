package com.city_life.part_activiy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.city_life.part_asynctask.GetFavListId;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class UserLogin extends Activity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public EditText edit_userName;
    /* access modifiers changed from: private */
    public EditText edit_userPwd;
    private AlertDialog mAlertDialog = null;
    private TextView mButtonLogin;
    private TextView mButtonRegister;
    private TextView mButtonReturn;
    private TextView mimawangji;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.user_login);
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_btn_right).setVisibility(0);
        ((TextView) findViewById(R.id.title_title)).setText("用户登录");
        ((TextView) findViewById(R.id.title_btn_right)).setVisibility(8);
        initView();
    }

    private void initView() {
        this.mButtonReturn = (TextView) findViewById(R.id.title_back);
        this.mButtonRegister = (TextView) findViewById(R.id.title_btn_right);
        this.mButtonLogin = (TextView) findViewById(R.id.btn_login_login);
        this.edit_userName = (EditText) findViewById(R.id.editt_user_login_userName);
        this.edit_userPwd = (EditText) findViewById(R.id.editt_user_login_userPwd);
        this.mimawangji = (TextView) findViewById(R.id.btn_mimawangji);
        this.mimawangji.setText(Html.fromHtml("<u>忘记密码</u>"));
        this.mimawangji.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(UserLogin.this, GetUserSMS.class);
                UserLogin.this.startActivity(intent);
            }
        });
        this.mButtonReturn.setOnClickListener(this);
        this.mButtonRegister.setOnClickListener(this);
        this.mButtonLogin.setOnClickListener(this);
        findViewById(R.id.btn_qq_login).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(UserLogin.this, QQLoginActivity.class);
                UserLogin.this.startActivity(intent);
                UserLogin.this.finish();
            }
        });
    }

    public void onClick(View v) {
        new Intent();
        switch (v.getId()) {
            case R.id.title_back /*2131230802*/:
                finish();
                return;
            case R.id.title_btn_right /*2131231092*/:
            default:
                return;
            case R.id.btn_login_login /*2131231212*/:
                if (this.edit_userName.getText().toString().equals("") || this.edit_userPwd.getText().toString().equals("")) {
                    Utils.showToast("账号和密码不能为空");
                    return;
                }
                Utils.showProcessDialog(this, "正在登录...");
                new Thread() {
                    public void run() {
                        new CurrentAync().execute(null);
                    }
                }.start();
                return;
        }
    }

    class CurrentAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair(Constants.SINA_USER_NAME, UserLogin.this.edit_userName.getText().toString()));
            param.add(new BasicNameValuePair("userPwd", UserLogin.this.edit_userPwd.getText().toString()));
            param.add(new BasicNameValuePair("type", "pengyou"));
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(UserLogin.this.getResources().getString(R.string.citylife_loginUserInfo_url), param);
                if (ShareApplication.debug) {
                    System.out.println("用户登录返回:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.list);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("countNum")) {
                data.obj = jsonobj.getString("countNum");
            }
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has("conent")) {
                        o.des = obj.getString("conent");
                    }
                    if (obj.has("age")) {
                        o.level = obj.getString("age");
                    }
                    if (obj.has("nickName")) {
                        o.title = obj.getString("nickName");
                    }
                    if (obj.has("sex")) {
                        o.other = obj.getString("sex");
                    }
                    if (obj.has("imgUrl")) {
                        o.icon = obj.getString("imgUrl");
                    }
                    if (obj.has("email")) {
                        o.other1 = obj.getString("email");
                    }
                } catch (Exception e) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          com.utils.PerfHelper.setInfo(java.lang.String, int):void
          com.utils.PerfHelper.setInfo(java.lang.String, long):void
          com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
          com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            Utils.dismissProcessDialog();
            if (!Utils.isNetworkAvailable(UserLogin.this)) {
                Utils.showToast((int) R.string.network_error);
                UserLogin.this.finish();
            } else if (result == null) {
                Toast.makeText(UserLogin.this, "请求失败，请稍后再试", 0).show();
            } else if (UploadUtils.FAILURE.equals(result.get("responseCode"))) {
                Toast.makeText(UserLogin.this, "登录失败，请输入正确的用户名和密码", 0).show();
            } else if (UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                ArrayList<Listitem> user_listArrayList = (ArrayList) result.get("results");
                PerfHelper.setInfo(PerfHelper.P_USERID, ((Listitem) user_listArrayList.get(0)).nid);
                PerfHelper.setInfo("phone", ((Listitem) user_listArrayList.get(0)).other1);
                PerfHelper.setInfo(PerfHelper.P_SHARE_AGE, ((Listitem) user_listArrayList.get(0)).level);
                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME, ((Listitem) user_listArrayList.get(0)).title);
                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE, ((Listitem) user_listArrayList.get(0)).icon);
                PerfHelper.setInfo(PerfHelper.P_USER_LOGIN, true);
                new GetFavListId().execute(null);
                UserLogin.this.finish();
            }
        }
    }
}
