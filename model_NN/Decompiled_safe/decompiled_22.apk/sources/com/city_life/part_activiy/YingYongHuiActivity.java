package com.city_life.part_activiy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import com.city_life.artivleactivity.BaseFragmentActivity;
import com.city_life.part_asynctask.UploadUtils;
import com.city_life.part_fragment.YingYongHuiListFragment;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;

public class YingYongHuiActivity extends BaseFragmentActivity {
    /* access modifiers changed from: private */
    public Fragment frag = null;
    public String id = "";
    private Fragment m_list_frag_1 = null;
    public String name = "";
    private TextView o_text;
    private TextView t_text;
    private Listitem z_item = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main_part_yingyonghui);
        this.id = getIntent().getStringExtra(LocaleUtil.INDONESIAN);
        this.name = getIntent().getStringExtra(Constants.PARAM_TITLE);
        this.o_text = (TextView) findViewById(R.id.title_remen);
        this.t_text = (TextView) findViewById(R.id.title_jingping);
        this.o_text.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                YingYongHuiActivity.this.setOnclick(v);
                YingYongHuiActivity.this.initFragment(v);
            }
        });
        this.t_text.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                YingYongHuiActivity.this.setOnclick(v);
                YingYongHuiActivity.this.initFragment(v);
            }
        });
        initFragment(findViewById(R.id.title_remen));
    }

    public void setOnclick(View v) {
        switch (v.getId()) {
            case R.id.title_remen /*2131230851*/:
                this.o_text.setTextColor(getResources().getColor(R.color.white));
                this.t_text.setTextColor(getResources().getColor(R.color.gread));
                this.o_text.setBackgroundResource(R.drawable.renmen_btn_n);
                this.t_text.setBackgroundResource(R.drawable.jingping_btn_h);
                return;
            case R.id.title_jingping /*2131230852*/:
                this.o_text.setTextColor(getResources().getColor(R.color.gread));
                this.t_text.setTextColor(getResources().getColor(R.color.white));
                this.o_text.setBackgroundResource(R.drawable.renmen_btn_h);
                this.t_text.setBackgroundResource(R.drawable.jingping_btn_n);
                return;
            default:
                return;
        }
    }

    public void initFragment(final View view) {
        Utils.h.postDelayed(new Runnable() {
            public void run() {
                FragmentTransaction fragmentTransaction = YingYongHuiActivity.this.getSupportFragmentManager().beginTransaction();
                Fragment findresult = YingYongHuiActivity.this.getSupportFragmentManager().findFragmentByTag(new StringBuilder(String.valueOf(view.getId())).toString());
                switch (view.getId()) {
                    case R.id.title_remen /*2131230851*/:
                        if (YingYongHuiActivity.this.frag != null) {
                            fragmentTransaction.detach(YingYongHuiActivity.this.frag);
                        }
                        if (findresult == null) {
                            YingYongHuiActivity.this.frag = YingYongHuiListFragment.newInstance(UploadUtils.FAILURE, UploadUtils.FAILURE);
                            break;
                        } else {
                            fragmentTransaction.attach(findresult);
                            YingYongHuiActivity.this.frag = findresult;
                            fragmentTransaction.commitAllowingStateLoss();
                            return;
                        }
                    case R.id.title_jingping /*2131230852*/:
                        if (YingYongHuiActivity.this.frag != null) {
                            fragmentTransaction.detach(YingYongHuiActivity.this.frag);
                        }
                        if (findresult == null) {
                            YingYongHuiActivity.this.frag = YingYongHuiListFragment.newInstance("2", "2");
                            break;
                        } else {
                            fragmentTransaction.attach(findresult);
                            YingYongHuiActivity.this.frag = findresult;
                            fragmentTransaction.commitAllowingStateLoss();
                            return;
                        }
                }
                fragmentTransaction.add(R.id.parts_content, YingYongHuiActivity.this.frag, new StringBuilder(String.valueOf(view.getId())).toString());
                fragmentTransaction.commitAllowingStateLoss();
            }
        }, 200);
    }
}
