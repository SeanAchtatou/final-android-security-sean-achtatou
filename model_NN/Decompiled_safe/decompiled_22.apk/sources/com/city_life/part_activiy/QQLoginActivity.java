package com.city_life.part_activiy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.city_life.part_asynctask.UploadUtils;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.contact.RContact;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.open.HttpStatusException;
import com.tencent.open.NetworkUnavailableException;
import com.tencent.tauth.Constants;
import com.tencent.tauth.IRequestListener;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.utils.PerfHelper;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.NameValuePair;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class QQLoginActivity extends Activity {
    String email_str;
    String headIco;
    boolean isfirst = true;
    Tencent mTencent;
    TextView mTextView;
    Handler mhandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    new CurrentAync().execute(null);
                    return;
                default:
                    return;
            }
        }
    };
    String my_appid = "222222";
    String nickname;
    String openid;
    String password;
    String scope = "all";
    String sex_str;
    String username;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.qq_user_binding);
        Context ctxContext = getApplicationContext();
        findViewById(R.id.title_back).setVisibility(0);
        findViewById(R.id.title_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                QQLoginActivity.this.finish();
            }
        });
        findViewById(R.id.title_btn_right).setVisibility(8);
        ((TextView) findViewById(R.id.title_title)).setText("QQ账号登陆");
        this.mTencent = Tencent.createInstance(this.my_appid, ctxContext);
        this.mTextView = (TextView) findViewById(R.id.qq_logo_text);
        qq_login();
    }

    public void qq_login() {
        if (!this.mTencent.isSessionValid()) {
            Toast.makeText(this, "正在跳转qq登录界面，请稍后...", 0).show();
            this.mTencent.login(this, this.scope, new My_tencent_UiListener());
            return;
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    class My_tencent_UiListener implements IUiListener {
        My_tencent_UiListener() {
        }

        public void onCancel() {
            Toast.makeText(QQLoginActivity.this, "您取消了QQ登录", 0).show();
            QQLoginActivity.this.finish();
        }

        public void onComplete(JSONObject arg0) {
            Toast.makeText(QQLoginActivity.this, "登录QQ成功", 0).show();
            new Handler().post(new Runnable() {
                public void run() {
                    QQLoginActivity.this.mTextView.setText("QQ登陆成功，正在连接本地服务器");
                }
            });
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("<<<<<-----------onComplete------------>>>>>\t\t\n");
            stringBuffer.append("下面是客户端调用用户信息需要的三要素：\t\t\n");
            try {
                stringBuffer.append("access_token：" + arg0.getString("access_token") + "\t\t\n");
                stringBuffer.append("openid：" + arg0.getString("openid") + "\t\t\n");
                stringBuffer.append("expires_in：" + arg0.getString("expires_in") + "\t\t\n");
                QQLoginActivity.this.openid = arg0.getString("openid");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            QQLoginActivity.this.mTencent.requestAsync(Constants.GRAPH_SIMPLE_USER_INFO, null, "GET", new MyIRequestListener(), null);
        }

        public void onError(UiError arg0) {
            Toast.makeText(QQLoginActivity.this, "请求失败，请稍后再试", 0).show();
        }
    }

    class MyIRequestListener implements IRequestListener {
        private Boolean mNeedReAuth = false;

        public MyIRequestListener() {
        }

        public void onComplete(JSONObject arg0, Object obj0) {
            if (!doComplete(arg0, obj0)) {
                try {
                    String qq_name = arg0.getString(RContact.COL_NICKNAME);
                    String string = arg0.getString("figureurl");
                    String string2 = arg0.getString("figureurl_1");
                    String string3 = arg0.getString("figureurl_2");
                    String qq_sex = arg0.getString("gender");
                    String string4 = arg0.getString("figureurl_qq_1");
                    String qq_q_img_max = arg0.getString("figureurl_qq_2");
                    QQLoginActivity.this.sex_str = qq_sex.equals("男") ? UploadUtils.FAILURE : "2";
                    QQLoginActivity.this.headIco = qq_q_img_max;
                    QQLoginActivity.this.email_str = String.valueOf(QQLoginActivity.this.openid) + "@pengyou.com";
                    QQLoginActivity.this.username = qq_name;
                    QQLoginActivity.this.nickname = qq_name;
                    QQLoginActivity.this.password = "123456";
                    QQLoginActivity.this.mhandler.obtainMessage(0).sendToTarget();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        /* access modifiers changed from: protected */
        public boolean doComplete(JSONObject response, Object state) {
            try {
                if (response.getInt("ret") != 100030) {
                    return false;
                }
                if (this.mNeedReAuth.booleanValue()) {
                    QQLoginActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            QQLoginActivity.this.mTencent.reAuth(QQLoginActivity.this, Constants.GRAPH_SIMPLE_USER_INFO, new My_tencent_UiListener());
                        }
                    });
                }
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("toddtest", response.toString());
                return false;
            }
        }

        public void onConnectTimeoutException(ConnectTimeoutException arg0, Object arg1) {
        }

        public void onHttpStatusException(HttpStatusException arg0, Object arg1) {
        }

        public void onIOException(IOException arg0, Object arg1) {
        }

        public void onJSONException(JSONException arg0, Object arg1) {
        }

        public void onMalformedURLException(MalformedURLException arg0, Object arg1) {
        }

        public void onNetworkUnavailableException(NetworkUnavailableException arg0, Object arg1) {
        }

        public void onSocketTimeoutException(SocketTimeoutException arg0, Object arg1) {
        }

        public void onUnknowException(Exception arg0, Object arg1) {
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mTencent.onActivityResult(requestCode, resultCode, data);
    }

    class CurrentAync extends AsyncTask<Void, Void, HashMap<String, Object>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((HashMap<String, Object>) ((HashMap) obj));
        }

        public CurrentAync() {
        }

        /* access modifiers changed from: protected */
        public HashMap<String, Object> doInBackground(Void... params) {
            ArrayList<NameValuePair> list = new ArrayList<>();
            BasicNameValuePair mPair01 = new BasicNameValuePair(sina_weibo.Constants.SINA_USER_NAME, QQLoginActivity.this.nickname);
            BasicNameValuePair mPair02 = new BasicNameValuePair("userPwd", QQLoginActivity.this.password);
            BasicNameValuePair mPair03 = new BasicNameValuePair("sex", QQLoginActivity.this.sex_str);
            BasicNameValuePair mPair04 = new BasicNameValuePair("email", QQLoginActivity.this.email_str);
            BasicNameValuePair mPair05 = new BasicNameValuePair("headIco", QQLoginActivity.this.headIco);
            BasicNameValuePair mPair06 = new BasicNameValuePair("type", "qq");
            list.add(mPair01);
            list.add(mPair02);
            list.add(mPair03);
            list.add(mPair04);
            list.add(mPair05);
            list.add(mPair06);
            HashMap<String, Object> mhashmap = new HashMap<>();
            try {
                String json = DNDataSource.list_FromNET(QQLoginActivity.this.getResources().getString(R.string.citylife_registerUserInfo_url), list);
                if (ShareApplication.debug) {
                    System.out.println("QQ绑定注册返回:" + json);
                }
                Data date = parseJson(json);
                mhashmap.put("responseCode", date.obj1);
                mhashmap.put("results", date.list);
                return mhashmap;
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public Data parseJson(String json) throws Exception {
            Data data = new Data();
            JSONObject jsonobj = new JSONObject(json);
            if (jsonobj.has("responseCode")) {
                if (jsonobj.getInt("responseCode") != 0) {
                    data.obj1 = jsonobj.getString("responseCode");
                    return data;
                }
                data.obj1 = jsonobj.getString("responseCode");
            }
            if (jsonobj.has("countNum")) {
                data.obj = jsonobj.getString("countNum");
            }
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has("conent")) {
                        o.des = obj.getString("conent");
                    }
                    if (obj.has("age")) {
                        o.level = obj.getString("age");
                    }
                    if (obj.has("nickName")) {
                        o.title = obj.getString("nickName");
                    }
                    if (obj.has("sex")) {
                        o.other = obj.getString("sex");
                    }
                    if (obj.has("imgUrl")) {
                        o.icon = obj.getString("imgUrl");
                    }
                } catch (Exception e) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          com.utils.PerfHelper.setInfo(java.lang.String, int):void
          com.utils.PerfHelper.setInfo(java.lang.String, long):void
          com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
          com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
        /* access modifiers changed from: protected */
        public void onPostExecute(HashMap<String, Object> result) {
            super.onPostExecute((Object) result);
            if (result == null || UploadUtils.FAILURE.equals(result.get("responseCode"))) {
                Toast.makeText(QQLoginActivity.this, "登录失败，请稍后再试", 0).show();
                QQLoginActivity.this.finish();
            } else if ("2".equals(result.get("responseCode"))) {
                Toast.makeText(QQLoginActivity.this, "登录失败，请稍后再试", 0).show();
                QQLoginActivity.this.finish();
            } else if ("3".equals(result.get("responseCode"))) {
                Toast.makeText(QQLoginActivity.this, "登录失败，请稍后再试", 0).show();
                QQLoginActivity.this.finish();
            } else if (UploadUtils.SUCCESS.equals(result.get("responseCode"))) {
                ArrayList<Listitem> user_listArrayList = (ArrayList) result.get("results");
                PerfHelper.setInfo(PerfHelper.P_USERID, ((Listitem) user_listArrayList.get(0)).nid);
                PerfHelper.setInfo(PerfHelper.P_SHARE_AGE, ((Listitem) user_listArrayList.get(0)).level);
                PerfHelper.setInfo(PerfHelper.P_SHARE_NAME, ((Listitem) user_listArrayList.get(0)).title);
                PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE, ((Listitem) user_listArrayList.get(0)).icon);
                if (((Listitem) user_listArrayList.get(0)).other.equals(UploadUtils.FAILURE)) {
                    PerfHelper.setInfo(PerfHelper.P_SHARE_SEX, "男");
                } else {
                    PerfHelper.setInfo(PerfHelper.P_SHARE_SEX, "女");
                }
                PerfHelper.setInfo(PerfHelper.P_USER_LOGIN, true);
                QQLoginActivity.this.startActivity(new Intent(QQLoginActivity.this, UserCenter.class));
                Utils.showToast("登录成功，跳转到个人中心");
                QQLoginActivity.this.finish();
            }
        }
    }
}
