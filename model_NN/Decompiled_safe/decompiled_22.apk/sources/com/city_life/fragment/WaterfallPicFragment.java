package com.city_life.fragment;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.city_life.view.OptionsContent;
import com.palmtrends.ad.AdAdapter;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.io.Serializable;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class WaterfallPicFragment extends BaseFragment<Listitem> implements OptionsContent.OptionsListener {
    ColorStateList csl_n;
    OptionsContent elasticScrollView;
    GestureDetector gd = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
        public boolean onSingleTapUp(MotionEvent e) {
            Listitem item = (Listitem) WaterfallPicFragment.this.view.getTag();
            if ("true".equals(item.isad)) {
                ClientShowAd.dealClick(WaterfallPicFragment.this.mContext, item.other);
            } else {
                Intent intent = new Intent();
                intent.setAction(WaterfallPicFragment.this.mContext.getResources().getString(R.string.activity_anim_article));
                intent.putExtra("item", item);
                intent.putExtra("type", "2");
                intent.putExtra("items", (Serializable) WaterfallPicFragment.this.mData.list);
                intent.putExtra("position", WaterfallPicFragment.this.mData.list.indexOf(item));
                WaterfallPicFragment.this.startActivity(intent);
            }
            return true;
        }
    });
    public FrameLayout.LayoutParams itemparam;
    public LinearLayout.LayoutParams itemparam1;
    LinearLayout left;
    public LinearLayout mContainers;
    View mLoading;
    public RelativeLayout mMain_layout;
    View.OnClickListener oncilc = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction(WaterfallPicFragment.this.mContext.getResources().getString(R.string.activity_keyword));
            intent.putExtra("parttpye", v.getTag().toString());
            WaterfallPicFragment.this.startActivity(intent);
        }
    };
    View.OnTouchListener ont = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            WaterfallPicFragment.this.view = v;
            WaterfallPicFragment.this.gd.onTouchEvent(event);
            return true;
        }
    };
    int[] p_count = {R.id.listitem_tag, R.id.listitem_tag_1, R.id.listitem_tag_2};
    LinearLayout reight;
    TextView[] tags = new TextView[5];
    View view;

    public static WaterfallPicFragment newInstance(String type, String partType) {
        WaterfallPicFragment tf = new WaterfallPicFragment();
        tf.initType(type, partType);
        return tf;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup vgroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, vgroup, savedInstanceState);
        this.mContext = inflater.getContext();
        if (this.mMain_layout == null) {
            this.mContainers = new LinearLayout(this.mContext);
            this.mMain_layout = new RelativeLayout(this.mContext);
            this.elasticScrollView = new OptionsContent(this.mContext);
            this.elasticScrollView.mListener = this;
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(-1, -1);
            this.mMain_layout.addView(this.elasticScrollView.getScrollView(), lp);
            this.mLoading = inflater.inflate((int) R.layout.loading_layout, (ViewGroup) null);
            this.mMain_layout.addView(this.mLoading, lp);
            initListFragment(inflater);
            this.mContainers.addView(this.mMain_layout);
        } else {
            this.mContainers.removeAllViews();
            this.mContainers = new LinearLayout(getActivity());
            this.mContainers.addView(this.mMain_layout);
        }
        this.mLength = 16;
        return this.mContainers;
    }

    public void initListFragment(LayoutInflater inflater) {
        this.left = new LinearLayout(this.mContext);
        this.reight = new LinearLayout(this.mContext);
        LinearLayout content = this.elasticScrollView.getContentView();
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, -2);
        this.left.setOrientation(1);
        this.reight.setOrientation(1);
        this.left.setGravity(1);
        this.reight.setGravity(1);
        lp.weight = 1.0f;
        int h = (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 194) / 640;
        int w = (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 280) / 640;
        this.itemparam = new FrameLayout.LayoutParams(w, h);
        this.itemparam1 = new LinearLayout.LayoutParams(w, -2);
        content.addView(this.left, lp);
        content.addView(this.reight, lp);
        addListener();
        initData();
        this.csl_n = getResources().getColorStateList(R.color.list_item_title_n);
    }

    public void update() {
        if (this.mData != null && this.mData.list.size() > 0) {
            if (this.mPage == 0) {
                this.left.removeAllViews();
                this.reight.removeAllViews();
            }
            int count = this.mData.list.size();
            for (int i = 0; i < count; i++) {
                if (i % 2 == 0) {
                    this.left.addView(getListItemview((View) null, (Listitem) this.mData.list.get(i), i));
                } else {
                    this.reight.addView(getListItemview((View) null, (Listitem) this.mData.list.get(i), i));
                }
            }
            if (this.mData.list.size() <= this.mLength - 1) {
                this.elasticScrollView.removeFooter();
            }
        }
    }

    public View getListHeadview(Listitem item) {
        return null;
    }

    public View getListItemview(View view2, Listitem item, int position) {
        View viewitem = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_video, (ViewGroup) null);
        ImageView icon = (ImageView) viewitem.findViewById(R.id.listitem_icon);
        TextView des = (TextView) viewitem.findViewById(R.id.listitem_des);
        TextView date = (TextView) viewitem.findViewById(R.id.listitem_date);
        icon.setLayoutParams(this.itemparam);
        ((TextView) viewitem.findViewById(R.id.listitem_title)).setText(item.title);
        String[] str = item.other2.split(",");
        int count = str.length;
        for (int i = 0; i < count; i++) {
            this.tags[i] = (TextView) viewitem.findViewById(this.p_count[i]);
            this.tags[i].setVisibility(0);
            this.tags[i].setText(str[i]);
            this.tags[i].setTag(str[i]);
            this.tags[i].setOnClickListener(this.oncilc);
            this.tags[i].setTextColor(this.csl_n);
        }
        date.setText(item.u_date.split(" ")[0]);
        des.setText(item.des);
        if (item.des.equals("")) {
            des.setVisibility(8);
        } else {
            des.setVisibility(0);
        }
        ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + item.icon, icon);
        viewitem.setOnTouchListener(this.ont);
        viewitem.setTag(item);
        viewitem.setLayoutParams(this.itemparam1);
        return viewitem;
    }

    public void findView() {
    }

    public void addListener() {
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                WaterfallPicFragment.this.mLoading.setVisibility(8);
                switch (msg.what) {
                    case FinalVariable.update /*1001*/:
                        WaterfallPicFragment.this.update();
                        return;
                    case FinalVariable.remove_footer /*1002*/:
                        WaterfallPicFragment.this.elasticScrollView.removeFooter();
                        return;
                    case FinalVariable.change /*1003*/:
                    case FinalVariable.deletefoot /*1005*/:
                    case FinalVariable.first_load /*1008*/:
                    case FinalVariable.load_image /*1009*/:
                    case FinalVariable.other /*1010*/:
                    default:
                        return;
                    case FinalVariable.error /*1004*/:
                        if (!Utils.isNetworkAvailable(WaterfallPicFragment.this.mContext)) {
                            Utils.showToast((int) R.string.network_error);
                            return;
                        } else if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            return;
                        } else {
                            Utils.showToast((int) R.string.network_error);
                            return;
                        }
                    case FinalVariable.addfoot /*1006*/:
                        WaterfallPicFragment.this.elasticScrollView.addFooter();
                        return;
                    case FinalVariable.nomore /*1007*/:
                        WaterfallPicFragment.this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                        if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            return;
                        } else {
                            Utils.showToast("没有更多数据了");
                            return;
                        }
                    case FinalVariable.first_update /*1011*/:
                        if (!(WaterfallPicFragment.this.mlistAdapter == null || WaterfallPicFragment.this.mlistAdapter.datas == null)) {
                            WaterfallPicFragment.this.mlistAdapter.datas.clear();
                            WaterfallPicFragment.this.mlistAdapter = null;
                        }
                        WaterfallPicFragment.this.update();
                        WaterfallPicFragment.this.fillAd(WaterfallPicFragment.this.mlistAdapter);
                        return;
                }
            }
        };
    }

    private void initfooter() {
    }

    public void fillAd(AdAdapter ad) {
        if (this.mPage != 0 || !this.isShowAD || this.mParttype.startsWith(DBHelper.FAV_FLAG) || this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
        }
    }

    public void loadmore() {
        if (!this.isloading) {
            this.isloading = true;
            super.loadMore();
        }
    }

    public void reflashing() {
        if (!this.isloading) {
            reFlush();
        }
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count);
        }
        String json = DNDataSource.list_FromNET(String.valueOf(Urls.app_api) + "?action=listsp", oldtype, page, count, parttype, isfirst);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public Data parseJson(String strs) throws Exception {
        System.out.println(strs);
        Data d = new Data();
        JSONObject jsonobj = new JSONObject(strs);
        if (!jsonobj.has(Constants.SINA_CODE) || jsonobj.getInt(Constants.SINA_CODE) != 0) {
            JSONArray list = jsonobj.getJSONArray("list");
            int count = list.length();
            for (int i = 0; i < count; i++) {
                JSONObject obj = list.getJSONObject(i);
                Listitem item = new Listitem();
                item.icon = obj.getString("icon");
                item.des = obj.getString("des");
                item.title = obj.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                item.author = obj.getString("author");
                item.other = obj.getString("quote");
                item.other1 = obj.getString("thumb2");
                item.other2 = obj.getString("keyword");
                item.nid = obj.getString(LocaleUtil.INDONESIAN);
                item.sa = this.mOldtype;
                item.u_date = obj.getString("adddate");
                item.getMark();
                d.list.add(item);
            }
            return d;
        }
        throw new DataException(jsonobj.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG));
    }
}
