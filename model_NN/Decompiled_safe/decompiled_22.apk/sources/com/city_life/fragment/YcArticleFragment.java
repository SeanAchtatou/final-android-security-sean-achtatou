package com.city_life.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.AraticleFragment;
import com.palmtrends.baseui.BaseArticleActivity;
import com.palmtrends.baseview.ImageDetailViewPager;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.Util;
import java.io.Serializable;

public class YcArticleFragment extends AraticleFragment {
    private static final String IMAGE_DATA_EXTRA = "resId";

    public YcArticleFragment() {
    }

    public YcArticleFragment(ImageDetailViewPager pager, FragmentActivity activity) {
        mPager = pager;
        mActivity = activity;
        setHasOptionsMenu(true);
    }

    public static AraticleFragment newInstance(int imageNum, ImageDetailViewPager pager, FragmentActivity activity) {
        YcArticleFragment f = new YcArticleFragment(pager, activity);
        Bundle args = new Bundle();
        args.putInt(IMAGE_DATA_EXTRA, imageNum);
        f.setArguments(args);
        return f;
    }

    public void onPageFinish() {
        if (BaseArticleActivity.o_items.get(new StringBuilder(String.valueOf(this.mArticleNum)).toString()) != null && this.isotherArticleback) {
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    YcArticleFragment.this.mWebView.pageDown(true);
                }
            }, 300);
        }
    }

    public boolean onClickLink(String url) {
        System.out.println("---------------" + url);
        if (!url.endsWith(Util.PHOTO_DEFAULT_EXT) && !url.endsWith(".png")) {
            return false;
        }
        Intent intent = new Intent();
        intent.setAction(this.mContext.getResources().getString(R.string.activity_pic));
        intent.putExtra("item", this.item);
        intent.putExtra("items", (Serializable) ShareApplication.items);
        startActivity(intent);
        return true;
    }
}
