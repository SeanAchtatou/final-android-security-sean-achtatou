package com.city_life.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.city_life.ui.PicInfoActivity;
import com.palmtrends.baseview.ImageDetailViewPager;
import com.palmtrends.loadimage.ImageWorker;
import com.palmtrends.loadimage.Utils;
import com.palmtrends.zoom.GestureImageView;
import com.pengyou.citycommercialarea.R;

public class PicDetailFragment extends Fragment {
    private static final String IMAGE_DATA_EXTRA = "resId";
    private PicInfoActivity mActivity;
    private String mImageNum;
    private GestureImageView mImageView;
    private ImageWorker mImageWorker;
    private ImageDetailViewPager mPager;

    public static PicDetailFragment newInstance(String number, ImageDetailViewPager pager, PicInfoActivity activity) {
        PicDetailFragment f = new PicDetailFragment(pager, activity);
        Bundle args = new Bundle();
        args.putString(IMAGE_DATA_EXTRA, number);
        f.setArguments(args);
        return f;
    }

    public PicDetailFragment(ImageDetailViewPager pager, PicInfoActivity activity) {
        this.mPager = pager;
        this.mActivity = activity;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mImageNum = getArguments() != null ? getArguments().getString(IMAGE_DATA_EXTRA) : "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate((int) R.layout.draw_detail_fragment, container, false);
        this.mImageView = (GestureImageView) v.findViewById(R.id.imageView);
        this.mImageView.setViewPager(this.mPager);
        this.mImageView.setOnPageChangeCallback(this.mActivity);
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (PicInfoActivity.class.isInstance(getActivity())) {
            PicInfoActivity picInfoActivity = (PicInfoActivity) getActivity();
            this.mImageWorker = PicInfoActivity.mImageWorker;
            this.mImageWorker.loadImage(this.mImageNum, this.mImageView);
        }
        if (View.OnClickListener.class.isInstance(getActivity()) && Utils.hasActionBar()) {
            this.mImageView.setOnClickListener((View.OnClickListener) getActivity());
        }
    }

    public void cancelWork() {
        ImageWorker.cancelWork(this.mImageView);
        this.mImageView.setImageDrawable(null);
        this.mImageView = null;
    }
}
