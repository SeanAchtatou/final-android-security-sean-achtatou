package com.city_life.fragment;

import com.palmtrends.entity.Listitem;
import com.palmtrends.entity.Pictorial;
import java.util.ArrayList;
import java.util.List;

public class PictorialOperate {
    public static List<Pictorial> packing(List<Listitem> data, int lenght) {
        boolean z;
        List<Pictorial> drawList = new ArrayList<>();
        int len = data.size();
        int count = len / lenght;
        int i = 0;
        while (i < count) {
            Pictorial p = new Pictorial();
            p.piclist = data.subList(lenght * i, (i + 1) * lenght);
            drawList.add(p);
            i++;
        }
        if (len % lenght == 0) {
            z = false;
        } else {
            z = true;
        }
        if (z) {
            Pictorial p2 = new Pictorial();
            p2.piclist = data.subList(lenght * i, len);
            drawList.add(p2);
        }
        return drawList;
    }

    public static List<Listitem> unboxing(List<Pictorial> data) {
        int len = data.size();
        List<Listitem> allListitems = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            allListitems.addAll(data.get(i).piclist);
        }
        return allListitems;
    }
}
