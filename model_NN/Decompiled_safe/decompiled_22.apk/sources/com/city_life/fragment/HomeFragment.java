package com.city_life.fragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.city_life.part_asynctask.UploadUtils;
import com.city_life.ui.HomeActivity;
import com.palmtrends.ad.AdAdapter;
import com.palmtrends.ad.ClientShowAd;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.BaseHomeFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class HomeFragment extends BaseHomeFragment {
    Animation ani_out = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.5f, 1, 0.0f);
    public LinearLayout.LayoutParams llp;

    public static HomeFragment newInstance(String type, String parttype) {
        HomeFragment tf = new HomeFragment();
        tf.initType(type, "home");
        return tf;
    }

    public boolean dealClick(Listitem item, int position) {
        if ("true".equals(item.isad)) {
            ClientShowAd.dealClick(this.mContext, item.other);
        } else {
            Intent intent = new Intent();
            intent.setAction(this.mContext.getResources().getString(R.string.activity_article));
            intent.putExtra("item", item);
            intent.putExtra("items", (Serializable) this.mlistAdapter.datas);
            intent.putExtra("position", position - this.mListview.getHeaderViewsCount());
            startActivity(intent);
        }
        return true;
    }

    public void fillAd(AdAdapter ad) {
        if (this.mPage != 0 || !this.isShowAD || this.isasload) {
        }
    }

    public void findView() {
        this.llp = new LinearLayout.LayoutParams(-1, ((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 334) - HomeActivity.dip2px(this.mContext, 20.0f)) / 592);
        super.findView();
        this.mListview.setDivider(null);
        this.mListview.setDividerHeight((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 20) / 480);
        this.ani_out.setDuration(500);
        this.mLength = 20;
        this.mFooter_limit = 20;
    }

    public class Hold {
        TextView date;
        TextView des;
        ImageView icon;
        TextView title;

        public Hold() {
        }
    }

    public View getListItemview(View view, Listitem item, int position) {
        Hold hold;
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_home, (ViewGroup) null);
        }
        if (view.getTag() == null) {
            hold = new Hold();
        } else {
            hold = (Hold) view.getTag();
        }
        hold.date.setText("更新时间：" + item.u_date);
        hold.des.setText(item.des);
        hold.title.setText(item.title);
        hold.icon.setLayoutParams(this.llp);
        if ("true".equals(item.isad)) {
            ShareApplication.mImageWorker.loadImage(String.valueOf(ClientShowAd.ad_main) + item.icon, hold.icon);
        } else {
            ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + item.icon, hold.icon);
        }
        if ("".equals(item.other1)) {
            item.other1 = UploadUtils.FAILURE;
            view.startAnimation(this.ani_out);
        }
        return view;
    }

    public class HandlerRun implements Runnable {
        View view;

        public HandlerRun(View v) {
            this.view = v;
        }

        public void run() {
            this.view.startAnimation(HomeFragment.this.ani_out);
        }
    }

    public Data getDataFromDB(String oldtype, int page, int count, String parttype) throws Exception {
        String json = DNDataSource.list_FromDB(oldtype, page, count, parttype);
        if (json == null || "".equals(json) || "null".equals(json)) {
            return null;
        }
        return parseHomeData(json);
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return null;
        }
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sa", oldtype));
        param.add(new BasicNameValuePair("offset", new StringBuilder(String.valueOf(page * count)).toString()));
        param.add(new BasicNameValuePair("count", new StringBuilder(String.valueOf(count)).toString()));
        param.add(new BasicNameValuePair("action", "top"));
        String json = DNDataSource.list_FromNET(this.mContext.getResources().getString(R.string.app_url), param);
        Data data = parseHomeData(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }
}
