package com.city_life.fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.Urls;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.entity.Pictorial;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;
import java.util.ArrayList;

public class PicListFragment extends LoadMoreListFragment<Pictorial> {
    View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            PicListFragment.this.jump((Listitem) v.getTag());
        }
    };
    Drawable defaultimage;
    LinearLayout.LayoutParams fra_param;
    LinearLayout.LayoutParams lin_param;
    AbsListView.LayoutParams list_param;
    View listhead;
    private int size = 2;

    public static PicListFragment newInstance(String type, String parttype) {
        PicListFragment tf = new PicListFragment();
        tf.initType(type, parttype);
        return tf;
    }

    public PicListFragment() {
        this.isShowAD = false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup containers, Bundle savedInstanceState) {
        return super.onCreateView(inflater, containers, savedInstanceState);
    }

    public void findView() {
        super.findView();
        this.list_param = new AbsListView.LayoutParams(PerfHelper.getIntData(PerfHelper.P_PHONE_W) / this.size, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 254) / 720);
        this.lin_param = new LinearLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 371) / 720, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 354) / 720);
        this.fra_param = new LinearLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 275) / 720, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 282) / 720);
        this.fra_param.gravity = 1;
        this.mLength = 8;
        this.mFooter_limit = 8;
    }

    public View getListHeadview(Pictorial item) {
        return null;
    }

    public boolean dealClick(Pictorial item, int position) {
        return true;
    }

    static class Viewitem {
        ImageView image;
        TextView text;

        Viewitem() {
        }
    }

    static class hold {
        public Viewitem[] vs;

        hold() {
        }
    }

    public View getListItemview(View itemView, Pictorial allPictorial, int position) {
        int count = allPictorial.piclist.size();
        hold h = null;
        if (itemView != null) {
            h = (hold) itemView.getTag();
        }
        if (!(h != null && h.vs.length == this.size && count == this.size)) {
            itemView = new LinearLayout(this.mContext);
            h = new hold();
            h.vs = new Viewitem[count];
            for (int i = 0; i < count; i++) {
                Viewitem vi2 = new Viewitem();
                View item = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_pic, (ViewGroup) null);
                item.setLayoutParams(this.list_param);
                vi2.image = (ImageView) item.findViewById(R.id.listitem_icon);
                vi2.image.setLayoutParams(this.fra_param);
                vi2.text = (TextView) item.findViewById(R.id.listitem_title);
                h.vs[i] = vi2;
                vi2.image.setTag(allPictorial.piclist.get(i));
                ((LinearLayout) itemView).addView(item);
                vi2.image.setOnClickListener(this.clickListener);
            }
            itemView.setTag(h);
        }
        for (int i2 = 0; i2 < count; i2++) {
            Listitem p = allPictorial.piclist.get(i2);
            Viewitem iv = h.vs[i2];
            iv.text.setText(p.title);
            iv.image.setTag(allPictorial.piclist.get(i2));
            ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + p.icon, iv.image);
        }
        return itemView;
    }

    public void jump(Listitem item) {
        Intent intent = new Intent();
        intent.setAction(this.mContext.getResources().getString(R.string.activity_pic));
        intent.putExtra("item", item);
        startActivity(intent);
        System.out.println("=========");
    }

    public Data getDataFromDB(String oldtype, int page, int count, String parttype) throws Exception {
        Data data = super.getDataFromDB(oldtype, page, count, parttype);
        data.list = (ArrayList) PictorialOperate.packing(data.list, this.size);
        return data;
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        Data data = super.getDataFromNet(url, oldtype, page, count, isfirst, parttype);
        data.list = (ArrayList) PictorialOperate.packing(data.list, this.size);
        return data;
    }
}
