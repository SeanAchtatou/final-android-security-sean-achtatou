package com.city_life.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.city_life.ui.HomeActivity;
import com.city_life.view.Panel;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.utils.FileUtils;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.io.File;
import sina_weibo.WeiboShareDao;

public class SettingFragment extends Fragment implements View.OnClickListener {
    private static final String KEY_CONTENT = "SettingFragment:parttype";
    ImageView big;
    View.OnClickListener click = new View.OnClickListener() {
        public void onClick(View v) {
            SettingFragment.this.things(v);
        }
    };
    LinearLayout containers;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FinalVariable.vb_success /*10000*/:
                    SettingFragment.this.initShare();
                    Utils.dismissProcessDialog();
                    Utils.showToast("取消绑定成功");
                    return;
                case 10001:
                default:
                    return;
                case 10002:
                    Utils.dismissProcessDialog();
                    Utils.showToast("取消绑定失败");
                    return;
            }
        }
    };
    View home;
    Intent intent;
    Context mContext;
    Panel mPanel;
    View main_view;
    LinearLayout manager_bangding_title;
    ImageView middle;
    String parttype;
    ImageView samll;
    long size = 0;
    TextView text;
    TextView title;

    public static SettingFragment newInstance(String type) {
        SettingFragment tf = new SettingFragment();
        tf.init(type);
        return tf;
    }

    public void init(String type) {
        this.parttype = type;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, this.parttype);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup view, Bundle savedInstanceState) {
        if (this.parttype == null && savedInstanceState != null && savedInstanceState.containsKey(KEY_CONTENT)) {
            this.parttype = savedInstanceState.getString(KEY_CONTENT);
        }
        if (this.main_view == null) {
            this.main_view = inflater.inflate((int) R.layout.setting_frame, (ViewGroup) null);
            this.mContext = inflater.getContext();
            this.containers = new LinearLayout(getActivity());
            this.containers.addView(this.main_view);
            init();
        } else {
            if (this.containers != null) {
                this.containers.removeAllViews();
            }
            this.containers = new LinearLayout(getActivity());
            this.containers.addView(this.main_view);
        }
        return this.containers;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.big_content) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "b");
            this.big.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_n));
            this.samll.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
            this.middle.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
        } else if (v.getId() == R.id.middle_content) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "m");
            this.middle.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_n));
            this.big.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
            this.samll.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
        } else if (v.getId() == R.id.small_content) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "s");
            this.big.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
            this.middle.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
            this.samll.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_n));
        }
    }

    public void init() {
        this.title = (TextView) this.main_view.findViewById(R.id.title_title);
        this.mPanel = (Panel) this.main_view.findViewById(R.id.setting_text);
        this.mPanel.setOnPanelListener(new Panel.OnPanelListener() {
            public void onPanelOpened(Panel panel) {
                SettingFragment.this.main_view.findViewById(R.id.setting_text_size_bar).setBackgroundResource(R.drawable.setting_more_h);
            }

            public void onPanelClosed(Panel panel) {
                SettingFragment.this.main_view.findViewById(R.id.setting_text_size_bar).setBackgroundResource(R.drawable.setting_more_n);
            }
        });
        this.title.setText("设置");
        this.home = this.main_view.findViewById(R.id.title_home);
        this.home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingFragment.this.getActivity().onKeyDown(4, null);
            }
        });
        this.size = 0;
        getFilesInfo(FileUtils.sdPath);
        this.text = (TextView) this.main_view.findViewById(R.id.st_clear_size);
        this.text.setText(FileUtils.formatFileSize((long) ((int) this.size)));
        if ("".equals(PerfHelper.getStringData(PerfHelper.P_TEXT))) {
            PerfHelper.setInfo(PerfHelper.P_TEXT, "m");
        }
        this.main_view.findViewById(R.id.setting_clear).setOnClickListener(this.click);
        this.main_view.findViewById(R.id.setting_fav).setOnClickListener(this.click);
        this.main_view.findViewById(R.id.setting_feedback).setOnClickListener(this.click);
        this.main_view.findViewById(R.id.setting_about).setOnClickListener(this.click);
        this.main_view.findViewById(R.id.setting_tuisong_view).setOnClickListener(this.click);
        this.main_view.findViewById(R.id.setting_help).setOnClickListener(this.click);
        initTextSize();
    }

    public void onResume() {
        super.onResume();
        initShare();
    }

    public void initTextSize() {
        this.big = (ImageView) this.main_view.findViewById(R.id.big);
        this.middle = (ImageView) this.main_view.findViewById(R.id.middle);
        this.samll = (ImageView) this.main_view.findViewById(R.id.samll);
        this.main_view.findViewById(R.id.big_content).setOnClickListener(this);
        this.main_view.findViewById(R.id.middle_content).setOnClickListener(this);
        this.middle.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.middle_h));
        this.main_view.findViewById(R.id.small_content).setOnClickListener(this);
        String text2 = PerfHelper.getStringData(PerfHelper.P_TEXT);
        if ("s".equals(text2)) {
            this.big.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
            this.middle.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
            this.samll.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_n));
        } else if ("m".equals(text2)) {
            this.big.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
            this.samll.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
            this.middle.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_n));
        } else if ("b".equals(text2)) {
            this.big.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_n));
            this.middle.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
            this.samll.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.text_size_h));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.utils.PerfHelper.setInfo(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.utils.PerfHelper.setInfo(java.lang.String, int):void
      com.utils.PerfHelper.setInfo(java.lang.String, long):void
      com.utils.PerfHelper.setInfo(java.lang.String, java.lang.String):void
      com.utils.PerfHelper.setInfo(java.lang.String, boolean):void */
    public void things(View view) {
        switch (view.getId()) {
            case R.id.setting_share /*2131231158*/:
                this.intent = new Intent();
                this.intent.setAction(this.mContext.getResources().getString(R.string.activity_st_share_manager));
                startActivity(this.intent);
                return;
            case R.id.setting_feedback /*2131231164*/:
                this.intent = new Intent();
                this.intent.setAction(this.mContext.getResources().getString(R.string.activity_st_feedback));
                getActivity().startActivity(this.intent);
                return;
            case R.id.setting_fav /*2131231166*/:
                this.intent = new Intent();
                this.intent.setAction(this.mContext.getResources().getString(R.string.activity_st_fav));
                getActivity().startActivity(this.intent);
                return;
            case R.id.setting_about /*2131231172*/:
                this.intent = new Intent();
                this.intent.setAction(getActivity().getResources().getString(R.string.activity_st_about));
                getActivity().startActivity(this.intent);
                return;
            case R.id.setting_clear /*2131231173*/:
                new AlertDialog.Builder(this.mContext).setMessage("是否清除缓存？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.showProcessDialog(SettingFragment.this.mContext, "正在清除缓存...");
                        new Thread() {
                            public void run() {
                                ShareApplication.mImageWorker.mImageCache.clearCaches();
                                SettingFragment.this.deleteFilesInfo(FileUtils.sdPath);
                                DBHelper.getDBHelper().deleteall(SettingFragment.this.mContext.getResources().getStringArray(R.array.app_sql_delete));
                                Utils.dismissProcessDialog();
                                Utils.showToast("缓存清理完毕");
                                Utils.h.post(new Runnable() {
                                    public void run() {
                                        SettingFragment.this.text.setText("0kb");
                                    }
                                });
                            }
                        }.start();
                    }
                }).setNegativeButton("否", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
                return;
            case R.id.setting_help /*2131231183*/:
                this.intent = new Intent();
                this.intent.setAction(this.mContext.getResources().getString(R.string.activity_help));
                getActivity().startActivity(this.intent);
                return;
            case R.id.setting_tuisong_view /*2131231184*/:
                System.out.println("不科学");
                if (PerfHelper.getBooleanData(PerfHelper.P_PUSH)) {
                    this.main_view.findViewById(R.id.setting_tuisong).setBackgroundResource(R.drawable.bind_binded);
                    PerfHelper.setInfo(PerfHelper.P_PUSH, false);
                    return;
                }
                this.main_view.findViewById(R.id.setting_tuisong).setBackgroundResource(R.drawable.bind_bind);
                PerfHelper.setInfo(PerfHelper.P_PUSH, true);
                return;
            default:
                if (PerfHelper.getBooleanData(PerfHelper.P_SHARE_STATE + view.getTag())) {
                    final View v = view;
                    new AlertDialog.Builder(this.mContext).setMessage("是否取消绑定？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Utils.showProcessDialog(SettingFragment.this.mContext, "正在取消绑定...");
                            WeiboShareDao.bind_unbinded(v.getTag().toString(), SettingFragment.this.handler);
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    }).show();
                    return;
                }
                String pushbind = "http://push.cms.palmtrends.com/wb/bind_v2.php?pid=" + FinalVariable.pid + "&cid=3" + "&uid=" + PerfHelper.getStringData(PerfHelper.P_USERID);
                Intent intent2 = new Intent();
                intent2.putExtra("m_mainurl", String.valueOf(pushbind) + "&sname=" + view.getTag().toString());
                intent2.putExtra("sname", view.getTag().toString());
                intent2.setAction(getResources().getString(R.string.activity_share_bind));
                this.mContext.startActivity(intent2);
                return;
        }
    }

    public void deleteFilesInfo(String str) {
        File f = new File(str);
        if (f.listFiles() != null) {
            for (File file : f.listFiles()) {
                if (file.isDirectory()) {
                    deleteFilesInfo(file.getAbsolutePath());
                } else {
                    file.delete();
                }
            }
        }
    }

    private void getFilesInfo(String str) {
        File f = new File(str);
        if (f.listFiles() != null) {
            for (File file : f.listFiles()) {
                if (file.isDirectory()) {
                    getFilesInfo(file.getAbsolutePath());
                } else {
                    this.size += file.length();
                    System.out.println(file.getAbsoluteFile());
                }
            }
        }
    }

    public void initShare() {
        this.size = 0;
        getFilesInfo(FileUtils.sdPath);
        this.text = (TextView) this.main_view.findViewById(R.id.st_clear_size);
        this.text.setText(FileUtils.formatFileSize(this.size));
        this.manager_bangding_title = (LinearLayout) this.main_view.findViewById(R.id.manager_content);
        this.manager_bangding_title.removeAllViews();
        String[] array = this.mContext.getResources().getStringArray(R.array.article_wb_list_name_sa);
        int number = count;
        for (String item : array) {
            if (item.startsWith("wx")) {
                number--;
            }
        }
        for (int i = 0; i < count; i++) {
            String item2 = array[i];
            if (item2.startsWith("wx")) {
                number--;
            } else {
                View view = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_share, (ViewGroup) null);
                ImageView iv = (ImageView) view.findViewById(R.id.manager_icon);
                TextView title2 = (TextView) view.findViewById(R.id.manager_title);
                TextView name = (TextView) view.findViewById(R.id.manager_name);
                TextView btn = (TextView) view.findViewById(R.id.manager_btn);
                this.manager_bangding_title.addView(view);
                if (i != number - 1) {
                    View v = new View(this.mContext);
                    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(-1, 1);
                    int px = HomeActivity.dip2px(this.mContext, 10.0f);
                    llp.setMargins(px, 0, px, 0);
                    v.setLayoutParams(llp);
                    this.manager_bangding_title.addView(v);
                }
                btn.setTag(item2);
                btn.setOnClickListener(this.click);
                if ("sina".equals(item2)) {
                    title2.setText("新浪微博");
                    iv.setImageResource(R.drawable.icon_sina_h);
                } else if ("qq".equals(item2)) {
                    title2.setText("腾讯微博");
                    iv.setImageResource(R.drawable.icon_qq);
                } else if ("renren".equals(item2)) {
                    title2.setText("人人网");
                } else if ("kaixin".equals(item2)) {
                    title2.setText("开心网");
                }
                if (PerfHelper.getBooleanData(PerfHelper.P_SHARE_STATE + item2)) {
                    btn.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.bind_binded));
                    name.setText(PerfHelper.getStringData(PerfHelper.P_SHARE_NAME + item2));
                    ShareApplication.mImageWorker.loadImage(String.valueOf(PerfHelper.getStringData(PerfHelper.P_SHARE_USER_IMAGE + item2)) + "/120", iv);
                } else {
                    name.setText("");
                    btn.setBackgroundDrawable(this.mContext.getResources().getDrawable(R.drawable.bind_bind));
                }
            }
        }
    }
}
