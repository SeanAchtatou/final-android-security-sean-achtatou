package com.city_life.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.dao.Urls;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.PerfHelper;
import java.io.Serializable;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class WQListFragment extends LoadMoreListFragment<Listitem> {
    public static BaseFragment<Listitem> newInstance(String type, String partType) {
        WQListFragment tf = new WQListFragment();
        tf.initType(type, partType);
        return tf;
    }

    public void findView() {
        super.findView();
        this.mIcon_Layout = new LinearLayout.LayoutParams((PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 108) / 480, (PerfHelper.getIntData(PerfHelper.P_PHONE_W) * 157) / 480);
    }

    public View getListHeadview(Listitem item) {
        TextView tv = new TextView(this.mContext);
        tv.setText(item.title);
        return tv;
    }

    public View getListItemview(View view, Listitem item, int position) {
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_fm, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.listitem_title)).setText(item.title);
        ((TextView) view.findViewById(R.id.listitem_date)).setText(Html.fromHtml("<font color='#000000'><big><big>" + item.other1 + "</big></big></font>月<big><big> <font color='#000000'>" + item.other2 + "</font></big></big>日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "<font color='#878787'>" + item.u_date + "</font>"));
        ((TextView) view.findViewById(R.id.listitem_year)).setText(String.valueOf(item.other) + "年");
        ImageView icon = (ImageView) view.findViewById(R.id.listitem_icon);
        icon.setLayoutParams(this.mIcon_Layout);
        if (item.icon != null && item.icon.length() > 10) {
            ShareApplication.mImageWorker.loadImage(String.valueOf(Urls.main) + item.icon, icon);
        }
        return view;
    }

    public void addListener() {
        super.addListener();
        if (this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            this.mListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                    final Listitem li = (Listitem) arg0.getItemAtPosition(arg2);
                    AlertDialog show = new AlertDialog.Builder(WQListFragment.this.getActivity()).setMessage("您确认要删除本条记录吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            WQListFragment.this.mlistAdapter.datas.remove(arg2 - WQListFragment.this.mListview.getHeaderViewsCount());
                            WQListFragment.this.mlistAdapter.notifyDataSetChanged();
                            DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{li.n_mark});
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                    return false;
                }
            });
        }
    }

    public boolean dealClick(Listitem item, int position) {
        if (this.mlistAdapter == null || this.mlistAdapter.datas.size() <= 0) {
            return true;
        }
        Intent intent = new Intent();
        intent.setAction(this.mContext.getResources().getString(R.string.activity_wangqiinfo));
        intent.putExtra("item", item);
        intent.putExtra("items", (Serializable) this.mlistAdapter.datas);
        intent.putExtra("position", position - this.mListview.getHeaderViewsCount());
        startActivity(intent);
        return true;
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count);
        }
        String json = DNDataSource.list_FromNET(url, oldtype, page, count, parttype, isfirst);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has(Constants.SINA_CODE) || jsonobj.getInt(Constants.SINA_CODE) != 0) {
            JSONArray jsonay = jsonobj.getJSONArray("list");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                o.title = obj.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                o.sa = this.mOldtype;
                o.des = obj.getString("des");
                o.u_date = obj.getString("adddate");
                o.icon = obj.getString("icon");
                o.other = obj.getString("keyword");
                o.getMark();
                data.list.add(o);
            }
            return data;
        }
        throw new DataException(jsonobj.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG));
    }
}
