package com.city_life.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.MySSLSocketFactory;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Items;
import com.palmtrends.exceptions.DataException;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class CommentsFragment extends LoadMoreListFragment<Items> {
    public static final String ARTICLE_ID = "article_id";
    public String mArticlid;
    public OnReflashTotal mOnReflashTotal;

    public interface OnReflashTotal {
        void onReflash(Data data);
    }

    public static CommentsFragment newInstance(String type) {
        CommentsFragment tf = new CommentsFragment();
        tf.init(type);
        return tf;
    }

    public void init(String type) {
        this.mArticlid = type;
        this.isShowAD = false;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString(ARTICLE_ID, this.mArticlid);
        super.onSaveInstanceState(outState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup vgroup, Bundle savedInstanceState) {
        if (this.mArticlid == null && savedInstanceState != null && savedInstanceState.containsKey(ARTICLE_ID)) {
            this.mArticlid = savedInstanceState.getString(ARTICLE_ID);
        }
        return super.onCreateView(inflater, vgroup, savedInstanceState);
    }

    public Data getDataFromNet(String Url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        return parseJson(MySSLSocketFactory.getinfo(String.valueOf(getResources().getString(R.string.citylife_getComment_list_url)) + "sellerId=" + this.mArticlid + "&pageNo=" + (page + 1) + "&pageNum=" + count, new ArrayList<>()));
    }

    public boolean dealClick(Items item, int position) {
        return true;
    }

    public Data parseJson(String strs) throws Exception {
        Data d = new Data();
        JSONObject list = new JSONObject(strs);
        if (!list.has("responseCode") || list.getInt("responseCode") == 0) {
            JSONArray json = list.getJSONArray("results");
            String how = "";
            if (list.has("countNum")) {
                how = list.getString("countNum");
            }
            ArrayList al = new ArrayList();
            int count = json.length();
            for (int i = 0; i < count; i++) {
                JSONObject obj = json.getJSONObject(i);
                Items o = new Items();
                try {
                    o.nid = obj.getString(LocaleUtil.INDONESIAN);
                    o.title = obj.getString("nickName");
                    o.des = obj.getString("conent");
                    o.level = obj.getString("level");
                    o.u_date = obj.getString("addtime");
                    o.icon = obj.getString("avatar");
                } catch (Exception e) {
                }
                al.add(o);
            }
            d.list = al;
            d.date = how;
            return d;
        }
        throw new DataException("暂无评论返回");
    }

    public Data getDataFromDB(String oldtype, int page, int count, String parttype) throws Exception {
        return null;
    }

    public View getListItemview(View itemView, Items item, int position) {
        if (itemView == null) {
            itemView = LayoutInflater.from(getActivity()).inflate((int) R.layout.listitem_comment, (ViewGroup) null);
        }
        TextView title = (TextView) itemView.findViewById(R.id.article_comment_title);
        TextView content = (TextView) itemView.findViewById(R.id.article_comment_des);
        TextView time = (TextView) itemView.findViewById(R.id.article_comment_time);
        String des = item.des;
        ((RatingBar) itemView.findViewById(R.id.comment_layout_listview_ratingbar)).setRating(Float.parseFloat(item.level));
        if (des == null || "null".equals(des)) {
            des = "";
        }
        content.setText(des);
        title.setText(item.title);
        time.setText(item.u_date);
        return itemView;
    }

    public void update() {
        this.mOnReflashTotal.onReflash(this.mData);
        super.update();
    }
}
