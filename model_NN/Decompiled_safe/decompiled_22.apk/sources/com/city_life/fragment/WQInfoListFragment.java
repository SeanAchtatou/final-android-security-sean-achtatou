package com.city_life.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import com.palmtrends.basefragment.BaseFragment;
import com.palmtrends.basefragment.LoadMoreListFragment;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class WQInfoListFragment extends LoadMoreListFragment<Listitem> {
    public static BaseFragment<Listitem> newInstance(String type, String partType) {
        WQInfoListFragment tf = new WQInfoListFragment();
        tf.initType(type, partType);
        return tf;
    }

    public void findView() {
        this.isShowAD = false;
        super.findView();
    }

    public View getListHeadview(Listitem item) {
        TextView tv = new TextView(this.mContext);
        tv.setText(item.title);
        return tv;
    }

    public View getListItemview(View view, Listitem item, int position) {
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.listitem_fminfo, (ViewGroup) null);
        }
        TextView title = (TextView) view.findViewById(R.id.listitem_title);
        if (DBHelper.getDBHelper().counts("readitem", "n_mark='" + item.n_mark + "' and read='true'") <= 0 || this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            title.setTextColor(-16777216);
        } else {
            title.setTextColor(this.mContext.getResources().getColor(R.color.readed));
        }
        title.setText(item.title);
        return view;
    }

    public void addListener() {
        super.addListener();
        if (this.mOldtype.startsWith(DBHelper.FAV_FLAG)) {
            this.mListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                    final Listitem li = (Listitem) arg0.getItemAtPosition(arg2);
                    AlertDialog show = new AlertDialog.Builder(WQInfoListFragment.this.getActivity()).setMessage("您确认要删除本条记录吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            WQInfoListFragment.this.mlistAdapter.datas.remove(arg2 - WQInfoListFragment.this.mListview.getHeaderViewsCount());
                            WQInfoListFragment.this.mlistAdapter.notifyDataSetChanged();
                            DBHelper.getDBHelper().delete("listitemfa", "n_mark=?", new String[]{li.n_mark});
                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                    return false;
                }
            });
        }
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count, boolean isfirst, String parttype) throws Exception {
        if (oldtype.startsWith(DBHelper.FAV_FLAG)) {
            return DNDataSource.list_Fav(oldtype.replace(DBHelper.FAV_FLAG, ""), page, count);
        }
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("sa", "list_fm"));
        param.add(new BasicNameValuePair(LocaleUtil.INDONESIAN, oldtype));
        param.add(new BasicNameValuePair("offset", new StringBuilder(String.valueOf(page * count)).toString()));
        param.add(new BasicNameValuePair("count", new StringBuilder(String.valueOf(count)).toString()));
        String json = DNDataSource.list_FromNET(url, param);
        Data data = parseJson(json);
        if (data == null || data.list == null || data.list.size() <= 0) {
            return data;
        }
        if (isfirst) {
            DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
        }
        DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        return data;
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has(Constants.SINA_CODE) || jsonobj.getInt(Constants.SINA_CODE) != 0) {
            JSONArray jsonay = jsonobj.getJSONArray("list");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString("aid");
                o.title = obj.getString(com.tencent.tauth.Constants.PARAM_TITLE);
                o.sa = this.mOldtype;
                o.getMark();
                data.list.add(o);
            }
            return data;
        }
        throw new DataException(jsonobj.getString(com.tencent.tauth.Constants.PARAM_SEND_MSG));
    }
}
