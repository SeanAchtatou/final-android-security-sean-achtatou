package com.city_life.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.city_life.entity.DensityUtil;
import com.city_life.part_activiy.ShiChangZhuActivity;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.exceptions.DataException;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.tencent.tauth.Constants;
import com.utils.FinalVariable;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

@SuppressLint({"ValidFragment"})
public class MarketViewpager extends Fragment {
    private ProfessionalItemAdapter adapter;
    public int count = 0;
    boolean data_is_fail = true;
    public RelativeLayout mContainers;
    public Context mContext;
    public Data mData;
    private GridView mGridView;
    Handler mHandler;
    public View mMain_layout;
    String mOldtype = "market_";
    public String mParttype = "market_";
    int pageNo = 1;
    int pageNum = 12;
    public Resources res;

    public MarketViewpager() {
    }

    public MarketViewpager(int page) {
        this.pageNo = page;
        this.mParttype = String.valueOf(this.mParttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No) + page;
        this.mOldtype = String.valueOf(this.mOldtype) + PerfHelper.getStringData(PerfHelper.P_CITY_No) + page;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.pageNo = savedInstanceState.getInt("pageNo");
            this.mParttype = String.valueOf(this.mParttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No) + this.pageNo;
            this.mOldtype = String.valueOf(this.mOldtype) + PerfHelper.getStringData(PerfHelper.P_CITY_No) + this.pageNo;
        }
        this.mContext = inflater.getContext();
        if (this.mMain_layout == null) {
            this.mContainers = new RelativeLayout(this.mContext);
            this.mMain_layout = inflater.inflate((int) R.layout.professionalmarket_item_layout, (ViewGroup) null);
            initListFragment(inflater);
            this.mContainers.addView(this.mMain_layout);
        } else {
            this.mContainers.removeAllViews();
            this.mContainers = new RelativeLayout(getActivity());
            this.mContainers.addView(this.mMain_layout);
        }
        setRetainInstance(false);
        return this.mContainers;
    }

    public void initListFragment(LayoutInflater inflater) {
        if (PerfHelper.getIntData(PerfHelper.P_PHONE_H) > 830) {
            this.pageNum = 12;
        } else {
            this.pageNum = 9;
        }
        this.res = ShareApplication.share.getResources();
        this.mGridView = (GridView) this.mMain_layout.findViewById(R.id.professionalmarket_item_gridview);
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                MarketViewpager.this.mMain_layout.findViewById(R.id.refurbish_linear).setVisibility(8);
                switch (msg.what) {
                    case FinalVariable.update /*1001*/:
                        if (ShareApplication.debug) {
                            System.out.println("跟新::" + MarketViewpager.this.mData.list);
                        }
                        MarketViewpager.this.update();
                        return;
                    case FinalVariable.remove_footer /*1002*/:
                    case FinalVariable.change /*1003*/:
                    case FinalVariable.deletefoot /*1005*/:
                    case FinalVariable.addfoot /*1006*/:
                    case FinalVariable.first_load /*1008*/:
                    case FinalVariable.load_image /*1009*/:
                    case FinalVariable.other /*1010*/:
                    default:
                        return;
                    case FinalVariable.error /*1004*/:
                        if (!Utils.isNetworkAvailable(MarketViewpager.this.mContext)) {
                            Utils.showToast((int) R.string.network_error);
                            return;
                        } else if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            return;
                        } else {
                            Utils.showToast((int) R.string.network_error);
                            return;
                        }
                    case FinalVariable.nomore /*1007*/:
                        MarketViewpager.this.mHandler.sendEmptyMessage(FinalVariable.remove_footer);
                        if (msg.obj != null) {
                            Utils.showToast(msg.obj.toString());
                            return;
                        } else {
                            Utils.showToast((int) R.string.no_data);
                            return;
                        }
                    case FinalVariable.first_update /*1011*/:
                        MarketViewpager.this.update();
                        return;
                }
            }
        };
        initData();
    }

    /* access modifiers changed from: private */
    public void update() {
        ArrayList<Listitem> list = (ArrayList) this.mData.list;
        if (ShareApplication.debug) {
            System.out.println("跟新2::" + this.mParttype + "{来}" + this.adapter);
        }
        if (this.adapter == null) {
            this.adapter = new ProfessionalItemAdapter(list);
            this.mGridView.setAdapter((ListAdapter) this.adapter);
            return;
        }
        this.adapter.adapter_list.clear();
        this.adapter.addDate(list);
        this.adapter.notifyDataSetChanged();
    }

    public void update_new(int page) {
        this.pageNo = page;
        this.mParttype = String.valueOf(this.mParttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No) + page;
        this.mOldtype = String.valueOf(this.mOldtype) + PerfHelper.getStringData(PerfHelper.P_CITY_No) + page;
        if (this.adapter != null) {
            this.adapter.adapter_list.clear();
            this.adapter.notifyDataSetChanged();
        }
        initData();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("pageNo", this.pageNo);
    }

    class ProfessionalItemAdapter extends BaseAdapter {
        /* access modifiers changed from: private */
        public ArrayList<Listitem> adapter_list;
        int[] color_sz = {R.color.gridView_for_itembgcolor01, R.color.gridView_for_itembgcolor02, R.color.gridView_for_itembgcolor03, R.color.gridView_for_itembgcolor04, R.color.gridView_for_itembgcolor05, R.color.gridView_for_itembgcolor06, R.color.gridView_for_itembgcolor07, R.color.gridView_for_itembgcolor08, R.color.gridView_for_itembgcolor09, R.color.gridView_for_itembgcolor010};
        int count;
        private int img_bg = R.drawable.market_button_item;
        RelativeLayout.LayoutParams rla;

        public ProfessionalItemAdapter(ArrayList<Listitem> adapter_list2) {
            this.adapter_list = adapter_list2;
            this.count = adapter_list2.size();
            if (this.count < MarketViewpager.this.pageNum) {
                this.count = MarketViewpager.this.pageNum;
                return;
            }
            if (PerfHelper.getIntData(PerfHelper.P_PHONE_H) > 880) {
                this.rla = new RelativeLayout.LayoutParams(DensityUtil.dip2px(MarketViewpager.this.mContext, 107.0f), DensityUtil.dip2px(MarketViewpager.this.mContext, 107.0f));
                this.rla.addRule(13);
            } else if (PerfHelper.getIntData(PerfHelper.P_PHONE_H) >= 879 || PerfHelper.getIntData(PerfHelper.P_PHONE_H) <= 810) {
                this.rla = new RelativeLayout.LayoutParams(DensityUtil.dip2px(MarketViewpager.this.mContext, 105.0f), DensityUtil.dip2px(MarketViewpager.this.mContext, 105.0f));
                this.rla.addRule(13);
            } else {
                this.rla = new RelativeLayout.LayoutParams(DensityUtil.dip2px(MarketViewpager.this.mContext, 99.0f), DensityUtil.dip2px(MarketViewpager.this.mContext, 99.0f));
                this.rla.addRule(13);
            }
            int remainder = this.count % 3;
            if (remainder > 0) {
                this.count -= remainder;
                this.count += 3;
            }
        }

        public void addDate(ArrayList<Listitem> adapter_list2) {
            this.adapter_list = adapter_list2;
            this.count = adapter_list2.size();
            if (this.count < MarketViewpager.this.pageNum) {
                this.count = MarketViewpager.this.pageNum;
                return;
            }
            int remainder = this.count % 3;
            if (remainder > 0) {
                this.count -= remainder;
                this.count += 3;
            }
        }

        public int getCount() {
            return this.count;
        }

        public Object getItem(int position) {
            return this.adapter_list.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public class ViewHolder {
            public ImageView mImageView;
            public TextView mTextView;
            public TextView mheadTextView;
            LinearLayout text_linear;

            public ViewHolder() {
            }
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (PerfHelper.getIntData(PerfHelper.P_PHONE_H) > 880) {
                this.rla = new RelativeLayout.LayoutParams(DensityUtil.dip2px(MarketViewpager.this.mContext, 107.0f), DensityUtil.dip2px(MarketViewpager.this.mContext, 107.0f));
                this.rla.addRule(13);
            } else if (PerfHelper.getIntData(PerfHelper.P_PHONE_H) >= 879 || PerfHelper.getIntData(PerfHelper.P_PHONE_H) <= 810) {
                this.rla = new RelativeLayout.LayoutParams(DensityUtil.dip2px(MarketViewpager.this.mContext, 105.0f), DensityUtil.dip2px(MarketViewpager.this.mContext, 105.0f));
                this.rla.addRule(13);
            } else {
                this.rla = new RelativeLayout.LayoutParams(DensityUtil.dip2px(MarketViewpager.this.mContext, 99.0f), DensityUtil.dip2px(MarketViewpager.this.mContext, 99.0f));
                this.rla.addRule(13);
            }
            if (convertView == null) {
                convertView = LayoutInflater.from(MarketViewpager.this.getActivity()).inflate((int) R.layout.home_page_grid_msg, (ViewGroup) null);
                viewHolder = new ViewHolder();
                viewHolder.mImageView = (ImageView) convertView.findViewById(R.id.imagev_img);
                viewHolder.text_linear = (LinearLayout) convertView.findViewById(R.id.text_linear);
                viewHolder.mheadTextView = (TextView) convertView.findViewById(R.id.head_text);
                viewHolder.mTextView = (TextView) convertView.findViewById(R.id.textv_name);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            if (position == this.adapter_list.size() || position > this.adapter_list.size()) {
                convertView.setVisibility(4);
                if ("虚位以待".length() > 3) {
                    viewHolder.text_linear.setOrientation(1);
                } else {
                    viewHolder.text_linear.setOrientation(0);
                }
                viewHolder.mheadTextView.setText("虚位以待".substring(0, 1));
                viewHolder.mTextView.setText("虚位以待".substring(1));
            } else {
                convertView.setVisibility(0);
                String current_str = this.adapter_list.get(position).title;
                if (current_str.length() > 3) {
                    viewHolder.text_linear.setOrientation(1);
                } else {
                    viewHolder.text_linear.setOrientation(0);
                }
                viewHolder.mheadTextView.setText(current_str.substring(0, 1));
                viewHolder.mTextView.setText(current_str.substring(1));
            }
            viewHolder.mImageView.setImageResource(this.img_bg);
            viewHolder.mImageView.setLayoutParams(this.rla);
            viewHolder.mImageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (position == ProfessionalItemAdapter.this.adapter_list.size() || position > ProfessionalItemAdapter.this.adapter_list.size()) {
                        Toast.makeText(MarketViewpager.this.getActivity(), "同城生活圈期待你的加入", 0).show();
                        return;
                    }
                    Intent intent = new Intent(MarketViewpager.this.getActivity(), ShiChangZhuActivity.class);
                    intent.putExtra(LocaleUtil.INDONESIAN, ((Listitem) ProfessionalItemAdapter.this.adapter_list.get(position)).nid);
                    intent.putExtra(Constants.PARAM_TITLE, ((Listitem) ProfessionalItemAdapter.this.adapter_list.get(position)).title);
                    MarketViewpager.this.getActivity().startActivity(intent);
                }
            });
            return convertView;
        }
    }

    public Data getDataFromNet(String url, String oldtype, int page, int count2, boolean isfirst, String parttype) throws Exception {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("pageNo", String.valueOf(page)));
        param.add(new BasicNameValuePair("pageNum", String.valueOf(count2)));
        param.add(new BasicNameValuePair("cityId", PerfHelper.getStringData(PerfHelper.P_CITY_No)));
        String json = DNDataSource.list_FromNET(url, param);
        Data data = parseJson(json);
        if (!(data == null || data.list == null || data.list.size() <= 0)) {
            if (isfirst) {
                DBHelper.getDBHelper().delete("listinfo", "listtype=?", new String[]{oldtype});
            }
            DBHelper.getDBHelper().insert(String.valueOf(oldtype) + page, json, oldtype);
        }
        if (ShareApplication.debug) {
            System.out.println("解析完毕:");
        }
        return data;
    }

    public Data getDataFromDB(String oldtype, int page, int count2, String parttype) throws Exception {
        String json = DNDataSource.list_FromDB(oldtype, page, count2, parttype);
        if (json == null || "".equals(json) || "null".equals(json)) {
            return null;
        }
        return parseJson(json);
    }

    public void initData() {
        new Thread() {
            String old;

            public void run() {
                this.old = String.valueOf(MarketViewpager.this.pageNo);
                try {
                    Data d = MarketViewpager.this.getDataFromDB(MarketViewpager.this.mOldtype, MarketViewpager.this.pageNo, MarketViewpager.this.pageNum, MarketViewpager.this.mParttype);
                    if (!(d == null || d.list == null || d.list.size() <= 0)) {
                        MarketViewpager.this.mData = d;
                        MarketViewpager.this.mHandler.sendEmptyMessage(FinalVariable.update);
                    }
                    Data d2 = MarketViewpager.this.getDataFromNet(MarketViewpager.this.res.getString(R.string.citylife_market_list_url), MarketViewpager.this.mOldtype, MarketViewpager.this.pageNo, MarketViewpager.this.pageNum, true, MarketViewpager.this.mParttype);
                    MarketViewpager.this.mData = d2;
                    MarketViewpager.this.onDataLoadComplete(d2, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void onDataLoadComplete(Data data, boolean isFirst) {
        if (data == null || data.list == null) {
            this.mHandler.sendEmptyMessage(FinalVariable.error);
            return;
        }
        if (ShareApplication.debug) {
            System.out.println("解析完毕:更新");
        }
        this.mHandler.sendEmptyMessage(FinalVariable.update);
    }

    public Data parseJson(String json) throws Exception {
        if (ShareApplication.debug) {
            System.out.println("请求返回:" + json);
        }
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has("responseCode") || jsonobj.getInt("responseCode") == 0) {
            if (jsonobj.has("countNum")) {
                data.obj = jsonobj.getString("countNum");
            }
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count2 = jsonay.length();
            for (int i = 0; i < count2; i++) {
                Listitem o = new Listitem();
                JSONObject obj = jsonay.getJSONObject(i);
                o.nid = obj.getString(LocaleUtil.INDONESIAN);
                o.title = obj.getString("type");
                o.sa = String.valueOf(this.mParttype) + this.pageNo;
                try {
                    if (obj.has("cityId")) {
                        o.other1 = obj.getString("cityId");
                    }
                    if (obj.has("sort")) {
                        o.other = obj.getString("sort");
                    }
                } catch (Exception e) {
                }
                o.getMark();
                data.list.add(o);
            }
            return data;
        }
        throw new DataException(this.res.getString(R.string.no_data));
    }
}
