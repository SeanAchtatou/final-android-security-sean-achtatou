package com.city_life.part_asynctask;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;
import org.apache.commons.httpclient.methods.MultipartPostMethod;
import org.json.JSONObject;

public class UploadUtils {
    private static final String CHARSET = "utf-8";
    public static final String FAILURE = "1";
    public static final String SUCCESS = "0";
    private static final String TAG = "uploadFile";
    private static final int TIME_OUT = 100000000;

    public static String uploadFile(File file, String RequestURL) {
        String BOUNDARY = UUID.randomUUID().toString();
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(RequestURL).openConnection();
            conn.setReadTimeout(TIME_OUT);
            conn.setConnectTimeout(TIME_OUT);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Charset", CHARSET);
            conn.setRequestProperty("connection", "keep-alive");
            conn.setRequestProperty("Content-Type", String.valueOf(MultipartPostMethod.MULTIPART_FORM_CONTENT_TYPE) + ";boundary=" + BOUNDARY);
            if (file != null) {
                DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                StringBuffer sb = new StringBuffer();
                sb.append("--");
                sb.append(BOUNDARY);
                sb.append("\r\n");
                sb.append("Content-Disposition: form-data; name=\"img\"; filename=\"" + file.getName() + "\"" + "\r\n");
                sb.append("Content-Type: application/octet-stream; charset=utf-8" + "\r\n");
                sb.append("\r\n");
                dos.write(sb.toString().getBytes());
                InputStream is = new FileInputStream(file);
                byte[] bytes = new byte[1024];
                while (true) {
                    int len = is.read(bytes);
                    if (len == -1) {
                        break;
                    }
                    dos.write(bytes, 0, len);
                }
                is.close();
                dos.write("\r\n".getBytes());
                dos.write((String.valueOf("--") + BOUNDARY + "--" + "\r\n").getBytes());
                dos.flush();
                if (conn.getResponseCode() != 200) {
                    return FAILURE;
                }
                InputStream input = conn.getInputStream();
                byte[] b = new byte[1024];
                String img_url = null;
                while (true) {
                    int j = input.read(b);
                    if (j <= 0) {
                        break;
                    }
                    img_url = new String(b, 0, j);
                }
                input.close();
                try {
                    JSONObject img_json = new JSONObject(img_url.replace("??????!", ""));
                    if (SUCCESS.equals(new StringBuilder(String.valueOf(img_json.get("responseCode").toString())).toString())) {
                        return (String) img_json.get("newImgUrl");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return FAILURE;
    }
}
