package com.city_life.part_asynctask;

import android.content.res.Resources;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.part;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetNearPartThread extends Thread {
    public void run() {
        if ((String.valueOf(PerfHelper.getStringData("nearbytime")) + PerfHelper.getStringData(PerfHelper.P_CITY_No)).equals("")) {
            PerfHelper.setInfo("nearbytime" + PerfHelper.getStringData(PerfHelper.P_CITY_No), UploadUtils.FAILURE);
        }
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("agoDate", String.valueOf(PerfHelper.getStringData("nearbytime")) + PerfHelper.getStringData(PerfHelper.P_CITY_No)));
        param.add(new BasicNameValuePair("cityId", PerfHelper.getStringData(PerfHelper.P_CITY_No)));
        try {
            String json = DNDataSource.list_FromNET(ShareApplication.share.getResources().getString(R.string.citylife_getArea_url), param);
            if (ShareApplication.debug) {
                System.out.println("二级栏目区域接口返回:" + json);
            }
            parseJson(json);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        super.run();
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (jsonobj.has("responseCode")) {
            if (jsonobj.getInt("responseCode") != 0) {
                data.obj1 = jsonobj.getString("responseCode");
                return data;
            }
            data.obj1 = jsonobj.getString("responseCode");
        }
        initparts_nearby(json, "nearbypart");
        return null;
    }

    public void initparts_nearby(String json, String parttype) throws Exception {
        DBHelper db = DBHelper.getDBHelper();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has("responseCode") || jsonobj.getInt("responseCode") == 0) {
            List<part> design_patrs = new ArrayList<>();
            String time = String.valueOf(PerfHelper.getStringData("nearbytime")) + PerfHelper.getStringData(PerfHelper.P_CITY_No);
            if (jsonobj.has("areaName")) {
                if (!time.equals(jsonobj.getString("lastTime"))) {
                    PerfHelper.setInfo("nearbytime" + PerfHelper.getStringData(PerfHelper.P_CITY_No), jsonobj.getString("areaName"));
                } else {
                    return;
                }
            }
            JSONArray jsonay = jsonobj.getJSONArray("area");
            int count = jsonay.length();
            part o1 = new part();
            o1.part_type = String.valueOf(parttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No);
            o1.part_index = 0;
            o1.part_sa = UploadUtils.SUCCESS;
            o1.part_name = "全部";
            design_patrs.add(o1);
            for (int i = 0; i < count; i++) {
                part o = new part();
                JSONObject obj = jsonay.getJSONObject(i);
                o.part_type = String.valueOf(parttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No);
                o.part_index = Integer.valueOf(i + 1);
                o.part_sa = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has("areaName")) {
                        o.part_name = obj.getString("areaName");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                design_patrs.add(o);
            }
            db.delete("part_list", "part_type=?", new String[]{String.valueOf(parttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No)});
            db.insert(design_patrs, "part_list", part.class);
        }
    }
}
