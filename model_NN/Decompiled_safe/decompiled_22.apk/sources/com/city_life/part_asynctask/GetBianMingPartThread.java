package com.city_life.part_asynctask;

import android.content.res.Resources;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.dao.DBHelper;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.part;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import sina_weibo.Constants;

public class GetBianMingPartThread extends Thread {
    public void run() {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("cityId", PerfHelper.getStringData(PerfHelper.P_CITY_No)));
        try {
            String json = DNDataSource.list_FromNET(ShareApplication.share.getResources().getString(R.string.citylife_convenience_title_url), param);
            if (ShareApplication.debug) {
                System.out.println("地区区域接口返回:" + json);
            }
            parseJson(json);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        super.run();
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (jsonobj.has("responseCode")) {
            if (jsonobj.getInt("responseCode") != 0) {
                data.obj1 = jsonobj.getString("responseCode");
                return data;
            }
            data.obj1 = jsonobj.getString("responseCode");
        }
        initparts_bianming(json, "convenience");
        return null;
    }

    public void initparts_bianming(String json, String parttype) throws Exception {
        DBHelper db = DBHelper.getDBHelper();
        JSONObject jsonobj = new JSONObject(json);
        if (!jsonobj.has("responseCode") || jsonobj.getInt("responseCode") == 0) {
            List<part> design_patrs = new ArrayList<>();
            JSONArray jsonay = jsonobj.getJSONArray("results");
            int count = jsonay.length();
            for (int i = 0; i < count; i++) {
                part o = new part();
                JSONObject obj = jsonay.getJSONObject(i);
                o.part_type = String.valueOf(parttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No);
                o.part_index = Integer.valueOf(i);
                o.part_sa = obj.getString(LocaleUtil.INDONESIAN);
                try {
                    if (obj.has(Constants.SINA_NAME)) {
                        o.part_name = obj.getString(Constants.SINA_NAME);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                design_patrs.add(o);
            }
            db.delete("part_list", "part_type=?", new String[]{String.valueOf(parttype) + PerfHelper.getStringData(PerfHelper.P_CITY_No)});
            db.insert(design_patrs, "part_list", part.class);
        }
    }
}
