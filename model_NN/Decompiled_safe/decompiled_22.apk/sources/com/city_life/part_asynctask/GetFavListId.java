package com.city_life.part_asynctask;

import android.content.res.Resources;
import android.os.AsyncTask;
import com.city_life.entity.CityLifeApplication;
import com.palmtrends.app.ShareApplication;
import com.palmtrends.datasource.DNDataSource;
import com.palmtrends.entity.Data;
import com.palmtrends.entity.Listitem;
import com.palmtrends.loadimage.Utils;
import com.pengyou.citycommercialarea.R;
import com.tencent.mm.sdk.platformtools.LocaleUtil;
import com.utils.PerfHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetFavListId extends AsyncTask<Void, Void, HashMap<String, Object>> {
    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((HashMap<String, Object>) ((HashMap) obj));
    }

    /* access modifiers changed from: protected */
    public HashMap<String, Object> doInBackground(Void... params) {
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("userId", PerfHelper.getStringData(PerfHelper.P_USERID)));
        param.add(new BasicNameValuePair("pageNo", UploadUtils.FAILURE));
        param.add(new BasicNameValuePair("pageNum", "100"));
        HashMap<String, Object> mhashmap = new HashMap<>();
        try {
            String json = DNDataSource.list_FromNET(ShareApplication.share.getResources().getString(R.string.citylife_getCollect_list_url), param);
            if (ShareApplication.debug) {
                System.out.println("收藏列表返回:" + json);
            }
            Data date = parseJson(json);
            mhashmap.put("responseCode", 1);
            mhashmap.put("results", date.list);
            return mhashmap;
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public Data parseJson(String json) throws Exception {
        Data data = new Data();
        JSONObject jsonobj = new JSONObject(json);
        if (jsonobj.has("responseCode")) {
            if (jsonobj.getInt("responseCode") != 0) {
                data.obj1 = jsonobj.getString("responseCode");
                return data;
            }
            data.obj1 = jsonobj.getString("responseCode");
        }
        JSONArray jsonay = jsonobj.getJSONArray("results");
        CityLifeApplication.fav_list_id = new ArrayList<>();
        int count = jsonay.length();
        for (int i = 0; i < count; i++) {
            Listitem o = new Listitem();
            JSONObject obj = jsonay.getJSONObject(i);
            try {
                if (obj.has(LocaleUtil.INDONESIAN)) {
                    o.nid = obj.getString(LocaleUtil.INDONESIAN);
                }
            } catch (Exception e) {
            }
            o.getMark();
            CityLifeApplication.fav_list_id.add(o);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(HashMap<String, Object> result) {
        super.onPostExecute((Object) result);
        Utils.dismissProcessDialog();
        if (result != null && !UploadUtils.FAILURE.equals(result.get("responseCode"))) {
            UploadUtils.SUCCESS.equals(result.get("responseCode"));
        }
    }
}
