package com.city_life.part_asynctask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.Toast;
import com.palmtrends.app.ShareApplication;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;
import java.io.File;

public class UploadFileTask extends AsyncTask<String, Void, String> {
    public static final String requestURL = ShareApplication.share.getResources().getString(R.string.citylife_FileImageUploadServlet_url);
    private Activity context = null;
    Handler hander;
    private ProgressDialog pdialog;

    public UploadFileTask(Activity ctx, Handler hander2) {
        this.context = ctx;
        this.hander = hander2;
        this.pdialog = ProgressDialog.show(this.context, "正在加载...", "系统正在处理您的请求");
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        this.pdialog.dismiss();
        if (UploadUtils.FAILURE.equalsIgnoreCase(result)) {
            Toast.makeText(this.context, "未知错误，上传失败!", 1).show();
            return;
        }
        Toast.makeText(this.context, "恭喜，上传成功!", 1).show();
        PerfHelper.setInfo(PerfHelper.P_SHARE_USER_IMAGE, result);
        this.hander.obtainMessage(0).sendToTarget();
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... params) {
        return UploadUtils.uploadFile(new File(params[0]), String.valueOf(requestURL) + "username=" + params[1]);
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Void... values) {
    }
}
