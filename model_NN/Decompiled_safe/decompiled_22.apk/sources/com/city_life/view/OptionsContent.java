package com.city_life.view;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Scroller;
import android.widget.TextView;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OptionsContent extends LinearLayout {
    private static final int DONE = 3;
    private static final int LOADING = 4;
    private static final int PULL_To_REFRESH = 1;
    private static final int REFRESHING = 2;
    private static final int RELEASE_To_REFRESH = 0;
    private static final String TAG = "OptionsScrollView";
    private static final Interpolator sInterpolator = new Interpolator() {
        public float getInterpolation(float t) {
            float t2 = t - 1.0f;
            return (t2 * t2 * t2 * t2 * t2) + 1.0f;
        }
    };
    private RotateAnimation animation;
    private ImageView arrowImageView;
    public boolean canreturn;
    public ProgressBar footer_pb;
    public TextView footer_text;
    public View header;
    public int headerHeight;
    public boolean isRecored;
    /* access modifiers changed from: private */
    public TextView lastUpdatedTextView;
    public LinearLayout mContent;
    public int mCurrentState = 3;
    public View mList_footer;
    public OptionsListener mListener;
    public ScrollView mScrollView;
    /* access modifiers changed from: private */
    public Scroller mScroller;
    public float poistion_start;
    private ProgressBar progressBar;
    public String reflashtime = "reflash_time";
    private RotateAnimation reverseAnimation;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
    /* access modifiers changed from: private */
    public float startY;
    private TextView tipsTextview;

    public interface OptionsListener {
        void loadmore();

        void reflashing();
    }

    public OptionsContent(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public OptionsContent(Context context) {
        super(context);
        initViews();
    }

    public void initViews() {
        setOrientation(1);
        this.header = inflate(getContext(), R.layout.xlistview_header, null);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, -2);
        this.mScroller = new Scroller(getContext(), sInterpolator);
        this.header.setVisibility(4);
        this.header.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                OptionsContent.this.headerHeight = OptionsContent.this.header.getHeight();
                int oldScrollX = OptionsContent.this.getScrollX();
                OptionsContent.this.mScroller.startScroll(oldScrollX, OptionsContent.this.getScrollY(), oldScrollX, OptionsContent.this.headerHeight, 10);
                OptionsContent.this.invalidate();
                OptionsContent.this.header.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                OptionsContent.this.header.setVisibility(0);
            }
        });
        addView(this.header, lp);
        this.mContent = new LinearLayout(getContext());
        addView(this.mContent, lp);
        this.mScrollView = new OptionsScrollView(getContext());
        this.mScrollView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.mScrollView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        this.mScrollView.addView(this);
        this.arrowImageView = (ImageView) this.header.findViewById(R.id.xlistview_header_arrow);
        this.progressBar = (ProgressBar) this.header.findViewById(R.id.xlistview_header_progressbar);
        this.tipsTextview = (TextView) this.header.findViewById(R.id.xlistview_header_hint_textview);
        this.lastUpdatedTextView = (TextView) this.header.findViewById(R.id.xlistview_header_time);
        this.mList_footer = LayoutInflater.from(getContext()).inflate((int) R.layout.footer, (ViewGroup) null);
        this.footer_text = (TextView) this.mList_footer.findViewById(R.id.footer_text);
        this.footer_pb = (ProgressBar) this.mList_footer.findViewById(R.id.footer_pb);
        addView(this.mList_footer, lp);
        this.animation = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.animation.setInterpolator(new LinearInterpolator());
        this.animation.setDuration(250);
        this.animation.setFillAfter(true);
        this.reverseAnimation = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.reverseAnimation.setInterpolator(new LinearInterpolator());
        this.reverseAnimation.setDuration(200);
        this.reverseAnimation.setFillAfter(true);
        this.mList_footer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OptionsContent.this.changeFooterState(0);
                new loadmore().execute(new String[0]);
            }
        });
        changeFooterState(1);
    }

    public class loadmore extends AsyncTask<String, String, String> {
        public loadmore() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            if (OptionsContent.this.mListener != null) {
                OptionsContent.this.mListener.loadmore();
                return null;
            }
            try {
                Thread.sleep(3000);
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            OptionsContent.this.changeFooterState(1);
            super.onPostExecute((Object) result);
        }
    }

    public LinearLayout getContentView() {
        return this.mContent;
    }

    public ScrollView getScrollView() {
        return this.mScrollView;
    }

    public void addFooter() {
        this.mList_footer.setVisibility(0);
    }

    public void removeFooter() {
        this.mList_footer.setVisibility(8);
    }

    public void changeFooterState(int state) {
        switch (state) {
            case 0:
                this.footer_text.setText("正在加载...");
                this.footer_pb.setVisibility(0);
                return;
            case 1:
                this.footer_text.setText("点击正在加载更多");
                this.footer_pb.setVisibility(8);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void smoothScrollTo(int dy) {
        int oldScrollX = getScrollX();
        this.mScroller.startScroll(oldScrollX, getScrollY(), oldScrollX, dy, 600);
        invalidate();
    }

    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(ev);
    }

    /* access modifiers changed from: package-private */
    public void enableChildrenCache() {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChildAt(i).setDrawingCacheEnabled(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void clearChildrenCache() {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChildAt(i).setDrawingCacheEnabled(false);
        }
    }

    public void computeScroll() {
        if (this.mScroller.isFinished()) {
            clearChildrenCache();
        } else if (this.mScroller.computeScrollOffset()) {
            int oldX = getScrollX();
            int oldY = getScrollY();
            int x = this.mScroller.getCurrX();
            int y = this.mScroller.getCurrY();
            if (!(oldX == x && oldY == y)) {
                scrollTo(x, y);
            }
            invalidate();
        } else {
            clearChildrenCache();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t - this.headerHeight, r, b - this.headerHeight);
    }

    public class OptionsScrollView extends ScrollView {
        public OptionsScrollView(Context context) {
            super(context);
        }

        public boolean dispatchTouchEvent(MotionEvent ev) {
            float y = ev.getY();
            switch (ev.getAction()) {
                case 0:
                    OptionsContent.this.poistion_start = ev.getY();
                    OptionsContent.this.isRecored = false;
                    break;
                case 1:
                    float scrollY = (float) OptionsContent.this.getScrollY();
                    if (!OptionsContent.this.canreturn || OptionsContent.this.mCurrentState != 0) {
                        if (OptionsContent.this.mCurrentState == 3 || OptionsContent.this.mCurrentState == 1) {
                            OptionsContent.this.mCurrentState = 3;
                            OptionsContent.this.smoothScrollTo((-OptionsContent.this.getScrollY()) + OptionsContent.this.headerHeight);
                            OptionsContent.this.changeHeaderViewByState();
                            break;
                        }
                    } else {
                        OptionsContent.this.smoothScrollTo(-OptionsContent.this.getScrollY());
                        OptionsContent.this.mCurrentState = 2;
                        OptionsContent.this.changeHeaderViewByState();
                        new loadData().execute(new String[0]);
                        break;
                    }
                    break;
                case 2:
                    float deltaY = OptionsContent.this.poistion_start - y;
                    if (13.0f <= Math.abs(deltaY) && OptionsContent.this.mCurrentState != 2) {
                        if (!OptionsContent.this.isRecored && getScrollY() == 0) {
                            OptionsContent.this.isRecored = true;
                            OptionsContent.this.startY = y;
                        }
                        float oldScrollY = (float) OptionsContent.this.getScrollY();
                        if (oldScrollY < ((float) (-OptionsContent.this.headerHeight))) {
                            deltaY *= Math.abs(((float) OptionsContent.this.headerHeight) / oldScrollY);
                        }
                        if (OptionsContent.this.mScrollView.getScrollY() < 5) {
                            if (oldScrollY == ((float) OptionsContent.this.headerHeight) && deltaY > 0.0f) {
                                OptionsContent.this.canreturn = false;
                                break;
                            } else {
                                if (y - OptionsContent.this.startY < ((float) ((OptionsContent.this.headerHeight * 3) / 2))) {
                                    if (OptionsContent.this.mCurrentState == 0) {
                                        OptionsContent.this.mCurrentState = 1;
                                        OptionsContent.this.changeHeaderViewByState();
                                        OptionsContent.this.canreturn = false;
                                    }
                                } else if (oldScrollY < ((float) (-((OptionsContent.this.headerHeight * 3) / 2))) && OptionsContent.this.mCurrentState != 0) {
                                    OptionsContent.this.mCurrentState = 0;
                                    OptionsContent.this.changeHeaderViewByState();
                                    OptionsContent.this.canreturn = true;
                                }
                                OptionsContent.this.poistion_start = y;
                                OptionsContent.this.scrollTo(getScrollX(), ((int) oldScrollY) + ((int) deltaY));
                                return true;
                            }
                        }
                    }
                    break;
            }
            return super.dispatchTouchEvent(ev);
        }

        public boolean onTouchEvent(MotionEvent ev) {
            return super.onTouchEvent(ev);
        }
    }

    public class loadData extends AsyncTask<String, String, String> {
        public loadData() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            if (OptionsContent.this.mListener != null) {
                OptionsContent.this.mListener.reflashing();
                return null;
            }
            try {
                Thread.sleep(3000);
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            OptionsContent.this.mCurrentState = 3;
            OptionsContent.this.changeHeaderViewByState();
            OptionsContent.this.smoothScrollTo((-OptionsContent.this.getScrollY()) + OptionsContent.this.headerHeight);
            OptionsContent.this.lastUpdatedTextView.setText(OptionsContent.this.sdf.format(new Date()));
            PerfHelper.setInfo(OptionsContent.this.reflashtime, OptionsContent.this.sdf.format(new Date()));
            super.onPostExecute((Object) result);
        }
    }

    /* access modifiers changed from: private */
    public void changeHeaderViewByState() {
        switch (this.mCurrentState) {
            case 0:
                this.arrowImageView.setVisibility(0);
                this.progressBar.setVisibility(8);
                this.tipsTextview.setVisibility(0);
                this.lastUpdatedTextView.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.startAnimation(this.animation);
                this.tipsTextview.setText("松开刷新");
                Log.i(TAG, "当前状态，松开刷新");
                return;
            case 1:
                this.progressBar.setVisibility(8);
                this.tipsTextview.setVisibility(0);
                this.lastUpdatedTextView.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.startAnimation(this.reverseAnimation);
                this.tipsTextview.setText("下拉刷新");
                Log.i(TAG, "当前状态，下拉刷新");
                return;
            case 2:
                this.progressBar.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(8);
                this.tipsTextview.setText("正在刷新...");
                this.lastUpdatedTextView.setVisibility(0);
                Log.i(TAG, "当前状态,正在刷新...");
                return;
            case 3:
                this.progressBar.setVisibility(8);
                this.arrowImageView.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setImageResource(R.drawable.ic_pulltorefresh_arrow);
                this.tipsTextview.setText("下拉刷新");
                this.lastUpdatedTextView.setVisibility(0);
                Log.i(TAG, "当前状态，done");
                return;
            default:
                return;
        }
    }
}
