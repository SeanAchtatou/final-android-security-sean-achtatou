package com.city_life.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebView;

public class TitleWebView extends WebView implements View.OnClickListener {
    ScrollInterface mt;

    public interface ScrollInterface {
        void onSChanged(int i, int i2, int i3, int i4);
    }

    public TitleWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public TitleWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TitleWebView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (this.mt != null) {
            this.mt.onSChanged(l, t, oldl, oldt);
        }
    }

    public void setOnCustomScroolChangeListener(ScrollInterface scrollInterface) {
        this.mt = scrollInterface;
    }

    public void onClick(View v) {
    }
}
