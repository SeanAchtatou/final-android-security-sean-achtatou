package com.city_life.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

public abstract class CategoryAdapter extends BaseAdapter {
    private List<Category> categories = new ArrayList();

    /* access modifiers changed from: protected */
    public abstract View getTitleView(String str, int i, View view, ViewGroup viewGroup);

    public void addCategory(String title, Adapter adapter) {
        this.categories.add(new Category(title, adapter));
    }

    public int getCount() {
        int total = 0;
        for (Category category : this.categories) {
            total += category.getAdapter().getCount() + 1;
        }
        return total;
    }

    public Object getItem(int position) {
        for (Category category : this.categories) {
            if (position == 0) {
                return category;
            }
            int size = category.getAdapter().getCount() + 1;
            if (position < size) {
                return category.getAdapter().getItem(position - 1);
            }
            position -= size;
        }
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getViewTypeCount() {
        int total = 1;
        for (Category category : this.categories) {
            total += category.getAdapter().getViewTypeCount();
        }
        return total;
    }

    public int getItemViewType(int position) {
        int typeOffset = 1;
        for (Category category : this.categories) {
            if (position == 0) {
                return 0;
            }
            int size = category.getAdapter().getCount() + 1;
            if (position < size) {
                return category.getAdapter().getItemViewType(position - 1) + typeOffset;
            }
            position -= size;
            typeOffset += category.getAdapter().getViewTypeCount();
        }
        return -1;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        int categoryIndex = 0;
        for (Category category : this.categories) {
            if (position == 0) {
                return getTitleView(category.getTitle(), categoryIndex, convertView, parent);
            }
            int size = category.getAdapter().getCount() + 1;
            if (position < size) {
                return category.getAdapter().getView(position - 1, convertView, parent);
            }
            position -= size;
            categoryIndex++;
        }
        return null;
    }
}
