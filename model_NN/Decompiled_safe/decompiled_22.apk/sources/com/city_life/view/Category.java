package com.city_life.view;

import android.widget.Adapter;

public class Category {
    private Adapter mAdapter;
    private String mTitle;

    public Category(String title, Adapter adapter) {
        this.mTitle = title;
        this.mAdapter = adapter;
    }

    public void setTile(String title) {
        this.mTitle = title;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public void setAdapter(Adapter adapter) {
        this.mAdapter = adapter;
    }

    public Adapter getAdapter() {
        return this.mAdapter;
    }
}
