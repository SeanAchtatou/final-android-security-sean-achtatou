package com.city_life.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class Titles extends RelativeLayout {
    int titleheigth;

    public Titles(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typearray = context.obtainStyledAttributes(attrs, R.styleable.main_title);
        this.titleheigth = typearray.getInt(4, 0);
        typearray.recycle();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        View back = findViewById(R.id.title_back);
        if (back != null) {
            back.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ((Activity) Titles.this.getContext()).finish();
                }
            });
        }
        setLayoutParams(new RelativeLayout.LayoutParams(-1, (this.titleheigth * PerfHelper.getIntData(PerfHelper.P_PHONE_W)) / 480));
    }
}
