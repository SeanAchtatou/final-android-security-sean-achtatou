package com.city_life.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class YuanImageView extends ImageView {
    private Paint paint = new Paint();

    public YuanImageView(Context context) {
        super(context);
    }

    public YuanImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public YuanImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (drawable != null) {
            Bitmap b = toRoundCorner(((BitmapDrawable) drawable).getBitmap(), 14);
            Rect rect = new Rect(0, 0, b.getWidth(), b.getHeight());
            this.paint.reset();
            canvas.drawBitmap(b, rect, rect, this.paint);
            return;
        }
        super.onDraw(canvas);
    }

    private Bitmap toRoundCorner(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        this.paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        this.paint.setColor(-12434878);
        int x = bitmap.getWidth();
        canvas.drawCircle((float) (x / 2), (float) (x / 2), (float) (x / 2), this.paint);
        this.paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, this.paint);
        return output;
    }
}
