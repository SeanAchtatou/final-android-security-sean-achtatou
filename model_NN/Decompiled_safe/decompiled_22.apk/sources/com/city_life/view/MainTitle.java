package com.city_life.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.pengyou.citycommercialarea.R;
import com.utils.PerfHelper;

public class MainTitle extends RelativeLayout {
    int hasback;
    int hashome;
    int hassetting;
    int hastitle;
    int titleheigth;

    public MainTitle(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typearray = context.obtainStyledAttributes(attrs, R.styleable.main_title);
        this.hashome = typearray.getInt(0, 8);
        this.hastitle = typearray.getInt(2, 8);
        this.hasback = typearray.getInt(3, 8);
        this.hassetting = typearray.getInt(1, 8);
        this.titleheigth = typearray.getInt(4, 60);
        typearray.recycle();
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        View back = findViewById(R.id.title_back);
        setPadding(0, 0, 0, 10);
        back.setVisibility(this.hasback);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((Activity) MainTitle.this.getContext()).finish();
            }
        });
        View title = findViewById(R.id.title_title);
        if (title != null) {
            title.setVisibility(this.hastitle);
        }
        View home = findViewById(R.id.title_home);
        if (home != null) {
            home.setVisibility(this.hashome);
        }
        View setting = findViewById(R.id.title_setting);
        if (setting != null) {
            setting.setVisibility(this.hassetting);
            setting.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(MainTitle.this.getResources().getString(R.string.activity_setting));
                    MainTitle.this.getContext().startActivity(intent);
                }
            });
        }
        int h = (this.titleheigth * PerfHelper.getIntData(PerfHelper.P_PHONE_W)) / 480;
        System.out.println(String.valueOf(h) + "===");
        setLayoutParams(new RelativeLayout.LayoutParams(-1, h));
    }
}
