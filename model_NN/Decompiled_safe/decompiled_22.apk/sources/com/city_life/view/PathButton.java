package com.city_life.view;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.pengyou.citycommercialarea.R;
import java.lang.reflect.Array;

public class PathButton {
    private static int height_center;
    /* access modifiers changed from: private */
    public static int item_width;
    private static int startX;
    private static int startY;
    private static int width;
    private static int width_center;
    private Activity activity;
    private int[] animation_duration = {300, 300, 300, 300};
    private ImageView buttonCenter;
    /* access modifiers changed from: private */
    public int count = 4;
    private int[][] distances;
    public int[][] drawableIds;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            int id = msg.what;
            if (id == -1) {
                PathButton.this.isloading = false;
                return;
            }
            for (int i = 0; i < PathButton.this.count; i++) {
                PathButton.this.imageViews[i].setBackgroundResource(PathButton.this.drawableIds[i][0]);
            }
            PathButton.this.imageViews[id].setBackgroundResource(PathButton.this.drawableIds[id][1]);
            if (!PathButton.this.isMenu || (PathButton.this.isMenu && id != PathButton.this.old_id)) {
                PathButton.this.oncheckedid.setCheckedId(id);
                PathButton.this.old_id = id;
            }
        }
    };
    private int[] imageIds;
    /* access modifiers changed from: private */
    public ImageView[] imageViews;
    /* access modifiers changed from: private */
    public boolean isMenu = true;
    /* access modifiers changed from: private */
    public Boolean isOpen = false;
    /* access modifiers changed from: private */
    public boolean isloading = false;
    private View.OnClickListener listener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() != R.id.about) {
                PathButton.this.centerRotate((ImageView) v, ((Integer) v.getTag()).intValue());
            } else if (!PathButton.this.isloading) {
                PathButton.this.isloading = true;
                if (PathButton.this.isOpen.booleanValue()) {
                    PathButton.this.isOpen = false;
                    for (int i = 0; i < PathButton.this.count; i++) {
                        PathButton.this.imageViews[i].startAnimation(PathButton.this.set_ins[i]);
                    }
                    PathButton.this.path_bg.startAnimation(PathButton.this.setAnimScale(PathButton.this.path_bg, 1.0f, 0.0f, 1.0f, 0.0f, false));
                    return;
                }
                PathButton.this.isOpen = true;
                for (int i2 = 0; i2 < PathButton.this.count; i2++) {
                    PathButton.this.imageViews[i2].startAnimation(PathButton.this.set_outs[i2]);
                }
                PathButton.this.path_bg.startAnimation(PathButton.this.setAnimScale(PathButton.this.path_bg, 0.0f, 1.0f, 0.0f, 1.0f, true));
            }
        }
    };
    /* access modifiers changed from: private */
    public int old_id;
    public onCheckedId oncheckedid;
    private RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(0, 0);
    /* access modifiers changed from: private */
    public ImageView path_bg;
    public View path_layout;
    private int rotate_duration = 60;
    private int rotate_offset = 60;
    /* access modifiers changed from: private */
    public AnimationSet[] set_ins;
    /* access modifiers changed from: private */
    public AnimationSet[] set_outs;
    private int traslate_offset = 100;

    public interface onCheckedId {
        void setCheckedId(int i);
    }

    public PathButton(Activity activity2, int count2, boolean isMenu2, int[][] drawableIds2) {
        this.activity = activity2;
        this.count = count2;
        this.isMenu = isMenu2;
        this.drawableIds = drawableIds2;
    }

    public void setIamges() {
        for (int i = 0; i < this.count; i++) {
            this.imageViews[i].setBackgroundResource(this.drawableIds[i][0]);
        }
    }

    private void findViews() {
    }

    public void initialButton(int id) {
        findViews();
        width = this.activity.getWindowManager().getDefaultDisplay().getWidth();
        width_center = (width * 63) / 480;
        height_center = (width * 60) / 480;
        item_width = (width * 54) / 480;
        int bottom = (width * 12) / 480;
        int margin = (width * 4) / 480;
        startX = (width - item_width) / 2;
        startY = bottom + margin;
        this.params = new RelativeLayout.LayoutParams(item_width, item_width);
        this.params.setMargins(startX, 0, 0, startY);
        this.params.addRule(14);
        this.params.addRule(12);
        int startX1 = (width - width_center) / 2;
        RelativeLayout.LayoutParams params_center = new RelativeLayout.LayoutParams(width_center, height_center);
        params_center.setMargins(startX1, 0, 0, bottom);
        params_center.addRule(14);
        params_center.addRule(12);
        this.buttonCenter.setLayoutParams(params_center);
        this.buttonCenter.setOnClickListener(this.listener);
        int w = (width * 228) / 480;
        int h = (w * 115) / 228;
        RelativeLayout.LayoutParams path_bg_param = new RelativeLayout.LayoutParams(w, h);
        path_bg_param.addRule(14);
        path_bg_param.addRule(12);
        this.path_bg.setLayoutParams(path_bg_param);
        this.old_id = id;
        if (this.isMenu) {
            this.oncheckedid.setCheckedId(this.old_id);
        }
        for (int i = 0; i < this.count; i++) {
            this.imageViews[i].setLayoutParams(this.params);
            this.imageViews[i].setBackgroundResource(this.drawableIds[i][0]);
            this.imageViews[i].setVisibility(4);
            this.imageViews[i].setOnClickListener(this.listener);
        }
        this.imageViews[this.old_id].setBackgroundResource(this.drawableIds[this.old_id][1]);
        int radiusX = w / 2;
        int radiusY = (h - (bottom + margin)) - (item_width / 2);
        int distance1X = (width * 48) / 480;
        int distance1Y = radiusY - 10;
        int distance2X = radiusX - 10;
        int distance3X = radiusX - 30;
        int distance3Y = (width * 30) / 480;
        this.distances = (int[][]) Array.newInstance(Integer.TYPE, this.count, 2);
        if (this.count == 3) {
            this.distances[0][0] = distance3X * -1;
            this.distances[0][1] = distance3Y * -1;
            this.distances[1][0] = (margin / 2) * -1;
            this.distances[1][1] = radiusY * -1;
            this.distances[2][0] = distance3X;
            this.distances[2][1] = distance3Y * -1;
        } else if (this.count == 4) {
            this.distances[0][0] = distance2X * -1;
            this.distances[0][1] = 0;
            this.distances[1][0] = distance1X * -1;
            this.distances[1][1] = distance1Y * -1;
            this.distances[2][0] = distance1X;
            this.distances[2][1] = distance1Y * -1;
            this.distances[3][0] = distance2X;
            this.distances[3][1] = 0;
        }
        initAnimation();
    }

    private void initAnimation() {
        this.set_ins = new AnimationSet[this.count];
        this.set_outs = new AnimationSet[this.count];
        for (int i = 0; i < this.count; i++) {
            AnimationSet set_out = new AnimationSet(false);
            set_out.addAnimation(animRotate(360.0f, 0.5f, 0.5f, true, this.rotate_offset));
            set_out.addAnimation(animTranslate((float) this.distances[i][0], (float) this.distances[i][1], this.distances[i][0] + startX, startY - this.distances[i][1], this.imageViews[i], i, (long) this.animation_duration[0], true));
            AnimationSet set_in = new AnimationSet(false);
            set_in.addAnimation(animRotate(360.0f, 0.5f, 0.5f, false, 0));
            set_in.addAnimation(animTranslate((float) (this.distances[i][0] * -1), (float) (this.distances[i][1] * -1), startX, startY, this.imageViews[i], i, (long) this.animation_duration[(this.count - i) - 1], false));
            this.set_ins[i] = set_in;
            this.set_outs[i] = set_out;
        }
    }

    /* access modifiers changed from: private */
    public void centerRotate(ImageView button, final int index) {
        if (!this.isloading) {
            if (this.isOpen.booleanValue()) {
                this.path_bg.startAnimation(setAnimScale(this.path_bg, 1.0f, 0.0f, 1.0f, 0.0f, false));
            } else {
                this.path_bg.startAnimation(setAnimScale(this.path_bg, 0.0f, 1.0f, 0.0f, 1.0f, true));
            }
            this.isOpen = false;
            this.isloading = true;
            for (int i = 0; i < this.count; i++) {
                this.imageViews[i].startAnimation(this.set_ins[i]);
            }
            if (button != null) {
                new Thread() {
                    public void run() {
                        try {
                            Thread.sleep(600);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        PathButton.this.handler.sendEmptyMessage(index);
                    }
                }.start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public Animation animRotate(float toDegrees, float pivotXValue, float pivotYValue, boolean isOut, int offset) {
        Animation animationRotate = new RotateAnimation(0.0f, toDegrees, 1, pivotXValue, 1, pivotYValue);
        animationRotate.setDuration((long) this.rotate_duration);
        animationRotate.setRepeatCount(3);
        if (isOut) {
            animationRotate.setStartOffset((long) offset);
        }
        animationRotate.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                animation.setFillAfter(true);
            }
        });
        return animationRotate;
    }

    /* access modifiers changed from: protected */
    public Animation animTranslate(float toX, float toY, int lastX, int lastY, ImageView button, int index, long durationMillis, boolean isOut) {
        Animation animationTranslate = new TranslateAnimation(0.0f, toX, 0.0f, toY);
        if (isOut) {
            animationTranslate.setInterpolator(new OvershootInterpolator(3.0f));
        } else {
            animationTranslate.setInterpolator(new AnticipateInterpolator(3.0f));
            animationTranslate.setStartOffset((long) this.traslate_offset);
        }
        final boolean z = isOut;
        final ImageView imageView = button;
        final int i = lastX;
        final int i2 = lastY;
        final int i3 = index;
        animationTranslate.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                if (z) {
                    imageView.setVisibility(0);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(PathButton.item_width, PathButton.item_width);
                params.setMargins(i, 0, 0, i2);
                params.addRule(12);
                imageView.setLayoutParams(params);
                imageView.clearAnimation();
                if (z && i3 == PathButton.this.count - 1) {
                    PathButton.this.handler.sendEmptyMessage(-1);
                }
                if (!z && i3 == PathButton.this.count - 1) {
                    PathButton.this.handler.sendEmptyMessage(-1);
                }
                if (!z) {
                    imageView.setVisibility(4);
                }
            }
        });
        animationTranslate.setDuration(durationMillis);
        return animationTranslate;
    }

    /* access modifiers changed from: private */
    public AnimationSet setAnimScale(ImageView button, float fromX, float toX, float fromY, float toY, boolean isOut) {
        AnimationSet animSet = new AnimationSet(false);
        AlphaAnimation alpha = new AlphaAnimation(fromX, toX);
        alpha.setDuration(300);
        alpha.setFillAfter(true);
        animSet.addAnimation(alpha);
        Animation animationScale = new ScaleAnimation(fromX, toX, fromY, toY, 1, 0.5f, 1, 0.45f);
        animationScale.setDuration(300);
        animationScale.setFillAfter(true);
        animSet.addAnimation(animationScale);
        animSet.setFillAfter(true);
        if (!isOut) {
            animSet.setStartOffset(100);
        }
        return animSet;
    }

    public void creadtCheckedId(onCheckedId onc) {
        this.oncheckedid = onc;
    }
}
