package eclipse.local.sdk;

import android.view.View;
import android.view.animation.Animation;
import eclipse.local.sdk.Util;

final class e implements Util.AnimationHelper.IHelper {
    e() {
    }

    public final void cancelAnimation(View view, Animation animation) {
        if (view != null) {
            view.setAnimation(null);
        }
    }
}
