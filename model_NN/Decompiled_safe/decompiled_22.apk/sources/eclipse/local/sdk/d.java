package eclipse.local.sdk;

import android.view.View;
import android.view.animation.Animation;
import eclipse.local.sdk.Util;

final class d implements Util.AnimationHelper.IHelper {
    d() {
    }

    public final void cancelAnimation(View view, Animation animation) {
        animation.cancel();
    }
}
