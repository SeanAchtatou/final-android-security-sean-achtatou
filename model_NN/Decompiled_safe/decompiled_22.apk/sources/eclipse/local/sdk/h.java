package eclipse.local.sdk;

import android.widget.ListView;
import eclipse.local.sdk.Util;

final class h implements Util.SmoothScrollFactory.IScroll {
    h() {
    }

    public final void doScroll(ListView listView) {
        if (listView.getFirstVisiblePosition() > 15) {
            listView.setSelection(15);
        }
        listView.smoothScrollToPosition(0);
    }
}
