package eclipse.local.sdk;

import android.media.ExifInterface;
import com.tencent.mm.sdk.platformtools.Log;
import java.io.IOException;

final class g {
    g() {
    }

    public static int a(String str) {
        ExifInterface exifInterface;
        int attributeInt;
        try {
            exifInterface = new ExifInterface(str);
        } catch (IOException e) {
            Log.e("MicroMsg.ExifHelper20Impl", "cannot read exif" + e);
            exifInterface = null;
        }
        if (exifInterface == null || (attributeInt = exifInterface.getAttributeInt("Orientation", -1)) == -1) {
            return 0;
        }
        switch (attributeInt) {
            case 3:
                return 180;
            case 4:
            case 5:
            case 7:
            default:
                return 0;
            case 6:
                return 90;
            case 8:
                return 270;
        }
    }
}
