package eclipse.local.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import eclipse.local.sdk.Util;
import java.io.InputStream;

final class c implements Util.BitmapFactory.Decode {
    c() {
    }

    public final Bitmap decodeFile(String str, float f) {
        return BitmapFactory.decodeFile(str);
    }

    public final Bitmap decodeStream(InputStream inputStream) {
        return BitmapFactory.decodeStream(inputStream);
    }

    public final Bitmap decodeStream(InputStream inputStream, float f) {
        return decodeStream(inputStream);
    }

    public final int fromDPToPix(Context context, float f) {
        return (int) f;
    }

    public final String getDisplayDensityType(Context context) {
        return "MDPI" + (context.getResources().getConfiguration().orientation == 2 ? "_L" : "_P");
    }
}
