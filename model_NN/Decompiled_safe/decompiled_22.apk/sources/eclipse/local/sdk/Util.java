package eclipse.local.sdk;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ListView;
import com.tencent.mm.sdk.platformtools.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Util {
    public static final int ANDROID_API_LEVEL_11 = 11;
    public static final int ANDROID_API_LEVEL_3 = 3;
    public static final int ANDROID_API_LEVEL_5 = 5;
    public static final int ANDROID_API_LEVEL_8 = 8;

    public static class AnimationHelper {

        public interface IHelper {
            void cancelAnimation(View view, Animation animation);
        }

        public static void cancelAnimation(View view, Animation animation) {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 8) {
                new d().cancelAnimation(view, animation);
            } else {
                new e().cancelAnimation(view, animation);
            }
        }
    }

    public static class BitmapFactory {
        private static final String TAG = "MicroMsg.BitmapFactory";

        public interface Decode {
            Bitmap decodeFile(String str, float f);

            Bitmap decodeStream(InputStream inputStream);

            Bitmap decodeStream(InputStream inputStream, float f);

            int fromDPToPix(Context context, float f);

            String getDisplayDensityType(Context context);
        }

        public static Bitmap decodeFile(String str, float f) {
            return Integer.valueOf(Build.VERSION.SDK).intValue() <= 3 ? new c().decodeFile(str, f) : new j().decodeFile(str, f);
        }

        public static Bitmap decodeStream(InputStream inputStream) {
            return Integer.valueOf(Build.VERSION.SDK).intValue() <= 3 ? new c().decodeStream(inputStream) : new j().decodeStream(inputStream);
        }

        public static Bitmap decodeStream(InputStream inputStream, float f) {
            return Integer.valueOf(Build.VERSION.SDK).intValue() <= 3 ? new c().decodeStream(inputStream, f) : new j().decodeStream(inputStream, f);
        }

        public static int fromDPToPix(Context context, float f) {
            return Integer.valueOf(Build.VERSION.SDK).intValue() <= 3 ? new c().fromDPToPix(context, f) : new j().fromDPToPix(context, f);
        }

        public static Bitmap getBitmapFromURL(String str) {
            try {
                Log.d(TAG, "get bitmap from url:" + str);
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                return decodeStream(httpURLConnection.getInputStream());
            } catch (IOException e) {
                Log.e(TAG, "get bitmap from url failed");
                e.printStackTrace();
                return null;
            }
        }

        public static String getDisplayDensityType(Context context) {
            return Integer.valueOf(Build.VERSION.SDK).intValue() <= 3 ? new c().getDisplayDensityType(context) : new j().getDisplayDensityType(context);
        }

        public static String getDisplaySizeType(Context context) {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            return "" + displayMetrics.heightPixels + "x" + displayMetrics.widthPixels;
        }
    }

    public static class ClipboardHelper {
        public static Intent getIntent(Context context) {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 11) {
                new b();
                ClipData primaryClip = ((ClipboardManager) context.getSystemService("clipboard")).getPrimaryClip();
                if (primaryClip == null || primaryClip.getItemCount() <= 0) {
                    return null;
                }
                ClipData.Item itemAt = primaryClip.getItemAt(0);
                if (itemAt == null) {
                    return null;
                }
                return itemAt.getIntent();
            }
            new a();
            return null;
        }

        public static CharSequence getText(Context context) {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 11) {
                new b();
                ClipData primaryClip = ((ClipboardManager) context.getSystemService("clipboard")).getPrimaryClip();
                if (primaryClip == null || primaryClip.getItemCount() <= 0) {
                    return null;
                }
                ClipData.Item itemAt = primaryClip.getItemAt(0);
                if (itemAt == null) {
                    return null;
                }
                return itemAt.getText();
            }
            new a();
            return ((android.text.ClipboardManager) context.getSystemService("clipboard")).getText();
        }

        public static Uri getUri(Context context) {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 11) {
                new b();
                ClipData primaryClip = ((ClipboardManager) context.getSystemService("clipboard")).getPrimaryClip();
                if (primaryClip == null || primaryClip.getItemCount() <= 0) {
                    return null;
                }
                ClipData.Item itemAt = primaryClip.getItemAt(0);
                if (itemAt == null) {
                    return null;
                }
                return itemAt.getUri();
            }
            new a();
            return null;
        }

        public static void setIntent(Context context, CharSequence charSequence, Intent intent) {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 11) {
                new b();
                ((ClipboardManager) context.getSystemService("clipboard")).setPrimaryClip(ClipData.newIntent(charSequence, intent));
                return;
            }
            new a();
        }

        public static void setText(Context context, CharSequence charSequence, CharSequence charSequence2) {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 11) {
                new b();
                ((ClipboardManager) context.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(charSequence, charSequence2));
                return;
            }
            new a();
            ((android.text.ClipboardManager) context.getSystemService("clipboard")).setText(charSequence2);
        }

        public static void setUri(Context context, CharSequence charSequence, Uri uri) {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 11) {
                new b();
                ((ClipboardManager) context.getSystemService("clipboard")).setPrimaryClip(ClipData.newRawUri(charSequence, uri));
                return;
            }
            new a();
        }
    }

    public static class ContactContentHelper {
        public static String getContactInsertPhone() {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 5) {
                new i();
                return "phone";
            }
            new f();
            return "phone";
        }

        public static String getContactType() {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 5) {
                new i();
                return "vnd.android.cursor.dir/contact";
            }
            new f();
            return "vnd.android.cursor.dir/contact-methods";
        }

        public static Uri getContactUri() {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 5) {
                new i();
                return ContactsContract.Contacts.CONTENT_URI;
            }
            new f();
            return Uri.parse("content://contacts/contact_methods");
        }
    }

    public static class ExifHelper {
        public static int getExifOrientation(String str) {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() < 5) {
                return 0;
            }
            new g();
            return g.a(str);
        }
    }

    public static class SmoothScrollFactory {

        public interface IScroll {
            void doScroll(ListView listView);
        }

        public static void scrollToTop(ListView listView) {
            if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 8) {
                new h().doScroll(listView);
            } else {
                new k().doScroll(listView);
            }
        }
    }
}
