package eclipse.local.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import eclipse.local.sdk.Util;
import java.io.InputStream;

final class j implements Util.BitmapFactory.Decode {
    j() {
    }

    public final Bitmap decodeFile(String str, float f) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        float f2 = 160.0f * f;
        options.inDensity = (int) f2;
        Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
        if (decodeFile != null) {
            decodeFile.setDensity((int) f2);
        }
        return decodeFile;
    }

    public final Bitmap decodeStream(InputStream inputStream) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDensity = 160;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeStream(inputStream, null, options);
    }

    public final Bitmap decodeStream(InputStream inputStream, float f) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDensity = (int) (160.0f * f);
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeStream(inputStream, null, options);
    }

    public final int fromDPToPix(Context context, float f) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return (int) ((((float) displayMetrics.densityDpi) * f) / 160.0f);
    }

    public final String getDisplayDensityType(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (displayMetrics.density < 1.0f ? "" + "LDPI" : displayMetrics.density >= 1.5f ? "" + "HDPI" : "" + "MDPI") + (context.getResources().getConfiguration().orientation == 2 ? "_L" : "_P");
    }
}
