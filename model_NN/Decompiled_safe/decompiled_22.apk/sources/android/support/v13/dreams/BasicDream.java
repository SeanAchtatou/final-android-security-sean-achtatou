package android.support.v13.dreams;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class BasicDream extends Activity {
    private static final boolean DEBUG = true;
    private static final String TAG = "BasicDream";
    /* access modifiers changed from: private */
    public boolean mPlugged = false;
    private final BroadcastReceiver mPowerIntentReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean plugged = true;
            if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                if (1 != intent.getIntExtra("plugged", 0)) {
                    plugged = false;
                }
                if (plugged != BasicDream.this.mPlugged) {
                    Log.d(BasicDream.TAG, "now " + (plugged ? "plugged in" : "unplugged"));
                    boolean unused = BasicDream.this.mPlugged = plugged;
                    if (BasicDream.this.mPlugged) {
                        BasicDream.this.getWindow().addFlags(128);
                    } else {
                        BasicDream.this.getWindow().clearFlags(128);
                    }
                }
            }
        }
    };
    private View mView;

    class BasicDreamView extends View {
        public BasicDreamView(Context c) {
            super(c);
        }

        public BasicDreamView(Context c, AttributeSet at) {
            super(c, at);
        }

        public void onAttachedToWindow() {
            setSystemUiVisibility(1);
        }

        public void onDraw(Canvas c) {
            BasicDream.this.onDraw(c);
        }
    }

    public void onStart() {
        super.onStart();
        setContentView(new BasicDreamView(this));
        getWindow().addFlags(524289);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.BATTERY_CHANGED");
        registerReceiver(this.mPowerIntentReceiver, filter);
    }

    public void onPause() {
        super.onPause();
        Log.d(TAG, "exiting onPause");
        finish();
    }

    public void onStop() {
        super.onStop();
        unregisterReceiver(this.mPowerIntentReceiver);
    }

    /* access modifiers changed from: protected */
    public View getContentView() {
        return this.mView;
    }

    public void setContentView(View v) {
        super.setContentView(v);
        this.mView = v;
    }

    /* access modifiers changed from: protected */
    public void invalidate() {
        getContentView().invalidate();
    }

    public void onDraw(Canvas c) {
    }

    public void onUserInteraction() {
        Log.d(TAG, "exiting onUserInteraction");
        finish();
    }
}
