package com.Ninjya;

import android.graphics.Rect;

class Monster4 extends CFall {
    protected int anime = 0;
    protected int baku_time = 0;
    protected int critical = 0;
    public int h = 80;
    public int w = 70;

    public Monster4(MoguraView pView, int nSpd, int nY) {
        super(pView, nSpd, nY);
    }

    public boolean hitTest(int tx, int ty) {
        return this.m_nX - 25 <= tx && tx < this.m_nX + this.w && this.m_nY - 10 <= ty && ty < this.m_nY + this.h;
    }

    public boolean rakkatest(int zy) {
        return zy < this.m_nY;
    }

    public Rect getRect() {
        return new Rect(this.m_nX, this.m_nY, this.m_nX + this.w, this.m_nY + this.h);
    }
}
