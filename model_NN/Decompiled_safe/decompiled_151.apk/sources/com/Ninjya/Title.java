package com.Ninjya;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

public class Title extends BaseActivity {
    private MediaPlayer _mediaPlayer;
    private boolean flg_vibrator;
    private Vibrator vibrator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.title);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        this._mediaPlayer = null;
        playSound();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return false;
        }
        startActivity(new Intent(this, Ninjya.class));
        stopSound();
        finish();
        return false;
    }

    public void onClickGamestart(View v) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
    }

    public void onClickHiscoreGet(View v) {
        startActivity(new Intent(this, Rank.class));
        stopSound();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this._mediaPlayer != null && this._mediaPlayer.isPlaying()) {
            this._mediaPlayer.pause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this._mediaPlayer != null) {
            this._mediaPlayer.start();
        } else if (this._mediaPlayer == null) {
            playSound();
        }
    }

    private void stopSound() {
        if (this._mediaPlayer != null) {
            if (this._mediaPlayer.isPlaying()) {
                this._mediaPlayer.stop();
            }
            this._mediaPlayer.release();
            this._mediaPlayer = null;
        }
    }

    private void playSound() {
        stopSound();
        this._mediaPlayer = MediaPlayer.create(this, (int) R.raw.jupiter);
        this._mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                mp.seekTo(0);
                mp.start();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        if (this.flg_vibrator) {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(Off)");
        } else {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(On)");
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public synchronized boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean result;
        result = super.onMenuItemSelected(featureId, item);
        if (item.getItemId() == R.id.menu_vib_switch) {
            this.flg_vibrator = !this.flg_vibrator;
            SharedPreferences.Editor editor = getSharedPreferences("prefkey", 0).edit();
            editor.putBoolean("vibflg", this.flg_vibrator);
            editor.commit();
            if (this.flg_vibrator) {
                this.vibrator.vibrate(100);
            }
        } else if (item.getItemId() == R.id.menu_apk) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
        } else if (item.getItemId() == R.id.menu_hp) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box/")));
        } else if (item.getItemId() == R.id.menu_end) {
            finish();
        }
        return result;
    }
}
