package com.Ninjya;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;

public class MoguraView extends View {
    private int DEF_HEIGHT;
    private int DEF_WIDTH;
    int MSCNT;
    private int Touch_flg;
    public MediaPlayer _mediaPlayer;
    private int a;
    private Rect ad_dst;
    private Bitmap ad_line;
    private Rect ad_src;
    int[] attack;
    private Rect back_src;
    private Bitmap bmpBakuhatu;
    private Bitmap bmpCritical;
    private Bitmap bmpHPmeta1;
    private Bitmap bmpHPmeta2;
    private Bitmap bmpMogura;
    private Bitmap bmpMogura2;
    private Bitmap bmpMogura3;
    private Bitmap bmpMogura3_baku;
    private Bitmap bmpMogura4;
    private Bitmap bmpMogura5;
    private Bitmap bmpMoguraHit;
    private Bitmap bmpMogura_kesi;
    private Bitmap bmpSora;
    private Bitmap bmpSora2;
    private Bitmap bmpSora3;
    private Bitmap bmpSora4;
    private Bitmap bmpTitle_haikei;
    private Bitmap bmpZimen2;
    private int combo;
    private Boolean combo_100;
    private Boolean combo_1000;
    private Boolean combo_200;
    private Boolean combo_300;
    private Boolean combo_400;
    private Boolean combo_500;
    private Boolean combo_600;
    private Boolean combo_700;
    private Boolean combo_800;
    private Boolean combo_900;
    private int combo_bonas;
    private Boolean combo_bonas_flg;
    private int combo_time;
    public int dispX;
    public int dispY;
    private boolean flg_vibrator;
    private int frame;
    private int frame_count;
    private int frame_count2;
    private int frame_count3;
    private int frame_count4;
    private int frame_count5;
    private Rect game_rect;
    private int haikei_time;
    RedrawHandler handler;
    private Boolean handler_flg;
    private int hp;
    private int hp_a;
    private int hp_b;
    private int hp_c;
    private boolean init_flg;
    public int metaX;
    private int meta_h;
    private int meta_w;
    private int monsterSpeed;
    private int monsterSpeed2;
    private int monsterSpeed3;
    private int monsterSpeed4;
    private int monsterSpeed5;
    private ArrayList<Monster> monsters;
    private ArrayList<Monster2> monsters2;
    private ArrayList<Monster3> monsters3;
    private ArrayList<Monster4> monsters4;
    private ArrayList<Monster5> monsters5;
    private OnSettingListener onSettingListener;
    private float re_size_height;
    private float re_size_width;
    private int score;
    SoundPool soundPool;
    private Vibrator vibrator;

    public interface OnSettingListener {
        void onSetting();
    }

    public MoguraView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.dispX = 480;
        this.dispY = 854;
        this.combo = 0;
        this.combo_bonas = 0;
        this.combo_time = 0;
        this.metaX = 1;
        this.handler_flg = false;
        this.combo_bonas_flg = false;
        this.init_flg = false;
        this.frame = 0;
        this.score = 0;
        this.hp = 100;
        this.hp_a = 0;
        this.hp_b = 0;
        this.hp_c = 0;
        this.meta_w = 0;
        this.meta_h = 0;
        this.haikei_time = 0;
        this.monsterSpeed = 1;
        this.monsterSpeed2 = 5;
        this.monsterSpeed3 = 2;
        this.monsterSpeed4 = 6;
        this.monsterSpeed5 = 3;
        this.frame_count = 35;
        this.frame_count2 = 50;
        this.frame_count3 = 250;
        this.frame_count4 = 100;
        this.frame_count5 = 180;
        this.combo_100 = false;
        this.combo_200 = false;
        this.combo_300 = false;
        this.combo_400 = false;
        this.combo_500 = false;
        this.combo_600 = false;
        this.combo_700 = false;
        this.combo_800 = false;
        this.combo_900 = false;
        this.combo_1000 = false;
        this.MSCNT = 5;
        this.attack = new int[this.MSCNT];
        this.Touch_flg = 0;
        this.a = 0;
        this.DEF_WIDTH = 480;
        this.DEF_HEIGHT = 800;
    }

    public MoguraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.dispX = 480;
        this.dispY = 854;
        this.combo = 0;
        this.combo_bonas = 0;
        this.combo_time = 0;
        this.metaX = 1;
        this.handler_flg = false;
        this.combo_bonas_flg = false;
        this.init_flg = false;
        this.frame = 0;
        this.score = 0;
        this.hp = 100;
        this.hp_a = 0;
        this.hp_b = 0;
        this.hp_c = 0;
        this.meta_w = 0;
        this.meta_h = 0;
        this.haikei_time = 0;
        this.monsterSpeed = 1;
        this.monsterSpeed2 = 5;
        this.monsterSpeed3 = 2;
        this.monsterSpeed4 = 6;
        this.monsterSpeed5 = 3;
        this.frame_count = 35;
        this.frame_count2 = 50;
        this.frame_count3 = 250;
        this.frame_count4 = 100;
        this.frame_count5 = 180;
        this.combo_100 = false;
        this.combo_200 = false;
        this.combo_300 = false;
        this.combo_400 = false;
        this.combo_500 = false;
        this.combo_600 = false;
        this.combo_700 = false;
        this.combo_800 = false;
        this.combo_900 = false;
        this.combo_1000 = false;
        this.MSCNT = 5;
        this.attack = new int[this.MSCNT];
        this.Touch_flg = 0;
        this.a = 0;
        this.DEF_WIDTH = 480;
        this.DEF_HEIGHT = 800;
        this.vibrator = (Vibrator) getContext().getSystemService("vibrator");
        Resources r = context.getResources();
        this.monsters = new ArrayList<>();
        this.monsters2 = new ArrayList<>();
        this.monsters3 = new ArrayList<>();
        this.monsters4 = new ArrayList<>();
        this.monsters5 = new ArrayList<>();
        this.handler = new RedrawHandler(this, 30);
        this.handler.start();
        this.bmpMogura = BitmapFactory.decodeResource(r, R.drawable.star);
        this.bmpMogura2 = BitmapFactory.decodeResource(r, R.drawable.star_red);
        this.bmpMogura3 = BitmapFactory.decodeResource(r, R.drawable.star3);
        this.bmpMogura4 = BitmapFactory.decodeResource(r, R.drawable.star4);
        this.bmpMogura5 = BitmapFactory.decodeResource(r, R.drawable.star5);
        this.bmpMoguraHit = BitmapFactory.decodeResource(r, R.drawable.star2);
        this.bmpMogura_kesi = BitmapFactory.decodeResource(r, R.drawable.star3_kesi);
        this.bmpMogura3_baku = BitmapFactory.decodeResource(r, R.drawable.star3_baku);
        this.bmpBakuhatu = BitmapFactory.decodeResource(r, R.drawable.bakuhatu);
        this.bmpCritical = BitmapFactory.decodeResource(r, R.drawable.critical);
        this.bmpZimen2 = BitmapFactory.decodeResource(r, R.drawable.zimen2);
        this.bmpSora = BitmapFactory.decodeResource(r, R.drawable.haikei4);
        this.bmpSora2 = BitmapFactory.decodeResource(r, R.drawable.haikei1);
        this.bmpSora3 = BitmapFactory.decodeResource(r, R.drawable.haikei2);
        this.bmpSora4 = BitmapFactory.decodeResource(r, R.drawable.haikei3);
        this.bmpHPmeta1 = BitmapFactory.decodeResource(r, R.drawable.hp_meta1);
        this.bmpHPmeta2 = BitmapFactory.decodeResource(r, R.drawable.hp_meta2);
        this.ad_line = BitmapFactory.decodeResource(context.getResources(), R.drawable.ad_back);
        this.meta_w = this.bmpHPmeta2.getWidth();
        this.meta_h = this.bmpHPmeta2.getHeight();
        this.hp_a = this.meta_w / 10;
        this.hp_a += this.metaX;
        this.hp_b = this.hp_a * 3;
        this.hp_c = this.hp_a * 2;
        this.soundPool = new SoundPool(this.MSCNT, 3, 0);
        this.attack[0] = this.soundPool.load(context, R.raw.yarare, 1);
        this.attack[1] = this.soundPool.load(context, R.raw.baku, 1);
        this.attack[2] = this.soundPool.load(context, R.raw.bakuha, 1);
        this.attack[3] = this.soundPool.load(context, R.raw.kumo_se, 1);
        this.attack[4] = this.soundPool.load(context, R.raw.tawara_se, 1);
        this.flg_vibrator = context.getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        playSound();
    }

    public MoguraView(Context c) {
        super(c);
        this.dispX = 480;
        this.dispY = 854;
        this.combo = 0;
        this.combo_bonas = 0;
        this.combo_time = 0;
        this.metaX = 1;
        this.handler_flg = false;
        this.combo_bonas_flg = false;
        this.init_flg = false;
        this.frame = 0;
        this.score = 0;
        this.hp = 100;
        this.hp_a = 0;
        this.hp_b = 0;
        this.hp_c = 0;
        this.meta_w = 0;
        this.meta_h = 0;
        this.haikei_time = 0;
        this.monsterSpeed = 1;
        this.monsterSpeed2 = 5;
        this.monsterSpeed3 = 2;
        this.monsterSpeed4 = 6;
        this.monsterSpeed5 = 3;
        this.frame_count = 35;
        this.frame_count2 = 50;
        this.frame_count3 = 250;
        this.frame_count4 = 100;
        this.frame_count5 = 180;
        this.combo_100 = false;
        this.combo_200 = false;
        this.combo_300 = false;
        this.combo_400 = false;
        this.combo_500 = false;
        this.combo_600 = false;
        this.combo_700 = false;
        this.combo_800 = false;
        this.combo_900 = false;
        this.combo_1000 = false;
        this.MSCNT = 5;
        this.attack = new int[this.MSCNT];
        this.Touch_flg = 0;
        this.a = 0;
        this.DEF_WIDTH = 480;
        this.DEF_HEIGHT = 800;
        setFocusable(true);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.dispX = w - 60;
        this.dispY = h;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (!this.init_flg) {
            Rect rect = new Rect(getLeft(), getTop(), getRight(), getBottom());
            if (this.re_size_width >= ((float) rect.right)) {
                this.re_size_width = ((float) this.DEF_WIDTH) / ((float) rect.right);
            } else {
                this.re_size_width = ((float) rect.right) / ((float) this.DEF_WIDTH);
            }
            if (this.re_size_height >= ((float) rect.bottom)) {
                this.re_size_height = ((float) this.DEF_HEIGHT) / ((float) rect.bottom);
            } else {
                this.re_size_height = ((float) rect.bottom) / ((float) this.DEF_HEIGHT);
            }
            this.ad_src = new Rect(0, 0, this.ad_line.getWidth(), this.ad_line.getHeight());
            this.ad_dst = new Rect(getLeft(), getTop(), getRight(), getTop() + ((int) (((float) this.ad_line.getHeight()) * this.re_size_height)));
            this.game_rect = new Rect(getLeft(), getTop() + ((int) (((float) this.ad_line.getHeight()) * this.re_size_height)), getRight(), getBottom());
            this.back_src = new Rect(0, 0, this.bmpSora.getWidth(), this.bmpSora.getHeight());
            this.init_flg = true;
        }
        if (!this.handler_flg.booleanValue() && this.hp <= 0) {
            this.handler.stop();
            this.frame = 0;
            this.monsters.clear();
            this.monsters2.clear();
            this.monsters3.clear();
            this.monsters4.clear();
            this.monsters5.clear();
            stopSound();
            canvas.drawBitmap(this.bmpSora2, this.back_src, this.game_rect, (Paint) null);
            OnSettingEvent();
            this.handler_flg = 1;
        }
        this.haikei_time = this.haikei_time + 1;
        if (this.haikei_time <= 500) {
            canvas.drawBitmap(this.bmpSora2, this.back_src, this.game_rect, (Paint) null);
        } else if (this.haikei_time > 500 && this.haikei_time <= 1000) {
            canvas.drawBitmap(this.bmpSora3, this.back_src, this.game_rect, (Paint) null);
        } else if (this.haikei_time > 1000 && this.haikei_time <= 1500) {
            canvas.drawBitmap(this.bmpSora4, this.back_src, this.game_rect, (Paint) null);
        } else if (this.haikei_time <= 1500 || this.haikei_time > 2000) {
            this.haikei_time = 0;
        } else {
            canvas.drawBitmap(this.bmpSora, this.back_src, this.game_rect, (Paint) null);
        }
        if (this.hp <= 50) {
            canvas.drawBitmap(this.bmpZimen2, 0.0f, (float) ((int) (680.0f * this.re_size_height)), (Paint) null);
        }
        canvas.drawBitmap(this.bmpHPmeta1, 0.0f, (float) this.ad_dst.bottom, (Paint) null);
        canvas.drawBitmap(this.bmpHPmeta2, new Rect(0, 0, this.meta_w, this.meta_h), new Rect((int) (31.0f * this.re_size_width), this.ad_dst.bottom, (this.meta_w + ((int) (31.0f * this.re_size_width))) - this.a, this.ad_dst.bottom + this.meta_h), (Paint) null);
        Paint paint2 = new Paint();
        paint2.setColor(-1);
        paint2.setTextSize(26.0f);
        canvas.drawText("SCORE", (float) ((int) (330.0f * this.re_size_width)), (float) (this.ad_dst.bottom + ((int) (40.0f * this.re_size_height))), paint2);
        canvas.drawText(new StringBuilder(String.valueOf(this.score)).toString(), (float) ((int) (340.0f * this.re_size_width)), (float) (this.ad_dst.bottom + ((int) (75.0f * this.re_size_height))), paint2);
        Paint combo_paint = new Paint();
        combo_paint.setColor(-1);
        combo_paint.setTextSize(26.0f);
        canvas.drawText("COMBO", (float) ((int) (10.0f * this.re_size_width)), (float) (this.ad_dst.bottom + ((int) (100.0f * this.re_size_height))), combo_paint);
        canvas.drawText(new StringBuilder(String.valueOf(this.combo)).toString(), (float) ((int) (30.0f * this.re_size_width)), (float) (this.ad_dst.bottom + ((int) (140.0f * this.re_size_height))), combo_paint);
        canvas.drawBitmap(this.ad_line, this.ad_src, this.ad_dst, (Paint) null);
        if (this.combo_bonas_flg.booleanValue()) {
            Paint combo_bonas_paint = new Paint();
            combo_bonas_paint.setColor(-1);
            combo_bonas_paint.setTextSize(24.0f);
            canvas.drawText("COMBO BONUS!!", (float) ((int) (10.0f * this.re_size_width)), (float) (this.ad_dst.bottom + ((int) (170.0f * this.re_size_height))), combo_bonas_paint);
            canvas.drawText("SCORE+" + this.combo_bonas, (float) ((int) (30.0f * this.re_size_width)), (float) (this.ad_dst.bottom + ((int) (200.0f * this.re_size_height))), combo_bonas_paint);
            this.combo_time = this.combo_time + 1;
            if (this.combo_time >= 80) {
                this.combo_time = 0;
                this.combo_bonas_flg = null;
            }
        }
        if (this.combo >= 100 && !this.combo_100.booleanValue()) {
            this.combo_bonas = 100;
            this.score = this.score + this.combo_bonas;
            this.combo_100 = 1;
            this.combo_bonas_flg = 1;
        } else if (this.combo >= 200 && !this.combo_200.booleanValue()) {
            this.combo_bonas = 300;
            this.score = this.score + this.combo_bonas;
            this.combo_200 = 1;
            this.combo_bonas_flg = 1;
        } else if (this.combo >= 300 && !this.combo_300.booleanValue()) {
            this.combo_bonas = 500;
            this.score = this.score + this.combo_bonas;
            this.combo_300 = 1;
            this.combo_bonas_flg = 1;
        } else if (this.combo >= 400 && !this.combo_400.booleanValue()) {
            this.combo_bonas = 800;
            this.score = this.score + this.combo_bonas;
            this.combo_400 = 1;
            this.combo_bonas_flg = 1;
        } else if (this.combo >= 500 && !this.combo_500.booleanValue()) {
            this.combo_bonas = 1000;
            this.score = this.score + this.combo_bonas;
            this.combo_500 = 1;
            this.combo_bonas_flg = 1;
        } else if (this.combo >= 600 && !this.combo_600.booleanValue()) {
            this.combo_bonas = 1200;
            this.score = this.score + this.combo_bonas;
            this.combo_600 = 1;
            this.combo_bonas_flg = 1;
        } else if (this.combo >= 700 && !this.combo_700.booleanValue()) {
            this.combo_bonas = 1500;
            this.score = this.score + this.combo_bonas;
            this.combo_700 = 1;
            this.combo_bonas_flg = 1;
        } else if (this.combo >= 800 && !this.combo_800.booleanValue()) {
            this.combo_bonas = 1800;
            this.score = this.score + this.combo_bonas;
            this.combo_800 = 1;
            this.combo_bonas_flg = 1;
        } else if (this.combo >= 900 && !this.combo_900.booleanValue()) {
            this.combo_bonas = 2000;
            this.score = this.score + this.combo_bonas;
            this.combo_900 = 1;
            this.combo_bonas_flg = 1;
        } else if (this.combo >= 1000 && !this.combo_1000.booleanValue()) {
            this.combo_bonas = 3000;
            this.score = this.score + this.combo_bonas;
            this.combo_1000 = 1;
            this.combo_bonas_flg = 1;
        }
        if (this.combo < 10) {
            this.combo_100 = null;
            this.combo_200 = null;
            this.combo_300 = null;
            this.combo_400 = null;
            this.combo_500 = null;
            this.combo_600 = null;
            this.combo_700 = null;
            this.combo_800 = null;
            this.combo_900 = null;
            this.combo_1000 = null;
        }
        int i = 0;
        while (i < this.monsters.size()) {
            Monster p = this.monsters.get(i);
            if (((float) p.GetY()) > 638.0f * this.re_size_height && p.baku_time == 0) {
                this.hp = this.hp - 10;
                this.a = this.a + this.hp_a;
                this.soundPool.play(this.attack[1], 1.0f, 1.0f, 0, 0, 1.0f);
                p.baku_time++;
                this.combo = 0;
                if (this.flg_vibrator) {
                    this.vibrator.vibrate(100);
                }
            }
            if (p.baku_time > 0) {
                p.baku_time++;
                if (p.baku_time > 10) {
                    this.monsters.remove(i);
                } else {
                    canvas.drawBitmap(this.bmpBakuhatu, (float) p.GetX(), (float) p.GetY(), (Paint) null);
                }
            }
            if (p.critical >= 2) {
                canvas.drawBitmap(this.bmpCritical, (float) (p.GetX() - 9), (float) (p.GetY() + 7), (Paint) null);
            }
            if (p.anime > 0) {
                p.anime++;
                if (p.anime <= 2) {
                    this.combo = this.combo + 1;
                    this.soundPool.play(this.attack[0], 1.0f, 1.0f, 0, 0, 1.0f);
                    canvas.drawBitmap(this.bmpMoguraHit, (float) p.GetX(), (float) p.GetY(), (Paint) null);
                } else if (p.anime > 10) {
                    this.monsters.remove(i);
                }
            } else if (p.baku_time == 0) {
                p.move();
                p.Draw(canvas, this.bmpMogura);
            }
            i++;
        }
        if (i > 30) {
        }
        int k = 0;
        while (k < this.monsters2.size()) {
            Monster2 s = this.monsters2.get(k);
            if (((float) s.GetY()) > 638.0f * this.re_size_height && s.baku_time == 0) {
                this.hp = this.hp - 10;
                this.a = this.a + this.hp_a;
                this.soundPool.play(this.attack[1], 1.0f, 1.0f, 0, 0, 1.0f);
                s.baku_time++;
                this.combo = 0;
                if (this.flg_vibrator) {
                    this.vibrator.vibrate(100);
                }
            }
            if (s.baku_time > 0) {
                s.baku_time++;
                if (s.baku_time > 10) {
                    this.monsters2.remove(k);
                } else {
                    canvas.drawBitmap(this.bmpBakuhatu, (float) s.GetX(), (float) s.GetY(), (Paint) null);
                }
            }
            if (s.critical >= 2) {
                canvas.drawBitmap(this.bmpCritical, (float) (s.GetX() - 9), (float) (s.GetY() + 7), (Paint) null);
            }
            if (s.anime > 0) {
                s.anime++;
                if (s.anime <= 2) {
                    this.combo = this.combo + 1;
                    this.soundPool.play(this.attack[0], 1.0f, 1.0f, 0, 0, 1.0f);
                    canvas.drawBitmap(this.bmpMoguraHit, (float) s.GetX(), (float) s.GetY(), (Paint) null);
                } else if (s.anime > 10) {
                    this.monsters2.remove(k);
                }
            } else if (s.baku_time == 0) {
                s.move();
                s.Draw(canvas, this.bmpMogura2);
            }
            k++;
        }
        if (k > 30) {
        }
        int tt = 0;
        while (tt < this.monsters3.size()) {
            Monster3 ss = this.monsters3.get(tt);
            if (((float) ss.GetY()) > 610.0f * this.re_size_height && ss.baku_time == 0) {
                this.hp = this.hp - 30;
                this.a = this.a + this.hp_b;
                this.soundPool.play(this.attack[2], 1.0f, 1.0f, 0, 0, 1.0f);
                ss.baku_time++;
                this.combo = 0;
                if (this.flg_vibrator) {
                    this.vibrator.vibrate(100);
                }
            }
            if (ss.baku_time > 0) {
                ss.baku_time++;
                if (ss.baku_time > 10) {
                    this.monsters3.remove(tt);
                } else {
                    canvas.drawBitmap(this.bmpMogura3_baku, (float) ss.GetX(), (float) ss.GetY(), (Paint) null);
                }
            }
            if (ss.critical >= 2) {
                canvas.drawBitmap(this.bmpCritical, (float) (ss.GetX() - 9), (float) (ss.GetY() + 13), (Paint) null);
            }
            if (ss.anime > 0) {
                ss.anime++;
                if (ss.anime <= 2) {
                    this.soundPool.play(this.attack[4], 1.0f, 1.0f, 0, 0, 1.0f);
                    canvas.drawBitmap(this.bmpMogura_kesi, (float) ss.GetX(), (float) ss.GetY(), (Paint) null);
                }
                if (ss.anime > 10) {
                    this.monsters3.remove(tt);
                }
            } else if (ss.baku_time == 0) {
                ss.move();
                ss.Draw(canvas, this.bmpMogura3);
            }
            tt++;
        }
        if (tt > 30) {
        }
        int kk = 0;
        while (kk < this.monsters4.size()) {
            Monster4 q = this.monsters4.get(kk);
            if (((float) q.GetY()) > 630.0f * this.re_size_height && q.baku_time == 0) {
                this.hp = this.hp - 10;
                this.a = this.a + this.hp_a;
                this.soundPool.play(this.attack[1], 1.0f, 1.0f, 0, 0, 1.0f);
                q.baku_time++;
                this.combo = 0;
                if (this.flg_vibrator) {
                    this.vibrator.vibrate(100);
                }
            }
            if (q.baku_time > 0) {
                q.baku_time++;
                if (q.baku_time > 10) {
                    this.monsters4.remove(kk);
                } else {
                    canvas.drawBitmap(this.bmpBakuhatu, (float) (q.GetX() - 6), (float) (q.GetY() - 10), (Paint) null);
                }
            }
            if (q.critical >= 2) {
                canvas.drawBitmap(this.bmpCritical, (float) (q.GetX() - 9), (float) (q.GetY() + 7), (Paint) null);
            }
            if (q.anime > 0) {
                q.anime++;
                if (q.anime <= 2) {
                    this.soundPool.play(this.attack[0], 1.0f, 1.0f, 0, 0, 1.0f);
                    canvas.drawBitmap(this.bmpMoguraHit, (float) q.GetX(), (float) q.GetY(), (Paint) null);
                } else if (q.anime > 10) {
                    this.monsters4.remove(kk);
                }
            } else if (q.baku_time == 0) {
                q.move();
                q.Draw(canvas, this.bmpMogura4);
            }
            kk++;
        }
        if (kk > 30) {
        }
        int kkk = 0;
        while (kkk < this.monsters5.size()) {
            Monster5 qq = this.monsters5.get(kkk);
            if (((float) qq.GetY()) > 630.0f * this.re_size_height && qq.baku_time == 0) {
                this.hp = this.hp - 20;
                this.a = this.a + this.hp_c;
                this.soundPool.play(this.attack[1], 1.0f, 1.0f, 0, 0, 1.0f);
                qq.baku_time++;
            }
            if (qq.baku_time > 0) {
                qq.baku_time++;
                if (qq.baku_time > 10) {
                    this.monsters5.remove(kkk);
                } else {
                    canvas.drawBitmap(this.bmpBakuhatu, (float) (qq.GetX() - 6), (float) (qq.GetY() - 10), (Paint) null);
                }
            }
            if (qq.critical >= 2) {
                canvas.drawBitmap(this.bmpCritical, (float) (qq.GetX() - 9), (float) (qq.GetY() + 7), (Paint) null);
            }
            if (qq.anime > 0) {
                qq.anime++;
                if (qq.anime <= 2) {
                    this.soundPool.play(this.attack[3], 1.0f, 1.0f, 0, 0, 1.0f);
                    canvas.drawBitmap(this.bmpMoguraHit, (float) qq.GetX(), (float) qq.GetY(), (Paint) null);
                } else if (qq.anime > 10) {
                    this.monsters5.remove(kkk);
                }
            } else if (qq.baku_time == 0) {
                qq.move();
                qq.Draw(canvas, this.bmpMogura5);
            }
            kkk++;
        }
        if (kkk > 30) {
        }
        if (this.frame == 500) {
            this.frame_count = this.frame_count - 1;
            this.monsterSpeed = this.monsterSpeed + 1;
            this.monsterSpeed2 = this.monsterSpeed2 + 1;
            this.monsterSpeed3 = this.monsterSpeed3 + 1;
            this.monsterSpeed4 = this.monsterSpeed4 + 1;
            this.monsterSpeed5 = this.monsterSpeed5 + 1;
            this.frame = 0;
        }
        if (this.frame % this.frame_count == 0) {
            this.monsters.add(new Monster(this, this.monsterSpeed, this.ad_dst.bottom));
        }
        if (this.score > 50 && this.frame % this.frame_count2 == 0) {
            this.monsters2.add(new Monster2(this, this.monsterSpeed2, this.ad_dst.bottom));
        }
        if (this.score > 300 && this.frame % this.frame_count3 == 0) {
            this.monsters3.add(new Monster3(this, this.monsterSpeed3, this.ad_dst.bottom));
        }
        if (this.score > 500 && this.frame % this.frame_count4 == 0) {
            this.monsters4.add(new Monster4(this, this.monsterSpeed4, this.ad_dst.bottom));
        }
        if (this.score > 1000 && this.frame % this.frame_count5 == 0) {
            this.monsters5.add(new Monster5(this, this.monsterSpeed5, this.ad_dst.bottom));
        }
        this.frame = this.frame + 1;
    }

    public boolean onTouchEvent(MotionEvent event) {
        int tx = (int) event.getX();
        int ty = (int) event.getY();
        if (event.getAction() == 0) {
            int i = 0;
            if (this.Touch_flg == 0) {
                while (i < this.monsters.size()) {
                    Monster p = this.monsters.get(i);
                    if (p.hitTest(tx, ty)) {
                        p.anime++;
                        if (p.critical <= 1) {
                            this.score = this.score + 1;
                            p.critical++;
                        }
                    }
                    i++;
                }
                if (i > 30) {
                }
                int k = 0;
                while (k < this.monsters2.size()) {
                    Monster2 s = this.monsters2.get(k);
                    if (s.hitTest(tx, ty)) {
                        s.anime++;
                        if (s.critical <= 1) {
                            this.score = this.score + 10;
                            s.critical++;
                        }
                    }
                    k++;
                }
                if (k > 30) {
                }
                int tt = 0;
                while (tt < this.monsters3.size()) {
                    Monster3 ss = this.monsters3.get(tt);
                    if (ss.hitTest(tx, ty)) {
                        if (ss.star3_hp > 0) {
                            ss.star3_hp--;
                            this.soundPool.play(this.attack[4], 1.0f, 1.0f, 0, 0, 1.0f);
                        } else if (ss.star3_hp <= 0) {
                            ss.anime++;
                            if (ss.critical <= 1) {
                                this.score = this.score + 50;
                                ss.critical++;
                            }
                        }
                    }
                    tt++;
                }
                if (tt > 30) {
                }
            }
            int kkk = 0;
            while (kkk < this.monsters4.size()) {
                Monster4 q = this.monsters4.get(kkk);
                if (q.hitTest(tx, ty)) {
                    q.anime++;
                    if (q.critical <= 1) {
                        this.score = this.score + 10;
                        q.critical++;
                    }
                }
                kkk++;
            }
            if (kkk > 20) {
            }
            int nn = 0;
            while (nn < this.monsters5.size()) {
                Monster5 yy = this.monsters5.get(nn);
                if (yy.hitTest(tx, ty)) {
                    if (yy.star5_hp > 0) {
                        yy.star5_hp--;
                        this.soundPool.play(this.attack[3], 1.0f, 1.0f, 0, 0, 1.0f);
                    } else if (yy.star5_hp <= 0) {
                        yy.anime++;
                        if (yy.critical <= 1) {
                            this.score = this.score + 30;
                            yy.critical++;
                        }
                    }
                }
                nn++;
            }
            if (nn > 30) {
            }
            this.Touch_flg = 1;
            return true;
        }
        if (event.getAction() == 1) {
            this.Touch_flg = 0;
        }
        return true;
    }

    private void playSound() {
        stopSound();
        this._mediaPlayer = MediaPlayer.create(getContext(), (int) R.raw.clapolka);
        this._mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                mp.seekTo(0);
                mp.start();
            }
        });
    }

    public void pauseSound() {
        if (this._mediaPlayer != null && this._mediaPlayer.isPlaying()) {
            this._mediaPlayer.pause();
        }
    }

    public void resumeSound() {
        if (this._mediaPlayer != null) {
            this._mediaPlayer.start();
        }
    }

    public void stopSound() {
        if (this._mediaPlayer != null) {
            if (this._mediaPlayer.isPlaying()) {
                this._mediaPlayer.stop();
            }
            this._mediaPlayer.release();
            this._mediaPlayer = null;
        }
    }

    public void destroyBGM() {
        if (this.soundPool != null) {
            this.soundPool.release();
        }
    }

    public void destroyBITMAP() {
        if (this.bmpZimen2 != null) {
            this.bmpZimen2.recycle();
        }
        if (this.bmpTitle_haikei != null) {
            this.bmpTitle_haikei.recycle();
        }
        if (this.bmpSora2 != null) {
            this.bmpSora2.recycle();
        }
        if (this.bmpMogura != null) {
            this.bmpMogura.recycle();
        }
        if (this.bmpMogura2 != null) {
            this.bmpMogura2.recycle();
        }
        if (this.bmpMogura_kesi != null) {
            this.bmpMogura_kesi.recycle();
        }
        if (this.bmpBakuhatu != null) {
            this.bmpBakuhatu.recycle();
        }
        if (this.bmpCritical != null) {
            this.bmpCritical.recycle();
        }
        if (this.bmpHPmeta1 != null) {
            this.bmpHPmeta1.recycle();
        }
        if (this.bmpHPmeta2 != null) {
            this.bmpHPmeta2.recycle();
        }
    }

    public int getScore() {
        return this.score;
    }

    public void setOnSettingListener(OnSettingListener listener) {
        this.onSettingListener = listener;
    }

    private void OnSettingEvent() {
        if (this.onSettingListener != null) {
            this.onSettingListener.onSetting();
        }
    }
}
