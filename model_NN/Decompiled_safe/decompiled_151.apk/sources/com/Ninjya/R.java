package com.Ninjya;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int refreshAnimation = 2130771970;
        public static final int requestInterval = 2130771969;
        public static final int testMode = 2130771972;
        public static final int visibility = 2130771971;
    }

    public static final class drawable {
        public static final int ad_back = 2130837504;
        public static final int bakuhatu = 2130837505;
        public static final int clearpic = 2130837506;
        public static final int critical = 2130837507;
        public static final int gameover = 2130837508;
        public static final int haikei1 = 2130837509;
        public static final int haikei2 = 2130837510;
        public static final int haikei3 = 2130837511;
        public static final int haikei4 = 2130837512;
        public static final int hp_meta1 = 2130837513;
        public static final int hp_meta2 = 2130837514;
        public static final int icon = 2130837515;
        public static final int pc_satbox = 2130837516;
        public static final int ranking_button = 2130837517;
        public static final int ranking_haikei = 2130837518;
        public static final int sat_logo = 2130837519;
        public static final int star = 2130837520;
        public static final int star2 = 2130837521;
        public static final int star3 = 2130837522;
        public static final int star3_baku = 2130837523;
        public static final int star3_kesi = 2130837524;
        public static final int star4 = 2130837525;
        public static final int star5 = 2130837526;
        public static final int star_red = 2130837527;
        public static final int start_button = 2130837528;
        public static final int title_haikei = 2130837529;
        public static final int zimen2 = 2130837530;
    }

    public static final class id {
        public static final int adproxy = 2131230722;
        public static final int credit = 2131230720;
        public static final int end_bt_end = 2131230725;
        public static final int end_bt_score = 2131230724;
        public static final int footer = 2131230749;
        public static final int header = 2131230746;
        public static final int menu_apk = 2131230756;
        public static final int menu_end = 2131230758;
        public static final int menu_hp = 2131230757;
        public static final int menu_vib_switch = 2131230755;
        public static final int no10name = 2131230744;
        public static final int no10score = 2131230745;
        public static final int no1name = 2131230726;
        public static final int no1score = 2131230727;
        public static final int no2name = 2131230728;
        public static final int no2score = 2131230729;
        public static final int no3name = 2131230730;
        public static final int no3score = 2131230731;
        public static final int no4name = 2131230732;
        public static final int no4score = 2131230733;
        public static final int no5name = 2131230734;
        public static final int no5score = 2131230735;
        public static final int no6name = 2131230736;
        public static final int no6score = 2131230737;
        public static final int no7name = 2131230738;
        public static final int no7score = 2131230739;
        public static final int no8name = 2131230740;
        public static final int no8score = 2131230741;
        public static final int no9name = 2131230742;
        public static final int no9score = 2131230743;
        public static final int play_view = 2131230721;
        public static final int rank_tx_touch = 2131230750;
        public static final int ranklistview = 2131230748;
        public static final int rankname = 2131230747;
        public static final int result = 2131230723;
        public static final int txtItem1 = 2131230752;
        public static final int txtItem21 = 2131230753;
        public static final int txtItem22 = 2131230754;
        public static final int txtRankNo = 2131230751;
    }

    public static final class layout {
        public static final int credit = 2130903040;
        public static final int game = 2130903041;
        public static final int gameover = 2130903042;
        public static final int hiscore = 2130903043;
        public static final int main = 2130903044;
        public static final int rank = 2130903045;
        public static final int rankrow = 2130903046;
        public static final int rankrow2 = 2130903047;
        public static final int title = 2130903048;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class raw {
        public static final int baku = 2130968576;
        public static final int bakuha = 2130968577;
        public static final int clapolka = 2130968578;
        public static final int jupiter = 2130968579;
        public static final int kumo_se = 2130968580;
        public static final int sat_logo = 2130968581;
        public static final int se_title = 2130968582;
        public static final int tawara_se = 2130968583;
        public static final int yarare = 2130968584;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }

    public static final class style {
        public static final int TableNameStyle = 2131099649;
        public static final int TableNoStyle = 2131099648;
        public static final int TableScoreStyle = 2131099650;
    }

    public static final class styleable {
        public static final int[] mediba_ad_sdk_android_MasAdView = {R.attr.backgroundColor, R.attr.requestInterval, R.attr.refreshAnimation, R.attr.visibility, R.attr.testMode};
        public static final int mediba_ad_sdk_android_MasAdView_backgroundColor = 0;
        public static final int mediba_ad_sdk_android_MasAdView_refreshAnimation = 2;
        public static final int mediba_ad_sdk_android_MasAdView_requestInterval = 1;
        public static final int mediba_ad_sdk_android_MasAdView_testMode = 4;
        public static final int mediba_ad_sdk_android_MasAdView_visibility = 3;
    }
}
