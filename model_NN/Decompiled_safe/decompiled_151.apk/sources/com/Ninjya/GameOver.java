package com.Ninjya;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import jp.co.arttec.satbox.scoreranklib.HttpCommunicationListener;
import jp.co.arttec.satbox.scoreranklib.RegistScoreController;

public class GameOver extends BaseActivity {
    private AlertDialog _alertDialog;
    /* access modifiers changed from: private */
    public EditText _editText;
    private Button btn_end;
    /* access modifiers changed from: private */
    public Button btn_score;
    private String[] data = new String[10];
    /* access modifiers changed from: private */
    public boolean flg_end = false;
    /* access modifiers changed from: private */
    public boolean flg_up = false;
    private int intKindGames = 3;
    private int intScoreNo;
    private String nichiji;
    private int score = 0;
    private int[] scoredate = new int[10];

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.gameover);
        this.score = getIntent().getExtras().getInt("SCORE");
        this.btn_end = (Button) findViewById(R.id.end_bt_end);
        this.btn_score = (Button) findViewById(R.id.end_bt_score);
        ((TextView) findViewById(R.id.result)).setText(new Integer(this.score).toString());
        this._editText = new EditText(this);
        this._editText.setFocusable(true);
        this._editText.setFocusableInTouchMode(true);
        this._editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        onMyScoreEntry("");
        this._alertDialog = new AlertDialog.Builder(this).setTitle("Score entry").setMessage("Enter your name").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameOver.this.registHighScore(GameOver.this._editText.getText().toString());
            }
        }).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setView(this._editText).create();
        this.btn_score.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                if (!GameOver.this.flg_up) {
                    GameOver.this.onScoreEntry();
                }
            }
        });
        this.btn_end.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                if (!GameOver.this.flg_end) {
                    GameOver.this.flg_end = true;
                    GameOver.this.startActivity(new Intent(GameOver.this, Title.class));
                    GameOver.this.finish();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void onScoreEntry() {
        this._alertDialog.show();
    }

    /* access modifiers changed from: private */
    public void registHighScore(String name) {
        RegistScoreController controller = new RegistScoreController(this.intKindGames, (long) this.score, name);
        controller.setActivity(this);
        controller.setOnFinishListener(new HttpCommunicationListener() {
            public void onFinish(boolean result) {
                Toast toast;
                if (result) {
                    GameOver.this.btn_score.setEnabled(false);
                    GameOver.this.flg_up = true;
                    toast = Toast.makeText(GameOver.this, "Score entry completion.", 1);
                } else {
                    toast = Toast.makeText(GameOver.this, "Score entry failure.", 1);
                }
                toast.show();
            }
        });
        controller.registScore();
    }

    private void onMyScoreEntry(String ranklevel) {
        SharedPreferences pref = getSharedPreferences("prefkey", 0);
        ArrayList<RankStrDelivery> itemSort = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            this.intScoreNo = i + 1;
            this.scoredate[i] = pref.getInt("Score" + ranklevel + String.valueOf(this.intScoreNo), 0);
            this.data[i] = pref.getString("Data" + ranklevel + String.valueOf(this.intScoreNo), "9999/99/99 99:99:99");
            itemSort.add(new RankStrDelivery(this.scoredate[i], this.data[i], "", ""));
        }
        setNichiji();
        itemSort.add(new RankStrDelivery(this.score, this.nichiji, "", ""));
        Collections.sort(itemSort, new Comparator<RankStrDelivery>() {
            public int compare(RankStrDelivery o1, RankStrDelivery o2) {
                return o2.getRankNo() - o1.getRankNo();
            }
        });
        SharedPreferences.Editor editor = pref.edit();
        this.intScoreNo = 0;
        for (int i2 = 0; i2 < 10; i2++) {
            this.intScoreNo = i2 + 1;
            RankStrDelivery rsd = (RankStrDelivery) itemSort.get(i2);
            editor.putInt("Score" + ranklevel + String.valueOf(this.intScoreNo), rsd.getRankNo());
            editor.putString("Data" + ranklevel + String.valueOf(this.intScoreNo), rsd.getItem1());
        }
        editor.commit();
    }

    private void setNichiji() {
        String strMonth;
        String strday;
        String strHour;
        String strMinute;
        String strSecond;
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(1);
        int month = calendar.get(2);
        int day = calendar.get(5);
        int hour = calendar.get(11);
        int minute = calendar.get(12);
        int second = calendar.get(13);
        String strYear = String.valueOf(year);
        if (month < 10) {
            strMonth = "0" + String.valueOf(month + 1);
        } else {
            strMonth = String.valueOf(month + 1);
        }
        if (day < 10) {
            strday = "0" + String.valueOf(day);
        } else {
            strday = String.valueOf(day);
        }
        if (hour < 10) {
            strHour = "0" + String.valueOf(hour);
        } else {
            strHour = String.valueOf(hour);
        }
        if (minute < 10) {
            strMinute = "0" + String.valueOf(minute);
        } else {
            strMinute = String.valueOf(minute);
        }
        if (second < 10) {
            strSecond = "0" + String.valueOf(second);
        } else {
            strSecond = String.valueOf(second);
        }
        this.nichiji = String.valueOf(strYear) + "/" + strMonth + "/" + strday + " " + strHour + ":" + strMinute + ":" + strSecond;
    }
}
