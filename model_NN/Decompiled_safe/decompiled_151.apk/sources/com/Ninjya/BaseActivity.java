package com.Ninjya;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;

public class BaseActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String titlename = String.valueOf(getTitle());
        String versionName = "";
        try {
            versionName = getPackageManager().getPackageInfo("com.Ninjya", 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        setTitle(String.valueOf(titlename) + "   Ver." + versionName);
    }
}
