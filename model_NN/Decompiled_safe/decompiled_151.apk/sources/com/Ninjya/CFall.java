package com.Ninjya;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

/* compiled from: Fall */
class CFall {
    protected float m_fAddRot;
    protected float m_fRot;
    protected int m_nSpd;
    protected int m_nX = ((int) Math.floor(Math.random() * ((double) this.m_pView.dispX)));
    protected int m_nY;
    protected MoguraView m_pView;

    public CFall(MoguraView pView, int nSpd, int nY) {
        this.m_pView = pView;
        this.m_nY = nY;
        this.m_nSpd = nSpd;
        this.m_fRot = 0.0f;
        this.m_fAddRot = (((float) Math.random()) * 6.0f) - 3.0f;
    }

    public void move() {
        float f = this.m_fRot + this.m_fAddRot;
        this.m_fRot = f;
        if (f >= 360.0f) {
            this.m_fRot -= 360.0f;
        }
        this.m_nY += this.m_nSpd;
        this.m_nY = Math.min(this.m_pView.dispY, Math.max(0, this.m_nY));
    }

    public void Draw(Canvas canvas, Bitmap pTex) {
        int nTexW = pTex.getWidth();
        int nTexH = pTex.getHeight();
        canvas.save();
        canvas.rotate(this.m_fRot, (float) (this.m_nX + (nTexW / 2)), (float) (this.m_nY + (nTexH / 2)));
        canvas.drawBitmap(pTex, (float) this.m_nX, (float) this.m_nY, (Paint) null);
        canvas.restore();
    }

    public int GetX() {
        return this.m_nX;
    }

    public int GetY() {
        return this.m_nY;
    }
}
