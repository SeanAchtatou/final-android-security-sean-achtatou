package mediba.ad.sdk.android;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class AdProxyURLConnector extends AdProxyConnector {
    private static final String _TAG = "AdProxyURLConnector";
    private HttpURLConnection http_connection;
    private URL lurl;

    public AdProxyURLConnector(String s, String s1, String s2, AdProxyConnectorListener h1, int j, Map map, String s3) {
        super(s1, s2, h1, j, map, s3);
        try {
            if (AdUtil.isLogEnable()) {
                Log.d(_TAG, s);
            }
            this.lurl = new URL(String.valueOf(s) + "?" + s3);
            this.url = this.lurl;
        } catch (Exception e) {
            this.lurl = null;
            this.c = e;
        }
        this.http_connection = null;
        this.retry_count = 0;
    }

    public final boolean connect() {
        String s2;
        boolean flag = false;
        if (this.lurl == null) {
            if (this.adproxyConnectorListener != null) {
                this.adproxyConnectorListener.a(this, new Exception("url was null"));
            }
            flag = false;
        } else {
            if (AdUtil.isLogEnable()) {
                Log.d(_TAG, "URLConn");
            }
            HttpURLConnection.setFollowRedirects(true);
            while (this.retry_count < this.max_retry && !flag) {
                if (AdUtil.isLogEnable()) {
                    Log.v(_TAG, "attempt " + this.retry_count + " to connect to url " + this.lurl);
                }
                BufferedWriter localBufferedWriter = null;
                try {
                    close_connection();
                    this.http_connection = (HttpURLConnection) this.lurl.openConnection();
                    if (this.http_connection != null) {
                        this.http_connection.setRequestProperty("User-Agent", user_agent());
                        this.http_connection.setConnectTimeout(this.timeout);
                        this.http_connection.setReadTimeout(this.timeout);
                        if (this.d != null) {
                            for (String s : this.d.keySet()) {
                                if (!(s == null || (s2 = (String) this.d.get(s)) == null)) {
                                    this.http_connection.addRequestProperty(s, s2);
                                }
                            }
                        }
                        this.http_connection.connect();
                        int response_code = this.http_connection.getResponseCode();
                        if (response_code >= 200 && response_code < 300) {
                            this.url = this.http_connection.getURL();
                            if (this.k) {
                                BufferedInputStream is = new BufferedInputStream(this.http_connection.getInputStream(), 4096);
                                byte[] arrayOfByte = new byte[4096];
                                ByteArrayOutputStream os = new ByteArrayOutputStream(4096);
                                while (true) {
                                    int readbuf = is.read(arrayOfByte);
                                    if (readbuf == -1) {
                                        break;
                                    }
                                    os.write(arrayOfByte, 0, readbuf);
                                }
                                this.buffer = os.toByteArray();
                            }
                            if (this.adproxyConnectorListener != null) {
                                this.adproxyConnectorListener.a(this);
                            }
                            flag = true;
                        }
                    }
                    close_connection();
                    if (localBufferedWriter != null) {
                        try {
                            localBufferedWriter.close();
                        } catch (Exception e) {
                        }
                    }
                    close_connection();
                } catch (Exception e2) {
                    Exception localException1 = e2;
                    Log.w(_TAG, "could not open connection to url " + this.lurl, localException1);
                    this.c = localException1;
                    flag = false;
                    if (localBufferedWriter != null) {
                        try {
                            localBufferedWriter.close();
                        } catch (Exception e3) {
                        }
                    }
                    close_connection();
                } catch (Throwable th) {
                    if (localBufferedWriter != null) {
                        try {
                            localBufferedWriter.close();
                        } catch (Exception e4) {
                        }
                    }
                    close_connection();
                    throw th;
                }
                this.retry_count++;
            }
        }
        if (this.adproxyConnectorListener != null && flag) {
            this.adproxyConnectorListener.a(this, this.c);
        }
        return flag;
    }

    private void close_connection() {
        if (this.http_connection != null) {
            this.http_connection.disconnect();
            this.http_connection = null;
        }
    }

    public final void disconnect() {
        close_connection();
        this.adproxyConnectorListener = null;
    }

    public final void run() {
        try {
            connect();
        } catch (Exception localException) {
            if (Log.isLoggable(_TAG, 6)) {
                Log.e(_TAG, "exception caught in AdProxyURLConnector.run(), " + localException.getMessage());
            }
        }
    }
}
