package mediba.ad.sdk.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import java.lang.ref.WeakReference;
import java.util.Vector;
import mediba.ad.sdk.android.AdProxy;

public class MasAdView extends RelativeLayout {
    private static String UserAgent = null;
    private static final String _TAG = "AdView";
    static final Handler handler = new Handler();
    private static boolean hasad;
    /* access modifiers changed from: private */
    public static int isAnimate;
    private static boolean isFirst;
    private static Vector<View> r = new Vector<>();
    private static boolean testmode;
    /* access modifiers changed from: private */
    public AdContainer adcontainer;
    public boolean isRefreshed;
    /* access modifiers changed from: private */
    public AdProxyListener mAdListener;
    private int mBackGroundColor;
    private int mPrimaryTextColor;
    private int mSecondaryTextColor;
    private boolean n;
    private boolean o;
    private int pRequestInterval;
    private AdProxy.a q;
    private RefreshHandler refleshHandler;
    private int requestInterval;
    private long request_time;

    public MasAdView(Activity activity) {
        this(activity, null, 0);
    }

    public MasAdView(Context context, AttributeSet attributeset) {
        this(context, attributeset, 0);
    }

    public MasAdView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.isRefreshed = false;
        this.n = true;
        isFirst = true;
        setFocusable(true);
        setFocusableInTouchMode(true);
        setDescendantFocusability(262144);
        setClickable(true);
        setSelected(true);
        setGravity(17);
        int bc = 1;
        int ri = 30;
        int ra = 1;
        int v = 0;
        boolean tm = false;
        if (attrs != null) {
            String schema = "http://schemas.android.com/apk/res/" + context.getPackageName();
            bc = attrs.getAttributeUnsignedIntValue(schema, "backgroundColor", 1);
            ri = attrs.getAttributeUnsignedIntValue(schema, "requestInterval", 30);
            ra = attrs.getAttributeUnsignedIntValue(schema, "refreshAnimation", 1);
            v = attrs.getAttributeUnsignedIntValue(schema, "visibility", 0);
            tm = attrs.getAttributeBooleanValue(schema, "testMode", false);
        }
        setBackgroundColor(bc);
        setRequestInterval(ri);
        setRefreshAnimation(ra);
        setVisibility(v);
        testMode(tm);
        setUserAgent(context);
        hasad = true;
        if (AdUtil.isLogEnable()) {
            Log.i(_TAG, "create ads by mediba");
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int width, int height) {
        int width2;
        int fixed_height;
        AdProxy adproxy;
        super.onMeasure(width, height);
        if (AdUtil.isLogEnable()) {
            Log.i(_TAG, "onMeasure");
        }
        int k1 = View.MeasureSpec.getSize(width);
        int l1 = View.MeasureSpec.getSize(height);
        int width_mode = View.MeasureSpec.getMode(width);
        int height_mode = View.MeasureSpec.getMode(height);
        if (width_mode == Integer.MIN_VALUE || width_mode == 1073741824) {
            width2 = k1;
        } else {
            width2 = MasAdManager.getScreenWidth(getContext());
        }
        if (height_mode == 1073741824) {
            fixed_height = l1;
        } else {
            fixed_height = 0;
            if (!(this.adcontainer == null || (adproxy = this.adcontainer.getAdProxyInstance()) == null)) {
                int tmp_height = adproxy.a(adproxy.getContainerHeight());
                if (AdUtil.isLogEnable()) {
                    Log.d(_TAG, "container height:" + tmp_height);
                }
                if (height_mode != Integer.MIN_VALUE || l1 >= tmp_height) {
                    fixed_height = tmp_height;
                }
            }
        }
        setMeasuredDimension(width2, fixed_height);
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "AdView.onMeasure:  widthSize " + k1 + " heightSize " + l1);
        }
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "AdView.onMeasure:  measuredWidth " + width2 + " measuredHeight " + fixed_height);
        }
        int j2 = getMeasuredWidth();
        int l2 = getMeasuredHeight();
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "AdView size is " + j2 + " by " + l2);
        }
        if (this.n) {
            startAdRequest();
            isFirst = false;
        }
    }

    private void triggerRequestThread(boolean flag) {
        if (AdUtil.isLogEnable()) {
            Log.i(_TAG, "triggerRequestThread");
        }
        synchronized (this) {
            if (flag) {
                if (this.requestInterval > 0 && getVisibility() == 0 && !isFirst) {
                    int i1 = this.requestInterval;
                    stopAdRequest();
                    if (c()) {
                        getClass();
                        this.refleshHandler = new RefreshHandler();
                        handler.postDelayed(this.refleshHandler, (long) i1);
                        if (AdUtil.isLogEnable()) {
                            Log.d(_TAG, "Ad refresh scheduled for " + i1 + " from now.");
                        }
                    }
                }
            }
            if (!flag || this.requestInterval == 0 || getVisibility() != 0) {
                stopAdRequest();
                if (AdUtil.isLogEnable()) {
                    Log.d(_TAG, "call stopAdRequest from triggerRequestThread");
                }
            }
        }
    }

    private void startAdRequest() {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "startAdRequest");
        }
        if ((this.n || super.getVisibility() == 0) && !this.o) {
            this.request_time = SystemClock.uptimeMillis();
            this.o = true;
            new AdRequestThread(this).start();
        }
    }

    private void stopAdRequest() {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "stopAdRequest");
        }
        if (this.refleshHandler != null) {
            this.refleshHandler.a = true;
            this.refleshHandler = null;
            if (AdUtil.isLogEnable()) {
                Log.v(_TAG, "Cancelled an ad refresh scheduled for the future.");
            }
        }
    }

    public void onWindowFocusChanged(boolean flag) {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "OnWindowFocusChange");
        }
        triggerRequestThread(flag);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i1) {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "OnWindowVisibilityChange");
        }
        triggerRequestThread(i1 == 0);
    }

    public void setAdListener(AdProxyListener adlistener) {
        synchronized (this) {
            this.mAdListener = adlistener;
        }
    }

    public AdProxyListener getAdListener() {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "AdListener getAdListener");
        }
        return this.mAdListener;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "void onAttachedToWindow");
        }
        triggerRequestThread(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "void onDetachedFromWindow");
        }
        triggerRequestThread(false);
        super.onDetachedFromWindow();
    }

    static AdProxyListener b(MasAdView adview) {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "AdListener b MasAdView adview");
        }
        return adview.mAdListener;
    }

    private boolean c() {
        AdProxy adproxy;
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "AdView.c");
        }
        if (this.adcontainer == null || (adproxy = this.adcontainer.getAdProxyInstance()) == null || !adproxy.d() || this.adcontainer.g() >= 120) {
            return true;
        }
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "Cannot refresh CPM ads.  Ignoring request to refresh the ad.");
        }
        return false;
    }

    static void c(MasAdView adview) {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "AdView.c MasAdView adview");
        }
        if (adview.mAdListener != null) {
            Handler handler2 = handler;
            adview.getClass();
            handler2.post(new AdFailed());
            hasad = false;
        }
    }

    static int h(MasAdView adview) {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "AdView.h MasAdView adview");
        }
        return adview.requestInterval;
    }

    static void i(MasAdView adview) {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "AdView.i MasAdView adview");
        }
        adview.startAdRequest();
    }

    private class RefreshHandler implements Runnable {
        boolean a;
        private WeakReference b;

        public final void run() {
            if (AdUtil.isLogEnable()) {
                Log.v(MasAdView._TAG, "private class RefreshHandler run");
            }
            try {
                MasAdView adview = (MasAdView) this.b.get();
                if (!this.a && adview != null) {
                    if (Log.isLoggable(MasAdView._TAG, 3)) {
                        int i1 = MasAdView.h(adview) / 1000;
                        if (AdUtil.isLogEnable()) {
                            Log.d(MasAdView._TAG, "Requesting a fresh ad because a request interval passed (" + i1 + " seconds).");
                        }
                    }
                    MasAdView.i(adview);
                }
            } catch (Exception e) {
                Exception exception = e;
                if (Log.isLoggable(MasAdView._TAG, 6)) {
                    Log.e(MasAdView._TAG, "exception caught in RefreshHandler.run(), " + exception.getMessage());
                }
            }
        }

        public RefreshHandler() {
            this.b = new WeakReference(MasAdView.this);
        }
    }

    static void b(MasAdView adview, AdContainer adcontainer2) {
        if (AdUtil.isLogEnable()) {
            Log.v(_TAG, "static void b MasAdView adview, AdContainer adcontainer");
        }
        adcontainer2.setVisibility(8);
    }

    private class AddContainer implements Runnable {
        private WeakReference a;
        private WeakReference b;
        private int c;
        private boolean d;

        public AddContainer(MasAdView adview, AdContainer g1, int i1, boolean flag) {
            if (AdUtil.isLogEnable()) {
                Log.v(MasAdView._TAG, "AddContainer MasAdView adview, AdContainer g1, int i1, boolean flag");
            }
            this.a = new WeakReference(adview);
            this.b = new WeakReference(g1);
            this.c = i1;
            this.d = flag;
        }

        public final void run() {
            if (AdUtil.isLogEnable()) {
                Log.v(MasAdView._TAG, "AddContainer.run()");
            }
            try {
                MasAdView adview = (MasAdView) this.a.get();
                AdContainer old = MasAdView.this.adcontainer;
                if (old != null) {
                    adview.setClickable(false);
                    if (MasAdView.isAnimate == 2) {
                        old.fadeOut(300);
                    } else if (MasAdView.isAnimate == 3) {
                        old.slideDownOut(300);
                    } else if (MasAdView.isAnimate == 4) {
                        old.slideUpOut(300);
                    }
                }
                AdContainer g1 = (AdContainer) this.b.get();
                if (adview != null && g1 != null) {
                    adview.removeAllViews();
                    adview.addView(g1);
                    MasAdView.a(adview, g1.getAdProxyInstance());
                    if (this.c == 0) {
                        if (this.d) {
                            MasAdView.a(adview, g1);
                        } else {
                            MasAdView.b(adview, g1);
                        }
                    }
                    if (MasAdView.isAnimate == 2) {
                        g1.fadeIn(300);
                    } else if (MasAdView.isAnimate == 3) {
                        g1.slideDownIn(300);
                    } else if (MasAdView.isAnimate == 4) {
                        g1.slideUpIn(300);
                    }
                    adview.setClickable(true);
                    MasAdView.c(adview, g1);
                }
            } catch (Exception e) {
                Log.e(MasAdView._TAG, "Unhandled exception placing AdContainer into AdView.", e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(AdProxy adproxy, AdContainer adcontainer2) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "a(adproxy,adcontainer)");
        }
        int i1 = super.getVisibility();
        double d1 = adproxy.a();
        if (d1 >= 0.0d) {
            setRequestInterval((int) d1);
            triggerRequestThread(true);
        }
        if (this.n) {
            this.n = false;
        }
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "adcontainer Layout");
        }
        adcontainer2.setEventStatus(adproxy);
        adcontainer2.setVisibility(i1);
        adcontainer2.setGravity(17);
        adproxy.setAdContainer(adcontainer2);
        adcontainer2.setLayoutParams(new RelativeLayout.LayoutParams(adproxy.a(adproxy.e()), adproxy.a(adproxy.getContainerHeight())));
        handler.post(new AddContainer(this, adcontainer2, i1, true));
    }

    static void a(MasAdView adview, AdContainer g1) {
        adview.adcontainer = g1;
    }

    static AdContainer c(MasAdView adview, AdContainer g1) {
        adview.adcontainer = g1;
        return g1;
    }

    /* access modifiers changed from: private */
    public static void a(MasAdView adview, AdProxy adProxyInstance) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "AdListner");
        }
        if (adview.mAdListener == null) {
            return;
        }
        if (adview.adcontainer == null || adview.adcontainer.getParent() == null) {
            try {
                AdUtil.onReceiveAd(adview);
            } catch (Exception exception) {
                Log.w(_TAG, "Unhandled exception raised in your AdListener.onReceiveAd.", exception);
                AdUtil.onFailedToReceiveAd(adview);
            }
        }
    }

    private class AdFailed implements Runnable {
        private WeakReference a;

        public AdFailed() {
            this.a = new WeakReference(MasAdView.this);
        }

        public final void run() {
            if (AdUtil.isLogEnable()) {
                Log.d(MasAdView._TAG, "AdFailed.run");
            }
            MasAdView adview = (MasAdView) this.a.get();
            if (adview == null) {
                return;
            }
            if (MasAdView.getAdContainer(adview) == null || MasAdView.getAdContainer(adview).getParent() == null) {
                try {
                    MasAdView.b(adview).onFailedToReceiveAd(adview);
                } catch (Exception e) {
                    adview.mAdListener.onReceiveRefreshedAd(adview);
                    Log.w(MasAdView._TAG, "Unhandled exception raised in your AdListener.onFailedToReceiveAd." + e);
                }
            } else {
                try {
                    MasAdView.b(adview).onFailedToReceiveRefreshedAd(adview);
                } catch (Exception e2) {
                    adview.mAdListener.onReceiveRefreshedAd(adview);
                    Log.w(MasAdView._TAG, "Unhandled exception raised in your AdListener.onFailedToReceiveRefreshedAd." + e2);
                }
            }
        }
    }

    static AdProxy.a d(MasAdView adview) {
        if (adview.q == null) {
            adview.q = new AdProxy.a(adview);
        }
        return adview.q;
    }

    static void b(View view) {
        r.remove(view);
    }

    static boolean a(MasAdView adview, boolean flag) {
        adview.o = false;
        return false;
    }

    static void b(MasAdView adview, boolean b) {
        adview.triggerRequestThread(true);
    }

    static long getLastRequestTime(MasAdView adview) {
        return adview.request_time;
    }

    static AdContainer getAdContainer(MasAdView adview) {
        return adview.adcontainer;
    }

    /* access modifiers changed from: protected */
    public void setUserAgent(Context context) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "setUserAgent");
        }
        UserAgent = new WebView(context).getSettings().getUserAgentString();
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "UserAgent: " + UserAgent);
        }
    }

    protected static String getUserAgent() {
        return UserAgent;
    }

    public void setEnabled(boolean flag) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "setEnabled");
        }
        super.setEnabled(flag);
        if (flag) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    public void testMode(boolean testmode2) {
        testmode = testmode2;
    }

    protected static boolean getTestMode() {
        return testmode;
    }

    private boolean hasAd() {
        return hasad;
    }

    public void setVisibility(final int i1) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "setVisibility");
        }
        if (super.getVisibility() != i1) {
            synchronized (this) {
                int k1 = getChildCount();
                for (int l1 = 0; l1 < k1; l1++) {
                    final View view = getChildAt(l1);
                    handler.post(new Runnable() {
                        public void run() {
                            view.setVisibility(i1);
                        }
                    });
                }
                super.setVisibility(i1);
                invalidate();
            }
        }
        triggerRequestThread(i1 == 0);
    }

    public void setRequestInterval(int i1) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "setRequestInterval");
        }
        int j1 = i1 * 1000;
        if (this.requestInterval != j1) {
            if (i1 > 0) {
                if (i1 < 30) {
                    Log.w(_TAG, "AdView.setRequestInterval(" + i1 + ") seconds must be >= " + 30);
                    i1 = 30;
                    j1 = 30 * 1000;
                } else if (i1 > 300) {
                    Log.w(_TAG, "AdView.setRequestInterval(" + i1 + ") seconds must be <= " + 300);
                    i1 = 300;
                    j1 = 300 * 1000;
                }
            }
            this.requestInterval = j1;
            if (i1 <= 0) {
                stopAdRequest();
            }
            if (AdUtil.isLogEnable()) {
                Log.i(_TAG, "Requesting fresh ads every " + i1 + " seconds.");
            }
        }
    }

    public int getRequestInterval() {
        return this.requestInterval;
    }

    public void setBackgroundColor(int i1) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "setBackgroundColor");
        }
        if (i1 == 1 || i1 == 2) {
            this.mBackGroundColor = i1;
        } else {
            Log.w(_TAG, "setBackgroundColor(int) must be 1(Black) or 2(White). Default is 1.");
            this.mBackGroundColor = 1;
        }
        if (this.mBackGroundColor == 1) {
            setTextColor(Color.rgb(255, 255, 255));
        } else if (this.mBackGroundColor == 2) {
            setTextColor(Color.rgb(0, 0, 0));
        }
        invalidate();
    }

    private void setTextColor(int k) {
        setPrimaryTextColor(k);
        setSecondaryTextColor(k);
    }

    private void setPrimaryTextColor(int k) {
        this.mPrimaryTextColor = -16777216 | k;
    }

    private void setSecondaryTextColor(int k) {
        this.mSecondaryTextColor = -16777216 | k;
    }

    public int getBackgroundColor() {
        return this.mBackGroundColor;
    }

    /* access modifiers changed from: protected */
    public int getPrimaryTextColor() {
        return this.mPrimaryTextColor;
    }

    /* access modifiers changed from: protected */
    public int getSecondaryTextColor() {
        return this.mSecondaryTextColor;
    }

    public void setGender(int i) {
        if (i == 1 || i == 2) {
            MasAdManager.gender = Integer.toString(i);
        } else {
            Log.w(_TAG, "Function AdView.setGender(int) must be 1(Male) or 2(Female)");
        }
    }

    public void setAge(String i) {
        String i2 = i.toUpperCase();
        if ("ABCDEFGHIJKLMNOPQ".matches(".*" + i2 + ".*")) {
            MasAdManager.age = i2;
        } else {
            Log.w(_TAG, "Function AdView.setAge(String) must be 1 Character ( A ‾ Q )");
        }
    }

    public void setAddress(int i) {
        if (i < 1 || i > 47) {
            Log.w(_TAG, "Function AdView.setAddress(int) must be 1 ‾ 47");
        } else {
            MasAdManager.address = Integer.toString(i);
        }
    }

    public void setRefreshAnimation(int sa) {
        if (sa == 0 || sa == 1 || sa == 2 || sa == 3 || sa == 4) {
            isAnimate = sa;
        } else {
            Log.w(_TAG, "setRefreshAnimation(int) must be 0 or 1 or 2 or 3 or 4. Default is 1.");
            isAnimate = 1;
        }
        if (isAnimate == 0) {
            stopAdRequest();
            this.pRequestInterval = this.requestInterval;
            this.requestInterval = 0;
        } else if (isAnimate == 1) {
            if (this.pRequestInterval > 0) {
                this.requestInterval = this.pRequestInterval;
            }
            startAdRequest();
        }
    }
}
