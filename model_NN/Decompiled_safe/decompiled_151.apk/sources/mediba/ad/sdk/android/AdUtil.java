package mediba.ad.sdk.android;

public class AdUtil {
    public static boolean isLogEnable() {
        return false;
    }

    public static void onReceiveAd(MasAdView adview) {
        MasAdProxyListener lsn;
        if (adview != null && (lsn = (MasAdProxyListener) adview.getAdListener()) != null) {
            if (adview.isRefreshed) {
                lsn.onReceiveRefreshedAd(adview);
            } else {
                lsn.onReceiveAd(adview);
            }
        }
    }

    public static void onFailedToReceiveAd(MasAdView adview) {
        MasAdProxyListener lsn;
        if (adview != null && (lsn = (MasAdProxyListener) adview.getAdListener()) != null) {
            if (adview.isRefreshed) {
                lsn.onFailedToReceiveRefreshedAd(adview);
            } else {
                lsn.onFailedToReceiveAd(adview);
            }
        }
    }
}
