package mediba.ad.sdk.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.Ninjya.R;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AdOverlay extends Dialog {
    static final String _TAG = "AdOverlay";

    public AdOverlay(Context context, int theme) {
        super(context, theme);
    }

    public AdOverlay(Context context) {
        super(context);
    }

    public static class Builder {
        private static float mdensity = -1.0f;
        private final int FP = -1;
        private final int WC = -2;
        /* access modifiers changed from: private */
        public Context context;
        private int deviceHeight;
        private int deviceWidth;
        private ImageView iBack;
        /* access modifiers changed from: private */
        public ImageView iClose;
        private URL imageURL;
        private String link;
        /* access modifiers changed from: private */
        public ProgressDialog mProgress;
        /* access modifiers changed from: private */
        public TextView mTitle;
        /* access modifiers changed from: private */
        public WebView mWebView;
        /* access modifiers changed from: private */
        public LinearLayout overlay_layout;
        private RelativeLayout title_bar;
        private LinearLayout titlebar_layout;
        private LinearLayout webview_layout;

        public Builder(Context context2, String link_path) {
            this.context = context2;
            this.link = link_path;
            Log.d(AdOverlay._TAG, context2.toString());
        }

        public AdOverlay create() {
            AdOverlay dialog = new AdOverlay(this.context);
            dialog.requestWindowFeature(1);
            mdensity = this.context.getResources().getDisplayMetrics().density;
            this.deviceWidth = MasAdManager.getScreenWidth(this.context);
            this.deviceHeight = MasAdManager.getScreenHeight(this.context);
            int titlebar_h = (int) (40.0f * mdensity);
            int close_w = (int) (86.0f * mdensity);
            int close_h = (int) (28.0f * mdensity);
            int close_margin = (int) (6.0f * mdensity);
            this.mProgress = new ProgressDialog(this.context);
            this.mProgress.requestWindowFeature(1);
            this.mProgress.setMessage("Loading");
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.deviceWidth, this.deviceHeight);
            this.overlay_layout = new LinearLayout(this.context);
            this.overlay_layout.setOrientation(1);
            this.overlay_layout.setVisibility(4);
            this.title_bar = new RelativeLayout(this.context);
            this.iBack = new ImageView(this.context);
            Bitmap bbm = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/header_bg.png"));
            Bitmap cbm = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/header_btn.png"));
            Bitmap cobm = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/header_btn_over.png"));
            this.iBack.setImageBitmap(bbm);
            this.iBack.setScaleType(ImageView.ScaleType.FIT_XY);
            this.iBack.setLayoutParams(new Gallery.LayoutParams(-1, titlebar_h));
            this.title_bar.addView(this.iBack);
            this.titlebar_layout = new LinearLayout(this.context);
            this.titlebar_layout.setOrientation(0);
            this.mTitle = new TextView(this.context);
            this.mTitle.setTextColor(-1);
            this.mTitle.setWidth(this.deviceWidth - ((int) (128.0f * mdensity)));
            this.mTitle.setSingleLine();
            this.mTitle.setFocusable(true);
            this.mTitle.setSelected(true);
            this.mTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            this.mTitle.setMarqueeRepeatLimit(10);
            this.mTitle.setGravity(16);
            this.mTitle.setPadding((int) (10.0f * mdensity), (int) (10.0f * mdensity), 0, 0);
            LinearLayout.LayoutParams close_set = new LinearLayout.LayoutParams(close_w, close_h);
            close_set.setMargins(close_margin, close_margin, close_margin, close_margin);
            this.iClose = new ImageView(this.context);
            this.iClose.setImageBitmap(cbm);
            final Bitmap bitmap = cobm;
            final Bitmap bitmap2 = cbm;
            this.iClose.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case R.styleable.mediba_ad_sdk_android_MasAdView_backgroundColor:
                            Builder.this.iClose.setImageBitmap(bitmap);
                            return false;
                        case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval:
                            Builder.this.iClose.setImageBitmap(bitmap2);
                            return false;
                        default:
                            return false;
                    }
                }
            });
            final AdOverlay adOverlay = dialog;
            this.iClose.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    adOverlay.dismiss();
                }
            });
            this.titlebar_layout.addView(this.mTitle);
            this.titlebar_layout.addView(this.iClose, close_set);
            this.title_bar.addView(this.titlebar_layout);
            LinearLayout.LayoutParams webview_set = new LinearLayout.LayoutParams(this.deviceWidth - ((int) (25.0f * mdensity)), this.deviceHeight - ((int) (90.0f * mdensity)));
            this.mWebView = new WebView(this.context);
            this.mWebView.setHorizontalScrollBarEnabled(true);
            this.mWebView.setVerticalScrollBarEnabled(true);
            this.mWebView.getSettings().setJavaScriptEnabled(true);
            this.mWebView.setWebViewClient(new AdWebViewClient(this, null));
            this.mWebView.loadUrl(this.link);
            this.overlay_layout.addView(this.title_bar);
            this.overlay_layout.addView(this.mWebView, webview_set);
            dialog.addContentView(this.overlay_layout, layoutParams);
            return dialog;
        }

        private class AdWebViewClient extends WebViewClient {
            boolean hasRedirect;

            private AdWebViewClient() {
                this.hasRedirect = false;
            }

            /* synthetic */ AdWebViewClient(Builder builder, AdWebViewClient adWebViewClient) {
                this();
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                super.shouldOverrideUrlLoading(view, url);
                if (!this.hasRedirect) {
                    view.loadUrl(url);
                    this.hasRedirect = true;
                } else if (this.hasRedirect) {
                    Builder.this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                }
                return true;
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Builder.this.mProgress.show();
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                String title = Builder.this.mWebView.getTitle();
                if (title != null && title.length() > 0) {
                    Builder.this.mTitle.setText(title);
                }
                Builder.this.mProgress.dismiss();
                Builder.this.overlay_layout.setVisibility(0);
                AdContainer.setProgressbarVisibility(4);
                AdContainer.setActionIconVisibility(0);
            }
        }

        public Bitmap getImageBitmap(String url) {
            Bitmap tmp_bm = null;
            try {
                BufferedInputStream bis = new BufferedInputStream(((HttpURLConnection) new URL(url).openConnection()).getInputStream(), 1048576);
                tmp_bm = BitmapFactory.decodeStream(bis, null, new BitmapFactory.Options());
                bis.close();
                return tmp_bm;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return tmp_bm;
            } catch (IOException e2) {
                e2.printStackTrace();
                return tmp_bm;
            }
        }

        public Bitmap getImageBitmap(URL url) {
            Bitmap tmp_bm = null;
            try {
                BufferedInputStream bis = new BufferedInputStream(((JarURLConnection) url.openConnection()).getInputStream(), 1048576);
                tmp_bm = BitmapFactory.decodeStream(bis, null, new BitmapFactory.Options());
                bis.close();
                return tmp_bm;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return tmp_bm;
            } catch (IOException e2) {
                e2.printStackTrace();
                return tmp_bm;
            }
        }
    }
}
