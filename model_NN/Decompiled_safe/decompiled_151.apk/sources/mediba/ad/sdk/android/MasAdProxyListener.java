package mediba.ad.sdk.android;

public abstract class MasAdProxyListener implements AdProxyListener {
    private String tag;

    public abstract void onFailedToReceiveAd(MasAdView masAdView);

    public abstract void onFailedToReceiveRefreshedAd(MasAdView masAdView);

    public abstract void onReceiveAd(MasAdView masAdView);

    public abstract void onReceiveRefreshedAd(MasAdView masAdView);

    public MasAdProxyListener(String tag2) {
        this.tag = tag2;
    }

    public void setTag(String tag2) {
        this.tag = tag2;
    }

    public String getTag() {
        return this.tag;
    }
}
