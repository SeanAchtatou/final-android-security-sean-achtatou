package mediba.ad.sdk.android;

import java.util.Map;

public final class AdProxyConnectorFactory {
    public static AdProxyConnector a(String s, String s1, String s2, AdProxyConnectorListener h, int j, Map map, String s3) {
        return new AdProxyURLConnector(s, s1, s2, h, j, null, s3);
    }

    public static AdProxyConnector a(String s, String s1, String s2, AdProxyConnectorListener h) {
        return a(s, s1, s2, h, 5000, null, null);
    }

    public static AdProxyConnector a(String s, String s1, String s2, AdProxyConnectorListener h, int j) {
        AdProxyConnector adconnector = a(s, null, s2, h, 5000, null, null);
        if (adconnector != null) {
            adconnector.setMaxRetry(1);
        }
        return adconnector;
    }

    public static AdProxyConnector a(String s, String s1, String s2) {
        return a(s, s1, s2, null);
    }
}
