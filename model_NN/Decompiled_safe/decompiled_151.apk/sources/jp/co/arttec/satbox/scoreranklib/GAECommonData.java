package jp.co.arttec.satbox.scoreranklib;

public class GAECommonData {
    public static final String DATE = "date";
    public static final String GAE_URI = "http://5.latest.satbox-scorerank.appspot.com/";
    public static final String ID = "id";
    public static final String KIND_GAMES = "kindGames";
    public static final String NAME = "name";
    public static final String SCORE = "score";
    public static final String VERSION = "version";
}
