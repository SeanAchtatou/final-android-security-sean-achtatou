package jp.co.arttec.satbox.scoreranklib;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class RegistScoreController extends AsyncTask<Void, Integer, Boolean> implements GAEController {
    private Activity _activity;
    private int _kindGames;
    private HttpCommunicationListener _listener;
    private String _name;
    private ProgressDialog _progress;
    private long _score;
    private boolean _success;

    public RegistScoreController() {
        this._kindGames = -1;
        this._score = 0;
        this._name = null;
        this._activity = null;
        this._progress = null;
        this._success = false;
        this._listener = null;
    }

    public RegistScoreController(int kindGames, long score, String name) {
        this._kindGames = kindGames;
        this._score = score;
        this._name = name;
        this._activity = null;
        this._progress = null;
        this._success = false;
        this._listener = null;
    }

    public void setActivity(Activity activity) {
        this._activity = activity;
    }

    public Activity getActivity() {
        return this._activity;
    }

    public void setOnFinishListener(HttpCommunicationListener listener) {
        this._listener = listener;
    }

    public void getHighScore() {
    }

    public void registScore() {
        execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Void... params) {
        boolean result = true;
        this._success = false;
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair(GAECommonData.KIND_GAMES, Integer.toString(this._kindGames)));
        nvps.add(new BasicNameValuePair(GAECommonData.SCORE, Long.toString(this._score)));
        nvps.add(new BasicNameValuePair(GAECommonData.NAME, this._name));
        try {
            HttpPost httpPost = new HttpPost(GAECommonData.GAE_URI);
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            new DefaultHttpClient().execute(httpPost);
            this._success = true;
        } catch (Exception e) {
            result = false;
        }
        return Boolean.valueOf(result);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this._activity != null) {
            this._progress = new ProgressDialog(this._activity);
            this._progress.setTitle("");
            this._progress.setMessage("");
            this._progress.setProgressStyle(0);
            this._progress.show();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean result) {
        if (this._progress != null) {
            this._progress.dismiss();
            this._progress = null;
        }
        if (this._listener != null) {
            this._listener.onFinish(this._success);
        }
    }
}
