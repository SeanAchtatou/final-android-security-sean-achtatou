package jp.co.nobot.libAdMaker;

import android.content.Intent;
import android.net.Uri;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Date;

class b extends WebViewClient {
    final /* synthetic */ libAdMaker a;

    b(libAdMaker libadmaker) {
        this.a = libadmaker;
    }

    public void onLoadResource(WebView webView, String str) {
        int indexOf;
        String str2;
        if (this.a.f) {
            webView.stopLoading();
        } else if (this.a.f()) {
            if (str.indexOf("http://images.ad-maker.info/apps") == 0) {
                try {
                    str2 = d.a(String.format("%s%s%s", this.a.l, Long.valueOf(new Date().getTime()), "mXFTQ9fp73rqK5aaOAuQ8yP8"));
                } catch (Exception e) {
                    str2 = "";
                }
                CookieManager.getInstance().setCookie("ad-maker.info", "admaker_sgt=" + str2 + "; domain=" + "ad-maker.info");
            }
            int indexOf2 = str.indexOf("expand.html");
            if (str.indexOf("exp=2") != -1 && !this.a.g) {
                this.a.g = true;
                this.a.c.loadUrl(str);
                this.a.c();
            }
            if (indexOf2 == -1 && (indexOf = str.indexOf("www/delivery/ck.php?oaparams")) != -1 && indexOf < 35) {
                try {
                    webView.stopLoading();
                    this.a.h.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public void onPageFinished(WebView webView, String str) {
        String title = webView.getTitle();
        if ((title != null ? title.indexOf("404") : -1) != -1) {
            this.a.g();
            if (this.a.u != null) {
                this.a.u.a(title);
                return;
            }
            return;
        }
        if (this.a.u != null && !this.a.k) {
            this.a.u.a();
        }
        this.a.k = true;
        CookieSyncManager.getInstance().sync();
        str.indexOf("exp=2");
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        this.a.g();
        if (this.a.u != null) {
            this.a.u.a(str);
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            webView.stopLoading();
            this.a.h.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }
}
