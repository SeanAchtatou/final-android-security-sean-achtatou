package com.a.a;

import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class a extends Thread {
    private Runnable a;
    private File b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;

    public a(File file, String str, String str2, String str3, Runnable runnable) {
        this.b = file;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.a = runnable;
    }

    private String a(String str, String[] strArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < strArr.length; i++) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(String.valueOf(str) + "/" + strArr[i])), 1024);
                char[] cArr = new char[1024];
                while (true) {
                    int read = bufferedReader.read(cArr);
                    if (read <= 0) {
                        break;
                    }
                    stringBuffer.append(cArr, 0, read);
                }
                bufferedReader.close();
            } catch (FileNotFoundException e2) {
                Log.v("Localytics_uploader", "File Not Found");
            } catch (IOException e3) {
                Log.v("Localytics_uploader", "IOException: " + e3.getMessage());
            }
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    private static void a(File file, File file2) {
        if (file2.exists()) {
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                FileOutputStream fileOutputStream = new FileOutputStream(file2, true);
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read <= 0) {
                        fileInputStream.close();
                        fileOutputStream.close();
                        file.delete();
                        return;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
            } catch (FileNotFoundException e2) {
                Log.v("Localytics_uploader", "File not found.");
            } catch (IOException e3) {
                Log.v("Localytics_uploader", "IO Exception: " + e3.getMessage());
            }
        } else {
            file.renameTo(file2);
        }
    }

    private void a(String str) {
        String[] list = this.b.list(new e(this));
        for (int i = 0; i < list.length; i++) {
            a(new File(String.valueOf(str) + "/" + list[i]), new File(String.valueOf(str) + "/" + this.d + list[i]));
        }
    }

    private void b(String str) {
        String[] list = this.b.list(new d(this));
        for (int i = 0; i < list.length; i++) {
            a(new File(String.valueOf(str) + "/" + list[i]), new File(String.valueOf(str) + "/" + this.d + c(list[i])));
        }
    }

    private String c(String str) {
        return String.valueOf(this.c) + str.substring(this.e.length());
    }

    private boolean d(String str) {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://analytics.localytics.com/api/datapoints/bulk");
        try {
            httpPost.setEntity(new StringEntity(str));
            Log.v("Localytics_uploader", "Upload complete. Status: " + defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode());
            return true;
        } catch (UnsupportedEncodingException e2) {
            Log.v("Localytics_uploader", "UnsuppEncodingException: " + e2.getMessage());
            return false;
        } catch (ClientProtocolException e3) {
            Log.v("Localytics_uploader", "ClientProtocolException: " + e3.getMessage());
            return false;
        } catch (IOException e4) {
            Log.v("Localytics_uploader", "IOException: " + e4.getMessage());
            return false;
        }
    }

    public void run() {
        try {
            if (this.b != null && this.b.exists()) {
                String absolutePath = this.b.getAbsolutePath();
                a(absolutePath);
                b(absolutePath);
                String[] list = this.b.list(new c(this));
                int length = list.length;
                String a2 = a(absolutePath, list);
                Log.v("Localytics_uploader", "Attempting to upload " + length + " files.");
                if (d(a2.toString())) {
                    for (int i = 0; i < list.length; i++) {
                        new File(String.valueOf(absolutePath) + "/" + list[i]).delete();
                    }
                }
            }
            if (this.a != null) {
                this.a.run();
            }
        } catch (Exception e2) {
            Log.v("Localytics_uploader", "Swallowing exception: " + e2.getMessage());
        }
    }
}
