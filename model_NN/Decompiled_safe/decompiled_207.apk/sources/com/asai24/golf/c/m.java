package com.asai24.golf.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Xml;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.d;
import com.asai24.golf.e.a;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.xmlpull.v1.XmlPullParser;

public class m {
    private Context a;
    private String b;
    private boolean c;
    private ArrayList d = new ArrayList();
    private ArrayList e = new ArrayList();
    private ArrayList f = new ArrayList();
    private ArrayList g = new ArrayList();
    private ArrayList h = new ArrayList();
    private ArrayList i = new ArrayList();
    private ArrayList j = new ArrayList();

    public m(Context context) {
        this.a = context;
        this.b = PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.yourgolf_account_auth_token_key), "");
    }

    private HashMap a(XmlPullParser xmlPullParser, int i2) {
        HashMap hashMap = new HashMap();
        for (int i3 = 0; i3 < i2; i3++) {
            hashMap.put(xmlPullParser.getAttributeName(i3), xmlPullParser.getAttributeValue(i3));
        }
        return hashMap;
    }

    private void a(HttpResponse httpResponse) {
        String str;
        InputStream inputStream = null;
        try {
            InputStream content = httpResponse.getEntity().getContent();
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(content, null);
                String str2 = "";
                for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                    switch (eventType) {
                        case 2:
                            String name = newPullParser.getName();
                            if (!name.equals("table")) {
                                if (name.equals("record")) {
                                    int attributeCount = newPullParser.getAttributeCount();
                                    if (!str2.equals("courses")) {
                                        if (!str2.equals("tees")) {
                                            if (!str2.equals("holes")) {
                                                if (!str2.equals("rounds")) {
                                                    if (!str2.equals("scores")) {
                                                        if (!str2.equals("score_details")) {
                                                            if (str2.equals("players")) {
                                                                this.j.add(a(newPullParser, attributeCount));
                                                                str = str2;
                                                                break;
                                                            }
                                                        } else {
                                                            this.i.add(a(newPullParser, attributeCount));
                                                            str = str2;
                                                            break;
                                                        }
                                                    } else {
                                                        this.h.add(a(newPullParser, attributeCount));
                                                        str = str2;
                                                        break;
                                                    }
                                                } else {
                                                    this.g.add(a(newPullParser, attributeCount));
                                                    str = str2;
                                                    break;
                                                }
                                            } else {
                                                this.f.add(a(newPullParser, attributeCount));
                                                str = str2;
                                                break;
                                            }
                                        } else {
                                            this.e.add(a(newPullParser, attributeCount));
                                            str = str2;
                                            break;
                                        }
                                    } else {
                                        this.d.add(a(newPullParser, attributeCount));
                                        str = str2;
                                        break;
                                    }
                                }
                            } else {
                                str = newPullParser.getAttributeValue(0);
                                break;
                            }
                        default:
                            str = str2;
                            break;
                    }
                    str2 = str;
                }
                c();
                b a2 = b.a(this.a);
                a2.a(this.d, this.e, this.f, this.g, this.h, this.i, this.j);
                d d2 = a2.d();
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.a).edit();
                edit.putString(this.a.getString(R.string.key_owner_name), d2.c());
                edit.commit();
                d2.close();
                if (content != null) {
                    try {
                        content.close();
                    } catch (IOException e2) {
                    }
                }
            } catch (Throwable th) {
                th = th;
                inputStream = content;
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e3) {
                    }
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
        }
    }

    private void c() {
        a aVar = new a(this.a);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.i.size()) {
                HashMap hashMap = (HashMap) this.i.get(i3);
                hashMap.put("club", aVar.b((String) hashMap.get("club")));
                this.i.set(i3, hashMap);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public String a() {
        this.c = false;
        String string = this.a.getString(R.string.restore_error_unkonwn);
        String deviceId = ((TelephonyManager) this.a.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            throw new RuntimeException("imei is null");
        }
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 5000);
        HttpResponse execute = new DefaultHttpClient(basicHttpParams).execute(new HttpGet("https://golfuser.yourgolf-online.com/userData/restore.xml?auth_token=" + URLEncoder.encode(this.b) + "&deviceId=" + URLEncoder.encode(deviceId)));
        int statusCode = execute.getStatusLine().getStatusCode();
        if (statusCode != 200) {
            return statusCode == 401 ? this.a.getString(R.string.restore_error_e0105) : statusCode == 403 ? this.a.getString(R.string.restore_error_e0201) : string;
        }
        this.c = true;
        a(execute);
        return this.a.getString(R.string.restore_success);
    }

    public boolean b() {
        return this.c;
    }
}
