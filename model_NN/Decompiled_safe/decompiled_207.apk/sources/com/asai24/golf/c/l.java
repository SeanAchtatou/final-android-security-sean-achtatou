package com.asai24.golf.c;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.asai24.golf.R;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class l extends e {
    private Context a;

    public l(Context context) {
        this.a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.asai24.golf.c.e.a(org.apache.http.client.HttpClient, android.content.Context):void
     arg types: [org.apache.http.impl.client.DefaultHttpClient, android.content.Context]
     candidates:
      com.asai24.golf.c.e.a(java.lang.String, java.lang.Object[]):void
      com.asai24.golf.c.e.a(org.apache.http.client.HttpClient, android.content.Context):void */
    public void a() {
        Resources resources = this.a.getResources();
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            a((HttpClient) defaultHttpClient, this.a);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(defaultHttpClient.execute(new HttpGet(resources.getString(R.string.temp_warning_url))).getEntity().getContent()));
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.a.openFileOutput(resources.getString(R.string.information_file), 0), "UTF-8");
            while (true) {
                int read = bufferedReader.read();
                if (read == -1) {
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                    return;
                }
                outputStreamWriter.write(read);
            }
        } catch (Exception e) {
            Log.e("YourGolfInfoAPI", "failed to get YourGolf info.", e);
        }
    }

    public String b() {
        String str = "";
        try {
            FileInputStream openFileInput = this.a.openFileInput(this.a.getResources().getString(R.string.information_file));
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            str = new String(bArr);
        } catch (Exception e) {
            Log.e("YourGolfInfoAPI", "failed to read YourGolf info from cahce.", e);
        }
        return str.replaceAll("\r\n", "\n").trim();
    }
}
