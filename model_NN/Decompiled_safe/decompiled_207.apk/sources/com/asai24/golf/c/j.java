package com.asai24.golf.c;

import android.content.Context;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Xml;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.d;
import com.asai24.golf.a.e;
import com.asai24.golf.a.g;
import com.asai24.golf.a.m;
import com.asai24.golf.a.p;
import com.asai24.golf.a.s;
import com.asai24.golf.a.w;
import com.asai24.golf.e.a;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;
import java.util.HashMap;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class j {
    private Context a;
    private String b;
    private boolean c;
    private String d;

    public j(Context context) {
        this.a = context;
        this.b = PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.yourgolf_account_auth_token_key), "");
    }

    private HashMap b() {
        InputStream inputStream;
        String deviceId = ((TelephonyManager) this.a.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            throw new RuntimeException("imei is null");
        }
        InputStream inputStream2 = null;
        try {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 5000);
            HttpResponse execute = new DefaultHttpClient(basicHttpParams).execute(new HttpGet("https://golfuser.yourgolf-online.com/userData/last_update.xml?auth_token=" + URLEncoder.encode(this.b) + "&deviceId=" + URLEncoder.encode(deviceId)));
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                InputStream content = execute.getEntity().getContent();
                try {
                    XmlPullParser newPullParser = Xml.newPullParser();
                    newPullParser.setInput(content, null);
                    HashMap hashMap = new HashMap();
                    for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                        switch (eventType) {
                            case 2:
                                if (newPullParser.getName().equals("update")) {
                                    hashMap.put(newPullParser.getAttributeValue(null, "name"), Long.valueOf(newPullParser.getAttributeValue(null, "time")));
                                    break;
                                }
                                break;
                        }
                    }
                    if (content != null) {
                        try {
                            content.close();
                        } catch (IOException e) {
                        }
                    }
                    return hashMap;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    inputStream = content;
                    th = th2;
                }
            } else {
                if (statusCode == 401) {
                    this.d = this.a.getString(R.string.backup_error_e0105);
                }
                if (0 != 0) {
                    try {
                        inputStream2.close();
                    } catch (IOException e2) {
                    }
                }
                return null;
            }
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                }
            }
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public String a(String str) {
        this.c = false;
        this.d = this.a.getString(R.string.backup_error_unkonwn);
        HashMap b2 = b();
        if (b2 == null) {
            return this.d;
        }
        String deviceId = ((TelephonyManager) this.a.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            throw new RuntimeException("imei is null");
        }
        b a2 = b.a(this.a);
        m D = a2.D(((Long) b2.get("course_modified")).longValue());
        e E = a2.E(((Long) b2.get("tee_modified")).longValue());
        s F = a2.F(((Long) b2.get("hole_modified")).longValue());
        p G = a2.G(((Long) b2.get("round_modified")).longValue());
        w H = a2.H(((Long) b2.get("score_modified")).longValue());
        g I = a2.I(((Long) b2.get("score_detail_modified")).longValue());
        d J = a2.J(((Long) b2.get("player_modified")).longValue());
        Cursor a3 = a2.a((Long) b2.get("course_deleted"));
        Cursor b3 = a2.b((Long) b2.get("tee_deleted"));
        Cursor c2 = a2.c((Long) b2.get("hole_deleted"));
        Cursor d2 = a2.d((Long) b2.get("round_deleted"));
        Cursor e = a2.e((Long) b2.get("score_deleted"));
        Cursor f = a2.f((Long) b2.get("score_detail_deleted"));
        Cursor g = a2.g((Long) b2.get("player_deleted"));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(byteArrayOutputStream);
        XmlSerializer newSerializer = Xml.newSerializer();
        newSerializer.setOutput(outputStreamWriter);
        try {
            newSerializer.startDocument("UTF-8", true);
            newSerializer.startTag("", "db");
            if (str != null) {
                newSerializer.attribute("", "account", str);
            }
            newSerializer.attribute("", "deviceId", deviceId);
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "courses");
            for (int i = 0; i < D.getCount(); i++) {
                D.moveToPosition(i);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "id", new StringBuilder(String.valueOf(D.b())).toString());
                newSerializer.attribute("", "course_name", D.d());
                newSerializer.attribute("", "club_name", D.c());
                newSerializer.attribute("", "ext_id", D.h());
                newSerializer.attribute("", "ext_type", new StringBuilder(String.valueOf(D.i())).toString());
                newSerializer.attribute("", "created", new StringBuilder(String.valueOf(D.j())).toString());
                newSerializer.attribute("", "modified", new StringBuilder(String.valueOf(D.k())).toString());
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "tees");
            for (int i2 = 0; i2 < E.getCount(); i2++) {
                E.moveToPosition(i2);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "id", new StringBuilder(String.valueOf(E.c())).toString());
                newSerializer.attribute("", "name", E.d());
                newSerializer.attribute("", "ext_id", E.f());
                newSerializer.attribute("", "ext_type", E.g());
                newSerializer.attribute("", "course_id", new StringBuilder(String.valueOf(E.h())).toString());
                newSerializer.attribute("", "created", new StringBuilder(String.valueOf(E.i())).toString());
                newSerializer.attribute("", "modified", new StringBuilder(String.valueOf(E.j())).toString());
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "holes");
            for (int i3 = 0; i3 < F.getCount(); i3++) {
                F.moveToPosition(i3);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "id", new StringBuilder(String.valueOf(F.b())).toString());
                newSerializer.attribute("", "hole_number", new StringBuilder(String.valueOf(F.d())).toString());
                newSerializer.attribute("", "yard", new StringBuilder(String.valueOf(F.l())).toString());
                newSerializer.attribute("", "par", new StringBuilder(String.valueOf(F.e())).toString());
                newSerializer.attribute("", "women_par", F.getString(F.getColumnIndexOrThrow("women_par")));
                newSerializer.attribute("", "handicap", new StringBuilder(String.valueOf(F.h())).toString());
                newSerializer.attribute("", "women_handicap", F.getString(F.getColumnIndexOrThrow("women_handicap")));
                newSerializer.attribute("", "lat", new StringBuilder(String.valueOf(F.m())).toString());
                newSerializer.attribute("", "lng", new StringBuilder(String.valueOf(F.n())).toString());
                newSerializer.attribute("", "tee_id", new StringBuilder(String.valueOf(F.c())).toString());
                newSerializer.attribute("", "created", new StringBuilder(String.valueOf(F.o())).toString());
                newSerializer.attribute("", "modified", new StringBuilder(String.valueOf(F.p())).toString());
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "rounds");
            for (int i4 = 0; i4 < G.getCount(); i4++) {
                G.moveToPosition(i4);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "id", new StringBuilder(String.valueOf(G.b())).toString());
                newSerializer.attribute("", "tee_id", new StringBuilder(String.valueOf(G.d())).toString());
                newSerializer.attribute("", "result_id", new StringBuilder(String.valueOf(G.j())).toString());
                String k = G.k();
                if (k != null && !k.equals("") && !k.equals("null")) {
                    newSerializer.attribute("", "yourgolf_id", new StringBuilder(String.valueOf(G.k())).toString());
                }
                newSerializer.attribute("", "created", new StringBuilder(String.valueOf(G.h())).toString());
                newSerializer.attribute("", "modified", new StringBuilder(String.valueOf(G.i())).toString());
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "scores");
            for (int i5 = 0; i5 < H.getCount(); i5++) {
                H.moveToPosition(i5);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "id", new StringBuilder(String.valueOf(H.b())).toString());
                newSerializer.attribute("", "hole_score", new StringBuilder(String.valueOf(H.c())).toString());
                newSerializer.attribute("", "round_id", new StringBuilder(String.valueOf(H.e())).toString());
                newSerializer.attribute("", "hole_id", new StringBuilder(String.valueOf(H.d())).toString());
                newSerializer.attribute("", "player_id", new StringBuilder(String.valueOf(H.f())).toString());
                newSerializer.attribute("", "game_score", new StringBuilder(String.valueOf(H.g())).toString());
                newSerializer.attribute("", "created", new StringBuilder(String.valueOf(H.h())).toString());
                newSerializer.attribute("", "modified", new StringBuilder(String.valueOf(H.i())).toString());
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            a aVar = new a(this.a);
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "score_details");
            for (int i6 = 0; i6 < I.getCount(); i6++) {
                I.moveToPosition(i6);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "id", new StringBuilder(String.valueOf(I.b())).toString());
                newSerializer.attribute("", "shot_number", new StringBuilder(String.valueOf(I.d())).toString());
                newSerializer.attribute("", "lat", new StringBuilder(String.valueOf(I.g())).toString());
                newSerializer.attribute("", "lng", new StringBuilder(String.valueOf(I.h())).toString());
                newSerializer.attribute("", "club", aVar.a(I.e()));
                newSerializer.attribute("", "shot_result", new StringBuilder(String.valueOf(I.f())).toString());
                newSerializer.attribute("", "score_id", new StringBuilder(String.valueOf(I.c())).toString());
                newSerializer.attribute("", "created", new StringBuilder(String.valueOf(I.i())).toString());
                newSerializer.attribute("", "modified", new StringBuilder(String.valueOf(I.j())).toString());
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "players");
            for (int i7 = 0; i7 < J.getCount(); i7++) {
                J.moveToPosition(i7);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "id", new StringBuilder(String.valueOf(J.b())).toString());
                newSerializer.attribute("", "name", J.c());
                newSerializer.attribute("", "ownner_flag", new StringBuilder(String.valueOf(J.d())).toString());
                newSerializer.attribute("", "created", new StringBuilder(String.valueOf(J.e())).toString());
                newSerializer.attribute("", "modified", new StringBuilder(String.valueOf(J.f())).toString());
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "course_delete");
            for (int i8 = 0; i8 < a3.getCount(); i8++) {
                a3.moveToPosition(i8);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "deleted_id", a3.getString(a3.getColumnIndexOrThrow("deleted_id")));
                newSerializer.attribute("", "deleted_date", a3.getString(a3.getColumnIndexOrThrow("deleted_date")));
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "tee_delete");
            for (int i9 = 0; i9 < b3.getCount(); i9++) {
                b3.moveToPosition(i9);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "deleted_id", b3.getString(b3.getColumnIndexOrThrow("deleted_id")));
                newSerializer.attribute("", "deleted_date", b3.getString(b3.getColumnIndexOrThrow("deleted_date")));
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "hole_delete");
            for (int i10 = 0; i10 < c2.getCount(); i10++) {
                c2.moveToPosition(i10);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "deleted_id", c2.getString(c2.getColumnIndexOrThrow("deleted_id")));
                newSerializer.attribute("", "deleted_date", c2.getString(c2.getColumnIndexOrThrow("deleted_date")));
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "round_delete");
            for (int i11 = 0; i11 < d2.getCount(); i11++) {
                d2.moveToPosition(i11);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "deleted_id", d2.getString(d2.getColumnIndexOrThrow("deleted_id")));
                newSerializer.attribute("", "deleted_date", d2.getString(d2.getColumnIndexOrThrow("deleted_date")));
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "score_delete");
            for (int i12 = 0; i12 < e.getCount(); i12++) {
                e.moveToPosition(i12);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "deleted_id", e.getString(e.getColumnIndexOrThrow("deleted_id")));
                newSerializer.attribute("", "deleted_date", e.getString(e.getColumnIndexOrThrow("deleted_date")));
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "score_detail_delete");
            for (int i13 = 0; i13 < f.getCount(); i13++) {
                f.moveToPosition(i13);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "deleted_id", f.getString(f.getColumnIndexOrThrow("deleted_id")));
                newSerializer.attribute("", "deleted_date", f.getString(f.getColumnIndexOrThrow("deleted_date")));
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.startTag("", "table");
            newSerializer.attribute("", "name", "player_delete");
            for (int i14 = 0; i14 < g.getCount(); i14++) {
                g.moveToPosition(i14);
                newSerializer.startTag("", "record");
                newSerializer.attribute("", "deleted_id", g.getString(g.getColumnIndexOrThrow("deleted_id")));
                newSerializer.attribute("", "deleted_date", g.getString(g.getColumnIndexOrThrow("deleted_date")));
                newSerializer.endTag("", "record");
            }
            newSerializer.endTag("", "table");
            newSerializer.endTag("", "db");
            newSerializer.endDocument();
            outputStreamWriter.flush();
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 5000);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            HttpPost httpPost = new HttpPost("https://golfuser.yourgolf-online.com/userData/store.xml?auth_token=" + URLEncoder.encode(this.b));
            httpPost.setEntity(new ByteArrayEntity(byteArrayOutputStream.toByteArray()));
            int statusCode = defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode();
            if (statusCode == 204) {
                this.c = true;
                this.d = this.a.getString(R.string.backup_success);
            } else if (statusCode == 401) {
                this.d = this.a.getString(R.string.backup_error_e0105);
            }
            String str2 = this.d;
            byteArrayOutputStream.close();
            outputStreamWriter.close();
            D.close();
            E.close();
            F.close();
            G.close();
            H.close();
            I.close();
            J.close();
            a3.close();
            b3.close();
            c2.close();
            d2.close();
            e.close();
            f.close();
            g.close();
            return str2;
        } catch (Throwable th) {
            byteArrayOutputStream.close();
            outputStreamWriter.close();
            D.close();
            E.close();
            F.close();
            G.close();
            H.close();
            I.close();
            J.close();
            a3.close();
            b3.close();
            c2.close();
            d2.close();
            e.close();
            f.close();
            g.close();
            throw th;
        }
    }

    public boolean a() {
        return this.c;
    }
}
