package com.asai24.golf.c;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.b.c;
import com.asai24.golf.b.d;
import com.asai24.golf.b.e;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class g extends e {
    String a = "";
    InputStream b;
    InputStreamReader c;
    BufferedReader d;
    XmlPullParserFactory e;
    XmlPullParser f;
    String g;
    private Context h;
    private Resources i = this.h.getResources();

    public g(Context context) {
        this.h = context;
    }

    private void b() {
        String d2 = GolfApplication.d();
        String e2 = GolfApplication.e();
        if (d2.length() == 0 || e2.length() == 0) {
            throw new c();
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.i.getString(R.string.oob_get_session_api));
        stringBuffer.append("?username=" + URLEncoder.encode(d2));
        stringBuffer.append("&password=" + URLEncoder.encode(e2));
        stringBuffer.append("&dev=" + URLEncoder.encode(this.i.getString(R.string.oob_api_key)));
        this.g = stringBuffer.toString();
        a("OobGetSessionAPI", "requestUrl" + this.g);
    }

    private void c() {
        try {
            HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(this.g));
            a("OobGetSessionAPI", "Response status code: " + execute);
            this.b = execute.getEntity().getContent();
            this.c = new InputStreamReader(this.b);
            this.d = new BufferedReader(this.c);
            this.e = XmlPullParserFactory.newInstance();
            this.e.setNamespaceAware(true);
            this.f = this.e.newPullParser();
            this.f.setInput(this.d);
        } catch (Exception e2) {
            throw e2;
        }
    }

    private void d() {
        try {
            int eventType = this.f.getEventType();
            while (eventType != 1) {
                if (!(eventType == 0 || eventType == 1)) {
                    if (eventType == 2) {
                        a("OobGetSessionAPI", "Start tag " + this.f.getName());
                        if (this.f.getName().equals("error")) {
                            throw new e();
                        }
                    } else if (eventType != 3 && eventType == 4) {
                        a("OobGetSessionAPI", "TAG Text :" + this.f.getText());
                        this.a = this.f.getText();
                        a("OobGetSessionAPI", "session:" + this.a);
                    }
                }
                eventType = this.f.next();
            }
            this.d.close();
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
            throw new Exception(e2);
        } catch (IOException e3) {
            e3.printStackTrace();
            throw new Exception(e3);
        }
    }

    public String a() {
        try {
            b();
            c();
            d();
            return this.a;
        } catch (c e2) {
            Log.e("OobGetSessionAPI", "NoSettingsException: ", e2);
            throw e2;
        } catch (e e3) {
            Log.e("OobGetSessionAPI", "Exception: ", e3);
            throw e3;
        } catch (Exception e4) {
            Log.e("OobGetSessionAPI", "Exception: ", e4);
            throw new d(e4.getMessage());
        }
    }
}
