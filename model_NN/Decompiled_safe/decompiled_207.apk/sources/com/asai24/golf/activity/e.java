package com.asai24.golf.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

class e extends Handler {
    final /* synthetic */ YourGolfAccountUpdate a;

    e(YourGolfAccountUpdate yourGolfAccountUpdate) {
        this.a = yourGolfAccountUpdate;
    }

    public void handleMessage(Message message) {
        this.a.v.dismiss();
        this.a.v = (ProgressDialog) null;
        Bundle data = message.getData();
        this.a.a(data.getString("msg"), data.getBoolean("result"));
    }
}
