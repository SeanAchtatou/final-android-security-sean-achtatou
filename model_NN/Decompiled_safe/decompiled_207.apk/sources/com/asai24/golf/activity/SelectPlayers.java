package com.asai24.golf.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.asai24.golf.GolfApplication;
import com.asai24.golf.R;
import com.asai24.golf.a.b;
import com.asai24.golf.a.d;
import com.asai24.golf.a.s;
import com.asai24.golf.d.a;
import com.asai24.golf.d.h;
import com.asai24.golf.d.l;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SelectPlayers extends GolfActivity implements View.OnClickListener {
    public static String[] b = {"_id", "name"};
    public static int c = 0;
    public static int d = 1;
    Resources e;
    private GolfApplication f;
    /* access modifiers changed from: private */
    public b g;
    private ListView h;
    private Button i;
    private Button j;
    private Button k;
    private Button l;
    private long m;
    private long n;
    /* access modifiers changed from: private */
    public List o = new ArrayList();
    /* access modifiers changed from: private */
    public List p = new ArrayList();
    /* access modifiers changed from: private */
    public long q;
    /* access modifiers changed from: private */
    public String r;
    private EditText s;
    private d t;
    private cj u;

    public void a(int i2) {
        c(this.e.getString(i2));
    }

    public void d() {
        this.t = this.g.a(this.o);
        Collections.sort(this.o);
        if (this.u == null) {
            this.u = new cj(this, this, this.t);
        } else {
            this.u.changeCursor(this.t);
        }
        this.h.setAdapter((ListAdapter) this.u);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_btn /*2131428169*/:
                if (this.o.size() >= 4) {
                    a((int) R.string.cannnot_add_member);
                    return;
                } else {
                    showDialog(0);
                    return;
                }
            case R.id.address_book_btn /*2131428170*/:
                if (this.o.size() >= 4) {
                    a((int) R.string.cannnot_add_member);
                    return;
                } else {
                    showDialog(1);
                    return;
                }
            case R.id.player_history_btn /*2131428171*/:
                if (this.o.size() >= 4) {
                    a((int) R.string.cannnot_add_member);
                    return;
                } else {
                    showDialog(3);
                    return;
                }
            case R.id.play_btn /*2131428172*/:
                this.f.a("/score_entry");
                this.f.b("/score_entry");
                this.f.a("AddYourFriends", "Play", "Play", 1);
                long a = this.g.a(this.m, this.n, this.o);
                s a2 = this.g.a(this.n, 1);
                long b2 = a2.b();
                a2.close();
                Intent intent = new Intent(this, ScoreEntryWithMap.class);
                intent.putExtra("playing_course_id", this.m);
                intent.putExtra("playing_tee_id", this.n);
                intent.putExtra("playing_round_id", a);
                intent.putExtra("playing_hole_id", b2);
                long[] jArr = new long[this.o.size()];
                for (int i2 = 0; i2 < jArr.length; i2++) {
                    jArr[i2] = ((Long) this.o.get(i2)).longValue();
                }
                intent.putExtra("player_ids", jArr);
                startActivity(intent);
                setResult(-1);
                finish();
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.select_players);
        this.f = (GolfApplication) getApplication();
        this.g = b.a(this);
        this.e = getResources();
        Bundle extras = getIntent().getExtras();
        this.m = extras.getLong("playing_course_id");
        this.n = extras.getLong("playing_tee_id");
        this.h = (ListView) findViewById(R.id.entry_players);
        this.i = (Button) findViewById(R.id.play_btn);
        this.j = (Button) findViewById(R.id.add_btn);
        this.k = (Button) findViewById(R.id.address_book_btn);
        this.l = (Button) findViewById(R.id.player_history_btn);
        this.a = Toast.makeText(this, "", 0);
        this.i.setOnClickListener(this);
        this.j.setOnClickListener(this);
        this.k.setOnClickListener(this);
        this.l.setOnClickListener(this);
        d d2 = this.g.d();
        this.o.add(Long.valueOf(d2.b()));
        d2.close();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        Cursor cursor;
        int a;
        switch (i2) {
            case 0:
                View inflate = LayoutInflater.from(this).inflate((int) R.layout.alert_dialog_text_entry, (ViewGroup) null);
                ((EditText) inflate.findViewById(R.id.name_edit)).setFilters(new InputFilter[]{new InputFilter.LengthFilter(10), new a(18)});
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.name_dialog_title).setView(inflate).setPositiveButton((int) R.string.ok, new bm(this, inflate)).setNegativeButton("Cancel", new bp(this)).setIcon(0).create();
            case 1:
                ArrayList arrayList = new ArrayList();
                if (Integer.parseInt(Build.VERSION.SDK) < 5) {
                    Cursor a2 = l.a(this);
                    cursor = a2;
                    a = l.a(a2);
                } else {
                    Cursor a3 = h.a(this);
                    cursor = a3;
                    a = h.a(a3);
                }
                if (cursor != null) {
                    int count = cursor.getCount();
                    for (int i3 = 0; i3 < count; i3++) {
                        cursor.moveToPosition(i3);
                        String string = cursor.getString(a);
                        if (!(string == null || string.length() == 0)) {
                            arrayList.add(string);
                        }
                    }
                    cursor.close();
                }
                String[] strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
                return new AlertDialog.Builder(this).setTitle((int) R.string.choose_from_contacts_title).setItems(strArr, new bq(this, strArr)).create();
            case 2:
                View inflate2 = LayoutInflater.from(this).inflate((int) R.layout.alert_dialog_text_entry, (ViewGroup) null);
                InputFilter[] inputFilterArr = {new InputFilter.LengthFilter(10), new a(18)};
                this.s = (EditText) inflate2.findViewById(R.id.name_edit);
                this.s.setFilters(inputFilterArr);
                return new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.name_dialog_title).setView(inflate2).setPositiveButton((int) R.string.ok, new bn(this, inflate2)).setNegativeButton("Cancel", new bo(this)).setIcon(0).create();
            case 3:
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                d b2 = this.g.b();
                while (!b2.isAfterLast()) {
                    Long valueOf = Long.valueOf(b2.b());
                    String c2 = b2.c();
                    if (!arrayList3.contains(c2) && !this.o.contains(valueOf)) {
                        arrayList2.add(valueOf);
                        arrayList3.add(c2);
                    }
                    b2.moveToNext();
                }
                b2.close();
                String[] strArr2 = (String[]) arrayList3.toArray(new String[arrayList3.size()]);
                return new AlertDialog.Builder(this).setTitle((int) R.string.choose_from_history).setItems(strArr2, new bj(this, (Long[]) arrayList2.toArray(new Long[arrayList2.size()]), strArr2)).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.u = null;
        this.f.a();
        this.f.c();
        com.asai24.golf.d.d.a(findViewById(R.id.select_players));
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.t.close();
        this.t = null;
        this.f.b();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        switch (i2) {
            case 2:
                this.s.setText(this.r);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        d();
    }
}
