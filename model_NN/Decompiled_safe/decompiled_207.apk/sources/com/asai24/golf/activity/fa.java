package com.asai24.golf.activity;

import android.content.DialogInterface;
import android.content.Intent;

class fa implements DialogInterface.OnClickListener {
    final /* synthetic */ SearchCourse a;

    fa(SearchCourse searchCourse) {
        this.a = searchCourse;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        int i2 = this.a.w.c() == 0 ? 2 : 1;
        Intent intent = new Intent(this.a, SearchCourse.class);
        intent.putExtra("APP_NAME", this.a.getPackageName());
        intent.putExtra("COURSE_MODE", i2);
        intent.putExtra("MODE", 0);
        intent.putExtra("CLUB_NAME", this.a.w.e());
        this.a.startActivity(intent);
    }
}
